CREATE OR REPLACE PACKAGE BODY APPS.TAC_PO_REQUISITIONS_PKG
AS
   FUNCTION read_data_to_stage (
      p_operation_unit    IN     VARCHAR2 DEFAULT 'All',
      p_request_id        IN     NUMBER,
      p_user_id           IN     NUMBER,
      p_file_name         IN     VARCHAR,
      o_dupplicate_file      OUT BOOLEAN,
      o_error                OUT VARCHAR2)
      RETURN tac_po_req_stg_tbl_type
   IS
      in_file         UTL_FILE.file_type;
      filename        VARCHAR2 (255);
      v_new_line      VARCHAR2 (1000);
      l_file_seq      NUMBER;

      i               PLS_INTEGER := 0;
      j               PLS_INTEGER;

      err_flag        CHAR (1);
      msg_errors      VARCHAR2 (1000);
      msg_rec         VARCHAR2 (1000);
      count_file      NUMBER := 0;

      l_lines_in      TAC_PO_REQUISITIONS_PKG.tac_po_req_stg_tbl_type;
      l_line_in_rec   TAC_PO_REQUISITIONS_PKG.tac_po_req_stg_rec_type;
   BEGIN
      o_dupplicate_file := FALSE;
      o_error := '';

      /*
      BEGIN
         SELECT SUBSTR (
                   SUBSTR (NVL (file_name, 'SMT002yyyymmdd000.txt'), -7),
                   1,
                   3)
           INTO filename
           FROM (  SELECT seq_id, file_name, import_date
                     FROM tac_po_req_import_stg
                    WHERE error_flag = 'N'
                 ORDER BY seq_id DESC)
          WHERE ROWNUM = 1 AND TRUNC (import_date) = TRUNC (SYSDATE);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            filename := '000';
      END;

      write_log ('File name 1.' || filename, 'LOG');
      l_file_seq := TO_NUMBER (filename) + 1;
      filename :=
            'SMT002'
         || TRIM (TO_CHAR (SYSDATE, 'yyyymmdd'))
         || TRIM (TO_CHAR (l_file_seq, '000'))
         || '.txt';
      write_log ('File name 2.' || filename, 'LOG');
      */

      in_file := UTL_FILE.FOPEN_NCHAR ('SMT002_IN', p_file_name, 'r');

      LOOP
         err_flag := 'N';
         msg_rec := '';

         BEGIN
            UTL_FILE.GET_LINE_NCHAR (in_file, v_new_line);
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               EXIT;
            WHEN OTHERS
            THEN
               err_flag := 'Y';
               msg_rec := SQLCODE || '-' || SQLERRM;
         END;

         j := 1;
         l_line_in_rec.seq_id := tac_req_import_temp_s.NEXTVAL;
         l_line_in_rec.request_id := p_request_id;
         l_line_in_rec.imported_by := p_user_id;
         l_line_in_rec.last_updated_by := p_user_id;

         -- IF (i != 0)
         -- THEN
         --IF (regexp_substr(v_new_line, '(^|\s|\W)TRANSACTION_ID($|\s|\W)', 1, 1, 'i') is null)
         --IF (SUBSTR (v_new_line, 1, 1) <> 'T')
         --THEN
         FOR item IN (    SELECT TRIM (REGEXP_SUBSTR (v_new_line,
                                                      '[^|]+',
                                                      1,
                                                      LEVEL))
                                    col_val
                            FROM DUAL
                      CONNECT BY REGEXP_SUBSTR (v_new_line,
                                                '[^|]+',
                                                1,
                                                LEVEL)
                                    IS NOT NULL)
         LOOP
            IF (j = 1)
            THEN
               BEGIN
                  l_line_in_rec.transaction_id := item.col_val;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'TRANSACTION_ID' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 2)
            THEN
               BEGIN
                  l_line_in_rec.interface_source_code := item.col_val;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'INTERFACE_SOURCE_CODE' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 3)
            THEN
               BEGIN
                  l_line_in_rec.batch_name := item.col_val;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'BATCH_NAME' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 4)
            THEN
               BEGIN
                  l_line_in_rec.creation_date :=
                     TO_DATE (item.col_val, 'DDMMYYYY');
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'CREATION_DATE' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 5)
            THEN
               BEGIN
                  l_line_in_rec.operating_unit := item.col_val;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'OPERATING_UNIT' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 6)
            THEN
               BEGIN
                  l_line_in_rec.source_type_code := item.col_val;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'SOURCE_TYPE_CODE' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 7)
            THEN
               BEGIN
                  l_line_in_rec.requisition_type := item.col_val;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'REQUISITION_TYPE' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 8)
            THEN
               BEGIN
                  l_line_in_rec.header_description := substr(item.col_val, 0, 240); /*Modify by spw@ice 4-Jul-2017*/
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'HEADER_DESCRIPTION' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 9)
            THEN
               BEGIN
                  l_line_in_rec.line_number := item.col_val;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'LINE_NUMBER' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 10)
            THEN
               BEGIN
                  l_line_in_rec.destination_type_code := item.col_val;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'DESTINATION_TYPE_CODE' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 11)
            THEN
               BEGIN
                  l_line_in_rec.line_type := item.col_val;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'LINE_TYPE' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 12)
            THEN
               BEGIN
                  l_line_in_rec.categories := item.col_val;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'CATEGORIES' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 13)
            THEN
               BEGIN
                  l_line_in_rec.line_description := substr(item.col_val, 0, 240); /*Modify by spw@ice 6-Jul-2017*/
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'LINE_DESCRIPTION' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 14)
            THEN
               BEGIN
                  l_line_in_rec.quantity := item.col_val;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'QUANTITY' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 15)
            THEN
               BEGIN
                  l_line_in_rec.unit_price := item.col_val;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'UNIT_PRICE' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 16)
            THEN
               BEGIN
                  l_line_in_rec.unit_of_measure := item.col_val;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'UNIT_OF_MEASURE' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 17)
            THEN
               BEGIN
                  l_line_in_rec.tax_code := item.col_val;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'TAX_CODE' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 18)
            THEN
               BEGIN
                  l_line_in_rec.urgent_flag := item.col_val;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'URGENT_FLAG' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 19)
            THEN
               BEGIN
                  l_line_in_rec.group_code := item.col_val;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'GROUP_CODE' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 20)
            THEN
               BEGIN
                  l_line_in_rec.user_name := item.col_val;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'USER_NAME' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 21)
            THEN
               BEGIN
                  l_line_in_rec.justification := substr(item.col_val, 0, 480); /*Modify by spw@ice 12-Jul-2017*/
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'JUSTIFICATION' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 22)
            THEN
               BEGIN
                  l_line_in_rec.note_to_buyer := item.col_val;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'NOTE_TO_BUYER' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 23)
            THEN
               BEGIN
                  l_line_in_rec.destination_organization_code := item.col_val;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec :=
                        'DESTINATION_ORGANIZATION_CODE' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 24)
            THEN
               BEGIN
                  l_line_in_rec.deliver_to_location_code := item.col_val;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'DELIVER_TO_LOCATION_CODE' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 25)
            THEN
               BEGIN
                  l_line_in_rec.autosource_flag := item.col_val;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'AUTOSOURCE_FLAG' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 26)
            THEN
               BEGIN
                  l_line_in_rec.suggested_vendor_code := item.col_val;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'SUGGESTED_VENDOR_CODE' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 27)
            THEN
               BEGIN
                  l_line_in_rec.suggested_vendor_site := item.col_val;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'SUGGESTED_VENDOR_SITE' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 28)
            THEN
               BEGIN
                  l_line_in_rec.need_by_date :=
                     TO_DATE (item.col_val, 'DDMMYYYY');
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'NEED_BY_DATE' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 29)
            THEN
               BEGIN
                  l_line_in_rec.gl_date := TO_DATE (item.col_val, 'DDMMYYYY');
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'GL_DATE' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 30)
            THEN
               BEGIN
                  l_line_in_rec.source_document_type := item.col_val;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'SOURCE_DOCUMENT_TYPE' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 31)
            THEN
               BEGIN
                  l_line_in_rec.source_document_number := item.col_val;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec := 'SOURCE_DOCUMENT_NUMBER' || '-' || SQLERRM;
               END;
            END IF;

            IF (j = 32)
            THEN
               BEGIN
                  l_line_in_rec.source_document_line_number :=
                     CHR (ASCII (item.col_val));
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     err_flag := 'Y';
                     msg_rec :=
                        'SOURCE_DOCUMENT_LINE_NUMBER' || '-' || SQLERRM;
               END;
            END IF;

            j := j + 1;
         END LOOP;


         msg_rec := msg_rec || CHR (13) || msg_rec;
         l_line_in_rec.error_flag := err_flag;
         l_line_in_rec.error_message := msg_rec;
         l_line_in_rec.file_name := p_file_name;

         INSERT INTO tac_po_req_import_stg
              VALUES l_line_in_rec;

         l_lines_in (i) := l_line_in_rec;
         --END IF;

         write_log ('Insert to stage table. = ' || TO_CHAR (i - 1), 'LOG');


         --END IF;

         i := i + 1;
      END LOOP;

      --DELETE FROM tac_po_req_import_stg where error_message like '%>0<%';
      UTL_FILE.fclose (in_file);
      RETURN l_lines_in;
   EXCEPTION
      WHEN OTHERS
      THEN
         RAISE;
         write_log ('File not existing.', 'LOG');
         RETURN l_lines_in;
   END;

   FUNCTION binding_stg_to_temp (
      p_po_req_stg_in IN tac_po_req_import_stg%ROWTYPE)
      RETURN tac_po_req_import_temp%ROWTYPE
   IS
      l_line_in_rec   tac_po_req_import_temp%ROWTYPE;
   BEGIN
      l_line_in_rec.seq_id := p_po_req_stg_in.seq_id;
      l_line_in_rec.request_id := p_po_req_stg_in.request_id;
      l_line_in_rec.transaction_id := p_po_req_stg_in.transaction_id;
      l_line_in_rec.operating_unit := p_po_req_stg_in.operating_unit;
      l_line_in_rec.interface_source_code :=
         p_po_req_stg_in.interface_source_code;
      l_line_in_rec.batch_id := p_po_req_stg_in.batch_name;
      l_line_in_rec.org_id := NULL;
      l_line_in_rec.source_type_code := p_po_req_stg_in.source_type_code;
      l_line_in_rec.requisition_type := p_po_req_stg_in.requisition_type;
      l_line_in_rec.header_description := p_po_req_stg_in.header_description;
      l_line_in_rec.line_no := p_po_req_stg_in.line_number;
      l_line_in_rec.preparer_name := 'SMT Dummy Buyer,';       --Fixed for SMT
      l_line_in_rec.destination_type_code :=
         p_po_req_stg_in.destination_type_code;
      l_line_in_rec.line_type := p_po_req_stg_in.line_type;
      l_line_in_rec.category_id := NULL;
      l_line_in_rec.item_description := p_po_req_stg_in.line_description;
      l_line_in_rec.quantity := p_po_req_stg_in.quantity;
      l_line_in_rec.unit_price := p_po_req_stg_in.unit_price;
      l_line_in_rec.unit_of_measure := p_po_req_stg_in.unit_of_measure;
      l_line_in_rec.authorization_status := 'INCOMPLETE';        --'APPROVED';
      l_line_in_rec.urgent_flag := p_po_req_stg_in.urgent_flag;
      l_line_in_rec.group_code := p_po_req_stg_in.group_code;
      l_line_in_rec.justification := p_po_req_stg_in.justification;
      l_line_in_rec.note_to_buyer := p_po_req_stg_in.note_to_buyer;
      l_line_in_rec.destination_organization_code :=
         p_po_req_stg_in.destination_organization_code;
      l_line_in_rec.deliver_to_location_code :=
         p_po_req_stg_in.deliver_to_location_code;
      l_line_in_rec.autosource_flag := p_po_req_stg_in.autosource_flag;
      l_line_in_rec.need_by_date := p_po_req_stg_in.need_by_date;
      l_line_in_rec.gl_date := p_po_req_stg_in.gl_date;
      l_line_in_rec.preparer_id := NULL;
      l_line_in_rec.charge_account_id := NULL;
      l_line_in_rec.deliver_to_requestor_id := NULL;
      l_line_in_rec.suggested_vendor_id := NULL;
      l_line_in_rec.suggested_vendor_site_id := NULL;
      l_line_in_rec.tax_code_id := NULL;
      l_line_in_rec.source_document_type :=
         p_po_req_stg_in.source_document_type;
      l_line_in_rec.source_document_number :=
         p_po_req_stg_in.source_document_number;

      BEGIN
         l_line_in_rec.source_document_line_number :=
            TO_NUMBER (
               CHR (ASCII (p_po_req_stg_in.source_document_line_number)));
      EXCEPTION
         WHEN OTHERS
         THEN
            write_log (
                  'character to number conversion error for SOURCE_DOCUMENT_LINE_NUMBER : '
               || ' ASCII-1 :'
               || ASCII (p_po_req_stg_in.source_document_line_number)
               || ' ASCII-2 :'
               || TO_NUMBER (CHR (ASCII ('1'))),
               'LOG');
      END;

      l_line_in_rec.last_updated_by := p_po_req_stg_in.last_updated_by;
      l_line_in_rec.imported_by := p_po_req_stg_in.imported_by;
      l_line_in_rec.creation_date := p_po_req_stg_in.creation_date;

      RETURN l_line_in_rec;
   EXCEPTION
      WHEN OTHERS
      THEN
         RAISE;
   END;

   FUNCTION pre_interface_data (p_request_id   IN NUMBER,
                                p_file_name    IN VARCHAR2)
      RETURN BOOLEAN
   IS
      l_accrual_acct   NUMBER;

      CURSOR fetch_pr (
         l_request_id IN NUMBER)
      IS
           SELECT t.*
             FROM tac_po_req_import_temp t, tac_po_req_import_stg s
            WHERE     t.seq_id = s.seq_id
                  AND t.request_id = l_request_id
                  AND s.file_name = p_file_name
                  AND t.error_flag = 'N'
         ORDER BY t.seq_id ASC;
   BEGIN
      FOR rec IN fetch_pr (p_request_id)
      LOOP
         SELECT accrued_code_combination_id
           INTO l_accrual_acct
           FROM po_system_parameters_all
          WHERE org_id = rec.org_id;

         BEGIN
            INSERT
              INTO PO_REQUISITIONS_INTERFACE_ALL (interface_source_code,
                                                  requisition_type,
                                                  batch_id,
                                                  group_code,
                                                  org_id,
                                                  tax_code_id,
                                                  need_by_date,
                                                  gl_date,
                                                  note_to_buyer,
                                                  justification,
                                                  urgent_flag,
                                                  header_description,
                                                  destination_type_code,
                                                  authorization_status,
                                                  preparer_id,
                                                  charge_account_id,
                                                  source_type_code,
                                                  unit_of_measure,
                                                  line_type_id,
                                                  category_id,
                                                  unit_price,
                                                  quantity,
                                                  destination_organization_id,
                                                  deliver_to_location_id,
                                                  deliver_to_requestor_id,
                                                  suggested_vendor_id,
                                                  suggested_vendor_site_id,
                                                  item_description,
                                                  autosource_flag,
                                                  accrual_account_id,
                                                  variance_account_id,
                                                  budget_account_id,
                                                  header_attribute15,
                                                  document_type_code,
                                                  rate_type,
                                                  rate_date,
                                                  autosource_doc_header_id,
                                                  autosource_doc_line_num,
                                                  note_to_approver) --## 05/07/2017 ## SURACHAI S. 
            VALUES (rec.interface_source_code,
                    rec.requisition_type,
                    rec.batch_id,
                    rec.batch_id,
                    rec.org_id,
                    rec.tax_code_id,
                    rec.need_by_date,
                    rec.gl_date,
                    rec.note_to_buyer,
                    rec.justification,
                    rec.urgent_flag,
                    rec.header_description,
                    UPPER (rec.destination_type_code),
                    rec.authorization_status,
                    rec.preparer_id,
                    rec.charge_account_id,
                    rec.source_type_code,
                    rec.unit_of_measure,
                    rec.line_type_id,
                    rec.category_id,
                    rec.unit_price,
                    rec.quantity,
                    rec.destination_organization_id,
                    rec.deliver_to_location_id,
                    rec.deliver_to_requestor_id,
                    rec.suggested_vendor_id,
                    rec.suggested_vendor_site_id,
                    rec.item_description,
                    rec.autosource_flag,
                    l_accrual_acct,
                    rec.charge_account_id,
                    rec.charge_account_id,
                    rec.batch_id,
                    UPPER (rec.source_document_type),
                    'User',
                    SYSDATE,
                    rec.source_doc_header_id,
                    rec.source_document_line_number,
                    rec.justification); --## 05/07/2017 ## SURACHAI S. 
         EXCEPTION
            WHEN OTHERS
            THEN
               write_log (
                     'Insert to PO_REQUISITIONS_INTERFACE_ALL '
                  || '-'
                  || rec.source_document_line_number,
                  'LOG');
               write_log (
                     'Insert to PO_REQUISITIONS_INTERFACE_ALL '
                  || '-'
                  || SQLERRM,
                  'LOG');
               RETURN FALSE;
         END;
      END LOOP;

      RETURN TRUE;
   EXCEPTION
      WHEN OTHERS
      THEN
         RAISE;
         write_log (SQLCODE || '-' || SQLERRM, 'LOG');
         RETURN FALSE;
   END;

   PROCEDURE insert_temp (p_rec IN tac_po_req_import_temp%ROWTYPE)
   IS
   BEGIN
      INSERT INTO tac_po_req_import_temp
           VALUES p_rec;
   EXCEPTION
      WHEN OTHERS
      THEN
         RAISE;
   END;

   FUNCTION validate_stg_to_temp (
      p_operation_unit   IN VARCHAR2 DEFAULT 'All',
      p_req_recs         IN tac_po_req_import_stg%ROWTYPE)
      RETURN BOOLEAN
   IS
      msg_rec                    VARCHAR2 (1000);
      tac_po_req_temp_rec_type   tac_po_req_import_temp%ROWTYPE;

      l_vat_code                 VARCHAR2 (20);
      l_rows                     NUMBER;
   BEGIN
      --FOR i IN p_req_recs.FIRST .. p_req_recs.LAST
      --LOOP
      tac_po_req_temp_rec_type := binding_stg_to_temp (p_req_recs);
      tac_po_req_temp_rec_type.error_flag := 'N';
      tac_po_req_temp_rec_type.error_message := '';
      msg_rec := NULL;
      l_vat_code := '';

      --write_log ('I := ' || i, 'LOG');

      /* Validation and get value.*/
      BEGIN
         SELECT hr.organization_id
           INTO tac_po_req_temp_rec_type.org_id
           FROM hr_all_organization_units hr
          WHERE UPPER (hr.name) = TRIM (UPPER (p_req_recs.operating_unit));
      EXCEPTION
         WHEN OTHERS
         THEN
            tac_po_req_temp_rec_type.error_flag := 'Y';
            msg_rec :=
                  CASE WHEN msg_rec IS NOT NULL THEN msg_rec || CHR (13) END
               || p_req_recs.operating_unit
               || ':Organization Units validation failed.';
      END;

      IF    (UPPER (TRIM (tac_po_req_temp_rec_type.source_type_code)) =
                'SUPPLIER')
         OR (UPPER (TRIM (tac_po_req_temp_rec_type.source_type_code)) =
                'VENDOR')
      THEN
         tac_po_req_temp_rec_type.source_type_code := 'VENDOR';
      ELSE
         tac_po_req_temp_rec_type.error_flag := 'Y';
         msg_rec :=
               CASE WHEN msg_rec IS NOT NULL THEN msg_rec || CHR (13) END
            || p_req_recs.source_type_code
            || ':Source type code validation failed.';
      END IF;

      IF (UPPER (TRIM (tac_po_req_temp_rec_type.requisition_type)) <>
             'PURCHASE REQUISITION')
      THEN
         tac_po_req_temp_rec_type.error_flag := 'Y';
         msg_rec :=
               CASE WHEN msg_rec IS NOT NULL THEN msg_rec || CHR (13) END
            || p_req_recs.requisition_type
            || ':Requisition Type validation failed.';
      ELSE
         tac_po_req_temp_rec_type.requisition_type := 'PURCHASE';
      END IF;

      IF tac_po_req_temp_rec_type.creation_date >
            tac_po_req_temp_rec_type.need_by_date
      THEN
         tac_po_req_temp_rec_type.error_flag := 'Y';
         msg_rec :=
               CASE WHEN msg_rec IS NOT NULL THEN msg_rec || CHR (13) END
            || TO_CHAR (p_req_recs.need_by_date, 'DD-MON-YYYY')
            || ':Need By Date must greater than Creation Date.';
      END IF;

      IF (UPPER (TRIM (tac_po_req_temp_rec_type.destination_type_code)) <>
             'EXPENSE')
      THEN
         tac_po_req_temp_rec_type.error_flag := 'Y';
         msg_rec :=
               CASE WHEN msg_rec IS NOT NULL THEN msg_rec || CHR (13) END
            || p_req_recs.destination_type_code
            || ':Destination Type Code validation failed.';
      END IF;

      IF tac_po_req_temp_rec_type.item_description IS NULL
      THEN
         tac_po_req_temp_rec_type.error_flag := 'Y';
         msg_rec :=
               CASE WHEN msg_rec IS NOT NULL THEN msg_rec || CHR (13) END
            || 'ITEM_DESCRIPTION is required.';
      END IF;

      IF tac_po_req_temp_rec_type.quantity IS NULL
      THEN
         tac_po_req_temp_rec_type.error_flag := 'Y';
         msg_rec :=
               CASE WHEN msg_rec IS NOT NULL THEN msg_rec || CHR (13) END
            || 'QUANTITY is required.';
      ELSIF tac_po_req_temp_rec_type.quantity = 0
      THEN
         tac_po_req_temp_rec_type.error_flag := 'Y';
         msg_rec :=
               CASE WHEN msg_rec IS NOT NULL THEN msg_rec || CHR (13) END
            || 'QUANTITY must be greater than zero.';
      END IF;

      IF tac_po_req_temp_rec_type.unit_price IS NULL
      THEN
         tac_po_req_temp_rec_type.error_flag := 'Y';
         msg_rec :=
               CASE WHEN msg_rec IS NOT NULL THEN msg_rec || CHR (13) END
            || 'Unit Price is required.';
      ELSIF tac_po_req_temp_rec_type.unit_price = 0
      THEN
         tac_po_req_temp_rec_type.error_flag := 'Y';
         msg_rec :=
               CASE WHEN msg_rec IS NOT NULL THEN msg_rec || CHR (13) END
            || 'Unit Price must be greater than zero.';
      END IF;

      /* Validation and get value.*/

      BEGIN
         SELECT COUNT (*)
           INTO l_rows
           FROM po_requisition_headers_all
          WHERE TRIM (attribute15) = TRIM (tac_po_req_temp_rec_type.batch_id);

         IF (l_rows > 0)
         THEN
            tac_po_req_temp_rec_type.error_flag := 'Y';
            msg_rec :=
                  CASE WHEN msg_rec IS NOT NULL THEN msg_rec || CHR (13) END
               || TO_CHAR (tac_po_req_temp_rec_type.batch_id)
               || ':Batch ID is duplicate.';
         END IF;
      END;

      /* Validation and get value.*/
      BEGIN
         SELECT p.person_id
           INTO tac_po_req_temp_rec_type.preparer_id
           FROM per_all_people_f p, fnd_user u
          WHERE     p.person_id = u.employee_id
                AND u.user_name = TRIM (p_req_recs.user_name)
                AND NVL (p.effective_end_date, SYSDATE + 1) >= SYSDATE;
      EXCEPTION
         WHEN OTHERS
         THEN
            tac_po_req_temp_rec_type.error_flag := 'Y';
            msg_rec :=
                  CASE WHEN msg_rec IS NOT NULL THEN msg_rec || CHR (13) END
               || tac_po_req_temp_rec_type.preparer_name
               || ':Preparer validation failed.';
      END;

      /* Validation and get value.*/
      BEGIN
         SELECT category_id
           INTO tac_po_req_temp_rec_type.category_id
           FROM mtl_category_set_valid_cats_v
          WHERE     1 = 1
                AND category_set_name = 'Purchasing'
                AND category_concat_segments = p_req_recs.categories;
      EXCEPTION
         WHEN OTHERS
         THEN
            tac_po_req_temp_rec_type.error_flag := 'Y';
            msg_rec :=
                  CASE WHEN msg_rec IS NOT NULL THEN msg_rec || CHR (13) END
               || p_req_recs.categories
               || ':Categories validation failed.';
      END;

      /* Validation and get value.*/
      BEGIN
         SELECT uom_code
           INTO tac_po_req_temp_rec_type.unit_of_measure
           FROM mtl_units_of_measure_tl
          WHERE uom_code = p_req_recs.unit_of_measure;
      EXCEPTION
         WHEN OTHERS
         THEN
            tac_po_req_temp_rec_type.error_flag := 'Y';
            msg_rec :=
                  CASE WHEN msg_rec IS NOT NULL THEN msg_rec || CHR (13) END
               || p_req_recs.unit_of_measure
               || ':Unit Of Measurement validation failed.';
      END;

      /* Validation and get value.*/
      BEGIN
         SELECT line_type_id
           INTO tac_po_req_temp_rec_type.line_type_id
           FROM po_line_types_v
          WHERE     line_type = tac_po_req_temp_rec_type.line_type
                AND NVL (inactive_date, SYSDATE + 1) >= SYSDATE;
      EXCEPTION
         WHEN OTHERS
         THEN
            tac_po_req_temp_rec_type.error_flag := 'Y';
            msg_rec :=
                  CASE WHEN msg_rec IS NOT NULL THEN msg_rec || CHR (13) END
               || tac_po_req_temp_rec_type.line_type
               || ':Line Type validation failed.';
      END;

      /* Validation and get value.*/
      BEGIN
         SELECT hr.organization_id
           INTO tac_po_req_temp_rec_type.destination_organization_id
           FROM org_organization_definitions hr
          WHERE UPPER (hr.organization_code) =
                   TRIM (
                      tac_po_req_temp_rec_type.destination_organization_code);
      EXCEPTION
         WHEN OTHERS
         THEN
            tac_po_req_temp_rec_type.error_flag := 'Y';
            msg_rec :=
                  CASE WHEN msg_rec IS NOT NULL THEN msg_rec || CHR (13) END
               || tac_po_req_temp_rec_type.destination_organization_code
               || ':Destination Organization validation failed.';
      END;

      /* Validation and get value.*/
      BEGIN
         SELECT location_id
           INTO tac_po_req_temp_rec_type.deliver_to_location_id
           FROM hr_locations_all
          WHERE location_code = TRIM (p_req_recs.deliver_to_location_code);
      EXCEPTION
         WHEN OTHERS
         THEN
            tac_po_req_temp_rec_type.error_flag := 'Y';
            msg_rec :=
                  CASE WHEN msg_rec IS NOT NULL THEN msg_rec || CHR (13) END
               || p_req_recs.deliver_to_location_code
               || ':Deliver To Location validation failed.';
      END;

      /* Validation and get value.*/
      BEGIN
         SELECT p.person_id
           INTO tac_po_req_temp_rec_type.deliver_to_requestor_id
           FROM per_all_people_f p, fnd_user u
          WHERE     p.person_id = u.employee_id
                AND u.user_name = TRIM (p_req_recs.user_name)
                AND NVL (p.effective_end_date, SYSDATE + 1) >= SYSDATE;
      EXCEPTION
         WHEN OTHERS
         THEN
            tac_po_req_temp_rec_type.error_flag := 'Y';
            msg_rec :=
                  CASE WHEN msg_rec IS NOT NULL THEN msg_rec || CHR (13) END
               || p_req_recs.user_name
               || ':Deliver To Requestor validation failed.';
      END;

      /* Validation and get value.*/
      BEGIN
         SELECT vendor_id
           INTO tac_po_req_temp_rec_type.suggested_vendor_id
           FROM po_vendors
          WHERE TRIM (vendor_name) = TRIM (p_req_recs.suggested_vendor_code);
      EXCEPTION
         WHEN OTHERS
         THEN
            tac_po_req_temp_rec_type.error_flag := 'Y';
            msg_rec :=
                  CASE WHEN msg_rec IS NOT NULL THEN msg_rec || CHR (13) END
               || p_req_recs.suggested_vendor_code
               || ':Suggested Vendor validation failed.';
      END;

      /* Validation and get value.*/
      BEGIN
         SELECT vendor_site_id, vat_code
           INTO tac_po_req_temp_rec_type.suggested_vendor_site_id, l_vat_code
           FROM po_vendor_sites_all
          WHERE     vendor_id = tac_po_req_temp_rec_type.suggested_vendor_id
                AND org_id = tac_po_req_temp_rec_type.org_id
                AND TRIM (vendor_site_code) =
                       TRIM (p_req_recs.suggested_vendor_site);
      EXCEPTION
         WHEN OTHERS
         THEN
            tac_po_req_temp_rec_type.error_flag := 'Y';
            msg_rec :=
                  CASE WHEN msg_rec IS NOT NULL THEN msg_rec || CHR (13) END
               || p_req_recs.suggested_vendor_site
               || ':Suggested Vendor Site validation failed.';
      END;

      /* Validation and get value.*/
      --         IF (l_vat_code = p_req_recs (i).tax_code)
      --         THEN
      BEGIN
         SELECT tax_id
           INTO tac_po_req_temp_rec_type.tax_code_id
           FROM AP_TAX_CODES_ALL
          WHERE     name = p_req_recs.tax_code
                AND org_id = tac_po_req_temp_rec_type.org_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            tac_po_req_temp_rec_type.error_flag := 'Y';
            msg_rec :=
                  CASE WHEN msg_rec IS NOT NULL THEN msg_rec || CHR (13) END
               || p_req_recs.tax_code
               || ':Tax Codes validation failed.';
      END;

      BEGIN
         SELECT po_header_id
           INTO tac_po_req_temp_rec_type.source_doc_header_id
           FROM po_headers_all
          WHERE TRIM (segment1) =
                   TRIM (tac_po_req_temp_rec_type.source_document_number);
      EXCEPTION
         WHEN OTHERS
         THEN
            tac_po_req_temp_rec_type.error_flag := 'Y';
            msg_rec :=
                  CASE WHEN msg_rec IS NOT NULL THEN msg_rec || CHR (13) END
               || tac_po_req_temp_rec_type.source_document_number
               || ':Source Document Number validation failed.';
      END;

      tac_po_req_temp_rec_type.charge_account_id :=
         get_account_charge (tac_po_req_temp_rec_type.preparer_id,
                             tac_po_req_temp_rec_type.org_id,
                             p_req_recs.categories);

      tac_po_req_temp_rec_type.error_message := msg_rec;

      IF (tac_po_req_temp_rec_type.error_flag = 'N')
      THEN
         insert_temp (tac_po_req_temp_rec_type);
      ELSE
         UPDATE tac_po_req_import_stg
            SET error_flag = 'Y', error_message = msg_rec
          WHERE seq_id = tac_po_req_temp_rec_type.seq_id;
      END IF;

      --END LOOP;

      RETURN TRUE;
   EXCEPTION
      WHEN OTHERS
      THEN
         RAISE;
         RETURN FALSE;
   END;

   FUNCTION is_fund_available (
      p_code_combination_id        IN     NUMBER,
      p_set_of_books_id            IN     NUMBER DEFAULT 1001,
      p_currency_code              IN     VARCHAR2 DEFAULT 'THB',
      p_period_name                IN     VARCHAR2,
      p_amount                     IN     NUMBER,
      o_budget                        OUT NUMBER,
      o_encumbrance                   OUT NUMBER,
      o_actual                        OUT NUMBER,
      o_funds_available               OUT NUMBER,
      o_req_encumbrance_amount        OUT NUMBER,
      o_po_encumbrance_amount         OUT NUMBER,
      o_other_encumbrance_amount      OUT NUMBER)
      RETURN BOOLEAN
   IS
      l_accounted_period_type   VARCHAR2 (3);
      l_period_set_name         VARCHAR2 (30);
      l_period_num              NUMBER;
      l_quarter_num             NUMBER;
      l_period_year             NUMBER;
      l_budget_version_id       NUMBER;
      l_encumbrance_type_id     NUMBER;
   BEGIN
      BEGIN
         SELECT gp.period_type,
                gsob.period_set_name,
                gp.period_num,
                gp.quarter_num,
                gp.period_year
           INTO l_accounted_period_type,
                l_period_set_name,
                l_period_num,
                l_quarter_num,
                l_period_year
           FROM gl_sets_of_books gsob, gl_periods gp
          WHERE     gsob.period_set_name = gp.period_set_name
                --AND gp.period_year = TO_CHAR (SYSDATE, 'RRRR')
                AND gp.period_name = p_period_name
                AND gsob.set_of_books_id = p_set_of_books_id
                AND adjustment_period_flag != 'Y';
      EXCEPTION
         WHEN OTHERS
         THEN
            RAISE;
      END;

      BEGIN
         SELECT budget_version_id
           INTO l_budget_version_id
           FROM gl_budgets_with_dates_v
          WHERE     set_of_books_id = NVL (p_set_of_books_id, 1001)
                AND status != 'R'
                AND status = 'C'
                -- Status 'C' denotes the Current Active Budget
                AND SYSDATE BETWEEN start_date AND end_date;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_budget_version_id := 1000;                     --Approved Budget
      END;

      BEGIN
         SELECT encumbrance_type_id                      --  ,encumbrance_type
           INTO l_encumbrance_type_id
           FROM gl_all_enc_types_view
          WHERE encumbrance_type = 'ALL';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_encumbrance_type_id := -1;
      END;


      GL_FUNDS_AVAILABLE_PKG.calc_funds ('YTDE', --x_amount_type                   VARCHAR2,
                                         p_code_combination_id, --           NUMBER,
                                         'E', --x_account_type                  VARCHAR2,
                                         NULL, --x_template_id                   NUMBER,
                                         p_set_of_books_id, --               NUMBER,
                                         p_currency_code, --                 VARCHAR2,
                                         'Y', --x_po_install_flag               VARCHAR2,
                                         l_accounted_period_type, --         VARCHAR2,
                                         l_period_set_name, --               VARCHAR2,
                                         p_period_name, --                   VARCHAR2,
                                         l_period_num, --                    NUMBER,
                                         l_quarter_num, --                   NUMBER,
                                         l_period_year, --                   NUMBER,
                                         'O', --x_closing_status                VARCHAR2,
                                         l_budget_version_id, --             NUMBER,
                                         l_encumbrance_type_id, --           NUMBER,
                                         NULL, --x_req_encumbrance_id            NUMBER := 1061,
                                         NULL, --x_po_encumbrance_id             NUMBER := 1060,
                                         o_budget,
                                         o_encumbrance,
                                         o_actual,
                                         o_funds_available,
                                         o_req_encumbrance_amount,
                                         o_po_encumbrance_amount,
                                         o_other_encumbrance_amount);

      IF (NVL (o_funds_available, 0) > NVL (p_amount, 0))
      THEN
         RETURN TRUE;
      ELSE
         RETURN FALSE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN FALSE;
   END;

   FUNCTION get_account_charge (p_person_id               IN NUMBER,
                                p_org_id                  IN NUMBER,
                                p_concatenated_segments   IN VARCHAR2)
      RETURN NUMBER
   IS
      ------------------
      l_segment1              VARCHAR2 (15);
      l_segment11             VARCHAR2 (15);
      l_segment10             VARCHAR2 (15);
      l_segment12             VARCHAR2 (15);
      l_segment13             VARCHAR2 (15);
      l_segment14             VARCHAR2 (15);
      l_segment18             VARCHAR2 (15);
      l_segment21             VARCHAR2 (15);
      l_segment19             VARCHAR2 (15);

      -------------------
      v_code_combination_id   NUMBER;
   BEGIN
      SELECT gcc.segment1,
             gcc.segment11,
             gcc.segment10,
             gcc.segment12,
             gcc.segment13,
             gcc.segment14,
             gcc.segment18,
             gcc.segment21,
             gcc.segment19
        INTO l_segment1,
             l_segment11,
             l_segment10,
             l_segment12,
             l_segment13,
             l_segment14,
             l_segment18,
             l_segment21,
             l_segment19
        FROM per_all_assignments_f per, gl_code_combinations gcc
       WHERE     per.default_code_comb_id = gcc.code_combination_id
             AND person_id = p_person_id
             AND effective_end_date > SYSDATE;

      FOR item
         IN (SELECT pre.segment_num, pre.segment_value
               FROM po_rule_expense_accounts pre,
                    hr_operating_units hou,
                    mtl_categories mc
              WHERE     hou.organization_id = pre.org_id
                    AND mc.category_id = pre.rule_value_id
                    AND pre.rule_type = 'ITEM CATEGORY'
                    AND hou.organization_id = p_org_id
                    AND (   mc.segment1
                         || '.'
                         || mc.segment2
                         || '.'
                         || mc.segment3
                         || '.'
                         || mc.segment4) = p_concatenated_segments)
      LOOP
         IF (item.segment_num = 'SEGMENT1')
         THEN
            l_segment1 := item.segment_value;
         ELSIF (item.segment_num = 'SEGMENT11')
         THEN
            l_segment11 := item.segment_value;
         ELSIF (item.segment_num = 'SEGMENT10')
         THEN
            l_segment10 := item.segment_value;
         ELSIF (item.segment_num = 'SEGMENT12')
         THEN
            l_segment12 := item.segment_value;
         ELSIF (item.segment_num = 'SEGMENT13')
         THEN
            l_segment13 := item.segment_value;
         ELSIF (item.segment_num = 'SEGMENT14')
         THEN
            l_segment14 := item.segment_value;
         ELSIF (item.segment_num = 'SEGMENT18')
         THEN
            l_segment18 := item.segment_value;
         ELSIF (item.segment_num = 'SEGMENT21')
         THEN
            l_segment21 := item.segment_value;
         ELSIF (item.segment_num = 'SEGMENT19')
         THEN
            l_segment19 := item.segment_value;
         END IF;
      END LOOP;


      SELECT code_combination_id
        INTO v_code_combination_id
        FROM gl_code_combinations
       WHERE     segment1 = l_segment1
             AND segment11 = l_segment11
             AND segment10 = l_segment10
             AND segment12 = l_segment12
             AND segment13 = l_segment13
             AND segment14 = l_segment14
             AND segment18 = l_segment18
             AND segment21 = l_segment21
             AND segment19 = l_segment19;

      RETURN v_code_combination_id;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   PROCEDURE create_logs (p_org_id          IN NUMBER,
                          p_concurrent_id   IN NUMBER,
                          p_start_date      IN DATE)
   IS
      l_today_timestamp           TIMESTAMP := SYSTIMESTAMP;
      p_end_date                  DATE;



      l_program_name              VARCHAR2 (50 BYTE) := 'SMT002';
      l_record_count_completion   NUMBER;
      l_record_count_failed       NUMBER;
      l_record_count_total        NUMBER;

      l_hour                      NUMBER;
      l_minute                    NUMBER;
      l_second                    NUMBER;

      l_pr_number                 VARCHAR2 (50 BYTE);
      l_pr_line                   NUMBER;
      l_pr_status                 VARCHAR2 (50 BYTE);
      l_po_number                 VARCHAR2 (50 BYTE);
      l_account                   VARCHAR2 (100 BYTE);

      v_process_start             DATE;
      v_process_finish            DATE;
      v_process_time              VARCHAR2 (20);

      l_reserve_flag              VARCHAR (1);

      CURSOR c_line (
         i_request_id NUMBER)
      IS
         SELECT stg.request_id,
                stg.operating_unit,
                stg.line_description,
                stg.quantity,
                stg.unit_price,
                stg.batch_name,
                stg.line_number,
                stg.categories,
                stg.unit_of_measure,
                stg.suggested_vendor_code,
                stg.suggested_vendor_site,
                stg.user_name,
                tmp.charge_account_id,
                DECODE (stg.error_flag,
                        'N', DECODE (inf.process_flag, 'ERROR', 'Y', 'N'),
                        'Y')
                   error_flag,
                DECODE (
                   stg.error_flag,
                   'Y', DECODE (inf.process_flag,
                                'ERROR', err.error_message,
                                stg.error_message),
                   NULL)
                   error_message
           FROM tac_po_req_import_stg stg,
                tac_po_req_import_temp tmp,
                po_requisitions_interface_all inf,
                po_interface_errors err
          WHERE     stg.request_id = tmp.request_id(+)
                AND stg.seq_id = tmp.seq_id(+)
                AND tmp.batch_id = inf.batch_id(+)
                AND inf.interface_source_code(+) = 'SMT'
                AND inf.transaction_id = err.interface_transaction_id(+)
                AND err.interface_type(+) = 'REQIMPORT'
                AND stg.request_id = i_request_id;
   /*
      SELECT stg.request_id,
             stg.operating_unit,
             stg.line_description,
             stg.quantity,
             stg.unit_price,
             stg.batch_name,
             stg.line_number,
             stg.categories,
             stg.unit_of_measure,
             stg.suggested_vendor_code,
             stg.suggested_vendor_site,
             stg.user_name,
             tmp.charge_account_id,
             stg.error_flag,
             stg.error_message
        FROM tac_po_req_import_stg stg, tac_po_req_import_temp tmp
       WHERE     stg.request_id = tmp.request_id(+)
             AND stg.seq_id = tmp.seq_id(+)
             --AND tmp.org_id(+) = p_org_id
             AND stg.request_id = i_request_id;
    */
   BEGIN
      v_process_finish := SYSDATE;
      v_process_time :=
         TO_CHAR (
            TO_TIMESTAMP (
               TO_CHAR (
                    TO_DATE ('00:00:00', 'HH24:MI:SS')
                  + (v_process_finish - p_start_date),
                  'HH24:MI:SS'),
               'HH24:MI:SS.FF'),
            'HH24:MI:SS.FF');
      write_log ('Loop create logs.' || p_concurrent_id, 'LOG');

      FOR i IN c_line (p_concurrent_id)
      LOOP
         write_log ('Loop : ' || 'xxx' || i.batch_name, 'LOG');
         l_reserve_flag := 'Y';

         BEGIN
            SELECT prh.segment1,
                   prl.line_num,
                   prh.authorization_status,
                   poh.segment1,
                   CASE
                      WHEN prh.authorization_status <> 'APPROVED'
                      THEN
                         DECODE (prd.encumbered_flag, 'N', 'N', 'Y')
                      ELSE
                         'Y'
                   END
              INTO l_pr_number,
                   l_pr_line,
                   l_pr_status,
                   l_po_number,
                   l_reserve_flag
              FROM po_requisition_headers_all prh,
                   po_requisition_lines_all prl,
                   po_req_distributions_all prd,
                   po_distributions_all pda,
                   po_headers_all poh
             WHERE     prh.requisition_header_id = prl.requisition_header_id
                   AND prd.requisition_line_id = prl.requisition_line_id
                   AND pda.req_distribution_id(+) = prd.distribution_id
                   AND pda.po_header_id = poh.po_header_id(+)
                   AND prh.interface_source_code = 'SMT'
                   AND prh.attribute15 = i.batch_name
                   AND ROWNUM < 2;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_pr_number := '';
               l_pr_line := 0;
               l_pr_status := '';
               l_po_number := '';
         END;

         IF (i.charge_account_id IS NOT NULL)
         THEN
            SELECT    gcc.segment1
                   || '-'
                   || gcc.segment11
                   || '-'
                   || gcc.segment10
                   || '-'
                   || gcc.segment12
                   || '-'
                   || gcc.segment13
                   || '-'
                   || gcc.segment14
                   || '-'
                   || gcc.segment18
                   || '-'
                   || gcc.segment21
                   || '-'
                   || gcc.segment19
              INTO l_account
              FROM gl_code_combinations gcc
             WHERE gcc.code_combination_id = i.charge_account_id;
         END IF;

         BEGIN
            INSERT INTO TACSMT_LOG_DETAIL (level_type,
                                           process_start,
                                           process_finish,
                                           processing_time,
                                           request_id,
                                           ERROR_TYPE,
                                           batch_id,
                                           operating_unit,
                                           pr_number,
                                           pr_line,
                                           pr_status,
                                           po_number,
                                           purchase_categories,
                                           line_desc,
                                           quantity,
                                           uom,
                                           unit_price,
                                           supplier_name,
                                           supplier_site,
                                           account,
                                           requester,
                                           date_from,
                                           date_to,
                                           invoice_number,
                                           invoice_amount,
                                           invoice_status,
                                           invoice_date,
                                           record_complete,
                                           record_failed,
                                           record_count,
                                           record_flag,
                                           supplier_number,
                                           ERROR_CODE,
                                           error_message)
                 VALUES (
                           'D',
                           p_start_date,
                           TO_DATE (
                              TO_CHAR (l_today_timestamp,
                                       'YYYY-MON-DD HH24:MI:SS'),
                              'YYYY-MON-DD HH24:MI:SS'),
                           v_process_time,
                           i.request_id,
                           'SMTPR Interface',
                           i.batch_name,
                           i.operating_unit,
                           l_pr_number,
                           l_pr_line,
                           l_pr_status,
                           l_po_number,
                           i.categories,
                           i.line_description,
                           i.quantity,
                           i.unit_of_measure,
                           i.unit_price,
                           i.suggested_vendor_code,
                           i.suggested_vendor_site,
                           l_account,
                           i.user_name,
                           p_start_date,
                           p_end_date,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           DECODE (i.error_flag, 'Y', 0, 1),
                           DECODE (i.error_flag, 'Y', 1, 0),
                           1,
                           NULL,
                           NULL,
                           DECODE (i.error_flag, 'Y', 'Error', 'Completed'),
                           DECODE (
                              l_reserve_flag,
                              'Y', i.error_message,
                                 'Reserve fund failed.'
                              || ' '
                              || i.error_message));
         EXCEPTION
            WHEN OTHERS
            THEN
               RAISE;
         END;
      END LOOP;

      SELECT SUM (DECODE (stg.error_flag, 'Y', 0, 1)) record_count_completion,
             SUM (DECODE (stg.error_flag, 'N', 0, 1)) record_count_failed,
             COUNT (*) record_count_total
        INTO l_record_count_completion,
             l_record_count_failed,
             l_record_count_total
        FROM tac_po_req_import_stg stg
       WHERE stg.request_id = p_concurrent_id;

      INSERT INTO TACSMT_LOG_SUMMARY (level_type,
                                      request_id,
                                      program_name,
                                      process_start,
                                      process_finish,
                                      processing_type,
                                      processing_time,
                                      record_count_completion,
                                      record_count_failed,
                                      record_count_total)
           VALUES (
                     'H',
                     p_concurrent_id,
                     l_program_name,
                     p_start_date,
                     TO_DATE (
                        TO_CHAR (l_today_timestamp, 'YYYY-MON-DD HH24:MI:SS'),
                        'YYYY-MON-DD HH24:MI:SS'),
                     'Upload',
                     v_process_time,
                     l_record_count_completion,
                     l_record_count_failed,
                     l_record_count_total);

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         RAISE;
   END;

   PROCEDURE main_process (errbuf                OUT VARCHAR2,
                           errcode               OUT VARCHAR2,
                           p_operation_unit   IN     VARCHAR2,
                           p_request_id       IN     NUMBER,
                           p_user_id          IN     NUMBER)
   IS
      l_lines_in              TAC_PO_REQUISITIONS_PKG.tac_po_req_stg_tbl_type;
      l_temp_out              TAC_PO_REQUISITIONS_PKG.tac_po_req_temp_tbl_type;
      l_load_data_file_err    NUMBER := 0;
      l_insert_temp           BOOLEAN := FALSE;
      l_per_data              BOOLEAN := FALSE;

      l_ou_id                 NUMBER;
      l_resp_id               NUMBER;

      l_invalid_flag          NUMBER := 0;
      l_valid_stg             NUMBER := 0;
      l_valid_tmp             NUMBER := 0;
      l_org_id                NUMBER;
      l_set_of_books_id       NUMBER;
      l_code_combination_id   NUMBER;
      l_currency_code         VARCHAR2 (3) := 'THB';
      l_period_name           VARCHAR2 (30);
      l_amount                NUMBER;


      lc_phase                VARCHAR2 (50);
      lc_status               VARCHAR2 (50);
      lc_dev_phase            VARCHAR2 (50);
      lc_dev_status           VARCHAR2 (50);
      lc_message              VARCHAR2 (50);
      l_req_return_status     BOOLEAN;

      l_start_date            DATE;

      lv_request_id           NUMBER;
      l_log_request_id        NUMBER;
      l_movfile_request_id    NUMBER;
      ln_request_id           NUMBER;
      l_list_file_req_id      NUMBER;

      l_source_file           VARCHAR2 (50);

      l_files_out             TAC_PO_REQUISITIONS_PKG.tac_po_req_file_tbl_type;

      l_dupplicate_file       BOOLEAN;
      l_error                 VARCHAR2 (3000);

      v_request_id            NUMBER := p_request_id;

      tac_po_req_stg_rec      tac_po_req_import_stg%ROWTYPE;
   BEGIN
      l_start_date := SYSDATE;

      SELECT MAX (request_id) INTO v_request_id FROM tac_po_req_import_stg;

      SELECT COUNT (*)
        INTO l_load_data_file_err
        FROM tac_po_req_import_stg
       WHERE request_id = v_request_id AND error_flag = 'Y';

      IF (l_load_data_file_err = 0)
      THEN
         FOR item IN (  SELECT *
                          FROM tac_po_req_files
                         WHERE request_id = v_request_id
                      ORDER BY file_name ASC)
         LOOP
            SELECT *
              INTO tac_po_req_stg_rec
              FROM tac_po_req_import_stg
             WHERE request_id = v_request_id AND file_name = item.file_name;

            IF (validate_stg_to_temp (p_operation_unit, tac_po_req_stg_rec))
            THEN
               IF (pre_interface_data (v_request_id, item.file_name))
               THEN
                  write_log ('Pre data completed.', 'LOG');
               END IF;
            END IF;
         END LOOP;

         FOR item
            IN (  SELECT DECODE (org_id,  102, 50265,  142, 50683) resp_id,
                         org_id
                    FROM tac_po_req_import_temp
                   WHERE request_id = v_request_id
                GROUP BY org_id)
         LOOP
            write_log ('Loop Concurrent.' || item.resp_id, 'LOG');
            fnd_global.apps_initialize (fnd_profile.VALUE ('USER_ID'),
                                        item.resp_id,
                                        fnd_profile.VALUE ('RESP_APPL_ID'),
                                        NULL,
                                        NULL);

            ln_request_id :=
               fnd_request.submit_request ('PO',
                                           'REQIMPORT',
                                           NULL,
                                           NULL,
                                           FALSE,
                                           'SMT',                      ---arg1
                                           NULL,
                                           'All',
                                           NULL,
                                           'N',
                                           'Y');
            COMMIT;

            IF ln_request_id = 0
            THEN
               write_log (
                  'Request Not Submitted due to "' || fnd_message.GET || '".',
                  'LOG');
            ELSE
               write_log (
                     'Program submitted successfully Request id :'
                  || ln_request_id,
                  'LOG');
            END IF;

            IF ln_request_id > 0
            THEN
               LOOP
                  --
                  --To make process execution to wait for 1st program to complete
                  --
                  l_req_return_status :=
                     fnd_concurrent.wait_for_request (
                        request_id   => ln_request_id,
                        INTERVAL     => 2,
                        max_wait     => 60,
                        phase        => lc_phase,
                        STATUS       => lc_status,
                        dev_phase    => lc_dev_phase,
                        dev_status   => lc_dev_status,
                        MESSAGE      => lc_message);
                  EXIT WHEN    UPPER (lc_phase) = 'COMPLETED'
                            OR UPPER (lc_status) IN
                                  ('CANCELLED', 'ERROR', 'TERMINATED');
               END LOOP;

               --
               --
               IF     UPPER (lc_phase) = 'COMPLETED'
                  AND UPPER (lc_status) = 'ERROR'
               THEN
                  write_log (
                        'Program completed in error. Oracle request id: '
                     || ln_request_id
                     || ' '
                     || SQLERRM,
                     'LOG');
               ELSIF     UPPER (lc_phase) = 'COMPLETED'
                     AND UPPER (lc_status) = 'NORMAL'
               THEN
                  write_log (
                        'Program request successful for request id: '
                     || ln_request_id,
                     'LOG');
               END IF;
            END IF;
         END LOOP;

         create_logs (0, v_request_id, l_start_date);
         write_log ('', 'LOG');
      END IF;
   END;

   PROCEDURE write_log (buff VARCHAR2, which VARCHAR2)
   IS
   BEGIN
      IF UPPER (which) = 'LOG'
      THEN
         fnd_file.put_line (FND_FILE.LOG, buff);
      ELSE
         fnd_file.put_line (FND_FILE.OUTPUT, buff);
      END IF;
   END write_log;

   FUNCTION listing_file (
      p_request_id          IN     NUMBER,
      p_master_request_id   IN     NUMBER,
      o_file_rec               OUT tac_po_req_file_tbl_type)
      RETURN BOOLEAN
   IS
      l_request_id    NUMBER;
      l_file_name     VARCHAR2 (50);
      in_file         UTL_FILE.file_type;
      l_new_line      VARCHAR2 (1000);
      i               NUMBER := 0;

      l_lines_in      TAC_PO_REQUISITIONS_PKG.tac_po_req_file_tbl_type;
      l_line_in_rec   TAC_PO_REQUISITIONS_PKG.tac_po_req_file_rec_type;
   BEGIN
      l_file_name := p_request_id || '.txt';

      --write_log ('Create File : ' || l_file_name, 'LOG');
      in_file := UTL_FILE.fopen ('SMT002_FILES', l_file_name, 'r');

      LOOP
         BEGIN
            UTL_FILE.GET_LINE (in_file, l_new_line);
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               EXIT;
         END;

         l_line_in_rec.request_id := p_master_request_id;
         l_line_in_rec.file_name := TRIM (l_new_line);
         l_line_in_rec.load_flag := 'N';
         l_line_in_rec.load_date := SYSDATE;
         l_line_in_rec.error_message := '';
         --write_log ('List File : ' || l_line_in_rec.file_name, 'LOG');
         l_lines_in (i) := l_line_in_rec;

         INSERT INTO TAC_PO_REQ_FILES
              VALUES l_line_in_rec;

         i := i + 1;
      END LOOP;

      COMMIT;
      UTL_FILE.fclose (in_file);
      RETURN TRUE;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN FALSE;
   END;

   PROCEDURE start_up (p_operation_unit   IN VARCHAR2,
                       p_request_id       IN NUMBER,
                       p_user_id          IN NUMBER)
   IS
      lv_request_id         NUMBER;
      lc_phase              VARCHAR2 (50);
      lc_status             VARCHAR2 (50);
      lc_dev_phase          VARCHAR2 (50);
      lc_dev_status         VARCHAR2 (50);
      lc_message            VARCHAR2 (50);
      l_req_return_status   BOOLEAN;

      l_get_file            BOOLEAN;

      l_lines_in            TAC_PO_REQUISITIONS_PKG.tac_po_req_file_tbl_type;
      l_lines_stg_in        TAC_PO_REQUISITIONS_PKG.tac_po_req_stg_tbl_type;
      l_lines_temp_out      TAC_PO_REQUISITIONS_PKG.tac_po_req_temp_tbl_type;

      l_duplicate_file      BOOLEAN;
      l_dup                 CHAR (1);
      l_error               VARCHAR2 (3000);
   BEGIN
      fnd_global.apps_initialize (
         user_id             => fnd_profile.VALUE ('USER_ID'),
         resp_id             => fnd_profile.VALUE ('RESP_ID'),
         resp_appl_id        => fnd_profile.VALUE ('RESP_APPL_ID'),
         security_group_id   => 0);

      lv_request_id :=
         fnd_request.submit_request ('PO',
                                     'SMT002LOAD',
                                     NULL,
                                     NULL,
                                     FALSE,
                                     '*.*');
      COMMIT;

      IF lv_request_id = 0
      THEN
         DBMS_OUTPUT.put_line (
            'Request Not Submitted due to "' || fnd_message.GET || '".');
      ELSE
         DBMS_OUTPUT.put_line (
               'The Program PROGRAM_1 submitted successfully � Request id :'
            || lv_request_id);
      END IF;

      IF lv_request_id > 0
      THEN
         LOOP
            l_req_return_status :=
               fnd_concurrent.wait_for_request (request_id   => lv_request_id,
                                                INTERVAL     => 2,
                                                max_wait     => 60,
                                                phase        => lc_phase,
                                                STATUS       => lc_status,
                                                dev_phase    => lc_dev_phase,
                                                dev_status   => lc_dev_status,
                                                MESSAGE      => lc_message);
            EXIT WHEN    UPPER (lc_phase) = 'COMPLETED'
                      OR UPPER (lc_status) IN
                            ('CANCELLED', 'ERROR', 'TERMINATED');
         END LOOP;

         --
         --
         IF UPPER (lc_phase) = 'COMPLETED' AND UPPER (lc_status) = 'ERROR'
         THEN
            DBMS_OUTPUT.put_line (
                  'The XX_PROGRAM_1 completed in error. Oracle request id: '
               || lv_request_id
               || ' '
               || SQLERRM);
         ELSIF     UPPER (lc_phase) = 'COMPLETED'
               AND UPPER (lc_status) = 'NORMAL'
         THEN
            DBMS_OUTPUT.put_line (
                  'The XX_PROGRAM_1 request successful for request id: '
               || lv_request_id);

            BEGIN
               l_get_file :=
                  listing_file (lv_request_id, p_request_id, l_lines_in);

               FOR item IN (  SELECT *
                                FROM tac_po_req_files
                               WHERE request_id = p_request_id
                            ORDER BY file_name ASC)
               LOOP
                  l_error := '';
                  l_duplicate_file := FALSE;
                  write_log ('File 1 : ' || item.file_name, 'LOG');
                  l_lines_stg_in :=
                     read_data_to_stage (p_operation_unit,
                                         p_request_id,
                                         p_user_id,
                                         item.file_name,
                                         l_duplicate_file,
                                         l_error);

                  IF l_duplicate_file = TRUE
                  THEN
                     l_dup := 'E';
                  ELSE
                     l_dup := 'Y';
                  END IF;

                  write_log ('Dupplicate File : ' || l_dup, 'LOG');
                  write_log ('Error Message : ' || l_error, 'LOG');
                  write_log ('p_request_id : ' || p_request_id, 'LOG');

                  UPDATE TAC_PO_REQ_FILES
                     SET LOAD_FLAG = l_dup, error_message = l_error
                   WHERE request_id = p_request_id;

                  COMMIT;
               END LOOP;
            EXCEPTION
               WHEN OTHERS
               THEN
                  DBMS_OUTPUT.put_line (
                        'OTHERS exception while submitting XX_PROGRAM_2: '
                     || SQLERRM);
            END;
         ELSE
            DBMS_OUTPUT.put_line (
                  'The XX_PROGRAM_1 request failed. Oracle request id: '
               || lv_request_id
               || ' '
               || SQLERRM);
         END IF;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (
            'OTHERS exception while submitting XX_PROGRAM_1: ' || SQLERRM);
   END;

   PROCEDURE main (errbuf                OUT VARCHAR2,
                   errcode               OUT VARCHAR2,
                   p_operation_unit   IN     VARCHAR2,
                   p_user_id          IN     NUMBER)
   IS
      l_request_id           NUMBER;
      l_imp_request_id       NUMBER;
      l_req_return_status    BOOLEAN;

      lc_phase               VARCHAR2 (50);
      lc_status              VARCHAR2 (50);
      lc_dev_phase           VARCHAR2 (50);
      lc_dev_status          VARCHAR2 (50);
      lc_message             VARCHAR2 (50);

      v_request_id           NUMBER;

      l_invalid_flag         NUMBER;
      l_movfile_request_id   NUMBER;
   BEGIN
      SELECT MAX (request_id) INTO v_request_id FROM tac_po_req_import_stg;

      write_log ('Create logs.' || v_request_id, 'LOG');
      --create_logs (v_request_id, SYSDATE);
      --create_logs (item.org_id, v_request_id, sysdate);

      l_request_id :=
         fnd_request.submit_request ('SQLAP',
                                     'TACSMT006',
                                     NULL,
                                     NULL,
                                     FALSE,
                                     'SMT002',                         ---arg1
                                     v_request_id);
      COMMIT;

      IF l_request_id = 0
      THEN
         write_log (
            'Request Not Submitted due to "' || fnd_message.GET || '".',
            'LOG');
      ELSE
         write_log (
            'Program submitted successfully Request id :' || l_request_id,
            'LOG');
      END IF;



      LOOP
         l_req_return_status :=
            fnd_concurrent.wait_for_request (request_id   => l_request_id,
                                             interval     => 2,
                                             max_wait     => 0,
                                             phase        => lc_phase,
                                             status       => lc_status,
                                             dev_phase    => lc_dev_phase,
                                             dev_status   => lc_dev_status,
                                             MESSAGE      => lc_message);

         EXIT WHEN lc_phase = 'Completed';
         DBMS_LOCK.SLEEP (10);
      END LOOP;

      BEGIN
         SELECT COUNT (*)
           INTO l_invalid_flag
           FROM tac_po_req_import_stg
          WHERE error_flag = 'Y' AND request_id = v_request_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            RAISE;
      END;

      --IF (l_invalid_flag = 0)
      --THEN
      FOR item IN (  SELECT file_name
                       FROM tac_po_req_import_stg
                      WHERE request_id = v_request_id AND error_flag = 'N'
                   GROUP BY file_name)
      LOOP
         l_movfile_request_id :=
            fnd_request.submit_request (
               'SQLAP',
               'TACAP_MVFILE_SMT',
               NULL,
               NULL,
               FALSE,
               '/MP/GERP_TEST/GERP/SMT002/INBOX/' || TRIM (item.file_name), ---arg1
               '/MP/GERP_TEST/GERP/SMT002/HISTORY');
         COMMIT;
      END LOOP;

      --ELSE
      FOR item IN (  SELECT file_name
                       FROM tac_po_req_import_stg
                      WHERE request_id = v_request_id AND error_flag = 'Y'
                   GROUP BY file_name)
      LOOP
         l_movfile_request_id :=
            fnd_request.submit_request (
               'SQLAP',
               'TACAP_MVFILE_SMT',
               NULL,
               NULL,
               FALSE,
               '/MP/GERP_TEST/GERP/SMT002/INBOX/' || TRIM (item.file_name), ---arg1
               '/MP/GERP_TEST/GERP/SMT002/ERROR');
         COMMIT;
      END LOOP;
   --END IF;
   END;
END TAC_PO_REQUISITIONS_PKG;
/

