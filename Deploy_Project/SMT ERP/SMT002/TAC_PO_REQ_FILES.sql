DROP TABLE APPS.TAC_PO_REQ_FILES CASCADE CONSTRAINTS;

--
-- TAC_PO_REQ_FILES  (Table) 
--
CREATE TABLE APPS.TAC_PO_REQ_FILES
(
  REQUEST_ID     NUMBER,
  FILE_NAME      VARCHAR2(100 BYTE),
  LOAD_FLAG      VARCHAR2(1 BYTE),
  LOAD_DATE      DATE,
  ERROR_MESSAGE  VARCHAR2(3000 CHAR)
)
TABLESPACE APPS_TS_TX_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;
