CREATE OR REPLACE PACKAGE APPS.TAC_PO_REQUISITIONS_PKG
AS
   TYPE tac_po_req_temp_rec_type IS RECORD
   (
      seq_id                          tac_po_req_import_temp.seq_id%TYPE,
      request_id                      tac_po_req_import_temp.request_id%TYPE,
      transaction_id                  tac_po_req_import_temp.transaction_id%TYPE,
      creation_date                   tac_po_req_import_temp.creation_date%TYPE,
      operating_unit                  tac_po_req_import_temp.operating_unit%TYPE,
      interface_source_code           tac_po_req_import_temp.interface_source_code%TYPE,
      source_type_code                tac_po_req_import_temp.source_type_code%TYPE,
      requisition_type                tac_po_req_import_temp.requisition_type%TYPE,
      destination_type_code           tac_po_req_import_temp.destination_type_code%TYPE,
      item_description                tac_po_req_import_temp.item_description%TYPE,
      quantity                        tac_po_req_import_temp.quantity%TYPE,
      unit_price                      tac_po_req_import_temp.unit_price%TYPE,
      authorization_status            tac_po_req_import_temp.authorization_status%TYPE,
      batch_id                        tac_po_req_import_temp.batch_id%TYPE,
      group_code                      tac_po_req_import_temp.group_code%TYPE,
      preparer_id                     tac_po_req_import_temp.preparer_id%TYPE,
      preparer_name                   tac_po_req_import_temp.preparer_name%TYPE,
      autosource_flag                 tac_po_req_import_temp.autosource_flag%TYPE,
      header_description              tac_po_req_import_temp.header_description%TYPE,
      urgent_flag                     tac_po_req_import_temp.urgent_flag%TYPE,
      justification                   tac_po_req_import_temp.justification%TYPE,
      note_to_buyer                   tac_po_req_import_temp.note_to_buyer%TYPE,
      item_id                         tac_po_req_import_temp.item_id%TYPE,
      item_segmemt1                   tac_po_req_import_temp.item_segmemt1%TYPE,
      charge_account_id               tac_po_req_import_temp.charge_account_id%TYPE,
      category_id                     tac_po_req_import_temp.category_id%TYPE,
      unit_of_measure                 tac_po_req_import_temp.unit_of_measure%TYPE,
      line_type_id                    tac_po_req_import_temp.line_type_id%TYPE,
      line_type                       tac_po_req_import_temp.line_type%TYPE,
      destination_organization_id     tac_po_req_import_temp.destination_organization_id%TYPE,
      destination_organization_code   tac_po_req_import_temp.destination_organization_code%TYPE,
      deliver_to_location_id          tac_po_req_import_temp.deliver_to_location_id%TYPE,
      deliver_to_location_code        tac_po_req_import_temp.deliver_to_location_code%TYPE,
      deliver_to_requestor_id         tac_po_req_import_temp.deliver_to_requestor_id%TYPE,
      suggested_vendor_id             tac_po_req_import_temp.suggested_vendor_id%TYPE,
      suggested_vendor_site_id        tac_po_req_import_temp.suggested_vendor_site_id%TYPE,
      need_by_date                    tac_po_req_import_temp.need_by_date%TYPE,
      gl_date                         tac_po_req_import_temp.gl_date%TYPE,
      org_id                          tac_po_req_import_temp.org_id%TYPE,
      line_no                         tac_po_req_import_temp.line_no%TYPE,
      tax_code_id                     tac_po_req_import_temp.tax_code_id%TYPE,
      source_document_type            tac_po_req_import_temp.source_document_type%TYPE,
      source_document_number          tac_po_req_import_temp.source_document_number%TYPE,
      source_document_line_number     tac_po_req_import_temp.source_document_line_number%TYPE,
      source_doc_header_id            tac_po_req_import_temp.source_doc_header_id%TYPE,
      last_updated_by                 tac_po_req_import_temp.last_updated_by%TYPE,
      last_update_date                tac_po_req_import_temp.last_update_date%TYPE,
      imported_by                     tac_po_req_import_temp.imported_by%TYPE,
      import_date                     tac_po_req_import_temp.import_date%TYPE,
      error_flag                      tac_po_req_import_temp.error_flag%TYPE,
      error_message                   tac_po_req_import_temp.error_message%TYPE
   );

   TYPE tac_po_req_stg_rec_type IS RECORD
   (
      seq_id                          tac_po_req_import_stg.seq_id%TYPE,
      request_id                      tac_po_req_import_stg.request_id%TYPE,
      transaction_id                  tac_po_req_import_stg.transaction_id%TYPE,
      interface_source_code           tac_po_req_import_stg.interface_source_code%TYPE,
      batch_name                      tac_po_req_import_stg.batch_name%TYPE,
      operating_unit                  tac_po_req_import_stg.operating_unit%TYPE,
      source_type_code                tac_po_req_import_stg.source_type_code%TYPE,
      requisition_type                tac_po_req_import_stg.requisition_type%TYPE,
      header_description              tac_po_req_import_stg.header_description%TYPE,
      line_number                     tac_po_req_import_stg.line_number%TYPE,
      destination_type_code           tac_po_req_import_stg.destination_type_code%TYPE,
      line_type                       tac_po_req_import_stg.line_type%TYPE,
      categories                      tac_po_req_import_stg.categories%TYPE,
      line_description                tac_po_req_import_stg.line_description%TYPE,
      quantity                        tac_po_req_import_stg.quantity%TYPE,
      unit_price                      tac_po_req_import_stg.unit_price%TYPE,
      unit_of_measure                 tac_po_req_import_stg.unit_of_measure%TYPE,
      tax_code                        tac_po_req_import_stg.tax_code%TYPE,
      urgent_flag                     tac_po_req_import_stg.urgent_flag%TYPE,
      group_code                      tac_po_req_import_stg.group_code%TYPE,
      user_name                       tac_po_req_import_stg.user_name%TYPE,
      justification                   tac_po_req_import_stg.justification%TYPE,
      note_to_buyer                   tac_po_req_import_stg.note_to_buyer%TYPE,
      destination_organization_code   tac_po_req_import_stg.destination_organization_code%TYPE,
      deliver_to_location_code        tac_po_req_import_stg.deliver_to_location_code%TYPE,
      autosource_flag                 tac_po_req_import_stg.autosource_flag%TYPE,
      suggested_vendor_code           tac_po_req_import_stg.suggested_vendor_code%TYPE,
      suggested_vendor_site           tac_po_req_import_stg.suggested_vendor_site%TYPE,
      need_by_date                    tac_po_req_import_stg.need_by_date%TYPE,
      gl_date                         tac_po_req_import_stg.gl_date%TYPE,
      source_document_type            tac_po_req_import_stg.source_document_type%TYPE,
      source_document_number          tac_po_req_import_stg.source_document_number%TYPE,
      source_document_line_number     tac_po_req_import_stg.source_document_line_number%TYPE,
      last_updated_by                 tac_po_req_import_stg.last_updated_by%TYPE,
      last_update_date                tac_po_req_import_stg.last_update_date%TYPE,
      imported_by                     tac_po_req_import_stg.imported_by%TYPE,
      creation_date                   tac_po_req_import_stg.creation_date%TYPE,
      import_date                     tac_po_req_import_stg.import_date%TYPE,
      error_flag                      tac_po_req_import_stg.error_flag%TYPE,
      error_message                   tac_po_req_import_stg.error_message%TYPE,
      file_name                       tac_po_req_import_stg.file_name%TYPE
   );

   TYPE tac_po_req_file_rec_type IS RECORD
   (
      request_id      tac_po_req_files.request_id%TYPE,
      file_name       tac_po_req_files.file_name%TYPE,
      load_flag       tac_po_req_files.load_flag%TYPE,
      load_date       tac_po_req_files.load_date%TYPE,
      error_message   tac_po_req_files.load_date%TYPE
   );

   tac_po_req_stg_rec_type1   tac_po_req_import_stg%ROWTYPE;

   TYPE tac_po_req_temp_tbl_type IS TABLE OF tac_po_req_temp_rec_type
                                       INDEX BY BINARY_INTEGER;

   TYPE tac_po_req_stg_tbl_type IS TABLE OF tac_po_req_stg_rec_type
                                      INDEX BY BINARY_INTEGER;

   TYPE tac_po_req_file_tbl_type IS TABLE OF tac_po_req_file_rec_type
                                       INDEX BY BINARY_INTEGER;

   FUNCTION read_data_to_stage (
      p_operation_unit    IN     VARCHAR2 DEFAULT 'All',
      p_request_id        IN     NUMBER,
      p_user_id           IN     NUMBER,
      p_file_name         IN     VARCHAR,
      o_dupplicate_file      OUT BOOLEAN,
      o_error                OUT VARCHAR2)
      RETURN tac_po_req_stg_tbl_type;

   FUNCTION binding_stg_to_temp (
      p_po_req_stg_in IN tac_po_req_import_stg%ROWTYPE)
      RETURN tac_po_req_import_temp%ROWTYPE;

   FUNCTION pre_interface_data (p_request_id   IN NUMBER,
                                p_file_name    IN VARCHAR2)
      RETURN BOOLEAN;

   PROCEDURE insert_temp (p_rec IN tac_po_req_import_temp%ROWTYPE);

   FUNCTION validate_stg_to_temp (
      p_operation_unit   IN VARCHAR2 DEFAULT 'All',
      p_req_recs         IN tac_po_req_import_stg%ROWTYPE)
      RETURN BOOLEAN;

   FUNCTION is_fund_available (
      p_code_combination_id        IN     NUMBER,
      p_set_of_books_id            IN     NUMBER DEFAULT 1001,
      p_currency_code              IN     VARCHAR2 DEFAULT 'THB',
      p_period_name                IN     VARCHAR2,
      p_amount                     IN     NUMBER,
      o_budget                        OUT NUMBER,
      o_encumbrance                   OUT NUMBER,
      o_actual                        OUT NUMBER,
      o_funds_available               OUT NUMBER,
      o_req_encumbrance_amount        OUT NUMBER,
      o_po_encumbrance_amount         OUT NUMBER,
      o_other_encumbrance_amount      OUT NUMBER)
      RETURN BOOLEAN;

   FUNCTION get_account_charge (p_person_id               IN NUMBER,
                                p_org_id                  IN NUMBER,
                                p_concatenated_segments   IN VARCHAR2)
      RETURN NUMBER;

   PROCEDURE main_process (errbuf                OUT VARCHAR2,
                           errcode               OUT VARCHAR2,
                           p_operation_unit   IN     VARCHAR2,
                           p_request_id       IN     NUMBER,
                           p_user_id          IN     NUMBER);

   PROCEDURE write_log (buff VARCHAR2, which VARCHAR2);

   FUNCTION listing_file (
      p_request_id          IN     NUMBER,
      p_master_request_id   IN     NUMBER,
      o_file_rec               OUT tac_po_req_file_tbl_type)
      RETURN BOOLEAN;

   PROCEDURE start_up (p_operation_unit   IN VARCHAR2,
                       p_request_id       IN NUMBER,
                       p_user_id          IN NUMBER);

   PROCEDURE main (errbuf                OUT VARCHAR2,
                   errcode               OUT VARCHAR2,
                   p_operation_unit   IN     VARCHAR2,
                   p_user_id          IN     NUMBER);
END TAC_PO_REQUISITIONS_PKG;
/

