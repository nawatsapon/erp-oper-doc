CREATE OR REPLACE PACKAGE BODY APPS.TACSMT_LOG_MONITOR_PKG
AS
   /******************************************************************************************************************************************************
     Package Name        :TACSMT_LOG_MONITOR_PKG
     Description         :This package is modified for moving pr data from source concession to destination concession

     Revisions:
     Ver             Date                Author                      Description
     ---------       ---------           ---------------             ----------------------------------
     1.0             25/07/2016        Suttisak Uamornlert         Package creation
    *****************************************************************************************************************************************************/
   /*-------------------------------*/
   /*  parameter p_debug_type       */
   /*  log -- FND_FILE.LOG          */
   /*  output -- FND_FILE.OUTPUT    */
   /*-------------------------------*/


   PROCEDURE call_logmonitor (p_program           IN VARCHAR2,
                              p_conc_request_id   IN NUMBER)
   IS
      l_poimport_req_id   NUMBER;

      l_finished          BOOLEAN;
      l_phase             VARCHAR2 (100);
      l_status            VARCHAR2 (100);
      l_dev_phase         VARCHAR2 (100);
      l_dev_status        VARCHAR2 (100);
      l_message           VARCHAR2 (100);
      l_check_data        VARCHAR2 (1);
   BEGIN
      l_poimport_req_id :=
         fnd_request.submit_request (application   => 'SQLAP',
                                     program       => 'TACSMT006',
                                     sub_request   => FALSE,
                                     argument1     => p_program, --'DTINVHUB002',
                                     argument2     => p_conc_request_id,
                                     argument3     => 'N');
      COMMIT;

      LOOP
         l_finished :=
            fnd_concurrent.wait_for_request (
               request_id   => l_poimport_req_id,
               interval     => 5,
               max_wait     => 0,
               phase        => l_phase,
               status       => l_status,
               dev_phase    => l_dev_phase,
               dev_status   => l_dev_status,
               MESSAGE      => l_message);

         EXIT WHEN l_phase = 'Completed';
         DBMS_LOCK.SLEEP (10);
      END LOOP;
   END;



   PROCEDURE log_SMT002 (p_concurrent_id IN NUMBER, p_filename IN VARCHAR2)
   IS
      l_destFile   UTL_FILE.file_type;
      l_path       dba_directories.directory_path%TYPE;
      l_file       VARCHAR2 (100);
      l_writetxt   VARCHAR2 (1000);

      CURSOR c_detail (
         p_req_id IN NUMBER)
      IS
         SELECT    level_type
                || '|'
                || processing_time
                || '|'
                || ERROR_TYPE
                || '|'
                || batch_id
                || '|'
                || operating_unit
                || '|'
                || pr_number
                || '|'
                || pr_status
                || '|'
                || pr_line
                || '|'
                || purchase_categories
                || '|'
                || line_desc
                || '|'
                || quantity
                || '|'
                || uom
                || '|'
                || unit_price
                || '|'
                || supplier_name
                || '|'
                || supplier_site
                || '|'
                || ACCOUNT
                || '|'
                || requester
                || '|'
                || ERROR_CODE
                || '|'
                || error_message
                   log_txt
           FROM TACSMT_LOG_DETAIL
          WHERE request_id = p_req_id;
   BEGIN
      DBMS_LOCK.SLEEP (60);
      g_step := 'in log_SMT002';
      fnd_file.put_line (fnd_file.LOG, g_step);

      g_step := 'create file';
      fnd_file.put_line (fnd_file.LOG, g_step);
      l_file := p_filename || TO_CHAR (SYSDATE, 'DDMMYYYY_HH24MI') || '.txt';


      g_step := 'open file :' || l_file;
      fnd_file.put_line (fnd_file.LOG, g_step);
      fnd_file.put_line (fnd_file.LOG,
                         'g_default_splunk_log:' || g_default_splunk_log);
      l_destFile := UTL_FILE.FOPEN_NCHAR (g_default_splunk_log, l_file, 'A');



      IF UTL_FILE.is_open (l_destFile)
      THEN
         --Write Header section
         fnd_file.put_line (fnd_file.LOG, 'Start write header');

         SELECT    level_type
                || '|'
                || request_id
                || '|'
                || program_name
                || '|'
                || TO_CHAR (process_start, 'dd/mm/yyyy HH24:MI')
                || '|'
                || TO_CHAR (process_finish, 'dd/mm/yyyy HH24:MI')
                || '|'
                || processing_type
                || '|'
                || processing_time
                || '|'
                || record_count_completion
                || '|'
                || record_count_failed
                || '|'
                || record_count_total
           INTO l_writetxt
           FROM TACSMT_LOG_SUMMARY
          WHERE request_id = p_concurrent_id AND ROWNUM < 2;

         UTL_FILE.PUTF_NCHAR (l_destFile, l_writetxt || CHR (10));
         UTL_FILE.FFLUSH (l_destFile);

         --Write detail section

         FOR i IN c_detail (p_concurrent_id)
         LOOP
            l_writetxt := i.log_txt;
            fnd_file.put_line (fnd_file.LOG, l_writetxt);
            UTL_FILE.PUT_LINE_NCHAR (l_destFile, l_writetxt);
            UTL_FILE.FFLUSH (l_destFile);
         END LOOP;

         g_step := 'Close file';
         UTL_FILE.fclose (l_destFile);
      ELSE
         fnd_file.put_line (fnd_file.LOG, 'Couldn''t OPEN FILE');
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            'Error in step ' || g_step || ' : ' || SQLERRM);
   END;

   PROCEDURE log_SMT003 (p_concurrent_id IN NUMBER, p_filename IN VARCHAR2)
   IS
      l_destFile   UTL_FILE.file_type;
      l_path       dba_directories.directory_path%TYPE;
      l_file       VARCHAR2 (100);
      l_writetxt   VARCHAR2 (1000);

      CURSOR c_detail (
         p_req_id IN NUMBER)
      IS
         SELECT    level_type
                || '|'
                || processing_time
                || '|'
                || ERROR_TYPE
                || '|'
                || operating_unit
                || '|'
                || TO_CHAR (date_from, 'dd/mm/yyyy HH24:MI')
                || '|'
                || TO_CHAR (date_to, 'dd/mm/yyyy HH24:MI')
                || '|'
                || record_complete
                || '|'
                || record_failed
                || '|'
                || record_count                                                
                || '|'
                || record_flag
                || '|'
                || ERROR_CODE
                || '|'
                || error_message
                   log_txt
           FROM TACSMT_LOG_DETAIL
          WHERE request_id = p_req_id;
   BEGIN
      DBMS_LOCK.SLEEP (60);
      g_step := 'in log_SMT003';
      fnd_file.put_line (fnd_file.LOG, g_step);

      g_step := 'create file';
      fnd_file.put_line (fnd_file.LOG, g_step);
      l_file := p_filename || TO_CHAR (SYSDATE, 'DDMMYYYY_HH24MI') || '.txt';


      g_step := 'open file :' || l_file;
      fnd_file.put_line (fnd_file.LOG, g_step);
      fnd_file.put_line (fnd_file.LOG,
                         'g_default_splunk_log:' || g_default_splunk_log);
      l_destFile := UTL_FILE.FOPEN_NCHAR (g_default_splunk_log, l_file, 'A');



      IF UTL_FILE.is_open (l_destFile)
      THEN
         --Write Header section
         fnd_file.put_line (fnd_file.LOG, 'Start write header');

         SELECT    level_type
                || '|'
                || request_id
                || '|'
                || program_name
                || '|'
                || TO_CHAR (process_start, 'dd/mm/yyyy HH24:MI')
                || '|'
                || TO_CHAR (process_finish, 'dd/mm/yyyy HH24:MI')
                || '|'
                || processing_type
                || '|'
                || processing_time
                || '|'
                || record_count_completion
                || '|'
                || record_count_failed
                || '|'
                || record_count_total
           INTO l_writetxt
           FROM TACSMT_LOG_SUMMARY
          WHERE request_id = p_concurrent_id;

         UTL_FILE.PUTF_NCHAR (l_destFile, l_writetxt || CHR (10));
         UTL_FILE.FFLUSH (l_destFile);

         --Write detail section

         FOR i IN c_detail (p_concurrent_id)
         LOOP
            l_writetxt := i.log_txt;
            fnd_file.put_line (fnd_file.LOG, l_writetxt);
            UTL_FILE.PUT_LINE_NCHAR (l_destFile, l_writetxt);
            UTL_FILE.FFLUSH (l_destFile);
         END LOOP;

         g_step := 'Close file';
         UTL_FILE.fclose (l_destFile);
      ELSE
         fnd_file.put_line (fnd_file.LOG, 'Couldn''t OPEN FILE');
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            'Error in step ' || g_step || ' : ' || SQLERRM);
   END;


   PROCEDURE log_SMT004 (p_concurrent_id IN NUMBER, p_filename IN VARCHAR2)
   IS
      l_destFile   UTL_FILE.file_type;
      l_path       dba_directories.directory_path%TYPE;
      l_file       VARCHAR2 (100);
      l_writetxt   VARCHAR2 (1000);

      CURSOR c_detail (
         p_req_id IN NUMBER)
      IS
         SELECT    x.level_type
                || '|'
                || x.processing_time
                || '|'
                || x.ERROR_TYPE
                || '|'
                || nvl((select segment1 from po_vendors where vendor_name = x.supplier_name), '')
                || '|'
                || x.supplier_name
                || '|'
                || x.supplier_site
                || '|'
                || x.operating_unit
                || '|'
                || x.record_flag
                || '|'
                || TO_CHAR (x.DATE_FROM, 'dd/mm/yyyy HH24:MI')
                || '|'
                || x.ERROR_CODE                
                || '|'
                || x.error_message
                   log_txt
           FROM TACSMT_LOG_DETAIL x
          WHERE x.request_id = p_req_id;
   BEGIN
      DBMS_LOCK.SLEEP (60);
      g_step := 'in log_SMT002';
      fnd_file.put_line (fnd_file.LOG, g_step);

      g_step := 'create file';
      fnd_file.put_line (fnd_file.LOG, g_step);
      l_file := p_filename || TO_CHAR (SYSDATE, 'DDMMYYYY_HH24MI') || '.txt';


      g_step := 'open file :' || l_file;
      fnd_file.put_line (fnd_file.LOG, g_step);
      fnd_file.put_line (fnd_file.LOG,
                         'g_default_splunk_log:' || g_default_splunk_log);
      l_destFile := UTL_FILE.FOPEN_NCHAR (g_default_splunk_log, l_file, 'A');



      IF UTL_FILE.is_open (l_destFile)
      THEN
         --Write Header section
         fnd_file.put_line (fnd_file.LOG, 'Start write header');

         SELECT    level_type
                || '|'
                || request_id
                || '|'
                || program_name
                || '|'
                ||  TO_CHAR (process_start, 'dd/mm/yyyy HH24:MI')
                || '|'
                ||  TO_CHAR (process_finish, 'dd/mm/yyyy HH24:MI')
                || '|'
                || processing_type
                || '|'
                || processing_time
                || '|'
                || record_count_completion
                || '|'
                || record_count_failed
                || '|'
                || record_count_total
           INTO l_writetxt
           FROM TACSMT_LOG_SUMMARY
          WHERE request_id = p_concurrent_id;

         UTL_FILE.PUTF_NCHAR (l_destFile, l_writetxt || CHR (10));
         UTL_FILE.FFLUSH (l_destFile);

         --Write detail section

         FOR i IN c_detail (p_concurrent_id)
         LOOP
            l_writetxt := i.log_txt;
            fnd_file.put_line (fnd_file.LOG, l_writetxt);
            UTL_FILE.PUT_LINE_NCHAR (l_destFile, l_writetxt);
            UTL_FILE.FFLUSH (l_destFile);
         END LOOP;

         g_step := 'Close file';
         UTL_FILE.fclose (l_destFile);
      ELSE
         fnd_file.put_line (fnd_file.LOG, 'Couldn''t OPEN FILE');
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            'Error in step ' || g_step || ' : ' || SQLERRM);
   END;
   
   PROCEDURE log_SMT005 (p_concurrent_id IN NUMBER, p_filename IN VARCHAR2)
   IS
      l_destFile   UTL_FILE.file_type;
      l_path       dba_directories.directory_path%TYPE;
      l_file       VARCHAR2 (100);
      l_writetxt   VARCHAR2 (1000);

      CURSOR c_detail (
         p_req_id IN NUMBER)
      IS
         SELECT    x.level_type
                || '|'
                || x.processing_time
                || '|'
                || x.ERROR_TYPE
                || '|'
                || x.batch_id
                || '|'
                || x.invoice_number
                || '|'
                || x.invoice_status
                || '|'
                || TO_CHAR (x.invoice_date, 'dd/mm/yyyy')
                || '|'
                || x.invoice_amount
                || '|'
                || x.supplier_name
                || '|'
                || x.supplier_site
                || '|'
                || x.payee_name
                || '|'
                || x.operating_unit
                || '|'
                || x.ERROR_CODE                
                || '|'
                || x.error_message
                   log_txt
           FROM TACSMT_LOG_DETAIL x
          WHERE x.request_id = p_req_id;
   BEGIN
      DBMS_LOCK.SLEEP (60);
      g_step := 'in log_SMT002';
      fnd_file.put_line (fnd_file.LOG, g_step);

      g_step := 'create file';
      fnd_file.put_line (fnd_file.LOG, g_step);
      l_file := p_filename || TO_CHAR (SYSDATE, 'DDMMYYYY_HH24MISS') || '.txt';


      g_step := 'open file :' || l_file;
      fnd_file.put_line (fnd_file.LOG, g_step);
      fnd_file.put_line (fnd_file.LOG,
                         'g_default_splunk_log:' || g_default_splunk_log);
      l_destFile := UTL_FILE.FOPEN_NCHAR (g_default_splunk_log, l_file, 'A');



      IF UTL_FILE.is_open (l_destFile)
      THEN
         --Write Header section
         fnd_file.put_line (fnd_file.LOG, 'Start write header');

         SELECT    level_type
                || '|'
                || request_id
                || '|'
                || program_name
                || '|'
                ||  TO_CHAR (process_start, 'dd/mm/yyyy HH24:MI')
                || '|'
                ||  TO_CHAR (process_finish, 'dd/mm/yyyy HH24:MI')
                || '|'
                || processing_type
                || '|'
                || processing_time
                || '|'
                || record_count_completion
                || '|'
                || record_count_failed
                || '|'
                || record_count_total
           INTO l_writetxt
           FROM TACSMT_LOG_SUMMARY
          WHERE request_id = p_concurrent_id;

         UTL_FILE.PUTF_NCHAR (l_destFile, l_writetxt || CHR (10));
         UTL_FILE.FFLUSH (l_destFile);

         --Write detail section

         FOR i IN c_detail (p_concurrent_id)
         LOOP
            l_writetxt := i.log_txt;
            fnd_file.put_line (fnd_file.LOG, l_writetxt);
            UTL_FILE.PUT_LINE_NCHAR (l_destFile, l_writetxt);
            UTL_FILE.FFLUSH (l_destFile);
         END LOOP;

         g_step := 'Close file';
         UTL_FILE.fclose (l_destFile);
      ELSE
         fnd_file.put_line (fnd_file.LOG, 'Couldn''t OPEN FILE');
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            'Error in step ' || g_step || ' : ' || SQLERRM);
   END;
   


   PROCEDURE Log_SMTADDITION (p_concurrent_id   IN NUMBER,
                              p_filename        IN VARCHAR2)
   IS
      l_destFile   UTL_FILE.file_type;
      l_path       dba_directories.directory_path%TYPE;
      l_file       VARCHAR2 (100);
      l_writetxt   VARCHAR2 (1000);

      CURSOR c_detail (
         p_req_id IN NUMBER)
      IS
         SELECT    level_type
                || '|'
                || processing_time
                || '|'
                || ERROR_TYPE
                || '|'
                || operating_unit
                || '|'
                || invoice_number
                || '|'
                || invoice_amount
                || '|'
                || invoice_status
                || '|'
                || invoice_date
                || '|'
                || supplier_name
                || '|'
                || supplier_site
                || '|'
                || po_number
                || '|'
                || batch_id
                || '|'                
                || ERROR_CODE
                || '|'
                || error_message
                   log_txt
           FROM TACSMT_LOG_DETAIL
          WHERE request_id = p_req_id;
   BEGIN
      DBMS_LOCK.SLEEP (60);
      g_step := 'in log_SMT002';
      fnd_file.put_line (fnd_file.LOG, g_step);

      g_step := 'create file';
      fnd_file.put_line (fnd_file.LOG, g_step);
      l_file := p_filename || TO_CHAR (SYSDATE, 'DDMMYYYY_HH24MI') || '.txt';


      g_step := 'open file :' || l_file;
      fnd_file.put_line (fnd_file.LOG, g_step);
      fnd_file.put_line (fnd_file.LOG,
                         'g_default_splunk_log:' || g_default_splunk_log);
      l_destFile := UTL_FILE.FOPEN_NCHAR (g_default_splunk_log, l_file, 'A');



      IF UTL_FILE.is_open (l_destFile)
      THEN
         --Write Header section
         fnd_file.put_line (fnd_file.LOG, 'Start write header');

         SELECT    level_type
                || '|'
                || request_id
                || '|'
                || program_name
                || '|'
                || TO_CHAR (process_start, 'dd/mm/yyyy HH24:MI')
                || '|'
                || TO_CHAR (process_finish, 'dd/mm/yyyy HH24:MI')
                || '|'
                || processing_type
                || '|'
                || processing_time
                || '|'
                || record_count_completion
                || '|'
                || record_count_failed
                || '|'
                || record_count_total
           INTO l_writetxt
           FROM TACSMT_LOG_SUMMARY
          WHERE request_id = p_concurrent_id;

         UTL_FILE.PUTF_NCHAR (l_destFile, l_writetxt || CHR (10));
         UTL_FILE.FFLUSH (l_destFile);

         --Write detail section

         FOR i IN c_detail (p_concurrent_id)
         LOOP
            l_writetxt := i.log_txt;
            fnd_file.put_line (fnd_file.LOG, l_writetxt);
            UTL_FILE.PUT_LINE_NCHAR (l_destFile, l_writetxt);
            UTL_FILE.FFLUSH (l_destFile);
         END LOOP;

         g_step := 'Close file';
         UTL_FILE.fclose (l_destFile);
      ELSE
         fnd_file.put_line (fnd_file.LOG, 'Couldn''t OPEN FILE');
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            'Error in step ' || g_step || ' : ' || SQLERRM);
   END;



   PROCEDURE LOG_MONITOR (errbuf               OUT VARCHAR2,
                          errcode              OUT VARCHAR2,
                          p_program_name    IN     VARCHAR2,
                          p_concurrent_id   IN     NUMBER-- p_folder_name   in varchar2,
                          )
   IS
      l_filename_SMT002        VARCHAR2 (50) := 'TACSMT_PRINBOUND_';
      l_filename_SMT003        VARCHAR2 (50) := 'TACSMT_PMTOUTBOUND_';
      l_filename_SMT004        VARCHAR2 (50) := 'TACSMT_SUPINBOUND_';
      l_filename_SMT005        VARCHAR2 (50) := 'TACSMT_APINBOUND_';
      l_filename_SMTADDITION   VARCHAR2 (50) := 'TACSMT_POAPOUTBOUND_';
   BEGIN
      --g_debug_flag := p_debug_flag;
      IF p_program_name = 'SMT002'
      THEN
         log_SMT002 (p_concurrent_id, l_filename_SMT002);
      ELSIF p_program_name = 'SMT003'
      THEN
         log_SMT003 (p_concurrent_id, l_filename_SMT003);
      ELSIF p_program_name = 'SMT004'
      THEN
         log_SMT004 (p_concurrent_id, l_filename_SMT004);
      ELSIF p_program_name = 'SMT005'
      THEN
         log_SMT005 (p_concurrent_id, l_filename_SMT005);         
      ELSIF p_program_name = 'SMTADDITION'
      THEN
         Log_SMTADDITION (p_concurrent_id, l_filename_SMTADDITION);
      END IF;
   END;
END tacsmt_log_monitor_pkg;
/

