CREATE OR REPLACE package APPS.tacsmt_log_monitor_pkg
AS
  /* DECLARE GLOBAL VARIABLE  */

    

--
--  TYPE t_grn_info IS TABLE OF r_grn_info INDEX BY BINARY_INTEGER ;
   TYPE t_log_detail IS TABLE OF apps.DTINVHUB_LOG_DETAIL%ROWTYPE INDEX BY BINARY_INTEGER;
  g_log_detail t_log_detail;
  g_log_detail_seq NUMBER :=0;

  g_default_splunk_log    VARCHAR2(50) :='SMT_SPLUNK';
  g_step  VARCHAR2(100);

  /*
    CREATE DIRECTORY INVHUB_BACKINV_INBOX AS '/var/tmp/INVOICEHUB/OUT_BOUND/BACKINVOICE/INBOX';
    GRANT READ ON DIRECTORY INVHUB_BACKINV_INBOX TO PUBLIC;

    CREATE DIRECTORY INVHUB_BACKINV_INBOX AS '/var/tmp/INVOICEHUB/OUT_BOUND/SUPPLIER/INBOX';
    GRANT READ ON DIRECTORY INVHUB_BACKINV_INBOX TO PUBLIC;
  */

   PROCEDURE call_logmonitor(p_program IN varchar2,p_conc_request_id IN NUMBER);

   PROCEDURE LOG_MONITOR( errbuf       OUT VARCHAR2,
                          errcode      OUT VARCHAR2,
                          p_program_name  in varchar2,
                          p_concurrent_id in NUMBER);
END tacsmt_log_monitor_pkg;
/

