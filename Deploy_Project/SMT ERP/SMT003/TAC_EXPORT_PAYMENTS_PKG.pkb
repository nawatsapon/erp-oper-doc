CREATE OR REPLACE PACKAGE BODY APPS.tac_export_payments_pkg
IS
   PROCEDURE main_process (p_request_id   IN NUMBER,
                           p_user_id      IN NUMBER,
                           p_org_id       IN NUMBER)
   IS
      CURSOR ch
      IS
           SELECT p_request_id request_id,
                  aip.last_update_date,
                  ou.name operating_unit,
                  map.batch_id,
                  ai.invoice_num,
                  ai.source,
                  aip.reversal_flag,
                  aip.amount invoice_payment_amount,
                  ac.check_number document_number,
                  ac.doc_sequence_value voucher_number,
                  ac.bank_account_name bank_account,
                  ast.name payment_document,
                  ac.payment_method_lookup_code payment_method,
                  ac.status_lookup_code payment_status,
                  ac.check_date payment_date,
                  ac.void_date,
                  ac.cleared_date,
                  DECODE (ad.match_status_flag,
                          'A', 'Validated',
                          'T', 'Needs Revalidation',
                          'N', 'Never Validated')
                     invoice_status,
                  DECODE (ai.payment_status_flag,
                          'Y', 'Full Paid',
                          'P', 'Partial Paid',
                          'N', 'Not Paid')
                     invoice_paid_status,
                  ai.amount_paid invoice_amount_paid,
                  SYSDATE export_date,
                  p_user_id exported_by,
                  ai.org_id
             FROM ap_invoice_payments_all aip,
                  ap_invoices_all ai,
                  ap_checks_all ac,
                  ap_check_stocks_all ast,
                  hr_operating_units ou,
                  tac_req_mapping_invoice map,
                  ap_invoice_distributions_all ad
            WHERE     aip.invoice_id = ai.invoice_id
                  AND aip.org_id = p_org_id
                  AND aip.check_id = ac.check_id
                  AND ac.check_stock_id = ast.check_stock_id
                  AND aip.org_id = ou.organization_id
                  AND ai.SOURCE = 'SMT'
                  --AND DECODE (aip.reversal_flag, 'Y', aip.amount, -1) < 0
                  AND ai.invoice_id = ad.invoice_id(+)
                  AND ad.line_type_lookup_code = 'ITEM'
                  AND NVL (ad.cancellation_flag, 'N') = 'N'
                  --                  AND aip.reversal_inv_pmt_id IS NOT NULL
                  AND ai.invoice_num = map.invoice_num(+)
                  AND aip.last_update_date > (SELECT MAX (last_update_date)
                                                FROM tac_export_payment_to_smt
                                               WHERE org_id = p_org_id)
         --                  AND ROWNUM < 5
         ORDER BY aip.last_update_date DESC;

      payment_rec   payment_tbl_type;
      pay_tab       payment_tbl_type;
      i             BINARY_INTEGER := 0;
   BEGIN
      OPEN ch;

      LOOP
         i := i + 1;

         FETCH ch INTO pay_tab (i);

         EXIT WHEN ch%NOTFOUND;                    /* cause of missing rows */

         insert_to_logs (pay_tab (i));
      END LOOP;

      write_file (pay_tab, p_org_id);
   END;

   PROCEDURE write_file (p_records IN payment_tbl_type, p_org_id IN NUMBER)
   IS
      fs               UTL_FILE.file_type;
      -- h             h_rec;
      l_src_filename   VARCHAR2 (100);
      vdr              VARCHAR2 (4000);                        -- count detail
      l_ou             VARCHAR2 (5);
      l_last_update    DATE := SYSDATE;
   BEGIN
      DBMS_OUTPUT.put_line ('');

      SELECT MAX (last_update_date)
        INTO l_last_update
        FROM tac_export_payment_to_smt
       WHERE org_id = p_org_id;

      SELECT DECODE (p_org_id, 102, 'DTAC', 'DTN') INTO l_ou FROM DUAL;

      l_src_filename :=
            'SMT_PAIDOUTBOUND_'
         || TO_CHAR (SYSDATE, 'ddmmyyy_hh24mmss')
         || '_'
         || l_ou
         || '.txt';
      fs := UTL_FILE.fopen ('SMT003_OUT', l_src_filename, 'W');

      FOR r IN p_records.FIRST .. p_records.LAST
      LOOP
         vdr :=
               TO_CHAR (p_records (r).last_update_date,
                        'ddmmyyyy hh24:mm:ss')
            || '|'
            || p_records (r).operating_unit
            || '|'
            || p_records (r).batch_id
            || '|'
            || p_records (r).invoice_num
            || '|'
            --|| p_records (r).source|| '|'
            || p_records (r).reversal_flag
            || '|'
            || p_records (r).invoice_payment_amount
            || '|'
            || p_records (r).document_number
            || '|'
            || p_records (r).voucher_number
            || '|'
            || p_records (r).bank_account
            || '|'
            || p_records (r).payment_document
            || '|'
            || p_records (r).payment_method
            || '|'
            || p_records (r).payment_status
            || '|'
            || p_records (r).payment_date
            || '|'
            || p_records (r).void_date
            || '|'
            || p_records (r).cleared_date
            || '|'
            || p_records (r).invoice_status
            || '|'
            || p_records (r).invoice_paid_status
            || '|'
            || p_records (r).invoice_amount_paid;
         UTL_FILE.put_line (fs, vdr);
      END LOOP;


      UTL_FILE.fclose (fs);
   EXCEPTION
      WHEN OTHERS
      THEN
         UTL_FILE.put (
            fs,
               'No Payment record found since'
            || TO_CHAR (l_last_update, ' <DD-MON-YYYY HH24:MM:SS>.'));
         UTL_FILE.fclose (fs);
   END;

   PROCEDURE insert_to_logs (p_records IN tac_export_payment_to_smt%ROWTYPE)
   IS
   BEGIN
      INSERT INTO tac_export_payment_to_smt
           VALUES p_records;
   END;

   PROCEDURE create_logs (errbuf OUT VARCHAR2, errcode OUT VARCHAR2)
   IS
      l_today_timestamp           TIMESTAMP := SYSTIMESTAMP;
      p_end_date                  DATE;

      l_program_name              VARCHAR2 (50 BYTE) := 'TACSMT003';
      l_record_count_completion   NUMBER;
      l_record_count_failed       NUMBER;
      l_record_count_total        NUMBER;

      l_hour                      NUMBER;
      l_minute                    NUMBER;
      l_second                    NUMBER;

      l_pr_number                 VARCHAR2 (50 BYTE);
      l_pr_line                   NUMBER;
      l_pr_status                 VARCHAR2 (50 BYTE);
      l_po_number                 VARCHAR2 (50 BYTE);
      l_account                   VARCHAR2 (100 BYTE);

      v_process_start             DATE;
      v_process_finish            DATE;
      v_process_time              VARCHAR2 (20);

      l_request_id                NUMBER;
      l_imp_request_id            NUMBER;
      l_req_return_status         BOOLEAN;

      lc_phase                    VARCHAR2 (50);
      lc_status                   VARCHAR2 (50);
      lc_dev_phase                VARCHAR2 (50);
      lc_dev_status               VARCHAR2 (50);
      lc_message                  VARCHAR2 (50);

      CURSOR c_line (
         i_request_id NUMBER)
      IS
           SELECT s.request_id,
                  hr.name operating_unit,
                  'Completed' status,
                  '' ERROR_CODE,
                  s.org_id,
                  COUNT (*) record_complete,
                  0 record_failed,
                  COUNT (*) recode_count,
                  MIN (s.last_update_date) date_from,
                  MAX (s.last_update_date) date_to
             FROM tac_export_payment_to_smt s, hr_all_organization_units hr
            WHERE     s.org_id = hr.organization_id
                  AND hr.TYPE = 'OPERATION UNIT'
                  AND s.request_id = i_request_id
         GROUP BY s.request_id, hr.name, s.org_id;

      v_request_id                NUMBER;
      v_start_date                DATE;
   BEGIN
      SELECT MAX (request_id)
        INTO v_request_id
        FROM tac_export_payment_to_smt;

      SELECT actual_start_date
        INTO v_start_date
        FROM FND_CONC_REQ_SUMMARY_V
       WHERE request_id = v_request_id;

      v_process_finish := SYSDATE;
      v_process_time :=
         TO_CHAR (
            TO_TIMESTAMP (
               TO_CHAR (
                    TO_DATE ('00:00:00', 'HH24:MI:SS')
                  + (v_process_finish - v_start_date),
                  'HH24:MI:SS'),
               'HH24:MI:SS.FF'),
            'HH24:MI:SS.FF');

      FOR i IN c_line (v_request_id)
      LOOP
         BEGIN
            INSERT INTO TACSMT_LOG_DETAIL (level_type,
                                           process_start,
                                           process_finish,
                                           processing_time,
                                           request_id,
                                           ERROR_TYPE,
                                           batch_id,
                                           operating_unit,
                                           pr_number,
                                           pr_line,
                                           pr_status,
                                           po_number,
                                           purchase_categories,
                                           line_desc,
                                           quantity,
                                           uom,
                                           unit_price,
                                           supplier_name,
                                           supplier_site,
                                           account,
                                           requester,
                                           date_from,
                                           date_to,
                                           invoice_number,
                                           invoice_amount,
                                           invoice_status,
                                           invoice_date,
                                           record_complete,
                                           record_failed,
                                           record_count,
                                           record_flag,
                                           supplier_number,
                                           ERROR_CODE,
                                           error_message)
                 VALUES (
                           'D',
                           v_start_date,
                           TO_DATE (
                              TO_CHAR (l_today_timestamp,
                                       'YYYY-MON-DD HH24:MI:SS'),
                              'YYYY-MON-DD HH24:MI:SS'),
                           v_process_time,
                           i.request_id,
                           'Export Payment',
                           NULL,
                           i.operating_unit,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           i.date_from,
                           i.date_to,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           i.record_complete,
                           i.record_failed,
                           i.recode_count,
                           NULL,
                           NULL,
                           i.status,
                           i.ERROR_CODE);
         EXCEPTION
            WHEN OTHERS
            THEN
               RAISE;
         END;
      END LOOP;

      SELECT 0 record_count_failed, COUNT (*) record_count_total
        INTO l_record_count_failed, l_record_count_total
        FROM tac_export_payment_to_smt stg
       WHERE stg.request_id = v_request_id;

      INSERT INTO TACSMT_LOG_SUMMARY (level_type,
                                      request_id,
                                      program_name,
                                      process_start,
                                      process_finish,
                                      processing_type,
                                      processing_time,
                                      record_count_completion,
                                      record_count_failed,
                                      record_count_total)
           VALUES (
                     'H',
                     v_request_id,
                     l_program_name,
                     v_start_date,
                     TO_DATE (
                        TO_CHAR (l_today_timestamp, 'YYYY-MON-DD HH24:MI:SS'),
                        'YYYY-MON-DD HH24:MI:SS'),
                     'Upload',
                     v_process_time,
                     l_record_count_total,
                     l_record_count_failed,
                     l_record_count_total);

      COMMIT;

      l_request_id :=
         fnd_request.submit_request ('SQLAP',
                                     'TACSMT006',
                                     NULL,
                                     NULL,
                                     FALSE,
                                     'SMT003',                         ---arg1
                                     v_request_id);

      COMMIT;

      IF l_request_id = 0
      THEN
         write_log (
            'Request Not Submitted due to "' || fnd_message.GET || '".',
            'LOG');
      ELSE
         write_log (
            'Program submitted successfully Request id :' || l_request_id,
            'LOG');
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RAISE;
   END;

   PROCEDURE write_log (buff VARCHAR2, which VARCHAR2)
   IS
   BEGIN
      IF UPPER (which) = 'LOG'
      THEN
         fnd_file.put_line (FND_FILE.LOG, buff);
      ELSE
         fnd_file.put_line (FND_FILE.OUTPUT, buff);
      END IF;
   END write_log;
END;
/

