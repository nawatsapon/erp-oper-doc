CREATE OR REPLACE PACKAGE APPS.tac_export_payments_pkg
IS
   TYPE temp_rec_type IS RECORD
   (
      request_id               tac_export_payment_to_smt.request_id%TYPE,
      last_update_date         tac_export_payment_to_smt.last_update_date%TYPE,
      operating_unit           tac_export_payment_to_smt.operating_unit%TYPE,
      batch_id                 tac_export_payment_to_smt.batch_id%TYPE,
      invoice_num              tac_export_payment_to_smt.invoice_num%TYPE,
      source                   tac_export_payment_to_smt.source%TYPE,
      reversal_flag            tac_export_payment_to_smt.reversal_flag%TYPE,
      invoice_payment_amount   tac_export_payment_to_smt.invoice_payment_amount%TYPE,
      document_number          tac_export_payment_to_smt.document_number%TYPE,
      voucher_number           tac_export_payment_to_smt.voucher_number%TYPE,
      bank_account             tac_export_payment_to_smt.bank_account%TYPE,
      payment_document         tac_export_payment_to_smt.payment_document%TYPE,
      payment_method           tac_export_payment_to_smt.payment_method%TYPE,
      payment_status           tac_export_payment_to_smt.payment_status%TYPE,
      payment_date             tac_export_payment_to_smt.payment_date%TYPE,
      void_date                tac_export_payment_to_smt.void_date%TYPE,
      cleared_date             tac_export_payment_to_smt.cleared_date%TYPE,
      invoice_status           tac_export_payment_to_smt.invoice_status%TYPE,
      invoice_paid_status      tac_export_payment_to_smt.invoice_paid_status%TYPE,
      invoice_amount_paid      tac_export_payment_to_smt.invoice_amount_paid%TYPE,
      export_date              tac_export_payment_to_smt.export_date%TYPE,
      exported_by              tac_export_payment_to_smt.exported_by%TYPE,
      org_id                   tac_export_payment_to_smt.org_id%TYPE
   );

   TYPE temp_tbl_type IS TABLE OF temp_rec_type
                            INDEX BY BINARY_INTEGER;

   TYPE payment_tbl_type IS TABLE OF tac_export_payment_to_smt%ROWTYPE
                               INDEX BY BINARY_INTEGER;



   PROCEDURE main_process (p_request_id IN NUMBER, p_user_id IN NUMBER, p_org_id IN NUMBER);

  PROCEDURE write_file (p_records IN payment_tbl_type, p_org_id IN NUMBER);

   PROCEDURE insert_to_logs (p_records IN tac_export_payment_to_smt%ROWTYPE);
   
   PROCEDURE create_logs (errbuf OUT VARCHAR2, errcode OUT VARCHAR2);
   
   PROCEDURE write_log (buff VARCHAR2, which VARCHAR2);
END;
/

