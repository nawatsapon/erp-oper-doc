CREATE OR REPLACE PACKAGE BODY APPS.TAC_AP_INTERFACE_UTIL_SMT_PKG
IS
   ----------------------------------------------------------------------------------------------------
   PROCEDURE write_log (param_msg VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.LOG, param_msg);
   END write_log;

   PROCEDURE write_out (param_msg VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.output, param_msg);
   END write_out;

   PROCEDURE conc_wait (p_conc_req_id NUMBER)
   IS
      phase        VARCHAR2 (30);
      status       VARCHAR2 (30);
      dev_phase    VARCHAR2 (30);
      dev_status   VARCHAR2 (30);
      MESSAGE      VARCHAR2 (1000);
   BEGIN
      --commit;
      IF fnd_concurrent.wait_for_request (p_conc_req_id,
                                          10,
                                          0,
                                          phase,
                                          status,
                                          dev_phase,
                                          dev_status,
                                          MESSAGE)
      THEN
         NULL;
      END IF;
   END conc_wait;

   FUNCTION get_directory_path (p_directory_name VARCHAR2)
      RETURN VARCHAR2
   IS
      v_directory_path   VARCHAR2 (500);
   BEGIN
      SELECT dir.directory_path
        INTO v_directory_path
        FROM all_directories dir
       WHERE dir.directory_name = p_directory_name;

      RETURN v_directory_path;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_directory_path;

   FUNCTION char_to_date (p_date_char      VARCHAR2,
                          p_format_mask    VARCHAR2 DEFAULT NULL)
      RETURN DATE
   IS
      v_date   DATE;
   BEGIN
      IF (p_date_char IS NULL)
      THEN
         RETURN NULL;
      END IF;

      IF (p_format_mask IS NOT NULL)
      THEN
         v_date := TO_DATE (p_date_char, p_format_mask);
         RETURN v_date;
      END IF;

      -- Format Mask = dd/mm/yyyy
      BEGIN
         v_date := TO_DATE (p_date_char, 'dd/mm/yyyy');
         RETURN v_date;
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      -- Format Mask = dd/m/yyyy
      BEGIN
         v_date := TO_DATE (p_date_char, 'dd/m/yyyy');
         RETURN v_date;
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      -- Format Mask = d/mm/yyyy
      BEGIN
         v_date := TO_DATE (p_date_char, 'd/mm/yyyy');
         RETURN v_date;
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      -- Format Mask = d/m/yyyy
      BEGIN
         v_date := TO_DATE (p_date_char, 'd/m/yyyy');
         RETURN v_date;
      EXCEPTION
         WHEN OTHERS
         THEN
            RETURN p_date_char;
      END;
   END char_to_date;

   FUNCTION extrack_data (p_buffer      IN VARCHAR2,
                          p_separator   IN VARCHAR2,
                          p_pos_data    IN NUMBER)
      RETURN VARCHAR2
   IS
      v_pos_start    NUMBER;
      v_pos_end      NUMBER;
      v_subsrt_len   NUMBER;
      v_data         VARCHAR2 (255);
   BEGIN
      IF p_pos_data = 1
      THEN
         v_pos_start := 1;
      ELSE
         v_pos_start :=
              INSTR (p_buffer,
                     p_separator,
                     1,
                     p_pos_data - 1)
            + 1;
      END IF;

      v_pos_end :=
         INSTR (p_buffer,
                p_separator,
                1,
                p_pos_data);

      IF v_pos_end = 0
      THEN
         v_subsrt_len := LENGTH (p_buffer);
      ELSE
         v_subsrt_len := v_pos_end - v_pos_start;
      END IF;

      v_data := SUBSTR (p_buffer, v_pos_start, v_subsrt_len);

      RETURN v_data;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END extrack_data;

   FUNCTION split_data (p_line_buff    IN VARCHAR2,
                        p_delimiter    IN VARCHAR2 DEFAULT '|',
                        p_debug_flag      VARCHAR2 DEFAULT 'N')
      RETURN dtac_ap_invoices_temp_int%ROWTYPE
   IS
      v_rec_data        dtac_ap_invoices_temp_int%ROWTYPE;
      v_creation_date   DATE := SYSDATE;
   BEGIN
      IF p_line_buff IS NULL
      THEN
         RETURN v_rec_data;
      END IF;

      v_rec_data.GROUP_ID := extrack_data (p_line_buff, p_delimiter, 1);
      v_rec_data.source :=
         NVL (extrack_data (p_line_buff, p_delimiter, 2), 'SMT');
      v_rec_data.invoice_type_lookup_code :=
         extrack_data (p_line_buff, p_delimiter, 3);
      v_rec_data.vendor_num := extrack_data (p_line_buff, p_delimiter, 4);
      v_rec_data.vendor_site_code :=
         extrack_data (p_line_buff, p_delimiter, 5);
      v_rec_data.invoice_num := extrack_data (p_line_buff, p_delimiter, 6);
      v_rec_data.invoice_date :=
         char_to_date (extrack_data (p_line_buff, p_delimiter, 7));

      v_rec_data.doc_category_code :=
         extrack_data (p_line_buff, p_delimiter, 8);
      v_rec_data.invoice_currency_code :=
         extrack_data (p_line_buff, p_delimiter, 9);
      v_rec_data.invoice_amount := extrack_data (p_line_buff, p_delimiter, 10);
      v_rec_data.gl_date :=
         char_to_date (extrack_data (p_line_buff, p_delimiter, 11));
      v_rec_data.invoice_description :=
         extrack_data (p_line_buff, p_delimiter, 12);
      v_rec_data.terms_name := extrack_data (p_line_buff, p_delimiter, 13);
      v_rec_data.payment_method_lookup_code :=
         extrack_data (p_line_buff, p_delimiter, 14);
      v_rec_data.employee_number :=
         extrack_data (p_line_buff, p_delimiter, 15);
      v_rec_data.line_number := extrack_data (p_line_buff, p_delimiter, 16);
      v_rec_data.line_type_lookup_code :=
         extrack_data (p_line_buff, p_delimiter, 17);
      v_rec_data.line_amount := extrack_data (p_line_buff, p_delimiter, 18);
      v_rec_data.line_tax_code := extrack_data (p_line_buff, p_delimiter, 19);
      v_rec_data.awt_group_name := extrack_data (p_line_buff, p_delimiter, 20);
      v_rec_data.dist_code_concatinated :=
         extrack_data (p_line_buff, p_delimiter, 21);
      v_rec_data.line_description :=
         extrack_data (p_line_buff, p_delimiter, 22);
      v_rec_data.act_vendor_num := extrack_data (p_line_buff, p_delimiter, 23);
      v_rec_data.act_vendor_site_code :=
         extrack_data (p_line_buff, p_delimiter, 24);
      v_rec_data.reporting_entity :=
         extrack_data (p_line_buff, p_delimiter, 25);
      v_rec_data.tax_invoice_date :=
         char_to_date (extrack_data (p_line_buff, p_delimiter, 26));
      v_rec_data.tax_invoice_num :=
         extrack_data (p_line_buff, p_delimiter, 27);
      v_rec_data.tax_acct_period :=
         extrack_data (p_line_buff, p_delimiter, 28);
      v_rec_data.wht_acct_period :=
         extrack_data (p_line_buff, p_delimiter, 29);
      v_rec_data.phor_ngor_dor := extrack_data (p_line_buff, p_delimiter, 30);
      v_rec_data.wht_condition := extrack_data (p_line_buff, p_delimiter, 31);
      v_rec_data.wht_revenue_type :=
         extrack_data (p_line_buff, p_delimiter, 32);
      v_rec_data.wht_revenue_name :=
         extrack_data (p_line_buff, p_delimiter, 33);
      v_rec_data.project_number := extrack_data (p_line_buff, p_delimiter, 34);
      v_rec_data.project_task := extrack_data (p_line_buff, p_delimiter, 35);
      v_rec_data.expenditure_type :=
         extrack_data (p_line_buff, p_delimiter, 36);
      v_rec_data.expenditure_organization :=
         extrack_data (p_line_buff, p_delimiter, 37);

      v_rec_data.status := extrack_data (p_line_buff, p_delimiter, 40);
      v_rec_data.eai_crtd_dttm := v_creation_date;

      v_rec_data.payee := extrack_data (p_line_buff, p_delimiter, 41);
      v_rec_data.mail_to_name := extrack_data (p_line_buff, p_delimiter, 42);
      v_rec_data.mail_to_address1 :=
         extrack_data (p_line_buff, p_delimiter, 43);
      v_rec_data.mail_to_address2 :=
         extrack_data (p_line_buff, p_delimiter, 44);
      v_rec_data.mail_to_address3 :=
         extrack_data (p_line_buff, p_delimiter, 45);
      v_rec_data.mail_to_address4 :=
         extrack_data (p_line_buff, p_delimiter, 46);

      v_rec_data.receipt_number := '';
      v_rec_data.match_option := '';
      v_rec_data.bill_placement_date := '';
      v_rec_data.po_number := '';
      v_rec_data.resend_flag := '';
      v_rec_data.error_msg := '';
      v_rec_data.seqno := '';
      v_rec_data.ap_liability_account :=
         extrack_data (p_line_buff, p_delimiter, 47);
      v_rec_data.pay_group := extrack_data (p_line_buff, p_delimiter, 48);
      v_rec_data.wht_act_supplier_name :=
         extrack_data (p_line_buff, p_delimiter, 49);
      v_rec_data.wht_address1 := extrack_data (p_line_buff, p_delimiter, 50);
      v_rec_data.wht_address2 := extrack_data (p_line_buff, p_delimiter, 51);
      v_rec_data.wht_supplier_tax_id :=
         extrack_data (p_line_buff, p_delimiter, 52);
      v_rec_data.wht_supplier_id_card :=
         extrack_data (p_line_buff, p_delimiter, 53);

      /*Modify On 10/31/2016 by spw@ice to following.
      - Mapping between Requisition and Invoice.
      - Keep Batch ID in the AP Invoice.
      */
      v_rec_data.batch_id := extrack_data (p_line_buff, p_delimiter, 54);

      /*----------------End modify--------------------*/

      IF p_debug_flag = 'Y'
      THEN
         write_log (g_log_line);
         write_log ('Line buffer = ' || p_line_buff);
         write_log ('        group_id  = ' || v_rec_data.GROUP_ID);
         write_log ('        source  = ' || v_rec_data.source);
         write_log (
               '        invoice_type_lookup_code  = '
            || v_rec_data.invoice_type_lookup_code);
         write_log ('        vendor_num  = ' || v_rec_data.vendor_num);
         write_log (
            '        vendor_site_code  = ' || v_rec_data.vendor_site_code);
         write_log ('        invoice_num  = ' || v_rec_data.invoice_num);
         write_log ('        invoice_date  = ' || v_rec_data.invoice_date);
         write_log (
            '        doc_category_code  = ' || v_rec_data.doc_category_code);
         write_log (
               '        invoice_currency_code  = '
            || v_rec_data.invoice_currency_code);
         write_log (
            '        invoice_amount  = ' || v_rec_data.invoice_amount);
         write_log ('        gl_date  = ' || v_rec_data.gl_date);
         write_log (
               '        invoice_description  = '
            || v_rec_data.invoice_description);
         write_log ('        terms_name  = ' || v_rec_data.terms_name);
         write_log (
               '        payment_method_lookup_code  = '
            || v_rec_data.payment_method_lookup_code);
         write_log (
            '        employee_number  = ' || v_rec_data.employee_number);
         write_log ('        line_number  = ' || v_rec_data.line_number);
         write_log (
               '        line_type_lookup_code  = '
            || v_rec_data.line_type_lookup_code);
         write_log ('        line_amount  = ' || v_rec_data.line_amount);
         write_log ('        line_tax_code  = ' || v_rec_data.line_tax_code);
         write_log (
            '        awt_group_name  = ' || v_rec_data.awt_group_name);
         write_log (
               '        dist_code_concatinated  = '
            || v_rec_data.dist_code_concatinated);
         write_log (
            '        line_description  = ' || v_rec_data.line_description);
         write_log (
            '        act_vendor_num  = ' || v_rec_data.act_vendor_num);
         write_log (
               '        act_vendor_site_code  = '
            || v_rec_data.act_vendor_site_code);
         write_log (
            '        reporting_entity  = ' || v_rec_data.reporting_entity);
         write_log (
            '        tax_invoice_date  = ' || v_rec_data.tax_invoice_date);
         write_log (
            '        tax_invoice_num  = ' || v_rec_data.tax_invoice_num);
         write_log (
            '        tax_acct_period  = ' || v_rec_data.tax_acct_period);
         write_log (
            '        wht_acct_period  = ' || v_rec_data.wht_acct_period);
         write_log ('        phor_ngor_dor  = ' || v_rec_data.phor_ngor_dor);
         write_log ('        wht_condition  = ' || v_rec_data.wht_condition);
         write_log (
            '        wht_revenue_type  = ' || v_rec_data.wht_revenue_type);
         write_log (
            '        wht_revenue_name  = ' || v_rec_data.wht_revenue_name);
         write_log (
            '        project_number  = ' || v_rec_data.project_number);
         write_log ('        project_task  = ' || v_rec_data.project_task);
         write_log (
            '        expenditure_type  = ' || v_rec_data.expenditure_type);
         write_log (
               '        expenditure_organization  = '
            || v_rec_data.expenditure_organization);
         write_log ('        payee  = ' || v_rec_data.payee);
         write_log ('        mail_to_name  = ' || v_rec_data.mail_to_name);
         write_log (
            '        mail_to_address1  = ' || v_rec_data.mail_to_address1);
         write_log (
            '        mail_to_address2  = ' || v_rec_data.mail_to_address2);
         write_log (
            '        mail_to_address3  = ' || v_rec_data.mail_to_address3);
         write_log (
            '        mail_to_address4  = ' || v_rec_data.mail_to_address4);
         write_log (
               '        ap_liability_account  = '
            || v_rec_data.ap_liability_account);
         write_log ('        pay_group  = ' || v_rec_data.pay_group);
         write_log (
               '        wht_act_supplier_name  = '
            || v_rec_data.wht_act_supplier_name);
         write_log ('        wht_address1  = ' || v_rec_data.wht_address1);
         write_log ('        wht_address2  = ' || v_rec_data.wht_address2);
         write_log (
               '        wht_supplier_tax_id  = '
            || v_rec_data.wht_supplier_tax_id);
         write_log (
               '        wht_supplier_id_card = '
            || v_rec_data.wht_supplier_id_card);
      END IF;

      RETURN v_rec_data;
   EXCEPTION
      WHEN OTHERS
      THEN
         --write_log('Error @tac_ap_interface_util_smt.split_data : ' || sqlcode || ' - ' || sqlerrm);
         v_rec_data.error_msg := SQLCODE || ' - ' || SQLERRM;
         RETURN v_rec_data;
   END split_data;

   PROCEDURE delete_temp_table (p_old_days NUMBER DEFAULT 45)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      DELETE FROM dtac_ap_invoices_temp_int
            WHERE eai_crtd_dttm <= (SYSDATE - p_old_days) AND source = 'SMT';

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END delete_temp_table;

   ---------------------------------------------------------------------------------------

   -- Added by AP@BAS on 07-Feb-2013 : update temp data for source STS only
   ---------------------------------------------------------------------------------------
   PROCEDURE update_temp_data (p_source VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      NULL;
   /* No operate because data can classified from source and bank
      -- Update record where DIST_CODE_CONCATINATED start with '08'
      update dtac_ap_invoices_temp_int a
      set doc_category_code = 'DTACN-IN'
         ,reporting_entity = '10050'
         ,vendor_site_code = 'DTN'
         ,act_vendor_site_code = 'N' || to_char(to_number(substr(act_vendor_site_code, 3, 999)), 'fm900')
      where dist_code_concatinated like '08%'
            and status = 'NEW'
            and source = p_source
            and act_vendor_site_code like 'H%';

      -- Update record where DIST_CODE_CONCATINATED start with '12'
      update dtac_ap_invoices_temp_int a
      set doc_category_code = 'INPB'
         ,reporting_entity = '10094'
         ,act_vendor_site_code = 'P' || to_char(to_number(substr(act_vendor_site_code, 3, 999)), 'fm900')
      where dist_code_concatinated like '12%'
            and source = p_source
            and status = 'NEW'
            and act_vendor_site_code like 'H%';

      -- Update record where DIST_CODE_CONCATINATED start with '09'
      update dtac_ap_invoices_temp_int a
      set doc_category_code = 'DTACN-IN'
         ,reporting_entity = '10051'
         ,act_vendor_site_code = 'W' || to_char(to_number(substr(act_vendor_site_code, 3, 999)), 'fm900')
      where dist_code_concatinated like '09%'
            and source = p_source
            and status = 'NEW'
            and act_vendor_site_code like 'H%';
      commit;
      */
   EXCEPTION
      WHEN OTHERS
      THEN
         write_log ('tac_ap_interface_util_smt.update_temp_data error.');
   END update_temp_data;

   ---------------------------------------------------------------------------------------
   PROCEDURE insert_invoice (ir_invoice IN ap_invoices_interface%ROWTYPE)
   IS
   BEGIN
      INSERT INTO ap_invoices_interface (invoice_id,
                                         invoice_num,
                                         invoice_type_lookup_code,
                                         invoice_date,
                                         po_number,
                                         vendor_id,
                                         vendor_num,
                                         vendor_name,
                                         vendor_site_id,
                                         vendor_site_code,
                                         invoice_amount,
                                         invoice_currency_code,
                                         exchange_rate,
                                         exchange_rate_type,
                                         exchange_date,
                                         terms_id,
                                         terms_name,
                                         description,
                                         awt_group_id,
                                         awt_group_name,
                                         last_update_date,
                                         last_updated_by,
                                         last_update_login,
                                         creation_date,
                                         created_by,
                                         attribute_category,
                                         attribute1,
                                         attribute2,
                                         attribute3,
                                         attribute4,
                                         attribute5,
                                         attribute6,
                                         attribute7,
                                         attribute8,
                                         attribute9,
                                         attribute10,
                                         attribute11,
                                         attribute12,
                                         attribute13,
                                         attribute14,
                                         attribute15,
                                         global_attribute_category,
                                         global_attribute1,
                                         global_attribute2,
                                         global_attribute3,
                                         global_attribute4,
                                         global_attribute5,
                                         global_attribute6,
                                         global_attribute7,
                                         global_attribute8,
                                         global_attribute9,
                                         global_attribute10,
                                         global_attribute11,
                                         global_attribute12,
                                         global_attribute13,
                                         global_attribute14,
                                         global_attribute15,
                                         global_attribute16,
                                         global_attribute17,
                                         global_attribute18,
                                         global_attribute19,
                                         global_attribute20,
                                         status,
                                         source,
                                         GROUP_ID,
                                         request_id,
                                         payment_cross_rate_type,
                                         payment_cross_rate_date,
                                         payment_cross_rate,
                                         payment_currency_code,
                                         workflow_flag,
                                         doc_category_code,
                                         voucher_num,
                                         payment_method_lookup_code,
                                         pay_group_lookup_code,
                                         goods_received_date,
                                         invoice_received_date,
                                         gl_date,
                                         accts_pay_code_combination_id,
                                         ussgl_transaction_code,
                                         exclusive_payment_flag,
                                         org_id,
                                         amount_applicable_to_discount,
                                         prepay_num,
                                         prepay_dist_num,
                                         prepay_apply_amount,
                                         prepay_gl_date,
                                         invoice_includes_prepay_flag,
                                         no_xrate_base_amount,
                                         vendor_email_address,
                                         terms_date,
                                         requester_id,
                                         ship_to_location,
                                         external_doc_ref)
           VALUES (ir_invoice.invoice_id,
                   ir_invoice.invoice_num,
                   ir_invoice.invoice_type_lookup_code,
                   ir_invoice.invoice_date,
                   ir_invoice.po_number,
                   ir_invoice.vendor_id,
                   ir_invoice.vendor_num,
                   ir_invoice.vendor_name,
                   ir_invoice.vendor_site_id,
                   ir_invoice.vendor_site_code,
                   ir_invoice.invoice_amount,
                   ir_invoice.invoice_currency_code,
                   ir_invoice.exchange_rate,
                   ir_invoice.exchange_rate_type,
                   ir_invoice.exchange_date,
                   ir_invoice.terms_id,
                   ir_invoice.terms_name,
                   ir_invoice.description,
                   ir_invoice.awt_group_id,
                   ir_invoice.awt_group_name,
                   ir_invoice.last_update_date,
                   ir_invoice.last_updated_by,
                   ir_invoice.last_update_login,
                   ir_invoice.creation_date,
                   ir_invoice.created_by,
                   ir_invoice.attribute_category,
                   ir_invoice.attribute1,
                   ir_invoice.attribute2,
                   ir_invoice.attribute3,
                   ir_invoice.attribute4,
                   ir_invoice.attribute5,
                   ir_invoice.attribute6,
                   ir_invoice.attribute7,
                   ir_invoice.attribute8,
                   ir_invoice.attribute9,
                   ir_invoice.attribute10,
                   ir_invoice.attribute11,
                   ir_invoice.attribute12,
                   ir_invoice.attribute13,
                   ir_invoice.attribute14,
                   ir_invoice.attribute15,
                   ir_invoice.global_attribute_category,
                   ir_invoice.global_attribute1,
                   ir_invoice.global_attribute2,
                   ir_invoice.global_attribute3,
                   ir_invoice.global_attribute4,
                   ir_invoice.global_attribute5,
                   ir_invoice.global_attribute6,
                   ir_invoice.global_attribute7,
                   ir_invoice.global_attribute8,
                   ir_invoice.global_attribute9,
                   ir_invoice.global_attribute10,
                   ir_invoice.global_attribute11,
                   ir_invoice.global_attribute12,
                   ir_invoice.global_attribute13,
                   ir_invoice.global_attribute14,
                   ir_invoice.global_attribute15,
                   ir_invoice.global_attribute16,
                   ir_invoice.global_attribute17,
                   ir_invoice.global_attribute18,
                   ir_invoice.global_attribute19,
                   ir_invoice.global_attribute20,
                   ir_invoice.status,
                   ir_invoice.source,
                   ir_invoice.GROUP_ID,
                   ir_invoice.request_id,
                   ir_invoice.payment_cross_rate_type,
                   ir_invoice.payment_cross_rate_date,
                   ir_invoice.payment_cross_rate,
                   ir_invoice.payment_currency_code,
                   ir_invoice.workflow_flag,
                   ir_invoice.doc_category_code,
                   ir_invoice.voucher_num,
                   ir_invoice.payment_method_lookup_code,
                   ir_invoice.pay_group_lookup_code,
                   ir_invoice.goods_received_date,
                   ir_invoice.invoice_received_date,
                   ir_invoice.gl_date,
                   ir_invoice.accts_pay_code_combination_id,
                   ir_invoice.ussgl_transaction_code,
                   ir_invoice.exclusive_payment_flag,
                   ir_invoice.org_id,
                   ir_invoice.amount_applicable_to_discount,
                   ir_invoice.prepay_num,
                   ir_invoice.prepay_dist_num,
                   ir_invoice.prepay_apply_amount,
                   ir_invoice.prepay_gl_date,
                   ir_invoice.invoice_includes_prepay_flag,
                   ir_invoice.no_xrate_base_amount,
                   ir_invoice.vendor_email_address,
                   ir_invoice.terms_date,
                   ir_invoice.requester_id,
                   ir_invoice.ship_to_location,
                   ir_invoice.external_doc_ref);
   END insert_invoice;

   ------------------------------------------------------------------------------
   PROCEDURE insert_invoice_line (
      ir_invoice_line IN ap_invoice_lines_interface%ROWTYPE)
   IS
   BEGIN
      INSERT INTO ap_invoice_lines_interface (invoice_id,
                                              invoice_line_id,
                                              line_number,
                                              line_type_lookup_code,
                                              line_group_number,
                                              amount,
                                              accounting_date,
                                              description,
                                              amount_includes_tax_flag,
                                              prorate_across_flag,
                                              tax_code,
                                              tax_code_id,
                                              tax_code_override_flag,
                                              tax_recovery_rate,
                                              tax_recovery_override_flag,
                                              tax_recoverable_flag,
                                              final_match_flag,
                                              po_header_id,
                                              po_line_id,
                                              po_line_location_id,
                                              po_distribution_id,
                                              po_unit_of_measure,
                                              inventory_item_id,
                                              quantity_invoiced,
                                              unit_price,
                                              distribution_set_id,
                                              dist_code_concatenated,
                                              dist_code_combination_id,
                                              awt_group_id,
                                              attribute_category,
                                              attribute1,
                                              attribute2,
                                              attribute3,
                                              attribute4,
                                              attribute5,
                                              attribute6,
                                              attribute7,
                                              attribute8,
                                              attribute9,
                                              attribute10,
                                              attribute11,
                                              attribute12,
                                              attribute13,
                                              attribute14,
                                              attribute15,
                                              global_attribute_category,
                                              global_attribute1,
                                              global_attribute2,
                                              global_attribute3,
                                              global_attribute4,
                                              global_attribute5,
                                              global_attribute6,
                                              global_attribute7,
                                              global_attribute8,
                                              global_attribute9,
                                              global_attribute10,
                                              global_attribute11,
                                              global_attribute12,
                                              global_attribute13,
                                              global_attribute14,
                                              global_attribute15,
                                              global_attribute16,
                                              global_attribute17,
                                              global_attribute18,
                                              global_attribute19,
                                              global_attribute20,
                                              po_release_id,
                                              balancing_segment,
                                              cost_center_segment,
                                              account_segment,
                                              project_id,
                                              task_id,
                                              expenditure_type,
                                              expenditure_item_date,
                                              expenditure_organization_id,
                                              project_accounting_context,
                                              pa_addition_flag,
                                              pa_quantity,
                                              stat_amount,
                                              type_1099,
                                              income_tax_region,
                                              assets_tracking_flag,
                                              price_correction_flag,
                                              ussgl_transaction_code,
                                              receipt_number,
                                              match_option,
                                              rcv_transaction_id,
                                              creation_date,
                                              created_by,
                                              last_update_date,
                                              last_updated_by,
                                              last_update_login,
                                              org_id,
                                              award_id,
                                              price_correct_inv_num)
           VALUES (ir_invoice_line.invoice_id,
                   ir_invoice_line.invoice_line_id,
                   ir_invoice_line.line_number,
                   ir_invoice_line.line_type_lookup_code,
                   ir_invoice_line.line_group_number,
                   ir_invoice_line.amount,
                   ir_invoice_line.accounting_date,
                   ir_invoice_line.description,
                   ir_invoice_line.amount_includes_tax_flag,
                   ir_invoice_line.prorate_across_flag,
                   ir_invoice_line.tax_code,
                   ir_invoice_line.tax_code_id,
                   ir_invoice_line.tax_code_override_flag,
                   ir_invoice_line.tax_recovery_rate,
                   ir_invoice_line.tax_recovery_override_flag,
                   ir_invoice_line.tax_recoverable_flag,
                   ir_invoice_line.final_match_flag,
                   ir_invoice_line.po_header_id,
                   ir_invoice_line.po_line_id,
                   ir_invoice_line.po_line_location_id,
                   ir_invoice_line.po_distribution_id,
                   ir_invoice_line.po_unit_of_measure,
                   ir_invoice_line.inventory_item_id,
                   ir_invoice_line.quantity_invoiced,
                   ir_invoice_line.unit_price,
                   ir_invoice_line.distribution_set_id,
                   ir_invoice_line.dist_code_concatenated,
                   ir_invoice_line.dist_code_combination_id,
                   ir_invoice_line.awt_group_id,
                   ir_invoice_line.attribute_category,
                   ir_invoice_line.attribute1,
                   ir_invoice_line.attribute2,
                   ir_invoice_line.attribute3,
                   ir_invoice_line.attribute4,
                   ir_invoice_line.attribute5,
                   ir_invoice_line.attribute6,
                   ir_invoice_line.attribute7,
                   ir_invoice_line.attribute8,
                   ir_invoice_line.attribute9,
                   ir_invoice_line.attribute10,
                   ir_invoice_line.attribute11,
                   ir_invoice_line.attribute12,
                   ir_invoice_line.attribute13,
                   ir_invoice_line.attribute14,
                   ir_invoice_line.attribute15,
                   ir_invoice_line.global_attribute_category,
                   ir_invoice_line.global_attribute1,
                   ir_invoice_line.global_attribute2,
                   ir_invoice_line.global_attribute3,
                   ir_invoice_line.global_attribute4,
                   ir_invoice_line.global_attribute5,
                   ir_invoice_line.global_attribute6,
                   ir_invoice_line.global_attribute7,
                   ir_invoice_line.global_attribute8,
                   ir_invoice_line.global_attribute9,
                   ir_invoice_line.global_attribute10,
                   ir_invoice_line.global_attribute11,
                   ir_invoice_line.global_attribute12,
                   ir_invoice_line.global_attribute13,
                   ir_invoice_line.global_attribute14,
                   ir_invoice_line.global_attribute15,
                   ir_invoice_line.global_attribute16,
                   ir_invoice_line.global_attribute17,
                   ir_invoice_line.global_attribute18,
                   ir_invoice_line.global_attribute19,
                   ir_invoice_line.global_attribute20,
                   ir_invoice_line.po_release_id,
                   ir_invoice_line.balancing_segment,
                   ir_invoice_line.cost_center_segment,
                   ir_invoice_line.account_segment,
                   ir_invoice_line.project_id,
                   ir_invoice_line.task_id,
                   ir_invoice_line.expenditure_type,
                   ir_invoice_line.expenditure_item_date,
                   ir_invoice_line.expenditure_organization_id,
                   ir_invoice_line.project_accounting_context,
                   ir_invoice_line.pa_addition_flag,
                   ir_invoice_line.pa_quantity,
                   ir_invoice_line.stat_amount,
                   ir_invoice_line.type_1099,
                   ir_invoice_line.income_tax_region,
                   ir_invoice_line.assets_tracking_flag,
                   ir_invoice_line.price_correction_flag,
                   ir_invoice_line.ussgl_transaction_code,
                   ir_invoice_line.receipt_number,
                   ir_invoice_line.match_option,
                   ir_invoice_line.rcv_transaction_id,
                   ir_invoice_line.creation_date,
                   ir_invoice_line.created_by,
                   ir_invoice_line.last_update_date,
                   ir_invoice_line.last_updated_by,
                   ir_invoice_line.last_update_login,
                   ir_invoice_line.org_id,
                   ir_invoice_line.award_id,
                   ir_invoice_line.price_correct_inv_num);
   END insert_invoice_line;

   ------------------------------------------------------------------------------
   --Modified by AP@BAS on 07-Feb-2013 : add parameter i_acc_ou_segment
   -- Add i_vendor_number parameter
   PROCEDURE update_status (i_inv_num           VARCHAR2,
                            i_vendor_number     VARCHAR2,
                            i_acc_ou_segment    VARCHAR2 := '01',
                            i_line_num          NUMBER := NULL,
                            i_status            VARCHAR2,
                            i_group_id          VARCHAR2,
                            p_msg_rec           VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      UPDATE dtac_ap_invoices_temp_int
         SET status = i_status,
             process_message =
                   CASE
                      WHEN process_message IS NOT NULL
                      THEN
                         process_message || ', ' /*Modify by spw@ice 6-Jul-2017 : change from chr(13) to ',' */
                   END
                || p_msg_rec,
             error_flag = DECODE (i_status, 'REJECTED', 'Y', 'N')
       WHERE     1 = 1
             AND invoice_num = i_inv_num
             -- Added by AP@BAS on 07-Feb-2013 : add criteria to filter record for update
             --AND dist_code_concatinated LIKE i_acc_ou_segment || '.%'
             --
             AND line_number = NVL (i_line_num, line_number)
             AND vendor_num = NVL (i_vendor_number, vendor_num)
             -- Added by spw@ice on 19-Dec-2016
             AND GROUP_ID = i_group_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         write_log ('Can not update interface status!');
         ROLLBACK;
   END update_status;

   ------------------------------------------------------------------------------
   --added by AP@BAS 07-Feb-2013 : mapping org_id to acc_ou_code
   ------------------------------------------------------------------------------
   FUNCTION get_mapping_org_id_acccode (p_org_id NUMBER)
      RETURN VARCHAR2
   IS
   BEGIN
      IF (p_org_id = 142)
      THEN
         RETURN '08';
      ELSIF (p_org_id = 218)
      THEN
         RETURN '12';
      ELSIF (p_org_id = 238)
      THEN
         RETURN '12';
      ELSIF (p_org_id = 144)
      THEN
         RETURN '09';
      ELSE
         RETURN '01';
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN '01';
   END get_mapping_org_id_acccode;

   ------------------------------------------------------------------------------
   PROCEDURE interface_ap_group_param (err_msg       OUT VARCHAR2,
                                       err_code      OUT VARCHAR2,
                                       i_org_id   IN     NUMBER,
                                       i_batch    IN     VARCHAR2,
                                       i_group    IN     VARCHAR2)
   IS
      v_mapped_org_id_acc_code   VARCHAR2 (10);

      CURSOR c_inv
      IS
           SELECT t.vendor_num,
                  t.GROUP_ID,
                  t.source,
                  t.invoice_type_lookup_code,
                  t.vendor_site_code,
                  t.invoice_num,
                  t.invoice_date,
                  t.doc_category_code,
                  t.invoice_currency_code,
                  0 invoice_amount,
                  t.gl_date,
                  t.invoice_description,
                  t.terms_name,
                  t.payment_method_lookup_code,
                  t.employee_number -- Added by AP@BAS on 07-Feb-2013 : for separate transaction by OU (01, 08, 12)
                                   ,
                  SUBSTR (t.dist_code_concatinated,
                          1,
                          INSTR (t.dist_code_concatinated, '.', 1) - 1)
                     acc_ou_segment                                         --
                                   ,
                  COUNT ('x') num_of_dist
             FROM dtac_ap_invoices_temp_int t
            WHERE     t.status = 'NEW'
                  AND t.GROUP_ID = i_group
                  AND t.dist_code_concatinated LIKE v_mapped_org_id_acc_code
         GROUP BY t.vendor_num,
                  t.GROUP_ID,
                  t.source,
                  t.invoice_type_lookup_code,
                  t.vendor_site_code,
                  t.invoice_num,
                  t.invoice_date,
                  t.doc_category_code,
                  t.invoice_currency_code,
                  t.gl_date,
                  t.invoice_description,
                  t.terms_name,
                  t.payment_method_lookup_code,
                  t.employee_number          -- Added by AP@BAS on 07-Feb-2013
                                   ,
                  SUBSTR (t.dist_code_concatinated,
                          1,
                          INSTR (t.dist_code_concatinated, '.', 1) - 1);

      -- Modify by AP@BAS on 07-Feb-2013 : add parameter i_acc_ou_segment
      CURSOR c_line (
         i_inv_num          IN VARCHAR2,
         i_vendor_number       VARCHAR2,
         i_acc_ou_segment      VARCHAR2 DEFAULT '01')
      IS
           SELECT l.GROUP_ID,
                  l.source,
                  l.invoice_type_lookup_code,
                  l.vendor_num,
                  l.vendor_site_code,
                  l.invoice_num,
                  l.invoice_date,
                  l.doc_category_code,
                  l.invoice_currency_code,
                  l.invoice_amount,
                  l.gl_date,
                  l.invoice_description,
                  l.terms_name,
                  l.payment_method_lookup_code,
                  l.employee_number,
                  l.line_number,
                  l.line_type_lookup_code,
                  l.line_amount,
                  l.line_tax_code,
                  l.awt_group_name,
                  l.dist_code_concatinated,
                  l.line_description,
                  l.act_vendor_num,
                  l.act_vendor_site_code,
                  l.reporting_entity,
                  l.tax_invoice_date,
                  l.tax_invoice_num,
                  l.tax_acct_period,
                  l.wht_acct_period,
                  l.phor_ngor_dor,
                  l.wht_condition,
                  l.wht_revenue_type,
                  l.wht_revenue_name,
                  l.project_number,
                  l.project_task,
                  l.expenditure_type,
                  l.expenditure_organization,
                  l.status,
                  l.eai_crtd_dttm,
                  l.payee,
                  l.mail_to_name,
                  l.mail_to_address1,
                  l.mail_to_address2,
                  l.mail_to_address3,
                  l.mail_to_address4,
                  l.receipt_number,
                  l.match_option,
                  l.bill_placement_date,
                  l.po_number,
                  l.resend_flag,
                  l.error_msg,
                  l.seqno,
                  ppa.project_id                                 --,pt.task_id
                                ,
                  hou.organization_id
             FROM dtac_ap_invoices_temp_int l,
                  pa_projects_all ppa                           --,PA_TASKS pt
                                     ,
                  hr_organization_units hou
            WHERE     l.status = 'NEW'
                  AND l.dist_code_concatinated LIKE i_acc_ou_segment || '.%'
                  AND l.project_number = ppa.segment1(+)
                  AND l.expenditure_organization = hou.name(+)
                  AND l.invoice_num = i_inv_num
                  AND l.vendor_num = i_vendor_number
         ORDER BY l.line_number;

      r_invoice                  ap_invoices_interface%ROWTYPE;
      r_invoice_line             ap_invoice_lines_interface%ROWTYPE;
      v_group                    VARCHAR2 (80);
      v_source                   VARCHAR2 (80);

      v_rate                     NUMBER;
      v_tax_ccid                 NUMBER;
      v_line_num                 NUMBER;
      v_site_auto_calc_flag      VARCHAR2 (10);
      v_supp_name                VARCHAR2 (100);
      v_period                   VARCHAR2 (30);
      --v_proj_id number(20);
      v_task_id                  NUMBER (20);
      v_expen_type               VARCHAR (100);
      v_expen_org                NUMBER (20);
      v_flag                     BOOLEAN := FALSE;
      v_chk_line                 BOOLEAN := FALSE;
      v_validate_error           BOOLEAN := FALSE;
      ap_req_id                  NUMBER;
      c_ap_req_id                NUMBER;
      v_i_group                  VARCHAR2 (80);
      v_ap_req_id                NUMBER;
      v_line_count               NUMBER := 0;
      v_site                     po_vendor_sites%ROWTYPE;
      v_save_pnt                 VARCHAR2 (100);
      v_tax_rate                 NUMBER;
      v_requester_id             NUMBER;

      v_count                    NUMBER := 0; -- Added by Korn.iCE on 20-OCT-2009.
      v_total_line_tax_amt       NUMBER := 0; -- Added by Korn.iCE on 20-OCT-2009.

      v_line_status_info         VARCHAR2 (32000);
      --v_line_error varchar2(32000);
      v_header_error             VARCHAR2 (32000);

      v_count_invalid_header     NUMBER := 0;
   --v_inv_line_error_list varchar2(10000);
   BEGIN
      --Init
      SAVEPOINT b4_process;
      write_log (' ');
      write_log (
         '+----------------------------------------------------------------------------+');
      write_log ('Start Concurrent AP Interface from FMS ');
      write_log (
         'Start Date: ' || TO_CHAR (SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
      write_log (' ');
      write_log ('Agruments');
      write_log ('-----------');
      write_log ('Org ID: ' || i_org_id);
      write_log ('Batch Name: ' || i_batch);
      write_log ('Group: ' || i_group);
      write_log (
         '+----------------------------------------------------------------------------+');

      /*
         ** added by AP@BAS on 30-Aug-2012
         ** for delete old transaction with 45 days
         */
      delete_temp_table (45);
      /*
         ** end added
         */

      -- Added by AP@BAS on 07-Feb-2013 : Update data depend on combination code (08.xxx, 12.xxx)
      update_temp_data (i_group);

      --

      --- Start added by Korn.iCE on 18-NOV-2009.
      --- Cause: System will create many records in AP_Chrg_Allocations_All and take a long time for run Invoice Validation.
      --- Purpose: Due to system can automatically calcucate tax by allocation then need not to import Line Tax for STS Invoice.
      SELECT COUNT (*), SUM (line_amount)
        INTO v_count, v_total_line_tax_amt
        FROM wmuser.dtac_ap_invoices_temp_int
       WHERE     1 = 1
             AND GROUP_ID = i_group
             AND status = 'NEW'
             AND line_type_lookup_code = 'TAX';

      BEGIN
         /*
            * do not delete line TAX for test project FMS
                  delete from wmuser.dtac_ap_invoices_temp_int
                  where 1=1
                        and group_id = i_group
                        and status = 'NEW'
                        and line_type_lookup_code = 'TAX';

                  commit;
            */
         --write_log('Delete ' || v_count || ' Line(s) TAX for STS Invoice.');
         write_log ('Total Line(s) TAX for FMS Invoice = ' || v_count);
         write_log ('Total Line TAX Amount = ' || v_total_line_tax_amt);
      EXCEPTION
         WHEN OTHERS
         THEN
            write_log (
               '!!! Package tac_ap_interface_util_smt.interface_ap_group_param');
            write_log ('!!! Error MSG: ' || SQLCODE || ' - ' || SQLERRM);
      END;

      write_log (
         '+----------------------------------------------------------------------------+');

      -- Modigy BY AP@BAS on 07-Feb-2013 : add group by substr(t.dist_code_concatinated,1, instr(t.dist_code_concatinated,'.',1)-1)
      /*
         ** Modify by AP@BAS on 27-Aug-2012
         ** for check duplicate invoice information e.g. invoice description, employee number.
         */
      write_log ('');
      v_mapped_org_id_acc_code :=
         get_mapping_org_id_acccode (i_org_id) || '.%';
      write_log ('v_mapped_org_id_acc_code = ' || v_mapped_org_id_acc_code);

      FOR r_inv_h
         IN (  SELECT t.vendor_num,
                      t.vendor_site_code,
                      t.invoice_num,
                      SUBSTR (t.dist_code_concatinated,
                              1,
                              INSTR (t.dist_code_concatinated, '.', 1) - 1)
                         acc_ou_segment,
                      COUNT (DISTINCT t.invoice_date) dup_invoice_date,
                      COUNT (DISTINCT t.doc_category_code)
                         dup_doc_category_code,
                      COUNT (DISTINCT t.invoice_currency_code)
                         dup_invoice_currency_code,
                      COUNT (DISTINCT t.gl_date) dup_gl_date,
                      COUNT (DISTINCT t.invoice_description)
                         dup_invoice_description,
                      COUNT (DISTINCT t.terms_name) dup_terms_name,
                      COUNT (DISTINCT t.payment_method_lookup_code)
                         dup_payment_method_lookup_code,
                      COUNT (DISTINCT t.employee_number) dup_employee_number
                 FROM dtac_ap_invoices_temp_int t
                WHERE t.status = 'NEW' AND t.GROUP_ID = i_group
             GROUP BY t.vendor_num,
                      t.vendor_site_code,
                      t.invoice_num,
                      SUBSTR (t.dist_code_concatinated,
                              1,
                              INSTR (t.dist_code_concatinated, '.', 1) - 1)
               HAVING    COUNT (DISTINCT t.invoice_date) > 1
                      OR COUNT (DISTINCT t.doc_category_code) > 1
                      OR COUNT (DISTINCT t.invoice_currency_code) > 1
                      OR COUNT (DISTINCT t.gl_date) > 1
                      OR COUNT (DISTINCT t.invoice_description) > 1
                      OR COUNT (DISTINCT t.terms_name) > 1
                      OR COUNT (DISTINCT t.payment_method_lookup_code) > 1
                      OR COUNT (DISTINCT t.employee_number) > 1)
      LOOP
         v_count_invalid_header := v_count_invalid_header + 1;
         v_header_error :=
            'Invoice number (' || r_inv_h.invoice_num || ') error with ';

         IF r_inv_h.dup_invoice_date > 1
         THEN
            v_header_error :=
               v_header_error || '|' || 'multiple invoice date,';
         END IF;

         IF r_inv_h.dup_doc_category_code > 1
         THEN
            v_header_error :=
               v_header_error || '|' || 'multiple document category,';
         END IF;

         IF r_inv_h.dup_invoice_currency_code > 1
         THEN
            v_header_error :=
               v_header_error || '|' || 'multiple currency code,';
         END IF;

         IF r_inv_h.dup_gl_date > 1
         THEN
            v_header_error := v_header_error || '|' || 'multiple GL date,';
         END IF;

         IF r_inv_h.dup_invoice_description > 1
         THEN
            v_header_error :=
               v_header_error || '|' || 'multiple invoice description,';
         END IF;

         IF r_inv_h.dup_terms_name > 1
         THEN
            v_header_error := v_header_error || '|' || 'multiple term,';
         END IF;

         IF r_inv_h.dup_payment_method_lookup_code > 1
         THEN
            v_header_error :=
               v_header_error || '|' || 'multiple payment method,';
         END IF;

         IF r_inv_h.dup_employee_number > 1
         THEN
            v_header_error := v_header_error || '|' || 'multiple requester,';
         END IF;

         write_log (v_header_error);
      END LOOP;

      IF v_count_invalid_header > 0
      THEN
         write_log ('');
         err_code := 2;
         err_msg := 'Invoice information invalid. See log for detail.';
         RETURN;
      END IF;

      --v_header_error := '';
      /*
         ** End modify by AP@BAS on 27-Aug-2012
         */

      FOR r_inv IN c_inv
      LOOP
         v_save_pnt := r_inv.invoice_num || r_inv.vendor_num;
         SAVEPOINT v_save_pnt;

         r_invoice := NULL;
         v_flag := FALSE;

         write_log ('Invoice Number : ' || r_inv.invoice_num);
         v_group := r_inv.GROUP_ID;
         v_source := r_inv.source;

         /** AP_INVOICES_INTERFACE **/
         --SEQ
         SELECT ap_invoices_interface_s.NEXTVAL
           INTO r_invoice.invoice_id
           FROM DUAL;

         --Supplier
         BEGIN
            SELECT v.vendor_id
              INTO r_invoice.vendor_id
              FROM po_vendors v
             WHERE v.segment1 = r_inv.vendor_num;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_flag := TRUE;
               write_log ('*Invalid Supplier : ' || r_inv.vendor_num);
               err_msg := '*** DATA ERROR : Interface data is invalid!';
               err_code := '1';
         END;

         --Site---
         BEGIN
            SELECT vs.*
              INTO v_site
              FROM po_vendor_sites vs
             WHERE     vs.vendor_site_code = r_inv.vendor_site_code
                   AND vs.vendor_id = r_invoice.vendor_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_flag := TRUE;

               write_log (
                  '*Invalid Supplier Site : ' || r_inv.vendor_site_code);
               write_log ('*Supplier Number : ' || r_inv.vendor_num);
               err_msg := '*** DATA ERROR : Interface data is invalid!';
               err_code := '1';
               v_site := NULL;
         END;

         r_invoice.vendor_site_id := v_site.vendor_site_id;
         r_invoice.accts_pay_code_combination_id :=
            v_site.accts_pay_code_combination_id;
         v_site_auto_calc_flag := v_site.auto_tax_calc_flag;

         --Term
         IF r_inv.terms_name IS NOT NULL
         THEN
            BEGIN
               SELECT t.name, r_inv.invoice_date
                 --,t.creation_date -- change to invoice_date
                 INTO r_invoice.terms_name, r_invoice.terms_date
                 FROM ap_terms t
                WHERE t.term_id = r_inv.terms_name;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_flag := TRUE;
                  write_log ('*Invalid Term Name : ' || r_inv.terms_name);
                  err_msg := '*** DATA ERROR : Interface data is invalid!';
                  err_code := '1';
            END;
         END IF;

         v_requester_id := 0;

         BEGIN
            SELECT person_id
              INTO v_requester_id
              FROM per_people_f
             WHERE     employee_number = TO_CHAR (r_inv.employee_number)
                   AND (    (TRUNC (effective_start_date) <= TRUNC (SYSDATE))
                        AND (TRUNC (effective_end_date) >= TRUNC (SYSDATE)));
         EXCEPTION
            WHEN OTHERS
            THEN
               v_flag := TRUE;
               write_log (
                  '*Invalid Employee Number : ' || r_inv.employee_number);
               err_msg := '*** DATA ERROR : Interface data is invalid!';
               err_code := '1';
               v_requester_id := 0;
         END;

         IF r_inv.gl_date IS NULL
         THEN
            r_invoice.gl_date := TRUNC (SYSDATE);
         ELSE
            r_invoice.gl_date := r_inv.gl_date;
         END IF;

         -- Validate Open GL Periods

         -- modified by AP@BAS on 07-Feb-2013 : org_id depend on dist_code_concatinated
         IF (r_inv.acc_ou_segment = '08')
         THEN
            --DTN
            r_invoice.org_id := g_dtn_org_id;
         ELSIF (r_inv.acc_ou_segment = '12')
         THEN
            --PSB
            r_invoice.org_id := g_psb_org_id;
         ELSE
            r_invoice.org_id := i_org_id;
         END IF;

         ---

         r_invoice.invoice_num := r_inv.invoice_num;
         r_invoice.invoice_type_lookup_code := r_inv.invoice_type_lookup_code; --'STANDARD';
         r_invoice.invoice_date := r_inv.invoice_date;
         r_invoice.vendor_num := r_inv.vendor_num;
         r_invoice.vendor_site_code := r_inv.vendor_site_code;
         r_invoice.invoice_amount := r_inv.invoice_amount;
         r_invoice.invoice_currency_code := r_inv.invoice_currency_code;
         r_invoice.source := r_inv.source;
         r_invoice.GROUP_ID := r_inv.GROUP_ID;
         r_invoice.doc_category_code := r_inv.doc_category_code;
         r_invoice.description := r_inv.invoice_description;
         r_invoice.terms_id := r_inv.terms_name;
         r_invoice.payment_method_lookup_code :=
            r_inv.payment_method_lookup_code;
         r_invoice.creation_date := SYSDATE;
         r_invoice.last_update_date := SYSDATE;
         r_invoice.created_by := fnd_global.user_id;
         r_invoice.last_updated_by := fnd_global.user_id;
         r_invoice.request_id := fnd_global.conc_request_id;
         r_invoice.attribute_category := 'JA.TH.APXINWKB.DISTRIBUTIONS';
         r_invoice.global_attribute_category := 'JA.TH.APXINWKB.DISTRIBUTIONS';

         r_invoice.requester_id := v_requester_id;

         IF v_flag
         THEN
            update_status (i_inv_num          => r_inv.invoice_num,
                           i_vendor_number    => r_inv.vendor_num,
                           i_acc_ou_segment   => r_inv.acc_ou_segment,
                           i_status           => 'REJECTED',
                           i_group_id         => r_inv.GROUP_ID,
                           p_msg_rec          => '');
            v_validate_error := TRUE;
         ELSE
            v_line_num := 0;
            r_invoice.invoice_amount := 0;
            v_chk_line := FALSE;

            -- Modify by AP@BAS on 07-Feb-2013 : add parameter nvl(r_inv.acc_ou_segment,'01')
            write_log ('r_inv.acc_ou_segment = ' || r_inv.acc_ou_segment);

            FOR r_line
               IN c_line (r_invoice.invoice_num,
                          r_invoice.vendor_num,
                          NVL (r_inv.acc_ou_segment, '01'))
            LOOP
               v_line_num := v_line_num + 1;
               r_invoice_line := NULL;
               v_chk_line := FALSE;

               v_line_status_info :=
                     'Row#'
                  || v_line_num
                  || ' |Invoice line#'
                  || r_line.line_number;

               /** AP_INVOICE_LINES_INTERFACE **/
               SELECT ap_invoice_lines_interface_s.NEXTVAL
                 INTO r_invoice_line.invoice_line_id
                 FROM DUAL;

               -- Validate data for Withholding Tax
               IF r_line.awt_group_name IS NOT NULL
               THEN
                  BEGIN
                     SELECT ag.GROUP_ID
                       INTO r_invoice_line.awt_group_id
                       FROM ap_awt_groups ag
                      WHERE ag.name = r_line.awt_group_name;
                  EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        v_chk_line := TRUE;
                        v_line_status_info :=
                              v_line_status_info
                           || '|Invalid Withholding Tax Group ('
                           || r_line.awt_group_name
                           || ')';
                        --write_log('*Invalid Withholding Tax Group : ' || r_line.awt_group_name);
                        err_msg :=
                           '*** DATA ERROR : Interface data is invalid!';
                        err_code := '1';
                  END;

                  IF r_line.wht_condition IS NULL
                  THEN
                     v_line_status_info :=
                        v_line_status_info || '|WHT Condition is NULL';
                     --write_log('*Incomplete Withholding Tax Information - WHT Condition is NULL');
                     v_chk_line := TRUE;
                     err_msg := '*** DATA ERROR : Interface data is invalid!';
                     err_code := '1';
                  END IF;

                  IF r_line.wht_revenue_type IS NULL
                  THEN
                     v_line_status_info :=
                        v_line_status_info || '|WHT Revenue Type is NULL';
                     --write_log('*Incomplete Withholding Tax Information - WHT Revenue Type is NULL');
                     v_chk_line := TRUE;
                     err_msg := '*** DATA ERROR : Interface data is invalid!';
                     err_code := '1';
                  END IF;

                  IF r_line.wht_revenue_name IS NULL
                  THEN
                     v_line_status_info :=
                        v_line_status_info || '|WHT Revenue Name NULL';
                     --write_log('*Incomplete Withholding Tax Information - WHT Revenue Name NULL');
                     v_chk_line := TRUE;
                     err_msg := '*** DATA ERROR : Interface data is invalid!';
                     err_code := '1';
                  END IF;

                  IF r_line.phor_ngor_dor IS NULL
                  THEN
                     v_line_status_info :=
                        v_line_status_info || '|Phor Ngor Dor is NULL';
                     --write_log('*Incomplete Withholding Tax Information - Phor Ngor Dor is NULL');
                     v_chk_line := TRUE;
                     err_msg := '*** DATA ERROR : Interface data is invalid!';
                     err_code := '1';
                  END IF;
               END IF;

               r_invoice_line.amount := r_line.line_amount;

               IF r_line.line_tax_code IS NOT NULL
               THEN
                  BEGIN
                     SELECT atc.tax_code_combination_id,
                            atc.tax_id,
                            atc.tax_rate
                       INTO v_tax_ccid, r_invoice_line.tax_code_id, v_rate
                       FROM ap_tax_codes_all atc -- change from ap_tax_codes to ap_tax_codes_all
                      WHERE     atc.enabled_flag = 'Y'
                            AND atc.name = r_line.line_tax_code
                            AND NVL (atc.start_date, SYSDATE) <= SYSDATE
                            AND NVL (atc.inactive_date, SYSDATE) >= SYSDATE
                            -- added by AP@BAS on 07-Feb-2013 : add criteria org_id
                            AND atc.org_id = r_invoice.org_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        v_chk_line := TRUE;
                        v_line_status_info :=
                              v_line_status_info
                           || '|Invalid Tax Code ('
                           || r_line.line_tax_code
                           || ')';
                        --write_log('*Invalid Tax Code : ' || r_line.line_tax_code);
                        err_msg :=
                           '*** DATA ERROR : Interface data is invalid!';
                        err_code := '1';
                  END;

                  r_invoice_line.tax_code := r_line.line_tax_code;
                  r_invoice_line.tax_code_override_flag := 'N';
                  r_invoice_line.tax_recoverable_flag := 'Y';
               ELSE
                  r_invoice_line.tax_code := NULL;
                  r_invoice_line.tax_recovery_rate := NULL;
               END IF;

               v_supp_name := NULL;

               IF     r_line.project_id IS NULL
                  AND r_line.project_task IS NULL
                  AND r_line.expenditure_type IS NULL
                  AND r_line.organization_id IS NULL
               THEN
                  v_validate_error := FALSE;
               ELSE
                  -- Project Number begin add
                  IF r_line.project_id IS NULL
                  THEN
                     v_validate_error := TRUE;
                     v_flag := TRUE;
                     v_line_status_info :=
                           v_line_status_info
                        || '|Invalid Project ('
                        || r_line.project_number
                        || ')';
                     write_log (
                        '*Invalid Project : ' || r_line.project_number);
                     err_msg := '*** DATA ERROR : Interface data is invalid!';
                     err_code := '1';
                  -- END;
                  END IF;                                        ------end add

                  -- Task Number begin add
                  IF r_line.project_id IS NOT NULL
                  THEN
                     BEGIN
                        SELECT pt.task_id
                          INTO v_task_id
                          FROM (SELECT *
                                  FROM pa_tasks pt
                                 WHERE pt.project_id(+) = r_line.project_id) pt --,PA_PROJECTS_ALL ppa
                         WHERE pt.task_number = r_line.project_task;
                     -- and pt.project_id = r_line.project_id;

                     EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                           v_validate_error := TRUE;
                           v_flag := TRUE;
                           v_line_status_info :=
                                 v_line_status_info
                              || '|Invalid Task ('
                              || r_line.project_task
                              || ')';
                           write_log (
                              '*Invalid Task : ' || r_line.project_task);
                           err_msg :=
                              '*** DATA ERROR : Interface data is invalid!';
                           err_code := '1';
                     END;
                  ELSE
                     v_validate_error := TRUE;
                     v_line_status_info :=
                           v_line_status_info
                        || '|Invalid Task ('
                        || r_line.project_task
                        || ')';
                     write_log ('*Invalid Task : ' || r_line.project_task);
                     err_msg := '*** DATA ERROR : Interface data is invalid!';
                     err_code := '1';
                  END IF;                                           ---end add

                  --  Expenditure Type begin add
                  BEGIN
                     SELECT pet.expenditure_type
                       INTO v_expen_type
                       FROM pa_expenditure_types pet
                      WHERE pet.expenditure_type = r_line.expenditure_type;
                  EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        v_validate_error := TRUE;
                        v_flag := TRUE;
                        v_line_status_info :=
                              v_line_status_info
                           || '|Invalid Expenditure Type ('
                           || r_line.expenditure_type
                           || ')';
                        --write_log('*Invalid Expenditure Type : ' || r_line.expenditure_type);
                        err_msg :=
                           '*** DATA ERROR : Interface data is invalid!';
                        err_code := '1';
                  END;

                  --END IF; ---end add

                  --  Expenditure Org begin add
                  IF r_line.organization_id IS NULL
                  THEN
                     BEGIN
                        SELECT hou.organization_id
                          INTO v_expen_org
                          FROM hr_organization_units hou,
                               dtac_ap_invoices_temp_int l
                         WHERE     hou.organization_id =
                                      r_line.organization_id
                               AND hou.name(+) = l.expenditure_organization;
                     EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                           v_validate_error := TRUE;
                           v_flag := TRUE;
                           v_line_status_info :=
                                 v_line_status_info
                              || '|Invalid Expenditure Organization ('
                              || r_line.expenditure_organization
                              || ')';
                           --write_log('*Invalid Expenditure Organization : ' || r_line.expenditure_organization);
                           err_msg :=
                              '*** DATA ERROR : Interface data is invalid!';
                           err_code := '1';
                     END;
                  END IF;                                           ---end add
               END IF;

               -----
               r_invoice_line.invoice_id := r_invoice.invoice_id;
               -- modify by AP@BAS on 07-Feb-2013 : new assign value from i_org_id to r_invoice.org_id
               --r_invoice_line.org_id := i_org_id;
               r_invoice_line.org_id := r_invoice.org_id;

               r_invoice_line.line_number := v_line_num;
               r_invoice_line.accounting_date := r_line.gl_date;
               r_invoice_line.project_id := r_line.project_id;  ---iCE-Tae add
               r_invoice_line.task_id := v_task_id; --r_line.task_id; ---iCE-Tae add
               r_invoice_line.expenditure_type := r_line.expenditure_type; ---iCE-Tae add
               r_invoice_line.expenditure_organization_id :=
                  r_line.organization_id;                       ---iCE-Tae add
               r_invoice_line.expenditure_item_date := SYSDATE; ---iCE-Tae add
               r_invoice_line.line_type_lookup_code :=
                  r_line.line_type_lookup_code;                      --'ITEM';
               r_invoice_line.description :=
                  v_supp_name || ',' || r_line.line_description;
               r_invoice_line.dist_code_concatenated :=
                  r_line.dist_code_concatinated;
               r_invoice_line.awt_group_name := r_line.awt_group_name;
               r_invoice_line.attribute4 := r_line.act_vendor_num;
               r_invoice_line.attribute5 := r_line.act_vendor_site_code;
               r_invoice_line.global_attribute_category :=
                  'JA.TH.APXINWKB.DISTRIBUTIONS';

               IF r_invoice_line.global_attribute16 IS NULL
               THEN
                  r_invoice_line.global_attribute16 := r_inv.source;
               END IF;

               IF r_line.tax_acct_period IS NULL
               THEN
                  r_invoice_line.attribute13 := v_period;
               ELSE
                  r_invoice_line.attribute13 := r_line.tax_acct_period;
               END IF;

               r_invoice_line.attribute15 := r_line.reporting_entity;
               r_invoice_line.global_attribute9 := v_supp_name;

               r_invoice_line.attribute_category :=
                  'JA.TH.APXINWKB.DISTRIBUTIONS';

               r_invoice_line.global_attribute20 := r_line.reporting_entity;

               /* Remark by AP@BAS on 14-Oct-2013
                     * FMS will send tax line to erp
                     if r_line.awt_group_name is not null then
                         r_invoice_line.global_attribute12 := r_line.wht_revenue_name;
                         r_invoice_line.global_attribute13 := r_line.wht_revenue_type;
                         r_invoice_line.global_attribute14 := r_line.wht_condition;
                         r_invoice_line.global_attribute15 := r_line.phor_ngor_dor;
                         r_invoice_line.global_attribute18 := r_line.wht_acct_period;
                     end if;
                     */
               --Added by AP@BAS on 14-Oct-2013
               r_invoice_line.global_attribute12 := r_line.wht_revenue_name;
               r_invoice_line.global_attribute13 := r_line.wht_revenue_type;
               r_invoice_line.global_attribute14 := r_line.wht_condition;
               r_invoice_line.global_attribute15 := r_line.phor_ngor_dor;
               r_invoice_line.global_attribute18 := r_line.wht_acct_period;

               r_invoice_line.created_by := fnd_global.user_id;
               r_invoice_line.creation_date := SYSDATE;

               IF v_chk_line
               THEN
                  --ROLLBACK TO SAVEPOINT rejected;

                  --write_log ('update dtac_ap_invoices_temp_int set status = ''PROCESSED'' where invoice_num = ''' || r_invoice.invoice_num || ''' and line_number='|| r_invoice_line.line_number ||';');
                  v_line_status_info := v_line_status_info || '| Rejected';
                  /*
                           ** remark and modify #2 by AP@BAS on 24-Aug-2012
                           ** because used r_invoice_line.line_number that running record count
                           ** fixed change r_invoice_line.line_number to r_line.line_number
                           --update_status(i_inv_num => r_invoice.invoice_num, i_line_num => r_invoice_line.line_number, i_status => 'REJECTED');
                           */
                  update_status (i_inv_num          => r_invoice.invoice_num,
                                 i_vendor_number    => r_inv.vendor_num,
                                 i_acc_ou_segment   => r_inv.acc_ou_segment,
                                 i_line_num         => r_line.line_number,
                                 i_status           => 'REJECTED',
                                 i_group_id         => r_inv.GROUP_ID,
                                 p_msg_rec          => '');

                  v_validate_error := TRUE;
               ELSE
                  IF     r_line.line_type_lookup_code = 'ITEM'
                     AND r_line.line_tax_code IS NOT NULL
                  THEN
                     BEGIN
                        SELECT tax_rate
                          INTO v_tax_rate
                          FROM ap_tax_codes_all
                         WHERE     name = r_line.line_tax_code
                               -- modify by AP@BAS on 07-Feb-2013 : change criteria from i_org_id to r_invoice.org_id
                               --and org_id = i_org_id
                               AND org_id = r_invoice.org_id
                               AND (   (enabled_flag = 'Y')
                                    OR (enabled_flag IS NULL))
                               AND (    (TRUNC (start_date) <=
                                            TRUNC (SYSDATE))
                                    AND (TRUNC (NVL (inactive_date, SYSDATE)) >=
                                            TRUNC (SYSDATE)));
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           v_tax_rate := 0;
                     END;

                     r_invoice_line.attribute9 := r_invoice_line.amount;
                     r_invoice_line.attribute8 :=
                        ROUND (r_invoice_line.amount * (v_tax_rate / 100), 2);
                     r_invoice_line.attribute10 := v_supp_name;
                  END IF;

                  /*
                           ** Modify by AP@BAS on 18-Sep-2012
                           ** not insert insert_invoice_line if have error accured atlease one error
                           --write_out('Invoice Number' || ' ' || (r_invoice.invoice_num) || ' ' || 'Line' || ' ' || (r_invoice_line.line_number) || ' ' ||'Source:' || ' ' || (r_invoice_line.global_attribute16));
                             insert_invoice_line(ir_invoice_line => r_invoice_line); -- ITEM
                           */
                  IF v_flag = FALSE
                  THEN
                     insert_invoice_line (ir_invoice_line => r_invoice_line); -- ITEM
                  END IF;

                  -- End modify
                  --
                  r_invoice.invoice_amount :=
                       NVL (r_invoice.invoice_amount, 0)
                     + r_invoice_line.amount;            -- For Invoice Header

                  v_line_status_info := v_line_status_info || '| Processed';

                  --write_log ('update dtac_ap_invoices_temp_int set status = ''PROCESSED'' where invoice_num = ''' || r_invoice.invoice_num || ''' and line_number='|| r_invoice_line.line_number ||';');
                  /*
                           ** remark and modify #2 by AP@BAS on 24-Aug-2012
                           ** because used r_invoice_line.line_number that running record count
                           ** fixed change r_invoice_line.line_number to r_line.line_number
                           --update_status(i_inv_num => r_invoice.invoice_num, i_line_num => r_invoice_line.line_number, i_status => 'PROCESSED');
                           */
                  update_status (i_inv_num          => r_invoice.invoice_num,
                                 i_vendor_number    => r_inv.vendor_num,
                                 i_acc_ou_segment   => r_inv.acc_ou_segment,
                                 i_line_num         => r_line.line_number,
                                 i_status           => 'PROCESSED',
                                 i_group_id         => r_inv.GROUP_ID,
                                 p_msg_rec          => '');

                  v_line_count := v_line_count + 1;
               END IF;

               IF v_chk_line
               THEN
                  v_flag := TRUE;
               END IF;

               write_log (v_line_status_info);
            END LOOP;                                                   --Line
         END IF;

         IF v_chk_line = FALSE
         THEN
            insert_invoice (ir_invoice => r_invoice);
            write_log ('Invoice ID: ' || r_invoice.invoice_id);
         END IF;
      /* fixed by spw@ice 16Feb2017 for Error MSG: -1086 - ORA-01086: savepoint 'V_SAVE_PNT' never established in this session or is invalid.
       IF v_flag = TRUE
       THEN
          write_log (
             'Rollback transaction of invoice number ' || r_inv.invoice_num);
          ROLLBACK TO SAVEPOINT v_save_pnt;
       END IF; */
      END LOOP;                                                      -- Header

      IF (v_line_count <= 0)
      THEN
         write_log ('No invoice line');
      END IF;

      IF NOT v_validate_error AND v_line_count > 0
      THEN
         COMMIT;                                                       -- SAVE

         BEGIN
            ap_req_id :=
               fnd_request.submit_request (application   => 'SQLAP',
                                           program       => 'APXIIMPT',
                                           argument1     => v_source,
                                           argument2     => i_group,
                                           argument3     => i_batch --to_char(v_source_header_id),
                                                                   ,
                                           argument4     => NULL,
                                           argument5     => NULL,
                                           argument6     => NULL,
                                           argument7     => 'N');
         END;

         v_i_group := i_group;
         v_ap_req_id := ap_req_id;

         COMMIT;

         --Add by Naj 19-jan-2009
         IF ap_req_id > 0
         THEN
            write_log ('Payable Interface Start Request ID: ' || ap_req_id);
            conc_wait (ap_req_id);

            c_ap_req_id :=
               fnd_request.submit_request (
                  application   => 'SQLAP',
                  program       => 'TACAP016_V3_EXTD',
                  argument1     => v_ap_req_id,
                  argument2     => v_i_group);
         ELSE
            write_log (
                  'Not implement Yet, Can not Submit Payable Open Interface '
               || TO_CHAR (SYSDATE, 'DD/MM/YYYY HH24:MI:SS')
               || ' ------');
         END IF;
      --Add by Naj 19-jan-2009
      END IF;                                                     -- Ctrl flag

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         write_log (
            'tac_ap_interface_util_smt.interface_ap_group_param exception .');
         write_log ('Error MSG: ' || SQLCODE || ' - ' || SQLERRM);
   END interface_ap_group_param;

   ------------------------------------------------------------------------------

   ---------------------------------------------------------------------------------------

   ----------------------------------------------------------------------------------------------

   PROCEDURE interface_ap (i_org_id       IN NUMBER,
                           p_request_id   IN NUMBER,
                           p_source       IN VARCHAR2)
   IS
      v_debug_step               VARCHAR2 (100);
      v_mapped_org_id_acc_code   VARCHAR2 (10);

      err_msg                    VARCHAR2 (1000);
      err_code                   VARCHAR2 (100);

      l_resp_id                  NUMBER;

      CURSOR c_grp
      IS
           SELECT t.GROUP_ID GROUP_ID
             FROM dtac_ap_invoices_temp_int t
            WHERE     t.status = 'NEW'              --andt.status = 'REJECTED'
                  AND t.source = p_source --and t.group_id = 'NANTHAWANS-DTN-CELLSITE-01052015.2_1'
                  AND t.dist_code_concatinated LIKE v_mapped_org_id_acc_code
                  AND t.org_id = i_org_id
         GROUP BY t.GROUP_ID
         ORDER BY t.GROUP_ID;

      CURSOR c_inv (
         i_group VARCHAR2)
      IS
           SELECT t.vendor_num,
                  t.GROUP_ID,
                  t.source,
                  t.invoice_type_lookup_code,
                  t.vendor_site_code,
                  t.invoice_num,
                  t.invoice_date,
                  t.doc_category_code,
                  t.invoice_currency_code,
                  t.invoice_amount invoice_amount,
                  t.gl_date,
                  t.invoice_description,
                  t.terms_name,
                  t.payment_method_lookup_code,
                  t.employee_number,
                  SUBSTR (t.dist_code_concatinated,
                          1,
                          INSTR (t.dist_code_concatinated, '.', 1) - 1)
                     acc_ou_segment,
                  t.ap_liability_account,
                  pay_group,
                  COUNT ('x') num_of_dist,
                  batch_id,
                  source_file_name,
                  t.dist_code_concatinated
             FROM dtac_ap_invoices_temp_int t
            WHERE t.status = 'NEW' AND t.GROUP_ID = i_group --AND t.dist_code_concatinated LIKE v_mapped_org_id_acc_code  /*Fixed by spw@ice 11-Jul-2017*/
                  AND t.org_id = i_org_id
         GROUP BY t.vendor_num,
                  t.GROUP_ID,
                  t.source,
                  t.invoice_type_lookup_code,
                  t.vendor_site_code,
                  t.invoice_num,
                  t.invoice_date,
                  t.doc_category_code,
                  t.invoice_currency_code,
                  t.invoice_amount,
                  t.gl_date,
                  t.invoice_description,
                  t.terms_name,
                  t.payment_method_lookup_code,
                  t.employee_number,
                  SUBSTR (t.dist_code_concatinated,
                          1,
                          INSTR (t.dist_code_concatinated, '.', 1) - 1),
                  t.ap_liability_account,
                  pay_group,
                  batch_id,
                  t.source_file_name,
                  t.dist_code_concatinated;

      CURSOR c_line (
         i_group               VARCHAR2,
         i_inv_num          IN VARCHAR2,
         i_vendor_number       VARCHAR2,
         i_acc_ou_segment      VARCHAR2 DEFAULT '01')
      IS
           SELECT l.GROUP_ID,
                  l.source,
                  l.invoice_type_lookup_code,
                  l.vendor_num,
                  l.vendor_site_code,
                  l.invoice_num,
                  l.invoice_date,
                  l.doc_category_code,
                  l.invoice_currency_code,
                  l.invoice_amount,
                  l.gl_date,
                  l.invoice_description,
                  l.terms_name,
                  l.payment_method_lookup_code,
                  l.employee_number,
                  l.line_number,
                  l.line_type_lookup_code,
                  l.line_amount,
                  l.line_tax_code,
                  l.awt_group_name,
                  l.dist_code_concatinated,
                  l.line_description,
                  l.act_vendor_num,
                  l.act_vendor_site_code,
                  l.reporting_entity,
                  l.tax_invoice_date,
                  l.tax_invoice_num,
                  l.tax_acct_period,
                  l.wht_acct_period,
                  l.phor_ngor_dor,
                  l.wht_condition,
                  l.wht_revenue_type,
                  l.wht_revenue_name,
                  l.project_number,
                  l.project_task,
                  l.expenditure_type,
                  l.expenditure_organization,
                  l.status,
                  l.eai_crtd_dttm,
                  l.payee,
                  l.mail_to_name,
                  l.mail_to_address1,
                  l.mail_to_address2,
                  l.mail_to_address3,
                  l.mail_to_address4,
                  l.receipt_number,
                  l.match_option,
                  l.bill_placement_date,
                  l.po_number,
                  l.resend_flag,
                  l.error_msg,
                  l.seqno,
                  ppa.project_id,
                  hou.organization_id,
                  l.wht_act_supplier_name,
                  l.wht_address1,
                  l.wht_address2,
                  l.wht_supplier_tax_id,
                  l.wht_supplier_id_card,
                  l.source_file_name
             FROM dtac_ap_invoices_temp_int l,
                  pa_projects_all ppa,
                  hr_organization_units hou
            WHERE     l.status = 'NEW'
                  AND l.GROUP_ID = i_group
                  --AND l.dist_code_concatinated LIKE i_acc_ou_segment || '.%'   /*Fixed by spw@ice 11-Jul-2017*/
                  AND l.project_number = ppa.segment1(+)
                  AND l.expenditure_organization = hou.name(+)
                  AND l.vendor_num = i_vendor_number
                  AND l.invoice_num = i_inv_num
                  AND l.org_id = i_org_id
         ORDER BY l.line_number;

      r_invoice                  ap_invoices_interface%ROWTYPE;
      r_invoice_line             ap_invoice_lines_interface%ROWTYPE;
      v_group                    VARCHAR2 (80);
      v_source                   VARCHAR2 (80);
      v_batch                    VARCHAR2 (80);

      v_rate                     NUMBER;
      v_tax_ccid                 NUMBER;
      v_line_num                 NUMBER;
      v_site_auto_calc_flag      VARCHAR2 (10);
      v_supp_name                VARCHAR2 (100);
      v_period                   VARCHAR2 (30);
      v_task_id                  NUMBER (20);
      v_expen_type               VARCHAR (100);
      v_expen_org                NUMBER (20);
      v_error_flag               BOOLEAN := FALSE;
      v_chk_line                 BOOLEAN := FALSE;
      v_validate_error           BOOLEAN := FALSE;
      ap_req_id                  NUMBER;
      c_ap_req_id                NUMBER;
      v_i_group                  VARCHAR2 (80);
      v_ap_req_id                NUMBER;
      v_line_count               NUMBER := 0;
      v_site                     po_vendor_sites_all%ROWTYPE;
      v_save_pnt                 VARCHAR2 (100);
      v_tax_rate                 NUMBER;
      v_requester_id             NUMBER;
      v_liability_account_id     NUMBER;
      v_dist_code_concatinated   NUMBER;
      v_gl_set_of_books_id       NUMBER
         := TO_NUMBER (fnd_profile.VALUE ('GL_SET_OF_BKS_ID'));
      v_chart_of_accounts_id     NUMBER;

      --v_count number := 0;
      --v_total_line_tax_amt number := 0;

      v_line_status_info         VARCHAR2 (32000);
      v_header_error             VARCHAR2 (32000);

      v_count_invalid_header     NUMBER := 0;

      /*Modify On 10/31/2016 by spw@ice to following.
      - Mapping between Requisition and Invoice.
      - Keep Batch ID in the AP Invoice.
      */
      v_req_rec                  requisition_rec;
      v_mapping                  TAC_REQ_MAPPING_INVOICE%ROWTYPE;

      msg_rec_h                  VARCHAR2 (3000);
      msg_rec                    VARCHAR2 (3000);
   /*----------------End modify--------------------*/
   BEGIN
      --Init
      SAVEPOINT b4_process;
      write_log (' ');
      write_log (
         '+----------------------------------------------------------------------------+');
      write_log ('Start Concurrent AP Interface from SMT ');
      write_log (
         'Start Date: ' || TO_CHAR (SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
      write_log (' ');
      write_log ('Agruments');
      write_log ('-----------');
      write_log ('Org ID: ' || i_org_id);
      write_log ('Source: ' || p_source);
      write_log (
         '+----------------------------------------------------------------------------+');

      --Delete history data in temp table 45 days passed
      --delete_temp_table (45);

      update_temp_data (p_source);

      write_log ('');
      v_mapped_org_id_acc_code :=
         get_mapping_org_id_acccode (i_org_id) || '.%';
      write_log ('v_mapped_org_id_acc_code = ' || v_mapped_org_id_acc_code);

      FOR r_grp IN c_grp
      LOOP
         write_log ('test loop');
         msg_rec_h := '';

         FOR r_inv_h
            IN (  SELECT t.vendor_num,
                         t.vendor_site_code,
                         t.invoice_num,
                         SUBSTR (t.dist_code_concatinated,
                                 1,
                                 INSTR (t.dist_code_concatinated, '.', 1) - 1)
                            acc_ou_segment,
                         COUNT (DISTINCT t.invoice_date) dup_invoice_date,
                         COUNT (DISTINCT t.doc_category_code)
                            dup_doc_category_code,
                         COUNT (DISTINCT t.invoice_currency_code)
                            dup_invoice_currency_code,
                         COUNT (DISTINCT t.gl_date) dup_gl_date,
                         COUNT (DISTINCT t.invoice_description)
                            dup_invoice_description,
                         COUNT (DISTINCT t.terms_name) dup_terms_name,
                         COUNT (DISTINCT t.payment_method_lookup_code)
                            dup_payment_method_lookup_code,
                         COUNT (DISTINCT t.employee_number) dup_employee_number,
                         COUNT (DISTINCT t.ap_liability_account)
                            dup_ap_liability_account
                    FROM dtac_ap_invoices_temp_int t
                   WHERE     t.status = 'NEW'
                         AND t.GROUP_ID = r_grp.GROUP_ID
                         AND t.org_id = i_org_id
                GROUP BY t.vendor_num,
                         t.vendor_site_code,
                         t.invoice_num,
                         SUBSTR (
                            t.dist_code_concatinated,
                            1,
                            INSTR (t.dist_code_concatinated, '.', 1) - 1)
                  HAVING    COUNT (DISTINCT t.invoice_date) > 1
                         OR COUNT (DISTINCT t.doc_category_code) > 1
                         OR COUNT (DISTINCT t.invoice_currency_code) > 1
                         OR COUNT (DISTINCT t.gl_date) > 1
                         OR COUNT (DISTINCT t.invoice_description) > 1
                         OR COUNT (DISTINCT t.terms_name) > 1
                         OR COUNT (DISTINCT t.payment_method_lookup_code) > 1
                         OR COUNT (DISTINCT t.employee_number) > 1
                         OR COUNT (DISTINCT t.ap_liability_account) > 1)
         LOOP
            v_count_invalid_header := v_count_invalid_header + 1;
            v_header_error :=
               'Invoice number (' || r_inv_h.invoice_num || ') error with ';
            msg_rec_h := '';

            IF r_inv_h.dup_invoice_date > 1
            THEN
               v_header_error :=
                  v_header_error || '|' || 'multiple invoice date,';
               msg_rec_h :=
                     CASE
                        WHEN msg_rec_h IS NOT NULL THEN msg_rec_h || ', '
                     END
                  || 'Multiple invoice date.';
            END IF;

            IF r_inv_h.dup_doc_category_code > 1
            THEN
               v_header_error :=
                  v_header_error || '|' || 'multiple document category,';
               msg_rec_h :=
                     CASE
                        WHEN msg_rec_h IS NOT NULL THEN msg_rec_h || ', '
                     END
                  || 'Multiple document category.';
            END IF;

            IF r_inv_h.dup_invoice_currency_code > 1
            THEN
               v_header_error :=
                  v_header_error || '|' || 'multiple currency code,';
               msg_rec_h :=
                     CASE
                        WHEN msg_rec_h IS NOT NULL THEN msg_rec_h || ', '
                     END
                  || 'Multiple currency code.';
            END IF;

            IF r_inv_h.dup_gl_date > 1
            THEN
               v_header_error := v_header_error || '|' || 'multiple GL date,';
               msg_rec_h :=
                     CASE
                        WHEN msg_rec_h IS NOT NULL THEN msg_rec_h || ', '
                     END
                  || 'Multiple GL date.';
            END IF;

            IF r_inv_h.dup_invoice_description > 1
            THEN
               v_header_error :=
                  v_header_error || '|' || 'multiple invoice description,';
               msg_rec_h :=
                     CASE
                        WHEN msg_rec_h IS NOT NULL THEN msg_rec_h || ', '
                     END
                  || 'Multiple invoice description.';
            END IF;

            IF r_inv_h.dup_terms_name > 1
            THEN
               v_header_error := v_header_error || '|' || 'multiple term,';
               msg_rec_h :=
                     CASE
                        WHEN msg_rec_h IS NOT NULL THEN msg_rec_h || ', '
                     END
                  || 'Multiple term.';
            END IF;

            IF r_inv_h.dup_payment_method_lookup_code > 1
            THEN
               v_header_error :=
                  v_header_error || '|' || 'multiple payment method,';
               msg_rec_h :=
                     CASE
                        WHEN msg_rec_h IS NOT NULL THEN msg_rec_h || ', '
                     END
                  || 'Multiple payment method.';
            END IF;

            IF r_inv_h.dup_employee_number > 1
            THEN
               v_header_error :=
                  v_header_error || '|' || 'multiple requester,';
               msg_rec_h :=
                     CASE
                        WHEN msg_rec_h IS NOT NULL THEN msg_rec_h || ', '
                     END
                  || 'Multiple requester.';
            END IF;

            IF r_inv_h.dup_ap_liability_account > 1
            THEN
               v_header_error :=
                  v_header_error || '|' || 'multiple liability account';
               msg_rec_h :=
                     CASE
                        WHEN msg_rec_h IS NOT NULL THEN msg_rec_h || ', '
                     END
                  || 'Multiple liability account.';
            END IF;

            write_log (v_header_error);
         END LOOP;

         IF v_count_invalid_header > 0
         THEN
            write_log ('');
            err_code := 2;
            /*msg_rec_h :=
                  'Source group '
               || r_grp.GROUP_ID
               || ' : Invoice information invalid. See log for detail.';*/
            EXIT;
         END IF;

         write_log (
            '+----------------------------------------------------------------------------+');
         write_log ('Group ID : ' || r_grp.GROUP_ID);

         FOR r_inv IN c_inv (r_grp.GROUP_ID)
         LOOP
            v_save_pnt := r_inv.invoice_num || r_inv.vendor_num;
            SAVEPOINT v_save_pnt;
            msg_rec_h := '';

            r_invoice := NULL;
            v_error_flag := FALSE;
            write_log ('Vendor Number : ' || r_inv.vendor_num);
            write_log ('Invoice Number : ' || r_inv.invoice_num);
            v_group := r_inv.GROUP_ID;
            v_source := r_inv.source;
            v_batch := r_inv.GROUP_ID;

            /** AP_INVOICES_INTERFACE **/
            --SEQ
            SELECT ap_invoices_interface_s.NEXTVAL
              INTO r_invoice.invoice_id
              FROM DUAL;

            --Supplier
            BEGIN
               SELECT v.vendor_id
                 INTO r_invoice.vendor_id
                 FROM po_vendors v
                WHERE v.segment1 = r_inv.vendor_num;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_error_flag := TRUE;
                  write_log ('*Invalid Supplier : ' || r_inv.vendor_num);
                  err_msg := '*** DATA ERROR : Interface data is invalid!';
                  err_code := '1';
                  msg_rec_h :=
                        CASE
                           WHEN msg_rec_h IS NOT NULL THEN msg_rec_h || ', '
                        END
                     || 'Supplier no data found.';
               WHEN OTHERS
               THEN
                  v_error_flag := TRUE;
                  write_log ('*Validate Supplier : ' || r_inv.vendor_num);
                  err_msg := 'Error : ' || SQLERRM;
                  err_code := '1';
                  msg_rec_h :=
                        CASE
                           WHEN msg_rec_h IS NOT NULL THEN msg_rec_h || ', '
                        END
                     || 'Validate Supplier Error : '
                     || SQLERRM;
            END;

            --Site---
            BEGIN
               SELECT vs.*
                 INTO v_site
                 FROM po_vendor_sites_all vs
                WHERE     vs.vendor_site_code = r_inv.vendor_site_code
                      AND vs.vendor_id = r_invoice.vendor_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  v_error_flag := TRUE;

                  write_log (
                     '*Invalid Supplier Site : ' || r_inv.vendor_site_code);
                  write_log ('*Supplier Number : ' || r_inv.vendor_num);
                  err_msg := '*** DATA ERROR : Interface data is invalid!';
                  err_code := '1';
                  v_site := NULL;
                  msg_rec_h :=
                        CASE
                           WHEN msg_rec_h IS NOT NULL THEN msg_rec_h || ', '
                        END
                     || 'Supplier Site no data found.';
               WHEN OTHERS
               THEN
                  v_error_flag := TRUE;
                  write_log (
                     '*Validate Supplier Site : ' || r_inv.vendor_site_code);
                  err_msg := 'Error : ' || SQLERRM;
                  err_code := '1';
                  msg_rec_h :=
                        CASE
                           WHEN msg_rec_h IS NOT NULL THEN msg_rec_h || ', '
                        END
                     || 'Validate Supplier Site Error : '
                     || SQLERRM;
            END;

            r_invoice.vendor_site_id := v_site.vendor_site_id;
            r_invoice.accts_pay_code_combination_id :=
               v_site.accts_pay_code_combination_id;
            v_site_auto_calc_flag := v_site.auto_tax_calc_flag;

            --Term
            IF r_inv.terms_name IS NOT NULL
            THEN
               BEGIN
                  SELECT t.term_id, t.name, r_inv.invoice_date
                    --,t.creation_date -- Change to invoice_date
                    INTO r_invoice.terms_id,
                         r_invoice.terms_name,
                         r_invoice.terms_date
                    FROM ap_terms t
                   WHERE t.name = r_inv.terms_name;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     v_error_flag := TRUE;
                     write_log ('*Invalid Term Name : ' || r_inv.terms_name);
                     err_msg := '*** DATA ERROR : Interface data is invalid!';
                     err_code := '1';
                     msg_rec_h :=
                           CASE
                              WHEN msg_rec_h IS NOT NULL
                              THEN
                                 msg_rec_h || ', '
                           END
                        || 'Term Name no data found.'
                        || SQLERRM;
                  WHEN OTHERS
                  THEN
                     v_error_flag := TRUE;
                     write_log ('*Validate Term Name : ' || r_inv.terms_name);
                     err_msg := 'Error : ' || SQLERRM;
                     err_code := '1';
                     msg_rec_h :=
                           CASE
                              WHEN msg_rec_h IS NOT NULL
                              THEN
                                 msg_rec_h || ', '
                           END
                        || 'Validate Term Name Error : '
                        || SQLERRM;
               END;
            END IF;

            v_requester_id := 0;

            BEGIN
               SELECT person_id
                 INTO v_requester_id
                 FROM per_people_f
                WHERE     employee_number = TO_CHAR (r_inv.employee_number)
                      AND (    (TRUNC (effective_start_date) <=
                                   TRUNC (SYSDATE))
                           AND (TRUNC (effective_end_date) >= TRUNC (SYSDATE)));
            EXCEPTION
               WHEN OTHERS
               THEN
                  v_error_flag := TRUE;
                  write_log (
                     '*Invalid Employee Number : ' || r_inv.employee_number);
                  err_msg := '*** DATA ERROR : Interface data is invalid!';
                  err_code := '1';
                  v_requester_id := 0;
                  msg_rec_h :=
                        CASE
                           WHEN msg_rec_h IS NOT NULL THEN msg_rec_h || ', '
                        END
                     || 'Validate Employee Error : '
                     || SQLERRM;
            END;

            -- get chart of account
            BEGIN
               SELECT sob.chart_of_accounts_id
                 INTO v_chart_of_accounts_id
                 FROM gl_sets_of_books sob
                WHERE sob.set_of_books_id = v_gl_set_of_books_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  v_error_flag := TRUE;
                  write_log (
                        '*Invalid Chart of Account that relate with SOB : '
                     || v_gl_set_of_books_id);
                  err_msg := '*** DATA ERROR : Interface data is invalid!';
                  err_code := '1';
                  msg_rec_h :=
                        CASE
                           WHEN msg_rec_h IS NOT NULL THEN msg_rec_h || ', '
                        END
                     || 'Validate Chart of Account that relate with SOB Error : '
                     || SQLERRM;
            END;

            -- Validate  dist_code_concatinated
            BEGIN
               SELECT gcc.code_combination_id
                 INTO v_dist_code_concatinated
                 FROM gl_code_combinations_kfv gcc
                WHERE     gcc.concatenated_segments =
                             r_inv.dist_code_concatinated
                      AND gcc.chart_of_accounts_id = v_chart_of_accounts_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  v_error_flag := TRUE;
                  write_log (
                        '*Invalid Concatenated Segment Account : '
                     || r_inv.dist_code_concatinated);
                  err_msg := '*** DATA ERROR : Interface data is invalid!';
                  err_code := '1';
                  msg_rec_h :=
                        CASE
                           WHEN msg_rec_h IS NOT NULL THEN msg_rec_h || ', '
                        END
                     || 'Validate Concatenated Segment Account Error : ' || r_inv.dist_code_concatinated;
                     --|| SQLERRM;
            END;

            -- Validate  ap_liability_account
            BEGIN
               SELECT gcc.code_combination_id
                 INTO v_liability_account_id
                 FROM gl_code_combinations_kfv gcc
                WHERE     gcc.concatenated_segments =
                             r_inv.ap_liability_account
                      AND gcc.chart_of_accounts_id = v_chart_of_accounts_id;
            EXCEPTION
               WHEN OTHERS
               THEN                  
                  v_error_flag := TRUE;
                  write_log (
                        '*Invalid Liability Account : '
                     || r_inv.ap_liability_account);
                  err_msg := '*** DATA ERROR : Interface data is invalid!';
                  err_code := '1';
                  msg_rec_h :=
                        CASE
                           WHEN msg_rec_h IS NOT NULL THEN msg_rec_h || ', '
                        END
                     || 'Validate Liability Account : '
                     || SQLERRM;                  
            END;

            IF r_inv.gl_date IS NULL
            THEN
               r_invoice.gl_date := TRUNC (SYSDATE);
            ELSE
               r_invoice.gl_date := r_inv.gl_date;
            END IF;

            --org_id depend on dist_code_concatinated
            IF (r_inv.acc_ou_segment = '08')
            THEN
               --DTN
               r_invoice.org_id := g_dtn_org_id;
            ELSIF (r_inv.acc_ou_segment = '12')
            THEN
               --PSB
               r_invoice.org_id := g_psb_org_id;
            ELSIF (r_inv.acc_ou_segment = '09')
            THEN
               --DTAC W
               r_invoice.org_id := g_dtw_org_id;
            ELSE
               r_invoice.org_id := i_org_id;
            END IF;

            ---
            v_debug_step := 'Assign r_invoice value';
            r_invoice.invoice_num := r_inv.invoice_num;
            r_invoice.invoice_type_lookup_code :=
               r_inv.invoice_type_lookup_code;                   --'STANDARD';
            r_invoice.invoice_date := r_inv.invoice_date;
            r_invoice.vendor_num := r_inv.vendor_num;
            r_invoice.vendor_site_code := r_inv.vendor_site_code;
            r_invoice.invoice_amount := r_inv.invoice_amount;
            r_invoice.invoice_currency_code := r_inv.invoice_currency_code;
            r_invoice.source := r_inv.source;
            r_invoice.GROUP_ID := r_inv.GROUP_ID;
            r_invoice.doc_category_code := r_inv.doc_category_code;
            r_invoice.description := r_inv.invoice_description;
            --r_invoice.terms_id := r_inv.terms_name;
            --r_invoice.terms_name := r_inv.terms_name;
            r_invoice.payment_method_lookup_code :=
               r_inv.payment_method_lookup_code;
            r_invoice.creation_date := SYSDATE;
            r_invoice.last_update_date := SYSDATE;
            r_invoice.created_by := fnd_global.user_id;
            r_invoice.last_updated_by := fnd_global.user_id;
            r_invoice.request_id := fnd_global.conc_request_id;
            r_invoice.attribute_category := 'JA.TH.APXINWKB.DISTRIBUTIONS';
            r_invoice.global_attribute_category :=
               'JA.TH.APXINWKB.DISTRIBUTIONS';
            r_invoice.requester_id := v_requester_id;
            r_invoice.accts_pay_code_combination_id := v_liability_account_id;
            r_invoice.pay_group_lookup_code := r_inv.pay_group;

            /*Modify On 10/31/2016 by spw@ice to following.
                - Mapping between Requisition and Invoice.
                - Keep Batch ID in the AP Invoice.
            */
            r_invoice.attribute5 := r_inv.batch_id;
            v_req_rec := get_requisition_by_batch_id (r_inv.batch_id);

            write_log ('r_inv.batch_id = ' || r_inv.batch_id);
            write_log (
                  'r_inv.requisition_header_id = '
               || v_req_rec.requisition_header_id);

            IF (v_req_rec.requisition_header_id IS NOT NULL)
            THEN
               v_mapping.batch_id := r_inv.batch_id;
               v_mapping.requisition_header_id :=
                  v_req_rec.requisition_header_id;
               v_mapping.requisition_num := v_req_rec.requisition_num;
               v_mapping.invoice_num := r_inv.invoice_num;
               v_mapping.invoice_date := r_inv.invoice_date;

               insert_to_mapping_table (v_mapping);
            END IF;

            /*-----------------------------------------------*/

            IF v_error_flag
            THEN
               v_debug_step := 'update_status';
               update_status (i_inv_num          => r_inv.invoice_num,
                              i_vendor_number    => r_inv.vendor_num,
                              i_acc_ou_segment   => r_inv.acc_ou_segment,
                              i_status           => 'REJECTED',
                              i_group_id         => r_inv.GROUP_ID,
                              p_msg_rec          => msg_rec_h);
               v_validate_error := TRUE;

               write_log (
                  '---------------------------------- Start Update TAC_AP_INF_FILES 1 ----------------------------------');
               write_log ('UPDATE P_REQUEST_ID : ' || p_request_id);
               write_log (
                  'UPDATE SOURCE_FILE_NAME: ' || r_inv.source_file_name);

               UPDATE tac_ap_inf_files
                  SET error_message = 'ERROR'
                WHERE     request_id = p_request_id
                      AND file_name = r_inv.source_file_name;

               write_log (
                  '---------------------------------- End Update TAC_AP_INF_FILES ----------------------------------');
               COMMIT;
            ELSE
               v_line_num := 0;
               r_invoice.invoice_amount := r_inv.invoice_amount;
               v_chk_line := FALSE;

               write_log ('r_inv.acc_ou_segment = ' || r_inv.acc_ou_segment);

               FOR r_line IN c_line (r_inv.GROUP_ID,
                                     r_invoice.invoice_num,
                                     r_invoice.vendor_num,
                                     NVL (r_inv.acc_ou_segment, '01'))
               LOOP
                  v_line_num := v_line_num + 1;
                  r_invoice_line := NULL;
                  v_chk_line := FALSE;
                  msg_rec := '';

                  v_line_status_info :=
                        'Row#'
                     || v_line_num
                     || ' |Invoice line#'
                     || r_line.line_number;

                  /** AP_INVOICE_LINES_INTERFACE **/
                  SELECT ap_invoice_lines_interface_s.NEXTVAL
                    INTO r_invoice_line.invoice_line_id
                    FROM DUAL;

                  -- Validate data for Withholding Tax
                  IF r_line.awt_group_name IS NOT NULL
                  THEN
                     BEGIN
                        SELECT ag.GROUP_ID
                          INTO r_invoice_line.awt_group_id
                          FROM ap_awt_groups ag
                         WHERE ag.name = r_line.awt_group_name;
                     EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                           v_chk_line := TRUE;
                           v_line_status_info :=
                                 v_line_status_info
                              || '|Invalid Withholding Tax Group ('
                              || r_line.awt_group_name
                              || ')';
                           err_msg :=
                              '*** DATA ERROR : Interface data is invalid!';
                           err_code := '1';

                           msg_rec :=
                                 CASE
                                    WHEN msg_rec IS NOT NULL
                                    THEN
                                       msg_rec || ', '
                                 END
                              || 'Withholding Tax Group ('
                              || r_line.awt_group_name
                              || ') no data found.';
                     END;

                     IF r_line.wht_condition IS NULL
                     THEN
                        v_line_status_info :=
                           v_line_status_info || '|WHT Condition is NULL';
                        v_chk_line := TRUE;
                        err_msg :=
                           '*** DATA ERROR : Interface data is invalid!';
                        err_code := '1';
                        msg_rec :=
                              CASE
                                 WHEN msg_rec IS NOT NULL
                                 THEN
                                    msg_rec || ', '
                              END
                           || '| WHT Condition is NULL.';
                     END IF;

                     IF r_line.wht_revenue_type IS NULL
                     THEN
                        v_line_status_info :=
                           v_line_status_info || '|WHT Revenue Type is NULL';
                        v_chk_line := TRUE;
                        err_msg :=
                           '*** DATA ERROR : Interface data is invalid!';
                        err_code := '1';
                        msg_rec :=
                              CASE
                                 WHEN msg_rec IS NOT NULL
                                 THEN
                                    msg_rec || ', '
                              END
                           || '| WHT Revenue Type is NULL.';
                     END IF;

                     IF r_line.wht_revenue_name IS NULL
                     THEN
                        v_line_status_info :=
                           v_line_status_info || '|WHT Revenue Name NULL';
                        v_chk_line := TRUE;
                        err_msg :=
                           '*** DATA ERROR : Interface data is invalid!';
                        err_code := '1';
                        msg_rec :=
                              CASE
                                 WHEN msg_rec IS NOT NULL
                                 THEN
                                    msg_rec || ', '
                              END
                           || '| WHT Revenue Name is NULL.';
                     END IF;

                     IF r_line.phor_ngor_dor IS NULL
                     THEN
                        v_line_status_info :=
                           v_line_status_info || '|Phor Ngor Dor is NULL';
                        v_chk_line := TRUE;
                        err_msg :=
                           '*** DATA ERROR : Interface data is invalid!';
                        err_code := '1';
                        msg_rec :=
                              CASE
                                 WHEN msg_rec IS NOT NULL
                                 THEN
                                    msg_rec || ', '
                              END
                           || '| Phor Ngor Dor is NULL.';
                     END IF;
                  END IF;

                  r_invoice_line.amount := r_line.line_amount;

                  IF r_line.line_tax_code IS NOT NULL
                  THEN
                     BEGIN
                        SELECT atc.tax_code_combination_id,
                               atc.tax_id,
                               atc.tax_rate
                          INTO v_tax_ccid, r_invoice_line.tax_code_id, v_rate
                          FROM ap_tax_codes_all atc
                         WHERE     atc.enabled_flag = 'Y'
                               AND atc.name = r_line.line_tax_code
                               AND NVL (atc.start_date, SYSDATE) <= SYSDATE
                               AND NVL (atc.inactive_date, SYSDATE) >=
                                      SYSDATE
                               AND atc.org_id = r_invoice.org_id;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           v_chk_line := TRUE;
                           v_line_status_info :=
                                 v_line_status_info
                              || '|Invalid Tax Code ('
                              || r_line.line_tax_code
                              || ')';
                           err_msg :=
                              '*** DATA ERROR : Interface data is invalid!';
                           err_code := '1';
                           msg_rec :=
                                 CASE
                                    WHEN msg_rec IS NOT NULL
                                    THEN
                                       msg_rec || ', '
                                 END
                              || '|Invalid Tax Code ('
                              || r_line.line_tax_code
                              || ')';
                     END;

                     r_invoice_line.tax_code := r_line.line_tax_code;
                     r_invoice_line.tax_code_override_flag := 'N';
                     r_invoice_line.tax_recoverable_flag := 'Y';
                  ELSE
                     r_invoice_line.tax_code := NULL;
                     r_invoice_line.tax_recovery_rate := NULL;
                  END IF;

                  v_supp_name := NULL;

                  IF     r_line.project_id IS NULL
                     AND r_line.project_task IS NULL
                     AND r_line.expenditure_type IS NULL
                     AND r_line.organization_id IS NULL
                  THEN
                     v_validate_error := FALSE;
                  ELSE
                     -- Project Number begin add
                     IF r_line.project_id IS NULL
                     THEN
                        v_validate_error := TRUE;
                        v_error_flag := TRUE;
                        v_line_status_info :=
                              v_line_status_info
                           || '|Invalid Project ('
                           || r_line.project_number
                           || ')';
                        write_log (
                           '*Invalid Project : ' || r_line.project_number);
                        err_msg :=
                           '*** DATA ERROR : Interface data is invalid!';
                        err_code := '1';

                        msg_rec :=
                              CASE
                                 WHEN msg_rec IS NOT NULL
                                 THEN
                                    msg_rec || ', '
                              END
                           || '|Invalid Project ('
                           || r_line.project_number
                           || ')';
                     -- END;
                     END IF;                                     ------end add

                     -- Task Number begin add
                     IF r_line.project_id IS NOT NULL
                     THEN
                        BEGIN
                           SELECT pt.task_id
                             INTO v_task_id
                             FROM (SELECT *
                                     FROM pa_tasks pt
                                    WHERE pt.project_id(+) =
                                             r_line.project_id) pt --,PA_PROJECTS_ALL ppa
                            WHERE pt.task_number = r_line.project_task;
                        -- and pt.project_id = r_line.project_id;

                        EXCEPTION
                           WHEN NO_DATA_FOUND
                           THEN
                              v_validate_error := TRUE;
                              v_error_flag := TRUE;
                              v_line_status_info :=
                                    v_line_status_info
                                 || '|Invalid Task ('
                                 || r_line.project_task
                                 || ')';
                              write_log (
                                 '*Invalid Task : ' || r_line.project_task);
                              err_msg :=
                                 '*** DATA ERROR : Interface data is invalid!';
                              err_code := '1';
                              msg_rec :=
                                    CASE
                                       WHEN msg_rec IS NOT NULL
                                       THEN
                                          msg_rec || ', '
                                    END
                                 || '|Invalid Task ('
                                 || r_line.project_task
                                 || ')';
                        END;
                     ELSE
                        v_validate_error := TRUE;
                        v_line_status_info :=
                              v_line_status_info
                           || '|Invalid Task ('
                           || r_line.project_task
                           || ')';
                        write_log ('*Invalid Task : ' || r_line.project_task);
                        err_msg :=
                           '*** DATA ERROR : Interface data is invalid!';
                        err_code := '1';
                        msg_rec :=
                              CASE
                                 WHEN msg_rec IS NOT NULL
                                 THEN
                                    msg_rec || ', '
                              END
                           || '|Invalid Task 2 ('
                           || r_line.project_task
                           || ')';
                     END IF;                                        ---end add

                     --  Expenditure Type begin add
                     BEGIN
                        SELECT pet.expenditure_type
                          INTO v_expen_type
                          FROM pa_expenditure_types pet
                         WHERE pet.expenditure_type = r_line.expenditure_type;
                     EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                           v_validate_error := TRUE;
                           v_error_flag := TRUE;
                           v_line_status_info :=
                                 v_line_status_info
                              || '|Invalid Expenditure Type ('
                              || r_line.expenditure_type
                              || ')';
                           --write_log('*Invalid Expenditure Type : ' || r_line.expenditure_type);
                           err_msg :=
                              '*** DATA ERROR : Interface data is invalid!';
                           err_code := '1';
                           msg_rec :=
                                 CASE
                                    WHEN msg_rec IS NOT NULL
                                    THEN
                                       msg_rec || ', '
                                 END
                              || '|Invalid Expenditure Type ('
                              || r_line.expenditure_type
                              || ')';
                     END;

                     --END IF; ---end add

                     --  Expenditure Org begin add
                     IF r_line.organization_id IS NULL
                     THEN
                        BEGIN
                           SELECT hou.organization_id
                             INTO v_expen_org
                             FROM hr_organization_units hou,
                                  dtac_ap_invoices_temp_int l
                            WHERE     hou.organization_id =
                                         r_line.organization_id
                                  AND hou.name(+) =
                                         l.expenditure_organization;
                        EXCEPTION
                           WHEN NO_DATA_FOUND
                           THEN
                              v_validate_error := TRUE;
                              v_error_flag := TRUE;
                              v_line_status_info :=
                                    v_line_status_info
                                 || '|Invalid Expenditure Organization ('
                                 || r_line.expenditure_organization
                                 || ')';
                              --write_log('*Invalid Expenditure Organization : ' || r_line.expenditure_organization);
                              err_msg :=
                                 '*** DATA ERROR : Interface data is invalid!';
                              err_code := '1';
                              msg_rec :=
                                    CASE
                                       WHEN msg_rec IS NOT NULL
                                       THEN
                                          msg_rec || ', '
                                    END
                                 || '|Invalid Expenditure Organization ('
                                 || r_line.expenditure_organization
                                 || ')';
                        END;
                     END IF;                                        ---end add
                  END IF;

                  r_invoice_line.invoice_id := r_invoice.invoice_id;
                  r_invoice_line.org_id := r_invoice.org_id;
                  r_invoice_line.line_number := v_line_num;
                  r_invoice_line.accounting_date := r_line.gl_date;
                  r_invoice_line.project_id := r_line.project_id; ---iCE-Tae add
                  r_invoice_line.task_id := v_task_id; --r_line.task_id; ---iCE-Tae add
                  r_invoice_line.expenditure_type := r_line.expenditure_type; ---iCE-Tae add
                  r_invoice_line.expenditure_organization_id :=
                     r_line.organization_id;                    ---iCE-Tae add
                  r_invoice_line.expenditure_item_date := SYSDATE; ---iCE-Tae add
                  r_invoice_line.line_type_lookup_code :=
                     r_line.line_type_lookup_code;                   --'ITEM';

                  IF (v_supp_name IS NULL)
                  THEN
                     r_invoice_line.description := r_line.line_description;
                  ELSE
                     r_invoice_line.description :=
                        v_supp_name || ',' || r_line.line_description;
                  END IF;

                  r_invoice_line.dist_code_concatenated :=
                     r_line.dist_code_concatinated;
                  r_invoice_line.awt_group_name := r_line.awt_group_name;
                  r_invoice_line.attribute4 := r_line.act_vendor_num;
                  r_invoice_line.attribute5 := r_line.act_vendor_site_code;
                  r_invoice_line.global_attribute_category :=
                     'JA.TH.APXINWKB.DISTRIBUTIONS';

                  IF r_invoice_line.global_attribute16 IS NULL
                  THEN
                     r_invoice_line.global_attribute16 := r_inv.source;
                  END IF;

                  IF r_line.tax_acct_period IS NULL
                  THEN
                     r_invoice_line.attribute13 := v_period;
                  ELSE
                     r_invoice_line.attribute13 := r_line.tax_acct_period;
                  END IF;

                  r_invoice_line.attribute15 := r_line.reporting_entity;
                  r_invoice_line.global_attribute9 := v_supp_name;
                  r_invoice_line.attribute_category :=
                     'JA.TH.APXINWKB.DISTRIBUTIONS';
                  r_invoice_line.global_attribute20 := r_line.reporting_entity;
                  r_invoice_line.global_attribute12 := r_line.wht_revenue_name;
                  r_invoice_line.global_attribute13 := r_line.wht_revenue_type;
                  r_invoice_line.global_attribute14 := r_line.wht_condition;
                  r_invoice_line.global_attribute15 := r_line.phor_ngor_dor;
                  r_invoice_line.global_attribute18 := r_line.wht_acct_period;

                  r_invoice_line.created_by := fnd_global.user_id;
                  r_invoice_line.creation_date := SYSDATE;

                  /* Overide value global_attribute ... 5,6,7,8,9 fomr interface data instead derived*/
                  r_invoice_line.global_attribute5 :=
                     r_line.wht_supplier_id_card;
                  r_invoice_line.global_attribute6 :=
                     r_line.wht_supplier_tax_id;
                  r_invoice_line.global_attribute7 := r_line.wht_address2;
                  r_invoice_line.global_attribute8 := r_line.wht_address1;
                  r_invoice_line.global_attribute9 :=
                     r_line.wht_act_supplier_name;

                  IF v_chk_line
                  THEN
                     --ROLLBACK TO SAVEPOINT rejected;
                     v_line_status_info := v_line_status_info || '| Rejected';
                     update_status (
                        i_inv_num          => r_invoice.invoice_num,
                        i_vendor_number    => r_inv.vendor_num,
                        i_acc_ou_segment   => r_inv.acc_ou_segment,
                        i_line_num         => r_line.line_number,
                        i_status           => 'REJECTED',
                        i_group_id         => r_inv.GROUP_ID,
                        p_msg_rec          => msg_rec);

                     v_validate_error := TRUE;
                     write_log (
                        '---------------------------------- Start Update TAC_AP_INF_FILES ----------------------------------');
                     write_log ('UPDATE P_REQUEST_ID : ' || p_request_id);
                     write_log (
                           'UPDATE SOURCE_FILE_NAME: '
                        || r_line.source_file_name);

                     UPDATE tac_ap_inf_files
                        SET error_message = 'ERROR'
                      WHERE     request_id = p_request_id
                            AND file_name = r_line.source_file_name;

                     write_log (
                        '---------------------------------- End Update TAC_AP_INF_FILES ----------------------------------');
                     COMMIT;
                  ELSE
                     IF     r_line.line_type_lookup_code = 'ITEM'
                        AND r_line.line_tax_code IS NOT NULL
                     THEN
                        BEGIN
                           SELECT tax_rate
                             INTO v_tax_rate
                             FROM ap_tax_codes_all
                            WHERE     name = r_line.line_tax_code
                                  AND org_id = r_invoice.org_id
                                  AND (   (enabled_flag = 'Y')
                                       OR (enabled_flag IS NULL))
                                  AND (    (TRUNC (start_date) <=
                                               TRUNC (SYSDATE))
                                       AND (TRUNC (
                                               NVL (inactive_date, SYSDATE)) >=
                                               TRUNC (SYSDATE)));
                        EXCEPTION
                           WHEN OTHERS
                           THEN
                              v_tax_rate := 0;
                        END;

                        r_invoice_line.attribute9 := r_invoice_line.amount;
                        r_invoice_line.attribute8 :=
                           ROUND (r_invoice_line.amount * (v_tax_rate / 100),
                                  2);
                        r_invoice_line.attribute10 := v_supp_name;
                     END IF;

                     IF v_error_flag = FALSE
                     THEN
                        insert_invoice_line (
                           ir_invoice_line => r_invoice_line);         -- ITEM
                     END IF;

                     --Remark by AP@BAS on 16-Oct-2014
                     -- Invoice amount header get direct from temp table
                     --r_invoice.invoice_amount := nvl(r_invoice.invoice_amount, 0) + r_invoice_line.amount; -- For Invoice Header -- SMT invoice not include tax
                     v_line_status_info := v_line_status_info || '| Processed';
                     update_status (
                        i_inv_num          => r_invoice.invoice_num,
                        i_vendor_number    => r_inv.vendor_num,
                        i_acc_ou_segment   => r_inv.acc_ou_segment,
                        i_line_num         => r_line.line_number,
                        i_status           => 'PROCESSED',
                        i_group_id         => r_inv.GROUP_ID,
                        p_msg_rec          => msg_rec);

                     v_line_count := v_line_count + 1;
                  END IF;

                  IF v_chk_line
                  THEN
                     v_error_flag := TRUE;
                  END IF;

                  write_log (v_line_status_info);
               END LOOP;                                                --Line
            END IF;

            --IF v_chk_line = FALSE
            IF v_error_flag = FALSE
            THEN
               insert_invoice (ir_invoice => r_invoice);
               write_log ('Invoice ID: ' || r_invoice.invoice_id);
            END IF;
         /* fixed by spw@ice 16Feb2017 for Error MSG: -1086 - ORA-01086: savepoint 'V_SAVE_PNT' never established in this session or is invalid.
         IF v_error_flag = TRUE
         THEN
            write_log (
                  'Rollback transaction of invoice number '
               || r_inv.invoice_num);
            ROLLBACK TO SAVEPOINT v_save_pnt;
         END IF;*/
         END LOOP;                                                   -- Header

         IF (v_line_count <= 0)
         THEN
            write_log ('No invoice line');
         END IF;

         --write_log('Stop import process for develop');

         --IF NOT v_validate_error AND v_line_count > 0
         IF v_line_count > 0
         THEN
            COMMIT;                                                    -- SAVE

            v_debug_step := 'submit_request APXIIMPT';

            BEGIN
                 SELECT DECODE (org_id,  102, 50282,  142, 50501)
                   INTO l_resp_id
                   FROM dtac_ap_invoices_temp_int
                  WHERE     status = 'PROCESSED'
                        AND source = 'SMT'
                        AND org_id = i_org_id
               GROUP BY org_id;

               fnd_global.apps_initialize (
                  fnd_profile.VALUE ('USER_ID'),
                  l_resp_id,
                  fnd_profile.VALUE ('RESP_APPL_ID'),
                  NULL,
                  NULL);
               ap_req_id :=
                  fnd_request.submit_request (application   => 'SQLAP',
                                              program       => 'APXIIMPT',
                                              argument1     => v_source,
                                              argument2     => v_group,
                                              argument3     => v_batch,
                                              argument4     => NULL,
                                              argument5     => NULL,
                                              argument6     => NULL,
                                              argument7     => 'N');
            END;

            v_i_group := v_group;
            v_ap_req_id := ap_req_id;

            UPDATE dtac_ap_invoices_temp_int
               SET inf_request_id = ap_req_id
             WHERE request_id = p_request_id AND org_id = i_org_id;

            COMMIT;

            IF ap_req_id > 0
            THEN
               write_log (
                  'Payable Interface Start Request ID: ' || ap_req_id);
               conc_wait (ap_req_id);
               v_debug_step := 'submit_request TACAP016_V3_EXTD';
               c_ap_req_id :=
                  fnd_request.submit_request (
                     application   => 'SQLAP',
                     program       => 'TACAP016_V3_EXTD',
                     argument1     => v_ap_req_id,
                     argument2     => v_i_group);
            ELSE
               write_log (
                     'Can not Submit Payable Open Interface '
                  || TO_CHAR (SYSDATE, 'DD/MM/YYYY HH24:MI:SS')
                  || ' ------');
            END IF;

         END IF;
         COMMIT;
      END LOOP;                                           -- end of group loop
   EXCEPTION
      WHEN OTHERS
      THEN
         write_log ('tac_ap_interface_util_smt.interface_ap exception .');
         write_log ('Error MSG: ' || SQLCODE || ' - ' || SQLERRM);
         write_log ('Last step process = ' || v_debug_step);
   END interface_ap;

   /*
     Move data file from inbox
          To history if no error.
          To error if has error.
   */
   PROCEDURE move_data_file (p_source_file VARCHAR2, p_dest_path VARCHAR2)
   IS
      v_req_id   NUMBER;
   BEGIN
      v_req_id :=
         fnd_request.submit_request (application   => 'SQLAP',
                                     program       => 'TACAP_MVFILE_SMT',
                                     argument1     => p_source_file,
                                     argument2     => p_dest_path);
      write_log (
            'Move file from '
         || p_source_file
         || ' to '
         || p_dest_path
         || ' success.');
   EXCEPTION
      WHEN OTHERS
      THEN
         write_log (
               'Move file from '
            || p_source_file
            || ' to '
            || p_dest_path
            || ' failed with error '
            || SQLERRM);
   END move_data_file;

   /*PROCEDURE import_data (err_msg              OUT VARCHAR2,
                          err_code             OUT VARCHAR2,
                          p_file_path              VARCHAR2,
                          p_file_name_format       VARCHAR2,
                          p_file_name              VARCHAR2 DEFAULT NULL,
                          p_delimiter              VARCHAR2 DEFAULT '|',
                          p_debug_flag             VARCHAR2 DEFAULT 'N')*/
   FUNCTION import_data (p_request_id IN NUMBER)
      RETURN BOOLEAN
   IS
      err_msg               VARCHAR2 (1000);
      err_code              VARCHAR2 (100);

      v_file_name           VARCHAR2 (100);
      v_date                DATE := SYSDATE;
      v_file_handle         UTL_FILE.file_type;
      v_line_data           NVARCHAR2 (32767);
      v_line_count          NUMBER := 0;

      v_rec_data            dtac_ap_invoices_temp_int%ROWTYPE;
      v_insert_error_flag   VARCHAR2 (1) := 'N';

      p_file_path           VARCHAR2 (100) := g_smt_inbox;
      --      p_file_name_format    VARCHAR2(100) := 'SMT$DATE$.txt';
      p_delimiter           VARCHAR2 (3) := '|';
      p_debug_flag          VARCHAR2 (5) := 'N';

      emesg                 VARCHAR2 (3000) := SQLERRM;
   BEGIN
      IF p_file_path IS NULL
      THEN
         RAISE UTL_FILE.invalid_path;
      END IF;

      FOR item IN (  SELECT *
                       FROM tac_ap_inf_files
                      WHERE request_id = p_request_id
                   ORDER BY file_name ASC)
      LOOP
         /*
          IF (p_file_name IS NOT NULL)
          THEN
             v_file_name := p_file_name;
          ELSE
             IF p_file_name_format IS NULL
             THEN
                RAISE UTL_FILE.invalid_filename;
             END IF;

             v_file_name := REPLACE (p_file_name_format,
                         '$DATE$',
                         TRIM (TO_CHAR (v_date, 'yyyymmdd')));
          END IF;
          */

         write_log (g_log_line);
         write_log ('Import date from file ' || item.file_name);
         write_out ('Import date from file ' || item.file_name);
         /*
         BEGIN
            --v_file_handle := utl_file.fopen(location => p_file_path, filename => v_file_name, open_mode => 'r');
            v_file_handle :=
               UTL_FILE.fopen_nchar (location    => p_file_path,
                                     filename    => item.file_name,
                                     open_mode   => 'r');
            UTL_FILE.fclose (v_file_handle);
         --write_log('Read file first time.');
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;*/

         --v_file_handle := utl_file.fopen(location => p_file_path, filename => v_file_name, open_mode => 'r');
         v_file_handle :=
            UTL_FILE.FOPEN_NCHAR (location    => p_file_path,
                                  filename    => item.file_name,
                                  open_mode   => 'r');

         SAVEPOINT before_insert_data;

         LOOP
            BEGIN
               --utl_file.get_line(file => v_file_handle, buffer => v_line_data);
               UTL_FILE.GET_LINE_NCHAR (file     => v_file_handle,
                                        buffer   => v_line_data);
               v_line_data :=
                  REPLACE (REPLACE (v_line_data, CHR (10), ''), CHR (13), '');

               IF (v_line_data = 'END')
               THEN
                  EXIT;
               END IF;

               v_line_count := v_line_count + 1;

               v_rec_data :=
                  split_data (p_line_buff    => v_line_data,
                              p_delimiter    => p_delimiter,
                              p_debug_flag   => p_debug_flag);

               BEGIN
                  INSERT
                    INTO dtac_ap_invoices_temp_int (
                            GROUP_ID,
                            source,
                            invoice_type_lookup_code,
                            vendor_num,
                            vendor_site_code,
                            invoice_num,
                            invoice_date,
                            doc_category_code,
                            invoice_currency_code,
                            invoice_amount,
                            gl_date,
                            invoice_description,
                            terms_name,
                            payment_method_lookup_code,
                            employee_number,
                            line_number,
                            line_type_lookup_code,
                            line_amount,
                            line_tax_code,
                            awt_group_name,
                            dist_code_concatinated,
                            line_description,
                            act_vendor_num,
                            act_vendor_site_code,
                            reporting_entity,
                            tax_invoice_date,
                            tax_invoice_num,
                            tax_acct_period,
                            wht_acct_period,
                            phor_ngor_dor,
                            wht_condition,
                            wht_revenue_type,
                            wht_revenue_name,
                            project_number,
                            project_task,
                            expenditure_type,
                            expenditure_organization,
                            status,
                            eai_crtd_dttm,
                            payee,
                            mail_to_name,
                            mail_to_address1,
                            mail_to_address2,
                            mail_to_address3,
                            mail_to_address4,
                            ap_liability_account,
                            pay_group,
                            wht_act_supplier_name,
                            wht_address1,
                            wht_address2,
                            wht_supplier_tax_id,
                            wht_supplier_id_card /*Modify On 10/31/2016 by spw@ice to following.
                                                 - Mapping between Requisition and Invoice.
                                                 - Keep Batch ID in the AP Invoice.
                                                 */
                                                ,
                            batch_id,
                            org_id,
                            source_file_name,
                            request_id /*----------------End modify--------------------*/
                                      )
                  VALUES (
                            v_rec_data.GROUP_ID,
                            v_rec_data.source,
                            v_rec_data.invoice_type_lookup_code,
                            v_rec_data.vendor_num,
                            v_rec_data.vendor_site_code,
                            v_rec_data.invoice_num,
                            v_rec_data.invoice_date,
                            v_rec_data.doc_category_code,
                            v_rec_data.invoice_currency_code,
                            v_rec_data.invoice_amount,
                            v_rec_data.gl_date,
                            v_rec_data.invoice_description,
                            v_rec_data.terms_name,
                            v_rec_data.payment_method_lookup_code,
                            v_rec_data.employee_number,
                            v_rec_data.line_number,
                            v_rec_data.line_type_lookup_code,
                            v_rec_data.line_amount,
                            v_rec_data.line_tax_code,
                            v_rec_data.awt_group_name,
                            v_rec_data.dist_code_concatinated,
                            v_rec_data.line_description,
                            v_rec_data.act_vendor_num,
                            v_rec_data.act_vendor_site_code,
                            v_rec_data.reporting_entity,
                            v_rec_data.tax_invoice_date,
                            v_rec_data.tax_invoice_num,
                            v_rec_data.tax_acct_period,
                            v_rec_data.wht_acct_period,
                            v_rec_data.phor_ngor_dor,
                            v_rec_data.wht_condition,
                            v_rec_data.wht_revenue_type,
                            v_rec_data.wht_revenue_name,
                            v_rec_data.project_number,
                            v_rec_data.project_task,
                            v_rec_data.expenditure_type,
                            v_rec_data.expenditure_organization,
                            v_rec_data.status,
                            v_date                  --v_rec_data.eai_crtd_dttm
                                  ,
                            v_rec_data.payee,
                            v_rec_data.mail_to_name,
                            v_rec_data.mail_to_address1,
                            v_rec_data.mail_to_address2,
                            v_rec_data.mail_to_address3,
                            v_rec_data.mail_to_address4,
                            v_rec_data.ap_liability_account,
                            v_rec_data.pay_group,
                            v_rec_data.wht_act_supplier_name,
                            v_rec_data.wht_address1,
                            v_rec_data.wht_address2,
                            v_rec_data.wht_supplier_tax_id,
                            v_rec_data.wht_supplier_id_card /*Modify On 10/31/2016 by spw@ice to following.
                                                            - Mapping between Requisition and Invoice.
                                                            - Keep Batch ID in the AP Invoice.
                                                            */
                                                           ,
                            v_rec_data.batch_id,
                            DECODE (v_rec_data.reporting_entity,
                                    '10000', 102,
                                    '10050', 142,
                                    NULL),
                            item.file_name,
                            p_request_id /*----------------End modify--------------------*/
                                        );

                  -- write_out('Line#' || to_char(v_line_count, 0000) || ' [ OK] =>  ' || v_line_data);
                  write_log (
                        'Line#'
                     || TO_CHAR (v_line_count, 0000)
                     || ' [ OK] =>  '
                     || v_line_data);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     v_insert_error_flag := 'Y';
                     write_log (
                           'Line#'
                        || TO_CHAR (v_line_count, 0000)
                        || ' [ERR] =>  '
                        || v_line_data);
                     write_out (
                           'Line#'
                        || TO_CHAR (v_line_count, 0000)
                        || ' [ERR] =>  '
                        || v_rec_data.error_msg);

                     --|| SQLERRM);

                     UPDATE tac_ap_inf_files
                        SET error_message = emesg, load_flag = 'N'
                      WHERE     request_id = p_request_id
                            AND file_name = item.file_name;
               END;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  EXIT;
            END;
         END LOOP;

         -- check open and close
         IF (UTL_FILE.is_open (file => v_file_handle))
         THEN
            UTL_FILE.fclose (file => v_file_handle);
         END IF;
      END LOOP;

      COMMIT;
      RETURN TRUE;
   EXCEPTION
      WHEN OTHERS
      THEN
         -- check open and close
         IF (UTL_FILE.is_open (file => v_file_handle))
         THEN
            UTL_FILE.fclose (file => v_file_handle);
         ELSE
            BEGIN
               ROLLBACK TO SAVEPOINT before_insert_data;
            EXCEPTION
               WHEN OTHERS
               THEN
                  NULL;
            END;
         END IF;

         write_log (
            'tac_ap_interface_util_smt.import_data exception when others .');
         write_log ('Error @import_data : ' || SQLCODE || ' - ' || SQLERRM);
         /*
         move_data_file (
            get_directory_path (g_smt_inbox) || '/' || v_file_name,
            get_directory_path (g_smt_error));
           */
         RETURN FALSE;
   END import_data;


   /*Modify On 10/31/2016 by spw@ice to following.
   - Mapping between Requisition and Invoice.
   - Keep Batch ID in the AP Invoice.
   */
   FUNCTION get_requisition_by_batch_id (p_batch_id IN VARCHAR2)
      RETURN requisition_rec
   IS
      v_rec   requisition_rec;
   BEGIN
      SELECT requisition_header_id, segment1
        INTO v_rec
        FROM PO_REQUISITION_HEADERS_ALL
       WHERE attribute15 = p_batch_id AND ROWNUM = 1;

      RETURN v_rec;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   PROCEDURE insert_to_mapping_table (
      p_rec IN TAC_REQ_MAPPING_INVOICE%ROWTYPE)
   IS
   BEGIN
      INSERT INTO TAC_REQ_MAPPING_INVOICE
           VALUES p_rec;
   END;

   PROCEDURE delete_processed
   IS
   BEGIN
      DELETE FROM dtac_ap_invoices_temp_int
            WHERE source = 'SMT' AND status = 'PROCESSED';
   END;

   FUNCTION listing_file (p_request_id   IN     NUMBER,
                          o_file_rec        OUT tac_ap_inf_file_tbl_type)
      RETURN BOOLEAN
   IS
      l_request_id    NUMBER;
      l_file_name     VARCHAR2 (50);
      in_file         UTL_FILE.file_type;
      l_new_line      VARCHAR2 (1000);
      i               NUMBER := 0;

      l_lines_in      tac_ap_interface_util_smt.tac_ap_inf_file_tbl_type;
      l_line_in_rec   tac_ap_interface_util_smt.tac_ap_inf_file_rec_type;
   BEGIN
      l_file_name := p_request_id || '.txt';

      --write_log ('Create File : ' || l_file_name, 'LOG');
      in_file := UTL_FILE.fopen ('SMT005_FILES', l_file_name, 'r');

      LOOP
         BEGIN
            UTL_FILE.GET_LINE (in_file, l_new_line);
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               EXIT;
         END;

         l_line_in_rec.request_id := p_request_id;
         l_line_in_rec.file_name := TRIM (l_new_line);
         l_line_in_rec.load_flag := 'N';
         l_line_in_rec.load_date := SYSDATE;
         l_line_in_rec.error_message := '';
         --write_log ('List File : ' || l_line_in_rec.file_name, 'LOG');
         l_lines_in (i) := l_line_in_rec;

         INSERT INTO TAC_AP_INF_FILES
              VALUES l_line_in_rec;

         i := i + 1;
      END LOOP;

      COMMIT;
      UTL_FILE.fclose (in_file);
      RETURN TRUE;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN FALSE;
   END;

   PROCEDURE startup_process (err_msg        OUT VARCHAR2,
                              err_code       OUT VARCHAR2,
                              p_org_id    IN     NUMBER,
                              p_user_id   IN     NUMBER)
   IS
      p_conc_request_id     NUMBER := fnd_global.conc_request_id;
      l_request_id          NUMBER;
      l_req_return_status   BOOLEAN;
      l_return              BOOLEAN;

      lc_phase              VARCHAR2 (50);
      lc_status             VARCHAR2 (50);
      lc_dev_phase          VARCHAR2 (50);
      lc_dev_status         VARCHAR2 (50);
      lc_message            VARCHAR2 (50);

      l_files               tac_ap_interface_util_smt.tac_ap_inf_file_tbl_type;
   BEGIN
      l_request_id :=
         fnd_request.submit_request ('SQLAP',
                                     'SMT005_LIST_FILES',
                                     NULL,
                                     NULL,
                                     FALSE,
                                     p_conc_request_id);
      COMMIT;

      LOOP
         l_req_return_status :=
            fnd_concurrent.wait_for_request (request_id   => l_request_id,
                                             interval     => 2,
                                             max_wait     => 0,
                                             phase        => lc_phase,
                                             status       => lc_status,
                                             dev_phase    => lc_dev_phase,
                                             dev_status   => lc_dev_status,
                                             MESSAGE      => lc_message);

         EXIT WHEN    UPPER (lc_phase) = 'COMPLETED'
                   OR UPPER (lc_status) IN
                         ('CANCELLED', 'ERROR', 'TERMINATED');
      END LOOP;

      IF UPPER (lc_phase) = 'COMPLETED' AND UPPER (lc_status) = 'ERROR'
      THEN
         write_log (
               'TAC List file SMT005 completed in error.'
            || l_request_id
            || ' ERROR : '
            || SQLERRM);
      ELSIF UPPER (lc_phase) = 'COMPLETED' AND UPPER (lc_status) = 'NORMAL'
      THEN
         IF (listing_file (p_conc_request_id, l_files))
         THEN
            IF (import_data (p_conc_request_id))
            THEN
               FOR item IN (  SELECT org_id
                                FROM dtac_ap_invoices_temp_int
                               WHERE status = 'NEW' AND source = 'SMT'
                            GROUP BY org_id)
               LOOP
                  interface_ap (item.org_id, p_conc_request_id, 'SMT');
               END LOOP;

               IF (create_logs (p_conc_request_id))
               THEN
                  FOR item IN (  SELECT *
                                   FROM tac_ap_inf_files
                                  WHERE request_id = p_conc_request_id
                               ORDER BY file_name ASC)
                  LOOP
                     write_log ('TAC List file SMT005 completed.');

                     IF (item.error_message IS NOT NULL)
                     THEN
                        write_log (
                           'tac_ap_interface_util_smt.import_data exception when insert data. See output for error detail.');
                        move_data_file (
                              get_directory_path (g_smt_inbox)
                           || '/'
                           || item.file_name,
                           get_directory_path (g_smt_error));
                     ELSE
                        move_data_file (
                              get_directory_path (g_smt_inbox)
                           || '/'
                           || item.file_name,
                           get_directory_path (g_smt_history));
                     END IF;
                  END LOOP;
               --delete_processed;
               END IF;
            END IF;
         END IF;
      END IF;
   END;

   FUNCTION get_error_message_line (p_invoice_id NUMBER)
      RETURN VARCHAR2
   IS
      l_messages   VARCHAR2 (1000) := '';
   BEGIN
      FOR t
         IN (SELECT rej.parent_id,
                    rej.reject_lookup_code,
                    msg.displayed_field
               FROM ap_invoice_lines_interface ail,
                    ap_interface_rejections rej,
                    ap_lookup_codes msg
              WHERE     rej.parent_table = 'AP_INVOICE_LINES_INTERFACE'
                    AND ail.invoice_line_id = rej.parent_id
                    AND rej.reject_lookup_code = msg.lookup_code(+)
                    AND msg.lookup_type(+) = 'REJECT CODE'
                    AND ail.invoice_id = p_invoice_id)
      LOOP
         l_messages := t.displayed_field;      /*Fixed by spw@ice 7-Jul-2017*/
      END LOOP;

      IF (l_messages IS NOT NULL OR l_messages <> '')
      THEN
         RETURN 'Standard Interface Error : ' || l_messages;
      ELSE
         RETURN '';
      END IF;
   END;

   FUNCTION create_logs (p_concurrent_id IN NUMBER)
      RETURN BOOLEAN
   IS
      l_today_timestamp           TIMESTAMP := SYSTIMESTAMP;
      p_end_date                  DATE;
      p_start_date                DATE;
      v_cont_req_id               NUMBER;


      l_program_name              VARCHAR2 (50 BYTE) := 'SMT004';
      l_record_count_completion   NUMBER := 0;
      l_record_count_failed       NUMBER := 0;
      l_record_count_total        NUMBER := 0;

      l_hour                      NUMBER;
      l_minute                    NUMBER;
      l_second                    NUMBER;

      v_process_start             DATE;
      v_process_finish            DATE;
      v_process_time              VARCHAR2 (20);

      CURSOR c_line (
         i_request_id NUMBER)
      IS
         SELECT org.name org_name,
                ven.vendor_name,
                tmp.*,
                DECODE (ad.match_status_flag,
                        'A', 'Validated',
                        'T', 'Needs Revalidation',
                        'N', 'Never Validated',
                        DECODE (ad.invoice_id, NULL, '', 'Never Validated'))
                   invoice_status,
                ai.invoice_date invoice_date_app,
                ai.invoice_amount invoice_amount_app,
                DECODE (tmp.error_flag,
                        'Y', 'Error',
                        DECODE (aif.status, 'REJECTED', 'Error', 'Complete'))
                   process_status,
                DECODE (
                   tmp.error_flag,
                   'Y', tmp.process_message,
                   DECODE (
                      aif.status,
                      'REJECTED',    'Standard Interface Error : '
                                  || msg.displayed_field,
                      NULL))
                   error_message,
                msg.displayed_field,
                DECODE (
                   tmp.error_flag,
                   'Y', 'CUSTOM',
                   DECODE (aif.status, 'REJECTED', 'STANDARD', 'NORMAL'))
                   error_state,
                aif.invoice_id
           FROM dtac_ap_invoices_temp_int tmp,
                hr_operating_units org,
                po_vendors ven,
                ap_invoices_all ai,
                ap_invoice_distributions_all ad,
                (  SELECT af.invoice_num,
                          MAX (af.request_id) request_id,
                          af.org_id,
                          MAX (af.invoice_id) invoice_id,
                          af.GROUP_ID,
                          af.source,
                          MAX (af.status) status
                     FROM ap_invoices_interface af
                 GROUP BY af.invoice_num,
                          af.org_id,
                          af.GROUP_ID,
                          af.source) aif,
                ap_interface_rejections air,
                ap_lookup_codes msg
          WHERE     tmp.request_id = i_request_id
                AND tmp.org_id = org.organization_id(+)
                AND tmp.vendor_num = ven.segment1(+)
                AND tmp.invoice_num = ai.invoice_num(+)
                AND tmp.org_id = ai.org_id(+)
                AND ai.invoice_id = ad.invoice_id(+)
                AND ad.line_type_lookup_code(+) = 'ITEM'
                AND tmp.invoice_num = aif.invoice_num(+)
                AND tmp.inf_request_id = aif.request_id(+)
                AND tmp.org_id = aif.org_id(+)
                AND aif.invoice_id = air.parent_id(+)
                AND tmp.GROUP_ID = aif.GROUP_ID(+)
                AND air.reject_lookup_code = msg.lookup_code(+)
                AND msg.lookup_type(+) = 'REJECT CODE'
                AND air.parent_table(+) = 'AP_INVOICES_INTERFACE'
                AND aif.source(+) = 'SMT';
   BEGIN
      v_process_finish := SYSDATE;

      SELECT actual_start_date
        INTO p_start_date
        FROM FND_CONC_REQ_SUMMARY_V
       WHERE request_id = p_concurrent_id;

      v_process_time :=
         TO_CHAR (
            TO_TIMESTAMP (
               TO_CHAR (
                    TO_DATE ('00:00:00', 'HH24:MI:SS')
                  + (v_process_finish - p_start_date),
                  'HH24:MI:SS'),
               'HH24:MI:SS.FF'),
            'HH24:MI:SS.FF');
      write_log ('Loop create logs.' || TO_CHAR (p_concurrent_id));

      FOR i IN c_line (p_concurrent_id)
      LOOP
         BEGIN
            l_record_count_total := l_record_count_total + 1;

            IF (i.process_status = 'Complete')
            THEN
               l_record_count_completion := l_record_count_completion + 1;
            ELSE
               l_record_count_failed := l_record_count_failed + 1;

               UPDATE tac_ap_inf_files
                  SET error_message = 'ERROR'
                WHERE     request_id = p_concurrent_id
                      AND file_name = i.source_file_name;
            END IF;

            IF (    UPPER (TRIM (i.process_status)) = 'ERROR'
                AND UPPER (TRIM (i.error_state)) = 'STANDARD'
                AND i.displayed_field IS NULL)
            THEN
               i.error_message := get_error_message_line (i.invoice_id);
            END IF;

            INSERT INTO TACSMT_LOG_DETAIL (LEVEL_TYPE,
                                           PROCESS_START,
                                           PROCESS_FINISH,
                                           PROCESSING_TIME,
                                           ERROR_TYPE,
                                           REQUEST_ID,
                                           OPERATING_UNIT,
                                           SUPPLIER_NAME,
                                           SUPPLIER_SITE,
                                           PAYEE_NAME,
                                           SUPPLIER_NUMBER,
                                           ERROR_CODE,
                                           ERROR_MESSAGE,
                                           DATE_FROM,
                                           BATCH_ID,
                                           INVOICE_NUMBER,
                                           INVOICE_STATUS,
                                           INVOICE_DATE,
                                           INVOICE_AMOUNT)
                 VALUES ('D',
                         p_start_date,
                         v_process_finish,
                         v_process_time,
                         'SMTAP Interface',
                         p_concurrent_id,
                         i.org_name,
                         i.vendor_name,
                         i.vendor_site_code,
                         i.payee,
                         i.vendor_num,
                         i.process_status,
                         REPLACE (i.error_message, CHR (10), '|'),
                         SYSDATE,
                         i.batch_id,
                         i.invoice_num,
                         i.invoice_status,
                         i.invoice_date_app,
                         i.invoice_amount_app);
         EXCEPTION
            WHEN OTHERS
            THEN
               RAISE;
         END;
      END LOOP;

      /*
    SELECT SUM (DECODE (stg.error_flag, 'N', 1, 0)),
           SUM (DECODE (stg.error_flag, 'Y', 1, 0)),
           COUNT (*)
      INTO l_record_count_completion,
           l_record_count_failed,
           l_record_count_total
      FROM dtac_ap_invoices_temp_int stg
     WHERE stg.request_id = p_concurrent_id;
      */
      INSERT INTO TACSMT_LOG_SUMMARY
           VALUES (
                     'H',
                     p_concurrent_id,
                     'TACSMT005',
                     p_start_date,
                     TO_DATE (
                        TO_CHAR (l_today_timestamp, 'YYYY-MON-DD HH24:MI:SS'),
                        'YYYY-MON-DD HH24:MI:SS'),
                     'Upload',
                     v_process_time,
                     l_record_count_completion,
                     l_record_count_failed,
                     l_record_count_total);

      COMMIT;

      v_cont_req_id :=
         fnd_request.submit_request (application   => 'SQLAP',
                                     program       => 'TACSMT006',
                                     argument1     => 'SMT005',        ---arg1
                                     argument2     => p_concurrent_id);
      COMMIT;
      RETURN TRUE;
   EXCEPTION
      WHEN OTHERS
      THEN
         RAISE;
   END;
/*----------------End modify--------------------*/
END TAC_AP_INTERFACE_UTIL_SMT_PKG;
/

