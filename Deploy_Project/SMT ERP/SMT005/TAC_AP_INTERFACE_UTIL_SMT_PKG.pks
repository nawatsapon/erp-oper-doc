CREATE OR REPLACE PACKAGE APPS.TAC_AP_INTERFACE_UTIL_SMT_PKG
IS
   -- Added by AP@BAS on 07-Feb-2013
   g_dtn_org_id             NUMBER := 142;
   g_psb_org_id             NUMBER := 218;                --TEST=238, PROD=218
   g_dtw_org_id             NUMBER := 144;
   --
   --   g_smt_inbox     CONSTANT VARCHAR2 (50) := 'SMT_INBOX005';
   g_smt_inbox     CONSTANT VARCHAR2 (50) := 'SMT_INBOX';
   g_smt_history   CONSTANT VARCHAR2 (50) := 'SMT_HISTORY';
   g_smt_error     CONSTANT VARCHAR2 (50) := 'SMT_ERROR';

   g_log_line               VARCHAR2 (250)
      := '+---------------------------------------------------------------------------+';

   TYPE requisition_rec IS RECORD
   (
      REQUISITION_HEADER_ID   NUMBER,
      REQUISITION_NUM         VARCHAR2 (20)
   );

   TYPE tac_ap_inf_file_rec_type IS RECORD
   (
      request_id      TAC_AP_INF_FILES.request_id%TYPE,
      file_name       TAC_AP_INF_FILES.file_name%TYPE,
      load_flag       TAC_AP_INF_FILES.load_flag%TYPE,
      load_date       TAC_AP_INF_FILES.load_date%TYPE,
      error_message   TAC_AP_INF_FILES.load_date%TYPE
   );

   TYPE tac_ap_inf_file_tbl_type IS TABLE OF tac_ap_inf_file_rec_type
                                       INDEX BY BINARY_INTEGER;

   PROCEDURE insert_invoice (ir_invoice IN ap_invoices_interface%ROWTYPE);

   PROCEDURE insert_invoice_line (
      ir_invoice_line IN ap_invoice_lines_interface%ROWTYPE);

   PROCEDURE interface_ap_group_param (err_msg       OUT VARCHAR2,
                                       err_code      OUT VARCHAR2,
                                       i_org_id   IN     NUMBER,
                                       i_batch    IN     VARCHAR2,
                                       i_group    IN     VARCHAR2);

   PROCEDURE update_temp_data (p_source VARCHAR2);

   PROCEDURE interface_ap (i_org_id       IN NUMBER,
                           p_request_id   IN NUMBER,
                           p_source       IN VARCHAR2);

   /*
     import_data is read data from text file and extract to table column
     p_file_path = directory name than reference to physical path
     p_file_name_format = text file name format that relate with date
     p_file_name = optional file name to specific to read and import directly. (process if not null)
   */

   /*
   PROCEDURE import_data (err_msg              OUT VARCHAR2,
                          err_code             OUT VARCHAR2,
                          p_file_path              VARCHAR2,
                          p_file_name_format       VARCHAR2,
                          p_file_name              VARCHAR2 DEFAULT NULL,
                          p_delimiter              VARCHAR2 DEFAULT '|',
                          p_debug_flag             VARCHAR2 DEFAULT 'N');
    */
   FUNCTION import_data (p_request_id IN NUMBER)
      RETURN BOOLEAN;

   /*Modify On 10/31/2016 by spw@ice to following.
   - Mapping between Requisition and Invoice.
   - Keep Batch ID in the AP Invoice.
   */
   FUNCTION get_requisition_by_batch_id (p_batch_id IN VARCHAR2)
      RETURN requisition_rec;

   PROCEDURE insert_to_mapping_table (
      p_rec IN TAC_REQ_MAPPING_INVOICE%ROWTYPE);

   PROCEDURE delete_processed;

   FUNCTION listing_file (p_request_id   IN     NUMBER,
                          o_file_rec        OUT tac_ap_inf_file_tbl_type)
      RETURN BOOLEAN;

   PROCEDURE startup_process (err_msg        OUT VARCHAR2,
                              err_code       OUT VARCHAR2,
                              p_org_id    IN     NUMBER,
                              p_user_id   IN     NUMBER);

   FUNCTION create_logs (p_concurrent_id IN NUMBER)
      RETURN BOOLEAN;
/*----------------End modify--------------------*/
END TAC_AP_INTERFACE_UTIL_SMT_PKG;
/

