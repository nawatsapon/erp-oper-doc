CREATE OR REPLACE PACKAGE APPS.TAC_AP_INTERFACE_DUMMY_SMT
IS
   -- Added by spw@ice on 1-Dec-2017
   g_dtn_org_id             NUMBER := 142;
   g_psb_org_id             NUMBER := 218;                --TEST=238, PROD=218
   g_dtw_org_id             NUMBER := 144;
   --
   g_smt_inbox     CONSTANT VARCHAR2 (50) := 'SMT_INBOX';
   g_smt_history   CONSTANT VARCHAR2 (50) := 'SMT_HISTORY';
   g_smt_error     CONSTANT VARCHAR2 (50) := 'SMT_ERROR';

   g_log_line               VARCHAR2 (250)
      := '+---------------------------------------------------------------------------+';

   PROCEDURE insert_invoice (ir_invoice IN ap_invoices_interface%ROWTYPE);

   PROCEDURE insert_invoice_line (
      ir_invoice_line IN ap_invoice_lines_interface%ROWTYPE);

   FUNCTION interface_ap (p_org_id_opt IN VARCHAR2, p_request_id IN NUMBER)
      RETURN BOOLEAN;

   PROCEDURE submit_request (p_org_id_opt   IN VARCHAR2,
                             p_request_id   IN NUMBER);

   PROCEDURE create_logs (errbuf OUT VARCHAR2, errcode OUT VARCHAR2);

   PROCEDURE write_log (buff VARCHAR2, which VARCHAR2);

   FUNCTION get_tax_amount (p_invoice_amount   IN NUMBER,
                            p_tax_name         IN VARCHAR2,
                            p_org_id           IN NUMBER)
      RETURN NUMBER;

   FUNCTION get_batch_from_req (p_po_number IN VARCHAR2)
      RETURN VARCHAR2;
END TAC_AP_INTERFACE_DUMMY_SMT;
/

