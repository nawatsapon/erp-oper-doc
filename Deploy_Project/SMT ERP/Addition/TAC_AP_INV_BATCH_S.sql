DROP SEQUENCE APPS.TAC_AP_INV_BATCH_S;

--
-- TAC_AP_INV_BATCH_S  (Sequence) 
--
CREATE SEQUENCE APPS.TAC_AP_INV_BATCH_S
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 5
  NOORDER;
