CREATE OR REPLACE PACKAGE BODY APPS.TAC_AP_INTERFACE_DUMMY_SMT
IS
   ----------------------------------------------------------------------------------------------------
   PROCEDURE write_log (param_msg VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.LOG, param_msg);
   END write_log;


   PROCEDURE insert_invoice (ir_invoice IN ap_invoices_interface%ROWTYPE)
   IS
   BEGIN
      INSERT INTO ap_invoices_interface
           VALUES ir_invoice;
   END insert_invoice;

   ------------------------------------------------------------------------------
   PROCEDURE insert_invoice_line (
      ir_invoice_line IN ap_invoice_lines_interface%ROWTYPE)
   IS
   BEGIN
      INSERT INTO ap_invoice_lines_interface
           VALUES ir_invoice_line;
   END insert_invoice_line;

   FUNCTION interface_ap (p_org_id_opt IN VARCHAR2, p_request_id IN NUMBER)
      RETURN BOOLEAN
   IS
      v_debug_step                   VARCHAR2 (100);
      v_mapped_org_id_acc_code       VARCHAR2 (10);

      CURSOR c_inv (
         i_org_optional VARCHAR2)
      IS
           SELECT v.vendor_id,
                  v.segment1 vendor_num,
                  'SMT' source,
                  poh.po_header_id,
                  poh.segment1 po_number,
                  vs.payment_method_lookup_code,
                  poh.org_id,
                  'STANDARD' invoice_type_lookup_code,
                  vs.vendor_site_code,
                  SYSDATE invoice_date,
                  (  SELECT prh.segment1
                       FROM po_distributions_all pda,
                            po_req_distributions_all prd,
                            po_requisition_lines_all prl,
                            po_requisition_headers_all prh
                      WHERE     pda.req_distribution_id = prd.distribution_id
                            AND prd.requisition_line_id = prl.requisition_line_id
                            AND prl.requisition_header_id =
                                   prh.requisition_header_id
                            AND pda.po_header_id = poh.po_header_id
                   GROUP BY prh.segment1)
                     invoice_num,
                  SYSDATE gl_date,
                  DECODE (poh.org_id,
                          102, 'INIR Dummy',
                          142, 'INIR_DTN-Dummy')
                     document_category_name,
                  poh.currency_code,
                  (  SELECT SUM (
                                 (  NVL (pll.quantity, 0)
                                  - NVL (pll.quantity_cancelled, 0))
                               * NVL (pol.unit_price, 0))
                               po_total
                       FROM po_lines_all pol, po_line_locations_all pll
                      WHERE     pol.po_header_id = pll.po_header_id
                            AND pol.po_line_id = pll.po_line_id
                            AND NVL (pol.cancel_flag, 'N') = 'N'
                            AND pol.closed_code = 'OPEN'
                            AND pll.quantity_billed = 0
                            AND pol.po_header_id = poh.po_header_id
                   GROUP BY pol.po_header_id)
                     po_total,
                  (SELECT atc.name
                     FROM po_line_locations_all pll, ap_tax_codes_all atc
                    WHERE     pll.tax_code_id = atc.tax_id(+)
                          AND pll.po_header_id = poh.po_header_id
                          AND ROWNUM = 1)
                     tax_code
             FROM po_headers_all poh, po_vendors v, po_vendor_sites_all vs
            WHERE     v.vendor_id(+) = poh.vendor_id
                  AND v.vendor_name = 'SMT Dummy Supplier'
                  AND poh.authorization_status = 'APPROVED'
                  AND poh.cancel_flag IS NULL
                  AND poh.closed_code = 'OPEN'
                  AND vs.vendor_site_id(+) = poh.vendor_site_id
                  AND (SELECT COUNT (*)
                         FROM po_lines_all pol, po_line_locations_all pll
                        WHERE     pol.po_header_id = pll.po_header_id
                              AND pol.po_line_id = pll.po_line_id
                              AND NVL (pol.cancel_flag, 'N') = 'N'
                              AND pol.closed_code = 'OPEN'
                              AND pll.quantity_billed = 0
                              AND pol.po_header_id = poh.po_header_id) > 0
         --                  AND ROWNUM < 2
         ORDER BY poh.creation_date ASC;

      CURSOR c_line (
         i_po_header_id IN NUMBER)
      IS
           SELECT pol.po_header_id,
                  pol.po_line_id,
                  pol.line_num,
                  pll.line_location_id,
                  pll.shipment_num,
                  pda.po_distribution_id,
                  pda.distribution_num,
                  'ITEM' line_type_lookup_code,
                    (NVL (pll.quantity, 0) - NVL (pll.quantity_cancelled, 0))
                  * NVL (pol.unit_price, 0)
                     amount,
                  atc.name tax_name,
                  pol.item_description,
                  pda.code_combination_id,
                  pll.quantity,
                  pol.unit_price,
                  pol.closed_code,
                  pll.quantity_billed,
                  pol.cancel_flag,
                  pll.match_option
             FROM po_lines_all pol,
                  po_line_locations_all pll,
                  po_distributions_all pda,
                  ap_tax_codes_all atc
            WHERE     pol.po_header_id = pll.po_header_id
                  AND pol.po_line_id = pll.po_line_id
                  AND pol.po_header_id = pda.po_header_id
                  AND pol.po_line_id = pda.po_line_id
                  AND pll.line_location_id = pda.line_location_id
                  AND NVL (pol.cancel_flag, 'N') = 'N'
                  AND pol.closed_code = 'OPEN'
                  AND pll.quantity_billed = 0
                  AND pll.tax_code_id = atc.tax_id
                  AND pol.po_header_id = i_po_header_id
         ORDER BY pol.line_num ASC;

      r_invoice                      ap_invoices_interface%ROWTYPE;
      r_invoice_line                 ap_invoice_lines_interface%ROWTYPE;
      v_group                        VARCHAR2 (80);

      v_batch                        VARCHAR2 (80);

      v_rate                         NUMBER;
      v_tax_ccid                     NUMBER;
      v_line_num                     NUMBER := 0;
      v_site_auto_calc_flag          VARCHAR2 (10);
      v_supp_name                    VARCHAR2 (100);
      v_period                       VARCHAR2 (30);
      v_task_id                      NUMBER (20);
      v_expen_type                   VARCHAR (100);
      v_expen_org                    NUMBER (20);
      v_error_flag                   BOOLEAN := FALSE;
      v_chk_line                     BOOLEAN := FALSE;
      v_validate_error               BOOLEAN := FALSE;
      ap_req_id                      NUMBER;
      vd_req_id                      NUMBER;

      c_ap_req_id                    NUMBER;
      v_i_group                      VARCHAR2 (80);
      v_ap_req_id                    NUMBER;
      v_site                         po_vendor_sites_all%ROWTYPE;
      v_save_pnt                     VARCHAR2 (100);
      v_tax_rate                     NUMBER;
      v_requester_id                 NUMBER;
      v_liability_account_id         NUMBER;
      v_prepay_code_combination_id   NUMBER;
      v_terms_id                     NUMBER;
      v_payment_method_lookup_code   NUMBER;

      v_gl_set_of_books_id           NUMBER
         := TO_NUMBER (fnd_profile.VALUE ('GL_SET_OF_BKS_ID'));
      v_chart_of_accounts_id         NUMBER;

      v_line_status_info             VARCHAR2 (32000);
      v_header_error                 VARCHAR2 (32000);

      v_count_invalid_header         NUMBER := 0;

      v_err_msg                      VARCHAR2 (32000);
      v_err_code                     VARCHAR2 (32000);

      r_po_match_invoice             tac_po_match_invoice_smt%ROWTYPE;

      v_invoice_count                NUMBER;

      v_tax_amount                   NUMBER;

      v_batch_running                NUMBER;
   BEGIN
      v_batch_running := TAC_AP_INV_BATCH_S.NEXTVAL;

      FOR r_inv IN c_inv (p_org_id_opt)
      LOOP
         v_err_msg := '';
         r_po_match_invoice := NULL;
         v_invoice_count := 0;
         v_save_pnt := r_inv.invoice_num || r_inv.vendor_num;
         SAVEPOINT v_save_pnt;

         r_invoice := NULL;
         v_error_flag := FALSE;

         /** AP_INVOICES_INTERFACE **/
         --SEQ
         SELECT ap_invoices_interface_s.NEXTVAL
           INTO r_invoice.invoice_id
           FROM DUAL;

         --Supplier
         r_invoice.vendor_id := r_inv.vendor_id;


         --Site---
         BEGIN
            SELECT vs.*
              INTO v_site
              FROM po_vendor_sites_all vs
             WHERE     TRIM (vs.vendor_site_code) = (r_inv.vendor_site_code)
                   AND vs.vendor_id = r_invoice.vendor_id
                   AND vs.org_id = r_inv.org_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_error_flag := TRUE;

               write_log (
                  '*Invalid Supplier Site : ' || r_inv.vendor_site_code);
               write_log ('*Supplier Number : ' || r_inv.vendor_num);
               v_err_msg := '*** DATA ERROR : Interface data is invalid!';
               v_err_code := '1';
               v_site := NULL;
            WHEN OTHERS
            THEN
               v_error_flag := TRUE;
               write_log (
                  '*Validate Supplier Site : ' || r_inv.vendor_site_code);
               v_err_msg := 'Error : ' || SQLERRM;
               v_err_code := '1';
         END;

         BEGIN
            SELECT p.person_id
              INTO v_requester_id
              FROM per_all_people_f p, fnd_user u
             WHERE     p.person_id = u.employee_id
                   AND p.last_name = 'SMT Dummy Buyer'
                   AND NVL (p.effective_end_date, SYSDATE + 1) >= SYSDATE;
         EXCEPTION
            WHEN OTHERS
            THEN
               v_error_flag := TRUE;
               write_log ('*Invalid Requester.');
               v_err_msg := 'Error : ' || SQLERRM;
               v_err_code := '1';
         END;

         r_invoice.vendor_site_id := v_site.vendor_site_id;
         r_invoice.accts_pay_code_combination_id :=
            v_site.accts_pay_code_combination_id;
         v_site_auto_calc_flag := v_site.auto_tax_calc_flag;

         -- get chart of account
         BEGIN
            SELECT sob.chart_of_accounts_id
              INTO v_chart_of_accounts_id
              FROM gl_sets_of_books sob
             WHERE sob.set_of_books_id = v_gl_set_of_books_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               v_error_flag := TRUE;
               write_log (
                     '*Invalid Chart of Account that relate with SOB : '
                  || v_gl_set_of_books_id);
               v_err_msg := '*** DATA ERROR : Interface data is invalid!';
               v_err_code := '1';
         END;


         -- Validate  ap_liability_account
         BEGIN
            SELECT accts_pay_code_combination_id,
                   prepay_code_combination_id,
                   terms_id,
                   payment_method_lookup_code
              INTO v_liability_account_id,
                   v_prepay_code_combination_id,
                   v_terms_id,
                   v_payment_method_lookup_code
              FROM po_vendor_sites_all
             WHERE     vendor_id = r_inv.vendor_id
                   AND vendor_site_code = r_inv.vendor_site_code
                   AND org_id = r_inv.org_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               v_error_flag := TRUE;
               v_err_msg := '*** DATA ERROR : Interface data is invalid!';
               v_err_code := '1';
         END;

         --          Validate  duplicate invoice number
         BEGIN
            SELECT COUNT (*)
              INTO v_invoice_count
              FROM ap_invoices_all
             WHERE invoice_num LIKE TRIM ('SMT' || r_inv.invoice_num) || '%';
         EXCEPTION
            WHEN OTHERS
            THEN
               v_error_flag := TRUE;
               v_err_msg := '*** DATA ERROR : Interface data is invalid!';
               v_err_code := '1';
         END;



         r_invoice.org_id := r_inv.org_id;

         ---
         v_debug_step := 'Assign r_invoice value';


         IF (v_invoice_count > 0)
         THEN
            r_invoice.invoice_num :=
               'SMT' || r_inv.invoice_num || '-' || TO_CHAR (v_invoice_count);
         ELSE
            r_invoice.invoice_num := 'SMT' || r_inv.invoice_num;
         END IF;

         r_invoice.invoice_type_lookup_code := r_inv.invoice_type_lookup_code; --'STANDARD';
         r_invoice.invoice_date := r_inv.invoice_date;
         r_invoice.vendor_num := r_inv.vendor_num;
         r_invoice.po_number := r_inv.po_number;
         r_invoice.gl_date := TRUNC (r_inv.gl_date);
         r_invoice.payment_method_lookup_code := v_payment_method_lookup_code;
         r_invoice.vendor_site_code := r_inv.vendor_site_code;

         v_tax_amount :=
            get_tax_amount (r_inv.po_total, r_inv.tax_code, r_inv.org_id);

         --v_tax_amount := 0;
         r_invoice.invoice_amount := ROUND (r_inv.po_total + v_tax_amount, 2);
         r_invoice.invoice_currency_code := r_inv.currency_code;
         r_invoice.source := r_inv.source;
         r_invoice.GROUP_ID :=
               'SMT'
            || TO_CHAR (SYSDATE, 'DDMMYYYY')
            || TO_CHAR (r_inv.org_id)
            || TO_CHAR (v_batch_running);
         v_group := r_invoice.GROUP_ID;
         r_invoice.doc_category_code := r_inv.document_category_name;
         r_invoice.terms_id := v_terms_id;
         r_invoice.creation_date := SYSDATE;
         r_invoice.last_update_date := SYSDATE;
         r_invoice.created_by := fnd_global.user_id;
         r_invoice.last_updated_by := fnd_global.user_id;
         r_invoice.request_id := fnd_global.conc_request_id;
         r_invoice.attribute_category := 'JA.TH.APXINWKB.DISTRIBUTIONS';
         r_invoice.global_attribute_category := 'JA.TH.APXINWKB.DISTRIBUTIONS';
         r_invoice.requester_id := v_requester_id;
         r_invoice.accts_pay_code_combination_id := v_liability_account_id;

         --r_invoice.pay_group_lookup_code := r_inv.pay_group;
         write_log ('po_header_id:1 ' || r_inv.po_header_id);

         --Get to logs
         r_po_match_invoice.operating_unit := r_inv.org_id;
         r_po_match_invoice.po_header_id := r_inv.po_header_id;
         r_po_match_invoice.po_number := r_inv.po_number;
         r_po_match_invoice.approval_status :=
            po_headers_sv3.get_po_status (r_inv.po_header_id);
         r_po_match_invoice.invoice_date := r_inv.invoice_date;
         r_po_match_invoice.invoice_num := r_invoice.invoice_num;
         r_po_match_invoice.vendor_id := r_inv.vendor_id;
         r_po_match_invoice.vendor_num := r_inv.vendor_num;
         r_po_match_invoice.vendor_site_code := r_inv.vendor_site_code;
         r_po_match_invoice.invoice_amount := r_invoice.invoice_amount;

         FOR r_line IN c_line (r_inv.po_header_id)
         LOOP
            v_line_num := v_line_num + 1;
            r_invoice_line := NULL;
            v_chk_line := FALSE;

            v_line_status_info :=
               'Row#' || v_line_num || ' |Invoice line#' || r_line.line_num;

            SELECT ap_invoice_lines_interface_s.NEXTVAL
              INTO r_invoice_line.invoice_line_id
              FROM DUAL;

            r_invoice_line.amount := r_line.amount;

            IF r_line.tax_name IS NOT NULL
            THEN
               BEGIN
                  SELECT atc.tax_code_combination_id,
                         atc.tax_id,
                         atc.tax_rate
                    INTO v_tax_ccid, r_invoice_line.tax_code_id, v_rate
                    FROM ap_tax_codes_all atc
                   WHERE     atc.enabled_flag = 'Y'
                         AND atc.name = r_line.tax_name
                         AND NVL (atc.start_date, SYSDATE) <= SYSDATE
                         AND NVL (atc.inactive_date, SYSDATE) >= SYSDATE
                         AND atc.org_id = r_invoice.org_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     v_chk_line := TRUE;
                     v_line_status_info :=
                           v_line_status_info
                        || '|Invalid Tax Code ('
                        || r_line.tax_name
                        || ')';
                     v_err_msg :=
                        '*** DATA ERROR : Interface data is invalid!';
                     v_err_code := '1';
               END;

               r_invoice_line.tax_code := r_line.tax_name;
               r_invoice_line.tax_code_override_flag := 'N';
               r_invoice_line.tax_recoverable_flag := 'Y';
               r_invoice_line.taxable_flag := 'Y';
            ELSE
               r_invoice_line.tax_code := NULL;
               r_invoice_line.tax_recovery_rate := NULL;
            END IF;

            v_supp_name := NULL;

            r_invoice_line.invoice_id := r_invoice.invoice_id;
            r_invoice_line.org_id := r_invoice.org_id;
            r_invoice_line.line_number := v_line_num;
            r_invoice_line.accounting_date := r_inv.gl_date;
            r_invoice_line.task_id := v_task_id; --r_line.task_id; ---iCE-Tae add
            r_invoice_line.expenditure_item_date := SYSDATE;    ---iCE-Tae add
            r_invoice_line.line_type_lookup_code :=
               r_line.line_type_lookup_code;                         --'ITEM';

            r_invoice_line.description := r_line.item_description;


            --r_invoice_line.dist_code_combination_id :=
            --   r_line.code_combination_id;
            --r_invoice_line.awt_group_name := r_line.awt_group_name;
            --r_invoice_line.attribute4 := r_line.act_vendor_num;
            --r_invoice_line.attribute5 := r_line.act_vendor_site_code;
            r_invoice_line.global_attribute_category :=
               'JA.TH.APXINWKB.DISTRIBUTIONS';

            IF r_invoice_line.global_attribute16 IS NULL
            THEN
               r_invoice_line.global_attribute16 := r_inv.source;
            END IF;

            r_invoice_line.attribute_category :=
               'JA.TH.APXINWKB.DISTRIBUTIONS';

            r_invoice_line.created_by := fnd_global.user_id;
            r_invoice_line.creation_date := SYSDATE;

            write_log ('po_header_id:2 ' || r_inv.po_header_id);

            --Get to logs
            r_po_match_invoice.po_line_number := r_line.line_num;
            r_po_match_invoice.quantity := r_line.quantity;
            r_po_match_invoice.unit_price := r_line.unit_price;
            r_po_match_invoice.line_amount := r_line.amount;
            r_po_match_invoice.closed_code := r_line.closed_code;
            r_po_match_invoice.quantity_billed := r_line.quantity_billed;
            r_po_match_invoice.cancel_flag := r_line.cancel_flag;
            r_po_match_invoice.match_option := r_line.match_option;
            r_po_match_invoice.tax_code := r_line.tax_name;
            r_po_match_invoice.payment_method_lookup_code :=
               r_inv.payment_method_lookup_code;
            r_po_match_invoice.gl_date := r_inv.gl_date;
            r_po_match_invoice.document_category_name :=
               r_inv.document_category_name;
            r_po_match_invoice.currency_code := r_inv.currency_code;
            r_po_match_invoice.error_flag := 'N';
            r_po_match_invoice.error_message := null;-- v_err_msg;
            r_po_match_invoice.request_id := p_request_id;
            r_po_match_invoice.batch_name := r_invoice.GROUP_ID;

            IF v_chk_line = FALSE
            THEN
               write_log ('po_header_id:3 ' || r_inv.po_header_id);
               insert_invoice_line (ir_invoice_line => r_invoice_line); -- ITEM
            END IF;

            INSERT INTO tac_po_match_invoice_smt
                 VALUES r_po_match_invoice;

            write_log (v_line_status_info);
         END LOOP;                                                      --Line


         IF v_chk_line = FALSE
         THEN
            insert_invoice (ir_invoice => r_invoice);
            write_log ('Invoice ID: ' || r_invoice.invoice_id);
         END IF;
      END LOOP;                                                      -- Header

      --v_i_group := v_group;
      --v_ap_req_id := ap_req_id;


      COMMIT;
      RETURN TRUE;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN FALSE;
         write_log ('tac_ap_interface_util_smt.interface_ap exception .');
         write_log ('Error MSG: ' || SQLCODE || ' - ' || SQLERRM);
         write_log ('Last step process = ' || v_debug_step);
   END interface_ap;

   PROCEDURE submit_request (p_org_id_opt   IN VARCHAR2,
                             p_request_id   IN NUMBER)
   IS
      v_source              VARCHAR2 (80) := 'SMT';
      ap_req_id             NUMBER;
      vd_req_id             NUMBER;

      lv_request_id         NUMBER;
      lc_phase              VARCHAR2 (50);
      lc_status             VARCHAR2 (50);
      lc_dev_phase          VARCHAR2 (50);
      lc_dev_status         VARCHAR2 (50);
      lc_message            VARCHAR2 (50);
      l_req_return_status   BOOLEAN;

      l_start_date          DATE;
   BEGIN
      BEGIN
         l_start_date := SYSDATE;


         IF (interface_ap (p_org_id_opt, p_request_id))
         THEN
            FOR item
               IN (  SELECT DECODE (operating_unit,  102, 50282,  142, 50501)
                               resp_id,
                            operating_unit,
                            batch_name
                       FROM tac_po_match_invoice_smt
                      WHERE request_id = p_request_id
                   GROUP BY operating_unit, batch_name)
            LOOP
               write_log ('Loop Concurrent.' || item.resp_id, 'LOG');
               fnd_global.apps_initialize (
                  fnd_profile.VALUE ('USER_ID'),
                  item.resp_id,
                  fnd_profile.VALUE ('RESP_APPL_ID'),
                  NULL,
                  NULL);

               ap_req_id :=
                  fnd_request.submit_request (
                     application   => 'SQLAP',
                     program       => 'APXIIMPT',
                     argument1     => v_source,
                     argument2     => item.batch_name,              --v_group,
                     argument3     => NULL,                         --v_batch,
                     argument4     => NULL,
                     argument5     => NULL,
                     argument6     => NULL,
                     argument7     => 'Y');
               COMMIT;

               IF ap_req_id = 0
               THEN
                  write_log (
                        'Request Not Submitted due to "'
                     || fnd_message.GET
                     || '".');
               ELSE
                  write_log (
                        'Program submitted successfully Request id :'
                     || ap_req_id);
               END IF;

               IF ap_req_id > 0
               THEN
                  LOOP
                     --
                     --To make process execution to wait for 1st program to complete
                     --
                     l_req_return_status :=
                        fnd_concurrent.wait_for_request (
                           request_id   => ap_req_id,
                           INTERVAL     => 2,
                           max_wait     => 60,
                           phase        => lc_phase,
                           STATUS       => lc_status,
                           dev_phase    => lc_dev_phase,
                           dev_status   => lc_dev_status,
                           MESSAGE      => lc_message);
                     EXIT WHEN    UPPER (lc_phase) = 'COMPLETED'
                               OR UPPER (lc_status) IN
                                     ('CANCELLED', 'ERROR', 'TERMINATED');
                  END LOOP;

                  IF     UPPER (lc_phase) = 'COMPLETED'
                     AND UPPER (lc_status) = 'ERROR'
                  THEN
                     write_log (
                           'Program completed in error. Oracle request id: '
                        || ap_req_id
                        || ' '
                        || SQLERRM);
                  ELSIF     UPPER (lc_phase) = 'COMPLETED'
                        AND UPPER (lc_status) = 'NORMAL'
                  THEN
                     FOR i
                        IN (SELECT *
                              FROM tac_po_match_invoice_smt
                             WHERE     request_id = p_request_id
                                   AND operating_unit = item.operating_unit)
                     LOOP
                        UPDATE ap_invoices_all
                           SET vat_code = i.tax_code
                         WHERE invoice_num = i.invoice_num;
                     END LOOP;

                     write_log (
                           'Program request successful for request id: '
                        || ap_req_id);
                  END IF;
               END IF;
            END LOOP;

            /*Invoice validation
            FOR item
               IN (  SELECT DECODE (operating_unit,  102, 50282,  142, 50501)
                               resp_id,
                            operating_unit,
                            batch_name
                       FROM tac_po_match_invoice_smt
                      WHERE request_id = p_request_id
                   GROUP BY operating_unit, batch_name)
            LOOP
               fnd_global.apps_initialize (
                  fnd_profile.VALUE ('USER_ID'),
                  item.resp_id,
                  fnd_profile.VALUE ('RESP_APPL_ID'),
                  NULL,
                  NULL);

               vd_req_id :=
                  apps.fnd_request.submit_request (
                     application   => 'SQLAP',
                     program       => 'APPRVL',
                     argument1     => 'All',
                     argument2     => NULL,                         --v_group,
                     argument3     => NULL,                         --v_batch,
                     argument4     => NULL,
                     argument5     => 584567,             --SMT Dummy Supplier
                     argument6     => NULL,
                     argument7     => NULL,
                     argument8     => NULL,
                     argument9     => 1001,
                     argument10    => 'N',
                     argument11    => item.operating_unit,
                     argument12    => 1000);
               COMMIT;
            END LOOP;
            */
         END IF;
      END;
   END;

   PROCEDURE create_logs (errbuf OUT VARCHAR2, errcode OUT VARCHAR2)
   IS
      l_today_timestamp           TIMESTAMP := SYSTIMESTAMP;
      p_end_date                  DATE;

      l_program_name              VARCHAR2 (50 BYTE) := 'TACSMT007';
      l_record_count_completion   NUMBER;
      l_record_count_failed       NUMBER;
      l_record_count_total        NUMBER;

      l_hour                      NUMBER;
      l_minute                    NUMBER;
      l_second                    NUMBER;

      l_pr_number                 VARCHAR2 (50 BYTE);
      l_pr_line                   NUMBER;
      l_pr_status                 VARCHAR2 (50 BYTE);
      l_po_number                 VARCHAR2 (50 BYTE);
      l_account                   VARCHAR2 (100 BYTE);

      v_process_start             DATE;
      v_process_finish            DATE;
      v_process_time              VARCHAR2 (20);

      l_request_id                NUMBER;
      l_imp_request_id            NUMBER;
      l_req_return_status         BOOLEAN;

      lc_phase                    VARCHAR2 (50);
      lc_status                   VARCHAR2 (50);
      lc_dev_phase                VARCHAR2 (50);
      lc_dev_status               VARCHAR2 (50);
      lc_message                  VARCHAR2 (50);
      
      /*
      CURSOR c_line (
         i_request_id NUMBER)
      IS
         SELECT s.request_id,
                hr.name operating_unit,
                s.invoice_num,
                s.quantity * s.unit_price invoice_amount,
                DECODE (ad.match_status_flag,
                        'A', 'Validated',
                        'T', 'Needs Revalidation',
                        'N', 'Never Validated', 'Never Validated')
                   invoice_status,
                ai.invoice_date,
                v.vendor_name,
                vs.vendor_site_code vendor_site,
                s.po_number,
                s.batch_name,
                DECODE (s.error_flag, 'Y', 'Error', 'Completed') status,
                DECODE (s.error_flag, 'Y', s.error_message, '') ERROR_CODE
           FROM tac_po_match_invoice_smt s,
                hr_all_organization_units hr,
                ap_invoices_all ai,
                ap_invoice_distributions_all ad,
                po_vendors v,
                po_vendor_sites_all vs
          WHERE     s.operating_unit = hr.organization_id
                AND hr.TYPE = 'OPERATION UNIT'
                AND s.invoice_num = ai.invoice_num(+)
                AND ai.invoice_id = ad.invoice_id(+)
                AND ad.line_type_lookup_code = 'ITEM'
                AND NVL (ad.cancellation_flag, 'N') = 'N'
                AND ai.vendor_id = v.vendor_id
                AND v.vendor_id = vs.vendor_id
                AND ai.org_id = vs.org_id
                AND s.request_id = i_request_id;
                */
      CURSOR c_line (
         i_request_id NUMBER)
      IS                
         SELECT s.request_id,
                hr.name operating_unit,
                s.invoice_num,
                s.quantity * s.unit_price invoice_amount,
                DECODE (ad.match_status_flag,
                        'A', 'Validated',
                        'T', 'Needs Revalidation',
                        'N', 'Never Validated', 'Never Validated')
                   invoice_status,
                ai.invoice_date,
                v.vendor_name,
                vs.vendor_site_code vendor_site,
                s.po_number,
                s.batch_name,
                --DECODE (s.error_flag, 'Y', 'Error', 'Completed') status,
                --DECODE (s.error_flag, 'Y', s.error_message, '') ERROR_CODE,
                DECODE (
                   s.error_flag,
                   'Y', 'CUSTOM',
                   DECODE (aif.status, 'REJECTED', 'STANDARD', 'NORMAL'))
                   error_stat,
                DECODE (
                   s.error_flag,
                   'Y', 'Error',
                   DECODE (aif.status, 'REJECTED', 'Error', 'Completed'))
                   status,                   
                DECODE (
                   s.error_flag,
                   'Y', s.error_message,
                   DECODE (
                      aif.status,
                      'REJECTED',    'Standard Interface Error : '
                                  || msg.displayed_field,
                      NULL))
                   ERROR_CODE
           FROM tac_po_match_invoice_smt s,
                hr_all_organization_units hr,
                ap_invoices_all ai,
                ap_invoice_distributions_all ad,
                po_vendors v,
                po_vendor_sites_all vs,
                ap_invoices_interface aif,
                ap_interface_rejections air,
                ap_lookup_codes msg
          WHERE     s.operating_unit = hr.organization_id
                AND hr.TYPE = 'OPERATION UNIT'
                AND s.invoice_num = ai.invoice_num(+)
                AND ai.invoice_id = ad.invoice_id(+)
                AND ad.line_type_lookup_code = 'ITEM'
                AND NVL (ad.cancellation_flag, 'N') = 'N'
                AND ai.vendor_id = v.vendor_id
                AND v.vendor_id = vs.vendor_id
                AND ai.org_id = vs.org_id
                
                AND ai.invoice_num = aif.invoice_num(+)
                AND ai.org_id = aif.org_id(+)                
                AND aif.invoice_id = air.parent_id(+)
                AND air.reject_lookup_code = msg.lookup_code(+)
                AND msg.lookup_type(+) = 'REJECT CODE'
                AND air.parent_table(+) = 'AP_INVOICES_INTERFACE'
                                
                AND s.request_id = i_request_id;                

      v_request_id                NUMBER;
      v_start_date                DATE;
   BEGIN
      SELECT MAX (request_id) INTO v_request_id FROM tac_po_match_invoice_smt;

      SELECT actual_start_date
        INTO v_start_date
        FROM FND_CONC_REQ_SUMMARY_V
       WHERE request_id = v_request_id;

      v_process_finish := SYSDATE;
      v_process_time :=
         TO_CHAR (
            TO_TIMESTAMP (
               TO_CHAR (
                    TO_DATE ('00:00:00', 'HH24:MI:SS')
                  + (v_process_finish - v_start_date),
                  'HH24:MI:SS'),
               'HH24:MI:SS.FF'),
            'HH24:MI:SS.FF');

      FOR i IN c_line (v_request_id)
      LOOP
         BEGIN
            INSERT INTO TACSMT_LOG_DETAIL (level_type,
                                           process_start,
                                           process_finish,
                                           processing_time,
                                           request_id,
                                           ERROR_TYPE,
                                           batch_id,
                                           operating_unit,
                                           pr_number,
                                           pr_line,
                                           pr_status,
                                           po_number,
                                           purchase_categories,
                                           line_desc,
                                           quantity,
                                           uom,
                                           unit_price,
                                           supplier_name,
                                           supplier_site,
                                           account,
                                           requester,
                                           date_from,
                                           date_to,
                                           invoice_number,
                                           invoice_amount,
                                           invoice_status,
                                           invoice_date,
                                           record_complete,
                                           record_failed,
                                           record_count,
                                           record_flag,
                                           supplier_number,
                                           ERROR_CODE,
                                           error_message)
                 VALUES (
                           'D',
                           v_start_date,
                           TO_DATE (
                              TO_CHAR (l_today_timestamp,
                                       'YYYY-MON-DD HH24:MI:SS'),
                              'YYYY-MON-DD HH24:MI:SS'),
                           v_process_time,
                           i.request_id,
                           'SMTPOAP Status',
                           get_batch_from_req(i.po_number),
                           i.operating_unit,
                           NULL,
                           NULL,
                           NULL,
                           i.po_number,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           i.vendor_name,
                           i.vendor_site,
                           l_account,
                           NULL,
                           v_start_date,
                           p_end_date,
                           i.invoice_num,
                           i.invoice_amount,
                           i.invoice_status,
                           i.invoice_date,
                           DECODE (i.status, 'Error', 0, 1),
                           DECODE (i.status, 'Error', 1, 0),
                           1,
                           NULL,
                           NULL,
                           i.status,
                           i.ERROR_CODE);
         EXCEPTION
            WHEN OTHERS
            THEN
               RAISE;
         END;
      END LOOP;

      SELECT SUM (DECODE (stg.error_flag, 'Y', 0, 1)) record_count_completion,
             SUM (DECODE (stg.error_flag, 'N', 0, 1)) record_count_failed,
             COUNT (*) record_count_total
        INTO l_record_count_completion,
             l_record_count_failed,
             l_record_count_total
        FROM tac_po_match_invoice_smt stg
       WHERE stg.request_id = v_request_id;

      INSERT INTO TACSMT_LOG_SUMMARY (level_type,
                                      request_id,
                                      program_name,
                                      process_start,
                                      process_finish,
                                      processing_type,
                                      processing_time,
                                      record_count_completion,
                                      record_count_failed,
                                      record_count_total)
           VALUES (
                     'H',
                     v_request_id,
                     l_program_name,
                     v_start_date,
                     TO_DATE (
                        TO_CHAR (l_today_timestamp, 'YYYY-MON-DD HH24:MI:SS'),
                        'YYYY-MON-DD HH24:MI:SS'),
                     'Upload',
                     v_process_time,
                     l_record_count_completion,
                     l_record_count_failed,
                     l_record_count_total);

      COMMIT;

      l_request_id :=
         fnd_request.submit_request ('SQLAP',
                                     'TACSMT006',
                                     NULL,
                                     NULL,
                                     FALSE,
                                     'SMTADDITION',                    ---arg1
                                     v_request_id);
      COMMIT;

      IF l_request_id = 0
      THEN
         write_log (
            'Request Not Submitted due to "' || fnd_message.GET || '".',
            'LOG');
      ELSE
         write_log (
            'Program submitted successfully Request id :' || l_request_id,
            'LOG');
      END IF;

      LOOP
         l_req_return_status :=
            fnd_concurrent.wait_for_request (request_id   => l_request_id,
                                             interval     => 2,
                                             max_wait     => 0,
                                             phase        => lc_phase,
                                             status       => lc_status,
                                             dev_phase    => lc_dev_phase,
                                             dev_status   => lc_dev_status,
                                             MESSAGE      => lc_message);

         EXIT WHEN lc_phase = 'Completed';
         DBMS_LOCK.SLEEP (10);
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         RAISE;
   END;

   PROCEDURE write_log (buff VARCHAR2, which VARCHAR2)
   IS
   BEGIN
      IF UPPER (which) = 'LOG'
      THEN
         fnd_file.put_line (FND_FILE.LOG, buff);
      ELSE
         fnd_file.put_line (FND_FILE.OUTPUT, buff);
      END IF;
   END write_log;

   FUNCTION get_tax_amount (p_invoice_amount   IN NUMBER,
                            p_tax_name         IN VARCHAR2,
                            p_org_id           IN NUMBER)
      RETURN NUMBER
   IS
      l_tax_rate   NUMBER;
   BEGIN
      SELECT tax_rate
        INTO l_tax_rate
        FROM AP_TAX_CODES_ALL
       WHERE     enabled_flag = 'Y'
             AND NVL (inactive_date, SYSDATE + 1) >= SYSDATE
             AND org_id = p_org_id
             AND name = p_tax_name;

      RETURN ( (p_invoice_amount * l_tax_rate) / 100);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_batch_from_req (p_po_number IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_batch_name   VARCHAR2 (100);
   BEGIN
        SELECT prh.attribute15
          INTO l_batch_name
          FROM po_distributions_all pda,
               po_req_distributions_all prd,
               po_requisition_lines_all prl,
               po_requisition_headers_all prh,
               po_headers_all poh
         WHERE     pda.req_distribution_id = prd.distribution_id
               AND prd.requisition_line_id = prl.requisition_line_id
               AND prl.requisition_header_id = prh.requisition_header_id
               AND pda.po_header_id = poh.po_header_id
               AND poh.segment1 = p_po_number
      GROUP BY prh.attribute15;

      RETURN l_batch_name;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN null;      
   END;
END TAC_AP_INTERFACE_DUMMY_SMT;
/

