CREATE OR REPLACE PACKAGE APPS.xcust_supplier_interface_smt
IS
   -- Author  : KITTIYA
   -- Created : 11/11/2016
   -- Purpose :
   -- Modify : spw@ice on 21 Feb 2017
   
   g_citibank               VARCHAR2 (30) := 'CITIBANK';
   g_kbank                  VARCHAR2 (30) := 'KBANK';

   g_out_inbox     CONSTANT VARCHAR2 (50) := 'SMT_SUPOUT_INBOX';
   g_out_history   CONSTANT VARCHAR2 (50) := 'SMT_SUPOUT_HISTORY';
   g_out_error     CONSTANT VARCHAR2 (50) := 'SMT_SUPOUT_ERROR';

   g_in_inbox      CONSTANT VARCHAR2 (50) := 'SMT_SUPIN_INBOX';
   g_in_history    CONSTANT VARCHAR2 (50) := 'SMT_SUPIN_HISTORY';
   g_in_error      CONSTANT VARCHAR2 (50) := 'SMT_SUPIN_ERROR';

   g_log_line               VARCHAR2 (250)
      := '+---------------------------------------------------------------------------+';

   PROCEDURE import_data (err_msg              OUT VARCHAR2,
                          err_code             OUT VARCHAR2,
                          p_default_group          VARCHAR2,
                          p_file_path              VARCHAR2,
                          p_file_name_format       VARCHAR2,
                          p_file_name              VARCHAR2 DEFAULT NULL,
                          p_delimiter              VARCHAR2 DEFAULT '|',
                          p_debug_flag             VARCHAR2 DEFAULT 'N',
                          p_user_id                NUMBER,
                          p_request_id             NUMBER,
                          p_rollback_option        VARCHAR2,
                          o_error_file_name    OUT VARCHAR2);

   PROCEDURE validate_temp_table (err_msg             OUT VARCHAR2,
                                  err_code            OUT VARCHAR2,
                                  p_default_group         VARCHAR2,
                                  p_request_id            NUMBER,
                                  p_rollback_option       VARCHAR2,
                                  p_error_file_name       VARCHAR2);

   PROCEDURE interface_vendor (
      err_msg              OUT VARCHAR2,
      err_code             OUT VARCHAR2,
      p_default_group          VARCHAR2,
      p_file_path              VARCHAR2,
      p_file_name_format       VARCHAR2,
      p_file_name              VARCHAR2 DEFAULT NULL,
      p_delimiter              VARCHAR2 DEFAULT '|',
      p_debug_flag             VARCHAR2,
      p_rollback_option        VARCHAR2);

   PROCEDURE create_logs (p_concurrent_id IN NUMBER);

   FUNCTION extrack_data (p_buffer      IN VARCHAR2,
                          p_delimiter   IN VARCHAR2,
                          p_pos_data    IN NUMBER)
      RETURN VARCHAR2;
END xcust_supplier_interface_smt;
/

