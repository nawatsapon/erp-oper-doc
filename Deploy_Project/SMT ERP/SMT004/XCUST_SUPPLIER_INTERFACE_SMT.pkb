CREATE OR REPLACE PACKAGE BODY APPS.xcust_supplier_interface_smt
IS
   PROCEDURE write_log (p_msg VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.LOG, p_msg);
      DBMS_OUTPUT.put_line (p_msg);
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END write_log;

   PROCEDURE write_log_warning (p_msg VARCHAR2)
   IS
   BEGIN
      write_log ('[WRN]' || ' : ' || p_msg);
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END write_log_warning;

   PROCEDURE write_log_error (p_msg VARCHAR2)
   IS
   BEGIN
      write_log ('[ERR]' || ' : ' || p_msg);
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END write_log_error;

   PROCEDURE write_log_info (p_msg VARCHAR2)
   IS
   BEGIN
      write_log ('[INF]' || ' : ' || p_msg);
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END write_log_info;

   PROCEDURE write_output (p_msg VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.output, p_msg);
      DBMS_OUTPUT.put_line (p_msg);
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END write_output;

   PROCEDURE write_to_filenchar (p_file_handler    UTL_FILE.file_type,
                                 p_line_data       VARCHAR2)
   IS
   BEGIN
      IF (UTL_FILE.is_open (p_file_handler))
      THEN
         UTL_FILE.put_line_nchar (file     => p_file_handler,
                                  buffer   => p_line_data);
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END write_to_filenchar;

   FUNCTION get_directory_path (p_directory_name VARCHAR2)
      RETURN VARCHAR2
   IS
      v_directory_path   VARCHAR2 (500);
   BEGIN
      SELECT dir.directory_path
        INTO v_directory_path
        FROM all_directories dir
       WHERE dir.directory_name = p_directory_name;

      RETURN v_directory_path;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_directory_path;

   FUNCTION char_to_date (p_date_char      VARCHAR2,
                          p_format_mask    VARCHAR2 DEFAULT NULL)
      RETURN DATE
   IS
      v_date   DATE;
   BEGIN
      IF (p_date_char IS NULL)
      THEN
         RETURN NULL;
      END IF;

      IF (p_format_mask IS NOT NULL)
      THEN
         v_date := TO_DATE (p_date_char, p_format_mask);
         RETURN v_date;
      END IF;

      -- Format Mask = dd/mm/yyyy
      BEGIN
         v_date := TO_DATE (p_date_char, 'dd/mm/yyyy');
         RETURN v_date;
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      -- Format Mask = dd/m/yyyy
      BEGIN
         v_date := TO_DATE (p_date_char, 'dd/m/yyyy');
         RETURN v_date;
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      -- Format Mask = d/mm/yyyy
      BEGIN
         v_date := TO_DATE (p_date_char, 'd/mm/yyyy');
         RETURN v_date;
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      -- Format Mask = d/m/yyyy
      BEGIN
         v_date := TO_DATE (p_date_char, 'd/m/yyyy');
         RETURN v_date;
      EXCEPTION
         WHEN OTHERS
         THEN
            RETURN p_date_char;
      END;
   END char_to_date;

   PROCEDURE conc_wait (param_req NUMBER)
   IS
      v_result     VARCHAR2 (1);
      phase        VARCHAR2 (30);
      status       VARCHAR2 (30);
      dev_phase    VARCHAR2 (30);
      dev_status   VARCHAR2 (30);
      MESSAGE      VARCHAR2 (1000);
   BEGIN
      COMMIT;

      IF fnd_concurrent.wait_for_request (param_req,
                                          10,
                                          0,
                                          phase,
                                          status,
                                          dev_phase,
                                          dev_status,
                                          MESSAGE)
      THEN
         NULL;
      END IF;
   END conc_wait;

   PROCEDURE move_data_file (p_source_file VARCHAR2, p_dest_path VARCHAR2)
   IS
      v_req_id   NUMBER;
   BEGIN
      v_req_id :=
         fnd_request.submit_request (application   => 'SQLAP',
                                     program       => 'TACAP_MVFILE_SMT',
                                     argument1     => p_source_file,
                                     argument2     => p_dest_path);
      write_log (
            'Move file from '
         || p_source_file
         || ' to '
         || p_dest_path
         || ' success.');
   EXCEPTION
      WHEN OTHERS
      THEN
         write_log (
               'Move file from '
            || p_source_file
            || ' to '
            || p_dest_path
            || ' failed with error '
            || SQLERRM);
   END move_data_file;

   PROCEDURE submit_interface_api (o_sup_req_id           OUT NUMBER,
                                   o_site_req_id          OUT NUMBER,
                                   o_cont_req_id          OUT NUMBER,
                                   p_conc_request_id   IN     NUMBER)
   IS
      v_sup_req_id    NUMBER := 0;
      v_site_req_id   NUMBER := 0;
      v_cont_req_id   NUMBER := 0;

      CURSOR c_site
      IS
         SELECT DISTINCT vendor_site_id, tmp.VENDOR_SITE_CODE, tmp.org_id
           FROM po_vendors pv,
                PO_VENDOR_SITES_ALL ps,
                xcust_ap_suppliers_temp_debug tmp
          WHERE     ps.vendor_id = pv.vendor_id
                AND ps.vendor_site_code = tmp.VENDOR_SITE_CODE
                AND ps.org_id = tmp.org_id
                AND TRIM (pv.vendor_name) = TRIM (tmp.vendor_name)
                AND tmp.request_id = p_conc_request_id;
   BEGIN
      COMMIT;
      -- Interface Supplier
      v_sup_req_id :=
         fnd_request.submit_request (application   => 'SQLAP',
                                     program       => 'APXSUIMP',
                                     argument1     => 'NEW',
                                     argument2     => '1000',
                                     argument3     => 'N',
                                     argument4     => 'N',
                                     argument5     => 'N');

      IF v_sup_req_id > 0
      THEN
         conc_wait (v_sup_req_id);
      END IF;

      o_sup_req_id := v_sup_req_id;

      --Supplier Sites Open Interface Import
      v_site_req_id :=
         fnd_request.submit_request (application   => 'SQLAP',
                                     program       => 'APXSSIMP',
                                     argument1     => 'NEW',
                                     argument2     => '1000',
                                     argument3     => 'N',
                                     argument4     => 'N',
                                     argument5     => 'N');

      IF v_site_req_id > 0
      THEN
         conc_wait (v_site_req_id);
      END IF;

      o_site_req_id := v_site_req_id;


      FOR r IN c_site
      LOOP
         UPDATE ap_sup_site_contact_int
            SET vendor_site_id = r.vendor_site_id
          WHERE     request_id = p_conc_request_id
                AND VENDOR_SITE_CODE = r.VENDOR_SITE_CODE
                AND org_id = r.org_id
                AND status = 'NEW';

         COMMIT;
      END LOOP;



      --  Supplier Contacts Open Interface Import
      v_cont_req_id :=
         fnd_request.submit_request (application   => 'SQLAP',
                                     program       => 'APXSCIMP',
                                     argument1     => 'NEW',
                                     argument2     => '1000',
                                     argument3     => 'N',
                                     argument4     => 'N',
                                     argument5     => 'N');

      IF v_cont_req_id > 0
      THEN
         conc_wait (v_cont_req_id);
      END IF;

      o_cont_req_id := v_cont_req_id;
   END submit_interface_api;

   PROCEDURE insert_ap_suppliers_int (
      ir_ap_suppliers_int IN ap_suppliers_int%ROWTYPE)
   IS
   BEGIN
      write_log (
            'Insert supplier interface for vendor = '
         || ir_ap_suppliers_int.vendor_name
         || ' ( '
         || ir_ap_suppliers_int.vendor_interface_id
         || ' )');

      DELETE FROM ap_suppliers_int
            WHERE STATUS = 'REJECTED';

      COMMIT;

      INSERT INTO ap_suppliers_int (vendor_interface_id,
                                    vendor_name,
                                    vendor_name_alt,
                                    segment1,
                                    vendor_type_lookup_code,
                                    MATCH_OPTION,
                                    terms_name,
                                    set_of_books_id,
                                    pay_date_basis_lookup_code,
                                    pay_group_lookup_code,
                                    payment_priority,
                                    invoice_currency_code,
                                    PAYMENT_CURRENCY_CODE,
                                    payment_method_lookup_code,
                                    terms_date_basis,
                                    vat_registration_num,
                                    allow_awt_flag,
                                    global_attribute_category,
                                    global_attribute18,
                                    global_attribute17,
                                    global_attribute15,
                                    global_attribute6,
                                    global_attribute20,
                                    attribute_category,
                                    attribute1,
                                    attribute2,
                                    attribute3,
                                    attribute4,
                                    attribute5,
                                    attribute6,
                                    attribute7,
                                    attribute8,
                                    attribute9,
                                    attribute10,
                                    attribute11,
                                    attribute12,
                                    attribute13,
                                    attribute14,
                                    attribute15,
                                    status,
                                    reject_code,
                                    request_id,
                                    creation_date,
                                    last_update_date,
                                    created_by,
                                    last_updated_by)
           VALUES (ir_ap_suppliers_int.vendor_interface_id,
                   ir_ap_suppliers_int.vendor_name,
                   ir_ap_suppliers_int.vendor_name_alt,
                   ir_ap_suppliers_int.segment1,
                   ir_ap_suppliers_int.vendor_type_lookup_code,
                   ir_ap_suppliers_int.MATCH_OPTION,
                   ir_ap_suppliers_int.terms_name,
                   ir_ap_suppliers_int.set_of_books_id,
                   ir_ap_suppliers_int.pay_date_basis_lookup_code,
                   ir_ap_suppliers_int.pay_group_lookup_code,
                   ir_ap_suppliers_int.payment_priority,
                   ir_ap_suppliers_int.invoice_currency_code,
                   ir_ap_suppliers_int.PAYMENT_CURRENCY_CODE,
                   ir_ap_suppliers_int.payment_method_lookup_code,
                   ir_ap_suppliers_int.terms_date_basis,
                   ir_ap_suppliers_int.vat_registration_num,
                   ir_ap_suppliers_int.allow_awt_flag,
                   ir_ap_suppliers_int.global_attribute_category,
                   ir_ap_suppliers_int.global_attribute18,
                   ir_ap_suppliers_int.global_attribute17,
                   ir_ap_suppliers_int.global_attribute15,
                   ir_ap_suppliers_int.global_attribute6,
                   ir_ap_suppliers_int.global_attribute20,
                   ir_ap_suppliers_int.attribute_category,
                   ir_ap_suppliers_int.attribute1,
                   ir_ap_suppliers_int.attribute2,
                   ir_ap_suppliers_int.attribute3,
                   ir_ap_suppliers_int.attribute4,
                   ir_ap_suppliers_int.attribute5,
                   ir_ap_suppliers_int.attribute6,
                   ir_ap_suppliers_int.attribute7,
                   ir_ap_suppliers_int.attribute8,
                   ir_ap_suppliers_int.attribute9,
                   ir_ap_suppliers_int.attribute10,
                   ir_ap_suppliers_int.attribute11,
                   ir_ap_suppliers_int.attribute12,
                   ir_ap_suppliers_int.attribute13,
                   ir_ap_suppliers_int.attribute14,
                   ir_ap_suppliers_int.attribute15,
                   ir_ap_suppliers_int.status,
                   ir_ap_suppliers_int.reject_code,
                   ir_ap_suppliers_int.request_id,
                   ir_ap_suppliers_int.creation_date,
                   ir_ap_suppliers_int.last_update_date,
                   ir_ap_suppliers_int.created_by,
                   ir_ap_suppliers_int.last_updated_by);
   EXCEPTION
      WHEN OTHERS
      THEN
         write_log ('[ERR] : @insert_ap_suppliers_int : ' || SQLERRM);
   END insert_ap_suppliers_int;

   PROCEDURE insert_ap_supplier_site_int (
      ir_ap_sup_site IN ap_supplier_sites_int%ROWTYPE)
   IS
   BEGIN
      write_log (
            'Insert supplier site interface for vendor = '
         || ir_ap_sup_site.vendor_site_code
         || ' ( '
         || ir_ap_sup_site.vendor_interface_id
         || ' )');

      DELETE FROM ap_supplier_sites_int
            WHERE STATUS = 'REJECTED';

      COMMIT;

      INSERT INTO ap_supplier_sites_int (vendor_interface_id,
                                         vendor_site_code,
                                         VENDOR_SITE_CODE_ALT,
                                         INACTIVE_DATE,
                                         purchasing_site_flag,
                                         pay_site_flag,
                                         address_line1,
                                         address_line2,
                                         address_line3,
                                         address_line4,
                                         city,
                                         zip,
                                         province,
                                         country,
                                         county,
                                         address_lines_alt,
                                         payment_method_lookup_code,
                                         terms_date_basis,
                                         distribution_set_name,
                                         accts_pay_code_combination_id,
                                         prepay_code_combination_id,
                                         MATCH_OPTION,
                                         pay_group_lookup_code,
                                         payment_priority,
                                         terms_name,
                                         pay_date_basis_lookup_code,
                                         invoice_currency_code,
                                         attribute1,
                                         attribute2,
                                         attribute3,
                                         attribute4,
                                         attribute12,
                                         attribute13,
                                         attribute14,
                                         attribute15,
                                         attribute5,
                                         attribute6,
                                         attribute7,
                                         attribute8,
                                         attribute9,
                                         attribute10,
                                         attribute_category,
                                         org_id,
                                         allow_awt_flag,
                                         AWT_GROUP_ID,
                                         AWT_GROUP_NAME,
                                         VAT_CODE,
                                         global_attribute6,
                                         global_attribute20,
                                         global_attribute18,
                                         global_attribute17,
                                         global_attribute16,
                                         global_attribute15,
                                         global_attribute14,
                                         global_attribute13,
                                         future_dated_payment_ccid,
                                         status,
                                         reject_code,
                                         request_id,
                                         creation_date,
                                         last_update_date,
                                         created_by,
                                         last_updated_by,
                                         email_address,
                                         global_attribute_category,
                                         vendor_id)
           VALUES (ir_ap_sup_site.vendor_interface_id,
                   ir_ap_sup_site.vendor_site_code,
                   ir_ap_sup_site.VENDOR_SITE_CODE_ALT,
                   ir_ap_sup_site.INACTIVE_DATE,
                   ir_ap_sup_site.purchasing_site_flag,
                   ir_ap_sup_site.pay_site_flag,
                   ir_ap_sup_site.address_line1,
                   ir_ap_sup_site.address_line2,
                   ir_ap_sup_site.address_line3,
                   ir_ap_sup_site.address_line4,
                   ir_ap_sup_site.city,
                   ir_ap_sup_site.zip,
                   ir_ap_sup_site.province,
                   ir_ap_sup_site.country,
                   ir_ap_sup_site.county,
                   ir_ap_sup_site.address_lines_alt,
                   ir_ap_sup_site.payment_method_lookup_code,
                   ir_ap_sup_site.terms_date_basis,
                   ir_ap_sup_site.distribution_set_name,
                   ir_ap_sup_site.accts_pay_code_combination_id,
                   ir_ap_sup_site.prepay_code_combination_id,
                   ir_ap_sup_site.MATCH_OPTION,
                   ir_ap_sup_site.pay_group_lookup_code,
                   ir_ap_sup_site.payment_priority,
                   ir_ap_sup_site.terms_name,
                   ir_ap_sup_site.pay_date_basis_lookup_code,
                   ir_ap_sup_site.invoice_currency_code,
                   ir_ap_sup_site.attribute1,
                   ir_ap_sup_site.attribute2,
                   ir_ap_sup_site.attribute3,
                   ir_ap_sup_site.attribute4,
                   ir_ap_sup_site.attribute12,
                   ir_ap_sup_site.attribute13,
                   ir_ap_sup_site.attribute14,
                   ir_ap_sup_site.attribute15,
                   ir_ap_sup_site.attribute5,
                   ir_ap_sup_site.attribute6,
                   ir_ap_sup_site.attribute7,
                   ir_ap_sup_site.attribute8,
                   ir_ap_sup_site.attribute9,
                   ir_ap_sup_site.attribute10,
                   ir_ap_sup_site.attribute_category,
                   ir_ap_sup_site.org_id,
                   ir_ap_sup_site.allow_awt_flag,
                   ir_ap_sup_site.AWT_GROUP_ID,
                   ir_ap_sup_site.AWT_GROUP_NAME,
                   ir_ap_sup_site.VAT_CODE,
                   ir_ap_sup_site.global_attribute6,
                   ir_ap_sup_site.global_attribute20,
                   ir_ap_sup_site.global_attribute18,
                   ir_ap_sup_site.global_attribute17,
                   ir_ap_sup_site.global_attribute16,
                   ir_ap_sup_site.global_attribute15,
                   ir_ap_sup_site.global_attribute14,
                   ir_ap_sup_site.global_attribute13,
                   ir_ap_sup_site.future_dated_payment_ccid,
                   ir_ap_sup_site.status,
                   ir_ap_sup_site.reject_code,
                   ir_ap_sup_site.request_id,
                   ir_ap_sup_site.creation_date,
                   ir_ap_sup_site.last_update_date,
                   ir_ap_sup_site.created_by,
                   ir_ap_sup_site.last_updated_by,
                   ir_ap_sup_site.email_address,
                   ir_ap_sup_site.global_attribute_category,
                   ir_ap_sup_site.vendor_id);
   EXCEPTION
      WHEN OTHERS
      THEN
         write_log ('[ERR] : @insert_ap_supplier_site_int : ' || SQLERRM);
   END insert_ap_supplier_site_int;

   PROCEDURE insert_ap_supplier_site_cont (
      ir_ap_sup_site_cont IN ap_sup_site_contact_int%ROWTYPE)
   IS
   BEGIN
      write_log (
            'Insert supplier site contact interface for vendor site = '
         || ir_ap_sup_site_cont.vendor_site_code
         || ' ( '
         || ir_ap_sup_site_cont.first_name
         || ' )');

      DELETE FROM ap_sup_site_contact_int
            WHERE STATUS = 'REJECTED';

      COMMIT;

      INSERT INTO ap_sup_site_contact_int (vendor_site_id,
                                           vendor_site_code,
                                           org_id,
                                           first_name,
                                           last_name,
                                           area_code,
                                           phone,
                                           status,
                                           email_address,
                                           fax,
                                           contact_name_alt,
                                           request_id,
                                           creation_date,
                                           last_update_date,
                                           created_by,
                                           last_updated_by,
                                           fax_area_code)
           VALUES (ir_ap_sup_site_cont.vendor_site_id,
                   ir_ap_sup_site_cont.vendor_site_code,
                   ir_ap_sup_site_cont.org_id,
                   ir_ap_sup_site_cont.first_name,
                   ir_ap_sup_site_cont.last_name,
                   ir_ap_sup_site_cont.area_code,
                   ir_ap_sup_site_cont.phone,
                   ir_ap_sup_site_cont.status,
                   ir_ap_sup_site_cont.email_address,
                   ir_ap_sup_site_cont.fax,
                   ir_ap_sup_site_cont.contact_name_alt,
                   ir_ap_sup_site_cont.request_id,
                   ir_ap_sup_site_cont.creation_date,
                   ir_ap_sup_site_cont.last_update_date,
                   ir_ap_sup_site_cont.created_by,
                   ir_ap_sup_site_cont.last_updated_by,
                   ir_ap_sup_site_cont.fax_area_code);
   EXCEPTION
      WHEN OTHERS
      THEN
         write_log ('[ERR] : @insert_ap_supplier_site_cont : ' || SQLERRM);
   END insert_ap_supplier_site_cont;

   FUNCTION default_vendors_row (p_row IN po_vendors%ROWTYPE)
      RETURN po_vendors%ROWTYPE
   IS
      v_vendors   po_vendors%ROWTYPE;
   BEGIN
      SELECT *
        INTO v_vendors
        FROM po_vendors
       WHERE vendor_id = p_row.vendor_id;

      IF (p_row.vendor_type_lookup_code IS NOT NULL)
      THEN
         v_vendors.vendor_type_lookup_code :=
            CASE
               WHEN p_row.vendor_type_lookup_code = '???' THEN NULL
               ELSE p_row.vendor_type_lookup_code
            END;
      END IF;

      IF (p_row.allow_awt_flag IS NOT NULL)
      THEN
         v_vendors.allow_awt_flag :=
            CASE
               WHEN p_row.allow_awt_flag = '???' THEN NULL
               ELSE p_row.allow_awt_flag
            END;
      END IF;

      RETURN v_vendors;
   END;

   PROCEDURE update_po_vendors (ir_po_vendors   IN     po_vendors%ROWTYPE,
                                o_status           OUT BOOLEAN)
   IS
      v_po_vendors   po_vendors%ROWTYPE;
   BEGIN
      o_status := FALSE;
      v_po_vendors := default_vendors_row (ir_po_vendors);

      /*write_log (
            'Update supplier type code 22: '
         || ir_po_vendors.VENDOR_TYPE_LOOKUP_CODE);*/

      UPDATE po_vendors
         SET VENDOR_TYPE_LOOKUP_CODE = v_po_vendors.VENDOR_TYPE_LOOKUP_CODE,
             allow_awt_flag = v_po_vendors.allow_awt_flag,
             global_attribute20 = v_po_vendors.global_attribute20,
             global_attribute6 = v_po_vendors.global_attribute6,
             attribute1 = v_po_vendors.attribute1,
             attribute2 = v_po_vendors.attribute2,
             global_attribute18 = v_po_vendors.global_attribute18,
             global_attribute17 = v_po_vendors.global_attribute17,
             global_attribute15 = v_po_vendors.global_attribute15,
             last_update_date = SYSDATE,
             last_updated_by = fnd_global.user_id,
             last_update_login = fnd_global.login_id
       WHERE vendor_id = v_po_vendors.vendor_id;

      o_status := FALSE;
   EXCEPTION
      WHEN OTHERS
      THEN
         o_status := TRUE;
         write_log ('Can not update vendor!');
   END update_po_vendors;

   FUNCTION default_vendors_site_row (p_row IN po_vendor_sites_all%ROWTYPE)
      RETURN po_vendor_sites_all%ROWTYPE
   IS
      v_vendor_site   po_vendor_sites_all%ROWTYPE;
   BEGIN
      SELECT *
        INTO v_vendor_site
        FROM po_vendor_sites_all
       WHERE vendor_site_id = p_row.vendor_site_id;

      IF (p_row.inactive_date IS NOT NULL)
      THEN
         v_vendor_site.inactive_date := p_row.inactive_date;
      END IF;

      IF (p_row.terms_id IS NOT NULL)
      THEN
         v_vendor_site.terms_id := p_row.terms_id;
      END IF;

      IF (p_row.pay_group_lookup_code IS NOT NULL)
      THEN
         v_vendor_site.pay_group_lookup_code :=
            CASE
               WHEN p_row.pay_group_lookup_code = '???' THEN NULL
               ELSE p_row.pay_group_lookup_code
            END;
      END IF;

      IF (p_row.address_line1 IS NOT NULL)
      THEN
         v_vendor_site.address_line1 :=
            CASE
               WHEN p_row.address_line1 = '???' THEN NULL
               ELSE p_row.address_line1
            END;
      END IF;

      IF (p_row.address_line2 IS NOT NULL)
      THEN
         v_vendor_site.address_line2 :=
            CASE
               WHEN p_row.address_line2 = '???' THEN NULL
               ELSE p_row.address_line2
            END;
      END IF;

      IF (p_row.address_line3 IS NOT NULL)
      THEN
         v_vendor_site.address_line3 :=
            CASE
               WHEN p_row.address_line3 = '???' THEN NULL
               ELSE p_row.address_line3
            END;
      END IF;

      IF (p_row.address_line4 IS NOT NULL)
      THEN
         v_vendor_site.address_line4 :=
            CASE
               WHEN p_row.address_line4 = '???' THEN NULL
               ELSE p_row.address_line4
            END;
      END IF;

      IF (p_row.city IS NOT NULL)
      THEN
         v_vendor_site.city :=
            CASE WHEN p_row.city = '???' THEN NULL ELSE p_row.city END;
      END IF;

      IF (p_row.zip IS NOT NULL)
      THEN
         v_vendor_site.zip :=
            CASE WHEN p_row.zip = '???' THEN NULL ELSE p_row.zip END;
      END IF;

      IF (p_row.province IS NOT NULL)
      THEN
         v_vendor_site.province :=
            CASE
               WHEN p_row.province = '???' THEN NULL
               ELSE p_row.province
            END;
      END IF;

      IF (p_row.pay_site_flag IS NOT NULL)
      THEN
         v_vendor_site.pay_site_flag :=
            CASE
               WHEN p_row.pay_site_flag = '???' THEN NULL
               ELSE p_row.pay_site_flag
            END;
      END IF;

      IF (p_row.purchasing_site_flag IS NOT NULL)
      THEN
         v_vendor_site.purchasing_site_flag :=
            CASE
               WHEN p_row.purchasing_site_flag = '???' THEN NULL
               ELSE p_row.purchasing_site_flag
            END;
      END IF;

      IF (p_row.accts_pay_code_combination_id IS NOT NULL)
      THEN
         v_vendor_site.accts_pay_code_combination_id :=
            p_row.accts_pay_code_combination_id;
      END IF;

      IF (p_row.prepay_code_combination_id IS NOT NULL)
      THEN
         v_vendor_site.prepay_code_combination_id :=
            p_row.prepay_code_combination_id;
      END IF;

      IF (p_row.match_option IS NOT NULL)
      THEN
         v_vendor_site.match_option :=
            CASE
               WHEN p_row.match_option = '???' THEN NULL
               ELSE p_row.match_option
            END;
      END IF;

      IF (p_row.payment_priority IS NOT NULL)
      THEN
         v_vendor_site.payment_priority := p_row.payment_priority;
      END IF;

      IF (p_row.terms_date_basis IS NOT NULL)
      THEN
         v_vendor_site.terms_date_basis :=
            CASE
               WHEN p_row.terms_date_basis = '???' THEN NULL
               ELSE p_row.terms_date_basis
            END;
      END IF;

      IF (p_row.pay_date_basis_lookup_code IS NOT NULL)
      THEN
         v_vendor_site.pay_date_basis_lookup_code :=
            CASE
               WHEN p_row.pay_date_basis_lookup_code = '???' THEN NULL
               ELSE p_row.pay_date_basis_lookup_code
            END;
      END IF;

      IF (p_row.payment_method_lookup_code IS NOT NULL)
      THEN
         v_vendor_site.payment_method_lookup_code :=
            CASE
               WHEN p_row.payment_method_lookup_code = '???' THEN NULL
               ELSE p_row.payment_method_lookup_code
            END;
      END IF;

      IF (p_row.allow_awt_flag IS NOT NULL)
      THEN
         v_vendor_site.allow_awt_flag :=
            CASE
               WHEN p_row.allow_awt_flag = '???' THEN NULL
               ELSE p_row.allow_awt_flag
            END;
      END IF;

      IF (p_row.awt_group_id IS NOT NULL)
      THEN
         v_vendor_site.awt_group_id := p_row.awt_group_id;
      END IF;

      IF (p_row.vat_code IS NOT NULL)
      THEN
         v_vendor_site.vat_code :=
            CASE
               WHEN p_row.vat_code = '???' THEN NULL
               ELSE p_row.vat_code
            END;
      END IF;

      IF (p_row.attribute_category IS NOT NULL)
      THEN
         v_vendor_site.attribute_category :=
            CASE
               WHEN p_row.attribute_category = '???' THEN NULL
               ELSE p_row.attribute_category
            END;
      END IF;

      IF (p_row.attribute1 IS NOT NULL)
      THEN
         v_vendor_site.attribute1 :=
            CASE
               WHEN p_row.attribute1 = '???' THEN NULL
               ELSE p_row.attribute1
            END;
      END IF;

      IF (p_row.attribute2 IS NOT NULL)
      THEN
         v_vendor_site.attribute2 :=
            CASE
               WHEN p_row.attribute2 = '???' THEN NULL
               ELSE p_row.attribute2
            END;
      END IF;

      IF (p_row.attribute3 IS NOT NULL)
      THEN
         v_vendor_site.attribute3 :=
            CASE
               WHEN p_row.attribute3 = '???' THEN NULL
               ELSE p_row.attribute3
            END;
      END IF;

      IF (p_row.attribute4 IS NOT NULL)
      THEN
         v_vendor_site.attribute4 :=
            CASE
               WHEN p_row.attribute4 = '???' THEN NULL
               ELSE p_row.attribute4
            END;
      END IF;

      IF (p_row.attribute5 IS NOT NULL)
      THEN
         v_vendor_site.attribute5 :=
            CASE
               WHEN p_row.attribute5 = '???' THEN NULL
               ELSE p_row.attribute5
            END;
      END IF;

      IF (p_row.attribute6 IS NOT NULL)
      THEN
         v_vendor_site.attribute6 :=
            CASE
               WHEN p_row.attribute6 = '???' THEN NULL
               ELSE p_row.attribute6
            END;
      END IF;

      IF (p_row.attribute7 IS NOT NULL)
      THEN
         v_vendor_site.attribute7 :=
            CASE
               WHEN p_row.attribute7 = '???' THEN NULL
               ELSE p_row.attribute7
            END;
      END IF;

      IF (p_row.attribute12 IS NOT NULL)
      THEN
         v_vendor_site.attribute12 :=
            CASE
               WHEN p_row.attribute12 = '???' THEN NULL
               ELSE p_row.attribute12
            END;
      END IF;

      IF (p_row.attribute13 IS NOT NULL)
      THEN
         v_vendor_site.attribute13 :=
            CASE
               WHEN p_row.attribute13 = '???' THEN NULL
               ELSE p_row.attribute13
            END;
      END IF;

      IF (p_row.attribute14 IS NOT NULL)
      THEN
         v_vendor_site.attribute14 :=
            CASE
               WHEN p_row.attribute14 = '???' THEN NULL
               ELSE p_row.attribute14
            END;
      END IF;

      IF (p_row.attribute15 IS NOT NULL)
      THEN
         v_vendor_site.attribute15 :=
            CASE
               WHEN p_row.attribute15 = '???' THEN NULL
               ELSE p_row.attribute15
            END;
      END IF;

      IF (p_row.vendor_site_code_alt IS NOT NULL)
      THEN
         v_vendor_site.vendor_site_code_alt :=
            CASE
               WHEN p_row.vendor_site_code_alt = '???' THEN NULL
               ELSE p_row.vendor_site_code_alt
            END;
      END IF;

      IF (p_row.vat_registration_num IS NOT NULL)
      THEN
         v_vendor_site.vat_registration_num :=
            CASE
               WHEN p_row.vat_registration_num = '???' THEN NULL
               ELSE p_row.vat_registration_num
            END;
      END IF;

      IF (p_row.email_address IS NOT NULL)
      THEN
         v_vendor_site.email_address :=
            CASE
               WHEN p_row.email_address = '???' THEN NULL
               ELSE p_row.email_address
            END;
      END IF;

      IF (p_row.global_attribute13 IS NOT NULL)
      THEN
         v_vendor_site.global_attribute13 :=
            CASE
               WHEN p_row.global_attribute13 = '???' THEN NULL
               ELSE p_row.global_attribute13
            END;
      END IF;

      IF (p_row.global_attribute14 IS NOT NULL)
      THEN
         v_vendor_site.global_attribute14 :=
            CASE
               WHEN p_row.global_attribute14 = '???' THEN NULL
               ELSE p_row.global_attribute14
            END;
      END IF;

      IF (p_row.global_attribute15 IS NOT NULL)
      THEN
         v_vendor_site.global_attribute15 :=
            CASE
               WHEN p_row.global_attribute15 = '???' THEN NULL
               ELSE p_row.global_attribute15
            END;
      END IF;

      IF (p_row.global_attribute16 IS NOT NULL)
      THEN
         v_vendor_site.global_attribute16 :=
            CASE
               WHEN p_row.global_attribute16 = '???' THEN NULL
               ELSE p_row.global_attribute16
            END;
      END IF;

      IF (p_row.global_attribute17 IS NOT NULL)
      THEN
         v_vendor_site.global_attribute17 :=
            CASE
               WHEN p_row.global_attribute17 = '???' THEN NULL
               ELSE p_row.global_attribute17
            END;
      END IF;

      IF (p_row.global_attribute18 IS NOT NULL)
      THEN
         v_vendor_site.global_attribute18 :=
            CASE
               WHEN p_row.global_attribute18 = '???' THEN NULL
               ELSE p_row.global_attribute18
            END;
      END IF;

      IF (p_row.global_attribute20 IS NOT NULL)
      THEN
         v_vendor_site.global_attribute20 :=
            CASE
               WHEN p_row.global_attribute20 = '???' THEN NULL
               ELSE p_row.global_attribute20
            END;
      END IF;
      
      IF (p_row.attribute8 IS NOT NULL)
      THEN
         v_vendor_site.attribute8 :=
            CASE
               WHEN p_row.attribute8 = '???' THEN NULL
               ELSE p_row.attribute8
            END;
      END IF;
      
      IF (p_row.attribute9 IS NOT NULL)
      THEN
         v_vendor_site.attribute9 :=
            CASE
               WHEN p_row.attribute9 = '???' THEN NULL
               ELSE p_row.attribute9
            END;
      END IF;     
      
      IF (p_row.attribute10 IS NOT NULL)
      THEN
         v_vendor_site.attribute10 :=
            CASE
               WHEN p_row.attribute10 = '???' THEN NULL
               ELSE p_row.attribute10
            END;
      END IF;   
      
      IF (p_row.country IS NOT NULL)
      THEN
         v_vendor_site.country :=
            CASE
               WHEN p_row.country = '???' THEN NULL
               ELSE p_row.country
            END;
      END IF;      
      
      IF (p_row.county IS NOT NULL)
      THEN
         v_vendor_site.county :=
            CASE
               WHEN p_row.county = '???' THEN NULL
               ELSE p_row.county
            END;
      END IF;      
      
      IF (p_row.address_lines_alt IS NOT NULL)
      THEN
         v_vendor_site.address_lines_alt :=
            CASE
               WHEN p_row.address_lines_alt = '???' THEN NULL
               ELSE p_row.address_lines_alt
            END;
      END IF;               

      RETURN v_vendor_site;
   END;

   PROCEDURE update_po_vendor_sites (
      ir_po_vendor_sites   IN     po_vendor_sites_all%ROWTYPE,
      o_status                OUT BOOLEAN)
   IS
      v_po_vendor_sites   po_vendor_sites_all%ROWTYPE;
   BEGIN
      write_log (
            'Update Global Attribute14 to => '
         || ir_po_vendor_sites.global_attribute14);
      write_log (
            'Update Global Attribute13 to => '
         || ir_po_vendor_sites.global_attribute13);
      write_log (
            'Update COUNTRY to => '
         || ir_po_vendor_sites.country);
      write_log (
            'Update COUNTY to => '
         || ir_po_vendor_sites.county);                  
                
      o_status := FALSE;
      v_po_vendor_sites := default_vendors_site_row (ir_po_vendor_sites);

      UPDATE po_vendor_sites_all
         SET inactive_date = v_po_vendor_sites.inactive_date,
             terms_id = v_po_vendor_sites.terms_id,
             pay_group_lookup_code = v_po_vendor_sites.pay_group_lookup_code,
             address_line1 = v_po_vendor_sites.address_line1,
             address_line2 = v_po_vendor_sites.address_line2,
             address_line3 = v_po_vendor_sites.address_line3,
             address_line4 = v_po_vendor_sites.address_line4,
             city = v_po_vendor_sites.city,
             zip = v_po_vendor_sites.zip,
             province = v_po_vendor_sites.province,
             pay_site_flag = v_po_vendor_sites.pay_site_flag,
             purchasing_site_flag = v_po_vendor_sites.purchasing_site_flag,
             accts_pay_code_combination_id =
                v_po_vendor_sites.accts_pay_code_combination_id,
             prepay_code_combination_id =
                v_po_vendor_sites.prepay_code_combination_id,
             match_option = v_po_vendor_sites.match_option,
             payment_priority = v_po_vendor_sites.payment_priority,
             terms_date_basis = v_po_vendor_sites.terms_date_basis,
             pay_date_basis_lookup_code =
                v_po_vendor_sites.pay_date_basis_lookup_code,
             payment_method_lookup_code =
                v_po_vendor_sites.payment_method_lookup_code,
             allow_awt_flag = v_po_vendor_sites.allow_awt_flag,
             awt_group_id = v_po_vendor_sites.awt_group_id,
             vat_code = v_po_vendor_sites.vat_code,
             attribute1 = v_po_vendor_sites.attribute1,
             attribute2 = v_po_vendor_sites.attribute2,
             attribute_category = v_po_vendor_sites.attribute_category,
             attribute3 = v_po_vendor_sites.attribute3,
             attribute4 = v_po_vendor_sites.attribute4,
             attribute5 = v_po_vendor_sites.attribute5,
             attribute6 = v_po_vendor_sites.attribute6,
             attribute7 = v_po_vendor_sites.attribute7,
             vendor_site_code_alt = v_po_vendor_sites.vendor_site_code_alt --Shop Name
                                                                          ,
             attribute12 = v_po_vendor_sites.attribute12    --Mail to Address1
                                                        ,
             attribute13 = v_po_vendor_sites.attribute13    --Mail to Address2
                                                        ,
             attribute14 = v_po_vendor_sites.attribute14    --Mail to Address3
                                                        ,
             attribute15 = v_po_vendor_sites.attribute15          --Owner Name
                                                        ,
             vat_registration_num = v_po_vendor_sites.vat_registration_num,
             global_attribute20 = v_po_vendor_sites.global_attribute20,
             global_attribute18 = v_po_vendor_sites.global_attribute18,
             global_attribute17 = v_po_vendor_sites.global_attribute17,
             global_attribute16 = v_po_vendor_sites.global_attribute16,
             global_attribute15 = v_po_vendor_sites.global_attribute15,
             global_attribute14 = v_po_vendor_sites.global_attribute14,
             global_attribute13 = v_po_vendor_sites.global_attribute13,
             last_update_date = SYSDATE,
             last_updated_by = fnd_global.user_id,
             last_update_login = fnd_global.login_id,
             email_address = v_po_vendor_sites.email_address,
             attribute8 = v_po_vendor_sites.attribute8,
             attribute9 = v_po_vendor_sites.attribute9,
             attribute10 = v_po_vendor_sites.attribute10,
             country = v_po_vendor_sites.country,
             county = v_po_vendor_sites.county,
             address_lines_alt = v_po_vendor_sites.address_lines_alt
       WHERE vendor_site_id = v_po_vendor_sites.vendor_site_id;

      COMMIT;
      o_status := FALSE;
   EXCEPTION
      WHEN OTHERS
      THEN
         o_status := TRUE;
         write_log ('Can not update vendor site! : ' || SQLERRM);
   --         write_log ('Can not update vendor site!');
   END update_po_vendor_sites;

   PROCEDURE update_po_vendor_contacts (
      ir_po_vendor_contacts   IN     po_vendor_contacts%ROWTYPE,
      o_status                   OUT BOOLEAN)
   IS
   BEGIN
      o_status := FALSE;

      UPDATE po_vendor_contacts
         SET first_name = ir_po_vendor_contacts.first_name,
             last_name = ir_po_vendor_contacts.last_name -- ,area_code = ir_po_vendor_contacts.area_code
                                                        ,
             phone = ir_po_vendor_contacts.phone,
             email_address = ir_po_vendor_contacts.email_address,
             fax = ir_po_vendor_contacts.fax,
             last_update_date = SYSDATE,
             last_updated_by = fnd_global.user_id,
             last_update_login = fnd_global.login_id,
             area_code = ir_po_vendor_contacts.area_code,
             fax_area_code = ir_po_vendor_contacts.fax_area_code
       WHERE vendor_contact_id = ir_po_vendor_contacts.vendor_contact_id;

      o_status := FALSE;
   EXCEPTION
      WHEN OTHERS
      THEN
         o_status := TRUE;
         write_log ('Can not update vendor contact!');
   END update_po_vendor_contacts;

   PROCEDURE delete_temp_table (p_source_name    VARCHAR2 DEFAULT 'SMT',
                                p_old_days       NUMBER DEFAULT 45)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      DELETE FROM xcust_ap_suppliers_temp_debug a
            WHERE     a.eai_crtd_dttm <= (SYSDATE - p_old_days)
                  AND a.default_group = NVL (p_source_name, 'SMT');

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END delete_temp_table;

   FUNCTION extrack_data (p_buffer      IN VARCHAR2,
                          p_delimiter   IN VARCHAR2,
                          p_pos_data    IN NUMBER)
      RETURN VARCHAR2
   IS
      v_pos_start    NUMBER;
      v_pos_end      NUMBER;
      v_subsrt_len   NUMBER;
      v_data         VARCHAR2 (2000);
   BEGIN
      IF p_pos_data = 1
      THEN
         v_pos_start := 1;
      ELSE
         v_pos_start :=
              INSTR (p_buffer,
                     p_delimiter,
                     1,
                     p_pos_data - 1)
            + 1;
      END IF;

      v_pos_end :=
         INSTR (p_buffer,
                p_delimiter,
                1,
                p_pos_data);

      IF v_pos_end = 0
      THEN
         v_subsrt_len := LENGTH (p_buffer);
      ELSE
         v_subsrt_len := v_pos_end - v_pos_start;
      END IF;

      v_data := SUBSTR (p_buffer, v_pos_start, v_subsrt_len);

      RETURN REPLACE (TRIM (v_data), CHR (10), '');
   EXCEPTION
      WHEN OTHERS
      THEN
         write_log (
            '[ERR] : @extrack_data : POS' || p_pos_data || ' : ' || SQLERRM);
         RETURN NULL;
   END extrack_data;

   FUNCTION split_data (p_line_buff    IN VARCHAR2,
                        p_delimiter    IN VARCHAR2 DEFAULT '|',
                        p_debug_flag      VARCHAR2 DEFAULT 'N')
      RETURN xcust_ap_suppliers_temp_debug%ROWTYPE
   IS
      v_rec_data        xcust_ap_suppliers_temp_debug%ROWTYPE;
      v_creation_date   DATE := SYSDATE;
      v_line_buff       VARCHAR2 (32000);
   BEGIN
      v_line_buff := TRIM (p_line_buff);

      IF v_line_buff IS NULL
      THEN
         RETURN v_rec_data;
      END IF;

      v_rec_data.vendor_number := extrack_data (p_line_buff, p_delimiter, 1);
      v_rec_data.vendor_name := extrack_data (p_line_buff, p_delimiter, 2);
      v_rec_data.vendor_name_alt := extrack_data (p_line_buff, p_delimiter, 3);
      v_rec_data.tax_id_flag := extrack_data (p_line_buff, p_delimiter, 4);
      v_rec_data.vat_registration_num :=
         extrack_data (p_line_buff, p_delimiter, 5);
      v_rec_data.vendor_type_lookup_code :=
         extrack_data (p_line_buff, p_delimiter, 6);
      v_rec_data.INVOICE_MATCH_OPTION :=
         extrack_data (p_line_buff, p_delimiter, 7);
      v_rec_data.terms_name := extrack_data (p_line_buff, p_delimiter, 8);
      v_rec_data.pay_group_lookup_code :=
         extrack_data (p_line_buff, p_delimiter, 9);
      v_rec_data.payment_priority :=
         extrack_data (p_line_buff, p_delimiter, 10);
      v_rec_data.terms_date_basis :=
         extrack_data (p_line_buff, p_delimiter, 11);
      v_rec_data.pay_date_basis_lookup_code :=
         extrack_data (p_line_buff, p_delimiter, 12);
      v_rec_data.payment_method_lookup_code :=
         extrack_data (p_line_buff, p_delimiter, 13);
      v_rec_data.allow_awt_flag := extrack_data (p_line_buff, p_delimiter, 14);
      v_rec_data.attribute1 := extrack_data (p_line_buff, p_delimiter, 15); -- RETAILER_CODE
      v_rec_data.attribute2 := extrack_data (p_line_buff, p_delimiter, 16); -- PAYEE
      v_rec_data.global_attribute20 :=
         extrack_data (p_line_buff, p_delimiter, 17);          --PHOR_NGOR_DOR
      v_rec_data.global_attribute18 :=
         extrack_data (p_line_buff, p_delimiter, 18);      -- SUPPLIER_ID_CARD
      v_rec_data.global_attribute17 :=
         extrack_data (p_line_buff, p_delimiter, 19);             -- CONDITION
      v_rec_data.global_attribute15 :=
         extrack_data (p_line_buff, p_delimiter, 20); -- ALTERNATE_SUPPLIER_TAX_ID
      write_log (
            'GLOBAL_ATTRIBUTE15 : '
         || extrack_data (p_line_buff, p_delimiter, 20));
      v_rec_data.vendor_site_code :=
         extrack_data (p_line_buff, p_delimiter, 21);
      v_rec_data.vendor_site_alt :=
         extrack_data (p_line_buff, p_delimiter, 22);
      v_rec_data.inactive_site_date :=
         extrack_data (p_line_buff, p_delimiter, 23);
      v_rec_data.address_line1 := extrack_data (p_line_buff, p_delimiter, 24);
      v_rec_data.address_line2 := extrack_data (p_line_buff, p_delimiter, 25);
      v_rec_data.address_line3 := extrack_data (p_line_buff, p_delimiter, 26);
      v_rec_data.address_line4 := extrack_data (p_line_buff, p_delimiter, 27);
      v_rec_data.city := extrack_data (p_line_buff, p_delimiter, 28);
      v_rec_data.province := extrack_data (p_line_buff, p_delimiter, 29);
      v_rec_data.zip := extrack_data (p_line_buff, p_delimiter, 30);
      v_rec_data.pay_site_flag := extrack_data (p_line_buff, p_delimiter, 31);
      v_rec_data.purchasing_site_flag :=
         extrack_data (p_line_buff, p_delimiter, 32);
      v_rec_data.first_name := extrack_data (p_line_buff, p_delimiter, 33);
      v_rec_data.last_name := extrack_data (p_line_buff, p_delimiter, 34);
      v_rec_data.phone := extrack_data (p_line_buff, p_delimiter, 35);
      v_rec_data.fax := extrack_data (p_line_buff, p_delimiter, 36);
      v_rec_data.email_address := extrack_data (p_line_buff, p_delimiter, 37);
      v_rec_data.SITE_LIABILITY_ACCOUNT :=
         extrack_data (p_line_buff, p_delimiter, 38);
      v_rec_data.SITE_PREPAYMENT_ACCOUNT :=
         extrack_data (p_line_buff, p_delimiter, 39);
      v_rec_data.SITE_INVOICE_MATCH_OPTION :=
         extrack_data (p_line_buff, p_delimiter, 40);
      v_rec_data.site_terms_name :=
         extrack_data (p_line_buff, p_delimiter, 41);
      v_rec_data.site_pay_group_lookup_code :=
         extrack_data (p_line_buff, p_delimiter, 42);
      v_rec_data.site_payment_priority :=
         extrack_data (p_line_buff, p_delimiter, 43);
      v_rec_data.site_terms_date_basis :=
         extrack_data (p_line_buff, p_delimiter, 44);
      v_rec_data.site_pay_date_basis_lookup_cod :=
         extrack_data (p_line_buff, p_delimiter, 45);
      v_rec_data.site_payment_method_lookup_cod :=
         extrack_data (p_line_buff, p_delimiter, 46);
      v_rec_data.site_allow_wht := extrack_data (p_line_buff, p_delimiter, 47);
      v_rec_data.site_wht_group := extrack_data (p_line_buff, p_delimiter, 48);
      v_rec_data.site_vat_code := extrack_data (p_line_buff, p_delimiter, 49);
      v_rec_data.site_attribute1 :=
         extrack_data (p_line_buff, p_delimiter, 50);    -- RETAILER_CODE_SITE
      v_rec_data.site_attribute2 :=
         extrack_data (p_line_buff, p_delimiter, 51);            -- PAYEE_SITE
      v_rec_data.site_attribute_category :=
         extrack_data (p_line_buff, p_delimiter, 52);    -- PAY_FROM_BANK_SITE
      v_rec_data.site_attribute3 :=
         extrack_data (p_line_buff, p_delimiter, 53);        -- BANK_CODE_SITE
      v_rec_data.site_attribute4 :=
         extrack_data (p_line_buff, p_delimiter, 54);     -- BANK_ACCOUNT_SITE
      v_rec_data.site_attribute5 :=
         extrack_data (p_line_buff, p_delimiter, 55);      -- CLIENT_CODE_SITE
      v_rec_data.site_attribute6 :=
         extrack_data (p_line_buff, p_delimiter, 56); -- DEBIT_ACCOUNT_CODE_SITE
      v_rec_data.site_attribute7 :=
         extrack_data (p_line_buff, p_delimiter, 57);  -- DELIVERY_METHOD_SITE
      v_rec_data.site_global_attribute20 :=
         extrack_data (p_line_buff, p_delimiter, 58);          --PHOR_NGOR_DOR
      v_rec_data.site_global_attribute18 :=
         extrack_data (p_line_buff, p_delimiter, 59);      -- SUPPLIER_ID_CARD
      v_rec_data.site_global_attribute17 :=
         extrack_data (p_line_buff, p_delimiter, 60);             -- CONDITION
      v_rec_data.site_global_attribute16 :=
         extrack_data (p_line_buff, p_delimiter, 61);     -- REVENUE_NAME_SITE
      v_rec_data.site_global_attribute15 :=
         extrack_data (p_line_buff, p_delimiter, 62); -- ALTERNATE_SUPPLIER_TAX_ID
      v_rec_data.site_global_attribute14 :=
         extrack_data (p_line_buff, p_delimiter, 63);    -- BRANCH_NUMBER_SITE
      v_rec_data.site_global_attribute13 :=
         extrack_data (p_line_buff, p_delimiter, 64);     -- REVENUE_TYPE_SITE
      v_rec_data.site_attribute12 :=
         extrack_data (p_line_buff, p_delimiter, 65);        -- MAIL_ADDRESS_1
      v_rec_data.site_attribute13 :=
         extrack_data (p_line_buff, p_delimiter, 66);        -- MAIL_ADDRESS_2
      v_rec_data.site_attribute14 :=
         extrack_data (p_line_buff, p_delimiter, 67);        -- MAIL_ADDRESS_3
      v_rec_data.site_owner_name :=
         extrack_data (p_line_buff, p_delimiter, 68);
      v_rec_data.org_id := extrack_data (p_line_buff, p_delimiter, 69);

      v_rec_data.ctb_tax_invoice_require :=
         extrack_data (p_line_buff, p_delimiter, 70);
      v_rec_data.ctb_receipt_require :=
         extrack_data (p_line_buff, p_delimiter, 71);
      v_rec_data.ctb_bill_require :=
         extrack_data (p_line_buff, p_delimiter, 72);
      v_rec_data.country_site := extrack_data (p_line_buff, p_delimiter, 73);
      v_rec_data.county_site := extrack_data (p_line_buff, p_delimiter, 74);
      v_rec_data.alternate_address :=
         extrack_data (p_line_buff, p_delimiter, 75);
      v_rec_data.area_code_tel := extrack_data (p_line_buff, p_delimiter, 76);
      v_rec_data.area_code_fax := extrack_data (p_line_buff, p_delimiter, 77);

      v_rec_data.error_message := NULL;
      RETURN v_rec_data;
   EXCEPTION
      WHEN OTHERS
      THEN
         v_rec_data.error_message :=
            'xcust_supplier_interface_smt.split_data : ' || SQLERRM;
         RETURN v_rec_data;
   END split_data;

   PROCEDURE import_data (err_msg              OUT VARCHAR2,
                          err_code             OUT VARCHAR2,
                          p_default_group          VARCHAR2,
                          p_file_path              VARCHAR2,
                          p_file_name_format       VARCHAR2,
                          p_file_name              VARCHAR2 DEFAULT NULL,
                          p_delimiter              VARCHAR2 DEFAULT '|',
                          p_debug_flag             VARCHAR2 DEFAULT 'N',
                          p_user_id                NUMBER,
                          p_request_id             NUMBER,
                          p_rollback_option        VARCHAR2,
                          o_error_file_name    OUT VARCHAR2)
   IS
      -- o_error_file_name varchar2(1000);
      v_file_name           VARCHAR2 (100);
      v_date                DATE := SYSDATE;
      v_file_handle         UTL_FILE.file_type;
      v_line_data           NVARCHAR2 (32767);
      v_line_count          NUMBER := 0;
      v_line_success        NUMBER := 0;
      v_line_error          NUMBER := 0;

      v_rec_data            xcust_ap_suppliers_temp_debug%ROWTYPE;
      v_insert_error_flag   VARCHAR2 (1) := 'N';

      v_error_file_name     VARCHAR2 (100) := '';
      v_error_file_handle   UTL_FILE.file_type;
      v_error_line_data     NVARCHAR2 (32767);
      v_error_file_path     VARCHAR2 (100);

      v_seq_id              NUMBER;
   BEGIN
      err_code := '0';
      delete_temp_table (p_default_group, 45);

      IF p_file_path IS NULL
      THEN
         RAISE UTL_FILE.invalid_path;
      END IF;

      IF (p_file_name IS NOT NULL)
      THEN
         v_file_name := p_file_name;
      ELSE
         IF p_file_name_format IS NULL
         THEN
            RAISE UTL_FILE.invalid_filename;
         END IF;

         v_file_name :=
            REPLACE (p_file_name_format,
                     '$DATE$',
                     TRIM (TO_CHAR (v_date, 'YYMMDD')));
      END IF;

      write_log (g_log_line);
      write_log (
            'Import data from file '
         || get_directory_path (p_file_path)
         || '/'
         || v_file_name);
      write_output (g_log_line);
      write_output (
            'Import data from file '
         || get_directory_path (p_file_path)
         || '/'
         || v_file_name);
      write_output (g_log_line);

      BEGIN
         --v_file_handle := utl_file.fopen(location => p_file_path, filename => v_file_name, open_mode => 'r');
         v_file_handle :=
            UTL_FILE.fopen_nchar (location    => p_file_path,
                                  filename    => v_file_name,
                                  open_mode   => 'r');


         UTL_FILE.fclose (v_file_handle);
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      --v_file_handle := utl_file.fopen(location => p_file_path, filename => v_file_name, open_mode => 'r');
      v_file_handle :=
         UTL_FILE.fopen_nchar (location    => p_file_path,
                               filename    => v_file_name,
                               open_mode   => 'r');

      v_error_file_path := tac_supplier_interface_smt.g_out_error;
      v_error_file_name := REPLACE (v_file_name, '.txt', '_Error.txt');
      o_error_file_name := v_error_file_name;

      BEGIN
         --v_error_file_handle := utl_file.fopen(location => p_file_path, filename => v_error_file_name, open_mode => 'w');
         v_error_file_handle :=
            UTL_FILE.fopen_nchar (location    => v_error_file_path,
                                  filename    => v_error_file_name,
                                  open_mode   => 'w');
         UTL_FILE.fclose (v_error_file_handle);
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      --v_error_file_handle := utl_file.fopen(location => p_file_path, filename => v_error_file_name, open_mode => 'w');
      v_error_file_handle :=
         UTL_FILE.fopen_nchar (location    => v_error_file_path,
                               filename    => v_error_file_name,
                               open_mode   => 'w');

      write_to_filenchar (v_error_file_handle, g_log_line);
      write_to_filenchar (
         v_error_file_handle,
            'Import data from file '
         || get_directory_path (p_file_path)
         || '/'
         || v_file_name);
      write_to_filenchar (v_error_file_handle, g_log_line);

      SAVEPOINT before_insert_data;

      LOOP
         BEGIN
            --utl_file.get_line(file => v_file_handle, buffer => v_line_data);
            UTL_FILE.get_line_nchar (file     => v_file_handle,
                                     buffer   => v_line_data);
            v_line_data :=
               REPLACE (REPLACE (v_line_data, CHR (10), ''), CHR (13), '');

            IF (v_line_data = 'END')
            THEN
               EXIT;
            END IF;

            v_line_count := v_line_count + 1;
            v_rec_data :=
               split_data (p_line_buff    => v_line_data,
                           p_delimiter    => p_delimiter,
                           p_debug_flag   => p_debug_flag);

            IF (v_rec_data.error_message IS NOT NULL)
            THEN
               v_insert_error_flag := 'Y';
               write_log (
                     'Line#'
                  || TO_CHAR (v_line_count, '999990')
                  || ' [ERR] =>  '
                  || v_line_data);
               write_output (
                     'Line#'
                  || TO_CHAR (v_line_count, '999990')
                  || ' [ERR] =>  '
                  || v_rec_data.error_message);
               write_output ('');

               write_to_filenchar (
                  v_error_file_handle,
                     'Line#'
                  || TO_CHAR (v_line_count, '999990')
                  || ' [ERR] =>  '
                  || v_rec_data.error_message);
               write_to_filenchar (v_error_file_handle, '');

               v_line_error := v_line_error + 1;
            ELSE
               BEGIN
                  v_rec_data.request_id := p_request_id;
                  v_rec_data.eai_crtd_dttm := v_date;
                  v_rec_data.source_file_name := v_file_name;
                  v_rec_data.status := 'NEW';
                  v_rec_data.DEFAULT_GROUP := 'SMT';

                  SELECT AP.XCUST_AP_SUPPLIERS_TEMP_INT_S.NEXTVAL
                    INTO v_seq_id
                    FROM DUAL;

                  v_rec_data.SEQ_ID := v_seq_id;

                  INSERT
                    INTO xcust_ap_suppliers_temp_debug (
                            vendor_number,
                            vendor_name,
                            vendor_name_alt,
                            TAX_ID_FLAG,
                            vat_registration_num,
                            vendor_type_lookup_code,
                            INVOICE_MATCH_OPTION,
                            terms_name,
                            pay_group_lookup_code,
                            payment_priority,
                            terms_date_basis,
                            pay_date_basis_lookup_code,
                            payment_method_lookup_code,
                            allow_awt_flag,
                            attribute1,
                            attribute2,
                            GLOBAL_ATTRIBUTE20,
                            GLOBAL_ATTRIBUTE18,
                            GLOBAL_ATTRIBUTE17,
                            GLOBAL_ATTRIBUTE15,
                            vendor_site_code,
                            VENDOR_SITE_ALT,
                            INACTIVE_SITE_DATE,
                            address_line1,
                            address_line2,
                            address_line3,
                            address_line4,
                            city,
                            province,
                            zip,
                            pay_site_flag,
                            purchasing_site_flag,
                            first_name,
                            last_name,
                            phone,
                            fax,
                            email_address,
                            SITE_LIABILITY_ACCOUNT,
                            SITE_PREPAYMENT_ACCOUNT,
                            SITE_INVOICE_MATCH_OPTION,
                            site_terms_name,
                            site_pay_group_lookup_code,
                            site_payment_priority,
                            site_terms_date_basis,
                            site_pay_date_basis_lookup_cod,
                            site_payment_method_lookup_cod,
                            SITE_ALLOW_WHT,
                            SITE_WHT_GROUP,
                            SITE_VAT_CODE,
                            site_attribute1,
                            site_attribute2,
                            SITE_ATTRIBUTE_CATEGORY,
                            SITE_ATTRIBUTE3,
                            SITE_ATTRIBUTE4,
                            SITE_ATTRIBUTE5,
                            SITE_ATTRIBUTE6,
                            SITE_ATTRIBUTE7,
                            SITE_GLOBAL_ATTRIBUTE20,
                            SITE_GLOBAL_ATTRIBUTE18,
                            SITE_GLOBAL_ATTRIBUTE17,
                            SITE_GLOBAL_ATTRIBUTE16,
                            SITE_GLOBAL_ATTRIBUTE15,
                            SITE_GLOBAL_ATTRIBUTE14,
                            SITE_GLOBAL_ATTRIBUTE13,
                            SITE_ATTRIBUTE12,
                            SITE_ATTRIBUTE13,
                            SITE_ATTRIBUTE14,
                            site_owner_name,
                            source_file_name,
                            DEFAULT_GROUP,
                            eai_crtd_dttm,
                            request_id,
                            status,
                            error_message,
                            branch_code,
                            org_id,
                            seq_id,
                            /*Add fields by spw@ice 22 Feb 2017*/
                            ctb_tax_invoice_require,
                            ctb_receipt_require,
                            ctb_bill_require,
                            country_site,
                            county_site,
                            alternate_address,
                            area_code_tel,
                            area_code_fax)
                  /*Add fields.*/
                  VALUES (REPLACE (v_rec_data.vendor_number, '?'),
                          v_rec_data.vendor_name,
                          v_rec_data.vendor_name_alt,
                          v_rec_data.TAX_ID_FLAG,
                          v_rec_data.vat_registration_num,
                          v_rec_data.vendor_type_lookup_code,
                          v_rec_data.INVOICE_MATCH_OPTION,
                          v_rec_data.terms_name,
                          v_rec_data.pay_group_lookup_code,
                          v_rec_data.payment_priority,
                          v_rec_data.terms_date_basis,
                          v_rec_data.pay_date_basis_lookup_code,
                          v_rec_data.payment_method_lookup_code,
                          v_rec_data.allow_awt_flag,
                          v_rec_data.attribute1,
                          --'???', NULL,
                          --v_rec_data.attribute1),
                          v_rec_data.attribute2,
                          --'???', NULL,
                          --v_rec_data.attribute2),
                          v_rec_data.GLOBAL_ATTRIBUTE20,
                          --'???', NULL,
                          --v_rec_data.GLOBAL_ATTRIBUTE20),
                          v_rec_data.GLOBAL_ATTRIBUTE18,
                          --'???', NULL,
                          --v_rec_data.GLOBAL_ATTRIBUTE18),
                          v_rec_data.GLOBAL_ATTRIBUTE17,
                          --'???', NULL,
                          --v_rec_data.GLOBAL_ATTRIBUTE17),
                          --DECODE (v_rec_data.GLOBAL_ATTRIBUTE15, '???', ''),
                          v_rec_data.GLOBAL_ATTRIBUTE15,
                          --TO_CHAR (v_date, 'DD-MON-YYYY HH24:MM:SS'),
                          v_rec_data.vendor_site_code,
                          v_rec_data.vendor_site_alt,
                          --'???', NULL,
                          --v_rec_data.vendor_site_alt),
                          v_rec_data.INACTIVE_SITE_DATE,
                          v_rec_data.address_line1,
                          --'???', NULL,
                          --v_rec_data.address_line1),
                          v_rec_data.address_line2,
                          --'???', NULL,
                          --v_rec_data.address_line2),
                          v_rec_data.address_line3,
                          --'???', NULL,
                          --v_rec_data.address_line3),
                          v_rec_data.address_line4,
                          --'???', NULL,
                          --v_rec_data.address_line4),
                          v_rec_data.city,
                          --'???', NULL,
                          --v_rec_data.city),
                          v_rec_data.province,
                          --'???', NULL,
                          --v_rec_data.province),
                          v_rec_data.zip,
                          --'???', NULL,
                          --v_rec_data.zip),
                          v_rec_data.pay_site_flag,
                          v_rec_data.purchasing_site_flag,
                          v_rec_data.first_name,
                          v_rec_data.last_name,
                          v_rec_data.phone,
                          --'???', NULL,
                          --v_rec_data.phone),
                          v_rec_data.fax,
                          --'???', NULL,
                          --v_rec_data.fax),
                          v_rec_data.email_address,
                          --'???', NULL,
                          --v_rec_data.email_address),
                          v_rec_data.SITE_LIABILITY_ACCOUNT,
                          v_rec_data.SITE_PREPAYMENT_ACCOUNT,
                          v_rec_data.SITE_INVOICE_MATCH_OPTION,
                          v_rec_data.site_terms_name,
                          v_rec_data.site_pay_group_lookup_code,
                          v_rec_data.site_payment_priority,
                          v_rec_data.site_terms_date_basis,
                          v_rec_data.site_pay_date_basis_lookup_cod,
                          v_rec_data.site_payment_method_lookup_cod,
                          v_rec_data.SITE_ALLOW_WHT,
                          v_rec_data.SITE_WHT_GROUP,
                          v_rec_data.SITE_VAT_CODE,
                          v_rec_data.site_attribute1,
                          --'???', NULL,
                          --v_rec_data.site_attribute1),
                          v_rec_data.site_attribute2,
                          --'???', NULL,
                          --v_rec_data.site_attribute2),
                          v_rec_data.SITE_ATTRIBUTE_CATEGORY,
                          --'???', NULL,
                          --v_rec_data.SITE_ATTRIBUTE_CATEGORY),
                          v_rec_data.SITE_ATTRIBUTE3,
                          --'???', NULL,
                          --v_rec_data.SITE_ATTRIBUTE3),
                          v_rec_data.SITE_ATTRIBUTE4,
                          --'???', NULL,
                          --v_rec_data.SITE_ATTRIBUTE4),
                          v_rec_data.SITE_ATTRIBUTE5,
                          --'???', NULL,
                          --v_rec_data.SITE_ATTRIBUTE5),
                          v_rec_data.SITE_ATTRIBUTE6,
                          --'???', NULL,
                          --v_rec_data.SITE_ATTRIBUTE6),
                          v_rec_data.SITE_ATTRIBUTE7,
                          --'???', NULL,
                          --v_rec_data.SITE_ATTRIBUTE7),
                          v_rec_data.SITE_GLOBAL_ATTRIBUTE20,
                          --'???', NULL,
                          --v_rec_data.SITE_GLOBAL_ATTRIBUTE20),
                          v_rec_data.SITE_GLOBAL_ATTRIBUTE18,
                          --'???', NULL,
                          --v_rec_data.SITE_GLOBAL_ATTRIBUTE18),
                          v_rec_data.SITE_GLOBAL_ATTRIBUTE17,
                          --'???', NULL,
                          --v_rec_data.SITE_GLOBAL_ATTRIBUTE17),
                          v_rec_data.SITE_GLOBAL_ATTRIBUTE16,
                          --'???', NULL,
                          --v_rec_data.SITE_GLOBAL_ATTRIBUTE16),
                          v_rec_data.SITE_GLOBAL_ATTRIBUTE15,
                          --'???', NULL,
                          --v_rec_data.SITE_GLOBAL_ATTRIBUTE15),
                          v_rec_data.SITE_GLOBAL_ATTRIBUTE14,
                          --'???', NULL,
                          --v_rec_data.SITE_GLOBAL_ATTRIBUTE14),
                          v_rec_data.SITE_GLOBAL_ATTRIBUTE13,
                          --'???', NULL,
                          --v_rec_data.SITE_GLOBAL_ATTRIBUTE13),
                          v_rec_data.SITE_ATTRIBUTE12,
                          --'???', NULL,
                          --v_rec_data.SITE_ATTRIBUTE12),
                          v_rec_data.SITE_ATTRIBUTE13,
                          --'???', NULL,
                          --v_rec_data.SITE_ATTRIBUTE13),
                          v_rec_data.SITE_ATTRIBUTE14,
                          --'???', NULL,
                          --v_rec_data.SITE_ATTRIBUTE14),
                          v_rec_data.site_owner_name,
                          --'???', NULL,
                          --v_rec_data.site_owner_name),
                          v_rec_data.source_file_name,
                          v_rec_data.DEFAULT_GROUP,
                          v_rec_data.eai_crtd_dttm,
                          v_rec_data.request_id,
                          v_rec_data.status,
                          v_rec_data.error_message,
                          v_rec_data.branch_code,
                          v_rec_data.org_id,
                          v_rec_data.seq_id,
                          /*Add fields by spw@ice 22 Feb 2017*/
                          v_rec_data.ctb_tax_invoice_require,
                          v_rec_data.ctb_receipt_require,
                          v_rec_data.ctb_bill_require,
                          v_rec_data.country_site,
                          v_rec_data.county_site,
                          v_rec_data.alternate_address,
                          v_rec_data.area_code_tel,
                          v_rec_data.area_code_fax);

                  /*Add fields.*/
                  v_line_success := v_line_success + 1;
                  write_log (
                        'Line#'
                     || TO_CHAR (v_line_count, '999990')
                     || ' [ OK] =>  '
                     || v_line_data);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     v_insert_error_flag := 'Y';
                     write_log (
                           'Line#'
                        || TO_CHAR (v_line_count, '999990')
                        || ' [ERR] =>  '
                        || v_line_data);
                     write_output (
                           'Line#'
                        || TO_CHAR (v_line_count, '999990')
                        || ' [ERR] =>  '
                        || NVL (v_rec_data.error_message, SQLERRM));
                     write_output ('');

                     write_to_filenchar (
                        v_error_file_handle,
                           'Line#'
                        || TO_CHAR (v_line_count, '999990')
                        || ' [ERR] =>  '
                        || NVL (v_rec_data.error_message, SQLERRM));
                     write_to_filenchar (v_error_file_handle, '');
               END;
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               EXIT;
         END;
      END LOOP;

      write_log (g_log_line);

      -- check open and close
      IF (UTL_FILE.is_open (file => v_file_handle))
      THEN
         UTL_FILE.fclose (file => v_file_handle);
      END IF;

      IF (v_insert_error_flag = 'Y')
      THEN
         IF (p_rollback_option = 'ALL')
         THEN
            ROLLBACK TO SAVEPOINT before_insert_data;
         ELSE
            COMMIT;
         END IF;

         write_log (
            'xcust_supplier_interface_smt.import_data exception when insert data. See output for error detail.');
         err_msg :=
            'Error @import_data : Some record error during insert to table. See output for detail.';

         IF v_line_count = v_line_error
         THEN
            err_code := '2';
         ELSE
            err_code := '1';
         END IF;

         move_data_file (
            get_directory_path (g_out_inbox) || '/' || v_file_name,
            get_directory_path (g_out_error));
      ELSE
         COMMIT;
         move_data_file (
            get_directory_path (g_out_inbox) || '/' || v_file_name,
            get_directory_path (g_out_history));
      END IF;

      IF (UTL_FILE.is_open (file => v_error_file_handle))
      THEN
         UTL_FILE.fclose (file => v_error_file_handle);
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         -- check open and close
         IF (UTL_FILE.is_open (file => v_file_handle))
         THEN
            UTL_FILE.fclose (file => v_file_handle);
         ELSE
            BEGIN
               ROLLBACK TO SAVEPOINT before_insert_data;
            EXCEPTION
               WHEN OTHERS
               THEN
                  NULL;
            END;
         END IF;

         IF (UTL_FILE.is_open (file => v_error_file_handle))
         THEN
            UTL_FILE.fclose (file => v_error_file_handle);
         END IF;

         write_log (
            'tac_supplier_interface_smt.import_data exception when others .');
         write_log ('Error @import_data : ' || SQLCODE || ' - ' || SQLERRM);

         err_msg := 'Error @import_data : ' || SQLCODE || ' - ' || SQLERRM;
         err_code := '2';
   END import_data;

   PROCEDURE validate_temp_table (err_msg             OUT VARCHAR2,
                                  err_code            OUT VARCHAR2,
                                  p_default_group         VARCHAR2,
                                  p_request_id            NUMBER,
                                  p_rollback_option       VARCHAR2,
                                  p_error_file_name       VARCHAR2)
   IS
      --pragma autonomous_transaction;

      CURSOR c_sup_temp (
         p_default_group    VARCHAR2,
         p_request_id       NUMBER)
      IS
           SELECT tmp.vendor_number,
                  tmp.vendor_name,
                  tmp.vendor_name_alt,
                  tmp.TAX_ID_FLAG,
                  tmp.vat_registration_num,
                  tmp.vendor_type_lookup_code,
                  tmp.INVOICE_MATCH_OPTION,
                  tmp.terms_name,
                  tmp.pay_group_lookup_code,
                  tmp.payment_priority,
                  tmp.terms_date_basis,
                  tmp.pay_date_basis_lookup_code,
                  tmp.payment_method_lookup_code,
                  tmp.allow_awt_flag,
                  tmp.attribute1,
                  tmp.attribute2,
                  tmp.global_attribute20,
                  tmp.global_attribute18,
                  tmp.global_attribute17,
                  tmp.global_attribute15,
                  tmp.vendor_site_code,
                  tmp.VENDOR_SITE_ALT,
                  tmp.INACTIVE_SITE_DATE,
                  tmp.address_line1,
                  tmp.address_line2,
                  tmp.address_line3,
                  tmp.address_line4,
                  tmp.city,
                  tmp.province,
                  tmp.zip,
                  tmp.pay_site_flag,
                  tmp.purchasing_site_flag,
                  tmp.first_name,
                  tmp.last_name,
                  tmp.phone,
                  tmp.fax,
                  tmp.email_address,
                  tmp.SITE_LIABILITY_ACCOUNT,
                  tmp.SITE_PREPAYMENT_ACCOUNT,
                  tmp.SITE_INVOICE_MATCH_OPTION,
                  tmp.site_terms_name,
                  tmp.site_pay_group_lookup_code,
                  tmp.site_payment_priority,
                  tmp.site_terms_date_basis,
                  tmp.site_pay_date_basis_lookup_cod,
                  tmp.site_payment_method_lookup_cod,
                  tmp.SITE_ALLOW_WHT,
                  tmp.SITE_WHT_GROUP,
                  tmp.SITE_VAT_CODE,
                  tmp.site_attribute1,
                  tmp.site_attribute2,
                  tmp.SITE_ATTRIBUTE_CATEGORY,
                  tmp.site_attribute3,
                  tmp.site_attribute4,
                  tmp.site_attribute5,
                  tmp.site_attribute6,
                  tmp.site_attribute7,
                  tmp.SITE_GLOBAL_ATTRIBUTE20,
                  tmp.SITE_GLOBAL_ATTRIBUTE18,
                  tmp.SITE_GLOBAL_ATTRIBUTE17,
                  tmp.SITE_GLOBAL_ATTRIBUTE16,
                  tmp.SITE_GLOBAL_ATTRIBUTE15,
                  tmp.SITE_GLOBAL_ATTRIBUTE14,
                  tmp.SITE_GLOBAL_ATTRIBUTE13,
                  tmp.site_attribute12,
                  tmp.site_attribute13,
                  tmp.site_attribute14,
                  tmp.site_owner_name,
                  tmp.source_file_name,
                  tmp.default_group,
                  tmp.eai_crtd_dttm,
                  tmp.request_id,
                  tmp.status,
                  tmp.error_message,
                  tmp.branch_code,
                  tmp.org_id,
                  tmp.ROWID row_id,
                  /*Add fields by spw@ice 22 Feb 2017*/
                  tmp.ctb_tax_invoice_require,
                  tmp.ctb_receipt_require,
                  tmp.ctb_bill_require,
                  tmp.country_site,
                  tmp.county_site,
                  tmp.alternate_address,
                  tmp.area_code_tel,
                  tmp.area_code_fax,
                  /*Add fields.*/
                  (SELECT    chk_dup.vendor_name
                          || ' ( '
                          || chk_dup.vendor_site_code
                          || ' )'
                          || ' ( '
                          || org.name
                          || ' )'
                     FROM xcust_ap_suppliers_temp_debug chk_dup,
                          hr_operating_units org
                    WHERE     chk_dup.org_id = org.organization_id
                          AND chk_dup.vendor_name = tmp.vendor_name
                          AND chk_dup.org_id = tmp.org_id
                          AND chk_dup.site_attribute4 = tmp.site_attribute4
                          AND chk_dup.seq_id <> tmp.seq_id
                          AND chk_dup.request_id = p_request_id --Add by spw@ice :To check in same file.
                          AND ROWNUM = 1)
                     site_bank_account_dup_temp,
                  (SELECT    dup_pv.vendor_name
                          || ' ( '
                          || dup_pvs.vendor_site_code
                          || ' )'
                     FROM po_vendors dup_pv, po_vendor_sites_all dup_pvs
                    WHERE     dup_pv.vendor_id = dup_pvs.vendor_id
                          AND dup_pv.end_date_active IS NULL
                          AND dup_pv.vendor_name = tmp.vendor_name
                          AND dup_pvs.vendor_site_code <> tmp.vendor_site_code
                          AND dup_pvs.attribute4 = tmp.site_attribute4
                          AND dup_pvs.org_id = tmp.org_id
                          AND ROWNUM = 1)
                     site_bank_account_dup_erp,
                  (SELECT chk_dup.vendor_name
                     FROM xcust_ap_suppliers_temp_debug chk_dup
                    WHERE     chk_dup.status = 'NEW'
                          AND chk_dup.vendor_name <> tmp.vendor_name
                          AND chk_dup.vat_registration_num =
                                 tmp.vat_registration_num
                          AND chk_dup.vendor_number IS NULL
                          AND ROWNUM = 1)
                     supp_vat_registration_dup_temp,
                  (SELECT chk_dup.vendor_name
                     FROM po_vendors chk_dup
                    WHERE     1 = 1
                          AND chk_dup.vendor_name = tmp.vendor_name
                          AND chk_dup.vat_registration_num =
                                 tmp.vat_registration_num
                          AND tmp.vendor_number IS NULL
                          AND ROWNUM = 1)
                     supp_vat_registration_dup_erp --,decode(tmp.attribute4, null, 1, decode(length(tmp.attribute4), 10, 1, 0)) supp_valid_bank_acc_len
                                                  ,
                  DECODE (tmp.site_attribute4,
                          NULL, 1,
                          DECODE (LENGTH (tmp.site_attribute4), 10, 1, 0))
                     site_valid_bank_acc_len --,decode(replace(translate(tmp.attribute4, '0123456789', '0000000000'), '0', ''), null, 1, 0) supp_valid_bank_acc_numberic
                                            ,
                  DECODE (
                     REPLACE (
                        TRANSLATE (tmp.site_attribute4,
                                   '0123456789',
                                   '0000000000'),
                        '0',
                        ''),
                     NULL, 1,
                     0)
                     site_valid_bank_acc_numberic,
                  DECODE (
                     REPLACE (
                        TRANSLATE (tmp.vat_registration_num,
                                   '0123456789',
                                   '0000000000'),
                        '0',
                        ''),
                     NULL, 1,
                     0)
                     validate_vat_regist_numeric,
                  DECODE (LENGTH (tmp.vat_registration_num),
                          10, 1,
                          13, 1,
                          0)
                     validate_vat_regist_len,
                  DECODE (
                     LENGTH (tmp.vat_registration_num),
                     13, DECODE (
                            SUBSTR (tmp.vat_registration_num, 1, 2),
                            '01', 1,
                            tac_validate_data.validate_supplier_tax (
                               tmp.vat_registration_num)),
                     1)
                     validate_vat_regist_pattern,
                  (  SELECT    chk_dup.vendor_name
                            || ' ( '
                            || chk_dup.vendor_site_code
                            || ' ) '
                       FROM xcust_ap_suppliers_temp_debug chk_dup
                      WHERE     1 = 1
                            AND chk_dup.request_id = tmp.request_id
                            AND chk_dup.vendor_name = tmp.vendor_name
                            AND chk_dup.vendor_site_code = tmp.vendor_site_code
                            AND chk_dup.vendor_number IS NULL
                            AND chk_dup.status = 'NEW'
                            AND chk_dup.org_id = tmp.org_id
                   GROUP BY chk_dup.vendor_name, chk_dup.vendor_site_code
                     HAVING COUNT (*) > 1)
                     supp_site_dup_rec_temp,
                  (  SELECT    chk_dup.vendor_name
                            || ' ( '
                            || chk_dup.vendor_site_code
                            || ' ) '
                       FROM xcust_ap_suppliers_temp_debug chk_dup
                      WHERE     1 = 1
                            AND chk_dup.request_id = tmp.request_id
                            AND chk_dup.vendor_name = tmp.vendor_name
                            AND chk_dup.vendor_site_code = tmp.vendor_site_code
                            AND chk_dup.org_id = tmp.org_id
                            AND chk_dup.vendor_number IS NULL
                            AND chk_dup.status = 'NEW'
                   GROUP BY chk_dup.vendor_name, chk_dup.vendor_site_code
                     HAVING COUNT (DISTINCT chk_dup.site_attribute4) > 1)
                     site_diff_bank_acc_temp -- ,decode(pv.vendor_id, null, null, decode(tmp.vendor_number, null, tmp.vendor_name, null)) supp_dup_vendor_name_erp
                                            ,
                  (SELECT DISTINCT segment1
                     FROM po_vendors pv
                    WHERE pv.segment1 = tmp.vendor_number)
                     dup_vendor_number,
                  (SELECT DISTINCT vendor_name
                     FROM po_vendors pv
                    WHERE REPLACE (TRIM (pv.vendor_name), ' ', '') =
                             REPLACE (TRIM (tmp.vendor_name), ' ', ''))
                     dup_vendor_name,
                  (SELECT VENDOR_SITE_CODE
                     FROM po_vendors pv, po_vendor_sites_all ps
                    WHERE     pv.vendor_id = ps.vendor_id
                          AND ps.vendor_site_code = tmp.vendor_site_code
                          AND ps.org_id = tmp.org_id
                          AND REPLACE (TRIM (pv.vendor_name), ' ', '') =
                                 REPLACE (TRIM (tmp.vendor_name), ' ', ''))
                     dup_vendor_site_number,
                  (SELECT DISTINCT VAT_REGISTRATION_NUM
                     FROM po_vendors pv
                    WHERE pv.VAT_REGISTRATION_NUM = tmp.VAT_REGISTRATION_NUM)
                     dup_VAT_REGISTRATION_NUM,
                  org.name org_name
             FROM xcust_ap_suppliers_temp_debug tmp, hr_operating_units org
            WHERE     1 = 1
                  AND tmp.status = 'NEW'
                  AND tmp.org_id = org.organization_id(+)
                  AND tmp.default_group = p_default_group
                  AND tmp.request_id = p_request_id
         ORDER BY tmp.vendor_name;

      limit_bulk            PLS_INTEGER := 1000;
      v_rec_error_flag      BOOLEAN := FALSE;
      v_rec_warn_flag       BOOLEAN := FALSE;
      v_validate_flag       BOOLEAN := FALSE;
      v_error_message       VARCHAR2 (1000);
      v_warn_message        VARCHAR2 (1000);
      v_line_buff           VARCHAR2 (4000);
      v_validate_total      NUMBER := 0;
      v_validate_error      NUMBER := 0;
      v_validate_pass       NUMBER := 0;

      v_temp_varchar250     VARCHAR2 (250);

      v_error_file_name     VARCHAR2 (100) := '';
      v_error_file_handle   UTL_FILE.file_type;
      v_error_line_data     NVARCHAR2 (32767);
      v_error_file_path     VARCHAR2 (100);

      v_process_start       DATE;
      v_process_finish      DATE;
      v_process_time        VARCHAR2 (20);
      v_record_flag         VARCHAR2 (10);

      v_dup_account_flag    NUMBER := 0;

      --Debug spw@ice 23 Feb 2017
      v_tax_regis_dup       NUMBER := 0;
      v_vendors_dup         NUMBER := 0;
   BEGIN
      v_process_start := SYSDATE;

      BEGIN
         --v_error_file_handle := utl_file.fopen(location => p_file_path, filename => v_error_file_name, open_mode => 'w');
         v_error_file_handle :=
            UTL_FILE.fopen_nchar (location    => g_out_error,
                                  filename    => p_error_file_name,
                                  open_mode   => 'a');
         UTL_FILE.fclose (v_error_file_handle);
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      --v_error_file_handle := utl_file.fopen(location => p_file_path, filename => v_error_file_name, open_mode => 'w');
      v_error_file_handle :=
         UTL_FILE.fopen_nchar (location    => g_out_error,
                               filename    => p_error_file_name,
                               open_mode   => 'a');

      write_output (g_log_line);
      write_output ('Validate supplier in temp table');
      write_output (g_log_line);

      write_to_filenchar (v_error_file_handle, g_log_line);
      write_to_filenchar (v_error_file_handle,
                          'Validate supplier in temp table');
      write_to_filenchar (v_error_file_handle, g_log_line);

      FOR r_sup_temp IN c_sup_temp (p_default_group, p_request_id)
      LOOP
         v_validate_total := v_validate_total + 1;
         v_error_message := NULL;
         v_warn_message := NULL;
         v_rec_error_flag := FALSE;
         v_rec_warn_flag := FALSE;

         v_line_buff :=
               r_sup_temp.vendor_name
            || '|'
            || r_sup_temp.vat_registration_num
            || '|'
            || r_sup_temp.pay_group_lookup_code
            || '|'
            || r_sup_temp.vendor_site_code
            || '|'
            || r_sup_temp.site_attribute4
            || '|'
            || r_sup_temp.org_id
            || '|'
            || r_sup_temp.site_pay_group_lookup_code;

         -----------------------------------------------------------------------
         -- validate bank account number of supplier
         -- validate bank account number duplication with another supplier
         -----------------------------------------------------------------------
         /* if (r_sup_temp.supp_bank_account_dup_temp is not null) then
         v_error_message := v_error_message || chr(10) || '[ERR] : Duplicated Bank Account Number in Temp Table between: ' || r_sup_temp.vendor_name ||
                ' and ' || r_sup_temp.supp_bank_account_dup_temp;
          end if;

          if (r_sup_temp.supp_bank_account_dup_erp is not null) then
         v_error_message := v_error_message || chr(10) || '[ERR] : Duplicated Bank Account Number with ERP between: ' || r_sup_temp.vendor_name ||
                ' and ' || r_sup_temp.supp_bank_account_dup_erp;
          end if;*/

         ------------------------------------------------------------------------------
         -- Validate Required fields
         ------------------------------------------------------------------------------
         IF (r_sup_temp.vendor_name IS NULL)
         THEN
            v_error_message :=
               v_error_message || CHR (10) || 'Vendor Name is required.';
         END IF;

         IF (r_sup_temp.vendor_type_lookup_code IS NULL)
         THEN
            v_error_message :=
               v_error_message || CHR (10) || 'Supplier Type is required.';
         END IF;

         IF (r_sup_temp.allow_awt_flag IS NULL)
         THEN
            v_error_message :=
               v_error_message || CHR (10) || 'Allow WHT is required.';
         END IF;

         IF (r_sup_temp.vendor_site_code IS NULL)
         THEN
            v_error_message :=
                  v_error_message
               || CHR (10)
               || 'Supplier Site Code is required.';
         END IF;

         IF (r_sup_temp.address_line1 IS NULL)
         THEN
            v_error_message :=
               v_error_message || CHR (10) || 'Address_Line1 is required.';
         END IF;

         IF (r_sup_temp.site_pay_group_lookup_code IS NULL)
         THEN
            v_error_message :=
                  v_error_message
               || CHR (10)
               || 'Site_Pay_Group_Lookup_Code is required.';
         END IF;

         IF (r_sup_temp.site_allow_wht IS NULL)
         THEN
            v_error_message :=
               v_error_message || CHR (10) || 'Site_Allow_Wht is required.';
         END IF;

         -----------------------------------------------------------------------
         -- validate bank account number of supplier site
         -- validate bank account number duplication with another supplier site
         -----------------------------------------------------------------------
         IF (r_sup_temp.site_bank_account_dup_temp IS NOT NULL)
         THEN
            v_error_message :=
                  v_error_message
               || CHR (10)
               || '[ERR] : Duplicated Bank Account Number Site.'
               || r_sup_temp.vendor_name
               || ' and '
               || r_sup_temp.site_bank_account_dup_temp;
         END IF;

         IF (r_sup_temp.site_bank_account_dup_erp IS NOT NULL)
         THEN
            v_error_message :=
                  v_error_message
               || CHR (10)
               || '[ERR] : Duplicated Bank Account Number Site with ERP between: '
               || r_sup_temp.vendor_name
               || ' and '
               || r_sup_temp.site_bank_account_dup_erp;
         END IF;

         -----------------------------------------------------------------------
         --Check Bank Account Over 10 digit in Temp Table
         -----------------------------------------------------------------------
         /* if (r_sup_temp.supp_valid_bank_acc_len = 0) then
         v_error_message := v_error_message || chr(10) || '[ERR] : Bank Account Number not 10 digits : ' || r_sup_temp.attribute4;
          end if;*/

         IF (r_sup_temp.site_valid_bank_acc_len = 0)
         THEN
            v_error_message :=
                  v_error_message
               || CHR (10)
               || '[ERR] : Bank Account Number Site not 10 digits : '
               || r_sup_temp.site_attribute4;
         END IF;



         ------------------------------------------------------------------------------
         -- Validate Duplicate record for new Supplier Site
         ------------------------------------------------------------------------------
         IF (r_sup_temp.supp_site_dup_rec_temp IS NOT NULL)
         THEN
            v_error_message :=
                  v_error_message
               || CHR (10)
               || '[ERR] : Duplicated Supplier Site record in Temp : '
               || r_sup_temp.supp_site_dup_rec_temp;
         END IF;

         ------------------------------------------------------------------------------
         -- Difference bank account for new supplier site
         ------------------------------------------------------------------------------
         IF (r_sup_temp.site_diff_bank_acc_temp IS NOT NULL)
         THEN
            v_error_message :=
                  v_error_message
               || CHR (10)
               || '[ERR] : Duplicated Bank Account for new Supplier Site in Temp : '
               || r_sup_temp.site_diff_bank_acc_temp;
         END IF;

         /**************** This here to validate account number ******************/
         IF LENGTH (NVL (r_sup_temp.site_attribute4, '')) > 0
         THEN
            BEGIN
               SELECT COUNT (*)
                 INTO v_dup_account_flag
                 FROM po_vendor_sites_all sit, po_vendors ven
                WHERE     sit.vendor_id = ven.vendor_id
                      AND TRIM (ven.vendor_name) =
                             TRIM (r_sup_temp.vendor_name)
                      AND sit.org_id = r_sup_temp.org_id
                      AND TRIM (sit.vendor_site_code) <>
                             TRIM (r_sup_temp.vendor_site_code)
                      AND TRIM (sit.attribute4) =
                             TRIM (r_sup_temp.site_attribute4);

               IF v_dup_account_flag <> 0
               THEN
                  v_error_message :=
                        v_error_message
                     || CHR (10)
                     || 'Bank Account Site : '
                     || TRIM (r_sup_temp.site_attribute4)
                     || ' is duplicate.';
               END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  v_error_message :=
                        v_error_message
                     || CHR (10)
                     || 'Check account duplicate error : '
                     || SQLERRM;
            END;
         END IF;

         ------------------------------------------------------------------------------
         -- Duplicated supplier number in erp
         ------------------------------------------------------------------------------
         IF (r_sup_temp.dup_vendor_number IS NOT NULL)
         THEN
            v_record_flag := 'Update';

            --Debug spw@ice 23 Feb 2017
            v_vendors_dup := 0;

            BEGIN
               SELECT COUNT (*)
                 INTO v_vendors_dup
                 FROM po_vendors
                WHERE CONCAT (CONCAT (segment1, vendor_name),
                              vat_registration_num) =
                         CONCAT (
                            CONCAT (TRIM (r_sup_temp.vendor_number),
                                    TRIM (r_sup_temp.vendor_name)),
                            TRIM (r_sup_temp.vat_registration_num));

               IF v_vendors_dup = 0
               THEN
                  v_error_message :=
                        v_error_message
                     || CHR (10)
                     || 'Error:  Supplier Code, Tax ID, Supplier Name mismatched to updated data.';
               END IF;
            END;

            IF r_sup_temp.dup_vat_registration_num IS NOT NULL
            THEN
               /*IF r_sup_temp.dup_vendor_site_number IS NOT NULL
               THEN
                  v_error_message :=
                        v_error_message
                     || CHR (10)
                     || '[ERR] : Supplier Number already exists in supplier Master of Source Supplier Number 1: '
                     || r_sup_temp.dup_vendor_number;
               ELSE*/
               IF (r_sup_temp.dup_vendor_name IS NULL)
               THEN
                  v_error_message :=
                        v_error_message
                     || CHR (10)
                     || '[ERR] : Supplier Number already exists in supplier Master of Source Supplier Number 2: '
                     || r_sup_temp.dup_vendor_number;
               ELSE
                  IF     r_sup_temp.First_name IS NOT NULL
                     AND r_sup_temp.Last_name IS NULL
                  THEN
                     v_error_message :=
                           v_error_message
                        || CHR (10)
                        || '[ERR] : Last Name cannot be null';
                  END IF;
               END IF;
            --END IF;
            /*ELSE
               v_error_message :=
                     v_error_message
                  || CHR (10)
                  || '[ERR] : Supplier Number already exists in supplier Master of Source Supplier Number : 3'
                  || r_sup_temp.dup_vendor_number;*/
            END IF;
         ELSE
            v_record_flag := 'Create';

            IF r_sup_temp.TAX_ID_FLAG = 'Y'
            THEN
               IF (r_sup_temp.vat_registration_num IS NOT NULL)
               THEN
                  -----------------------------------------------------------------------
                  --Check Vat Registeration length must 10 / 13 digits / null
                  -----------------------------------------------------------------------
                  IF (r_sup_temp.validate_vat_regist_len = 0)
                  THEN
                     v_error_message :=
                           v_error_message
                        || CHR (10)
                        || '[ERR] : Supplier Tax id should be 10 or 13 digits : '
                        || r_sup_temp.vat_registration_num;
                  END IF;

                  ------------------------------------------------------------------------------
                  -- Validate tax must numeric
                  ------------------------------------------------------------------------------
                  IF (r_sup_temp.validate_vat_regist_numeric = 0)
                  THEN
                     v_error_message :=
                           v_error_message
                        || CHR (10)
                        || '[ERR] : Supplier Tax id should be numeric : '
                        || r_sup_temp.vat_registration_num;
                  END IF;

                  ------------------------------------------------------------------------------
                  -- Validate tax pattern
                  ------------------------------------------------------------------------------
                  IF (r_sup_temp.validate_vat_regist_pattern = 0)
                  THEN
                     v_error_message :=
                           v_error_message
                        || CHR (10)
                        || '[ERR] : Supplier Tax id does not comply Central Civil-Registration : '
                        || r_sup_temp.vat_registration_num;
                  END IF;

                  IF     (NVL (r_sup_temp.validate_vat_regist_len, 0) != 0)
                     AND (NVL (r_sup_temp.validate_vat_regist_numeric, 0) !=
                             0)
                     AND (NVL (r_sup_temp.validate_vat_regist_pattern, 0) !=
                             0)
                  THEN
                     ------------------------------------------------------------------------------
                     -- Validate Vat Registration Duplicate for new Supplier
                     ------------------------------------------------------------------------------
                     IF (r_sup_temp.supp_vat_registration_dup_temp
                            IS NOT NULL)
                     THEN
                        v_error_message :=
                              v_error_message
                           || CHR (10)
                           || '[ERR] : Duplicated Supplier Tax id in Temp Table between: '
                           || r_sup_temp.vendor_name
                           || ' and '
                           || r_sup_temp.supp_vat_registration_dup_temp;
                     END IF;

                     v_tax_regis_dup := 0;

                     BEGIN
                        SELECT COUNT (*)
                          INTO v_tax_regis_dup
                          FROM po_vendors
                         WHERE TRIM (vat_registration_num) =
                                  TRIM (r_sup_temp.vat_registration_num);

                        IF v_tax_regis_dup > 0
                        THEN
                           v_error_message :=
                                 v_error_message
                              || CHR (10)
                              || '[ERR] : Duplicated Supplier Tax id in ERP';
                        END IF;
                     END;

                     IF    (r_sup_temp.supp_vat_registration_dup_erp IS NULL)
                        OR (r_sup_temp.supp_vat_registration_dup_temp
                               IS NOT NULL)
                     THEN
                        IF (r_sup_temp.dup_vendor_name IS NOT NULL)
                        THEN
                           v_error_message :=
                                 v_error_message
                              || CHR (10)
                              || '[ERR] : Supplier Name already exists in supplier Master of Source Supplier Name : '
                              || r_sup_temp.dup_vendor_name;
                        END IF;
                     END IF;
                  END IF;
               END IF;
            ELSE
               IF     (r_sup_temp.vat_registration_num IS NULL)
                  AND r_sup_temp.site_attribute2 IS NOT NULL
               THEN
                  IF (r_sup_temp.dup_vendor_name IS NOT NULL)
                  THEN
                     v_error_message :=
                           v_error_message
                        || CHR (10)
                        || '[ERR] : Supplier Name already exists in supplier Master of Source Supplier Name : '
                        || r_sup_temp.dup_vendor_name;
                  ELSE
                     ------------------------------------------------------------------------------
                     -- Validate First Name , Last Name
                     ------------------------------------------------------------------------------
                     IF     r_sup_temp.First_name IS NOT NULL
                        AND r_sup_temp.Last_name IS NULL
                     THEN
                        v_error_message :=
                              v_error_message
                           || CHR (10)
                           || '[ERR] : Last Name cannot be null';
                     END IF;
                  END IF;
               ELSE
                  v_error_message :=
                        v_error_message
                     || CHR (10)       --|| '[ERR] : Supplier Tax id is null';
                     || '[ERR] : Payee site name is null';
               END IF;
            END IF;
         END IF;

         ------------------------------------------------------------------------------
         -- Validate ORG_ID
         ------------------------------------------------------------------------------
         IF (r_sup_temp.org_name IS NULL)
         THEN
            v_error_message :=
               v_error_message || CHR (10) || '[ERR] : Invalid ORG_ID';
         END IF;



         ------------------------------------------------------------------------------
         -- Validate Vendor Type
         ------------------------------------------------------------------------------
         IF r_sup_temp.vendor_type_lookup_code IS NOT NULL
         THEN
            BEGIN
               SELECT flv.meaning
                 INTO v_temp_varchar250
                 FROM fnd_lookup_values flv
                WHERE     flv.lookup_type = 'VENDOR TYPE'
                      AND flv.enabled_flag = 'Y'
                      AND UPPER (flv.lookup_code) =
                             UPPER (r_sup_temp.vendor_type_lookup_code);
            EXCEPTION
               WHEN OTHERS
               THEN
                  v_error_message :=
                        v_error_message
                     || CHR (10)
                     || '[ERR] : Supplier Type ('
                     || r_sup_temp.vendor_type_lookup_code
                     || ') not found in ERP master.';
            END;
         END IF;

         ------------------------------------------------------------------------------
         -- Validate Supplier Pay Group
         ------------------------------------------------------------------------------
         /* if r_sup_temp.pay_group_lookup_code is not null then
         begin
           select flv.meaning
           into v_temp_varchar250
           from fnd_lookup_values flv
           where flv.lookup_type = 'PAY GROUP'
           and flv.lookup_code = r_sup_temp.pay_group_lookup_code;
         exception
           when others then
          v_error_message := v_error_message || chr(10) || '[ERR] : Supplier Pay group (' || r_sup_temp.pay_group_lookup_code ||
                 ') not found in ERP master.';
         end;
          else
         v_error_message := v_error_message || chr(10) || '[ERR] : Supplier Pay group empty.';
          end if;*/

         ------------------------------------------------------------------------------
         -- Validate Supplier Site Pay Group
         ------------------------------------------------------------------------------
         IF r_sup_temp.site_pay_group_lookup_code IS NOT NULL
         THEN
            BEGIN
               SELECT flv.meaning
                 INTO v_temp_varchar250
                 FROM fnd_lookup_values flv
                WHERE     flv.lookup_type = 'PAY GROUP'
                      AND flv.lookup_code =
                             r_sup_temp.site_pay_group_lookup_code;
            EXCEPTION
               WHEN OTHERS
               THEN
                  v_error_message :=
                        v_error_message
                     || CHR (10)
                     || '[ERR] : Supplier Site Pay group ('
                     || r_sup_temp.site_pay_group_lookup_code
                     || ') not found in ERP master.';
            END;
         ELSE
            v_error_message :=
                  v_error_message
               || CHR (10)
               || '[ERR] : Supplier Site Pay group empty.';
         END IF;

         ------------------------------------------------------------------------------
         -- Validate Supplier Payment Term
         ------------------------------------------------------------------------------
         /* if r_sup_temp.terms_name is not null then
         begin
           select at.name into v_temp_varchar250 from ap_terms at where at.name = r_sup_temp.terms_name;
         exception
           when others then
          v_error_message := v_error_message || chr(10) || '[ERR] : Supplier Payment term (' || r_sup_temp.terms_name ||
                 ') not found in ERP master.';
         end;
          else
         v_error_message := v_error_message || chr(10) || '[ERR] : Supplier Payment term empty';
          end if;

          ------------------------------------------------------------------------------
          -- Validate Supplier Site Payment Term
          ------------------------------------------------------------------------------
          if r_sup_temp.site_terms_name is not null then
         begin
           select at.name into v_temp_varchar250 from ap_terms at where at.name = r_sup_temp.site_terms_name;
         exception
           when others then
          v_error_message := v_error_message || chr(10) || '[ERR] : Supplier Site Payment term (' || r_sup_temp.site_terms_name ||
                 ') not found in ERP master.';
         end;
          else
         v_error_message := v_error_message || chr(10) || '[ERR] : Supplier Site Payment term empty';
          end if;
             */

         ------------------------------------------------------------------------------
         --  Get country short name if have country name
         ------------------------------------------------------------------------------
         /* SMT input short name
            if r_sup_temp.country is not null then
              begin
                select ft.territory_short_name into v_temp_varchar250 from fnd_territories_vl ft where ft.territory_code = r_sup_temp.country;
              exception
                when others then
                  v_error_message := v_error_message || chr(10) || '[ERR] : Country (' || r_sup_temp.country || ') not found in ERP master.';
              end;
            end if;
            */

         IF (v_warn_message IS NOT NULL OR v_error_message IS NOT NULL)
         THEN
            write_output (v_line_buff);
            UTL_FILE.put_line_nchar (file     => v_error_file_handle,
                                     buffer   => v_line_buff);
         END IF;

         IF (v_warn_message IS NOT NULL)
         THEN
            v_warn_message :=
               REPLACE (SUBSTR (v_warn_message, 2, 999),
                        CHR (10),
                        CHR (10) || '     ');
            write_output ('     ' || v_warn_message);
            UTL_FILE.put_line_nchar (file     => v_error_file_handle,
                                     buffer   => '     ' || v_warn_message);
            v_rec_warn_flag := TRUE;
         END IF;

         IF (v_error_message IS NOT NULL)
         THEN
            v_error_message :=
               REPLACE (SUBSTR (v_error_message, 2, 999),
                        CHR (10),
                        CHR (10) || '     ');
            write_output ('     ' || v_error_message);
            UTL_FILE.put_line_nchar (file     => v_error_file_handle,
                                     buffer   => '     ' || v_error_message);
            v_rec_error_flag := TRUE;
         END IF;

         /*
          v_process_finish := SYSDATE;
          v_process_time :=
             TO_CHAR (
                TO_TIMESTAMP (
                   TO_CHAR (
                        TO_DATE ('00:00:00', 'HH24:MI:SS')
                      + (v_process_finish - v_process_start),
                      'HH24:MI:SS'),
                   'HH24:MI:SS.FF'),
                'HH24:MI:SS.FF');
         */
         IF (NOT v_rec_error_flag)
         THEN
            --v_validate_pass := v_validate_pass + 1;

            UPDATE xcust_ap_suppliers_temp_debug tmp
               SET status = 'VALIDATED',
                   tmp.error_message = '',
                   update_flag = DECODE (v_record_flag, 'Update', 'Y', 'N')
             WHERE tmp.ROWID = r_sup_temp.row_id;
         /*
            INSERT INTO TACSMT_LOG_DETAIL (LEVEL_TYPE,
                                           PROCESS_START,
                                           PROCESS_FINISH,
                                           PROCESSING_TIME,
                                           ERROR_TYPE,
                                           REQUEST_ID,
                                           OPERATING_UNIT,
                                           SUPPLIER_NAME,
                                           SUPPLIER_SITE,
                                           RECORD_FLAG,
                                           SUPPLIER_NUMBER,
                                           ERROR_CODE,
                                           ERROR_MESSAGE,
                                           DATE_FROM)
                 VALUES ('D',
                         v_process_start,
                         v_process_finish,
                         v_process_time,
                         'SMT Supplier Interface',
                         p_request_id,
                         r_sup_temp.org_name,
                         r_sup_temp.vendor_name,
                         r_sup_temp.vendor_site_code,
                         v_record_flag,
                         r_sup_temp.vendor_number,
                         'Complete',
                         '',
                         SYSDATE);

            COMMIT;
            */
         ELSE
            --v_validate_error := v_validate_error + 1;

            UPDATE xcust_ap_suppliers_temp_debug tmp
               SET status = 'REJECTED',
                   tmp.error_message = SUBSTR (v_error_message, 1, 999),
                   update_flag = DECODE (v_record_flag, 'Update', 'Y', 'N')
             WHERE tmp.ROWID = r_sup_temp.row_id;
         /*
         INSERT INTO TACSMT_LOG_DETAIL (LEVEL_TYPE,
                                        PROCESS_START,
                                        PROCESS_FINISH,
                                        PROCESSING_TIME,
                                        ERROR_TYPE,
                                        REQUEST_ID,
                                        OPERATING_UNIT,
                                        SUPPLIER_NAME,
                                        SUPPLIER_SITE,
                                        RECORD_FLAG,
                                        SUPPLIER_NUMBER,
                                        ERROR_CODE,
                                        ERROR_MESSAGE,
                                        DATE_FROM)
              VALUES ('D',
                      v_process_start,
                      v_process_finish,
                      v_process_time,
                      'SMT Supplier Interface',
                      p_request_id,
                      r_sup_temp.org_name,
                      r_sup_temp.vendor_name,
                      r_sup_temp.vendor_site_code,
                      v_record_flag,
                      r_sup_temp.vendor_number,
                      'Error',
                      v_error_message,
                      SYSDATE);

         COMMIT;
         */
         END IF;
      END LOOP;

      write_log (g_log_line);
      write_log ('Validate Temp Table Summary');
      write_log ('Total Record : ' || v_validate_total);
      write_log ('Validate Pass : ' || v_validate_pass);
      write_log ('Validate Error : ' || v_validate_error);
      write_log (g_log_line);

      IF (v_validate_total = v_validate_pass)
      THEN
         err_code := '0';
         err_msg := '';
      ELSIF (v_validate_total = v_validate_error)
      THEN
         err_code := '2';
         err_msg := 'Have error in temp table, see output for error detail.';
      ELSE
         err_code := '1';
         err_msg :=
            'Have some error in temp table, see output for error detail.';
      END IF;

      IF (UTL_FILE.is_open (file => v_error_file_handle))
      THEN
         UTL_FILE.fclose (file => v_error_file_handle);
      END IF;
   --COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := '2';
         err_msg := SQLERRM;

         IF (UTL_FILE.is_open (file => v_error_file_handle))
         THEN
            UTL_FILE.fclose (file => v_error_file_handle);
         END IF;
   END validate_temp_table;

   PROCEDURE process_new_site_contact (p_default_group          VARCHAR2,
                                       p_request_id             NUMBER,
                                       p_vendor_name            VARCHAR2,
                                       p_vendor_interface_id    NUMBER,
                                       p_vendor_site_code       VARCHAR2,
                                       p_org_id                 NUMBER)
   IS
      CURSOR c_site_contact (
         p_default_group          VARCHAR2,
         p_request_id             NUMBER,
         p_vendor_name            VARCHAR2,
         p_vendor_interface_id    NUMBER,
         p_vendor_site_code       VARCHAR2,
         p_org_id                 NUMBER)
      IS
         SELECT tmp.ROWID rowid_temp,
                p_vendor_interface_id vendor_interface_id,
                tmp.vendor_name,
                tmp.vendor_site_code,
                tmp.org_id,
                tmp.first_name,
                tmp.last_name,
                REPLACE (tmp.phone, '-', '') phone,
                'NEW' status,
                tmp.email_address,
                REPLACE (tmp.fax, '-', '') fax,
                tmp.request_id,
                (SELECT DISTINCT vendor_site_id
                   FROM po_vendors pv, PO_VENDOR_SITES_ALL ps
                  WHERE     ps.vendor_id = pv.vendor_id
                        AND ps.vendor_site_code = tmp.VENDOR_SITE_CODE
                        AND ps.org_id = tmp.org_id
                        AND TRIM (pv.vendor_name) = TRIM (p_vendor_name))
                   vendor_site_id,
                tmp.area_code_tel,
                tmp.area_code_fax
           FROM xcust_ap_suppliers_temp_debug tmp
          WHERE     tmp.status = 'VALIDATED'
                AND (tmp.first_name IS NOT NULL OR tmp.last_name IS NOT NULL)
                AND tmp.default_group = p_default_group
                AND tmp.request_id = p_request_id
                AND TRIM (tmp.vendor_name) = TRIM (p_vendor_name)
                AND TRIM (tmp.vendor_site_code) = TRIM (p_vendor_site_code)
                AND tmp.org_id = p_org_id
                AND NOT EXISTS
                           (SELECT pvc.vendor_contact_id
                              FROM po_vendors pv,
                                   po_vendor_sites_all pvs,
                                   po_vendor_contacts pvc
                             WHERE     pv.vendor_id = pvs.vendor_id
                                   AND pvs.vendor_site_id =
                                          pvc.vendor_site_id
                                   AND TRIM (pv.vendor_name) =
                                          TRIM (tmp.vendor_name)
                                   AND pvs.vendor_site_code =
                                          tmp.vendor_site_code
                                   AND pvs.org_id = tmp.org_id
                                   AND (   TRIM (pvc.first_name) =
                                              TRIM (tmp.first_name)
                                        OR TRIM (pvc.last_name) =
                                              TRIM (tmp.last_name)));

      r_ap_sup_site_cont    ap_sup_site_contact_int%ROWTYPE;
      r_po_vendor_contact   po_vendor_contacts%ROWTYPE;
   BEGIN
      --New Supplier Site Contact
      FOR r_site_contact IN c_site_contact (p_default_group,
                                            p_request_id,
                                            p_vendor_name,
                                            p_vendor_interface_id,
                                            p_vendor_site_code,
                                            p_org_id)
      LOOP
         --r_ap_sup_site_cont.vendor_site_id := r_site_contact.vendor_site_id;
         r_ap_sup_site_cont.vendor_site_code :=
            r_site_contact.vendor_site_code;
         r_ap_sup_site_cont.org_id := r_site_contact.org_id;
         r_ap_sup_site_cont.first_name := r_site_contact.first_name;
         r_ap_sup_site_cont.last_name := r_site_contact.last_name;
         r_ap_sup_site_cont.phone := r_site_contact.phone;
         r_ap_sup_site_cont.status := r_site_contact.status;
         r_ap_sup_site_cont.email_address := r_site_contact.email_address;
         r_ap_sup_site_cont.fax := r_site_contact.fax;
         r_ap_sup_site_cont.contact_name_alt :=
            r_site_contact.vendor_interface_id;
         r_ap_sup_site_cont.request_id := r_site_contact.request_id;

         r_ap_sup_site_cont.area_code := r_site_contact.area_code_tel;
         r_ap_sup_site_cont.fax_area_code := r_site_contact.area_code_fax;

         insert_ap_supplier_site_cont (
            ir_ap_sup_site_cont => r_ap_sup_site_cont);

         UPDATE xcust_ap_suppliers_temp_debug tmp
            SET status = 'PROCESSED'
          WHERE tmp.ROWID = r_site_contact.rowid_temp;
      END LOOP;
   END process_new_site_contact;

   PROCEDURE process_exists_site_contact (p_default_group          VARCHAR2,
                                          p_request_id             NUMBER,
                                          p_vendor_name            VARCHAR2,
                                          p_vendor_interface_id    NUMBER,
                                          p_vendor_site_code       VARCHAR2,
                                          p_org_id                 NUMBER)
   IS
      CURSOR c_site_contact (
         p_default_group          VARCHAR2,
         p_request_id             NUMBER,
         p_vendor_name            VARCHAR2,
         p_vendor_interface_id    NUMBER,
         p_vendor_site_code       VARCHAR2,
         p_org_id                 NUMBER)
      IS
         SELECT tmp.ROWID rowid_temp,
                p_vendor_interface_id vendor_interface_id,
                tmp.vendor_name,
                tmp.vendor_site_code,
                tmp.org_id,
                tmp.first_name,
                tmp.last_name,
                REPLACE (tmp.phone, '-', '') phone,
                'NEW' status,
                tmp.email_address,
                REPLACE (tmp.fax, '-', '') fax,
                tmp.request_id,
                pv.vendor_id,
                pvs.vendor_site_id,
                pvc.vendor_contact_id,
                tmp.area_code_tel,
                tmp.area_code_fax
           FROM xcust_ap_suppliers_temp_debug tmp,
                po_vendors pv,
                po_vendor_sites_all pvs,
                po_vendor_contacts pvc
          WHERE     tmp.status = 'VALIDATED'
                AND TRIM (pv.vendor_name) = TRIM (tmp.vendor_name)
                AND pv.vendor_id = pvs.vendor_id
                AND pvs.vendor_site_id = pvc.vendor_site_id
                AND pvs.vendor_site_code = p_vendor_site_code
                AND pvs.org_id = p_org_id
                AND (   TRIM (pvc.first_name) = TRIM (tmp.first_name)
                     OR TRIM (pvc.last_name) = TRIM (tmp.last_name))
                AND (tmp.first_name IS NOT NULL OR tmp.last_name IS NOT NULL)
                AND tmp.default_group = p_default_group
                AND tmp.request_id = p_request_id
                AND TRIM (tmp.vendor_name) = TRIM (p_vendor_name)
                AND tmp.vendor_site_code = p_vendor_site_code
                AND tmp.org_id = p_org_id;

      r_po_vendor_contact       po_vendor_contacts%ROWTYPE;

      v_update_contact_status   BOOLEAN := FALSE;
   BEGIN
      NULL;

      --Existing Supplier Site Contact
      FOR r_site_contact IN c_site_contact (p_default_group,
                                            p_request_id,
                                            p_vendor_name,
                                            p_vendor_interface_id,
                                            p_vendor_site_code,
                                            p_org_id)
      LOOP
         r_po_vendor_contact := NULL;
         write_log (
            'Update Supplier site Contact : ' || r_site_contact.vendor_name);

         r_po_vendor_contact.vendor_site_id := r_site_contact.vendor_site_id;
         r_po_vendor_contact.vendor_contact_id :=
            r_site_contact.vendor_contact_id;
         r_po_vendor_contact.first_name := r_site_contact.first_name;
         r_po_vendor_contact.last_name := r_site_contact.last_name;
         r_po_vendor_contact.phone := r_site_contact.phone;
         r_po_vendor_contact.email_address := r_site_contact.email_address;
         r_po_vendor_contact.fax := r_site_contact.fax;
         r_po_vendor_contact.last_update_date := SYSDATE;

         r_po_vendor_contact.area_code := r_site_contact.area_code_tel;
         r_po_vendor_contact.fax_area_code := r_site_contact.area_code_fax;

         update_po_vendor_contacts (
            ir_po_vendor_contacts   => r_po_vendor_contact,
            o_status                => v_update_contact_status);

         UPDATE xcust_ap_suppliers_temp_debug tmp
            SET status = 'PROCESSED'
          WHERE tmp.ROWID = r_site_contact.rowid_temp;
      END LOOP;
   END process_exists_site_contact;

   PROCEDURE process_new_sup_site (
      p_default_group          VARCHAR2,
      p_request_id             NUMBER,
      p_vendor_name            VARCHAR2,
      p_vendor_interface_id    NUMBER,
      r_ap_supplier_default    dtac_suppliers_int_default_v%ROWTYPE)
   IS
      CURSOR c_supplier_site (
         p_default_group          VARCHAR2,
         p_request_id             NUMBER,
         p_vendor_name            VARCHAR2,
         p_vendor_interface_id    NUMBER)
      IS
           SELECT tmp.ROWID rowid_temp,
                  p_vendor_interface_id vendor_interface_id,
                  p_request_id request_id,
                  tmp.vendor_site_code,
                  tmp.VENDOR_SITE_ALT,
                  TO_DATE (tmp.INACTIVE_SITE_DATE, 'DDMMYYYY')
                     INACTIVE_SITE_DATE,
                  tmp.address_line1,
                  tmp.address_line2,
                  tmp.address_line3,
                  tmp.address_line4,
                  tmp.city,
                  tmp.zip,
                  tmp.province,
                  NVL (tmp.site_payment_method_lookup_cod, 'CHECK')
                     site_payment_method_lookup_cod,
                  NVL (tmp.site_terms_date_basis, 'Current')
                     site_terms_date_basis,
                  NVL (tmp.site_payment_priority, 99) site_payment_priority,
                  (SELECT term_id
                     FROM AP_TERMS_TL
                    WHERE name = NVL (tmp.site_terms_name, 'Immediately'))
                     term_id,
                  NVL (tmp.site_terms_name, 'Immediately') site_terms_name,
                  tmp.site_pay_group_lookup_code,
                  NVL (tmp.site_pay_date_basis_lookup_cod, 'DUE')
                     site_pay_date_basis_lookup_cod,
                  tmp.site_attribute1,
                  tmp.site_attribute2,
                  NVL (tmp.SITE_ATTRIBUTE_CATEGORY, 'CITIBANK')
                     SITE_ATTRIBUTE_CATEGORY,
                  tmp.site_attribute3,
                  tmp.site_attribute4,
                  tmp.site_attribute5,
                  tmp.site_attribute6,
                  tmp.site_attribute7,
                  tmp.org_id,
                  tmp.allow_awt_flag,
                  tmp.SITE_WHT_GROUP,
                  tmp.SITE_VAT_CODE,
                  (SELECT CODE_COMBINATION_ID
                     FROM GL_CODE_COMBINATIONS_KFV
                    WHERE CONCATENATED_SEGMENTS = TMP.SITE_LIABILITY_ACCOUNT)
                     ACCTS_PAY_CODE_COMBINATION_ID,
                  (SELECT CODE_COMBINATION_ID
                     FROM GL_CODE_COMBINATIONS_KFV
                    WHERE CONCATENATED_SEGMENTS = TMP.SITE_PREPAYMENT_ACCOUNT)
                     PREPAY_CODE_COMBINATION_ID,
                  (SELECT GROUP_ID
                     FROM AP_AWT_GROUPS
                    WHERE NAME = tmp.SITE_WHT_GROUP)
                     AWT_GROUP_ID,
                  'JA.TH.APXVDMVD.SITES' global_attribute_category,
                  tmp.vat_registration_num global_attribute18,
                  tmp.global_attribute20,
                  tmp.default_group global_attribute6,
                  'NEW' status,
                  --NVL (tmp.branch_code, '�ӹѡ�ҹ�˭�')
                  tmp.branch_code branch_code,
                  NVL (tmp.pay_site_flag, 'Y') pay_site_flag,
                  NVL (tmp.purchasing_site_flag, 'Y') purchasing_site_flag,
                  tmp.SITE_LIABILITY_ACCOUNT,
                  tmp.SITE_PREPAYMENT_ACCOUNT,
                  NVL (
                     DECODE (tmp.SITE_INVOICE_MATCH_OPTION,
                             'Purchase Order', 'P',
                             'Receipt', 'R'),
                     'P')
                     SITE_INVOICE_MATCH_OPTION,
                  tmp.SITE_GLOBAL_ATTRIBUTE20,
                  tmp.SITE_GLOBAL_ATTRIBUTE18,
                  tmp.SITE_GLOBAL_ATTRIBUTE17,
                  tmp.SITE_GLOBAL_ATTRIBUTE16,
                  tmp.SITE_GLOBAL_ATTRIBUTE15,
                  tmp.SITE_GLOBAL_ATTRIBUTE14,
                  tmp.SITE_GLOBAL_ATTRIBUTE13,
                  tmp.SITE_ATTRIBUTE12,
                  tmp.SITE_ATTRIBUTE13,
                  tmp.SITE_ATTRIBUTE14,
                  tmp.site_owner_name attribute15,
                  tmp.email_address,
                  tmp.vat_registration_num,
                  tmp.site_owner_name,
                  (SELECT vendor_site_id
                     FROM po_vendor_sites_all pvs
                    WHERE     pvs.vendor_id = pv.vendor_id
                          AND pvs.vendor_site_code = tmp.vendor_site_code
                          AND pvs.org_id = tmp.org_id)
                     erp_vendor_site_id,
                  pv.vendor_id,
                  /*Add fields by spw@ice 22 Feb 2017*/
                  tmp.ctb_tax_invoice_require,
                  tmp.ctb_receipt_require,
                  tmp.ctb_bill_require,
                  tmp.country_site,
                  tmp.county_site,
                  tmp.alternate_address,
                  tmp.area_code_tel,
                  tmp.area_code_fax
             /*Add fields.*/
             FROM xcust_ap_suppliers_temp_debug tmp,
                  hr_operating_units org,
                  po_vendors pv
            WHERE     tmp.status = 'VALIDATED'
                  AND tmp.org_id = org.organization_id(+)
                  AND tmp.vendor_number = pv.segment1(+)
                  AND tmp.default_group = p_default_group
                  AND tmp.request_id = p_request_id
                  AND TRIM (tmp.vendor_name) = TRIM (p_vendor_name)
         ORDER BY tmp.vendor_site_code;

      r_ap_sup_site   ap_supplier_sites_int%ROWTYPE;
   BEGIN
      FOR r_supplier_site IN c_supplier_site (p_default_group,
                                              p_request_id,
                                              p_vendor_name,
                                              p_vendor_interface_id)
      LOOP
         IF (r_supplier_site.erp_vendor_site_id IS NULL)
         THEN
            --New supplier site
            --Assign value from temp to ap_supplier_site
            r_ap_sup_site.vendor_interface_id :=
               r_supplier_site.vendor_interface_id;
            r_ap_sup_site.vendor_id := r_supplier_site.vendor_id;
            r_ap_sup_site.request_id := r_supplier_site.request_id;
            r_ap_sup_site.vendor_site_code := r_supplier_site.vendor_site_code;
            r_ap_sup_site.VENDOR_SITE_CODE_ALT :=
               r_supplier_site.VENDOR_SITE_ALT;
            r_ap_sup_site.INACTIVE_DATE := r_supplier_site.INACTIVE_SITE_DATE;
            r_ap_sup_site.address_line1 := r_supplier_site.address_line1;
            r_ap_sup_site.address_line2 := r_supplier_site.address_line2;
            r_ap_sup_site.address_line3 := r_supplier_site.address_line3;
            r_ap_sup_site.address_line4 := r_supplier_site.address_line4;
            r_ap_sup_site.city := r_supplier_site.city;
            r_ap_sup_site.zip := r_supplier_site.zip;
            r_ap_sup_site.province := r_supplier_site.province;
            r_ap_sup_site.payment_method_lookup_code :=
               r_supplier_site.site_payment_method_lookup_cod;
            r_ap_sup_site.terms_date_basis :=
               r_supplier_site.site_terms_date_basis;
            r_ap_sup_site.payment_priority :=
               r_supplier_site.site_payment_priority;

            r_ap_sup_site.terms_name :=
               NVL (r_supplier_site.site_terms_name,
                    r_ap_supplier_default.terms_name);
            r_ap_sup_site.pay_group_lookup_code :=
               NVL (r_supplier_site.site_pay_group_lookup_code,
                    r_ap_supplier_default.pay_group_lookup_code);

            r_ap_sup_site.pay_date_basis_lookup_code :=
               r_supplier_site.site_pay_date_basis_lookup_cod;
            r_ap_sup_site.attribute1 := r_supplier_site.site_attribute1;
            r_ap_sup_site.attribute2 := r_supplier_site.site_attribute2;
            r_ap_sup_site.org_id := r_supplier_site.org_id;
            r_ap_sup_site.allow_awt_flag := r_supplier_site.allow_awt_flag;
            r_ap_sup_site.AWT_GROUP_ID := r_supplier_site.AWT_GROUP_ID;
            r_ap_sup_site.AWT_GROUP_NAME := r_supplier_site.SITE_WHT_GROUP;
            r_ap_sup_site.VAT_CODE := r_supplier_site.SITE_VAT_CODE;
            r_ap_sup_site.global_attribute_category :=
               r_supplier_site.global_attribute_category; --'JA.TH.APXVDMVD.SITES';
            --r_ap_sup_site.global_attribute18 := r_supplier_site.vat_registration_num;
            r_ap_sup_site.global_attribute20 :=
               r_supplier_site.site_global_attribute20;
            r_ap_sup_site.global_attribute18 :=
               r_supplier_site.site_global_attribute18;
            r_ap_sup_site.global_attribute17 :=
               r_supplier_site.site_global_attribute17;
            r_ap_sup_site.global_attribute16 :=
               r_supplier_site.site_global_attribute16;
            r_ap_sup_site.global_attribute15 :=
               r_supplier_site.site_global_attribute15;
            r_ap_sup_site.global_attribute14 :=
               r_supplier_site.site_global_attribute14;
            r_ap_sup_site.global_attribute13 :=
               r_supplier_site.site_global_attribute13;

            r_ap_sup_site.global_attribute6 := p_default_group;
            r_ap_sup_site.status := 'NEW';

            --r_ap_sup_site.global_attribute14 := r_supplier_site.branch_code; @fix by spw@ice  27/12/2016
            -- NVL (r_supplier_site.branch_code, '�ӹѡ�ҹ�˭�');

            --if null get from default
            r_ap_sup_site.pay_site_flag :=
               NVL (r_supplier_site.pay_site_flag,
                    r_ap_supplier_default.pay_site_flag);
            r_ap_sup_site.purchasing_site_flag :=
               NVL (r_supplier_site.purchasing_site_flag,
                    r_ap_supplier_default.purchase_site_flag);

            /*r_ap_sup_site.accts_pay_code_combination_id := nvl(r_supplier_site.ACCTS_PAY_CODE_COMBINATION_ID, r_ap_supplier_default.accts_pay_ccid);
            r_ap_sup_site.prepay_code_combination_id := nvl(r_supplier_site.PREPAY_CODE_COMBINATION_ID, r_ap_supplier_default.prepay_ccid);
            r_ap_sup_site.future_dated_payment_ccid := nvl(r_ap_supplier_default.future_dated_payment_ccid
                         ,r_ap_supplier_default.future_dated_payment_ccid);*/
            r_ap_sup_site.accts_pay_code_combination_id :=
               r_supplier_site.ACCTS_PAY_CODE_COMBINATION_ID;
            r_ap_sup_site.prepay_code_combination_id :=
               r_supplier_site.PREPAY_CODE_COMBINATION_ID;
            -- end if null get from default

            r_ap_sup_site.MATCH_OPTION :=
               r_supplier_site.SITE_INVOICE_MATCH_OPTION;
            r_ap_sup_site.attribute12 := r_supplier_site.site_attribute12;
            r_ap_sup_site.attribute13 := r_supplier_site.site_attribute13;
            r_ap_sup_site.attribute14 := r_supplier_site.site_attribute14;
            r_ap_sup_site.attribute15 := r_supplier_site.site_owner_name;
            r_ap_sup_site.attribute3 := r_supplier_site.SITE_ATTRIBUTE3; --nvl(r_supplier_site.SITE_ATTRIBUTE3, r_ap_supplier_default.attribute3);
            r_ap_sup_site.attribute4 := r_supplier_site.SITE_ATTRIBUTE4; --nvl(r_supplier_site.SITE_ATTRIBUTE4, r_ap_supplier_default.attribute4);


            IF r_supplier_site.site_attribute_category IS NULL
            THEN
               r_supplier_site.site_attribute_category := g_citibank;
            END IF;

            r_ap_sup_site.attribute_category :=
               NVL (r_supplier_site.site_attribute_category, g_citibank);

            IF r_supplier_site.site_attribute_category = g_citibank
            THEN
               --'CITIBANK'
               r_ap_sup_site.attribute5 :=
                  NVL (r_supplier_site.SITE_ATTRIBUTE5,
                       r_ap_supplier_default.attribute5);
               r_ap_sup_site.attribute6 :=
                  NVL (r_supplier_site.SITE_ATTRIBUTE6,
                       r_ap_supplier_default.attribute6);
               r_ap_sup_site.attribute7 :=
                  NVL (r_supplier_site.SITE_ATTRIBUTE7,
                       r_ap_supplier_default.attribute7);

               r_ap_sup_site.attribute8 :=
                  r_supplier_site.ctb_tax_invoice_require;
               r_ap_sup_site.attribute9 := r_supplier_site.ctb_receipt_require;
               r_ap_sup_site.attribute10 := r_supplier_site.ctb_bill_require;
               r_ap_sup_site.country :=
                  NVL (r_supplier_site.country_site, 'TH');
               r_ap_sup_site.county := r_supplier_site.county_site;
               r_ap_sup_site.address_lines_alt :=
                  r_supplier_site.alternate_address;
            ELSIF r_supplier_site.site_attribute_category = g_kbank
            THEN
               --'KBANK'
               r_ap_sup_site.attribute5 :=
                  NVL (r_supplier_site.vendor_site_code, '');
               r_ap_sup_site.attribute6 :=
                  NVL (r_supplier_site.address_line1, '');
               r_ap_sup_site.attribute7 :=
                  NVL (r_supplier_site.address_line2, '');
               r_ap_sup_site.attribute8 :=
                  NVL (r_supplier_site.address_line3, '');
               r_ap_sup_site.attribute9 :=
                  NVL (r_supplier_site.address_line4, '');
               r_ap_sup_site.attribute10 := NVL (r_supplier_site.city, '');
            END IF;

            r_ap_sup_site.email_address := r_supplier_site.email_address;

            insert_ap_supplier_site_int (ir_ap_sup_site => r_ap_sup_site);
         END IF;

         --New Supplier Site Contact

         process_new_site_contact (p_default_group,
                                   p_request_id,
                                   p_vendor_name,
                                   p_vendor_interface_id,
                                   r_supplier_site.vendor_site_code,
                                   r_supplier_site.org_id);

         UPDATE xcust_ap_suppliers_temp_debug tmp
            SET status = 'PROCESSED'
          WHERE tmp.ROWID = r_supplier_site.rowid_temp;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         write_log ('PROCESS_NEW_SUP_SITE Error : ' || SQLERRM);
   END process_new_sup_site;

   PROCEDURE process_exists_sup_site (p_default_group          VARCHAR2,
                                      p_request_id             NUMBER,
                                      p_vendor_name            VARCHAR2,
                                      p_vendor_interface_id    NUMBER)
   IS
      CURSOR c_supplier_site (
         p_default_group          VARCHAR2,
         p_request_id             NUMBER,
         p_vendor_name            VARCHAR2,
         p_vendor_interface_id    NUMBER)
      IS
           SELECT tmp.ROWID rowid_temp,
                  p_vendor_interface_id vendor_interface_id,
                  p_request_id request_id,
                  tmp.vendor_name,
                  tmp.vendor_site_code,
                  TO_DATE (tmp.INACTIVE_SITE_DATE, 'DDMMYYYY')
                     INACTIVE_SITE_DATE,
                  (SELECT term_id
                     FROM AP_TERMS_TL
                    WHERE name = NVL (tmp.site_terms_name, 'Immediately'))
                     term_id,
                  NVL (tmp.site_terms_name, 'Immediately') site_terms_name,
                  tmp.site_pay_group_lookup_code,
                  tmp.address_line1,
                  tmp.address_line2,
                  tmp.address_line3,
                  tmp.address_line4,
                  tmp.city,
                  tmp.zip,
                  tmp.province,
                  NVL (tmp.pay_site_flag, 'Y') pay_site_flag,
                  NVL (tmp.purchasing_site_flag, 'Y') purchasing_site_flag,
                  (SELECT CODE_COMBINATION_ID
                     FROM GL_CODE_COMBINATIONS_KFV
                    WHERE CONCATENATED_SEGMENTS = TMP.SITE_LIABILITY_ACCOUNT)
                     ACCTS_PAY_CODE_COMBINATION_ID,
                  (SELECT CODE_COMBINATION_ID
                     FROM GL_CODE_COMBINATIONS_KFV
                    WHERE CONCATENATED_SEGMENTS = TMP.SITE_PREPAYMENT_ACCOUNT)
                     PREPAY_CODE_COMBINATION_ID,
                  NVL (
                     DECODE (tmp.SITE_INVOICE_MATCH_OPTION,
                             'Purchase Order', 'P',
                             'Receipt', 'R'),
                     'P')
                     SITE_INVOICE_MATCH_OPTION,
                  NVL (tmp.site_payment_priority, 99) site_payment_priority,
                  NVL (tmp.site_terms_date_basis, 'Current')
                     site_terms_date_basis,
                  NVL (tmp.site_pay_date_basis_lookup_cod, 'DUE')
                     site_pay_date_basis_lookup_cod,
                  NVL (tmp.site_payment_method_lookup_cod, 'CHECK')
                     site_payment_method_lookup_cod,
                  --DECODE (tmp.ALLOW_AWT_FLAG, 'N', 'N', tmp.SITE_ALLOW_WHT)
                  tmp.SITE_ALLOW_WHT SITE_ALLOW_WHT,
                  (SELECT GROUP_ID
                     FROM AP_AWT_GROUPS
                    WHERE NAME = tmp.SITE_WHT_GROUP)
                     AWT_GROUP_ID,
                  tmp.SITE_WHT_GROUP,
                  tmp.SITE_VAT_CODE,
                  tmp.SITE_attribute1,
                  tmp.SITE_attribute2,
                  NVL (tmp.SITE_ATTRIBUTE_CATEGORY, 'CITIBANK')
                     SITE_ATTRIBUTE_CATEGORY,
                  tmp.SITE_attribute3,
                  tmp.SITE_attribute4,
                  tmp.SITE_attribute5,
                  tmp.SITE_attribute6,
                  tmp.SITE_attribute7,
                  tmp.VENDOR_SITE_ALT,
                  tmp.SITE_attribute12                      --Mail to Address1
                                      ,
                  tmp.SITE_attribute13                      --Mail to Address2
                                      ,
                  tmp.SITE_attribute14                      --Mail to Address3
                                      ,
                  tmp.SITE_OWNER_NAME                             --Owner Name
                                     ,
                  tmp.site_global_attribute20,
                  tmp.site_global_attribute18,
                  tmp.site_global_attribute17,
                  tmp.site_global_attribute16,
                  tmp.site_global_attribute15,
                  tmp.site_global_attribute14,
                  tmp.site_global_attribute13,
                  tmp.org_id,
                  tmp.vat_registration_num,
                  'JA.TH.APXVDMVD.SITES' global_attribute_category,
                  tmp.default_group global_attribute6,
                  'NEW' status,
                  --NVL (tmp.branch_code, '�ӹѡ�ҹ�˭�')
                  tmp.branch_code branch_code,
                  tmp.email_address,
                  tmp.default_group,
                  pv.vendor_id erp_vendor_id,
                  pvs.vendor_site_id erp_vendor_site_id,
                  tmp.seq_id,
                  /*Add fields by spw@ice 22 Feb 2017*/
                  tmp.ctb_tax_invoice_require,
                  tmp.ctb_receipt_require,
                  tmp.ctb_bill_require,
                  tmp.country_site,
                  tmp.county_site,
                  tmp.alternate_address,
                  tmp.area_code_tel,
                  tmp.area_code_fax
             /*Add fields.*/
             FROM xcust_ap_suppliers_temp_debug tmp,
                  hr_operating_units org,
                  po_vendors pv,
                  po_vendor_sites_all pvs
            WHERE     tmp.status = 'VALIDATED'
                  AND tmp.org_id = org.organization_id
                  AND tmp.vendor_number = pv.segment1
                  AND tmp.vendor_site_code = pvs.vendor_site_code
                  AND tmp.org_id = pvs.org_id
                  AND pv.vendor_id = pvs.vendor_id
                  AND tmp.default_group = p_default_group
                  AND tmp.request_id = p_request_id
                  AND tmp.vendor_name = p_vendor_name
         ORDER BY tmp.vendor_site_code, tmp.seq_id;

      r_po_vendor_sites      po_vendor_sites_all%ROWTYPE;
      v_update_vendor_site   BOOLEAN := FALSE;
   BEGIN
      FOR r_supplier_site IN c_supplier_site (p_default_group,
                                              p_request_id,
                                              p_vendor_name,
                                              p_vendor_interface_id)
      LOOP
         IF (r_supplier_site.erp_vendor_site_id IS NOT NULL)
         THEN
            r_po_vendor_sites := NULL;
            r_po_vendor_sites.vendor_id := r_supplier_site.erp_vendor_id;
            r_po_vendor_sites.vendor_site_id :=
               r_supplier_site.erp_vendor_site_id;
            r_po_vendor_sites.global_attribute6 :=
               r_supplier_site.default_group;
            r_po_vendor_sites.INACTIVE_DATE :=
               r_supplier_site.INACTIVE_SITE_DATE;
            r_po_vendor_sites.terms_id := r_supplier_site.term_id;
            r_po_vendor_sites.pay_group_lookup_code :=
               r_supplier_site.SITE_PAY_GROUP_LOOKUP_CODE;

            r_po_vendor_sites.address_line1 := r_supplier_site.address_line1;
            r_po_vendor_sites.address_line2 := r_supplier_site.address_line2;
            r_po_vendor_sites.address_line3 := r_supplier_site.address_line3;
            r_po_vendor_sites.address_line4 := r_supplier_site.address_line4;
            r_po_vendor_sites.city := r_supplier_site.city;
            r_po_vendor_sites.zip := r_supplier_site.zip;
            r_po_vendor_sites.province := r_supplier_site.province;
            r_po_vendor_sites.PAY_SITE_FLAG := r_supplier_site.PAY_SITE_FLAG;
            r_po_vendor_sites.PURCHASING_SITE_FLAG :=
               r_supplier_site.PURCHASING_SITE_FLAG;

            r_po_vendor_sites.ACCTS_PAY_CODE_COMBINATION_ID :=
               r_supplier_site.ACCTS_PAY_CODE_COMBINATION_ID;
            r_po_vendor_sites.PREPAY_CODE_COMBINATION_ID :=
               r_supplier_site.PREPAY_CODE_COMBINATION_ID;
            r_po_vendor_sites.MATCH_OPTION :=
               r_supplier_site.SITE_INVOICE_MATCH_OPTION;
            r_po_vendor_sites.PAYMENT_PRIORITY :=
               r_supplier_site.SITE_PAYMENT_PRIORITY;
            r_po_vendor_sites.TERMS_DATE_BASIS :=
               r_supplier_site.SITE_TERMS_DATE_BASIS;
            r_po_vendor_sites.PAY_DATE_BASIS_LOOKUP_CODE :=
               r_supplier_site.SITE_PAY_DATE_BASIS_LOOKUP_COD;
            r_po_vendor_sites.PAYMENT_METHOD_LOOKUP_CODE :=
               r_supplier_site.SITE_PAYMENT_METHOD_LOOKUP_COD;
            r_po_vendor_sites.ALLOW_AWT_FLAG := r_supplier_site.SITE_ALLOW_WHT;
            r_po_vendor_sites.AWT_GROUP_ID := r_supplier_site.AWT_GROUP_ID;
            r_po_vendor_sites.VAT_CODE := r_supplier_site.SITE_VAT_CODE;

            r_po_vendor_sites.attribute1 := r_supplier_site.site_attribute1;
            r_po_vendor_sites.attribute2 := r_supplier_site.site_attribute2;
            r_po_vendor_sites.ATTRIBUTE_CATEGORY :=
               r_supplier_site.site_ATTRIBUTE_CATEGORY;
            r_po_vendor_sites.attribute3 := r_supplier_site.site_attribute3;
            r_po_vendor_sites.attribute4 := r_supplier_site.site_attribute4;
            r_po_vendor_sites.attribute5 := r_supplier_site.site_attribute5;
            r_po_vendor_sites.attribute6 := r_supplier_site.site_attribute6;
            r_po_vendor_sites.attribute7 := r_supplier_site.site_attribute7;
            r_po_vendor_sites.vat_registration_num :=
               r_supplier_site.vat_registration_num;
            r_po_vendor_sites.global_attribute20 :=
               r_supplier_site.site_global_attribute20;
            r_po_vendor_sites.global_attribute18 :=
               r_supplier_site.site_global_attribute18;
            r_po_vendor_sites.global_attribute17 :=
               r_supplier_site.site_global_attribute17;
            r_po_vendor_sites.global_attribute16 :=
               r_supplier_site.site_global_attribute16;
            r_po_vendor_sites.global_attribute15 :=
               r_supplier_site.site_global_attribute15;
            r_po_vendor_sites.global_attribute14 :=
               r_supplier_site.site_global_attribute14;
            r_po_vendor_sites.global_attribute13 :=
               r_supplier_site.site_global_attribute13;



            r_po_vendor_sites.vendor_site_code_alt :=
               r_supplier_site.VENDOR_SITE_ALT;
            r_po_vendor_sites.attribute12 := r_supplier_site.site_attribute12;
            r_po_vendor_sites.attribute13 := r_supplier_site.site_attribute13;
            r_po_vendor_sites.attribute14 := r_supplier_site.site_attribute14;
            r_po_vendor_sites.attribute15 := r_supplier_site.site_owner_name;
            --r_po_vendor_sites.vat_registration_num := r_supplier_site.vat_registration_num;


            r_po_vendor_sites.last_update_date := SYSDATE;

            r_po_vendor_sites.email_address := r_supplier_site.email_address;
            r_po_vendor_sites.global_attribute14 :=
               r_supplier_site.site_global_attribute14;

            --r_supplier_site.branch_code;

            --NVL (r_supplier_site.branch_code, '�ӹѡ�ҹ�˭�');

            --
            IF r_supplier_site.site_pay_group_lookup_code IS NOT NULL
            THEN
               r_po_vendor_sites.pay_group_lookup_code :=
                  r_supplier_site.site_pay_group_lookup_code;
            ELSE
               BEGIN
                  SELECT pay_group_lookup_code
                    INTO r_po_vendor_sites.pay_group_lookup_code
                    FROM po_vendor_sites_all
                   WHERE vendor_site_id = r_supplier_site.erp_vendor_site_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     fnd_file.put_line (
                        fnd_file.LOG,
                           'Pay Group Lookup Code of Vendor Site Name : '
                        || r_supplier_site.vendor_site_code
                        || ' Not Found in Vendor.');
               END;
            END IF;

            write_log (
                  'Update Supplier site : '
               || r_supplier_site.vendor_name
               || '( '
               || r_supplier_site.vendor_site_code
               || ' ) ');

            write_log ('SEQ ID : ' || r_supplier_site.seq_id);

            r_po_vendor_sites.vendor_site_code :=
               r_supplier_site.vendor_site_code;

            IF r_supplier_site.site_attribute_category = g_citibank
            THEN
               r_po_vendor_sites.attribute8 :=
                  r_supplier_site.ctb_tax_invoice_require;
               r_po_vendor_sites.attribute9 :=
                  r_supplier_site.ctb_receipt_require;
               r_po_vendor_sites.attribute10 :=
                  r_supplier_site.ctb_bill_require;
            END IF;

            r_po_vendor_sites.country :=
               NVL (r_supplier_site.country_site, 'TH');
            r_po_vendor_sites.county := r_supplier_site.county_site;
            r_po_vendor_sites.address_lines_alt :=
               r_supplier_site.alternate_address;
            write_log ('country_site [assign] : ' || NVL (r_supplier_site.country_site, 'TH'));
            write_log ('county_site [assign] : ' || r_supplier_site.county_site);
            update_po_vendor_sites (
               ir_po_vendor_sites   => r_po_vendor_sites,
               o_status             => v_update_vendor_site);
         END IF;

         --New Supplier Site Contact
         process_new_site_contact (p_default_group,
                                   p_request_id,
                                   p_vendor_name,
                                   p_vendor_interface_id,
                                   r_supplier_site.vendor_site_code,
                                   r_supplier_site.org_id);

         process_exists_site_contact (p_default_group,
                                      p_request_id,
                                      p_vendor_name,
                                      p_vendor_interface_id,
                                      r_supplier_site.vendor_site_code,
                                      r_supplier_site.org_id);

         UPDATE xcust_ap_suppliers_temp_debug tmp
            SET status = 'PROCESSED'
          WHERE tmp.ROWID = r_supplier_site.rowid_temp;
      END LOOP;
   END process_exists_sup_site;

   PROCEDURE send_supplier_info_back (
      p_default_group       VARCHAR2,
      p_file_name_format    VARCHAR2,
      p_file_name           VARCHAR2 DEFAULT NULL,
      p_delimiter           VARCHAR2 DEFAULT '|',
      p_request_id          NUMBER)
   IS
      CURSOR c_supplier (
         p_request_id NUMBER)
      IS
           SELECT a.segment1 vendor_num,
                  tmp.vendor_name,
                  tmp.vat_registration_num,
                  tmp.address_line1,
                  tmp.address_line2,
                  tmp.address_line3,
                  tmp.address_line4,
                  tmp.city,
                  tmp.province,
                  tmp.zip
             FROM xcust_ap_suppliers_temp_debug tmp, ap_suppliers_int a
            WHERE     tmp.request_id = p_request_id
                  AND tmp.request_id = a.request_id
                  AND tmp.vendor_name = a.vendor_name
                  AND tmp.vat_registration_num = a.vat_registration_num
                  AND tmp.status = 'PROCESSED'
         ORDER BY 1;

      v_date          DATE := SYSDATE;
      v_file_name     VARCHAR2 (100) := '';
      v_file_handle   UTL_FILE.file_type;
      v_line_data     NVARCHAR2 (32767);
      v_file_path     VARCHAR2 (100) := g_in_inbox;

      v_line_count    NUMBER := 0;
      v_address       VARCHAR2 (2000);
   BEGIN
      IF (p_file_name IS NOT NULL)
      THEN
         v_file_name := REPLACE (p_file_name, '_OUT_', '_IN_');
      ELSE
         IF p_file_name_format IS NULL
         THEN
            RAISE UTL_FILE.invalid_filename;
         END IF;

         v_file_name :=
            REPLACE (
               REPLACE (p_file_name_format,
                        '$DATE$',
                        TRIM (TO_CHAR (v_date, 'YYMMDD'))),
               '_OUT_',
               '_IN_');
      END IF;

      BEGIN
         --v_file_handle := utl_file.fopen(location => v_file_path, filename => v_file_name, open_mode => 'w');
         v_file_handle :=
            UTL_FILE.fopen_nchar (location    => v_file_path,
                                  filename    => v_file_name,
                                  open_mode   => 'w');
         UTL_FILE.fclose (v_file_handle);
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      --v_file_handle := utl_file.fopen(location => v_file_path, filename => v_file_name, open_mode => 'w');
      v_file_handle :=
         UTL_FILE.fopen_nchar (location    => v_file_path,
                               filename    => v_file_name,
                               open_mode   => 'w');
      COMMIT;

      FOR r_sup IN c_supplier (p_request_id)
      LOOP
         v_line_count := v_line_count + 1;
         v_line_data :=
               r_sup.vendor_num
            || p_delimiter
            || r_sup.vendor_name
            || p_delimiter
            || r_sup.vat_registration_num;

         IF (r_sup.address_line1 IS NOT NULL)
         THEN
            v_address := v_address || ' ' || r_sup.address_line1;
         END IF;

         IF (r_sup.address_line2 IS NOT NULL)
         THEN
            v_address := v_address || ' ' || r_sup.address_line2;
         END IF;

         IF (r_sup.address_line3 IS NOT NULL)
         THEN
            v_address := v_address || ' ' || r_sup.address_line3;
         END IF;

         IF (r_sup.address_line4 IS NOT NULL)
         THEN
            v_address := v_address || ' ' || r_sup.address_line4;
         END IF;

         IF (r_sup.city IS NOT NULL)
         THEN
            v_address := v_address || ' ' || r_sup.city;
         END IF;

         IF (r_sup.province IS NOT NULL)
         THEN
            v_address := v_address || ' ' || r_sup.province;
         END IF;

         IF (r_sup.zip IS NOT NULL)
         THEN
            v_address := v_address || ' ' || r_sup.zip;
         END IF;

         IF (v_address IS NOT NULL)
         THEN
            v_line_data := v_line_data || p_delimiter || TRIM (v_address);
         END IF;

         write_to_filenchar (v_file_handle, v_line_data);
      END LOOP;
   EXCEPTION
      WHEN UTL_FILE.invalid_filename
      THEN
         write_log (
            'tac_supplier_interface_smt.send_supplier_info_back exception when others .');
         write_log_error (
               '@send_supplier_info_back : '
            || 'Invalid file name / file name format.');
      WHEN OTHERS
      THEN
         -- check open and close
         IF (UTL_FILE.is_open (file => v_file_handle))
         THEN
            UTL_FILE.fclose (file => v_file_handle);
         END IF;

         write_log (
            'tac_supplier_interface_smt.send_supplier_info_back exception when others .');
         write_log_error ('@send_supplier_info_back : ' || SQLERRM);
   END send_supplier_info_back;

   /*
   p_rollback_option = 'ALL' , 'RECORD'
   */
   PROCEDURE interface_vendor (
      err_msg              OUT VARCHAR2,
      err_code             OUT VARCHAR2,
      p_default_group          VARCHAR2,
      p_file_path              VARCHAR2,
      p_file_name_format       VARCHAR2,
      p_file_name              VARCHAR2 DEFAULT NULL,
      p_delimiter              VARCHAR2 DEFAULT '|',
      p_debug_flag             VARCHAR2,
      p_rollback_option        VARCHAR2)
   IS
      --Fetch group
      CURSOR c_get_supplier_default (p_default_group VARCHAR2)
      IS
         SELECT row_id,
                default_group,
                accts_pay_ccid,
                prepay_ccid,
                future_dated_payment_ccid,
                terms_id,
                pay_group_lookup_code,
                payment_priority,
                terms_date_basis,
                pay_date_basis_lookup_code,
                payment_method_lookup_code,
                invoice_currency_code,
                pay_site_flag,
                purchase_site_flag,
                attribute5,
                attribute6,
                attribute7,
                attribute8,
                attribute9,
                attribute10,
                attribute11,
                last_update_date,
                last_updated_by,
                last_update_login,
                creation_date,
                created_by,
                terms_name,
                pay_group_name,
                client_code_desc
           FROM dtac_suppliers_int_default_v dasid
          WHERE dasid.default_group = p_default_group;

      --Fetch vendor header
      CURSOR c_supplier (
         p_default_group    VARCHAR2,
         p_request_id       NUMBER)
      IS
           SELECT DISTINCT
                  tmp.vendor_name,
                  tmp.vendor_name_alt,
                  '' VENDOR_NUMBER                         --tmp.VENDOR_NUMBER
                                  ,
                  tmp.TAX_ID_FLAG,
                  UPPER (tmp.vendor_type_lookup_code) vendor_type_lookup_code,
                  NVL (
                     DECODE (tmp.INVOICE_MATCH_OPTION,
                             'Purchase Order', 'P',
                             'Receipt', 'R'),
                     'P')
                     INVOICE_MATCH_OPTION,
                  org.set_of_books_id set_of_books_id,
                  tmp.request_id v_request_id,
                  tmp.vat_registration_num,
                  NVL (tmp.allow_awt_flag, 'Y') allow_awt_flag,
                  'JA.TH.APXVDMVD.PO_VENDORS' global_attribute_category,
                  tmp.default_group,
                  tmp.attribute1,
                  tmp.attribute2                             --,tmp.attribute3
                                                             --,tmp.attribute4
                  ,
                  tmp.global_attribute20,
                  tmp.global_attribute18,
                  tmp.global_attribute17,
                  tmp.global_attribute15,
                  'NEW' status                     -- if null get from default
                              ,
                  (SELECT term_id
                     FROM AP_TERMS_TL
                    WHERE name = NVL (tmp.TERMS_NAME, 'Immediately'))
                     term_id,
                  NVL (tmp.terms_name, 'Immediately') terms_name,
                  tmp.pay_group_lookup_code,
                  NVL (tmp.payment_priority, 99) payment_priority,
                  NVL (tmp.terms_date_basis, 'Current') terms_date_basis,
                  NVL (tmp.pay_date_basis_lookup_code, 'DUE')
                     pay_date_basis_lookup_code,
                  NVL (tmp.payment_method_lookup_code, 'CHECK')
                     payment_method_lookup_code   --,tmp.invoice_currency_code
                                               ,
                  g_citibank attribute_category                             --
                                               ,
                  pv.vendor_id,
                  tmp.SITE_ATTRIBUTE12,
                  tmp.SITE_ATTRIBUTE13,
                  tmp.SITE_ATTRIBUTE14,
                  tmp.site_owner_name,
                  TMP.VENDOR_SITE_CODE,
                  TMP.ORG_ID,
                  (SELECT vendor_site_id
                     FROM PO_VENDOR_SITES_ALL ps
                    WHERE     ps.vendor_site_code = tmp.VENDOR_SITE_CODE
                          AND ps.vendor_id = pv.vendor_id
                          AND ps.org_id = tmp.org_id)
                     vendor_site_id,
                  tmp.seq_id,
                  /*Add fields by spw@ice 22 Feb 2017*/
                  tmp.ctb_tax_invoice_require,
                  tmp.ctb_receipt_require,
                  tmp.ctb_bill_require,
                  tmp.country_site,
                  tmp.county_site,
                  tmp.alternate_address,
                  tmp.area_code_tel,
                  tmp.area_code_fax
             /*Add fields.*/
             FROM xcust_ap_suppliers_temp_debug tmp,
                  hr_operating_units org,
                  po_vendors pv
            WHERE     tmp.status = 'VALIDATED'
                  AND tmp.org_id = org.organization_id(+)
                  AND tmp.vendor_number = pv.segment1(+)
                  AND tmp.default_group = p_default_group
                  AND tmp.request_id = p_request_id
         ORDER BY tmp.vendor_name, tmp.seq_id;

      r_ap_suppliers_int               ap_suppliers_int%ROWTYPE;
      r_ap_sup_site                    ap_supplier_sites_int%ROWTYPE;
      r_ap_sup_site_cont               ap_sup_site_contact_int%ROWTYPE;
      r_ap_supplier_default            dtac_suppliers_int_default_v%ROWTYPE;
      r_ap_sup_to_sts                  dtac_ap_suppliers_to_sts%ROWTYPE;
      r_dtac_ap_sup_output             dtac_ap_suppliers_output%ROWTYPE;
      r_po_vendors                     po_vendors%ROWTYPE;
      r_po_vendor_sites                po_vendor_sites_all%ROWTYPE;
      r_po_vendor_contact              po_vendor_contacts%ROWTYPE;

      v_user_id                        NUMBER;
      v_conc_request_id                NUMBER;
      v_vendor_interface_id            NUMBER;

      v_update_sup_status              BOOLEAN;
      v_update_sup_site_status         BOOLEAN;
      v_update_site_cont_status        BOOLEAN;

      v_supplier_api_request_id        NUMBER;
      v_supplier_site_api_request_id   NUMBER;
      v_site_cont_api_request_id       NUMBER;

      v_error_file_name                VARCHAR2 (100);
      v_import_status                  VARCHAR2 (10);

      v_process_start                  DATE;
      v_process_finish                 DATE;
      v_process_time                   VARCHAR2 (20);
      c_process                        NUMBER;
      c_reject                         NUMBER;
      c_total                          NUMBER;
      v_cont_req_id                    NUMBER;

      v_supplier_name_buff             VARCHAR2 (100);
   BEGIN
      v_process_start := SYSDATE;
      err_code := '0';

      v_user_id := fnd_profile.VALUE ('USER_ID');
      v_conc_request_id := fnd_profile.VALUE ('CONC_REQUEST_ID');

      OPEN c_get_supplier_default (p_default_group);

      FETCH c_get_supplier_default INTO r_ap_supplier_default;

      CLOSE c_get_supplier_default;

      -- Import supplier data from text file to temp table
      write_log (
         'Import data start : ' || TO_CHAR (SYSDATE, 'dd-mm-yyyy hh24:mi:ss'));
      import_data (err_msg              => err_msg,
                   err_code             => err_code,
                   p_default_group      => NVL (p_default_group, 'SMT'),
                   p_file_path          => p_file_path,
                   p_file_name_format   => p_file_name_format,
                   p_file_name          => p_file_name,
                   p_delimiter          => p_delimiter,
                   p_debug_flag         => p_debug_flag,
                   p_user_id            => v_user_id,
                   p_request_id         => v_conc_request_id,
                   p_rollback_option    => p_rollback_option,
                   o_error_file_name    => v_error_file_name);
      write_log (
            'Import data finished : '
         || TO_CHAR (SYSDATE, 'dd-mm-yyyy hh24:mi:ss'));
      -- Check import status
      v_import_status := err_code;

      /* ----------spw@ice debug-------------- */
      IF (NVL (err_code, '99') = '2')
      THEN
         RETURN;
      END IF;

      -- Validate data in temp table
      write_log (
            'Validate data start : '
         || TO_CHAR (SYSDATE, 'dd-mm-yyyy hh24:mi:ss'));

      validate_temp_table (err_msg             => err_msg,
                           err_code            => err_code,
                           p_default_group     => p_default_group,
                           p_request_id        => v_conc_request_id,
                           p_rollback_option   => p_rollback_option,
                           p_error_file_name   => v_error_file_name);
      write_log (
            'Validate data finished : '
         || TO_CHAR (SYSDATE, 'dd-mm-yyyy hh24:mi:ss'));
      COMMIT;

      -- Query supplier that validated
      write_log (
            'Supplier loop start : '
         || TO_CHAR (SYSDATE, 'dd-mm-yyyy hh24:mi:ss'));

      v_supplier_name_buff := '';

      FOR r_sup IN c_supplier (p_default_group, v_conc_request_id)
      LOOP
         IF (r_sup.vendor_id IS NULL)
         THEN
            --Assign value from temp to ap_suppliers_int
            r_ap_suppliers_int.vendor_name := r_sup.vendor_name;
            r_ap_suppliers_int.vendor_name_alt := r_sup.vendor_name_alt;
            r_ap_suppliers_int.segment1 := r_sup.VENDOR_NUMBER;
            r_ap_suppliers_int.vendor_type_lookup_code :=
               r_sup.vendor_type_lookup_code;
            r_ap_suppliers_int.match_option := r_sup.INVOICE_MATCH_OPTION;
            r_ap_suppliers_int.set_of_books_id := r_sup.set_of_books_id;
            r_ap_suppliers_int.request_id := v_conc_request_id;
            r_ap_suppliers_int.vat_registration_num :=
               r_sup.vat_registration_num;
            r_ap_suppliers_int.allow_awt_flag := r_sup.allow_awt_flag;
            r_ap_suppliers_int.global_attribute_category :=
               'JA.TH.APXVDMVD.PO_VENDORS';
            r_ap_suppliers_int.global_attribute6 := r_sup.default_group;
            r_ap_suppliers_int.attribute1 := r_sup.attribute1;
            r_ap_suppliers_int.attribute2 := r_sup.attribute2;
            --r_ap_suppliers_int.attribute3 := r_sup.attribute3;
            --r_ap_suppliers_int.attribute4 := r_sup.attribute4;
            r_ap_suppliers_int.global_attribute20 := r_sup.global_attribute20;
            r_ap_suppliers_int.global_attribute18 := r_sup.global_attribute18;
            r_ap_suppliers_int.global_attribute17 := r_sup.global_attribute17;
            r_ap_suppliers_int.global_attribute15 := r_sup.global_attribute15;
            r_ap_suppliers_int.status := 'NEW';

            -- Global Attribute

            --if null get from default
            r_ap_suppliers_int.TERMS_ID := r_sup.term_id;
            r_ap_suppliers_int.terms_name :=
               NVL (r_sup.terms_name, r_ap_supplier_default.terms_name);
            r_ap_suppliers_int.pay_group_lookup_code :=
               NVL (r_sup.pay_group_lookup_code,
                    r_ap_supplier_default.pay_group_lookup_code);

            r_ap_suppliers_int.payment_priority :=
               NVL (r_sup.payment_priority,
                    r_ap_supplier_default.payment_priority);
            r_ap_suppliers_int.terms_date_basis :=
               NVL (r_sup.terms_date_basis,
                    r_ap_supplier_default.terms_date_basis);
            r_ap_suppliers_int.pay_date_basis_lookup_code :=
               NVL (r_sup.pay_date_basis_lookup_code,
                    r_ap_supplier_default.pay_date_basis_lookup_code);
            r_ap_suppliers_int.payment_method_lookup_code :=
               NVL (r_sup.payment_method_lookup_code,
                    r_ap_supplier_default.payment_method_lookup_code);
            r_ap_suppliers_int.invoice_currency_code := 'THB';
            r_ap_suppliers_int.payment_currency_code := 'THB';

            write_log (
                  ':: vendor_name : '
               || r_sup.vendor_name
               || ' || v_supplier_name_buff : '
               || v_supplier_name_buff
               || ' ) ');

            --Insert new supplier header when other vendor name.
            --Fixed for dupplicate vendor header.
            IF r_sup.vendor_name != NVL (v_supplier_name_buff, 'xxx')
            THEN
               write_log (
                     'Before :: vendor_name : '
                  || r_sup.vendor_name
                  || ' || v_supplier_name_buff : '
                  || v_supplier_name_buff
                  || ' ) ');

               -- new supplier
               SELECT ap_suppliers_int_s.NEXTVAL
                 INTO v_vendor_interface_id
                 FROM DUAL;

               r_ap_suppliers_int.vendor_interface_id := v_vendor_interface_id;
               v_supplier_name_buff := r_ap_suppliers_int.vendor_name;

               write_log (
                     'After :: vendor_name : '
                  || r_sup.vendor_name
                  || ' || v_supplier_name_buff : '
                  || v_supplier_name_buff
                  || ' ) ');

               insert_ap_suppliers_int (
                  ir_ap_suppliers_int => r_ap_suppliers_int);
            END IF;

            --Insert new supplier site

            process_new_sup_site (
               p_default_group         => p_default_group,
               p_request_id            => v_conc_request_id,
               p_vendor_name           => r_sup.vendor_name,
               p_vendor_interface_id   => v_vendor_interface_id,
               r_ap_supplier_default   => r_ap_supplier_default);

            UPDATE xcust_ap_suppliers_temp_debug tmp
               SET vendor_interface_id = v_vendor_interface_id
             WHERE tmp.seq_id = r_sup.seq_id;

            COMMIT;
         ELSE
            --======================================   update =====================================---
            -- Old supplier
            write_log (
                  'Update Supplier : '
               || r_sup.vendor_name
               || ' ( '
               || r_sup.vendor_id
               || ' ) ');

            SELECT ap_suppliers_int_s.NEXTVAL
              INTO v_vendor_interface_id
              FROM DUAL;

            r_po_vendors := NULL;
            r_po_vendors.vendor_id := r_sup.vendor_id;

            r_ap_suppliers_int.vendor_type_lookup_code :=
               r_sup.vendor_type_lookup_code;
            r_ap_suppliers_int.invoice_currency_code := 'THB';
            r_ap_suppliers_int.PAYMENT_CURRENCY_CODE := 'THB';
            r_ap_suppliers_int.allow_awt_flag := r_sup.allow_awt_flag;
            r_po_vendors.global_attribute6 := r_sup.default_group;
            r_po_vendors.attribute2 := r_sup.attribute1;
            r_po_vendors.attribute2 := r_sup.attribute2;
            r_ap_suppliers_int.global_attribute20 := r_sup.global_attribute20;
            r_ap_suppliers_int.global_attribute18 := r_sup.global_attribute18;
            r_ap_suppliers_int.global_attribute17 := r_sup.global_attribute17;
            r_ap_suppliers_int.global_attribute15 := r_sup.global_attribute15;
            r_po_vendors.last_update_date := SYSDATE;
            v_update_sup_status := FALSE;

            r_po_vendors.vendor_type_lookup_code :=
               r_sup.vendor_type_lookup_code;
            r_po_vendors.allow_awt_flag := r_sup.allow_awt_flag;

            IF r_sup.VENDOR_SITE_CODE IS NULL AND r_sup.ORG_ID IS NULL
            THEN
               --write_log ('Update supplier type code 1: ' || r_sup.vendor_type_lookup_code);
               update_po_vendors (ir_po_vendors   => r_po_vendors,
                                  o_status        => v_update_sup_status);
            ELSE
               --write_log ('Update supplier type code 2: ' || r_sup.vendor_type_lookup_code);
               update_po_vendors (ir_po_vendors   => r_po_vendors,
                                  o_status        => v_update_sup_status);

               IF r_sup.vendor_site_id IS NULL
               THEN
                  process_new_sup_site (
                     p_default_group         => p_default_group,
                     p_request_id            => v_conc_request_id,
                     p_vendor_name           => r_sup.vendor_name,
                     p_vendor_interface_id   => v_vendor_interface_id,
                     r_ap_supplier_default   => r_ap_supplier_default);
               ELSE
                  process_exists_sup_site (
                     p_default_group         => p_default_group,
                     p_request_id            => v_conc_request_id,
                     p_vendor_name           => r_sup.vendor_name,
                     p_vendor_interface_id   => v_vendor_interface_id);
               END IF;
            END IF;

            UPDATE xcust_ap_suppliers_temp_debug tmp
               SET vendor_interface_id = v_vendor_interface_id
             WHERE tmp.seq_id = r_sup.seq_id;

            COMMIT;
         END IF;

         UPDATE xcust_ap_suppliers_temp_debug tmp
            SET status = 'PROCESSED',
                vendor_interface_id = v_vendor_interface_id
          WHERE     tmp.status = 'VALIDATED'
                AND tmp.vendor_name = r_sup.vendor_name
                AND NVL (tmp.vat_registration_num, '99') =
                       NVL (r_sup.vat_registration_num, '99')
                AND tmp.request_id = v_conc_request_id
                AND tmp.default_group = r_sup.default_group;

         COMMIT;
      END LOOP;

      write_log (
            'Supplier loop finished : '
         || TO_CHAR (SYSDATE, 'dd-mm-yyyy hh24:mi:ss'));

      write_log (g_log_line);
      write_log (
            'Interface API start : '
         || TO_CHAR (SYSDATE, 'dd-mm-yyyy hh24:mi:ss'));

      submit_interface_api (o_sup_req_id        => v_supplier_api_request_id,
                            o_site_req_id       => v_supplier_site_api_request_id,
                            o_cont_req_id       => v_site_cont_api_request_id,
                            p_conc_request_id   => v_conc_request_id);

      write_log_info ('Submit Interface API');
      write_log_info (
         'Supplier Interface request ID : ' || v_supplier_api_request_id);
      write_log_info (
            'Supplier Site Interface request ID : '
         || v_supplier_site_api_request_id);
      write_log_info (
            'Supplier Site Contact Interface request ID : '
         || v_site_cont_api_request_id);
      write_log (
            'Interface API Finished: '
         || TO_CHAR (SYSDATE, 'dd-mm-yyyy hh24:mi:ss'));
      write_log (g_log_line);

      send_supplier_info_back (p_default_group      => p_default_group,
                               p_file_name_format   => p_file_name_format,
                               p_file_name          => p_file_name,
                               p_delimiter          => p_delimiter,
                               p_request_id         => v_conc_request_id);

      IF (v_import_status != '0')
      THEN
         IF (err_code != '2')
         THEN
            err_code := v_import_status;
         END IF;
      END IF;

      create_logs (v_conc_request_id);
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := '2';
         err_msg := SQLERRM;
   END interface_vendor;

   PROCEDURE create_logs (p_concurrent_id IN NUMBER)
   IS
      l_today_timestamp           TIMESTAMP := SYSTIMESTAMP;
      p_end_date                  DATE;
      p_start_date                DATE;
      v_cont_req_id               NUMBER;


      l_program_name              VARCHAR2 (50 BYTE) := 'SMT004';
      l_record_count_completion   NUMBER := 0;
      l_record_count_failed       NUMBER := 0;
      l_record_count_total        NUMBER := 0;

      l_hour                      NUMBER;
      l_minute                    NUMBER;
      l_second                    NUMBER;

      v_process_start             DATE;
      v_process_finish            DATE;
      v_process_time              VARCHAR2 (20);

      CURSOR c_line (
         i_request_id NUMBER)
      IS
         SELECT org.name org_name,
                inf.status status_inf,
                inf.reject_code,
                DECODE (
                   tmp.update_flag,
                   'N', DECODE (
                           tmp.status,
                           'PROCESSED', DECODE (inf.status,
                                                'REJECTED', 'REJECTED',
                                                'PROCESSED'),
                           tmp.status),
                   tmp.status)
                   status_flag,
                DECODE (
                   tmp.update_flag,
                   'N', DECODE (
                           tmp.status,
                           'PROCESSED', DECODE (
                                           inf.status,
                                           'REJECTED',    'Standard Error : '
                                                       || REPLACE (
                                                             REPLACE (
                                                                inf.reject_code,
                                                                CHR (13),
                                                                ''),
                                                             CHR (10),
                                                             '. '),
                                           NULL),
                           tmp.error_message),
                   tmp.error_message)
                   status_message,
                tmp.*
           FROM xcust_ap_suppliers_temp_debug tmp,
                hr_operating_units org,
                ap_suppliers_int inf
          WHERE     tmp.request_id = i_request_id
                AND tmp.org_id = org.organization_id(+)
                AND tmp.vendor_interface_id = inf.vendor_interface_id(+);
   /*
      SELECT org.name org_name, tmp.*
        FROM xcust_ap_suppliers_temp_debug tmp, hr_operating_units org
       WHERE     tmp.request_id = i_request_id
             AND tmp.org_id = org.organization_id(+);*/
   BEGIN
      v_process_finish := SYSDATE;

      SELECT actual_start_date
        INTO p_start_date
        FROM FND_CONC_REQ_SUMMARY_V
       WHERE request_id = p_concurrent_id;

      v_process_time :=
         TO_CHAR (
            TO_TIMESTAMP (
               TO_CHAR (
                    TO_DATE ('00:00:00', 'HH24:MI:SS')
                  + (v_process_finish - p_start_date),
                  'HH24:MI:SS'),
               'HH24:MI:SS.FF'),
            'HH24:MI:SS.FF');
      write_log ('Loop create logs.' || TO_CHAR (p_concurrent_id));

      FOR i IN c_line (p_concurrent_id)
      LOOP
         IF (i.status_message IS NULL)
         THEN
            l_record_count_completion := l_record_count_completion + 1;
         ELSE
            l_record_count_failed := l_record_count_failed + 1;
         END IF;

         l_record_count_total := l_record_count_total + 1;

         BEGIN
            INSERT INTO TACSMT_LOG_DETAIL (LEVEL_TYPE,
                                           PROCESS_START,
                                           PROCESS_FINISH,
                                           PROCESSING_TIME,
                                           ERROR_TYPE,
                                           REQUEST_ID,
                                           OPERATING_UNIT,
                                           SUPPLIER_NAME,
                                           SUPPLIER_SITE,
                                           RECORD_FLAG,
                                           SUPPLIER_NUMBER,
                                           ERROR_CODE,
                                           ERROR_MESSAGE,
                                           DATE_FROM)
                 VALUES (
                           'D',
                           p_start_date,
                           v_process_finish,
                           v_process_time,
                           'SMT Supplier Interface',
                           p_concurrent_id,
                           i.org_name,
                           i.vendor_name,
                           i.vendor_site_code,
                           DECODE (i.update_flag, 'Y', 'Update', 'Create'),
                           i.vendor_number,
                           DECODE (i.status_message,
                                   NULL, 'Complete',
                                   'Error'),
                           REPLACE (i.status_message, CHR (10), '|'),
                           SYSDATE);
         EXCEPTION
            WHEN OTHERS
            THEN
               RAISE;
         END;
      END LOOP;

      INSERT INTO TACSMT_LOG_SUMMARY
           VALUES (
                     'H',
                     p_concurrent_id,
                     'TACSMT004',
                     p_start_date,
                     TO_DATE (
                        TO_CHAR (l_today_timestamp, 'YYYY-MON-DD HH24:MI:SS'),
                        'YYYY-MON-DD HH24:MI:SS'),
                     'Upload',
                     v_process_time,
                     l_record_count_completion,
                     l_record_count_failed,
                     l_record_count_total);

      COMMIT;

      v_cont_req_id :=
         fnd_request.submit_request (application   => 'SQLAP',
                                     program       => 'TACSMT006',
                                     argument1     => 'SMT004',        ---arg1
                                     argument2     => p_concurrent_id);
      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         RAISE;
   END;
END xcust_supplier_interface_smt;
/

