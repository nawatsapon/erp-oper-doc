CREATE OR REPLACE PACKAGE APPS.TAC_PKG_TCNX_CUST AS
/******************************************************************************
    NAME: APPS.tac_pkg_tcnx_cust
    PURPOSE:tac customize global package

    REVISIONS:
    Ver                 Date                    Author                              Description
    ----------          ---------------         --------------------                ----------------------------------------
    1.0                 16/08/2016              wsmice                              1. Created this package.
******************************************************************************/
    
    MAX_LINESIZE   binary_integer := 32767;
    
    PROCEDURE P_SET_COMPLETE_STATUS ( 
                                                                        /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                                        PIO_ERRMSG IN OUT VARCHAR2, */ 
                                                                        PI_STATUS IN VARCHAR2, 
                                                                        PI_MESSAGE IN VARCHAR2 
                                                                );
    FUNCTION FN_GET_PROCESS_DATE /*( 
                                                                PIO_ERRCODE IN OUT VARCHAR2, 
                                                                PIO_ERRMSG IN OUT VARCHAR2 
                                                          )*/  RETURN TIMESTAMP;
    FUNCTION FN_GET_PROCESS_TIME ( 
                                                                /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                                PIO_ERRMSG IN OUT VARCHAR2, */
                                                                PI_PROCESS_DATE IN TIMESTAMP 
                                                         ) RETURN TIMESTAMP;
    PROCEDURE P_GET_DIRECTORIES ( 
                                                            /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                            PIO_ERRMSG IN OUT VARCHAR2, */
                                                            PI_DIR_TYPE IN VARCHAR2, 
                                                            PI_DIR_NAME IN VARCHAR2 
                                                       );
--    PROCEDURE P_GET_PROCESS_DATE;
--    PROCEDURE P_GET_PROCESS_TIME (
--                                                                PI_PROCESS_DATE IN TIMESTAMP
--                                                         );
    PROCEDURE P_INSERT_LOG002_SUMMARY ( 
                                                                        /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                                        PIO_ERRMSG IN OUT VARCHAR2, */ 
                                                                        PI_CONC_REQUEST_ID IN NUMBER/*, 
                                                                        PI_PROCESS_DATE IN TIMESTAMP */
                                                                   );
--    PROCEDURE P_UPDATE_LOG002_SUMMARY ( 
--                                                                            /*PIO_ERRCODE IN OUT VARCHAR2, 
--                                                                            PIO_ERRMSG IN OUT VARCHAR2, */
--                                                                            PI_FILE_COUNT IN NUMBER, 
--                                                                            PI_PROCESS_TIME IN TIMESTAMP 
--                                                                    );
    PROCEDURE P_INSERT_LOG002_DETAIL ( 
                                                                    /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                                    PIO_ERRMSG IN OUT VARCHAR2, */ 
                                                                    PI_CONC_REQUEST_ID IN NUMBER, 
                                                                    PI_FILE_COUNT IN NUMBER, 
                                                                    PI_ERROR_TYPE IN VARCHAR2, 
                                                                    PI_PATH_NAME IN VARCHAR2, 
                                                                    PI_FILE_NAME IN VARCHAR2, 
--                                                                    PI_PROCESS_DATE IN TIMESTAMP, 
                                                                    PI_STATUS IN VARCHAR2, 
                                                                    PI_ERRCODE IN VARCHAR2, 
                                                                    PI_ERRMSG IN VARCHAR2 
                                                               );
--    PROCEDURE P_UPDATE_LOG002_DETAIL ( 
--                                                                    PI_PROCESS_DATE DATE, 
--                                                                    PI_STATUS IN VARCHAR2, 
--                                                                    PI_ERRCODE IN VARCHAR2, 
--                                                                    PI_ERRMSG IN VARCHAR2 
--                                                               );
    PROCEDURE P_INSERT_LOG003_SUMMARY ( 
                                                                        /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                                        PIO_ERRMSG IN OUT VARCHAR2, */
                                                                        PI_CONC_REQUEST_ID IN NUMBER, 
                                                                        PI_FILE_NAME IN VARCHAR2, 
                                                                        PI_PROCESS_DATE IN TIMESTAMP 
                                                                   );
    PROCEDURE P_UPDATE_LOG003_SUMMARY ( 
                                                                        /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                                        PIO_ERRMSG IN OUT VARCHAR2, */ 
                                                                        PI_CONC_REQUEST_ID IN NUMBER, 
                                                                        PI_FILE_NAME IN VARCHAR2, 
--                                                                        PI_PROCESS_DATE IN TIMESTAMP, 
                                                                        PI_RECORD_COUNT_COMPLETE IN NUMBER, 
                                                                        PI_RECORD_COUNT_ERROR IN NUMBER 
                                                                    );
    PROCEDURE P_INSERT_LOG003_DETAIL ( 
                                                                    /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                                    PIO_ERRMSG IN OUT VARCHAR2, */ 
                                                                    PI_CONC_REQUEST_ID IN NUMBER, 
                                                                    PI_ERROR_TYPE IN VARCHAR2, 
                                                                    PI_FILE_NAME IN VARCHAR2, 
                                                                    PI_LINE_TYPE IN VARCHAR2, 
                                                                    PI_INVOICE_NO IN VARCHAR2, 
                                                                    PI_LINE_NUMBER IN NUMBER, 
                                                                    PI_CUSTOMER_NO IN VARCHAR2, 
                                                                    PI_ITEM IN VARCHAR2, 
                                                                    PI_AMOUNT IN NUMBER, 
                                                                    PI_PROCESS_DATE IN TIMESTAMP, 
                                                                    PI_STATUS IN VARCHAR2, 
                                                                    PI_ERRCODE IN VARCHAR2, 
                                                                    PI_ERRMSG IN VARCHAR2 
                                                               );
--    PROCEDURE P_UPDATE_LOG003_DETAIL ( 
--                                                                    PI_CONC_REQUEST_ID IN NUMBER, 
--                                                                    PI_LINE_NUMBER IN NUMBER, 
--                                                                    PI_STATUS IN VARCHAR2, 
----                                                                    PI_PROCESS_DATE IN TIMESTAMP, 
--                                                                    PI_ERRCODE IN VARCHAR2, 
--                                                                    PI_ERRMSG IN VARCHAR2
--                                                               );
    PROCEDURE P_INSERT_LOG004_SUMMARY ( 
                                                                        /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                                        PIO_ERRMSG IN OUT VARCHAR2, */
                                                                        PI_CONC_REQUEST_ID IN NUMBER/*, 
                                                                        PI_PROCESS_DATE IN TIMESTAMP */
                                                                   );
--    PROCEDURE P_UPDATE_LOG004_SUMMARY ( 
--                                                                            /*PIO_ERRCODE IN OUT VARCHAR2, 
--                                                                            PIO_ERRMSG IN OUT VARCHAR2, */
--                                                                            PI_FILE_COUNT IN NUMBER, 
--                                                                            PI_PROCESS_TIME IN TIMESTAMP 
--                                                                    );
    PROCEDURE P_INSERT_LOG004_DETAIL ( 
                                                                    /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                                    PIO_ERRMSG IN OUT VARCHAR2, */ 
                                                                    PI_CONC_REQUEST_ID IN NUMBER, 
                                                                    PI_FILE_COUNT IN NUMBER, 
                                                                    PI_ERROR_TYPE IN VARCHAR2, 
                                                                    PI_PATH_NAME IN VARCHAR2, 
                                                                    PI_FILE_NAME IN VARCHAR2, 
--                                                                    PI_PROCESS_DATE IN TIMESTAMP, 
                                                                    PI_STATUS IN VARCHAR2, 
                                                                    PI_ERRCODE IN VARCHAR2, 
                                                                    PI_ERRMSG IN VARCHAR2  
                                                               );
--    PROCEDURE P_UPDATE_LOG004_DETAIL ( 
--                                                                    PI_PROCESS_DATE DATE, 
--                                                                    PI_STATUS IN VARCHAR2, 
--                                                                    PI_ERRCODE IN VARCHAR2, 
--                                                                    PI_ERRMSG IN VARCHAR2 
--                                                               );
    PROCEDURE P_READ_AND_INSERT ( 
                                                            /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                            PIO_ERRMSG IN OUT VARCHAR2, */
                                                            PI_CONC_REQUEST_ID IN NUMBER, 
                                                            PI_MONTH IN VARCHAR2, 
                                                            PI_YEAR IN NUMBER, 
                                                            PI_BATCH_SOURCE_NAME IN VARCHAR2, 
                                                            PI_INF_LINE_CONTEXT IN VARCHAR2, 
                                                            PI_CUST_TRX_TYPE IN VARCHAR2, 
                                                            PI_TERM_NAME IN VARCHAR2, 
                                                            PI_FILE_NAME IN VARCHAR2, 
                                                            PI_DIR_NAME IN VARCHAR2 
                                                       );
    PROCEDURE P_LOG_MONITOR ( 
                                                        PIO_ERRCODE IN OUT VARCHAR2, 
                                                        PIO_ERRMSG IN OUT VARCHAR2, 
                                                        PI_LOG_NAME IN VARCHAR2, 
                                                        PI_CONC_REQUEST_ID IN NUMBER, 
                                                        PI_DIR_NAME IN VARCHAR2 
                                                 );
    PROCEDURE P_LOG_MONITOR ( 
                                                        PI_LOG_NAME IN VARCHAR2, 
                                                        PI_CONC_REQUEST_ID IN NUMBER, 
                                                        PI_DIR_NAME IN VARCHAR2 
                                                 );
    
END TAC_PKG_TCNX_CUST; 
/

