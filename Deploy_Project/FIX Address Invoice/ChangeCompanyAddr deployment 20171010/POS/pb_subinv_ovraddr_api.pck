CREATE OR REPLACE PACKAGE pb_subinv_ovraddr_api IS

PROCEDURE validate_setup
(i_ou_code		IN	VARCHAR2	:=	NULL
,i_subinv_code	IN	VARCHAR2	:=	NULL
,o_result		OUT	BOOLEAN
,o_error_msg	OUT	VARCHAR2);

END pb_subinv_ovraddr_api;
/
CREATE OR REPLACE PACKAGE BODY pb_subinv_ovraddr_api IS

C_DFLT_DATE_FROM	CONSTANT DATE	:=	TO_DATE('01/01/0001','dd/mm/yyyy');
C_DFLT_DATE_TO		CONSTANT DATE	:=	TO_DATE('31/12/9999','dd/mm/yyyy');

PROCEDURE validate_setup
(i_ou_code		IN	VARCHAR2	:=	NULL
,i_subinv_code	IN	VARCHAR2	:=	NULL
,o_result		OUT	BOOLEAN
,o_error_msg	OUT	VARCHAR2)
IS
	CURSOR c_setup IS
		SELECT	*
		FROM	pb_subinv_ovraddr soa
		WHERE	soa.ou_code					=	NVL(i_ou_code,soa.ou_code)
		AND		soa.subinv_code				=	NVL(i_subinv_code,soa.subinv_code)
		AND		NVL(soa.disable_flag,'N')	<>	'Y'
		ORDER BY soa.ou_code, soa.subinv_code, NVL(soa.eff_date_from,C_DFLT_DATE_FROM), soa.eff_date_to;
	r_setup_prev	c_setup%ROWTYPE;
BEGIN
	o_result	:=	TRUE;
	o_error_msg	:=	NULL;
	FOR r_setup IN c_setup
	LOOP
		IF NVL(r_setup.eff_date_from,C_DFLT_DATE_FROM) > NVL(r_setup.eff_date_to,C_DFLT_DATE_TO) THEN
			o_result	:=	FALSE;
			o_error_msg	:=	'Override Address is defined with Effective Date From > To, for ou_code ' || r_setup.ou_code || ' and subinv_code ' || r_setup.subinv_code;
			EXIT;	-- Exit loop if error
		ELSIF r_setup_prev.ou_code IS NULL AND r_setup.subinv_code IS NULL THEN
			NULL;	-- No previous record
		ELSIF r_setup.ou_code	=	r_setup_prev.ou_code		AND
		   r_setup.subinv_code	=	r_setup_prev.subinv_code	AND
		   NVL(r_setup.eff_date_from,C_DFLT_DATE_FROM)	<= NVL(r_setup_prev.eff_date_to,C_DFLT_DATE_TO) AND
		   NVL(r_setup.eff_date_to,C_DFLT_DATE_TO)		>= NVL(r_setup_prev.eff_date_from,C_DFLT_DATE_FROM)
		THEN		-- Overlap address
			o_result	:=	FALSE;
			o_error_msg	:=	'Override Address is overlap for ou_code ' || r_setup.ou_code || ' and subinv_code ' || r_setup.subinv_code;
			EXIT;	-- Exit loop if error
		END IF;
		r_setup_prev	:=	r_setup;
	END LOOP;
END validate_setup;

END pb_subinv_ovraddr_api;
/
