/*
SELECT iv.ou_code, iv.subinv_code
	  ,iv.subinv_add || ' ' || DECODE(iv.amphur_code, NULL, NULL, DECODE(iv.province_code, 'BKK', 'ࢵ', '�����')) ||
	   pb_amphur_pkg.get_amphur_name(iv.amphur_code) address1
	  ,pb_province_pkg.get_name(iv.province_code) || ' ' || iv.postal_code address2
	  ,iv.subinv_taxid org_tax_id
	  ,iv.branch_code subinv_branch_code
FROM pb_subinv iv
ORDER BY iv.ou_code, iv.subinv_code;
*/
-- Create table
CREATE TABLE PB_SUBINV_OVRADDR
(ou_code		VARCHAR2(3) NOT NULL
,subinv_code	VARCHAR2(10) NOT NULL
,eff_date_from	DATE
,eff_date_to	DATE
,disable_flag	VARCHAR2(1)
,subinv_add		VARCHAR2(200)
,amphur_code	VARCHAR2(5)
,province_code	VARCHAR2(3)
,postal_code	VARCHAR2(5)
,region_code	VARCHAR2(3)
,upd_by			VARCHAR2(15)
,upd_date		DATE
,upd_pgm		VARCHAR2(8)
);
-- Add comments to the columns 
COMMENT ON COLUMN PB_SUBINV_OVRADDR.eff_date_from
  IS '���� Start date �����ռ�㹡�� override address';
COMMENT ON COLUMN PB_SUBINV_OVRADDR.eff_date_to
  IS '���� End date �����ռ�㹡�� override address';
COMMENT ON COLUMN PB_SUBINV_OVRADDR.disable_flag
  IS '�к�㹡óշ���ͧ��� disable ��¡�� (Y=Disable ������зӧҹ����͹�������¡�ù��)';

-- Create/Recreate primary, unique and foreign key constraints 
ALTER TABLE PB_SUBINV_OVRADDR
  ADD CONSTRAINT UKPB_SUBINV_OVRADDR UNIQUE (OU_CODE, SUBINV_CODE, eff_date_from, eff_date_to);
-- Create/Recreate indexes 
CREATE INDEX PB_SUBINV_OVRADDR_N1 ON PB_SUBINV_OVRADDR (OU_CODE, SUBINV_CODE);
-- Grant/Revoke object privileges 
GRANT SELECT ON PB_SUBINV_OVRADDR TO SELECTED_POS_ROLE;
GRANT SELECT ON PB_SUBINV_OVRADDR TO WMUSER;
