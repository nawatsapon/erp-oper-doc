CREATE OR REPLACE PACKAGE tac_ovraddr_api IS

PROCEDURE validate_setup
(i_org_id		IN	NUMBER	:=	NULL
,o_result		OUT	BOOLEAN
,o_error_msg	OUT	VARCHAR2);

FUNCTION get_ovraddr
(i_org_id	IN	NUMBER	:=	NULL
,i_eff_date	IN	DATE	:=	TRUNC(SYSDATE))
RETURN fnd_lookup_values_vl%ROWTYPE;

END tac_ovraddr_api;
/
CREATE OR REPLACE PACKAGE BODY tac_ovraddr_api IS

C_DFLT_DATE_FROM	CONSTANT DATE	:=	TO_DATE('01/01/0001','dd/mm/yyyy');
C_DFLT_DATE_TO		CONSTANT DATE	:=	TO_DATE('31/12/9999','dd/mm/yyyy');

PROCEDURE validate_setup
(i_org_id		IN	NUMBER	:=	NULL
,o_result		OUT	BOOLEAN
,o_error_msg	OUT	VARCHAR2)
IS
	CURSOR c_setup IS
		SELECT	*
		FROM	fnd_lookup_values_vl lv
		WHERE	lv.lookup_type	=	'TAC_ADDRESS_OVERRIDE'
		AND		lv.tag	LIKE	i_org_id || DECODE(i_org_id,NULL,NULL,'-') || '%'
		AND		NVL(lv.enabled_flag,'Y')	=	'Y'
		ORDER BY lv.tag, NVL(lv.start_date_active,C_DFLT_DATE_FROM), lv.end_date_active;
	r_setup_prev	c_setup%ROWTYPE;
BEGIN
	o_result	:=	TRUE;
	o_error_msg	:=	NULL;
	FOR r_setup IN c_setup
	LOOP
		IF NVL(r_setup.start_date_active,C_DFLT_DATE_FROM) > NVL(r_setup.end_date_active,C_DFLT_DATE_TO) THEN
			o_result	:=	FALSE;
			o_error_msg	:=	'Override Address is defined with Effective Date From > To, for OrgID & Reporting Entity ' || r_setup.tag;
			EXIT;	-- Exit loop if error
		ELSIF r_setup_prev.row_id IS NULL THEN
			NULL;	-- No previous record
		ELSIF r_setup_prev.tag IS NULL THEN
			o_result	:=	FALSE;
			o_error_msg	:=	'Override Address is defined with no tag, please check setup';
			EXIT;	-- Exit loop if error
		ELSIF r_setup.tag	=	r_setup_prev.tag	AND
		   NVL(r_setup.start_date_active,C_DFLT_DATE_FROM)	<= NVL(r_setup_prev.end_date_active,C_DFLT_DATE_TO) AND
		   NVL(r_setup.end_date_active,C_DFLT_DATE_TO)		>= NVL(r_setup_prev.start_date_active,C_DFLT_DATE_FROM)
		THEN		-- Overlap address
			o_result	:=	FALSE;
			o_error_msg	:=	'Override Address is overlap for tag ' || r_setup.tag;
			EXIT;	-- Exit loop if error
		END IF;
		r_setup_prev	:=	r_setup;
	END LOOP;
END validate_setup;

FUNCTION get_ovraddr
(i_org_id	IN	NUMBER	:=	NULL
,i_eff_date	IN	DATE	:=	TRUNC(SYSDATE))
RETURN fnd_lookup_values_vl%ROWTYPE
IS
	v_org_id	NUMBER	:=	NVL(i_org_id,RTRIM(SUBSTRB(USERENV('CLIENT_INFO'),1,10)));
	CURSOR	c1 IS
	SELECT	*
	FROM	fnd_lookup_values_vl lv
	WHERE	lv.lookup_type	=	'TAC_ADDRESS_OVERRIDE'
	AND		lv.tag	LIKE	v_org_id || DECODE(v_org_id,NULL,NULL,'-') || '%'
	AND		NVL(lv.enabled_flag,'Y')	=	'Y'
	AND		TRUNC(i_eff_date) BETWEEN NVL(lv.start_date_active,C_DFLT_DATE_FROM) AND NVL(lv.end_date_active,C_DFLT_DATE_TO)
	ORDER BY lv.start_date_active;
BEGIN
	FOR rec IN c1
	LOOP
		RETURN rec;
	END LOOP;
	RETURN NULL;
END get_ovraddr;

END tac_ovraddr_api;
/
