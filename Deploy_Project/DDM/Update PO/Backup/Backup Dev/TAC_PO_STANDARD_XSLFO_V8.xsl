﻿<?xml version="1.0" encoding="UTF-8"?>
 <!-- $Header: PO_STANDARD_XSLFO.xsl 115.12.11510.6 2005/02/14 11:55:54 manram ship $ --> 
 <!-- dbdrv: none --> 

<xsl:stylesheet version="1.0"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:fo="http://www.w3.org/1999/XSL/Format"  >

<!-- Attribute Set for table header labels with right align and space at end -->
	<xsl:attribute-set name="table_head">
		<xsl:attribute name="font-size">8pt</xsl:attribute>
		<xsl:attribute name="font-family">Times</xsl:attribute>
		<xsl:attribute name="space-end">3pt</xsl:attribute>
	</xsl:attribute-set>

<!-- Attribute Set for table header labels with right align and space at start -->
	<xsl:attribute-set name="table_head1">
		<xsl:attribute name="font-size">9pt</xsl:attribute>
		<xsl:attribute name="font-family">Times</xsl:attribute>
		<xsl:attribute name="space-start">3pt</xsl:attribute>
    <xsl:attribute name="text-align">center</xsl:attribute>    
	</xsl:attribute-set>

<!-- Attribute Set for table header labels with right align and space at end and space before and after -->
	<xsl:attribute-set name="table_head2">
		<xsl:attribute name="font-size">8pt</xsl:attribute>
		<xsl:attribute name="font-family">Times</xsl:attribute>
		<xsl:attribute name="space-end">5pt</xsl:attribute>
		<xsl:attribute name="space-before">1.2pt</xsl:attribute>
		<xsl:attribute name="space-after">1.2pt</xsl:attribute>
	</xsl:attribute-set>

<!-- Attribute Set for table cell with border, background and right align attributes -->
	<xsl:attribute-set name="table_cell_heading1">
		<xsl:attribute name="background-color">#E7E7E7</xsl:attribute>
		<xsl:attribute name="text-align">right</xsl:attribute>
		<xsl:attribute name="border-width">.5pt</xsl:attribute>
		<xsl:attribute name="height">4mm</xsl:attribute>
	</xsl:attribute-set>

<!-- Attribute Set for table cell with border, background and left align attributes -->
	<xsl:attribute-set name="table_cell_heading2">
		<xsl:attribute name="background-color">#E7E7E7</xsl:attribute>
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="border-width">.5pt</xsl:attribute>
		<xsl:attribute name="height">4mm</xsl:attribute>
	</xsl:attribute-set>

<!-- Attribute Set for table cell with out border and right align attribute -->
	<xsl:attribute-set name="table_cell_heading3">
			<xsl:attribute name="text-align">right</xsl:attribute>
	</xsl:attribute-set>
<!-- TM CUSTOMIZE : Attribute Set for PURCHASE ORDER TITLE -->
	<xsl:attribute-set name="table_cell.title">
		<xsl:attribute name="font-weight">bold</xsl:attribute>	
    <xsl:attribute name="font-family">Tahoma</xsl:attribute>    
    <xsl:attribute name="text-align">right</xsl:attribute>
		<xsl:attribute name="font-size">16pt</xsl:attribute>
	</xsl:attribute-set>

  <xsl:attribute-set name="tac_table_ship1">
    <xsl:attribute name="text-align">left</xsl:attribute>
    <xsl:attribute name="border-left-style">solid</xsl:attribute>
    <xsl:attribute name="border-left-width">1pt</xsl:attribute>
    <xsl:attribute name="border-right-style">none</xsl:attribute>
    <xsl:attribute name="border-right-width">1pt</xsl:attribute>
    <xsl:attribute name="border-top-style">solid</xsl:attribute>
    <xsl:attribute name="border-top-width">1pt</xsl:attribute>
    <xsl:attribute name="border-bottom-style">solid</xsl:attribute>
    <xsl:attribute name="border-bottom-width">1pt</xsl:attribute>
    <xsl:attribute name="padding-top">0pt</xsl:attribute>
    <xsl:attribute name="padding-left">0pt</xsl:attribute>
    <xsl:attribute name="padding-right">0pt</xsl:attribute>
    <xsl:attribute name="padding-bottom">0pt</xsl:attribute>    
  </xsl:attribute-set>

	<xsl:attribute-set name="tac_form_data">
		<xsl:attribute name="font-size">9pt</xsl:attribute>
		<xsl:attribute name="font-family">Tahoma</xsl:attribute>
		<xsl:attribute name="space-start">3pt</xsl:attribute>
    <xsl:attribute name="font-weight">bold</xsl:attribute>
                <xsl:attribute name="white-space-treatment">preserve</xsl:attribute>
                <xsl:attribute name="white-space-collapse">false</xsl:attribute>
                <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
	</xsl:attribute-set>

<!-- Attribute Set for table cell same as table_cell_heading1 except top padding. -->
	<xsl:attribute-set name="table_cell_heading4">
		<xsl:attribute name="background-color">#E7E7E7</xsl:attribute>
		<xsl:attribute name="text-align">right</xsl:attribute>
		<xsl:attribute name="padding-top">1.2pt</xsl:attribute>
		<xsl:attribute name="border-width">.5pt</xsl:attribute>
		<xsl:attribute name="height">4mm</xsl:attribute>
	</xsl:attribute-set>

<!-- Attribute Set for table cell with border and center align attributes -->
	<xsl:attribute-set name="heading.center.align">
			<xsl:attribute name="font-weight">bold</xsl:attribute>	
			<xsl:attribute name="text-align">center</xsl:attribute>
			<xsl:attribute name="height">4mm</xsl:attribute>
	</xsl:attribute-set>

<!-- Attribute Set for cover page heading -->
	<xsl:attribute-set name="coverpage.label">
		<xsl:attribute name="font-style">italic</xsl:attribute>
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="font-family">Times</xsl:attribute>
	</xsl:attribute-set>

<!--Bug#4088207:Added three attributes to preserve the white spaces and line breaks -->
<!-- Attribute Set for field values with space at start -->
	<xsl:attribute-set name="form_data">
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="font-family">Times</xsl:attribute>
		<xsl:attribute name="space-start">3pt</xsl:attribute>
                <xsl:attribute name="white-space-treatment">preserve</xsl:attribute>
                <xsl:attribute name="white-space-collapse">false</xsl:attribute>
                <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
	</xsl:attribute-set>

<!-- Attribute Set for field values with space at end -->
	<xsl:attribute-set name="form_data1">
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="font-family">Times</xsl:attribute>
		<xsl:attribute name="space-end">3pt</xsl:attribute>
	</xsl:attribute-set>

<!-- Attribute Set for field values with space at start, before and after -->
	<xsl:attribute-set name="form_data2">
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="font-family">Times</xsl:attribute>
		<xsl:attribute name="space-start">5pt</xsl:attribute>
		<xsl:attribute name="space-before">3pt</xsl:attribute>
		<xsl:attribute name="space-after">3pt</xsl:attribute> 
	</xsl:attribute-set>

<!-- Attribute Set for field values with out space -->
	<xsl:attribute-set name="form_data3">
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="font-family">Times</xsl:attribute>
	</xsl:attribute-set>

<!-- Attribute Set for cover page text -->
	<xsl:attribute-set name="cover_page_text">
		<xsl:attribute name="font-size">10pt</xsl:attribute>
		<xsl:attribute name="font-family">Times</xsl:attribute>
		<xsl:attribute name="space-start">3pt</xsl:attribute>
		<xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
	</xsl:attribute-set>

<!-- Attribute Set for field values with border -->
	<xsl:attribute-set name="table.cell">
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="border-width">.5pt</xsl:attribute>
		<xsl:attribute name="height">4mm</xsl:attribute>
		<xsl:attribute name="text-align">left</xsl:attribute>
	</xsl:attribute-set>

<!-- Attribute Set for field values with out border -->
	<xsl:attribute-set name="table.cell2">
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="height">4mm</xsl:attribute>
	</xsl:attribute-set>

<!-- Attribute Set for field values with border and right align -->
	<xsl:attribute-set name="table.cell3">
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="border-width">.5pt</xsl:attribute>
		<xsl:attribute name="height">4mm</xsl:attribute>
		<xsl:attribute name="text-align">right</xsl:attribute>
	</xsl:attribute-set>

<!-- Attribute Set for field values with border and right align -->
	<xsl:attribute-set name="table.cell4">
		<xsl:attribute name="text-align">center</xsl:attribute>
		<xsl:attribute name="height">4mm</xsl:attribute>
	</xsl:attribute-set>

<!-- Attribute Set for field values with right align -->
	<xsl:attribute-set name="table.cell5">
		<xsl:attribute name="text-align">right</xsl:attribute>
		<xsl:attribute name="height">4mm</xsl:attribute>
	</xsl:attribute-set>

<!-- Attribute Set for field values with left align -->
	<xsl:attribute-set name="table.cell6">
		<xsl:attribute name="text-align">left</xsl:attribute>
		<xsl:attribute name="height">0mm</xsl:attribute>
    <xsl:attribute name="font-size">9pt</xsl:attribute>
		<xsl:attribute name="font-family">Times</xsl:attribute>    
	</xsl:attribute-set>

<!-- Attribute Set for field values with border and no height -->
	<xsl:attribute-set name="table.cell7">
		<xsl:attribute name="font-weight">bold</xsl:attribute>
		<xsl:attribute name="border-width">.5pt</xsl:attribute>
		<xsl:attribute name="height">4mm</xsl:attribute>
	</xsl:attribute-set>

<!-- Attribute Set for Legal entity details -->
	<xsl:attribute-set name="legal_details_style">
		<xsl:attribute name="font-size">9pt</xsl:attribute>
		<xsl:attribute name="font-family">Times</xsl:attribute>
		<xsl:attribute name="text-align">left</xsl:attribute>
		<!-- <xsl:attribute name="font-weight">bold</xsl:attribute> -->
	</xsl:attribute-set>

<!-- Attribute Set same as Legal entity details with center align -->
	<xsl:attribute-set name="test_style">
		<xsl:attribute name="font-size">12pt</xsl:attribute>
		<xsl:attribute name="font-family">Times</xsl:attribute>
		<xsl:attribute name="text-align">center</xsl:attribute>
		<xsl:attribute name="font-weight">bold</xsl:attribute>
	</xsl:attribute-set>

<!-- Attribute for lines table with border width attribute -->
	<xsl:attribute-set name="lines.table.style">
		<xsl:attribute name="border-width">.5pt</xsl:attribute>
	</xsl:attribute-set>

<!-- Variable for holding the root tag object -->
<xsl:variable name="ROOT_OBJ" select="(/PO_DATA)"/>

<!-- Variable for holding boilerplate messages -->
<xsl:variable name="BOILER_PLATE_MESSAGES_OBJ" select="($ROOT_OBJ/MESSAGE/MESSAGE_ROW)"/>

<!-- Variable for holding the lines root object -->
<xsl:variable name="LINES_ROOT_OBJ" select="($ROOT_OBJ/LINES/LINES_ROW)"/>

<!-- Variable for holding the shipments root object -->
<xsl:variable name="LINE_LOCATIONS_ROOT_OBJ" select="($LINES_ROOT_OBJ/LINE_LOCATIONS/LINE_LOCATIONS_ROW)"/>

<!-- Variable for holding the DISTRIBUTIONS root object -->
<xsl:variable name="DISTRIBUTIONS_ROOT_OBJ" select="($LINE_LOCATIONS_ROOT_OBJ/DISTRIBUTIONS/DISTRIBUTIONS_ROW)"/>

<!-- Variable for holding the line long attachments root object -->
<xsl:variable name="LINE_LONG_ATTACHMENTS_ROOT_OBJ" select="($ROOT_OBJ/LINE_ATTACHMENTS/ID)"/>

<!-- Variable for holding the header long attachments root object -->
<xsl:variable name="HEADER_LONG_ATTACHMENTS_ROOT_OBJ" select="($ROOT_OBJ/HEADER_ATTACHMENTS)"/>

<!-- Bug#3999145: The variable should hold the reference of line location id tag -->
<!-- Variable for holding the shipment long attachments root object -->
<xsl:variable name="SHIPMENT_ATTACHMENTS_ROOT_OBJ" select="($ROOT_OBJ/SHIPMENT_ATTACHMENTS/LINE_LOCATION_ID)"/>

<!-- variable to control the display of boilerplate texts. The current value is true because 1 is equal to 1. -->
<xsl:variable name="DisplayBoilerPlate" select="1=1" />

<!-- variable which controls the display of Charge Account. Current value is false because 1 is not equal to 2 -->
<xsl:variable name="PSA" select="1 = 2"/>
	
<!-- Variable which contains the text that will be displayed in the header -->
<xsl:variable name="header_text">
	<xsl:text>  </xsl:text>
</xsl:variable>

<!-- Variable which contains the text that will be displayed in the footer -->
<xsl:variable name="footer_text">
	<xsl:text>  </xsl:text>
</xsl:variable>

<!-- variable to find the total amount -->
<xsl:variable name="total_amount">
	<xsl:text>0</xsl:text>
</xsl:variable>

<!-- variable to print multiple word or not at header level ship to -->

<xsl:variable name="print_multiple">
	<xsl:if test="$ROOT_OBJ/DIST_SHIPMENT_COUNT &gt; 1">
		<xsl:text>Y</xsl:text>
	</xsl:if>
</xsl:variable>

<!-- Variable to identify whether Contract Terms and Conditions exists for current PO -->
<xsl:variable name="CON_TERMS_EXIST_FLAG" select="$ROOT_OBJ/WITH_TERMS='Y'"/>

<!-- This key is used to find the distinct of requestor id's -->
<xsl:key name="distinct-person_id" match="/PO_DATA/LINES/LINES_ROW/LINE_LOCATIONS/LINE_LOCATIONS_ROW/DISTRIBUTIONS/DISTRIBUTIONS_ROW/DELIVER_TO_PERSON_ID" use="."/>

  <!--TM CUSTOMIZE : DTAC Varaible -->
  <xsl:variable name="DTAC_NAME_th">บมจ. โทเทิ่ล แอ็คเซ็ส คอมมูนิเคชั่น</xsl:variable>
  <xsl:variable name="DTAC_NAME_en">TOTAL ACCESS COMMUNICATION PUBLIC COMPANY LIMITED</xsl:variable>
  <xsl:variable name="DTAC_ADDR1_th">319 อาคารจัตุรัสจามจุรี ชั้น 41 ถ.พญาไท </xsl:variable>
  <xsl:variable name="DTAC_ADDR1_en">319 Chamchuri Square Building, 41st Floor, Phayathai Road, </xsl:variable>
  <xsl:variable name="DTAC_ADDR2_th">แขวงปทุมวัน เขตปทุมวัน  กรุงเทพมหานคร 10330</xsl:variable>
  <xsl:variable name="DTAC_ADDR2_en">Pathumwan, Pathumwan, Bangkok 10330 </xsl:variable>
  <xsl:variable name="DTAC_ADDR3_th">โทร. 0 2202 8000      โทรสาร 0 2202 8283 </xsl:variable>
  <xsl:variable name="DTAC_ADDR3_en">Tel. 66 2202 8000     Fax. 66 2202 8283 </xsl:variable>

  <xsl:variable name="DTACN_NAME_th">บริษัท ดีแทค ไตรเน็ต จำกัด</xsl:variable>
  <xsl:variable name="DTACN_NAME_en">dtac TriNet Co., Ltd.</xsl:variable>
  <xsl:variable name="DTACN_ADDR1_th">319 อาคารจัตุรัสจามจุรี ชั้น 41 ถนนพญาไท</xsl:variable>
  <xsl:variable name="DTACN_ADDR1_en">319 Chamchuri Square Building, 41st Floor, Phayathai Road,</xsl:variable>
  <xsl:variable name="DTACN_ADDR2_th">แขวงปทุมวัน เขตปทุมวัน  กรุงเทพมหานคร 10330 </xsl:variable>
  <xsl:variable name="DTACN_ADDR2_en">Pathumwan, Pathumwan, Bangkok 10330 </xsl:variable>
  <xsl:variable name="DTACN_ADDR3_th">โทร. 0 2202 8000 ext. 30902-3 </xsl:variable>
  <xsl:variable name="DTACN_ADDR3_en">Tel. 66 2202 8000 ext. 30902-3 </xsl:variable>

  <xsl:variable name="DTAC_NAME_N_th">บมจ. โทเทิ่ล แอ็คเซ็ส คอมมูนิเคชั่น</xsl:variable>
  <xsl:variable name="DTAC_NAME_N_en">TOTAL ACCESS COMMUNICATION PUBLIC COMPANY LIMITED</xsl:variable>
  <xsl:variable name="DTAC_ADDR1_N_th">205 ม.3 ตำบลหัวรอ        </xsl:variable>
  <xsl:variable name="DTAC_ADDR1_N_en">205 Moo.3 Tambol. Hua Ro </xsl:variable>
  <xsl:variable name="DTAC_ADDR2_N_th">อำเภอเมือง  จังหวัดพิษณุโลก  65000 </xsl:variable>
  <xsl:variable name="DTAC_ADDR2_N_en">Muang, Phitsanulok 65000 </xsl:variable>
  <xsl:variable name="DTAC_ADDR3_N_th">โทร.0 5521 8616  โทรสาร 0 5521 8716 </xsl:variable>
  <xsl:variable name="DTAC_ADDR3_N_en">Tel.0 5521 8616  Fax 0 5521 8716 </xsl:variable>

  <xsl:variable name="DTAC_NAME_NE_th">บมจ. โทเทิ่ล แอ็คเซ็ส คอมมูนิเคชั่น</xsl:variable>
  <xsl:variable name="DTAC_NAME_NE_en">TOTAL ACCESS COMMUNICATION PUBLIC COMPANY LIMITED</xsl:variable>
  <xsl:variable name="DTAC_ADDR1_NE_th">126/68-70 หมู่ 16 ถนนมิตรภาพ ตำบลในเมือง</xsl:variable>
  <xsl:variable name="DTAC_ADDR1_NE_en">126/68-70 Moo 16 Mittraphap Road, </xsl:variable>
  <xsl:variable name="DTAC_ADDR2_NE_th">อำเภอเมือง จังหวัดขอนแก่น 40000 </xsl:variable>
  <xsl:variable name="DTAC_ADDR2_NE_en">Amphur Muang, Khonkaen 40000 </xsl:variable>
  <xsl:variable name="DTAC_ADDR3_NE_th">โทร. 0 433 34800 โทรสาร 0 4333 4585 </xsl:variable>
  <xsl:variable name="DTAC_ADDR3_NE_en">Tel. 0 433 34800 Fax. 0 4333 4585 </xsl:variable>

  <xsl:variable name="DTAC_NAME_S_th">บมจ. โทเทิ่ล แอ็คเซ็ส คอมมูนิเคชั่น</xsl:variable>
  <xsl:variable name="DTAC_NAME_S_en">TOTAL ACCESS COMMUNICATION PUBLIC COMPANY LIMITED</xsl:variable>
  <xsl:variable name="DTAC_ADDR1_S_th">660/5 ถนนหน้าเมือง  ตำบลตลาด </xsl:variable>
  <xsl:variable name="DTAC_ADDR1_S_en">660/5  Namuang Road , Tambol Talad </xsl:variable>
  <xsl:variable name="DTAC_ADDR2_S_th">อำเภอเมือง จังหวัดสุราษฎร์ธานี  84000 </xsl:variable>
  <xsl:variable name="DTAC_ADDR2_S_en">Amphur Muang, Suratthani  84000 </xsl:variable>
  <xsl:variable name="DTAC_ADDR3_S_th">โทร. 0-7721-3600  โทรสาร. 0-7721-4880 </xsl:variable>
  <xsl:variable name="DTAC_ADDR3_S_en">Tel. 0-7721-3600  Fax. 0-7721-4880 </xsl:variable>

  <xsl:variable name="DTAC_NAME_E_th">บมจ. โทเทิ่ล แอ็คเซ็ส คอมมูนิเคชั่น</xsl:variable>
  <xsl:variable name="DTAC_NAME_E_en">TOTAL ACCESS COMMUNICATION PUBLIC COMPANY LIMITED</xsl:variable>
  <xsl:variable name="DTAC_ADDR1_E_th">221/2 ถนนลงหาดบางแสน  ตำบลแสนสุข </xsl:variable>
  <xsl:variable name="DTAC_ADDR1_E_en">221/2 Ronghadbangsaen Road, Tambol Saensuk, </xsl:variable>
  <xsl:variable name="DTAC_ADDR2_E_th">อำเภอเมืองชลบุรี จังหวัดชลบุรี 20130 </xsl:variable>
  <xsl:variable name="DTAC_ADDR2_E_en">Amphur Muang Chonburi, Chonburi 20130 </xsl:variable>
  <xsl:variable name="DTAC_ADDR3_E_th">โทร. 0-3874-6340 โทรสาร. 0-3874-6344  </xsl:variable>
  <xsl:variable name="DTAC_ADDR3_E_en">Tel. 0-3874-6340 Fax. 0-3874-6344</xsl:variable>

  <xsl:variable name="DTACW_NAME_th">บริษัท ดีแทค บรอดแบนด์ จำกัด </xsl:variable>
  <xsl:variable name="DTACW_NAME_en">DTAC Broadband Company Limited</xsl:variable>
  <xsl:variable name="DTACW_ADDR1_th">319 อาคารจัตุรัสจามจุรี ชั้น 41 ถนนพญาไท </xsl:variable>
  <xsl:variable name="DTACW_ADDR1_en">319 Chamchuri Square Building, 41st Floor, Phayathai Road</xsl:variable>
  <xsl:variable name="DTACW_ADDR2_th">แขวงปทุมวัน เขตปทุมวัน  กรุงเทพมหานคร 10330</xsl:variable>
  <xsl:variable name="DTACW_ADDR2_en">Pathumwan, Bangkok, Thailand 10330 </xsl:variable>
  <xsl:variable name="DTACW_ADDR3_th">โทร. 0 2202 8000      โทรสาร 0 2202 8828</xsl:variable>
  <xsl:variable name="DTACW_ADDR3_en">Tel. 66 2202 8000     Fax. 66 2202 8828</xsl:variable>

  <!-- Add by AP@BAS on 12-Jul-2012 -->
  <xsl:variable name="DTACI_NAME_th">บริษัท ดีแทค อินเตอร์เนต เซอร์วิส จำกัด</xsl:variable>
  <xsl:variable name="DTACI_NAME_en">DTAC Internet Service Company Limited</xsl:variable>
  <xsl:variable name="DTACI_ADDR1_th">319 อาคารจัตุรัสจามจุรี ชั้น 41 ถนนพญาไท </xsl:variable>
  <xsl:variable name="DTACI_ADDR1_en">319 Chamchuri Square Building, 41st Floor, Phayathai Road</xsl:variable>
  <xsl:variable name="DTACI_ADDR2_th">แขวงปทุมวัน เขตปทุมวัน กรุงเทพมหานคร 10330</xsl:variable>
  <xsl:variable name="DTACI_ADDR2_en">Pathumwan, Pathumwan, Bangkok, Thailand 10330 </xsl:variable>
  <xsl:variable name="DTACI_ADDR3_th">โทร. 0 2202 8000</xsl:variable>
  <xsl:variable name="DTACI_ADDR3_en">Tel. 66 2202 8000</xsl:variable>
  <!-- End add by AP@BAS on 12-Jul-2012 -->

  <!-- Add by AP@BAS on 13-Dec-2012 for PAYSBUY-->
  <xsl:variable name="PAYSBUY_NAME_th">บริษัท เพย์สบาย จำกัด</xsl:variable>
  <xsl:variable name="PAYSBUY_NAME_en">PAYSBUY CO.,LTD.</xsl:variable>
  <xsl:variable name="PAYSBUY_ADDR1_th">319  อาคารจัตุรัสจามจุรี  ชั้น 41 ถนนพญาไท</xsl:variable>
  <xsl:variable name="PAYSBUY_ADDR1_en">319 Chamchuri Square Building, 41st Floor, Phayathai Road</xsl:variable>
  <xsl:variable name="PAYSBUY_ADDR2_th">แขวงปทุมวัน เขตปทุมวัน กรุงเทพมหานคร 10330</xsl:variable>
  <xsl:variable name="PAYSBUY_ADDR2_en">Pathumwan, Pathumwan, Bangkok, Thailand 10330</xsl:variable>
  <xsl:variable name="PAYSBUY_ADDR3_th">โทร. 0 2160 5463-5</xsl:variable>
  <xsl:variable name="PAYSBUY_ADDR3_en">Tel. 66 2160 5463-5</xsl:variable>
  <!-- End add by AP@BAS on 13-Dec-2012 for PAYSBUY -->

  <!-- Start added by AP@BAS on 02-Nov-2015 for DTAC Accerelate -->
  <xsl:variable name="Accerelate_NAME_th">บริษัท ดีแทค แอ็คเซเลเรท จำกัด</xsl:variable>
  <xsl:variable name="Accerelate_NAME_en">dtac accelerate co,.Ltd.</xsl:variable>
  <xsl:variable name="Accerelate_ADDR1_th">319 อาคารจัตุรัสจามจุรี ห้องเลขที่ 220-221 ชั้น 2 ถนนพญาไท</xsl:variable>
  <xsl:variable name="Accerelate_ADDR1_en">319 Chamchuri Square Building, Room 220-221, 2nd Floor, Phayathai Road</xsl:variable>
  <xsl:variable name="Accerelate_ADDR2_th">แขวงปทุมวัน เขตปทุมวัน  กรุงเทพมหานคร 10330</xsl:variable>
  <xsl:variable name="Accerelate_ADDR2_en">Pathumwan, Pathumwan, Bangkok, Thailand 10330</xsl:variable>
  <xsl:variable name="Accerelate_ADDR3_th">โทร 0 2202 8000 ext. 30902-3</xsl:variable>
  <xsl:variable name="Accerelate_ADDR3_en">Tel 66 2202 8000 ext. 30902-3</xsl:variable>
  <!-- End added by AP@BAS on 02-Nov-2015 for DTAC Accerelate -->

  <!-- Start added by AP@BAS on 14-Jun-2017 for DTN-T -->
  <xsl:variable name="DTNT_NAME_th">บริษัท เทเลแอสเสท จำกัด</xsl:variable>
  <xsl:variable name="DTNT_NAME_en">Tele Assets Company Limited</xsl:variable>
  <xsl:variable name="DTNT_ADDR1_th">319 อาคารจัตุรัสจามจุรี ชั้น 41 ถ.พญาไท </xsl:variable>
  <xsl:variable name="DTNT_ADDR1_en">319 Chamchuri Square Building, 41st Floor, Phayathai Road,</xsl:variable>
  <xsl:variable name="DTNT_ADDR2_th">แขวงปทุมวัน เขตปทุมวัน  กรุงเทพมหานคร 10330</xsl:variable>
  <xsl:variable name="DTNT_ADDR2_en">Pathumwan, Pathumwan, Bangkok 10330 </xsl:variable>
  <xsl:variable name="DTNT_ADDR3_th">โทร. 0 2202 8000      โทรสาร 0 2202 8283 </xsl:variable>
  <xsl:variable name="DTNT_ADDR3_en">Tel. 66 2202 8000     Fax. 66 2202 8283 </xsl:variable>
  <!-- End added by AP@BAS on 14-Jun-2017 for DTN-T -->

  <xsl:variable name="PO_TITLE_th">ใบสั่งซื้อ </xsl:variable>
  <xsl:variable name="PO_TITLE_en">PURCHASE ORDER </xsl:variable>
  
  <xsl:variable name="PO_FO_TYPE_TH">ใบสั่งซื้อ:</xsl:variable> 
  <xsl:variable name="PO_FO_REVISION_DATE_TH">วันที่สั่งซื้อ:</xsl:variable>
  <xsl:variable name="PO_WF_NOTIF_ORDER_TH">เอกสารการแก้ไข ครั้งที่:</xsl:variable>
  <xsl:variable name="PO_FO_REVISION_TH">วันที่ทำการแก้ไข:</xsl:variable>
  <xsl:variable name="AgreementNo_TH">สัญญาเลขที่:</xsl:variable>
  <xsl:variable name="QuotationNo_TH">ใบเสนอราคาเลขที่:</xsl:variable>
  <xsl:variable name="QuotationDate_TH">วันที่เสนอราคา:</xsl:variable>
  <xsl:variable name="Buyer_TH">ผู้สั่งซื้อ:</xsl:variable>
  <xsl:variable name="PO_FO_VENDOR_TH">ผู้จัดจำหน่าย:</xsl:variable>
  <xsl:variable name="PO_FO_DELIVER_TO_LOCATION_TH">สถานที่จัดส่ง:</xsl:variable>
  <xsl:variable name="WarrantyTerm_TH">การรับประกัน:</xsl:variable>
  <xsl:variable name="CreditTerm_TH">การชำระเงิน:</xsl:variable>
  <xsl:variable name="PO_WF_NOTIF_LINE_NUMBER_TH">ลำดับที่</xsl:variable>
  <xsl:variable name="PO_WF_NOTIF_PART_NO_DESC_TH">รายการ</xsl:variable>
  <xsl:variable name="PO_WF_NOTIF_QUANTITY_TH"></xsl:variable>
  <xsl:variable name="PO_WF_NOTIF_UOM_TH">หน่วย</xsl:variable>
  <xsl:variable name="PO_WF_NOTIF_UNIT_PRICE_TH">ราคาต่อหน่วย</xsl:variable>
  <xsl:variable name="PO_WF_NOTIF_AMOUNT_TH">เงิน (บาท)</xsl:variable>
  <xsl:variable name="OrderReceivedby_TH">ตรวจรับการสั่งซื้อโดย:</xsl:variable>
  <xsl:variable name="Approvedby_TH">อนุมัติการสั่งซื้อโดย:</xsl:variable>
  <xsl:variable name="Date_TH">วันที่:</xsl:variable>
  <xsl:variable name="VENDOR_COUNTRY_CHK">
    <xsl:value-of select="$ROOT_OBJ/VENDOR_COUNTRY/text()"/>
  </xsl:variable>
  <xsl:variable name="CURRENCY_CODE_CHK">
<!--	<xsl:value-of select="$ROOT_OBJ/CURRENCY_CODE/text()"/>	--> 
	<xsl:value-of select="$ROOT_OBJ/HEADER_PO_LANG/text()"/>
  </xsl:variable>
  <xsl:variable name="BPA_CHECK">
	<xsl:value-of select="$ROOT_OBJ/HEADER_BPA_CHECK/text()"/>
  </xsl:variable>

<!-- 
	The DRAFT word should appear in the following conditions:
	1.Document status is incomplete or requires re-approval
	2.Document status is pre-approved because of pending approval and 	
	'DRAFT' will not be displayed if the requires signature field is checked and acceptance required field is 'Document and Signature' 
	3.Document status is In process, because approval isn't complete
	Note: if document is pre-approved, the internal flag requires signature is checked, and acceptance required is document and signature (binding design), 
	the draft word doesn't apply. In this situation the document is pending signature and the supplier shouldn't see the word draft. 

	Bug#3612548: Draft is not printed when PO is REJECTED. Added the condition.
	Bug#3650429: Draft is not printed in PO if PO is in PRE-APPROVED and Acceptance flag is set to document and signature.
	(Removing the condition ACCEPTANCE_REQUIRED_FLAG != 'S' and modified PENDING_SIGNATURE_FLAG ='N' when document is in PRE-APPROVED status)
     
 -->

<xsl:variable name="print_draft" select="$ROOT_OBJ/AUTHORIZATION_STATUS = 'N' or $ROOT_OBJ/AUTHORIZATION_STATUS = 'INCOMPLETE' or $ROOT_OBJ/AUTHORIZATION_STATUS = 'REJECTED' or $ROOT_OBJ/AUTHORIZATION_STATUS = 'REQUIRES REAPPROVAL' or $ROOT_OBJ/AUTHORIZATION_STATUS = 'IN PROCESS' or ($ROOT_OBJ/AUTHORIZATION_STATUS ='PRE-APPROVED' and $ROOT_OBJ/PENDING_SIGNATURE_FLAG !='Y' )"/>

<xsl:variable name="print_signature" select="$ROOT_OBJ/AUTHORIZATION_STATUS='APPROVED'"/>

<xsl:template match="/">

    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:fox="http://xml.apache.org/fop/extensions">

<!--bug#3650689: Changed the margin-top of simple-page-master to 0.5,
			     margin-bottom of region-body to 1.0,
			     extent of region-after to 0.66 -->
<fo:layout-master-set>

	<fo:simple-page-master master-name="cover-page" margin-top="0.5in" margin-bottom="0.0in" margin-left="0.71in" margin-right="0.71in" page-width="8.5in" page-height="11in" >
   <fo:region-before region-name="xsl-region-header" extent="0.5in"/>
   <fo:region-body region-name="xsl-region-body" margin-top="0.5in" margin-bottom="1.0in"/>
   <fo:region-after region-name="xsl-region-footer" extent="0.66in"/>
  </fo:simple-page-master> 
  
	<fo:simple-page-master  margin-top="0.25in" margin-bottom="0.0in" margin-left="0.71in" margin-right="0.71in" page-width="8.5in" page-height="11in" master-name="details-first-page">
		<fo:region-body margin-top="0.5in"  margin-bottom="1.0in" />
		<fo:region-before region-name="first-page" extent="0.5in"/>
		<fo:region-after extent="0.66in"/>
	</fo:simple-page-master>

	<fo:simple-page-master  margin-top="0.25in"  margin-bottom="0.0in" margin-left="0.71in" margin-right="0.71in" page-width="8.5in" page-height="11in" master-name="details-remaining-page">
		<fo:region-body margin-top="0.5in"  margin-bottom="1.0in" />
		<fo:region-before region-name="remaining-page" extent="0.5in"/>
		<fo:region-after extent="0.66in"/>
	</fo:simple-page-master>

	<fo:page-sequence-master master-name="details-page" > 
		<fo:conditional-page-master-reference master-reference="details-first-page"    page-position="first" /> 
		<fo:repeatable-page-master-reference master-reference="details-remaining-page" page-position="rest" /> 
	</fo:page-sequence-master> 
</fo:layout-master-set>

<!--TM CUSTOMIZE : TM Delete Cover Page , Please see the standard template for custom in the future. -->
<fo:page-sequence master-reference="details-page">
	
	<!-- Block for displaying footer details -->
	<fo:static-content flow-name="xsl-region-after">
		<fo:block xsl:use-attribute-sets="form_data">  
      <fo:table>
        <fo:table-column column-width="90mm"/> <fo:table-column column-width="90mm"/>
        <fo:table-body>
          <fo:table-row>
<!--
            <fo:table-cell>	
              <fo:block> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_PROPRIETARY_INFORMATION'][1]/TEXT"/> </fo:block> 
            </fo:table-cell>
 -->
            <fo:table-cell xsl:use-attribute-sets="table.cell5">	<fo:block> 
            <!-- Logic to generate the page numbers: If the PO has Contract Terms then use fo:page-number-citation id as
                 in the contracts terms. Here we hard coded the id as "eod". If the name in contracts stylesheet
                 changes then same has to change here, otherwise the page numbers will not be generated. -->
                 <!-- bug#3836856: Removed the hard coded value and called the template which will take the printing of page numbers. -->
              <xsl:call-template name="pageNumber"/>
              </fo:block> 
            </fo:table-cell>
          </fo:table-row>
        </fo:table-body>
      </fo:table>
		</fo:block>
	</fo:static-content>

	<!-- Block for displaying header static content with Legal entity name -->
	<fo:static-content flow-name="first-page">
	 <fo:block xsl:use-attribute-sets="form_data">
	 <fo:table>
		<fo:table-column column-width="60mm"/> 
    <fo:table-column column-width="60mm"/> 
    <fo:table-column column-width="70mm"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell >
            <fo:block>
            <!-- Code for displaying the image at top left corner in the first page.
                 For displaying the company logo, uncomment the fo:inline tag and
                 specify the image location.
            -->
            </fo:block> 
            </fo:table-cell>
            <fo:table-cell xsl:use-attribute-sets="table.cell4"> 
              <fo:block xsl:use-attribute-sets="form_data">  
                <xsl:if test="$print_draft">
                  <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_DRAFT'][1]/TEXT"/>
                </xsl:if>
              </fo:block> 
            </fo:table-cell>
			<!--
            <fo:table-cell xsl:use-attribute-sets="table.cell5">	
              <fo:block xsl:use-attribute-sets="form_data" > 
                <xsl:value-of select="$ROOT_OBJ/DOCUMENT_TYPE"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="$ROOT_OBJ/SEGMENT1"/>, <fo:leader leader-pattern="space" leader-length="1.0pt"/> <xsl:value-of select="$ROOT_OBJ/REVISION_NUM" />
              </fo:block> 
            </fo:table-cell>
			 -->
				</fo:table-row>
        <fo:table-row>
          <fo:table-cell>
             <!-- TM CUSTOMIZE : Add DTAC Logo -->
        <fo:block>
		
		<!-- DTAC Accerelate Logo -->
		<xsl:if test="$ROOT_OBJ/ORG_ID = '378'">
				  <fo:inline>

				<fo:instream-foreign-object content-type="image/jpg"
								width="94.5pt"
								height="94.5pt"
								xsl:alt="An Image"
								xsl:image-uid="">/9j/4AAQSkZJRgABAQEA3ADcAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcG
												BwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwM
												DAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCADwAPADASIA
												AhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQA
												AAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3
												ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWm
												p6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEA
												AwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSEx
												BhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElK
												U1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3
												uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKK
												KACiiigAooooAKKKKACiiigAooooAKKKKACijOOtYfjb4m+HPhrbQTeItf0XQYblikL6hfRWqykD
												JCl2G4gdhV06cpyUYK7fRGdatTpQdSrJRS3bdl97NyivNtP/AGxPhXqV00MfxB8JK6dTLqMUSfgz
												kA/ga9D07UrbV7CC6tLiC6tblFlhmhcPHKjDIZWHBBHII61rXwlejb20HG/dNfmc+EzHC4q/1arG
												dt+WSf5Nk1FFFc52BRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAF
												FFZ/izxNaeC/C2pazqEphsNJtZby5kwTsijQu7YHXCgmnGLbSW7JnNQi5Sdkj5p/b+/b5P7Paf8A
												CK+FJLS58Y3UO+4mYCVNGRh8jMp4aVgdyq3AG1mBVlDfnB4n8Uan411y41PWdRvtW1K6IM11eTtP
												NLhQo3MxJOFAAz0AA6CrHj3xxqPxL8bat4h1ebz9S1m6ku7hhnaGdidqgk4UZwozwAB2rIr+ouGe
												GsPlOFjCEU6jXvS6t9Vfsui+e5/DPG3GmM4gx0qtSTVFN8kOiXRtdZPq/ltZBXoPwB/af8Zfs2a4
												t34a1SWOzd99xpk5Mljd525LxZADEKo3rtcAYDAEg+fUV72KwtHEUnRrxUovdNXR8rgcfiMHWjic
												LNwnHVNOzX9fifsr+zT+0JpP7THwpsfE+lRtatKTBe2buHexuFALxEjG4YIZWwNyspwpOB39fmd/
												wSY+Ktx4M/aSl8NkyNYeMrKSN0ULgT26PNHISRuwEE64BAJkBOcDH6Y5z0r+ZOLsiWVZlLDU/gaU
												o+j6fJprztc/tvw94oln2TQxlX+JFuM7bcytr8007dL2CiigkDrxXzJ9uFFJuHrRuHrRcLi0UUUA
												FFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAVx/7QmgXniv4CeN9K0+Brm/1LQL+1toVI
												DSyyW0ioozxksQOfWuwoIyOla0KrpVI1I7xaf3amGKoRr0Z0ZbSTT+asfhWOgor2v9u79mG5/Zq+
												NF0lvCo8M6/JJe6RJGrKkKFstbHP8URIHU5Qo3BJA8Ur+t8vx9LG4aGKoO8ZK6/y9Vs/M/z7zbK8
												RluMqYHFRtODaf6NeTWqfVMKKKCcDJ6Cuw8490/4JsWkl3+2l4N8tXIi+2SOyjIUCyn5PoMkD8RX
												6wDgV8bf8Emv2Z7vwN4av/iDrVrPa33iKAWmlRSqUZbLKu02M8iV1TblQdsQYErIK+ya/nHxEzOl
												jM3aou6pxUL92m2/ubt8j+yPB7JK+XcPxliFaVaTqWe6TUUvvUb+jI7u7isLWSeeSOGGFS7u7BVR
												QMkkngADvX51ftcf8FRde8cavf6F8OrmTQ/D0bGL+1kUpf6gACGdCeYIyTlcASfKp3JkoPfP+CrP
												xguPh5+zkmi2F0tve+L7wWMgV2SU2iKXmKlSOCRHGwOQUmYEc1+ZAr6bw64Uw+IpPM8ZFS1aintp
												vJrrrou1m+1vivGLj3GYTELJcum4aJzknaWu0U90rau2rulte9rXdcvvFGrXGoape3epX922+e5u
												pmmmmb1Z2JLH6muq+Ev7RHjb4F3yTeFfEep6VGshla1WXzLSViu0s8D5jY7QBkrkYBBBAI4uiv2G
												thKFWn7GrBSj2aTX3H87YfH4mhWWIoVJRmteZNp39Vqfq7+xT+21pv7Vvh+a0uYIdK8W6VCr31ir
												5jnXIUzwZO4x7sAqclCygk5DN7vX4r/An4q3XwQ+MPh3xXaGQvo14kssabQ08J+WaIFgQN8bOucc
												bsjkV+08ZygOQc9/Wv54484bp5VjIyw+lOom0uzW69NU1626H9geFXGlbPsunDGO9ai0pP8AmTvy
												yfS+jTt2v1Fooor4U/UgooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAOd+KPwm8PfGfwn
												NofibSrXVtMmYOYpQQUcAgOjDDI4BIDKQQCRnk18O/Fn/gjrr2n6o83gnxJpuo6e+5hb6uWt7mID
												G1Q8aMkhPOWIjA44r9BKK9/JeJsxyttYSpaL3i9U/l0fmrM+T4l4IyjPUnmFK81opJ2kl2ut15O6
												XQ/LHw9/wS4+MWt6v9lutD03RoDn/S7vVIHh/KFpH5/3Pyr6O/Zs/wCCTmg+Ab631fx5fW/irUIC
												kkenRRsunQuDk793zXA4XhgikbgyMDx9gUV6+Y+IOcYum6TmoJ78qs382218mj57J/CPh3L6yrqn
												KrJarnd0vkkk/mmIqhQOBkUtFFfEn6afE3/BZ7wpdXfhHwHrqeX9i068u7CXLYbzJ0jdMD0xbSfp
												XwJX7LftNfAqx/aN+DGr+Fbx1t5LxRLZ3OwM1pcId0cgyDgZG1sYJRnAIzmvx/8AHfgXV/hl4w1D
												QNesJ9N1bS5TDcW8o+ZDjIII4ZWBDKwyGUggkEGv37wyzalWy76jf36beneLd7/e2n207o/kzxty
												Cvh85/tTlbp1klfopRXLZ/JJrvr2Zk0UUE4zX6Ufiw+3tpLyeOGGN5ppmCIiKWZ2JwAAOpJ7V+5t
												lEbe0ijJLFFC59cCvzX/AOCZ/wCyNqHxS+Jen+O9Wtzb+F/DVyLi1MgdW1K7Q5jEZBHyROA7Nkgl
												QmGy+39LFGBjrivwnxRzWjiMXSwlJ3dJPm9ZW09Ulr623P6n8DMhxGFwFfMa6sqziop9Yxv73o3L
												T0vs0FFFFflp+6hRRRQAUUUUAFFFFAH55/8ABfL/AIK9+O/+CVei/C+PwD4b8Ja3qXj6XU2nn1+O
												4mgtI7MWnyrHDLExZzdg7i+FEeNp3ZXzf/ghR/wXX+KP/BTf9qDxR8P/AIgeF/AWlWuleFpvENre
												eH7e7tpA8V3aW5idZ7iYMGF1uyCpBTvnjxX/AIPKev7OH/czf+4ivD/+DRT/AJSR+N/+ya3/AP6d
												NKoLSXLc/ouoor4z/wCC5f8AwUR8a/8ABMz9kLRfH3gPS/C+razqfiq10J4det557VYZLW7mZgsM
												0Tb91uoB3YwTweCAlI+zKK/F3/gk1/wcX/G79vD/AIKBeAfhT4v8L/CvTfDvir+0Ptdxo+m38N7H
												9n066uk2NLeSIMvAgOUbKlgMHBH7RUA1YKKKKBBRRRQAUUUUAFFFFABRRRQAV598ev2XvBf7SOii
												18UaSlxcQqy21/AfJvLMkMMpIOSAWLbHDISASpwK9BorfDYmrQqKrQk4yWzTszlxuCw+Loyw+Kgp
												wlupJNP5M+Htd/4Iu2M+rzvpfxBu7SwZsww3WjrczIPRpFmjDH3CL9K6z4S/8Eh/A/hC8iuvFGr6
												p4unhdj5AT7BZyAjADIrNISDzkSgHAyCMg/WtFfR1uN88qU/ZSxDt5KKf3pJ/ifGYfww4Xo1vbww
												kb+blJf+Ayk4/gVtH0e08P6Xb2NjbW9lZWcawwQQRiOKFFACoqjAVQAAAOABWd46+IuhfDHQ31Px
												Dq+naLp6Ns8+8nWFGbBIRcn5mIBwoyTjgVS+MvxW0z4I/DLWfFWsNINP0eDzXVBl5XJCxxr/ALTu
												yqM4GWGSBk1+Qvxx+Pvif9ojxrPrniXUJbiRnY21orEW1ghx+7iToowACerYyxJya6eE+EK2dVJV
												Jy5acXrLdt72Xn3b2utzj4/8QsPw1ShRpw5601eMdko7Xlbp0SW9nqrH6QXn/BT74M211DHH4mu7
												lJThpY9JugkXHVt0YbH+6DXo3wf/AGmvAnx6Mq+E/E2narPCGZ7b5oLpVUqC/kyBZNmXUb9u3Jxn
												NfjNU+k6rd6DqdvfWF3c2N7aSLNBcW8rRSwOpyGVlwVYHkEHIr9ExfhXgJUrYarOM+7s180kvwZ+
												P4Dx3zaFdSxlCnKn1UVKLXo3KS+9P1R+5uaK+fP+Ce37W8/7Tvw2urfW3tx4s8OMkN8Y12C9iYfu
												7naAFVmKurKpIDJnCh1UfQdfiuY5fWwOJnhMQrSi7P8ARrya1Xkf0tk2b4bNMFTx+DleE1dd+zT8
												07p+aCvH/wBr39vn4Q/sH+E7PWPit430rwpBqTtHY28iyXN7flSofybaFXmkVS6BmVCqb13FcjNf
												/goZ+2Npn7A37HXjf4q6naNqR8M2aixsFYKb+9mkSC2hJJyEM0ib2GSsYdgrFcH+SD42fG3xt+1j
												8aNW8YeMdX1PxX4w8U3nmTzy7pZZXdsJDEg4SNQQkcSAKihVVQABXEerGNz96fE//B3l8ArK1B0f
												4e/F7UbgSqpW6tNOtItmfmYMt3ISQOQpUZ7kV2/wS/4Opv2X/iv4th0nWk+IPw8SaLd/aWv6PFJY
												ebuVRFus5p5ATuJ3PGqAKSzLwD8Efsjf8Gmvxb+NHge3134meNtE+E51CATW2lJpza3qkJ3sNtyi
												ywwwkqFcBJpWw4DKjBlGh+03/wAGjXxX+G/hT+0vhh8QvDnxNureJ5LjS76xPh68mYFdiW5aaeF2
												OWJMssIG0ctngKtE77/g768YaR8QPDP7MOuaBqmna3ourW/iO6sb+wuUubW8hcaOySRyISrowIIZ
												SQRXkP8AwaKf8pI/G/8A2TW//wDTppVfmh40vPFXh2EeCPEUmu6fH4R1G8U6DqPmxf2PeuY4rsG3
												fHkzE28SSDaGzCob7ox+l/8AwaKf8pI/G/8A2TW//wDTppVA2rRP6EviR8TvDXwb8F3viTxf4h0P
												wr4d03Z9r1TV76KxsrXe6xp5k0rKibndFGSMsygckCvyO/4Og/2xPhH8f/8Agn14Z0XwH8U/hx42
												1m38eWV5LYaB4lstSuY4FsNQVpWjhkZggZ0UsRgF1GckV9y/8Fr/ANnHxn+1t/wTL+JXw9+Hui/8
												JB4w8Qf2X/Z9h9rgtPP8nVbO4k/eTuka7YopG+ZhnbgZJAP83H7W/wDwSW/aC/YU+Gtp4w+Kvw/P
												hXw5fajHpMF3/bmm32+5eOWVY9ltcSOMpDIdxUKNuM5IBCYLqdt/wQL+J3hr4N/8FaPhP4k8X+Id
												D8K+HdO/tf7XqmsX8VjZWu/Rr6NPMmlZUXc7ooyRlmAHJFf0lH/gpt+zaOf+Gg/gfx/1PWl//H6/
												kp/Zw/Zw8aftb/GbRvh78PdG/wCEg8YeIPP+wWH2uC0+0eTBJcSfvJ3SNdsUUjfMwztwMkgH6nP/
												AAbi/tnYP/Fmj/4Vmh//ACZQVKKb3P6nlYMAQcg1gfEv4q+F/gv4PufEXjHxJoPhPw/ZMi3Gp6zq
												EVhZwF2CIHllZUUsxCjJ5JA6mq/wS8HX3w7+DHhDw/qd0l9qWhaLZ6fd3CnKzzQwJG7g7VyGZSfu
												jr0HSvzf/wCDpb9hLxb+1f8Asz/D3xf4D8O+JvF/iX4d63PbS6Ro1v8AapXsNQSNZZxAoM0rpPbW
												ahYgSFmlZl2qWQM0tT1P44f8HKX7JfwattRSz8c6p461PTZzA9h4a0W4naYg4LRXE6w2sid9yzEE
												dCa8kH/B3Z+zdgk+CPjgD/2B9L/+WNfmn+zn/wAGzP7VXx/0VNSvvDvhv4bWNxaxXdq3i7VTbz3C
												uM7Db20c88Migjck8cTL0IyCK9utP+DQP40P4bkln+KHwwj1gY2WsYvntm65zMYQw7f8sj3/ABC7
												RP0Z/Zo/4OS/2Vf2jdSt9PufF+qfDfVby4NvBbeM7D7BG4CFvMa6iaW0iTgjMsyHPGORn7yhmS4i
												SSN1eNwGVlOQwPQg+lfyTf8ABRX/AIJB/Gb/AIJjTaTc/ELTtI1Hw5rkn2ay8RaDcyXelyXOwubZ
												2kjjkil2BmCyRqHCOULiN9v3j/waq/8ABSjWtC+L1x+zf4q1W8v/AA94itrjUvBizu839l3kCNPc
												2cY2nZDLCss/zOqI9u21S9wxIJx0uj96qKK+Ef8Agvx/wVL1D/gmn+yvp8Xg+S0X4m/Ea4m03QJL
												iF5E0uCKNTdagFKGOR4fNgRI5GAL3COVkSORCEpHq37bf/BXr4A/8E/5bjT/AIgeOrQ+KYoHmTw1
												pMbahq7sI0kWN4o8rbmRZEKNctEjg5DYBI+Rm/4O7P2cdwC+BvjaR6nSdLH/ALkK/Av4bfDbx9+2
												N8e7Lw94estb8d/EPx1qMjojzGe91O6kLSzTzTSt/vyyTSsFVQ7uwAZh+sHwm/4M7/GeteE1n8c/
												G7w34b1wysPseieHptatRHgbWM8s9q28nOVEWBgYZs8BbilufX/wk/4Oq/2WPiP4gkstZb4jeALd
												ITIL/XtAWe2dgQPLAsJrmXcc5yYwvByw4z9+/B744eDP2hPBUXiTwJ4q8PeMdBmcxLf6NfxXtuJA
												AWjLxsQrqGGVOGGeQK/no/bC/wCDVn48fALS9R1n4e6voHxf0OwRXFtZI2ma7IoiLyuLOQvEwUqV
												CR3DyuSu2MkkC1/wa9fAz4qR/wDBTLV5La48T+E/DHw6tLoePdJnNzZxXdyYrmztbC7g4BnjuJJZ
												lSZcobSbGGFAOKtdH9G1FFFBmfJX/BYfW5bL9nnQbKN541vtfiMmwkI6JBOdjevzFWAPdM9q/OCv
												1S/4KZfCG9+Lf7Ll+dOWSa98M3UetrAgBM6RpIkoySPuxSu+BkkxgAEkV+VoIIyK/oXwyr055P7O
												D96Mnf56p/d+R/IXjbha9PiL21Re7OEeV9LLRr5O7fqu4UUUV+hn5AfRP/BLPxdP4Z/bA0mzhjR0
												8Q2F3p8xbqiLEbkEe+63UfQmv1LHQV+en/BH34OXOsfEbXfHFxAh07R7VtMtHdCS11KUZyh6ApEM
												N7Tr71+hY+mK/nXxJr0qmdNU94xipeur/JpH9h+C2Fr0eG4yrbTnKUf8Oi/Fpv5n5E/8HgPxEGmf
												shfCnwn9nLHW/GEmrefn/V/ZLKaLZjvu+25/4BX58/8ABs7+zjZftA/8FU/Dl9qUen3Nh8NtJvPF
												72t3biZbmWIx2tuUB4WSK4u4Z1Y/dNuCMHBr7f8A+Dxf/klPwK/7C2rf+ibWvm7/AINFf+Uknjb/
												ALJrf/8Ap00qvgT9dT90/ouooooMz+f/AP4O8v2cNA+H37SHwu+JemRJbax8RtLvtO1iOOFEW4k0
												02oiuXYDc8rRXixEsThLaIDpXI/8Gin/ACkj8b/9k1v/AP06aVXuH/B5T1/Zw/7mb/3EV4f/AMGi
												n/KSPxv/ANk1v/8A06aVQap+6f0XV+XH/B29/wAo1PCf/ZRbD/03alX6j1+XH/B29/yjU8J/9lFs
												P/TdqVBEdz8qf+DcP/lM58G/+43/AOmPUK/qdr+WL/g3D/5TOfBv/uN/+mPUK/qdoHPcKKK/PX/g
												s3/wXp8Of8EzLiDwP4T0qx8cfFzUbX7U9jcTMmneHIXRvJmvCnzSO7bStsjIxj3O0kQaLzQlK5+h
												WcdaMg5wQcV/J/8AFb/gs1+15+198QrG0g+K3j601G/vWi0rRPBEkmjlnlf5LaOOxCS3GCQqCVpX
												PHzE5J05vC3/AAUOurs3Emm/tnyTsdxka38SlyfXOM5oL5D+hP8A4LMfCfTfjR/wSv8Ajxo+qPcp
												bWXhC811DA4R/P05RqEAJIPymW1jDDuu4AgnI/mY/wCCVviO/wDC3/BTD4AXWnXdxZXEvxC0O0eS
												FyjNDPfwwzRkj+F4pHRh3ViOhru/jH4r/bu8HfDTW5/iHqH7WWneDL2zlsNXbxLJr6aVNbTRtFJF
												P9p/dFHRmUhuDmvN/wDgmZ/yki/Z8/7KV4c/9OltQVGNkf2HV/O7/wAHePiG/uf+Cgvw/wBKkvLh
												9Msvh5bXdvalyYoZptS1BJZFXoGdYIQSOSIlz0Ff0RV/Oj/wd1/8pI/BH/ZNbD/06arQRDc9l/4M
												7vgjZ3OvfG34kXembr+zg03w3peoea48uKVpri9hCA7TuaKwbcQSNgAIBYH9ya/Ib/gz9/5NL+LH
												/Y3Rf+kUdfrzQKe4UAAEkDrRRQSFFFFACMoYHIzXxD+1n/wSlbxBrN/4j+GctlZvckzS+HpcQw78
												MW+zSfdTc23ETAIpZsOqhUH2/R16jNevk2eYzK63t8JKze6eqa81/TXRnz3EnC2XZ5hvq2YQ5ktU
												1pKL7p/mtU+qZ+PHiL9jL4reF9bGn3Xw/wDFEtwdvzWlk15BycD97Duj+vzcd8V698Cf+CT/AI68
												ba5DN41MHhHRYpFM0azx3N9cpwSIxGWRMjI3OcqedjdK/SnA9BR+FfY4vxQzSrS9nSjGDe7Sbfyu
												2l87n5zl/gdkdCv7WtUqVIp6RbSXzaSb+TRjfD34faP8K/Bun6BoNjDp2k6ZH5VvBHnCjOSSTyzM
												xLFiSWJJJJJrZoor86qVJTk5zd29W3u2+p+x0aMKUI0qaSjFJJLRJLZJdEj8ZP8Ag8WgdvhH8DJQ
												jmNNY1VWbB2gmC2IBPqcH8jXzL/waP6jb2P/AAUq8WxTTwwyXnw5v4YFdwrTONR0xyqg/ebYjtgc
												4Vj0Br9Hv+Dob9nHW/jz/wAEwrrV9BtTeXHwz8QWvim9hitnmuJbFYp7WcoEUkLGLpZ5GbCrFbyM
												T8or+f3/AIJ+/tfah+wb+2L4E+LOnafDq7+Er53ubGQlReWk0Mlvcxqw+7I0E0oRjkK+1irAFTB0
												xV4n9jNFeOfseft9/CT9vDwFa698M/Gmj680lstzd6V9oSPVtIy7JturUnzYTvRgCw2vjcjOjKx3
												f2lP2u/hj+x74JbxD8TvHHh7wZpZSR4TqFyFnvdi7mS3gXMtxIBzsiR29qDOx+Q//B5T1/Zw/wC5
												m/8AcRXh/wDwaKf8pI/G/wD2TW//APTppVfK3/BX7/gpFc/8FPf2wb7x1Bpl1ofhTSbNNF8Nabcu
												rXMNjG7uJZ9hKCeWSR5GClggZI98gjEjfVP/AAaKf8pI/G//AGTW/wD/AE6aVQaWtE/our8uP+Dt
												7/lGp4T/AOyi2H/pu1Kv1Hr82P8Ag6s+GmqeO/8AglkdV09IWtPBXi/TNZ1MvIFZbd1nsVKj+JvP
												vYBj0LHtQRHc/In/AINw/wDlM58G/wDuN/8Apj1Cv6na/kt/4Ik/tC+Ev2V/+Conwp8ceOdVTQ/C
												2l3N/bXt/IjPHaG6026tI3faCQglnj3N0VcseAa/piH/AAU3/ZtPT9oP4If+F1pf/wAfoHNanuFf
												x8f8FPviTffFv/gox8cde1DUbrVHuPG+q29tPcOWdbSC6kgtYhnkLHbxxRqOyoo7V/YOCCAQcg1/
												H9/wVN+HWo/Cn/gpJ8ddF1PTZdKmTxxq15b28iBD9kubqS5tZABwEkt5YpF/2XFA6e5/Rr/wQu/Y
												k+H37KH/AAT6+G+ueFtGgXxN8SvDOm+J/EOtTor3+oT3drHceSZMZWCESbI4lwoCliDJJI7/AGZj
												2r4A/wCCBn/BTr4b/tXfsa/D74bwatp+h/Er4c+H7Pw5eeHbq6C3V9DZW4gjvbYMFM8bxQq7hATC
												xKtxsd/u3xf4z0f4feGL/W9f1XTdE0XS4WuL2/v7lLa1tIlGWkkkchUUDqWIAoIe55B/wU4A/wCH
												bX7QnA/5Jr4j/wDTXc1/LF/wTM/5SRfs+f8AZSvDn/p0tq/Yz/gtZ/wcSfCy2+BXj34M/CCa2+JH
												iHxjpNz4e1PXoCW0DS7a6jlgufKmVgbq4ER+QxZgBmVzI/ltC345/wDBMz/lJF+z5/2Urw5/6dLa
												g0gtD+w6v50f+Dus/wDGyTwQO/8AwrWw/wDTpqtf0XV+EX/B4D+zLqNn8UPhV8Zrf7XcaTqOlyeC
												7/8AdKILGeCaa8tfnzkvOtxd8YwBaE5+bFBMHqesf8Ge3iCyuf2afjDpUdzC+o2Xia0up7cNmSKK
												W02xuR2VmhlAPcxt6V+wtfzOf8G3f/BTLwz+wL+1F4h8N+Pbux0XwJ8Vre1tbrWrhHxpF/atKbR5
												HBwlu4uJ43YqQrPE7MkaSNX9KfhPxdpPj3wxp+t6Fqmna1o2rW6Xdjf2Nwlza3kLqGSWORCVdGUg
												hlJBByKAmtTQorzL9p39s74V/sZeDxrnxQ8d+HvBtjKjyW6X1xm7vwhUOLe3QNNcMu9crEjkBgSM
												V8o/8E4v+Dgr4a/8FHv2r9f+GGi+Gta8Islh9v8ADF7rV7brP4jEaBrqE26E+VMgLOiJJNvhilkY
												xbNhCbH33RRRQIKCcDNFct8cfG1z8NPgt4v8R2ccM134f0W81KCOYExvJDA8iq2CDtJUA4IOK0pU
												pVJxpx3bSXzMq9aNKnKrPaKbfotT5Y/b1/4KWy/CrX7nwZ8PZ7SXXrN/L1PViqzxac4zm3jVgVaY
												HG8sCqcrgvny/iPXP2jfHniu6WfUvGviq8lSQyp5mqzkRMTn5Buwv0XAFed3mtT6tfzXd1cTXN1c
												yNNNNK5eSV2OWZmPJJJJJPJNCXnTJBr+n8j4YwOW4eNKnBSl1k1dt9fRdl0P4k4p4wzTOcVKvWqy
												jC/uwTajFdNFu+7er9LJfRnwL/4KK/En4N63E93rl74s0h5Fa5sdYuHuGkXIyI5mzJG20EDBKgnJ
												Rulfpt8Dvjf4f/aD+Hdn4l8N3f2ixufkljfCz2cwA3wyqCdsi5HHIIKsCVZWP4hJegdyK+2/+CMH
												xNvY/iN4s8Hlnl0y900awoaRisE0UscJKp0BdZl3HqfKT0r5LxA4Wwk8DLMMNBQqQs3ZWUlezuu6
												3vv010t914TcbZhSzOGU4yo6lKpdLmbbjK11ZvWztZra7TVtb/oNruhWPijRLzTNTs7XUdN1GB7a
												6tbmJZoLmJ1KvG6MCrIykgqQQQSDX8/P/BUn/g1/8ffCXxrrnjH9nixPjTwBcO12nhX7XnXNBXZJ
												JJFF5pAvIE2BY8Obk+bGnlzFWmf+hCivwk/qeMrH8V/xY+Anjr4CatHY+OvBXi3wVfyqGS317R7j
												TZnUjIIWZFJBHtXsP7Kf/BJP9oX9sfxHpVr4O+F3iiHStWCyr4g1exl03RYoC6q0xuplCSBQ+4pF
												5krKG2I5BFf137RyMDmgADoAKC/aM/AH/goD/wAG+mpfsdf8Ev8AwfZeCfCOufFz41av4xtLrxXq
												nh3SLjUZbS2Fjej7LaRIhlSyjldA0hUNNJteTaBDFDu/8Gt37F/xf+AP7d/jTxH48+F3xA8D6FJ4
												CutNjvfEGgXWlxTXMmo6fIkSGdE3sUhlb5c4Cc4yM/vDRQTzO1grjf2hvgJ4Y/aj+CPif4e+M9Oi
												1Twz4tsJLC9hZEZlDcrLGWDBJo3CyRvjKSIjDlQa7Kigk/mh/at/4Nf/ANpP4K/EG8tvh/o9h8Wf
												Caxm4ttVsNQtNOuVQE/u5rW5mRxNxnEJlUgjDZJVfCj/AMEQv2sgCR8C/G2R/wBMov8A4uv606KC
												/aM88/ZF+GusfBj9lD4YeDvENyt5r/hPwlpWjalOshkWe5t7OKGVwx5YF0Y5PJzmvhr/AILtf8EK
												7b/gobpLfEn4bJp+k/GTRbPyZYJSILbxhbxj93byuSFjuUXKxTNww2xyMECPD+lAIOcHOKKCU7H8
												ePxg/wCCa/7QPwE1i/s/FfwZ+JGljTrg2st2ug3FzYO/pHdRK8EoPZo3ZT2Jqf4Nf8Ew/wBof4+6
												xYWfhT4LfEe/XU5DFDezaFPZaduAyQ93OqW8Yx3eRRkgZyRX9g+BnOBmgKBkgAE0F+0Z+DfwL/4N
												ZtW+Hn7IvxJ8b/FueXxH8S4PCGpXXhLwV4auHkit9R/s2VrYXMqgNcXK3LIBDCfK3QjMlwkhQfHX
												/BOn/gnR+0F4V/4KA/A3V9X+Bnxf0fSNH8f6FqF/fah4Q1C0tbK3h1CCWWWSWSJURFRGYliOB61/
												VTRQLnYV5z+1r+yx4R/bU/Z68TfDPxza3N14c8UWwhna2l8m5tZFYSRTwvghZY5FR13BlJUBlZSy
												n0aigi5/L7+3d/wbm/tDfsgeJ7iXwv4c1H4x+DCYxa6x4XsWmvcuWHlzacjPcI425LRiWIK6fvdx
												Kr8U3a+MPgT4qvdMuB4l8G65B+6u7WQT6ddx/wCzIh2uPoRX9rHXqM0bR6CgtVD+PX4Df8E2v2gv
												2rr3TpfBHwj8f+ILXXSzW2rPpclrpc+FLFjfT7LZcju0gySBySK/ZT/giR/wbreKP2Lfjn4a+Nnx
												X8VWcHi/QIbhtL8L6IRcQ2hurI27m9uWXDyItxcIYoAUDpG4nkUlD+uwAHQAUUA5thRRRQQFc78X
												/Av/AAtH4T+J/DIufsR8RaTdaZ9o2b/I86Fo9+3IzjdnGRnHUV0VBGQRzzV06kqc1ODs07r1RnWo
												xq05Upq6kmn6PRn8/wDrmlXvhTXr7S9Rt5rLUdNuJLW6t5Bh4JUYq6MOxVgQfpUSXhAzkkV+p37f
												H/BMO0/ae1qbxb4UvrXRPGkixR3K3hYWOpquE3SFFZ45FQABgrBgiqVGd4+AvGf7BHxo8AahFbX3
												w58S3Es33W023Goxj6vbl1X/AIERX9KZHxll+YUIzlUjCdvejJpWfW1915r52P5C4j8PM0yzEypw
												pSqU7+7KKbTXS9tn3T67XR5ol7jAORX3l/wRN+Fd1d+JfF/juQSpZW1qug23QpPI7xzzd8hoxHB2
												wRN14rzL9nf/AIJDfEn4j+ILOfxnbx+CvDgdJLgy3Ec2oXEbKxKwxIWCPkKpMxTbvyFfaVP6efC/
												4Z6N8HfAOl+GfD9mlhpGkQiG3hXr1JZmP8TMxZmY8lmJPJr5Xjzi/CSwcsvwc1OU/iad0le+60be
												1uiv5H2Xhl4f42GYwzXH03ThTu4qSs5SastN0le93u7Wurm9XJfF74/eBP2fdGttS8feNfCXgjTr
												yb7PBda/q9vpsE8mC2xHmdVZsAnAOcAmutr89/27f+CZn7Eniv8AbC1H4qftB+K9B0jxP420+GN9
												G8Q+O49Bsb420cdut3EglhnLrFHFGdsvlfKCU3EsfxI/pBI+1fg5+0d8PP2ibK+ufh/488G+OrbT
												HWO8l8P61bamlqzAlVkMDsELAEgHGcH0rs2YKCSQAO9fz/8A7WXhz9nf/gnf/wAFIv2XfG/7Ifjj
												RJ4dd186X4v0rwz4yj8Q2y2Yu7JCjlpZpIzcw3NyhEkhU+QjRqjozH6k/wCDobxp428c2P7O/wAA
												PC2p22j6f8dfFcthqM8kk0YmkhmsILWKUocNbebf+a6FHJe3hZcFMMDcT798L/8ABQj4B+N/FVjo
												Wi/G/wCEOsa3qdwtpZ6fY+MdOuLu7mZgqxRxJMXd2YgBVBJJxivX8jGe1fnzqn/BsP8Asjaj8K18
												PReEPE1hq62cVsfE0HiW8bVWkQLuuCkjtZ+a+CWAtvLG9tqL8uPGv+Dh/wAQar/wT7/4JB/DT4N/
												D3xD4jstH1G6tPBt9fTXYkvdR0i2sJhJbSy4DKJmSHesWxTGrxbRExjICSex+h8X7f8A8B5/Ga+G
												4/jZ8I38Qvef2culr4w043put/l+QIfO3+bv+XZjdu4xmvXK/O/wt/wbBfsp6b8ErXw1qvhfXNU8
												ULpv2S58Wx69fW1/JclcNdx24ma0RgxyqNC6ABQwfknhf+Cvuqat/wAEbv8Agh74e+Gvww8TeKbp
												7/VovBVt4i1G+VtWsba5F7fTMkiIoX5IXtowoUxxSLtYMitQFl0PuzxT/wAFBfgJ4H8WXuga38bv
												hFo+u6bcNZ3enX3jDTre7tZ1ba0UkTzB0cHgqQCDxivXuCPUGvzU+An/AAbOfsw237KXh/RPGPhj
												Uda8eXmhqNW8Uw+Ib2G6W/liLPNbxLItsqxPIRErwMpWOPzRK25mw/8Ag2h+IXjHwba/tDfs7+J9
												YXxHZfs9+LxpOlalvkJKSz3tvLDGrk7IBJp7SovUG5kB4wAA0uhz3/BuJI037cH7ebuzO7+ObQsx
												OSx/tDXuT71+qvjTxtovw38KX+veItX0zQdD0qE3F7qOo3SWtpZxjq8krkIijuWIAr8qP+DcD/k9
												z9vH/seLP/0v16ua/wCClDp/wUm/4L9fDD9l/wAYajrifB7wbp41jV9EtbwWsWp340+4vjMzIoc7
												4mtrbJbdGhuDE0TSuxBtan6kfCL9sT4R/tA+JJtG8BfFP4c+N9Xtrc3ctjoHiSz1K5ihDKplaOGR
												mCBnQbiMZZRnkV1nxF+Jfhz4QeDrzxF4t1/RfC/h/Ttn2rU9WvorKztt7rGm+WVlRdzuqjJGWYAc
												kV+P3/BbX/gi18EP2Mf2INQ+NfwQ07VPhh44+F+qaZqNvc6drd/cveeZfW9qm157h2glikmSZJYi
												GBiIOdwZPbv+CsXxc1T4/f8ABs1e+OdbNs2t+MfB/g7WtRNvH5cJubjUNJlm2LztXzHbAycDHJoF
												Y/QLV/j14G8P+GvDmtX/AIz8KWOj+MJre30G+uNWt4rbW5bhN9vHayM4WdpV+ZFjLFxyuRS/F348
												+Bv2ftBg1Xx74z8KeCNLupxbQ3mv6tb6bbyylWYRrJM6qXKqx2g5wpPavyV/4Jsf8ET9T/bq+GXw
												l+O/7RPinX7K50PQvDlt8PfC/h25jhtbHQtNt4havdtKkxLXewXDRwNHt8523BpfKt/pP9vn/gmP
												+xT4x/a5m+Lf7QXiXQ9C8QeM9OjtW0nX/HCaDp2rPapFCLxF82GdpUhEMREcwiChSY95LECyPsz4
												NftLfDj9ouHUJPh74/8ABXjuPSTGt83h7XLXVFszIGKCQwO2wsFbG7Gdpx0NeRf8FcP2x/Ev7A37
												BPjP4qeELDQ9T8QeHJtPjtrbV4pZbOQXF9BbvvWKSNzhJWIw45AzkZB/Hr/goT4d/Zw/4Jq/tgfs
												+fF79kPxt4YvpbHWmi8S6D4c8crrsckEckJwxM000a3MMtzBJvkKFQm1VIct9Zf8HRX/AATx8F/E
												L9nTXv2kb3VPFEXjnwNpGk+HLCxhuIF0mW3fViC8sZhMpk/06XBWVR8qfLwdwFldH6Q/sf8Axf1H
												9oL9kv4XePdYgsrXV/G/hHStfvYLNWW2hnurOKeRIwzMwQNIQoZmOAMknmtb4w/tCeAf2edJtL/x
												/wCOPCHgax1CU29rceINYt9MiuZANxRGmdQzAc4BJxzXwv8A8EJf+CRPw2/ZJ+G3gH49+G9c8cX3
												jD4nfDXT/wC1LPUry1l02D7fFZX03kJHbpIuJYlC75HwhIO44YfJn/BNf4C+Af8Agu3/AMFBf2mf
												iX8eIvEHjfSvBt/Z6d4Q0a81OWxttM02e61A28RW1aNgYYrVAFRwjPPcSSLJJJvAFkftB8Ivjr4I
												/aB8Nzaz4C8Y+FvG2j29y1nLfaDqsGpW0U6qjtE0kLMocLIjFScgOpxgiuqr8T/2of2O/DX/AARt
												/wCCxv7KHiT4CT3nhfw/8Z9bHhHW/DjXl1d2pgN5Y21yWklmZ5UkS+jkWOQssU1pHIM/KqfthQJo
												KKKKBBQWA6mgnAJ9K/I//gpt/wAFPdd+L3xC1fwN4E1l9N8CaS8lhdXen3Hz+I3wUlYyL/y7feRU
												QlZFy7FgyrH7vD+QYjNsT7Cholq29kv1fZfpdngcRcRYbJ8N7evq3pGK3b/RLq/1sj9QfEv7QfgH
												wZ4kOjax438I6TrClQbG91i3t7kFgCo8t3DcgjHHORXXDa+CCCDX84Md8PXmvbv2Qf29PG37H/ii
												2l0e+l1Hww8/mahoFxKfsl2p4cp18mUjGJEGcqu4OoKn7vH+F1SFFywtbmmujVr+ju7fP70fn+A8
												VoTrqGLocsG907teqsr/AC+5n7o0VjfDzx7pfxS8C6R4k0W4F3pOuWkd7aS4Kl45FDLkHlTg4IPI
												OQeRWzX5TODjJxkrNH67CcZxUou6eqA9DX4jf8E0vhn8EP2oP+C0f7X/APwveLwr418b2Hjm8svB
												WneMLsXgu7eG71OG4ihtZ2MVz5FtBaKqMrmGONSgUKSP25r5R/bO/wCCKP7Ov7eHxEm8ZePPBMw8
												Y3UUEFzrWlalPYXN3HCMIsqo3lSNswnmPGZNiIocKigSaJn5f/8ABcZf2dfDP7cn7MnhX4H6Z8Kd
												J1/QPFUv/CXW3grTLO2Fu7ahYRW8V29qgQzI8F2PKdjJFyWVRIpb3/8A4OcfFU3wU+PH7G3xXv8A
												R9Wv/CPw58a3V9q09lCHKstxpV0luCxVBNLFZXJRWYBvJfkAEj6z0T/gg1+yX4bTwudP+D2mWVx4
												P1BtU027h1bUUuxcF45A004uPMuVVokKJOzpH84RVDuG+i/j3+z94M/ah+FWreCPH/h7T/E/hbW4
												jFd2F2pKt6OjKQ8cin5lkRldGAZWBANAcxk+Jv2wfhT4P+CifEjUfiR4It/AMyloPEA1q3fTrogs
												NsMyuUlclWUIhZmYYAJ4r8vv+DprxfpX7Q3/AATO+DXxL8GX0WveDdT8T293a30EbqJIbvT7h4ZC
												rKGj+4VZXCsrEKwDZA958Mf8Gu37JOgfEfVtcu/DnjDXNL1IEW/h698SXCabpfIOYXh8u6YjBH76
												4k+8e+CPs7xH+yl8NfFf7Ptz8KL3wN4Yb4bXdqbJ/DkWnxwackW/zAEijCiNlkxIrptZZAHUhgGo
												BNJlPwv+2d8J/F37PNh8WbT4h+EIvhvqUEdxF4hvdTisrCMSOI1SWSZkEMgkIjaOTa6yZRlDgrX5
												1f8AByN410n9tX/gjH4T+J3wv1C28W+BbHxlYa9Pqtu3kpHZmK+04uUl2yblu7mKFo9u9WLblG1s
												eqQf8GvP7JUXxZl8Rt4b8Xy6NJD5S+Fm8SXA0mJtoHmCQYvS2QWw10VyT8uMAfbXgb9nLwF8Nvgf
												B8M9D8IeHtP+H8FhLpn/AAj6WMbafLbS7vOikiYFZBKZJDJvDGQyOX3FmJATSPz/AP2TP+COf7AX
												7U/7Mvhn4i6D8PdJvtM1LSba81CRfHWqSNpU7W8cs1vdeXfskM8W/EkZI2EEGvon/gmB+z7+yr8G
												LLx/d/svf8I9cWN3f2ml+JLvRvEd3rdrNcW8H2iBFmmnmjIWO+OWhbbudkYl42VPF/Fn/Brj+yb4
												j+Jem69aaF4z0HTLHb5/h6x8RyvpupYJJ815xJdLuzg+VcR8AYxyT9c/skfsJ/CP9hPwle6L8J/A
												+k+D7PU3WS9kgaS4u78qXKCa5mZ55Qhkk2K7kIHYKFBNANn5l/8ABBL4y+Dvgh/wUS/bX8NeMvF3
												hXwt4g8TfEW203RtO1XV7e0udYul1PWo2gtUdwZ5A8sS7Y9xzIg6sM+U/wDBTz4GfDEf8HKnh1P2
												hf7Jj+EPxG0K2vZ573U59PtYgumz2UBmuIXjaIC9tEyd6qFZS5CE1+qPjL/gkF+zh49/aZs/jFqH
												ww01fiNY6tb67Hq1nqF7Yq1/BMJo7l7eCZIJJfNUOzPGxkP392TXbftefsG/CL9vLwnp+i/FnwRp
												ni+z0iVprCSWSa1u7Fn27/KuIHSaNX2JvVXCvsTcDtGAfNrc+RPjt/wRc/4J3/sw+BW8TfELwf4b
												8HaH8wjutU8eaxbrdOI2k8uFWvt00pRHKxxhnbadqk1e/wCC43hHwn4B/wCDfjxvongM2h8EaTo/
												hm18Pm1vGvbc2Catpi25jnZnMqGIIQ5ZiwwcnOai+Dv/AAa9fsn/AAt1W9u9U0Hxh49NxMJreHxB
												4gkWGww2dka2a2+9egIm8zIH1z9kfGj9kb4c/tA/s43Pwj8UeFrK5+G91bWlmdCspJdNtoYLWWKW
												3hi+zNG0UcbwRbVjKgBAuNuRQK5y3/BMn/lG3+z5/wBk18Of+mu2r8sP+Cd3w5+CX7VH/BcT9rKP
												4/xeH/F3jyw8Y3tr4H0nxU4mtr+1t7m+t5kW3l/c3DQWkNkscbhisal0XEZdP2j+GHw30X4N/DXw
												94Q8N2Z07w74V0220fS7TzpJvstrbxLFDHvkLO21EUbmYscZJJya+bv20P8Agin+zr+3l8Qrjxj4
												88FSr4yu4ILa41vSdRn0+5uo4QVQSqjeVK4TCeY8bSbERN21ECgJn5U/8HHw/Zo8HeLvhV4P+C2k
												fCXR/Gmja3cv4ltvBek2ds9rHujjSG6ktUCCRZEk/cu3mJ1KgMCf0Y/4OTY2f/gjf8UyqswS40Us
												QMhR/a9mMn8SB+NdTF/wQN/ZIt/A9h4di+DunQ6dp2oHVI3i1nUku5J9oUGW5FwJpUUA7Y5HZELO
												VVS7Z+ovir8KfDfxw+HWr+EfF+i6f4i8Na9bm1v9PvohLBcxnBwQehBAYMMFWUEEEAgBy2Pnb/gk
												j+078Nfib+xJ8E/Bnhv4heB/EHjHw/8ADXQ/7U0LTddtbvU9N8mwtYZvPt43MsXlyssbb1G12CnB
												IFfkd/wRv/4J7fs//Fz9ub9oT4NftG6XpWt+O/DmuCz8K2E2tX+lNqBt579b9rbyJofOyq2sgVtz
												eWC6jaJCP2O/ZF/4JMfs/fsIfEq98X/CnwB/wiviLUdNk0e4u/7c1K+8y1klilaPZc3EiDMkER3B
												Qw24BwSDz/7c/wDwRZ/Z8/4KD63fa9458ITWHjS+t0tj4n0K7aw1MBNoVnA3QTuERYw1xFKVQBRg
												BcAJnkVp/wAExf8Agnz+yL+0/wDDyzOk+D/CfxWfW7G+8Kadd+PNTbUHvklaW0kW2lvW3K01uVQy
												IY5JAsXzO6o36EjoO9fGH7I//BAT9mP9jrxR4e8SaJ4KvPEnjHwxP9qsde8SalLfXCTg7km8gFLQ
												SRthkdYFZGVWUhgGr7PAwAPSgTYUUUUCOY+N2v6n4U+C/i/VdFjaXWdM0W8urBBH5hedIHaMbf4s
												uBx3r+cmG+JHXNf0usMqRjOa/A7/AIKLfsV6r+xX8e9Q0+Ozu38GavM114e1AxsYngYk/Zmck5lh
												+42TuYBXwA4Ffq/hdj6EKtbCzdpzs152vdfK97evY/JfFPLq1WlRxUFeELp+V7Wfztb7u541He9M
												mpo7zrhqwkuyD1/Ouz+BXwc8TftGfEzTfCPhPTpdR1fU5AowD5VsmQGmlYA7IlyCzHp7kgH9jq1Y
												U4OpUdorVt7JH4vTw86k1TppuT0SW7Z+yv8AwRq1C9vv2BPCyXVuIYLa81CK0cHPnxfa5XL+37xp
												F/4BX1LXCfsyfAjT/wBmX4DeGPAumTG5tvD1mIXuCrIbqdmMk821mYr5kzyPt3ELu2jgCu7r+V84
												xUMTjq2IpfDOUmvRts/q/JcLUw2X0MPV+KEIp+qSQUUUV5p6YUUUUAFFFFABRmivjX/guz8evH/7
												PX7Bt3q3w+n1DTLzUtZtdM1TV7ESrc6NYyLIzTpKhHks0yQQ+Yen2jAw7Iw7MvwcsXiaeGg0nNpX
												e2pjiKypU5VWr2Vz7KzRX4Kf8EA/j38QbP8Ab98N+EdP17xDdeD/ABBBqU2uaa00k9iAtnJKtyyE
												lI5PPit187AY7tm7DkH9669LiLIp5TilhpzUrpNNaaO61XTbuc2XY5Yul7RRtZ2CiiivBO8KKKKA
												CiiigAooooAKKKKACiiigAzXx78eP+C4XwO+CXi2TRbW81zxxcwAia48NwQ3FnC/BCedJLGkmQes
												W9RyCcgiuQ/4L7/tT3/wT/Zn0fwTo891Z6j8TLma2ubmIshTT7cRtcRh1YFWkaWBCCGVommUjkV+
												LqXORgEYr9L4P4Lo4/D/AF3Gt8rbUUtL20bb9bpJdj4Lijimtg631XCJcyWretr7JL01P3T+BH/B
												b/4H/G3xZHotzea54IuZwBDP4jt4bezmc5yvnRyyJHgDrKUU8AEk4r6o8ceAND+KHhi50XxJo+ma
												9o95t8+x1C1S5t5trBlLI4KkhlDDI4IBHIr+YhZxjgkZr9oP+CCP7Uuo/Gr9m3V/BOsS3N3f/DOe
												C2tbqTLBtPuFkNvEWLElomhmQDCqsYhUZwcPi7gyll1BY/ASajFq6b2u9Gnvvb79zPhniirjqrwW
												Oim5J2aW/dNbbf8ADHpc3/BGH9nKW6jkXwDNEiZ3Rrr2pbZM+ubgnj2Ir2j4Cfsw+Af2YPDsul+A
												/C+meHba4x9okgVnubvDOy+dO5aWXaZH272baGIGBxXeUV8Jic3x2Ih7LEVpzj2cm19zZ9hhsnwO
												Hn7XD0Yxl3UUn96QUUUV5x6IUUUUAFFFFABRRRQAVS8SeGtO8Y6Be6Tq9hZappepwPa3dndwLPb3
												ULqVeORGBV0ZSQVIIIJBq7RTTad0Jo4L4O/stfDf9nu6v7jwN4F8J+ErjVGLXc2laXDayTgsXCMy
												KCUDE7UztXoABXe5oryj9u3TfEWs/sV/Fm08JxXlx4jufCOpxafFaRySXU0ptZAEhWP5zM3RAvO8
												rXRFzxFaKqSu5NK7d/LX0IaVOD5VouiPL/F//Baz9mLwR42ufD978VdMlvrSbyJZLLTb6+sw3H3b
												mGF4XHPLK5UYOSMGvpvw74i0/wAX6BY6tpN9Z6npep28d1Z3lpMs9vdwuoZJY3UlXRlIIZSQQQRx
												X8iiXmeSTk/jX9C3/BvPpPi3Sf8AgmtoI8TBY9OudXv7jw3HtCummvIDlhgH5rr7W6k5yjoQcEV9
												5xbwbhsqwcMTQqNu6TUra3T1Vkrbba6ddNfHy3NKuIqunUikrX0/U+4aKGYKCSQAK+W9c/4LTfsw
												+H/Hb+HZ/i3ost/HcLbNNbWV5dWG5sYIu4oWtynIy4k2DnLDBx8JhsFiMQ2sPTlO2/Km7etj16la
												nTtzyS9XY+pKKp+HfEen+L9AsdV0m/s9U0vU7dLqzvLSZZ7e7hdQySxupKujKQQykgggirlczTTs
												zRMKK574lfFzwn8GdATVvGHifw/4U0qWZbZLzWNRhsbdpWBIjEkrKpYhWIGckKfQ074c/Ffwt8Yv
												D/8Aa/hHxJoHinSfNaH7bpGoRX1v5igFk8yJmXcARkZyMir9lPk9pZ279PvFzxvy31N+gkDkkAUV
												+Xv/AAX9/wCCj/if4Lazonwh+H2vX3h/Ur+zXV/EWpadMIruKJnIt7WOZH8yFmMbySDarFDAAxR5
												Fb0MnymtmWKjhKOjfV7JLdnJj8dDCUXWnrbp3fY/QnxZ+098NPAXjEeHdd+IfgbRfEBKAaZf69a2
												14S4BUeU7h/mBBHHII9a7kMDyCCK/k3SQjgHNfev/BE3/gpH4m+Anx98MfC3XNQ1HV/h/wCM75NI
												srJ5PMGiX1zKBDLAG5WN5n2yIpC/vWkwWUhvus38OJ4bCyr4arzyirtNWulvbV/d+J8xgeLlVrql
												Xp8qbsne9vXT8T6s/wCDkT4M6v4m+C/w+8dWCST6Z4N1C7sdSSOJ3aBb1YPLndgNqRh7YRksRl54
												gOTX4/x3pzjOcV/Uv4t8J6b488Mahous2VtqWk6tbSWl5aXCB4rmF1KujKeqlSQR71+Wv7T/APwb
												dzX+v3WpfB/xpp9jZ3l0GTQ/EglEdhEQxYJeRCR5ArbFVHi3bSS0rMPm6uCeL8JhsKsBjZcnK3yu
												2lm72dttW9drHDxRw3iMRiHi8Mua9rrrdK1/u+Z+XEd77mv2C/4Nyfg1q3hb4KePPHV6kkOm+M9Q
												tbLTkkhZDMlks4knVjw6NJctHkdGt5AelcP+y/8A8G4E+meIrPUvi94006/srO63SaH4cWUx38QC
												lQ95KI3jDNuVkSLdtHyyqxyv6k+GfDWn+DPDthpGk2Vtp2l6Xbx2lpaW0YjhtoUUKkaKOFVVAAA4
												AFHG3F+ExOFeAwUufma5nZpWTvZX3d0tdreouF+Ga+HxCxeJXLa9l1u1a7+Reooor8nP0MKKKKAC
												iiigAooooAKKKKACiiigAoPPHrRRQB87+P8A/gk3+zp8UPjNJ4/174VeH7/xPcXQvrmUyXEdpez7
												izST2iSC2mZ2JLmSNvMJJfdk19AaPo9p4e0m1sLC1t7KxsYlgt7eCMRxQRqAqoijAVQAAAOABirF
												FdFfGV6yjGrNyUdrtu3pfYiNOEW3FJXPgj/g44+NPiP4Q/8ABPRLTw9qE2mL418SW3h3VJIWZJZb
												J7a7nlhDKQQsht0Rx0aNpEIKsRX8/qXP6V/Vn+1d+yz4R/bL+Bmt/D7xraTXOi6yikS27iO6sZkO
												6K4hcg7ZEYAjIKkZVgyMyn8d9f8A+DXH4zW3i64g0vx98Mr3QUuNsF5dS31teSQ5++1utvIiPjnY
												JmGeN/ev1TgPiPLcHgZYfEzUJKTd31Tt17ra33dbeBm2Aq1aqnFXVvuPfP8Ag1z+NniHxj8Jfij4
												I1G+lu9A8GXmnXukRSuzmyN6Lvz40ySFiL2yuEUAB5JW5Lk1+lPxx+KVr8Dvgt4v8a31tcXll4P0
												W81u4t4CBLPHbQPMyLuwNxCEDPGTXmf/AAT3/YG8J/8ABPH4CweDfDckup6hdyC81zWrhNk+sXZU
												KZNmSIolACpECQijku7PI/pHx9+FUXx1+BnjTwRPeyadD4x0K+0OS6jjEj2y3Nu8JkCkgMVD5xkZ
												x1r4LPcdhcZm1TE01anKS8tNE389X31PVwlGpSwypv4kv6+4/mG/ag/au8Z/tifGbU/HPjnUzf6v
												qB2Qwx7ltdNtwzGO1t4yT5cKbmwMkkszMWdmdpv2Vv2r/GX7HHxn0zxz4H1I2Gr6efLmhkBe11O2
												YgyWtxGCPMhfaMjIKsqupV0R15L45fBTxT+zb8VdZ8F+MtJu9G1/Q7h7eeGaNkWUKxAljJA8yJwN
												ySL8rqQQSDWn+zP+zr4v/a1+MOk+B/BOlzanrOqyqrPscwWEO9Ve5uHVWMcKbgWfBxkAAsQD/Q8q
												WD+p8r5fY8vly8tvutY+FlRre15lfnv87n9THwf+Jth8avhL4X8ZaVFdQ6X4t0m11mzjuVVJ0huI
												UmjDhSwDhXGQCQDnk9a/Dj/g4g+Dup/Dj/goHc+KJ0u5NJ+IGkWl9aXDQusCS28S2kturn5WdBDF
												IwU5UXMeQNwJ/cT4MfDCx+CPwf8ACngvTJ7u503wjo9potpNdFWnlhtoUhRnKgKXKoCSABknAHSu
												W/a1/ZC8D/tr/CK68GeO9NkvdOlfz7W5t5PJu9MuArKtxA+CFkUMfvBlYEhlZSVP8/8ADmd08rzL
												6wk3Td4vvyt6fNWTPs8zwMsXhvZP4tH8/wCrn8vKXNe0/wDBO74d6l8X/wBuf4T6FpUM81zL4nsr
												yUQzJFJFbW0q3NzKrOQMxwRSvjknZgAkgH781f8A4Naw+tytp/xtaHTZJXaNLjwp5s8EefkUst2q
												yMBwWwgPUKOg+2P+CeX/AAS2+H3/AATv0K8m0J7zxD4v1m3jg1PxDqKIs7oqoWgt0UYgt2lUyeXl
												2JKh5JPLj2/puccfZasJNYSTnOSaSs1a+l3dLbstX+J8phOG8Q60XVXLFO+6/Cx9L0UUV+GH34UU
												UUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAHH/Fr9nnwD8fLezi8c+C
												fCXjGPTjIbQa3pFvf/ZC4Acx+ajbC21clcZ2j0FO+E37P/gT4C2V3beB/BfhPwdBfsr3SaJpNvp6
												3TKCFaTykXeQCQC2SMn1rrqK2+sVfZ+y5ny9ru33bE8kb81tQooorEoKKKKACiiigAooooAKKKKA
												CiiigD//2Q==</fo:instream-foreign-object>
			</fo:inline>
		</xsl:if>
		
		<!-- DTAC Logo -->
		<xsl:if test="$ROOT_OBJ/ORG_ID != '458'">
		<xsl:if test="$ROOT_OBJ/ORG_ID != '378'">
			<xsl:if test="$ROOT_OBJ/ORG_ID != '140'">
				<xsl:if test="$ROOT_OBJ/ORG_ID != '144'">
					<xsl:if test="$ROOT_OBJ/ORG_ID != '218'">
							  <fo:inline>

							<fo:instream-foreign-object content-type="image/png"
											width="94.5pt"
											height="48.0pt"
											xsl:alt="An Image"
											xsl:image-uid="">iVBORw0KGgoAAAANSUhEUgAAAG4AAAA6CAIAAADeJvtNAAAAAXNSR0IArs4c6QAA AAlwSFlzAAAOxAAADsQBlSsOGwAAILlJREFUeF7tfHmMXFe55znn7lW3qquq97bb W3cSJ47txHFeEpyEZfQUlmFgHkiBIAYxg0DiwYAEIwEaQQDNCDQsA39EeSRRSJ4g QwYmkExIHkEhm7PYTrzhJMRur2273XvXevczv++c6krjmPCM+o01aK5K1VW37j33 nt/5fd/3+75zbnMpJfv/23IgwLMsW452dBspvSRnqcFSQTsMJk3WZCxkjDNms9hj kZGmMrPS1OG2wLG4fCZYwniDsVqWzcvsdCZPpel0JBuMJ4IJGfUIsdIwhwxzwDLL jPmZ9BImU4n2GQcVRGKyCH8Yc9SFLsi2vFBmkmXUE8JGQSkISnQyZgw9tllms4Rn KR0lDW6YIeNNg0A8FcaH681XJqYmm8FsK5lptBZacZTyNJM8qXlWkjesgVzxqrXr rlpZucR3exkzASUumKbMEJkpAgWi+9cBpQRRFDUEOrXIdeIdcFNQgkAmlxwuBWgm CXPdBckPNVo7JqsvnJx6da4xlYiA20lq8FRaGXO4cNBUWk9kK0ojGUd5g62rFN+1 YfRfrexe49g2oIwTZgqgGSl+2n8dUAI9sA/UILPTr7YjVjaXEbwcPOVgJq8xdiiI do2d2H3k9KGFcIp7dcuPTD/hZiYzcM1IIpFGZpraphNlWV1GVtFhWTOaPrZapP/h ui23XHZxRUocIHG8wTMuJOO49IUCczkNXEOpoxgZMMvwIhThOiUHHeE/E4NFnI8z 9tjJ2ecnZg8eGa82Um6XEjMXSCOIZEInp1xkgqOxhMNdBq5hd7U8I2I1Jlo8ni/X Jj+08aK/33rlaindDFDiJPhNwKhM4gJ5S+XRlmlDW5qPypxBSJg7BRUG1uCbIBda 5fzlevLowZn7dp986mjzFKss5CpTWTodLLSyWiZqnM8LWTfShkhCI00EwphphZYV R3HajDBANnca1Waj2sC1CEJcD2ZOrmVxEJepO+fbzHJCScyQzEJAaBMDfQMSBCjn HDg2BDsWsycOn/lfL+w/Vee12K2FIsxELGQiwoQ1pNniZpCJIOVJgniT2lFqwUc2 m9MyqvqOacwtRGPH1pe6r16zNqf6StbNYeDqmhdU1y2ngVNPgJtylvB2II0gKIGk xYQVCDbO+M9eOvzE2PjJmDd4V8YtGD0sOJZhyuKUE+ppJhBHmHQy7khpEafTBUe0 3CTj1Xq+2bhyRf/NN16xbXWxIKVHcSY1CE34YES7CyWElE9bTl0Ja4bJ6bgDyyTP iV0gvh0xMc/5Px2bfmDf4QMLtShXlCmQQnTJ4gyBA4AKaVitCGSUmTAl4jzQIX4j /ky4zdOliG3uHbrp0ktvuLh/TcEg0YODyL7hkXENuOO2kz5fw1yu45cbSgCo5CRC h0ITXTUDZsAFHmqldzy1Z/f03KybTzzfkHYaSxAyTYUEsVJBuQLnggPeIIqbSdYU Ruba6bCfbuzOXzc8ct2KgUscUUCcjiWCGq4CSaXMQBhMIH4Tmsvqsc4L5WWFUneG HJiCEi6MA05RY3ya8YdePfq/9x6aNN3Qr6SGLUKZhbIVp1HC41QkaZbFCQSQw+I8 D4pmNFA0R4fKw93+pauH1vi5HsZKjBWh2GMKY8yEY07hH9A+/DNFvL8mVi7SAvEU GlL1zDBbSAc5P1Bt3PHUzhMRD/2eKjxgKtxmlLXCahjWkyQV3LLNnCl6c+aasr2h nFtfyo+UC2u7vCLLgjjODNMWhgXd3nbHGVHeINbrUAfNRAJdqbALtS0nK0GXmEIM gjjyGWXchoGkcJLxX48d/+WeV4JcJbS76pFAoEgmxvI8KhYL5UK+r9S1uqcymHPW VroGHNbFWI5lLpNIXWQaA0khbJPBAyBbUgasvAeUvuI/TB0HIuz8PwwlmbG6fUoS 8QlQtghK6ZIuUTLS4C3Of1+t37tz/8k4W8isWsxNI1+0+SUDfG3FXen6va7T7Ti9 ll2hMUAbZL5SAimDJBSyyERwxHrsQ6CxCEI1ShD7+KjvQUH5xxvOVVLp/54+Qr0g I9PouLl20CB42sIGHg/D38kj6Pbb2FHxAsFXkmxEExk3UOChuEvCnCIBwKxzvmd2 /p7Hn5WFXKW313e9td3dfY65qpQH+xCIwWIgYVMgUbpeg6NAoqYzaVLzaK09cIuB jTL7TKZZmhnC5qINpRYkhmHggxAiSZCjcnyNoihNU8/z8E4jpH7VWOOrxl1v2KMH QB9wzg0H6Bb0ibgKkQm3olMS2vS5Ko/V+0jYoFlUtCBOYF/wSYLokuCO0HkTX4hD yjXKiG7AcISNBrAXBSHU1pBr75pbeOSZHavXDG9bP9JviAFh+IsZeueydM3OiGoo UcdQAQbkbIeUP+6WQoHAx01RuqMwAmr6qF//+tfHjh37wAc+0NfXBxyxHz3H1mq1 TLV1gCMUFCJAR8OqoXwjvvoSaAStAT6NJj63j88SiGDVLO5HORz9jao76otEKgLR EsaWYZkGOKRuXNEOIw7piHIkClwBKUJpcYHSQwRpaKDQaDQ5PxVFD+39/fFjJ27a etW/WTs8KJmfSNFMmGeAkGC8qnG2xSjBQEOjvtPNUHZtKKa/cVtqvB0cNeOA3Yc/ /OFHHnnkN7/5zY033ohf0WG84yeACBQ6iJ8FWecqmptnIdu5Ysd7aPQBKB0MiUz3 Dnhgq5x8FZozGceLigmZTHAP1EUScnHKQEbLEjgH8WQmSKaC1lSazgbpQtCSceqk qIRFEQjqmpFtTiXJ0bnZV48cu2h47S3X/M0NtuhOZTGg8IswrgQNx0V0EUSHD/I2 2oapBtIx93NAiW5oc9adIVpLqT9gz/vf//5f/epXL7zwwtVXXx2GYbVaffjhh9eu XXvDDTd0yNUBSzOxAx8xaYnH0Hhp9PUp+sNSOlObBCWQUfVY2LHKVrjNkHAgjlCt kdu8JRBPFPMZrzM2EWfjC9Ujs82xk9Pj01Ox5F6h6BeKFGjSqLvko4m5VnWiPn+6 1ZxLIqQxlVzpmuFVb+vtvq6UG8YtNFUBXL0wfvADeqwAL0GpzV4R809t6J7ule4S uIY9CPa2jeFg+PCRj3zkwQcffOyxx66//nrg8tRTT731rW+Fvd99992FQkGfrrHo UA9ndcAFcB2rB0xLDVlzsDOKuhEaRXXv9CJTJv1BEkQVeDAQgpsi5jxCzYuLWcZ3 zlV/fvDoXbsO3L59zz/u2PvM6YmT3Gr4lZbTNZ+YseEV+/sTDmefdtn5kZ7hS/tW jVZW9OTKc7PVR7e/eNsjz92589DTtWQeOsih2puqwenIg3I7VXlo0xFGl5jOtelb V+6a3jVf0EPHcTQu6Cd8In4COppfGmKEHWz6YI1mJ3TgLByDFvBuwe4UXjTAKoJ1 iNxhPfZr/4ufdHAz4QrhLDUDKKhol8kM/EgzNBx+kE9l7NDpud2nJ/aOnxqvN+um mXo53tsLsGPJ6qB2lpZcR1g8atZKJhso54cLfsF25tLsSC34/ZnZscicSKv7Zmf3 PzPxxJm591y+6oNr+7uRVEqJ2RhSOgSq9jM0ym+utNFJ3RPNx7m5ud/97nevvfYa gBgeHr7kkktGR0dLpZKGCUDMzs5OTEwAI1j6yZMn8/k8wAI3cQC80ZEjR/bt2zc+ Po5fcfDq1auvvfbadevWddiHE/WRBw4cePnll4MgcF23q6trw4YN8BhoCicSnYMs iWlOK3MQJkjbCIrVNhkyChAIKftnqs8fnjowdnymVsMUQubmpO3gAwYUZYgsTZBq 5Dyn7Of6bDmaE5v7e0f8fFnNWCHVGU/Y9pPBjtOnXzh24kwYNJDpJVFfzr7lspU3 X7lxNWOFVOZ13QMkgFtBKkTimxwSoizQOic1gRG4AByffPLJ7373u08//fT8/Lzm HdC86aab9uzZ8+KLL2I/iPnFL35xZmYGWPf09KDzGAOA9c1vfhPHf+Mb30B0Ghsb q9fRY9oA0+WXX/6xj33sM5/5DL5qkz9+/Pi3v/3txx9//PDhw8AU8AFKjBliGg4b HBykm0EvqCzDIGswtQfLoVnAVswaNh+T8uHXDu08dGZiLgmixC/2OXkfjh4CAAaP 9NcyUL7JLCMrOmYpL9eWclf3lC8ynW7giKkCyfKCt2RaZrLfLzqWlyJvyXlZEE4m jX888Op4tfqxjZuvr/hUntBRXBfdKWtq27em6RtJCvoAkYMHD37pS196/vnn3/72 t7/rXe/q7u4GoAAXDhEd9n0fuOBsMBRIwRIBPXYCGvBXB/ozZ85MT09/9KMfveqq q4Bao9E4derUPffc89WvfhWtQQZoUgPH22677bLLLvv6179eLpdx4s6dOx966KHt 27evXLny05/+NPnWGglLUAHVQcYjloUs8/isxZ6crt6za8+LM5Op7HLNsmk5BiVv FNgN1MNk7Iq0YElXhnkeD1UKoyuHNlSKFxt2SWY5FpNoQpLMzbFW/Myp6mFpP318 4sDkXDU14gQ8Qz182qnOvnfdZf/xLVveUjGNhnJNSJVMFtJcLnkZYiVGDkieK/4A kc997nM//OEPP/nJT6LnK1as0LRaWFgA137wgx/kcjlE7euuuw4AAd+bb775fe97 3/e+9z3tDYEpwJ2cnITtg4YadGxg8Xe+852vfOUroDbAAkY/+9nPPvGJT4yMjNx1 113QA/owjM0rr7zyzDPPgJhbtmwBK3GPUCPkK0GECFKxi5/m7P49Ez986NknDp6u ipKV6815FccrhplZy2Rgm6FtZJ5peAaXkS+TdZ61tadyY3d5g8LRY7GBmk8SQLNT DR0agUVO3u4ulwq2l0uMfGKbkcXtXl4efvrgiX94fO9z03ENA4K5XbOdDEIItWdq 4Lz/BI61Wg0+DvyCiQFH4IINnYTpYc/GjRu17wMTsae/vx8YAUF8gJljD77iJ3Bq 69atHRxxOo5597vfjWE4ffo03AIICHHabDaBpsYRe/COkcBX8HHz5s06uAk74Q6K AwrRlsX3N6Pbnz942+Pbd09WrcpqxxkyYscCSWII3BSJjFVweMGCZ81YhEntwbx7 xdDQNb2964VRybAbVIKMgJqC8asaOrSOQPkszvluAbcMiZ5yF14u8aPYb+a6f3fi xPefeOnp6dYMHDR8Nid1iXaQSsJFvEmhB9EGSCGG6KCstQt6hdgNh7hq1Srt1DSJ tGMFxQCK9oAYA+zUX7EBdxANw4M9cAg4GKcT14TAhfAOVpLzUYIBP+lMFI3gSB3x hZUZFnITtMX5H5rhXc/vu+uFZ08gHA+srrVsGXo4JEoDSG/HMjxMpKaZ2QqsZoAS 7Oqurk2rhjcODa504XsAV2wis6J5RjJUZMgIaIghGc+aYQtT4I7vCcdASoTQzQPk l2VZ7E+GVmxfmPzvzz333EIVk2gqvcFyCyRSVNh9k3IEIEO3gWCHj1oeAVkAilFD 7xBtNVL4oH/VGkh/0AoJ0R/eAKHp85///Gc/+1kQ7Vvf+hYO1r/qswAfUNNDhT3A UWsDPUj6AyrRpLxx2xMpu+f5PY8dOzJVzDVyXmB5nlu2UhtrICILqRZGlQEvu9Uq BclAxtbZ+Sv6Vmwa6O23KQslRQWZRaEDg06rMrg0UYQARUGteqsRwfBzJpZnhFDT qKFL28zcSFpBobTQ3bWrOnPvnj1/SGJUktrZK/iZ6tUI594A2aK2a4tEfAVBtJwE yton6pPJHtQGFus9ABox58tf/vJ73/tegPjTn/4Ujg/ZEcYUjhXuFadrGQTU0KCu WaBB7Wd1yx0cKR+HJwKNpk3+y/2HHz149Bj6WOhhtptiEtAILTuweWqTejOkgRpt YNlxxZOrXHNz0b+24q83QS2cg4Y4Mnvya9rJkSSwPVQ3cB9cNhLFa5aUTMNFciUw k1gPkxkOeQu+in67vP6FE417t+87nCQh2gKCCIemQO1HYqZbFQM6LxgzxsuGKbhu K4Dn4tQxlc7pJA8QADX0X/s1DZwuYaDzWt/Aou+77z5ELYgnxJmf/OQn999/v37/ /ve/D+hh71QrVUmUPrFDUu2U0Y72G/owkVqsafK9c80H9v3hBLOTHJQMCq+ILEhz atys20biSSCCKjYcYSyMIG9FIxVv62D3+pxRyqST6DkqsBwXy0iwI8cG1eGCMxS4 BfxNTNYQm1FcEUbBgM9KY2Oe5wJDhm5q+bw7SSuhs+LJsbmHXzk1KWUAMtN0rMrC CTcydC09qWRFE2SsWq+HUWyZZIbYSA6r5F0nJDqVBCLa+jStdC6kiQb/8MADDyD+ INx/4QtfeOc737lp0ybEYoQRBH2EHV3+wbnatHVowpihQezXaOrh0cYuQoPNMvbb /a+dXGhYTkkktoCzjoSLGKOyIU7qkaZQfcv0pczF0WDe27Cid6TiuJhW0amSylF0 tU4X0FXndB/BLWRUCFoolQBijvqSa6Ntyk7VcTge1V1w2uVu4dEdr24/Nl/HqiEX XMeaF6zUAMHpIupF/kJ/xqWiKES9pe0CqGZP2Z4OzTpX6eSFGlmdC+oNWCDPQTQH fDr56xANckofA3Zr+akHQ6Om01a863QLrqBdHo0ZP1qP9h8/hQTRMfNWbHuhmYtN LzMhWGAVzEIfTdcUbhyXs2wkX7hiaHC05OfbkymqXEn+FmSgmq/adK91jzm0IabA MJQwegwMNznVC7mHdJ8KzCh+8gjLWkzMPEox1Ux+uePAWJTNA2AT81/wmOg/OWGd mlMNWrFgYKC/u1JeqC4gbVWdpDVDFEgtC04QghEeDf3UkV1HD0DTKa8BO4hw7Ieq 76ShupqJndoVauerqYeMszMMemC00wB/dbGDymUHT5+ZjmJME2DZowWRkNq5zPGE a1s2N63UyEyHe0bmNOuDjG3t793S3dWnbpzqYbAqWmSF+yU3qatMHTD1B+hPLBtg WLWCc/AFqakUuJDQUKIiJ1tchjQklpsVK3unavfveO20ZAExSwXKNhHbPCdySYZi 1KXrL0Xu+uO77w7CAIeQrxYCPu7WW29FCgQUQLoOB/ET6NaJ6Yg/UKPIZPbu3avp hnfgAjThQ6empgCTDj7Q6vgJ6hIHa3x18RjCExr+xIkTevyM/3TrrbtOTb40MbvA nJh5PHMgj1zTdMDDnEAOJHjicpmXcQ9LN/eWrx0eWGcbOVo4gTit7gBVlsU8TxVt 9TIJeiEENDg7Hgbj89WIW5inibHsL85otjZBbRM6KUZJxIQ7hZZP4F3N1HBqSXJq cnplb/e6kgcfbdEilrM37RALef+5555FXRIlHaBYrzeOHT165513/vjHPwZkuLcP fehDEJg4EtCg5vbSSy/BWpFQw7QvuugioAYskK1DSOIzuIy0Ha4TWQ3MGcNwyy23 4CfU4XEiUnVAiQFAvIJ0x1DhSIwZyh/XXHMNeZIZKbGo7HuP7/pDiyden8xcJzV9 TGXlOc+niR06IsXf/jTbVC69Y2RwU8UDe6mcBLoQkWB6pra7xTkM5H7aC1JZd5rz 52q137526ExmVhOnFhq1FlZLWrWa0YzDUCxgyZ+LJVVgdGyGiYPA3AzrTnXi3aOV z7910ybTLEBZnUMRkSVge+jBX37zv/zXnTt2YgyhvLKE5M7HP/7xHTt2IBdC4viO d7wDt4LS7+233/6jH/0IehukRlHnF7/4BbJpFDXuuOMO5I6ILTpJB/Q45d577wVn Ue7EV1wFZZGvfe1rUKBQYPAbYDeGZGBgAH4Wcmrbtm3U25qUv2/I//ZPO549M9fw u0PEK2kVbbPgMROhx4lzPLUa9VE3957LRt/SX+yBREcAMTKJviubFMzWVRw9tQBX jMqjKn8jp6dJ8H21+oMvvzrB7KnIrCVWKzGCwKpVzWYUNPkst5ueg/KHIxOnxfKR 4SRp5GYLffVTn9gy+vdb1yPWtddzaUCVKFIjBezI8YNkt//oH1CWUOMrr992Peo6 P//5z0GlT33qU8h8dJSAo9i9ezfsER4AAKF+oTXmo48+ioPxAUxEHvnBD34QQANi 8BHZPUxeGzWqcJBKGB4duwE0CihIwOEBdDWTVoxNSHbf/pN3bn9pws2lftGzvLLr 5E3MjoauldlxY8h13ja69m0reldCDWClFCpIKDHCJmlmAd2kyWl4aUov1Qfsos6q WifM7FC19j/3HTjFnYnMqmZ2ADQbvN6wGqi5yVlmNl2LuQJRKBeZxSaVSkLbk+nh PX+7svKf37NtY97yMAGu1JbSjrSpKUb1XcmbN3qAzp5O1NbecOmmIX6Tc/XVtJxa Gq86s2z6AN0Ccqus35Q3XNS37eJhP5w362e8ZN7n9YJsFuK6U5te45nv3HjxthW9 JaCHEItZRugY+DnqgA6qSpIvzZcX5ZC+RsFx+vwCayFVhH/G+eToOMpGqC/TAl9a To2RxrKViAI91mBYUTPwewZfm6k+fmi8rhwFCK6neih+Yw5Ord3SchNKWa3HhlBG A5ima6eGOr1Ziqne09kWx4WOOeunpcdQfr1YddcCSzd7VvvGrV+5FUD4nrFiqMcr 5v2ck08DL6j1i2z9YM81F626cfXw5aUuCHfQBlxsh2d4yLYwJFg0ovpxB0y0tmUS piqVA8V4zobp2MRkC36QW6iKUL8RezCvlKCoR0sPMJ+EilySUQSjQNyq53PO7Mws KgpbRlb0mdAGFMjonZykAgiAtvUW9Yz8NcQpZap/hmhvTsNz/qq52VH4na9LD0ZO l8ZhhPXyTduaYGwa66GCQMSxgwTec4u2VabsBwOfuQIzZos5sfqrpn3bOOIjJc1q PQFNyJLuyTC3CN2Kru+eqf6PXS8ftexpM5fEThzJahQGUNjNCGkIrQiEYzaR/Jmg LCZTZNiEkghbtXxS/+rfbvl3o4OWynBIOlIGBd7BUbZDnXpKgFhCq/vJE5z31uGX ButPoYn9+te2r/7jI1HF5ag8OgZkT7ZCZlda4vpC/vpK6aquwsW21Seln0Z+liCM YuKB4ANearWOKnsrZuorLw2ySz7riNSX84YwF0RZBa2nBL2EkaAk5aBchEQgc2k9 CzSqRL6fWWGAwUuDWBTLM4a58+CRaoQslvJH0qHkSpT+p/hDSxmIoEp5vp5gnSeY OpvspEZnna09RgflpZ+XHgmnk8LmBAs92crFDb8Z5hvSbcl8KDHFmYdAikMRhZiV Rf/RE/gs1K06kzFtEMkvq6BKPVx8Uc/QMvW5y7PWDHbbyL5puh0JLDRAahopkg8L s2vwCog2gCZLLRSfkshOMc9hhmjWyY9NTp5ZmEOrerqcslhahtmWSErA6szy/Oz6 LE/35uB3Dl7qfM86BZM6VMgCWWA7LmYdLGTdZLhwDugcHpsRFs2IkWdfDCxaRbYL QLq9ToKjndbib4jmNKGY0eK0db7bT+VWLADOYqwmQrmQTBUVAYQyDDomQyLk6TFo S8IF1QzbrGONQu5M09g3H6FCC9eBYrPFsGo9pfqICl6Lk+WGSrfOY3sTWz4boyWE fZOzYLeWxJI7uygEKtwWCRsHL6TyQFkv8EYajinGPCW4Chq1AryzHKqdcKssWZmd eoIMSRLJA9Qwwa8Uz9HJEd+9sr8nH7W4EWaWtGPmYH4dNoH5cwcFCLA+lCzBgpkm lvYLE6lmPnP8wJwJc8/MWfMZvkorbVksiFnQAPB6CTXxXuXl6kmo88ByuQ/VI6kD 4eLn10WO+mlR7Wju6UPbkfOP70bvVOFo0XOSftHL+ViBs4sHulZ1l2UTz5JgMkSx ERobCskAA1ExQvJFa6nx3FPTShtO2nTilpvWLWMmymDsqq5MGkhZtL5U2+aXG5a/ pL3zM4o/e4XOgCw1e1q1oFaKD/nOpUMDg7bjU8IpItRghIlCHtEe5VrypKrki7kg M47sKMhFzXySWkmjXsXadKV+0BCMQ1j68RIl7/QfHZQu4LbMUHZoq2OPXpVJ3YcA xywwk5sHS5jDyIXImTJAiUo9IUSFIsydCQeZqnrAThXukEzFKDZD/jRbzRjOmmhP i4qVclDP4LWlQvsZqAuIo+7r8m86lLa7SRU1ihAWx6Q5G7XFlr5yPxafw+xRfUBo U2VdIwOahscR4yw7MTBNxrCGBis28Y4NYQbVNLpfyHf4chCZoqNyTuRR9MLGvy5W LlWXr48Ron+C8pENj8nkFb1d20ZXFTFjxmPULFEvh4ukKQLkKszC9JmXWLnQchuW XbfcGubNMReMJ1KUMZP4RwFl8dESbeAqWTj3lZefJ3+yxX8pVqJdHX8UP7HhwWRM +SRmmg1b/MaRgbW+4yd4RqyJWUiQEJmktJTUhPekGV7IJtcLvXzk5GJWMGlVHVWe SNep53k70U0tZ9Q4/ot05p89GMt79U763bm+FpwkqTHp4ppYRSQ8Kdc6xr++fPSS nOVWp7tY6KPAhpVidhZ4eALPaJhGyFEDhn73MMvL66015ZKfc5EdmDYlkLRuSdNR ZUAa2HOKin82Dstw4HJDuSR/bFeN2nJed51EJ8JFnsm/Kdl/d/nI5t6u5MwJFsyX u91QNgLRSnyOVUiBFTXMpOXIZhaWPGOkp1Ci//yAwi6hSBPObXumnFw/ZvB6CrsM sPwlTSwvlNr1Uy+XaE8NqX5XJJLkNPsYu6HX/7dXrN80UObzE8HMeI6EehDLKrNb rCh5IQmseiubWT3YtWmojCcB6GkfSWGoTUWSB1oCtSsdFzbuLOcjUOpfWaBjtL7w 9RoHfcJOXW9TSSXxEv/RgcWCVzk7MF//7djxpw4eOdEKWbkbC19jPKsnzLDeTMOw 37I+dd3l/37DqgGEpixSD4BjIDAHpDJe+ucbWFOAZj0b/4oE219SGPpLOPjGc5YX ypRmumjlIFa5LF6LoAQR21CickEMpSwIpV5Mkgl6ikKyXSendxwef/nE+FwzZKYR p8n8zHRPb88H3rbt79YMjAjupUANiwLwaBCkJZSTep7MCLDYNibR7pnxIpTnV9ZY HhzJEHW1fVk2FMITegYceYiC8vWwSrVyXQ2j1Zlk7PQcCSq4CdZyQchYBqpNc1E6 Wa0ePdM4PT0DwMtdfn9/ZbS3ayUXPsrFMpRQAHjqlp6bMsBMPBolMQfAW5hfN2TO iBSUF+7/kCwvlCj7YEYIK2KgxxcNmupf+hkpJbE7UFLNjJ45ASZqtRvhTI/j0+qc DDDT2gM1HPQviWhdN57EwNMWKF/4MG6chrW1kocJb+GrqaHE0ed4Rm9ZePLnG/k/ 88KS0nR8+WoAAAAASUVORK5CYII=</fo:instream-foreign-object>
						
							  </fo:inline>
					</xsl:if>
				</xsl:if>
			</xsl:if>
		</xsl:if>	<!-- 378 -->
		</xsl:if>	<!-- 458 -->
		
		<!-- BAYSBUY Logo -->
		<xsl:if test="$ROOT_OBJ/ORG_ID = '218'">
			      <fo:inline>

					<fo:instream-foreign-object content-type="image/gif"
								    width="108pt"
								    height="24pt"
								    xsl:alt="An Image"
								    xsl:image-uid="">R0lGODlhtAAoANUAALCvrjLH6HJxbvzs0P7+/gS34eaimldXVAkIBfLy8qTTZhhCmt7c2+bo4sXD
			wp0mEZmZl1mFJP307To5N+WIDYeHhSxVpPr6+f25LklIRe0wIuH4/PTt7I7HQf768dbm9JezdYUZ
			A+6wXVFRTv7ZjP/++/f69L7K5PDz+O324PqyJueQHP/6+2VlY4G5Nl9eW6OioZGQjyUkItHQ0O76
			/fb29u4+MfnNfxgXFfv7+/39/cXW6n9+e8LF3wAAAP///yH5BAAAAAAALAAAAAC0ACgAAAb/wJ9w
			SCwaj8ikcslsOp/QqHRKrVqv2Kx2y+16v+CweEwum3+0wObMbruVhF98EwjQ3vh8ex4oFO5xeoKD
			XHx+doSJilSBdH5+d0KBQwSTknJQcZaUYpucT55JgaFjk46PiJ+aR6tOlpVEr0Who5y1WrJIrZiX
			rF2VH32PkJiwvbGZk8a6pHC8rqBgzVWBOxYLC9fZNZdxDB7LRDo5OuXm5p6b5OU5rxcX5kfm70Y5
			Qjo18JX7gef+4/ZskWsn7wKRCzXiHVnHLYumHtgiWkBRxMADA7uGADjwoqNHjy0EVJhRhAMMHgI+
			vghZwYEQBxxfHABQpAJHjiSFXIAh4AAP/wAMGDgAQJSogwQ/GKhc+kKAS40cR9AsciFGBh4NGbS4
			yQPpkBktOo6I4eWEtmwUhxgI8UCDAQm8YPHwQbeu3bo4YHydgODuXRwVdHCYUFdGA4196R7wmuBF
			Yh84ZLyIQRRGDB4VDjvwyxkHj4AC6gowUmMEXbI/arSwC2FIjQN4n3aBiG3iEAcP2GpwWwLZXASP
			7wL3IYNkaR/DOfdFQHMGDuQ+WsRJQLjvBAZDINANThfHVRjgOfxwsJx73eWtf/z2wYM0bMhPOWSA
			LgP7D+3ow5jF1tBiCN27vSVHIL8h4F0GCCLI13BkOYecgQkqCFxfLejwAwwTMqcedBoO8f/CgzKM
			EGEGE2QwAg8OGEQecAeOiMNwGRi0XntFvAadXnE42NcBP+gY3TTUxEEbRQ78919bu7llSQXQHcBB
			AlBCyYBpfbXnwHOQwRAllA18uCNSOaxmHQwyQEfjELD1hZoRBDTAgIrDTWBQETFANwE3M7p3IxEw
			PIgABGn6MMFhY5ilw1pGApikgEKsV6ERTFY5HpbFQdqkVw1Ul6EPGXiF5oMTHCDqqByNdNuEMsQA
			waqrxkClDxU0aqaefek1hA5iboqDbFSYkMKvwKbQwLDDMpDCB7ix9cCyD9jgrA0BEtgkBzVEWcMM
			1bE3KV0yOKDDO+8QsN5iGj03IXEktVL/J3ScdTfVisn5BZwADeVZY6C2DpEph2oeI0UKCnQgsMAu
			RGDwwSCUMACUHDzJgQQQR9ywhT9EyqIMOGSM8bmtXQkqiROEnIEMw5E7hMXA2WpJAyOYd551mmUo
			c2LAtVDvrPfuWYTHw73gqRUAuzBwwQdHAIIJA4jggShGFPjY09xix7PLD/pgshDyQfdCQJskwFNI
			AoQd9gHD7drjcjLAAIDabM8n6YZ9xVqEDl76kC8R61WqRdAEF330DwNQIAJcnwzID9zxCjfBU1Mr
			VzJSxogJKyX4JFDD5UkwUCZ848U5JxF1WmdQaH290MDlqDOQ7d2wYGideANSIwTAfRsM/0JDA6wg
			OOFARiooBOABD7zahG5rIA/BgwdDy18WUbfcQihl4gjUV2/9AQtCRhJ5yOFgffUv9pWBPShP8P2C
			wE1VBARxFr93wESD4GnuFOzehKNNeIyupc0T8TwRM6CavPoyAqTAi2rLsZXqqjZAHxTwCBiiy6Bw
			MYSgGc0El6Cf7gbHhLkopiFK4J72mlaXBxLhPdr6CpbapbicbIaFeAnMXgSIgBHYxwjakaD7rLCM
			FHRAfsUAnO5WsEHCJQEAIzhABmJgkGkohXotuCEmlqfEGIBQCK5S4t20kkRSeXFUAoAA7JJygC5+
			cQQtQJEROACBnpBqBE2BQUCe8ZLqCZtgjFeARSAsJ4lA5I6IRBTc0pIArgsQZAk5KGQ45KBIqoCL
			YpIopCQlOcdbTVKSvhhCIhtZOCJs0pBA6sIAMKACFZBSBTcY5CJWyUoljBIDsMQACXrTylracgiv
			hOUsb8nLVpZAAqSUZW9C2ctinmGUKtglHY3JzDeMUpnNjGYeBgBNaVrzmtjMpja3yc1uevOb4Ayn
			OMdJzlYGAQA7</fo:instream-foreign-object>
				
			</fo:inline>
		</xsl:if>
        </fo:block>
		
          </fo:table-cell>
          <fo:table-cell>
          </fo:table-cell>
          <fo:table-cell vertical-align="middle">
            <fo:block xsl:use-attribute-sets="table_cell.title" >
              <xsl:choose>
                <!-- <xsl:when test="$VENDOR_COUNTRY_CHK = 'TH' or $VENDOR_COUNTRY_CHK = 'Thailand'"> -->
		<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
                  <xsl:value-of select="$PO_TITLE_th"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="$PO_TITLE_en"/>
                </xsl:otherwise>
              </xsl:choose>
            </fo:block>
          </fo:table-cell>
        </fo:table-row>
			</fo:table-body>
	</fo:table>
	</fo:block>
	<fo:block font-size="8pt"> 
    <xsl:value-of select="$header_text"/> </fo:block>
    </fo:static-content>
	
	<fo:static-content flow-name="remaining-page">
	 <fo:block xsl:use-attribute-sets="form_data">
	 <fo:table>
		<fo:table-column column-width="60mm"/> 
    <fo:table-column column-width="60mm"/> 
    <fo:table-column column-width="60mm"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell xsl:use-attribute-sets="table.cell6">
            <fo:block xsl:use-attribute-sets="form_data3">
              <!--TM CUSTOMIZE : Don't show OU Name -->
              <!--xsl:value-of select="$ROOT_OBJ/OU_NAME"/ -->              
            </fo:block> 
          </fo:table-cell>
					<fo:table-cell xsl:use-attribute-sets="table.cell4"> 
            <fo:block xsl:use-attribute-sets="form_data">  
              <xsl:if test="$print_draft">
                <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_DRAFT'][1]/TEXT"/>
              </xsl:if>
            </fo:block> 
          </fo:table-cell>
					<fo:table-cell xsl:use-attribute-sets="table.cell5">	
						<fo:block xsl:use-attribute-sets="form_data" > 
              <!-- TM CUSTOMIZE : Rem -->
              <!--
							<xsl:value-of select="$ROOT_OBJ/DOCUMENT_TYPE"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> 
			  -->
							<xsl:value-of select="$ROOT_OBJ/SEGMENT1"/>
							<fo:leader leader-pattern="space" leader-length="2.0pt"/>
							<xsl:value-of select="$ROOT_OBJ/VENDOR_NAME"/>
			  <!--
							, <fo:leader leader-pattern="space" leader-length="1.0pt"/>  <xsl:value-of select="$ROOT_OBJ/REVISION_NUM" />
              -->
            </fo:block> 
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
	</fo:table>
	</fo:block>
	<fo:block font-size="8pt"> <xsl:value-of select="$header_text"/> </fo:block>
</fo:static-content>
                      
<fo:flow flow-name="xsl-region-body" >
	<xsl:apply-templates select="PO_DATA"/>
</fo:flow>
</fo:page-sequence>
</fo:root>
</xsl:template>

<xsl:template match="PO_DATA">


<!-- If the Test flag is Y display the boilerplate -->
<xsl:if test="TEST_FLAG ='Y'">
	<fo:block xsl:use-attribute-sets="test_style" > <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_TEST'][1]/TEXT"/> </fo:block>
	<fo:block>  <fo:leader leader-pattern="space" leader-length="1.0pt"/>  </fo:block>
</xsl:if>

<fo:block id="page_ref"/>

<!-- table for displaying the Legal Entity and PA details -->
<fo:table table-layout="fixed" text-align="center">
<fo:table-column column-width="110mm"/> 
<fo:table-column column-width="80mm"/>
<fo:table-body>
<fo:table-row>
	<fo:table-cell>
		<fo:block xsl:use-attribute-sets="legal_details_style">

      <!--TM CUSTOMIZE : Display DTAC Name & Address & Standard -->

	<xsl:choose>
          <xsl:when test="ORG_ID = '102'">
        <xsl:choose>
          <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
	  <xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
            <xsl:value-of select="$DTAC_NAME_th"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR1_th"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR2_th"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR3_th"/>
            <fo:block/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$DTAC_NAME_en"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR1_en"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR2_en"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR3_en"/>
            <fo:block/>
          </xsl:otherwise>
        </xsl:choose>
		</xsl:when>
	</xsl:choose>

	<xsl:choose>
            <xsl:when test="ORG_ID = '218'">
            <xsl:choose>
            <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
                <xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
                    <xsl:value-of select="$PAYSBUY_NAME_th"/>
                    <fo:block/>
                    <xsl:value-of select="$PAYSBUY_ADDR1_th"/>
                    <fo:block/>
                    <xsl:value-of select="$PAYSBUY_ADDR2_th"/>
                    <fo:block/>
                    <xsl:value-of select="$PAYSBUY_ADDR3_th"/>
                    <fo:block/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$PAYSBUY_NAME_en"/>
                    <fo:block/>
                    <xsl:value-of select="$PAYSBUY_ADDR1_en"/>
                    <fo:block/>
                    <xsl:value-of select="$PAYSBUY_ADDR2_en"/>
                    <fo:block/>
                    <xsl:value-of select="$PAYSBUY_ADDR3_en"/>
                    <fo:block/>
                </xsl:otherwise>
            </xsl:choose>
            </xsl:when>
        </xsl:choose>


		<xsl:choose>
            <xsl:when test="ORG_ID = '378'">
            <xsl:choose>
                <xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
                    <xsl:value-of select="$Accerelate_NAME_th"/>
                    <fo:block/>
                    <xsl:value-of select="$Accerelate_ADDR1_th"/>
                    <fo:block/>
                    <xsl:value-of select="$Accerelate_ADDR2_th"/>
                    <fo:block/>
                    <xsl:value-of select="$Accerelate_ADDR3_th"/>
                    <fo:block/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$Accerelate_NAME_en"/>
                    <fo:block/>
                    <xsl:value-of select="$Accerelate_ADDR1_en"/>
                    <fo:block/>
                    <xsl:value-of select="$Accerelate_ADDR2_en"/>
                    <fo:block/>
                    <xsl:value-of select="$Accerelate_ADDR3_en"/>
                    <fo:block/>
                </xsl:otherwise>
            </xsl:choose>
            </xsl:when>
        </xsl:choose>
		

<xsl:choose>
            <xsl:when test="ORG_ID = '144'">
            <xsl:choose>
            <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
                <xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
                    <xsl:value-of select="$DTACW_NAME_th"/>
                    <fo:block/>
                    <xsl:value-of select="$DTACW_ADDR1_th"/>
                    <fo:block/>
                    <xsl:value-of select="$DTACW_ADDR2_th"/>
                    <fo:block/>
                    <xsl:value-of select="$DTACW_ADDR3_th"/>
                    <fo:block/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$DTACW_NAME_en"/>
                    <fo:block/>
                    <xsl:value-of select="$DTACW_ADDR1_en"/>
                    <fo:block/>
                    <xsl:value-of select="$DTACW_ADDR2_en"/>
                    <fo:block/>
                    <xsl:value-of select="$DTACW_ADDR3_en"/>
                    <fo:block/>
                </xsl:otherwise>
            </xsl:choose>
            </xsl:when>
        </xsl:choose>

	<xsl:choose>
          <xsl:when test="ORG_ID = '142'">
        <xsl:choose>
          <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
	  <xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
            <xsl:value-of select="$DTACN_NAME_th"/>
            <fo:block/>
            <xsl:value-of select="$DTACN_ADDR1_th"/>
            <fo:block/>
            <xsl:value-of select="$DTACN_ADDR2_th"/>
            <fo:block/>
            <xsl:value-of select="$DTACN_ADDR3_th"/>
            <fo:block/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$DTACN_NAME_en"/>
            <fo:block/>
            <xsl:value-of select="$DTACN_ADDR1_en"/>
            <fo:block/>
            <xsl:value-of select="$DTACN_ADDR2_en"/>
            <fo:block/>
            <xsl:value-of select="$DTACN_ADDR3_en"/>
            <fo:block/>
          </xsl:otherwise>
        </xsl:choose>
		</xsl:when>
	</xsl:choose>


<xsl:choose>
            <xsl:when test="ORG_ID = '140'">
            <xsl:choose>
            <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
                <xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
                    <xsl:value-of select="$DTACI_NAME_th"/>
                    <fo:block/>
                    <xsl:value-of select="$DTACI_ADDR1_th"/>
                    <fo:block/>
                    <xsl:value-of select="$DTACI_ADDR2_th"/>
                    <fo:block/>
                    <xsl:value-of select="$DTACI_ADDR3_th"/>
                    <fo:block/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$DTACI_NAME_en"/>
                    <fo:block/>
                    <xsl:value-of select="$DTACI_ADDR1_en"/>
                    <fo:block/>
                    <xsl:value-of select="$DTACI_ADDR2_en"/>
                    <fo:block/>
                    <xsl:value-of select="$DTACI_ADDR3_en"/>
                    <fo:block/>
                </xsl:otherwise>
            </xsl:choose>
            </xsl:when>
        </xsl:choose>

	<xsl:choose>
          <xsl:when test="ORG_ID = '104'">
        <xsl:choose>
          <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
	  <xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
            <xsl:value-of select="$DTAC_NAME_N_th"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR1_N_th"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR2_N_th"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR3_N_th"/>
            <fo:block/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$DTAC_NAME_N_en"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR1_N_en"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR2_N_en"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR3_N_en"/>
            <fo:block/>
          </xsl:otherwise>
        </xsl:choose>
		</xsl:when>
	</xsl:choose>


	<xsl:choose>
          <xsl:when test="ORG_ID = '105'">
        <xsl:choose>
          <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
	  <xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
            <xsl:value-of select="$DTAC_NAME_E_th"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR1_E_th"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR2_E_th"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR3_E_th"/>
            <fo:block/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$DTAC_NAME_E_en"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR1_E_en"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR2_E_en"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR3_E_en"/>
            <fo:block/>
          </xsl:otherwise>
        </xsl:choose>
		</xsl:when>
	</xsl:choose>

	<xsl:choose>
        <xsl:when test="ORG_ID = '106'">
        <xsl:choose>
		<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
            <xsl:value-of select="$DTAC_NAME_S_th"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR1_S_th"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR2_S_th"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR3_S_th"/>
            <fo:block/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$DTAC_NAME_S_en"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR1_S_en"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR2_S_en"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR3_S_en"/>
            <fo:block/>
          </xsl:otherwise>
        </xsl:choose>
		</xsl:when>
	</xsl:choose>

	<xsl:choose>
		<xsl:when test="ORG_ID = '103'">
        <xsl:choose>
		<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
            <xsl:value-of select="$DTAC_NAME_NE_th"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR1_NE_th"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR2_NE_th"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR3_NE_th"/>
            <fo:block/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$DTAC_NAME_NE_en"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR1_NE_en"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR2_NE_en"/>
            <fo:block/>
            <xsl:value-of select="$DTAC_ADDR3_NE_en"/>
            <fo:block/>
          </xsl:otherwise>
        </xsl:choose>
		</xsl:when>
	</xsl:choose>

  <!-- Start added by AP@BAS on 14-Jun-2017 for DTN-T -->
	<xsl:choose>
		<xsl:when test="ORG_ID = '458'">
        <xsl:choose>
		<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
            <xsl:value-of select="$DTNT_NAME_th"/>
            <fo:block/>
            <xsl:value-of select="$DTNT_ADDR1_th"/>
            <fo:block/>
            <xsl:value-of select="$DTNT_ADDR2_th"/>
            <fo:block/>
            <xsl:value-of select="$DTNT_ADDR3_th"/>
            <fo:block/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="$DTNT_NAME_en"/>
            <fo:block/>
            <xsl:value-of select="$DTNT_ADDR1_en"/>
            <fo:block/>
            <xsl:value-of select="$DTNT_ADDR2_en"/>
            <fo:block/>
            <xsl:value-of select="$DTNT_ADDR3_en"/>
            <fo:block/>
        </xsl:otherwise>
        </xsl:choose>
		</xsl:when>
	</xsl:choose>	
  <!-- End added by AP@BAS on 14-Jun-2017 for DTN-T -->

		</fo:block>
	</fo:table-cell>
	<fo:table-cell>
    
		<fo:table table-layout="fixed" >
		<fo:table-column column-width="80mm"/>
		<fo:table-body>
		<fo:table-row>
			<fo:table-cell xsl:use-attribute-sets="table.cell"> 
        <fo:block xsl:use-attribute-sets="tac_form_data"> 
          <fo:leader leader-pattern="space" leader-length="1.0pt"/>	
	<xsl:choose>
            <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
	    <xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
              <xsl:text> ใบสั่งซื้อเลขที่: </xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text> Purchase Order No.: </xsl:text>
          </xsl:otherwise>
        </xsl:choose>

          <fo:leader leader-pattern="space" leader-length="2.0pt"/>
          <fo:inline font-weight="normal">
            <xsl:value-of select="SEGMENT1"/> 
          </fo:inline>
        </fo:block> 
      </fo:table-cell> 
		</fo:table-row>    
		<fo:table-row>
  		<fo:table-cell xsl:use-attribute-sets="table.cell">  
        <fo:block xsl:use-attribute-sets="tac_form_data">
          <xsl:choose>
            <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
	    <xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
              <xsl:text> วันที่สั่งซื้อ: </xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text> Order Date: </xsl:text>
          </xsl:otherwise>
        </xsl:choose>
          <fo:leader leader-pattern="space" leader-length="2.0pt"/>
          <fo:inline font-weight="normal">          
            <xsl:value-of select="substring(CREATION_DATE,1,11)"/>
          </fo:inline>
        </fo:block> 
      </fo:table-cell> 
		</fo:table-row>    
		<fo:table-row>
			<fo:table-cell xsl:use-attribute-sets="table.cell">  
        <fo:block xsl:use-attribute-sets="tac_form_data"> 
          <xsl:choose>
            <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
	    <xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
              <fo:inline font-size="9.0pt">
                <xsl:text> เอกสารแก้ไข ครั้งที่: </xsl:text>
              </fo:inline>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text> Revision No.: </xsl:text>
          </xsl:otherwise>
        </xsl:choose>         
          <fo:leader leader-pattern="space" leader-length="2.0pt"/>
          <fo:inline font-weight="normal">          
            <xsl:value-of select="REVISION_NUM"/> 
          </fo:inline>
        </fo:block> 
      </fo:table-cell> 
		</fo:table-row>    
		<fo:table-row>
			<fo:table-cell xsl:use-attribute-sets="table.cell">  
        <fo:block xsl:use-attribute-sets="tac_form_data"> 
          <xsl:choose>
            <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
	    <xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
              <xsl:text> วันที่ทำการแก้ไข: </xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text> Revised Date: </xsl:text>
            </xsl:otherwise>
          </xsl:choose>             
          <fo:leader leader-pattern="space" leader-length="2.0pt"/>
          <fo:inline font-weight="normal">          
            <xsl:value-of select="substring(REVISED_DATE,1,11)"/> 
          </fo:inline>
        </fo:block> 
      </fo:table-cell> 
		</fo:table-row> 
    <fo:table-row>
			<fo:table-cell xsl:use-attribute-sets="table.cell">  
        <fo:block xsl:use-attribute-sets="tac_form_data"> 
          <xsl:choose>
            <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
	    <xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
              <xsl:text> สัญญาเลขที่: </xsl:text>
            </xsl:when>
            <xsl:otherwise>
             <xsl:text> Agreement No.: </xsl:text>
            </xsl:otherwise>
          </xsl:choose>    
          <fo:leader leader-pattern="space" leader-length="2.0pt"/>
          <fo:inline font-weight="normal">
            <xsl:value-of select="ATTRIBUTE14"/> 
          </fo:inline>            
        </fo:block> 
      </fo:table-cell> 
		</fo:table-row>  
    <fo:table-row>
			<fo:table-cell xsl:use-attribute-sets="table.cell">  
        <fo:block xsl:use-attribute-sets="tac_form_data"> 
          <xsl:choose>
            <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
	    <xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
              <xsl:text> ใบเสนอราคาเลขที่: </xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text> Quotation No.: </xsl:text>
            </xsl:otherwise>
          </xsl:choose>    
          <fo:leader leader-pattern="space" leader-length="2.0pt"/>
          <fo:inline font-weight="normal">
            <xsl:value-of select="ATTRIBUTE9"/> 
          </fo:inline>
        </fo:block> 
      </fo:table-cell> 
		</fo:table-row>  
    <fo:table-row>
			<fo:table-cell xsl:use-attribute-sets="table.cell">  
        <fo:block xsl:use-attribute-sets="tac_form_data"> 
          <xsl:choose>
            <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
	    <xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
              <xsl:text> วันที่เสนอราคา: </xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text> Quotation Date: </xsl:text>
            </xsl:otherwise>
          </xsl:choose>    
          <fo:leader leader-pattern="space" leader-length="2.0pt"/>
          <fo:inline font-weight="normal">
            <xsl:value-of select="ATTRIBUTE10"/> 
          </fo:inline>
        </fo:block> 
      </fo:table-cell> 
		</fo:table-row>  
    <fo:table-row>
			<fo:table-cell xsl:use-attribute-sets="table.cell">  
        <fo:block xsl:use-attribute-sets="tac_form_data">
          <xsl:choose>
            <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
	    <xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
			<fo:inline font-weight="normal">
              <xsl:text> ผู้สั่งซื้อ: </xsl:text><xsl:value-of select="BUYER_TH"/>
			 </fo:inline>
            </xsl:when>
            <xsl:otherwise>
			<!--fo:inline font-weight="normal" -->
              <xsl:text> Buyer: </xsl:text><xsl:value-of select="BUYER_ENG"/>
			 <!--/fo:inline -->
            </xsl:otherwise>
          </xsl:choose>         
          <fo:leader leader-pattern="space" leader-length="2.0pt"/>
          <fo:inline font-weight="normal">
            <!--xsl:value-of select="DOCUMENT_BUYER_TITLE"/ -->
			<!--xsl:value-of select="DOCUMENT_BUYER_LAST_NAME"/> 
            <fo:leader leader-pattern="space" leader-length="1.0pt"/>
            <xsl:value-of select="DOCUMENT_BUYER_FIRST_NAME"/ -->
            <!--fo:leader leader-pattern="space" leader-length="1.0pt"/>
            <xsl:value-of select="DOCUMENT_BUYER_LAST_NAME"/ -->            
          </fo:inline>
        </fo:block> 
      </fo:table-cell> 
		</fo:table-row>       
		</fo:table-body>
		</fo:table>
	</fo:table-cell>
</fo:table-row>
</fo:table-body>
</fo:table>
<fo:block/>
<fo:block>
  <fo:leader leader-pattern="space" leader-length="1pt"/>
</fo:block>
<!-- Table for displaying Supplier, Ship To and Bill To address -->
<!-- TM CUSTOMIZE : DTAC Layout -->

<fo:table table-layout="fixed">
  <fo:table-column column-width="95mm"/>
  <fo:table-column column-width="95mm"/>
  <fo:table-body>
    <fo:table-row>
      <fo:table-cell xsl:use-attribute-sets="table.cell" number-columns-spanned="2">
        <fo:table table-layout="fixed">
          <fo:table-column column-width="30mm"/>
          <fo:table-column column-width="65mm"/>
            <fo:table-body>
              <fo:table-row>
                <fo:table-cell>
                  <fo:block xsl:use-attribute-sets="form_data">
                    <xsl:if test="$DisplayBoilerPlate">
                      <xsl:choose>
                        <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
			<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
                          <xsl:value-of select="$PO_FO_VENDOR_TH" />	
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_VENDOR'][1]/TEXT"/>
                        </xsl:otherwise>
                      </xsl:choose>
                     </xsl:if>
                  </fo:block>

				<fo:block xsl:use-attribute-sets="form_data" font-weight="normal">
							<fo:inline space-before="2pt">
								<xsl:text>(</xsl:text><xsl:value-of select="VENDOR_NUM"/><xsl:text>)</xsl:text>
							</fo:inline>
				</fo:block>

                </fo:table-cell>
                <fo:table-cell>
                  <fo:block xsl:use-attribute-sets="form_data" font-weight="normal">
                    <!--xsl:value-of select="VENDOR_NUM"/><fo:leader leader-pattern="space" leader-length="1.0pt"/ --><xsl:value-of select="VENDOR_NAME"/>
                  <fo:block/>
                  <xsl:value-of select="VENDOR_ADDRESS_LINE1"/> <xsl:value-of select="VENDOR_ADDRESS_LINE2"/> <xsl:value-of select="VENDOR_ADDRESS_LINE3"/><fo:leader leader-pattern="space" leader-length="2.0pt"/>
                  <!--fo:block/>
                  <xsl:value-of select="VENDOR_ADDRESS_LINE2"/ -->
                  <!--fo:block/>
                  <xsl:value-of select="VENDOR_ADDRESS_LINE3"/ -->
                  <!--fo:block/ -->
                  <!--xsl:value-of select="VENDOR_PHONE"/ -->
                  <!--fo:block/ -->
                  <xsl:if test="VENDOR_CITY !=''">
                    <xsl:value-of select="VENDOR_CITY"/>,<fo:leader leader-pattern="space" leader-length="2.0pt"/>
                  </xsl:if>
                  <xsl:value-of select="VENDOR_STATE"/><fo:leader leader-pattern="space" leader-length="3.0pt"/><xsl:value-of select="VENDOR_POSTAL_CODE"/>
	
                  <xsl:choose>
                    <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
		    <xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
					<fo:block/>
					<xsl:text>โทร. </xsl:text><xsl:value-of select="VENDOR_PHONE"/><fo:leader leader-pattern="space" leader-length="2.0pt"/><xsl:text>แฟ็กซ์. </xsl:text><xsl:value-of select="VENDOR_FAX"/><fo:leader leader-pattern="space" leader-length="2.0pt"/><xsl:text>อีเมล์. </xsl:text><xsl:value-of select="VENDOR_EMAIL_ADDRESS"/>
					</xsl:when>
                    <xsl:otherwise>
                      <fo:block/>
                      <xsl:text>Tel. </xsl:text><xsl:value-of select="VENDOR_PHONE"/><fo:leader leader-pattern="space" leader-length="2.0pt"/><xsl:text>FAX. </xsl:text><xsl:value-of select="VENDOR_FAX"/><fo:leader leader-pattern="space" leader-length="2.0pt"/><xsl:text>E-MAIL. </xsl:text><xsl:value-of select="VENDOR_EMAIL_ADDRESS"/>
                    </xsl:otherwise>
                  </xsl:choose>            

                  <xsl:choose>
                    <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> </xsl:when> -->
		    <xsl:when test="$CURRENCY_CODE_CHK = 'THB'"> </xsl:when>
                    <xsl:otherwise>
                      <fo:block/>
                      <xsl:value-of select="VENDOR_COUNTRY"/>
                    </xsl:otherwise>
                  </xsl:choose>                  
                </fo:block>
              </fo:table-cell>                                
            </fo:table-row>
          </fo:table-body> 
        </fo:table>
      </fo:table-cell>
      </fo:table-row>

        <fo:table-row>
          <fo:table-cell xsl:use-attribute-sets="table.cell">
            <fo:table table-layout="fixed">
              <fo:table-column column-width="30mm"/>
              <fo:table-column column-width="65mm"/>
                <fo:table-body>
                  <fo:table-row>
                    <fo:table-cell>

                    <xsl:choose>
                          <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
			  <xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
                            <fo:block xsl:use-attribute-sets="form_data" font-weight="normal">
                        <xsl:if test="$DisplayBoilerPlate">
                          <xsl:text>Inco Term: </xsl:text>                            
                         </xsl:if>
                      </fo:block>
	
                          </xsl:when>
                          <xsl:otherwise>
                            <fo:block xsl:use-attribute-sets="form_data">
                        <xsl:if test="$DisplayBoilerPlate">
                          <xsl:text>Inco Term: </xsl:text>                            
                         </xsl:if>
                      </fo:block>
                          </xsl:otherwise>
                        </xsl:choose>
  
                    </fo:table-cell>
                    <fo:table-cell>
                      <fo:block xsl:use-attribute-sets="form_data" font-weight="normal">
                        <xsl:value-of select="ATTRIBUTE12"/>
                    </fo:block>
                  </fo:table-cell>                                
                </fo:table-row>
                </fo:table-body> 
              </fo:table>
            </fo:table-cell>

             
            <fo:table-cell xsl:use-attribute-sets="table.cell">
              <fo:table table-layout="fixed">
                <fo:table-column column-width="30mm"/>
                <fo:table-column column-width="65mm"/>
                  <fo:table-body>
                    <fo:table-row>
                      <fo:table-cell>
                        

                       <xsl:choose>
                          <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
			  <xsl:when test="$CURRENCY_CODE_CHK = 'THB'">

                            <fo:block xsl:use-attribute-sets="form_data" font-weight="normal">
                        <xsl:if test="$DisplayBoilerPlate">
                          <xsl:text>สกุลเงิน: </xsl:text>                            
                         </xsl:if>
                      </fo:block>
	
                          </xsl:when>
                          <xsl:otherwise>
                            <fo:block xsl:use-attribute-sets="form_data">
                        <xsl:if test="$DisplayBoilerPlate">
                          <xsl:text>Currency: </xsl:text>                            
                         </xsl:if>
                      </fo:block>
                          </xsl:otherwise>
                        </xsl:choose> 

                      </fo:table-cell>
                      <fo:table-cell>
                        <fo:block xsl:use-attribute-sets="form_data" font-weight="normal">
                          <xsl:value-of select="$ROOT_OBJ/CURRENCY_CODE"/>
                      </fo:block>
                    </fo:table-cell>                                
                  </fo:table-row>
                </fo:table-body> 
              </fo:table>
            </fo:table-cell>
        </fo:table-row>

      <!-- </xsl:if> -->

      <fo:table-row>      
      <fo:table-cell xsl:use-attribute-sets="table.cell">
        <fo:table table-layout="fixed">
          <fo:table-column column-width="30mm"/>
            <fo:table-column column-width="65mm"/>
              <fo:table-body>
                <fo:table-row>
                  <fo:table-cell>
                    <fo:block xsl:use-attribute-sets="form_data">
                      <xsl:if test="$DisplayBoilerPlate">
                        <xsl:choose>
                          <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
			  <xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
                            <xsl:text>การรับประกัน: </xsl:text>	
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:text>Warranty Term: </xsl:text>
                          </xsl:otherwise>
                        </xsl:choose>
                       </xsl:if>
                    </fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                    <fo:block xsl:use-attribute-sets="form_data" font-weight="normal">
                      <xsl:value-of select="ATTRIBUTE13"/>
                  </fo:block>
                </fo:table-cell>                                
              </fo:table-row>
            </fo:table-body> 
          </fo:table>
        </fo:table-cell>
        <fo:table-cell xsl:use-attribute-sets="table.cell">
          <fo:table table-layout="fixed">
            <fo:table-column column-width="30mm"/>
            <fo:table-column column-width="65mm"/>
              <fo:table-body>
                <fo:table-row>
                  <fo:table-cell>
                    <fo:block xsl:use-attribute-sets="form_data">
                      <xsl:if test="$DisplayBoilerPlate">
                        <xsl:choose>
                          <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
			  <xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
                            <xsl:text>การชำระเงิน: </xsl:text>	
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:text>Credit Term: </xsl:text>
                          </xsl:otherwise>
                        </xsl:choose>
                       </xsl:if>
                    </fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                    <fo:block xsl:use-attribute-sets="form_data" font-weight="normal">
                      <xsl:value-of select="$ROOT_OBJ/PAYMENT_TERMS"/>
			<xsl:choose>
                          <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
			  <xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
                            <xsl:text> วัน </xsl:text>	
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:text> Days </xsl:text>
                          </xsl:otherwise>
                        </xsl:choose>
                  </fo:block>
                </fo:table-cell>                                
              </fo:table-row>
            </fo:table-body> 
          </fo:table>
        </fo:table-cell>
      </fo:table-row>
    </fo:table-body>
  </fo:table>
  
<fo:block><fo:leader leader-pattern="space" leader-length="1pt"/>  </fo:block>

<!-- Table for displaying the line, shipment and distribution data -->

<xsl:if test="count(LINES/LINES_ROW/LINE_NUM) > 0">

<fo:table xsl:use-attribute-sets="lines.table.style">
  <fo:table-column column-width="12.5mm"/> 
  <fo:table-column column-width="80mm"/>  
  <fo:table-column column-width="28mm"/>
  <fo:table-column column-width="12mm"/> 
  <fo:table-column column-width="25.5mm"/> 
  <fo:table-column column-width="35mm"/> 
  <fo:table-header>

<fo:table-row >
<fo:table-cell  xsl:use-attribute-sets="table_cell_heading2" >
  <fo:block xsl:use-attribute-sets="table_head1">
    <xsl:if test="$DisplayBoilerPlate ">
      <xsl:choose>
        <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
	<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
           <xsl:text>ลำดับที่</xsl:text>	
         </xsl:when>
         <xsl:otherwise>
          <xsl:text>No.</xsl:text>
         </xsl:otherwise>
        </xsl:choose>
    </xsl:if>
  </fo:block>
</fo:table-cell>
<fo:table-cell  xsl:use-attribute-sets="table_cell_heading2" >
  <fo:block xsl:use-attribute-sets="table_head1">
    <xsl:if test="$DisplayBoilerPlate ">
      <xsl:choose>
        <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
	<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
          <xsl:text>รายการ</xsl:text>	
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>Description</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </fo:block>
</fo:table-cell>
<fo:table-cell  xsl:use-attribute-sets="table_cell_heading2" >
  <fo:block xsl:use-attribute-sets="table_head1">
    <xsl:if test="$DisplayBoilerPlate ">
      <xsl:choose>
        <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
	<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
          <xsl:text>จำนวน</xsl:text>	
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>Quantity</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </fo:block>
</fo:table-cell>	

<fo:table-cell  xsl:use-attribute-sets="table_cell_heading1" >
  <fo:block xsl:use-attribute-sets="table_head1">
    <xsl:if test="$DisplayBoilerPlate ">
      <xsl:choose>
        <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
	<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
          <xsl:text>หน่วย</xsl:text>	
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>UOM</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </fo:block>
</fo:table-cell>

<fo:table-cell  xsl:use-attribute-sets="table_cell_heading2" >
  <fo:block xsl:use-attribute-sets="table_head1">
    <xsl:if test="$DisplayBoilerPlate ">
      <xsl:choose>
        <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
	<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
          <xsl:text>ราคาต่อหน่วย</xsl:text>	
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>Unit Price</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
    <fo:block/>(<xsl:value-of select="$ROOT_OBJ/CURRENCY_CODE"/>)
  </fo:block>
</fo:table-cell>

<fo:table-cell  xsl:use-attribute-sets="table_cell_heading1" >
  <fo:block xsl:use-attribute-sets="table_head1">
    <xsl:if test="$DisplayBoilerPlate ">
      <xsl:choose>
        <!-- <xsl:when test="VENDOR_COUNTRY = 'TH' or VENDOR_COUNTRY = 'Thailand'"> -->
	<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
          <xsl:text>จำนวนเงิน</xsl:text>	
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>Amount</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
	<fo:block/> (<xsl:value-of select="$ROOT_OBJ/CURRENCY_CODE"/>)
  </fo:block>
</fo:table-cell>

</fo:table-row>

	<fo:table-row> 
	  <fo:table-cell number-columns-spanned="8"> 
		<fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block>	
	  </fo:table-cell> 
	</fo:table-row>

</fo:table-header>
<!--  end of header part -->

<!-- start of displaying line, shipment and distribution details -->
<fo:table-body>
<xsl:for-each select="$LINES_ROOT_OBJ">

	<xsl:variable name="lineID" select="PO_LINE_ID"/>

	<xsl:variable name="shipment_count" select="count(LINE_LOCATIONS/LINE_LOCATIONS_ROW/SHIP_TO_LOCATION_ID)"/>

	<xsl:variable name="distribution_count" select="count(LINE_LOCATIONS/LINE_LOCATIONS_ROW/DISTRIBUTIONS/DISTRIBUTIONS_ROW/CHARGE_ACCOUNT)"/>

	<!-- Variable for to display charge account or not -->
	<xsl:variable name="charge_account" select="$PSA='Y'"/>

	<!-- Variable to find UN number is null or not -->
	<xsl:variable name="un_num" select="UN_NUMBER !=''"/>

	<!-- Variable to find hazard class is null or not -->
	<xsl:variable name="hz_class" select="HAZARD_CLASS !=''"/>

	<!-- Variable to find NOTE_TO_VENDOR is null or not -->
	<xsl:variable name="vendor_note" select="NOTE_TO_VENDOR !=''"/>

	<!-- Variable to find GLOBAL_AGREEMENT_FLAG  is null or not -->
	<xsl:variable name="ga_flag" select="GLOBAL_AGREEMENT_FLAG !=''"/>

	<!-- Variable to find CONTRACT_NUM  is null or not -->
	<xsl:variable name="con_num" select="CONTRACT_NUM !=''"/>
	
	<!-- Variable to find CONTRACT_NUM  is null or not -->
	<xsl:variable name="sup_quote_num" select="QUOTE_VENDOR_QUOTE_NUMBER !=''"/>

	<!-- Variable to find short attachments  are null or not -->
	<xsl:variable name="short_attachments" select="LINE_SHORT_TEXT/LINE_SHORT_TEXT_ROW/SHORT_TEXT != ''"/>

	<xsl:variable name="long_attachments" select="$LINE_LONG_ATTACHMENTS_ROOT_OBJ != ''"/>

        <!-- Logic to identify whether long text attachments exists for the current line -->
        <xsl:variable name="line_long_attach">
                <xsl:text>N</xsl:text>
        <xsl:for-each select="$LINE_LONG_ATTACHMENTS_ROOT_OBJ">
                <xsl:if test="$lineID = .">
					<xsl:text>Y</xsl:text>
                </xsl:if>
        </xsl:for-each>
        </xsl:variable>
      
	<xsl:if test = "LINE_TYPE = 'QUANTITY' "> <!--and PURCHASE_BASIS = 'GOODS'"> -->
		<fo:table-row >
			<fo:table-cell xsl:use-attribute-sets="table.cell4"> 
				<fo:block xsl:use-attribute-sets="form_data3"> 
					<xsl:value-of select="LINE_NUM"/> 
				</fo:block> 
			</fo:table-cell>

			<fo:table-cell xsl:use-attribute-sets="table.cell6"> 
				<fo:block xsl:use-attribute-sets="form_data">
					<xsl:value-of select="ITEM_NUM"/> 
					<xsl:text> - </xsl:text>
					<xsl:value-of select="ITEM_DESCRIPTION"/> 
				</fo:block>
				<fo:block xsl:use-attribute-sets="form_data">

				<!--          <xsl:if test="$shipment_count = 1">            -->
				<xsl:if test="LINE_LOCATIONS/LINE_LOCATIONS_ROW/PROMISED_DATE!=''">
					<xsl:choose>                       
						<!-- <xsl:when test="$VENDOR_COUNTRY_CHK = 'TH' or $VENDOR_COUNTRY_CHK = 'Thailand'"> -->
						<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
							<xsl:text>สถานที่ส่ง: </xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>Ship To: </xsl:text>
						</xsl:otherwise>
					</xsl:choose>

					<xsl:value-of select="LINE_LOCATIONS/LINE_LOCATIONS_ROW/SHIP_TO_LOCATION_NAME"/>
					<xsl:if test="LINE_LOCATIONS/LINE_LOCATIONS_ROW/SHIP_TO_ADDRESS_LINE1 !=''">
						<fo:block/>
						<xsl:value-of select="LINE_LOCATIONS/LINE_LOCATIONS_ROW/SHIP_TO_ADDRESS_LINE1"/>
					</xsl:if>
					<xsl:if test="LINE_LOCATIONS/LINE_LOCATIONS_ROW/SHIP_TO_ADDRESS_LINE2 !=''">
						<fo:block/><xsl:value-of select="LINE_LOCATIONS/LINE_LOCATIONS_ROW/SHIP_TO_ADDRESS_LINE2"/>
					</xsl:if>
					<xsl:if test="LINE_LOCATIONS/LINE_LOCATIONS_ROW/SHIP_TO_ADDRESS_LINE3 !=''">
						<fo:block/>
						<xsl:value-of select="LINE_LOCATIONS/LINE_LOCATIONS_ROW/SHIP_TO_ADDRESS_LINE3"/>
					</xsl:if>
					<xsl:if test="LINE_LOCATIONS/LINE_LOCATIONS_ROW/SHIP_TO_ADDRESS_LINE_INFO !=''">
						<fo:block/>
						<xsl:value-of select="LINE_LOCATIONS/LINE_LOCATIONS_ROW/SHIP_TO_ADDRESS_LINE_INFO"/>
					</xsl:if>

					<xsl:choose>                                
						<!-- <xsl:when test="$VENDOR_COUNTRY_CHK = 'TH' or $VENDOR_COUNTRY_CHK = 'Thailand'"> -->
						<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
							<fo:block/><xsl:text>วันกำหนดส่ง: </xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<fo:block/><xsl:text>Due Date: </xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:value-of select="substring(LINE_LOCATIONS/LINE_LOCATIONS_ROW/PROMISED_DATE,1,11)"/>
				</xsl:if>
				<!--xsl:if test="LINE_LOCATIONS/LINE_LOCATIONS_ROW/NEED_BY_DATE!=''">
				<fo:block/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_WF_NOTIF_NEEDBY_DATE'][1]/TEXT"/>
				<fo:block/> <xsl:value-of select="LINE_LOCATIONS/LINE_LOCATIONS_ROW/NEED_BY_DATE"/>
				</xsl:if -->
				<!--          </xsl:if>		 -->
			</fo:block>
		</fo:table-cell>
      <!-- Bug3670603: As per the bug Promised and need by date are added. -->
<!--      
			<fo:table-cell xsl:use-attribute-sets="table.cell6">
        <fo:block xsl:use-attribute-sets="form_data">
          <xsl:if test="$shipment_count = 1">            
            <xsl:if test="LINE_LOCATIONS/LINE_LOCATIONS_ROW/PROMISED_DATE!=''">
              <fo:block/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_WF_NOTIF_PROMISED_DATE'][1]/TEXT"/>
              <fo:block/> <xsl:value-of select="LINE_LOCATIONS/LINE_LOCATIONS_ROW/PROMISED_DATE"/>

            </xsl:if>
            <xsl:if test="LINE_LOCATIONS/LINE_LOCATIONS_ROW/NEED_BY_DATE!=''">
              <fo:block/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_WF_NOTIF_NEEDBY_DATE'][1]/TEXT"/>
              <fo:block/> <xsl:value-of select="LINE_LOCATIONS/LINE_LOCATIONS_ROW/NEED_BY_DATE"/>
            </xsl:if>
          </xsl:if>
        </fo:block> 
      </fo:table-cell>
 -->			
			<fo:table-cell xsl:use-attribute-sets="table.cell5"> 
        <fo:block xsl:use-attribute-sets="form_data1" text-align="right">
          					<xsl:choose>
					<xsl:when test="LINE_TYPE_ID=1060">
						<xsl:value-of select="UNIT_PRICE"/> <fo:block/> 
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of  select="QUANTITY"/> <fo:block/> 
					</xsl:otherwise>
					</xsl:choose>
        </fo:block> 
      </fo:table-cell>

	<fo:table-cell xsl:use-attribute-sets="table.cell6"> 
        <fo:block xsl:use-attribute-sets="form_data" text-align="center">
          <xsl:value-of  select="UNIT_MEAS_LOOKUP_CODE"/>
        </fo:block> 
      </fo:table-cell>

	<fo:table-cell xsl:use-attribute-sets="table.cell5"> 
        <fo:block xsl:use-attribute-sets="form_data1">
          <xsl:choose>
	<xsl:when test="LINE_TYPE_ID=1060">
		<xsl:value-of select="QUANTITY"/> <fo:block/>
	</xsl:when>
	<xsl:otherwise>
		<xsl:value-of  select="UNIT_PRICE"/> <fo:block/>
	</xsl:otherwise>
	</xsl:choose>
        </fo:block> 
      </fo:table-cell>
      <!-- bug#3836856: retrieved the taxable flag value from shipments -->
<!--      
			<fo:table-cell xsl:use-attribute-sets="table.cell6"> 
        <fo:block xsl:use-attribute-sets="form_data">
			    <xsl:if test="$shipment_count = 1">
            <xsl:value-of  select="LINE_LOCATIONS/LINE_LOCATIONS_ROW/TAXABLE_FLAG"/>
			   </xsl:if>
        </fo:block> 
      </fo:table-cell>
 -->
	  <fo:table-cell xsl:use-attribute-sets="table.cell5"> 
        <fo:block xsl:use-attribute-sets="form_data1">
          <xsl:value-of select="LINE_AMOUNT"/>
        </fo:block> 
      </fo:table-cell>

	  </fo:table-row>

		<!-- AP@IS ES -->
		<fo:table-row>
			<fo:table-cell>
				<fo:block>
					<fo:leader leader-pattern="space" leader-length="1.0pt" /> 
				</fo:block>
			</fo:table-cell>
			<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="table.cell6">
				<fo:block xsl:use-attribute-sets="form_data">
					<xsl:text>Inventory Org: </xsl:text> <xsl:value-of select="$LINES_ROOT_OBJ/GLOBAL_ATTRIBUTE20"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		<!-- end of AP@IS ES -->
		<!-- dTAC: Start Section -->
		<xsl:if test="$BPA_CHECK = 'Y'">
			<fo:table-row>
				<fo:table-cell>
					<fo:block>
						<fo:leader leader-pattern="space" leader-length="1.0pt" /> 
					</fo:block>
				</fo:table-cell>
				<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="table.cell6">
					<xsl:choose>
						<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
							<fo:block xsl:use-attribute-sets="form_data">
								<xsl:value-of select="LINE_ORI_UNIT_PRICE_TH"/>
							</fo:block>
						</xsl:when>
						<xsl:otherwise>
							<fo:block xsl:use-attribute-sets="form_data">
								<xsl:value-of select="LINE_ORI_UNIT_PRICE_EN"/>
							</fo:block>
						</xsl:otherwise>
					</xsl:choose>
				</fo:table-cell>
			</fo:table-row>
		</xsl:if>
		<!-- dTAC: End Section -->

		<!-- Bug3670603: Item description should display in the same row if it spans more than one column. -->
<!--    
		<fo:table-row >
			<fo:table-cell xsl:use-attribute-sets="table.cell4"> <fo:block> </fo:block> </fo:table-cell>
			<fo:table-cell xsl:use-attribute-sets="table.cell6" number-columns-spanned="7"> <fo:block xsl:use-attribute-sets="form_data">
				<xsl:value-of select="ITEM_DESCRIPTION"/>
			</fo:block> </fo:table-cell>
		</fo:table-row>
 -->
		<!-- end of Bug3670603 -->	
		
    <!-- Bug#3999145: Added the condition to identify long text line attachments are there for the current line -->
	<!--<xsl:if test="$PSA or $un_num or $hz_class or $vendor_note or $ga_flag or $con_num or $sup_quote_num or $short_attachments or $line_long_attach !='N' ">   -->
	<xsl:if test="$PSA or $un_num or $hz_class or $vendor_note or $ga_flag or $con_num or $sup_quote_num or $short_attachments or $long_attachments ">
		<fo:table-row> 
			<fo:table-cell number-columns-spanned="7"> 
				<fo:block> <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> 
			</fo:table-cell> 
		</fo:table-row>
		<fo:table-row >
			<fo:table-cell > 
        <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> 
      </fo:table-cell>
			<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="table.cell6"> 
			<fo:block xsl:use-attribute-sets="form_data">

				<!-- to display the charge Account -->
				<xsl:if test="$charge_account">
					<xsl:if test="$DisplayBoilerPlate">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_CHARGE_ACCOUNT'][1]/TEXT"/>
					</xsl:if>
					<xsl:choose>
					<xsl:when test="$distribution_count =1">
						<xsl:value-of select="$DISTRIBUTIONS_ROOT_OBJ/CHARGE_ACCOUNT"/> <fo:block/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_WF_NOTIF_MULTIPLE'][1]/TEXT"/> <fo:block/>
					</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<!-- end of charge account -->
				
				<!-- Un number row -->
				<xsl:if test="$un_num">
					<xsl:if test="$DisplayBoilerPlate">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_UN_NUMBER'][1]/TEXT"/>
					</xsl:if>
					<fo:leader leader-pattern="space" leader-length="1.0pt"/> <xsl:value-of select="UN_NUMBER"/> <fo:block/>
				</xsl:if>
				<!-- end of Un number row -->

				<!-- Hazard class -->
				<xsl:if test="$hz_class">
					<xsl:if test="$DisplayBoilerPlate">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_HAZARD_CLASS'][1]/TEXT"/>
					</xsl:if>
					<fo:leader leader-pattern="space" leader-length="1.0pt"/> <xsl:value-of select="HAZARD_CLASS"/> <fo:block/>
				</xsl:if>
				<!-- end of Hazard class -->

				<!-- Vendor Notes -->
				<xsl:if test="$vendor_note">
					<xsl:value-of select="NOTE_TO_VENDOR"/> <fo:block/>
				</xsl:if>
				<!-- end of note to vendor -->

				<!-- for lines short text -->
				<xsl:for-each select="LINE_SHORT_TEXT/LINE_SHORT_TEXT_ROW">
					<xsl:value-of select ="SHORT_TEXT"/> <fo:block/>
				</xsl:for-each>

				<!-- for long text -->
				<xsl:for-each select="$LINE_LONG_ATTACHMENTS_ROOT_OBJ">
					<xsl:if test="$lineID = .">
						<xsl:variable name="line" select="position()" />
						<xsl:value-of select="../TEXT[$line]"/>	<fo:block/> 	
					</xsl:if>
				</xsl:for-each>

				<!-- Global agreement value -->
				<xsl:if test="$ga_flag">
					<xsl:if test="$DisplayBoilerPlate">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_LINE_REF_BPA'][1]/TEXT"/>
					</xsl:if>
					<fo:leader leader-pattern="space" leader-length="1.0pt"/> <xsl:value-of select="SEGMENT1"/> <fo:block/>
				</xsl:if>
				<!-- end of global agreement flag -->

				<!-- Contract agreement  -->
				<xsl:if test="$con_num">
					<xsl:if test="$DisplayBoilerPlate">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_LINE_REF_CONTRACT'][1]/TEXT"/>
					</xsl:if>
					<fo:leader leader-pattern="space" leader-length="1.0pt"/> <xsl:value-of select="CONTRACT_NUM"/> <fo:block/>
				</xsl:if>
				<!-- end of contract num -->

				<!-- Supplier quotation number -->
				<xsl:if test="$sup_quote_num">
					<xsl:if test="$DisplayBoilerPlate">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_LINE_SUPPLIER_QUOTATION'][1]/TEXT"/>
					</xsl:if>
					<fo:leader leader-pattern="space" leader-length="1.0pt"/> <xsl:value-of select="QUOTE_VENDOR_QUOTE_NUMBER"/> <fo:block/>
				</xsl:if>
				<!-- end of Supplier quotation number -->
			 				
			</fo:block> 
			</fo:table-cell>
		</fo:table-row>
		</xsl:if>


		<!-- display canceled details -->
		<xsl:if test="CANCEL_FLAG='Y'">
		<fo:table-row> <fo:table-cell number-columns-spanned="8"> <fo:block > <fo:leader leader-pattern="space" leader-length="2.0pt"/> </fo:block>	</fo:table-cell> </fo:table-row>
		<fo:table-row  keep-together.within-page="always">
			<fo:table-cell > <fo:block >  </fo:block> </fo:table-cell>
			<fo:table-cell number-columns-spanned="7">  <fo:block> 
				<fo:table>
				<fo:table-column column-width="70mm"/>
				<fo:table-body> <fo:table-row> <fo:table-cell xsl:use-attribute-sets="table.cell7">  <fo:block xsl:use-attribute-sets="form_data2">
				<fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_LINE_CANCELED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="substring(CANCEL_DATE,1,11)"/> <fo:block/>
				<fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_ORIGINAL_QTY_ORDERED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="sum(LINE_LOCATIONS/LINE_LOCATIONS_ROW/QUANTITY)"/> <fo:block/>
				<fo:leader leader-pattern="space" leader-length="24.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_QUANTITY_CANCELED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="sum(LINE_LOCATIONS/LINE_LOCATIONS_ROW/QUANTITY_CANCELLED)"/> <fo:block/>
				</fo:block> </fo:table-cell> </fo:table-row> </fo:table-body>
				</fo:table>
			</fo:block> </fo:table-cell>
		</fo:table-row>
		</xsl:if>
		<!-- end of canceled details -->

		<!-- start of shipment details -->
    <xsl:if test="$shipment_count &gt; 0"> <!-- bug#3823799:condition to check whether there are any shipments -->
      <xsl:for-each select="LINE_LOCATIONS/LINE_LOCATIONS_ROW">
        <!--Bug#3999145: Holds the current line location ID value -->
        <xsl:variable name="lineLocID" select="LINE_LOCATION_ID"/>
      		<fo:table-row> 
            <fo:table-cell number-columns-spanned="8"> 
              <fo:block font-size="8pt"> <fo:leader leader-pattern="space" leader-length="2.0pt"/> </fo:block>	
            </fo:table-cell> 
          </fo:table-row>

          <fo:table-row  >
            <fo:table-cell > 
              <fo:block > </fo:block> 
            </fo:table-cell>
            <fo:table-cell xsl:use-attribute-sets="table.cell6">  
              <fo:block xsl:use-attribute-sets="form_data"> 
                <fo:table>
                  <fo:table-column column-width="3.5mm"/> 
                  <fo:table-column column-width="75mm"/>
                    <fo:table-body>
                      <fo:table-row >
                        <fo:table-cell > 
                          <fo:block>  </fo:block> 
                        </fo:table-cell>
                        <fo:table-cell > 
                          <fo:block>  
                            <xsl:if test="$DisplayBoilerPlate ">
                              <xsl:if test="$print_multiple ='Y'">
                                <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_SHIP_TO'][1]/TEXT"/> <fo:block/>
                              </xsl:if>
                            </xsl:if>
                       
                            <!-- bug#3594831: added drop_ship ship to details -->
                            <!-- Bug3670603: Address details should be displayed before Promised and Need by date -->
                            <xsl:if test="DROP_SHIP_FLAG='Y'">
                              <xsl:if test="SHIP_CUST_NAME !=''"> <xsl:value-of select="SHIP_CUST_NAME"/>  </xsl:if>
                              <xsl:if test="SHIP_CONT_NAME !=''"> <fo:block/> <xsl:value-of select="SHIP_CONT_NAME"/>  </xsl:if>
                              <xsl:if test="SHIP_CONT_EMAIL !=''"> <fo:block/><xsl:value-of select="SHIP_CONT_EMAIL"/> </xsl:if>
                              <xsl:if test="SHIP_CONT_PHONE !=''">
                              <xsl:if test="$DisplayBoilerPlate ">
                                 <fo:block/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_TELEPHONE'][1]/TEXT"/>:
                              </xsl:if>
                              <fo:leader leader-pattern="space" leader-length="3.0pt"/> <xsl:value-of select="SHIP_CONT_PHONE"/> 
                            </xsl:if>
                            <xsl:if test="SHIP_TO_CONTACT_FAX !=''"> 
                              <xsl:if test="$DisplayBoilerPlate ">
                                <fo:block/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_FAX'][1]/TEXT"/>:
                              </xsl:if>
                              <fo:leader leader-pattern="space" leader-length="3.0pt"/> <xsl:value-of select="SHIP_TO_CONTACT_FAX"/> 
                            </xsl:if>
                          </xsl:if>
                          <!-- end of bug#3594831 -->
                          <xsl:if test="$print_multiple ='Y'">
                            <fo:block/> <xsl:value-of select="SHIP_TO_ADDRESS_LINE1"/>
                            <xsl:if test="SHIP_TO_ADDRESS_LINE2 !=''"> <fo:block/><xsl:value-of select="SHIP_TO_ADDRESS_LINE2"/> </xsl:if>
                            <xsl:if test="SHIP_TO_ADDRESS_LINE3 !=''"> <fo:block/><xsl:value-of select="SHIP_TO_ADDRESS_LINE3"/> </xsl:if>
                            <xsl:if test="SHIP_TO_ADDRESS_INFO !=''"> <fo:block/> <xsl:value-of select="SHIP_TO_ADDRESS_INFO"/> </xsl:if>
                            <xsl:choose>
                              <!-- <xsl:when test="$VENDOR_COUNTRY_CHK = 'TH' or $VENDOR_COUNTRY_CHK = 'Thailand'"> </xsl:when> -->
			      <xsl:when test="$CURRENCY_CODE_CHK = 'THB'"> </xsl:when>
                              <xsl:otherwise>
                                <xsl:if test="SHIP_TO_COUNTRY !=''"> <fo:block/><xsl:value-of select="SHIP_TO_COUNTRY"/>	</xsl:if>
                              </xsl:otherwise>
                            </xsl:choose>
                            <xsl:if test="PROMISED_DATE !=''">
                              <fo:block/>
                              <fo:block/>
                              <fo:block>
                              <xsl:choose>                                
                                <!-- <xsl:when test="$VENDOR_COUNTRY_CHK = 'TH' or $VENDOR_COUNTRY_CHK = 'Thailand'"> -->
				<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
                                <xsl:text>กำหนดส่งสินค้า: </xsl:text>
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:text>Delivery Date: </xsl:text>
                                </xsl:otherwise>
                              </xsl:choose>
                              <xsl:value-of select="PROMISED_DATE"/> 
                              </fo:block>
                            </xsl:if>
                          </xsl:if>
                        </fo:block> 
                      </fo:table-cell>
                    </fo:table-row>                    
                  </fo:table-body>
                </fo:table>
              <!-- end of bug#3594831 -->
              </fo:block> 
            </fo:table-cell>
<!-- TM CUSTOMMIZE : Not show with this format -->
      <!-- Bug3670603: As per the bug Promised and need by date are added. -->
		</fo:table-row>
		<!-- for lines short text -->
                <!--Bug#3999145: Removed fo:leader tag to reduce the space between the attachments -->
  <xsl:for-each select="LINE_LOC_SHORT_TEXT/LINE_LOC_SHORT_TEXT_ROW">
		<fo:table-row >
			<fo:table-cell > <fo:block >  </fo:block> </fo:table-cell>
			<fo:table-cell xsl:use-attribute-sets="table.cell6" number-columns-spanned="7">  <fo:block xsl:use-attribute-sets="form_data"> 
		                <!--Bug#4088207: Call LINE_LOCATION_ATTACHMENT_TEMP template to display the attachment -->
        <xsl:call-template name="LINE_LOCATION_ATTACHMENT_TEMP">
          <xsl:with-param name="ATTACHMENT_TEXT" select="SHORT_TEXT"/>
				</xsl:call-template>                                
			</fo:block> </fo:table-cell>
		</fo:table-row>
	</xsl:for-each>

		<!-- for long text -->
                <!--Bug#3999145: Modified the test condition to display the shipment long attachments for the current shipment -->
		<xsl:for-each select="$SHIPMENT_ATTACHMENTS_ROOT_OBJ">
			<xsl:if test="$lineLocID = .">
				<xsl:variable name="line" select="position()" />
				<fo:table-row >
				<fo:table-cell > <fo:block >  </fo:block> </fo:table-cell>
				<fo:table-cell xsl:use-attribute-sets="table.cell6" number-columns-spanned="7">  <fo:block xsl:use-attribute-sets="form_data"> 
                                        
          <!--Bug#4088207: Call LINE_LOCATION_ATTACHMENT_TEMP template to display the attachment -->
           <xsl:call-template name="LINE_LOCATION_ATTACHMENT_TEMP">
                  <xsl:with-param name="ATTACHMENT_TEXT" select="../LONG_TEXT[$line]"/>
           </xsl:call-template>
                                        
				</fo:block> </fo:table-cell>
		</fo:table-row>
			</xsl:if>
		</xsl:for-each>

		<!-- display canceled details -->
		<xsl:if test="CANCEL_FLAG='Y'">
		<fo:table-row> <fo:table-cell number-columns-spanned="8"> <fo:block > <fo:leader leader-pattern="space" leader-length="2.0pt"/> </fo:block>	</fo:table-cell> </fo:table-row>
		<fo:table-row keep-together.within-page="always" >
			<fo:table-cell > <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell>
			<fo:table-cell number-columns-spanned="7">  <fo:block> 
				<fo:table>
				<fo:table-column column-width="5mm"/> <fo:table-column column-width="78mm"/>
				<fo:table-body> <fo:table-row> 
				<fo:table-cell> <fo:block> </fo:block> </fo:table-cell>
				<fo:table-cell xsl:use-attribute-sets="table.cell7">  <fo:block xsl:use-attribute-sets="form_data2" >
				<fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_SHIPMENT_CANCELED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="substring(CANCEL_DATE,1,11)"/> <fo:block/>
				<fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_ORIGINAL_QTY_ORDERED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="QUANTITY"/> <fo:block/>
				<fo:leader leader-pattern="space" leader-length="24.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_QUANTITY_CANCELED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="QUANTITY_CANCELLED"/> <fo:block/>
				</fo:block> </fo:table-cell> </fo:table-row> </fo:table-body>
				</fo:table>
			</fo:block> </fo:table-cell>
		</fo:table-row>
		<fo:table-row> <fo:table-cell number-columns-spanned="8"  font-size="8pt"> <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block>	</fo:table-cell> </fo:table-row>
		</xsl:if>
		<!-- end of canceled details -->

		<fo:table-row> <fo:table-cell number-columns-spanned="8" font-size="4pt"> <fo:block> <fo:leader leader-pattern="space" leader-length="0.0pt"/>  </fo:block></fo:table-cell> </fo:table-row>
		
		<!-- display deliver to details -->
    
<!-- iCE CUSTOMIZE : Not Show Deliver Detaial for DTAC -->    

		<xsl:for-each select="DISTRIBUTIONS/DISTRIBUTIONS_ROW">
		<xsl:if test="../../DROP_SHIP_FLAG !='' or REQUESTER_DELIVER_FIRST_NAME !='' ">
			<fo:table-row>
			<fo:table-cell > <fo:block> </fo:block> </fo:table-cell>
			<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="table.cell6">  <fo:block xsl:use-attribute-sets="form_data"> 
			<fo:table>
			<fo:table-column column-width="4mm"/>
      <fo:table-column column-width="20mm"/> 
      <fo:table-column column-width="150mm"/>
			<fo:table-body> 
				<fo:table-row> 
          <fo:table-cell > <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell>
          <fo:table-cell > 
            <fo:block>
              <xsl:if test="DELIVER_TO_PERSON_ID !=''"> <!-- bug#3594831: Deliver To message will not displayed if id is null -->
                <xsl:if test="$DisplayBoilerPlate ">
                  <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_DELIVER_TO_LOCATION'][1]/TEXT"/>
                </xsl:if> 
              </xsl:if>  <!-- bug#3594831: -->
            </fo:block> 
          </fo:table-cell>
			
          <fo:table-cell>  
            <fo:block>
              <!-- start of displaying deliver to details -->
              <xsl:if test="DELIVER_TO_PERSON_ID !=''"> <!-- bug#3594831: Deliver To message will not displayed if id is null -->
		
		<xsl:choose>
			<!-- <xsl:when test="$VENDOR_COUNTRY_CHK = 'TH' or $VENDOR_COUNTRY_CHK = 'Thailand'"> -->
			<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
				<xsl:value-of select="REQUESTER_THAI_NAME"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="NAME_TEMPLATE">
                		<xsl:with-param name="FIRST_NAME" select="REQUESTER_DELIVER_FIRST_NAME"/>
                		<xsl:with-param name="LAST_NAME" select="REQUESTER_DELIVER_LAST_NAME"/>
                		<xsl:with-param name="TITLE" select="REQUESTER_DELIVER_TITLE"/>
                		</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>

                <xsl:if test="QUANTITY_ORDERED !=''"> 
                  <fo:leader leader-pattern="space" leader-length="2.0pt"/> (<xsl:value-of select="QUANTITY_ORDERED"/>)
                </xsl:if>
                <fo:block/>
					
					<xsl:text>Email: </xsl:text> <xsl:value-of select="EMAIL_ADDRESS"/> <fo:block/>
					<xsl:text>Tel No.: </xsl:text>
					<xsl:value-of select="WORK_TELEPHONE"/> <fo:block/>
					<xsl:text>PR No.: </xsl:text> <xsl:value-of select="PR_NNUMBER"/> <fo:block/>
					<xsl:text>Project Code: </xsl:text> <xsl:value-of select="PROJECT_CODE"/> <fo:block/>
                                        <xsl:text>Requester Location: </xsl:text> <xsl:value-of select="LOCATION_CODE"/> <fo:block/>
					<xsl:text>Department: </xsl:text> <xsl:value-of select="DEPARTMENT_CODE"/> <fo:block/>
					<xsl:text>Division: </xsl:text> <xsl:value-of select="DIVISION_CODE"/> <fo:block/>
					<xsl:text>Group: </xsl:text> <xsl:value-of select="GROUP_CODE"/> <fo:block/>
		         <!-- end of deliver details -->
              </xsl:if>  <!-- bug#3594831: -->
              
				</fo:block> </fo:table-cell>
				</fo:table-row>
			</fo:table-body></fo:table>
			</fo:block> </fo:table-cell>
			</fo:table-row>
		</xsl:if>    
		</xsl:for-each>   	<!-- end of distributions if -->
		
	</xsl:for-each> <!-- end of shipment if -->
        </xsl:if> <!-- end of if for shipment count -->
	
  <fo:table-row> 
    <fo:table-cell number-columns-spanned="8"> 
      <fo:block > <fo:leader leader-pattern="space" leader-length="2.0pt"/> </fo:block>	
    </fo:table-cell> 
  </fo:table-row>
</xsl:if> <!-- end of Goods line type -->

	<!-- amount based line -->
	<!-- Note by AP@IS ES : PO Standard template focus hear  -->
	<xsl:if test = "LINE_TYPE = 'AMOUNT'">
		
		<fo:table-row >
			<fo:table-cell xsl:use-attribute-sets="table.cell4"> 
        <fo:block xsl:use-attribute-sets="form_data3"> 
          <xsl:value-of select="LINE_NUM"/> 
        </fo:block> 
      </fo:table-cell>
      
      <fo:table-cell xsl:use-attribute-sets="table.cell6"> 
        <!--fo:block xsl:use-attribute-sets="form_data" -->
		 <fo:block>
          <xsl:value-of select="ITEM_NUM"/> 
          <xsl:text> - </xsl:text>
          <xsl:value-of select="ITEM_DESCRIPTION"/> 
        </fo:block> 
      </fo:table-cell>
<!-- Modify columns (Quantity and Unit Price) in amount based line type -->
<!-- by ndl 05-APR-2007 -->

     	<fo:table-cell xsl:use-attribute-sets="table.cell5"> 
        <fo:block xsl:use-attribute-sets="form_data1" text-align="center">
		<xsl:if test="ORDER_TYPE_LOOKUP_CODE='QUANTITY'"> <xsl:value-of  select="QUANTITY"/></xsl:if>
		<xsl:if test="ORDER_TYPE_LOOKUP_CODE='AMOUNT'"> <xsl:value-of  select="UNIT_PRICE"/></xsl:if>
        </fo:block> 
      </fo:table-cell>
     
     	<fo:table-cell xsl:use-attribute-sets="table.cell6"> 
        <fo:block xsl:use-attribute-sets="form_data" text-align="center">
          <xsl:value-of  select="UNIT_MEAS_LOOKUP_CODE"/>
        </fo:block> 
      </fo:table-cell>
      
      <fo:table-cell xsl:use-attribute-sets="table.cell5"> 
        <fo:block xsl:use-attribute-sets="form_data1">
		<xsl:if test="ORDER_TYPE_LOOKUP_CODE='QUANTITY'"> <xsl:value-of  select="UNIT_PRICE"/></xsl:if>
		<xsl:if test="ORDER_TYPE_LOOKUP_CODE='AMOUNT'"> <xsl:value-of  select="QUANTITY"/></xsl:if>  
        </fo:block> 
      </fo:table-cell>
      
			<fo:table-cell xsl:use-attribute-sets="table.cell5"> 
        <fo:block xsl:use-attribute-sets="form_data1">
          <xsl:value-of  select="LINE_AMOUNT"/>
        </fo:block> 
      </fo:table-cell>
		</fo:table-row>

		<!-- AP@IS ES -->
		<fo:table-row>
			<fo:table-cell>
				<fo:block>
					<fo:leader leader-pattern="space" leader-length="1.0pt" /> 
				</fo:block>
			</fo:table-cell>
			<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="table.cell6">
				<fo:block xsl:use-attribute-sets="form_data">
					<xsl:text>Inventory Org: </xsl:text> <xsl:value-of select="$LINES_ROOT_OBJ/GLOBAL_ATTRIBUTE20"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		<!-- end of AP@IS ES -->
		<!-- dTAC: Start Section -->
		<xsl:if test="$BPA_CHECK = 'Y'">
		<fo:table-row>
			<fo:table-cell>
				<fo:block>
					<fo:leader leader-pattern="space" leader-length="1.0pt" /> 
				</fo:block>
			</fo:table-cell>
			<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="table.cell6">
				<xsl:choose>
					<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
						<fo:block xsl:use-attribute-sets="form_data">
							<xsl:value-of select="$LINES_ROOT_OBJ/LINE_ORI_UNIT_PRICE_TH"/>
						</fo:block>
					</xsl:when>
					<xsl:otherwise>
						<fo:block xsl:use-attribute-sets="form_data">
							<xsl:value-of select="$LINES_ROOT_OBJ/LINE_ORI_UNIT_PRICE_EN"/>
						</fo:block>
					</xsl:otherwise>
				</xsl:choose>
			</fo:table-cell>
		</fo:table-row>
		</xsl:if>
		<!-- dTAC: End Section -->

		<!-- Bug#3999145: Added the condition to identify long text line attachments are there for the current line -->
    <!-- Bug#4088207: Removed "or $line_long_attach" from the if condition as it repeated twice -->
    <!--< xsl:if test="$PSA or $un_num or $hz_class or $vendor_note or $ga_flag or $con_num or $sup_quote_num or $short_attachments or $line_long_attach !='N'"> -->
	<xsl:if test="$PSA or $un_num or $hz_class or $vendor_note or $ga_flag or $con_num or $sup_quote_num or $short_attachments or $long_attachments ">
		<fo:table-row>
			<fo:table-cell number-columns-spanned="7"> 
				<fo:block> <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> 
			</fo:table-cell> 
		</fo:table-row>

		<fo:table-row >
			<fo:table-cell > <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell>
			<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="table.cell6"> 
			<fo:block xsl:use-attribute-sets="form_data">

				<!-- to display the charge Account -->
				<xsl:if test="$charge_account">
					<xsl:if test="$DisplayBoilerPlate">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_CHARGE_ACCOUNT'][1]/TEXT"/>
					</xsl:if>
					<xsl:choose>
					<xsl:when test="$distribution_count =1">
						<xsl:value-of select="$DISTRIBUTIONS_ROOT_OBJ/CHARGE_ACCOUNT"/> <fo:block/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_WF_NOTIF_MULTIPLE'][1]/TEXT"/> <fo:block/>
					</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<!-- end of charge account -->
				
				<!-- Un number row -->
				<xsl:if test="$un_num">
					<xsl:if test="$DisplayBoilerPlate">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_UN_NUMBER'][1]/TEXT"/>
					</xsl:if>
					<fo:leader leader-pattern="space" leader-length="1.0pt"/> <xsl:value-of select="UN_NUMBER"/> <fo:block/>
				</xsl:if>
				<!-- end of Un number row -->

				<!-- Hazard class -->
				<xsl:if test="$hz_class">
					<xsl:if test="$DisplayBoilerPlate">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_HAZARD_CLASS'][1]/TEXT"/>
					</xsl:if>
					<fo:leader leader-pattern="space" leader-length="1.0pt"/> <xsl:value-of select="HAZARD_CLASS"/> <fo:block/>
				</xsl:if>
				<!-- end of Hazard class -->

				<!-- Vendor Notes -->
				<xsl:if test="$vendor_note">
					<xsl:value-of select="NOTE_TO_VENDOR"/> <fo:block/>
				</xsl:if>
				<!-- end of note to vendor -->

				<!-- for lines short text -->
				<xsl:for-each select="LINE_SHORT_TEXT/LINE_SHORT_TEXT_ROW">	
					<xsl:value-of select ="SHORT_TEXT"/> <fo:block/>
				</xsl:for-each>

				<!-- for long text -->
				<xsl:for-each select="$LINE_LONG_ATTACHMENTS_ROOT_OBJ">
					<xsl:if test="$lineID = .">
						<xsl:variable name="line" select="position()" />
						<xsl:value-of select="../TEXT[$line]"/>	<fo:block/> 	
					</xsl:if>
				</xsl:for-each>

				<!-- Global agreement value -->
				<xsl:if test="$ga_flag">
					<xsl:if test="$DisplayBoilerPlate">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_LINE_REF_BPA'][1]/TEXT"/>
					</xsl:if>
					<fo:leader leader-pattern="space" leader-length="1.0pt"/> <xsl:value-of select="SEGMENT1"/> <fo:block/>
				</xsl:if>
				<!-- end of global agreement flag -->

				<!-- Contract agreement  -->
				<xsl:if test="$con_num">
					<xsl:if test="$DisplayBoilerPlate">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_LINE_REF_CONTRACT'][1]/TEXT"/>
					</xsl:if>
					<fo:leader leader-pattern="space" leader-length="1.0pt"/> <xsl:value-of select="CONTRACT_NUM"/> <fo:block/>
				</xsl:if>
				<!-- end of contract num -->

				<!-- Supplier quotation number -->
				<xsl:if test="$sup_quote_num">
					<xsl:if test="$DisplayBoilerPlate">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_LINE_SUPPLIER_QUOTATION'][1]/TEXT"/>
					</xsl:if>
					<fo:leader leader-pattern="space" leader-length="1.0pt"/> <xsl:value-of select="QUOTE_VENDOR_QUOTE_NUMBER"/> <fo:block/>
				</xsl:if>
				<!-- end of Supplier quotation number -->
			 				
			</fo:block> 
			</fo:table-cell>
		</fo:table-row>
		</xsl:if>
		
		<!-- display canceled details -->

		<xsl:if test="CANCEL_FLAG='Y'">
		<fo:table-row> <fo:table-cell number-columns-spanned="8"> <fo:block > <fo:leader leader-pattern="space" leader-length="2.0pt"/> </fo:block>	</fo:table-cell> </fo:table-row>
		<fo:table-row  keep-together.within-page="always">
			<fo:table-cell > <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell>
			<fo:table-cell number-columns-spanned="7">  <fo:block> 
				<fo:table>
				<fo:table-column column-width="70mm"/>
				<fo:table-body> <fo:table-row> <fo:table-cell xsl:use-attribute-sets="table.cell7">  <fo:block xsl:use-attribute-sets="form_data2">
				<fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_LINE_CANCELED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="substring(CANCEL_DATE,1,11)"/> <fo:block/>
				<fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_ORIGINAL_QTY_ORDERED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="sum(LINE_LOCATIONS/LINE_LOCATIONS_ROW/QUANTITY)"/> <fo:block/>
				<fo:leader leader-pattern="space" leader-length="24.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_QUANTITY_CANCELED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="sum(LINE_LOCATIONS/LINE_LOCATIONS_ROW/QUANTITY_CANCELLED)"/> <fo:block/>
				</fo:block> </fo:table-cell> </fo:table-row> </fo:table-body>
				</fo:table>
			</fo:block> </fo:table-cell>
		</fo:table-row>
		</xsl:if>
		<!-- end of canceled details -->

		<xsl:for-each select="LINE_LOCATIONS/LINE_LOCATIONS_ROW">
      <!--Bug#3999145: Holds the current line location ID -->
      <xsl:variable name="lineLocID" select="LINE_LOCATION_ID"/>
        <fo:table-row> 
          <fo:table-cell number-columns-spanned="8"> 
            <fo:block font-size="8pt"> <fo:leader leader-pattern="space" leader-length="2.0pt"/></fo:block>	
          </fo:table-cell> 
        </fo:table-row>

        <fo:table-row  >
          <fo:table-cell > <fo:block >  </fo:block> </fo:table-cell>
          <fo:table-cell xsl:use-attribute-sets="table.cell6">  
            <fo:block xsl:use-attribute-sets="form_data"> 
          <fo:table>
          <fo:table-column column-width="3.5mm"/> 
          <fo:table-column column-width="75mm"/>
          <fo:table-body>
          <fo:table-row >
          <fo:table-cell > <fo:block> </fo:block> </fo:table-cell>
          <fo:table-cell > <fo:block>
		  <fo:block/>
          <fo:table-cell > <fo:block>  </fo:block> </fo:table-cell>


            <xsl:if test="$DisplayBoilerPlate ">
              <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_SHIP_TO'][1]/TEXT"/> <fo:block/>
            </xsl:if>
            <!-- bug#3594831: added drop_ship ship to details -->
            <!-- Bug3670603: Address details should be displayed before Promised and Need by date -->
            <xsl:if test="DROP_SHIP_FLAG='Y'">
              <xsl:if test="SHIP_CUST_NAME !=''"> <xsl:value-of select="SHIP_CUST_NAME"/>  </xsl:if>
              <xsl:if test="SHIP_CONT_NAME !=''"> <fo:block/> <xsl:value-of select="SHIP_CONT_NAME"/>  </xsl:if>
              <xsl:if test="SHIP_CONT_EMAIL !=''"> <fo:block/><xsl:value-of select="SHIP_CONT_EMAIL"/> </xsl:if>
              <xsl:if test="SHIP_CONT_PHONE !=''">
                <xsl:if test="$DisplayBoilerPlate ">
                   <fo:block/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_TELEPHONE'][1]/TEXT"/>:
                </xsl:if>
                <fo:leader leader-pattern="space" leader-length="3.0pt"/> <xsl:value-of select="SHIP_CONT_PHONE"/> 
              </xsl:if>
              <xsl:if test="SHIP_TO_CONTACT_FAX !=''"> 
                <xsl:if test="$DisplayBoilerPlate ">
                   <fo:block/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_FAX'][1]/TEXT"/>:
                </xsl:if>
                 <fo:leader leader-pattern="space" leader-length="3.0pt"/> <xsl:value-of select="SHIP_TO_CONTACT_FAX"/> 
              </xsl:if>
            </xsl:if>
            <!-- end of bug#3594831 -->
            <xsl:choose>
              <xsl:when test="$print_multiple ='Y'">
                <fo:block/> <xsl:value-of select="SHIP_TO_ADDRESS_LINE1"/>
                <xsl:if test="SHIP_TO_ADDRESS_LINE2 !=''"> <fo:block/><xsl:value-of select="SHIP_TO_ADDRESS_LINE2"/> </xsl:if>
                <xsl:if test="SHIP_TO_ADDRESS_LINE3 !=''"> <fo:block/><xsl:value-of select="SHIP_TO_ADDRESS_LINE3"/> </xsl:if>
                <xsl:if test="SHIP_TO_ADDRESS_INFO !=''"> <fo:block/> <xsl:value-of select="SHIP_TO_ADDRESS_INFO"/> </xsl:if>
                <xsl:choose>
                  <!-- <xsl:when test="$VENDOR_COUNTRY_CHK = 'TH' or $VENDOR_COUNTRY_CHK = 'Thailand'"> </xsl:when> -->
		  <xsl:when test="$CURRENCY_CODE_CHK = 'THB'"> </xsl:when>
                  <xsl:otherwise>
                    <xsl:if test="SHIP_TO_COUNTRY !=''"> <fo:block/><xsl:value-of select="SHIP_TO_COUNTRY"/>	</xsl:if>
                   </xsl:otherwise>
                 </xsl:choose>
                  <xsl:if test="PROMISED_DATE !=''">
                    <fo:block/>
                    <fo:block/>
                    <fo:block>
                      <xsl:choose>                                
                        <!-- <xsl:when test="$VENDOR_COUNTRY_CHK = 'TH' or $VENDOR_COUNTRY_CHK = 'Thailand'"> -->
			<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
                        <xsl:text>กำหนดส่งสินค้า: </xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:text>Delivery Date: </xsl:text>
                        </xsl:otherwise>
                      </xsl:choose>
                      <xsl:value-of select="PROMISED_DATE"/> 
                    </fo:block>

                  </xsl:if>
              </xsl:when>
              <xsl:otherwise>
<!--iCE Add Shipment -->
                <xsl:if test="$shipment_count &gt; 0">
                <fo:block/> 
				<xsl:value-of select="LINE_LOCATIONS/LINE_LOCATIONS_ROW/SHIP_TO_LOCATION_NAME"/>
			<xsl:if test="LINE_LOCATIONS/LINE_LOCATIONS_ROW/SHIP_TO_ADDRESS_LINE1 !=''"><fo:block/><xsl:value-of select="LINE_LOCATIONS/LINE_LOCATIONS_ROW/SHIP_TO_ADDRESS_LINE1"/></xsl:if>
			<xsl:if test="LINE_LOCATIONS/LINE_LOCATIONS_ROW/SHIP_TO_ADDRESS_LINE2 !=''"><fo:block/><xsl:value-of select="LINE_LOCATIONS/LINE_LOCATIONS_ROW/SHIP_TO_ADDRESS_LINE2"/></xsl:if>
			<xsl:if test="LINE_LOCATIONS/LINE_LOCATIONS_ROW/SHIP_TO_ADDRESS_LINE3 !=''"><fo:block/><xsl:value-of select="LINE_LOCATIONS/LINE_LOCATIONS_ROW/SHIP_TO_ADDRESS_LINE3"/></xsl:if>
			<xsl:if test="LINE_LOCATIONS/LINE_LOCATIONS_ROW/SHIP_TO_ADDRESS_LINE_INFO !=''"><fo:block/><xsl:value-of select="LINE_LOCATIONS/LINE_LOCATIONS_ROW/SHIP_TO_ADDRESS_LINE_INFO"/></xsl:if>

				<xsl:value-of select="SHIP_TO_ADDRESS_LINE1"/>
                <xsl:if test="SHIP_TO_ADDRESS_LINE2 !=''"> <fo:block/><xsl:value-of select="SHIP_TO_ADDRESS_LINE2"/> </xsl:if>
                <xsl:if test="SHIP_TO_ADDRESS_LINE3 !=''"> <fo:block/><xsl:value-of select="SHIP_TO_ADDRESS_LINE3"/> </xsl:if>
                <xsl:if test="SHIP_TO_ADDRESS_INFO !=''"> <fo:block/> <xsl:value-of select="SHIP_TO_ADDRESS_INFO"/> </xsl:if>
				<!--xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_USE_SHIP_ADDRESS_TOP'][1]/TEXT"/ --> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <fo:page-number-citation ref-id="page_ref" />
                </xsl:if>
              </xsl:otherwise>
            </xsl:choose> 
			</fo:block> </fo:table-cell>
			</fo:table-row>
			</fo:table-body>
			</fo:table>
			<!-- end of bug#3594831 -->
			</fo:block> 
    </fo:table-cell>
		</fo:table-row>

      <!-- for lines short text -->
                <!--Bug#3999145: Removed fo:leader tag to reduce the space between the attachments -->
                <xsl:for-each select="LINE_LOC_SHORT_TEXT/LINE_LOC_SHORT_TEXT_ROW">
		<fo:table-row >
			<fo:table-cell > <fo:block >  </fo:block> </fo:table-cell>
			<fo:table-cell xsl:use-attribute-sets="table.cell6" number-columns-spanned="7">  <fo:block xsl:use-attribute-sets="form_data"> 
                                <!--Bug#4088207: Call LINE_LOCATION_ATTACHMENT_TEMP template to display the attachment -->
                                <xsl:call-template name="LINE_LOCATION_ATTACHMENT_TEMP">
                                        <xsl:with-param name="ATTACHMENT_TEXT" select="SHORT_TEXT"/>
                                </xsl:call-template>
			</fo:block> </fo:table-cell>
		</fo:table-row>
		</xsl:for-each>

		<!-- for long text -->
                <!--Bug#3999145: Modified the test condition to display the shipment attachments for the current shipment -->
                <xsl:for-each select="$SHIPMENT_ATTACHMENTS_ROOT_OBJ">
			<xsl:if test="$lineLocID = .">
				<xsl:variable name="line" select="position()" />
				<fo:table-row >
				<fo:table-cell > <fo:block >  </fo:block> </fo:table-cell>
				<fo:table-cell xsl:use-attribute-sets="table.cell6" number-columns-spanned="7">  <fo:block xsl:use-attribute-sets="form_data"> 

					<!--Bug#4088207: Call LINE_LOCATION_ATTACHMENT_TEMP template to display the attachment -->
                                         <xsl:call-template name="LINE_LOCATION_ATTACHMENT_TEMP">
                                                <xsl:with-param name="ATTACHMENT_TEXT" select="../LONG_TEXT[$line]"/>
                                         </xsl:call-template>

				</fo:block> </fo:table-cell>
		</fo:table-row>
			</xsl:if>
		</xsl:for-each>
		
		<!-- display canceled details -->
		<xsl:if test="CANCEL_FLAG='Y'">
		<fo:table-row> <fo:table-cell number-columns-spanned="8"> <fo:block > <fo:leader leader-pattern="space" leader-length="2.0pt"/> </fo:block>	</fo:table-cell> </fo:table-row>
		<fo:table-row  keep-together.within-page="always">
			<fo:table-cell > <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell>
			<fo:table-cell number-columns-spanned="7">  <fo:block> 
				<fo:table>
				<fo:table-column column-width="5mm"/> <fo:table-column column-width="78mm"/>
				<fo:table-body> <fo:table-row> 
				<fo:table-cell> <fo:block> </fo:block> </fo:table-cell>
				<fo:table-cell xsl:use-attribute-sets="table.cell7">  <fo:block xsl:use-attribute-sets="form_data2">
				<fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_SHIPMENT_CANCELED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="substring(CANCEL_DATE,1,11)"/> <fo:block/>
				<fo:leader leader-pattern="space" leader-length="20.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_ORIGINAL_SHIPMENT_QTY'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="QUANTITY"/> <fo:block/>
				<fo:leader leader-pattern="space" leader-length="40.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_QUANTITY_CANCELED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="QUANTITY"/> <fo:block/>
				</fo:block> </fo:table-cell> </fo:table-row> </fo:table-body>
				</fo:table>
			</fo:block> </fo:table-cell>
		</fo:table-row>
		<fo:table-row> <fo:table-cell number-columns-spanned="8"  font-size="8pt"> <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block>	</fo:table-cell> </fo:table-row>
		</xsl:if>
		<!-- end of canceled details -->

		<fo:table-row> <fo:table-cell number-columns-spanned="8" font-size="4pt"> <fo:block> <fo:leader leader-pattern="space" leader-length="0.0pt"/>  </fo:block></fo:table-cell> </fo:table-row>

		<!-- display deliver to details -->
		<xsl:for-each select="DISTRIBUTIONS/DISTRIBUTIONS_ROW">
		<xsl:if test="../../DROP_SHIP_FLAG !='' or REQUESTER_DELIVER_FIRST_NAME !='' ">
			<fo:table-row>
			<fo:table-cell > <fo:block> <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell>
			<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="table.cell6">  <fo:block xsl:use-attribute-sets="form_data"> 
			<fo:table>
			<fo:table-column column-width="4mm"/><fo:table-column column-width="20mm"/> <fo:table-column column-width="150mm"/>
			<fo:table-body> 
				<fo:table-row> 
				<fo:table-cell > <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell>
				<fo:table-cell > <fo:block>
					<xsl:if test="DELIVER_TO_PERSON_ID !=''"> <!-- bug#3594831: Deliver To message will not displayed if id is null -->
					<xsl:if test="$DisplayBoilerPlate ">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_DELIVER_TO_LOCATION'][1]/TEXT"/>
					</xsl:if> 
					</xsl:if> <!-- bug#3594831: end -->
				</fo:block> </fo:table-cell>
			
				<fo:table-cell>  <fo:block>
					<!-- start of displaying deliver to details -->
					<xsl:if test="DELIVER_TO_PERSON_ID !=''"> <!-- bug#3594831: Deliver To message will not displayed if id is null -->
						

					<xsl:choose>
						<!-- <xsl:when test="$VENDOR_COUNTRY_CHK = 'TH' or $VENDOR_COUNTRY_CHK = 'Thailand'"> -->
						<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
							<xsl:value-of select="REQUESTER_THAI_NAME"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="NAME_TEMPLATE">
							<xsl:with-param name="FIRST_NAME" select="REQUESTER_DELIVER_FIRST_NAME"/>
							<xsl:with-param name="LAST_NAME" select="REQUESTER_DELIVER_LAST_NAME"/>
							<xsl:with-param name="TITLE" select="REQUESTER_DELIVER_TITLE"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>


						
						<xsl:if test="QUANTITY_ORDERED !=''"> 
							<fo:leader leader-pattern="space" leader-length="2.0pt"/>(<xsl:value-of select="QUANTITY_ORDERED"/>)
						</xsl:if>
						<fo:block/>

					
					<xsl:text>Email: </xsl:text> <xsl:value-of select="EMAIL_ADDRESS"/> <fo:block/>
					<xsl:text>Tel No.: </xsl:text>
					<xsl:value-of select="WORK_TELEPHONE"/> <fo:block/>
					<xsl:text>PR No.: </xsl:text> <xsl:value-of select="PR_NNUMBER"/> <fo:block/>
					<xsl:text>Project Code : </xsl:text> <xsl:value-of select="PROJECT_CODE"/> <fo:block/>
					<xsl:text>Requester Location: </xsl:text> <xsl:value-of select="LOCATION_CODE"/> <fo:block/>
					<xsl:text>Department: </xsl:text> <xsl:value-of select="DEPARTMENT_CODE"/> <fo:block/>
					<xsl:text>Division: </xsl:text> <xsl:value-of select="DIVISION_CODE"/> <fo:block/>
					<xsl:text>Group: </xsl:text> <xsl:value-of select="GROUP_CODE"/> <fo:block/>
		
					<!-- end of deliver details -->
					</xsl:if>  <!-- bug#3594831: end -->

					<!-- start of drop ship -->
					<xsl:if test="../../DROP_SHIP_FLAG ='Y'">
						<!--shipping method -->
						<xsl:if test="$DisplayBoilerPlate ">
							<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_SHIP_METHOD'][1]/TEXT"/>
						</xsl:if> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="../../SHIPPING_METHOD"/> <fo:block/>	
						<!-- shipping instructions -->
						<xsl:if test="$DisplayBoilerPlate ">
							<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_SHIPPING_INSTRUCTION'][1]/TEXT"/>
						</xsl:if> <fo:leader leader-pattern="space" leader-length="2.0pt"/>  <xsl:value-of select="../../SHIPPING_INSTRUCTIONS"/> <fo:block/>
						<!-- packing instructions -->
						<xsl:if test="$DisplayBoilerPlate ">
							<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_PACKING_INSTRUCTION'][1]/TEXT"/>
						</xsl:if> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="../../PACKING_INSTRUCTIONS"/> <fo:block/>
						
						<!-- customer po number, shipment number and line number -->
						<xsl:if test="$DisplayBoilerPlate ">
							<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_CUST_PO_NUMBER'][1]/TEXT"/>
						</xsl:if> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="../../CUSTOMER_PO_NUM"/>
						<fo:leader leader-pattern="space" leader-length="2.0pt"/>
						<xsl:if test="$DisplayBoilerPlate ">
							<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_LINE_NUMBER'][1]/TEXT"/>
						</xsl:if><fo:leader leader-pattern="space" leader-length="2.0pt"/><xsl:value-of select="../../CUSTOMER_PO_LINE_NUM"/>
						<fo:leader leader-pattern="space" leader-length="2.0pt"/>
						<xsl:if test="$DisplayBoilerPlate ">
							<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_SHIP_NUMBER'][1]/TEXT"/>
						</xsl:if><fo:leader leader-pattern="space" leader-length="2.0pt"/><xsl:value-of select="../../CUSTOMER_PO_SHIPMENT_NUM"/> <fo:block/>

						<!-- customer product instructions -->

						<xsl:if test="$DisplayBoilerPlate ">
							<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_CUST_ITEM_DESC'][1]/TEXT"/>
						</xsl:if><fo:leader leader-pattern="space" leader-length="2.0pt"/><xsl:value-of select="../../CUSTOMER_PRODUCT_DESC"/>

					</xsl:if>

					<!-- end of drop ship -->
				</fo:block> </fo:table-cell>
				</fo:table-row>
			</fo:table-body></fo:table>
			</fo:block> </fo:table-cell>
			</fo:table-row>
		</xsl:if>
		</xsl:for-each> 	<!-- end of distributions if -->
		
		</xsl:for-each> <!-- end of shipment if -->
		<fo:table-row> <fo:table-cell number-columns-spanned="8"> <fo:block > <fo:leader leader-pattern="space" leader-length="2.0pt"/> </fo:block>	</fo:table-cell> </fo:table-row>
	</xsl:if> <!-- end of amount line type -->


	
	<!-- xsl for displaying the Rate Based Temp Labor Details -->
	<xsl:if test = "LINE_TYPE = 'RATE' and PURCHASE_BASIS = 'TEMP LABOR'">		
		
		<fo:table-row  >
			<fo:table-cell xsl:use-attribute-sets="table.cell4"> <fo:block xsl:use-attribute-sets="form_data3"> <xsl:value-of select="LINE_NUM"/> </fo:block> </fo:table-cell>
			<fo:table-cell  xsl:use-attribute-sets="table.cell6">  <fo:block xsl:use-attribute-sets="form_data"> 
				<!-- Supplier Item number -->
				<xsl:if test="VENDOR_PRODUCT_NUM !=''">
					<xsl:if test="$DisplayBoilerPlate ">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_SUPPLIER_ITEM'][1]/TEXT"/>
					</xsl:if> 
				<fo:leader leader-pattern="space" leader-length="1.0pt"/> <xsl:value-of select="VENDOR_PRODUCT_NUM"/> <fo:block/> </xsl:if>
				<xsl:if test="JOB_NAME !=''">
					<xsl:value-of select="JOB_NAME"/> <fo:block/>
				</xsl:if>
				<xsl:value-of select="ITEM_DESCRIPTION"/> 
			</fo:block> </fo:table-cell>
			<fo:table-cell > <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block>	</fo:table-cell>
			<fo:table-cell > <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block>	</fo:table-cell>
			<fo:table-cell  xsl:use-attribute-sets="table.cell6">  <fo:block xsl:use-attribute-sets="form_data"> 
				<xsl:value-of select="UNIT_MEAS_LOOKUP_CODE"/>
			</fo:block> </fo:table-cell>
			<fo:table-cell  xsl:use-attribute-sets="table.cell5">  <fo:block xsl:use-attribute-sets="form_data1"> 
				<xsl:value-of select="UNIT_PRICE"/>
			</fo:block> </fo:table-cell>
			<fo:table-cell  xsl:use-attribute-sets="table.cell6">  <fo:block xsl:use-attribute-sets="form_data"> 
                                <!-- bug#3836856: retrieved the taxable flag value from shipments -->
				<xsl:value-of select="LINE_LOCATIONS/LINE_LOCATIONS_ROW/TAXABLE_FLAG"/>
			</fo:block> </fo:table-cell>
			<fo:table-cell  xsl:use-attribute-sets="table.cell5">  <fo:block xsl:use-attribute-sets="form_data1"> 
				<xsl:value-of select="LINE_AMOUNT"/>
			</fo:block> </fo:table-cell>

		</fo:table-row>

		
		<!-- AP@IS ES -->
		<fo:table-row>
			<fo:table-cell>
				<fo:block>
					<fo:leader leader-pattern="space" leader-length="1.0pt" /> 
				</fo:block>
			</fo:table-cell>
			<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="table.cell6">
				<fo:block xsl:use-attribute-sets="form_data">
					<xsl:text>Inventory Org: </xsl:text> <xsl:value-of select="$LINES_ROOT_OBJ/GLOBAL_ATTRIBUTE20"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		<!-- end of AP@IS ES -->
		<!-- dTAC: Start Section -->
		<xsl:if test="$BPA_CHECK = 'Y'">
		<fo:table-row>
			<fo:table-cell>
				<fo:block>
					<fo:leader leader-pattern="space" leader-length="1.0pt" /> 
				</fo:block>
			</fo:table-cell>
			<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="table.cell6">
				<xsl:choose>
					<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
						<fo:block xsl:use-attribute-sets="form_data">
							<xsl:value-of select="$LINES_ROOT_OBJ/LINE_ORI_UNIT_PRICE_TH"/>
						</fo:block>
					</xsl:when>
					<xsl:otherwise>
						<fo:block xsl:use-attribute-sets="form_data">
							<xsl:value-of select="$LINES_ROOT_OBJ/LINE_ORI_UNIT_PRICE_EN"/>
						</fo:block>
					</xsl:otherwise>
				</xsl:choose>
			</fo:table-cell>
		</fo:table-row>
		</xsl:if>
		<!-- dTAC: End Section -->

		<!-- Bug#3999145: Added the condition to identify long text line attachments are there for the current line -->
        <!-- <xsl:if test="$PSA or $vendor_note or $ga_flag or $short_attachments or $line_long_attach !='N'"> -->
		<xsl:if test="$PSA or $vendor_note or $ga_flag or $short_attachments or $long_attachments ">
		<fo:table-row>
			<fo:table-cell number-columns-spanned="7">
				<fo:block> <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block>
			</fo:table-cell>
		</fo:table-row>
		<fo:table-row >
			<fo:table-cell > <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell>
			<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="table.cell6"> 
			<fo:block xsl:use-attribute-sets="form_data">

				<!-- to display the charge Account -->
				<xsl:if test="$charge_account">
					<xsl:if test="$DisplayBoilerPlate">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_CHARGE_ACCOUNT'][1]/TEXT"/>
					</xsl:if>
					<xsl:choose>
					<xsl:when test="$distribution_count =1">
						<xsl:value-of select="$DISTRIBUTIONS_ROOT_OBJ/CHARGE_ACCOUNT"/> <fo:block/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_WF_NOTIF_MULTIPLE'][1]/TEXT"/> <fo:block/>
					</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<!-- end of charge account -->
				
				
				<!-- Vendor Notes -->
				<xsl:if test="$vendor_note">
					<xsl:value-of select="NOTE_TO_VENDOR"/> <fo:block/>
				</xsl:if>
				<!-- end of note to vendor -->

				<!-- for lines short text -->
				<xsl:for-each select="LINE_SHORT_TEXT/LINE_SHORT_TEXT_ROW">	
					<xsl:value-of select ="SHORT_TEXT"/> <fo:block/>
				</xsl:for-each>

				<!-- for long text -->
				<xsl:for-each select="$LINE_LONG_ATTACHMENTS_ROOT_OBJ">
					<xsl:if test="$lineID = .">
						<xsl:variable name="line" select="position()" />
						<xsl:value-of select="../TEXT[$line]"/>	<fo:block/> 	
					</xsl:if>
				</xsl:for-each>

				<!-- Global agreement value -->
				<xsl:if test="$ga_flag">
					<xsl:if test="$DisplayBoilerPlate">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_LINE_REF_BPA'][1]/TEXT"/>
					</xsl:if>
					<fo:leader leader-pattern="space" leader-length="1.0pt"/> <xsl:value-of select="SEGMENT1"/> <fo:block/>
				</xsl:if>
				<!-- end of global agreement flag -->

			</fo:block> 
			</fo:table-cell>
		</fo:table-row>
		</xsl:if>
				
		<fo:table-row>
			<fo:table-cell number-columns-spanned="8">
				<fo:block > <fo:leader leader-pattern="space" leader-length="2.0pt"/> </fo:block>
			</fo:table-cell>
		</fo:table-row>
		<fo:table-row >
			<fo:table-cell > <fo:block >  </fo:block> </fo:table-cell>
			<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="table.cell6">  <fo:block xsl:use-attribute-sets="form_data"> 
				<xsl:if test="CONTRACTOR_LAST_NAME != '' or CONTRACTOR_FIRST_NAME !=''">
				<xsl:if test="$DisplayBoilerPlate ">
					<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_CONTRACTOR_NAME'][1]/TEXT"/>
				</xsl:if> <fo:leader leader-pattern="space" leader-length="2.0pt"/>
				<xsl:call-template name="NAME_TEMPLATE">
				<xsl:with-param name="FIRST_NAME" select="CONTRACTOR_FIRST_NAME"/>
				<xsl:with-param name="LAST_NAME" select="CONTRACTOR_LAST_NAME"/>
				<xsl:with-param name="TITLE" select="CONTRACTOR_TITLE"/>
				</xsl:call-template> <fo:block/>
				</xsl:if>
				
				<!-- Start Date -->
				<fo:leader leader-pattern="space" leader-length="29.0pt"/>
				<xsl:if test="$DisplayBoilerPlate ">
					<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_START_DATE'][1]/TEXT"/>
				</xsl:if> <fo:leader leader-pattern="space" leader-length="2.0pt"/>
				<xsl:value-of select="substring(START_DATE,1,11)"/> <fo:block/>

				<!-- end date -->
				<xsl:if test="EXPIRATION_DATE !=''">
				<fo:leader leader-pattern="space" leader-length="32.0pt"/>
				<xsl:if test="$DisplayBoilerPlate ">
					<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_END_DATE'][1]/TEXT"/>
				</xsl:if><fo:leader leader-pattern="space" leader-length="2.0pt"/>
				<xsl:value-of select="substring(EXPIRATION_DATE,1,11)"/> <fo:block/>
				</xsl:if>

				<!-- Price differentials block -->		
				<xsl:if test="count(PRICE_DIFF/PRICE_DIFF_ROW/PRICE_TYPE) &gt; 0">
				<fo:leader leader-pattern="space" leader-length="10.0pt"/>
					<xsl:if test="$DisplayBoilerPlate ">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_PRICE_DIFFERENTIALS'][1]/TEXT"/>
					</xsl:if> <fo:block/>
				<xsl:for-each select="PRICE_DIFF/PRICE_DIFF_ROW">
					<fo:leader leader-pattern="space" leader-length="10.0pt"/>
					<xsl:value-of select="PRICE_TYPE"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="MULTIPLIER"/> <fo:block/>
				</xsl:for-each>

				</xsl:if>

			</fo:block>
			</fo:table-cell>
		</fo:table-row>
		

		<!-- display canceled details -->
		<xsl:if test="CANCEL_FLAG='Y'">
		<fo:table-row> <fo:table-cell number-columns-spanned="8"> <fo:block > <fo:leader leader-pattern="space" leader-length="2.0pt"/> </fo:block>	</fo:table-cell> </fo:table-row>
		<fo:table-row  keep-together.within-page="always">
			<fo:table-cell > <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell>
			<fo:table-cell number-columns-spanned="7">  <fo:block> 
				<fo:table>
				<fo:table-column column-width="70mm"/>
				<fo:table-body> <fo:table-row> <fo:table-cell xsl:use-attribute-sets="table.cell7">  <fo:block xsl:use-attribute-sets="form_data2">
				<fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_LINE_CANCELED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="substring(CANCEL_DATE,1,11)"/> <fo:block/>
				<fo:leader leader-pattern="space" leader-length="24.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_ORIGINAL_AMOUNT_ORDERED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="TOTAL_LINE_AMOUNT"/> <fo:block/>
				<fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_AMOUNT_CANCELED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="CANCELED_AMOUNT"/> <fo:block/>
				</fo:block> </fo:table-cell> </fo:table-row> </fo:table-body>
				</fo:table>
			</fo:block> </fo:table-cell>
		</fo:table-row>
		</xsl:if>
		<!-- end of canceled details -->

		
		<!-- start of shipment details -->
		<xsl:for-each select="LINE_LOCATIONS/LINE_LOCATIONS_ROW">

                <!--Bug#3999145: Holds the current line location ID -->
                <xsl:variable name="lineLocID" select="LINE_LOCATION_ID"/>

		<fo:table-row> <fo:table-cell number-columns-spanned="8"> <fo:block > <fo:leader leader-pattern="space" leader-length="2.0pt"/> </fo:block>	</fo:table-cell> </fo:table-row>

		<fo:table-row> <fo:table-cell number-columns-spanned="7"> <fo:block> </fo:block> </fo:table-cell> </fo:table-row>
		<fo:table-row  >
			<fo:table-cell > <fo:block > </fo:block> </fo:table-cell>
			<fo:table-cell xsl:use-attribute-sets="table.cell6" number-columns-spanned="7">  <fo:block xsl:use-attribute-sets="form_data"> 
			<xsl:if test="$DisplayBoilerPlate ">
				<fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_SHIP_TO'][1]/TEXT"/> <fo:block/>
			</xsl:if><fo:block/>

			<xsl:choose>
				<xsl:when test="$print_multiple ='Y'">
					<fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of select="SHIP_TO_ADDRESS_LINE1"/>
					<xsl:if test="SHIP_TO_ADDRESS_LINE2 !=''"> <fo:block/><fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of select="SHIP_TO_ADDRESS_LINE2"/> </xsl:if>
					<xsl:if test="SHIP_TO_ADDRESS_LINE3 !=''"> <fo:block/> <fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of select="SHIP_TO_ADDRESS_LINE3"/> </xsl:if>
					<xsl:if test="SHIP_TO_ADDRESS_INFO !=''"> <fo:block/> <fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of select="SHIP_TO_ADDRESS_INFO"/> </xsl:if>
					<xsl:if test="SHIP_TO_COUNTRY !=''"> <fo:block/> <fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of select="SHIP_TO_COUNTRY"/>	</xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_USE_SHIP_ADDRESS_TOP'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <fo:page-number-citation ref-id="page_ref" />
				</xsl:otherwise>
			</xsl:choose> <fo:block/>
			<!-- for lines short text -->
                        <xsl:for-each select="LINE_LOC_SHORT_TEXT/LINE_LOC_SHORT_TEXT_ROW">

				<!--Bug#4088207: Call LINE_LOCATION_ATTACHMENT_TEMP template to display the attachment -->
                                <xsl:call-template name="LINE_LOCATION_ATTACHMENT_TEMP">
                                        <xsl:with-param name="ATTACHMENT_TEXT" select="SHORT_TEXT"/>
                                </xsl:call-template>

			</xsl:for-each>

			<!-- for long text -->
                        <!--Bug#3999145: Modified the test condition to display the shipment attachments for the current shipment -->
                     	<xsl:for-each select="$SHIPMENT_ATTACHMENTS_ROOT_OBJ">
				<xsl:if test="$lineLocID = .">
					<xsl:variable name="line" select="position()" />

					 <!--Bug#4088207: Call LINE_LOCATION_ATTACHMENT_TEMP template to display the attachment -->
                                         <xsl:call-template name="LINE_LOCATION_ATTACHMENT_TEMP">
                                                <xsl:with-param name="ATTACHMENT_TEXT" select="../LONG_TEXT[$line]"/>
                                         </xsl:call-template>
                                        
				</xsl:if>
			</xsl:for-each>

			</fo:block> </fo:table-cell>
		</fo:table-row>
		
		
		<!-- display canceled details -->
		<xsl:if test="CANCEL_FLAG='Y'">
		<fo:table-row> <fo:table-cell number-columns-spanned="8"> <fo:block > <fo:leader leader-pattern="space" leader-length="2.0pt"/> </fo:block>	</fo:table-cell> </fo:table-row>
		<fo:table-row  keep-together.within-page="always">
			<fo:table-cell > <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell>
			<fo:table-cell number-columns-spanned="7">  <fo:block> 
				<fo:table>
				<fo:table-column column-width="5mm"/> <fo:table-column column-width="78mm"/>
				<fo:table-body> <fo:table-row> 
				<fo:table-cell> <fo:block> </fo:block> </fo:table-cell>
				<fo:table-cell xsl:use-attribute-sets="table.cell7">  <fo:block xsl:use-attribute-sets="form_data2">
				<fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_SHIPMENT_CANCELED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="substring(CANCEL_DATE,1,11)"/> <fo:block/>
				<fo:leader leader-pattern="space" leader-length="24.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_ORIGINAL_AMOUNT_ORDERED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="TOTAL_SHIPMENT_AMOUNT"/> <fo:block/>
				<fo:leader leader-pattern="space" leader-length="6.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_AMOUNT_CANCELED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="AMOUNT_CANCELLED"/> <fo:block/>
				</fo:block> </fo:table-cell> </fo:table-row> </fo:table-body>
				</fo:table>
			</fo:block> </fo:table-cell>
		</fo:table-row>
		<fo:table-row> <fo:table-cell number-columns-spanned="8"  font-size="8pt"> <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block>	</fo:table-cell> </fo:table-row>
		</xsl:if>
		<!-- end of canceled details -->

		<fo:table-row> <fo:table-cell number-columns-spanned="8" font-size="4pt"> <fo:block> <fo:leader leader-pattern="space" leader-length="0.0pt"/>  </fo:block></fo:table-cell> </fo:table-row>
		<!-- display deliver to details -->
		<xsl:for-each select="DISTRIBUTIONS/DISTRIBUTIONS_ROW">
		<xsl:if test="REQUESTER_DELIVER_FIRST_NAME !='' ">
			<fo:table-row>
			<fo:table-cell > <fo:block> <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell>
			<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="table.cell6">  <fo:block xsl:use-attribute-sets="form_data"> 
			<fo:table>
			<fo:table-column column-width="4mm"/><fo:table-column column-width="20mm"/> <fo:table-column column-width="100mm"/>
			<fo:table-body> 
				<fo:table-row> 
				<fo:table-cell > <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell>
				<fo:table-cell > <fo:block>
					<xsl:if test="$DisplayBoilerPlate ">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_DELIVER_TO_LOCATION'][1]/TEXT"/>
					</xsl:if> 
				</fo:block> </fo:table-cell>
			
				<fo:table-cell>  <fo:block>
					<!-- start of displaying deliver to details -->
					<xsl:call-template name="NAME_TEMPLATE">
					<xsl:with-param name="FIRST_NAME" select="REQUESTER_DELIVER_FIRST_NAME"/>
					<xsl:with-param name="LAST_NAME" select="REQUESTER_DELIVER_LAST_NAME"/>
					<xsl:with-param name="TITLE" select="REQUESTER_DELIVER_TITLE"/>
					</xsl:call-template>
			
					<fo:leader leader-pattern="space" leader-length="2.0pt"/> 
					<fo:block/> <xsl:value-of select="EMAIL_ADDRESS"/>
					<!-- end of deliver details -->
					
				</fo:block> </fo:table-cell>
				</fo:table-row>
			</fo:table-body></fo:table>
			</fo:block> </fo:table-cell>
			</fo:table-row>
		</xsl:if>
		</xsl:for-each> 	<!-- end of distributions if -->
		
		</xsl:for-each> <!-- end of shipment if -->
		<fo:table-row> <fo:table-cell number-columns-spanned="8"> <fo:block > <fo:leader leader-pattern="space" leader-length="2.0pt"/> </fo:block>	</fo:table-cell> </fo:table-row>
	</xsl:if>


	<!-- Displays Fixed Price Labor Line -->
	<xsl:if test = "LINE_TYPE = 'FIXED PRICE' and PURCHASE_BASIS = 'TEMP LABOR'">		
				
		<fo:table-row  >
			<fo:table-cell xsl:use-attribute-sets="table.cell4"> <fo:block xsl:use-attribute-sets="form_data3"> <xsl:value-of select="LINE_NUM"/> </fo:block> </fo:table-cell>
			<fo:table-cell  xsl:use-attribute-sets="table.cell6">  <fo:block xsl:use-attribute-sets="form_data"> 
				<!-- Supplier Item number -->
				<xsl:if test="VENDOR_PRODUCT_NUM !=''">
					<xsl:if test="$DisplayBoilerPlate ">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_SUPPLIER_ITEM'][1]/TEXT"/>
					</xsl:if> 
				<fo:leader leader-pattern="space" leader-length="1.0pt"/> <xsl:value-of select="VENDOR_PRODUCT_NUM"/> <fo:block/> </xsl:if>
				<xsl:value-of select="JOB_NAME"/> <fo:block/>
				<xsl:value-of select="ITEM_DESCRIPTION"/> 
			</fo:block> </fo:table-cell>
			<fo:table-cell > <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block>	</fo:table-cell>
			<fo:table-cell > <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block>	</fo:table-cell>
			<fo:table-cell > <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block>	</fo:table-cell>
			<fo:table-cell > <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block>	</fo:table-cell>
			<fo:table-cell  xsl:use-attribute-sets="table.cell6">  <fo:block xsl:use-attribute-sets="form_data"> 
                                <!-- bug#3836856: retrieved the taxable flag value from shipments -->
				<xsl:value-of select="LINE_LOCATIONS/LINE_LOCATIONS_ROW/TAXABLE_FLAG"/>
			</fo:block> </fo:table-cell>
			<fo:table-cell  xsl:use-attribute-sets="table.cell5">  <fo:block xsl:use-attribute-sets="form_data1"> 
				<xsl:value-of select="LINE_AMOUNT"/>
			</fo:block> </fo:table-cell>

		</fo:table-row>

		<!-- AP@IS ES -->
		<fo:table-row>
			<fo:table-cell>
				<fo:block>
					<fo:leader leader-pattern="space" leader-length="1.0pt" /> 
				</fo:block>
			</fo:table-cell>
			<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="table.cell6">
				<fo:block xsl:use-attribute-sets="form_data">
					<xsl:text>Inventory Org: </xsl:text> <xsl:value-of select="$LINES_ROOT_OBJ/GLOBAL_ATTRIBUTE20"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		<!-- end of AP@IS ES -->
		<!-- dTAC: Start Section -->
		<xsl:if test="$BPA_CHECK = 'Y'">
		<fo:table-row>
			<fo:table-cell>
				<fo:block>
					<fo:leader leader-pattern="space" leader-length="1.0pt" /> 
				</fo:block>
			</fo:table-cell>
			<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="table.cell6">
				<xsl:choose>
					<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
						<fo:block xsl:use-attribute-sets="form_data">
							<xsl:value-of select="$LINES_ROOT_OBJ/LINE_ORI_UNIT_PRICE_TH"/>
						</fo:block>
					</xsl:when>
					<xsl:otherwise>
						<fo:block xsl:use-attribute-sets="form_data">
							<xsl:value-of select="$LINES_ROOT_OBJ/LINE_ORI_UNIT_PRICE_EN"/>
						</fo:block>
					</xsl:otherwise>
				</xsl:choose>
			</fo:table-cell>
		</fo:table-row>
		</xsl:if>
		<!-- dTAC: End Section -->

		<!-- Bug#3999145: Added the condition to identify long text line attachments are there for the current line -->
        <!-- <xsl:if test="$PSA or $vendor_note or $ga_flag or $short_attachments or $line_long_attach !='N'">  -->
		<xsl:if test="$PSA or $vendor_note or $ga_flag or $short_attachments or $long_attachments">
		<fo:table-row> <fo:table-cell number-columns-spanned="8"> <fo:block > <fo:leader leader-pattern="space" leader-length="2.0pt"/> </fo:block>	</fo:table-cell> </fo:table-row>

		<fo:table-row >
			<fo:table-cell > <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell>
			<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="table.cell6"> 
			<fo:block xsl:use-attribute-sets="form_data">

				<!-- to display the charge Account -->
				<xsl:if test="$charge_account">
					<xsl:if test="$DisplayBoilerPlate">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_CHARGE_ACCOUNT'][1]/TEXT"/>
					</xsl:if>
					<xsl:choose>
					<xsl:when test="$distribution_count =1">
						<xsl:value-of select="$DISTRIBUTIONS_ROOT_OBJ/CHARGE_ACCOUNT"/> <fo:block/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_WF_NOTIF_MULTIPLE'][1]/TEXT"/> <fo:block/>
					</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<!-- end of charge account -->
				
				
				<!-- Vendor Notes -->
				<xsl:if test="$vendor_note">
					<xsl:value-of select="NOTE_TO_VENDOR"/> <fo:block/>
				</xsl:if>
				<!-- end of note to vendor -->

				<!-- for lines short text -->
				<xsl:for-each select="LINE_SHORT_TEXT/LINE_SHORT_TEXT_ROW">	
					<xsl:value-of select ="SHORT_TEXT"/> <fo:block/>
				</xsl:for-each>

				<!-- for long text -->
				<xsl:for-each select="$LINE_LONG_ATTACHMENTS_ROOT_OBJ">
					<xsl:if test="$lineID = .">
						<xsl:variable name="line" select="position()" />
						<xsl:value-of select="../TEXT[$line]"/>	<fo:block/> 	
					</xsl:if>
				</xsl:for-each>

				<!-- Global agreement value -->
				<xsl:if test="$ga_flag">
					<xsl:if test="$DisplayBoilerPlate">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_LINE_REF_BPA'][1]/TEXT"/>
					</xsl:if>
					<fo:leader leader-pattern="space" leader-length="1.0pt"/> <xsl:value-of select="SEGMENT1"/> <fo:block/>
				</xsl:if>
				<!-- end of global agreement flag -->

			</fo:block> 
			</fo:table-cell>
		</fo:table-row>
		</xsl:if>
		
		<fo:table-row> <fo:table-cell number-columns-spanned="8"> <fo:block> <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell> </fo:table-row>
		<fo:table-row >
			<fo:table-cell > <fo:block >  </fo:block> </fo:table-cell>
			<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="table.cell6">  <fo:block xsl:use-attribute-sets="form_data"> 
				<xsl:if test="CONTRACTOR_LAST_NAME != '' or CONTRACTOR_FIRST_NAME !=''">
				<xsl:if test="$DisplayBoilerPlate ">
					<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_CONTRACTOR_NAME'][1]/TEXT"/>
				</xsl:if> <fo:leader leader-pattern="space" leader-length="2.0pt"/>
				<xsl:call-template name="NAME_TEMPLATE">
				<xsl:with-param name="FIRST_NAME" select="CONTRACTOR_FIRST_NAME"/>
				<xsl:with-param name="LAST_NAME" select="CONTRACTOR_LAST_NAME"/>
				<xsl:with-param name="TITLE" select="CONTRACTOR_TITLE"/>
				</xsl:call-template> <fo:block/>
				</xsl:if>
				
				<!-- Start Date -->
				<fo:leader leader-pattern="space" leader-length="29.0pt"/>
				<xsl:if test="$DisplayBoilerPlate ">
					<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_START_DATE'][1]/TEXT"/>
				</xsl:if> <fo:leader leader-pattern="space" leader-length="2.0pt"/>
				<xsl:value-of select="substring(START_DATE,1,11)"/> <fo:block/>

				<!-- end date -->
				<xsl:if test="EXPIRATION_DATE !=''">
				<fo:leader leader-pattern="space" leader-length="32.0pt"/>
				<xsl:if test="$DisplayBoilerPlate ">
					<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_END_DATE'][1]/TEXT"/>
				</xsl:if><fo:leader leader-pattern="space" leader-length="2.0pt"/>
				<xsl:value-of select="substring(EXPIRATION_DATE,1,11)"/>
				</xsl:if>
			</fo:block> </fo:table-cell>
		</fo:table-row>

		<!-- display canceled details -->
		<xsl:if test="CANCEL_FLAG='Y'">
		<fo:table-row> <fo:table-cell number-columns-spanned="8"> <fo:block> <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell> </fo:table-row>
		<fo:table-row  keep-together.within-page="always">
			<fo:table-cell > <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell>
			<fo:table-cell number-columns-spanned="7">  <fo:block> 
				<fo:table>
				<fo:table-column column-width="70mm"/>
				<fo:table-body> <fo:table-row> <fo:table-cell xsl:use-attribute-sets="table.cell7">  <fo:block xsl:use-attribute-sets="form_data2">
				<fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_LINE_CANCELED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="substring(CANCEL_DATE,1,11)"/> <fo:block/>
				<fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_ORIGINAL_AMOUNT_ORDERED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="TOTAL_LINE_AMOUNT"/> <fo:block/>
				<fo:leader leader-pattern="space" leader-length="24.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_AMOUNT_CANCELED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="CANCELED_AMOUNT"/> <fo:block/>
				</fo:block> </fo:table-cell> </fo:table-row> </fo:table-body>
				</fo:table>
			</fo:block> </fo:table-cell>
		</fo:table-row>
		</xsl:if>

		<!-- end of canceled details -->

		<!-- start of shipment details -->
		<xsl:for-each select="LINE_LOCATIONS/LINE_LOCATIONS_ROW">

                <!--Bug#3999145: Holds the current line location ID -->
                <xsl:variable name="lineLocID" select="LINE_LOCATION_ID"/>

		<fo:table-row> <fo:table-cell number-columns-spanned="8"> <fo:block> <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell> </fo:table-row>
		
		<fo:table-row  >
			<fo:table-cell > <fo:block >  </fo:block> </fo:table-cell>
			<fo:table-cell xsl:use-attribute-sets="table.cell6" number-columns-spanned="7">  <fo:block xsl:use-attribute-sets="form_data"> 
			<xsl:if test="$DisplayBoilerPlate ">
				<fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_SHIP_TO'][1]/TEXT"/> <fo:block/>
			</xsl:if> <fo:block/>
			<xsl:choose>
				<xsl:when test="$print_multiple ='Y'">
					<fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of select="SHIP_TO_ADDRESS_LINE1"/>
					<xsl:if test="SHIP_TO_ADDRESS_LINE2 !=''"> <fo:block/><fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of select="SHIP_TO_ADDRESS_LINE2"/> </xsl:if>
					<xsl:if test="SHIP_TO_ADDRESS_LINE3 !=''"> <fo:block/> <fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of select="SHIP_TO_ADDRESS_LINE3"/> </xsl:if>
					<xsl:if test="SHIP_TO_ADDRESS_INFO !=''"> <fo:block/> <fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of select="SHIP_TO_ADDRESS_INFO"/> </xsl:if>
					<xsl:if test="SHIP_TO_COUNTRY !=''"> <fo:block/> <fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of select="SHIP_TO_COUNTRY"/>	</xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_USE_SHIP_ADDRESS_TOP'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <fo:page-number-citation ref-id="page_ref" />
				</xsl:otherwise> 
			</xsl:choose><fo:block/>
			<!-- for lines short text -->
                        <xsl:for-each select="LINE_LOC_SHORT_TEXT/LINE_LOC_SHORT_TEXT_ROW">
				<!--Bug#4088207: Call LINE_LOCATION_ATTACHMENT_TEMP template to display the attachment -->
                                 <xsl:call-template name="LINE_LOCATION_ATTACHMENT_TEMP">
                                        <xsl:with-param name="ATTACHMENT_TEXT" select="SHORT_TEXT"/>
                                 </xsl:call-template>
			</xsl:for-each>

			<!-- for long text -->
                        <!--Bug#3999145: Modified the test condition to display the shipment attachments for the current shipment -->
                        <xsl:for-each select="$SHIPMENT_ATTACHMENTS_ROOT_OBJ">
				<xsl:if test="$lineLocID = .">
					<xsl:variable name="line" select="position()" />

					 <!--Bug#4088207: Call LINE_LOCATION_ATTACHMENT_TEMP template to display the attachment -->
                                         <xsl:call-template name="LINE_LOCATION_ATTACHMENT_TEMP">
                                                <xsl:with-param name="ATTACHMENT_TEXT" select="../LONG_TEXT[$line]"/>
                                         </xsl:call-template>

				</xsl:if>
			</xsl:for-each>


			</fo:block> </fo:table-cell>
			
		</fo:table-row>

		<!-- display canceled details -->
		<xsl:if test="CANCEL_FLAG='Y'">
		<fo:table-row> <fo:table-cell number-columns-spanned="8"> <fo:block> <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell> </fo:table-row>
		<fo:table-row  keep-together.within-page="always">
			<fo:table-cell > <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell>
			<fo:table-cell number-columns-spanned="7">  <fo:block> 
				<fo:table>
				<fo:table-column column-width="5mm"/> <fo:table-column column-width="78mm"/>
				<fo:table-body> <fo:table-row> 
				<fo:table-cell> <fo:block> </fo:block> </fo:table-cell>
				<fo:table-cell xsl:use-attribute-sets="table.cell7">  <fo:block xsl:use-attribute-sets="form_data2">
				<fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_SHIPMENT_CANCELED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="substring(CANCEL_DATE,1,11)"/> <fo:block/>
				<fo:leader leader-pattern="space" leader-length="20.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_ORIGINAL_AMOUNT_ORDERED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="TOTAL_SHIPMENT_AMOUNT"/> <fo:block/>
				<fo:leader leader-pattern="space" leader-length="34.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_AMOUNT_CANCELED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="AMOUNT_CANCELLED"/> <fo:block/>
				</fo:block> </fo:table-cell> </fo:table-row> </fo:table-body>
				</fo:table>
			</fo:block> </fo:table-cell>
		</fo:table-row>
		<fo:table-row> <fo:table-cell number-columns-spanned="8"  font-size="8pt"> <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block>	</fo:table-cell> </fo:table-row>
		</xsl:if>
		<!-- end of canceled details -->

		<fo:table-row> <fo:table-cell number-columns-spanned="8" font-size="4pt"> <fo:block> <fo:leader leader-pattern="space" leader-length="0.0pt"/>  </fo:block></fo:table-cell> </fo:table-row>
		<!-- display deliver to details -->
		<xsl:for-each select="DISTRIBUTIONS/DISTRIBUTIONS_ROW">
		<xsl:if test="REQUESTER_DELIVER_FIRST_NAME !='' ">
			<fo:table-row>
			<fo:table-cell > <fo:block> <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell>
			<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="table.cell6">  <fo:block xsl:use-attribute-sets="form_data"> 
			<fo:table>
			<fo:table-column column-width="4mm"/><fo:table-column column-width="20mm"/> <fo:table-column column-width="100mm"/>

			<fo:table-body> 
				<fo:table-row> 
				<fo:table-cell > <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell>
				<fo:table-cell > <fo:block>
					<xsl:if test="$DisplayBoilerPlate ">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_DELIVER_TO_LOCATION'][1]/TEXT"/>
					</xsl:if> 
				</fo:block> </fo:table-cell>
			
				<fo:table-cell>
				<fo:block>
					<!-- start of displaying deliver to details -->
					<xsl:call-template name="NAME_TEMPLATE">
					<xsl:with-param name="FIRST_NAME" select="REQUESTER_DELIVER_FIRST_NAME"/>
					<xsl:with-param name="LAST_NAME" select="REQUESTER_DELIVER_LAST_NAME"/>
					<xsl:with-param name="TITLE" select="REQUESTER_DELIVER_TITLE"/>
					</xsl:call-template>
					<xsl:if test="QUANTITY_ORDERED !=''"> 
						<fo:leader leader-pattern="space" leader-length="2.0pt"/>(<xsl:value-of select="QUANTITY_ORDERED"/>)
					</xsl:if>
					<fo:block/>
					
					<xsl:value-of select="EMAIL_ADDRESS"/>
					<!-- end of deliver details -->
					
				</fo:block>
				</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
			</fo:table>
			</fo:block> </fo:table-cell>
			</fo:table-row>
		</xsl:if>
		</xsl:for-each> 	<!-- end of distributions if -->
		</xsl:for-each> <!-- end of shipment if -->
		<fo:table-row> <fo:table-cell number-columns-spanned="8"> <fo:block > <fo:leader leader-pattern="space" leader-length="2.0pt"/> </fo:block>	</fo:table-cell> </fo:table-row>
	</xsl:if>

	<!-- For displaying Fixed price Lines -->
	<xsl:if test = "LINE_TYPE = 'FIXED PRICE' and PURCHASE_BASIS = 'SERVICES'">
		
		<fo:table-row  >
			<fo:table-cell xsl:use-attribute-sets="table.cell4"> <fo:block xsl:use-attribute-sets="form_data3"> <xsl:value-of select="LINE_NUM"/> </fo:block> </fo:table-cell>
			<fo:table-cell  xsl:use-attribute-sets="table.cell6">  <fo:block xsl:use-attribute-sets="form_data"> 
				<!-- Supplier Item number -->
				<xsl:if test="VENDOR_PRODUCT_NUM !=''">
					<xsl:if test="$DisplayBoilerPlate ">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_SUPPLIER_ITEM'][1]/TEXT"/>
					</xsl:if> 
				<fo:leader leader-pattern="space" leader-length="1.0pt"/> <xsl:value-of select="VENDOR_PRODUCT_NUM"/> <fo:block/> </xsl:if>
				<xsl:value-of select="ITEM_DESCRIPTION"/> 
			</fo:block> </fo:table-cell>
			<fo:table-cell xsl:use-attribute-sets="table.cell6"> <fo:block xsl:use-attribute-sets="form_data">
			<xsl:if test="$shipment_count = 1">
				<!-- Bug3670603: As per the bug Promised and need by date are added. -->
				<xsl:if test="LINE_LOCATIONS/LINE_LOCATIONS_ROW/PROMISED_DATE!=''">
				<fo:block/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_WF_NOTIF_PROMISED_DATE'][1]/TEXT"/>
				<fo:block/> <xsl:value-of select="LINE_LOCATIONS/LINE_LOCATIONS_ROW/PROMISED_DATE"/>
				</xsl:if>
				<xsl:if test="LINE_LOCATIONS/LINE_LOCATIONS_ROW/NEED_BY_DATE!=''">
				<fo:block/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_WF_NOTIF_NEEDBY_DATE'][1]/TEXT"/>
				<fo:block/> <xsl:value-of select="LINE_LOCATIONS/LINE_LOCATIONS_ROW/NEED_BY_DATE"/>
				</xsl:if>
			</xsl:if>
			</fo:block> </fo:table-cell>
			<fo:table-cell text-align="right"> <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block>	</fo:table-cell>
			<fo:table-cell text-align="right"> <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block>	</fo:table-cell>
			<fo:table-cell text-align="right"> <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block>	</fo:table-cell>
			<fo:table-cell xsl:use-attribute-sets="table.cell6"> <fo:block xsl:use-attribute-sets="form_data">
			    <xsl:if test="$shipment_count = 1">
				<xsl:value-of  select="LINE_LOCATIONS/LINE_LOCATIONS_ROW/TAXABLE_FLAG"/>
			   </xsl:if>
			</fo:block> </fo:table-cell>
			<fo:table-cell  xsl:use-attribute-sets="table.cell5">  <fo:block xsl:use-attribute-sets="form_data1"> 
				<xsl:value-of select="LINE_AMOUNT"/>
			</fo:block> </fo:table-cell>

		</fo:table-row>

		<!-- AP@IS ES -->
		<fo:table-row>
			<fo:table-cell>
				<fo:block>
					<fo:leader leader-pattern="space" leader-length="1.0pt" /> 
				</fo:block>
			</fo:table-cell>
			<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="table.cell6">
				<fo:block xsl:use-attribute-sets="form_data">
					<xsl:text>Inventory Org: </xsl:text> <xsl:value-of select="$LINES_ROOT_OBJ/GLOBAL_ATTRIBUTE20"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		<!-- end of AP@IS ES -->
		<!-- dTAC: Start Section -->
		<xsl:if test="$BPA_CHECK = 'Y'">
		<fo:table-row>
			<fo:table-cell>
				<fo:block>
					<fo:leader leader-pattern="space" leader-length="1.0pt" /> 
				</fo:block>
			</fo:table-cell>
			<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="table.cell6">
				<xsl:choose>
					<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
						<fo:block xsl:use-attribute-sets="form_data">
							<xsl:value-of select="$LINES_ROOT_OBJ/LINE_ORI_UNIT_PRICE_TH"/>
						</fo:block>
					</xsl:when>
					<xsl:otherwise>
						<fo:block xsl:use-attribute-sets="form_data">
							<xsl:value-of select="$LINES_ROOT_OBJ/LINE_ORI_UNIT_PRICE_EN"/>
						</fo:block>
					</xsl:otherwise>
				</xsl:choose>
			</fo:table-cell>
		</fo:table-row>
		</xsl:if>
		<!-- dTAC: End Section -->

		<!-- Bug#3999145: Added the condition to identify long text line attachments are there for the current line -->
                <xsl:if test="$PSA or $un_num or $hz_class or $vendor_note or $ga_flag or $short_attachments or $long_attachments ">

		<fo:table-row> <fo:table-cell number-columns-spanned="7"> <fo:block> <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell> </fo:table-row>

		<fo:table-row >
			<fo:table-cell > <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell>
			<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="table.cell6"> 
			<fo:block xsl:use-attribute-sets="form_data">

				<!-- to display the charge Account -->
				<xsl:if test="$charge_account">
					<xsl:if test="$DisplayBoilerPlate">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_CHARGE_ACCOUNT'][1]/TEXT"/>
					</xsl:if>
					<xsl:choose>
					<xsl:when test="$distribution_count =1">
						<xsl:value-of select="$DISTRIBUTIONS_ROOT_OBJ/CHARGE_ACCOUNT"/> <fo:block/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_WF_NOTIF_MULTIPLE'][1]/TEXT"/> <fo:block/>
					</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<!-- end of charge account -->
				
				<!-- Un number row -->
				<xsl:if test="$un_num">
					<xsl:if test="$DisplayBoilerPlate">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_UN_NUMBER'][1]/TEXT"/>
					</xsl:if>
					<fo:leader leader-pattern="space" leader-length="1.0pt"/> <xsl:value-of select="UN_NUMBER"/> <fo:block/>
				</xsl:if>
				<!-- end of Un number row -->

				<!-- Hazard class -->
				<xsl:if test="$hz_class">
					<xsl:if test="$DisplayBoilerPlate">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_HAZARD_CLASS'][1]/TEXT"/>
					</xsl:if>
					<fo:leader leader-pattern="space" leader-length="1.0pt"/> <xsl:value-of select="HAZARD_CLASS"/> <fo:block/>
				</xsl:if>
				<!-- end of Hazard class -->

				<!-- Vendor Notes -->
				<xsl:if test="$vendor_note">
					<xsl:value-of select="NOTE_TO_VENDOR"/> <fo:block/>
				</xsl:if>
				<!-- end of note to vendor -->

				<!-- for lines short text -->
				<xsl:for-each select="LINE_SHORT_TEXT/LINE_SHORT_TEXT_ROW">	
					<xsl:value-of select ="SHORT_TEXT"/> <fo:block/>
				</xsl:for-each>

				<!-- for long text -->
				<xsl:for-each select="$LINE_LONG_ATTACHMENTS_ROOT_OBJ">
					<xsl:if test="$lineID = .">
						<xsl:variable name="line" select="position()" />
						<xsl:value-of select="../TEXT[$line]"/>	<fo:block/> 	
					</xsl:if>
				</xsl:for-each>

				<!-- Global agreement value -->
				<xsl:if test="$ga_flag">
					<xsl:if test="$DisplayBoilerPlate">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_LINE_REF_BPA'][1]/TEXT"/>
					</xsl:if>
					<fo:leader leader-pattern="space" leader-length="1.0pt"/> <xsl:value-of select="SEGMENT1"/> <fo:block/>
				</xsl:if>
				<!-- end of global agreement flag -->
					
			</fo:block> 
			</fo:table-cell>
		</fo:table-row>
		</xsl:if>

		<!-- display canceled details -->
		<xsl:if test="CANCEL_FLAG='Y'">
		<fo:table-row> <fo:table-cell number-columns-spanned="8"> <fo:block > <fo:leader leader-pattern="space" leader-length="2.0pt"/> </fo:block>	</fo:table-cell> </fo:table-row>
		<fo:table-row  keep-together.within-page="always">
			<fo:table-cell > <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell>
			<fo:table-cell number-columns-spanned="7">  <fo:block> 
				<fo:table>
				<fo:table-column column-width="70mm"/>
				<fo:table-body> <fo:table-row> <fo:table-cell xsl:use-attribute-sets="table.cell7">  <fo:block xsl:use-attribute-sets="form_data2">
				<fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_LINE_CANCELED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="substring(CANCEL_DATE,1,11)"/> <fo:block/>
				<fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_ORIGINAL_AMOUNT_ORDERED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="TOTAL_LINE_AMOUNT"/> <fo:block/>
				<fo:leader leader-pattern="space" leader-length="24.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_AMOUNT_CANCELED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="CANCELED_AMOUNT"/> <fo:block/>
				</fo:block> </fo:table-cell> </fo:table-row> </fo:table-body>
				</fo:table>
			</fo:block> </fo:table-cell>
		</fo:table-row>
		<fo:table-row> <fo:table-cell number-columns-spanned="8"  font-size="8pt"> <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block>	</fo:table-cell> </fo:table-row>
		</xsl:if>
		<!-- end of canceled details -->

		<fo:table-row> <fo:table-cell number-columns-spanned="8" font-size="4pt"> <fo:block> <fo:leader leader-pattern="space" leader-length="0.0pt"/>  </fo:block></fo:table-cell> </fo:table-row>

		<!-- start of shipment details -->
		<xsl:for-each select="LINE_LOCATIONS/LINE_LOCATIONS_ROW">

                <!--Bug#3999145: Holds the current line location ID -->
                <xsl:variable name="lineLocID" select="LINE_LOCATION_ID"/>

		<fo:table-row> <fo:table-cell number-columns-spanned="8"> <fo:block font-size="8pt"> <fo:leader leader-pattern="space" leader-length="2.0pt"/> </fo:block>	</fo:table-cell> </fo:table-row>

		<fo:table-row  >
			<fo:table-cell > <fo:block >  </fo:block> </fo:table-cell>
			<fo:table-cell xsl:use-attribute-sets="table.cell6">  <fo:block xsl:use-attribute-sets="form_data"> 
			<fo:table>
			<fo:table-column column-width="3.5mm"/> <fo:table-column column-width="34.5mm"/>
			<fo:table-body>
			<fo:table-row >
			<fo:table-cell > <fo:block>  </fo:block> </fo:table-cell>
			<fo:table-cell > <fo:block>  
			<xsl:if test="$DisplayBoilerPlate ">
				<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_SHIP_TO'][1]/TEXT"/> <fo:block/>
			</xsl:if>
			<!-- bug#3594831: added drop_ship ship to details -->
			<!-- Bug3670603: Address details should be displayed before Promised and Need by date -->
			<xsl:if test="DROP_SHIP_FLAG='Y'">
				<xsl:if test="SHIP_CUST_NAME !=''"> <xsl:value-of select="SHIP_CUST_NAME"/>  </xsl:if>
				<xsl:if test="SHIP_CONT_NAME !=''"> <fo:block/> <xsl:value-of select="SHIP_CONT_NAME"/>  </xsl:if>
				<xsl:if test="SHIP_CONT_EMAIL !=''"> <fo:block/><xsl:value-of select="SHIP_CONT_EMAIL"/> </xsl:if>
				<xsl:if test="SHIP_CONT_PHONE !=''">
					<xsl:if test="$DisplayBoilerPlate ">
						 <fo:block/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_TELEPHONE'][1]/TEXT"/>:
					</xsl:if>
					<fo:leader leader-pattern="space" leader-length="3.0pt"/> <xsl:value-of select="SHIP_CONT_PHONE"/> 
				</xsl:if>
				<xsl:if test="SHIP_TO_CONTACT_FAX !=''"> 
					<xsl:if test="$DisplayBoilerPlate ">
						 <fo:block/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_FAX'][1]/TEXT"/>:
					</xsl:if>
					 <fo:leader leader-pattern="space" leader-length="3.0pt"/> <xsl:value-of select="SHIP_TO_CONTACT_FAX"/> 
				</xsl:if>
			</xsl:if>
			<!-- end of bug#3594831 -->
			<xsl:choose>
				<xsl:when test="$print_multiple ='Y'">
					<fo:block/> <xsl:value-of select="SHIP_TO_ADDRESS_LINE1"/>
					<xsl:if test="SHIP_TO_ADDRESS_LINE2 !=''"> <fo:block/><xsl:value-of select="SHIP_TO_ADDRESS_LINE2"/> </xsl:if>
					<xsl:if test="SHIP_TO_ADDRESS_LINE3 !=''"> <fo:block/><xsl:value-of select="SHIP_TO_ADDRESS_LINE3"/> </xsl:if>
					<xsl:if test="SHIP_TO_ADDRESS_INFO !=''"> <fo:block/> <xsl:value-of select="SHIP_TO_ADDRESS_INFO"/> </xsl:if>
					<xsl:if test="SHIP_TO_COUNTRY !=''"> <fo:block/><xsl:value-of select="SHIP_TO_COUNTRY"/>	</xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$shipment_count &gt; 1">
					<fo:block/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_USE_SHIP_ADDRESS_TOP'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <fo:page-number-citation ref-id="page_ref" />
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose> 
			</fo:block> </fo:table-cell>
			</fo:table-row>
			</fo:table-body>
			</fo:table>
			<!-- end of bug#3594831 -->
			</fo:block> </fo:table-cell>

			<xsl:if test="$shipment_count &gt; 1">
				<fo:table-cell xsl:use-attribute-sets="table.cell6">  <fo:block xsl:use-attribute-sets="form_data"> 
					<!-- Bug3670603: As per the bug Promised and need by date are added. -->
					<xsl:if test="PROMISED_DATE!=''">
					<fo:block/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_WF_NOTIF_PROMISED_DATE'][1]/TEXT"/>
					<fo:block/> <xsl:value-of select="PROMISED_DATE"/>
					</xsl:if>
					<xsl:if test="NEED_BY_DATE!=''">
					<fo:block/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_WF_NOTIF_NEEDBY_DATE'][1]/TEXT"/>
					<fo:block/> <xsl:value-of select="NEED_BY_DATE"/>
					</xsl:if> 
				</fo:block> </fo:table-cell>

				<fo:table-cell text-align="center"> <fo:block > </fo:block>	</fo:table-cell>

				<fo:table-cell text-align="center"> <fo:block > </fo:block>	</fo:table-cell>

				<fo:table-cell text-align="center"> <fo:block > </fo:block>	</fo:table-cell>

				<fo:table-cell xsl:use-attribute-sets="table.cell6">  <fo:block xsl:use-attribute-sets="form_data"> 
					<xsl:value-of select="TAXABLE_FLAG"/> 
				</fo:block> </fo:table-cell>

				<fo:table-cell xsl:use-attribute-sets="table.cell5">  <fo:block xsl:use-attribute-sets="form_data1"> 
				<xsl:value-of select="AMOUNT"/> 
				</fo:block> </fo:table-cell>

			</xsl:if>
		</fo:table-row>
                
                <!-- Bug#3823799: to display Ship to address at top of page which spans more than one column -->
                <xsl:if test="$print_multiple != 'Y' and $shipment_count = 1">
		<fo:table-row >
			<fo:table-cell > <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell>
			<fo:table-cell xsl:use-attribute-sets="table.cell6" number-columns-spanned="7">  <fo:block xsl:use-attribute-sets="form_data"> 
				<fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_USE_SHIP_ADDRESS_TOP'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <fo:page-number-citation ref-id="page_ref" />
			</fo:block> </fo:table-cell>
		</fo:table-row>
		</xsl:if>

		<!-- for lines short text -->
                <!--Bug#3999145: Removed fo:leader tag to reduce the space between the attachments -->
                <xsl:for-each select="LINE_LOC_SHORT_TEXT/LINE_LOC_SHORT_TEXT_ROW">
		<fo:table-row >
			<fo:table-cell > <fo:block > </fo:block> </fo:table-cell>
			<fo:table-cell xsl:use-attribute-sets="table.cell6" number-columns-spanned="7">  <fo:block xsl:use-attribute-sets="form_data"> 
				
                                <!--Bug#4088207: Call LINE_LOCATION_ATTACHMENT_TEMP template to display the attachment -->
                                 <xsl:call-template name="LINE_LOCATION_ATTACHMENT_TEMP">
                                        <xsl:with-param name="ATTACHMENT_TEXT" select="SHORT_TEXT"/>
                                 </xsl:call-template>

			</fo:block> </fo:table-cell>
		</fo:table-row>
		</xsl:for-each>

		<!-- for long text -->
                <!--Bug#3999145: Modified the test condition to display the shipment attachments for the current shipment -->
		<xsl:for-each select="$SHIPMENT_ATTACHMENTS_ROOT_OBJ">
			<xsl:if test="$lineLocID = .">
				<xsl:variable name="line" select="position()" />
				<fo:table-row >
				<fo:table-cell > <fo:block >  </fo:block> </fo:table-cell>
				<fo:table-cell xsl:use-attribute-sets="table.cell6" number-columns-spanned="7">  <fo:block xsl:use-attribute-sets="form_data"> 

					<!--Bug#4088207: Call LINE_LOCATION_ATTACHMENT_TEMP template to display the attachment -->
                                         <xsl:call-template name="LINE_LOCATION_ATTACHMENT_TEMP">
                                                <xsl:with-param name="ATTACHMENT_TEXT" select="../LONG_TEXT[$line]"/>
                                         </xsl:call-template>

				</fo:block> </fo:table-cell>
		</fo:table-row>
			</xsl:if>
		</xsl:for-each>

		
		<!-- display canceled details -->
		<xsl:if test="CANCEL_FLAG='Y'">
		<fo:table-row> <fo:table-cell number-columns-spanned="8"> <fo:block > <fo:leader leader-pattern="space" leader-length="2.0pt"/> </fo:block>	</fo:table-cell> </fo:table-row>
		<fo:table-row  keep-together.within-page="always">
			<fo:table-cell > <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell>
			<fo:table-cell number-columns-spanned="7">  <fo:block> 
				<fo:table>
				<fo:table-column column-width="5mm"/> <fo:table-column column-width="78mm"/>
				<fo:table-body> <fo:table-row> 
				<fo:table-cell> <fo:block> </fo:block> </fo:table-cell>
				<fo:table-cell xsl:use-attribute-sets="table.cell7">  <fo:block xsl:use-attribute-sets="form_data2">
				<fo:leader leader-pattern="space" leader-length="10.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_SHIPMENT_CANCELED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="substring(CANCEL_DATE,1,11)"/> <fo:block/>
				<fo:leader leader-pattern="space" leader-length="20.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_ORIGINAL_AMOUNT_ORDERED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="TOTAL_SHIPMENT_AMOUNT"/> <fo:block/>
				<fo:leader leader-pattern="space" leader-length="40.0pt"/> <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_AMOUNT_CANCELED'][1]/TEXT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="AMOUNT_CANCELLED"/> <fo:block/>
				</fo:block> </fo:table-cell> </fo:table-row> </fo:table-body>
				</fo:table>
			</fo:block> </fo:table-cell>
		</fo:table-row>
		<fo:table-row>
			<fo:table-cell number-columns-spanned="8"  font-size="8pt">
				<fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block>
			</fo:table-cell>
		</fo:table-row>
		</xsl:if>
		<!-- end of canceled details -->

		<fo:table-row>
			<fo:table-cell number-columns-spanned="8" font-size="4pt">
				<fo:block> <fo:leader leader-pattern="space" leader-length="0.0pt"/>  </fo:block>
			</fo:table-cell>
		</fo:table-row>

		<!-- display deliver to details -->
		<xsl:for-each select="DISTRIBUTIONS/DISTRIBUTIONS_ROW">
		<xsl:if test="REQUESTER_DELIVER_FIRST_NAME !='' ">
			<fo:table-row>
			<fo:table-cell > <fo:block> <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell>
			<fo:table-cell number-columns-spanned="7" xsl:use-attribute-sets="table.cell6">  <fo:block xsl:use-attribute-sets="form_data"> 
			<fo:table>
			<fo:table-column column-width="4mm"/><fo:table-column column-width="20mm"/> <fo:table-column column-width="100mm"/>
			<fo:table-body> 
				<fo:table-row> 
				<fo:table-cell > <fo:block > <fo:leader leader-pattern="space" leader-length="1.0pt"/> </fo:block> </fo:table-cell>
				<fo:table-cell > <fo:block>
					<xsl:if test="$DisplayBoilerPlate ">
						<xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_DELIVER_TO_LOCATION'][1]/TEXT"/>
					</xsl:if> 
				</fo:block> </fo:table-cell>
			
				<fo:table-cell>  <fo:block>
					<!-- start of displaying deliver to details -->
					<xsl:call-template name="NAME_TEMPLATE">
					<xsl:with-param name="FIRST_NAME" select="REQUESTER_DELIVER_FIRST_NAME"/>
					<xsl:with-param name="LAST_NAME" select="REQUESTER_DELIVER_LAST_NAME"/>
					<xsl:with-param name="TITLE" select="REQUESTER_DELIVER_TITLE"/>
					</xsl:call-template>
					<xsl:if test="QUANTITY_ORDERED !=''"> 
					<fo:leader leader-pattern="space" leader-length="2.0pt"/>(===><xsl:value-of select="QUANTITY_ORDERED"/>)
					</xsl:if>
					<fo:block/> <xsl:value-of select="EMAIL_ADDRESS"/>
					<!-- end of deliver details -->
					
				</fo:block> </fo:table-cell>
				</fo:table-row>
			</fo:table-body></fo:table>
			</fo:block> </fo:table-cell>
			</fo:table-row>
		</xsl:if>
		</xsl:for-each> 	<!-- end of distributions if -->
		</xsl:for-each> <!-- end of shipment if -->
		<fo:table-row> <fo:table-cell number-columns-spanned="8"> <fo:block > <fo:leader leader-pattern="space" leader-length="2.0pt"/> </fo:block>	</fo:table-cell> </fo:table-row>
	</xsl:if>

</xsl:for-each> <!-- end of Line, shipments and distributions details -->

<!-- dTAC: Start Section-->
	<xsl:if test="$BPA_CHECK = 'Y'">
	<fo:table-row>
		<fo:table-cell>
			<fo:block>
				<fo:leader leader-pattern="space" leader-length="1.0pt" /> 
			</fo:block>
		</fo:table-cell>
		<fo:table-cell  number-columns-spanned="7" xsl:use-attribute-sets="table.cell6" >
			<fo:block xsl:use-attribute-sets="form_data">
				<fo:inline xsl:use-attribute-sets="legal_details_style">
					<xsl:value-of select="HEADER_ACC_DISPLAY_EN"/>
				</fo:inline>
			</fo:block>
		</fo:table-cell> 
	</fo:table-row>
	</xsl:if>
<!-- dTAC: End Section-->

<fo:table-row>
<fo:table-cell  number-columns-spanned="4" xsl:use-attribute-sets="table_cell_heading1" >
	<fo:block xsl:use-attribute-sets="form_data">
		<fo:inline xsl:use-attribute-sets="legal_details_style">
			<xsl:text>  </xsl:text>
		</fo:inline>
	</fo:block>
</fo:table-cell> 

	<fo:table-cell text-align = "left" number-columns-spanned="1" xsl:use-attribute-sets="table_cell_heading1" >
		<fo:block xsl:use-attribute-sets="form_data">
			<fo:inline xsl:use-attribute-sets="legal_details_style">
				<xsl:text>Total </xsl:text>
			</fo:inline>
		</fo:block>
	</fo:table-cell> 

<fo:table-cell  number-columns-spanned="2" xsl:use-attribute-sets="table_cell_heading1" ><fo:block xsl:use-attribute-sets="form_data">
		<fo:inline xsl:use-attribute-sets="legal_details_style">
			 <xsl:value-of select="TOTAL_AMOUNT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/>
		</fo:inline>
	</fo:block> </fo:table-cell> 
</fo:table-row>

<fo:table-row> 
	<fo:table-cell  number-columns-spanned="4" xsl:use-attribute-sets="table_cell_heading1" ><fo:block xsl:use-attribute-sets="form_data">
		<fo:inline xsl:use-attribute-sets="legal_details_style">
		<xsl:text>  </xsl:text>
</fo:inline></fo:block> </fo:table-cell> 

	<fo:table-cell  text-align = "left" number-columns-spanned="1" xsl:use-attribute-sets="table_cell_heading1" ><fo:block xsl:use-attribute-sets="form_data">
		 <fo:inline xsl:use-attribute-sets="legal_details_style">
			<xsl:text>VAT </xsl:text>
			<!--xsl:if test="TAX_RATE!=0"><xsl:value-of select="TAX_RATE"/><xsl:text>%</xsl:text></xsl:if -->
		</fo:inline>
            </fo:block>
          </fo:table-cell>

	<fo:table-cell  number-columns-spanned="2" xsl:use-attribute-sets="table_cell_heading1" ><fo:block xsl:use-attribute-sets="form_data">
		<fo:inline xsl:use-attribute-sets="legal_details_style">
			 <xsl:value-of select="TOT_VAT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/>
		</fo:inline>
	</fo:block> </fo:table-cell> 
</fo:table-row>

<fo:table-row> 
	<fo:table-cell  text-align = "center" number-columns-spanned="4" xsl:use-attribute-sets="table_cell_heading1" >
		<fo:block xsl:use-attribute-sets="form_data">
              <xsl:choose>
                <!-- <xsl:when test="$VENDOR_COUNTRY_CHK = 'TH' or $VENDOR_COUNTRY_CHK = 'Thailand'"> -->
				<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
				   <fo:inline xsl:use-attribute-sets="legal_details_style">
					<xsl:value-of select="TOTAL_AMOUNT_TXT"/>
				  </fo:inline>
                </xsl:when>
                <xsl:otherwise>
				  <fo:inline xsl:use-attribute-sets="legal_details_style">
					<xsl:value-of select="TOTAL_AMOUNT_TXT_ENG"/>
				  </fo:inline>
                </xsl:otherwise>
              </xsl:choose>
		</fo:block>
	</fo:table-cell>

	<fo:table-cell  text-align = "left" number-columns-spanned="1" xsl:use-attribute-sets="table_cell_heading1" ><fo:block xsl:use-attribute-sets="form_data">
		 <fo:inline xsl:use-attribute-sets="legal_details_style">
			<xsl:text>Grand Total</xsl:text>
		</fo:inline>
            </fo:block>
          </fo:table-cell>

	<fo:table-cell  number-columns-spanned="2" xsl:use-attribute-sets="table_cell_heading1" ><fo:block xsl:use-attribute-sets="form_data">
		<fo:inline xsl:use-attribute-sets="legal_details_style">
			 <xsl:value-of select="TOT_AMOUNT"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/>
		</fo:inline>
	</fo:block> </fo:table-cell> 
</fo:table-row>

</fo:table-body>
</fo:table>

<fo:table space-before="20pt">
<fo:table-column column-width="180mm"/> <!--fo:table-column column-width="40mm"/><fo:table-column column-width="40mm"/ -->
<fo:table-body>
<!-- Add Payment Terms -->
<fo:table-cell xsl:use-attribute-sets="table.cell6" vertical-align="top">
<xsl:choose>
         <!-- <xsl:when test="$VENDOR_COUNTRY_CHK = 'TH' or $VENDOR_COUNTRY_CHK = 'Thailand'"> -->
	 <xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
						<fo:inline xsl:use-attribute-sets="legal_details_style">
						<xsl:text>หมายเหตุ</xsl:text>
						</fo:inline><fo:block/>
						<fo:inline xsl:use-attribute-sets="legal_details_style">
						<xsl:text>1. ใบสั่งซื้อฉบับนี้จะสมบูรณ์เมื่อมีลายเซ็นต์ผู้ที่มีอำนาจอนุมัติสั่งซื้อแล้วเท่านั้น</xsl:text>
						</fo:inline><fo:block/>
						<fo:inline xsl:use-attribute-sets="legal_details_style">
						<xsl:text>2. โปรดแนบใบสั่งซื้อทุกครั้งเมื่อจัดส่งใบกำกับภาษี หรือใบแจ้งหนี้</xsl:text>
						</fo:inline><fo:block/>
		 </xsl:when>
         <xsl:otherwise>
						<fo:inline xsl:use-attribute-sets="legal_details_style">
						<xsl:text>Important Notice</xsl:text>
						</fo:inline><fo:block/>
						<fo:inline xsl:use-attribute-sets="legal_details_style">
						<xsl:text>1. The complete purchase order must have authorized signature.</xsl:text>
						</fo:inline><fo:block/>
						<fo:inline xsl:use-attribute-sets="legal_details_style">
						<xsl:text>2. For your bill placement, please attach Purchase Order with Tax Invoice/Invoice.</xsl:text>
						</fo:inline><fo:block/>

		 </xsl:otherwise>
</xsl:choose>
</fo:table-cell>
</fo:table-body>
</fo:table>

<fo:table space-before="20pt">
<fo:table-column column-width="100mm"/> <fo:table-column column-width="40mm"/><fo:table-column column-width="40mm"/>
<fo:table-body>

<!---Add Signature -->
<xsl:if test="$print_signature">
<fo:table-row> 

	<fo:table-cell xsl:use-attribute-sets="table.cell6" vertical-align="top">
	<fo:block>
              <xsl:choose>
                <!-- <xsl:when test="$VENDOR_COUNTRY_CHK = 'TH' or $VENDOR_COUNTRY_CHK = 'Thailand'"> -->
		<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
				   <fo:inline xsl:use-attribute-sets="legal_details_style">
					<xsl:text> ตอบรับใบสั่งซื้อโดย: </xsl:text>
				   </fo:inline>
                </xsl:when>
                <xsl:otherwise>
				   <fo:inline xsl:use-attribute-sets="legal_details_style">
					<xsl:text> Order Received by: </xsl:text>
				   </fo:inline>
                </xsl:otherwise>
              </xsl:choose>
	</fo:block>
	</fo:table-cell>

		<fo:table-cell xsl:use-attribute-sets="table.cell6" text-align="left"><fo:block xsl:use-attribute-sets="form_data">
              <xsl:choose>
                <!-- <xsl:when test="$VENDOR_COUNTRY_CHK = 'TH' or $VENDOR_COUNTRY_CHK = 'Thailand'"> -->
		<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
				   <fo:inline xsl:use-attribute-sets="legal_details_style">
					<xsl:text> อนุมัติการสั่งซื้อโดย: </xsl:text>
				  </fo:inline>
                </xsl:when>
                <xsl:otherwise>
				  <fo:inline xsl:use-attribute-sets="legal_details_style">
					<xsl:text> Approved by: </xsl:text>
				  </fo:inline>
                </xsl:otherwise>
              </xsl:choose>
            </fo:block>
          </fo:table-cell>

	<fo:table-cell height="4mm">
	<fo:block>
		<fo:inline space-before="2pt">
			<fo:external-graphic content-width="96pt" content-height="43pt" src="url:{concat('http://cygnus32.tac.co.th:8000/OA_MEDIA/', EMP_ID, '.jpg')}"/>
		</fo:inline>
	</fo:block> 
	</fo:table-cell> 
</fo:table-row>
</xsl:if>
<!--End add signature -->


<fo:table-row> 

	<fo:table-cell xsl:use-attribute-sets="table.cell6">
	<fo:block>
              <xsl:choose>
                <!-- <xsl:when test="$VENDOR_COUNTRY_CHK = 'TH' or $VENDOR_COUNTRY_CHK = 'Thailand'"> -->
		<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
				   <fo:inline xsl:use-attribute-sets="legal_details_style">
					<xsl:text>  </xsl:text>
				  </fo:inline>
                </xsl:when>
                <xsl:otherwise>
				  <fo:inline xsl:use-attribute-sets="legal_details_style">
					<xsl:text>  </xsl:text>
				  </fo:inline>
                </xsl:otherwise>
              </xsl:choose>
	</fo:block>
	</fo:table-cell>

	<fo:table-cell xsl:use-attribute-sets="table.cell6">
	<fo:block>
		<fo:inline space-before="2pt">
			<xsl:text> </xsl:text>
		</fo:inline>
	</fo:block>
	</fo:table-cell>

	<fo:table-cell xsl:use-attribute-sets="table.cell6"><fo:block xsl:use-attribute-sets="form_data">
              <xsl:choose>
                <!-- <xsl:when test="$VENDOR_COUNTRY_CHK = 'TH' or $VENDOR_COUNTRY_CHK = 'Thailand'"> -->
		<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
				   <fo:inline xsl:use-attribute-sets="legal_details_style">
					<xsl:value-of select="NAME_TH"/>
				  </fo:inline>
                </xsl:when>
                <xsl:otherwise>
				  <fo:inline xsl:use-attribute-sets="legal_details_style">
					<xsl:value-of select="NAME_ENG"/>
				  </fo:inline>
                </xsl:otherwise>
              </xsl:choose>
            </fo:block>
          </fo:table-cell>
</fo:table-row>

<fo:table-row> 

	<fo:table-cell xsl:use-attribute-sets="table.cell6" vertical-align="middle">
	<fo:block>
              <xsl:choose>
                <!-- <xsl:when test="$VENDOR_COUNTRY_CHK = 'TH' or $VENDOR_COUNTRY_CHK = 'Thailand'"> -->
		<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
				   <fo:inline xsl:use-attribute-sets="legal_details_style">
					<xsl:text> วันที่: </xsl:text>
				  </fo:inline>
                </xsl:when>
                <xsl:otherwise>
				  <fo:inline xsl:use-attribute-sets="legal_details_style">
					<xsl:text> Date: </xsl:text>
				  </fo:inline>
                </xsl:otherwise>
              </xsl:choose>
	</fo:block>
	</fo:table-cell>

	<fo:table-cell xsl:use-attribute-sets="table.cell6" text-align="left" vertical-align="middle"><fo:block xsl:use-attribute-sets="form_data">
				<xsl:choose>
                <!-- <xsl:when test="$VENDOR_COUNTRY_CHK = 'TH' or $VENDOR_COUNTRY_CHK = 'Thailand'"> -->
		<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
				   <fo:inline xsl:use-attribute-sets="legal_details_style">
					<xsl:text> วันที่: </xsl:text>
				  </fo:inline>
                </xsl:when>
                <xsl:otherwise>
				  <fo:inline xsl:use-attribute-sets="legal_details_style">
					<xsl:text> Date: </xsl:text>
				  </fo:inline>
                </xsl:otherwise>
              </xsl:choose>
            </fo:block>
          </fo:table-cell>

	<fo:table-cell xsl:use-attribute-sets="table.cell6" vertical-align="middle"><fo:block xsl:use-attribute-sets="form_data">
				<xsl:choose>
                <!-- <xsl:when test="$VENDOR_COUNTRY_CHK = 'TH' or $VENDOR_COUNTRY_CHK = 'Thailand'"> -->
		<xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
				   <fo:inline xsl:use-attribute-sets="legal_details_style">
					<xsl:value-of select="substring(APPROVED_DATE, 1, 11)"/>
				  </fo:inline>
                </xsl:when>
                <xsl:otherwise>
				  <fo:inline xsl:use-attribute-sets="legal_details_style">
					<xsl:value-of select="substring(APPROVED_DATE, 1, 11)"/>
				  </fo:inline>
                </xsl:otherwise>
              </xsl:choose>
            </fo:block>
          </fo:table-cell>
</fo:table-row>

</fo:table-body>
</fo:table>

<fo:block break-after='page'/>

<xsl:choose>
         <!-- <xsl:when test="$VENDOR_COUNTRY_CHK = 'TH' or $VENDOR_COUNTRY_CHK = 'Thailand'"> -->
	 <xsl:when test="$CURRENCY_CODE_CHK = 'THB'">
						
<fo:table table-layout="fixed" width="100%">
<fo:table-column column-width="100%"/>
<fo:table-body>
<fo:table-row> 

	<fo:table-cell xsl:use-attribute-sets="table.cell6" vertical-align="top" text-align="center">
	<fo:block font-weight="bold" font-size="11pt">
		  <xsl:text>เงื่อนไขและข้อตกลงทั่วไป</xsl:text>
	</fo:block>
	</fo:table-cell>

</fo:table-row>
</fo:table-body>
</fo:table>

<fo:table space-before="20pt">
<fo:table-column column-width="49%"/>
<fo:table-column column-width="2%"/>
<fo:table-column column-width="49%"/>
<fo:table-body>

                        <fo:table-row>
                           <fo:table-cell padding-end="5.4pt" padding-bottom="0.0pt" padding-start="5.4pt" padding-top="0.0pt" vertical-align="top" height="2.6pt" number-columns-spanned="1">
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">1. วัตถุประสงค์</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">เงื่อนไขและข้อตกลงทั่วไปนี้ให้ใช้บังคับแก่การซื้อขายจ้างทำของ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">และ/หรือบริการอื่นใด (ซึ่งต่อไปนี้เรียกว่า“สินค้า”) ที่บริษัทได้</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ออกใบสั่งซื้อให้แก่ผู้ขาย/ผู้รับจ้าง/ผู้ให้บริการ (ซึ่งต่อไปนี้เรียก</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ว่า“ผู้ขาย”)</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">“สินค้า” ให้หมายรวมถึง เอกสารใดๆ คู่มือการปฏิบัติงานคู่มือทาง</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">เทคนิค บัญชีรายการสินค้า คุณลักษณะเฉพาะ และ/หรือสิ่งอื่นใด</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ที่เกี่ยวข้องกับสินค้า</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">2. ใบสั่งซื้อ</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ใบสั่งซื้อจะมีผลสมบูรณ์ต่อเมื่อผู้มีอำนาจลงนามของบริษัทได้ลง</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">นามแล้วเท่านั้น ผู้ขายต้องตรวจให้แน่ใจว่าผู้มีอำนาจลงนามของ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">บริษัทได้ลงนามในใบสั่งซื้อที่ตนได้รับแล้ว ทั้งนี้ ใบสั่งซื้อที่ออก</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ภายใต้สัญญาที่จัดทำขึ้นไว้โดยเฉพาะจะต้องอ้างอิงถึงสัญญา</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">นั้นๆด้วย</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">3. การยกเลิกและการเปลี่ยนแปลง</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">บริษัทมีสิทธิยกเลิกใบสั่งซื้อโดยการส่งหนังสือยกเลิกให้แก่ผู้ขาย</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">อย่างน้อย 5 (ห้า) วัน ก่อนวันส่งมอบที่ได้ตกลงกันไว้ ทั้งนี้ ให้นำ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ข้อกำหนดในข้อ 2. (ใบสั่งซื้อ) ของเงื่อนไขและข้อตกลงทั่วไปนี้</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">มาใช้แก่หนังสือยกเลิกนี้ด้วย</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">บริษัทมีสิทธิที่จะเปลี่ยนแปลงใบสั่งซื้อ โดยการส่งหนังสือแจ้ง</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">เปลี่ยนแปลงให้แก่ผู้ขายเป็นระยะเวลาอย่างน้อย 5 (ห้า) วัน </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ก่อนวันส่งมอบที่ได้ตกลงกันไว้ ผู้ขายต้องส่งมอบสินค้าตามที่มี</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">การแจ้งเปลี่ยนแปลงดังกล่าวให้แก่บริษัท หากการเปลี่ยนแปลง</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">นั้นไม่เป็นการเพิ่มภาระอันเกินควรต่อผู้ขาย ทั้งนี้ ให้นำข้อกำหนด</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ในข้อ 2. (ใบสั่งซื้อ) ของเงื่อนไขและข้อตกลงทั่วไปนี้มาใช้แก่</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">หนังสือแจ้งเปลี่ยนแปลงนี้ด้วย</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ผู้ขายจะไม่เรียกร้องค่าเสียหาย ค่าธรรมเนียม ค่าภาษีศุลกากร </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">หรือค่าสูญเสียรายได้อื่นใดในทางเศรษฐกิจที่เกิดขึ้นจากการ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ยกเลิกและการเปลี่ยนแปลงการสั่งซื้อดังกล่าว</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">4. การยืนยันการสั่งซื้อ</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ผู้ขายจะต้องยืนยันว่าได้รับใบสั่งซื้อโดยส่งหนังสือยืนยัน หรือ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">โดยลงนามในใบสั่งซื้อและส่งกลับมายังบริษัทโดยทางโทรสาร</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ภายใน 3 (สาม) วันหลังจากที่ได้รับใบสั่งซื้อดังกล่าว ทั้งนี้ </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ห้ามมิให้ใช้ข้อบังคับเงื่อนไขและข้อตกลงอื่นใดนอกเหนือจาก</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">เงื่อนไขและข้อตกลงทั่วไปฉบับนี้ เว้นแต่คู่สัญญาทั้งสองฝ่าย</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ได้ตกลงกันเป็นหนังสืออย่างชัดแจ้ง</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">5. ราคาสินค้า – การชำระเงิน - การแจ้งหนี้</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">5.1 ราคาสินค้าจะต้องมีการตกลงกันก่อนที่จะออกใบสั่งซื้อ </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">(ซึ่งต่อไปนี้เรียกว่า “ราคาสินค้า”) โดยต้องไม่มีค่าใช้จ่ายอื่นใด</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">เพิ่มเติมในราคาสินค้าในภายหลัง เช่น ค่าธรรมเนียมในการสั่ง</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">หรือการแจ้งราคาสินค้า ค่าบรรจุหีบห่อสินค้า หรือค่าธรรมเนียม</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">หรือภาษีอื่นใด เป็นต้น ราคาสินค้าจะไม่รวมภาษีมูลค่าเพิ่ม </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ทั้งนี้ ผู้ขายต้องรับผิดชอบในค่าอากรแสตมป์</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                           </fo:table-cell>
                           <fo:table-cell padding-end="5.4pt" padding-bottom="0.0pt" padding-start="5.4pt" padding-top="0.0pt" vertical-align="top" height="2.6pt" number-columns-spanned="1">
                              <fo:block height="9.0pt"/>
                           </fo:table-cell>
                           <fo:table-cell padding-end="5.4pt" padding-bottom="0.0pt" padding-start="5.4pt" padding-top="0.0pt" vertical-align="top" height="2.6pt" number-columns-spanned="1">
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">5.2 เว้นแต่ตกลงกันไว้เป็นอย่างอื่น บริษัทจะชำระราคาสินค้าให้</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">แก่ผู้ขายภายใน 60 (หกสิบ) วันหลังจากวันกำหนดจ่ายเงินครั้ง</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ต่อไปของบริษัท และหลังจากบริษัทได้รับใบแจ้งหนี้ที่ถูกต้อง</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">สมบูรณ์ตามข้อ 5.3 ณ ที่อยู่ที่ถูกต้องตามข้อ 5.4 ทั้งนี้ ผู้ขายต้อง</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ไม่ออกใบแจ้งหนี้ก่อนที่จะได้ส่งมอบสินค้าทั้งหมดแล้วเสร็จตาม</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ข้อ 6 และต้องไม่ออกใบแจ้งหนี้ช้ากว่า 30 วัน นับจากวันที่ตนมี</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">สิทธิออกใบแจ้งหนี้</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">5.3 ใบแจ้งหนี้จะต้องระบุเลขที่ใบสั่งซื้อของบริษัทคุณลักษณะ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">เฉพาะและรายละเอียดต่างๆ ของสินค้าตามที่ได้กำหนดไว้ใน</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ใบสั่งซื้อ ทั้งนี้ ใบกำกับภาษีจะต้องจัดทำให้สอดคล้องกับ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ประมวลรัษฎากรด้วย</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">5.4 เว้นแต่กำหนดไว้เป็นอย่างอื่น ผู้ขายจะส่งใบแจ้งหนี้ไปตาม</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ที่อยู่ตามที่บริษัทได้กำหนดไว้</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">5.5  ในกรณีที่บริษัทไม่ชำระราคาสินค้าภายในกำหนดระยะเวลา</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ตามข้อ 5.2 และบริษัทไม่มีเหตุที่จะอ้างเพื่อระงับการจ่ายเงินดัง</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">กล่าวได้ ผู้ขายมีสิทธิเรียกร้องดอกเบี้ยผิดนัดชำระหนี้ในอัตรา</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ร้อยละ 5 (ห้า) ต่อปีของจำนวนเงินที่ค้างชำระได้</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">6. การส่งมอบ - กรรมสิทธิ์และความเสี่ยงภัย</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">6.1 การส่งมอบสินค้านั้นให้ส่งมอบ ณ ที่อยู่ที่กำหนดไว้ในใบสั่ง</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ซื้อหรือสถานที่อื่นที่ได้ตกลงกันไว้เป็นหนังสือเว้นแต่กำหนดไว้</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">เป็นอย่างอื่น เงื่อนไขการส่งมอบสินค้าให้เป็นไปตาม DDP </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">(Delivery Duty Paid) Incoterm 2000 และช่วงเวลาการส่ง</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">มอบสินค้าคือระหว่าง 9:00น. จนถึง 15:00 น. ของวันทำการ</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">การส่งมอบสินค้าจะแล้วเสร็จเมื่อสินค้าทั้งหมดนั้นได้ส่งมอบ ณ </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ที่อยู่ตามที่ได้กำหนดไว้ในข้อนี้  และบริษัทได้ตรวจสอบและรับ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">มอบสินค้าเรียบร้อยแล้ว การส่งมอบสินค้าบางส่วนตามใบสั่งซื้อ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">นั้นไม่สามารถกระทำได้ เว้นแต่จะได้ตกลงกันไว้เป็นอย่างอื่น </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ทั้งนี้ การส่งมอบสินค้าบางส่วนหลังจากวันส่งมอบสินค้าที่ได้</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">กำหนดไว้ ให้ถือเป็นการส่งมอบสินค้าล่าช้าทั้งจำนวน</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">6.2 เมื่อมีการส่งมอบสินค้าแล้วเสร็จตามข้อ 6.1 กรรมสิทธิ์และ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ความเสี่ยงภัยจะโอนจากผู้ขายไปยังบริษัท</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">6.3 หากการส่งมอบสินค้าไม่เป็นไปตามเงื่อนไขและข้อตกลงทั่ว</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ไปนี้ ผู้ขายต้องรับผิดชอบต่อค่าใช้จ่ายใดๆ ที่เกิดขึ้น (เช่น </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ค่าขนส่งสินค้าที่นำมาทดแทนสินค้ารายการเดิม) กับบริษัท</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ไม่ว่าโดยทางตรงหรือทางอ้อม</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">7. ความล่าช้า-เบี้ยปรับ-การเยียวยาความเสียหาย-การส่งมอบ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ก่อนกำหนด</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">7.1 ในกรณีที่มีการส่งมอบสินค้าล่าช้าเกินวันส่งมอบที่ตกลงกันไว้ </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">บริษัทมีสิทธิเรียกร้องเบี้ยปรับรายวันจากผู้ขายเป็นจำนวนเงินเท่า</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">กับร้อยละ 0.25 (ศูนย์จุดสองห้า) ของราคาสินค้าที่ระบุไว้ในใบสั่ง</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ซื้อจนกว่าจะมีการส่งมอบสินค้าแล้วเสร็จตามข้อ 6.  แต่ไม่เกิน</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ร้อยละ 15 (สิบห้า) ของราคาสินค้า ทั้งนี้ ไม่ว่ากรณีใดๆ จำนวน</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">เบี้ยปรับจะต้องไม่น้อยกว่า 100 (หนึ่งร้อย) บาท ต่อวัน</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                           </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                           <fo:table-cell padding-end="5.4pt" padding-bottom="0.0pt" padding-start="5.4pt" padding-top="0.0pt" vertical-align="top" height="643.0pt" number-columns-spanned="1">
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">การใช้สิทธิเรียกร้องเบี้ยปรับดังกล่าว มิได้เป็นการตัดสิทธิใน</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">การเรียกร้องการเยียวยาความเสียหายอื่นใด</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">7.2 บริษัทมีสิทธิยกเลิกใบสั่งซื้อ โดยการส่งหนังสือยกเลิกแก่</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ผู้ขาย และมีสิทธิเรียกร้องค่าเสียหายในกรณีที่ไม่มีการส่งมอบ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">สินค้าหรือมีความเป็นไปได้ว่าจะส่งมอบสินค้าไม่ทันภายใน 60 </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">(หกสิบ) วันนับตั้งแต่วันกำหนดส่งมอบสินค้า (วันเริ่มต้นการคิด</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">เบี้ยปรับ)</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">7.3 เว้นแต่ตกลงกันไว้เป็นอย่างอื่น ผู้ขายจะต้องไม่ส่งมอบสินค้า</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ก่อนวันกำหนดส่งมอบที่ได้ตกลงกันไว้ (ซึ่งต่อไปนี้เรียกว่า </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">“การส่งมอบก่อนกำหนด”)</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="start" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ถึงแม้ว่าการส่งมอบก่อนกำหนดจะกระทำแล้วเสร็จตามข้อ 6 ผู้</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ขายจะต้องไม่ออกใบแจ้งหนี้ก่อนวันส่งมอบสินค้า ตามที่กำหนด</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ไว้ตามข้อ 5.2 แต่กรรมสิทธิ์และความเสี่ยงภัยจะโอนจากผู้ขาย</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">ไปยังบริษัทหากมีการส่งมอบสินค้าแล้วเสร็จตามข้อ 6 ทั้งนี้ </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">การส่งมอบก่อนกำหนดจะไม่มีผลกระทบต่อสิทธิในการยกเลิก </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false" color="#000000">และ/หรือการเปลี่ยนแปลงใบสั่งซื้อตามข้อ 3</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">8. คำรับรอง และข้อตกลงกระทำการ</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ผู้ขายขอรับรองและตกลงต่อบริษัทว่า</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">8.1 สินค้าที่สั่งซื้อจะต้องตรงตามคุณลักษณะเฉพาะที่ได้มีการ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ตกลงกันไว้ โดยเป็นสินค้าที่มีมาตรฐานสูงและเหมาะกับ </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">วัตถุประสงค์การใช้งาน</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">8.2 ผู้ขายไม่ถูกจำกัดสิทธิในการขาย และ/หรือจัดหาสินค้า</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">แต่อย่างใด</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">8.3 ผู้ขายตกลงว่าจะส่งมอบหนังสือให้ความเห็นชอบ เอกสาร</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ต่างๆใบอนุญาต หรือหนังสือให้ความยินยอม ที่จำเป็นสำหรับ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">สินค้า (ถ้ามี) ให้แก่บริษัท</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">8.4 ผู้ขายตกลงว่าจะไม่โอนสิทธิ และ/หรือหน้าที่ใดๆ ภายใต้</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ใบสั่งซื้อนี้ให้แก่บุคคลอื่น โดยไม่ได้รับความยินยอมเป็น</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ลายลักษณ์อักษรจากบริษัทก่อน อย่างไรก็ตาม ผู้ขายและ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">บริษัทตกลงว่า บริษัทมีสิทธิโอนสิทธิ และ/หรือ หน้าที่ใดๆ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ของตนภายใต้ใบสั่งซื้อฉบับนี้ให้กับบริษัทในเครือของบริษัทได้</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">โดยไม่ต้องได้รับความยินยอมจากผู้ขาย ทั้งนี้ ผู้ขายตกลงว่าจะ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ให้ความร่วมมือกับบริษัทอย่างดีที่สุดและดำเนินการต่างๆ ที่</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">จำเป็นและเกี่ยวข้องตามที่บริษัทร้องขอ เพื่อทำให้การโอนสิทธิ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">และ/หรือ หน้าที่ใดๆ ภายใต้ใบสั่งซื้อนี้มีผลสมบูรณ์ตามกฎหมาย </fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ทั้งนี้ ในการตีความข้อสัญญานี้ ให้คำว่า “บริษัทในเครือ</fo:inline>
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="nil" font-family="SimSun" white-space-collapse="false">”</fo:inline>
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false"> </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">หมายถึง บริษัทลูก หรือบริษัทแม่ของบริษัท หรือบริษัทลูกอื่นๆ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ของบริษัทแม่เดียวกัน</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="nil" font-family="SimSun" white-space-collapse="false">“</fo:inline>
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">บริษัทแม่</fo:inline>
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="nil" font-family="SimSun" white-space-collapse="false">”</fo:inline>
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false"> หมายถึง บริษัทใดๆ ที่มีบริษัทเป็นบริษัทลูก</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="nil" font-family="SimSun" white-space-collapse="false">“</fo:inline>
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">บริษัทลูก</fo:inline>
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="nil" font-family="SimSun" white-space-collapse="false">”</fo:inline>
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false"> หมายถึง บริษัทแห่งหนึ่ง (เรียกแทนว่าบริษัท ก)</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">จะถือว่าเป็นบริษัทลูกของบริษัทอีกแห่งหนึ่ง (เรียกแทนว่า </fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt">
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">บริษัท ข) ต่อเมื่อ (1) บริษัท ข ถือหุ้นในบริษัท ก มากกว่า</fo:inline>
                              </fo:block>
                           </fo:table-cell>
                           <fo:table-cell padding-end="5.4pt" padding-bottom="0.0pt" padding-start="5.4pt" padding-top="0.0pt" vertical-align="top" height="643.0pt" number-columns-spanned="1">
                              <fo:block height="9.0pt"/>
                           </fo:table-cell>
                           <fo:table-cell padding-end="5.4pt" padding-bottom="0.0pt" padding-start="5.4pt" padding-top="0.0pt" vertical-align="top" height="643.0pt" number-columns-spanned="1">
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ร้อยละ 25 ไม่ว่าทางตรงหรือทางอ้อม หรือ (2) บริษัท ก </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">เป็นบริษัทลูกของบริษัทลูกอื่นของบริษัท ข</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">8.5 [ข้อนี้ใช้สำหรับใบสั่งซื้อที่ออกโดยบริษัท เพย์สบาย จำกัด </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">เท่านั้น]  ผู้ขายตกลงยินยอมให้ (ก) ผู้ตรวจสอบภายใน และ/หรือ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ผู้ตรวจสอบภายนอกของบริษัท (ซึ่งกำหนดหรืออาจถูกกำหนด</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">โดยดุลพินิจของบริษัทแต่เพียงฝ่ายเดียว) และ/หรือ (ข) ธนาคาร</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">แห่งประเทศไทยและ/หรือหน่วยงานราชการใดๆ และ/หรือ (ค)</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false"> ผู้แทนของ (ก) และ/หรือ (ข) มีสิทธิที่จะตรวจสอบสินค้า </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">การดำเนินงาน การควบคุมภายในและ/หรือหน้าที่อื่นๆ ที่เกี่ยวข้อง</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ของผู้ขายในการขายหรือจัดหาสินค้าตามที่ระบุในใบสั่งซื้อนี้ </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ไม่ว่า ณ เวลาใดเวลาหนึ่งตามที่ (ก) และ/หรือ (ข) และ/หรือ </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">(ค) เห็นสมควร เพื่อให้เป็นไปตามวัตถุประสงค์ตามที่ระบุในข้อนี้ </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ผู้ขายตกลงที่จะให้ความร่วมมืออย่างเต็มที่กับบุคคลตามที่ระบุใน</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ข้อ (ก) – (ค) ข้างต้นและดำเนินการให้ความช่วยเหลือต่างๆ </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">เท่าที่จำเป็น และตามสมควรโดยปราศจากการคัดค้านหรือโต้แย้ง</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ใดๆ เพื่ออำนวยความสะดวกให้แก่บุคคลตามที่ระบุในข้อ (ก) – (ค)</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ข้างต้นไม่ว่าในการตรวจสอบ ณ เวลาใดเวลาหนึ่ง</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">การดำเนินการตรวจสอบใดๆ ภายใต้ข้อนี้ ไม่ทำให้ผู้ขายหลุดพ้น</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">จากหน้าที่หรือความรับผิดใดๆ ภายใต้เงื่อนไขและข้อตกลงตามที่</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ระบุในใบสั่งซื้อนี้และไม่ถือเป็นการให้สิทธิใดๆ แก่ผู้ขายในการ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">เรียกร้องค่าเสียหายใดๆ เพิ่มเติมจากบริษัท</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">9. สิทธิตามกฎหมาย - ค่าเสียหาย</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">9.1 สำหรับกรรมสิทธิ์ในส่วนหนึ่งส่วนใดของสินค้าที่มิได้โอนมายัง</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">บริษัท ผู้ขายรับรองว่าบริษัทมีสิทธิอย่างไม่จำกัดในการใช้ เปลี่ยน</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">แปลง จำหน่าย หรือใช้ประโยชน์อื่นใด หรือการจัดการสินค้า</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ที่ได้รับ</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">9.2 สินค้าที่ส่งมอบจะต้องปราศจากภาระผูกพันอื่นใดทั้งสิ้น </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ในกรณีที่บุคคลภายนอกได้ใช้สิทธิเรียกร้องอันเกี่ยวกับภาระผูกพัน</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">อื่นใดนั้นผู้ขายจะชดใช้ค่าเสียหายให้แก่บริษัท และจะดำเนินการ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ใดๆ ให้ภาระผูกพันอื่นใดดังกล่าวที่มีอยู่นั้นสิ้นสุดลงทันที รวมทั้ง</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">จะรับผิดชอบค่าใช้จ่ายทั้งหมดที่เกี่ยวข้องกับการดำเนินการให้</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ภาระผูกพันอื่นใดนั้นสิ้นสุดลง</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">“ภาระผูกพันอื่นใด รวมถึง การรอนสิทธิ บุริมสิทธิ การยึดทรัพย์ </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">สิทธิยึดหน่วง ภาระติดพัน ข้อผูกพันใดๆ ภาระจำยอม การจำนอง </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">การจำนำ หรือการละเมิดทรัพย์สินทางปัญญา ของบุคคลภายนอก”</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">10. ระยะเวลาการรับประกัน</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ผู้ขายรับประกันว่าสินค้าที่สั่งซื้อนั้นอยู่ในสภาพที่ไม่เสียหาย และ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">/หรือชำรุดบกพร่องใดๆ และตกลงรับประกันสินค้าเป็นระยะเวลา 2 </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">(สอง) ปีนับตั้งแต่วันที่กรรมสิทธิ์และความเสี่ยงภัยโอนไปยังบริษัท </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">เว้นแต่คู่สัญญาทั้งสองฝ่ายจะตกลงกันไว้เป็นอย่างอื่น ผู้ขายต้อง</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">เป็นผู้รับผิดชอบค่าใช้จ่ายทั้งหมด ที่เกิดจากการเปลี่ยนสินค้าที่</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ชำรุดกับสินค้าที่มีคุณลักษณะไม่ด้อยกว่าที่ระบุในใบสั่งซื้อ แก้ไข</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ข้อเสียหาย และ/หรือข้อชำรุดบกพร่องใดๆ ของสินค้าที่ไม่ได้มี</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">สาเหตุมาจากการเสื่อมสภาพการใช้งาน  หรือการใช้ที่ไม่ถูกวิธี </fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt">
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">นอกเหนือจากการแก้ไข และ/หรือพยายามที่จะแก้ไขข้อเสียหาย</fo:inline>
                              </fo:block>
                           </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                           <fo:table-cell padding-end="5.4pt" padding-bottom="0.0pt" padding-start="5.4pt" padding-top="0.0pt" vertical-align="top" height="224.5pt" number-columns-spanned="1">
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">และ/หรือ การชำรุดบกพร่องของผู้ขายดังกล่าวแล้ว บริษัทยังคงมี</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">สิทธิในการเรียกร้องการเยียวยาความเสียหายอื่นใดได้อีกด้วย</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">11. การระงับข้อพิพาท – การบังคับใช้กฎหมาย</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">บริษัทและผู้ขายต้องพยายามระงับข้อพิพาทโดยการเจรจาต่อ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">รองระหว่างกัน โดยที่คู่สัญญาทั้งสองฝ่ายอาจตกลงรายละเอียด</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ที่เกี่ยวข้องกับการระงับข้อพิพาทก็ได้</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ข้อพิพาทต่างๆ ที่เกิดขึ้น หรือที่เกี่ยวเนื่องกับเงื่อนไขและข้อ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ตกลงทั่วไปฉบับนี้ให้ใช้กฎหมายไทยบังคับ</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">12. ทั่วไป</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">12.1 คู่สัญญาแต่ละฝ่ายตกลงที่จะรักษาข้อมูลทั้งหมด ที่ได้รับ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">จากคู่สัญญาอีกฝ่ายหนึ่งไว้เป็นความลับ ซึ่งรวมถึงแต่ไม่จำกัด</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">เฉพาะคุณลักษณะเฉพาะ ราคา และปริมาณสินค้า</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">12.2 ใบสั่งซื้อ การยกเลิก การเปลี่ยนแปลงและการยืนยันการ</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">สั่งซื้อภายใต้เงื่อนไขและข้อตกลงทั่วไปนี้สามารถจัดทำและส่ง</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">ออกในรูปแบบของอีเมล์ได้</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt" font-family="Tahoma" font-size="9.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">12.3 ในกรณีที่มีการขัดกันระหว่างเงื่อนไขและ ข้อตกลงทั่วไป</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline style-name="Normal" height="9.0pt" font-size="9.0pt" font-family-generic="swiss" font-family="Tahoma" white-space-collapse="false">นี้กับสัญญา (ถ้ามี) ให้ใช้ข้อความในสัญญาบังคับ</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" height="9.0pt"/>
                           </fo:table-cell>
                           <fo:table-cell padding-end="5.4pt" padding-bottom="0.0pt" padding-start="5.4pt" padding-top="0.0pt" vertical-align="top" height="224.5pt" number-columns-spanned="1">
                              <fo:block height="9.0pt"/>
                           </fo:table-cell>
                           <fo:table-cell padding-end="5.4pt" padding-bottom="0.0pt" padding-start="5.4pt" padding-top="0.0pt" vertical-align="top" height="224.5pt" number-columns-spanned="1">
                              <fo:block height="9.0pt"/>
                           </fo:table-cell>
                        </fo:table-row>

</fo:table-body>
</fo:table>

	 </xsl:when>
         <xsl:otherwise>
		
<fo:table table-layout="fixed" width="100%">
<fo:table-column column-width="100%"/>
<fo:table-body>
<fo:table-row> 

	<fo:table-cell xsl:use-attribute-sets="table.cell6" vertical-align="top" text-align="center">
	<fo:block font-weight="bold" font-size="14pt">
		  <fo:inline xsl:use-attribute-sets="legal_details_style">
		  <xsl:text>GENERAL TERMS AND CONDITIONS</xsl:text>
		  </fo:inline>
	</fo:block>
	</fo:table-cell>

</fo:table-row>
</fo:table-body>
</fo:table>

<fo:table space-before="20pt">
<fo:table-column column-width="49%"/>
<fo:table-column column-width="2%"/>
<fo:table-column column-width="49%"/>
<fo:table-body>

                        <fo:table-row>
                           <fo:table-cell padding-end="5.4pt" padding-bottom="0.0pt" padding-start="5.4pt" padding-top="0.0pt" vertical-align="top" height="688.0pt" number-columns-spanned="1">
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">1. Purpose</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">These General Terms and Conditions are applicable for all purchases, hire of work and/or any other services (hereinafter the "Goods") made by Company by issuance of the Purchase Order to Supplier.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">Goods shall include any documents, operation manuals, technical manuals, catalogues, specifications and/or other materials in relation to the Goods.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">2. Purchase Orders</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">Purchase Orders are valid only when they are signed by Company's authorised person. Supplier shall ensure that Company's authorised person has signed a received Purchase Order. Purchase Orders issued under any specific agreement shall refer to such agreement.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">3. Cancellation and Change</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">Company has the right to cancel a Purchase Order by giving Supplier a letter of cancellation at least five (5) calendar days before the agreed delivery date. The requirements stated in clause 2 (Purchase Orders) apply equally to the letter of cancellation.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">Company has the right to change any Purchase Order by giving Supplier a written change order at least five (5) calendar days before the agreed delivery date. Supplier shall deliver Goods in accordance with such modification(s), provided that such change does not place an unreasonable burden on Supplier. The requirements stated in clause 2 (Purchase Orders) apply equally to the change order.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">Supplier shall not claim any damages, fee, tariffs or any other form of economic compensation arising from such cancellation or change.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">4. Order Confirmation</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">Supplier shall either confirm receipt of the Purchase Order by a letter of confirmation or signing and returning the Purchase Order via facsimile within three (3) calendar days after receipt of the Purchase Order to Company. Other terms and conditions than these General Terms and Conditions do not apply unless they have been mutually and explicitly agreed in writing.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">5. Price – Payment – Invoicing</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">5.1 The price of the Goods shall be agreed prior to issuance of the Purchase Order (hereinafter the "Price"). No additional costs such as order/invoice fees, packing charges, other fees or taxes can subsequently be added to the Price. The Price is exclusive of Value Added Tax. </fo:inline>
                              </fo:block>
                           </fo:table-cell>
                           <fo:table-cell padding-end="5.4pt" padding-bottom="0.0pt" padding-start="5.4pt" padding-top="0.0pt" vertical-align="top" height="688.0pt" number-columns-spanned="1">
                              <fo:block height="10.0pt"/>
                           </fo:table-cell>
                           <fo:table-cell padding-end="5.4pt" padding-bottom="0.0pt" padding-start="5.4pt" padding-top="0.0pt" vertical-align="top" height="688.0pt" number-columns-spanned="1">
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">Supplier shall be responsible for the stamp duties.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">5.2 Unless otherwise agreed, the payment will be made within sixty (60) calendar days after Company's next payment date, after Company receives a correct invoice issued correctly under clause 5.3 and sent to the correct address under clause 5.4.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">Supplier cannot issue an invoice before completion of delivery as provided in clause 6. However, the invoice shall not be issued later than thirty (30) calendar days after Supplier has the right to issue the invoice.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">5.3 The invoice shall include Company's Purchase Order number, specifications and details in accordance with the Goods set out in the Purchase Order. Tax invoices must be prepared in accordance with the Revenue Code.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">5.4 Unless otherwise stated, the invoice shall be delivered to address set by Company.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">5.5 In the event that Company does not carry out payment in accordance with clause 5.2 and Company has no cause to withhold payment, Supplier may claim an interest payment on the delayed amount equivalent to 5 (five) % per annum.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">6. Delivery, Title and Risk</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">6.1 Delivery shall take place at the address stated in the Purchase Order, or as otherwise agreed in writing.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">Unless otherwise agreed, delivery condition shall be DDP ("Delivered Duty Paid") in accordance with Incoterms 2000 and delivery shall take place between 09:00 and 15:00 on workdays.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">Delivery is completed when all Goods have been delivered at the stated delivery address under this clause and completely inspected and accepted by Company. Partial delivery of a Purchase Order is not permitted unless otherwise agreed. A Purchase Order partially delivered after the stated delivery date shall be considered as delayed in its entirety.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">6.2 Upon completion of delivery under clause 6.1, title and risk shall pass from Supplier to Company.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">6.3 If the Goods is not delivered in accordance with this General Terms and Conditions, Supplier shall be liable for any out-of-pocket expenses (such as freight on Goods supplied in replacement of Goods originally delivered) incurred directly or indirectly by Company.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">7. Delay – Penalty – Remedies- Early Delivery</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">7.1 If a delivery is delayed from the agreed delivery date, Company has the right to claim a penalty from Supplier at</fo:inline>
                              </fo:block>
                           </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                           <fo:table-cell padding-end="5.4pt" padding-bottom="0.0pt" padding-start="5.4pt" padding-top="0.0pt" vertical-align="top" height="688.0pt" number-columns-spanned="1">
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">the rate of 0.25% of the Price of the Purchase Order per calendar day until delivery is completed under clause 6, subject to a maximum of 15% of the Price. Notwithstanding the above, in no event shall the daily penalty be less than THB 100. Advancing a claim for penalties does not hinder Company in advancing claims for other remedies as well.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">7.2 Company has the right to cancel the Purchase Order by giving Supplier a letter of cancellation and claim for damages if delivery has not taken place or is not likely to take place within sixty (60) calendar days of the delivery date (the penalty period to commence).</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">7.3 Supplier shall not deliver the Goods before the agreed delivery date (hereinafter "Early Delivery"), unless otherwise agreed. Although Early Delivery is completed under Clause 6, Supplier shall not invoice earlier than the stated delivery date under clause 5.2. Title and risk shall not pass from Supplier to Company until delivery is complete under clause 6. The right to cancel and/or change the Purchase Order in accordance with clause 3 is not affected by Early Delivery.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">8. Representations, Warranties and Undertakings</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">Supplier represents and warrants to Company that:</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">8.1 Ordered Goods shall be in accordance with agreed specifications. The Goods shall be of high standard and suitable for their intended purpose.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">8.2 Supplier has the unrestricted right to sell and/or provide the Goods.</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt"> </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">8.3 Supplier shall ensure that approval, documents, permits, licenses or consents required for the Goods (if any) will be properly delivered to Company.</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt"> </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">8.4 Supplier agrees not to assign or transfer any of its rights and/or obligations under this Purchase Order unless Company's prior written consent is obtained.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">Nonetheless, Supplier agrees that Company may assign and/or transfer any of its rights and/or obligations under this Purchase Order to any Affiliate of Company, without Supplier’s consent. In this regard, Supplier agrees to use its best effort to provide necessary cooperation to Company and take necessary or relevant action as requested by Company such that the assignment and/or transfer any of the rights and/or obligations under this Purchase Order and General Terms and Conditions are legally effective.  </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt"> </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">For the purpose of this clause;</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt"> </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">“Affiliate of Company” means a Subsidiary or a Parent Company of Company or any other Subsidiary of that Parent Company;</fo:inline>
                              </fo:block>
                           </fo:table-cell>
                           <fo:table-cell padding-end="5.4pt" padding-bottom="0.0pt" padding-start="5.4pt" padding-top="0.0pt" vertical-align="top" height="688.0pt" number-columns-spanned="1">
                              <fo:block height="10.0pt"/>
                           </fo:table-cell>
                           <fo:table-cell padding-end="5.4pt" padding-bottom="0.0pt" padding-start="5.4pt" padding-top="0.0pt" vertical-align="top" height="688.0pt" number-columns-spanned="1">
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">“Parent Company” means a company in respect of which Company is a Subsidiary; and</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt"> </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">For the term “Subsidiary”, a company (i.e. Company A) is considered to be a Subsidiary of another company (i.e. Company B) if: (i) Company B owns directly or indirectly more than 25% of the issued share capital of Company A; or (ii) Company A is a Subsidiary of another Subsidiary of Company B.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">8.5 [THIS CLAUSE APPLIES ONLY TO THE PURCHASE ORDER ISSUED BY PAYSBUY CO., LTD.] Supplier acknowledges and agrees that each of (i) the internal and/or external auditor(s) designated or to be designated by Company at Company’s sole discretion, (ii) the Bank of Thailand and/or any governmental authorities and/or (iii) the representative(s) of either of the foregoing (i) and/or (ii), shall have the right to inspect the Goods, business operation, internal control and/or any other relevant functions of Supplier at any time as each of (i) and/or (ii) and/or (iii) deems appropriate. </fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">For the purpose of this clause, Supplier agrees to give a full cooperation with any and all necessary and reasonable assistance to enable any of the persons in (i) to (iii) above to inspect and examine at any time without any objection or disruption.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">Such inspection under this clause, shall not relieve Supplier of any obligation or liability under this terms and conditions of this Purchase Order, nor shall it give any entitlement to Supplier to claim additional charges from Company.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">9. Lawful right – Indemnification</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">9.1 For a part of the Goods which its ownership has not been transferred, Supplier warrants that Company has an unlimited right to use, modify, sell or otherwise exploit or dispose of the Goods received.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">9.2 Ordered Goods shall be delivered free from all Encumbrances. Supplier shall indemnify Company against any claim by third party regarding the Encumbrances. Supplier shall immediately effect the removal of any such</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">Encumbrances. Supplier shall indemnify Company and cover all cost related to such removal of the Encumbrances.</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt"> </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">"Encumbrances" shall include all evictions, preferential rights, lines, right of retention, charges, servitude, encumbrances, mortgages, pledge or infringement of intellectual property of the third party.</fo:inline>
                              </fo:block>
                           </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                           <fo:table-cell padding-end="5.4pt" padding-bottom="0.0pt" padding-start="5.4pt" padding-top="0.0pt" vertical-align="top" height="490.5pt" number-columns-spanned="1">
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">10. Warranty Period</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">Supplier warrants that the ordered Goods are without faults and/or defects and the warranty applies for the period of two (2) years from the date when title and risk pass from Supplier to Company, unless otherwise agreed by the parties.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">Supplier shall, at its own cost and risk, replace the defected Goods with Goods that meet specifications not less than those provided in Purchase Order, amend, correct faults and/or defects that are not due to normal wear and tear or incorrect use. Irrespective of Suppliers corrections and/or attempts to correct faults and defects, Company retains the right to advance claims for other remedies.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">11. Conflict Resolution – Governing Law</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">Company and Supplier shall attempt to resolve their conflicts through negotiation. Detailed rules relating to conflict resolution can be agreed between the parties.</fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt"> </fo:inline>
                              </fo:block>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">Thai law shall apply to any disputes that arise out of or in connection with this General Terms and Conditions.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">12. Generals</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline xml:space="preserve" white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">12.1 Either party agrees to keep confidential any and all information received from the other party that after a cautious and loyal examination would be classified as confidential. This includes, but is not limited to specifications, the Price and volume of Goods procured.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">12.2 Purchase Orders, cancellation, changes orders and order confirmation under this General Terms and Conditions may be made and sent in a form of e-mail from Company's authorised person.</fo:inline>
                              </fo:block>
                              <fo:block orphans="2" widows="2" linefeed-treatment="preserve" start-indent="0.0pt" text-align="justify" padding-bottom="0.0pt" end-indent="0.0pt" padding-top="0.0pt" font-family="Times New Roman" font-size="10.0pt" height="10.0pt"/>
                              <fo:block> 
                                 <fo:inline white-space-collapse="false" style-name="Normal" height="10.0pt" font-family-generic="roman" font-family="Times New Roman" font-size="10.0pt">12.3 If there is any conflict between this General Terms and Conditions and the agreement (if any) governing the same matter, the agreement shall prevail.</fo:inline>
                              </fo:block>
                           </fo:table-cell>
                           <fo:table-cell padding-end="5.4pt" padding-bottom="0.0pt" padding-start="5.4pt" padding-top="0.0pt" vertical-align="top" height="490.5pt" number-columns-spanned="1">
                              <fo:block height="10.0pt"/>
                           </fo:table-cell>
                           <fo:table-cell padding-end="5.4pt" padding-bottom="0.0pt" padding-start="5.4pt" padding-top="0.0pt" vertical-align="top" height="490.5pt" number-columns-spanned="1">
                              <fo:block height="10.0pt"/>
                           </fo:table-cell>
                        </fo:table-row>

</fo:table-body>
</fo:table>

         </xsl:otherwise>
</xsl:choose>

</xsl:if>

<!-- Display text file data in new page -->

<xsl:if test="TEXT_FILE !='' ">
<fo:block xsl:use-attribute-sets="form_data"> 
	<xsl:value-of select="TEXT_FILE"/>
</fo:block>
</xsl:if>
<fo:block id="last-page" >
	
</fo:block>

</xsl:template>

<xsl:template name="NAME_TEMPLATE">
	<xsl:param name="FIRST_NAME"/>
	<xsl:param name="LAST_NAME"/>
	<xsl:param name="TITLE"/>
	<xsl:if test="$LAST_NAME != ''"> <xsl:value-of select="$LAST_NAME"/>,</xsl:if>  <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="$TITLE"/> <fo:leader leader-pattern="space" leader-length="2.0pt"/> <xsl:value-of select="$FIRST_NAME"/> 
</xsl:template>

<!-- Template for displaying the page numbers -->
<!-- bug#3836856: Template for displaying the page numbers at right bottom of the page -->
<xsl:template name="pageNumber">
	<xsl:variable name="po_page">
	   <xsl:value-of  select="$BOILER_PLATE_MESSAGES_OBJ[MESSAGE='PO_FO_PAGE'][1]/TEXT"/>
	</xsl:variable>
	<!-- Get the String before the PAGE_NUM token -->
	<xsl:variable name="string_before_page_num">
	    <xsl:value-of select="substring-before($po_page,'&amp;PAGE_NUM')"/>
	</xsl:variable>
	<!-- Get the String after the PAGE_NUM token -->
	<xsl:variable name="string_after_page_num">
	    <xsl:value-of select="substring-after($po_page,'&amp;PAGE_NUM')"/>
	</xsl:variable>
	<!-- If the String after PAGE_NUM token contains the token END_PAGE -->
	<xsl:if test="contains($string_after_page_num,'&amp;END_PAGE')">
	    <xsl:variable name="string_before_end_page">
		<xsl:value-of select="substring-before($string_after_page_num,'&amp;END_PAGE')"/>
	    </xsl:variable>
	    <xsl:variable name="string_after_end_page">
		<xsl:value-of select="substring-after($string_after_page_num,'&amp;END_PAGE')"/>
	    </xsl:variable>
	    <xsl:value-of select="$string_before_page_num"/><fo:page-number/>
	    <xsl:value-of select="$string_before_end_page"/>
	    <xsl:choose>
		<xsl:when test="$ROOT_OBJ/WITH_TERMS='N' or $ROOT_OBJ/IS_ATTACHED_DOC='Y'">
		    <fo:page-number-citation ref-id="last-page"/> 
		</xsl:when>
		<xsl:otherwise>
		    <fo:page-number-citation ref-id="eod"/> 
		</xsl:otherwise>
	    </xsl:choose>
	    <xsl:value-of select="$string_after_end_page"/>
	</xsl:if>
	<!-- If the String before PAGE_NUM token contains the token END_PAGE -->
	<xsl:if test="contains($string_before_page_num,'&amp;END_PAGE')">
	    <xsl:variable name="string_before_end_page">
		<xsl:value-of select="substring-before($string_before_page_num,'&amp;END_PAGE')"/>
	    </xsl:variable>
	    <xsl:variable name="string_after_end_page">
		<xsl:value-of select="substring-after($string_before_page_num,'&amp;END_PAGE')"/>
	    </xsl:variable>
	    <xsl:value-of select="$string_before_end_page"/>
	    <xsl:choose>
		<xsl:when test="$CON_TERMS_EXIST_FLAG='N' or $ROOT_OBJ/IS_ATTACHED_DOC='Y'">
		    <fo:page-number-citation ref-id="last-page"/> 
		</xsl:when>
		<xsl:otherwise>
		    <fo:page-number-citation ref-id="eod"/> 
		</xsl:otherwise>
	    </xsl:choose>
	    <xsl:value-of select="$string_after_end_page"/>
	    <fo:page-number/><xsl:value-of select="$string_after_page_num"/>
        </xsl:if> 
  </xsl:template>

<!--Bug#4088207: The wrppped attachment line is not aligned to start of the line. 
                 Created the template to display the attachment in new table -->
<xsl:template name="LINE_LOCATION_ATTACHMENT_TEMP">
        <xsl:param name="ATTACHMENT_TEXT"/>
        <fo:table>
        <fo:table-column column-width="3.5mm"/> <fo:table-column column-width="163.5mm"/>
        <fo:table-body>
        <fo:table-row >
        <fo:table-cell > <fo:block>  </fo:block> </fo:table-cell>
        <fo:table-cell > <fo:block> <xsl:value-of select ="$ATTACHMENT_TEXT"/> 
        </fo:block> </fo:table-cell>
        </fo:table-row>
        </fo:table-body>
        </fo:table> 
</xsl:template>

</xsl:stylesheet>