CREATE OR REPLACE PACKAGE tac_ifrs_interface_api IS

-- Author  : TM
-- Created : 28/08/2017
-- Purpose : Interface data from IFRS into ERP

d2c_format			CONSTANT VARCHAR2(50)	:=	'dd/mm/yyyy';
g_datechar_format	CONSTANT VARCHAR2(50)	:=	'dd/mm/yyyy hh24:mi:ss';
n2c_format			CONSTANT VARCHAR2(20)	:=	'999999999999.9999';
c2dt_format			CONSTANT VARCHAR2(50)	:=	'yyyy/mm/dd hh24:mi:ss';

g_gl_inbox		CONSTANT VARCHAR2(50)	:=	'IFRS_GL_INBOX';
g_gl_error		CONSTANT VARCHAR2(50)	:=	'IFRS_GL_ERROR';
g_gl_history	CONSTANT VARCHAR2(50)	:=	'IFRS_GL_HISTORY';

g_log_line		VARCHAR2(250)	:=	'+---------------------------------------------------------------------------+';

PROCEDURE interface_gl
(err_msg					OUT VARCHAR2
,err_code					OUT VARCHAR2
,p_set_of_books_id			IN	NUMBER
,p_file_path				IN	VARCHAR2
,p_file_name_format			IN	VARCHAR2
,p_file_name				IN	VARCHAR2	:=	NULL
,p_delimiter				IN	VARCHAR2	:=	'|'
,p_debug_flag				IN	VARCHAR2	:=	'N'
,p_purge_days				IN	NUMBER		:=	45
,p_insert_interface_table	IN	VARCHAR2	:=	'Y'
,p_file_date				IN	DATE		:=	SYSDATE
);

END tac_ifrs_interface_api;
/
CREATE OR REPLACE PACKAGE BODY tac_ifrs_interface_api IS

-- Author  : TM
-- Created : 28/08/2017
-- Purpose : Interface data from IFRS into ERP

--error_open_file	EXCEPTION;
error_output_path	EXCEPTION;
error_file_name		EXCEPTION;

PROCEDURE conc_wait
(p_conc_req_id	IN	NUMBER)
IS
	phase		VARCHAR2(30);
	status		VARCHAR2(30);
	dev_phase	VARCHAR2(30);
	dev_status	VARCHAR2(30);
	message		VARCHAR2(1000);
BEGIN
	COMMIT;
	IF fnd_concurrent.wait_for_request
		(p_conc_req_id	,10			,0
		,phase			,status		,dev_phase
		,dev_status		,message) THEN
		NULL;
	END IF;
END conc_wait;

PROCEDURE conc_wait
(p_conc_req_id	IN			NUMBER
,p_phase		OUT NOCOPY	VARCHAR2
,p_status		OUT NOCOPY	VARCHAR2
,p_message		OUT NOCOPY	VARCHAR2
,p_max_wait		IN			NUMBER	:=	60)
IS
	v_phase			VARCHAR2(30);
	v_status		VARCHAR2(30);
	v_dev_phase		VARCHAR2(30);
	v_dev_status	VARCHAR2(30);
	v_message		VARCHAR2(1000);
	v_wait_complete	BOOLEAN;
BEGIN
	COMMIT;
	IF p_conc_req_id > 0 THEN
		v_wait_complete	:=	fnd_concurrent.wait_for_request
								(request_id	=>	p_conc_req_id
								,INTERVAL	=>	10
								,max_wait	=>	p_max_wait
								-- out arguments
								,phase		=>	v_phase
								,status		=>	v_status
								,dev_phase	=>	v_dev_phase
								,dev_status	=>	v_dev_status
								,message	=>	v_message);
	END IF;
	p_phase		:=	v_phase;
	p_status	:=	v_status;
	p_message	:=	v_message;
END conc_wait;

PROCEDURE write_log
(p_msg	IN	VARCHAR2)
IS
BEGIN
	fnd_file.put_line(fnd_file.log, p_msg);
EXCEPTION
	WHEN OTHERS THEN
		NULL;
END write_log;

PROCEDURE write_output
(p_msg	IN	VARCHAR2)
IS
BEGIN
	fnd_file.put_line(fnd_file.output, p_msg);
EXCEPTION
	WHEN OTHERS THEN
		NULL;
END write_output;

PROCEDURE write_line
(p_file_handle	IN OUT	utl_file.file_type
,p_line_buff	IN		VARCHAR2)
IS
	user_error	VARCHAR2(255); -- to store translated file_error
BEGIN
	utl_file.put_line(p_file_handle, p_line_buff);
	utl_file.fflush(p_file_handle);
EXCEPTION
	WHEN utl_file.invalid_filehandle THEN
		user_error	:=	'Invalid filehandle';
		write_log(user_error);
	WHEN utl_file.invalid_operation THEN
		user_error	:=	'Invalid operation';
		write_log(user_error);
	WHEN utl_file.write_error THEN
		user_error	:=	'Write error';
		write_log(user_error);
	WHEN OTHERS THEN
		user_error	:=	'Error others.';
		write_log(user_error);
END write_line;

PROCEDURE write_line_nchar
(p_file_handle	IN OUT	utl_file.file_type
,p_line_buff	IN		NVARCHAR2)
IS
	user_error	VARCHAR2(255); -- to store translated file_error
BEGIN
	utl_file.put_line_nchar(p_file_handle, p_line_buff);
	utl_file.fflush(p_file_handle);
EXCEPTION
	WHEN utl_file.invalid_filehandle THEN
		user_error	:=	'Invalid filehandle';
		write_log(user_error);
	WHEN utl_file.invalid_operation THEN
		user_error	:=	'Invalid operation';
		write_log(user_error);
	WHEN utl_file.write_error THEN
		user_error	:=	'Write error';
		write_log(user_error);
	WHEN OTHERS THEN
		user_error	:=	'Error others.';
		write_log(user_error);
END write_line_nchar;

FUNCTION get_directory_path
(p_directory_name	IN	VARCHAR2)
RETURN VARCHAR2
IS
	v_path	VARCHAR2(250);
BEGIN
	SELECT	directory_path
	INTO	v_path
	FROM	all_directories
	WHERE	directory_name	=	p_directory_name;
	
	RETURN v_path;
EXCEPTION
	WHEN OTHERS THEN
		RETURN NULL;
END get_directory_path;

FUNCTION char_to_date
(p_date_char	IN	VARCHAR2
,p_format_mask	IN	VARCHAR2	:=	NULL)
RETURN DATE
IS
	v_date	DATE;
BEGIN
	IF (p_date_char IS NULL) THEN
		RETURN NULL;
	END IF;
	
	IF (p_format_mask IS NOT NULL) THEN
		v_date	:=	to_date(p_date_char, p_format_mask);
		RETURN v_date;
	END IF;
	
	-- Format Mask = dd/mm/yyyy hh24:mi:ss
	BEGIN
		v_date	:=	to_date(p_date_char, 'dd/mm/yyyy hh24:mi:ss');
		RETURN v_date;
	EXCEPTION
		WHEN OTHERS THEN
			NULL;
	END;
	
	-- Format Mask = dd/mm/yyyy
	BEGIN
		v_date	:=	to_date(p_date_char, 'dd/mm/yyyy');
		RETURN v_date;
	EXCEPTION
		WHEN OTHERS THEN
			NULL;
	END;
	
	-- Format Mask = dd/m/yyyy
	BEGIN
		v_date	:=	to_date(p_date_char, 'dd/m/yyyy');
		RETURN v_date;
	EXCEPTION
		WHEN OTHERS THEN
			NULL;
	END;
	
	-- Format Mask = d/mm/yyyy
	BEGIN
		v_date	:=	to_date(p_date_char, 'd/mm/yyyy');
		RETURN v_date;
	EXCEPTION
		WHEN OTHERS THEN
			NULL;
	END;
	
	-- Format Mask = d/m/yyyy
	BEGIN
		v_date	:=	to_date(p_date_char, 'd/m/yyyy');
		RETURN v_date;
	EXCEPTION
		WHEN OTHERS THEN
			RETURN p_date_char;
	END;
END char_to_date;

FUNCTION extract_data
(p_buffer		IN	VARCHAR2
,p_separator	IN	VARCHAR2
,p_pos_data		IN	NUMBER)
RETURN VARCHAR2
IS
	v_pos_start  NUMBER;
	v_pos_end	NUMBER;
	v_subsrt_len NUMBER;
	v_data	VARCHAR2(255);
BEGIN
	IF p_pos_data = 1 THEN
		v_pos_start	:=	1;
	ELSE
		v_pos_start	:=	instr(p_buffer, p_separator, 1, p_pos_data - 1) + 1;
	END IF;
	
	v_pos_end	:=	instr(p_buffer, p_separator, 1, p_pos_data);
	
	IF v_pos_end = 0 THEN
		v_subsrt_len	:=	length(p_buffer);
	ELSE
		v_subsrt_len	:=	v_pos_end - v_pos_start;
	END IF;
	
	v_data	:=	substr(p_buffer, v_pos_start, v_subsrt_len);
	
	RETURN v_data;
EXCEPTION
	WHEN OTHERS THEN
		RETURN NULL;
END extract_data;

FUNCTION split_gl_data
(p_line_buff	IN	VARCHAR2
,p_delimiter	IN	VARCHAR2	:=	'|'
,p_debug_flag	IN	VARCHAR2	:=	'N')
RETURN dtac_ifrs_gl_interface_temp%ROWTYPE
IS
	v_rec_data dtac_ifrs_gl_interface_temp%ROWTYPE;
BEGIN
	IF p_line_buff IS NULL THEN
		RETURN v_rec_data;
	END IF;
	v_rec_data.status					:=	extract_data(p_line_buff
														,p_delimiter
														,1);
	v_rec_data.set_of_books_id			:=	extract_data(p_line_buff
														,p_delimiter
														,2);
	v_rec_data.accounting_date			:=	char_to_date(
											extract_data(p_line_buff
														,p_delimiter
														,3)
														,'dd/mm/yyyy');
	v_rec_data.currency_code			:=	extract_data(p_line_buff
														,p_delimiter
														,4);
	v_rec_data.date_created				:=	char_to_date(
											extract_data(p_line_buff
														,p_delimiter
														,5)
														,'dd/mm/yy');
	v_rec_data.created_by				:=	extract_data(p_line_buff
														,p_delimiter
														,6);
	v_rec_data.actual_flag				:=	extract_data(p_line_buff
														,p_delimiter
														,7);
	v_rec_data.user_je_category_name	:=	extract_data(p_line_buff
														,p_delimiter
														,8);
	v_rec_data.user_je_source_name		:=	extract_data(p_line_buff
														,p_delimiter
														,9);
	v_rec_data.accounted_dr				:=	extract_data(p_line_buff
														,p_delimiter
														,10);
	v_rec_data.accounted_cr				:=	extract_data(p_line_buff
														,p_delimiter
														,11);
	v_rec_data.currency_conversion_date	:=	char_to_date(
											extract_data(p_line_buff
														,p_delimiter
														,12)
														,g_datechar_format);
	v_rec_data.currency_conversion_rate	:=	extract_data(p_line_buff
														,p_delimiter
														,13);
	v_rec_data.entered_dr				:=	extract_data(p_line_buff
														,p_delimiter
														,14);
	v_rec_data.entered_cr				:=	extract_data(p_line_buff
														,p_delimiter
														,15);
	v_rec_data.je_line_num				:=	extract_data(p_line_buff
														,p_delimiter
														,16);
	v_rec_data.period_name				:=	upper(
											extract_data(p_line_buff
														,p_delimiter
														,17));
	v_rec_data.reference1				:=	extract_data(p_line_buff
														,p_delimiter
														,18);
	v_rec_data.reference4				:=	extract_data(p_line_buff
														,p_delimiter
														,19);
	v_rec_data.reference5				:=	extract_data(p_line_buff
														,p_delimiter
														,20);
	v_rec_data.segment1					:=	extract_data(p_line_buff
														,p_delimiter
														,21);
	v_rec_data.segment11				:=	extract_data(p_line_buff
														,p_delimiter
														,22);
	v_rec_data.segment10				:=	extract_data(p_line_buff
														,p_delimiter
														,23);
	v_rec_data.segment12				:=	extract_data(p_line_buff
														,p_delimiter
														,24);
	v_rec_data.segment13				:=	extract_data(p_line_buff
														,p_delimiter
														,25);
	v_rec_data.segment14				:=	extract_data(p_line_buff
														,p_delimiter
														,26);
	v_rec_data.segment18				:=	extract_data(p_line_buff
														,p_delimiter
														,27);
	v_rec_data.segment19				:=	extract_data(p_line_buff
														,p_delimiter
														,29);	-- Resequence column
	v_rec_data.segment21				:=	extract_data(p_line_buff
														,p_delimiter
														,28);	-- Resequence column
	v_rec_data.reference21				:=	extract_data(p_line_buff
														,p_delimiter
														,30);
	
	IF p_debug_flag = 'Y' THEN
		write_log(g_log_line);
		write_log('Line data = ' || p_line_buff);
		/*
		write_log('		company_code  = ' || v_rec_data.company_code);
		write_log('		transaction_type  = ' || v_rec_data.transaction_type);
		write_log('		grn_number  = ' || v_rec_data.grn_number);
		write_log('		po_number  = ' || v_rec_data.po_number);
		write_log('		release_number  = ' || v_rec_data.release_number);
		write_log('		po_line_number  = ' || v_rec_data.po_line_number);
		write_log('		product_sku_code  = ' || v_rec_data.product_sku_code);
		write_log('		invoice_number  = ' || v_rec_data.invoice_number);
		write_log('		rate_date  = ' || v_rec_data.rate_date);
		write_log('		qty  = ' || v_rec_data.qty);
		write_log('		inventory_org_code  = ' || v_rec_data.inventory_org_code);
		write_log('		sub_inventory  = ' || v_rec_data.sub_inventory);
		write_log('		locator  = ' || v_rec_data.locator);
		write_log('		create_by  = ' || v_rec_data.create_by);
		write_log('		creation_date  = ' || v_rec_data.creation_date);
		*/
	END IF;
	
	RETURN v_rec_data;
EXCEPTION
	WHEN OTHERS THEN
		--write_log('Error @tac_ap_interface_util_smt.split_grn_data : ' || sqlcode || ' - ' || sqlerrm);
		v_rec_data.error_msg	:=	SQLCODE || ' - ' || SQLERRM;
		RETURN v_rec_data;
END split_gl_data;

PROCEDURE move_data_file
(p_source_file	IN	VARCHAR2
,p_dest_path	IN	VARCHAR2)
IS
	v_movefile_req_id	NUMBER;
	v_phase				VARCHAR2(100);
	v_status			VARCHAR2(100);
	v_message			VARCHAR2(1000);
BEGIN
	write_log(' ');
	write_log('Moving File ' || p_source_file || ' to ' || p_dest_path);
	
	v_movefile_req_id	:=	fnd_request.submit_request
								(application	=>	'FND'
								,program		=>	'TACFND_MVFILE'
								,argument1		=>	p_source_file
								,argument2		=>	p_dest_path);
	COMMIT;
	write_log('Concurrent Move File is ' || v_movefile_req_id);
	
	IF (v_movefile_req_id > 0) THEN
		conc_wait(v_movefile_req_id, v_phase, v_status, v_message, 60);
		IF (v_status = 'E') THEN
			write_log('Move file from ' || p_source_file || ' to ' || p_dest_path || ' failed.');
		ELSE
			write_log('Move file from ' || p_source_file || ' to ' || p_dest_path || ' successful.');
		END IF;
	ELSE
		write_log('Move file from ' || p_source_file || ' to ' || p_dest_path ||
					' failed, No concurrent program [TACFND_MVFILE].');
	END IF;
	write_log(' ');
EXCEPTION
	WHEN OTHERS THEN
		write_log('Move file from ' || p_source_file || ' to ' ||
					p_dest_path || ' failed with error ' || SQLERRM);
END move_data_file;

FUNCTION validate_gl
(p_request_id	IN	NUMBER)
RETURN VARCHAR2
IS
	v_result	VARCHAR2(1000)	:=	NULL;
BEGIN
	IF (p_request_id IS NOT NULL) THEN
		RETURN '';
	ELSE
		RETURN 'ERROR : No request identify.';
	END IF;
EXCEPTION
	WHEN OTHERS THEN
		v_result	:=	'ERROR : ' || SQLERRM;
		RETURN v_result;
END validate_gl;

PROCEDURE interface_gl
(err_msg					OUT VARCHAR2
,err_code					OUT VARCHAR2
,p_set_of_books_id			IN	NUMBER
,p_file_path				IN	VARCHAR2
,p_file_name_format			IN	VARCHAR2
,p_file_name				IN	VARCHAR2	:=	NULL
,p_delimiter				IN	VARCHAR2	:=	'|'
,p_debug_flag				IN	VARCHAR2	:=	'N'
,p_purge_days				IN	NUMBER		:=	45
,p_insert_interface_table	IN	VARCHAR2	:=	'Y'
,p_file_date				IN	DATE		:=	SYSDATE)
IS
	CURSOR c_books IS
		SELECT	lv.lookup_code, lv.attribute1 sob_code_in_file, lv.attribute2 sob_id
		FROM	fnd_lookup_values lv
		WHERE	lv.lookup_type	=	'TAC_IFRS_INTERFACE'
		AND		lv.attribute2	=	NVL(p_set_of_books_id,lv.attribute2)
		ORDER BY lv.lookup_code;

	v_file_name				VARCHAR2(100);
	v_date					DATE	:=	p_file_date;
	v_file_handle			utl_file.file_type;
	v_line_data				NVARCHAR2(32767);
	v_line_count			NUMBER	:=	0;
	v_total_line_count		NUMBER	:=	0;
	
	v_rec_data				dtac_ifrs_gl_interface_temp %ROWTYPE;
	v_insert_error_flag		VARCHAR2(1)	:=	'N';
	
	v_request_id			NUMBER	:=	fnd_profile.value('CONC_REQUEST_ID');
	v_creation_date			DATE	:=	SYSDATE;
	v_debug_flag			VARCHAR2(10);
	v_delimiter				VARCHAR2(10);
	
	v_login_id				NUMBER	:=	fnd_profile.value('LOGIN_ID');
	v_user_id				NUMBER	:=	fnd_profile.value('USER_ID');
	v_header_interface_id	NUMBER;
	v_group_interface_id	NUMBER;
	v_interface_transaction_id		NUMBER;
--	v_receipt_group_interface_id	NUMBER;
	v_created_by			NUMBER;
	v_employee_id			NUMBER;
	v_submit_request_id		NUMBER;
	
	v_insert_itf_table		VARCHAR2(10)	:=	'Y';
	
	v_count_gl				NUMBER	:=	0;
	
BEGIN
	err_code		:=	'2';
	v_delimiter		:=	p_delimiter;
	v_debug_flag	:=	p_debug_flag;
	
	write_log(' ');
	write_log('+---------------------------------------------------------------------------+');
	write_log('Start Concurrent GL Interface from IFRS ');
	write_log('Start Date: ' ||
				to_char(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
	write_log(' ');
	write_log('Agruments');
	write_log('-----------');
	write_log('p_set_of_books_id        =  ' || p_set_of_books_id);
	write_log('p_file_path              =  ' || p_file_path);
	write_log('p_file_name_format       =  ' || p_file_name_format);
	write_log('p_file_name              =  ' || p_file_name);
	write_log('p_delimiter              =  ' || p_delimiter);
	write_log('p_debug_flag             =  ' || p_debug_flag);
	write_log('p_purge_days             =  ' || p_purge_days);
	write_log('p_insert_interface_table =  ' || p_insert_interface_table);
	write_log('p_file_date              =  ' || TO_DATE(p_file_date,'DD/MM/YYYY'));
	
	write_log('+----------------------------------------------------------------------------+');
	
	-- Start import data from text file
	v_insert_itf_table	:=	nvl(p_insert_interface_table, 'Y');
	IF p_file_path IS NULL THEN
		RAISE utl_file.invalid_path;
	END IF;
	
	BEGIN
		DELETE	dtac_ifrs_gl_interface_temp tmp
		WHERE	tmp.interface_date <= (SYSDATE - nvl(p_purge_days, 45));
		COMMIT;
	EXCEPTION
		WHEN OTHERS THEN
			NULL;
	END;
	
	SAVEPOINT before_insert_temp_all;
	FOR r_books IN c_books
	LOOP
		IF (p_file_name IS NOT NULL) THEN
			v_file_name	:=	p_file_name;
		ELSE
			IF p_file_name_format IS NULL THEN
				RAISE utl_file.invalid_filename;
			END IF;
			v_file_name	:=	REPLACE(p_file_name_format,
								'$DATE$',
								TRIM(to_char(v_date, 'yyyymmdd')));
			v_file_name	:=	REPLACE(v_file_name,
								'$SOB_FILENAME$',
								r_books.sob_code_in_file);
		END IF;
		
		write_log(g_log_line);
		write_log('Import GL data from file ' ||
					get_directory_path(g_gl_inbox) || '/' || v_file_name);
		write_log(' ');
		
		BEGIN
			--v_file_handle	:=	utl_file.fopen(location	=>	p_file_path, filename	=>	v_file_name, open_mode	=>	'r');
			
			v_file_handle	:=	utl_file.fopen_nchar(location		=>	p_file_path
													,filename		=>	v_file_name
													,open_mode		=>	'r'
													,max_linesize	=>	4000);
			utl_file.fclose(v_file_handle);
		EXCEPTION
			WHEN OTHERS THEN
				write_log(' Error first read : ' || SQLERRM);
				write_log(' ================');
				write_log('p_file_path = ' || p_file_path);
				write_log('v_file_name = ' || v_file_name);
				write_log(' ================');
		END;
		--v_file_handle	:=	utl_file.fopen(location	=>	p_file_path, filename	=>	v_file_name, open_mode	=>	'r');
		v_file_handle	:=	utl_file.fopen_nchar(location		=>	p_file_path
												,filename		=>	v_file_name
												,open_mode		=>	'r'
												,max_linesize	=>	4000);
		
		-- Read first line for header (ignore BOM byte)
		utl_file.get_line_nchar	(file	=>	v_file_handle
								,buffer	=>	v_line_data);
		v_line_data		:=	'';
		v_line_count	:=	0;	-- Clear number of lines
		
		SAVEPOINT before_insert_temp;
		LOOP
			BEGIN
				--utl_file.get_line(file	=>	v_file_handle, buffer	=>	v_line_data);
				utl_file.get_line_nchar	(file	=>	v_file_handle
										,buffer	=>	v_line_data);
				v_line_data	:=	REPLACE( REPLACE(v_line_data, chr(10), '') ,chr(13) ,'');
				IF (v_line_data = 'END' OR v_line_data IS NULL) THEN
					EXIT;
				END IF;
				v_line_count	:=	v_line_count + 1;
				
				v_rec_data		:=	split_gl_data(p_line_buff	=>	v_line_data
												 ,p_delimiter	=>	v_delimiter
												 ,p_debug_flag	=>	v_debug_flag);
				IF v_rec_data.set_of_books_id = r_books.sob_id THEN
					BEGIN
						v_rec_data.request_id		:=	v_request_id;
						v_rec_data.interface_date	:=	SYSDATE;
						
						INSERT INTO dtac_ifrs_gl_interface_temp
							(status
							,set_of_books_id
							,accounting_date
							,currency_code
							,date_created
							,created_by
							,actual_flag
							,user_je_category_name
							,user_je_source_name
							,accounted_dr
							,accounted_cr
							,currency_conversion_date
							,currency_conversion_rate
							,entered_dr
							,entered_cr
							,je_line_num
							,period_name
							,reference1
							,reference4
							,reference5
							,segment1
							,segment11
							,segment10
							,segment12
							,segment13
							,segment14
							,segment18
							,segment19
							,segment21
							,reference21
							,request_id
							,interface_date
							,validate_status
							,error_msg)
						VALUES
							(v_rec_data.status
							,v_rec_data.set_of_books_id
							,v_rec_data.accounting_date
							,v_rec_data.currency_code
							,v_rec_data.date_created
							,v_rec_data.created_by
							,v_rec_data.actual_flag
							,v_rec_data.user_je_category_name
							,v_rec_data.user_je_source_name
							,v_rec_data.accounted_dr
							,v_rec_data.accounted_cr
							,v_rec_data.currency_conversion_date
							,v_rec_data.currency_conversion_rate
							,v_rec_data.entered_dr
							,v_rec_data.entered_cr
							,v_rec_data.je_line_num
							,v_rec_data.period_name
							,v_rec_data.reference1
							,v_rec_data.reference4
							,v_rec_data.reference5
							,v_rec_data.segment1
							,v_rec_data.segment11
							,v_rec_data.segment10
							,v_rec_data.segment12
							,v_rec_data.segment13
							,v_rec_data.segment14
							,v_rec_data.segment18
							,v_rec_data.segment19
							,v_rec_data.segment21
							,v_rec_data.reference21
							,v_rec_data.request_id
							,v_rec_data.interface_date
							,v_rec_data.validate_status
							,v_rec_data.error_msg);
						
						write_log('Line#' || to_char(v_line_count, 90000) || ' [ OK]	=>	' || v_line_data);
					EXCEPTION
						WHEN OTHERS THEN
							v_insert_error_flag	:=	'Y';
							write_log('Line#' || to_char(v_line_count, 90000) || ' [ERR]	=>	' || v_line_data);
							write_output('Line#' || to_char(v_line_count, 90000) || ' [ERR]	=>	' || SQLERRM);
					END;
				ELSE	-- SOB ID specified in text file is not matched to the setup
					v_insert_error_flag	:=	'Y';
					write_log('Line#' || to_char(v_line_count, 90000) || ' [ERR]	=>	Incorrect SOB_ID ' || v_line_data);
					write_output('Line#' || to_char(v_line_count, 90000) || ' [ERR]	=>	Incorrect SOB_ID ' || v_rec_data.set_of_books_id);
				END IF;
				EXCEPTION
					WHEN no_data_found THEN
						EXIT;
					WHEN OTHERS THEN
						v_insert_error_flag	:=	'Y';
						write_log('Read data from text file error :  ' || SQLERRM);
						EXIT;
				END;
		END LOOP;

		v_total_line_count	:=	v_total_line_count + v_line_count;	-- Total line count from all files
		-- check open and close
		IF (utl_file.is_open(file	=>	v_file_handle)) THEN
			utl_file.fclose(file	=>	v_file_handle);
		END IF;
		
		IF (v_insert_error_flag = 'Y') THEN
			ROLLBACK TO before_insert_temp;
			write_output(' ');
			write_output(' ');
			write_output(g_log_line);
			
			write_log(' ');
			write_log('tac_ifrs_interface_api.interface_gl @import_data exception when insert data. See output for error detail.');
			
			err_msg	:=	'Error tac_ifrs_interface_api.interface_gl @import_data : Some record error during insert to table. See output for detail.';
			err_code	:=	'2';
/*			move_data_file(get_directory_path(g_gl_inbox) || '/' || v_file_name,
							get_directory_path(g_gl_error));
			
			RETURN;
		ELSE
			COMMIT;
			move_data_file(get_directory_path(g_gl_inbox) || '/' || v_file_name,
							get_directory_path(g_gl_history));*/
		END IF;
		-- End of import data from text file
		IF (p_file_name IS NOT NULL) THEN
			EXIT;	-- specify filename
		END IF;
	END LOOP;

-- Move file (if error / if not error)
	FOR r_books IN c_books
	LOOP
		IF (p_file_name IS NOT NULL) THEN
			v_file_name	:=	p_file_name;
		ELSE
			IF p_file_name_format IS NULL THEN
				RAISE utl_file.invalid_filename;
			END IF;
			v_file_name	:=	REPLACE(p_file_name_format,
								'$DATE$',
								TRIM(to_char(v_date, 'yyyymmdd')));
			v_file_name	:=	REPLACE(v_file_name,
								'$SOB_FILENAME$',
								r_books.sob_code_in_file);
		END IF;

		IF (v_insert_error_flag = 'Y') THEN
			move_data_file(get_directory_path(g_gl_inbox) || '/' || v_file_name,
							get_directory_path(g_gl_error));
		ELSE
			move_data_file(get_directory_path(g_gl_inbox) || '/' || v_file_name,
							get_directory_path(g_gl_history));
		END IF;
		-- End of import data from text file
		IF (p_file_name IS NOT NULL) THEN
			EXIT;	-- specify filename
		END IF;
	END LOOP;

	IF (v_insert_error_flag = 'Y') THEN
		ROLLBACK TO before_insert_temp_all;
		RETURN;
/*	ELSE
		COMMIT;*/
	END IF;

	SAVEPOINT before_insert_interface;
	v_insert_error_flag	:=	'N';
	-- validate GL
	IF (validate_gl(v_request_id) IS NOT NULL) THEN
		write_log('Validate : Failed.');
	ELSE
		write_log('Validate : Pass.');
		IF (v_insert_itf_table = 'Y') THEN
			BEGIN
				INSERT INTO gl_interface
					(status
					,set_of_books_id
					,accounting_date
					,currency_code
					,date_created
					,created_by
					,actual_flag
					,user_je_category_name
					,user_je_source_name
					,accounted_dr
					,accounted_cr
					,currency_conversion_date
					,currency_conversion_rate
					,entered_dr
					,entered_cr
					,je_line_num
					,period_name
					,reference1
					,reference4
					,reference5
					,segment1
					,segment11
					,segment10
					,segment12
					,segment13
					,segment14
					,segment18
					,segment19
					,segment21
					,reference21)
				SELECT status
					,set_of_books_id
					,accounting_date
					,currency_code
					,date_created
					,created_by
					,actual_flag
					,user_je_category_name
					,user_je_source_name
					,accounted_dr
					,accounted_cr
					,currency_conversion_date
					,currency_conversion_rate
					,entered_dr
					,entered_cr
					,je_line_num
					,period_name
					,reference1
					,reference4
					,reference5
					,segment1
					,segment11
					,segment10
					,segment12
					,segment13
					,segment14
					,segment18
					,segment19
					,segment21
					,reference21
				FROM	dtac_ifrs_gl_interface_temp tmp
				WHERE	tmp.request_id = v_request_id
				AND		tmp.status = 'NEW';
				v_count_gl	:=	SQL%ROWCOUNT;
--				COMMIT;
			EXCEPTION
				WHEN OTHERS THEN
					v_insert_error_flag	:=	'Y';
					ROLLBACK TO before_insert_interface;
					write_output(' ');
					write_output(' ');
					write_output(SQLERRM);
						
					write_log(' ');
					write_log('tac_ifrs_interface_api.interface_gl @insert interface exception when insert data to gl_interface table. See output for error detail.');
						
					err_msg		:=	'Error tac_ifrs_interface_api.interface_gl @insert interface : Some record during insert data to gl_interface table. See output for detail.';
					err_code	:=	'2';
					RETURN;
			END;
		END IF;
	END IF;

	IF (v_count_gl = v_total_line_count OR v_insert_itf_table = 'N') THEN
		err_code	:=	'0';
	ELSE
		err_code	:=	'2';
		write_output(' ');
		write_output('ERROR : Total records that insert to gl_interface table are not equal (' ||
						v_count_gl || '/' || v_line_count ||
						' with text file.');
			
		write_log(' ');
		write_log('ERROR : Total records that insert to gl_interface table are not equal (' ||
					v_count_gl || '/' || v_line_count ||
					' with text file.');
	END IF;
	COMMIT;
	write_log('+----------------------------------------------------------------------------+');

EXCEPTION
	WHEN utl_file.invalid_path THEN
		write_output('Invalid File Location');
		write_log('Invalid File Location');
		raise_application_error(-20052, 'Invalid File Location');
	WHEN utl_file.invalid_operation THEN
		write_output('Invalid File Operation');
		write_log('Invalid Operation');
		raise_application_error(-20054, 'Invalid Operation');
	WHEN utl_file.read_error THEN
		write_output('Read File Error');
		write_log('Read Error');
		raise_application_error(-20055, 'Read Error');
	WHEN utl_file.invalid_maxlinesize THEN
		write_output('Line Size Exceeds 4000');
		write_log('Line Size Exceeds 4000');
		raise_application_error(-20060, 'Line Size Exceeds 4000');
	WHEN utl_file.invalid_filename THEN
		write_output('Invalid File Name');
		write_log('Invalid File Name');
		raise_application_error(-20061, 'Invalid File Name');
	WHEN utl_file.access_denied THEN
		write_output('File Access Denied By');
		write_log('File Access Denied By');
		raise_application_error(-20062, 'File Access Denied By');
	WHEN OTHERS THEN
		IF (utl_file.is_open(file	=>	v_file_handle)) THEN
			utl_file.fclose(file	=>	v_file_handle);
		END IF;
		err_code	:=	'2';
		err_msg		:=	SQLERRM;
		write_output(SQLERRM);
		raise_application_error(-20011, SQLCODE || ': ' || SQLERRM);
END interface_gl;

END tac_ifrs_interface_api;
/
