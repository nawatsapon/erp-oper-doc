-- Add/modify columns 
alter table SC_TRANS_TYPE add ccb_cust_req_flag varchar2(1);
-- Add comments to the columns 
comment on column SC_TRANS_TYPE.ccb_cust_req_flag
  is 'Y=Require CCB Customer in this transaction type';


-- Add/modify columns 
alter table SC_TRANSACTION add ref_ccb_cust_flag varchar2(1);
alter table SC_TRANSACTION add ifrs_inf_flag varchar2(1);
alter table SC_TRANSACTION add ifrs_inf_code varchar2(30);
alter table SC_TRANSACTION add ccb_cust_no varchar2(30);
-- Add comments to the columns 
comment on column SC_TRANSACTION.ref_ccb_cust_flag
  is 'Y=This transaction is referred to CCB Customer';
comment on column SC_TRANSACTION.ifrs_inf_flag
  is 'Y=This transaction is interfaced to IFRS already';
comment on column SC_TRANSACTION.ifrs_inf_code
  is 'Process ID generated interface file to IFRS';
comment on column SC_TRANSACTION.ccb_cust_no
  is 'Customer Code from CCB';

-- IFRS Interface
CREATE TABLE sc_ifrs_interface
(OU_CODE			VARCHAR2(3)
,SUBINV_CODE		VARCHAR2(10)
,DOCUMENT_TYPE		VARCHAR2(3)
,DOCUMENT_NUMBER	VARCHAR2(15)
,DOCUMENT_DATE		DATE
,TRANS_CODE			VARCHAR2(10)
,TRANS_DESC			VARCHAR2(100)
,TRANSACTION_TYPE	VARCHAR2(3)
,SUBSCRIBER_NUMBER	VARCHAR2(50)
,CUSTOMER_NUMBER	VARCHAR2(13)
,INVOICE_AMOUNT		NUMBER(12,2)
,PROCESS_ID			NUMBER
,PROCESS_DATE		DATE);
grant select on sc_ifrs_interface to SELECTED_POS_ROLE;

-- IFRS Interface Error Log
create table sc_ifrs_inf_error_log
(process_id				NUMBER
,process_date			DATE
,step					NUMBER
,step_desc				VARCHAR2(400)
,seq					NUMBER
,log_code				VARCHAR2(100)
,log_desc				VARCHAR2(400)
,process_flag			VARCHAR2(20)
,inf_lock_flag			VARCHAR2(20)
--,inf_transaction_mode	VARCHAR2(20)
,inf_upd_by				VARCHAR2(15)
,inf_upd_date			DATE
,inf_upd_pgm			VARCHAR2(10)
);
-- Grant/Revoke object privileges 
grant select on sc_ifrs_inf_error_log to SELECTED_POS_ROLE;

-- Create table
create global temporary table tmp_process_csv
(
  process_id    NUMBER not null,
  seq           NUMBER not null,
  message       VARCHAR2(4000),
  generate_date DATE
)
on commit preserve rows;
-- Create/Recreate indexes 
create index tmp_process_csv_idx on tmp_process_csv (process_id, seq);
