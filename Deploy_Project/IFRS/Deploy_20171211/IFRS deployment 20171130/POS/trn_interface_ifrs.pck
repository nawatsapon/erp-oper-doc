CREATE OR REPLACE PACKAGE BODY TRN_INTERFACE_IFRS
IS
/*
Date		By				Description
----------	--------------	---------------------------------------------
22/08/2017	TM				Created
*/

C_JOB_NAME			CONSTANT VARCHAR2(1000)	:=	'Interface to IFRS';
GR_LOG				sc_ifrs_inf_error_log%ROWTYPE;
G_CNT_ROW_RETURN	NUMBER := 0;
G_CNT_ROW_ERROR		NUMBER := 0;
G_TOTAL_AMOUNT		NUMBER := 0;
G_INF_SEQ			NUMBER := 1;
G_PROCESSID			PB_PARAMETER_SETUP.PARAMETER_VALUE%TYPE;

G_OU_CODE			su_organize.ou_code%TYPE;
G_TRX_BACK_DAYS		NUMBER;
G_START_DATE		DATE;
G_END_DATE			DATE;
G_LOG_BACK_DAYS		NUMBER;
G_N_NO				NUMBER;
G_N_ID				NUMBER;
G_JOB_BROKEN		VARCHAR2(10);

e_abnormal_end		EXCEPTION;

G_DIRECTORY			VARCHAR2(4000);
G_RESULT_STATUS		VARCHAR2(4000);
G_FILENAME			VARCHAR2(4000);

CURSOR c_data
		(i_ou_code			IN	VARCHAR2
		,i_ifrs_inf_flag	IN	VARCHAR2
		,i_process_id		IN	NUMBER	:=	NULL) IS
	SELECT	t.*, DECODE(doc_type,'1',1,'3',-1) multiplier
	FROM	sc_transaction t
	WHERE	ou_code				LIKE	i_ou_code 
	AND		doc_type			IN ('1','3')	-- 1=Invoice, 3=C/N
	AND		NVL(ac_type,'N')	=	'N'
	AND		upd_date BETWEEN G_START_DATE AND TRUNC(G_END_DATE)+0.99999
	AND		NVL(ifrs_inf_flag,'N') LIKE NVL(i_ifrs_inf_flag,'%')
	AND		(i_process_id IS NULL OR t.ifrs_inf_code = i_process_id)
	AND		EXISTS (SELECT	'a'
					FROM	sc_trans_dtl d
					WHERE	d.OU_CODE		=	t.OU_CODE
					AND		d.SUBINV_CODE	=	t.SUBINV_CODE
					AND		d.DOC_NO		=	t.DOC_NO
					AND		d.DOC_TYPE		=	t.DOC_TYPE
					AND		d.trans_code IN (SELECT	tt.trans_code
											FROM	sc_trans_type tt
											WHERE	tt.ou_code				=	t.ou_code
											AND		tt.ccb_cust_req_flag	=	'Y')
					)
	ORDER BY t.ou_code, t.doc_type, t.doc_no
	FOR UPDATE NOWAIT;

-- Start Forward Declarations
PROCEDURE write_log
(p_write	sc_ifrs_inf_error_log%ROWTYPE);

PROCEDURE remove_process
(i_upd  VARCHAR2
,i_pgm  VARCHAR2);
-- End Forward Declarations

------------------------------------------
PROCEDURE validate
(i_upd		IN	VARCHAR2
,i_pgm		IN	VARCHAR2
,P_RESULT	OUT	VARCHAR2)
IS
	CNT   NUMBER := 0;
BEGIN
	P_RESULT := 'COMPLETE';
	
	CNT := 0;

	-->> Initial log
	GR_LOG.PROCESS_ID		:=	G_PROCESSID;
	GR_LOG.INF_UPD_BY		:=	i_upd;
	GR_LOG.INF_UPD_PGM		:=	i_pgm;

	-->> Lock Transaction Pending.
	GR_LOG.STEP				:=	2;
	GR_LOG.STEP_DESC		:=	'Validate data';
	GR_LOG.PROCESS_FLAG		:=	'Prepare Data';
	GR_LOG.INF_LOCK_FLAG	:=	'Lock';

	SELECT	NVL(COUNT(*), 0)
	INTO	CNT
	FROM	sc_transaction t
	WHERE	ou_code				LIKE	G_OU_CODE 
	AND		doc_type			IN ('1','3')	-- 1=Invoice, 3=C/N
	AND		NVL(ac_type,'N')	=	'N'
	AND		upd_date BETWEEN G_START_DATE AND TRUNC(G_END_DATE)+0.99999
	AND		NVL(ifrs_inf_flag,'N') LIKE 'W'
	AND		t.ifrs_inf_code		=	G_PROCESSID
	AND		EXISTS (SELECT	'a'
					FROM	sc_trans_dtl d
					WHERE	d.OU_CODE		=	t.OU_CODE
					AND		d.SUBINV_CODE	=	t.SUBINV_CODE
					AND		d.DOC_NO		=	t.DOC_NO
					AND		d.DOC_TYPE		=	t.DOC_TYPE
					AND		d.trans_code IN (SELECT	tt.trans_code
											FROM	sc_trans_type tt
											WHERE	tt.ou_code				=	t.ou_code
											AND		tt.ccb_cust_req_flag	=	'Y')
					)
	-- Error Checking
	AND		(t.ou_code IS NULL OR t.doc_no IS NULL OR t.cust_tel IS NULL OR t.ccb_cust_no IS NULL)
	;
		
	IF NVL(CNT, 0) > 0 THEN
		P_RESULT := 'Primary Key contains NULL value';
	ELSE
		P_RESULT := 'Complete validation : Primary Key';
	END IF;

	IF P_RESULT NOT LIKE 'Complete%' THEN
		GR_LOG.PROCESS_DATE		:=	SYSDATE;
		GR_LOG.SEQ				:=	G_INF_SEQ;
		GR_LOG.LOG_CODE			:=	G_PROCESSID || ' Validate Error';
		GR_LOG.LOG_DESC			:=	'Auto revert status ' || TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS') ||
									P_RESULT;
		GR_LOG.INF_UPD_DATE		:=	SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;

		REMOVE_PROCESS('AUTO', 'BACK-JOB');
		G_RESULT_STATUS	:=	'FAILED';
	ELSE
		SELECT	NVL(COUNT(*), 0)
		INTO	CNT
		FROM	sc_transaction t
		WHERE	ou_code				LIKE	G_OU_CODE 
		AND		doc_type			IN ('1','3')	-- 1=Invoice, 3=C/N
		AND		NVL(ac_type,'N')	=	'N'
		AND		upd_date BETWEEN G_START_DATE AND TRUNC(G_END_DATE)+0.99999
		AND		NVL(ifrs_inf_flag,'N') LIKE 'W'
		AND		t.ifrs_inf_code		=	G_PROCESSID
		AND		EXISTS (SELECT	'a'
						FROM	sc_trans_dtl d
						WHERE	d.OU_CODE		=	t.OU_CODE
						AND		d.SUBINV_CODE	=	t.SUBINV_CODE
						AND		d.DOC_NO		=	t.DOC_NO
						AND		d.DOC_TYPE		=	t.DOC_TYPE
						AND		d.trans_code IN (SELECT	tt.trans_code
												FROM	sc_trans_type tt
												WHERE	tt.ou_code				=	t.ou_code
												AND		tt.ccb_cust_req_flag	=	'Y')
						)
		-- Error Checking
		AND		(t.net_amt	<=	0)
		;
			
		IF NVL(CNT, 0) > 0 THEN
			P_RESULT := 'Invoice or C/N Net Amount is 0 or negative amount';
		ELSE
			P_RESULT := 'Complete validation : Invoice or C/N Amount';
		END IF;

		IF P_RESULT NOT LIKE 'Complete%' THEN
			GR_LOG.PROCESS_DATE		:=	SYSDATE;
			GR_LOG.SEQ				:=	G_INF_SEQ;
			GR_LOG.LOG_CODE			:=	G_PROCESSID || ' Validate Error';
			GR_LOG.LOG_DESC			:=	'Auto revert status ' || TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS') ||
										P_RESULT;
			GR_LOG.INF_UPD_DATE		:=	SYSDATE;
			WRITE_LOG(GR_LOG);
			G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;

			REMOVE_PROCESS('AUTO', 'BACK-JOB');
			G_RESULT_STATUS	:=	'FAILED';
		END IF;
	END IF;

END validate;
------------------------------------------
PROCEDURE start_process
(i_upd		IN	VARCHAR2
,i_pgm		IN	VARCHAR2)
IS
	
	CURSOR SELORG IS
		SELECT OU_CODE FROM SU_ORGANIZE
		WHERE ou_code LIKE G_OU_CODE
		ORDER BY OU_CODE;
	
	v_found			BOOLEAN	:=	FALSE;
	v_found_overall	BOOLEAN	:=	FALSE;
BEGIN
/*	-->> Get Process ID Running.
	BEGIN
		SELECT	LPAD( NVL(TO_NUMBER(PARAMETER_VALUE),1), 5, 0)
		INTO	G_PROCESSID
		FROM	PB_PARAMETER_SETUP
		WHERE	PGM_SETUP		=	'IFRSINFP'
		AND		PARAMETER_NAME	=	'IFRS Interface Process ID';
		BEGIN
			UPDATE	PB_PARAMETER_SETUP
			SET		PARAMETER_VALUE	=	NVL(TO_NUMBER(PARAMETER_VALUE), 0) + 1
			WHERE	PGM_SETUP		=	'IFRSINFP'
			AND		PARAMETER_NAME	=	'IFRS Interface Process ID';
		END;
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			BEGIN
				G_PROCESSID := '00001';
				INSERT INTO PB_PARAMETER_SETUP
					(PGM_SETUP
					,PARAMETER_NAME
					,PARAMETER_VALUE
					,TRANSFER
					,UPD_BY
					,UPD_DATE
					,UPD_PGM)
				VALUES
					('IFRSINFP'
					,'IFRS Interface Process ID'
					,1
					,SYSDATE
					,i_upd
					,SYSDATE
					,i_pgm);
			END;
	END;
*/	
	-->> Initial log
	GR_LOG.PROCESS_ID		:=	G_PROCESSID;
	GR_LOG.INF_UPD_BY		:=	i_upd;
	GR_LOG.INF_UPD_PGM		:=	i_pgm;
	
	-->> Lock Transaction Pending.
	GR_LOG.STEP				:=	1;
	GR_LOG.STEP_DESC		:=	'Lock row';
	GR_LOG.PROCESS_FLAG		:=	'Prepare Data';
	GR_LOG.INF_LOCK_FLAG	:=	'Lock';
	FOR OU IN SELORG LOOP
		BEGIN
			v_found	:=	FALSE;
			FOR r_data IN c_data(ou.ou_code, 'N')
			LOOP
				v_found	:=	TRUE;
				UPDATE	sc_transaction t
				SET		ifrs_inf_flag	=	'W' ---> Prepare Status Lock Wait for Process
						,ifrs_inf_code	=	G_PROCESSID
				WHERE	ou_code				=	ou.ou_code
				AND		doc_type			IN ('1','3')	-- 1=Invoice, 3=C/N
				AND		NVL(ac_type,'N')	=	'N'
				AND		upd_date BETWEEN G_START_DATE AND TRUNC(G_END_DATE)+0.99999
				AND		NVL(ifrs_inf_flag,'N') LIKE 'N'
				AND		EXISTS (SELECT	'a'
								FROM	sc_trans_dtl d
								WHERE	d.OU_CODE		=	t.OU_CODE
								AND		d.SUBINV_CODE	=	t.SUBINV_CODE
								AND		d.DOC_NO		=	t.DOC_NO
								AND		d.DOC_TYPE		=	t.DOC_TYPE
								AND		d.trans_code IN (SELECT	tt.trans_code
														FROM	sc_trans_type tt
														WHERE	tt.ou_code				=	t.ou_code
														AND		tt.ccb_cust_req_flag	=	'Y')
								);
				GR_LOG.PROCESS_DATE		:=	SYSDATE;
				GR_LOG.SEQ				:=	G_INF_SEQ;
				GR_LOG.LOG_CODE			:=	G_PROCESSID || ' Complete';
				GR_LOG.LOG_DESC			:=	'Lock (sc_transaction OU:' || ou.ou_code || ') Complete at ' || TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS') ||
											' for creation date ' || to_char(G_START_DATE, 'DD/MM/RRRR');
				GR_LOG.INF_UPD_DATE		:=	SYSDATE;
				WRITE_LOG(GR_LOG);
				G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
				EXIT;	-- Process once.
			END LOOP;
			IF NOT v_found THEN
				RAISE NO_DATA_FOUND;
			ELSE
				v_found_overall	:=	v_found;
			END IF;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				GR_LOG.PROCESS_DATE		:=	SYSDATE;
				GR_LOG.SEQ				:=	G_INF_SEQ;
				GR_LOG.LOG_CODE			:=	G_PROCESSID || ' SQL%NOTFOUND';
				GR_LOG.LOG_DESC			:=	'Lock (sc_transaction OU:' || ou.ou_code || ') Transaction not found at ' || TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS') ||
											' for creation date' || to_char(G_START_DATE, 'DD/MM/RRRR');
				GR_LOG.INF_UPD_DATE		:=	SYSDATE;
				WRITE_LOG(GR_LOG);
				G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		END;
		COMMIT;
	END LOOP;
	IF NOT v_found_overall THEN
		G_RESULT_STATUS		:=	'WARNING';
	END IF;
END start_process;
------------------------------------------
PROCEDURE ending_process
(i_upd		IN	VARCHAR2
,i_pgm		IN	VARCHAR2)
IS
	CURSOR SELORG IS
		SELECT OU_CODE FROM SU_ORGANIZE
		WHERE ou_code LIKE G_OU_CODE
		ORDER BY OU_CODE;
	
	v_found		BOOLEAN	:=	FALSE;
BEGIN
	-->> Initial log
	GR_LOG.PROCESS_ID		:=	G_PROCESSID;
	GR_LOG.INF_UPD_BY		:=	i_upd;
	GR_LOG.INF_UPD_PGM		:=	i_pgm;
	
	-->> Unlock Transaction Pending.
	GR_LOG.STEP				:=	3;
	GR_LOG.STEP_DESC		:=	'Unlock row';
	GR_LOG.PROCESS_FLAG		:=	'Prepare Data';
	GR_LOG.INF_LOCK_FLAG	:=	'Unlock';

	FOR OU IN SELORG LOOP
		BEGIN
			FOR r_data IN c_data(ou.ou_code, 'W', G_PROCESSID)
			LOOP
				v_found	:=	TRUE;
				UPDATE	sc_transaction t
				SET		ifrs_inf_flag	=	'Y' ---> Update Process Flag Finished.
				WHERE	ou_code = ou.ou_code
				AND		doc_type			IN ('1','3')	-- 1=Invoice, 3=C/N
				AND		NVL(ac_type,'N')	=	'N'
				AND		upd_date BETWEEN G_START_DATE AND TRUNC(G_END_DATE)+0.99999
				AND		NVL(ifrs_inf_flag,'N') LIKE 'W'
				AND		t.ifrs_inf_code		=	G_PROCESSID
				AND		EXISTS (SELECT	'a'
								FROM	sc_trans_dtl d
								WHERE	d.OU_CODE		=	t.OU_CODE
								AND		d.SUBINV_CODE	=	t.SUBINV_CODE
								AND		d.DOC_NO		=	t.DOC_NO
								AND		d.DOC_TYPE		=	t.DOC_TYPE
								AND		d.trans_code IN (SELECT	tt.trans_code
														FROM	sc_trans_type tt
														WHERE	tt.ou_code				=	t.ou_code
														AND		tt.ccb_cust_req_flag	=	'Y')
								);
				GR_LOG.PROCESS_DATE		:=	SYSDATE;
				GR_LOG.SEQ				:=	G_INF_SEQ;
				GR_LOG.LOG_CODE			:=	G_PROCESSID || ' Complete';
				GR_LOG.LOG_DESC			:=	'Unlock (sc_transaction OU:' || ou.ou_code || ') Complete at ' || TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS') ||
											' for creation date' || to_char(G_START_DATE, 'DD/MM/RRRR');
				GR_LOG.INF_UPD_DATE		:=	SYSDATE;
				WRITE_LOG(GR_LOG);
				G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
				EXIT;	-- Process once.
			END LOOP;
			IF NOT v_found THEN
				RAISE NO_DATA_FOUND;
			END IF;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				GR_LOG.PROCESS_DATE		:=	SYSDATE;
				GR_LOG.SEQ				:=	G_INF_SEQ;
				GR_LOG.LOG_CODE			:=	G_PROCESSID || ' SQL%NOTFOUND';
				GR_LOG.LOG_DESC			:=	'Unlock (sc_transaction OU:' || ou.ou_code || ') Transaction not found at ' || TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS') ||
											' for creation date' || to_char(G_START_DATE, 'DD/MM/RRRR');
				GR_LOG.INF_UPD_DATE		:=	SYSDATE;
				WRITE_LOG(GR_LOG);
				G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		END;
		COMMIT;
	END LOOP;
END ending_process;
------------------------------------------
PROCEDURE remove_process
(i_upd		IN	VARCHAR2
,i_pgm		IN	VARCHAR2)
IS
	CURSOR SELORG IS
		SELECT OU_CODE FROM SU_ORGANIZE
		WHERE ou_code LIKE G_OU_CODE
		ORDER BY OU_CODE;
	
	v_found		BOOLEAN	:=	FALSE;
BEGIN
	-->> Initial log
	GR_LOG.PROCESS_ID		:=	G_PROCESSID;
	GR_LOG.INF_UPD_BY		:=	i_upd;
	GR_LOG.INF_UPD_PGM		:=	i_pgm;
	
	-->> Unlock Transaction Pending.
	GR_LOG.STEP				:=	9;
	GR_LOG.STEP_DESC		:=	'Remove process';
	GR_LOG.PROCESS_FLAG		:=	'Warning : Map Error';
	GR_LOG.INF_LOCK_FLAG	:=	'Unlock';

	FOR OU IN SELORG LOOP
		BEGIN
			FOR r_data IN c_data(ou.ou_code, '%', G_PROCESSID)
			LOOP
				v_found	:=	TRUE;
				UPDATE	sc_transaction t
				SET		ifrs_inf_flag	=	'N' ---> Update Process Flag Finished.
				WHERE	ou_code = ou.ou_code
				AND		doc_type			IN ('1','3')	-- 1=Invoice, 3=C/N
				AND		NVL(ac_type,'N')	=	'N'
				AND		upd_date BETWEEN G_START_DATE AND TRUNC(G_END_DATE)+0.99999
				AND		NVL(ifrs_inf_flag,'N') LIKE '%'
				AND		t.ifrs_inf_code		=	G_PROCESSID
				AND		EXISTS (SELECT	'a'
								FROM	sc_trans_dtl d
								WHERE	d.OU_CODE		=	t.OU_CODE
								AND		d.SUBINV_CODE	=	t.SUBINV_CODE
								AND		d.DOC_NO		=	t.DOC_NO
								AND		d.DOC_TYPE		=	t.DOC_TYPE
								AND		d.trans_code IN (SELECT	tt.trans_code
														FROM	sc_trans_type tt
														WHERE	tt.ou_code				=	t.ou_code
														AND		tt.ccb_cust_req_flag	=	'Y')
								);
				GR_LOG.PROCESS_DATE		:=	SYSDATE;
				GR_LOG.SEQ				:=	G_INF_SEQ;
				GR_LOG.LOG_CODE			:=	G_PROCESSID || ' Warning';
				GR_LOG.LOG_DESC			:=	'Unlock (sc_transaction OU:' || ou.ou_code || ') Complete at ' || TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS') ||
											' for creation date' || to_char(G_START_DATE, 'DD/MM/RRRR');
				GR_LOG.INF_UPD_DATE		:=	SYSDATE;
				WRITE_LOG(GR_LOG);
				G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
				EXIT;	-- Process once.
			END LOOP;
			IF NOT v_found THEN
				RAISE NO_DATA_FOUND;
			END IF;
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				GR_LOG.PROCESS_DATE		:=	SYSDATE;
				GR_LOG.SEQ				:=	G_INF_SEQ;
				GR_LOG.LOG_CODE			:=	G_PROCESSID || ' SQL%NOTFOUND';
				GR_LOG.LOG_DESC			:=	'Unlock (sc_transaction OU:' || ou.ou_code || ') Transaction not found at ' || TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS') ||
											' for creation date' || to_char(G_START_DATE, 'DD/MM/RRRR');
				GR_LOG.INF_UPD_DATE		:=	SYSDATE;
				WRITE_LOG(GR_LOG);
				G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		END;
		COMMIT;
	END LOOP;
END remove_process;
------------------------------------------
PROCEDURE clear_log IS
BEGIN
	DELETE	sc_ifrs_inf_error_log
	WHERE	inf_upd_date < TRUNC(SYSDATE) - G_LOG_BACK_DAYS;

	DELETE	sc_ifrs_interface
	WHERE	process_date < TRUNC(SYSDATE) - G_LOG_BACK_DAYS;
END clear_log;
------------------------------------------
PROCEDURE write_log
(p_write	sc_ifrs_inf_error_log%ROWTYPE) IS
BEGIN
	BEGIN
		INSERT INTO sc_ifrs_inf_error_log
			(process_id
			,process_date
			,step
			,step_desc
			,seq
			,log_code
			,log_desc
			,process_flag
			,inf_lock_flag
			,inf_upd_by
			,inf_upd_date
			,inf_upd_pgm)
		VALUES
			(p_write.process_id
			,p_write.process_date
			,p_write.step
			,p_write.step_desc
			,p_write.seq
			,p_write.log_code
			,p_write.log_desc
			,p_write.process_flag
			,p_write.inf_lock_flag
			,p_write.inf_upd_by
			,p_write.inf_upd_date
			,p_write.inf_upd_pgm);
	END;
END write_log;
------------------------------------------
PROCEDURE prepare_data
(i_upd	IN	VARCHAR2
,i_pgm	IN	VARCHAR2)
IS
	CURSOR c_trans_dtl
		(i_ou_code		IN	VARCHAR2
		,i_subinv_code	IN	VARCHAR2
		,i_doc_no		IN	VARCHAR2
		,i_doc_type		IN	VARCHAR2)
	IS
		SELECT	d.*, tt.trans_desc
		FROM	sc_trans_dtl d, sc_trans_type tt
		WHERE	d.OU_CODE				=	i_ou_code
		AND		d.SUBINV_CODE			=	i_subinv_code
		AND		d.DOC_NO				=	i_doc_no
		AND		d.DOC_TYPE				=	i_doc_type
		AND		d.trans_code			=	tt.trans_code
		AND		tt.ou_code				=	i_ou_code
		AND		tt.trans_code			=	d.trans_code
		AND		tt.ccb_cust_req_flag	=	'Y';

BEGIN
	G_CNT_ROW_RETURN	:=	0;
	G_TOTAL_AMOUNT		:=	0;
	
	-->> Initial log
	GR_LOG.PROCESS_ID		:=	G_PROCESSID;
	GR_LOG.INF_UPD_BY		:=	i_upd;
	GR_LOG.INF_UPD_PGM		:=	i_pgm;

	-->> Get data to interface
	GR_LOG.STEP				:=	2;
	GR_LOG.STEP_DESC		:=	'Get Data';
	GR_LOG.PROCESS_FLAG		:=	'Prepare Data';
	GR_LOG.INF_LOCK_FLAG	:=	'Unlock';

	FOR r_data IN c_data(G_OU_CODE, 'W', G_PROCESSID)
	LOOP
		FOR r_trans_dtl IN c_trans_dtl(r_data.ou_code, r_data.subinv_code, r_data.doc_no, r_data.doc_type) LOOP
			G_CNT_ROW_RETURN	:=	NVL(G_CNT_ROW_RETURN, 0) + 1;
			G_TOTAL_AMOUNT		:=	NVL(G_TOTAL_AMOUNT, 0) + NVL(r_trans_dtl.amount,0);
			BEGIN
				<<INSERT_PREPARE_DATA>>
				INSERT INTO sc_ifrs_interface
					(OU_CODE
					,SUBINV_CODE
					,DOCUMENT_TYPE
					,DOCUMENT_NUMBER
					,DOCUMENT_DATE
					,TRANS_CODE
					,TRANS_DESC
					,TRANSACTION_TYPE
					,SUBSCRIBER_NUMBER
					,CUSTOMER_NUMBER
					,INVOICE_AMOUNT
					,PROCESS_ID
					,PROCESS_DATE)
				VALUES
					(r_data.ou_code
					,r_data.subinv_code
					,r_data.doc_type
					,r_data.doc_no
					,r_data.doc_date
					,r_trans_dtl.trans_code
					,r_trans_dtl.trans_desc
					,SUBSTR(r_data.doc_no,1,2)
					,r_data.cust_tel
					,r_data.ccb_cust_no
-- Edit by TM 01/11/2017 : Change amount's source column
--					,r_data.net_amt -- * r_data.multiplier
					,r_trans_dtl.amount -- * r_data.multiplier
-- End Edit by TM 01/11/2017 : Change amount's source column
					,G_PROCESSID
					,SYSDATE);
				GR_LOG.PROCESS_DATE		:=	SYSDATE;
				GR_LOG.SEQ				:=	G_INF_SEQ;
				GR_LOG.LOG_CODE			:=	G_PROCESSID || ' Complete';
				GR_LOG.LOG_DESC			:=	SUBSTR('Document No : ' || r_data.doc_no || ' Doc Date : ' || TO_CHAR(r_data.doc_date,'DD/MM/RRRR') ||
											' Trans Code : ' || r_trans_dtl.trans_code || ' Subscriber No : ' || r_data.cust_tel ||
											' CCB Customer No : ' || r_data.ccb_cust_no || ' Invoice Amt : ' || r_trans_dtl.amount, 1, 400);
				GR_LOG.INF_UPD_DATE		:=	SYSDATE;
				WRITE_LOG(GR_LOG);
				G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
			EXCEPTION
				WHEN OTHERS THEN
					GR_LOG.PROCESS_DATE		:=	SYSDATE;
					GR_LOG.SEQ				:=	G_INF_SEQ;
					GR_LOG.LOG_CODE			:=	SUBSTR(G_PROCESSID || ' ' || SQLERRM, 1, 100);
					GR_LOG.LOG_DESC			:=	SUBSTR('Document No : ' || r_data.doc_no || ' Doc Date : ' || TO_CHAR(r_data.doc_date,'DD/MM/RRRR') ||
												' Trans Code : ' || r_trans_dtl.trans_code || ' Subscriber No : ' || r_data.cust_tel ||
												' CCB Customer No : ' || r_data.ccb_cust_no || ' Invoice Amt : ' || r_trans_dtl.amount, 1, 400);
					GR_LOG.INF_UPD_DATE		:=	SYSDATE;
					WRITE_LOG(GR_LOG);
					G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
			END INSERT_PREPARE_DATA;
-- Loop all records, not the first one			EXIT;	-- Process only 1st row
		END LOOP;
	END LOOP;
	
	IF NVL(G_CNT_ROW_RETURN, 0) = 0 THEN
		GR_LOG.PROCESS_DATE		:=	SYSDATE;
		GR_LOG.SEQ				:=	G_INF_SEQ;
		GR_LOG.LOG_CODE			:=	G_PROCESSID || ' SQL%NOTFOUND';
		GR_LOG.LOG_DESC			:=	'Transaction not found';
		GR_LOG.INF_UPD_DATE		:=	SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
	END IF;
	COMMIT;
END prepare_data;
------------------------------------------
PROCEDURE sent_interface
(i_upd			IN		VARCHAR2
,i_pgm			IN		VARCHAR2
,i_output_path	IN		VARCHAR2
,io_status		IN OUT	VARCHAR2)
IS
	t_text			glb_text_output_util.varchar_tabtyp;
	t_format		glb_text_output_util.number_tabtyp;
	v_filename		VARCHAR2(100)	:=	G_FILENAME || '.DAT';
	v_filename_sync	VARCHAR2(100)	:=	G_FILENAME || '.SYNC';

	CURSOR c_data2sent IS
		SELECT	t.*, DECODE(t.ou_code,'TAC','DTAC',t.ou_code) ou_code_convert
		FROM	sc_ifrs_interface t
		WHERE	process_id	=	G_PROCESSID
		ORDER BY ou_code_convert, t.document_type, t.document_number;
BEGIN
	G_INF_SEQ			:=	1;
	G_CNT_ROW_RETURN	:=	0;
	G_TOTAL_AMOUNT		:=	0;
	
	io_status			:=	glb_text_output_util.NO_ERROR;

	-->> Initial log
	GR_LOG.PROCESS_ID		:=	G_PROCESSID;
	GR_LOG.INF_UPD_BY		:=	i_upd;
	GR_LOG.INF_UPD_PGM		:=	i_pgm;

	-->> Lock Transaction Pending.
	GR_LOG.STEP				:=	5;
	GR_LOG.STEP_DESC		:=	'Generate Interface file';
	GR_LOG.PROCESS_FLAG		:=	'Sent Interface';
	GR_LOG.INF_LOCK_FLAG	:=	'Unlock';

	FOR i IN 1..9
	LOOP
		t_format(i)	:=	-1;
	END LOOP;
	glb_text_output_util.set_format
		(it_col_len		=>	t_format
		,o_status		=>	io_status);
	IF io_status <> glb_text_output_util.NO_ERROR THEN
		RAISE e_abnormal_end;
	END IF;

	FOR r_data IN c_data2sent LOOP
		BEGIN
			G_CNT_ROW_RETURN	:=	G_CNT_ROW_RETURN + 1;
			G_TOTAL_AMOUNT		:=	G_TOTAL_AMOUNT + NVL(r_data.invoice_amount,0);

			t_text(1)	:=	r_data.ou_code_convert;
			t_text(2)	:=	r_data.document_number;
			t_text(3)	:=	TO_CHAR(r_data.document_date,'YYYY-MM-DD HH:MI:SS');
			t_text(4)	:=	r_data.trans_code;
			t_text(5)	:=	r_data.trans_desc;
			t_text(6)	:=	r_data.transaction_type;
			t_text(7)	:=	r_data.subscriber_number;
			t_text(8)	:=	r_data.customer_number;
			t_text(9)	:=	r_data.invoice_amount;
			glb_text_output_util.add_txt
				(it_message			=>	t_text
				,i_delimiter		=>	NULL
				,i_separator		=>	'|'
				);

			GR_LOG.PROCESS_DATE		:=	SYSDATE;
			GR_LOG.SEQ				:=	G_INF_SEQ;
			GR_LOG.LOG_CODE			:=	G_PROCESSID || ' Complete';
			GR_LOG.LOG_DESC			:=	SUBSTR('Document No : ' || r_data.document_number ||
											' Doc Date : ' || TO_CHAR(r_data.document_date,'DD/MM/RRRR') ||
											' Trans Code : ' || r_data.trans_code ||
											' Subscriber No : ' || r_data.subscriber_number ||
											' Customer No : ' || r_data.customer_number ||
											' Invoice Amt : ' || r_data.invoice_amount, 1, 400);
			GR_LOG.INF_UPD_DATE		:=	SYSDATE;
			WRITE_LOG(GR_LOG);
			G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		EXCEPTION
			WHEN OTHERS THEN
				G_CNT_ROW_ERROR			:=	NVL(G_CNT_ROW_ERROR, 0) + 1;
				GR_LOG.PROCESS_DATE		:=	SYSDATE;
				GR_LOG.SEQ				:=	G_INF_SEQ;
				GR_LOG.LOG_CODE			:=	SUBSTR(G_PROCESSID || SQLERRM,1,100);
				GR_LOG.LOG_DESC			:=	SUBSTR('Document No : ' || r_data.document_number ||
												' Doc Date : ' || TO_CHAR(r_data.document_date,'DD/MM/RRRR') ||
												' Trans Code : ' || r_data.trans_code ||
												' Subscriber No : ' || r_data.subscriber_number ||
												' Customer No : ' || r_data.customer_number ||
												' Invoice Amt : ' || r_data.invoice_amount, 1, 400);
				GR_LOG.INF_UPD_DATE		:=	SYSDATE;
				WRITE_LOG(GR_LOG);
				G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		END;
	END LOOP;
	---------------------------------------
	IF G_CNT_ROW_RETURN = 0 THEN
		GR_LOG.PROCESS_DATE		:=	SYSDATE;
		GR_LOG.SEQ				:=	G_INF_SEQ;
		GR_LOG.LOG_CODE			:=	SUBSTR(G_PROCESSID || ' SQL%NOTFOUND',1,100);
		GR_LOG.LOG_DESC			:=	'No Data Found';
		GR_LOG.INF_UPD_DATE		:=	SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
	ELSE
		glb_text_output_util.write2file
			(i_file_loc			=>	i_output_path
			,i_file_name		=>	v_filename
			,o_status			=>	io_status);
		IF io_status <> glb_text_output_util.NO_ERROR THEN
			RAISE e_abnormal_end;
		END IF;

		t_format.DELETE;
		t_format(1)	:=	-1;
		t_format(2)	:=	-1;
		glb_text_output_util.set_format	-- Clear previous data
			(it_col_len		=>	t_format
			,o_status		=>	io_status);
		IF io_status <> glb_text_output_util.NO_ERROR THEN
			RAISE e_abnormal_end;
		END IF;

		t_text.DELETE;
		t_text(1)	:=	G_CNT_ROW_RETURN;
		t_text(2)	:=	G_TOTAL_AMOUNT;
		glb_text_output_util.add_txt
			(it_message			=>	t_text
			,i_delimiter		=>	NULL
			,i_separator		=>	'|'
			);

		glb_text_output_util.write2file
			(i_file_loc			=>	i_output_path
			,i_file_name		=>	v_filename_sync
			,o_status			=>	io_status);
		IF io_status <> glb_text_output_util.NO_ERROR THEN
			RAISE e_abnormal_end;
		END IF;

	END IF;
	COMMIT;
EXCEPTION
	WHEN e_abnormal_end THEN
		GR_LOG.PROCESS_DATE		:=	SYSDATE;
		GR_LOG.SEQ				:=	G_INF_SEQ;
		GR_LOG.LOG_CODE			:=	G_PROCESSID || ' Generate File Error';
		GR_LOG.LOG_DESC			:=	'Auto revert status ' || TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS') ||
									io_status;
		GR_LOG.INF_UPD_DATE		:=	SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
END sent_interface;

PROCEDURE send_email
(i_smtp_server			IN		VARCHAR2	:=	'mail-gw.tac.co.th'
,i_smtp_port			IN		VARCHAR2	:=	25
,i_from_email			IN		VARCHAR2	:=	'ERPOperationSupport@dtac.co.th'
,i_to_email				IN		VARCHAR2	-- Separate by ,
,i_cc_email				IN		VARCHAR2	:=	NULL
,i_bcc_email			IN		VARCHAR2	:=	NULL
,i_subject				IN		VARCHAR2	:=	'[POS] Interface Termination Fee to IFRS - ' || G_RESULT_STATUS
,i_reply_email			IN		VARCHAR2	:=	'ifrs_interface@dtac.co.th'
,i_priority				IN		NUMBER		:=	3	-- Normal priority
,io_status				IN OUT	VARCHAR2
)
IS
	C_ERROR				CONSTANT VARCHAR2(40)	:=	'$$ERROR$$';
	NEW_LINE_IN_MSG		CONSTANT VARCHAR2(10)	:=	'<BR>';
	v_blob				BLOB;
	v_msg_template		VARCHAR2(32000)	:=
		'Dear All,' || NEW_LINE_IN_MSG ||
		NEW_LINE_IN_MSG ||
		'Please be informed that POS interface termination Fee to IFRS executed with ' || G_RESULT_STATUS || '.' || 
		CASE WHEN G_RESULT_STATUS='SUCCESS' THEN NULL ELSE '  See more detail in file enclosed.' END || NEW_LINE_IN_MSG ||
		NEW_LINE_IN_MSG ||
		'Job No. : ' || G_N_NO || NEW_LINE_IN_MSG ||
		'Job Status : Complete' || CASE WHEN G_RESULT_STATUS='SUCCESS' THEN NULL ELSE ' with ' || G_RESULT_STATUS END || NEW_LINE_IN_MSG ||
		'Job Broken : ' || G_JOB_BROKEN || NEW_LINE_IN_MSG ||
		NEW_LINE_IN_MSG ||
		'Thank you & Regards,' || NEW_LINE_IN_MSG ||
		'POS auto job' || NEW_LINE_IN_MSG;
	v_msg				v_msg_template%TYPE;
	v_error				VARCHAR2(4000);
	b_has_attachment	BOOLEAN	:=	G_RESULT_STATUS <> 'SUCCESS';

-- Start Temporary for checking email
	t_text			glb_text_output_util.varchar_tabtyp;
	t_format		glb_text_output_util.number_tabtyp;
	v_filename		VARCHAR2(4000)	:=	G_FILENAME || '.CSV';
-- End Temporary for checking email

BEGIN
	io_status	:=	glb_text_output_util.NO_ERROR;

	v_msg	:=	v_msg_template;
-- Error => write error HTML file
	IF G_RESULT_STATUS <> 'SUCCESS' THEN
		FOR i IN 1..7
		LOOP
			t_format(i)	:=	-1;
		END LOOP;

		glb_text_output_util.set_format
			(it_col_len		=>	t_format
			,o_status		=>	io_status
			,i_clear_data	=>	'Y');
		IF io_status <> glb_text_output_util.NO_ERROR THEN
			RAISE e_abnormal_end;
		END IF;

		-- Headings
		t_text(1)	:=	'Step';
		t_text(2)	:=	'Log Code';
		t_text(3)	:=	'Log Description';
		t_text(4)	:=	'Status';
		t_text(5)	:=	'Process Flag';
		t_text(6)	:=	'INF Lock Flag';
		t_text(7)	:=	'INF Update Date';
		
		glb_text_output_util.add_txt
		(it_message			=>	t_text
		/*,i_delimiter		=>	glb_text_output_util.HTML_TBL_HDR_DELIM_B	--'<TH>'
		,i_delimiter_end	=>	glb_text_output_util.HTML_TBL_HDR_DELIM_E	--'</TH>' || glb_bulk_run_report_api.NEW_LINE
		,i_separator		=>	glb_text_output_util.HTML_TBL_SEPARATOR		--NULL
		,i_line_prefix		=>	glb_text_output_util.HTML_TBL_HDR_ROW_PREFIX	--'<TABLE BORDER=1><TR>'
		,i_line_suffix		=>	glb_text_output_util.HTML_TBL_HDR_ROW_SUFFIX*/);	--'</TR>');	

		FOR r_error IN (SELECT	t.*
						FROM	sc_ifrs_inf_error_log t
						WHERE	process_id	=	G_PROCESSID
						ORDER BY inf_upd_date, step, seq)
		LOOP
			t_text(1)	:=	r_error.step;
			t_text(2)	:=	r_error.log_code;
			t_text(3)	:=	r_error.log_desc;
			t_text(4)	:=	G_RESULT_STATUS;
			t_text(5)	:=	r_error.process_flag;
			t_text(6)	:=	r_error.inf_lock_flag;
			t_text(7)	:=	r_error.inf_upd_date;
			
			glb_text_output_util.add_txt
			(it_message			=>	t_text
			/*,i_delimiter		=>	glb_text_output_util.HTML_TBL_DATA_DELIM_B	--'<TH>'
			,i_delimiter_end	=>	glb_text_output_util.HTML_TBL_DATA_DELIM_E	--'</TH>' || glb_bulk_run_report_api.NEW_LINE
			,i_separator		=>	glb_text_output_util.HTML_TBL_SEPARATOR		--NULL
			,i_line_prefix		=>	glb_text_output_util.HTML_TBL_DATA_ROW_PREFIX	--'<TABLE BORDER=1><TR>'
			,i_line_suffix		=>	glb_text_output_util.HTML_TBL_DATA_ROW_SUFFIX*/);	--'</TR>');	
		END LOOP;
		glb_text_output_util.write2file
			(i_file_loc			=>	G_DIRECTORY
			,i_file_name		=>	v_filename
			,o_status			=>	io_status);
		IF io_status <> glb_text_output_util.NO_ERROR THEN
			RAISE e_abnormal_end;
		END IF;
	END IF;

	IF b_has_attachment THEN
		v_blob	:=	mail_tools.get_local_binary_data
						(p_dir	=>	G_DIRECTORY
						,p_file	=>	v_filename);
	ELSE
		v_blob	:=	EMPTY_BLOB();
	END IF;

-- Start Temporary for checking email
	t_format.DELETE;
	t_format(1)	:=	-1;
	glb_text_output_util.set_format	-- Clear previous data
		(it_col_len		=>	t_format
		,o_status		=>	io_status);
	IF io_status <> glb_text_output_util.NO_ERROR THEN
		RAISE e_abnormal_end;
	END IF;

	t_text.DELETE;
	t_text(1)	:=	'SMTP Server: ' || i_smtp_server;
	glb_text_output_util.add_txt
		(it_message			=>	t_text
		,i_delimiter		=>	NULL
		);

	t_text.DELETE;
	t_text(1)	:=	'SMTP Port: ' || i_smtp_port;
	glb_text_output_util.add_txt
		(it_message			=>	t_text
		,i_delimiter		=>	NULL
		);

	t_text.DELETE;
	t_text(1)	:=	'From email: ' || i_from_email;
	glb_text_output_util.add_txt
		(it_message			=>	t_text
		,i_delimiter		=>	NULL
		);

	t_text.DELETE;
	t_text(1)	:=	'To email: ' || i_to_email;
	glb_text_output_util.add_txt
		(it_message			=>	t_text
		,i_delimiter		=>	NULL
		);

	t_text.DELETE;
	t_text(1)	:=	'CC email: ' || i_cc_email;
	glb_text_output_util.add_txt
		(it_message			=>	t_text
		,i_delimiter		=>	NULL
		);

	t_text.DELETE;
	t_text(1)	:=	'BCC email: ' || i_bcc_email;
	glb_text_output_util.add_txt
		(it_message			=>	t_text
		,i_delimiter		=>	NULL
		);

	t_text.DELETE;
	t_text(1)	:=	'Email Subject: ' || i_subject;
	glb_text_output_util.add_txt
		(it_message			=>	t_text
		,i_delimiter		=>	NULL
		);

	IF b_has_attachment THEN
		t_text.DELETE;
		t_text(1)	:=	'Email Attached File: ' || v_filename;
		glb_text_output_util.add_txt
			(it_message			=>	t_text
			,i_delimiter		=>	NULL
			);
	END IF;

	t_text.DELETE;
	t_text(1)	:=	'Email Message:';
	glb_text_output_util.add_txt
		(it_message			=>	t_text
		,i_delimiter		=>	NULL
		);

	t_text.DELETE;
	t_text(1)	:=	v_msg;
	glb_text_output_util.add_txt
		(it_message			=>	t_text
		,i_delimiter		=>	NULL
		);

	glb_text_output_util.write2file
		(i_file_loc			=>	G_DIRECTORY
		,i_file_name		=>	'email_' || TO_CHAR(SYSDATE,'YYYYMMDD_HH24MISSSSS')
		,o_status			=>	io_status);
	IF io_status <> glb_text_output_util.NO_ERROR THEN
		RAISE e_abnormal_end;
	END IF;
-- End Temporary for checking email

	mail_tools.sendmail
		(smtp_server		=>	i_smtp_server
		,smtp_server_port	=>	i_smtp_port
		,from_name			=>	i_from_email
		,to_name			=>	i_to_email	-- list of TO email addresses separated by commas (,)
		,cc_name			=>	i_cc_email	-- list of CC email addresses separated by commas (,)
		,bcc_name			=>	i_bcc_email	-- list of BCC email addresses separated by commas (,)
		,subject			=>	i_subject
		,message			=>	v_msg
		,priority			=>	i_priority	-- 1-5 1 being the highest priority and 3 normal priority
		,filename			=>	v_filename
		,binaryfile			=>	v_blob);
END send_email;

--@@@ AUTO RUNNING PROCESS @@@ --
PROCEDURE main_outbound
(p_ou				IN	in_req_head.ou_code%TYPE
,p_trx_date			IN	VARCHAR2
,p_trx_back_days	IN	NUMBER
,p_log_back_days	IN	NUMBER
,p_output_path		IN	VARCHAR2
,N_NO				IN	NUMBER
,N_ID				IN	NUMBER)
IS
	V_RESULT VARCHAR2(400) := 'Default';
	V_WORK   VARCHAR2(1);
	SUC_CNT  NUMBER;
	ERR_CNT  NUMBER;
	M_SEQ    NUMBER;
BEGIN
	SELECT	MAX(j.broken)
	INTO	G_JOB_BROKEN
	FROM	all_jobs j
	WHERE	j.job	=	N_NO;

	G_DIRECTORY	:=	NULL;	-- Clear value
	G_RESULT_STATUS	:=	'SUCCESS';
	G_FILENAME	:=	TO_CHAR(SYSDATE,'YYYYMMDD') || '_ERP_TERMINATION_FEE';
	
	FOR rec IN (SELECT	directory_name
				FROM	all_directories
				WHERE	directory_path	=	p_output_path
				AND		directory_name LIKE 'IFRS_POS_DIR%')
	LOOP
		G_DIRECTORY	:=	rec.directory_name;
		EXIT;
	END LOOP;
	
	IF G_DIRECTORY IS NULL THEN
		SELECT	MAX(directory_name)
		INTO	G_DIRECTORY
		FROM	all_directories
		WHERE	directory_name LIKE 'IFRS_POS_DIR%';
		
		IF G_DIRECTORY = 'IFRS_POS_DIR' THEN
			G_DIRECTORY	:=	G_DIRECTORY || '_001';
		ELSE
			G_DIRECTORY	:=	'IFRS_POS_DIR_' || TO_CHAR(TO_NUMBER(SUBSTR(G_DIRECTORY,14))+1,'fm000');
		END IF;
		
		EXECUTE IMMEDIATE 'CREATE DIRECTORY ' || G_DIRECTORY || ' AS ''' || p_output_path || '''';
	END IF;
	G_DIRECTORY	:=	UPPER(G_DIRECTORY);	-- Make sure we always refer directory name with uppercase

-- Set parameters
	G_OU_CODE			:=	NVL(p_ou,'%');
	G_TRX_BACK_DAYS		:=	p_trx_back_days;
	IF p_trx_date IS NULL THEN
		G_START_DATE	:=	TRUNC(SYSDATE) - G_TRX_BACK_DAYS;
	ELSE
		G_START_DATE	:=	TO_DATE(p_trx_date,'DD/MM/RRRR');
	END IF;
	G_END_DATE			:=	G_START_DATE;
	G_LOG_BACK_DAYS		:=	p_log_back_days;
	G_N_NO				:=	n_no;
	G_N_ID				:=	n_id;

-- Clear log
	G_INF_SEQ	:=	1;
	clear_log;
	
	G_PROCESSID	:=	NULL;	-- Clear value

	-->> Get Process ID Running.
	IF G_PROCESSID IS NULL THEN
		BEGIN
			SELECT	LPAD(NVL(TO_NUMBER(PARAMETER_VALUE), 1), 5, 0)
			INTO	G_PROCESSID
			FROM	PB_PARAMETER_SETUP
			WHERE	PGM_SETUP = 'IFRSINFP'
			AND		PARAMETER_NAME = 'IFRS Interface Process ID';

			UPDATE	PB_PARAMETER_SETUP
			SET		PARAMETER_VALUE = TO_CHAR(NVL(TO_NUMBER(PARAMETER_VALUE),0) + 1)
			WHERE	PGM_SETUP = 'IFRSINFP'
			AND		PARAMETER_NAME = 'IFRS Interface Process ID';
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				G_PROCESSID := '00001';
				INSERT INTO PB_PARAMETER_SETUP
					(PGM_SETUP
					,PARAMETER_NAME
					,PARAMETER_VALUE
					,TRANSFER
					,UPD_BY
					,UPD_DATE
					,UPD_PGM)
				VALUES
					('IFRSINFP'
					,'IFRS Interface Process ID'
					,G_PROCESSID + 1	-- Next process id
					,SYSDATE
					,'AUTO'
					,SYSDATE
					,'BACK-JOB');
		END;
	END IF;

	glb_text_output_util.g_process_id	:=	G_PROCESSID;

	-- CHECK WORK CONDITION
	V_WORK := 'Y';
	--V_WORK := CHECK_JOB_WORK(N_NO);
	
	IF NVL(V_WORK, 'N') = 'Y' THEN
		G_CNT_ROW_RETURN := 0;
		G_CNT_ROW_ERROR := 0;
		
		-- WRITE START PROCESS
		M_SEQ := GET_MONITOR_SEQ(N_ID);
		
		BEGIN
			INSERT INTO JOBS_MONITOR
				(JOB_ID,
				 JOB_SEQ,
				 JOB_NAME,
				 JOB_MESG,
				 JOB_TIME,
				 SUC_CNT,
				 ERR_CNT,
				 UPD_BY,
				 UPD_DATE,
				 UPD_PGM,
				 STATUS)
			VALUES
				(N_ID,
				 M_SEQ,
				 C_JOB_NAME,
				 'Start to run : IFRS_INTERFACE',
				 SYSDATE,
				 G_CNT_ROW_RETURN,
				 G_CNT_ROW_ERROR,
				 'AUTO',
				 SYSDATE,
				 'BACK-JOB',
				 'Start Run Process ' || G_PROCESSID);
		EXCEPTION
			WHEN OTHERS THEN
				NULL;
		END;
		
		start_process('AUTO'
					 ,'BACK-JOB');
		-- @@@ Validate
		validate('AUTO'
				,'BACK-JOB'
				,v_result);
		IF v_result NOT LIKE 'Complete%' THEN
			GOTO end_process;
		END IF;

		-- @@@ Prepare Data @@@ --
		prepare_data('AUTO'
					,'BACK-JOB');
		
		ending_process	('AUTO'
						,'BACK-JOB');
		
		--> SENT INTERFACE TO OA
		sent_interface	('AUTO'
						,'BACK-JOB'
						,G_DIRECTORY
						,v_result);
		IF v_result <> glb_text_output_util.NO_ERROR THEN
			GOTO end_process;
		END IF;
		GOTO EXIT_PROCESS;
		-- @@@     End      @@@ --
		<<END_PROCESS>>
		BEGIN
			-->> Initial log
			GR_LOG.PROCESS_ID		:=	G_PROCESSID;
			GR_LOG.INF_UPD_BY		:=	'AUTO';
			GR_LOG.INF_UPD_PGM		:=	'BACK-JOB';

			-->> Lock Transaction Pending.
			GR_LOG.STEP				:=	0.6;
			GR_LOG.STEP_DESC		:=	'Error Interface';
			GR_LOG.PROCESS_FLAG		:=	'Error';
			GR_LOG.INF_LOCK_FLAG	:=	'Unlock';

			GR_LOG.PROCESS_DATE		:=	SYSDATE;
			GR_LOG.SEQ				:=	0;
			GR_LOG.LOG_CODE			:=	G_PROCESSID || ' Error Validation / Generating file';
			GR_LOG.LOG_DESC			:=	v_result;
			GR_LOG.INF_UPD_DATE		:=	SYSDATE;
			WRITE_LOG(GR_LOG);

			G_RESULT_STATUS	:=	'FAILED';
			REMOVE_PROCESS	('AUTO'
							,'BACK-JOB');
		END END_PROCESS;
		
		<<EXIT_PROCESS>>
	-- WRITE END PROCESS

		-- Send email
		send_email	(i_to_email				=>	'ERPOperationSupport@dtac.co.th'
					,i_cc_email				=>	NULL
					,i_bcc_email			=>	NULL
					,io_status				=>	v_result
					);
		IF v_result <> glb_text_output_util.NO_ERROR THEN
			GOTO end_process_2;
		END IF;

		BEGIN
			INSERT INTO JOBS_MONITOR
				(JOB_ID,
				 JOB_SEQ,
				 JOB_NAME,
				 JOB_MESG,
				 JOB_TIME,
				 SUC_CNT,
				 ERR_CNT,
				 UPD_BY,
				 UPD_DATE,
				 UPD_PGM,
				 STATUS)
			VALUES
				(N_ID,
				 M_SEQ + 0.1,
				 C_JOB_NAME,
				 'Post to IFRS_INTERFACE Complete',
				 SYSDATE,
				 G_CNT_ROW_RETURN,
				 G_CNT_ROW_ERROR,
				 'AUTO',
				 SYSDATE,
				 'BACK-JOB',
				 'Complete Process ' || G_PROCESSID);
			GOTO EXIT_PROCESS_2;
		EXCEPTION
			WHEN OTHERS THEN
				NULL;
		END EXIT_PROCESS;

		<<END_PROCESS_2>>
		BEGIN
			-->> Initial log
			GR_LOG.PROCESS_ID		:=	G_PROCESSID;
			GR_LOG.INF_UPD_BY		:=	'AUTO';
			GR_LOG.INF_UPD_PGM		:=	'BACK-JOB';

			-->> Lock Transaction Pending.
			GR_LOG.STEP				:=	0.7;
			GR_LOG.STEP_DESC		:=	'Error Interface';
			GR_LOG.PROCESS_FLAG		:=	'Error';
			GR_LOG.INF_LOCK_FLAG	:=	'Unlock';

			GR_LOG.PROCESS_DATE		:=	SYSDATE;
			GR_LOG.SEQ				:=	0;
			GR_LOG.LOG_CODE			:=	G_PROCESSID || ' Error Sending Email';
			GR_LOG.LOG_DESC			:=	v_result;
			GR_LOG.INF_UPD_DATE		:=	SYSDATE;
			WRITE_LOG(GR_LOG);

			G_RESULT_STATUS	:=	'FAILED';
			REMOVE_PROCESS	('AUTO'
							,'BACK-JOB');
		END END_PROCESS_2;

		<<EXIT_PROCESS_2>>
		--V_WORK := CLEAR_JOB_WORK(N_NO);
		COMMIT;
	END IF;
END main_outbound;
-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@--

END TRN_INTERFACE_IFRS;
/
