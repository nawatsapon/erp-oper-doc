-- Add/modify columns 
alter table SC_IFRS_INF_ERROR_LOG add ou_code varchar2(3);
alter table SC_IFRS_INF_ERROR_LOG add subinv_code varchar2(10);
alter table SC_IFRS_INF_ERROR_LOG add doc_no varchar2(15);

COMMENT ON COLUMN SC_IFRS_INF_ERROR_LOG.ou_code IS 'OU Code';
COMMENT ON COLUMN SC_IFRS_INF_ERROR_LOG.subinv_code IS 'Subinventory Code';
COMMENT ON COLUMN SC_IFRS_INF_ERROR_LOG.doc_no IS 'Document Number';
