CREATE OR REPLACE PACKAGE TRN_INTERFACE_IFRS
AUTHID CURRENT_USER
IS
/*
Date		By				Description
----------	--------------	---------------------------------------------
22/08/2017	TM				Created
*/

---**************************************************--

--@@@ AUTO RUNNING PROCESS @@@ --
PROCEDURE main_outbound
(p_ou				IN	in_req_head.ou_code%TYPE
,p_trx_date			IN	VARCHAR2
,p_trx_back_days	IN	NUMBER
,p_log_back_days	IN	NUMBER
,p_output_path		IN	VARCHAR2
,N_NO				IN	NUMBER
,N_ID				IN	NUMBER
);

END TRN_INTERFACE_IFRS;