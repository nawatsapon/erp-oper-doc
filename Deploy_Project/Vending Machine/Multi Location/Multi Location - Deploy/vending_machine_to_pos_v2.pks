CREATE OR REPLACE 
PACKAGE vending_machine_to_pos_v2 is
  procedure export_sn_with_prices (
    err_msg   out varchar2,
    err_code  out varchar2,
    --p_vending_code varchar2,
    p_document_date varchar2
  );
  procedure export_sim_sold_by_others (
    err_msg   out varchar2,
    err_code  out varchar2,
    --p_vending_code varchar2,
    p_document_date varchar2
  );
end vending_machine_to_pos_v2;
