CREATE OR REPLACE PACKAGE SC_TRX_ITF_DTAC_ONE
AUTHID CURRENT_USER
IS
/*
Date		By				Description
----------	--------------	---------------------------------------------
30/12/2017	TM				Created
*/

---**************************************************--

--@@@ AUTO RUNNING PROCESS @@@ --
PROCEDURE main_inbound
(p_source_system	IN	VARCHAR2
,p_ou				IN	in_req_head.ou_code%TYPE
,p_file_date		IN	VARCHAR2
,p_file_back_days	IN	NUMBER
,p_log_back_days	IN	NUMBER
,p_input_path		IN	VARCHAR2
,p_field_separator	IN	VARCHAR2
,p_mail_to			IN	VARCHAR2	-- Send email to ?? upon completion
,p_vat_tolerance	IN	NUMBER
,N_NO				IN	NUMBER
,N_ID				IN	NUMBER);

END SC_TRX_ITF_DTAC_ONE;
/
CREATE OR REPLACE PACKAGE BODY SC_TRX_ITF_DTAC_ONE
IS
/*
Date		By				Description
----------	--------------	---------------------------------------------
30/12/2017	TM				Created
28/02/2018	TM				Add exception while getting count
*/

C_JOB_NAME			CONSTANT VARCHAR2(1000)	:=	'Interface from DTAC One';
C_SOURCE_MODULE		CONSTANT VARCHAR2(30)	:=	'DTAC ONE';
C_DFLT_START_DATE	CONSTANT DATE	:=	TO_DATE('01/01/0001','dd/mm/yyyy');
C_DFLT_END_DATE		CONSTANT DATE	:=	TO_DATE('31/12/9999','dd/mm/yyyy');
GR_LOG				inbound_text_file_error_log%ROWTYPE;
G_CNT_DOC_IMPORT	NUMBER	:=	0;
G_CNT_DOC_ERROR		NUMBER	:=	0;
G_CNT_DTL_ERROR		NUMBER	:=	0;
G_CNT_PAY_ERROR		NUMBER	:=	0;
G_CNT_RECTYP_ERROR	NUMBER	:=	0;
G_CUT_FIELDS_ERROR	NUMBER	:=	0;
G_TOTAL_AMOUNT		NUMBER	:=	0;
G_INF_SEQ			NUMBER	:=	1;
G_FILE_NUM			NUMBER	:=	0;
G_PROCESSID			PB_PARAMETER_SETUP.PARAMETER_VALUE%TYPE;
G_PROGRAM_NAME		VARCHAR2(100)	:=	'DTAC_ONE_INB2POS';
G_SOURCE_SYSTEM		VARCHAR2(100);
G_FIELD_SEPARATOR	VARCHAR2(10);

G_VAT_TOLERANCE		NUMBER;
G_FILE_ID			NUMBER;
G_OU_CODE			su_organize.ou_code%TYPE;
G_FILE_BACK_DAYS	NUMBER;
G_START_DATE		DATE;
G_END_DATE			DATE;
G_LOG_BACK_DAYS		NUMBER;
G_N_NO				NUMBER;
G_N_ID				NUMBER;
G_JOB_BROKEN		VARCHAR2(10);

e_abnormal_end		EXCEPTION;

G_DIRECTORY			VARCHAR2(4000);
G_DIRECTORY_LOG		VARCHAR2(4000);	-- Add by TM 02-Aug-2018
G_RESULT_STATUS		VARCHAR2(4000);
G_FILENAME			VARCHAR2(4000);
G_FILENAME_PATTERN	VARCHAR2(4000);

-- Start Forward Declarations
PROCEDURE write_log
(p_write	inbound_text_file_error_log%ROWTYPE);

PROCEDURE remove_process
(i_upd	VARCHAR2
,i_pgm	VARCHAR2);
-- End Forward Declarations

------------------------------------------
PROCEDURE mark_file_status
(i_status	IN	VARCHAR2
,i_file_id	IN	NUMBER	:=	G_FILE_ID)
IS
PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
	IF i_file_id IS NOT NULL THEN
		UPDATE	inbound_text_file
		SET		file_inf_flag	=	i_status
		WHERE	file_id			=	i_file_id;
	END IF;
	COMMIT;
END mark_file_status;
------------------------------------------
FUNCTION insert_itf
(ir_transaction_itf	IN	SC_TRANSACTION_ITF%ROWTYPE
,i_upd				IN	VARCHAR2)
RETURN VARCHAR2
IS
	v_return			VARCHAR2(4000);
	r_transaction_itf	SC_TRANSACTION_ITF%ROWTYPE	:=	ir_transaction_itf;
BEGIN
	INSERT INTO sc_transaction_itf
		(ou_code
		,subinv_code
--		,doc_no		-- Generate later
		,doc_type	-- Validate later must be 1
		,doc_date
--		,cat_code	-- Get later 
		,cust_code
		,cust_name
		,cust_add
		,cust_amphur
		,cust_province
		,cust_postal
		,cust_tel
--		,gross_amt	-- Get later from net_amt - WHT
		,total_amt	-- Validate & update later to net_amt - vat_amt
--		,net_amt	-- Get later from SUM(SC_TRANS_DTL_ITF.amount) for the document
		,vat_code
--		,vat_rate	-- Get later
		,vat_amt
		,salesperson
		,team_header
		,shop_mgr
		,area_mgr
/*		,remark
		,cn_flag			-- Use default value 'N'
		,interface_status	-- Use default value 'N'
		,change_amt*/
		,tax_id
		,branch_code
--		,ac_type	-- Use default value 'N'
		,cre_by
		,cre_date
/*		,status				-- Use default value 'N'
		,ref_ccb_cust_flag	-- Use default value 'N'*/
		,source_module
		,source_reference
		,file_id
		,line_num
--		,itf_status	-- Use default value 'N'
		)
	VALUES
		(r_transaction_itf.ou_code
		,r_transaction_itf.subinv_code
--		,doc_no
		,r_transaction_itf.doc_type
		,r_transaction_itf.doc_date
--		,cat_code
		,r_transaction_itf.cust_code
		,r_transaction_itf.cust_name
		,r_transaction_itf.cust_add
		,r_transaction_itf.cust_amphur
		,r_transaction_itf.cust_province
		,r_transaction_itf.cust_postal
		,r_transaction_itf.cust_tel
--		,gross_amt
		,r_transaction_itf.total_amt
--		,net_amt
		,r_transaction_itf.vat_code
--		,vat_rate
		,r_transaction_itf.vat_amt
		,r_transaction_itf.salesperson
		,r_transaction_itf.team_header
		,r_transaction_itf.shop_mgr
		,r_transaction_itf.area_mgr
/*		,remark
		,cn_flag
		,interface_status
		,change_amt*/
		,r_transaction_itf.tax_id
		,r_transaction_itf.branch_code
--		,ac_type
		,i_upd		-- cre_by
		,SYSDATE	-- cre_date
/*		,status
		,ref_ccb_cust_flag*/
		,r_transaction_itf.source_module
		,r_transaction_itf.source_reference
		,r_transaction_itf.file_id
		,r_transaction_itf.line_num
--		,itf_status
		);
	GR_LOG.PROCESS_DATE	:=	SYSDATE;
	GR_LOG.SEQ			:=	G_INF_SEQ;
	GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Insert ITF Data';
	GR_LOG.LOG_DESC		:=	SUBSTR('Line# : ' || TO_CHAR(r_transaction_itf.line_num,'000000') || ', Line Type : H, Source Reference : ' || r_transaction_itf.source_reference, 1, 400);
	GR_LOG.INF_UPD_DATE	:=	SYSDATE;
-- Not log non-Error	WRITE_LOG(GR_LOG);
	G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
	RETURN glb_text_output_util.NO_ERROR;
EXCEPTION
	WHEN OTHERS THEN
		v_return			:=	glb_text_output_util.C_ERROR || SQLERRM;
		GR_LOG.PROCESS_DATE	:=	SYSDATE;
		GR_LOG.SEQ			:=	G_INF_SEQ;
		GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Insert ITF Data Error - ' || SQLERRM;
		GR_LOG.LOG_DESC		:=	SUBSTR('Line# : ' || TO_CHAR(r_transaction_itf.line_num,'000000') || ', Line Type : H, Source Reference : ' || r_transaction_itf.source_reference, 1, 400);
		GR_LOG.INF_UPD_DATE	:=	SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ			:=	NVL(G_INF_SEQ, 0) + 1;
		G_CNT_DOC_ERROR		:=	G_CNT_DOC_ERROR + 1;
		mark_file_status('E');
		RETURN v_return;
END insert_itf;
------------------------------------------
FUNCTION insert_itf
(ir_trans_dtl_itf	IN	SC_TRANS_DTL_ITF%ROWTYPE
,i_upd				IN	VARCHAR2)
RETURN VARCHAR2
IS
	v_return			VARCHAR2(4000);
	r_trans_dtl_itf		SC_TRANS_DTL_ITF%ROWTYPE	:=	ir_trans_dtl_itf;
BEGIN
	INSERT INTO sc_trans_dtl_itf
		(ou_code
		,subinv_code
--		,doc_no		-- Update later
		,doc_type
		,seq_no
		,trans_code
		,qty	-- Get later from AMOUNT
		,unitprice
		,amount
		,std_price
		,inv_seq	-- Get later from SEQ_NO
--		,cn_flag	-- Use default value 'N'
		,net_amt	-- Get later from AMOUNT
		,gross_amt	-- Get later from AMOUNT
		,cre_by
		,cre_date
		,source_module
		,source_reference
		,hdr_source_ref
		,file_id
		,line_num
--		,itf_status	-- Use default value 'N'
		)
	VALUES
		(r_trans_dtl_itf.ou_code
		,r_trans_dtl_itf.subinv_code
--		,doc_no
		,r_trans_dtl_itf.doc_type
		,r_trans_dtl_itf.seq_no
		,r_trans_dtl_itf.trans_code
		,r_trans_dtl_itf.amount	--qty
		,1	-- unitprice
		,r_trans_dtl_itf.amount
		,1	-- std_price
		,r_trans_dtl_itf.seq_no	--inv_seq
--		,cn_flag
		,r_trans_dtl_itf.amount	--net_amt
		,r_trans_dtl_itf.amount	--gross_amt 
		,i_upd		-- cre_by
		,SYSDATE	-- cre_date
		,r_trans_dtl_itf.source_module
		,r_trans_dtl_itf.source_reference
		,r_trans_dtl_itf.hdr_source_ref
		,r_trans_dtl_itf.file_id
		,r_trans_dtl_itf.line_num
--		,itf_status
		);
	GR_LOG.PROCESS_DATE	:=	SYSDATE;
	GR_LOG.SEQ			:=	G_INF_SEQ;
	GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Insert ITF Data';
	GR_LOG.LOG_DESC		:=	SUBSTR('Line# : ' || TO_CHAR(r_trans_dtl_itf.line_num,'000000') || ', Line Type : D, Source Reference : ' || r_trans_dtl_itf.source_reference, 1, 400);
	GR_LOG.INF_UPD_DATE	:=	SYSDATE;
-- Not log non-Error	WRITE_LOG(GR_LOG);
	G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
	RETURN glb_text_output_util.NO_ERROR;
EXCEPTION
	WHEN OTHERS THEN
		v_return			:=	glb_text_output_util.C_ERROR || SQLERRM;
		GR_LOG.PROCESS_DATE	:=	SYSDATE;
		GR_LOG.SEQ			:=	G_INF_SEQ;
		GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Insert ITF Data Error - ' || SQLERRM;
		GR_LOG.LOG_DESC		:=	SUBSTR('Line# : ' || TO_CHAR(r_trans_dtl_itf.line_num,'000000') || ', Line Type : D, Source Reference : ' || r_trans_dtl_itf.source_reference, 1, 400);
		GR_LOG.INF_UPD_DATE	:=	SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ			:=	NVL(G_INF_SEQ, 0) + 1;
		G_CNT_DTL_ERROR		:=	G_CNT_DTL_ERROR + 1;
		mark_file_status('E');
		RETURN v_return;
END insert_itf;
------------------------------------------
FUNCTION insert_itf
(ir_trans_pay_itf	IN	SC_TRANS_PAY_ITF%ROWTYPE
,i_upd				IN	VARCHAR2)
RETURN VARCHAR2
IS
	v_return			VARCHAR2(4000);
	r_trans_pay_itf		SC_TRANS_PAY_ITF%ROWTYPE	:=	ir_trans_pay_itf;
BEGIN
	IF r_trans_pay_itf.bank_code IS NOT NULL AND r_trans_pay_itf.credit_card IS NOT NULL THEN
		r_trans_pay_itf.cr_bank_rate	:=	SA_CREDITRATE_PKG.GET_RATE (r_trans_pay_itf.bank_code, r_trans_pay_itf.credit_card);
	END IF;
	r_trans_pay_itf.pay_net		:=	r_trans_pay_itf.pay_amt;
	INSERT INTO sc_trans_pay_itf
		(ou_code
		,subinv_code
		,doc_type
--		,doc_no		-- Update later
		,payment_code
		,ref_no
		,bank_code
		,credit_card
		,cr_bank_rate
		,pay_amt
		,approve_no
		,pay_net
		,cre_by
		,cre_date
		,source_module
		,source_reference
		,hdr_source_ref
		,file_id
		,line_num
--		,itf_status	-- Use default value 'N'
		)
	VALUES
		(r_trans_pay_itf.ou_code
		,r_trans_pay_itf.subinv_code
		,r_trans_pay_itf.doc_type
--		,doc_no
		,r_trans_pay_itf.payment_code
		,r_trans_pay_itf.ref_no
		,r_trans_pay_itf.bank_code
		,r_trans_pay_itf.credit_card
		,r_trans_pay_itf.cr_bank_rate
		,r_trans_pay_itf.pay_amt
		,r_trans_pay_itf.approve_no
		,r_trans_pay_itf.pay_net
		,i_upd		-- cre_by
		,SYSDATE	-- cre_date
		,r_trans_pay_itf.source_module
		,r_trans_pay_itf.source_reference
		,r_trans_pay_itf.hdr_source_ref
		,r_trans_pay_itf.file_id
		,r_trans_pay_itf.line_num
--		,itf_status
		);
	GR_LOG.PROCESS_DATE	:=	SYSDATE;
	GR_LOG.SEQ			:=	G_INF_SEQ;
	GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Insert ITF Data';
	GR_LOG.LOG_DESC		:=	SUBSTR('Line# : ' || TO_CHAR(r_trans_pay_itf.line_num,'000000') || ', Line Type : P, Source Reference : ' || r_trans_pay_itf.source_reference, 1, 400);
	GR_LOG.INF_UPD_DATE	:=	SYSDATE;
-- Not log non-Error	WRITE_LOG(GR_LOG);
	G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
	RETURN glb_text_output_util.NO_ERROR;
EXCEPTION
	WHEN OTHERS THEN
		v_return			:=	glb_text_output_util.C_ERROR || SQLERRM;
		GR_LOG.PROCESS_DATE	:=	SYSDATE;
		GR_LOG.SEQ			:=	G_INF_SEQ;
		GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Insert ITF Data Error - ' || SQLERRM;
		GR_LOG.LOG_DESC		:=	SUBSTR('Line# : ' || TO_CHAR(r_trans_pay_itf.line_num,'000000') || ', Line Type : P, Source Reference : ' || r_trans_pay_itf.source_reference, 1, 400);
		GR_LOG.INF_UPD_DATE	:=	SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ			:=	NVL(G_INF_SEQ, 0) + 1;
		G_CNT_PAY_ERROR		:=	G_CNT_PAY_ERROR + 1;
		mark_file_status('E');
		RETURN v_return;
END insert_itf;
------------------------------------------
PROCEDURE validate_n_dflt
(i_upd		IN	VARCHAR2
,i_pgm		IN	VARCHAR2
,P_RESULT	OUT	VARCHAR2)
IS
	b_error			BOOLEAN;
	b_overall_error	BOOLEAN	:=	FALSE;
	CURSOR c_vat IS
		SELECT	vat_code
		FROM	sc_transaction_itf t
		WHERE	t.file_id	=	G_FILE_ID
		AND		t.ou_code	=	G_OU_CODE
		AND		t.vat_code IS NOT NULL
		GROUP BY vat_code
		ORDER BY vat_code;
	CURSOR c_cust IS
		SELECT	t.ou_code, t.cust_code, MIN(t.line_num) first_line, MAX(t.line_num) last_line, COUNT(*) n_rows
		FROM	sc_transaction_itf t
		WHERE	t.file_id	=	G_FILE_ID
		AND		t.ou_code	=	G_OU_CODE
		GROUP BY t.ou_code, t.cust_code
		ORDER BY t.ou_code, t.cust_code;
	CURSOR c_cust_info
		(i_ou_code		IN	VARCHAR2
		,i_cust_code	IN	VARCHAR2) IS
		SELECT	cust_code,
				rtrim(ltrim(substr(pr.pre_name||' '||ct.first_name||' '||ct.last_name,1,200) )) cust_name,
				ct.CUR_ADD,
				ct.CUR_AMPHUR,     
				ct.CUR_PROVINCE,
				ct.CUR_POSTAL,
				ct.CUR_TEL,
				ct.cat_code,
				ct.branch_code,
				ct.doc_no	tax_id
		FROM    ci_pre_name pr, ci_customer ct
		WHERE   ct.ou_code     = i_ou_code
		AND     ct.CUST_CODE   = i_cust_code
		AND     pr.pre_code    = ct.pre_code
		AND     ct.INACTIVE IS NULL;
BEGIN
	-->> Initial log
	GR_LOG.PROCESS_ID		:=	G_PROCESSID;
	GR_LOG.INF_UPD_BY		:=	i_upd;
	GR_LOG.INF_UPD_PGM		:=	i_pgm;

	-->> Lock Transaction Pending.
	GR_LOG.STEP				:=	G_FILE_NUM + .2;
	GR_LOG.STEP_DESC		:=	'Validate data';
	GR_LOG.PROCESS_FLAG		:=	'Prepare Data';
	GR_LOG.INF_LOCK_FLAG	:=	'Lock';

---------------- CRITICAL ERROR
-- 14.1 SOURCE_REFERENCE ����� Header record ������ӡѹ�Ѻ�����ŷ�����������к�����
	FOR r_data IN  (SELECT	t.*
					FROM	sc_transaction_itf t, sc_transaction tx
					WHERE	t.file_id			=	G_FILE_ID
					AND		t.ou_code			=	G_OU_CODE
					AND		t.source_module		=	tx.source_module
					AND		t.source_reference	=	tx.source_reference)
	LOOP
		b_overall_error	:=	TRUE;	-- Error found
		
		P_RESULT			:=	'Header with source_reference already interfaced';
		GR_LOG.PROCESS_DATE	:=	SYSDATE;
		GR_LOG.SEQ			:=	G_INF_SEQ;
		GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Critical Error : ' || P_RESULT;
		GR_LOG.LOG_DESC		:=	'Error : Header with interfaced source_reference=' || r_data.source_reference ||
								' (line# ' || r_data.line_num || ')';
		GR_LOG.INF_UPD_DATE	:=	SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
	END LOOP;

-- 14.2 SOURCE_REFERENCE ����� Header record ������ӡѹ�ͧ
	FOR rec IN (SELECT	t.source_reference
				FROM	sc_transaction_itf t
				WHERE	file_id		=	G_FILE_ID
				AND		ou_code		=	G_OU_CODE
				GROUP BY t.source_reference
				HAVING	COUNT(*)	>	1)
	LOOP
		b_overall_error	:=	TRUE;	-- Error found
		FOR r_data IN  (SELECT	*
						FROM	sc_transaction_itf t
						WHERE	file_id				=	G_FILE_ID
						AND		ou_code				=	G_OU_CODE
						AND		source_reference	=	rec.source_reference
						ORDER BY line_num)
		LOOP
			P_RESULT			:=	'Header with duplicate source_reference';
			GR_LOG.PROCESS_DATE	:=	SYSDATE;
			GR_LOG.SEQ			:=	G_INF_SEQ;
			GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Critical Error : ' || P_RESULT;
			GR_LOG.LOG_DESC		:=	'Error : Header with duplicate source_reference=' || r_data.source_reference ||
									' (line# ' || r_data.line_num || ')';
			GR_LOG.INF_UPD_DATE	:=	SYSDATE;
			WRITE_LOG(GR_LOG);
			G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		END LOOP;
	END LOOP;

-- 15. SOURCE_REFERENCE ����� Detail record ������ӡѹ�Ѻ�����ŷ��������� ���� Header record ���ǡѹ
	FOR rec IN (SELECT	t.hdr_source_ref, t.source_reference
				FROM	sc_trans_dtl_itf t
				WHERE	file_id		=	G_FILE_ID
				AND		ou_code		=	G_OU_CODE
				GROUP BY t.hdr_source_ref, t.source_reference
				HAVING	COUNT(*)	>	1)
	LOOP
		b_overall_error	:=	TRUE;	-- Error found
		FOR r_data IN  (SELECT	*
						FROM	sc_trans_dtl_itf t
						WHERE	file_id				=	G_FILE_ID
						AND		ou_code				=	G_OU_CODE
						AND		hdr_source_ref		=	rec.hdr_source_ref
						AND		source_reference	=	rec.source_reference
						ORDER BY line_num)
		LOOP
			P_RESULT			:=	'Detail with duplicate source_reference under same header';
			GR_LOG.PROCESS_DATE	:=	SYSDATE;
			GR_LOG.SEQ			:=	G_INF_SEQ;
			GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Critical Error : ' || P_RESULT;
			GR_LOG.LOG_DESC		:=	'Error : Detail with duplicate source_reference=' || r_data.source_reference ||
									' Header''s source_reference=' || r_data.hdr_source_ref ||
									' (line# ' || r_data.line_num || ')';
			GR_LOG.INF_UPD_DATE	:=	SYSDATE;
			WRITE_LOG(GR_LOG);
			G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		END LOOP;
	END LOOP;

-- 16. SOURCE_REFERENCE ����� Payment record ������ӡѹ�Ѻ�����ŷ��������� ���� Header record ���ǡѹ
	FOR rec IN (SELECT	t.hdr_source_ref, t.source_reference
				FROM	sc_trans_pay_itf t
				WHERE	file_id		=	G_FILE_ID
				AND		ou_code		=	G_OU_CODE
				GROUP BY t.hdr_source_ref, t.source_reference
				HAVING	COUNT(*)	>	1)
	LOOP
		b_overall_error	:=	TRUE;	-- Error found
		FOR r_data IN  (SELECT	*
						FROM	sc_trans_pay_itf t
						WHERE	file_id				=	G_FILE_ID
						AND		ou_code				=	G_OU_CODE
						AND		hdr_source_ref		=	rec.hdr_source_ref
						AND		source_reference	=	rec.source_reference
						ORDER BY line_num)
		LOOP
			P_RESULT			:=	'Payment with duplicate source_reference under same header';
			GR_LOG.PROCESS_DATE	:=	SYSDATE;
			GR_LOG.SEQ			:=	G_INF_SEQ;
			GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Critical Error : ' || P_RESULT;
			GR_LOG.LOG_DESC		:=	'Error : Payment with duplicate source_reference=' || r_data.source_reference ||
									' Header''s source_reference=' || r_data.hdr_source_ref ||
									' (line# ' || r_data.line_num || ')';
			GR_LOG.INF_UPD_DATE	:=	SYSDATE;
			WRITE_LOG(GR_LOG);
			G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		END LOOP;
	END LOOP;

	IF b_overall_error THEN
		mark_file_status('E');
		RETURN;	-- Return upon critical error
	END IF;

---------------- NORMAL ERROR
-- 1. ��ͧ�觢����ŵ���ӴѺ 1 Header record ������� Detail ��� Payment ���ú��͹������� Header record ����
--		Header with no Detail
	FOR rec IN (SELECT	t.source_reference
				FROM	sc_transaction_itf t
				WHERE	file_id		=	G_FILE_ID
				AND		ou_code		=	G_OU_CODE
				MINUS
				SELECT	d.hdr_source_ref
				FROM	sc_trans_dtl_itf d
				WHERE	file_id		=	G_FILE_ID
				AND		ou_code		=	G_OU_CODE)
	LOOP
		b_overall_error	:=	TRUE;	-- Error found
		FOR r_data IN  (SELECT	*
						FROM	sc_transaction_itf t
						WHERE	file_id				=	G_FILE_ID
						AND		ou_code				=	G_OU_CODE
						AND		source_reference	=	rec.source_reference)
		LOOP
			P_RESULT			:=	'Header with no detail';
			GR_LOG.PROCESS_DATE	:=	SYSDATE;
			GR_LOG.SEQ			:=	G_INF_SEQ;
			GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Validate Error : ' || P_RESULT;
			GR_LOG.LOG_DESC		:=	'Error : Header with no detail, source_reference=' || r_data.source_reference ||
									' (line# ' || r_data.line_num || ')';
			GR_LOG.INF_UPD_DATE	:=	SYSDATE;
			WRITE_LOG(GR_LOG);
			G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		END LOOP;
	END LOOP;

-- 1. ��ͧ�觢����ŵ���ӴѺ 1 Header record ������� Detail ��� Payment ���ú��͹������� Header record ����
--		Header with no Payment
	FOR rec IN (SELECT	t.source_reference
				FROM	sc_transaction_itf t
				WHERE	file_id		=	G_FILE_ID
				AND		ou_code		=	G_OU_CODE
				MINUS
				SELECT	d.hdr_source_ref
				FROM	sc_trans_pay_itf d
				WHERE	file_id		=	G_FILE_ID
				AND		ou_code		=	G_OU_CODE)
	LOOP
		b_overall_error	:=	TRUE;	-- Error found
		FOR r_data IN  (SELECT	*
						FROM	sc_transaction_itf t
						WHERE	file_id				=	G_FILE_ID
						AND		ou_code				=	G_OU_CODE
						AND		source_reference	=	rec.source_reference)
		LOOP
			P_RESULT			:=	'Header with no payment';
			GR_LOG.PROCESS_DATE	:=	SYSDATE;
			GR_LOG.SEQ			:=	G_INF_SEQ;
			GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Validate Error : ' || P_RESULT;
			GR_LOG.LOG_DESC		:=	'Error : Header with no detail, source_reference=' || r_data.source_reference ||
									' (line# ' || r_data.line_num || ')';
			GR_LOG.INF_UPD_DATE	:=	SYSDATE;
			WRITE_LOG(GR_LOG);
			G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		END LOOP;
	END LOOP;

-- 2. ���ǹ�ͧ Header record  ����Ǩ�ͺ��� COMPANY_CODE ����к��� ��ͧ�������ԧ� setup �ͧ�к� POS ����
--		==> Cancelled ��������ҵ�ͧ�ç�Ѻ������� ���ǹ�ٻ��� data ��к���������

-- 3. ���ǹ�ͧ Header record  ����Ǩ�ͺ��� SUBINV_CODE ����к��� ��ͧ�������ԧ� setup �ͧ�к� POS ����������ԧ����� COMPANY_CODE ����кش���
	FOR rec IN (SELECT	subinv_code
				FROM	sc_transaction_itf t
				WHERE	file_id		=	G_FILE_ID
				AND		ou_code		=	G_OU_CODE
				GROUP BY subinv_code
				MINUS
				SELECT	s.subinv_code
				FROM	pb_subinv s
				WHERE	s.ou_code	=	G_OU_CODE)
	LOOP
		b_overall_error	:=	TRUE;	-- Error found
		FOR r_data IN  (SELECT	*
						FROM	sc_transaction_itf t
						WHERE	file_id		=	G_FILE_ID
						AND		ou_code		=	G_OU_CODE
						AND		subinv_code	=	rec.subinv_code
						ORDER BY t.line_num)
		LOOP
			P_RESULT			:=	'Incorrect SUBINV_CODE';
			GR_LOG.PROCESS_DATE	:=	SYSDATE;
			GR_LOG.SEQ			:=	G_INF_SEQ;
			GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Validate Error : ' || P_RESULT;
			GR_LOG.LOG_DESC		:=	'Error : Subinventory Code=' || r_data.subinv_code ||
									' not found (line# ' || r_data.line_num || ')';
			GR_LOG.INF_UPD_DATE	:=	SYSDATE;
			WRITE_LOG(GR_LOG);
			G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		END LOOP;
	END LOOP;

-- 4. ���ǹ�ͧ Header record  ����Ǩ�ͺ��� CUSTOMER_CODE ����к��� ��ͧ�������ԧ��к� POS
	FOR r_cust IN c_cust
	LOOP
		b_error	:=	TRUE;
		FOR r_master IN c_cust_info	(i_ou_code		=>	r_cust.ou_code
									,i_cust_code	=>	r_cust.cust_code)
		LOOP
			b_error	:=	FALSE;
			UPDATE	sc_transaction_itf t
			SET		t.cust_name			=	NVL(t.cust_name,	r_master.cust_name)
					,t.cust_add			=	NVL(t.cust_add,		r_master.cur_add)
					,t.cust_amphur		=	NVL(t.cust_amphur,	r_master.cur_amphur)
					,t.cust_province	=	NVL(t.cust_province,r_master.cur_province)
					,t.cust_postal		=	NVL(t.cust_postal,	r_master.cur_postal)
					,t.cust_tel			=	NVL(t.cust_tel,		r_master.cur_tel)
					,t.cat_code			=	NVL(t.cat_code,		r_master.cat_code)
					,t.branch_code		=	NVL(t.branch_code,	r_master.branch_code)
					,t.tax_id			=	NVL(t.tax_id,		r_master.tax_id)
			WHERE	t.file_id	=	G_FILE_ID
			AND		t.ou_code	=	G_OU_CODE
			AND		t.cust_code	=	r_master.cust_code;
			EXIT;	-- Process once
		END LOOP;
		IF b_error THEN
			P_RESULT			:=	'Incorrect CUSTOMER_CODE';
			GR_LOG.PROCESS_DATE	:=	SYSDATE;
			GR_LOG.SEQ			:=	G_INF_SEQ;
			GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Validate Error : ' || P_RESULT;
			GR_LOG.LOG_DESC		:=	'Error : Customer Code=' || r_cust.cust_code ||
									' not found (line# ' || r_cust.first_line || '-' || r_cust.last_line ||
									', total=' || r_cust.n_rows || ' lines)';

			GR_LOG.INF_UPD_DATE	:=	SYSDATE;
			WRITE_LOG(GR_LOG);
			G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;

			b_overall_error	:=	TRUE;	-- Error found
		END IF;
	END LOOP;

-- 5. ���ǹ�ͧ Header record  ����Ǩ�ͺ��� CUSTOMER_PROVINCE ����к��� ��ͧ�������ԧ� setup �ͧ�к� POS
	FOR rec IN (SELECT	t.cust_province
				FROM	sc_transaction_itf t
				WHERE	file_id		=	G_FILE_ID
				AND		ou_code		=	G_OU_CODE
				AND		cust_province IS NOT NULL
				GROUP BY cust_province
				MINUS
				SELECT	province_code
				FROM	pb_province p
				WHERE	inactive IS NULL)
	LOOP
		b_overall_error	:=	TRUE;	-- Error found
		FOR r_data IN  (SELECT	*
						FROM	sc_transaction_itf t
						WHERE	file_id			=	G_FILE_ID
						AND		ou_code			=	G_OU_CODE
						AND		cust_province	=	rec.cust_province
						ORDER BY t.line_num)
		LOOP
			P_RESULT			:=	'Incorrect CUSTOMER_PROVINCE';
			GR_LOG.PROCESS_DATE	:=	SYSDATE;
			GR_LOG.SEQ			:=	G_INF_SEQ;
			GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Validate Error : ' || P_RESULT;
			GR_LOG.LOG_DESC		:=	'Error : Customer Province=' || r_data.cust_province ||
									' not found (line# ' || r_data.line_num || ')';
			GR_LOG.INF_UPD_DATE	:=	SYSDATE;
			WRITE_LOG(GR_LOG);
			G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		END LOOP;
	END LOOP;

-- 6. ���ǹ�ͧ Header record  ����Ǩ�ͺ��� CUSTOMER_AMPHUR ����к��� ��ͧ�������ԧ� setup �ͧ�к� POS
--		������������ CUSTOMER_PROVINCE ����кش��� (����кص�ͧ�кط�����ʨѧ��Ѵ ������������)
	FOR rec IN (SELECT	t.cust_province, t.cust_amphur
				FROM	sc_transaction_itf t
				WHERE	file_id		=	G_FILE_ID
				AND		ou_code		=	G_OU_CODE
				AND		(cust_province IS NOT NULL OR cust_amphur IS NOT NULL)
				GROUP BY cust_province, t.cust_amphur
				MINUS
				SELECT	province_code, amphur_code
				FROM	pb_amphur
				WHERE	inactive IS NULL)
	LOOP
		b_overall_error	:=	TRUE;	-- Error found
		FOR r_data IN  (SELECT	*
						FROM	sc_transaction_itf t
						WHERE	file_id			=	G_FILE_ID
						AND		ou_code			=	G_OU_CODE
						AND		(cust_province	=	rec.cust_province	OR cust_province || rec.cust_province IS NULL)
						AND		(cust_amphur	=	rec.cust_amphur		OR cust_amphur || rec.cust_amphur IS NULL)
						ORDER BY t.line_num)
		LOOP
			P_RESULT			:=	'Incorrect CUSTOMER_AMPHUR';
			GR_LOG.PROCESS_DATE	:=	SYSDATE;
			GR_LOG.SEQ			:=	G_INF_SEQ;
			GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Validate Error : ' || P_RESULT;
			GR_LOG.LOG_DESC		:=	'Error : Customer Province=' || r_data.cust_province ||
									' Customer Amphur=' || r_data.cust_amphur ||
									' not found (line# ' || r_data.line_num || ')';
			GR_LOG.INF_UPD_DATE	:=	SYSDATE;
			WRITE_LOG(GR_LOG);
			G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		END LOOP;
	END LOOP;

-- 7. ���ǹ�ͧ Header record  ����Ǩ�ͺ��Ҷ���к� CUSTOMER_TAX_ID  ��ͧ�� 13 digits ����
--		㹡ó�����к������ ���礨ҡ��ҷ�����Ҩҡ�������١�����к�  (������Ѻ��� null ��)
	FOR r_data IN (SELECT	*
				FROM	sc_transaction_itf t
				WHERE	file_id		=	G_FILE_ID
				AND		ou_code		=	G_OU_CODE
				AND		tax_id IS NOT NULL
				AND		LENGTH(RTRIM(t.tax_id)) <> 13)
	LOOP
		b_overall_error		:=	TRUE;	-- Error found

		P_RESULT			:=	'Incorrect CUSTOMER_TAX_ID';
		GR_LOG.PROCESS_DATE	:=	SYSDATE;
		GR_LOG.SEQ			:=	G_INF_SEQ;
		GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Validate Error : ' || P_RESULT;
		GR_LOG.LOG_DESC		:=	'Error : Customer Tax ID=' || r_data.tax_id ||
								' - value specified with length <> 13 digits (line# ' || r_data.line_num || ')';
		GR_LOG.INF_UPD_DATE	:=	SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
	END LOOP;

-- 8. Validate ����к� CUSTOMER_BRANCH_CODE ��ͧ�к� CUSTOMER_TAX_ID ���� (�ҡ��ҷ������ ���ͨ��繤�ҷ���Ҩҡ�������١�����к�����)
	FOR r_data IN  (SELECT	*
					FROM	sc_transaction_itf t
					WHERE	file_id		=	G_FILE_ID
					AND		ou_code		=	G_OU_CODE
					AND		t.branch_code IS NOT NULL
					AND		tax_id IS NULL)
	LOOP
		b_overall_error		:=	TRUE;	-- Error found

		P_RESULT			:=	'Incorrect CUSTOMER_BRANCH_CODE & CUSTOMER_TAX_ID';
		GR_LOG.PROCESS_DATE	:=	SYSDATE;
		GR_LOG.SEQ			:=	G_INF_SEQ;
		GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Validate Error : ' || P_RESULT;
		GR_LOG.LOG_DESC		:=	'Error : Customer Branch Code=' || r_data.branch_code || ' with Tax ID=NULL' ||
								' (line# ' || r_data.line_num || ')';
		GR_LOG.INF_UPD_DATE	:=	SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
	END LOOP;

-- 9. Validate ���ǹ�ͧ Header record  ����Ǩ�ͺ��� TOTAL_AMOUNT ����к��� ��ͧ <> 0
	FOR r_data IN  (SELECT	*
					FROM	sc_transaction_itf t
					WHERE	file_id		=	G_FILE_ID
					AND		ou_code		=	G_OU_CODE
					AND		(t.total_amt IS NULL OR t.total_amt = 0) )
	LOOP
		b_overall_error		:=	TRUE;	-- Error found

		P_RESULT			:=	'Incorrect TOTAL_AMOUNT';
		GR_LOG.PROCESS_DATE	:=	SYSDATE;
		GR_LOG.SEQ			:=	G_INF_SEQ;
		GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Validate Error : ' || P_RESULT;
		GR_LOG.LOG_DESC		:=	'Error : Total Amount=' ||
								CASE WHEN r_data.total_amt IS NULL THEN 'NULL' ELSE r_data.total_amt END ||
								' (line# ' || r_data.line_num || ')';
		GR_LOG.INF_UPD_DATE	:=	SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
	END LOOP;

-- 10. Validate ���ǹ�ͧ Header record  ����Ǩ�ͺ��� VAT_CODE ����к��� ��ͧ�������ԧ� setup �ͧ�к� POS ����
	FOR r_vat IN c_vat
	LOOP
		b_error	:=	TRUE;
		FOR r_master IN (SELECT vat_code, vat_rate FROM sa_vat WHERE vat_code = r_vat.vat_code AND INACTIVE IS NULL)
		LOOP
			b_error	:=	FALSE;
			UPDATE	sc_transaction_itf t
			SET		t.vat_rate	=	r_master.vat_rate
			WHERE	t.file_id	=	G_FILE_ID
			AND		ou_code		=	G_OU_CODE
			AND		t.vat_code	=	r_master.vat_code;
			EXIT;	-- Process once
		END LOOP;
		IF b_error THEN
			P_RESULT	:=	'VAT Code Error';
			FOR r_data IN  (SELECT	*
							FROM	sc_transaction_itf t
							WHERE	file_id		=	G_FILE_ID
							AND		ou_code		=	G_OU_CODE
							AND		vat_code	=	r_vat.vat_code
							ORDER BY line_num)
			LOOP
				GR_LOG.PROCESS_DATE	:=	SYSDATE;
				GR_LOG.SEQ			:=	G_INF_SEQ;
				GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Validate Error : ' || P_RESULT;
				GR_LOG.LOG_DESC		:=	'Error : VAT Code=' || r_data.vat_code ||
										' not found (line# ' || r_data.line_num || ')';
				GR_LOG.INF_UPD_DATE	:=	SYSDATE;
				WRITE_LOG(GR_LOG);
				G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
			END LOOP;
			b_overall_error	:=	TRUE;	-- Error 1 means overall error
		END IF;
	END LOOP;

-- 11. Validate ���ǹ�ͧ Header record  ����Ǩ�ͺ��� VAT_AMOUNT ����к��� ��ͧ��ҡѺ TOTAL_AMOUNT * VAT % ��� VAT_CODE �к���
--		��������� Diff ��ͧ����Թ���� Tolerance ����˹� (��˹�� parameter �ͧ process)
	FOR r_data IN  (SELECT	t.*, ROUND(t.total_amt * NVL(t.vat_rate,0) / (100 + NVL(t.vat_rate,0)), 2) vat_estimate
					FROM	sc_transaction_itf t
					WHERE	file_id		=	G_FILE_ID
					AND		ou_code		=	G_OU_CODE
					AND		NVL(t.vat_amt,0) NOT BETWEEN ROUND(t.total_amt * NVL(t.vat_rate,0) / (100 + NVL(t.vat_rate,0)), 2) - NVL(G_VAT_TOLERANCE,0)
												 AND	 ROUND(t.total_amt * NVL(t.vat_rate,0) / (100 + NVL(t.vat_rate,0)), 2) + NVL(G_VAT_TOLERANCE,0)
					ORDER BY line_num)
	LOOP
		b_overall_error		:=	TRUE;	-- Error found

		P_RESULT			:=	'Incorrect VAT_AMOUNT';
		GR_LOG.PROCESS_DATE	:=	SYSDATE;
		GR_LOG.SEQ			:=	G_INF_SEQ;
		GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Validate Error : ' || P_RESULT;
		GR_LOG.LOG_DESC		:=	'Error : VAT Amount=' || r_data.vat_amt || ', Estimated VAT=' || r_data.vat_estimate ||
								', with Tolerance=' || NVL(G_VAT_TOLERANCE,0) || 
								' (line# ' || r_data.line_num || ')';
		GR_LOG.INF_UPD_DATE	:=	SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
	END LOOP;

-- 12. ���ǹ�ͧ Header record  ����Ǩ�ͺ��� SALESPERSON ����к��� ��ͧ�������ԧ� setup �ͧ�к� POS
--		������Է�Էӧҹ��Ңҷ���кص�� SUBINV_CODE ������� � �ѹ����� DOCUMENT_DATE ����к��Ҵ���
	FOR r_sales IN (SELECT	t.subinv_code, t.salesperson, TRUNC(t.doc_date) document_date
							,MIN(t.line_num) first_line, MAX(t.line_num) last_line, COUNT(*) n_rows
					FROM	sc_transaction_itf t
					WHERE	file_id		=	G_FILE_ID
					AND		ou_code		=	G_OU_CODE
					GROUP BY t.subinv_code, t.salesperson, TRUNC(t.doc_date))
	LOOP
		b_error	:=	TRUE;
		FOR r_master IN (SELECT	s.person_code, s.team_header, s.area_mgr, s.shop_mgr
						FROM	sa_salesperson s, sa_salesperson_subinv_vm ss
						WHERE	s.person_code	=	ss.person_code
						AND		ss.subinv_code	=	r_sales.subinv_code
						AND		ss.person_code	=	r_sales.salesperson
						AND		ss.eff_date		<=	r_sales.document_date
						AND		r_sales.document_date BETWEEN NVL(ss.start_date,C_DFLT_START_DATE) AND NVL(ss.end_date,C_DFLT_END_DATE)
						ORDER BY s.eff_date DESC, ss.start_date DESC )
		LOOP
			b_error	:=	FALSE;
/* Original Logic from
			SA_SALESPERSON_PKG.GET_SALES_TEAM
				(r_transaction_itf.salesperson, TRUNC(r_transaction_itf.doc_date),
				 r_transaction_itf.team_header, r_transaction_itf.area_mgr, r_transaction_itf.shop_mgr);*/
			UPDATE	sc_transaction_itf t
			SET		t.team_header	=	r_master.team_header
					,t.area_mgr		=	r_master.area_mgr
					,t.shop_mgr		=	r_master.shop_mgr
			WHERE	t.file_id		=	G_FILE_ID
			AND		ou_code			=	G_OU_CODE
			AND		t.salesperson	=	r_master.person_code;
			EXIT;	-- Process once
		END LOOP;
		IF b_error THEN
			P_RESULT	:=	'Incorrect SALESPERSON';
			FOR r_data IN  (SELECT	*
							FROM	sc_transaction_itf t
							WHERE	file_id		=	G_FILE_ID
							AND		ou_code		=	G_OU_CODE
							AND		subinv_code	=	r_sales.subinv_code
							AND		salesperson	=	r_sales.salesperson
							AND		t.doc_date BETWEEN TRUNC(r_sales.document_date) AND TRUNC(r_sales.document_date) + 0.99999
							ORDER BY line_num)
			LOOP
				GR_LOG.PROCESS_DATE	:=	SYSDATE;
				GR_LOG.SEQ			:=	G_INF_SEQ;
				GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Validate Error : ' || P_RESULT;
				GR_LOG.LOG_DESC		:=	'Error : Subinventory Code=' || r_data.subinv_code ||
										' Salesperson=' || r_data.salesperson ||
										' Document Date=' || r_data.doc_date ||
										' not found (line# ' || r_data.line_num || ')';
				GR_LOG.INF_UPD_DATE	:=	SYSDATE;
				WRITE_LOG(GR_LOG);
				G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
			END LOOP;
			b_overall_error	:=	TRUE;	-- Error 1 means overall error
		END IF;
	END LOOP;

-- 13.1 ����� Header ��ͧ��Ǩ�ͺ Total_Amount = SUM(Amount �ҡ Detail record)
	FOR r_data IN  (SELECT	t.line_num, t.source_reference, t.total_amt, SUM(d.amount) sum_dtl_amt
					FROM	sc_transaction_itf t, sc_trans_dtl_itf d
					WHERE	t.file_id			=	G_FILE_ID
					AND		t.ou_code			=	G_OU_CODE
					AND		d.file_id			=	G_FILE_ID
					AND		d.ou_code			=	G_OU_CODE
					AND		d.hdr_source_ref	=	t.source_reference
					GROUP BY t.line_num, t.source_reference, t.total_amt
--					HAVING	t.total_amt <> SUM(d.amount)	-- Get all
					ORDER BY t.line_num)
	LOOP
		IF r_data.total_amt = r_data.sum_dtl_amt THEN
			UPDATE	sc_transaction_itf t
			SET		net_amt		=	r_data.sum_dtl_amt
					,gross_amt	=	r_data.sum_dtl_amt
			WHERE	file_id		=	G_FILE_ID
			AND		ou_code		=	G_OU_CODE
			AND		source_reference	=	r_data.source_reference;
		ELSE
			b_overall_error		:=	TRUE;	-- Error found

			P_RESULT			:=	'Incorrect TOTAL_AMOUNT <> SUM(AMOUNT in Detail)';
			GR_LOG.PROCESS_DATE	:=	SYSDATE;
			GR_LOG.SEQ			:=	G_INF_SEQ;
			GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Validate Error : ' || P_RESULT;
			GR_LOG.LOG_DESC		:=	'Error : Total Amount=' || r_data.total_amt || ', Total Amount in Detail=' || r_data.sum_dtl_amt ||
									' (line# ' || r_data.line_num || ')';
			GR_LOG.INF_UPD_DATE	:=	SYSDATE;
			WRITE_LOG(GR_LOG);
			G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		END IF;
	END LOOP;

-- 13.2 ����� Header ��ͧ��Ǩ�ͺ Total_Amount = SUM(Pay_Amt �ҡ Payment record)
	FOR r_data IN  (SELECT	t.line_num, t.source_reference, t.total_amt, SUM(p.pay_amt) sum_pay_amt
					FROM	sc_transaction_itf t, sc_trans_pay_itf p
					WHERE	t.file_id			=	G_FILE_ID
					AND		t.ou_code			=	G_OU_CODE
					AND		p.file_id			=	G_FILE_ID
					AND		p.ou_code			=	G_OU_CODE
					AND		p.hdr_source_ref	=	t.source_reference
					GROUP BY t.line_num, t.source_reference, t.total_amt
					HAVING	t.total_amt <> SUM(p.pay_amt)
					ORDER BY t.line_num)
	LOOP
		b_overall_error		:=	TRUE;	-- Error found

		P_RESULT			:=	'Incorrect TOTAL_AMOUNT <> SUM(AMOUNT in Payment)';
		GR_LOG.PROCESS_DATE	:=	SYSDATE;
		GR_LOG.SEQ			:=	G_INF_SEQ;
		GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Validate Error : ' || P_RESULT;
		GR_LOG.LOG_DESC		:=	'Error : Total Amount=' || r_data.total_amt || ', Total Amount in Payment=' || r_data.sum_pay_amt ||
								' (line# ' || r_data.line_num || ')';
		GR_LOG.INF_UPD_DATE	:=	SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
	END LOOP;

-- 17. ���ǹ�ͧ Detail record  ����Ǩ�ͺ��� SEQ_NO ������ӡѹ ���� Header record ���ǡѹ
	FOR rec IN (SELECT	d.hdr_source_ref, d.seq_no
				FROM	sc_trans_dtl_itf d
				WHERE	d.file_id	=	G_FILE_ID
				AND		d.ou_code	=	G_OU_CODE
				GROUP BY d.hdr_source_ref, d.seq_no
				HAVING	COUNT(*) <> 1)
	LOOP
		b_overall_error	:=	TRUE;	-- Error found
		FOR r_data IN  (SELECT	*
						FROM	sc_trans_dtl_itf t
						WHERE	file_id			=	G_FILE_ID
						AND		ou_code			=	G_OU_CODE
						AND		hdr_source_ref	=	rec.hdr_source_ref
						AND		seq_no			=	rec.seq_no
						ORDER BY t.line_num)
		LOOP
			P_RESULT			:=	'Detail : Duplicate SEQ_NO in same header';
			GR_LOG.PROCESS_DATE	:=	SYSDATE;
			GR_LOG.SEQ			:=	G_INF_SEQ;
			GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Validate Error : ' || P_RESULT;
			GR_LOG.LOG_DESC		:=	'Error : Header Source Reference=' || r_data.hdr_source_ref ||
									' Detail Sequence Num=' || r_data.seq_no ||
									' not found (line# ' || r_data.line_num || ')';
			GR_LOG.INF_UPD_DATE	:=	SYSDATE;
			WRITE_LOG(GR_LOG);
			G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		END LOOP;
	END LOOP;

-- 18. ���ǹ�ͧ Detail record  ����Ǩ�ͺ��� TRANS_CODE ����к��� ��ͧ�������ԧ� setup �ͧ�к� POS ����
	FOR r_error IN (SELECT	t.trans_code
					FROM	sc_trans_dtl_itf t
					WHERE	file_id		=	G_FILE_ID
					AND		ou_code		=	G_OU_CODE
					GROUP BY t.trans_code
					ORDER BY t.trans_code)
	LOOP
		b_error	:=	TRUE;
		P_RESULT	:=	'Incorrect TRANS_CODE (Not found)';
		FOR r_master IN (SELECT	trans_code, trans_desc, ccb_cust_req_flag
						FROM 	sc_trans_type
						WHERE 	ou_code		=	G_OU_CODE
						AND		trans_code	=	r_error.trans_code
						AND 	trunc(SYSDATE) BETWEEN NVL(eff_from, C_DFLT_START_DATE) AND NVL(eff_to, C_DFLT_END_DATE)
						AND		nvl(apple_care_status,'N')	<>	'Y')
		LOOP
			IF r_master.ccb_cust_req_flag = 'Y' THEN	-- Not possible to link
				P_RESULT	:=	'Incorrect TRANS_CODE (Need CCB Customer)';	-- Keep it as error to reuse same loop
			ELSE
				b_error	:=	FALSE;
			END IF;
			EXIT;	-- Process once
		END LOOP;
		IF b_error THEN
			FOR r_data IN  (SELECT	*
							FROM	sc_trans_dtl_itf t
							WHERE	file_id		=	G_FILE_ID
							AND		ou_code		=	G_OU_CODE
							AND		trans_code	=	r_error.trans_code
							ORDER BY line_num)
			LOOP
				GR_LOG.PROCESS_DATE	:=	SYSDATE;
				GR_LOG.SEQ			:=	G_INF_SEQ;
				GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Validate Error : ' || P_RESULT;
				GR_LOG.LOG_DESC		:=	'Error : Trans Code=' || r_data.trans_code ||
										' not found or incorrect (line# ' || r_data.line_num || ')';
				GR_LOG.INF_UPD_DATE	:=	SYSDATE;
				WRITE_LOG(GR_LOG);
				G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
			END LOOP;
			b_overall_error	:=	TRUE;	-- Error 1 means overall error
		END IF;
	END LOOP;

-- 19. ���ǹ�ͧ Payment record  ����Ǩ�ͺ��� SEQ_NO ������ӡѹ ���� Header record ���ǡѹ
--		Cancelled, as no seq_no in payment record

-- 20. ���ǹ�ͧ Payment record  ����Ǩ�ͺ��� PAYMENT_CODE ����к��� ��ͧ�������ԧ� setup �ͧ�к� POS ����
	FOR r_error IN (SELECT	t.payment_code
					FROM	sc_trans_pay_itf t
					WHERE	file_id		=	G_FILE_ID
					AND		ou_code		=	G_OU_CODE
					GROUP BY t.payment_code
					ORDER BY t.payment_code)
	LOOP
		b_error	:=	TRUE;
		FOR r_master IN (SELECT	payment_code, payment_desc, type_service, credit_card, change
						FROM 	sa_payment
						WHERE 	inactive IS NULL
						AND		NVL(type_service,'0')	!=	'2'
						AND		payment_code			=	r_error.payment_code)
		LOOP
			b_error	:=	FALSE;
	-- 23. ���ǹ�ͧ Payment  ����к� Payment Code ����繡�ê��д��ºѵ��ôԵ �е�ͧ�к� CREDIT_CARD_TYPE ��������
			IF r_master.credit_card = 'Y' THEN
			-- Original logic from Sa_Payment_Pkg.GET_CREDIT_CARD(r_master.payment_code)
				FOR r_data IN  (SELECT	*
								FROM	sc_trans_pay_itf t
								WHERE	file_id			=	G_FILE_ID
								AND		ou_code			=	G_OU_CODE
								AND		payment_code	=	r_error.payment_code
								AND		credit_card IS NULL
								ORDER BY line_num)
				LOOP
					P_RESULT	:=	'Incorrect CREDIT_CARD_TYPE in the PAYMENT_CODE';
					GR_LOG.PROCESS_DATE	:=	SYSDATE;
					GR_LOG.SEQ			:=	G_INF_SEQ;
					GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Validate Error : ' || P_RESULT;
					GR_LOG.LOG_DESC		:=	'Error : Payment Code=' || r_data.payment_code ||
											' Credit Card Type must be specified (line# ' || r_data.line_num || ')';
					GR_LOG.INF_UPD_DATE	:=	SYSDATE;
					WRITE_LOG(GR_LOG);
					G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
					b_overall_error	:=	TRUE;	-- Error 1 means overall error
				END LOOP;
			END IF;

	-- 24. ���ǹ�ͧ Payment  ����к� Payment Code ����繡�ê��д��ºѵ��ôԵ������ �е�ͧ�к� REF_NO ��������
			IF r_master.change = 'Y' THEN
			-- Original logic from Sa_Payment_Pkg.GET_CREDIT_CARD(r_master.payment_code)
				FOR r_data IN  (SELECT	*
								FROM	sc_trans_pay_itf t
								WHERE	file_id			=	G_FILE_ID
								AND		ou_code			=	G_OU_CODE
								AND		payment_code	=	r_error.payment_code
								AND		ref_no IS NULL
								ORDER BY line_num)
				LOOP
					P_RESULT	:=	'Incorrect REF_NO in the PAYMENT_CODE';
					GR_LOG.PROCESS_DATE	:=	SYSDATE;
					GR_LOG.SEQ			:=	G_INF_SEQ;
					GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Validate Error : ' || P_RESULT;
					GR_LOG.LOG_DESC		:=	'Error : Payment Code=' || r_data.payment_code ||
											' Payment Ref. No. must be specified (line# ' || r_data.line_num || ')';
					GR_LOG.INF_UPD_DATE	:=	SYSDATE;
					WRITE_LOG(GR_LOG);
					G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
					b_overall_error	:=	TRUE;	-- Error 1 means overall error
				END LOOP;
			END IF;

			EXIT;	-- Process once
		END LOOP;
		IF b_error THEN
			P_RESULT	:=	'Incorrect PAYMENT_CODE';
			FOR r_data IN  (SELECT	*
							FROM	sc_trans_pay_itf t
							WHERE	file_id			=	G_FILE_ID
							AND		ou_code			=	G_OU_CODE
							AND		payment_code	=	r_error.payment_code
							ORDER BY line_num)
			LOOP
				GR_LOG.PROCESS_DATE	:=	SYSDATE;
				GR_LOG.SEQ			:=	G_INF_SEQ;
				GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Validate Error : ' || P_RESULT;
				GR_LOG.LOG_DESC		:=	'Error : Payment Code=' || r_data.payment_code ||
										' not found (line# ' || r_data.line_num || ')';
				GR_LOG.INF_UPD_DATE	:=	SYSDATE;
				WRITE_LOG(GR_LOG);
				G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
			END LOOP;
			b_overall_error	:=	TRUE;	-- Error 1 means overall error
		END IF;
	END LOOP;

-- 21. ���ǹ�ͧ Payment record  ����Ǩ�ͺ��� BANK_CODE ����к��� ��ͧ�������ԧ� setup �ͧ�к� POS ����
	FOR r_error IN (SELECT	t.bank_code
					FROM	sc_trans_pay_itf t
					WHERE	file_id		=	G_FILE_ID
					AND		ou_code		=	G_OU_CODE
					AND		bank_code IS NOT NULL
					GROUP BY t.bank_code
					ORDER BY t.bank_code)
	LOOP
		b_error	:=	TRUE;
		FOR r_master IN (SELECT	bank_code,bank_name
						FROM	sa_bank
						WHERE	inactive IS NULL
						AND		bank_code = r_error.bank_code
						ORDER BY bank_code)
		LOOP
			b_error	:=	FALSE;
			EXIT;	-- Process once
		END LOOP;
		IF b_error THEN
			P_RESULT	:=	'Incorrect TRANS_CODE ';
			FOR r_data IN  (SELECT	*
							FROM	sc_trans_pay_itf t
							WHERE	file_id		=	G_FILE_ID
							AND		ou_code		=	G_OU_CODE
							AND		bank_code	=	r_error.bank_code
							ORDER BY line_num)
			LOOP
				GR_LOG.PROCESS_DATE	:=	SYSDATE;
				GR_LOG.SEQ			:=	G_INF_SEQ;
				GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Validate Error : ' || P_RESULT;
				GR_LOG.LOG_DESC		:=	'Error : Bank Code=' || r_data.bank_code ||
										' not found (line# ' || r_data.line_num || ')';
				GR_LOG.INF_UPD_DATE	:=	SYSDATE;
				WRITE_LOG(GR_LOG);
				G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
			END LOOP;
			b_overall_error	:=	TRUE;	-- Error 1 means overall error
		END IF;
	END LOOP;

-- 22. ���ǹ�ͧ Payment record  ����Ǩ�ͺ��� CREDIT_CARD_TYPE ����к��� ��ͧ�������ԧ� setup �ͧ�к� POS ����
	FOR r_error IN (SELECT	t.credit_card
					FROM	sc_trans_pay_itf t
					WHERE	file_id		=	G_FILE_ID
					AND		ou_code		=	G_OU_CODE
					AND		credit_card IS NOT NULL
					GROUP BY t.credit_card
					ORDER BY t.credit_card)
	LOOP
		b_error	:=	TRUE;
		FOR r_master IN (SELECT	credit_code,credit_name
						FROM	sa_credittype
						WHERE	inactive IS NULL
						AND		credit_code = r_error.credit_card
						ORDER BY credit_code)
		LOOP
			b_error	:=	FALSE;
			EXIT;	-- Process once
		END LOOP;
		IF b_error THEN
			P_RESULT	:=	'Incorrect CREDIT_CARD_TYPE';
			FOR r_data IN  (SELECT	*
							FROM	sc_trans_pay_itf t
							WHERE	file_id		=	G_FILE_ID
							AND		ou_code		=	G_OU_CODE
							AND		credit_card	=	r_error.credit_card
							ORDER BY line_num)
			LOOP
				GR_LOG.PROCESS_DATE	:=	SYSDATE;
				GR_LOG.SEQ			:=	G_INF_SEQ;
				GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Validate Error : ' || P_RESULT;
				GR_LOG.LOG_DESC		:=	'Error : Credit Card Type=' || r_data.credit_card ||
										' not found (line# ' || r_data.line_num || ')';
				GR_LOG.INF_UPD_DATE	:=	SYSDATE;
				WRITE_LOG(GR_LOG);
				G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
			END LOOP;
			b_overall_error	:=	TRUE;	-- Error 1 means overall error
		END IF;
	END LOOP;

----------------------------------
	IF b_overall_error THEN
		P_RESULT			:=	'Validation failed';
		GR_LOG.PROCESS_DATE	:=	SYSDATE;
		GR_LOG.SEQ			:=	G_INF_SEQ;
		GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Validate Error';
		GR_LOG.LOG_DESC		:=	'Auto revert status ' || TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS') ||
								P_RESULT;
		GR_LOG.INF_UPD_DATE	:=	SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;

		REMOVE_PROCESS('AUTO', 'BACK-JOB');
		G_RESULT_STATUS :=	'FAILED';
		
		mark_file_status('E');
	ELSE	-- Update total amount
		P_RESULT	:=	'COMPLETE';
	END IF;

END validate_n_dflt;
------------------------------------------
FUNCTION get_count
(i_filename	IN	VARCHAR2)
RETURN NUMBER
IS
	v_file		UTL_FILE.file_type;
	v_message	VARCHAR2(4000);
BEGIN
	v_file	:=	UTL_FILE.fopen	(location		=>	G_DIRECTORY
								,filename		=>	i_filename
								,open_mode		=>	'r');
	BEGIN
		UTL_FILE.get_line(file	=>	v_file, buffer	=>	v_message);
		v_message	:=	REPLACE(v_message,CHR(13));
		RETURN TO_NUMBER(LTRIM(RTRIM(v_message)));
	EXCEPTION
		WHEN OTHERS THEN
			RETURN 0;
	END;
EXCEPTION
	WHEN OTHERS THEN
		-->> Initial log
		GR_LOG.PROCESS_ID		:=	G_PROCESSID;
		GR_LOG.INF_UPD_BY		:=	'AUTO';
		GR_LOG.INF_UPD_PGM		:=	'BACK-JOB';
		
		-->> Lock Transaction Pending.
		GR_LOG.STEP				:=	G_FILE_NUM + .1;
		GR_LOG.STEP_DESC		:=	'Get count Filename=' || i_filename || ' directory=' || G_DIRECTORY;
		GR_LOG.PROCESS_FLAG		:=	'Error while getting total records';
		GR_LOG.INF_LOCK_FLAG	:=	'Unlock';

		GR_LOG.PROCESS_DATE	:=	SYSDATE;
		GR_LOG.SEQ			:=	G_INF_SEQ;
		GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Get Count';
		GR_LOG.LOG_DESC		:=	'Error while getting record count ' || SQLERRM;
		GR_LOG.INF_UPD_DATE	:=	SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		RETURN 0;
END get_count;
------------------------------------------
PROCEDURE start_process
(i_upd		IN	VARCHAR2
,i_pgm		IN	VARCHAR2
,P_RESULT	OUT	VARCHAR2)
IS
	v_status		VARCHAR2(4000);
BEGIN
	P_RESULT	:=	'COMPLETE';

	-->> Initial log
	GR_LOG.PROCESS_ID		:=	G_PROCESSID;
	GR_LOG.INF_UPD_BY		:=	i_upd;
	GR_LOG.INF_UPD_PGM		:=	i_pgm;
	
	-->> Lock Transaction Pending.
	GR_LOG.STEP				:=	G_FILE_NUM + .1;
	GR_LOG.STEP_DESC		:=	'Import file';
	GR_LOG.PROCESS_FLAG		:=	'Import Text to INBOUND_TEXT_FILE';
	GR_LOG.INF_LOCK_FLAG	:=	'Unlock';

	GR_LOG.PROCESS_DATE	:=	SYSDATE;
	GR_LOG.SEQ			:=	G_INF_SEQ;
	GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Import Start';
	GR_LOG.LOG_DESC		:=	'Import text file from DTAC One for OU:' || G_OU_CODE || ' Start at ' || TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS') ||
							' for file date ' || to_char(G_START_DATE, 'DD/MM/RRRR');
	GR_LOG.INF_UPD_DATE	:=	SYSDATE;
	WRITE_LOG(GR_LOG);
	G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;

	BEGIN
		GLB_TEXT_INBOUND_UTIL.read_file_utf8
			(i_file_loc			=>	G_DIRECTORY
			,i_file_name		=>	G_FILENAME
			,i_cre_by			=>	i_upd
			,i_file_inf_code	=>	G_PROCESSID
			,i_program_name		=>	G_PROGRAM_NAME
			,o_file_id			=>	G_FILE_ID
			,o_status			=>	v_status);
		IF v_status = glb_text_output_util.NO_ERROR THEN
			-- No Error
			NULL;
		ELSE	-- Error
			P_RESULT			:=	'Import Text File Error';
			GR_LOG.PROCESS_DATE	:=	SYSDATE;
			GR_LOG.SEQ			:=	G_INF_SEQ;
			GR_LOG.LOG_CODE		:=	SUBSTR(G_PROCESSID || ' Import Error : ' || P_RESULT || ' : Status = ' || v_status,1,100);
			GR_LOG.LOG_DESC		:=	'Import text file from DTAC One for OU:' || G_OU_CODE || ' Error at ' || TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS') ||
									' for file date ' || to_char(G_START_DATE, 'DD/MM/RRRR');
			GR_LOG.INF_UPD_DATE	:=	SYSDATE;
			WRITE_LOG(GR_LOG);
			G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
			mark_file_status('E');
			RETURN;	-- Return to caller program upon error
		END IF;

		GR_LOG.PROCESS_DATE	:=	SYSDATE;
		GR_LOG.SEQ			:=	G_INF_SEQ;
		GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Import Finish : File ID = ' || G_FILE_ID;
		GR_LOG.LOG_DESC		:=	'Import text file from DTAC One for OU:' || G_OU_CODE || ' Finish at ' || TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS') ||
								' for file date ' || to_char(G_START_DATE, 'DD/MM/RRRR');
		GR_LOG.INF_UPD_DATE	:=	SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;

	EXCEPTION
		WHEN OTHERS THEN
			P_RESULT			:=	'Import Text File Error';
			GR_LOG.PROCESS_DATE	:=	SYSDATE;
			GR_LOG.SEQ			:=	G_INF_SEQ;
			GR_LOG.LOG_CODE		:=	SUBSTR(G_PROCESSID || ' Import Error - ' || SQLERRM, 1, 100);
			GR_LOG.LOG_DESC		:=	'Import text file from DTAC One for OU:' || G_OU_CODE || ' Error at ' || TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS') ||
									' for file date ' || to_char(G_START_DATE, 'DD/MM/RRRR');
			GR_LOG.INF_UPD_DATE	:=	SYSDATE;
			WRITE_LOG(GR_LOG);
			G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
			mark_file_status('E');
			RETURN;	-- Return to caller program upon error
	END;
	COMMIT;
END start_process;
------------------------------------------
PROCEDURE ending_process
(i_upd	IN	VARCHAR2
,i_pgm	IN	VARCHAR2)
IS
BEGIN
	-->> Initial log
	GR_LOG.PROCESS_ID		:=	G_PROCESSID;
	GR_LOG.INF_UPD_BY		:=	i_upd;
	GR_LOG.INF_UPD_PGM		:=	i_pgm;
	
	-->> Unlock Transaction Pending.
	GR_LOG.STEP				:=	G_FILE_NUM + .3;
	GR_LOG.STEP_DESC		:=	'Unlock row';
	GR_LOG.PROCESS_FLAG		:=	'Complete Interface';
	GR_LOG.INF_LOCK_FLAG	:=	'Unlock';

	mark_file_status('Y');	-- Interfaced

	GR_LOG.PROCESS_DATE	:=	SYSDATE;
	GR_LOG.SEQ			:=	G_INF_SEQ;
	GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Complete';
	GR_LOG.LOG_DESC		:=	'Unlock (sc_transaction OU:' || G_OU_CODE || ') Complete at ' || TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS') ||
							' for creation date' || to_char(G_START_DATE, 'DD/MM/RRRR');
	GR_LOG.INF_UPD_DATE	:=	SYSDATE;
	WRITE_LOG(GR_LOG);
	G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;

END ending_process;
------------------------------------------
PROCEDURE remove_process
(i_upd		IN	VARCHAR2
,i_pgm		IN	VARCHAR2)
IS
BEGIN
	-->> Initial log
	GR_LOG.PROCESS_ID		:=	G_PROCESSID;
	GR_LOG.INF_UPD_BY		:=	i_upd;
	GR_LOG.INF_UPD_PGM		:=	i_pgm;
	
	-->> Unlock Transaction Pending.
	GR_LOG.STEP				:=	G_FILE_NUM + .9;
	GR_LOG.STEP_DESC		:=	'Remove process';
	GR_LOG.PROCESS_FLAG		:=	'Warning : Map Error';
	GR_LOG.INF_LOCK_FLAG	:=	'Unlock';

	mark_file_status('E');	-- Error

	GR_LOG.PROCESS_DATE	:=	SYSDATE;
	GR_LOG.SEQ			:=	G_INF_SEQ;
	GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Warning';
	GR_LOG.LOG_DESC		:=	'Unlock (sc_transaction OU:' || G_OU_CODE || ') Complete at ' || TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS') ||
							' for creation date' || to_char(G_START_DATE, 'DD/MM/RRRR');
	GR_LOG.INF_UPD_DATE	:=	SYSDATE;
	WRITE_LOG(GR_LOG);
	G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
/*		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				GR_LOG.PROCESS_DATE	:=	SYSDATE;
				GR_LOG.SEQ			:=	G_INF_SEQ;
				GR_LOG.LOG_CODE		:=	G_PROCESSID || ' SQL%NOTFOUND';
				GR_LOG.LOG_DESC		:=	'Unlock (sc_transaction OU:' || ou.ou_code || ') Transaction not found at ' || TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS') ||
										' for creation date' || to_char(G_START_DATE, 'DD/MM/RRRR');
				GR_LOG.INF_UPD_DATE	:=	SYSDATE;
				WRITE_LOG(GR_LOG);
				G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		END;*/
END remove_process;
------------------------------------------
PROCEDURE clear_log IS
BEGIN
	DELETE	inbound_text_file_error_log
	WHERE	inf_upd_date < TRUNC(SYSDATE) - G_LOG_BACK_DAYS;

	DELETE	sc_ifrs_interface
	WHERE	process_date < TRUNC(SYSDATE) - G_LOG_BACK_DAYS;
END clear_log;
------------------------------------------
PROCEDURE write_log
(p_write	inbound_text_file_error_log%ROWTYPE) IS
BEGIN
	INSERT INTO inbound_text_file_error_log
		(program_name
		,process_id
		,process_date
		,step
		,step_desc
		,seq
		,log_code
		,log_desc
		,process_flag
		,inf_lock_flag
		,inf_upd_by
		,inf_upd_date
		,inf_upd_pgm)
	VALUES
		(G_PROGRAM_NAME
		,p_write.process_id
		,p_write.process_date
		,p_write.step
		,p_write.step_desc
		,p_write.seq
		,p_write.log_code
		,p_write.log_desc
		,p_write.process_flag
		,p_write.inf_lock_flag
		,p_write.inf_upd_by
		,p_write.inf_upd_date
		,p_write.inf_upd_pgm);
END write_log;
------------------------------------------
PROCEDURE prepare_data
(i_upd		IN	VARCHAR2
,i_pgm		IN	VARCHAR2
,P_RESULT	OUT	VARCHAR2)
IS
	CURSOR c_data IS
		SELECT	*
		FROM	inbound_text_file_line t
		WHERE	t.file_id	=	G_FILE_ID
		ORDER BY t.line_num;
	t_field_value		GLB_TEXT_INBOUND_UTIL.varchar_tabtyp;
	r_transaction_itf	SC_TRANSACTION_ITF%ROWTYPE	:=	NULL;
	r_trans_dtl_itf		SC_TRANS_DTL_ITF%ROWTYPE	:=	NULL;
	r_trans_pay_itf		SC_TRANS_PAY_ITF%ROWTYPE	:=	NULL;
BEGIN
	G_CNT_DOC_IMPORT		:=	0;
	G_TOTAL_AMOUNT			:=	0;
	
	-->> Initial log
	GR_LOG.PROCESS_ID		:=	G_PROCESSID;
	GR_LOG.INF_UPD_BY		:=	i_upd;
	GR_LOG.INF_UPD_PGM		:=	i_pgm;

	-->> Get data to interface
	GR_LOG.STEP				:=	G_FILE_NUM + .2;
	GR_LOG.STEP_DESC		:=	'Split Data';
	GR_LOG.PROCESS_FLAG		:=	'Prepare Data';
	GR_LOG.INF_LOCK_FLAG	:=	'Unlock';

	FOR r_data IN c_data
	LOOP
		GLB_TEXT_INBOUND_UTIL.cut_fields
			(i_text			=>	r_data.text
			,i_separator	=>	G_FIELD_SEPARATOR
			,ot_field_value	=>	t_field_value
			,o_status		=>	P_RESULT);
		IF P_RESULT = glb_text_output_util.NO_ERROR THEN
			-- No Error
			NULL;
		ELSE	-- Error
			GR_LOG.PROCESS_DATE	:=	SYSDATE;
			GR_LOG.SEQ			:=	G_INF_SEQ;
			GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Split Data Error : Status = ' || P_RESULT;
			GR_LOG.LOG_DESC		:=	'Split data in text file from DTAC One for OU:' || G_OU_CODE || ' Error in line# ' || r_data.line_num || ' at ' || TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS') ||
									' for file date ' || to_char(G_START_DATE, 'DD/MM/RRRR');
			GR_LOG.INF_UPD_DATE	:=	SYSDATE;
			WRITE_LOG(GR_LOG);
			G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;

			G_CUT_FIELDS_ERROR	:=	G_CUT_FIELDS_ERROR + 1;	-- Count cut fields error
			
			mark_file_status('E');
		END IF;
		
		IF t_field_value(1)	=	'H' THEN		-- Header
			G_CNT_DOC_IMPORT					:=	G_CNT_DOC_IMPORT + 1;
			r_transaction_itf					:=	NULL;	-- Clear value
			r_transaction_itf.file_id			:=	r_data.file_id;		-- Refer to file_id
			r_transaction_itf.line_num			:=	r_data.line_num;	-- Refer to line_num in the file
			r_transaction_itf.ou_code			:=	t_field_value(2);
			r_transaction_itf.subinv_code		:=	t_field_value(3);
			r_transaction_itf.source_module		:=	C_SOURCE_MODULE || '-' || G_SOURCE_SYSTEM;
			r_transaction_itf.source_reference	:=	t_field_value(4);
			r_transaction_itf.doc_type			:=	t_field_value(5);
-- Edit by TM 06-Aug-2018 : Change document date to sysdate
--			r_transaction_itf.doc_date			:=	TO_DATE(t_field_value(6),'YYYY-MM-DD HH24:MI:SS');
			r_transaction_itf.doc_date			:=	SYSDATE;
			r_transaction_itf.remark			:=	'Original Document Date = ' || t_field_value(6);
-- End Edit by TM 06-Aug-2018 : Change document date to sysdate
			r_transaction_itf.cust_code			:=	t_field_value(7);
			r_transaction_itf.cust_name			:=	t_field_value(8);
			r_transaction_itf.cust_add			:=	t_field_value(9);
			r_transaction_itf.cust_amphur		:=	t_field_value(10);
			r_transaction_itf.cust_province		:=	t_field_value(11);
			r_transaction_itf.cust_postal		:=	t_field_value(12);
			r_transaction_itf.cust_tel			:=	t_field_value(13);
			r_transaction_itf.tax_id			:=	t_field_value(14);
			r_transaction_itf.branch_code		:=	t_field_value(15);
			r_transaction_itf.total_amt			:=	t_field_value(16);
			r_transaction_itf.vat_code			:=	t_field_value(17);
			r_transaction_itf.vat_amt			:=	t_field_value(18);
			r_transaction_itf.salesperson		:=	t_field_value(19);
			IF r_transaction_itf.ou_code = G_OU_CODE THEN
				-- No Error
				P_RESULT	:=	insert_itf(r_transaction_itf, i_upd);
				IF P_RESULT  = glb_text_output_util.NO_ERROR THEN
					-- No Error
					NULL;
				ELSE	-- Error
					GR_LOG.PROCESS_DATE	:=	SYSDATE;
					GR_LOG.SEQ			:=	G_INF_SEQ;
					GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Split Data Error : Insert Interface Error = ' || P_RESULT || ' Record Type = ' || t_field_value(1);
					GR_LOG.LOG_DESC		:=	'Split data in text file from DTAC One for OU:' || G_OU_CODE || ' Error in line# ' || r_data.line_num || ' at ' || TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS') ||
											' for file date ' || to_char(G_START_DATE, 'DD/MM/RRRR');
					GR_LOG.INF_UPD_DATE	:=	SYSDATE;
					WRITE_LOG(GR_LOG);
					G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;

					G_CNT_DOC_ERROR	:=	G_CNT_DOC_ERROR + 1;	-- Count Doc Header error
				END IF;
			ELSE	-- Error
				GR_LOG.PROCESS_DATE	:=	SYSDATE;
				GR_LOG.SEQ			:=	G_INF_SEQ;
				GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Split Data Error : OU Code in text file <> OU Code in filename, Record Type = ' || t_field_value(1);
				GR_LOG.LOG_DESC		:=	'Split data in text file from DTAC One for OU:' || G_OU_CODE || ', OU Code in text file:' || r_transaction_itf.ou_code || ', Error in line# ' || r_data.line_num || ' at ' || TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS') ||
										' for file date ' || to_char(G_START_DATE, 'DD/MM/RRRR');
				GR_LOG.INF_UPD_DATE	:=	SYSDATE;
				WRITE_LOG(GR_LOG);
				G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;

				G_CNT_DOC_ERROR	:=	G_CNT_DOC_ERROR + 1;	-- Count Doc Header error
			END IF;
		ELSIF t_field_value(1)	=	'D' THEN	-- Detail
			r_trans_dtl_itf						:=	NULL;	-- Clear value
			r_trans_dtl_itf.file_id				:=	r_data.file_id;		-- Refer to file_id
			r_trans_dtl_itf.line_num			:=	r_data.line_num;	-- Refer to line_num in the file
			r_trans_dtl_itf.ou_code				:=	r_transaction_itf.ou_code;
			r_trans_dtl_itf.subinv_code			:=	r_transaction_itf.subinv_code;
			r_trans_dtl_itf.doc_type			:=	r_transaction_itf.doc_type;
			r_trans_dtl_itf.hdr_source_ref		:=	r_transaction_itf.source_reference;
			r_trans_dtl_itf.source_module		:=	C_SOURCE_MODULE || '-' || G_SOURCE_SYSTEM;
			r_trans_dtl_itf.source_reference	:=	t_field_value(2);
			r_trans_dtl_itf.seq_no				:=	t_field_value(3);
			r_trans_dtl_itf.trans_code			:=	t_field_value(4);
			r_trans_dtl_itf.amount				:=	t_field_value(5);
			P_RESULT	:=	insert_itf(r_trans_dtl_itf, i_upd);
			IF P_RESULT  = glb_text_output_util.NO_ERROR THEN
				-- No Error
				NULL;
			ELSE	-- Error
				GR_LOG.PROCESS_DATE	:=	SYSDATE;
				GR_LOG.SEQ			:=	G_INF_SEQ;
				GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Split Data Error : Insert Interface Error = ' || P_RESULT || ' Record Type = ' || t_field_value(1);
				GR_LOG.LOG_DESC		:=	'Split data in text file from DTAC One for OU:' || G_OU_CODE || ' Error in line# ' || r_data.line_num || ' at ' || TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS') ||
										' for file date ' || to_char(G_START_DATE, 'DD/MM/RRRR');
				GR_LOG.INF_UPD_DATE	:=	SYSDATE;
				WRITE_LOG(GR_LOG);
				G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;

				G_CNT_DTL_ERROR	:=	G_CNT_DTL_ERROR + 1;	-- Count Doc Detail error
			END IF;
		ELSIF t_field_value(1)	=	'P' THEN	-- Payment
			r_trans_pay_itf						:=	NULL;	-- Clear value
			r_trans_pay_itf.file_id				:=	r_data.file_id;		-- Refer to file_id
			r_trans_pay_itf.line_num			:=	r_data.line_num;	-- Refer to line_num in the file
			r_trans_pay_itf.ou_code				:=	r_transaction_itf.ou_code;
			r_trans_pay_itf.subinv_code			:=	r_transaction_itf.subinv_code;
			r_trans_pay_itf.doc_type			:=	r_transaction_itf.doc_type;
			r_trans_pay_itf.hdr_source_ref		:=	r_transaction_itf.source_reference;
			r_trans_pay_itf.source_module		:=	C_SOURCE_MODULE || '-' || G_SOURCE_SYSTEM;
			r_trans_pay_itf.source_reference	:=	t_field_value(2);
			r_trans_pay_itf.payment_code		:=	t_field_value(4);
			r_trans_pay_itf.ref_no				:=	t_field_value(5);
			r_trans_pay_itf.bank_code			:=	t_field_value(6);
			r_trans_pay_itf.credit_card			:=	t_field_value(7);
			r_trans_pay_itf.pay_amt				:=	t_field_value(8);
			r_trans_pay_itf.approve_no			:=	t_field_value(9);
			P_RESULT	:=	insert_itf(r_trans_pay_itf, i_upd);
			IF P_RESULT  = glb_text_output_util.NO_ERROR THEN
				-- No Error
				NULL;
			ELSE	-- Error
				GR_LOG.PROCESS_DATE	:=	SYSDATE;
				GR_LOG.SEQ			:=	G_INF_SEQ;
				GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Split Data Error : Insert Interface Error = ' || P_RESULT || ' Record Type = ' || t_field_value(1);
				GR_LOG.LOG_DESC		:=	'Split data in text file from DTAC One for OU:' || G_OU_CODE || ' Error in line# ' || r_data.line_num || ' at ' || TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS') ||
										' for file date ' || to_char(G_START_DATE, 'DD/MM/RRRR');
				GR_LOG.INF_UPD_DATE	:=	SYSDATE;
				WRITE_LOG(GR_LOG);
				G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;

				G_CNT_PAY_ERROR	:=	G_CNT_PAY_ERROR + 1;	-- Count Doc Payment error
			END IF;
		ELSIF t_field_value.COUNT > 0 THEN	-- Error
			GR_LOG.PROCESS_DATE	:=	SYSDATE;
			GR_LOG.SEQ			:=	G_INF_SEQ;
			GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Split Data Error : Incorrect Record Type = ' || t_field_value(1);
			GR_LOG.LOG_DESC		:=	'Split data in text file from DTAC One for OU:' || G_OU_CODE || ' Error in line# ' || r_data.line_num || ' at ' || TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS') ||
									' for file date ' || to_char(G_START_DATE, 'DD/MM/RRRR');
			GR_LOG.INF_UPD_DATE	:=	SYSDATE;
			WRITE_LOG(GR_LOG);
			G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;

			G_CNT_RECTYP_ERROR	:=	G_CNT_RECTYP_ERROR + 1;	-- Count Record Type error
		END IF;

	END LOOP;
	
	IF NVL(G_CNT_DOC_IMPORT, 0) = 0 THEN
		GR_LOG.PROCESS_DATE	:=	SYSDATE;
		GR_LOG.SEQ			:=	G_INF_SEQ;
		GR_LOG.LOG_CODE		:=	G_PROCESSID || ' SQL%NOTFOUND';
		GR_LOG.LOG_DESC		:=	'Transaction not found';
		GR_LOG.INF_UPD_DATE	:=	SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		
		P_RESULT			:=	'No Document imported';
		mark_file_status('E');
	ELSIF NVL(G_CUT_FIELDS_ERROR, 0) <> 0 THEN
		GR_LOG.PROCESS_DATE	:=	SYSDATE;
		GR_LOG.SEQ			:=	G_INF_SEQ;
		GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Cut Fields Error';
		GR_LOG.LOG_DESC		:=	'There are ' || G_CUT_FIELDS_ERROR || ' cut field error(s).';
		GR_LOG.INF_UPD_DATE	:=	SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		
		P_RESULT			:=	'Cut Field Error';
		mark_file_status('E');
	ELSIF NVL(G_CNT_DOC_ERROR, 0) <> 0 THEN
		GR_LOG.PROCESS_DATE	:=	SYSDATE;
		GR_LOG.SEQ			:=	G_INF_SEQ;
		GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Document Error';
		GR_LOG.LOG_DESC		:=	'There are ' || G_CNT_DOC_ERROR || ' document(s) error.';
		GR_LOG.INF_UPD_DATE	:=	SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		
		P_RESULT			:=	'Document Error';
		mark_file_status('E');
	ELSIF NVL(G_CNT_DTL_ERROR, 0) <> 0 THEN
		GR_LOG.PROCESS_DATE	:=	SYSDATE;
		GR_LOG.SEQ			:=	G_INF_SEQ;
		GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Detail Record Error';
		GR_LOG.LOG_DESC		:=	'There are ' || G_CNT_DTL_ERROR || ' detail record(s) error.';
		GR_LOG.INF_UPD_DATE	:=	SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		
		P_RESULT			:=	'Detail Record Error';
		mark_file_status('E');
	ELSIF NVL(G_CNT_PAY_ERROR, 0) <> 0 THEN
		GR_LOG.PROCESS_DATE	:=	SYSDATE;
		GR_LOG.SEQ			:=	G_INF_SEQ;
		GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Payment Record Error';
		GR_LOG.LOG_DESC		:=	'There are ' || G_CNT_PAY_ERROR || ' payment record(s) error.';
		GR_LOG.INF_UPD_DATE	:=	SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		
		P_RESULT			:=	'Payment Record Error';
		mark_file_status('E');
	ELSIF NVL(G_CNT_RECTYP_ERROR, 0) <> 0 THEN
		GR_LOG.PROCESS_DATE	:=	SYSDATE;
		GR_LOG.SEQ			:=	G_INF_SEQ;
		GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Record Type Error';
		GR_LOG.LOG_DESC		:=	'There are ' || G_CNT_RECTYP_ERROR || ' record type error(s).';
		GR_LOG.INF_UPD_DATE	:=	SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		
		P_RESULT			:=	'Record Type Error';
		mark_file_status('E');
	ELSE
		P_RESULT			:=	'COMPLETE';
	END IF;
	COMMIT;
EXCEPTION
	WHEN OTHERS THEN
		-->> Initial log
		GR_LOG.PROCESS_ID		:=	G_PROCESSID;
		GR_LOG.INF_UPD_BY		:=	'AUTO';
		GR_LOG.INF_UPD_PGM		:=	'BACK-JOB';
		
		-->> Lock Transaction Pending.
		GR_LOG.STEP				:=	G_FILE_NUM + .1;
		GR_LOG.STEP_DESC		:=	'Prepare data';
		GR_LOG.PROCESS_FLAG		:=	'Error while preparing';
		GR_LOG.INF_LOCK_FLAG	:=	'Unlock';

		GR_LOG.PROCESS_DATE	:=	SYSDATE;
		GR_LOG.SEQ			:=	G_INF_SEQ;
		GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Prepare Data';
		GR_LOG.LOG_DESC		:=	'Error while prepare data ' || SQLERRM;
		GR_LOG.INF_UPD_DATE	:=	SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;

		P_RESULT			:=	'Error while preparing data';
END prepare_data;
------------------------------------------
PROCEDURE interface_data
(i_upd			IN		VARCHAR2
,i_pgm			IN		VARCHAR2
,io_status		IN OUT	VARCHAR2)
IS
	v_prefix	pb_parameter_setup.parameter_value%TYPE
					:=	PB_PARAMETER_SETUP_PKG.GET_PARAMETER_VALUE('DTSCRT02','IV_PREFIX');

	CURSOR c_hdr IS
		SELECT	t.*
		FROM	sc_transaction_itf t
		WHERE	t.file_id	=	G_FILE_ID
		AND		t.ou_code	=	G_OU_CODE
		FOR UPDATE NOWAIT
		ORDER BY line_num;
BEGIN
	G_INF_SEQ				:=	1;
	G_CNT_DOC_IMPORT		:=	0;
	G_TOTAL_AMOUNT			:=	0;
	
	io_status				:=	glb_text_output_util.NO_ERROR;

	-->> Initial log
	GR_LOG.PROCESS_ID		:=	G_PROCESSID;
	GR_LOG.INF_UPD_BY		:=	i_upd;
	GR_LOG.INF_UPD_PGM		:=	i_pgm;

	-->> Lock Transaction Pending.
	GR_LOG.STEP				:=	G_FILE_NUM + .5;
	GR_LOG.STEP_DESC		:=	'Generate Interface file';
	GR_LOG.PROCESS_FLAG		:=	'Sent Interface';
	GR_LOG.INF_LOCK_FLAG	:=	'Unlock';

	SAVEPOINT b4interface;

	FOR r_hdr IN c_hdr LOOP
		r_hdr.doc_no	:=	PB_RUNNING_PKG.GET_RUNNING
								(r_hdr.ou_code
								,v_prefix
								,r_hdr.subinv_code
								,r_hdr.doc_date
								,i_upd, i_pgm);
		r_hdr.total_amt	:=	r_hdr.total_amt - NVL(r_hdr.vat_amt,0);

		UPDATE	sc_transaction_itf
		SET		doc_no		=	r_hdr.doc_no
				,total_amt	=	r_hdr.total_amt	-- Total Amt is exclude VAT
		WHERE CURRENT OF c_hdr;
		
		UPDATE	sc_trans_dtl_itf
		SET		doc_no			=	r_hdr.doc_no
		WHERE	file_id			=	G_FILE_ID
		AND		ou_code			=	G_OU_CODE
		AND		hdr_source_ref	=	r_hdr.source_reference;
		
		UPDATE	sc_trans_pay_itf
		SET		doc_no			=	r_hdr.doc_no
		WHERE	file_id			=	G_FILE_ID
		AND		ou_code			=	G_OU_CODE
		AND		hdr_source_ref	=	r_hdr.source_reference;

		BEGIN
			INSERT INTO sc_transaction
				(ou_code					,subinv_code					,doc_no
				,doc_type					,doc_date						,cat_code
				,cust_code					,cust_name						,cust_add
				,cust_amphur				,cust_province					,cust_postal
				,cust_tel					,gross_amt						,total_amt
				,net_amt					,vat_code						,vat_rate
				,vat_amt					,salesperson					,team_header
				,shop_mgr					,area_mgr						,remark
				,cn_flag					,interface_statsus				,change_amt
				,tax_id						,branch_code					,ac_type
				,cre_by						,cre_date						,status
				,ref_ccb_cust_flag			,source_module					,source_reference
				,wht_code	,wht_rate	,wht_amt	,upd_pgm	,upd_by		,upd_date)
			VALUES
				(r_hdr.ou_code				,r_hdr.subinv_code				,r_hdr.doc_no
				,r_hdr.doc_type				,r_hdr.doc_date					,r_hdr.cat_code
				,r_hdr.cust_code			,r_hdr.cust_name				,r_hdr.cust_add
				,r_hdr.cust_amphur			,r_hdr.cust_province			,r_hdr.cust_postal
				,r_hdr.cust_tel				,r_hdr.gross_amt				,r_hdr.total_amt
				,r_hdr.net_amt				,r_hdr.vat_code					,r_hdr.vat_rate
				,r_hdr.vat_amt				,r_hdr.salesperson				,r_hdr.team_header
				,r_hdr.shop_mgr				,r_hdr.area_mgr					,r_hdr.remark
				,r_hdr.cn_flag				,r_hdr.interface_status			,r_hdr.change_amt
				,r_hdr.tax_id				,r_hdr.branch_code				,r_hdr.ac_type
				,r_hdr.cre_by				,r_hdr.cre_date					,r_hdr.status
				,r_hdr.ref_ccb_cust_flag	,r_hdr.source_module			,r_hdr.source_reference
				,'00'		,0			,0			,i_pgm		,i_upd		,SYSDATE);

			INSERT INTO sc_trans_dtl
				(ou_code			,subinv_code		,doc_no				,doc_type			,seq_no
				,trans_code			,qty				,unitprice			,amount				,std_price
				,inv_seq			,cn_flag			,net_amt			,gross_amt			,cre_by
				,cre_date			,source_module		,source_reference
				,upd_pgm	,upd_by		,upd_date)
			SELECT	ou_code			,subinv_code		,doc_no				,doc_type			,seq_no
					,trans_code		,qty				,unitprice			,amount				,std_price
					,inv_seq		,cn_flag			,net_amt			,gross_amt			,cre_by
					,cre_date		,source_module		,source_reference
					,i_pgm		,i_upd		,SYSDATE
			FROM	sc_trans_dtl_itf
			WHERE	file_id			=	G_FILE_ID
			AND		ou_code			=	G_OU_CODE
			AND		hdr_source_ref	=	r_hdr.source_reference;

			INSERT INTO sc_trans_pay
				(ou_code			,subinv_code		,doc_type			,doc_no				,payment_code
				,ref_no				,bank_code			,credit_card		,cr_bank_rate		,pay_amt
				,approve_no			,pay_net			,cre_by				,cre_date			,source_module
				,source_reference
				,upd_pgm	,upd_by		,upd_date)
			SELECT	ou_code			,subinv_code		,doc_type			,doc_no				,payment_code
					,ref_no			,bank_code			,credit_card		,cr_bank_rate		,pay_amt
					,approve_no		,pay_net			,cre_by				,cre_date			,source_module
					,source_reference
					,i_pgm		,i_upd		,SYSDATE
			FROM	sc_trans_pay_itf
			WHERE	file_id			=	G_FILE_ID
			AND		ou_code			=	G_OU_CODE
			AND		hdr_source_ref	=	r_hdr.source_reference;

			G_CNT_DOC_IMPORT	:=	G_CNT_DOC_IMPORT + 1;
			G_TOTAL_AMOUNT		:=	G_TOTAL_AMOUNT + NVL(r_hdr.gross_amt,0);

			GR_LOG.PROCESS_DATE	:=	SYSDATE;
			GR_LOG.SEQ			:=	G_INF_SEQ;
			GR_LOG.LOG_CODE		:=	G_PROCESSID || ' Complete';
			GR_LOG.LOG_DESC		:=	SUBSTR('Source Document No : ' || r_hdr.source_reference ||
									' Document No : ' || r_hdr.doc_no ||
									' Document Type : ' || r_hdr.doc_type ||
									' Doc Date : ' || TO_CHAR(r_hdr.doc_date,'DD/MM/RRRR HH24:MI:SS') ||
									' Customer No : ' || r_hdr.cust_code ||
									' Gross Amt : ' || r_hdr.gross_amt ||
									' Total Amt : ' || r_hdr.total_amt ||
									' Net Amt : ' || r_hdr.net_amt, 1, 400);
			GR_LOG.INF_UPD_DATE	:=	SYSDATE;
			WRITE_LOG(GR_LOG);
			G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		EXCEPTION
			WHEN OTHERS THEN
				io_status			:=	glb_text_output_util.C_ERROR || SQLERRM;
				G_CNT_DOC_ERROR		:=	NVL(G_CNT_DOC_ERROR, 0) + 1;
				GR_LOG.PROCESS_DATE	:=	SYSDATE;
				GR_LOG.SEQ			:=	G_INF_SEQ;
				GR_LOG.LOG_CODE		:=	SUBSTR(G_PROCESSID || SQLERRM,1,100);
				GR_LOG.LOG_DESC		:=	SUBSTR('Source Document No : ' || r_hdr.source_reference ||
										' Document No : ' || r_hdr.doc_no ||
										' Document Type : ' || r_hdr.doc_type ||
										' Doc Date : ' || TO_CHAR(r_hdr.doc_date,'DD/MM/RRRR HH24:MI:SS') ||
										' Customer No : ' || r_hdr.cust_code ||
										' Gross Amt : ' || r_hdr.gross_amt ||
										' Total Amt : ' || r_hdr.total_amt ||
										' Net Amt : ' || r_hdr.net_amt, 1, 400);
				GR_LOG.INF_UPD_DATE	:=	SYSDATE;
				WRITE_LOG(GR_LOG);
				G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		END;
	END LOOP;
	
	---------------------------------------
	IF G_CNT_DOC_ERROR = 0 THEN
		GR_LOG.PROCESS_DATE	:=	SYSDATE;
		GR_LOG.SEQ			:=	G_INF_SEQ;
		GR_LOG.LOG_CODE		:=	SUBSTR(G_PROCESSID || ' Interface Complete for OU : ' || G_OU_CODE,1,100);
		GR_LOG.LOG_DESC		:=	'Total = ' || G_CNT_DOC_IMPORT || 'documents, with total gross amt = ' || TO_CHAR(G_TOTAL_AMOUNT,'fm999,999,999,999,999,990.00');
		GR_LOG.INF_UPD_DATE	:=	SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		COMMIT;
	ELSE
		ROLLBACK TO b4interface;
		GR_LOG.PROCESS_DATE	:=	SYSDATE;
		GR_LOG.SEQ			:=	G_INF_SEQ;
		GR_LOG.LOG_CODE		:=	SUBSTR(G_PROCESSID || ' Interface Error for OU : ' || G_OU_CODE,1,100);
		GR_LOG.LOG_DESC		:=	'Total Error = ' || G_CNT_DOC_ERROR || 'documents, Total passed = ' || G_CNT_DOC_IMPORT ||
								' (all records are rollback, not interface for the whole file)';
		GR_LOG.INF_UPD_DATE	:=	SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ	:=	NVL(G_INF_SEQ, 0) + 1;
		io_status			:=	glb_text_output_util.C_ERROR || 'Interface Error for OU : ' || G_OU_CODE;

		mark_file_status('E');
	END IF;
END interface_data;

PROCEDURE send_email
(i_smtp_server	IN		VARCHAR2	:=	'mail-gw.tac.co.th'
,i_smtp_port	IN		VARCHAR2	:=	25
,i_from_email	IN		VARCHAR2	:=	'ERPOperationSupport@dtac.co.th'
,i_to_email		IN		VARCHAR2	-- Separate by ,
,i_cc_email		IN		VARCHAR2	:=	NULL
,i_bcc_email	IN		VARCHAR2	:=	NULL
,i_subject		IN		VARCHAR2	:=	'[POS] DTAC One interface to POS - ' || G_RESULT_STATUS
,i_priority		IN		NUMBER		:=	3   -- Normal priority
,io_status		IN OUT	VARCHAR2
)
IS
	NEW_LINE_IN_MSG		CONSTANT VARCHAR2(10)	:=	'<BR>';
	v_blob				BLOB;
	v_msg_template		VARCHAR2(32000) :=
		'Dear All,' || NEW_LINE_IN_MSG ||
		NEW_LINE_IN_MSG ||
		'Please be informed that DTAC One interface to POS is executed with ' || G_RESULT_STATUS || '.' || 
		CASE WHEN G_RESULT_STATUS='SUCCESS' THEN NULL ELSE '  See more detail in file enclosed.' END || NEW_LINE_IN_MSG ||
		NEW_LINE_IN_MSG ||
		'Job No. : ' || G_N_NO || NEW_LINE_IN_MSG ||
		'Job Status : Complete' || CASE WHEN G_RESULT_STATUS='SUCCESS' THEN NULL ELSE ' with ' || G_RESULT_STATUS END || NEW_LINE_IN_MSG ||
		'Job Broken : ' || G_JOB_BROKEN || NEW_LINE_IN_MSG ||
		NEW_LINE_IN_MSG ||
		'Thank you & Regards,' || NEW_LINE_IN_MSG ||
		'POS auto job' || NEW_LINE_IN_MSG;
	v_msg				v_msg_template%TYPE;
	b_has_attachment	BOOLEAN :=	G_RESULT_STATUS <> 'SUCCESS';

-- Start Temporary for checking email
	t_text			glb_text_output_util.varchar_tabtyp;
	t_format		glb_text_output_util.number_tabtyp;
	v_filename		VARCHAR2(4000)	:=	SUBSTR(G_FILENAME,1,LENGTH(G_FILENAME)-4) || '.CSV';
-- End Temporary for checking email

BEGIN
	io_status	:=	glb_text_output_util.NO_ERROR;

	v_msg		:=	v_msg_template;
-- Error => write error HTML file
	IF G_RESULT_STATUS <> 'SUCCESS' THEN
		FOR i IN 1..7
		LOOP
			t_format(i) :=	-1;
		END LOOP;

		glb_text_output_util.set_format
			(it_col_len		=>	t_format
			,o_status		=>	io_status
			,i_clear_data	=>	'Y');
		IF io_status <> glb_text_output_util.NO_ERROR THEN
			RAISE e_abnormal_end;
		END IF;

		-- Headings
		t_text(1)	:=	'Step';
		t_text(2)	:=	'Log Code';
		t_text(3)	:=	'Log Description';
		t_text(4)	:=	'Status';
		t_text(5)	:=	'Process Flag';
		t_text(6)	:=	'INF Lock Flag';
		t_text(7)	:=	'INF Update Date';
		
		glb_text_output_util.add_txt
			(it_message			=>	t_text
			/*,i_delimiter		=>	glb_text_output_util.HTML_TBL_HDR_DELIM_B   --'<TH>'
			,i_delimiter_end	=>	glb_text_output_util.HTML_TBL_HDR_DELIM_E   --'</TH>' || glb_bulk_run_report_api.NEW_LINE
			,i_separator		=>	glb_text_output_util.HTML_TBL_SEPARATOR	 --NULL
			,i_line_prefix		=>	glb_text_output_util.HTML_TBL_HDR_ROW_PREFIX	--'<TABLE BORDER=1><TR>'
			,i_line_suffix		=>	glb_text_output_util.HTML_TBL_HDR_ROW_SUFFIX*/);	--'</TR>'); 

		FOR r_error IN (SELECT	t.*
						FROM	inbound_text_file_error_log t
						WHERE	program_name	=	G_PROGRAM_NAME
						AND		process_id		=	G_PROCESSID
						ORDER BY inf_upd_date, step, seq)
		LOOP
			t_text(1)	:=	r_error.step;
			t_text(2)	:=	r_error.log_code;
			t_text(3)	:=	r_error.log_desc;
			t_text(4)	:=	G_RESULT_STATUS;
			t_text(5)	:=	r_error.process_flag;
			t_text(6)	:=	r_error.inf_lock_flag;
			t_text(7)	:=	r_error.inf_upd_date;
			
			glb_text_output_util.add_txt
			(it_message			=>	t_text
			/*,i_delimiter		=>	glb_text_output_util.HTML_TBL_DATA_DELIM_B  --'<TH>'
			,i_delimiter_end	=>	glb_text_output_util.HTML_TBL_DATA_DELIM_E  --'</TH>' || glb_bulk_run_report_api.NEW_LINE
			,i_separator		=>	glb_text_output_util.HTML_TBL_SEPARATOR	 --NULL
			,i_line_prefix		=>	glb_text_output_util.HTML_TBL_DATA_ROW_PREFIX   --'<TABLE BORDER=1><TR>'
			,i_line_suffix		=>	glb_text_output_util.HTML_TBL_DATA_ROW_SUFFIX*/);   --'</TR>'); 
		END LOOP;
		glb_text_output_util.write2file
			(i_file_loc		=>	G_DIRECTORY_LOG
			,i_file_name	=>	v_filename
			,o_status		=>	io_status);
		IF io_status <> glb_text_output_util.NO_ERROR THEN
			RAISE e_abnormal_end;
		END IF;
	END IF;

	IF b_has_attachment THEN
		v_blob	:=	mail_tools.get_local_binary_data
						(p_dir	=>	G_DIRECTORY_LOG
						,p_file	=>	v_filename);
	ELSE
		v_blob	:=	EMPTY_BLOB();
	END IF;

-- Start Temporary for checking email
	t_format.DELETE;
	t_format(1) :=	-1;
	glb_text_output_util.set_format -- Clear previous data
		(it_col_len	=>	t_format
		,o_status	=>	io_status);
	IF io_status <> glb_text_output_util.NO_ERROR THEN
		RAISE e_abnormal_end;
	END IF;

	t_text.DELETE;
	t_text(1)	:=	'SMTP Server: ' || i_smtp_server;
	glb_text_output_util.add_txt
		(it_message		=>	t_text
		,i_delimiter	=>	NULL
		);

	t_text.DELETE;
	t_text(1)	:=	'SMTP Port: ' || i_smtp_port;
	glb_text_output_util.add_txt
		(it_message		=>	t_text
		,i_delimiter	=>	NULL
		);

	t_text.DELETE;
	t_text(1)	:=	'From email: ' || i_from_email;
	glb_text_output_util.add_txt
		(it_message		=>	t_text
		,i_delimiter	=>	NULL
		);

	t_text.DELETE;
	t_text(1)	:=	'To email: ' || i_to_email;
	glb_text_output_util.add_txt
		(it_message		=>	t_text
		,i_delimiter	=>	NULL
		);

	t_text.DELETE;
	t_text(1)	:=	'CC email: ' || i_cc_email;
	glb_text_output_util.add_txt
		(it_message		=>	t_text
		,i_delimiter	=>	NULL
		);

	t_text.DELETE;
	t_text(1)	:=	'BCC email: ' || i_bcc_email;
	glb_text_output_util.add_txt
		(it_message		=>	t_text
		,i_delimiter	=>	NULL
		);

	t_text.DELETE;
	t_text(1)	:=	'Email Subject: ' || i_subject;
	glb_text_output_util.add_txt
		(it_message		=>	t_text
		,i_delimiter	=>	NULL
		);

	IF b_has_attachment THEN
		t_text.DELETE;
		t_text(1)	:=	'Email Attached File: ' || v_filename;
		glb_text_output_util.add_txt
			(it_message		=>	t_text
			,i_delimiter	=>	NULL
			);
	END IF;

	t_text.DELETE;
	t_text(1)	:=	'Email Message:';
	glb_text_output_util.add_txt
		(it_message		=>	t_text
		,i_delimiter	=>	NULL
		);

	t_text.DELETE;
	t_text(1)	:=	v_msg;
	glb_text_output_util.add_txt
		(it_message		=>	t_text
		,i_delimiter	=>	NULL
		);

	glb_text_output_util.write2file
		(i_file_loc		=>	G_DIRECTORY_LOG
		,i_file_name	=>	'email_' || TO_CHAR(SYSDATE,'YYYYMMDD_HH24MISSSSS')
		,o_status		=>	io_status);
	IF io_status <> glb_text_output_util.NO_ERROR THEN
		RAISE e_abnormal_end;
	END IF;
-- End Temporary for checking email

	mail_tools.sendmail
		(smtp_server		=>	i_smtp_server
		,smtp_server_port	=>	i_smtp_port
		,from_name			=>	i_from_email
		,to_name			=>	i_to_email  -- list of TO email addresses separated by commas (,)
		,cc_name			=>	i_cc_email  -- list of CC email addresses separated by commas (,)
		,bcc_name			=>	i_bcc_email -- list of BCC email addresses separated by commas (,)
		,subject			=>	i_subject
		,message			=>	v_msg
		,priority			=>	i_priority  -- 1-5 1 being the highest priority and 3 normal priority
		,filename			=>	v_filename
		,binaryfile			=>	v_blob);
END send_email;

-- Add by TM 02-Aug-2018
FUNCTION get_dirobj4log
(i_input_path	IN	VARCHAR2)
RETURN VARCHAR2
IS
	v_path		VARCHAR2(4000)	:=	i_input_path;
BEGIN
	IF v_path LIKE '_%/' THEN
		v_path	:=	SUBSTR(v_path,1,LENGTH(v_path)-1);
	END IF;
	
	IF v_path IS NOT NULL THEN
		v_path	:=	v_path || '/LOG';
	ELSE
		v_path	:=	'/LOG';
	END IF;

	FOR rec IN (SELECT	directory_name
				FROM	all_directories
				WHERE	directory_path	=	v_path
				AND		directory_name LIKE 'DTAC_ONE_LOG_DIR%')
	LOOP
		G_DIRECTORY_LOG	:=	rec.directory_name;
		EXIT;
	END LOOP;
	
	IF G_DIRECTORY_LOG IS NULL THEN
		SELECT	MAX(directory_name)
		INTO	G_DIRECTORY_LOG
		FROM	all_directories
		WHERE	directory_name LIKE 'DTAC_ONE_LOG_DIR%';
		
		IF G_DIRECTORY_LOG = 'DTAC_ONE_LOG_DIR' THEN
			G_DIRECTORY_LOG :=	G_DIRECTORY_LOG || '_001';
		ELSE
			G_DIRECTORY_LOG :=	'DTAC_ONE_LOG_DIR_' || TO_CHAR(NVL(TO_NUMBER(SUBSTR(G_DIRECTORY_LOG,18)),0)+1,'fm000');
		END IF;
		
		EXECUTE IMMEDIATE 'CREATE DIRECTORY ' || G_DIRECTORY_LOG || ' AS ''' || v_path || '''';
	END IF;
	G_DIRECTORY_LOG	:=	UPPER(G_DIRECTORY_LOG); -- Make sure we always refer directory name with uppercase

	RETURN G_DIRECTORY_LOG;
END get_dirobj4log;

--@@@ AUTO RUNNING PROCESS @@@ --
PROCEDURE main_inbound
(p_source_system	IN	VARCHAR2	-- 15 chars
,p_ou				IN	in_req_head.ou_code%TYPE
,p_file_date		IN	VARCHAR2
,p_file_back_days	IN	NUMBER
,p_log_back_days	IN	NUMBER
,p_input_path		IN	VARCHAR2
,p_field_separator	IN	VARCHAR2
,p_mail_to			IN	VARCHAR2	-- Send email to ?? upon completion
,p_vat_tolerance	IN	NUMBER
,N_NO				IN	NUMBER
,N_ID				IN	NUMBER)
IS
	v_result	VARCHAR2(400)	:=	'Default';
	v_work		VARCHAR2(1);
	suc_cnt		NUMBER;
	err_cnt		NUMBER;
	m_seq		NUMBER;
	v_total_headers		NUMBER;
BEGIN
	SELECT	MAX(j.broken)
	INTO	G_JOB_BROKEN
	FROM	all_jobs j
	WHERE	j.job	=	N_NO;

	G_DIRECTORY			:=	NULL;   -- Clear value
	G_DIRECTORY_LOG		:=	NULL;   -- Clear value
	G_RESULT_STATUS		:=	'SUCCESS';
	G_SOURCE_SYSTEM		:=	p_source_system;
	G_FIELD_SEPARATOR	:=	p_field_separator;
	
	FOR rec IN (SELECT	directory_name
				FROM	all_directories
				WHERE	directory_path	=	p_input_path
				AND		directory_name LIKE 'DTAC_ONE_DIR%')
	LOOP
		G_DIRECTORY	:=	rec.directory_name;
		EXIT;
	END LOOP;
	
	IF G_DIRECTORY IS NULL THEN
		SELECT	MAX(directory_name)
		INTO	G_DIRECTORY
		FROM	all_directories
		WHERE	directory_name LIKE 'DTAC_ONE_DIR%';
		
		IF G_DIRECTORY = 'DTAC_ONE_DIR' THEN
			G_DIRECTORY :=	G_DIRECTORY || '_001';
		ELSE
			G_DIRECTORY :=	'DTAC_ONE_DIR_' || TO_CHAR(NVL(TO_NUMBER(SUBSTR(G_DIRECTORY,14)),0)+1,'fm000');
		END IF;
		
		EXECUTE IMMEDIATE 'CREATE DIRECTORY ' || G_DIRECTORY || ' AS ''' || p_input_path || '''';
	END IF;
	G_DIRECTORY	:=	UPPER(G_DIRECTORY); -- Make sure we always refer directory name with uppercase

	G_DIRECTORY_LOG	:=	get_dirobj4log(p_input_path);	-- Add by TM 02-Aug-2018

-- Set parameters
	G_OU_CODE			:=	NVL(p_ou,'%');
	G_FILE_BACK_DAYS	:=	p_file_back_days;
	IF p_file_date IS NULL THEN
		G_START_DATE	:=	TRUNC(SYSDATE) - G_FILE_BACK_DAYS;
	ELSE
		G_START_DATE	:=	TO_DATE(p_file_date,'DD/MM/RRRR');
	END IF;
	G_END_DATE			:=	G_START_DATE;
	G_LOG_BACK_DAYS		:=	p_log_back_days;
	G_N_NO				:=	n_no;
	G_N_ID				:=	n_id;
	G_FILENAME_PATTERN	:=	UPPER(p_source_system) || '_$OU$_' || TO_CHAR(G_START_DATE,'YYYYMMDD') || '.DAT';

-- Clear log
	G_INF_SEQ	:=	1;
	clear_log;
	
	G_PROCESSID	:=	NULL;   -- Clear value

	-->> Get Process ID Running.
	IF G_PROCESSID IS NULL THEN
		BEGIN
			SELECT	LPAD(NVL(TO_NUMBER(PARAMETER_VALUE), 1), 5, 0)
			INTO	G_PROCESSID
			FROM	PB_PARAMETER_SETUP
			WHERE	PGM_SETUP = 'DTC1INFP'
			AND		PARAMETER_NAME = 'DTAC One Interface Process ID';

			UPDATE	PB_PARAMETER_SETUP
			SET		PARAMETER_VALUE = TO_CHAR(NVL(TO_NUMBER(PARAMETER_VALUE),0) + 1)
			WHERE	PGM_SETUP = 'DTC1INFP'
			AND		PARAMETER_NAME = 'DTAC One Interface Process ID';
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				G_PROCESSID	:=	'00001';
				INSERT INTO PB_PARAMETER_SETUP
				(PGM_SETUP
				,PARAMETER_NAME
				,PARAMETER_VALUE
				,TRANSFER
				,UPD_BY
				,UPD_DATE
				,UPD_PGM)
				VALUES
				('DTC1INFP'
				,'DTAC One Interface Process ID'
				,G_PROCESSID + 1	-- Next process id
				,SYSDATE
				,'AUTO'
				,SYSDATE
				,'BACK-JOB');
		END;
	END IF;

	glb_text_output_util.g_process_id	:=	G_PROCESSID;

	-- CHECK WORK CONDITION
	V_WORK	:=	'Y';

	IF NVL(V_WORK, 'N') = 'Y' THEN
		G_CNT_DOC_IMPORT	:=	0;
		G_CNT_DOC_ERROR		:=	0;

		-- WRITE START PROCESS
		M_SEQ	:=	GET_MONITOR_SEQ(N_ID);
			
		BEGIN
			INSERT INTO JOBS_MONITOR
				(JOB_ID,
				 JOB_SEQ,
				 JOB_NAME,
				 JOB_MESG,
				 JOB_TIME,
				 SUC_CNT,
				 ERR_CNT,
				 UPD_BY,
				 UPD_DATE,
				 UPD_PGM,
				 STATUS)
			VALUES
				(N_ID,
				 M_SEQ,
				 C_JOB_NAME,
				 'Start to run : DTAC_ONE_INTERFACE',
				 SYSDATE,
				 G_CNT_DOC_IMPORT,
				 G_CNT_DOC_ERROR,
				 'AUTO',
				 SYSDATE,
				 'BACK-JOB',
				 'Start Run Process ' || G_PROCESSID);
		EXCEPTION
			WHEN OTHERS THEN
				NULL;
		END;
		
		G_FILE_NUM	:=	0;
		FOR r_org IN (SELECT ou_code FROM su_organize WHERE ou_code LIKE G_OU_CODE ORDER BY OU_CODE)
		LOOP
			G_CNT_DOC_IMPORT	:=	0;
			G_CNT_DOC_ERROR		:=	0;
			G_CNT_DTL_ERROR		:=	0;
			G_CNT_PAY_ERROR		:=	0;
			G_CNT_RECTYP_ERROR	:=	0;
			G_CUT_FIELDS_ERROR	:=	0;
			
			G_FILE_NUM	:=	G_FILE_NUM + 1;
			G_OU_CODE	:=	r_org.ou_code;
			G_FILENAME	:=	REPLACE(G_FILENAME_PATTERN,'$OU$',UPPER(G_OU_CODE));
			v_total_headers	:=	get_count(SUBSTR(G_FILENAME,1,LENGTH(G_FILENAME)-3) || 'SYNC');

			start_process('AUTO'
						 ,'BACK-JOB'
						 ,v_result);
			IF UPPER(v_result) NOT LIKE 'COMPLETE%' THEN
				GOTO end_process;
			END IF;

			-- @@@ Prepare Data @@@ --
			prepare_data('AUTO'
						,'BACK-JOB'
						,v_result);
			IF G_CNT_DOC_IMPORT <> v_total_headers THEN
				-- Count headers <> Actual headers
				mark_file_status('E');
				v_result	:=	'Number of document headers <> number from .SYNC file';
				GOTO end_process;
			ELSIF UPPER(v_result) NOT LIKE 'COMPLETE%' THEN
				GOTO end_process;
			END IF;

			-- @@@ Validate
			G_VAT_TOLERANCE	:=	p_vat_tolerance;
			validate_n_dflt	('AUTO'
							,'BACK-JOB'
							,v_result);
			IF UPPER(v_result) NOT LIKE 'COMPLETE%' THEN
				GOTO end_process;
			END IF;
			
			--> INBOUND INTERFACE
			interface_data	('AUTO'
							,'BACK-JOB'
							,v_result);
			IF v_result <> glb_text_output_util.NO_ERROR THEN
				REMOVE_PROCESS  ('AUTO'
								,'BACK-JOB');
				GOTO end_process;
			END IF;

			ending_process  ('AUTO'
							,'BACK-JOB');
			GOTO EXIT_PROCESS;
			-- @@@	 End	  @@@ --
			<<END_PROCESS>>
			BEGIN
				-->> Initial log
				GR_LOG.PROCESS_ID		:=	G_PROCESSID;
				GR_LOG.INF_UPD_BY		:=	'AUTO';
				GR_LOG.INF_UPD_PGM		:=	'BACK-JOB';

				-->> Lock Transaction Pending.
				GR_LOG.STEP				:=	G_FILE_NUM + .8;
				GR_LOG.STEP_DESC		:=	'Error Interface';
				GR_LOG.PROCESS_FLAG		:=	'Error';
				GR_LOG.INF_LOCK_FLAG	:=	'Unlock';

				GR_LOG.PROCESS_DATE		:=	SYSDATE;
				GR_LOG.SEQ				:=	0;
				GR_LOG.LOG_CODE			:=	G_PROCESSID || ' Error Interface from DTAC One';
				GR_LOG.LOG_DESC			:=	v_result;
				GR_LOG.INF_UPD_DATE		:=	SYSDATE;
				WRITE_LOG(GR_LOG);

				G_RESULT_STATUS	:=	'FAILED';
			END END_PROCESS;
			
			<<EXIT_PROCESS>>
			NULL;
		-- WRITE END PROCESS
		END LOOP;

-- After finish all files
		-- Send email
		send_email
			(i_to_email		=>	p_mail_to
			,i_cc_email		=>	NULL
			,i_bcc_email	=>	NULL
			,io_status		=>	v_result
			);
		IF v_result <> glb_text_output_util.NO_ERROR THEN
			GOTO end_process_2;
		END IF;

		BEGIN
			INSERT INTO JOBS_MONITOR
				(JOB_ID,
				 JOB_SEQ,
				 JOB_NAME,
				 JOB_MESG,
				 JOB_TIME,
				 SUC_CNT,
				 ERR_CNT,
				 UPD_BY,
				 UPD_DATE,
				 UPD_PGM,
				 STATUS)
			VALUES
				(N_ID,
				 M_SEQ + 0.1,
				 C_JOB_NAME,
				 'Import file from DTAC_ONE_INTERFACE ' || DECODE(G_RESULT_STATUS,'SUCCESS','Complete','Failed'),
				 SYSDATE,
				 G_CNT_DOC_IMPORT,
				 G_CNT_DOC_ERROR,
				 'AUTO',
				 SYSDATE,
				 'BACK-JOB',
				 'Complete Process ' || G_PROCESSID);
			GOTO EXIT_PROCESS_2;
		EXCEPTION
			WHEN OTHERS THEN
				NULL;
		END EXIT_PROCESS;

		<<END_PROCESS_2>>
		BEGIN
			-->> Initial log
			GR_LOG.PROCESS_ID		:=	G_PROCESSID;
			GR_LOG.INF_UPD_BY		:=	'AUTO';
			GR_LOG.INF_UPD_PGM		:=	'BACK-JOB';

			-->> Lock Transaction Pending.
			GR_LOG.STEP				:=	G_FILE_NUM + 1.9;
			GR_LOG.STEP_DESC		:=	'Error Interface';
			GR_LOG.PROCESS_FLAG		:=	'Error';
			GR_LOG.INF_LOCK_FLAG	:=	'Unlock';

			GR_LOG.PROCESS_DATE		:=	SYSDATE;
			GR_LOG.SEQ				:=	0;
			GR_LOG.LOG_CODE			:=	G_PROCESSID || ' Error Sending Email';
			GR_LOG.LOG_DESC			:=	v_result;
			GR_LOG.INF_UPD_DATE		:=	SYSDATE;
			WRITE_LOG(GR_LOG);

			G_RESULT_STATUS		:=	'FAILED';
			mark_file_status('E');
		END END_PROCESS_2;

		<<EXIT_PROCESS_2>>
		--V_WORK	:=	CLEAR_JOB_WORK(N_NO);
		COMMIT;
	END IF;
END main_inbound;
-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@--

END SC_TRX_ITF_DTAC_ONE;
/
