ALTER TABLE SC_TRANSACTION ADD source_module VARCHAR2(30);
ALTER TABLE SC_TRANSACTION ADD source_reference VARCHAR2(100);

ALTER TABLE SC_TRANS_DTL ADD source_module VARCHAR2(30);
ALTER TABLE SC_TRANS_DTL ADD source_reference VARCHAR2(100);

ALTER TABLE SC_TRANS_PAY ADD source_module VARCHAR2(30);
ALTER TABLE SC_TRANS_PAY ADD source_reference VARCHAR2(100);

COMMENT ON COLUMN SC_TRANSACTION.source_module IS 'Source module in case the record was interfaced from external';
COMMENT ON COLUMN SC_TRANSACTION.source_reference IS 'Source reference in case the record was interfaced from external';

COMMENT ON COLUMN SC_TRANS_DTL.source_module IS 'Source module in case the record was interfaced from external';
COMMENT ON COLUMN SC_TRANS_DTL.source_reference IS 'Source reference in case the record was interfaced from external';

COMMENT ON COLUMN SC_TRANS_PAY.source_module IS 'Source module in case the record was interfaced from external';
COMMENT ON COLUMN SC_TRANS_PAY.source_reference IS 'Source reference in case the record was interfaced from external';
