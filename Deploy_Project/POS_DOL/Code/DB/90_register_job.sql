/*
select * FROM tab WHERE tname LIKE '%PARAM%';

SELECT * FROM jobs_setup;	-- Submit job
SELECT * FROM jobs_proc;	-- Register job
SELECT * FROM PB_PARAMETER_SETUP;
SELECT * FROM sa_param_lists;
SELECT * FROM sa_param_set;
*/

INSERT INTO jobs_proc
(seq, 
job_name, 
job_desc, 
proc_name, 
function_id, 
upd_by, 
upd_date, 
upd_pgm)
VALUES
((SELECT MAX(seq)+1 FROM jobs_proc), 
'Interface from DTAC One', 
'Interface POS Other Payments from DTAC One', 
'SC_TRX_ITF_DTAC_ONE.main_inbound', 
'DTJPPC18', 
'SS-STAFF', 
SYSDATE, 
'DTPBRT14');
