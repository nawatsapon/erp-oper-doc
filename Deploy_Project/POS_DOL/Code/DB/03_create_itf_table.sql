-- Create table
CREATE TABLE SC_TRANSACTION_ITF
(
  ou_code           VARCHAR2(3),
  subinv_code       VARCHAR2(10),
  doc_no            VARCHAR2(15),
  doc_type          VARCHAR2(3) DEFAULT '1',
  doc_date          DATE,
  cat_code          VARCHAR2(5),
  cust_code         VARCHAR2(13),
  cust_name         VARCHAR2(200),
  cust_add          VARCHAR2(400),
--  cust_tumbon       VARCHAR2(8),
  cust_amphur       VARCHAR2(8),
  cust_province     VARCHAR2(8),
  cust_postal       VARCHAR2(5),
  cust_tel          VARCHAR2(50),
  gross_amt         NUMBER(19,2),
  total_amt         NUMBER(12,2),
  net_amt           NUMBER(12,2),
  vat_code          VARCHAR2(2),
  vat_rate          NUMBER(5,2),
  vat_amt           NUMBER(12,2),
/*  wht_code          VARCHAR2(2),
  wht_rate          NUMBER(5,2),
  wht_amt           NUMBER(12,2),
  refer_iv_no       VARCHAR2(15),
  reprint           NUMBER(2),
  cn_reason         VARCHAR2(3),
  tf_s_c            DATE,
  tf_c_o            DATE,*/
  salesperson       VARCHAR2(30),
  team_header       VARCHAR2(30),
  shop_mgr          VARCHAR2(30),
  area_mgr          VARCHAR2(30),
  remark            VARCHAR2(500),
  cn_flag           VARCHAR2(1) DEFAULT 'N',
  interface_status  VARCHAR2(1) DEFAULT 'N',
--  reprint_reason    VARCHAR2(2),
  change_amt        NUMBER,
  tax_id            VARCHAR2(30),
  branch_code       VARCHAR2(100),
  ac_type           VARCHAR2(1) DEFAULT 'N',
/*  ac_item           VARCHAR2(30),
  ac_serial         VARCHAR2(50),
  ac_imei           VARCHAR2(50),
  ac_meid           VARCHAR2(50),
  ac_lang           VARCHAR2(3),
  ac_hwdop          DATE,
  ac_email          VARCHAR2(200),
  ac_status         VARCHAR2(1) DEFAULT 'P',
  ac_date           DATE,
  ac_warr_date      DATE,
  ac_warr_period    NUMBER,
  ac_message        VARCHAR2(400),
  ac_updsts         VARCHAR2(15),
  ac_upddate        DATE,
  ac_reason         VARCHAR2(10),
  ac_chgremark      VARCHAR2(400),
  ac_chgby          VARCHAR2(15),*/
  cre_by            VARCHAR2(15),
  cre_date          DATE DEFAULT SYSDATE,
  status            VARCHAR2(1) DEFAULT 'N',
  ref_ccb_cust_flag VARCHAR2(1) DEFAULT 'N',

  source_module		VARCHAR2(30),
  source_reference	VARCHAR2(100),
  file_id			NUMBER,
  line_num			NUMBER,
  itf_status		VARCHAR2(1) DEFAULT 'N'
);
-- Add comments to the columns 
COMMENT ON COLUMN SC_TRANSACTION_ITF.ou_code			IS 'Organization Code';
COMMENT ON COLUMN SC_TRANSACTION_ITF.subinv_code		IS 'Subinventory / �����Ң�';
COMMENT ON COLUMN SC_TRANSACTION_ITF.doc_no				IS '�Ţ����͡���';
COMMENT ON COLUMN SC_TRANSACTION_ITF.doc_type			IS '�������͡��� (��˹���� 1=���, 3=�Ѻ�׹, Ŵ˹���â��)';
COMMENT ON COLUMN SC_TRANSACTION_ITF.doc_date			IS '�ѹ����͡���';
COMMENT ON COLUMN SC_TRANSACTION_ITF.cat_code			IS '������١���';
COMMENT ON COLUMN SC_TRANSACTION_ITF.cust_code			IS '�����١���';
COMMENT ON COLUMN SC_TRANSACTION_ITF.cust_name			IS '����/��������´�١���';
COMMENT ON COLUMN SC_TRANSACTION_ITF.cust_add			IS '�������/��ҹ�Ţ���';
COMMENT ON COLUMN SC_TRANSACTION_ITF.cust_amphur		IS '���������';
COMMENT ON COLUMN SC_TRANSACTION_ITF.cust_province		IS '���ʨѧ��Ѵ';
COMMENT ON COLUMN SC_TRANSACTION_ITF.cust_postal		IS '������ɳ��������������';
COMMENT ON COLUMN SC_TRANSACTION_ITF.cust_tel			IS '�������Ѿ���١���';
COMMENT ON COLUMN SC_TRANSACTION_ITF.gross_amt			IS '��Ť���Ѻ��� (Net Amount) �ѡ������Ť������ (Withholding Tax Amount)';
COMMENT ON COLUMN SC_TRANSACTION_ITF.total_amt			IS '��Ť����� �ѡ������Ť������';
COMMENT ON COLUMN SC_TRANSACTION_ITF.net_amt			IS '��Ť����� ���������Ť������';
COMMENT ON COLUMN SC_TRANSACTION_ITF.vat_code			IS '����������Ť������';
COMMENT ON COLUMN SC_TRANSACTION_ITF.vat_rate			IS '�ѵ��������Ť������';
COMMENT ON COLUMN SC_TRANSACTION_ITF.vat_amt			IS '������Ť������ (�Դ�ҡ�ҹ���ժ�ͧ Total Amount * Vat Rate)';
COMMENT ON COLUMN SC_TRANSACTION_ITF.salesperson		IS 'Salesperson';
COMMENT ON COLUMN SC_TRANSACTION_ITF.team_header		IS 'Team Header';
COMMENT ON COLUMN SC_TRANSACTION_ITF.shop_mgr			IS 'Shop Manager';
COMMENT ON COLUMN SC_TRANSACTION_ITF.area_mgr			IS 'Area Manager';
COMMENT ON COLUMN SC_TRANSACTION_ITF.remark				IS '�����˵� / ��������´�������';
COMMENT ON COLUMN SC_TRANSACTION_ITF.cn_flag			IS 'ʶҹС���͡�Ŵ˹��';
COMMENT ON COLUMN SC_TRANSACTION_ITF.interface_status	IS 'ʶҹС�� Interface';
COMMENT ON COLUMN SC_TRANSACTION_ITF.change_amt			IS '�Թ�͹';
COMMENT ON COLUMN SC_TRANSACTION_ITF.tax_id				IS '�Ţ��Шӵ�Ǽ�����������ҡ���ҧ�ԧ (�������Թ���)';
COMMENT ON COLUMN SC_TRANSACTION_ITF.branch_code		IS '��������´/�����ҢҢͧ������������ҡ���ҧ�ԧ (�������Թ���)';
COMMENT ON COLUMN SC_TRANSACTION_ITF.cre_by				IS 'Created By';
COMMENT ON COLUMN SC_TRANSACTION_ITF.cre_date			IS 'Created Date';
COMMENT ON COLUMN SC_TRANSACTION_ITF.status				IS 'ʶҹ��͡���(N=Normal, C=C/N)';
COMMENT ON COLUMN SC_TRANSACTION_ITF.ref_ccb_cust_flag	IS 'Y=This transaction is referred to CCB Customer';
COMMENT ON COLUMN SC_TRANSACTION_ITF.source_module		IS 'Source module in case the record was interfaced from external';
COMMENT ON COLUMN SC_TRANSACTION_ITF.source_reference	IS 'Source reference in case the record was interfaced from external';
COMMENT ON COLUMN SC_TRANSACTION_ITF.file_id			IS 'File ID';
COMMENT ON COLUMN SC_TRANSACTION_ITF.line_num			IS 'Line Number in text file';
COMMENT ON COLUMN SC_TRANSACTION_ITF.itf_status			IS 'Interface Status (Y=Can be interfaced, N=Not yet interface, E=Error, default=N)';

-- Create/Recreate indexes 
CREATE INDEX SC_TRANSACTION_ITF_N1 ON SC_TRANSACTION_ITF (file_id, line_num);
CREATE INDEX SC_TRANSACTION_ITF_N2 ON SC_TRANSACTION_ITF (file_id, source_module, source_reference);



-- Create table
CREATE TABLE SC_TRANS_DTL_ITF
(
  ou_code           VARCHAR2(3),
  subinv_code       VARCHAR2(10),
  doc_no            VARCHAR2(15),
  doc_type          VARCHAR2(3) DEFAULT '1',
  seq_no            NUMBER(5),
  trans_code        VARCHAR2(20),
  qty               NUMBER(10),
  unitprice         NUMBER(12,2) DEFAULT 1,
  amount            NUMBER(12,2),
  std_price         NUMBER(12,2) DEFAULT 1,
  inv_seq           NUMBER(4),
  cn_flag           VARCHAR2(1) DEFAULT 'N',
  net_amt           NUMBER(19,2),
  gross_amt         NUMBER(19,2),
/*  last_cn_reference VARCHAR2(15),
  last_cn_date      DATE,
  cn_qty            NUMBER(10),
  cn_amount         NUMBER(12,2),
  cn_net_amt        NUMBER(19,2),
  tf_s_c            DATE,
  tf_c_o            DATE,*/
  cre_by            VARCHAR2(15),
  cre_date          DATE,

  source_module		VARCHAR2(30),
  source_reference	VARCHAR2(100),
  hdr_source_ref	VARCHAR2(100),
  file_id			NUMBER,
  line_num			NUMBER,
  itf_status		VARCHAR2(1) DEFAULT 'N'
);
-- Add comments to the columns 
COMMENT ON COLUMN SC_TRANS_DTL_ITF.ou_code			IS 'Organization Code';
COMMENT ON COLUMN SC_TRANS_DTL_ITF.subinv_code		IS 'Subinventory / �����Ң�';
COMMENT ON COLUMN SC_TRANS_DTL_ITF.doc_no			IS '�Ţ����͡���';
COMMENT ON COLUMN SC_TRANS_DTL_ITF.doc_type			IS '�������͡��� (��˹���� 1=���, 3=�Ѻ�׹, Ŵ˹���â��)';
COMMENT ON COLUMN SC_TRANS_DTL_ITF.seq_no			IS '�ӴѺ��¡��';
COMMENT ON COLUMN SC_TRANS_DTL_ITF.trans_code		IS '������¡�ê����Թ';
COMMENT ON COLUMN SC_TRANS_DTL_ITF.qty				IS '�ӹǹ˹��� (Default �����ҡѺ��Ť�Ң�� Amount ����)';
COMMENT ON COLUMN SC_TRANS_DTL_ITF.unitprice		IS '�Ҥҵ��˹��� (Default ��ҡѺ 1 �ҷ����)';
COMMENT ON COLUMN SC_TRANS_DTL_ITF.amount			IS '��Ť���Ѻ���� (Amount) : ˹��ºҷ';
COMMENT ON COLUMN SC_TRANS_DTL_ITF.std_price		IS '�Ҥ��ҵðҹ���˹��� (Default ��ҡѺ 1 �ҷ����)';
COMMENT ON COLUMN SC_TRANS_DTL_ITF.inv_seq			IS '�ӴѺ��¡��';
COMMENT ON COLUMN SC_TRANS_DTL_ITF.cn_flag			IS 'ʶҹС�ù���¡��';
COMMENT ON COLUMN SC_TRANS_DTL_ITF.net_amt			IS '��Ť���Ѻ���� (Amount) : ˹��ºҷ';
COMMENT ON COLUMN SC_TRANS_DTL_ITF.gross_amt		IS '��Ť�ҡ�͹�ѡ��ǹŴ';
COMMENT ON COLUMN SC_TRANS_DTL_ITF.cre_by			IS 'Created By';
COMMENT ON COLUMN SC_TRANS_DTL_ITF.cre_date			IS 'Created Date';
COMMENT ON COLUMN SC_TRANS_DTL_ITF.source_module	IS 'Source module in case the record was interfaced from external';
COMMENT ON COLUMN SC_TRANS_DTL_ITF.source_reference	IS 'Source reference in case the record was interfaced from external';
COMMENT ON COLUMN SC_TRANS_DTL_ITF.hdr_source_ref	IS 'Transaction Header''s Source reference in case the record was interfaced from external';
COMMENT ON COLUMN SC_TRANS_DTL_ITF.file_id			IS 'File ID';
COMMENT ON COLUMN SC_TRANS_DTL_ITF.line_num			IS 'Line Number in text file';
COMMENT ON COLUMN SC_TRANS_DTL_ITF.itf_status		IS 'Interface Status (Y=Can be interfaced, N=Not yet interface, E=Error, default=N)';

-- Create/Recreate primary, unique and foreign key constraints 
ALTER TABLE SC_TRANS_DTL_ITF ADD CONSTRAINT SC_TRANS_DTL_ITF_PK PRIMARY KEY (file_id, line_num);
-- Create/Recreate indexes 
CREATE INDEX SC_TRANS_DTL_ITF_N1 ON SC_TRANS_DTL_ITF (file_id, itf_status);



-- Create table
CREATE TABLE SC_TRANS_PAY_ITF
(
  ou_code          VARCHAR2(3),
  subinv_code      VARCHAR2(10),
  doc_type         VARCHAR2(3),
  doc_no           VARCHAR2(15),
  payment_code     VARCHAR2(10),
  ref_no           VARCHAR2(30),
  bank_code        VARCHAR2(5),
  credit_card      VARCHAR2(5),
  cr_bank_rate     NUMBER(4,2),
  pay_amt          NUMBER(12,2),
  approve_no       VARCHAR2(30),
  pay_net          NUMBER(12,2),
/*  tf_s_c           DATE,
  tf_c_o           DATE,*/
  cre_by           VARCHAR2(15),
  cre_date         DATE,

  source_module		VARCHAR2(30),
  source_reference	VARCHAR2(100),
  hdr_source_ref	VARCHAR2(100),
  file_id			NUMBER,
  line_num			NUMBER,
  itf_status		VARCHAR2(1) DEFAULT 'N'
);
-- Add comments to the columns 
COMMENT ON COLUMN SC_TRANS_PAY_ITF.ou_code			IS 'Organization Code';
COMMENT ON COLUMN SC_TRANS_PAY_ITF.subinv_code		IS 'Subinventory / �����Ң�';
COMMENT ON COLUMN SC_TRANS_PAY_ITF.doc_type			IS '�������͡��� (��˹���� 1=���, 3=�Ѻ�׹, Ŵ˹���â��)';
COMMENT ON COLUMN SC_TRANS_PAY_ITF.doc_no			IS '�Ţ����͡���';
COMMENT ON COLUMN SC_TRANS_PAY_ITF.payment_code		IS '���ʻ���������Ѻ�����Թ�� �Թʴ, �ѵ��ôԵ �繵�';
COMMENT ON COLUMN SC_TRANS_PAY_ITF.ref_no			IS '�����Ţ��ҧ�ԧ�ҡ����ٴ�ѵ� (EDC Reference / Approve No)';
COMMENT ON COLUMN SC_TRANS_PAY_ITF.bank_code		IS '���ʸ�Ҥ��';
COMMENT ON COLUMN SC_TRANS_PAY_ITF.credit_card		IS '���ʻ������ѵ��ôԵ �� VISA';
COMMENT ON COLUMN SC_TRANS_PAY_ITF.cr_bank_rate		IS '�ѵ�Ҥ������¼�ҹ�ѵ��ôԵ';
COMMENT ON COLUMN SC_TRANS_PAY_ITF.pay_amt			IS '�ӹǹ�Թ������';
COMMENT ON COLUMN SC_TRANS_PAY_ITF.approve_no		IS '�����Ţ͹��ѵ���ҧ�ԧ';
COMMENT ON COLUMN SC_TRANS_PAY_ITF.pay_net			IS '�Թ�����¤���Թ��Ҩ�ԧ';
COMMENT ON COLUMN SC_TRANS_PAY_ITF.cre_by			IS 'Created By';
COMMENT ON COLUMN SC_TRANS_PAY_ITF.cre_date			IS 'Created Date';
COMMENT ON COLUMN SC_TRANS_PAY_ITF.source_module	IS 'Source module in case the record was interfaced from external';
COMMENT ON COLUMN SC_TRANS_PAY_ITF.source_reference	IS 'Source reference in case the record was interfaced from external';
COMMENT ON COLUMN SC_TRANS_DTL_ITF.hdr_source_ref	IS 'Transaction Header''s Source reference in case the record was interfaced from external';
COMMENT ON COLUMN SC_TRANS_PAY_ITF.file_id			IS 'File ID';
COMMENT ON COLUMN SC_TRANS_PAY_ITF.line_num			IS 'Line Number in text file';
COMMENT ON COLUMN SC_TRANS_PAY_ITF.itf_status		IS 'Interface Status (Y=Can be interfaced, N=Not yet interface, E=Error, default=N)';

-- Create/Recreate primary, unique and foreign key constraints 
ALTER TABLE SC_TRANS_PAY_ITF ADD CONSTRAINT SC_TRANS_PAY_ITF_PK PRIMARY KEY (file_id, line_num);
-- Create/Recreate indexes 
CREATE INDEX SC_TRANS_PAY_ITF_N1 ON SC_TRANS_PAY_ITF (file_id, itf_status);
