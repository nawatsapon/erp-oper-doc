CREATE TABLE inbound_text_file
(file_id			NUMBER
,file_name			VARCHAR2(100)
,file_directory		VARCHAR2(100)
,program_name		VARCHAR2(100)
,file_inf_flag		VARCHAR2(1)
,file_inf_code		VARCHAR2(30)
,cre_by				VARCHAR2(15)
,cre_date			DATE
,CONSTRAINT inbound_text_file_pk PRIMARY KEY(file_id)
);

COMMENT ON TABLE inbound_text_file IS 'Inbound text file for generic purpose';
COMMENT ON COLUMN inbound_text_file.file_id IS 'File ID';
COMMENT ON COLUMN inbound_text_file.file_name IS 'Physical File Name';
COMMENT ON COLUMN inbound_text_file.file_directory IS 'Directory Object Name';
COMMENT ON COLUMN inbound_text_file.program_name IS 'Program Name for inbound file interface';
COMMENT ON COLUMN inbound_text_file.file_inf_flag IS 'File Interface Flag (Y, N, E)';
COMMENT ON COLUMN inbound_text_file.file_inf_code IS 'Process ID for File Interface';


CREATE TABLE inbound_text_file_line
(file_id			NUMBER
,line_num			NUMBER
,text				VARCHAR2(4000)
,CONSTRAINT inbound_text_file_line_pk PRIMARY KEY(file_id, line_num)
,CONSTRAINT inbound_text_file_line_fk FOREIGN KEY(file_id) REFERENCES inbound_text_file(file_id)
);

COMMENT ON TABLE inbound_text_file_line IS 'Inbound text file lines for generic purpose';
COMMENT ON COLUMN inbound_text_file_line.file_id IS 'File ID';
COMMENT ON COLUMN inbound_text_file_line.line_num IS 'Line Number in text file';
COMMENT ON COLUMN inbound_text_file_line.text IS 'Text in the input file';


GRANT SELECT ON inbound_text_file TO SELECTED_POS_ROLE;
GRANT SELECT ON inbound_text_file_line TO SELECTED_POS_ROLE;


-- Sequence
CREATE SEQUENCE inbound_text_file_s;


-- Create table for error log
create table inbound_text_file_error_log
(
  program_name  VARCHAR2(100),
  process_id    NUMBER,
  process_date  DATE,
  step          NUMBER,
  step_desc     VARCHAR2(400),
  seq           NUMBER,
  log_code      VARCHAR2(100),
  log_desc      VARCHAR2(400),
  process_flag  VARCHAR2(40),
  inf_lock_flag VARCHAR2(20),
  inf_upd_by    VARCHAR2(15),
  inf_upd_date  DATE,
  inf_upd_pgm   VARCHAR2(10)
);
CREATE INDEX inbound_text_file_error_log_n1 ON inbound_text_file_error_log(program_name, process_date);
CREATE INDEX inbound_text_file_error_log_n2 ON inbound_text_file_error_log(process_id, step, seq);
-- Grant/Revoke object privileges 
grant select on inbound_text_file_error_log to SELECTED_POS_ROLE;
