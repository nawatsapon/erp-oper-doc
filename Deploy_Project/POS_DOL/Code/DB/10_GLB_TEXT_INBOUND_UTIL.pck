CREATE OR REPLACE PACKAGE GLB_TEXT_INBOUND_UTIL IS
/*
Date		By				Description
----------	--------------	---------------------------------------------
22/08/2017	TM				Created
*/

-- Constants for generating HTML output (used in bulk reporting)
NEW_LINE					CONSTANT VARCHAR2(10)	:=	CHR(13) || CHR(10);

HTML_HDR_DELIM_B			CONSTANT VARCHAR2(1000)	:=	'<B>';
HTML_HDR_DELIM_E			CONSTANT VARCHAR2(1000)	:=	'</B>' || NEW_LINE || '<P>';
HTML_HDR_SEPARATOR			CONSTANT VARCHAR2(1000)	:=	NULL;
HTML_HDR_PREFIX				CONSTANT VARCHAR2(1000)	:=	'<HTML>';

HTML_TBL_HDR_DELIM_B		CONSTANT VARCHAR2(1000)	:=	'<TH>';
HTML_TBL_HDR_DELIM_B_L		CONSTANT VARCHAR2(1000)	:=	'<TH align = "left">';
HTML_TBL_HDR_DELIM_E		CONSTANT VARCHAR2(1000)	:=	'</TH>' || NEW_LINE;
HTML_TBL_SEPARATOR			CONSTANT VARCHAR2(1000)	:=	NULL;
HTML_TBL_HDR_ROW_PREFIX		CONSTANT VARCHAR2(1000)	:=	'<TABLE BORDER=1><TR>';
HTML_TBL_HDR_ROW_PREFIX_NB	CONSTANT VARCHAR2(1000)	:=	'<TABLE BORDER=0><TR>';
HTML_TBL_HDR_ROW_SUFFIX		CONSTANT VARCHAR2(1000)	:=	'</TR>';

HTML_TBL_DATA_DELIM_B		CONSTANT VARCHAR2(1000)	:=	'<TD>';
HTML_TBL_DATA_DELIM_E		CONSTANT VARCHAR2(1000)	:=	'</TD>' || NEW_LINE;
HTML_TBL_DATA_ROW_PREFIX	CONSTANT VARCHAR2(1000)	:=	'<TR>';
HTML_TBL_DATA_ROW_SUFFIX	CONSTANT VARCHAR2(1000)	:=	'</TR>';

HTML_TBL_SUFFIX				CONSTANT VARCHAR2(1000)	:=	'</TABLE>';
HTML_FILE_SUFFIX			CONSTANT VARCHAR2(1000)	:=	'</HTML>';

TYPE varchar_tabtyp IS TABLE OF VARCHAR2(4000) INDEX BY BINARY_INTEGER;
TYPE number_tabtyp IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;

PROCEDURE cut_fields
(i_text				IN	VARCHAR2
,i_delimiter		IN	VARCHAR2	:=	'"'
,i_separator		IN	VARCHAR2	:=	','
,i_trim_flag		IN	BOOLEAN		:=	TRUE
,ot_field_value		OUT	varchar_tabtyp
,o_status			OUT	VARCHAR2);

PROCEDURE read_file
(i_file_loc			IN	VARCHAR2
,i_file_name		IN	VARCHAR2
,i_cre_by			IN	VARCHAR2
,i_file_inf_code	IN	VARCHAR2
,i_program_name		IN	VARCHAR2
,i_dup_file_allow	IN	BOOLEAN	:=	FALSE
,o_file_id			OUT	NUMBER
,o_status			OUT	VARCHAR2);

PROCEDURE read_file_utf8
(i_file_loc			IN	VARCHAR2
,i_file_name		IN	VARCHAR2
,i_cre_by			IN	VARCHAR2
,i_file_inf_code	IN	VARCHAR2
,i_program_name		IN	VARCHAR2
,i_dup_file_allow	IN	BOOLEAN	:=	FALSE
,o_file_id			OUT	NUMBER
,o_status			OUT	VARCHAR2);

END GLB_TEXT_INBOUND_UTIL;
/
CREATE OR REPLACE PACKAGE BODY GLB_TEXT_INBOUND_UTIL IS
/*
Date		By				Description
----------	--------------	---------------------------------------------
22/08/2017	TM				Created
*/

PROCEDURE cut_fields
(i_text				IN	VARCHAR2
,i_delimiter		IN	VARCHAR2	:=	'"'
,i_separator		IN	VARCHAR2	:=	','
,i_trim_flag		IN	BOOLEAN		:=	TRUE
,ot_field_value		OUT	varchar_tabtyp
,o_status			OUT	VARCHAR2)
IS
	v_text					VARCHAR2(4000)	:=	REPLACE(REPLACE(i_text,CHR(13)),CHR(10));
	v_pos					NUMBER;
	v_separator_full		VARCHAR2(100)	:=	i_separator;
	v_len_separator_full	NUMBER	:=	LENGTH(v_separator_full);
	v_field_value			VARCHAR2(4000);
	v_delimiter_end_pos		NUMBER;
	v_delim_tmp_pos			NUMBER;
	v_tmp					NUMBER;
BEGIN
	-- Initial status output parameter
	o_status	:=	glb_text_output_util.NO_ERROR;
	ot_field_value.DELETE;

	WHILE v_text IS NOT NULL OR NVL(v_pos,0) <> 0
	LOOP
		v_field_value	:=	NULL;	-- Clear data
		IF i_delimiter IS NOT NULL AND v_text LIKE i_delimiter || '%' THEN
			v_delim_tmp_pos		:=	LENGTH(i_delimiter)+1;
			LOOP
				v_tmp			:=	INSTR(v_text,i_delimiter,v_delim_tmp_pos);
				EXIT WHEN NVL(v_tmp,0) = 0 OR SUBSTR(v_text,v_tmp) NOT LIKE i_delimiter||i_delimiter||'%';
				v_delim_tmp_pos	:=	v_tmp+LENGTH(i_delimiter)*2;
			END LOOP;
			v_delimiter_end_pos	:=	INSTR(v_text,i_delimiter,v_delim_tmp_pos);
		ELSE
			v_delimiter_end_pos	:=	1;
		END IF;

		v_pos	:=	INSTR(v_text, v_separator_full, v_delimiter_end_pos);
		IF v_pos > 0 THEN
			v_field_value	:=	SUBSTR(v_text, 1, v_pos-1);
		ELSIF v_pos IS NOT NULL THEN
			v_field_value	:=	v_text;
			v_text			:=	NULL;
		END IF;

		IF v_field_value LIKE i_delimiter || '%' THEN
			v_field_value	:=	SUBSTR(v_field_value, NVL(LENGTH(i_delimiter),0)+1);
			v_field_value	:=	SUBSTR(v_field_value, 1, LENGTH(v_field_value)-NVL(LENGTH(i_delimiter),0));
		END IF;

		IF i_trim_flag THEN
			v_field_value	:=	LTRIM(RTRIM(v_field_value));
		END IF;

		IF i_delimiter IS NOT NULL THEN
			v_field_value	:=	REPLACE(v_field_value,i_delimiter || i_delimiter, i_delimiter);
		END IF;

		ot_field_value(ot_field_value.COUNT+1)	:=	v_field_value;
		IF v_pos IS NOT NULL THEN
			v_text	:=	SUBSTR(v_text, v_pos+v_len_separator_full);
		END IF;
	END LOOP;
END cut_fields;

PROCEDURE read_file
(i_file_loc			IN	VARCHAR2
,i_file_name		IN	VARCHAR2
,i_cre_by			IN	VARCHAR2
,i_file_inf_code	IN	VARCHAR2
,i_program_name		IN	VARCHAR2
,i_dup_file_allow	IN	BOOLEAN	:=	FALSE
,o_file_id			OUT	NUMBER
,o_status			OUT	VARCHAR2)
IS
PRAGMA AUTONOMOUS_TRANSACTION;
	v_file			UTL_FILE.FILE_TYPE;
	v_file_name		VARCHAR2(1000);
	v_file_loc		VARCHAR2(1000);
	v_message		VARCHAR2(4000);
	v_file_id		NUMBER;
	v_line_num		NUMBER	:=	0;
BEGIN
	v_message := NULL;

	v_file_loc	:=	NVL(i_file_loc, v_file_loc);
	v_file_name	:=	NVL(i_file_name, v_file_name);
	
	IF NOT i_dup_file_allow THEN
		FOR rec IN (SELECT	*
					FROM	inbound_text_file
					WHERE	file_name		=	v_file_name
					AND		file_directory	=	v_file_loc
					AND		program_name	=	i_program_name
					AND		NVL(file_inf_flag,'N')	<>	'E')
		LOOP
			o_status	:=	glb_text_output_util.C_ERROR || 'File name & path are duplicate with non-error file ,Input File Path : '||v_file_loc||' - '||v_file_name;
			RETURN;
			EXIT;	-- process once
		END LOOP;
	END IF;

	v_file	:=	UTL_FILE.fopen	(location	=>	v_file_loc
								,filename	=>	v_file_name
								,open_mode	=>	'r'
								,max_linesize=> 32767);

-- Make sure file_id must not duplicate
	LOOP
		SELECT	inbound_text_file_s.NEXTVAL
		INTO	v_file_id
		FROM	dual;
		FOR rec IN (SELECT * FROM inbound_text_file WHERE file_id = v_file_id)
		LOOP
			v_file_id	:=	NULL;
		END LOOP;
		IF v_file_id IS NOT NULL THEN
			INSERT INTO inbound_text_file
				(file_id
				,file_name
				,file_directory
				,program_name
				,file_inf_flag
				,file_inf_code
				,cre_by
				,cre_date)
			VALUES
				(v_file_id
				,v_file_name
				,v_file_loc
				,i_program_name
				,'N'
				,i_file_inf_code
				,i_cre_by
				,SYSDATE);
			EXIT;
		END IF;
	END LOOP;

	LOOP
		BEGIN
			UTL_FILE.get_line(file	=>	v_file, buffer	=>	v_message);
		EXCEPTION
			WHEN NO_DATA_FOUND THEN	-- End of file
				EXIT;	-- End loop
		END;
		v_line_num	:=	v_line_num + 1;
		INSERT INTO inbound_text_file_line
			(file_id
			,line_num
			,text)
		VALUES
			(v_file_id
			,v_line_num
			,v_message);
	END LOOP;

	UTL_FILE.fclose(file => v_file);

	o_file_id	:=	v_file_id;
	o_status	:=	glb_text_output_util.NO_ERROR;

	COMMIT;
EXCEPTION
	WHEN value_error THEN
		ROLLBACK;
		o_status	:=	glb_text_output_util.C_ERROR || 'Can not read file ,Line too long to store ,Input File Path : '||v_file_loc||v_file_name;
	WHEN utl_file.invalid_filehandle THEN
		ROLLBACK;
		o_status	:=	glb_text_output_util.C_ERROR || 'Can not read file ,Invalid file handle ,Input File Path : '||v_file_loc||v_file_name;
	WHEN utl_file.invalid_operation THEN
		ROLLBACK;
		o_status	:=	glb_text_output_util.C_ERROR || 'Can not read file ,Invaild operation ,Input File Path : '||v_file_loc||v_file_name;
	WHEN utl_file.read_error THEN
		ROLLBACK;
		o_status	:=	glb_text_output_util.C_ERROR || 'Can not read file ,Invalid file path ,Input File Path : '||v_file_loc||v_file_name;
	WHEN utl_file.charsetmismatch THEN
		ROLLBACK;
		o_status	:=	glb_text_output_util.C_ERROR || 'Can not read file ,Characterset missmatched ,Input File Path : '||v_file_loc||v_file_name;
END read_file;
----------------------
PROCEDURE read_file_utf8
(i_file_loc			IN	VARCHAR2
,i_file_name		IN	VARCHAR2
,i_cre_by			IN	VARCHAR2
,i_file_inf_code	IN	VARCHAR2
,i_program_name		IN	VARCHAR2
,i_dup_file_allow	IN	BOOLEAN	:=	FALSE
,o_file_id			OUT	NUMBER
,o_status			OUT	VARCHAR2)
IS
PRAGMA AUTONOMOUS_TRANSACTION;
	v_file			UTL_FILE.FILE_TYPE;
	v_file_name		VARCHAR2(1000);
	v_file_loc		VARCHAR2(1000);
	v_message		VARCHAR2(4000);
	v_file_id		NUMBER;
	v_line_num		NUMBER	:=	0;
BEGIN
	v_message := NULL;

	v_file_loc	:=	NVL(i_file_loc, v_file_loc);
	v_file_name	:=	NVL(i_file_name, v_file_name);
	
	IF NOT i_dup_file_allow THEN
		FOR rec IN (SELECT	*
					FROM	inbound_text_file
					WHERE	file_name		=	v_file_name
					AND		file_directory	=	v_file_loc
					AND		program_name	=	i_program_name
					AND		NVL(file_inf_flag,'N')	<>	'E')
		LOOP
			o_status	:=	glb_text_output_util.C_ERROR || 'File name & path are duplicate with non-error file ,Input File Path : '||v_file_loc||' - '||v_file_name;
			RETURN;
			EXIT;	-- process once
		END LOOP;
	END IF;

	v_file	:=	UTL_FILE.fopen_nchar(location	=>	v_file_loc
									,filename	=>	v_file_name
									,open_mode	=>	'r'
									,max_linesize=> 32767);

-- Make sure file_id must not duplicate
	LOOP
		SELECT	inbound_text_file_s.NEXTVAL
		INTO	v_file_id
		FROM	dual;
		FOR rec IN (SELECT * FROM inbound_text_file WHERE file_id = v_file_id)
		LOOP
			v_file_id	:=	NULL;
		END LOOP;
		IF v_file_id IS NOT NULL THEN
			INSERT INTO inbound_text_file
				(file_id
				,file_name
				,file_directory
				,program_name
				,file_inf_flag
				,file_inf_code
				,cre_by
				,cre_date)
			VALUES
				(v_file_id
				,v_file_name
				,v_file_loc
				,i_program_name
				,'N'
				,i_file_inf_code
				,i_cre_by
				,SYSDATE);
			EXIT;
		END IF;
	END LOOP;

	LOOP
		BEGIN
			UTL_FILE.get_line_nchar(file	=>	v_file, buffer	=>	v_message);
		EXCEPTION
			WHEN NO_DATA_FOUND THEN	-- End of file
				EXIT;	-- End loop
		END;
		v_line_num	:=	v_line_num + 1;
		INSERT INTO inbound_text_file_line
			(file_id
			,line_num
			,text)
		VALUES
			(v_file_id
			,v_line_num
			,v_message);
	END LOOP;

	UTL_FILE.fclose(file => v_file);

	o_file_id	:=	v_file_id;
	o_status	:=	glb_text_output_util.NO_ERROR;

	COMMIT;
EXCEPTION
	WHEN value_error THEN
		ROLLBACK;
		o_status	:=	glb_text_output_util.C_ERROR || 'Can not read file ,Line too long to store ,Input File Path : '||v_file_loc||v_file_name;
	WHEN utl_file.invalid_filehandle THEN
		ROLLBACK;
		o_status	:=	glb_text_output_util.C_ERROR || 'Can not read file ,Invalid file handle ,Input File Path : '||v_file_loc||v_file_name;
	WHEN utl_file.invalid_operation THEN
		ROLLBACK;
		o_status	:=	glb_text_output_util.C_ERROR || 'Can not read file ,Invaild operation ,Input File Path : '||v_file_loc||v_file_name;
	WHEN utl_file.read_error THEN
		ROLLBACK;
		o_status	:=	glb_text_output_util.C_ERROR || 'Can not read file ,Invalid file path ,Input File Path : '||v_file_loc||v_file_name;
	WHEN utl_file.charsetmismatch THEN
		ROLLBACK;
		o_status	:=	glb_text_output_util.C_ERROR || 'Can not read file ,Characterset missmatched ,Input File Path : '||v_file_loc||v_file_name;
END read_file_utf8;
-----------------------
END GLB_TEXT_INBOUND_UTIL;
/
