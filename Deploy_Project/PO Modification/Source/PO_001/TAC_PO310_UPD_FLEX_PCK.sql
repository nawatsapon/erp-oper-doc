CREATE OR REPLACE PACKAGE TAC_PO310_UPD_FLEX_PCK IS

-- @'D:\GoodJob\iCE\dTAC\PO_001\TAC_PO310_UPD_FLEX_PCK.sql'
-- dTAC: PO Cross currency program (After PO_AUTOCREATE_DOC)
-- Update: 21 June 2017
---------------------------------------------------------------------------------------
   PROCEDURE update_felx (
      itemtype    IN              VARCHAR2,
      itemkey     IN              VARCHAR2,
      actid       IN              NUMBER,
      funcmode    IN              VARCHAR2,
      resultout   OUT NOCOPY      VARCHAR2
   );
---------------------------------------------------------------------------------------
END TAC_PO310_UPD_FLEX_PCK;
/


CREATE OR REPLACE PACKAGE BODY TAC_PO310_UPD_FLEX_PCK IS
-- @'D:\GoodJob\iCE\dTAC\PO_001\TAC_PO310_UPD_FLEX_PCK.sql'
-- dTAC: PO Cross currency program (After PO_AUTOCREATE_DOC)
-- Update: 21 June 2017
---------------------------------------------------------------------------------------
   PROCEDURE update_felx (
      itemtype    IN              VARCHAR2,
      itemkey     IN              VARCHAR2,
      actid       IN              NUMBER,
      funcmode    IN              VARCHAR2,
      resultout   OUT NOCOPY      VARCHAR2
   )
   IS
      x_progress             VARCHAR2 (300);
      x_doc_id               NUMBER                     := NULL;
      x_doc_num              po_headers.segment1%TYPE   := NULL;
      x_preparer_id          NUMBER                     := NULL;
      x_doc_type             VARCHAR2 (25)              := NULL;
      x_doc_subtype          VARCHAR2 (25)              := NULL;
      x_doc_type_to_create   VARCHAR2 (25);
      x_faxnum               VARCHAR2 (30)              := NULL;
      x_emailaddress         VARCHAR2 (2000)            := NULL;
      x_default_method       VARCHAR2 (25)              := NULL;
      xfrom_header_id        NUMBER;
      
	  xattribute1			VARCHAR2 (150);
      xattribute2			VARCHAR2 (150);
      xattribute3			VARCHAR2 (150);
      xattribute4			VARCHAR2 (150);

      xattribute9            VARCHAR2 (150);
      xattribute10           VARCHAR2 (150);
      xattribute12           VARCHAR2 (150);
      xattribute13           VARCHAR2 (150);
      xattribute14           VARCHAR2 (150);
   BEGIN
      x_doc_type_to_create :=
         po_wf_util_pkg.getitemattrtext (itemtype      => itemtype,
                                         itemkey       => itemkey,
                                         aname         => 'DOC_TYPE_TO_CREATE'
                                        );

      IF (x_doc_type_to_create = 'RELEASE') THEN
         x_doc_type := 'RELEASE';
         x_doc_subtype := 'BLANKET';
      ELSE
         x_doc_type := 'PO';
         x_doc_subtype := 'STANDARD';
      END IF;

      x_preparer_id :=
         po_wf_util_pkg.getitemattrnumber (itemtype      => itemtype,
                                           itemkey       => itemkey,
                                           aname         => 'SUGGESTED_BUYER_ID'
                                          );
      x_doc_id :=
         po_wf_util_pkg.getitemattrnumber (itemtype      => itemtype,
                                           itemkey       => itemkey,
                                           aname         => 'AUTOCREATED_DOC_ID'
                                          );
      po_vendor_sites_sv.get_transmission_defaults
                                        (p_document_id           => x_doc_id,
                                         p_document_type         => x_doc_type,
                                         p_document_subtype      => x_doc_subtype,
                                         p_preparer_id           => x_preparer_id,
                                         x_default_method        => x_default_method,
                                         x_email_address         => x_emailaddress,
                                         x_fax_number            => x_faxnum,
                                         x_document_num          => x_doc_num
                                        );

      BEGIN

         SELECT MAX (from_header_id)
           INTO xfrom_header_id
           FROM po_lines_all
          WHERE po_header_id = x_doc_id;
      
	  EXCEPTION
         WHEN NO_DATA_FOUND THEN
            xfrom_header_id := 0;

      END;

      xattribute9 := '';
      xattribute10 := '';
      xattribute12 := '';
      xattribute13 := '';
      xattribute14 := '';

      xattribute1 := '';
      xattribute2 := '';
      xattribute3 := '';
      xattribute4 := '';

      BEGIN

         SELECT attribute9, attribute10, attribute12, attribute13, attribute14,
				attribute1, attribute2, attribute3, attribute4
           INTO xattribute9, xattribute10, xattribute12, xattribute13, xattribute14,
				xattribute1, xattribute2, xattribute3, xattribute4
           FROM po_headers_all
          WHERE po_header_id = xfrom_header_id;

      EXCEPTION

         WHEN NO_DATA_FOUND THEN
            xattribute9 := '';
            xattribute10 := '';
            xattribute12 := '';
            xattribute13 := '';
            xattribute14 := '';

			xattribute1 := '';
			xattribute2 := '';
			xattribute3 := '';
			xattribute4 := '';

      END;

      UPDATE po_headers_all
         SET attribute_category = 'PO',
             attribute1 = xattribute1,
             attribute2 = xattribute2,
             attribute3 = xattribute3,
             attribute4 = xattribute4,
             attribute9 = xattribute9,
             attribute10 = xattribute10,
             attribute12 = xattribute12,
             attribute13 = xattribute13,
             attribute14 = xattribute14
       WHERE po_header_id = x_doc_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         wf_core.CONTEXT ('po_autocreate_doc', 'UPDATE_FELX', x_progress);
         RAISE;
   END update_felx;
---------------------------------------------------------------------------------------
END TAC_PO310_UPD_FLEX_PCK;
/
