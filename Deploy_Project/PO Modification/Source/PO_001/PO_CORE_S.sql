CREATE OR REPLACE PACKAGE PO_CORE_S AUTHID CURRENT_USER AS
-- $Header: POXCOC1S.pls 115.20 2004/07/20 10:17:57 arudas ship $
-- $Header: POXCOC1B.pls 115.43.11510.2 2004/11/25 02:03:52 smeghani ship $
-- @'D:\GoodJob\iCE\dTAC\PO_001\PO_CORE_S.sql'
-- dTAC: PO Cross currency program (After PO_AUTOCREATE_DOC)
-- Update: 20 June 2017


--------------------------------------------------------------------------------
-- Public exceptions
--------------------------------------------------------------------------------

g_INVALID_CALL_EXC               EXCEPTION;

-----------------------------------------------------------------------------
-- Public variables
-----------------------------------------------------------------------------


-- Document types

g_doc_type_REQUISITION           CONSTANT
   PO_DOCUMENT_TYPES.document_type_code%TYPE
   := 'REQUISITION'
   ;
g_doc_type_PO                    CONSTANT
   PO_DOCUMENT_TYPES.document_type_code%TYPE
   := 'PO'
   ;
g_doc_type_PA                    CONSTANT
   PO_DOCUMENT_TYPES.document_type_code%TYPE
   := 'PA'
   ;
g_doc_type_RELEASE               CONSTANT
   PO_DOCUMENT_TYPES.document_type_code%TYPE
   := 'RELEASE'
   ;
-- For is_encumbrance_on:
g_doc_type_ANY                   CONSTANT
   PO_DOCUMENT_TYPES.document_type_code%TYPE
   := 'ANY'
   ;


-- Document levels

g_doc_level_HEADER               CONSTANT
   VARCHAR2(30)
   := 'HEADER'
   ;
g_doc_level_LINE                 CONSTANT
   VARCHAR2(30)
   := 'LINE'
   ;
g_doc_level_SHIPMENT             CONSTANT
   VARCHAR2(30)
   := 'SHIPMENT'
   ;
g_doc_level_DISTRIBUTION         CONSTANT
   VARCHAR2(30)
   := 'DISTRIBUTION'
   ;


-- Shipment types

g_ship_type_STANDARD             CONSTANT
   PO_LINE_LOCATIONS_ALL.shipment_type%TYPE
   := 'STANDARD'
   ;
g_ship_type_PLANNED              CONSTANT
   PO_LINE_LOCATIONS_ALL.shipment_type%TYPE
   := 'PLANNED'
   ;
g_ship_type_SCHEDULED            CONSTANT
   PO_LINE_LOCATIONS_ALL.shipment_type%TYPE
   := 'SCHEDULED'
   ;
g_ship_type_BLANKET               CONSTANT
   PO_LINE_LOCATIONS_ALL.shipment_type%TYPE
   := 'BLANKET'
   ;


-- Distribution types

g_dist_type_STANDARD             CONSTANT
   VARCHAR2(25)
   := 'STANDARD'
   ;
g_dist_type_PLANNED              CONSTANT
   VARCHAR2(25)
   := 'PLANNED'
   ;
g_dist_type_SCHEDULED            CONSTANT
   VARCHAR2(25)
   := 'SCHEDULED'
   ;
g_dist_type_BLANKET              CONSTANT
   VARCHAR2(25)
   := 'BLANKET'
   ;
g_dist_type_AGREEMENT            CONSTANT
   VARCHAR2(25)
   := 'AGREEMENT'
   ;


-- closed codes

g_clsd_FINALLY_CLOSED            CONSTANT
   PO_HEADERS_ALL.closed_code%TYPE
   := 'FINALLY CLOSED'
   ;
g_clsd_OPEN                      CONSTANT
   PO_HEADERS_ALL.closed_code%TYPE
   := 'OPEN'
   ;

-- Common parameter values:

g_parameter_YES CONSTANT VARCHAR2(1) := 'Y';
g_parameter_NO  CONSTANT VARCHAR2(1) := 'N';

-----------------------------------------------------------------------------
-- Public procedures
-----------------------------------------------------------------------------


  FUNCTION  get_ussgl_option     RETURN VARCHAR2;

  FUNCTION  get_gl_set_of_bks_id RETURN VARCHAR2;

/* ===========================================================================
  FUNCTION get_conversion_rate (
		x_from_currency		VARCHAR2,
		x_to_currency		VARCHAR2,
		x_conversion_date	DATE,
		x_conversion_type	VARCHAR2 DEFAULT NULL ) RETURN NUMBER;

  DESCRIPTION    : Returns the rate between the two currencies for a
                   given conversion date and conversion type.
  CLIENT/SERVER  : SERVER

  LIBRARY NAME   :

  OWNER          : GKELLNER

  PARAMETERS     :   x_set_of_books_id          Set of Books you are in
                     x_from_currency		From currency
                     x_conversion_date	        Conversion date
                     x_conversion_type	        Conversion type

  RETURN         :   Rate                       The conversion rate between
                                                the two currencies

  NOTES          : We need this cover on top of gl_currency_api.get_rate
                   so that we can handle the gl_currency_api.no_rate and
                   no_data_found exception properly

=========================================================================== */
  FUNCTION get_conversion_rate (
		x_set_of_books_id	NUMBER,
		x_from_currency		VARCHAR2,
		x_conversion_date	DATE,
		x_conversion_type	VARCHAR2 DEFAULT NULL) RETURN NUMBER;

--  PRAGMA   RESTRICT_REFERENCES(get_conversion_rate,WNDS,WNPS,RNPS);


  PROCEDURE get_displayed_value (x_lookup_type	IN  VARCHAR2,
				 x_lookup_code 	IN  VARCHAR2,
				 x_disp_value	OUT NOCOPY VARCHAR2,
				 x_description	OUT NOCOPY VARCHAR2,
				 x_validate	IN  BOOLEAN);

  /* Created by Raj Bhakta 10/30/96 */

  PROCEDURE validate_lookup_info(
            p_lookup_rec IN OUT NOCOPY RCV_SHIPMENT_HEADER_SV.LookupRecType);

  PROCEDURE get_displayed_value (x_lookup_type       IN  VARCHAR2,
                                 x_lookup_code       IN  VARCHAR2,
			         x_disp_value	     OUT NOCOPY VARCHAR2,
			         x_description       OUT NOCOPY VARCHAR2);


  PROCEDURE get_displayed_value (x_lookup_type       IN  VARCHAR2,
                                 x_lookup_code       IN  VARCHAR2,
			         x_disp_value	     OUT NOCOPY VARCHAR2);


  PROCEDURE get_org_sob (x_org_id    OUT NOCOPY NUMBER,
                         x_org_name  OUT NOCOPY VARCHAR2,
                         x_sob_id    OUT NOCOPY NUMBER) ;

  PROCEDURE get_po_parameters (  x_currency_code                 OUT NOCOPY VARCHAR2,
                                 x_coa_id                        OUT NOCOPY NUMBER,
                                 x_po_encumberance_flag          OUT NOCOPY VARCHAR2,
                                 x_req_encumberance_flag         OUT NOCOPY VARCHAR2,
                                 x_sob_id                        OUT NOCOPY NUMBER,
                                 x_ship_to_location_id           OUT NOCOPY NUMBER,
                                 x_bill_to_location_id           OUT NOCOPY NUMBER,
                                 x_fob_lookup_code               OUT NOCOPY VARCHAR2,
                                 x_freight_terms_lookup_code     OUT NOCOPY VARCHAR2,
                                 x_terms_id                      OUT NOCOPY NUMBER,
                                 x_default_rate_type             OUT NOCOPY VARCHAR2,
                                 x_taxable_flag                  OUT NOCOPY VARCHAR2,
                                 x_receiving_flag                OUT NOCOPY VARCHAR2,
                                 x_enforce_buyer_name_flag       OUT NOCOPY VARCHAR2,
                                 x_enforce_buyer_auth_flag       OUT NOCOPY VARCHAR2,
                                 x_line_type_id                  OUT NOCOPY NUMBER,
                                 x_manual_po_num_type            OUT NOCOPY VARCHAR2,
                                 x_po_num_code                   OUT NOCOPY VARCHAR2,
                                 x_price_lookup_code             OUT NOCOPY VARCHAR2,
                                 x_invoice_close_tolerance       OUT NOCOPY NUMBER,
                                 x_receive_close_tolerance       OUT NOCOPY NUMBER,
                                 x_security_structure_id         OUT NOCOPY NUMBER,
                                 x_expense_accrual_code          OUT NOCOPY VARCHAR2,
                                 x_inv_org_id                    OUT NOCOPY NUMBER,
                                 x_rev_sort_ordering             OUT NOCOPY NUMBER,
                                 x_min_rel_amount                OUT NOCOPY NUMBER,
                                 x_notify_blanket_flag           OUT NOCOPY VARCHAR2,
                                 x_budgetary_control_flag        OUT NOCOPY VARCHAR2,
                                 x_user_defined_req_num_code     OUT NOCOPY VARCHAR2,
                                 x_rfq_required_flag             OUT NOCOPY VARCHAR2,
                                 x_manual_req_num_type           OUT NOCOPY VARCHAR2,
                                 x_enforce_full_lot_qty          OUT NOCOPY VARCHAR2,
                                 x_disposition_warning_flag      OUT NOCOPY VARCHAR2,
                                 x_reserve_at_completion_flag    OUT NOCOPY VARCHAR2,
                                 x_user_defined_rcpt_num_code    OUT NOCOPY VARCHAR2,
                                 x_manual_rcpt_num_type          OUT NOCOPY VARCHAR2,
			         x_use_positions_flag		 OUT NOCOPY VARCHAR2,
			         x_default_quote_warning_delay   OUT NOCOPY NUMBER,
		  	         x_inspection_required_flag      OUT NOCOPY VARCHAR2,
		  	         x_user_defined_quote_num_code   OUT NOCOPY VARCHAR2,
		  	         x_manual_quote_num_type	 OUT NOCOPY VARCHAR2,
		  	         x_user_defined_rfq_num_code     OUT NOCOPY VARCHAR2,
		  	         x_manual_rfq_num_type		 OUT NOCOPY VARCHAR2,
				 x_ship_via_lookup_code	         OUT NOCOPY VARCHAR2,
				 x_qty_rcv_tolerance		 OUT NOCOPY NUMBER);


  PROCEDURE get_item_category_structure ( x_category_set_id OUT NOCOPY NUMBER,
                                          x_structure_id    OUT NOCOPY NUMBER ) ;

  FUNCTION get_product_install_status (x_product_name IN VARCHAR2) RETURN VARCHAR2 ;

  PROCEDURE get_global_values(x_userid        OUT NOCOPY number,
                              x_logonid       OUT NOCOPY number,
                              x_last_upd_date OUT NOCOPY date,
                              x_current_date  OUT NOCOPY date ) ;

  PROCEDURE GET_PERIOD_NAME (x_sob_id   IN NUMBER,
                             x_period  OUT NOCOPY VARCHAR2,
                             x_gl_date OUT NOCOPY DATE );

/*===========================================================================
  FUNCTION NAME:	get_total

  DESCRIPTION:          Calculates the total of an object

  PARAMETERS:

  Parameter	         IN/OUT	Datatype   Description
  -------------          ------ ---------- ----------------------------
  x_object_type		  IN    VARCHAR2   Object Type
                                           'H' - for PO Header
                                           'L' - for PO Line
                                           'B' - for PO Blanket
                                           'P' - for Po Planned
                                           'E' - for Requisition Header
                                           'I' - for Requisition Line
                                           'C' - for Contract
                                           'R' - for Release

  x_object_id    	  IN    NUMBER     Id of the object to be
                                           totalled

  x_base_cur_result	  IN    BOOLEAN    Result in Base Currency

  RETURN VALUE:	   Returns total of the object

  DESIGN REFERENCES:

  ALGORITHM:

  NOTES:

  OPEN ISSUES:

  CLOSED ISSUES:

  CHANGE HISTORY:
===========================================================================*/
--IF this function is changed, please investigate the impact on get_archive_total, get_archive_total_for_any_rev as well.--<CONTERMS FPJ>
FUNCTION  GET_TOTAL (x_object_type     IN VARCHAR2,
                     x_object_id       IN NUMBER,
                     x_base_cur_result IN BOOLEAN) RETURN NUMBER;

--pragma restrict_references (get_total, WNDS);

FUNCTION  get_total (x_object_type     IN VARCHAR2,
                     x_object_id       IN NUMBER) RETURN NUMBER;

--pragma restrict_references (get_total, WNDS);

--<CONTERMS FPJ START>
--FUNCTION to get archive total amount when the document is Standard Purchase order
--IF GET_TOTAL is changed, please investigate the impact on this function as well.
--Please check package bosy for more detailed comments
FUNCTION  get_archive_total (p_object_id       IN NUMBER,
                             p_doc_type        IN VARCHAR2,
                             p_doc_subtype     IN VARCHAR2,
                             p_base_cur_result IN VARCHAR2 DEFAULT 'N') RETURN NUMBER;
--<CONTERMS FPJ END>


--<POC FPJ START>

--FUNCTION to get archive total amount for a specified revision when the document is Standard Purchase order
--IF GET_TOTAL/GET_ARCHIVE_TOTAL is changed, please investigate the impact on this function as well.
--Name: GET_ARCHIVE_TOTAL_FOR_ANY_REV
--Pre-reqs:
--  None
--Modifies:
--  None.
--Locks:
--  None.
--Function:
--   Returns total amount for any specified revision for Standard Purchase order
--Parameters:
--IN:
--  p_object_id
--     PO header id
--  p_object_type
--  'H' for Standard
--  'L' for Standard PO Line
--  'S' for Shipment Line
--  'R' for Release
--  p_doc_type
--     The main doc type for PO. Valid values are 'PO', 'RELEASE'
--  p_doc_subtype
--     The lookup code of the document. Valid values are 'STANDARD', 'BLANKET'
--  p_doc_revision
--     The Revision of the PO header
--  p_base_cur_result
--     Whether result should be returned in base currency or transaction currency
--      Valid Values are
--      'Y'- Return result in Base/Functional Currency for the org
--      'N'- Return result in Transaction currency in po
--       Default 'N'
--Testing:
--  None
--End of Comments
---------------------------------------------------------------------------

FUNCTION  get_archive_total_for_any_rev (p_object_id       IN NUMBER,
                             p_object_type     IN VARCHAR2,
                             p_doc_type        IN VARCHAR2,
                             p_doc_subtype     IN VARCHAR2,
                             p_doc_revision    IN NUMBER,
                             p_base_cur_result IN VARCHAR2)
RETURN NUMBER;


--<POC FPJ END>



/*===================================================================================

    FUNCTION:    get_ga_amount_released                <GA FPI>

    DESCRIPTION: Gets the total Amount Released for a particular Global Agreement.
                 That is, sum up the total for all uncancelled Standard PO lines
                 which reference that Global Agreement.

===================================================================================*/
FUNCTION get_ga_amount_released
(
    p_po_header_id             PO_HEADERS_ALL.po_header_id%TYPE,
    p_convert_to_base          BOOLEAN := FALSE
)
RETURN NUMBER;


/*===================================================================================

    FUNCTION:    get_ga_line_amount_released                <GA FPI>

    DESCRIPTION: Gets the total Amount Released for a Global Agreement line.
                 That is, sum up the total for all uncancelled Standard PO lines
                 which reference that Global Agreement line.

===================================================================================*/
PROCEDURE get_ga_line_amount_released
(
    p_po_line_id             IN       PO_LINES_ALL.po_line_id%TYPE,
    p_po_header_id           IN       PO_HEADERS_ALL.po_header_id%TYPE,
    x_quantity_released      OUT NOCOPY      NUMBER,
    x_amount_released        OUT NOCOPY      NUMBER
);

/*===================================================================================

    FUNCTION:    GET_RELEASE_LINE_TOTAL                Bug#3771735

    DESCRIPTION: Function is being added to get the correct cumulative Amount of all
		 shipments of  a  Release which correspond to the same line of a BPA.
		 The function will be used during the PDF generation of a Incomplete
		 Blanket Release.

===================================================================================*/

FUNCTION  GET_RELEASE_LINE_TOTAL
(
	p_line_id      IN PO_LINES_ALL.PO_LINE_ID%TYPE ,
	p_release_id   IN PO_RELEASES_ALL.PO_RELEASE_ID%TYPE
) RETURN NUMBER;


-------------------------------------------
-- Document id helper procedures
-------------------------------------------


PROCEDURE get_document_ids(
   p_doc_type                       IN             VARCHAR2
,  p_doc_level                      IN             VARCHAR2
,  p_doc_level_id_tbl               IN             po_tbl_number
,  x_doc_id_tbl                     OUT NOCOPY     po_tbl_number
);


PROCEDURE get_line_ids(
   p_doc_type                       IN             VARCHAR2
,  p_doc_level                      IN             VARCHAR2
,  p_doc_level_id_tbl               IN             po_tbl_number
,  x_line_id_tbl                    OUT NOCOPY     po_tbl_number
);


PROCEDURE get_line_location_ids(
   p_doc_type                       IN             VARCHAR2
,  p_doc_level                      IN             VARCHAR2
,  p_doc_level_id_tbl               IN             po_tbl_number
,  x_line_location_id_tbl           OUT NOCOPY     po_tbl_number
);


PROCEDURE get_distribution_ids(
   p_doc_type                       IN             VARCHAR2
,  p_doc_level                      IN             VARCHAR2
,  p_doc_level_id_tbl               IN             po_tbl_number
,  x_distribution_id_tbl            OUT NOCOPY     po_tbl_number
);


FUNCTION is_encumbrance_on(
   p_doc_type                       IN             VARCHAR2
,  p_org_id                         IN             NUMBER
)  RETURN BOOLEAN;


FUNCTION get_translated_text                                  -- <SERVICES FPJ>
(   p_message_name        IN    VARCHAR2
,   p_token1              IN    VARCHAR2 := NULL
,   p_value1              IN    VARCHAR2 := NULL
,   p_token2              IN    VARCHAR2 := NULL
,   p_value2              IN    VARCHAR2 := NULL
,   p_token3              IN    VARCHAR2 := NULL
,   p_value3              IN    VARCHAR2 := NULL
,   p_token4              IN    VARCHAR2 := NULL
,   p_value4              IN    VARCHAR2 := NULL
,   p_token5              IN    VARCHAR2 := NULL
,   p_value5              IN    VARCHAR2 := NULL
) RETURN VARCHAR2;

--<Shared Proc FPJ START>
FUNCTION Check_Doc_Number_Unique(p_Segment1 In VARCHAR2,
                                  p_org_id IN VARCHAR2,
                                  p_Type_lookup_code IN VARCHAR2)
RETURN BOOLEAN;

PROCEDURE check_inv_org_in_sob
(
    x_return_status OUT NOCOPY VARCHAR2,
    p_inv_org_id    IN  NUMBER,
    p_sob_id        IN  NUMBER,
    x_in_sob        OUT NOCOPY BOOLEAN
);

PROCEDURE get_inv_org_ou_id
(
    x_return_status OUT NOCOPY VARCHAR2,
    p_inv_org_id    IN  NUMBER,
    x_ou_id         OUT NOCOPY NUMBER
);

PROCEDURE get_inv_org_sob_id
(
    x_return_status OUT NOCOPY VARCHAR2,
    p_inv_org_id    IN  NUMBER,
    x_sob_id        OUT NOCOPY NUMBER
);

PROCEDURE get_inv_org_info
(
    x_return_status         OUT NOCOPY VARCHAR2,
    p_inv_org_id            IN  NUMBER,
    x_business_group_id     OUT NOCOPY NUMBER,
    x_set_of_books_id       OUT NOCOPY NUMBER,
    x_chart_of_accounts_id  OUT NOCOPY NUMBER,
    x_operating_unit_id     OUT NOCOPY NUMBER,
    x_legal_entity_id       OUT NOCOPY NUMBER
);

--<Shared Proc FPJ END>

PROCEDURE should_display_reserved(
   p_doc_type                       IN             VARCHAR2
,  p_doc_level                      IN             VARCHAR2
,  p_doc_level_id                   IN             NUMBER
,  x_display_reserved_flag          OUT NOCOPY     VARCHAR2
);

PROCEDURE is_fully_reserved(
   p_doc_type                       IN             VARCHAR2
,  p_doc_level                      IN             VARCHAR2
,  p_doc_level_id                   IN             NUMBER
,  x_fully_reserved_flag            OUT NOCOPY     VARCHAR2
);

PROCEDURE are_any_dists_reserved(
   p_doc_type                       IN             VARCHAR2
,  p_doc_level                      IN             VARCHAR2
,  p_doc_level_id                   IN             NUMBER
,  x_some_dists_reserved_flag       OUT NOCOPY     VARCHAR2
);

PROCEDURE get_reserved_lookup(
   x_displayed_field                  OUT NOCOPY     VARCHAR2
);

-- Bug 3373453 START
PROCEDURE validate_yes_no_param (
  x_return_status   OUT NOCOPY VARCHAR2,
  p_parameter_name  IN VARCHAR2,
  p_parameter_value IN VARCHAR2
);
-- Bug 3373453 END

FUNCTION get_session_gt_nextval
RETURN NUMBER
;

END PO_CORE_S;
/


CREATE OR REPLACE PACKAGE BODY PO_CORE_S AS
-- $Header: POXCOC1B.pls 115.43.11510.2 2004/11/25 02:03:52 smeghani ship $
-- @'D:\GoodJob\iCE\dTAC\PO_001\PO_CORE_S.sql'
-- dTAC: PO Cross currency program (After PO_AUTOCREATE_DOC)
-- Update: 20 June 2017


-----------------------------------------------------------------------------
-- Declare private package variables.
-----------------------------------------------------------------------------

-- Debugging

g_pkg_name                       CONSTANT
   VARCHAR2(30)
   := 'PO_CORE_S'
   ;
g_log_head                       CONSTANT
   VARCHAR2(50)
   := 'po.plsql.' || g_pkg_name || '.'
   ;

g_debug_stmt                     CONSTANT
   BOOLEAN
   := PO_DEBUG.is_debug_stmt_on
   ;
g_debug_unexp                    CONSTANT
   BOOLEAN
   := PO_DEBUG.is_debug_unexp_on
   ;


-----------------------------------------------------------------------------
-- Declare private package types.
-----------------------------------------------------------------------------


-- Bug 3292870
TYPE g_rowid_char_tbl IS TABLE OF VARCHAR2(18);



-----------------------------------------------------------------------------
-- Define procedures.
-----------------------------------------------------------------------------

PROCEDURE get_open_encumbrance_stats(
   p_doc_type                       IN             VARCHAR2
,  p_doc_level                      IN             VARCHAR2
,  p_doc_level_id                   IN             NUMBER
,  x_reserved_count                 OUT NOCOPY     NUMBER
,  x_unreserved_count               OUT NOCOPY     NUMBER
,  x_prevented_count                OUT NOCOPY     NUMBER
);

-- <GC FPJ START>
-- Prototype of get_gc_amount_released

FUNCTION get_gc_amount_released
(
    p_po_header_id         IN NUMBER,
    p_convert_to_base      IN BOOLEAN := FALSE
) RETURN NUMBER;

-- <GC FPJ END>


/* ===========================================================================

  FUNCTION NAME : get_ussgl_option

  DESCRIPTION   :

  CLIENT/SERVER : SERVER

  LIBRARY NAME  :

  OWNER         : SUBHAJIT PURKAYASTHA

  PARAMETERS    :

  ALGORITHM     : Use fnd_profile.get function to retreive the value

  NOTES         :  Add sqlcode param in  the call to po_message_c.sql_error
                   - SI 04/08

=========================================================================== */
FUNCTION get_ussgl_option RETURN VARCHAR2 is

  x_progress      VARCHAR2(3) := NULL;
  x_option_value  VARCHAR2(1);
begin
  x_progress := 10;

  fnd_profile.get('USSGL_OPTION',x_option_value);

  RETURN(x_option_value);

  EXCEPTION
  WHEN OTHERS THEN
     po_message_s.sql_error('get_ussgl_option', x_progress, sqlcode);
  RAISE;

end get_ussgl_option;

/* ===========================================================================

  FUNCTION NAME : get_gl_set_of_bks_id

  DESCRIPTION   :

  CLIENT/SERVER : SERVER

  LIBRARY NAME  :

  OWNER         : SUBHAJIT PURKAYASTHA

  PARAMETERS    :

  ALGORITHM     : Use fnd_profile.get function to retreive the value

  NOTES         :

=========================================================================== */
FUNCTION get_gl_set_of_bks_id RETURN VARCHAR2 is

  x_progress      VARCHAR2(3) := NULL;
  x_option_value  VARCHAR2(3);
begin
  x_progress := 10;

  fnd_profile.get('GL_SET_OF_BKS_ID',x_option_value);

  RETURN(x_option_value);

  EXCEPTION
  WHEN OTHERS THEN
     po_message_s.sql_error('get_gl_set_of_bks_id', x_progress, sqlcode);
  RAISE;

end get_gl_set_of_bks_id;

/* ===========================================================================
  FUNCTION get_conversion_rate (
		x_set_of_books_id       NUMBER,
		x_from_currency		VARCHAR2,
		x_conversion_date	DATE,
		x_conversion_type	VARCHAR2 DEFAULT NULL ) RETURN NUMBER;

  DESCRIPTION    : Returns the rate between the two currencies for a
                   given conversion date and conversion type.
  CLIENT/SERVER  : SERVER

  LIBRARY NAME   :

  OWNER          : GKELLNER

  PARAMETERS     :   x_set_of_books_id		Set of Books you are in
                     x_from_currency		From currency
                     x_conversion_date	        Conversion date
                     x_conversion_type	        Conversion type

  RETURN         :   Rate                       The conversion rate between
                                                the two currencies

  NOTES          : We need this cover on top of gl_currency_api.get_rate
                   so that we can handle the gl_currency_api.no_rate and
                   no_data_found exception properly

=========================================================================== */
  FUNCTION get_conversion_rate (
		x_set_of_books_id       NUMBER,
		x_from_currency		VARCHAR2,
		x_conversion_date	DATE,
		x_conversion_type	VARCHAR2 DEFAULT NULL )

  RETURN NUMBER IS

  x_conversion_rate NUMBER := 0;
  x_progress  VARCHAR2(3) := '000';

  BEGIN

      x_conversion_rate := gl_currency_api.get_rate (
          x_set_of_books_id,
          x_from_currency  ,
	  x_conversion_date,
	  x_conversion_type);

      -- <2694908>: Truncate rate value (as done in PO_CURRENCY_SV.get_rate( ))
      x_conversion_rate := round(x_conversion_rate, 15);

      return (x_conversion_rate);

   EXCEPTION
   WHEN gl_currency_api.no_rate THEN
       return(NULL);
   WHEN no_data_found THEN
       return(NULL);
   WHEN OTHERS THEN
       RAISE;

  END get_conversion_rate;

/* ===========================================================================
  PROCEDURE NAME : get_org_sob (x_org_id    OUT NOCOPY NUMBER,
                                x_org_name  OUT NOCOPY VARCHAR2,
                                x_sob_id    OUT NOCOPY NUMBER)

  DESCRIPTION    :

  CLIENT/SERVER  : SERVER

  LIBRARY NAME   :

  OWNER          : SUBHAJIT PURKAYASTHA

  PARAMETERS     : x_org_id   - Return's the Id of the organization
                   x_org_name - Return's the name of the organization
                   x_sob_id   - Return's the SOB id

  ALGORITHM      : AOL function fnd_profile.get('MFG_ORGANIZATION_ID') returns
                   the default id of the org.
                   If id <> 0 then
                     retreive organization name and sob id from
                     org_organization_definitions table based on org_id
                   else
                     retreive purchasing organization from
                     financials_system_parameters and gl_sets_of_books table

  NOTES          :

=========================================================================== */

PROCEDURE get_org_sob (x_org_id    OUT NOCOPY NUMBER,
                       x_org_name  OUT NOCOPY VARCHAR2,
                       x_sob_id    OUT NOCOPY NUMBER) is
    x_progress  VARCHAR2(3) := NULL;
    org_id      NUMBER;

/** <UTF8 FPI> **/
/** tpoon 9/27/2002 **/
/** Changed org_name to use %TYPE **/
--    org_name    VARCHAR2(60);
    org_name    hr_all_organization_units.name%TYPE;

    sob_id      NUMBER;
begin
  x_progress := 10;
  fnd_profile.get('MFG_ORGANIZATION_ID',org_id);
  if org_id <> 0 then

    x_progress := 20;
    select  organization_name,
            set_of_books_id
    into    org_name,
            sob_id
    FROM   org_organization_definitions
    WHERE  organization_id = org_id ;

  else
    --Get purchasing organization
    x_progress := 30;
    SELECT  fsp.inventory_organization_id,
            fsp.set_of_books_id,
            sob.name
    INTO    org_id,
            sob_id,
            org_name
    FROM    financials_system_parameters fsp,
            gl_sets_of_books sob
    WHERE   fsp.set_of_books_id = sob.set_of_books_id;

  end if;
  x_progress := 40;
  --Associate each of the OUT NOCOPY variable with local variable

  x_org_id   := org_id;
  x_org_name := org_name;
  x_sob_id   := sob_id;

  EXCEPTION
  WHEN OTHERS THEN
    po_message_s.sql_error('get_org_sob', x_progress, sqlcode);
    RAISE;
  end get_org_sob;

/* ===========================================================================
  PROCEDURE NAME: get_po_parameters

  DESCRIPTION   :

  CLIENT/SERVER : SERVER

  LIBRARY NAME  :

  OWNER         : SUBHAJIT PURKAYASTHA

  PARAMETERS    : x_currency_code
                  x_coa_id
                  x_po_encumberance_flag
                  x_req_encumberance_flag
                  x_sob_id
                  x_ship_to_location_id
                  x_bill_to_location_id
                  x_fob_lookup_code
                  x_freight_terms_lookup_code
                  x_terms_id
                  x_default_rate_type
                  x_taxable_flag
                  x_receiving_flag
                  x_enforce_buyer_name_flag
                  x_enforce_buyer_auth_flag
                  x_line_type_id
                  x_manual_po_num_type
                  x_po_num_code
                  x_price_lookup_code
                  x_invoice_close_tolerance
                  x_receive_close_tolerance
                  x_security_structure_id
                  x_expense_accrual_code
                  x_inv_org_id
                  x_rev_sort_ordering
                  x_min_rel_amount
                  x_notify_blanket_flag
                  x_budgetary_control_flag
                  x_user_defined_req_num_code
                  x_rfq_required_flag
                  x_manual_req_num_type
                  x_enforce_full_lot_qty
                  x_disposition_warning_flag
                  x_reserve_at_completion_flag
                  x_user_defined_rcpt_num_code
                  x_manual_rcpt_num_type
		  x_use_positions_flag
		  x_default_quote_warning_delay
		  x_inspection_required_flag
		  x_user_defined_quote_num_code
		  x_manual_quote_num_type
		  x_user_defined_rfq_num_code
		  x_manual_rfq_num_type
		  x_ship_via_lookup_code
		  x_qty_rcv_tolerance


  ALGORITHM     : Retreive po parameters from
                  financials_system_parameters,gl_sets_of_books,
                  po_system_parameters and rcv_parameters tables.

  NOTES         :

=========================================================================== */
PROCEDURE get_po_parameters (  x_currency_code                 OUT NOCOPY VARCHAR2,
                               x_coa_id                        OUT NOCOPY NUMBER,
                               x_po_encumberance_flag          OUT NOCOPY VARCHAR2,
                               x_req_encumberance_flag         OUT NOCOPY VARCHAR2,
                               x_sob_id                        OUT NOCOPY NUMBER,
                               x_ship_to_location_id           OUT NOCOPY NUMBER,
                               x_bill_to_location_id           OUT NOCOPY NUMBER,
                               x_fob_lookup_code               OUT NOCOPY VARCHAR2,
                               x_freight_terms_lookup_code     OUT NOCOPY VARCHAR2,
                               x_terms_id                      OUT NOCOPY NUMBER,
                               x_default_rate_type             OUT NOCOPY VARCHAR2,
                               x_taxable_flag                  OUT NOCOPY VARCHAR2,
                               x_receiving_flag                OUT NOCOPY VARCHAR2,
                               x_enforce_buyer_name_flag       OUT NOCOPY VARCHAR2,
                               x_enforce_buyer_auth_flag       OUT NOCOPY VARCHAR2,
                               x_line_type_id                  OUT NOCOPY NUMBER,
                               x_manual_po_num_type            OUT NOCOPY VARCHAR2,
                               x_po_num_code                   OUT NOCOPY VARCHAR2,
                               x_price_lookup_code             OUT NOCOPY VARCHAR2,
                               x_invoice_close_tolerance       OUT NOCOPY NUMBER,
                               x_receive_close_tolerance       OUT NOCOPY NUMBER,
                               x_security_structure_id         OUT NOCOPY NUMBER,
                               x_expense_accrual_code          OUT NOCOPY VARCHAR2,
                               x_inv_org_id                    OUT NOCOPY NUMBER,
                               x_rev_sort_ordering             OUT NOCOPY NUMBER,
                               x_min_rel_amount                OUT NOCOPY NUMBER,
                               x_notify_blanket_flag           OUT NOCOPY VARCHAR2,
                               x_budgetary_control_flag        OUT NOCOPY VARCHAR2,
                               x_user_defined_req_num_code     OUT NOCOPY VARCHAR2,
                               x_rfq_required_flag             OUT NOCOPY VARCHAR2,
                               x_manual_req_num_type           OUT NOCOPY VARCHAR2,
                               x_enforce_full_lot_qty          OUT NOCOPY VARCHAR2,
                               x_disposition_warning_flag      OUT NOCOPY VARCHAR2,
                               x_reserve_at_completion_flag    OUT NOCOPY VARCHAR2,
                               x_user_defined_rcpt_num_code    OUT NOCOPY VARCHAR2,
                               x_manual_rcpt_num_type          OUT NOCOPY VARCHAR2,
			       x_use_positions_flag	       OUT NOCOPY VARCHAR2,
			       x_default_quote_warning_delay   OUT NOCOPY NUMBER,
		  	       x_inspection_required_flag      OUT NOCOPY VARCHAR2,
		  	       x_user_defined_quote_num_code   OUT NOCOPY VARCHAR2,
		  	       x_manual_quote_num_type	       OUT NOCOPY VARCHAR2,
		  	       x_user_defined_rfq_num_code     OUT NOCOPY VARCHAR2,
		  	       x_manual_rfq_num_type	       OUT NOCOPY VARCHAR2,
		  	       x_ship_via_lookup_code	       OUT NOCOPY VARCHAR2,
		  	       x_qty_rcv_tolerance	       OUT NOCOPY NUMBER
                              ) is

  x_progress     VARCHAR2(3) := NULL;
begin
  x_progress := 10;

  SELECT  sob.currency_code,
          sob.chart_of_accounts_id,
          nvl(fsp.purch_encumbrance_flag,'N'),
          nvl(fsp.req_encumbrance_flag,'N'),
          sob.set_of_books_id,
          fsp.ship_to_location_id,
          fsp.bill_to_location_id,
          fsp.fob_lookup_code,
          fsp.freight_terms_lookup_code,
          fsp.terms_id,
          psp.default_rate_type,
	  --togeorge 07/03/2001
	  --Bug# 1839659
	  --We are no more using this flag from psp.
          --psp.taxable_flag,
	  null,
	  --
          psp.receiving_flag,
          nvl(psp.enforce_buyer_name_flag, 'N'),
          nvl(psp.enforce_buyer_authority_flag,'N'),
          psp.line_type_id,
          psp.manual_po_num_type,
          psp.user_defined_po_num_code,
          psp.price_type_lookup_code,
          psp.invoice_close_tolerance,
          psp.receive_close_tolerance,
          psp.security_position_structure_id,
          psp.expense_accrual_code,
          fsp.inventory_organization_id,
          fsp.revision_sort_ordering,
          psp.min_release_amount,
          nvl(psp.notify_if_blanket_flag,'N'),
          nvl(sob.enable_budgetary_control_flag,'N'),
          psp.user_defined_req_num_code,
          nvl(psp.rfq_required_flag,'N'),
          psp.manual_req_num_type,
          psp.enforce_full_lot_quantities,
          psp.disposition_warning_flag,
          nvl(fsp.reserve_at_completion_flag,'N'),
          psp.user_defined_receipt_num_code,
          psp.manual_receipt_num_type,
	  fsp.use_positions_flag,
	  psp.default_quote_warning_delay,
	  psp.inspection_required_flag,
	  psp.user_defined_quote_num_code,
	  psp.manual_quote_num_type,
	  psp.user_defined_rfq_num_code,
	  psp.manual_rfq_num_type,
	  fsp.ship_via_lookup_code,
	  rcv.qty_rcv_tolerance
  INTO    x_currency_code       ,
          x_coa_id               ,
          x_po_encumberance_flag  ,
          x_req_encumberance_flag  ,
          x_sob_id                  ,
          x_ship_to_location_id      ,
          x_bill_to_location_id       ,
          x_fob_lookup_code         ,
          x_freight_terms_lookup_code,
          x_terms_id           ,
          x_default_rate_type   ,
          x_taxable_flag         ,
          x_receiving_flag        ,
          x_enforce_buyer_name_flag,
          x_enforce_buyer_auth_flag,
          x_line_type_id       ,
          x_manual_po_num_type  ,
          x_po_num_code          ,
          x_price_lookup_code     ,
          x_invoice_close_tolerance,
          x_receive_close_tolerance,
          x_security_structure_id,
          x_expense_accrual_code,
          x_inv_org_id      ,
          x_rev_sort_ordering,
          x_min_rel_amount    ,
          x_notify_blanket_flag,
          x_budgetary_control_flag,
          x_user_defined_req_num_code,
          x_rfq_required_flag,
          x_manual_req_num_type,
          x_enforce_full_lot_qty,
          x_disposition_warning_flag,
          x_reserve_at_completion_flag,
          x_user_defined_rcpt_num_code,
          x_manual_rcpt_num_type,
	  x_use_positions_flag,
	  x_default_quote_warning_delay,
	  x_inspection_required_flag,
	  x_user_defined_quote_num_code,
	  x_manual_quote_num_type,
	  x_user_defined_rfq_num_code,
	  x_manual_rfq_num_type,
	  x_ship_via_lookup_code,
	  x_qty_rcv_tolerance
  FROM    financials_system_parameters fsp,
          gl_sets_of_books sob,
          po_system_parameters psp,
	  rcv_parameters  rcv
  WHERE   fsp.set_of_books_id = sob.set_of_books_id
  AND     rcv.organization_id (+) = fsp.inventory_organization_id;

  EXCEPTION
  WHEN OTHERS THEN
    po_message_s.sql_error('get_po_parameters', x_progress, sqlcode);
    RAISE;

end get_po_parameters;

/* ===========================================================================
  PROCEDURE NAME: get_item_category_structure(x_category_set_id OUT NOCOPY NUMBER,
                                              x_structure_id    OUT NOCOPY NUMBER)

  DESCRIPTION   :

  CLIENT/SERVER : SERVER

  LIBRARY NAME  :

  OWNER         : SUBHAJIT PURKAYASTHA

  PARAMETERS    : Category_set_id NUMBER
                  structure_id    NUMBER

  ALGORITHM     : Retreive category_set_id and structure_id from
                  mtl_default_sets_view for functional_area_id = 2

  NOTES         :

=========================================================================== */
PROCEDURE get_item_category_structure (  x_category_set_id OUT NOCOPY NUMBER,
                                         x_structure_id    OUT NOCOPY NUMBER ) is
    x_progress  VARCHAR2(3) := NULL;
begin
  x_progress := 10;
  SELECT mdsv.category_set_id,
         mdsv.structure_id
  INTO   x_category_set_id,
         x_structure_id
  FROM   mtl_default_sets_view mdsv
  WHERE  mdsv.functional_area_id = 2;

  EXCEPTION
  WHEN OTHERS THEN
    po_message_s.sql_error('get_item_category_structure', x_progress, sqlcode);
    RAISE;

end get_item_category_structure;

/* ===========================================================================
  FUNCTION NAME : get_product_install_status (x_product_name IN VARCHAR2)
                                                RETURN VARCHAR2

  DESCRIPTION   : Returns the product's installation status

  CLIENT/SERVER : SERVER

  LIBRARY NAME  :

  OWNER         : SUBHAJIT PURKAYASTHA

  PARAMETERS    : x_product_name - Name of the product
                  For eg - 'INV','PO','ENG'

  ALGORITHM     : Use fnd_installation.get function to retreive
                  the status of product installation.
                  Function expects product id to be passed
                  Product Id will be derived from FND_APPLICATION table
                  Product       Product Id
                  --------      -----------
                  INV           401
                  PO            201

  NOTES         : valid installation status:
                  I - Product is installed
                  S - Product is partially installed
                  N - Product is not installed
                  L - Product is a local (custom) application


=========================================================================== */

FUNCTION get_product_install_status ( x_product_name IN VARCHAR2) RETURN VARCHAR2 IS
  x_progress     VARCHAR2(3) := NULL;
  x_app_id       NUMBER;
  x_install      BOOLEAN;
  x_status       VARCHAR2(1);
  x_org          VARCHAR2(1);
  x_temp_product_name varchar2(10);
begin
  --Retreive product id from fnd_application based on product name
  x_progress := 10;

  select application_id
  into   x_app_id
  from   fnd_application
  where application_short_name = x_product_name ;

  --get product installation status
  x_progress := 20;
  x_install := fnd_installation.get(x_app_id,x_app_id,x_status,x_org);

  if x_product_name in ('OE', 'ONT') then

	if Oe_install.get_active_product() in ('OE', 'ONT') then
		x_status := 'I';
	else
		x_status := 'N';
	end if;
  end if;

  RETURN(x_status);

  EXCEPTION
    WHEN NO_DATA_FOUND then
      null;
      RETURN(null);
    WHEN OTHERS THEN
    po_message_s.sql_error('get_product_install_status', x_progress, sqlcode);
      RAISE;

end get_product_install_status;

/* ===========================================================================
  PROCEDURE NAME: get_global_values(x_userid        OUT NOCOPY number,
                                    x_logonid       OUT NOCOPY number,
                                    x_last_upd_date OUT NOCOPY date,
                                    x_current_date  OUT NOCOPY date )

  DESCRIPTION   :

  CLIENT/SERVER : SERVER

  LIBRARY NAME  :

  OWNER         : SUBHAJIT PURKAYASTHA

  PARAMETERS    : x_userid        - Returns fnd user_id
                  x_logonid       - Return fnd logon id
                  x_last_upd_date - Returns sysdate
                  x_current_date  - Returns sysdate

  ALGORITHM     :

  NOTES         :

=========================================================================== */

PROCEDURE get_global_values(x_userid        OUT NOCOPY number,
                            x_logonid       OUT NOCOPY number,
                            x_last_upd_date OUT NOCOPY date,
                            x_current_date  OUT NOCOPY date ) is
  x_progress VARCHAR2(3) := NULL;
begin
  x_progress := 10;
  x_userid        := fnd_global.user_id;

  x_progress := 20;
  x_logonid       := fnd_global.login_id;

  x_progress := 30;
  x_last_upd_date := sysdate;

  x_progress := 40;
  x_current_date  := sysdate;

  EXCEPTION
  WHEN OTHERS THEN
    po_message_s.sql_error('get_global_values', x_progress, sqlcode);
    RAISE;

end get_global_values;

/*===========================================================================

  PROCEDURE NAME : GET_PERIOD_NAME

  DESCRIPTION   : Based on system date, function returns appropriate period
                  and gl_date

  CLIENT/SERVER : SERVER

  LIBRARY NAME  :

  OWNER         : SUBHAJIT PURKAYASTHA

  PARAMETERS    : sob_id      - Set_of_books_id
                  period_name - Period Name
                  gl_date     - GL Date
  ALGORITHM     :

  NOTES         :

===========================================================================*/
-------------------------------------------------------------------------------
--Start of Comments
--Name: get_period_name
--Pre-reqs:
--  None.
--Modifies:
--  None.
--Locks:
--  None.
--Function:
--  Retrieves the GL period name and date for SYSDATE,
--  if the date is in a usable period (valid for GL and PO).
--Parameters:
--IN:
--x_sob_id
--  Set of books.
--OUT:
--x_gl_period
--  The period name corresponding to SYSDATE.
--x_gl_date
--  SYSDATE.
--Notes:
--  This procedure was refactored in FPJ to call the more generalized
--  procedure get_period_info.  However, the parameter names were
--  not changed to meet standards, as that may have impacted calling code.
--Testing:
--
--End of Comments
-------------------------------------------------------------------------------
PROCEDURE get_period_name (
   x_sob_id                         IN             NUMBER
,  x_period                         OUT NOCOPY     VARCHAR2
,  x_gl_date                        OUT NOCOPY     DATE
)
IS


l_log_head     CONSTANT VARCHAR2(100) := g_log_head||'GET_PERIOD_NAME';
l_progress     VARCHAR2(3) := '000';

l_period_name_tbl       po_tbl_varchar30;
l_period_year_tbl       po_tbl_number;
l_period_num_tbl        po_tbl_number;
l_quarter_num_tbl       po_tbl_number;
l_invalid_period_flag   VARCHAR2(1);

BEGIN

IF g_debug_stmt THEN
   PO_DEBUG.debug_begin(l_log_head);
   PO_DEBUG.debug_var(l_log_head,l_progress,'x_sob_id',x_sob_id);
END IF;

l_progress := '010';

PO_PERIODS_SV.get_period_info(
   p_roll_logic => NULL
,  p_set_of_books_id => x_sob_id
,  p_date_tbl => po_tbl_date( SYSDATE )
,  x_period_name_tbl => l_period_name_tbl
,  x_period_year_tbl => l_period_year_tbl
,  x_period_num_tbl => l_period_num_tbl
,  x_quarter_num_tbl => l_quarter_num_tbl
,  x_invalid_period_flag => l_invalid_period_flag
);

l_progress := '020';

x_period := l_period_name_tbl(1);

IF (l_invalid_period_flag = FND_API.G_FALSE) THEN

   l_progress := '030';
   IF g_debug_stmt THEN
      PO_DEBUG.debug_stmt(l_log_head,l_progress,'valid period');
   END IF;

   x_gl_date := TRUNC(SYSDATE);

END IF;

l_progress := '900';

IF g_debug_stmt THEN
   PO_DEBUG.debug_var(l_log_head,l_progress,'x_period',x_period);
   PO_DEBUG.debug_var(l_log_head,l_progress,'x_gl_date',x_gl_date);
   PO_DEBUG.debug_end(l_log_head);
END IF;

EXCEPTION
WHEN OTHERS THEN
   IF g_debug_unexp THEN
      PO_DEBUG.debug_exc(l_log_head,l_progress);
   END IF;
   RAISE;

END get_period_name;




/* ===========================================================================
  PROCEDURE NAME: get_displayed_value(x_lookup_type       IN  VARCHAR2,
                                      x_lookup_code       IN  VARCHAR2,
				      x_disp_value	  OUT NOCOPY VARCHAR2,
				      x_description       OUT NOCOPY VARCHAR2,
				      x_validate  	  IN  BOOLEAN)

  DESCRIPTION   : Obtain the  displayed field and description. This procedure
		  also performs active date validation.

  CLIENT/SERVER : SERVER

  LIBRARY NAME  :

  OWNER         : Ramana Mulpury

  PARAMETERS    : lookup_type		VARCHAR2
                  lookup_type		VARCHAR2
 		  displayed_field 	VARCHAR2
		  descriptioni    	VARCHAR2
		  validate		BOOLEAN

  ALGORITHM     : Get the displayed field and description from the
		  table po_lookup_codes. These values are validated
		  against the inactive date if x_validate is set to TRUE
		  No validation is performed if x_validate is set to
		  FALSE.
  NOTES         :

=========================================================================== */
PROCEDURE get_displayed_value (x_lookup_type       IN  VARCHAR2,
                               x_lookup_code       IN  VARCHAR2,
			       x_disp_value	   OUT NOCOPY VARCHAR2,
			       x_description       OUT NOCOPY VARCHAR2,
			       x_validate	   IN  BOOLEAN) IS
    x_progress  VARCHAR2(3) := NULL;
begin

  x_progress := 10;

  IF  (x_validate = TRUE) THEN

  SELECT plc.displayed_field,
         plc.description
  INTO   x_disp_value,
         x_description
  FROM   po_lookup_codes plc
  WHERE  plc.lookup_code = x_lookup_code
  AND    plc.lookup_type = x_lookup_type
  AND    sysdate < nvl(plc.inactive_date,sysdate + 1);

  ELSE
   get_displayed_value(x_lookup_type, x_lookup_code, x_disp_value, x_description);

  END IF;

  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    x_disp_value   := NULL;
    x_description  := NULL;

  WHEN OTHERS THEN
    po_message_s.sql_error('get_displayed_value', x_progress, sqlcode);
    RAISE;

end get_displayed_value;



/* ===========================================================================
  PROCEDURE NAME: get_displayed_value(x_lookup_type       IN  VARCHAR2,
                                      x_lookup_code       IN  VARCHAR2,
				      x_disp_value	  OUT NOCOPY VARCHAR2,
				      x_description       OUT NOCOPY VARCHAR2)

  DESCRIPTION   : Obtain the  displayed field and description

  CLIENT/SERVER : SERVER

  LIBRARY NAME  :

  OWNER         : Ramana Mulpury

  PARAMETERS    : lookup_type		VARCHAR2
                  lookup_type		VARCHAR2
 		  displayed_field 	VARCHAR2
		  descriptioni    	VARCHAR2

  ALGORITHM     : Get the displayed field and description from the
		  table po_lookup_codes.

  NOTES         :

=========================================================================== */
PROCEDURE get_displayed_value (x_lookup_type       IN  VARCHAR2,
                               x_lookup_code       IN  VARCHAR2,
			       x_disp_value	   OUT NOCOPY VARCHAR2,
			       x_description       OUT NOCOPY VARCHAR2) IS
    x_progress  VARCHAR2(3) := NULL;
begin

  x_progress := 10;

  SELECT plc.displayed_field,
         plc.description
  INTO   x_disp_value,
         x_description
  FROM   po_lookup_codes plc
  WHERE  plc.lookup_code = x_lookup_code
  AND    plc.lookup_type = x_lookup_type;

  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    x_disp_value   := NULL;
    x_description  := NULL;

  WHEN OTHERS THEN
    po_message_s.sql_error('get_displayed_value', x_progress, sqlcode);
    RAISE;

end get_displayed_value;


/* ===========================================================================
  PROCEDURE NAME: get_displayed_value(x_lookup_type       IN  VARCHAR2,
                                      x_lookup_code       IN  VARCHAR2,
				      x_disp_value	  OUT NOCOPY VARCHAR2)

  DESCRIPTION   : Obtain the  displayed field . This is an overloaded
		  procedure

  CLIENT/SERVER : SERVER

  LIBRARY NAME  :

  OWNER         : Ramana Mulpury

  PARAMETERS    : lookup_type   VARCHAR2
                  lookup_code   VARCHAR2
 		  displayed_field VARCHAR2


  ALGORITHM     : Get the displayed field from the table po_lookup_codes.

  NOTES         :

=========================================================================== */
PROCEDURE get_displayed_value (x_lookup_type       IN  VARCHAR2,
                               x_lookup_code       IN  VARCHAR2,
			       x_disp_value	   OUT NOCOPY VARCHAR2) IS

x_progress  VARCHAR2(3) := NULL;

begin

  x_progress := 10;

  SELECT plc.displayed_field
  INTO   x_disp_value
  FROM   po_lookup_codes plc
  WHERE  plc.lookup_code = x_lookup_code
  AND    plc.lookup_type = x_lookup_type;

  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    x_disp_value   := NULL;

  WHEN OTHERS THEN
    po_message_s.sql_error('get_displayed_value', x_progress, sqlcode);
    RAISE;

end get_displayed_value;

/*===========================================================================
  FUNCTION NAME:	get_total

===========================================================================*/
FUNCTION  get_total (x_object_type     IN VARCHAR2,
                     x_object_id       IN NUMBER) RETURN NUMBER IS
	x_total		NUMBER;
BEGIN
    x_total := get_total(x_object_type,
		  	 x_object_id,
		  	 NULL);
    return(x_total);
END;

/*===========================================================================
  FUNCTION NAME:	get_total

===========================================================================*/
--<CONTERMS FPJ START>
-- When making any change to this function please check if get_archive_total/get_archive_total_for_any_rev
-- also needs to be change correspondigly
--<CONTERMS FPJ END>
FUNCTION  get_total (x_object_type     IN VARCHAR2,
                     x_object_id       IN NUMBER,
                     x_base_cur_result IN BOOLEAN) RETURN NUMBER IS
  x_progress       VARCHAR2(3) := NULL;
  x_base_currency  VARCHAR2(16);
  x_po_currency    VARCHAR2(16);
  x_min_unit       NUMBER;
  x_base_min_unit  NUMBER;
  x_precision      INTEGER;
  x_base_precision INTEGER;
  x_result_fld     NUMBER;

BEGIN

  if (x_object_type in ('H','B') ) then

    if x_base_cur_result then
      /* Result should be returned in base currency. Get the currency code
         of the PO and the base currency code
      */
      x_progress := 10;
      po_core_s2.get_po_currency (x_object_id,
                       x_base_currency,
                       x_po_currency );

      /* Chk if base_currency = po_currency */
      if x_base_currency <> x_po_currency then
        /* Get precision and minimum accountable unit of the PO CURRENCY */
        x_progress := 20;
        po_core_s2.get_currency_info (x_po_currency,
                           x_precision,
                           x_min_unit );

        /* Get precision and minimum accountable unit of the base CURRENCY */
        x_progress := 30;
        po_core_s2.get_currency_info (x_base_currency,
                           x_base_precision,
                           x_base_min_unit );



        if X_base_min_unit is null  then

          if X_min_unit is null then

            x_progress := 40;

/* 958792 kbenjami 8/25/99.  Proprogated fix from R11.
   849493 - SVAIDYAN: Do a sum(round()) instead of round(sum()) since what
                      we pass to GL is the round of individual dist. amounts
                      and the sum of these rounded values is what should be
                      displayed as the header total.
*/
            -- <SERVICES FPJ>
            -- For the new Services lines, quantity will be null.
            -- Hence, added a decode statement to use amount directly
            -- in the total amount calculation when quantity is null.
            --< Bug 3549096 > Use _ALL tables instead of org-striped views.
            SELECT nvl(sum(round(
                   round(
                       (decode(POD.quantity_ordered,
                               null,
                               (nvl(POD.amount_ordered, 0) -
                               nvl(POD.amount_cancelled, 0)),
                               ((nvl(POD.quantity_ordered, 0) -
                               nvl(POD.quantity_cancelled, 0)) *
                               nvl(PLL.price_override, 0))
                              )
                       * POD.rate
                       )
                   , X_precision) ,
                   X_base_precision)),0)
            INTO   X_result_fld
            FROM   PO_DISTRIBUTIONS_ALL POD, PO_LINE_LOCATIONS_ALL PLL
            WHERE  PLL.po_header_id = X_object_id
            AND    PLL.shipment_type in ('STANDARD','PLANNED','BLANKET')
            AND    PLL.line_location_id = POD.line_location_id;

          else
            x_progress := 42;

            -- <SERVICES FPJ>
            -- For the new Services lines, quantity will be null.
            -- Hence, added a decode statement to use amount directly
            -- in the total amount calculation when quantity is null.
            --< Bug 3549096 > Use _ALL tables instead of org-striped views.
            SELECT nvl(sum(round(
                   round(
                       decode(POD.quantity_ordered,
                              null,
                              (nvl(POD.amount_ordered, 0) -
                              nvl(POD.amount_cancelled, 0)),
                              ((nvl(POD.quantity_ordered, 0) -
                              nvl(POD.quantity_cancelled, 0)) *
                              nvl(PLL.price_override, 0))
                             )
                       * POD.rate / X_min_unit
                        )
                   * X_min_unit , X_base_precision)),0)
            INTO   X_result_fld
            FROM   PO_DISTRIBUTIONS_ALL POD, PO_LINE_LOCATIONS_ALL PLL
            WHERE  PLL.po_header_id = X_object_id
            AND    PLL.shipment_type in ('STANDARD','PLANNED','BLANKET')
            AND    PLL.line_location_id = POD.line_location_id;

          end if;

        else /* base_min_unit is NOT null */

          if X_min_unit is null then
            x_progress := 44;

            -- <SERVICES FPJ>
            -- For the new Services lines, quantity will be null.
            -- Hence, added a decode statement to use amount directly
            -- in the total amount calculation when quantity is null.
            --< Bug 3549096 > Use _ALL tables instead of org-striped views.
            SELECT nvl(sum(round( round(
                   decode(POD.quantity_ordered,
                          null,
                          (nvl(POD.amount_ordered, 0) -
                          nvl(POD.amount_cancelled, 0)),
                          (nvl(POD.quantity_ordered, 0) -
                          nvl(POD.quantity_cancelled, 0))
                          * nvl(PLL.price_override, 0)
                         )
                   * POD.rate , X_precision)
                   / X_base_min_unit ) * X_base_min_unit) ,0)
            INTO   X_result_fld
            FROM   PO_DISTRIBUTIONS_ALL POD, PO_LINE_LOCATIONS_ALL PLL
            WHERE  PLL.po_header_id = X_object_id
            AND    PLL.shipment_type in ('STANDARD','PLANNED','BLANKET')
            AND    PLL.line_location_id = POD.line_location_id;

          else
            x_progress := 46;

            -- <SERVICES FPJ>
            -- For the new Services lines, quantity will be null.
            -- Hence, added a decode statement to use amount directly
            -- in the total amount calculation when quantity is null.
            --< Bug 3549096 > Use _ALL tables instead of org-striped views.
            SELECT nvl(sum(round( round(
                   decode(POD.quantity_ordered,
                          null,
                          (nvl(POD.amount_ordered, 0) -
                          nvl(POD.amount_cancelled, 0)),
                          (nvl(POD.quantity_ordered, 0) -
                          nvl(POD.quantity_cancelled, 0))
                          * nvl(PLL.price_override, 0)
                         )
                   * POD.rate /
                   X_min_unit) * X_min_unit  / X_base_min_unit)
                   * X_base_min_unit) , 0)
            INTO   X_result_fld
            FROM   PO_DISTRIBUTIONS_ALL POD, PO_LINE_LOCATIONS_ALL PLL
            WHERE  PLL.po_header_id = X_object_id
            AND    PLL.shipment_type in ('STANDARD','PLANNED','BLANKET')
            AND    PLL.line_location_id = POD.line_location_id;

          end if;

        end if;

      end if;  /* x_base_currency <> x_po_currency */

    else

      /* if we donot want result converted to base currency or if
         the currencies are the same then do the check without
         rate conversion */

/* 958792 kbenjami 8/25/99.  Proprogated fix from R11.
   849493 - SVAIDYAN: Do a sum(round()) instead of round(sum()) since what
                      we pass to GL is the round of individual dist. amounts
                      and the sum of these rounded values is what should be
                      displayed as the header total.
*/

      x_progress := 50;

      --< Bug 3549096 > Use _ALL tables instead of org-striped views.
      SELECT c.minimum_accountable_unit,
             c.precision
      INTO   x_min_unit,
             x_precision
      FROM   FND_CURRENCIES C,
             PO_HEADERS_ALL     PH
      WHERE  PH.po_header_id  = x_object_id
      AND    C.currency_code  = PH.CURRENCY_CODE;

      if x_min_unit is null then
	x_progress := 53;

	-- <SERVICES FPJ>
        -- For the new Services lines, quantity will be null.
        -- Hence, added a decode statement to use amount directly
        -- in the total amount calculation when quantity is null.
        --< Bug 3549096 > Use _ALL tables instead of org-striped views.
        select sum(round(
               decode(pll.quantity,
                      null,
                      (pll.amount - nvl(pll.amount_cancelled,0)),
                      (pll.quantity - nvl(pll.quantity_cancelled,0))
                      * nvl(pll.price_override,0)
                     )
               ,x_precision))
        INTO   x_result_fld
        FROM   PO_LINE_LOCATIONS_ALL PLL
        WHERE  PLL.po_header_id   = x_object_id
        AND    PLL.shipment_type in ('STANDARD','PLANNED','BLANKET');

      else
		/* Bug 1111926: GMudgal 2/18/2000
		** Incorrect placement of brackets caused incorrect rounding
		** and consequently incorrect PO header totals
		*/
        x_progress := 56;

        -- <SERVICES FPJ>
        -- For the new Services lines, quantity will be null.
        -- Hence, added a decode statement to use amount directly
        -- in the total amount calculation when quantity is null.
        --< Bug 3549096 > Use _ALL tables instead of org-striped views.
        select sum(round(
               decode(pll.quantity,
                      null,
                      (pll.amount - nvl(pll.amount_cancelled, 0)),
                      (pll.quantity - nvl(pll.quantity_cancelled, 0))
                      * nvl(pll.price_override,0)
                     )
               / x_min_unit)
               * x_min_unit)
        INTO   x_result_fld
        FROM   PO_LINE_LOCATIONS_ALL PLL
        WHERE  PLL.po_header_id   = x_object_id
        AND    PLL.shipment_type in ('STANDARD','PLANNED','BLANKET');

      end if;

    end if;

    -- <GA FPI START>
    --

    -- <GC FPJ>
    -- Change x_object_type for GA from 'G' to 'GA'

    ELSIF ( x_object_type = 'GA' ) THEN                       -- Global Agreement

        x_result_fld := get_ga_amount_released( x_object_id, x_base_cur_result );
    --
    -- <GA FPI END>

   -- <GC FPJ START>
   ELSIF (x_object_type = 'GC') THEN                       -- Global Contract
       x_result_fld := get_gc_amount_released
                       (
                           p_po_header_id    => x_object_id,
                           p_convert_to_base => x_base_cur_result
                       );

   -- <GC FPJ END>

   elsif (x_object_type = 'P') then /* For PO Planned */

     if x_base_cur_result then

      /* Result should be returned in base currency. Get the currency code
         of the PO and the base currency code */

      x_progress := 60;
      po_core_s2.get_po_currency (x_object_id,
                       x_base_currency,
                       x_po_currency );

      /* Chk if base_currency = po_currency */
      if x_base_currency <> x_po_currency then
        /* Get precision and minimum accountable unit of the PO CURRENCY */
        x_progress := 70;
        po_core_s2.get_currency_info (x_po_currency,
                           x_precision,
                           x_min_unit );

        /* Get precision and minimum accountable unit of the base CURRENCY */
        x_progress := 80;
        po_core_s2.get_currency_info (x_base_currency,
                           x_base_precision,
                           x_base_min_unit );

/* iali - Bug 482497 - 05/09/97
   For Planned PO the PLL.shipment_type should be 'PLANNED' and not
   'SCHEDULED' as it was before. Adding both in the where clause by replacing
   eqality check with in clause.
*/
-- Bugs 482497 and 602664, lpo, 12/22/97
-- Actually, for planned PO, the shipment_type should remain to be 'SCHEDULED'.
-- This will calculate the total released amount. (a shipment type of 'PLANNED'
-- indicates the lines in the planned PO, therefore using IN ('PLANNED',
-- 'SCHEDULED') will calculate the total released amount plus the amount
-- agreed, which is not what we want.
-- Refer to POXBWN3B.pls for fix to bug 482497.

        if X_base_min_unit is null  then

          if X_min_unit is null then

            x_progress := 90;

/* 958792 kbenjami 8/25/99.  Proprogated fix from R11.
   849493 - SVAIDYAN: Do a sum(round()) instead of round(sum()) since what
                      we pass to GL is the round of individual dist. amounts
                      and the sum of these rounded values is what should be
                      displayed as the header total.
*/
            --< Bug 3549096 > Use _ALL tables instead of org-striped views.
            SELECT nvl(sum(round( round((nvl(POD.quantity_ordered, 0) -
                   nvl(POD.quantity_cancelled, 0)) *
                   nvl(PLL.price_override, 0) * POD.rate, X_precision) ,
                   X_base_precision)),0)
            INTO   X_result_fld
            FROM   PO_DISTRIBUTIONS_ALL POD, PO_LINE_LOCATIONS_ALL PLL
            WHERE  PLL.po_header_id     = X_object_id
-- Bugs 482497 and 602664, lpo, 12/22/97
            AND    PLL.shipment_type    = 'SCHEDULED'
-- End of fix. Bugs 482497 and 602664, lpo, 12/22/97
            AND    PLL.line_location_id = POD.line_location_id;

          else
            x_progress := 92;
            --< Bug 3549096 > Use _ALL tables instead of org-striped views.
            SELECT nvl(sum(round( round((nvl(POD.quantity_ordered, 0) -
                   nvl(POD.quantity_cancelled, 0)) *
                   nvl(PLL.price_override, 0) * POD.rate /
                   X_min_unit) * X_min_unit , X_base_precision)),0)
            INTO   X_result_fld
            FROM   PO_DISTRIBUTIONS_ALL POD, PO_LINE_LOCATIONS_ALL PLL
            WHERE  PLL.po_header_id     = X_object_id
-- Bugs 482497 and 602664, lpo, 12/22/97
            AND    PLL.shipment_type   	= 'SCHEDULED'
-- End of fix. Bugs 482497 and 602664, lpo, 12/22/97
            AND    PLL.line_location_id = POD.line_location_id;
          end if;

        else /* base_min_unit is NOT null */

          if X_min_unit is null then
            x_progress := 94;
            --< Bug 3549096 > Use _ALL tables instead of org-striped views.
            SELECT nvl(sum(round( round((nvl(POD.quantity_ordered, 0) -
                   nvl(POD.quantity_cancelled, 0)) *
                   nvl(PLL.price_override, 0) * POD.rate , X_precision)
                   / X_base_min_unit ) * X_base_min_unit) ,0)
            INTO   X_result_fld
            FROM   PO_DISTRIBUTIONS_ALL POD, PO_LINE_LOCATIONS_ALL PLL
            WHERE  PLL.po_header_id     = X_object_id
-- Bugs 482497 and 602664, lpo, 12/22/97
            AND    PLL.shipment_type    = 'SCHEDULED'
-- End of fix. Bugs 482497 and 602664, lpo, 12/22/97
            AND    PLL.line_location_id = POD.line_location_id;

          else
            x_progress := 96;
            --< Bug 3549096 > Use _ALL tables instead of org-striped views.
            SELECT nvl(sum(round( round((nvl(POD.quantity_ordered, 0) -
                                         nvl(POD.quantity_cancelled, 0)) *
                   nvl(PLL.price_override, 0) * POD.rate /
                   X_min_unit) * X_min_unit  / X_base_min_unit)
                   * X_base_min_unit) , 0)
            INTO   X_result_fld
            FROM   PO_DISTRIBUTIONS_ALL POD, PO_LINE_LOCATIONS_ALL PLL
            WHERE  PLL.po_header_id     = X_object_id
-- Bugs 482497 and 602664, lpo, 12/22/97
            AND    PLL.shipment_type    = 'SCHEDULED'
-- End of fix. Bugs 482497 and 602664, lpo, 12/22/97
            AND    PLL.line_location_id = POD.line_location_id;
          end if;

        end if;

      end if;  /* x_base_currency <> x_po_currency */

    else

      /* if we donot want result converted to base currency or if
         the currencies are the same then do the check without
         rate conversion */
      x_progress := 100;
      --< Bug 3549096 > Use _ALL tables instead of org-striped views.
      SELECT c.minimum_accountable_unit,
             c.precision
      INTO   x_min_unit,
             x_precision
      FROM   FND_CURRENCIES C,
             PO_HEADERS_ALL     PH
      WHERE  PH.po_header_id  = x_object_id
      AND    C.currency_code  = PH.CURRENCY_CODE;

/* 958792 kbenjami 8/25/99.  Proprogated fix from R11.
   849493 - SVAIDYAN: Do a sum(round()) instead of round(sum()) since what
                      we pass to GL is the round of individual dist. amounts
                      and the sum of these rounded values is what should be
                      displayed as the header total.
*/
      if x_min_unit is null then
        x_progress := 103;
        --< Bug 3549096 > Use _ALL tables instead of org-striped views.
        select sum(round((pll.quantity - nvl(pll.quantity_cancelled,0))*
                         nvl(pll.price_override,0),x_precision))
        INTO   x_result_fld
        FROM   PO_LINE_LOCATIONS_ALL PLL
        WHERE  PLL.po_header_id   = x_object_id
-- Bugs 482497 and 602664, lpo, 12/22/97
        AND    PLL.shipment_type  = 'SCHEDULED';
-- Bugs 482497 and 602664, lpo, 12/22/97
      else
		/* Bug 1111926: GMudgal 2/18/2000
		** Incorrect placement of brackets caused incorrect rounding
		** and consequently incorrect PO header totals
		*/
        x_progress := 106;
        --< Bug 3549096 > Use _ALL tables instead of org-striped views.
        select sum(round((pll.quantity -
                          nvl(pll.quantity_cancelled,0)) *
                          nvl(pll.price_override,0)/x_min_unit)*
                          x_min_unit)
        INTO   x_result_fld
        FROM   PO_LINE_LOCATIONS_ALL PLL
        WHERE  PLL.po_header_id   = x_object_id
-- Bugs 482497 and 602664, lpo, 12/22/97
        AND    PLL.shipment_type  = 'SCHEDULED';
-- End of fix. Bugs 482497 and 602664, lpo, 12/22/97
      end if;

    end if;

  elsif (x_object_type = 'E' ) then /* Requisition Header */
    x_progress := 110;
    po_core_s2.get_req_currency (x_object_id,
                      x_base_currency );

    x_progress := 120;
    po_core_s2.get_currency_info (x_base_currency,
                       x_base_precision,
                       x_base_min_unit );

    if x_base_min_unit is null then
      x_progress := 130;

/*    Bug No. 1431811 Changing the round of sum to sum of rounded totals

      round(sum((nvl(quantity,0) * nvl(unit_price,0))), x_base_precision)
*/
      -- <SERVICES FPJ>
      -- For the new Services lines, quantity will be null.
      -- Hence, added a decode statement to use amount directly
      -- in the total amount calculation when quantity is null.
      --< Bug 3549096 > Use _ALL tables instead of org-striped views.
      -- Bug 3877772, include cancelled lines with with delivered quantity
      select sum(round(
             decode(quantity,
                    null,
	            nvl(amount, 0),
                    ((nvl(quantity,0) - nvl(quantity_cancelled,0)) * nvl(unit_price,0))
                   )
	     , x_base_precision))
      INTO   x_result_fld
      FROM   PO_REQUISITION_LINES_ALL
      WHERE  requisition_header_id            = x_object_id
      -- AND    nvl(cancel_flag, 'N')            = 'N'
      AND    nvl(modified_by_agent_flag, 'N') = 'N';

    else
      x_progress := 135;

/*    Bug No. 1431811 Changing the round of sum to sum of rounded totals
      select
      round(sum((nvl(quantity,0) * nvl(unit_price,0)/x_base_min_unit)*
                 x_base_min_unit))
*/
      -- <SERVICES FPJ>
      -- For the new Services lines, quantity will be null.
      -- Hence, added a decode statement to use amount directly
      -- in the total amount calculation when quantity is null.
      --< Bug 3549096 > Use _ALL tables instead of org-striped views.
      -- Bug 3877772, include cancelled lines with with delivered quantity
      select sum(round(
             decode(quantity,
                    null,
                    nvl(amount, 0),
                    ((nvl(quantity,0) - nvl(quantity_cancelled,0)) * nvl(unit_price,0))
                   )
             /x_base_min_unit)*
             x_base_min_unit)
      INTO   x_result_fld
      FROM   PO_REQUISITION_LINES_ALL
      WHERE  requisition_header_id            = x_object_id
      -- AND    nvl(cancel_flag, 'N')            = 'N'
      AND    nvl(modified_by_agent_flag, 'N') = 'N';

    end if;

  elsif (x_object_type = 'I' ) then /* Requisition Line */

    x_progress := 140;
    po_core_s2.get_req_currency (x_object_id,
                      x_base_currency );

    x_progress := 150;
    po_core_s2.get_currency_info (x_base_currency,
                       x_base_precision,
                       x_base_min_unit );

    if x_base_min_unit is null then
      x_progress := 160;

/*    Bug No. 1431811 Changing the round of sum to sum of rounded totals

      select
      round(sum((nvl(quantity,0) * nvl(unit_price,0))), x_base_precision)
*/
      -- <SERVICES FPJ>
      -- For the new Services lines, quantity will be null.
      -- Hence, added a decode statement to use amount directly
      -- in the total amount calculation when quantity is null.
      --< Bug 3549096 > Use _ALL tables instead of org-striped views.
      -- Bug 3877772, include cancelled lines with with delivered quantity
      select sum(round(
	     decode(quantity,
                    null,
                    nvl(amount, 0),
                    ((nvl(quantity,0) - nvl(quantity_cancelled,0)) * nvl(unit_price,0))
                   )
             , x_base_precision))
      INTO   x_result_fld
      FROM   PO_REQUISITION_LINES_ALL
      WHERE  requisition_line_id              = x_object_id
      -- AND    nvl(cancel_flag, 'N')            = 'N'
      AND    nvl(modified_by_agent_flag, 'N') = 'N';

    else
      x_progress := 165;
      -- <SERVICES FPJ>
      -- For the new Services lines, quantity will be null.
      -- Hence, added a decode statement to use amount directly
      -- in the total amount calculation when quantity is null.
      --< Bug 3549096 > Use _ALL tables instead of org-striped views.
      -- Bug 3877772, include cancelled lines with with delivered quantity
      select round(sum((
             decode(quantity,
                    null,
                    nvl(amount, 0),
                    ((nvl(quantity,0) - nvl(quantity_cancelled,0)) * nvl(unit_price,0))
                   )
             /x_base_min_unit)*
             x_base_min_unit))
      INTO   x_result_fld
      FROM   PO_REQUISITION_LINES_ALL
      WHERE  requisition_line_id              = x_object_id
      -- AND    nvl(cancel_flag, 'N')            = 'N'
      AND    nvl(modified_by_agent_flag, 'N') = 'N';

    end if;
    x_progress := 160;

    elsif (x_object_type = 'J' ) then /* Requisition Distribution */

    x_progress := 162;
    po_core_s2.get_req_currency (x_object_id,
                      x_base_currency );

    x_progress := 164;
    po_core_s2.get_currency_info (x_base_currency,
                       x_base_precision,
                       x_base_min_unit );

    x_progress := 166;

    -- <SERVICES FPJ>
    -- Modified the SELECT statement to take account into Services
    -- lines. For the new Services lines, quantity will be null.
    -- Hence, added decode statements to use amount directly
    -- in the total amount calculation when quantity is null.
    --< Bug 3549096 > Use _ALL tables instead of org-striped views.
    SELECT
    sum( decode
       ( x_base_min_unit, NULL,
             decode(quantity, NULL,
                    round( nvl(PORD.req_line_amount, 0),
                           x_base_precision),
                    round( nvl(PORD.req_line_quantity, 0) *
                           nvl(PORL.unit_price, 0),
                           x_base_precision)
                    ),
             decode(quantity, NULL,
                    round((nvl(PORD.req_line_amount, 0) /
                           x_base_min_unit) *
                           x_base_min_unit),
                    round((nvl(PORD.req_line_quantity, 0) *
                           nvl(PORL.unit_price, 0) /
                           x_base_min_unit) *
                           x_base_min_unit)
                   )))
    INTO   x_result_fld
    FROM   PO_REQ_DISTRIBUTIONS_ALL PORD,
           PO_REQUISITION_LINES_ALL PORL
    WHERE  PORD.distribution_id             = x_object_id
    AND    PORD.requisition_line_id         = PORL.requisition_line_id;

  elsif (x_object_type = 'C' ) then /* Contract */

    x_progress := 170;
    --< Bug 3549096 > Use _ALL tables instead of org-striped views.
    SELECT c.minimum_accountable_unit,
           c.precision
    INTO   x_min_unit,
           x_precision
    FROM   FND_CURRENCIES C,
           PO_HEADERS_ALL     PH
    WHERE  PH.po_header_id  = x_object_id
    AND    C.currency_code  = PH.CURRENCY_CODE;

/* 716188 - SVAIDYAN : Changed the sql stmt to select only Standard and Planned
   POs that reference this contract and also to convert the amount into the
   Contract's currency. This is achieved by converting the PO amt first to the
   functional currency and then changing this to the Contract currency */

/* 716188 - Added an outer join on PO_DISTRIBUTIONS */
/* 866358 - BPESCHAN: Changed the sql stmt to select quantity_ordered and
   quantity_cancelled from PO_DISTRIBUTIONS instead of PO_LINE_LOCATIONS.
   This fix prevents incorrect calculation for amount release when more then
   one distribution exists. */

/* 958792 kbenjami 8/25/99.  Proprogated fix from R11.
   849493 - SVAIDYAN: Do a sum(round()) instead of round(sum()) since what
                      we pass to GL is the round of individual dist. amounts
                      and the sum of these rounded values is what should be
                      displayed as the header total.
*/
/*Bug3760487:Purchase Order form was displaying incorrect released
  amount for foreign currency contract when the PO currency is same
  as the contract currency and the rates were different.Added the decode
  to perform the currency conversion only when the currency code of
  PO and contract are different.
  Also removed the join to FND_CURRENCIES
*/
    if x_min_unit is null then
      x_progress := 172;
      --< Bug 3549096 > Use _ALL tables instead of org-striped views.
      SELECT
      nvl(sum(decode(PH.currency_code, PH1.currency_code,
                      round((nvl(POD.quantity_ordered,0) -
                         nvl(POD.quantity_cancelled,0))
                         * nvl(pll.price_override,0),x_precision),
                      round((nvl(POD.quantity_ordered,0) -
                         nvl(POD.quantity_cancelled,0))
                         * nvl(pll.price_override,0)
                         * nvl(POD.rate, nvl(PH1.rate,1))/nvl(PH.rate,1),x_precision)
              )),0)
      INTO   x_result_fld
      FROM   PO_DISTRIBUTIONS_ALL POD,
             PO_LINE_LOCATIONS_ALL PLL,
             PO_LINES_ALL PL,
             PO_HEADERS_ALL PH,
             PO_HEADERS_ALL PH1
             --,FND_CURRENCIES C
      WHERE  PH.po_header_id      = x_object_id
      AND    PH.po_header_id      = PL.contract_id           -- <GC FPJ>
      --AND    PH.currency_code     = C.currency_code
      AND    PL.po_line_id        = PLL.po_line_id
      AND    PLL.shipment_type in ('STANDARD','PLANNED')
      AND    POD.line_location_id(+) = PLL.line_location_id
      AND    PH1.po_header_id = PL.po_header_id;
    else

/* 958792 kbenjami 8/25/99.  Proprogated fix from R11.
   849493 - SVAIDYAN: Do a sum(round()) instead of round(sum()) since what
                      we pass to GL is the round of individual dist. amounts
                      and the sum of these rounded values is what should be
                      displayed as the header total.
*/
      x_progress := 174;
      --< Bug 3549096 > Use _ALL tables instead of org-striped views.
      SELECT
      nvl(sum(decode(PH.currency_code, PH1.currency_code,
                 round((nvl(POD.quantity_ordered,0) -
                    nvl(POD.quantity_cancelled,0))
                    * nvl(pll.price_override,0)/x_min_unit),
                 round((nvl(POD.quantity_ordered,0) -
                    nvl(POD.quantity_cancelled,0))
                    * nvl(pll.price_override,0)
                    * nvl(POD.rate, nvl(PH1.rate,1))/nvl(PH.rate,1)/x_min_unit))
              * x_min_unit),0)
      INTO   x_result_fld
      FROM   PO_DISTRIBUTIONS_ALL POD,
             PO_LINE_LOCATIONS_ALL PLL,
             PO_LINES_ALL PL,
             PO_HEADERS_ALL PH,
             PO_HEADERS_ALL PH1
             --,FND_CURRENCIES C
      WHERE  PH.po_header_id      = x_object_id
      AND    PH.po_header_id      = PL.contract_id         -- <GC FPJ>
      --AND    PH.currency_code     = C.currency_code
      AND    PL.po_line_id        = PLL.po_line_id
      AND    PLL.shipment_type in ('STANDARD','PLANNED')
      AND    POD.line_location_id(+) = PLL.line_location_id
      AND    PH1.po_header_id = PL.po_header_id;
    end if;


  elsif (x_object_type = 'R' ) then /* Release */

    if x_base_cur_result then
      x_progress := 180;
      --< Bug 3549096 > Use _ALL tables instead of org-striped views.
      SELECT GSB.currency_code,
             POH.currency_code
      INTO   x_base_currency,
             x_po_currency
      FROM   PO_HEADERS_ALL POH,
             FINANCIALS_SYSTEM_PARAMS_ALL FSP,
             GL_SETS_OF_BOOKS GSB,
             PO_RELEASES_ALL POR
      WHERE  POR.po_release_id   = x_object_id
      AND    POH.po_header_id    = POR.po_header_id
      AND    NVL(POR.org_id,-99) = NVL(FSP.org_id,-99)      --< Bug 3549096 >
      AND    FSP.set_of_books_id = GSB.set_of_books_id;

      if (x_base_currency <> x_po_currency) then
        /* Get precision and minimum accountable unit of the PO CURRENCY */
        x_progress := 190;
        po_core_s2.get_currency_info (x_po_currency,
                           x_precision,
                           x_min_unit );

        /* Get precision and minimum accountable unit of the base CURRENCY */
        x_progress := 200;
        po_core_s2.get_currency_info (x_base_currency,
                           x_base_precision,
                           x_base_min_unit );

        if x_base_min_unit is null then
          if x_min_unit is null then
            x_progress := 210;

/* 958792 kbenjami 8/25/99.  Proprogated fix from R11.
   849493 - SVAIDYAN: Do a sum(round()) instead of round(sum()) since what
                      we pass to GL is the round of individual dist. amounts
                      and the sum of these rounded values is what should be
                      displayed as the header total.
*/
            -- <SERVICES FPJ>
            -- For the new Services lines, quantity will be null.
            -- Hence, added a decode statement to use amount directly
            -- in the total amount calculation when quantity is null.
            --< Bug 3549096 > Use _ALL tables instead of org-striped views.
            select nvl(sum(round(round(
                   decode(POD.quantity_ordered,
                          null,
                          nvl(POD.amount_ordered, 0),
                          (nvl(POD.quantity_ordered,0) *
                          nvl(PLL.price_override,0))
                         )
                   * POD.rate
                   ,x_precision),x_base_precision)),0)
            INTO x_result_fld
            FROM PO_DISTRIBUTIONS_ALL POD, PO_LINE_LOCATIONS_ALL PLL
            WHERE PLL.po_release_id    = x_object_id
            AND   PLL.line_location_id = POD.line_location_id
            AND   PLL.shipment_type in ('SCHEDULED','BLANKET');

          else
            x_progress := 212;
            -- <SERVICES FPJ>
            -- For the new Services lines, quantity will be null.
            -- Hence, added a decode statement to use amount directly
            -- in the total amount calculation when quantity is null.
            --< Bug 3549096 > Use _ALL tables instead of org-striped views.
	    SELECT nvl(sum(round( round(
                   decode(POD.quantity_ordered,
                          null,
                          nvl(POD.amount_ordered, 0),
                          (nvl(POD.quantity_ordered, 0) *
                          nvl(PLL.price_override, 0))
                         )
                   * POD.rate /
                   X_min_unit) * X_min_unit , X_base_precision)),0)
            INTO x_result_fld
            FROM PO_DISTRIBUTIONS_ALL POD, PO_LINE_LOCATIONS_ALL PLL
            WHERE PLL.po_release_id    = x_object_id
            AND   PLL.line_location_id = POD.line_location_id
            AND   PLL.shipment_type in ('SCHEDULED','BLANKET');

          end if;
        else
          if X_min_unit is null then
            x_progress := 214;
            -- <SERVICES FPJ>
            -- For the new Services lines, quantity will be null.
            -- Hence, added a decode statement to use amount directly
            -- in the total amount calculation when quantity is null.
            --< Bug 3549096 > Use _ALL tables instead of org-striped views.
            SELECT nvl(sum(round( round(
                   decode(POD.quantity_ordered,
                          null,
                          nvl(POD.amount_ordered, 0),
                          (nvl(POD.quantity_ordered, 0) *
                          nvl(PLL.price_override, 0))
                         )
                   * POD.rate
                   , X_precision)
                   / X_base_min_unit ) * X_base_min_unit) ,0)
            INTO   X_result_fld
            FROM PO_DISTRIBUTIONS_ALL POD, PO_LINE_LOCATIONS_ALL PLL
            WHERE PLL.po_release_id    = x_object_id
            AND   PLL.line_location_id = POD.line_location_id
            AND   PLL.shipment_type in ('SCHEDULED','BLANKET');

          else
            x_progress := 216;
            -- <SERVICES FPJ>
            -- For the new Services lines, quantity will be null.
            -- Hence, added a decode statement to use amount directly
            -- in the total amount calculation when quantity is null.
            --< Bug 3549096 > Use _ALL tables instead of org-striped views.
	    SELECT nvl(sum(round( round(
                   decode(POD.quantity_ordered,
                          null,
                          nvl(POD.amount_ordered, 0),
                          (nvl(POD.quantity_ordered, 0) *
                          nvl(PLL.price_override, 0))
                         )
                   * POD.rate /
                   X_min_unit) * X_min_unit  / X_base_min_unit)
                   * X_base_min_unit) , 0)
            INTO   X_result_fld
            FROM PO_DISTRIBUTIONS_ALL POD, PO_LINE_LOCATIONS_ALL PLL
            WHERE PLL.po_release_id    = x_object_id
            AND   PLL.line_location_id = POD.line_location_id
            AND   PLL.shipment_type in ('SCHEDULED','BLANKET');

          end if;
        end if;

      end if;
    else
      x_progress := 220;
      --< Bug 3549096 > Use _ALL tables instead of org-striped views.
      SELECT c.minimum_accountable_unit,
             c.precision
      INTO   x_min_unit,
             x_precision
      FROM   FND_CURRENCIES C,
             PO_RELEASES_ALL    POR,
             PO_HEADERS_ALL     PH
      WHERE  POR.po_release_id = x_object_id
      AND    PH.po_header_id   = POR.PO_HEADER_ID
      AND    C.currency_code   = PH.CURRENCY_CODE;

      if x_min_unit is null then

/* 958792 kbenjami 8/25/99.  Proprogated fix from R11.
   849493 - SVAIDYAN: Do a sum(round()) instead of round(sum()) since what
                      we pass to GL is the round of individual dist. amounts
                      and the sum of these rounded values is what should be
                      displayed as the header total.
*/
        x_progress := 222;
        -- <SERVICES FPJ>
        -- For the new Services lines, quantity will be null.
        -- Hence, added a decode statement to use amount directly
        -- in the total amount calculation when quantity is null.
        --< Bug 3549096 > Use _ALL tables instead of org-striped views.
        select sum(round(
               decode(pll.quantity,
                      null,
                      (pll.amount - nvl(pll.amount_cancelled,0)),
                      ((pll.quantity - nvl(pll.quantity_cancelled,0)) *
                      nvl(pll.price_override,0))
                     )
               ,x_precision))
        INTO   x_result_fld
        FROM   PO_LINE_LOCATIONS_ALL PLL
        WHERE  PLL.po_release_id   = x_object_id
        AND    PLL.shipment_type  in ( 'SCHEDULED','BLANKET');

      else
		/* Bug 1111926: GMudgal 2/18/2000
		** Incorrect placement of brackets caused incorrect rounding
		** and consequently incorrect PO header totals
		*/
        x_progress := 224;

        -- <SERVICES FPJ>
        -- For the new Services lines, quantity will be null.
        -- Hence, added a decode statement to use amount directly
        -- in the total amount calculation when quantity is null.
        --< Bug 3549096 > Use _ALL tables instead of org-striped views.
        select sum(round(
               decode(pll.quantity,
                      null,
                     (pll.amount - nvl(pll.amount_cancelled,0)),
                     ((pll.quantity - nvl(pll.quantity_cancelled,0)) *
                     nvl(pll.price_override,0))
                     )
               /x_min_unit) *
               x_min_unit)
        INTO   x_result_fld
        FROM   PO_LINE_LOCATIONS_ALL PLL
        WHERE  PLL.po_release_id   = x_object_id
        AND    PLL.shipment_type  in ( 'SCHEDULED','BLANKET');

      end if;

    end if;

  elsif (x_object_type = 'L' ) then /* Po Line */
    x_progress := 230;
    --< Bug 3549096 > Use _ALL tables instead of org-striped views.
    SELECT sum(c.minimum_accountable_unit),
           sum(c.precision)
    INTO   x_min_unit,
           x_precision
    FROM   FND_CURRENCIES C,
           PO_HEADERS_ALL     PH,
           PO_LINES_ALL POL
    WHERE  POL.po_line_id   = x_object_id
    AND    PH.po_header_id  = POL.po_header_id
    AND    C.currency_code  = PH.CURRENCY_CODE;

    if x_min_unit is null then
      x_progress := 232;

/*    Bug No. 1431811 Changing the round of sum to sum of rounded totals
      select round(sum((pll.quantity - nvl(pll.quantity_cancelled,0))*
                       nvl(pll.price_override,0)),x_precision)
*/
/*    Bug No. 1849112 In the previous fix of 143811 by mistake x_precision
      was not used.
*/
      -- <SERVICES FPJ>
      -- For the new Services lines, quantity will be null.
      -- Hence, added a decode statement to use amount directly
      -- in the total amount calculation when quantity is null.
      --< Bug 3549096 > Use _ALL tables instead of org-striped views.
      select sum(round((
             decode(pll.quantity,
                    null,
                    (pll.amount - nvl(pll.amount_cancelled, 0)),
                    (pll.quantity - nvl(pll.quantity_cancelled,0))
                    * nvl(pll.price_override,0)
                   )
             ),x_precision))
      INTO   x_result_fld
      FROM   PO_LINE_LOCATIONS_ALL PLL
      WHERE  PLL.po_line_id   = x_object_id
      AND    PLL.shipment_type  in ( 'STANDARD','BLANKET','PLANNED');

    else
      x_progress := 234;

/*    Bug No. 1431811 Changing the round of sum to sum of rounded totals
*/
      -- <SERVICES FPJ>
      -- For the new Services lines, quantity will be null.
      -- Hence, added a decode statement to use amount directly
      -- in the total amount calculation when quantity is null.
      --< Bug 3549096 > Use _ALL tables instead of org-striped views.
      select sum(round((
             decode(pll.quantity,
                    null,
                    (pll.amount - nvl(pll.amount_cancelled,0)),
                    (pll.quantity - nvl(pll.quantity_cancelled,0))
                    * nvl(pll.price_override,0)
                   )
             / x_min_unit) * x_min_unit))
      INTO   x_result_fld
      FROM   PO_LINE_LOCATIONS_ALL PLL
      WHERE  PLL.po_line_id   = x_object_id
      AND    PLL.shipment_type  in ( 'STANDARD','BLANKET','PLANNED');

    end if;

  elsif (x_object_type = 'S' ) then /* PO Shipment */
    x_progress := 240;
    --< Bug 3549096 > Use _ALL tables instead of org-striped views.
    SELECT c.minimum_accountable_unit,
           c.precision
    INTO   x_min_unit,
           x_precision
    FROM   FND_CURRENCIES C,
           PO_HEADERS_ALL     PH,
           PO_LINE_LOCATIONS_ALL PLL
    WHERE  PLL.line_location_id   = x_object_id
    AND    PH.po_header_id        = PLL.po_header_id
    AND    C.currency_code        = PH.CURRENCY_CODE;

    if x_min_unit is null then
      x_progress := 242;

/*    Bug No. 1431811 Changing the round of sum to sum of rounded totals
      select round(sum((pll.quantity - nvl(pll.quantity_cancelled,0))*
                       nvl(pll.price_override,0)),x_precision)
*/
      -- <SERVICES FPJ>
      -- For the new Services lines, quantity will be null.
      -- Hence, added a decode statement to use amount directly
      -- in the total amount calculation when quantity is null.
      --< Bug 3549096 > Use _ALL tables instead of org-striped views.
      select sum(round((
             decode(pll.quantity,
                    null,
                    (pll.amount - nvl(pll.amount_cancelled,0)),
                    (pll.quantity - nvl(pll.quantity_cancelled,0))
                    * nvl(pll.price_override,0)
                   )
             ),x_precision))
      INTO   x_result_fld
      FROM   PO_LINE_LOCATIONS_ALL PLL
      WHERE  PLL.line_location_id   = x_object_id;

    else
      x_progress := 244;
      -- <SERVICES FPJ>
      -- For the new Services lines, quantity will be null.
      -- Hence, added a decode statement to use amount directly
      -- in the total amount calculation when quantity is null.
      --< Bug 3549096 > Use _ALL tables instead of org-striped views.
      select round(sum((
             decode(pll.quantity,
                    null,
                    (pll.amount - nvl(pll.amount_cancelled,0)),
                    (pll.quantity - nvl(pll.quantity_cancelled,0))
                    * nvl(pll.price_override,0)
                   )
             /x_min_unit) * x_min_unit))
      INTO   x_result_fld
      FROM   PO_LINE_LOCATIONS_ALL PLL
      WHERE  PLL.line_location_id   = x_object_id;

    end if;

  end if; /* x_object_type */

  /* If x_result_fld has a null value, return 0 as the total. */
  IF x_result_fld IS NULL THEN
	x_result_fld := 0;
  END IF;

  RETURN(x_result_fld);

  EXCEPTION
  WHEN OTHERS THEN
    RETURN(0);
    RAISE;

END get_total;

--<CONTERMS FPJ START>
-------------------------------------------------------------------------------
--Start of Comments
--Name: GET_ARCHIVE_TOTAL
--Pre-reqs:
--  None
--Modifies:
--  None.
--Locks:
--  None.
--Function:
--   Returns total amount for latest archived revision for Standard Purchase order
--Parameters:
--IN:
--  p_object_id
--     PO header id
--  p_doc_type
--     The main doc type for PO. Valid values are 'PO'
--  p_doc_subtype
--     The lookup code of the document. Valid values are 'STANDARD'
--  p_base_cur_result
--     Whether result should be returned in base currency or transaction currency
--      Valid Values are
--      'Y'- Return result in Base/Functional Currency for the org
--      'N'- Return result in Transaction currency in po
--       Default 'N'
--Testing:
--  None
--End of Comments
---------------------------------------------------------------------------
FUNCTION  get_archive_total (p_object_id       IN NUMBER,
                             p_doc_type        IN VARCHAR2,
                             p_doc_subtype     IN VARCHAR2,
                             p_base_cur_result IN VARCHAR2) RETURN NUMBER IS

  l_base_currency       PO_HEADERS_ALL.CURRENCY_CODE%TYPE;
  l_po_currency         PO_HEADERS_ALL.CURRENCY_CODE%TYPE;
  l_min_unit            FND_CURRENCIES.MINIMUM_ACCOUNTABLE_UNIT%TYPE;
  l_base_min_unit       FND_CURRENCIES.MINIMUM_ACCOUNTABLE_UNIT%TYPE;
  l_precision           FND_CURRENCIES.PRECISION%TYPE;
  l_base_precision      FND_CURRENCIES.PRECISION%TYPE;
  l_archive_total_amt   NUMBER;
  l_progress            VARCHAR2(3):='000';
BEGIN
  l_progress := '010';
  IF (p_doc_type = 'PO') AND (p_doc_subtype = 'STANDARD') then

        --Get the currency code of the PO and the base currency code

        po_core_s2.get_po_currency (x_object_id=>p_object_id,
                                    x_base_currency =>l_base_currency,
                                    x_po_currency  =>l_po_currency );
      l_progress := '020';
       --Get precision and minimum accountable unit of the PO CURRENCY
        po_core_s2.get_currency_info (x_currency_code => l_po_currency,
                                      x_precision=>l_precision,
                                      x_min_unit=>l_min_unit );
      l_progress := '030';
       -- Chk if base_currency = po_currency
       IF (p_base_cur_result = 'Y') AND (l_base_currency <> l_po_currency) then

          l_progress := '040';
            --Get precision and minimum accountable unit of the base CURRENCY

            po_core_s2.get_currency_info (x_currency_code => l_base_currency,
                                            x_precision=>l_base_precision,
                                             x_min_unit=>l_base_min_unit );


       ELSE -- if l_base_currency <> l_po_currency
         l_base_precision := l_precision;
         l_base_min_unit  := l_min_unit;
       END IF;  -- if l_base_currency <> l_po_currency
       l_progress := '050';
          --SQL WHAT- This query returns the total amount for an archived SPO
          --SQL WHY- To check if archived amount different from working copy amt. for a po in contract terms
          --SQL JOIN- Location id in PO_LINE_LOCATIONS_ARCHIVE_ALL and PO_DISTRIBUTIONS_ARCHIVE_ALL
          SELECT nvl(sum(round( ( (round( ( ( decode(POD.quantity_ordered, NULL,
                                                       (nvl(POD.amount_ordered,0) -
                                                         nvl(POD.amount_cancelled,0)
                                                       ),
                                                       ( (nvl(POD.quantity_ordered,0) -
                                                           nvl(POD.quantity_cancelled,0)
                                                         )*
                                                            nvl(PLL.price_override, 0)
                                                        )
                                                      ) *
                                               decode(p_base_cur_result,'Y',nvl(POD.rate,1),1)
                                            )/
                                             nvl(l_min_unit,1)
                                          ),decode(l_min_unit,null,l_precision,0)
                                        )*
                                         nvl(l_min_unit,1)
                                  )/
                                   nvl(l_base_min_unit,1)
                                )
                                 ,decode(l_base_min_unit,null,l_base_precision,0)
                              )*
                               nvl(l_base_min_unit,1)
                        ), 0
                      )
            INTO   l_archive_total_amt
            FROM   PO_DISTRIBUTIONS_ARCHIVE_ALL POD, PO_LINE_LOCATIONS_ARCHIVE_ALL PLL
            WHERE  PLL.po_header_id = p_object_id
            AND    PLL.shipment_type in ('STANDARD')
            AND    PLL.line_location_id = POD.line_location_id
            AND    PLL.LATEST_EXTERNAL_FLAG = 'Y'
            AND    POD.LATEST_EXTERNAL_FLAG = 'Y';

        l_progress := '060';
  END if;-- (p_doc_type = 'PO') AND (p_doc_subtype = 'STANDARD')
  l_progress := '070';
  --If l_archive_total_amt has a null value, return 0 as the total.
  IF l_archive_total_amt IS NULL THEN
	l_archive_total_amt := 0;
  END IF;
  l_progress := '080';
  RETURN(l_archive_total_amt);

EXCEPTION
   WHEN OTHERS THEN
            IF fnd_msg_pub.check_msg_level (fnd_msg_pub.g_msg_lvl_unexp_error)
            THEN
              fnd_msg_pub.add_exc_msg (g_pkg_name, 'GET_ARCHIVE_TOTAL',
                  SUBSTRB (SQLERRM , 1 , 200) || ' at location ' || l_progress);
            END IF;
            RETURN(0);
END get_archive_total;
--<CONTERMS FPJ END>



--<POC FPJ START>


FUNCTION  get_archive_total_for_any_rev (p_object_id       IN NUMBER,
                             p_object_type in varchar2,
                             p_doc_type        IN VARCHAR2,
                             p_doc_subtype     IN VARCHAR2,
                             p_doc_revision    IN NUMBER,
                             p_base_cur_result IN VARCHAR2) RETURN NUMBER IS

  l_base_currency       PO_HEADERS_ALL.CURRENCY_CODE%TYPE;
  l_po_currency         PO_HEADERS_ALL.CURRENCY_CODE%TYPE;
  l_min_unit            FND_CURRENCIES.MINIMUM_ACCOUNTABLE_UNIT%TYPE;
  l_base_min_unit       FND_CURRENCIES.MINIMUM_ACCOUNTABLE_UNIT%TYPE;
  l_precision           FND_CURRENCIES.PRECISION%TYPE;
  l_base_precision      FND_CURRENCIES.PRECISION%TYPE;
  l_archive_total_amt   NUMBER;
  l_progress            VARCHAR2(3):='000';

BEGIN
  l_progress := '010';
  IF (p_doc_type = 'PO') AND (p_doc_subtype = 'STANDARD') then

    IF p_object_type = 'H' then

        --Get the currency code of the PO and the base currency code

        po_core_s2.get_po_currency (x_object_id=>p_object_id,
                                    x_base_currency =>l_base_currency,
                                    x_po_currency  =>l_po_currency );
        l_progress := '020';
        --Get precision and minimum accountable unit of the PO CURRENCY
        po_core_s2.get_currency_info (x_currency_code => l_po_currency,
                                      x_precision=>l_precision,
                                      x_min_unit=>l_min_unit );
        l_progress := '030';
        -- Chk if base_currency = po_currency
        IF (p_base_cur_result = 'Y') AND (l_base_currency <> l_po_currency) then

            l_progress := '040';
            --Get precision and minimum accountable unit of the base CURRENCY

            po_core_s2.get_currency_info (x_currency_code => l_base_currency,
                                          x_precision=>l_base_precision,
                                          x_min_unit=>l_base_min_unit );


        ELSE -- if l_base_currency <> l_po_currency
            l_base_precision := l_precision;
            l_base_min_unit  := l_min_unit;
        END IF;  -- if l_base_currency <> l_po_currency
        l_progress := '050';
          --SQL WHAT- This query returns the total amount for an archived SPO
          --SQL WHY- To derive the total amount for an archived PO revision
          --SQL JOIN- Location id in PO_LINE_LOCATIONS_ARCHIVE_ALL and PO_DISTRIBUTIONS_ARCHIVE_ALL
        SELECT nvl(sum(round( ( (round( ( ( decode(POD.quantity_ordered, NULL,
                                                       (nvl(POD.amount_ordered,0) -
                                                         nvl(POD.amount_cancelled,0)
                                                       ),
                                                       ( (nvl(POD.quantity_ordered,0) -
                                                           nvl(POD.quantity_cancelled,0)
                                                         )*
                                                            nvl(PLL.price_override, 0)
                                                        )
                                                      ) *
                                               decode(p_base_cur_result,'Y',nvl(POD.rate,1),1)
                                            )/
                                             nvl(l_min_unit,1)
                                          ),decode(l_min_unit,null,l_precision,0)
                                        )*
                                         nvl(l_min_unit,1)
                                  )/
                                   nvl(l_base_min_unit,1)
                                )
                                 ,decode(l_base_min_unit,null,l_base_precision,0)
                              )*
                               nvl(l_base_min_unit,1)
                        ), 0
                      )
        INTO   l_archive_total_amt
        FROM   PO_DISTRIBUTIONS_ARCHIVE_ALL POD, PO_LINE_LOCATIONS_ARCHIVE_ALL PLL
        WHERE  PLL.po_header_id = p_object_id
        AND    PLL.shipment_type in ('STANDARD')
        AND    PLL.line_location_id = POD.line_location_id
        AND    PLL.REVISION_NUM = (SELECT MAX(pll1.revision_num) FROM PO_LINE_LOCATIONS_ARCHIVE_ALL PLL1
                                   WHERE pll1.line_location_id = pll.line_location_id AND
                                             pll1.revision_num <= p_doc_revision)
        AND    POD.REVISION_NUM = (SELECT MAX(pdd1.revision_num) FROM PO_DISTRIBUTIONS_ARCHIVE_ALL PDD1
                                   WHERE pdd1.po_distribution_id = pod.po_distribution_id AND
                                         pdd1.revision_num <= p_doc_revision);

        l_progress := '060';


    elsif (p_object_type = 'S' ) then /* PO Shipment */
        l_progress := '070';
        --< Bug 3549096 > Use _ALL tables instead of org-striped views.
        SELECT c.minimum_accountable_unit,
               c.precision
        INTO   l_min_unit,
               l_precision
        FROM   FND_CURRENCIES C,
               PO_HEADERS_ALL     PH,
               PO_LINE_LOCATIONS_ALL PLL
        WHERE  PLL.line_location_id   = p_object_id
        AND    PH.po_header_id        = PLL.po_header_id
        AND    C.currency_code        = PH.CURRENCY_CODE;

        if l_min_unit is null then
          l_progress := '80';

          --SQL WHAT- This query returns the amount for the appropriate revision of a line_location
          --SQL WHY-  To return the amount for line location
          --SQL JOIN- Use MAX to derive the appropriate revision of a line location
          -- <SERVICES FPJ>
          -- For the new Services lines, quantity will be null.
          -- Hence, added a decode statement to use amount directly
          -- in the total amount calculation when quantity is null.
          SELECT sum(round((
                 decode(pll.quantity,
                        null,
                        (pll.amount - nvl(pll.amount_cancelled,0)),
                        (pll.quantity - nvl(pll.quantity_cancelled,0))
                        * nvl(pll.price_override,0)
                       )
                 ),l_precision))
          INTO   l_archive_total_amt
          FROM   PO_LINE_LOCATIONS_ARCHIVE_ALL PLL
          WHERE  PLL.line_location_id   = p_object_id AND
          PLL.revision_num = (SELECT MAX(pll1.revision_num) FROM PO_LINE_LOCATIONS_ARCHIVE_ALL PLL1 WHERE
                              pll1.line_location_id = pll.line_location_id AND
                              pll1.revision_num <= p_doc_revision) ;

        else
          l_progress := '90';
          -- <SERVICES FPJ>
          -- For the new Services lines, quantity will be null.
          -- Hence, added a decode statement to use amount directly
          -- in the total amount calculation when quantity is null.
          select round(sum((
                 decode(pll.quantity,
                        null,
                        (pll.amount - nvl(pll.amount_cancelled,0)),
                        (pll.quantity - nvl(pll.quantity_cancelled,0))
                        * nvl(pll.price_override,0)
                   )
                 /l_min_unit) * l_min_unit))
          INTO   l_archive_total_amt
          FROM   PO_LINE_LOCATIONS_ARCHIVE_ALL PLL
          WHERE  PLL.line_location_id   = p_object_id AND
          PLL.revision_num = (SELECT MAX(pll1.revision_num) FROM PO_LINE_LOCATIONS_ARCHIVE_ALL PLL1 WHERE
                              pll1.line_location_id = pll.line_location_id AND
                              pll1.revision_num <= p_doc_revision) ;

        end if;

       elsif (p_object_type = 'L' ) then /* Po Line */
          l_progress := '100';
          --< Bug 3549096 > Use _ALL tables instead of org-striped views.
          SELECT sum(c.minimum_accountable_unit),
                 sum(c.precision)
          INTO   l_min_unit,
                 l_precision
          FROM   FND_CURRENCIES C,
                 PO_HEADERS_ALL     PH,
                 PO_LINES_ALL POL
          WHERE  POL.po_line_id   = p_object_id
          AND    PH.po_header_id  = POL.po_header_id
          AND    C.currency_code  = PH.CURRENCY_CODE;

          if l_min_unit is null then
            l_progress := '105';

      -- <SERVICES FPJ>
      -- For the new Services lines, quantity will be null.
      -- Hence, added a decode statement to use amount directly
      -- in the total amount calculation when quantity is null.
            select sum(round((
                   decode(pll.quantity,
                          null,
                          (pll.amount - nvl(pll.amount_cancelled, 0)),
                          (pll.quantity - nvl(pll.quantity_cancelled,0))
                          * nvl(pll.price_override,0)
                         )
                      ),l_precision))
            INTO   l_archive_total_amt
            FROM   PO_LINE_LOCATIONS_ARCHIVE_ALL PLL
            WHERE  PLL.po_line_id   = p_object_id
            AND    PLL.shipment_type  in ( 'STANDARD','BLANKET','PLANNED')
            AND    PLL.revision_num = (SELECT MAX(pll1.revision_num) FROM PO_LINE_LOCATIONS_ARCHIVE_ALL PLL1 WHERE
                                        pll1.line_location_id = pll.line_location_id AND
                                        pll1.revision_num <= p_doc_revision) ;

          else
            l_progress := '110';

      -- <SERVICES FPJ>
      -- For the new Services lines, quantity will be null.
      -- Hence, added a decode statement to use amount directly
      -- in the total amount calculation when quantity is null.
            select sum(round((
                   decode(pll.quantity,
                          null,
                          (pll.amount - nvl(pll.amount_cancelled,0)),
                          (pll.quantity - nvl(pll.quantity_cancelled,0))
                          * nvl(pll.price_override,0)
                         )
                   / l_min_unit) * l_min_unit))
            INTO   l_archive_total_amt
            FROM   PO_LINE_LOCATIONS_ARCHIVE_ALL PLL
            WHERE  PLL.po_line_id   = p_object_id
            AND    PLL.shipment_type  in ( 'STANDARD','BLANKET','PLANNED')
            AND    PLL.revision_num = (SELECT MAX(pll1.revision_num) FROM PO_LINE_LOCATIONS_ARCHIVE_ALL PLL1 WHERE
                                       pll1.line_location_id = pll.line_location_id AND
                                       pll1.revision_num <= p_doc_revision) ;


          end if;
      end if; /* p_object_type */

  elsif (p_doc_type = 'RELEASE') AND (p_doc_subtype = 'BLANKET') then


     if (p_object_type = 'R' ) then /* Release */

       if p_base_cur_result = 'Y' then
         l_progress := '180';
         --< Bug 3549096 > Use _ALL tables instead of org-striped views.
         SELECT GSB.currency_code,
                POH.currency_code
         INTO   l_base_currency,
                l_po_currency
         FROM   PO_HEADERS_ALL POH,
                FINANCIALS_SYSTEM_PARAMS_ALL FSP,
                GL_SETS_OF_BOOKS GSB,
                PO_RELEASES_ALL POR
         WHERE  POR.po_release_id   = p_object_id
         AND    POH.po_header_id    = POR.po_header_id
         AND    NVL(POR.org_id,-99) = NVL(FSP.org_id,-99)   --< Bug 3549096 >
         AND    FSP.set_of_books_id = GSB.set_of_books_id;

         if (l_base_currency <> l_po_currency) then
           /* Get precision and minimum accountable unit of the PO CURRENCY */
           l_progress := '190';
           po_core_s2.get_currency_info (l_po_currency,
                              l_precision,
                              l_min_unit );

           /* Get precision and minimum accountable unit of the base CURRENCY */
           l_progress := '200';
           po_core_s2.get_currency_info (l_base_currency,
                              l_base_precision,
                              l_base_min_unit );

           if l_base_min_unit is null then
             if l_min_unit is null then
               l_progress := '210';

               -- <SERVICES FPJ>
               -- For the new Services lines, quantity will be null.
               -- Hence, added a decode statement to use amount directly
               -- in the total amount calculation when quantity is null.
               select nvl(sum(round(round(
                      decode(POD.quantity_ordered,
                             null,
                             nvl(POD.amount_ordered, 0),
                             (nvl(POD.quantity_ordered,0) *
                             nvl(PLL.price_override,0))
                            )
                      * POD.rate
                      ,l_precision),l_base_precision)),0)
               INTO l_archive_total_amt
               FROM PO_DISTRIBUTIONS_ARCHIVE_ALL POD, PO_LINE_LOCATIONS_ARCHIVE_ALL PLL
               WHERE PLL.po_release_id    = p_object_id
               AND   PLL.line_location_id = POD.line_location_id
               AND   PLL.shipment_type in ('SCHEDULED','BLANKET')
               AND   PLL.REVISION_NUM = (SELECT MAX(pll1.revision_num) FROM PO_LINE_LOCATIONS_ARCHIVE_ALL PLL1
                                         WHERE pll1.line_location_id = pll.line_location_id AND
                                               pll1.revision_num <= p_doc_revision)
               AND   POD.REVISION_NUM = (SELECT MAX(pdd1.revision_num) FROM PO_DISTRIBUTIONS_ARCHIVE_ALL PDD1
                                         WHERE pdd1.po_distribution_id = pod.po_distribution_id AND
                                               pdd1.revision_num <= p_doc_revision);


             else
               l_progress := '212';
               -- <SERVICES FPJ>
               -- For the new Services lines, quantity will be null.
               -- Hence, added a decode statement to use amount directly
               -- in the total amount calculation when quantity is null.
	       SELECT nvl(sum(round( round(
                      decode(POD.quantity_ordered,
                             null,
                             nvl(POD.amount_ordered, 0),
                             (nvl(POD.quantity_ordered, 0) *
                             nvl(PLL.price_override, 0))
                            )
                      * POD.rate /
                      l_min_unit) * l_min_unit , l_base_precision)),0)
               INTO l_archive_total_amt
               FROM PO_DISTRIBUTIONS_ARCHIVE_ALL POD, PO_LINE_LOCATIONS_ARCHIVE_ALL PLL
               WHERE PLL.po_release_id    = p_object_id
               AND   PLL.line_location_id = POD.line_location_id
               AND   PLL.shipment_type in ('SCHEDULED','BLANKET')
               AND   PLL.REVISION_NUM = (SELECT MAX(pll1.revision_num) FROM PO_LINE_LOCATIONS_ARCHIVE_ALL PLL1
                                         WHERE pll1.line_location_id = pll.line_location_id AND
                                               pll1.revision_num <= p_doc_revision)
               AND   POD.REVISION_NUM = (SELECT MAX(pdd1.revision_num) FROM PO_DISTRIBUTIONS_ARCHIVE_ALL PDD1
                                         WHERE pdd1.po_distribution_id = pod.po_distribution_id AND
                                               pdd1.revision_num <= p_doc_revision);

             end if;
           else
             if l_min_unit is null then
               l_progress := '214';
               -- <SERVICES FPJ>
               -- For the new Services lines, quantity will be null.
               -- Hence, added a decode statement to use amount directly
               -- in the total amount calculation when quantity is null.
               SELECT nvl(sum(round( round(
                      decode(POD.quantity_ordered,
                             null,
                             nvl(POD.amount_ordered, 0),
                             (nvl(POD.quantity_ordered, 0) *
                             nvl(PLL.price_override, 0))
                            )
                      * POD.rate
                      , l_precision)
                      / l_base_min_unit ) * l_base_min_unit) ,0)
               INTO   l_archive_total_amt
               FROM PO_DISTRIBUTIONS_ARCHIVE_ALL POD, PO_LINE_LOCATIONS_ARCHIVE_ALL PLL
               WHERE PLL.po_release_id    = p_object_id
               AND   PLL.line_location_id = POD.line_location_id
               AND   PLL.shipment_type in ('SCHEDULED','BLANKET')
               AND   PLL.REVISION_NUM = (SELECT MAX(pll1.revision_num) FROM PO_LINE_LOCATIONS_ARCHIVE_ALL PLL1
                                         WHERE pll1.line_location_id = pll.line_location_id AND
                                               pll1.revision_num <= p_doc_revision)
               AND   POD.REVISION_NUM = (SELECT MAX(pdd1.revision_num) FROM PO_DISTRIBUTIONS_ARCHIVE_ALL PDD1
                                         WHERE pdd1.po_distribution_id = pod.po_distribution_id AND
                                               pdd1.revision_num <= p_doc_revision);

             else
               l_progress := '216';
            -- <SERVICES FPJ>
               -- For the new Services lines, quantity will be null.
               -- Hence, added a decode statement to use amount directly
               -- in the total amount calculation when quantity is null.
	       SELECT nvl(sum(round( round(
                      decode(POD.quantity_ordered,
                             null,
                             nvl(POD.amount_ordered, 0),
                             (nvl(POD.quantity_ordered, 0) *
                             nvl(PLL.price_override, 0))
                            )
                      * POD.rate /
                      l_min_unit) * l_min_unit  / l_base_min_unit)
                      * l_base_min_unit) , 0)
               INTO   l_archive_total_amt
               FROM PO_DISTRIBUTIONS_ARCHIVE_ALL POD, PO_LINE_LOCATIONS_ARCHIVE_ALL PLL
               WHERE PLL.po_release_id    = p_object_id
               AND   PLL.line_location_id = POD.line_location_id
               AND   PLL.shipment_type in ('SCHEDULED','BLANKET')
               AND   PLL.REVISION_NUM = (SELECT MAX(pll1.revision_num) FROM PO_LINE_LOCATIONS_ARCHIVE_ALL PLL1
                                         WHERE pll1.line_location_id = pll.line_location_id AND
                                               pll1.revision_num <= p_doc_revision)
               AND   POD.REVISION_NUM = (SELECT MAX(pdd1.revision_num) FROM PO_DISTRIBUTIONS_ARCHIVE_ALL PDD1
                                         WHERE pdd1.po_distribution_id = pod.po_distribution_id AND
                                               pdd1.revision_num <= p_doc_revision);

             end if;
           end if;

         end if;
       else
         l_progress := '220';
         --< Bug 3549096 > Use _ALL tables instead of org-striped views.
         SELECT c.minimum_accountable_unit,
                c.precision
         INTO   l_min_unit,
                l_precision
         FROM   FND_CURRENCIES C,
                PO_RELEASES_ALL    POR,
                PO_HEADERS_ALL     PH
         WHERE  POR.po_release_id = p_object_id
         AND    PH.po_header_id   = POR.PO_HEADER_ID
         AND    C.currency_code   = PH.CURRENCY_CODE;

         if l_min_unit is null then

   /* 958792 kbenjami 8/25/99.  Proprogated fix from R11.
      849493 - SVAIDYAN: Do a sum(round()) instead of round(sum()) since what
                         we pass to GL is the round of individual dist. amounts
                         and the sum of these rounded values is what should be
                         displayed as the header total.
   */
           l_progress := '222';
           -- <SERVICES FPJ>
           -- For the new Services lines, quantity will be null.
           -- Hence, added a decode statement to use amount directly
           -- in the total amount calculation when quantity is null.
           select sum(round(
                  decode(pll.quantity,
                         null,
                         (pll.amount - nvl(pll.amount_cancelled,0)),
                         ((pll.quantity - nvl(pll.quantity_cancelled,0)) *
                         nvl(pll.price_override,0))
                        )
                  ,l_precision))
           INTO   l_archive_total_amt
           FROM   PO_LINE_LOCATIONS_ARCHIVE_ALL PLL
           WHERE  PLL.po_release_id   = p_object_id
           AND    PLL.shipment_type  in ( 'SCHEDULED','BLANKET')
           AND   PLL.REVISION_NUM = (SELECT MAX(pll1.revision_num) FROM PO_LINE_LOCATIONS_ARCHIVE_ALL PLL1
                                     WHERE pll1.line_location_id = pll.line_location_id AND
                                           pll1.revision_num <= p_doc_revision);

         else
		   /* Bug 1111926: GMudgal 2/18/2000
		   ** Incorrect placement of brackets caused incorrect rounding
		   ** and consequently incorrect PO header totals
		   */
           l_progress := '224';

           -- <SERVICES FPJ>
           -- For the new Services lines, quantity will be null.
           -- Hence, added a decode statement to use amount directly
           -- in the total amount calculation when quantity is null.
           select sum(round(
                  decode(pll.quantity,
                         null,
                        (pll.amount - nvl(pll.amount_cancelled,0)),
                        ((pll.quantity - nvl(pll.quantity_cancelled,0)) *
                        nvl(pll.price_override,0))
                        )
                  /l_min_unit) *
                  l_min_unit)
           INTO   l_archive_total_amt
           FROM   PO_LINE_LOCATIONS_ARCHIVE_ALL PLL
           WHERE  PLL.po_release_id   = p_object_id
           AND    PLL.shipment_type  in ( 'SCHEDULED','BLANKET')
           AND   PLL.REVISION_NUM = (SELECT MAX(pll1.revision_num) FROM PO_LINE_LOCATIONS_ARCHIVE_ALL PLL1
                                     WHERE pll1.line_location_id = pll.line_location_id AND
                                           pll1.revision_num <= p_doc_revision);

         end if;

       end if;


     end if;
  END IF;-- (p_doc_type = 'PO') AND (p_doc_subtype = 'STANDARD')


  l_progress := '250';
  --If l_archive_total_amt has a null value, return 0 as the total.
  IF l_archive_total_amt IS NULL THEN
	l_archive_total_amt := 0;
  END IF;
  l_progress := '260';
  RETURN(l_archive_total_amt);

EXCEPTION
   WHEN OTHERS THEN
            IF fnd_msg_pub.check_msg_level (fnd_msg_pub.g_msg_lvl_unexp_error)
            THEN
                  fnd_msg_pub.add_exc_msg (g_pkg_name, 'GET_ARCHIVE_TOTAL_FOR_ANY_REV',
                  SUBSTRB (SQLERRM , 1 , 200) || ' at location ' || l_progress);
            END IF;
            RETURN(0);
END get_archive_total_for_any_rev;

--<POC FPJ END>
/*===================================================================================

    FUNCTION:    GET_RELEASE_LINE_TOTAL                Bug#3771735

    DESCRIPTION: Function is being added to get the correct cumulative Amount of all
		 shipments of  a  Release which correspond to the same line of a BPA.
		 The function will be used during the PDF generation of a Incomplete
		 Blanket Release.

===================================================================================*/
FUNCTION  GET_RELEASE_LINE_TOTAL
(
	p_line_id      IN PO_LINES_ALL.PO_LINE_ID%TYPE ,
	p_release_id   IN PO_RELEASES_ALL.PO_RELEASE_ID%TYPE
)
RETURN NUMBER
IS
  x_min_unit       NUMBER;
  x_precision      INTEGER;
  x_result_fld     NUMBER;

BEGIN

    SELECT sum(c.minimum_accountable_unit),
           sum(c.precision)
    INTO   x_min_unit,
           x_precision
    FROM   FND_CURRENCIES C,
           PO_HEADERS_ALL     PH,
           PO_LINES_ALL POL
    WHERE  POL.po_line_id   = p_line_id
    AND    PH.po_header_id  = POL.po_header_id
    AND    C.currency_code  = PH.CURRENCY_CODE;

    if x_min_unit is null then
      -- <SERVICES FPJ>
      -- For the new Services lines, quantity will be null.
      -- Hence, added a decode statement to use amount directly
      -- in the total amount calculation when quantity is null.
      --Use _ALL tables instead of org-striped views.
      select sum(round((
             decode(pll.quantity,
                    null,
                    (pll.amount - nvl(pll.amount_cancelled, 0)),
                    (pll.quantity - nvl(pll.quantity_cancelled,0))
                    * nvl(pll.price_override,0)
                   )
             ),x_precision))
      INTO   x_result_fld
      FROM   PO_LINE_LOCATIONS_ALL PLL
      WHERE  PLL.po_line_id   = p_line_id
      AND    PLL.po_release_id  = p_release_id
      AND    PLL.shipment_type  in ( 'STANDARD','BLANKET','PLANNED');

    else
      -- <SERVICES FPJ>
      -- For the new Services lines, quantity will be null.
      -- Hence, added a decode statement to use amount directly
      -- in the total amount calculation when quantity is null.
      -- Use _ALL tables instead of org-striped views.
      select sum(round((
             decode(pll.quantity,
                    null,
                    (pll.amount - nvl(pll.amount_cancelled,0)),
                    (pll.quantity - nvl(pll.quantity_cancelled,0))
                    * nvl(pll.price_override,0)
                   )
             / x_min_unit) * x_min_unit))
      INTO   x_result_fld
      FROM   PO_LINE_LOCATIONS_ALL PLL
      WHERE  PLL.po_line_id   = p_line_id
      AND    PLL.po_release_id   = p_release_id
      AND    PLL.shipment_type  in ( 'STANDARD','BLANKET','PLANNED');

    end if;
    return(x_result_fld);
END GET_RELEASE_LINE_TOTAL;

/*===================================================================================

    FUNCTION:    get_ga_amount_released                  <GA FPI>

    DESCRIPTION: Gets the total Amount Released for a particular Global Agreement.
                 That is, sum up the total for all uncancelled Standard PO lines
                 which reference that Global Agreement.

===================================================================================*/
FUNCTION get_ga_amount_released
(
    p_po_header_id             PO_HEADERS_ALL.po_header_id%TYPE,
    p_convert_to_base          BOOLEAN := FALSE
)
RETURN NUMBER
IS
    l_base_currency            FND_CURRENCIES.currency_code%TYPE;
    l_base_precision           FND_CURRENCIES.precision%TYPE;
    l_base_ext_precision       FND_CURRENCIES.extended_precision%TYPE;
    l_base_min_unit            FND_CURRENCIES.minimum_accountable_unit%TYPE;

    l_po_currency              FND_CURRENCIES.currency_code%TYPE;
    l_po_precision             FND_CURRENCIES.precision%TYPE;
    l_po_ext_precision         FND_CURRENCIES.extended_precision%TYPE;
    l_po_min_unit              FND_CURRENCIES.minimum_accountable_unit%TYPE;

    l_rate                     PO_HEADERS_ALL.rate%TYPE;

    x_total                    NUMBER;

BEGIN

    -- Get the functional currency code for the current org's Set of Books
    -- and the currency code defined for the PO
    PO_CORE_S2.get_po_currency( p_po_header_id,               -- IN
                                l_base_currency,              --OUT
                                l_po_currency );              --OUT

    -- Get the Precision (never NULL), Extended Precision, Minimum Accountable Unit
    -- for the Set of Books' currency code...
    FND_CURRENCY.get_info( l_base_currency,                   -- IN
                           l_base_precision,                  --OUT
                           l_base_ext_precision,              --OUT
                           l_base_min_unit );                 --OUT

    -- and for the PO's currency code...
    FND_CURRENCY.get_info( l_po_currency,                     -- IN
                           l_po_precision,                    --OUT
                           l_po_ext_precision,                --OUT
                           l_po_min_unit );                   --OUT

    -- NOTE: All Standard PO's must have the same currency code as the
    -- Global Agreement that they reference. The currency conversion rate used
    -- will be the rate between the Global Agreement's currency and the owning
    -- org's currency.

    IF  (   ( p_convert_to_base )               -- convert back to base currency
        AND ( l_base_currency <> l_po_currency ) ) THEN

        -- Get the rate from PO_HEADERS_ALL for the Global Agreement.
        SELECT     rate
        INTO       l_rate
        FROM       po_headers_all
        WHERE      po_header_id = p_po_header_id;

        IF ( l_base_min_unit IS NOT NULL ) THEN

            IF ( l_po_min_unit IS NOT NULL ) THEN

	      -- <SERVICES FPJ>
              -- For the new Services lines, quantity will be null.
              -- Hence, added a decode statement to use amount directly
              -- in the total amount calculation when quantity is null.
	      SELECT sum ( 
				Decode ( tac_po_currency_pkg.check_bpa_3(pol.po_header_id),			-- dTAC
				'N',	 ( round (  ( round (  (  ( decode(pol.quantity, null,
		                                       (pod.amount_ordered -
		                                       pod.amount_cancelled),
		                                       (( pod.quantity_ordered
                                                       - pod.quantity_cancelled )
		                                       * poll.price_override)
							     )
					             )
                                                    * l_rate )
                                                 / l_po_min_unit )
                                         * l_po_min_unit )
                                      / l_base_min_unit )
                              * l_base_min_unit ) ,
				tac_po_currency_pkg.get_ori_price (pol.po_header_id,				-- dTAC
						 ( round (  ( round (  (  ( decode(pol.quantity, null,
		                                       (pod.amount_ordered -
		                                       pod.amount_cancelled),
		                                       (( pod.quantity_ordered
                                                       - pod.quantity_cancelled )
		                                       * poll.price_override)
							     )
					             )
                                                    * l_rate )
                                                 / l_po_min_unit )
                                         * l_po_min_unit )
                                      / l_base_min_unit )
                              * l_base_min_unit )
							  )
				) )			
                INTO      x_total
                FROM      po_distributions_all    pod,
                          po_line_locations_all   poll,
                          po_lines_all            pol
                WHERE     pod.line_location_id = poll.line_location_id
                AND       poll.po_line_id = pol.po_line_id
                AND       pol.from_header_id = p_po_header_id;

            ELSE          -- ( l_po_min_unit IS NULL )

	       -- <SERVICES FPJ>
               -- For the new Services lines, quantity will be null.
               -- Hence, added a decode statement to use amount directly
               -- in the total amount calculation when quantity is null.
	       SELECT sum ( 
			   Decode ( tac_po_currency_pkg.check_bpa_3(pol.po_header_id),		-- dTAC
			   'N' , ( round (  (  (decode(pol.quantity, null,
		                                   (pod.amount_ordered -
		                                   pod.amount_cancelled),
		                                   (( pod.quantity_ordered
                                                   - pod.quantity_cancelled )
		                                   * poll.price_override)
						  )
					   )
                                         * l_rate )
                                      / l_base_min_unit )
                              * l_base_min_unit )
				, tac_po_currency_pkg.get_ori_price (pol.po_header_id,			-- dTAC
					( round (  (  (decode(pol.quantity, null,
		                                   (pod.amount_ordered -
		                                   pod.amount_cancelled),
		                                   (( pod.quantity_ordered
                                                   - pod.quantity_cancelled )
		                                   * poll.price_override)
						  )
					   )
                                         * l_rate )
                                      / l_base_min_unit )
                              * l_base_min_unit ) ) )
							  )
                INTO      x_total
                FROM      po_distributions_all    pod,
                          po_line_locations_all   poll,
                          po_lines_all            pol
                WHERE     pod.line_location_id = poll.line_location_id
                AND       poll.po_line_id = pol.po_line_id
                AND       pol.from_header_id = p_po_header_id;

            END IF;

        ELSE              -- ( l_base_min_unit IS NULL )

            IF ( l_po_min_unit IS NOT NULL ) THEN

               -- <SERVICES FPJ>
               -- For the new Services lines, quantity will be null.
               -- Hence, added a decode statement to use amount directly
               -- in the total amount calculation when quantity is null.
	       SELECT    sum (
				Decode ( tac_po_currency_pkg.check_bpa_3(pol.po_header_id),		-- dTAC
				'N' , ( round (  (  (decode(pol.quantity, null,
		                                   (pod.amount_ordered -
		                                   pod.amount_cancelled),
		                                   (( pod.quantity_ordered
                                                   - pod.quantity_cancelled )
		                                   * poll.price_override)
					          )
					   )
                                         * l_rate )
                                      / l_po_min_unit )
                              * l_po_min_unit )
					, tac_po_currency_pkg.get_ori_price (pol.po_header_id,		-- dTAC
					  ( round (  (  (decode(pol.quantity, null,
		                                   (pod.amount_ordered -
		                                   pod.amount_cancelled),
		                                   (( pod.quantity_ordered
                                                   - pod.quantity_cancelled )
		                                   * poll.price_override)
					          )
					   )
                                         * l_rate )
                                      / l_po_min_unit )
                              * l_po_min_unit ) )
						) )
                INTO      x_total
                FROM      po_distributions_all    pod,
                          po_line_locations_all   poll,
                          po_lines_all            pol
                WHERE     pod.line_location_id = poll.line_location_id
                AND       poll.po_line_id = pol.po_line_id
                AND       pol.from_header_id = p_po_header_id;

            ELSE          -- ( l_po_min_unit IS NULL )

               -- <SERVICES FPJ>
               -- For the new Services lines, quantity will be null.
               -- Hence, added a decode statement to use amount directly
               -- in the total amount calculation when quantity is null.
	       SELECT sum (
					Decode ( tac_po_currency_pkg.check_bpa_3(pol.po_header_id),		-- dTAC 
					'N' ,	( (decode(pol.quantity, null,
		                        (pod.amount_ordered - pod.amount_cancelled),
	                                (( pod.quantity_ordered - pod.quantity_cancelled ) * poll.price_override) ) ) * l_rate )
						, tac_po_currency_pkg.get_ori_price (pol.po_header_id,		-- dTAC
							( (decode(pol.quantity, null,
		                        (pod.amount_ordered - pod.amount_cancelled),
	                                (( pod.quantity_ordered - pod.quantity_cancelled ) * poll.price_override) ) ) * l_rate ) )
					) )
                INTO      x_total
                FROM      po_distributions_all    pod,
                          po_line_locations_all   poll,
                          po_lines_all            pol
                WHERE     pod.line_location_id = poll.line_location_id
                AND       poll.po_line_id = pol.po_line_id
                AND       pol.from_header_id = p_po_header_id;

            END IF;

        END IF;

    ELSE                  -- just get po_currency (no conversion necessary)

        IF ( l_po_min_unit IS NOT NULL ) THEN

	   -- <SERVICES FPJ>
           -- For the new Services lines, quantity will be null.
           -- Hence, added a decode statement to use amount directly
           -- in the total amount calculation when quantity is null.
	   SELECT sum (
				Decode ( tac_po_currency_pkg.check_bpa_3(pol.po_header_id),		-- dTAC 
				'N'	, ( round (  (decode(pol.quantity, null,
                                       (pod.amount_ordered - pod.amount_cancelled),
                                       (( pod.quantity_ordered - pod.quantity_cancelled ) * poll.price_override) ) )
                                  / l_po_min_unit ) * l_po_min_unit )
					, tac_po_currency_pkg.get_ori_price (pol.po_header_id,		-- dTAC
					  ( round (  (decode(pol.quantity, null,
                                       (pod.amount_ordered - pod.amount_cancelled),
                                       (( pod.quantity_ordered - pod.quantity_cancelled ) * poll.price_override) ) )
                                  / l_po_min_unit ) * l_po_min_unit ) )
				)	)
            INTO      x_total
            FROM      po_distributions_all    pod,
                      po_line_locations_all   poll,
                      po_lines_all            pol
            WHERE     pod.line_location_id = poll.line_location_id
            AND       poll.po_line_id = pol.po_line_id
            AND       pol.from_header_id = p_po_header_id;

        ELSE              -- ( l_po_min_unit IS NULL )

           -- <SERVICES FPJ>
           -- For the new Services lines, quantity will be null.
           -- Hence, added a decode statement to use amount directly
           -- in the total amount calculation when quantity is null.
	   SELECT sum (
				Decode ( tac_po_currency_pkg.check_bpa_3(pol.po_header_id),		-- dTAC
				'N' , decode( pol.quantity, null,
                          ( pod.amount_ordered - pod.amount_cancelled),
		                  ( ( pod.quantity_ordered - pod.quantity_cancelled ) * poll.price_override) )
				, tac_po_currency_pkg.get_ori_price (pol.po_header_id,		-- dTAC
					decode( pol.quantity, null,
                          ( pod.amount_ordered - pod.amount_cancelled),
		                  ( ( pod.quantity_ordered - pod.quantity_cancelled ) * poll.price_override) ) )
			 ) )
            INTO      x_total
            FROM      po_distributions_all    pod,
                      po_line_locations_all   poll,
                      po_lines_all            pol
            WHERE     pod.line_location_id = poll.line_location_id
            AND       poll.po_line_id = pol.po_line_id
            AND       pol.from_header_id = p_po_header_id;

        END IF;

    END IF;

    return(x_total);

EXCEPTION
    WHEN OTHERS THEN
        return(0);

END get_ga_amount_released;


/*===================================================================================

    FUNCTION:    get_ga_line_amount_released                <GA FPI>

    DESCRIPTION: Gets the total Amount Released for a Global Agreement line.
                 That is, sum up the total for all uncancelled Standard PO lines
                 which reference that Global Agreement line.

===================================================================================*/
PROCEDURE get_ga_line_amount_released
(
    p_po_line_id             IN       PO_LINES_ALL.po_line_id%TYPE,
    p_po_header_id           IN       PO_HEADERS_ALL.po_header_id%TYPE,
    x_quantity_released      OUT NOCOPY      NUMBER,
    x_amount_released        OUT NOCOPY      NUMBER
)
IS
    l_base_currency            FND_CURRENCIES.currency_code%TYPE;

    l_po_currency              FND_CURRENCIES.currency_code%TYPE;
    l_po_precision             FND_CURRENCIES.precision%TYPE;
    l_po_ext_precision         FND_CURRENCIES.extended_precision%TYPE;
    l_po_min_unit              FND_CURRENCIES.minimum_accountable_unit%TYPE;

    l_rate                     PO_HEADERS_ALL.rate%TYPE;

BEGIN

    -- Get the currency code defined for the PO
    PO_CORE_S2.get_po_currency( p_po_header_id,               -- IN
                                l_base_currency,              --OUT
                                l_po_currency );              --OUT

    -- Get the Precision (never NULL), Extended Precision, Minimum Accountable Unit
    -- for the PO's currency code...
    FND_CURRENCY.get_info( l_po_currency,                     -- IN
                           l_po_precision,                    --OUT
                           l_po_ext_precision,                --OUT
                           l_po_min_unit );                   --OUT

    IF ( l_po_min_unit IS NOT NULL ) THEN

       -- <SERVICES FPJ>
       -- For the new Services lines, quantity will be null.
       -- Hence, added a decode statement to use amount directly
       -- in the total amount calculation when quantity is null.
       SELECT    sum (
			Decode ( tac_po_currency_pkg.check_bpa_3(pol.po_header_id),		-- dTAC
					'N', round ((decode(pol.quantity, null,
		                        (pod.amount_ordered - pod.amount_cancelled),
		                        (( pod.quantity_ordered - pod.quantity_cancelled ) * poll.price_override)) )
                              / l_po_min_unit ) * l_po_min_unit ,
						tac_po_currency_pkg.get_ori_price (pol.po_header_id,		-- dTAC
						 round ((decode(pol.quantity, null,
		                        (pod.amount_ordered - pod.amount_cancelled),
		                        (( pod.quantity_ordered - pod.quantity_cancelled ) * poll.price_override)) )
                              / l_po_min_unit ) * l_po_min_unit	   )
					)
				),
                  sum ( pod.quantity_ordered - pod.quantity_cancelled )
        INTO      x_amount_released,
                  x_quantity_released
        FROM      po_distributions_all    pod,
                  po_line_locations_all   poll,
                  po_lines_all            pol
        WHERE     pod.line_location_id = poll.line_location_id
        AND       poll.po_line_id = pol.po_line_id
        AND       pol.from_line_id = p_po_line_id;

    ELSE              -- ( l_po_min_unit IS NULL )

       -- <SERVICES FPJ>
       -- For the new Services lines, quantity will be null.
       -- Hence, added a decode statement to use amount directly
       -- in the total amount calculation when quantity is null.
       SELECT sum (
			Decode ( tac_po_currency_pkg.check_bpa_3(pol.po_header_id),		-- dTAC
			'N' , decode( pol.quantity, null,
						( pod.amount_ordered - pod.amount_cancelled ),
						( ( pod.quantity_ordered - pod.quantity_cancelled ) * poll.price_override) ) ,
				tac_po_currency_pkg.get_ori_price (pol.po_header_id,		-- dTAC
					decode( pol.quantity, null,
						( pod.amount_ordered - pod.amount_cancelled ),
						( ( pod.quantity_ordered - pod.quantity_cancelled ) * poll.price_override) ) )
		     ) ),
                  sum ( pod.quantity_ordered - pod.quantity_cancelled )
        INTO      x_amount_released,
                  x_quantity_released
        FROM      po_distributions_all    pod,
                  po_line_locations_all   poll,
                  po_lines_all            pol
        WHERE     pod.line_location_id = poll.line_location_id
        AND       poll.po_line_id = pol.po_line_id
        AND       pol.from_line_id = p_po_line_id;

    END IF;

END get_ga_line_amount_released;


-- <GC FPJ START>
/**=========================================================================
* Function: get_gc_amount_released
* Effects:  Calculate amount released given a global contract
* Requires: None
* Modifies: None
* Return:   amount released. Whether the amount is converted to base currency
*           is determined by p_convert_to_base
=========================================================================**/

FUNCTION get_gc_amount_released
(
    p_po_header_id         IN NUMBER,
    p_convert_to_base      IN BOOLEAN := FALSE
) RETURN NUMBER
IS

l_base_currency   FND_CURRENCIES.currency_code%TYPE;
l_base_precision  FND_CURRENCIES.precision%TYPE;
l_base_min_unit   FND_CURRENCIES.minimum_accountable_unit%TYPE;

l_po_currency     FND_CURRENCIES.currency_code%TYPE;
l_po_precision    FND_CURRENCIES.precision%TYPE;
l_po_min_unit     FND_CURRENCIES.minimum_accountable_unit%TYPE;

l_rate            PO_HEADERS_ALL.rate%TYPE;
l_total           NUMBER;

BEGIN
    PO_CORE_S2.get_po_currency ( x_object_id     => p_po_header_id,
                                 x_base_currency => l_base_currency,
                                 x_po_currency   => l_po_currency     );

    PO_CORE_S2.get_currency_info ( x_currency_code => l_base_currency,
                                   x_precision     => l_base_precision,
                                   x_min_unit      => l_base_min_unit  );

    PO_CORE_S2.get_currency_info ( x_currency_code => l_po_currency,
                                   x_precision     => l_po_precision,
                                   x_min_unit      => l_po_min_unit  );


    IF ( p_convert_to_base AND
         l_base_currency <> l_po_currency ) THEN


        -- SQL What: Get the rate of the contract header
        -- SQL Why : If we are to convert to base currency, we need to
        --           use the rate on the contract header for conversion

        SELECT rate
        INTO   l_rate
        FROM   po_headers_all POH
        WHERE  POH.po_header_id = p_po_header_id;

        IF (l_base_min_unit IS NOT NULL) THEN

            IF (l_po_min_unit IS NOT NULL) THEN

                -- SQL What: calculate amount released from all lines across OU
                --           referencing this GC, with the amount being
                --           converted to base currency
                -- SQL Why:  This is the return value

                SELECT SUM(
                        ROUND( (
                           ROUND ( ( ( (NVL(POD.quantity_ordered, 0) -
                                        NVL(POD.quantity_cancelled, 0)
                                       ) * POLL.price_override
                                     ) * l_rate
                                   ) / l_po_min_unit
                                 ) * l_po_min_unit
                               ) / l_base_min_unit
                             ) * l_base_min_unit
                          )
                INTO   l_total
                FROM   po_distributions_all  POD,
                       po_line_locations_all POLL,
                       po_lines_all          POL
                WHERE  POD.line_location_id = POLL.line_location_id
                AND    POLL.po_line_id = POL.po_line_id
                AND    POL.contract_id = p_po_header_id;

            ELSE                  -- (l_po_min_unit is null)

                -- SQL What: calculate amount released from all lines across OU
                --           referencing this GC, with the amount being
                --           converted to base currency
                -- SQL Why:  This is the return value

                SELECT SUM(
                         ROUND(
                           ROUND ( ( (NVL(pod.quantity_ordered, 0) -
                                      NVL(pod.quantity_cancelled, 0)
                                     ) * poll.price_override
                                   ) * l_rate
                                   , l_po_precision
                                 ) / l_base_min_unit
                              ) * l_base_min_unit
                          )
                INTO   l_total
                FROM   po_distributions_all  POD,
                       po_line_locations_all POLL,
                       po_lines_all          POL
                WHERE  POD.line_location_id = POLL.line_location_id
                AND    POLL.po_line_id = POL.po_line_id
                AND    POL.contract_id = p_po_header_id;
            END IF;

        ELSE                     -- (l_base_min_unit IS NULL)

            IF (l_po_min_unit IS NOT NULL) THEN

                -- SQL What: calculate amount released from all lines across OU
                --           referencing this GC, with the amount being
                --           converted to base currency
                -- SQL Why:  This is the return value

                SELECT SUM(
                         ROUND(
                           ROUND ( ( ( (NVL(pod.quantity_ordered, 0) -
                                        NVL(pod.quantity_cancelled, 0)
                                       ) * poll.price_override
                                     ) * l_rate
                                   ) / l_po_min_unit
                                 ) * l_po_min_unit
                                 , l_base_precision
                              )
                          )
                INTO   l_total
                FROM   po_distributions_all  POD,
                       po_line_locations_all POLL,
                       po_lines_all          POL
                WHERE  POD.line_location_id = POLL.line_location_id
                AND    POLL.po_line_id = POL.po_line_id
                AND    POL.contract_id = p_po_header_id;

            ELSE                -- (l_po_min_unit IS NULL)

                -- SQL What: calculate amount released from all lines across OU
                --           referencing this GC, with the amount being
                --           converted to base currency
                -- SQL Why:  This is the return value

                SELECT SUM(
                         ROUND(
                           ROUND ( ( (NVL(pod.quantity_ordered, 0) -
                                      NVL(pod.quantity_cancelled, 0)
                                     ) * poll.price_override
                                   ) * l_rate
                                   , l_po_precision
                                 )
                               , l_base_precision
                              )
                          )
                INTO   l_total
                FROM   po_distributions_all  POD,
                       po_line_locations_all POLL,
                       po_lines_all          POL
                WHERE  POD.line_location_id = POLL.line_location_id
                AND    POLL.po_line_id = POL.po_line_id
                AND    POL.contract_id = p_po_header_id;

            END IF;

        END IF;

    ELSE                   -- (no base currency conversion)

        IF (l_po_min_unit IS NOT NULL) THEN

            -- SQL What: calculate amount released from all lines across OU
            --           referencing this GC in GC currency
            -- SQL Why:  This is the return value

            SELECT SUM(
                     ROUND( ( ( NVL(pod.quantity_ordered, 0) -
                                NVL(pod.quantity_cancelled, 0)
                              ) * poll.price_override
                            ) / l_po_min_unit
                          ) * l_po_min_unit
                      )
            INTO   l_total
            FROM   po_distributions_all  POD,
                   po_line_locations_all POLL,
                   po_lines_all          POL
            WHERE  POD.line_location_id = POLL.line_location_id
            AND    POLL.po_line_id = POL.po_line_id
            AND    POL.contract_id = p_po_header_id;

        ELSE               -- (l_po_min unit IS NULL)

            -- SQL What: calculate amount released from all lines across OU
            --           referencing this GC in GC currency
            -- SQL Why:  This is the return value

            SELECT SUM(
                     ROUND( ( NVL(pod.quantity_ordered, 0) -
                              NVL(pod.quantity_cancelled, 0)
                            ) * poll.price_override
                            , l_po_precision
                          )
                      )
            INTO   l_total
            FROM   po_distributions_all  POD,
                   po_line_locations_all POLL,
                   po_lines_all          POL
            WHERE  POD.line_location_id = POLL.line_location_id
            AND    POLL.po_line_id = POL.po_line_id
            AND    POL.contract_id = p_po_header_id;

        END IF;

    END IF;

    RETURN l_total;

EXCEPTION
    WHEN OTHERS THEN
        RETURN 0;
END get_gc_amount_released;

-- <GC FPJ END>

/* ===========================================================================
  PROCEDURE NAME : validate_lookup_info (
                   p_lookup_rec IN OUT NOCOPY RCV_SHIPMENT_HEADER_SV.LookupRecType)

  DESCRIPTION    :

  CLIENT/SERVER  : SERVER

  LIBRARY NAME   :

  OWNER          : Raj Bhakta

  PARAMETERS     : p_lookup_rec IN OUT NOCOPY RCV_SHIPMENT_HEADER_SV.LookupRecType

  ALGORITHM      :

  NOTES          : Generic procedure which accepts the lookup record and
                   returns error status and error message depending on
                   business rules. The lookup record has type and code as
                   components.

  CHANGE HISTORY : Raj Bhakta 10/30/96 created

=========================================================================== */

 PROCEDURE validate_lookup_info(
          p_lookup_rec IN OUT NOCOPY RCV_SHIPMENT_HEADER_SV.LookupRecType) IS


 cursor C is
   SELECT inactive_date
   FROM po_lookup_codes
   WHERE
       lookup_type = p_lookup_rec.lookup_type and
       lookup_code = p_lookup_rec.lookup_code;

 X_sysdate Date := sysdate;

 X_inactive_date   po_lookup_codes.INACTIVE_DATE%TYPE;

 BEGIN

   OPEN C;
   FETCH C INTO X_inactive_date;

   IF C%ROWCOUNT = 0 then
      CLOSE C;
      p_lookup_rec.error_record.error_status := 'E';
      p_lookup_rec.error_record.error_message := 'INVALID_LOOKUP';
      RETURN;

   ELSE

      IF X_sysdate > nvl(X_inactive_date,X_sysdate + 1) THEN

           CLOSE C;
           p_lookup_rec.error_record.error_status := 'E';
           p_lookup_rec.error_record.error_message := 'INACTIVE_LOOKUP';
           RETURN;
      END IF;

      LOOP
        FETCH C INTO X_inactive_date;
        IF C%NOTFOUND THEN

           p_lookup_rec.error_record.error_status := 'S';
           p_lookup_rec.error_record.error_message := NULL;
           EXIT;

        END IF;
        IF C%FOUND THEN

           p_lookup_rec.error_record.error_status := 'E';
           p_lookup_rec.error_record.error_message := 'TOOMANYROWS';
           CLOSE C;
           EXIT;

        END IF;

       END LOOP;

   END IF;

 EXCEPTION
    WHEN others THEN
           p_lookup_rec.error_record.error_status := 'U';
           p_lookup_rec.error_record.error_message := sqlerrm;

 END validate_lookup_info;





-------------------------------------------
-- Document id helper procedures
-------------------------------------------




-------------------------------------------------------------------------------
--Start of Comments
--Name: get_document_ids
--Pre-reqs:
--  None.
--Modifies:
--  None.
--Locks:
--  None.
--Function:
--  Returns the header-level document id for the given ids.
--Parameters:
--IN:
--p_doc_type
--  Document type.  Use the g_doc_type_<> variables, where <> is:
--    REQUISITION
--    PA
--    PO
--    RELEASE
--p_doc_level
--  The type of ids that are being passed.  Use g_doc_level_<>
--    HEADER
--    LINE
--    SHIPMENT
--    DISTRIBUTION
--p_doc_level_id_tbl
--  Ids of the doc level type of which to derive the document header id.
--OUT:
--x_doc_id_tbl
--  Header-level ids of the input ids.
--Testing:
--
--End of Comments
-------------------------------------------------------------------------------
PROCEDURE get_document_ids(
   p_doc_type                       IN             VARCHAR2
,  p_doc_level                      IN             VARCHAR2
,  p_doc_level_id_tbl               IN             po_tbl_number
,  x_doc_id_tbl                     OUT NOCOPY     po_tbl_number
)
IS

l_log_head     CONSTANT VARCHAR2(100) := g_log_head||'GET_DOCUMENT_IDS';
l_progress     VARCHAR2(3) := '000';

l_id_key    NUMBER;

-- Bug 3292870
l_rowid_char_tbl    g_rowid_char_tbl;

BEGIN

IF g_debug_stmt THEN
   PO_DEBUG.debug_begin(l_log_head);
   PO_DEBUG.debug_var(l_log_head,l_progress,'p_doc_type', p_doc_type);
   PO_DEBUG.debug_var(l_log_head,l_progress,'p_doc_level', p_doc_level);
   PO_DEBUG.debug_var(l_log_head,l_progress,'p_doc_level_id_tbl', p_doc_level_id_tbl);
END IF;

l_progress := '010';

IF (p_doc_level = g_doc_level_HEADER) THEN

   l_progress := '020';
   IF g_debug_stmt THEN
      PO_DEBUG.debug_stmt(l_log_head,l_progress,'headers');
   END IF;

   x_doc_id_tbl := p_doc_level_id_tbl;

ELSE

   l_progress := '100';
   IF g_debug_stmt THEN
      PO_DEBUG.debug_stmt(l_log_head,l_progress,'not headers');
   END IF;

   -- We need to get outthe header ids in the same ordering as the
   -- input id table.
   -- We can't do a FORALL ... SELECT (PL/SQL limitation),
   -- but we can to a FORALL ... INSERT ... RETURNING.

   ----------------------------------------------------------------
   -- PO_SESSION_GT column mapping
   --
   -- num1     doc level id
   -- num2     header id of num1
   ----------------------------------------------------------------

   l_id_key := get_session_gt_nextval();

   l_progress := '110';

   IF (p_doc_type = g_doc_type_REQUISITION) THEN

      l_progress := '120';
      IF g_debug_stmt THEN
         PO_DEBUG.debug_stmt(l_log_head,l_progress,'requisition');
      END IF;

      IF (p_doc_level = g_doc_level_LINE) THEN

         l_progress := '130';
         IF g_debug_stmt THEN
            PO_DEBUG.debug_stmt(l_log_head,l_progress,'line');
         END IF;

         /* Start Bug 3292870: Split query to make it compatible with 8i db. */

         FORALL i IN 1 .. p_doc_level_id_tbl.COUNT
         INSERT INTO PO_SESSION_GT ( key, num1 )
         VALUES
         (  l_id_key
         ,  p_doc_level_id_tbl(i)
         )
         RETURNING ROWIDTOCHAR(rowid)
         BULK COLLECT INTO l_rowid_char_tbl
         ;

         FORALL i IN 1 .. p_doc_level_id_tbl.COUNT
         UPDATE PO_SESSION_GT
         SET num2 =
            (
               SELECT PRL.requisition_header_id
               FROM PO_REQUISITION_LINES_ALL PRL
               WHERE PRL.requisition_line_id = p_doc_level_id_tbl(i)
            )
         WHERE rowid = CHARTOROWID(l_rowid_char_tbl(i))
         RETURNING num2
         BULK COLLECT INTO x_doc_id_tbl
         ;

         /* End Bug 3292870 */

         l_progress := '140';

      ELSIF (p_doc_level = g_doc_level_DISTRIBUTION) THEN

         l_progress := '150';
         IF g_debug_stmt THEN
            PO_DEBUG.debug_stmt(l_log_head,l_progress,'distribution');
         END IF;

         /* Start Bug 3292870: Split query to make it compatible with 8i db. */

         FORALL i IN 1 .. p_doc_level_id_tbl.COUNT
         INSERT INTO PO_SESSION_GT ( key, num1 )
         VALUES
         (  l_id_key
         ,  p_doc_level_id_tbl(i)
         )
         RETURNING ROWIDTOCHAR(rowid)
         BULK COLLECT INTO l_rowid_char_tbl
         ;

         FORALL i IN 1 .. p_doc_level_id_tbl.COUNT
         UPDATE PO_SESSION_GT
         SET num2 =
            (
               SELECT PRL.requisition_header_id
               FROM
                  PO_REQUISITION_LINES_ALL PRL
               ,  PO_REQ_DISTRIBUTIONS_ALL PRD
               WHERE PRL.requisition_line_id = PRD.requisition_line_id
               AND PRD.distribution_id = p_doc_level_id_tbl(i)
            )
         WHERE rowid = CHARTOROWID(l_rowid_char_tbl(i))
         RETURNING num2
         BULK COLLECT INTO x_doc_id_tbl
         ;

         /* End Bug 3292870 */

         l_progress := '160';

      ELSE
         l_progress := '170';
         RAISE g_INVALID_CALL_EXC;
      END IF;

      l_progress := '190';

   ELSE -- PO, PA, RELEASE

      l_progress := '200';
      IF g_debug_stmt THEN
         PO_DEBUG.debug_stmt(l_log_head,l_progress,'not req');
      END IF;

      IF (p_doc_level = g_doc_level_LINE) THEN

         l_progress := '210';
         IF g_debug_stmt THEN
            PO_DEBUG.debug_stmt(l_log_head,l_progress,'line');
         END IF;

         /* Start Bug 3292870: Split query to make it compatible with 8i db. */

         FORALL i IN 1 .. p_doc_level_id_tbl.COUNT
         INSERT INTO PO_SESSION_GT ( key, num1 )
         VALUES
         (  l_id_key
         ,  p_doc_level_id_tbl(i)
         )
         RETURNING ROWIDTOCHAR(rowid)
         BULK COLLECT INTO l_rowid_char_tbl
         ;

         FORALL i IN 1 .. p_doc_level_id_tbl.COUNT
         UPDATE PO_SESSION_GT
         SET num2 =
            (
               SELECT POL.po_header_id
               FROM PO_LINES_ALL POL
               WHERE POL.po_line_id = p_doc_level_id_tbl(i)
            )
         WHERE rowid = CHARTOROWID(l_rowid_char_tbl(i))
         RETURNING num2
         BULK COLLECT INTO x_doc_id_tbl
         ;

         /* End Bug 3292870 */

         l_progress := '220';

      ELSIF (p_doc_level = g_doc_level_SHIPMENT) THEN

         l_progress := '230';
         IF g_debug_stmt THEN
            PO_DEBUG.debug_stmt(l_log_head,l_progress,'shipment');
         END IF;

         /* Start Bug 3292870: Split query to make it compatible with 8i db. */

         FORALL i IN 1 .. p_doc_level_id_tbl.COUNT
         INSERT INTO PO_SESSION_GT ( key, num1 )
         VALUES
         (  l_id_key
         ,  p_doc_level_id_tbl(i)
         )
         RETURNING ROWIDTOCHAR(rowid)
         BULK COLLECT INTO l_rowid_char_tbl
         ;

         FORALL i IN 1 .. p_doc_level_id_tbl.COUNT
         UPDATE PO_SESSION_GT
         SET num2 =
            (
               SELECT DECODE( p_doc_type
                           ,  g_doc_type_RELEASE, POLL.po_release_id
                           ,  POLL.po_header_id
                           )
               FROM PO_LINE_LOCATIONS_ALL POLL
               WHERE POLL.line_location_id = p_doc_level_id_tbl(i)
            )
         WHERE rowid = CHARTOROWID(l_rowid_char_tbl(i))
         RETURNING num2
         BULK COLLECT INTO x_doc_id_tbl
         ;

         /* End Bug 3292870 */

         l_progress := '240';

      ELSIF (p_doc_level = g_doc_level_DISTRIBUTION) THEN

         l_progress := '250';
         IF g_debug_stmt THEN
            PO_DEBUG.debug_stmt(l_log_head,l_progress,'distribution');
         END IF;

         /* Start Bug 3292870: Split query to make it compatible with 8i db. */

         FORALL i IN 1 .. p_doc_level_id_tbl.COUNT
         INSERT INTO PO_SESSION_GT ( key, num1 )
         VALUES
         (  l_id_key
         ,  p_doc_level_id_tbl(i)
         )
         RETURNING ROWIDTOCHAR(rowid)
         BULK COLLECT INTO l_rowid_char_tbl
         ;

         FORALL i IN 1 .. p_doc_level_id_tbl.COUNT
         UPDATE PO_SESSION_GT
         SET num2 =
            (
               SELECT DECODE( p_doc_type
                           ,  g_doc_type_RELEASE, POD.po_release_id
                           ,  POD.po_header_id
                           )
               FROM PO_DISTRIBUTIONS_ALL POD
               WHERE POD.po_distribution_id = p_doc_level_id_tbl(i)
            )
         WHERE rowid = CHARTOROWID(l_rowid_char_tbl(i))
         RETURNING num2
         BULK COLLECT INTO x_doc_id_tbl
         ;

         /* End Bug 3292870 */

         l_progress := '260';

      ELSE
         l_progress := '270';
         RAISE g_INVALID_CALL_EXC;
      END IF;

      l_progress := '280';

   END IF;

   l_progress := '290';

END IF;

l_progress := '900';

IF g_debug_stmt THEN
   PO_DEBUG.debug_var(l_log_head,l_progress,'x_doc_id_tbl',x_doc_id_tbl);
   PO_DEBUG.debug_end(l_log_head);
END IF;

EXCEPTION
WHEN OTHERS THEN
   IF g_debug_unexp THEN
      PO_DEBUG.debug_exc(l_log_head,l_progress);
   END IF;
   RAISE;

END get_document_ids;




-------------------------------------------------------------------------------
--Start of Comments
--Name: get_line_ids
--Pre-reqs:
--  None.
--Modifies:
--  None.
--Locks:
--  None.
--Function:
--  Returns the line-level ids for the given ids.
--Parameters:
--IN:
--p_doc_type
--  Document type.  Use the g_doc_type_<> variables, where <> is:
--    REQUISITION
--    PA
--    PO
--    RELEASE
--p_doc_level
--  The type of ids that are being passed.  Use g_doc_level_<>
--    HEADER
--    LINE
--    SHIPMENT
--    DISTRIBUTION
--p_doc_level_id_tbl
--  Ids of the doc level type of which to derive the document header id.
--OUT:
--x_line_id_tbl
--  Line-level ids of the input ids.
--Testing:
--
--End of Comments
-------------------------------------------------------------------------------
PROCEDURE get_line_ids(
   p_doc_type                       IN             VARCHAR2
,  p_doc_level                      IN             VARCHAR2
,  p_doc_level_id_tbl               IN             po_tbl_number
,  x_line_id_tbl                    OUT NOCOPY     po_tbl_number
)
IS

l_log_head     CONSTANT VARCHAR2(100) := g_log_head||'GET_LINE_IDS';
l_progress     VARCHAR2(3) := '000';

l_id_key    NUMBER;

--Bug 3292870
l_rowid_char_tbl     g_rowid_char_tbl;

BEGIN

IF g_debug_stmt THEN
   PO_DEBUG.debug_begin(l_log_head);
   PO_DEBUG.debug_var(l_log_head,l_progress,'p_doc_type', p_doc_type);
   PO_DEBUG.debug_var(l_log_head,l_progress,'p_doc_level', p_doc_level);
   PO_DEBUG.debug_var(l_log_head,l_progress,'p_doc_level_id_tbl', p_doc_level_id_tbl);
END IF;

l_progress := '010';

IF (p_doc_level = g_doc_level_LINE) THEN

   l_progress := '020';
   IF g_debug_stmt THEN
      PO_DEBUG.debug_stmt(l_log_head,l_progress,'lines');
   END IF;

   x_line_id_tbl := p_doc_level_id_tbl;

ELSIF (p_doc_level = g_doc_level_HEADER) THEN

   l_progress := '100';
   IF g_debug_stmt THEN
      PO_DEBUG.debug_stmt(l_log_head,l_progress,'headers');
   END IF;

   ----------------------------------------------------
   --Algorithm:
   --
   -- 1. Load the ids into the scratchpad.
   -- 2. Join to the appropriate tables to bulk collect the line ids.
   ----------------------------------------------------

   ---------------------------------------
   -- PO_SESSION_GT column mapping
   --
   -- num1     doc_level_id
   ---------------------------------------

   l_id_key := get_session_gt_nextval();

   l_progress := '110';

   FORALL i IN 1 .. p_doc_level_id_tbl.COUNT
   INSERT INTO PO_SESSION_GT ( key, num1 )
   VALUES ( l_id_key, p_doc_level_id_tbl(i) )
   ;

   l_progress := '120';

   IF (p_doc_type = g_doc_type_REQUISITION) THEN

      l_progress := '130';
      IF g_debug_stmt THEN
         PO_DEBUG.debug_stmt(l_log_head,l_progress,'req headers');
      END IF;

      -- Gather all of the req line ids below this entity level.

      SELECT PRL.requisition_line_id
      BULK COLLECT INTO x_line_id_tbl
      FROM
         PO_REQUISITION_LINES_ALL PRL
      ,  PO_SESSION_GT IDS
      WHERE PRL.requisition_header_id = IDS.num1
      AND IDS.key = l_id_key
      ;

      l_progress := '140';

   ELSIF (p_doc_type IN (g_doc_type_PO, g_doc_type_PA)) THEN

      l_progress := '150';
      IF g_debug_stmt THEN
         PO_DEBUG.debug_stmt(l_log_head,l_progress,'po/pa headers');
      END IF;

      -- Gather all of the line ids below this entity level.

      SELECT POL.po_line_id
      BULK COLLECT INTO x_line_id_tbl
      FROM
         PO_LINES_ALL POL
      ,  PO_SESSION_GT IDS
      WHERE POL.po_header_id = IDS.num1
      AND IDS.key = l_id_key
      ;

      l_progress := '160';

   ELSE
      l_progress := '180';
      RAISE g_INVALID_CALL_EXC;
   END IF;

   l_progress := '190';

ELSIF (p_doc_level = g_doc_level_SHIPMENT
      AND p_doc_type IN (g_doc_type_PO, g_doc_type_PA, g_doc_type_RELEASE))
THEN

   l_progress := '200';
   IF g_debug_stmt THEN
      PO_DEBUG.debug_stmt(l_log_head,l_progress,'shipments');
   END IF;

   -- We need to get out the line ids in the same ordering as the
   -- input id table.
   -- We can't do a FORALL ... SELECT (PL/SQL limitation),
   -- but we can to a FORALL ... INSERT ... RETURNING.

   ----------------------------------------------------------------
   -- PO_SESSION_GT column mapping
   --
   -- num1     doc level id
   -- num2     line id of num1
   ----------------------------------------------------------------

   l_id_key := get_session_gt_nextval();

   l_progress := '210';

   /* Start Bug 3292870: Split query to make it compatible with 8i db. */

   FORALL i IN 1 .. p_doc_level_id_tbl.COUNT
   INSERT INTO PO_SESSION_GT ( key, num1 )
   VALUES
   (  l_id_key
   ,  p_doc_level_id_tbl(i)
   )
   RETURNING ROWIDTOCHAR(rowid)
   BULK COLLECT INTO l_rowid_char_tbl
   ;

   FORALL i IN 1 .. p_doc_level_id_tbl.COUNT
   UPDATE PO_SESSION_GT
   SET num2 =
      (
         SELECT POLL.po_line_id
         FROM PO_LINE_LOCATIONS_ALL POLL
         WHERE POLL.line_location_id = p_doc_level_id_tbl(i)
      )
   WHERE rowid = CHARTOROWID(l_rowid_char_tbl(i))
   RETURNING num2
   BULK COLLECT INTO x_line_id_tbl
   ;

   /* End Bug 3292870 */

   l_progress := '220';

ELSIF (p_doc_level = g_doc_level_DISTRIBUTION) THEN

   l_progress := '300';
   IF g_debug_stmt THEN
      PO_DEBUG.debug_stmt(l_log_head,l_progress,'distributions');
   END IF;

   -- We need to get out the line ids in the same ordering as the
   -- input id table.
   -- We can't do a FORALL ... SELECT (PL/SQL limitation),
   -- but we can to a FORALL ... INSERT ... RETURNING.

   ----------------------------------------------------------------
   -- PO_SESSION_GT column mapping
   --
   -- num1     doc level id
   -- num2     line id of num1
   ----------------------------------------------------------------

   l_id_key := get_session_gt_nextval();

   l_progress := '310';

   IF (p_doc_type = g_doc_type_REQUISITION) THEN

      l_progress := '320';
      IF g_debug_stmt THEN
         PO_DEBUG.debug_stmt(l_log_head,l_progress,'req');
      END IF;

      /* Start Bug 3292870: Split query to make it compatible with 8i db. */

      FORALL i IN 1 .. p_doc_level_id_tbl.COUNT
      INSERT INTO PO_SESSION_GT ( key, num1 )
      VALUES
      (  l_id_key
      ,  p_doc_level_id_tbl(i)
      )
      RETURNING ROWIDTOCHAR(rowid)
      BULK COLLECT INTO l_rowid_char_tbl
      ;

      FORALL i IN 1 .. p_doc_level_id_tbl.COUNT
      UPDATE PO_SESSION_GT
      SET num2 =
         (
            SELECT PRD.requisition_line_id
            FROM PO_REQ_DISTRIBUTIONS_ALL PRD
            WHERE PRD.distribution_id = p_doc_level_id_tbl(i)
         )
      WHERE rowid = CHARTOROWID(l_rowid_char_tbl(i))
      RETURNING num2
      BULK COLLECT INTO x_line_id_tbl
      ;

      /* End Bug 3292870 */

      l_progress := '330';

   ELSE

      l_progress := '340';
      IF g_debug_stmt THEN
         PO_DEBUG.debug_stmt(l_log_head,l_progress,'not req');
      END IF;

      /* Start Bug 3292870: Split query to make it compatible with 8i db. */

      FORALL i IN 1 .. p_doc_level_id_tbl.COUNT
      INSERT INTO PO_SESSION_GT ( key, num1 )
      VALUES
      (  l_id_key
      ,  p_doc_level_id_tbl(i)
      )
      RETURNING ROWIDTOCHAR(rowid)
      BULK COLLECT INTO l_rowid_char_tbl
      ;

      FORALL i IN 1 .. p_doc_level_id_tbl.COUNT
      UPDATE PO_SESSION_GT
      SET num2 =
         (
            SELECT POD.po_line_id
            FROM PO_DISTRIBUTIONS_ALL POD
            WHERE POD.po_distribution_id = p_doc_level_id_tbl(i)
         )
      WHERE rowid = CHARTOROWID(l_rowid_char_tbl(i))
      RETURNING num2
      BULK COLLECT INTO x_line_id_tbl
      ;

      /* End Bug 3292870 */

      l_progress := '350';

   END IF;

   l_progress := '370';

ELSE
   l_progress := '390';
   RAISE g_INVALID_CALL_EXC;
END IF;

l_progress := '900';

IF g_debug_stmt THEN
   PO_DEBUG.debug_var(l_log_head,l_progress,'x_line_id_tbl',x_line_id_tbl);
   PO_DEBUG.debug_end(l_log_head);
END IF;

EXCEPTION
WHEN OTHERS THEN
   IF g_debug_unexp THEN
      PO_DEBUG.debug_exc(l_log_head,l_progress);
   END IF;
   RAISE;

END get_line_ids;




-------------------------------------------------------------------------------
--Start of Comments
--Name: get_line_location_ids
--Pre-reqs:
--  None.
--Modifies:
--  None.
--Locks:
--  None.
--Function:
--  Retrieves the ids of shipments corresponding to the given doc level.
--Parameters:
--IN:
--p_doc_type
--  Document type.  Use the g_doc_type_<> variables, where <> is:
--    PO
--    PA
--    RELEASE
--    - REQUISITION not currently supported.
--p_doc_level
--  The type of ids that are being passed.  Use g_doc_level_<>
--    HEADER
--    LINE
--    SHIPMENT
--    DISTRIBUTION
--p_doc_level_id_tbl
--  Ids of the doc level type with which to find related shipments.
--OUT:
--x_line_location_id_tbl
--  The ids of the related shipments that were found.
--  If p_doc_level is DISTRIBUTION, the entries in this table
--  will map one-to-one to the input id table.
--Testing:
--
--End of Comments
-------------------------------------------------------------------------------
PROCEDURE get_line_location_ids(
   p_doc_type                       IN             VARCHAR2
,  p_doc_level                      IN             VARCHAR2
,  p_doc_level_id_tbl               IN             po_tbl_number
,  x_line_location_id_tbl           OUT NOCOPY     po_tbl_number
)
IS

l_log_head     CONSTANT VARCHAR2(100) := g_log_head||'GET_LINE_LOCATION_IDS';
l_progress     VARCHAR2(3) := '000';

l_doc_level_id_key      NUMBER;

-- Bug 3292870
l_rowid_char_tbl   g_rowid_char_tbl;

BEGIN

IF g_debug_stmt THEN
   PO_DEBUG.debug_begin(l_log_head);
   PO_DEBUG.debug_var(l_log_head,l_progress,'p_doc_type', p_doc_type);
   PO_DEBUG.debug_var(l_log_head,l_progress,'p_doc_level', p_doc_level);
   PO_DEBUG.debug_var(l_log_head,l_progress,'p_doc_level_id_tbl', p_doc_level_id_tbl);
END IF;

l_progress := '010';

-----------------------------------------------------------------
--Algorithm:
--
-- -  Insert the doc level ids into the scratchpad table.
-- -  Join to the main doc tables against these doc level ids
--       to retrieve the linked line location ids.
-----------------------------------------------------------------

IF (p_doc_level = g_doc_level_SHIPMENT) THEN

   l_progress := '100';
   IF g_debug_stmt THEN
      PO_DEBUG.debug_stmt(l_log_head,l_progress,'shipments');
   END IF;

   x_line_location_id_tbl := p_doc_level_id_tbl;

ELSE

   l_progress := '200';
   IF g_debug_stmt THEN
      PO_DEBUG.debug_stmt(l_log_head,l_progress,'not shipments');
   END IF;

   -- Load the entity ids into the scratchpad.

   ---------------------------------------
   -- PO_SESSION_GT column mapping
   --
   -- num1     document_level_id
   ---------------------------------------

   l_doc_level_id_key := get_session_gt_nextval();

   l_progress := '210';

   FORALL i IN 1 .. p_doc_level_id_tbl.COUNT
   INSERT INTO PO_SESSION_GT ( key, num1 )
   VALUES ( l_doc_level_id_key, p_doc_level_id_tbl(i) )
   ;

   l_progress := '220';

   -- We need to derive the appropriate line location ids for the given
   -- entity id and entity level.

   -- NOT SUPPORTED FOR REQUISITIONS

   IF (  p_doc_type = g_doc_type_RELEASE
         AND p_doc_level = g_doc_level_HEADER)
   THEN

      l_progress := '410';
      IF g_debug_stmt THEN
         PO_DEBUG.debug_stmt(l_log_head,l_progress,'release');
      END IF;

      SELECT POLL.line_location_id
      BULK COLLECT INTO x_line_location_id_tbl
      FROM
         PO_LINE_LOCATIONS_ALL POLL
      ,  PO_SESSION_GT IDS
      WHERE POLL.po_release_id = IDS.num1
      AND IDS.key = l_doc_level_id_key
      ;

      l_progress := '420';

   ELSIF (p_doc_level = g_doc_level_HEADER) THEN

      l_progress := '430';
      IF g_debug_stmt THEN
         PO_DEBUG.debug_stmt(l_log_head,l_progress,'headers');
      END IF;

      SELECT POLL.line_location_id
      BULK COLLECT INTO x_line_location_id_tbl
      FROM
         PO_LINE_LOCATIONS_ALL POLL
      ,  PO_SESSION_GT IDS
      WHERE POLL.po_header_id = IDS.num1
      AND POLL.shipment_type <> g_ship_type_SCHEDULED
      AND POLL.shipment_type <> g_ship_type_BLANKET
         -- don't pick up release shipments for POs/PAs
      AND IDS.key = l_doc_level_id_key
      ;

      l_progress := '440';

   ELSIF (p_doc_level = g_doc_level_LINE) THEN

      l_progress := '450';
      IF g_debug_stmt THEN
         PO_DEBUG.debug_stmt(l_log_head,l_progress,'lines');
      END IF;

      SELECT POLL.line_location_id
      BULK COLLECT INTO x_line_location_id_tbl
      FROM
         PO_LINE_LOCATIONS_ALL POLL
      ,  PO_SESSION_GT IDS
      WHERE POLL.po_line_id = IDS.num1
      AND POLL.shipment_type <> g_ship_type_SCHEDULED
      AND POLL.shipment_type <> g_ship_type_BLANKET
         -- don't pick up release shipments for POs/PAs
      AND IDS.key = l_doc_level_id_key
      ;

      l_progress := '460';

   ELSIF (p_doc_level = g_doc_level_DISTRIBUTION) THEN

      -- We need to get out the header ids in the same ordering as the
      -- input id table.
      -- We can't do a FORALL ... SELECT (PL/SQL limitation),
      -- but we can to a FORALL ... INSERT ... RETURNING.

      ----------------------------------------------------------------
      -- PO_SESSION_GT column mapping
      --
      -- num1     doc level id
      -- num2     line location id of num1
      ----------------------------------------------------------------

      l_progress := '470';
      IF g_debug_stmt THEN
         PO_DEBUG.debug_stmt(l_log_head,l_progress,'distributions');
      END IF;

      /* Start Bug 3292870: Split query to make it compatible with 8i db. */

      FORALL i IN 1 .. p_doc_level_id_tbl.COUNT
      INSERT INTO PO_SESSION_GT ( key, num1 )
      VALUES
      (  l_doc_level_id_key
      ,  p_doc_level_id_tbl(i)
      )
      RETURNING ROWIDTOCHAR(rowid)
      BULK COLLECT INTO l_rowid_char_tbl
      ;

      FORALL i IN 1 .. p_doc_level_id_tbl.COUNT
      UPDATE PO_SESSION_GT
      SET num2 =
         (
            SELECT POD.line_location_id
            FROM PO_DISTRIBUTIONS_ALL POD
            WHERE POD.po_distribution_id = p_doc_level_id_tbl(i)
         )
      WHERE rowid = CHARTOROWID(l_rowid_char_tbl(i))
      RETURNING num2
      BULK COLLECT INTO x_line_location_id_tbl
      ;

      /* End Bug 3292870 */

      l_progress := '480';

   ELSE
      l_progress := '490';
      RAISE g_INVALID_CALL_EXC;
   END IF;

   l_progress := '500';

END IF;

l_progress := '900';

IF g_debug_stmt THEN
   PO_DEBUG.debug_var(l_log_head,l_progress,'x_line_location_id_tbl',x_line_location_id_tbl);
   PO_DEBUG.debug_end(l_log_head);
END IF;

EXCEPTION
WHEN OTHERS THEN
   IF g_debug_unexp THEN
      PO_DEBUG.debug_exc(l_log_head,l_progress);
   END IF;
   RAISE;

END get_line_location_ids;




-------------------------------------------------------------------------------
--Start of Comments
--Name: get_distribution_ids
--Pre-reqs:
--  None.
--Modifies:
--  None.
--Locks:
--  None.
--Function:
--  Retrieves the ids of distributions below the given doc level.
--Parameters:
--IN:
--p_doc_type
--  Document type.  Use the g_doc_type_<> variables, where <> is:
--    REQUISITION
--    PA
--    PO
--    RELEASE
--p_doc_level
--  The type of ids that are being passed.  Use g_doc_level_<>
--    HEADER
--    LINE
--    SHIPMENT
--    DISTRIBUTION
--p_doc_level_id_tbl
--  Ids of the doc level type with which to find related distributions.
--OUT:
--x_dist_id_tbl
--  The ids of the related distributions that were found.
--Testing:
--
--End of Comments
-------------------------------------------------------------------------------
PROCEDURE get_distribution_ids(
   p_doc_type                       IN             VARCHAR2
,  p_doc_level                      IN             VARCHAR2
,  p_doc_level_id_tbl               IN             po_tbl_number
,  x_distribution_id_tbl            OUT NOCOPY     po_tbl_number
)
IS

l_log_head     CONSTANT VARCHAR2(100) := g_log_head||'GET_DISTRIBUTION_IDS';
l_progress     VARCHAR2(3) := '000';

l_doc_level_id_key      NUMBER;

BEGIN

IF g_debug_stmt THEN
   PO_DEBUG.debug_begin(l_log_head);
   PO_DEBUG.debug_var(l_log_head,l_progress,'p_doc_type', p_doc_type);
   PO_DEBUG.debug_var(l_log_head,l_progress,'p_doc_level', p_doc_level);
   PO_DEBUG.debug_var(l_log_head,l_progress,'p_doc_level_id_tbl', p_doc_level_id_tbl);
END IF;

l_progress := '010';

-----------------------------------------------------------------
--Algorithm:
--
-- -  Insert the doc level ids into the scratchpad table.
-- -  Join to the main doc tables against these doc level ids
--       to retrieve the linked distribution ids.
-----------------------------------------------------------------

IF (p_doc_level = g_doc_level_DISTRIBUTION) THEN

   l_progress := '100';
   IF g_debug_stmt THEN
      PO_DEBUG.debug_stmt(l_log_head,l_progress,'distributions');
   END IF;

   x_distribution_id_tbl := p_doc_level_id_tbl;

ELSE

   l_progress := '200';
   IF g_debug_stmt THEN
      PO_DEBUG.debug_stmt(l_log_head,l_progress,'not dists');
   END IF;

   -- Load the entity ids into the scratchpad.

   ---------------------------------------
   -- PO_SESSION_GT column mapping
   --
   -- num1     document_level_id
   ---------------------------------------

   l_doc_level_id_key := get_session_gt_nextval();

   l_progress := '210';

   FORALL i IN 1 .. p_doc_level_id_tbl.COUNT
   INSERT INTO PO_SESSION_GT ( key, num1 )
   VALUES ( l_doc_level_id_key, p_doc_level_id_tbl(i) )
   ;

   l_progress := '220';

   -- We need to derive the appropriate distribution ids for the given
   -- entity id and entity level.

   IF (p_doc_type = g_doc_type_REQUISITION) THEN

      l_progress := '300';
      IF g_debug_stmt THEN
         PO_DEBUG.debug_stmt(l_log_head,l_progress,'requisition');
      END IF;

      -- Gather all of the req distribution ids below this entity level.

      IF (p_doc_level = g_doc_level_HEADER) THEN

         l_progress := '310';
         IF g_debug_stmt THEN
            PO_DEBUG.debug_stmt(l_log_head,l_progress,'headers');
         END IF;

         SELECT PRD.distribution_id
         BULK COLLECT INTO x_distribution_id_tbl
         FROM
            PO_REQUISITION_LINES_ALL PRL
         ,  PO_REQ_DISTRIBUTIONS_ALL PRD
         ,  PO_SESSION_GT IDS
         WHERE PRL.requisition_header_id = IDS.num1
         AND PRD.requisition_line_id = PRL.requisition_line_id
         AND IDS.key = l_doc_level_id_key
         ;

         l_progress := '320';

      ELSIF (p_doc_level = g_doc_level_LINE) THEN

         l_progress := '330';
         IF g_debug_stmt THEN
            PO_DEBUG.debug_stmt(l_log_head,l_progress,'lines');
         END IF;

         SELECT PRD.distribution_id
         BULK COLLECT INTO x_distribution_id_tbl
         FROM
            PO_REQ_DISTRIBUTIONS_ALL PRD
         ,  PO_SESSION_GT IDS
         WHERE PRD.requisition_line_id = IDS.num1
         AND IDS.key = l_doc_level_id_key
         ;

         l_progress := '340';

      ELSE
         l_progress := '370';
         RAISE g_INVALID_CALL_EXC;
      END IF;

      l_progress := '390';

   ELSE -- PO, PA, Release, etc.

      l_progress := '400';
      IF g_debug_stmt THEN
         PO_DEBUG.debug_stmt(l_log_head,l_progress,'not req');
      END IF;

      -- Gather all of the distribution ids below this entity level.

      IF (  p_doc_type = g_doc_type_RELEASE
            AND p_doc_level = g_doc_level_HEADER
         )
      THEN

         l_progress := '410';
         IF g_debug_stmt THEN
            PO_DEBUG.debug_stmt(l_log_head,l_progress,'release');
         END IF;

         SELECT POD.po_distribution_id
         BULK COLLECT INTO x_distribution_id_tbl
         FROM
            PO_DISTRIBUTIONS_ALL POD
         ,  PO_SESSION_GT IDS
         WHERE POD.po_release_id = IDS.num1
         AND IDS.key = l_doc_level_id_key
         ;

         l_progress := '420';

      ELSIF (p_doc_level = g_doc_level_HEADER) THEN

         l_progress := '430';
         IF g_debug_stmt THEN
            PO_DEBUG.debug_stmt(l_log_head,l_progress,'headers');
         END IF;

         SELECT POD.po_distribution_id
         BULK COLLECT INTO x_distribution_id_tbl
         FROM
            PO_DISTRIBUTIONS_ALL POD
         ,  PO_SESSION_GT IDS
         WHERE POD.po_header_id = IDS.num1
         AND POD.po_release_id IS NULL
         -- Don't pick up Release distributions when acting on a PPO/BPA/GA.
         -- Not using distribution_type due to dependency issues.
         AND IDS.key = l_doc_level_id_key
         ;

         l_progress := '440';

      ELSIF (p_doc_level = g_doc_level_LINE) THEN

         l_progress := '450';
         IF g_debug_stmt THEN
            PO_DEBUG.debug_stmt(l_log_head,l_progress,'lines');
         END IF;

         SELECT POD.po_distribution_id
         BULK COLLECT INTO x_distribution_id_tbl
         FROM
            PO_DISTRIBUTIONS_ALL POD
         ,  PO_SESSION_GT IDS
         WHERE POD.po_line_id = IDS.num1
         AND POD.po_release_id IS NULL
         -- Don't pick up SR distributions when acting on a PPO.
         -- Not using distribution_type due to dependency issues.
         AND IDS.key = l_doc_level_id_key
         ;

         l_progress := '460';

      ELSIF (p_doc_level = g_doc_level_SHIPMENT) THEN

         l_progress := '470';
         IF g_debug_stmt THEN
            PO_DEBUG.debug_stmt(l_log_head,l_progress,'shipments');
         END IF;

         SELECT POD.po_distribution_id
         BULK COLLECT INTO x_distribution_id_tbl
         FROM
            PO_DISTRIBUTIONS_ALL POD
         ,  PO_SESSION_GT IDS
         WHERE POD.line_location_id = IDS.num1
         AND IDS.key = l_doc_level_id_key
         ;

         l_progress := '480';

      ELSE
         l_progress := '490';
         RAISE g_INVALID_CALL_EXC;
      END IF;

      l_progress := '500';

   END IF; -- Req vs. PO/PA/Release

   l_progress := '600';

END IF; -- entity type <> DISTRIBUTION

l_progress := '900';

IF g_debug_stmt THEN
   PO_DEBUG.debug_var(l_log_head,l_progress,'x_distribution_id_tbl',x_distribution_id_tbl);
   PO_DEBUG.debug_end(l_log_head);
END IF;

EXCEPTION
WHEN OTHERS THEN
   IF g_debug_unexp THEN
      PO_DEBUG.debug_exc(l_log_head,l_progress);
   END IF;
   RAISE;

END get_distribution_ids;




------------------------------------------------------------------------------
--Start of Comments
--Name: is_encumbrance_on
--Pre-reqs:
--  Org context may need to be set prior to calling.
--Modifies:
--  None.
--Locks:
--  None.
--Function:
--  This procedure checks if encumbrance is ON for a document type in a
--  specified org.
--Parameters:
--IN:
--p_doc_type
--  The type of doc to check.  Use g_doc_type_<> where <> is:
--    REQUISITION       - req enc
--    PO                - purch enc
--    RELEASE           - purch enc
--    PA                - both req and purch enc are on
--    ANY               - either req or purch enc or both are on
--p_org_id
--  The org id to check the encumbrance status in.
--  If NULL is passed, the org context is assumed to have been set
--  by the caller.
--Returns:
--  FALSE   the encumbrance for p_doc_type is NOT on
--  TRUE    the encumbrance for p_doc_type is on
--Testing:
--
--End of Comments
-------------------------------------------------------------------------------
FUNCTION is_encumbrance_on(
   p_doc_type                       IN             VARCHAR2
,  p_org_id                         IN             NUMBER
)  RETURN BOOLEAN
IS

l_log_head     CONSTANT VARCHAR2(100) := g_log_head||'IS_ENCUMBRANCE_ON';
l_progress     VARCHAR2(3) := '000';

l_req_enc_flag       FINANCIALS_SYSTEM_PARAMS_ALL.req_encumbrance_flag%TYPE;
l_purch_enc_flag     FINANCIALS_SYSTEM_PARAMS_ALL.purch_encumbrance_flag%TYPE;

l_req_encumbrance_on    BOOLEAN;
l_po_encumbrance_on  BOOLEAN;

l_enc_on    BOOLEAN;

BEGIN

IF g_debug_stmt THEN
   PO_DEBUG.debug_begin(l_log_head);
   PO_DEBUG.debug_var(l_log_head,l_progress,'p_doc_type', p_doc_type);
   PO_DEBUG.debug_var(l_log_head,l_progress,'p_org_id', p_org_id);
END IF;

l_progress := '010';

-- Get the FSP encumbrance status values.
-- If org id is not passed in, use the org-striped table,
-- otherwise use the _ALL table.

IF (p_org_id IS NULL) THEN

   l_progress := '020';
   IF g_debug_stmt THEN
      PO_DEBUG.debug_stmt(l_log_head,l_progress,'using FINANCIALS_SYSTEM_PARAMETERS');
   END IF;

   SELECT FSP.req_encumbrance_flag, FSP.purch_encumbrance_flag
   INTO l_req_enc_flag, l_purch_enc_flag
   FROM FINANCIALS_SYSTEM_PARAMETERS FSP
   ;

   l_progress := '030';

ELSE

   l_progress := '040';
   IF g_debug_stmt THEN
      PO_DEBUG.debug_stmt(l_log_head,l_progress,'using FINANCIALS_SYSTEM_PARAMS_ALL');
   END IF;

   SELECT FSP.req_encumbrance_flag, FSP.purch_encumbrance_flag
   INTO l_req_enc_flag, l_purch_enc_flag
   FROM FINANCIALS_SYSTEM_PARAMS_ALL FSP
   WHERE FSP.org_id = p_org_id
   ;

   l_progress := '050';

END IF;

l_progress := '060';

IF g_debug_stmt THEN
   PO_DEBUG.debug_var(l_log_head,l_progress,'l_req_enc_flag',l_req_enc_flag);
   PO_DEBUG.debug_var(l_log_head,l_progress,'l_purch_enc_flag',l_purch_enc_flag);
END IF;

-- Set the vars for encumbrance checking.

IF (l_req_enc_flag = 'Y') THEN
   l_req_encumbrance_on := TRUE;
ELSE
   l_req_encumbrance_on := FALSE;
END IF;

l_progress := '070';

IF (l_purch_enc_flag = 'Y') THEN
   l_po_encumbrance_on := TRUE;
ELSE
   l_po_encumbrance_on := FALSE;
END IF;

l_progress := '080';

IF g_debug_stmt THEN
   PO_DEBUG.debug_var(l_log_head,l_progress,'l_req_encumbrance_on',l_req_encumbrance_on);
   PO_DEBUG.debug_var(l_log_head,l_progress,'l_po_encumbrance_on',l_po_encumbrance_on);
END IF;

-- Set the return value for the appropriate doc type.

IF (p_doc_type = g_doc_type_REQUISITION) THEN

   l_progress := '100';

   l_enc_on := l_req_encumbrance_on;

ELSIF (p_doc_type IN (g_doc_type_PO, g_doc_type_RELEASE)) THEN

   l_progress := '110';

   l_enc_on := l_po_encumbrance_on;

ELSIF (p_doc_type = g_doc_type_PA) THEN

   l_progress := '120';

   l_enc_on := (l_req_encumbrance_on AND l_po_encumbrance_on);

ELSIF (p_doc_type = g_doc_type_ANY) THEN

   l_progress := '130';

   l_enc_on := (l_req_encumbrance_on OR l_po_encumbrance_on);

ELSE
   l_progress := '170';
   RAISE g_INVALID_CALL_EXC;
END IF;

l_progress := '900';

IF g_debug_stmt THEN
   PO_DEBUG.debug_var(l_log_head,l_progress,'l_enc_on',l_enc_on);
   PO_DEBUG.debug_end(l_log_head);
END IF;

RETURN(l_enc_on);

EXCEPTION
WHEN OTHERS THEN
   IF g_debug_unexp THEN
      PO_DEBUG.debug_var(l_log_head,l_progress,'l_enc_on',l_enc_on);
      PO_DEBUG.debug_exc(l_log_head,l_progress);
   END IF;
   RAISE;

END is_encumbrance_on;


-----------------------------------------------------------------<SERVICES FPJ>
-------------------------------------------------------------------------------
--Start of Comments
--Name: get_translated_text
--Pre-reqs:
--  None.
--Modifies:
--  None.
--Locks:
--  None.
--Function:
--  This function takes in a message name and token-value pairs and returns
--  the translated text as a VARCHAR2 String.
--Parameters:
--IN:
--p_message_name
--  Name of message in Message Dictionary.
--p_token1
--  Name of token variable in the message (only applies if a token exists).
--p_value1
--  Value to subsitute for token (only applies if a token exists).
--Returns:
--  VARCHAR2 - The translated and token-substituted message.
--Testing:
--  None.
--End of Comments
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
FUNCTION get_translated_text
(   p_message_name    IN  VARCHAR2
,   p_token1          IN  VARCHAR2 -- := NULL
,   p_value1          IN  VARCHAR2 -- := NULL
,   p_token2          IN  VARCHAR2 -- := NULL
,   p_value2          IN  VARCHAR2 -- := NULL
,   p_token3          IN  VARCHAR2 -- := NULL
,   p_value3          IN  VARCHAR2 -- := NULL
,   p_token4          IN  VARCHAR2 -- := NULL
,   p_value4          IN  VARCHAR2 -- := NULL
,   p_token5          IN  VARCHAR2 -- := NULL
,   p_value5          IN  VARCHAR2 -- := NULL
) RETURN VARCHAR2
IS
    l_application_name        VARCHAR2(3) := 'PO';

BEGIN

    ---------------------------------------------------------------------------
    -- Set Message on Stack ---------------------------------------------------
    ---------------------------------------------------------------------------

    FND_MESSAGE.set_name(l_application_name, p_message_name);

    ---------------------------------------------------------------------------
    -- Substitute Tokens ------------------------------------------------------
    ---------------------------------------------------------------------------

    IF ( p_token1 IS NOT NULL ) THEN
        FND_MESSAGE.set_token(p_token1, p_value1);
    END IF;

    IF ( p_token2 IS NOT NULL ) THEN
        FND_MESSAGE.set_token(p_token2, p_value2);
    END IF;

    IF ( p_token3 IS NOT NULL ) THEN
        FND_MESSAGE.set_token(p_token3, p_value3);
    END IF;

    IF ( p_token4 IS NOT NULL ) THEN
        FND_MESSAGE.set_token(p_token4, p_value4);
    END IF;

    IF ( p_token5 IS NOT NULL ) THEN
        FND_MESSAGE.set_token(p_token5, p_value5);
    END IF;

    ---------------------------------------------------------------------------
    -- Return Translated Message ----------------------------------------------
    ---------------------------------------------------------------------------

    return (FND_MESSAGE.get);

    ---------------------------------------------------------------------------

END get_translated_text;

--<Shared Proc FPJ START>
-------------------------------------------------------------------------------
--Start of Comments
--Name: CHECK_DOC_NUMBER_UNIQUE
--Pre-reqs:
--  None
--Modifies:
--  None.
--Locks:
--  None.
--Function:
--   Returns boolean indicating whether a segment1 value with given type lookup
--   code passed in is unique in given operating unit. The uniqueness test is
--   done in PO trasaction and history tables. The uniqueness test spans Sourcing's
--   transaction tables also.
--Parameters:
--IN:
--  p_segment1
--     The doc number whose uniqueness needs to be tested
--  p_org_id
--     The operating unit where the uniqueness needs to be tested
--  p_type_lookup_code
--     The lookup code of the document. Valid values are 'STANDARD', 'PLANNED',
--     'CONTRACT','BLANKET', 'RFQ', 'QUOTATION'
--Testing:
--  None
--End of Comments
---------------------------------------------------------------------------
 FUNCTION Check_Doc_Number_Unique(p_Segment1 In VARCHAR2,
                                  p_org_id IN VARCHAR2,
			          p_Type_lookup_code IN VARCHAR2)
 RETURN boolean  is

 l_Unique             boolean;
 l_non_unique_seg1    Varchar2(20);
 l_api_name           CONSTANT VARCHAR2(30) := 'Check_Doc_Number_Unique';
 l_progress           varchar2(3) := '000';
 l_duplicate_exists   varchar2(1);
 l_pon_install_status varchar2(1);
 l_status             varchar2(10);
 BEGIN

    IF p_Type_lookup_code NOT IN ('RFQ', 'QUOTATION') THEN

        l_progress := '010';

        SELECT 'N'
        into l_duplicate_exists
        from sys.dual
        where not exists
          (SELECT 'po number is not unique'
           FROM   po_headers_all ph
           WHERE  ph.segment1 = p_segment1
           AND    ph.type_lookup_code IN
                  ('STANDARD','CONTRACT','BLANKET','PLANNED')
           AND    nvl(ph.org_id, -99) = nvl(p_org_id, -99));

        l_Progress := '020';

        SELECT 'N'
        into l_duplicate_exists
        from sys.dual
        where not exists
           (SELECT 'po number is not unique'
           FROM   po_history_pos_all ph
           WHERE  ph.segment1 = p_segment1
           AND    ph.type_lookup_code IN
                ('STANDARD','CONTRACT','BLANKET','PLANNED')
           AND    nvl(ph.org_id, -99) = nvl(p_org_id, -99));

      --Get the install status of Sourcing
      po_setup_s1.get_sourcing_startup(l_pon_install_status);

      if nvl(l_pon_install_status,'N') ='I' then
	   if p_Type_lookup_code in ('STANDARD','BLANKET') then
	      pon_auction_po_pkg.check_unique(p_org_id,p_segment1,l_status);
	      if l_status = 'SUCCESS' then
		    l_Unique :=TRUE;
          else
		    raise no_data_found;
          end if;
        end if;
      end if;

        l_Unique:= TRUE;

       return(l_Unique);

    ELSIF  (p_Type_lookup_code = 'RFQ') THEN

	-- RFQ specific processing

        l_progress := '030';

        SELECT 'N'
        into l_duplicate_exists
        from sys.dual
        where not exists
          (SELECT 'rfq number is not unique'
           FROM   po_headers_all ph
           WHERE  ph.segment1 = p_segment1
           AND    ph.type_lookup_code = 'RFQ'
           AND    nvl(ph.org_id, -99) = nvl(p_org_id, -99));

        l_Progress := '040';

        SELECT 'N'
        into l_duplicate_exists
        from sys.dual
        where not exists
           (SELECT 'rfq number is not unique'
           FROM   po_history_pos_all ph
           WHERE  ph.segment1 = p_segment1
           AND    ph.type_lookup_code = 'RFQ'
           AND    nvl(ph.org_id, -99) = nvl(p_org_id, -99));

        l_Unique:= TRUE;

        return(l_Unique);

    ELSIF  (p_Type_lookup_code = 'QUOTATION') THEN

	-- Quotation specific processing

        l_progress := '050';

        SELECT 'N'
        into l_duplicate_exists
        from sys.dual
        where not exists
          (SELECT 'quote number is not unique'
           FROM   po_headers_all ph
           WHERE  ph.segment1 = p_segment1
           AND    ph.type_lookup_code = 'QUOTATION'
           AND    nvl(ph.org_id, -99) = nvl(p_org_id, -99));

        l_Progress := '060';

        SELECT 'N'
        into l_duplicate_exists
        from sys.dual
        where not exists
           (SELECT 'quote number is not unique'
           FROM   po_history_pos_all ph
           WHERE  ph.segment1 = p_segment1
           AND    ph.type_lookup_code = 'QUOTATION'
           AND    nvl(ph.org_id, -99) = nvl(p_org_id, -99));

        l_Unique:= TRUE;

        return(l_Unique);

    END IF;

EXCEPTION
        WHEN NO_DATA_FOUND THEN
             --Bug 3417966 No need to set the message here
             --fnd_message.set_name('PO', 'PO_ALL_ENTER_UNIQUE');
             l_Unique:= FALSE;
             RETURN(l_Unique);
        WHEN OTHERS THEN
            l_unique := FALSE;
            IF fnd_msg_pub.check_msg_level (fnd_msg_pub.g_msg_lvl_unexp_error)
            THEN
              fnd_msg_pub.add_exc_msg (g_pkg_name, l_api_name,
                  SUBSTRB (SQLERRM , 1 , 200) || ' at location ' || l_progress);
            END IF;
            RETURN(l_Unique);
END Check_doc_number_Unique;

--------------------------------------------------------------------------------
--Start of Comments
--Name: check_inv_org_in_sob
--Pre-reqs:
--  None.
--Modifies:
--  FND_LOG
--  FND_MSG_PUB
--Locks:
--  None.
--Function:
--  Checks if p_inv_org_id is in the Set of Books p_sob_id. If p_sob_id
--  is NULL, then defaults p_sob_id to be the current OU's SOB.  Appends to the
--  API message list upon error.
--Parameters:
--IN:
--p_inv_org_id
--  The inventory organization ID.
--p_sob_id
--  The set of books ID, or NULL to default the current OU's SOB.
--OUT:
--x_return_status
--  FND_API.g_ret_sts_success - if the procedure completed successfully
--  FND_API.g_ret_sts_unexp_error - unexpected error occurred
--x_in_sob
--  TRUE if p_inv_org_id is within the set of books p_sob_id.
--  FALSE otherwise.
--End of Comments
--------------------------------------------------------------------------------
PROCEDURE check_inv_org_in_sob
(
    x_return_status OUT NOCOPY VARCHAR2,
    p_inv_org_id    IN  NUMBER,
    p_sob_id        IN  NUMBER,
    x_in_sob        OUT NOCOPY BOOLEAN
)
IS
    l_progress VARCHAR2(3);
    l_in_sob VARCHAR2(1);
BEGIN
    l_progress := '000';
    x_return_status := FND_API.g_ret_sts_success;

    IF g_debug_stmt THEN
        PO_DEBUG.debug_stmt
           (p_log_head => g_log_head||'check_inv_org_in_sob',
            p_token    => 'invoked',
            p_message  => 'inv org ID: '||p_inv_org_id||' sob ID: '||p_sob_id);
    END IF;

    BEGIN

        IF (p_sob_id IS NULL) THEN
            l_progress := '010';

            --SQL What: Check if inv org p_inv_org_id is in current set of books
            --SQL Why: Outcome determines output parameter x_in_sob
            SELECT 'Y'
              INTO l_in_sob
              FROM financials_system_parameters fsp,
                   hr_organization_information hoi,
                   mtl_parameters mp
             WHERE mp.organization_id = p_inv_org_id
               AND mp.organization_id = hoi.organization_id
               AND hoi.org_information_context = 'Accounting Information'
               AND hoi.org_information1 = TO_CHAR(fsp.set_of_books_id);
        ELSE
            l_progress := '020';

            --SQL What: Check if inv org p_inv_org_id is in SOB p_sob_id
            --SQL Why: Outcome determines output parameter x_in_sob
            SELECT 'Y'
              INTO l_in_sob
              FROM hr_organization_information hoi,
                   mtl_parameters mp
             WHERE mp.organization_id = p_inv_org_id
               AND mp.organization_id = hoi.organization_id
               AND hoi.org_information_context = 'Accounting Information'
               AND hoi.org_information1 = TO_CHAR(p_sob_id);
        END IF;

        -- Successful select means inv org is in the SOB.
        x_in_sob := TRUE;

    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            l_progress := '030';
            x_in_sob := FALSE;
    END;

    IF g_debug_stmt THEN
        PO_DEBUG.debug_var
           (p_log_head => g_log_head||'check_inv_org_in_sob',
            p_progress => l_progress,
            p_name     => 'x_in_sob',
            p_value    => x_in_sob);
    END IF;

EXCEPTION
    WHEN OTHERS THEN
        x_return_status := FND_API.g_ret_sts_unexp_error;
        x_in_sob := FALSE;
        FND_MSG_PUB.add_exc_msg(p_pkg_name       => g_pkg_name,
                                p_procedure_name => 'check_inv_org_in_sob',
                                p_error_text     => 'Progress: '||l_progress||
                                           ' Error: '||SUBSTRB(SQLERRM,1,215));
        IF g_debug_unexp THEN
            PO_DEBUG.debug_exc
               (p_log_head => g_log_head||'check_inv_org_in_sob',
                p_progress => l_progress);
        END IF;
END check_inv_org_in_sob;

--------------------------------------------------------------------------------
--Start of Comments
--Name: get_inv_org_ou_id
--Pre-reqs:
--  None.
--Modifies:
--  FND_LOG
--  FND_MSG_PUB
--Locks:
--  None.
--Function:
--  Gets the operating unit associated with p_inv_org_id.  If p_inv_org_id is
--  NULL, then just return a NULL x_ou_id. Appends to the API message list upon
--  error.
--Parameters:
--IN:
--p_inv_org_id
--  The inventory organization ID.
--OUT:
--x_return_status
--  FND_API.g_ret_sts_success - if the procedure completed successfully
--  FND_API.g_ret_sts_unexp_error - unexpected error occurred
--x_ou_id
--  The operating unit ID associated with the inventory org p_inv_org_id. This
--  will be NULL if p_inv_org_id is passed in as NULL.
--End of Comments
--------------------------------------------------------------------------------
PROCEDURE get_inv_org_ou_id
(
    x_return_status OUT NOCOPY VARCHAR2,
    p_inv_org_id    IN  NUMBER,
    x_ou_id         OUT NOCOPY NUMBER
)
IS
    l_progress VARCHAR2(3);
BEGIN
    l_progress := '000';
    x_return_status := FND_API.g_ret_sts_success;

    IF g_debug_stmt THEN
        PO_DEBUG.debug_stmt
           (p_log_head => g_log_head||'get_inv_org_ou_id',
            p_token    => 'invoked',
            p_message  => 'inv org ID: '||p_inv_org_id);
    END IF;

    --< Bug 3370735 Start >
    IF (p_inv_org_id IS NULL) THEN
        -- Null out x_ou_id and return when the inv org ID is NULL
        x_ou_id := NULL;
        RETURN;
    END IF;
    --< Bug 3370735 Start >

    l_progress := '010';

    --SQL What: Get the operating unit associated with p_inv_org_id
    --SQL Why: Return value as output parameter x_ou_id
    SELECT TO_NUMBER(hoi.org_information3)
      INTO x_ou_id
      FROM hr_organization_information hoi,
           mtl_parameters mp
     WHERE mp.organization_id = p_inv_org_id
       AND mp.organization_id = hoi.organization_id
       AND hoi.org_information_context = 'Accounting Information';

    l_progress := '020';

    IF g_debug_stmt THEN
        PO_DEBUG.debug_var
           (p_log_head => g_log_head||'get_inv_org_ou_id',
            p_progress => l_progress,
            p_name     => 'x_ou_id',
            p_value    => x_ou_id);
    END IF;

EXCEPTION
    WHEN OTHERS THEN
        x_return_status := FND_API.g_ret_sts_unexp_error;
        FND_MSG_PUB.add_exc_msg(p_pkg_name       => g_pkg_name,
                                p_procedure_name => 'get_inv_org_ou_id',
                                p_error_text     => 'Progress: '||l_progress||
                                           ' Error: '||SUBSTRB(SQLERRM,1,215));
        IF g_debug_unexp THEN
            PO_DEBUG.debug_exc
               (p_log_head => g_log_head||'get_inv_org_ou_id',
                p_progress => l_progress);
        END IF;
END get_inv_org_ou_id;

--------------------------------------------------------------------------------
--Start of Comments
--Name: get_inv_org_sob_id
--Pre-reqs:
--  None.
--Modifies:
--  FND_LOG
--  FND_MSG_PUB
--Locks:
--  None.
--Function:
--  Gets the set of books ID associated with p_inv_org_id.  Appends to the
--  API message list upon error.
--Parameters:
--IN:
--p_inv_org_id
--  The inventory organization ID.
--OUT:
--x_return_status
--  FND_API.g_ret_sts_success - if the procedure completed successfully
--  FND_API.g_ret_sts_unexp_error - unexpected error occurred
--x_sob_id
--  The set of books ID associated with the inventory org p_inv_org_id.
--End of Comments
--------------------------------------------------------------------------------
PROCEDURE get_inv_org_sob_id
(
    x_return_status OUT NOCOPY VARCHAR2,
    p_inv_org_id    IN  NUMBER,
    x_sob_id        OUT NOCOPY NUMBER
)
IS
    l_progress VARCHAR2(3);
BEGIN
    l_progress := '000';
    x_return_status := FND_API.g_ret_sts_success;

    IF g_debug_stmt THEN
        PO_DEBUG.debug_stmt
           (p_log_head => g_log_head||'get_inv_org_sob_id',
            p_token    => 'invoked',
            p_message  => 'inv org ID: '||p_inv_org_id);
    END IF;

    l_progress := '010';

    --SQL What: Get the set of books ID associated with p_inv_org_id
    --SQL Why: Return value as output parameter x_sob_id
    SELECT TO_NUMBER(hoi.org_information1)
      INTO x_sob_id
      FROM hr_organization_information hoi,
           mtl_parameters mp
     WHERE mp.organization_id = p_inv_org_id
       AND mp.organization_id = hoi.organization_id
       AND hoi.org_information_context = 'Accounting Information';

    l_progress := '020';

    IF g_debug_stmt THEN
        PO_DEBUG.debug_var
           (p_log_head => g_log_head||'get_inv_org_sob_id',
            p_progress => l_progress,
            p_name     => 'x_sob_id',
            p_value    => x_sob_id);
    END IF;

EXCEPTION
    WHEN OTHERS THEN
        x_return_status := FND_API.g_ret_sts_unexp_error;
        FND_MSG_PUB.add_exc_msg(p_pkg_name       => g_pkg_name,
                                p_procedure_name => 'get_inv_org_sob_id',
                                p_error_text     => 'Progress: '||l_progress||
                                           ' Error: '||SUBSTRB(SQLERRM,1,215));
        IF g_debug_unexp THEN
            PO_DEBUG.debug_exc
               (p_log_head => g_log_head||'get_inv_org_sob_id',
                p_progress => l_progress);
        END IF;
END get_inv_org_sob_id;

--------------------------------------------------------------------------------
--Start of Comments
--Name: get_inv_org_info
--Pre-reqs:
--  None.
--Modifies:
--  FND_LOG
--  FND_MSG_PUB
--Locks:
--  None.
--Function:
--  Gets the following information associated with p_inv_org_id:
--      business group ID
--      set of books ID
--      chart of accounts ID
--      operating unit ID
--      legal entity ID
--  Appends to the API message list upon error.
--Parameters:
--IN:
--p_inv_org_id
--  The inventory organization ID.
--OUT:
--x_return_status
--  FND_API.g_ret_sts_success - if the procedure completed successfully
--  FND_API.g_ret_sts_unexp_error - unexpected error occurred
--x_business_group_id
--  The business group ID associated with the inventory org p_inv_org_id.
--x_set_of_books_id
--  The set of books ID associated with the inventory org p_inv_org_id.
--x_chart_of_accounts_id
--  The chart of accounts ID associated with the inventory org p_inv_org_id.
--x_operating_unit_id
--  The operating unit ID associated with the inventory org p_inv_org_id.
--x_legal_entity_id
--  The legal entity ID associated with the inventory org p_inv_org_id.
--End of Comments
--------------------------------------------------------------------------------
PROCEDURE get_inv_org_info
(
    x_return_status         OUT NOCOPY VARCHAR2,
    p_inv_org_id            IN  NUMBER,
    x_business_group_id     OUT NOCOPY NUMBER,
    x_set_of_books_id       OUT NOCOPY NUMBER,
    x_chart_of_accounts_id  OUT NOCOPY NUMBER,
    x_operating_unit_id     OUT NOCOPY NUMBER,
    x_legal_entity_id       OUT NOCOPY NUMBER
)
IS
    l_progress VARCHAR2(3);
BEGIN
    l_progress := '000';
    x_return_status := FND_API.g_ret_sts_success;

    IF g_debug_stmt THEN
        PO_DEBUG.debug_stmt
           (p_log_head => g_log_head||'get_inv_org_info',
            p_token    => 'invoked',
            p_message  => 'inv org ID: '||p_inv_org_id);
    END IF;

    l_progress := '010';

    --SQL What: Get the important ID's associated with p_inv_org_id. These ID's
    --  are the same as those returned by ORG_ORGANIZATION_DEFINITIONS.
    --SQL Why: Return value of ID's as output parameters to procedure.
    SELECT haou.business_group_id,
           gsob.set_of_books_id,
           gsob.chart_of_accounts_id,
           TO_NUMBER(hoi.org_information3),
           TO_NUMBER(hoi.org_information2)
      INTO x_business_group_id,
           x_set_of_books_id,
           x_chart_of_accounts_id,
           x_operating_unit_id,
           x_legal_entity_id
      FROM hr_organization_information hoi,
           hr_all_organization_units haou,
           mtl_parameters mp,
           gl_sets_of_books gsob
     WHERE mp.organization_id = p_inv_org_id
       AND mp.organization_id = haou.organization_id
       AND haou.organization_id = hoi.organization_id
       AND hoi.org_information_context = 'Accounting Information'
       AND TO_NUMBER(hoi.org_information1) = gsob.set_of_books_id;

    l_progress := '020';

    IF g_debug_stmt THEN
        PO_DEBUG.debug_stmt
           (p_log_head => g_log_head||'get_inv_org_info',
            p_token    => 'output',
            p_message  => 'bg ID: '||x_business_group_id||' sob ID: '||
                x_set_of_books_id||' coa ID: '||x_chart_of_accounts_id||
                ' ou ID: '||x_operating_unit_id||' le ID: '||
                x_legal_entity_id);
    END IF;

EXCEPTION
    WHEN OTHERS THEN
        x_return_status := FND_API.g_ret_sts_unexp_error;
        FND_MSG_PUB.add_exc_msg(p_pkg_name       => g_pkg_name,
                                p_procedure_name => 'get_inv_org_info',
                                p_error_text     => 'Progress: '||l_progress||
                                           ' Error: '||SUBSTRB(SQLERRM,1,215));
        IF g_debug_unexp THEN
            PO_DEBUG.debug_exc
               (p_log_head => g_log_head||'get_inv_org_info',
                p_progress => l_progress);
        END IF;
END get_inv_org_info;

--<Shared Proc FPJ END>



--------------------------------------------------------------------------------
--Start of Comments
--Name: get_open_encumbrance_stats
--Pre-reqs:
--  None.
--Modifies:
--  None.
--Locks:
--  None.
--Function:
--  Classifies the distributions below the given entity that are not
--  cancelled or finally closed.
--  This procedure is being used by functions that are embedded in SQL
--  statements in other products (views, etc.), so it is not allowed
--  to modify anything (except in an autonomous transaction).
--  Because of this restriction, the utility procedures that use
--  global temp tables cannot be leveraged.
--Parameters:
--IN:
--p_doc_type
--  Document type.  Use the g_doc_type_<> variables, where <> is:
--    REQUISITION
--    PA
--    PO
--    RELEASE
--p_doc_level
--  The type of ids that are being passed.  Use g_doc_level_<>
--    HEADER
--    LINE
--    SHIPMENT
--    DISTRIBUTION
--p_doc_level_id
--  Id of the doc level type on which the action is being taken.
--OUT:
--x_reserved_count
--  The number of (non-prevent) distributions that are reserved.
--x_unreserved_count
--  The number of non-prevent distributions that are unreserved.
--x_prevented_count
--  The number of distributions that have prevent_encumbrance_flag set to 'Y'.
--End of Comments
--------------------------------------------------------------------------------
PROCEDURE get_open_encumbrance_stats(
   p_doc_type                       IN             VARCHAR2
,  p_doc_level                      IN             VARCHAR2
,  p_doc_level_id                   IN             NUMBER
,  x_reserved_count                 OUT NOCOPY     NUMBER
,  x_unreserved_count               OUT NOCOPY     NUMBER
,  x_prevented_count                OUT NOCOPY     NUMBER
)
IS

l_proc_name CONSTANT VARCHAR2(30) := 'GET_OPEN_ENCUMBRANCE_STATS';
l_log_head  CONSTANT VARCHAR2(100) := g_log_head || l_proc_name;
l_progress  VARCHAR2(3) := '000';

BEGIN

IF g_debug_stmt THEN
   PO_DEBUG.debug_begin(l_log_head);
   PO_DEBUG.debug_var(l_log_head,l_progress,'p_doc_type',p_doc_type);
   PO_DEBUG.debug_var(l_log_head,l_progress,'p_doc_level',p_doc_level);
   PO_DEBUG.debug_var(l_log_head,l_progress,'p_doc_level_id',p_doc_level_id);
END IF;

l_progress := '010';

IF (p_doc_type = g_doc_type_REQUISITION) THEN

   l_progress := '100';

   --SQL What:
   --    Count the distributions that fall into different categories.
   --SQL Where:
   --    Not Cancelled, not Finally Closed.
   --    The id passed in can be header, line, or dist. id.
   --    An inner query is used in the FROM clause to avoid duplicating
   --    any logic, while maintaining a decent SQL plan.

   SELECT

      -- reserved rows that are not prevent_encumbrance
      -- i.e., prevent_encumbrance_flag <> Y and encumbered_flag = Y
      COUNT(   DECODE(  PRD.prevent_encumbrance_flag
                     ,  'Y', NULL
                     ,  DECODE(  PRD.encumbered_flag
                              ,  'Y', 'Y'
                              ,  NULL
                              )
                     )
            )

      -- unreserved rows that are not prevent_encumbrance
      -- i.e., prevent_encumbrance_flag <> Y and encumbered_flag <> Y
   ,  COUNT(   DECODE(  PRD.prevent_encumbrance_flag
                     ,  'Y', NULL
                     ,  DECODE(  PRD.encumbered_flag
                              ,  'Y', NULL
                              ,  'N'
                              )
                     )
            )

      -- prevent_encumbrance rows
      -- i.e., prevent_encumbrance_flag = Y
   ,  COUNT(   DECODE(  PRD.prevent_encumbrance_flag
                     ,  'Y', 'Y'
                     ,  NULL
                     )
            )

   INTO
      x_reserved_count
   ,  x_unreserved_count
   ,  x_prevented_count

   FROM
      PO_REQ_DISTRIBUTIONS_ALL PRD
   ,  PO_REQUISITION_LINES_ALL PRL
   ,  (
         SELECT
            p_doc_level_id             dist_id
         FROM DUAL
         WHERE p_doc_level = g_doc_level_DISTRIBUTION
         UNION ALL
         SELECT
            PRD1.distribution_id       dist_id
         FROM PO_REQ_DISTRIBUTIONS_ALL PRD1
         WHERE p_doc_level = g_doc_level_LINE
         AND   PRD1.requisition_line_id = p_doc_level_id
         UNION ALL
         SELECT
            PRD2.distribution_id       dist_id
         FROM
            PO_REQ_DISTRIBUTIONS_ALL PRD2
         ,  PO_REQUISITION_LINES_ALL PRL2
         WHERE p_doc_level = g_doc_level_HEADER
         AND   PRD2.requisition_line_id = PRL2.requisition_line_id
         AND   PRL2.requisition_header_id = p_doc_level_id
      ) DIST_IDS

   WHERE PRL.requisition_line_id = PRD.requisition_line_id
   AND   NVL(PRL.cancel_flag,'N') <> 'Y'
   AND   NVL(PRL.closed_code,g_clsd_OPEN) <> g_clsd_FINALLY_CLOSED
   AND   PRD.distribution_id = DIST_IDS.dist_id
   ;

   l_progress := '190';

ELSE  -- not a requisition

   l_progress := '200';

   --SQL What:
   --    Count the distributions that fall into different categories.
   --SQL Where:
   --    Not Cancelled, not Finally Closed.
   --    The id passed in can be header, line, shipment, dist., or release id.

   SELECT

      -- reserved rows that are not prevent_encumbrance
      -- i.e., prevent_encumbrance_flag <> Y and encumbered_flag = Y
      COUNT(   DECODE(  POD.prevent_encumbrance_flag
                     ,  'Y', NULL
                     ,  DECODE(  POD.encumbered_flag
                              ,  'Y', 'Y'
                              ,  NULL
                              )
                     )
            )

      -- unreserved rows that are not prevent_encumbrance
      -- i.e., prevent_encumbrance_flag <> Y and encumbered_flag <> Y
   ,  COUNT(   DECODE(  POD.prevent_encumbrance_flag
                     ,  'Y', NULL
                     ,  DECODE(  POD.encumbered_flag
                              ,  'Y', NULL
                              ,  'N'
                              )
                     )
            )

      -- prevent_encumbrance rows
      -- i.e., prevent_encumbrance_flag = Y
   ,  COUNT(   DECODE(  POD.prevent_encumbrance_flag
                     ,  'Y', 'Y'
                     ,  NULL
                     )
            )

   INTO
      x_reserved_count
   ,  x_unreserved_count
   ,  x_prevented_count

   FROM
      PO_DISTRIBUTIONS_ALL POD
   ,  PO_LINE_LOCATIONS_ALL POLL
   ,  PO_HEADERS_ALL POH

   WHERE POLL.line_location_id(+) = POD.line_location_id
   AND   POH.po_header_id = POD.po_header_id
   AND
      (   (p_doc_type <> g_doc_type_PA AND NVL(POLL.cancel_flag,'N') <> 'Y')
      OR  (p_doc_type = g_doc_type_PA AND NVL(POH.cancel_flag,'N') <> 'Y')
      )
   AND
      (
         (  p_doc_type <> g_doc_type_PA
            AND NVL(POLL.closed_code,g_clsd_OPEN) <> g_clsd_FINALLY_CLOSED
         )
      OR
         (  p_doc_type = g_doc_type_PA
            AND NVL(POH.closed_code,g_clsd_OPEN) <> g_clsd_FINALLY_CLOSED
         )
      )
   AND
      (
         (  p_doc_level = g_doc_level_DISTRIBUTION
            AND   POD.po_distribution_id = p_doc_level_id
         )
      OR
         (  p_doc_level = g_doc_level_SHIPMENT
            AND   POD.line_location_id = p_doc_level_id
         )
      OR
         (  p_doc_level = g_doc_level_LINE
            AND   POD.po_line_id = p_doc_level_id
         )
      OR
         (  p_doc_level = g_doc_level_HEADER
            AND   p_doc_type = g_doc_type_RELEASE
            AND   POD.po_release_id = p_doc_level_id
         )
      OR
         (  p_doc_level = g_doc_level_HEADER
            AND   p_doc_type <> g_doc_type_RELEASE
            AND   POD.po_header_id = p_doc_level_id
         )
      )
   -- Make sure that release dists are not picked up for BPAs, PPOs.
   AND
      (
         (  p_doc_type <> g_doc_type_RELEASE
            AND   POD.po_release_id IS NULL
         )
      OR
         (  p_doc_type = g_doc_type_RELEASE
            AND   POD.po_release_id IS NOT NULL
         )
      )
   ;

   l_progress := '290';

END IF;

IF g_debug_stmt THEN
   PO_DEBUG.debug_stmt(l_log_head,l_progress,'got encumbrance counts');
END IF;

l_progress := '900';
IF g_debug_stmt THEN
   PO_DEBUG.debug_var(l_log_head,l_progress,'x_reserved_count',x_reserved_count);
   PO_DEBUG.debug_var(l_log_head,l_progress,'x_unreserved_count',x_unreserved_count);
   PO_DEBUG.debug_var(l_log_head,l_progress,'x_prevented_count',x_prevented_count);
   PO_DEBUG.debug_end(l_log_head);
END IF;

EXCEPTION
WHEN OTHERS THEN
   IF g_debug_unexp THEN
      PO_DEBUG.debug_var(l_log_head,l_progress,'x_reserved_count',x_reserved_count);
      PO_DEBUG.debug_var(l_log_head,l_progress,'x_unreserved_count',x_unreserved_count);
      PO_DEBUG.debug_var(l_log_head,l_progress,'x_prevented_count',x_prevented_count);
      PO_DEBUG.debug_exc(l_log_head,l_progress);
   END IF;
   RAISE;

END get_open_encumbrance_stats;




--------------------------------------------------------------------------------
--Start of Comments
--Name: should_display_reserved
--Pre-reqs:
--  None.
--Modifies:
--  None.
--Locks:
--  None.
--Function:
--  Determines whether or not the status of the given entity should display
--  as "Reserved".
--  This procedure is being used by functions that are embedded in SQL
--  statements in other products (views, etc.), so it is not allowed
--  to modify anything (except in an autonomous transaction).
--Parameters:
--IN:
--p_doc_type
--  Document type.  Use the g_doc_type_<> variables, where <> is:
--    REQUISITION
--    PA
--    PO
--    RELEASE
--p_doc_level
--  The type of ids that are being passed.  Use g_doc_level_<>
--    HEADER
--    LINE
--    SHIPMENT
--    DISTRIBUTION
--p_doc_level_id
--  Id of the doc level type on which the action is being taken.
--OUT:
--x_display_reserved_flag
--  VARCHAR2(1)
--  'Y' - "Reserved" should be displayed
--  'N' - don't display "Reserved"
--End of Comments
--------------------------------------------------------------------------------
PROCEDURE should_display_reserved(
   p_doc_type                       IN             VARCHAR2
,  p_doc_level                      IN             VARCHAR2
,  p_doc_level_id                   IN             NUMBER
,  x_display_reserved_flag          OUT NOCOPY     VARCHAR2
)
IS

l_proc_name CONSTANT VARCHAR2(30) := 'SHOULD_DISPLAY_RESERVED';
l_log_head  CONSTANT VARCHAR2(100) := g_log_head || l_proc_name;
l_progress  VARCHAR2(3) := '000';

l_reserved_count   NUMBER;
l_unreserved_count NUMBER;
l_prevented_count  NUMBER;

BEGIN

IF g_debug_stmt THEN
   PO_DEBUG.debug_begin(l_log_head);
   PO_DEBUG.debug_var(l_log_head,l_progress,'p_doc_type',p_doc_type);
   PO_DEBUG.debug_var(l_log_head,l_progress,'p_doc_level',p_doc_level);
   PO_DEBUG.debug_var(l_log_head,l_progress,'p_doc_level_id',p_doc_level_id);
END IF;

l_progress := '010';

get_open_encumbrance_stats(
   p_doc_type           => p_doc_type
,  p_doc_level          => p_doc_level
,  p_doc_level_id       => p_doc_level_id
,  x_reserved_count     => l_reserved_count
,  x_unreserved_count   => l_unreserved_count
,  x_prevented_count    => l_prevented_count
);

l_progress := '020';

IF (l_unreserved_count = 0 AND l_reserved_count > 0) THEN
   x_display_reserved_flag := 'Y';
ELSE
   x_display_reserved_flag := 'N';
END IF;

l_progress := '900';
IF g_debug_stmt THEN
   PO_DEBUG.debug_var(l_log_head,l_progress,'x_display_reserved_flag',x_display_reserved_flag);
   PO_DEBUG.debug_end(l_log_head);
END IF;

EXCEPTION
WHEN OTHERS THEN
   IF g_debug_unexp THEN
PO_DEBUG.debug_var(l_log_head,l_progress,'x_display_reserved_flag',x_display_reserved_flag);
      PO_DEBUG.debug_exc(l_log_head,l_progress);
   END IF;
   RAISE;

END should_display_reserved;




--------------------------------------------------------------------------------
--Start of Comments
--Name: is_fully_reserved
--Pre-reqs:
--  None.
--Modifies:
--  None.
--Locks:
--  None.
--Function:
--  Determines whether or not all of the distributions below the given entity that
--  can be reserved are reserved.
--Parameters:
--IN:
--p_doc_type
--  Document type.  Use the g_doc_type_<> variables, where <> is:
--    REQUISITION
--    PA
--    PO
--    RELEASE
--p_doc_level
--  The type of ids that are being passed.  Use g_doc_level_<>
--    HEADER
--    LINE
--    SHIPMENT
--    DISTRIBUTION
--p_doc_level_id
--  Id of the doc level type on which the action is being taken.
--OUT:
--x_fully_reserved_flag
--  VARCHAR2(1)
--  'Y' - all of the dists that can be reserved are reserved
--  'N' - there is at least one non-prevent dist that is not reserved
--End of Comments
--------------------------------------------------------------------------------
PROCEDURE is_fully_reserved(
   p_doc_type                       IN             VARCHAR2
,  p_doc_level                      IN             VARCHAR2
,  p_doc_level_id                   IN             NUMBER
,  x_fully_reserved_flag            OUT NOCOPY     VARCHAR2
)
IS

l_proc_name CONSTANT VARCHAR2(30) := 'IS_FULLY_RESERVED';
l_log_head  CONSTANT VARCHAR2(100) := g_log_head || l_proc_name;
l_progress  VARCHAR2(3) := '000';

l_reserved_count   NUMBER;
l_unreserved_count NUMBER;
l_prevented_count  NUMBER;

BEGIN

IF g_debug_stmt THEN
   PO_DEBUG.debug_begin(l_log_head);
   PO_DEBUG.debug_var(l_log_head,l_progress,'p_doc_type',p_doc_type);
   PO_DEBUG.debug_var(l_log_head,l_progress,'p_doc_level',p_doc_level);
   PO_DEBUG.debug_var(l_log_head,l_progress,'p_doc_level_id',p_doc_level_id);
END IF;

l_progress := '010';

get_open_encumbrance_stats(
   p_doc_type           => p_doc_type
,  p_doc_level          => p_doc_level
,  p_doc_level_id       => p_doc_level_id
,  x_reserved_count     => l_reserved_count
,  x_unreserved_count   => l_unreserved_count
,  x_prevented_count    => l_prevented_count
);

l_progress := '020';

IF (l_unreserved_count > 0) THEN
   x_fully_reserved_flag := 'N';
ELSE
   x_fully_reserved_flag := 'Y';
END IF;

l_progress := '900';
IF g_debug_stmt THEN
   PO_DEBUG.debug_var(l_log_head,l_progress,'x_fully_reserved_flag',x_fully_reserved_flag);
   PO_DEBUG.debug_end(l_log_head);
END IF;

EXCEPTION
WHEN OTHERS THEN
   IF g_debug_unexp THEN
PO_DEBUG.debug_var(l_log_head,l_progress,'x_fully_reserved_flag',x_fully_reserved_flag);
      PO_DEBUG.debug_exc(l_log_head,l_progress);
   END IF;
   RAISE;

END is_fully_reserved;




--------------------------------------------------------------------------------
--Start of Comments
--Name: are_any_dists_reserved
--Pre-reqs:
--  None.
--Modifies:
--  None.
--Locks:
--  None.
--Function:
--  Determines if any of the distributions below the given entity are reserved.
--Parameters:
--IN:
--p_doc_type
--  Document type.  Use the g_doc_type_<> variables, where <> is:
--    REQUISITION
--    PA
--    PO
--    RELEASE
--p_doc_level
--  The type of ids that are being passed.  Use g_doc_level_<>
--    HEADER
--    LINE
--    SHIPMENT
--    DISTRIBUTION
--p_doc_level_id
--  Id of the doc level type on which the action is being taken.
--OUT:
--x_some_dists_reserved_flag
--  VARCHAR2(1)
--  'Y' - at least one distribution is reserved
--  'N' - no distributions are reserved
--End of Comments
--------------------------------------------------------------------------------
PROCEDURE are_any_dists_reserved(
   p_doc_type                       IN             VARCHAR2
,  p_doc_level                      IN             VARCHAR2
,  p_doc_level_id                   IN             NUMBER
,  x_some_dists_reserved_flag       OUT NOCOPY     VARCHAR2
)
IS

l_proc_name CONSTANT VARCHAR2(30) := 'ARE_ANY_DISTS_RESERVED';
l_log_head  CONSTANT VARCHAR2(100) := g_log_head || l_proc_name;
l_progress  VARCHAR2(3) := '000';

l_reserved_count   NUMBER;
l_unreserved_count NUMBER;
l_prevented_count  NUMBER;

BEGIN

IF g_debug_stmt THEN
   PO_DEBUG.debug_begin(l_log_head);
   PO_DEBUG.debug_var(l_log_head,l_progress,'p_doc_type',p_doc_type);
   PO_DEBUG.debug_var(l_log_head,l_progress,'p_doc_level',p_doc_level);
   PO_DEBUG.debug_var(l_log_head,l_progress,'p_doc_level_id',p_doc_level_id);
END IF;

l_progress := '010';

get_open_encumbrance_stats(
   p_doc_type           => p_doc_type
,  p_doc_level          => p_doc_level
,  p_doc_level_id       => p_doc_level_id
,  x_reserved_count     => l_reserved_count
,  x_unreserved_count   => l_unreserved_count
,  x_prevented_count    => l_prevented_count
);

l_progress := '020';

IF (l_reserved_count > 0) THEN
   x_some_dists_reserved_flag := 'Y';
ELSE
   x_some_dists_reserved_flag := 'N';
END IF;

l_progress := '900';
IF g_debug_stmt THEN
   PO_DEBUG.debug_var(l_log_head,l_progress,'x_some_dists_reserved_flag',x_some_dists_reserved_flag);
   PO_DEBUG.debug_end(l_log_head);
END IF;

EXCEPTION
WHEN OTHERS THEN
   IF g_debug_unexp THEN
PO_DEBUG.debug_var(l_log_head,l_progress,'x_some_dists_reserved_flag',x_some_dists_reserved_flag);
      PO_DEBUG.debug_exc(l_log_head,l_progress);
   END IF;
   RAISE;

END are_any_dists_reserved;




--------------------------------------------------------------------------------
--Start of Comments
--Name: get_reserved_lookup
--Pre-reqs:
--  None.
--Modifies:
--  None.
--Locks:
--  None.
--Function:
--  Gets the text to display for the "Reserved" keyword.
--Parameters:
--OUT:
--x_displayed_field
--  PO_LOOKUP_CODES.displayed_field%TYPE
--  The text corresponding to the 'RESERVED' code.
--End of Comments
--------------------------------------------------------------------------------
PROCEDURE get_reserved_lookup(
   x_displayed_field                  OUT NOCOPY     VARCHAR2
)
IS

l_proc_name CONSTANT VARCHAR2(30) := 'GET_RESERVED_LOOKUP';
l_log_head  CONSTANT VARCHAR2(100) := g_log_head || l_proc_name;
l_progress  VARCHAR2(3) := '000';

BEGIN

IF g_debug_stmt THEN
   PO_DEBUG.debug_begin(l_log_head);
END IF;

l_progress := '010';

SELECT POLC.displayed_field
INTO x_displayed_field
FROM PO_LOOKUP_CODES POLC
WHERE POLC.lookup_type = 'DOCUMENT STATE'
AND POLC.lookup_code = 'RESERVED'
;

l_progress := '900';
IF g_debug_stmt THEN
   PO_DEBUG.debug_var(l_log_head,l_progress,'x_displayed_field',x_displayed_field);
   PO_DEBUG.debug_end(l_log_head);
END IF;

EXCEPTION
WHEN OTHERS THEN
   IF g_debug_unexp THEN
      PO_DEBUG.debug_var(l_log_head,l_progress,'x_displayed_field',x_displayed_field);
      PO_DEBUG.debug_exc(l_log_head,l_progress);
   END IF;

END get_reserved_lookup;




-- Bug 3373453 START
-------------------------------------------------------------------------------
--Start of Comments
--Name: validate_yes_no_param
--Pre-reqs:
--  None.
--Modifies:
--  None.
--Locks:
--  None.
--Function:
--  Validates that the given parameter value is one of the following:
--    null, G_PARAMETER_YES, or G_PARAMETER_NO
--  Returns an error for any other value.
--Parameters:
--IN:
--p_parameter_name
--  Name of the parameter; used in the error message
--p_parameter_value
--  Parameter value to be validated
--OUT:
--x_return_status
--  FND_API.G_RET_STS_SUCCESS if the parameter value is valid.
--  FND_API.G_RET_STS_ERROR if the parameter value is not valid.
--  FND_API.G_RET_STS_UNEXP_ERROR if an unexpected error occurred.
--End of Comments
-------------------------------------------------------------------------------
PROCEDURE validate_yes_no_param (
  x_return_status   OUT NOCOPY VARCHAR2,
  p_parameter_name  IN VARCHAR2,
  p_parameter_value IN VARCHAR2
) IS
  l_proc_name CONSTANT VARCHAR2(30) := 'VALIDATE_YES_NO_PARAM';
BEGIN
  x_return_status := FND_API.G_RET_STS_SUCCESS;

  IF (p_parameter_value IS NOT NULL)
     AND (p_parameter_value NOT IN (G_PARAMETER_YES, G_PARAMETER_NO)) THEN

    FND_MESSAGE.set_name('PO', 'PO_INVALID_YES_NO_PARAM');
    FND_MESSAGE.set_token('PARAMETER_NAME', p_parameter_name);
    FND_MESSAGE.set_token('PARAMETER_VALUE', p_parameter_value);
    FND_MSG_PUB.add;

    x_return_status := FND_API.G_RET_STS_ERROR;
    RETURN;
  END IF;

EXCEPTION
  WHEN OTHERS THEN
    PO_DEBUG.handle_unexp_error(p_pkg_name => g_pkg_name,
                                p_proc_name => l_proc_name );
    x_return_status := FND_API.G_RET_STS_UNEXP_ERROR;
END validate_yes_no_param;
-- Bug 3373453 END




--------------------------------------------------------------------------------
--Start of Comments
--Name: get_session_gt_nextval
--Pre-reqs:
--  None.
--Modifies:
--  PO_SESSION_GT_S
--Locks:
--  None.
--Function:
--  Retrieves the next sequence number from the PO_SESSION_GT_S sequence.
--Returns:
--  PO_SESSION_GT_S.nextval
--End of Comments
--------------------------------------------------------------------------------
FUNCTION get_session_gt_nextval
RETURN NUMBER
IS

l_proc_name CONSTANT VARCHAR2(30) := 'GET_SESSION_GT_NEXTVAL';
l_log_head  CONSTANT VARCHAR2(100) := g_log_head || l_proc_name;
l_progress  VARCHAR2(3) := '000';

x_nextval   NUMBER;

BEGIN

IF g_debug_stmt THEN
   PO_DEBUG.debug_begin(l_log_head);
END IF;

l_progress := '010';

SELECT PO_SESSION_GT_S.nextval
INTO x_nextval
FROM DUAL
;

l_progress := '900';

IF g_debug_stmt THEN
   PO_DEBUG.debug_var(l_log_head,l_progress,'x_nextval',x_nextval);
   PO_DEBUG.debug_end(l_log_head);
END IF;

RETURN(x_nextval);

EXCEPTION
WHEN OTHERS THEN
   PO_MESSAGE_S.sql_error(g_pkg_name,l_proc_name,l_progress,SQLCODE,SQLERRM);
   IF g_debug_stmt THEN
      PO_DEBUG.debug_var(l_log_head,l_progress,'x_nextval',x_nextval);
   END IF;
   RAISE;

END get_session_gt_nextval;




END PO_CORE_S;
/
