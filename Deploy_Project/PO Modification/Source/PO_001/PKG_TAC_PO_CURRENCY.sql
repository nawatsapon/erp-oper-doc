Create or replace package TAC_PO_CURRENCY_PKG is
-- @'D:\GoodJob\iCE\dTAC\PO_001\PKG_TAC_PO_CURRENCY.sql'
-- dTAC: PO Cross Currency Auto Convert Program
-- Update: 28 June 2017
	FUNCTION CHECK_BPA_1 (p_inf_header_id Number) RETURN VARCHAR2;	-- From Interface Header ID, return Y or N
	FUNCTION CHECK_BPA_2 (p_header_id Number) RETURN VARCHAR2;		-- From Blanket-PO Header ID, return Y or N
	FUNCTION CHECK_BPA_3 (p_header_id Number) RETURN VARCHAR2;		-- From Std PO Header ID -> Blancket-PO, return Y or N
	FUNCTION DFF_CURRECY_CODE (p_inf_header_id Number) RETURN VARCHAR2;
	FUNCTION GET_EXCHANGE_RATE (p_date			Date,
								p_from_curr		Varchar2,
								p_to_curr		Varchar2,
								p_type			Number) RETURN NUMBER;

	FUNCTION GET_ORI_PRICE (p_header_id number, p_amount Number) RETURN NUMBER;	-- From Std PO Header ID -> Blancket-PO, Return USD amount

	FUNCTION GET_PO_LANG (p_header_id Number, p_currency Varchar2) RETURN VARCHAR2;
	FUNCTION GET_PRICE (p_req_line_id Number, p_amount Number) RETURN NUMBER;
	FUNCTION GET_PR_CURRENCY (p_header_id	Number) RETURN VARCHAR2;

	FUNCTION PO_FORM_EXCH_RATE (p_header_id		number,
								p_po_date		date,
								p_lang			varchar2) RETURN VARCHAR2;	-- Blanket PO Header id return blanket currency code.

	FUNCTION PO_FORM_ORI_PRICE (p_header_id		number,
								p_line_id		number,
								p_lang			varchar2) RETURN VARCHAR2;	-- Blanket PO Header id and PO Line ID return line original price on po form. 

End TAC_PO_CURRENCY_PKG;
/


Create or replace package body TAC_PO_CURRENCY_PKG IS
-- @'D:\GoodJob\iCE\dTAC\PO_001\PKG_TAC_PO_CURRENCY.sql'
-- dTAC: PO Cross Currency Auto Convert Program
-- Update: 28 June 2017

	vErrorCode 		Number;
	vErrorMSG		Varchar2(1000);
	PohDFF			Varchar2(10) := 'PO';

	FUNCTION CHECK_BPA_1(p_inf_header_id Number) RETURN VARCHAR2 Is -- From Interface Header ID, return Y or N
	-- Used by PO_INTERFACE_S only
		cursor curInf is
			Select distinct rl.document_type_code	,
				rl.currency_code					,
				rl.blanket_po_header_id				-- , rl.blanket_po_line_num
			From po_requisition_lines_all rl,
				 po_lines_interface li
			Where rl.requisition_line_id = li.requisition_line_id
			  and li.interface_header_id = p_inf_header_id
			Order by rl.blanket_po_header_id; 			-- Parameter

		cursor curPO(p_header_id Number) IS
			Select ph.segment1					,
				ph.type_lookup_code				,
				ph.currency_code				,
				Nvl(ph.global_agreement_flag, 'N')	global_flag,
				ph.attribute_category			,
				ph.attribute1					to_currency,
				ph.attribute2					rate_type,
				Nvl(ph.attribute3,'N')			po_convert,			-- Y/N
				ph.attribute4					po_lang				-- US/TH
			From po_headers_all ph
			Where ph.po_header_id = p_header_id						-- Parameter
			  and ph.type_lookup_code = 'BLANKET'
			Order by ph.segment1;
			
		bCheck		Varchar2(1);
		nCount		Pls_integer;
		recInf		curInf%Rowtype;
		recPO		curPO%Rowtype;
		
		
	Begin
		bCheck := 'N';
		nCount := Null;
		recInf := Null;
		recPO := Null;

		Open curInf;
			Loop
				Fetch curInf Into recInf;
				Exit When ( curInf%Notfound or recInf.Blanket_po_header_id is not null );
			End Loop;
		Close curInf;

		If recInf.Blanket_po_header_id is not null and Nvl(recInf.Currency_code, 'THB') <> 'THB' Then

			Open curPO(recInf.Blanket_po_header_id);
				Fetch curPO Into recPO;
			Close curPO;

-- Check daily rate
			Select count(dr.conversion_rate)
			Into nCount
			From gl_daily_rates dr
			Where dr.from_currency = recPO.Currency_code
			  and dr.to_currency = recPO.To_currency
			  and dr.conversion_date = trunc(sysdate)		-- use = on production
			  and dr.conversion_type = recPO.Rate_type ;

			If nvl(nCount,0) <> 0 Then
				If recPO.global_flag = 'Y' and recPO.Attribute_category = PohDFF and
					recPO.Po_convert = 'Y' and recPO.Rate_type is not null and Nvl(recPO.To_currency,'THB') <> recPO.Currency_code Then

					bCheck := 'Y';
				
				End If;
			End If;

		End If;

		Return bCheck;

	Exception
		When Others Then

			vErrorCode := SQLCODE;
			vErrorMSG  := SQLERRM;

			Return bCheck;

	End CHECK_BPA_1;


	FUNCTION CHECK_BPA_2(p_header_id Number) RETURN VARCHAR2 IS		-- From Blanket-PO Header ID, return Y or N
		cursor curPO IS
			Select ph.segment1					,
				ph.currency_code				,
				ph.type_lookup_code				,
				Nvl(ph.global_agreement_flag, 'N')	global_flag,
				ph.attribute_category			,
				ph.attribute1					to_currency,
				ph.attribute2					rate_type,
				Nvl(ph.attribute3,'N')			po_convert,			-- Y/N
				ph.attribute4					po_lang				-- US/TH
			From po_headers_all ph
			Where ph.po_header_id = p_header_id						-- Parameter: Blanket Header ID
			  and ph.type_lookup_code = 'BLANKET'
			Order by ph.segment1;

		recPO		curPO%Rowtype;
		bCheck		Varchar2(1);

	Begin

		bCheck := 'N';

		Open curPO;
			Fetch curPO Into recPO;
		Close curPO;

		If recPO.global_flag = 'Y' and recPO.Attribute_category = PohDFF and
			recPO.Po_convert = 'Y' and recPO.Rate_type is not null and
			Nvl(recPO.To_currency,'THB') <> recPO.Currency_code Then

			bCheck := 'Y';

		End If;

		Return bCheck;

	End CHECK_BPA_2;


	FUNCTION CHECK_BPA_3(p_header_id Number) RETURN VARCHAR2 Is
		cursor curPO Is
			Select distinct l.from_header_id
			From  po_lines_all l
			Where l.po_header_id = p_header_id;		-- Parameter Line.PO_Header_id back to Blanket Header ID

		l_po_header_id		Number;
		bCheck				Varchar2(1);

	Begin
		bCheck := 'N';

		Open curPO;

			Loop
				Fetch curPO Into l_po_header_id;
				bCheck := check_bpa_2(l_po_header_id);
				Exit when ( curPO%notfound or bCheck = 'Y' );
			End Loop;

		Close curPO;

		Return bCheck;

	End CHECK_BPA_3;


	FUNCTION DFF_CURRECY_CODE(p_inf_header_id Number) RETURN VARCHAR2 Is
		cursor curInf is
			Select distinct rh.attribute_category	,
				rh.attribute1						to_currency,
				rh.attribute2						rate_type,
				rl.document_type_code				,
				rl.blanket_po_header_id				,
				rl.blanket_po_line_num				,
				rl.requisition_line_id
			From po_requisition_headers_all rh,
				 po_requisition_lines_all rl,
				 po_lines_interface li
			Where rh.requisition_header_id = rl.requisition_header_id
			  and rl.requisition_line_id = li.requisition_line_id
			  and li.interface_header_id = p_inf_header_id			-- Parameter
			Order by rh.attribute_category nulls last, rl.requisition_line_id;

		cursor curPO(l_header_id Number) IS
			Select ph.segment1					,
				ph.currency_code				,
				ph.type_lookup_code				,
				Nvl(ph.global_agreement_flag, 'N')	global_flag,
				ph.attribute_category			,
				ph.attribute1					to_currency,
				ph.attribute2					rate_type,
				Nvl(ph.attribute3,'N')			po_convert,			-- Y/N
				ph.attribute4					po_lang				-- US/TH
			From po_headers_all ph
			Where ph.po_header_id = l_header_id						-- Parameter
			  and ph.attribute_category = PohDFF
			  and ph.type_lookup_code = 'BLANKET'
			Order by ph.segment1;

		recInf		curInf%Rowtype;
		recPO		curPO%Rowtype;

	Begin
		recInf := Null;
		recPO := Null;

		Open curInf;
			Loop
				Fetch curInf Into recInf;
				Exit When ( curInf%Notfound or recInf.Blanket_po_header_id is not null );
			End Loop;
		Close curInf;

		If recInf.Blanket_po_header_id is not null Then
			Open curPO(recInf.Blanket_po_header_id);
				Fetch curPO Into recPO;
			Close curPO;

			Return recPO.To_currency;
		End If;

		IF recInf.To_currency is not null Then
			Return recInf.To_currency;
		End If;

		Return Null;

	Exception
		When Others Then
			Return Null;

	End DFF_CURRECY_CODE;


	FUNCTION GET_EXCHANGE_RATE (p_date Date, p_from_curr Varchar2, p_to_curr Varchar2, p_type Number) RETURN NUMBER Is

		cursor curRate Is
			Select dr.conversion_rate
			From gl_daily_rates dr
			Where dr.from_currency = p_from_curr
			  and dr.to_currency = p_to_curr
			  and dr.conversion_date = trunc(p_date)		-- use = on production
			  and dr.conversion_type = p_type
			  and rownum = 1
			Order by conversion_date desc ;

		recRate		curRate%Rowtype;

	Begin
		
		Open curRate;
			Fetch curRate Into recRate;
		Close curRate;
		
		If recRate.conversion_rate is not null Then

			Return recRate.conversion_rate;

		End If;
		
		Return 1; -- not found;
	
	Exception
		When Others Then
			Return 1;

	End GET_EXCHANGE_RATE;


	FUNCTION GET_ORI_PRICE (p_header_id number, p_amount Number) RETURN NUMBER Is
		cursor curRate IS
			Select distinct poh.creation_date	,
				poh1.attribute_category			,
				poh1.attribute1					to_currency,
				poh1.attribute2					rate_type,
				poh1.currency_code				,
				tac_po_currency_pkg.get_exchange_rate(poh.creation_date, poh1.attribute1, poh1.currency_code, poh1.attribute2) exc_rate
			From po_lines_all pol,
				 po_headers_all poh,
				 po_headers_all poh1
			where pol.po_header_id = p_header_id			-- Parameter
			  and pol.from_header_id = poh1.po_header_id
			  and poh1.type_lookup_code = 'BLANKET'
			  and poh.po_header_id = pol.po_header_id;
		
		recRate		curRate%Rowtype;
	Begin
		
		Open curRate;
			Fetch curRate Into recRate;
		Close curRate;

		If recRate.attribute_category = PohDFF and recRate.exc_rate is not null Then

			Return round(p_amount * recRate.exc_rate, 4);

		End If;

		Return p_amount;

	End GET_ORI_PRICE;


	FUNCTION GET_PO_LANG (p_header_id Number, p_currency Varchar2) RETURN VARCHAR2 Is -- Return THB or USD
		cursor curPO Is
			Select 	ph.attribute_category	,
				ph.attribute1				to_currency,
				ph.attribute2				rate_type,
				Nvl(ph.attribute3,'N')		po_convert,			-- Y/N
				ph.attribute4				po_lang				-- US/TH
			From po_headers_all ph
			Where ph.po_header_id = p_header_id;

		cursor curBlk Is
			Select distinct poh.segment1	,
				poh.type_lookup_code			,
				poh.currency_code			,
				Nvl(poh.global_agreement_flag, 'N')	global_flag,
				poh.attribute_category		,
				poh.attribute1				to_currency,
				poh.attribute2				rate_type,
				Nvl(poh.attribute3,'N')		po_convert,			-- Y/N
				poh.attribute4				po_lang				-- US/TH
			From  po_lines_all l,
				  po_headers_all poh
			Where l.po_header_id = p_header_id					-- PO Number
			  and l.from_header_id = poh.po_header_id			-- Back to Blanket number
			  and poh.type_lookup_code = 'BLANKET'
			Order by poh.segment1;

		recPO		curPO%Rowtype;
		recBLK		curBlk%Rowtype;
		vLang		Varchar2(40);
		
	Begin
		recPO := Null;
		recBLK := Null;
		vLang := p_currency;	-- Return default

		Open curPO;
			Fetch curPO Into recPO;
		Close curPO;

		If recPO.Attribute_category = PohDFF and recPO.Po_lang is not null Then
			If recPO.Po_lang = 'TH' Then
				vLang := 'THB';
			Else
				vLang := 'USD';
			End If;
		Else	-- Blanket Or Not?
			Open curBLK;
				Fetch curBLK Into recBLK;
			Close curBLK;

			IF recBLK.Attribute_category = PohDFF and recBLK.Po_lang is not null Then

				If recBLK.Po_lang = 'TH' Then
					vLang := 'THB';
				Else
					vLang := 'USD';
				End If;

			End If;

		End If;	-- Else return p_currency

		Return vLang;

	End GET_PO_LANG;


	FUNCTION GET_PRICE (p_req_line_id Number, p_amount Number) RETURN NUMBER Is
	-- Used by PO_INTERFACE_S Only because, po creation date =  sysdate, so find exchage rate as of sysdate
		cursor curReq Is
			Select rl.requisition_header_id		,
				rl.document_type_code			,
				rl.blanket_po_header_id			,
				rl.blanket_po_line_num			,
				rl.currency_code				,
				rl.currency_unit_price			,
				rl.unit_price
			From po_requisition_lines_all rl
			Where rl.requisition_line_id = p_req_line_id			-- Parameter
			Order by rl.requisition_header_id;

		cursor curPO(p_header_id Number) Is
			Select ph.segment1					,
				ph.type_lookup_code				,
				ph.currency_code				,
				Nvl(ph.global_agreement_flag, 'N')	global_flag,
				ph.attribute_category			,
				ph.attribute1					to_currency,
				ph.attribute2					rate_type,
				Nvl(ph.attribute3,'N')			po_convert,			-- Y/N
				ph.attribute4					po_lang				-- US/TH				
			From po_headers_all ph
			Where ph.po_header_id = p_header_id						-- Parameter
			  and ph.type_lookup_code = 'BLANKET'
			Order by ph.segment1;

		recReq		curReq%Rowtype;
		recPO		curPO%Rowtype;		

		nRate				Number;
		l_rate_type			Varchar2(30);
		l_currency_code		Varchar2(15);

	Begin
		recReq := Null;
		recPO := Null;
		l_rate_type	:= Null;
		l_currency_code	:= Null;

		Open curReq;
			Fetch curReq Into recReq;
		Close curReq;

		Open curPO (recReq.blanket_po_header_id);
			Fetch curPO Into recPO;
		Close curPO;

		If recPO.attribute_category = PohDFF and recPO.Po_convert = 'Y' Then

			l_rate_type := recPO.rate_type;
			l_currency_code := recPO.to_currency;

		End If;

		If recReq.Currency_code <>  NVL(l_currency_code, recReq.Currency_code) and recPO.Po_convert = 'Y' Then -- find exchage rate as of sysdate
			nRate := tac_po_currency_pkg.get_exchange_rate(sysdate, recReq.Currency_code , l_currency_code, l_rate_type);
			Return p_amount * nRate;
		End If;

		Return p_amount;
	
	Exception
		When Others Then
			return p_amount;

	End GET_PRICE;


	FUNCTION GET_PR_CURRENCY(p_header_id	Number) RETURN VARCHAR2 Is
		cursor curPR IS
			Select distinct rl.currency_code
			From po_distributions_all pd,
				 po_req_distributions_all rd,
				 po_requisition_lines_all rl
			Where pd.po_header_id = p_header_id			-- Parameter
			  and pd.req_distribution_id = rd.distribution_id
			  and rd.requisition_line_id = rl.requisition_line_id;
		
		recPR		curPR%Rowtype;
	Begin

		Open curPR;
			Fetch curPR Into recPR;
		Close curPR;

		Return recPR.Currency_code;

	End GET_PR_CURRENCY;


	FUNCTION PO_FORM_ORI_PRICE (p_header_id		number,
								p_line_id		number,
								p_lang			varchar2) RETURN VARCHAR2 Is
		
		cursor curPrice Is
			Select l.unit_price, h.currency_code
			From po_lines_all l,
				 po_headers_all h
			Where h.po_header_id = l.po_header_id
			  and l.po_header_id = p_header_id		-- Parameter
			  and l.po_line_id = p_line_id ;		-- Parameter

		nPrice		Number;
		vCode		Varchar2(30);

	Begin
		Open curPrice;
			Fetch curPrice Into nPrice, vCode;
		Close curPrice;
	
		If p_lang = 'TH' Then
			Return '�Ҥҵ��˹��� = ' || To_char(nPrice,'fm999,999,999,999.00') || ' ' || vCode;
		Else
			Return 'Original Unit Price = ' || To_char(nPrice,'fm999,999,999,999.00') || ' ' ||vCode ;
		End If;

		Return '';

	Exception
		When Others Then
			Return '';

	End PO_FORM_ORI_PRICE;


	FUNCTION PO_FORM_EXCH_RATE (p_header_id		number,
								p_po_date		date,
								p_lang			varchar2) RETURN VARCHAR2 IS

		cursor curPO Is
			Select distinct h.segment1	,
				h.type_lookup_code			,
				h.currency_code			,
				Nvl(h.global_agreement_flag, 'N')	global_flag,
				h.attribute_category		,
				h.attribute1				to_currency,
				h.attribute2				rate_type,
				Nvl(h.attribute3,'N')		po_convert,			-- Y/N
				h.attribute4				po_lang				-- US/TH		
			From po_headers_all h,
				(   Select distinct l.from_header_id
                    From po_lines_all l
                    Where po_header_id = p_header_id ) l
			Where h.po_header_id = l.from_header_id			-- Parameter
			  and h.type_lookup_code = 'BLANKET';		
		
		cursor curType(l_type Varchar2) Is
			Select description, user_conversion_type, conversion_type
			From gl_daily_conversion_types
			Where conversion_type = l_type;

		cursor curDailyRate(l_from Varchar2, l_to Varchar2, l_date Date, l_type Varchar2) Is
			Select conversion_date
			From gl_daily_rates
			Where from_currency = l_from
			  and to_currency = l_to
			  and conversion_date < trunc(l_date)
			  and conversion_type = l_type
			  and rownum = 1
			Order by conversion_date desc;

		cursor curName(l_code Varchar2) Is
			Select name
			From fnd_currencies_tl
			Where currency_code = l_code;

		recPO			curPO%Rowtype;
		recRateType		curType%Rowtype;

		nRate			Number;
		dRateDate		Date;
		vDisplay		Varchar2(1000);
		vFrom			Varchar2(30);
		vTo				Varchar2(30);

	Begin
		
		recPO := Null;
		recRateType := Null;
		vDisplay := Null;
		vFrom := Null;
		vTo := Null;

		Open curPO;
			Fetch curPO Into recPO;
		Close curPO;

		Open curType(recPO.Rate_type);	-- BUYING, SELLING, WEIGHT
			Fetch curType Into recRateType;
		Close curType;

		Open curDailyRate(recPO.Currency_code, recPO.To_currency, p_po_date, recPO.Rate_type);
			Fetch curDailyRate Into dRateDate;
		Close curDailyRate;

		nRate := tac_po_currency_pkg.get_exchange_rate (p_po_date, recPO.Currency_code, recPO.To_currency, recPO.Rate_type);

		Open curName(recPO.Currency_code);
			Fetch curName Into vTo;
		Close curName;

		Open curName(recPO.To_currency);
			Fetch curName Into vFrom;
		Close curName;
		
		If recRateType.User_conversion_type = 'WEIGHT' Then

			vDisplay := vDisplay || 'Equipment PO in ' || recPO.To_currency || ' currency based on weighted-average Interbank Exchange Rate ' || recPO.Currency_code || '/' || recPO.To_currency || ' announced by the Bank of Thailand at the end of the previous working day on which the Purchase Order is issued.' ;
			vDisplay := vDisplay || CHR(13) ; -- CHR(10)
			vDisplay := vDisplay || CHR(13) ; -- CHR(10)
			vDisplay := vDisplay || 'Weighted-average Interbank Exchange Rate as of ' || to_char(dRateDate, 'fmDD Month YYYY' ) || ' is ' || To_char(nRate,'fm999,999.0000') || ' ' ||vFrom || '/' || vTo ;

		ElsIf recRateType.User_conversion_type = 'SELLING' Then --

			vDisplay := vDisplay || 'Equipment PO in ' || recPO.To_currency || ' currency based on Average Selling Rates  ' || recPO.Currency_code || '/' || recPO.To_currency || ' announced by the Bank of Thailand at the end of the previous working day on which the Purchase Order is issued.' ;
			vDisplay := vDisplay || CHR(13) ;
			vDisplay := vDisplay || CHR(13) ; -- CHR(10)
			vDisplay := vDisplay || 'Average Selling Rates ' || recPO.Currency_code || '/' || recPO.To_currency || ' announced by the Bank of Thailand as of ' || to_char(dRateDate, 'fmDD Month YYYY' ) || ' = ' || To_char(nRate,'fm999,999.0000') || ' ' ||vFrom || '/' || vTo ;

		ElsIf recRateType.User_conversion_type = 'BUYING' Then -- Average Buying Rate

			vDisplay := vDisplay || 'Equipment PO in ' || recPO.To_currency || ' currency based on Average Buying Rate ' || recPO.Currency_code || '/' || recPO.To_currency || ' announced by the Bank of Thailand at the end of the previous working day on which the Purchase Order is issued.' ;
			vDisplay := vDisplay || CHR(13) ;
			vDisplay := vDisplay || CHR(13) ; -- CHR(10)
			vDisplay := vDisplay || 'Average Buying Rates ' || recPO.Currency_code || '/' || recPO.To_currency || ' announced by the Bank of Thailand as of ' || to_char(dRateDate, 'fmDD Month YYYY' ) || ' = ' || To_char(nRate,'fm999,999.0000') || ' ' ||vFrom || '/' || vTo ;

		End If;

--		vDisplay := vRateType || 'Equipment PO in THB currency based on weighted-average Interbank Exchange Rate USD/THB announced by the Bank of Thailand at the end of the previous working day on which the Purchase Order is issued.' ;
--		vDisplay := vRateType || ' Interbank Exchange Rate as of ' || to_char(dRateDate, 'fmDD Month YYYY' ) || ' is ' || To_char(nRate,'fm999,999.0000') || ' Baht/US Dollar';
--		vDisplay := vRateType || ' Interbank Exchange Rate as of ' || to_char(dRateDate, 'fmDD Month YYYY' ) || ' is ' || To_char(nRate,'fm999,999.0000') || ' ' ||vFrom || '/' || vTo ;

		Return vDisplay;
	
	Exception
		When Others Then
			Return '-- Rate no found -- ';

	End PO_FORM_EXCH_RATE;


End TAC_PO_CURRENCY_PKG;
/