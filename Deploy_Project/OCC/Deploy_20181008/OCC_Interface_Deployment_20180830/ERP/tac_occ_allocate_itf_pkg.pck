CREATE OR REPLACE PACKAGE tac_occ_allocate_itf_pkg IS

-- Original Program : TAC_IC_ITFC_API.IC116_ITFC
-- Author  : TM
-- Created : 20-Jun-2018
-- Purpose : Interface ERP data to OCC

------------------------------------------------------------------------------------------
FUNCTION char2date
(i_text		IN	VARCHAR2
,i_format	IN	VARCHAR2)
RETURN DATE;
------------------------------------------------------------------------------------------
PROCEDURE alloc_dealloc
(err_msg			OUT	VARCHAR2
,err_code			OUT	VARCHAR2
,i_interface_type		VARCHAR2
,i_start_date			VARCHAR2
,i_end_date				VARCHAR2
,i_output_dirobj_name	VARCHAR2
,i_file_name_format		VARCHAR2
,i_encrypt_path			VARCHAR2
,i_encrypt_file_suffix	VARCHAR2
,i_encrypt_passwd_key	VARCHAR2
,i_sftp_host			VARCHAR2
,i_sftp_user			VARCHAR2
,i_sftp_remote_path		VARCHAR2
,i_file_path_error		VARCHAR2
,i_file_path_history	VARCHAR2
,i_separator			VARCHAR2	:=	'|'
,i_to_email				VARCHAR2	:=	'Nawatsapon.Thanaratchaikun@dtac.co.th');
--,i_to_email				VARCHAR2	:=	'ERPOperationSupport@dtac.co.th');
------------------------------------------------------------------------------------------
END tac_occ_allocate_itf_pkg;
/
CREATE OR REPLACE PACKAGE BODY tac_occ_allocate_itf_pkg IS

-- Original Program : TAC_IC_ITFC_API.IC116_ITFC
-- Get query from tac_dms_interface_util.allocate_deallocate
-- Author  : TM
-- Created : 20-Jun-2018
-- Purpose : Interface ERP data to OCC

g_org_id   				NUMBER := FND_PROFILE.VALUE('ORG_ID');
g_log_in   				NUMBER := FND_GLOBAL.CONC_LOGIN_ID;
g_user_id   			NUMBER := FND_GLOBAL.USER_ID;
g_request_id  			NUMBER := FND_GLOBAL.CONC_REQUEST_ID;
g_no_error				VARCHAR2(100)	:=	'No Error';
max_linesize			BINARY_INTEGER	:=	32767;

C_DEFAULT_START_DATE	DATE	:=	TO_DATE('01/01/2001','dd/mm/yyyy');

-----------------------------------------------------------------------------------------
PROCEDURE conc_wait
(
	p_conc_req_id NUMBER
   ,p_phase       OUT NOCOPY VARCHAR2
   ,p_status      OUT NOCOPY VARCHAR2
   ,p_message     OUT NOCOPY VARCHAR2
   ,p_max_wait    NUMBER DEFAULT 60
) IS
	v_phase         VARCHAR2(30);
	v_status        VARCHAR2(30);
	v_dev_phase     VARCHAR2(30);
	v_dev_status    VARCHAR2(30);
	v_message       VARCHAR2(1000);
	v_wait_complete BOOLEAN;
BEGIN
	COMMIT;
	IF p_conc_req_id > 0 THEN
		LOOP
			v_wait_complete := fnd_concurrent.wait_for_request(request_id => p_conc_req_id,
															   INTERVAL   => 10,
															   max_wait   => p_max_wait,
															   -- out arguments
															   phase      => v_phase,
															   status     => v_status,
															   dev_phase  => v_dev_phase,
															   dev_status => v_dev_status,
															   message    => v_message);
			EXIT WHEN v_phase <> 'Running';
			dbms_lock.sleep(2);
		END LOOP;
	END IF;
	p_phase   := v_phase;
	p_status  := v_status;
	p_message := v_message;
END conc_wait;
-----------------------------------------------------------------------------------------
PROCEDURE write_log(param_msg VARCHAR2) IS
BEGIN
	fnd_file.put_line(fnd_file.log, param_msg);
END write_log;
------------------------------------------------------------------------------------------
PROCEDURE write_line
(
	p_file_handle IN OUT utl_file.file_type
   ,p_line_buff   IN VARCHAR2
) IS
	user_error VARCHAR2(255); -- to store translated file_error
BEGIN
	
	utl_file.put_line(p_file_handle, p_line_buff);
	utl_file.fflush(p_file_handle);
EXCEPTION
	WHEN utl_file.invalid_filehandle THEN
		user_error := 'Invalid filehandle';
		write_log(user_error);
	WHEN utl_file.invalid_operation THEN
		user_error := 'Invalid operation';
		write_log(user_error);
	WHEN utl_file.write_error THEN
		user_error := 'Write error';
		write_log(user_error);
	WHEN OTHERS THEN
		user_error := 'Error others.';
		write_log(user_error);
END write_line;
------------------------------------------------------------------------------------------
FUNCTION clear_spchar
(i_input		IN	VARCHAR2
,i_separator	IN	VARCHAR2)
RETURN VARCHAR2
IS
BEGIN
	RETURN REPLACE(REPLACE(REPLACE(LTRIM(i_input,''''),CHR(10)),CHR(13)),i_separator);
END clear_spchar;
------------------------------------------------------------------------------------------
FUNCTION get_directory_path
(p_directory_name	IN	VARCHAR2)
RETURN VARCHAR2 IS
	v_path VARCHAR2(250);
BEGIN
	SELECT	directory_path
	INTO	v_path
	FROM	all_directories
	WHERE	directory_name = p_directory_name;
	
	RETURN v_path;
EXCEPTION
	WHEN OTHERS THEN
		RETURN NULL;
END get_directory_path;
------------------------------------------------------------------------------------------
PROCEDURE encrypt_data_file
(
	p_input_filepath	VARCHAR2
   ,p_input_filename	VARCHAR2
   ,p_output_filepath	VARCHAR2
   ,p_output_filename	VARCHAR2
   ,p_enc_algorithm		VARCHAR2
   ,p_enc_password		VARCHAR2
) IS
	v_encrypt_req_id	NUMBER;
	v_phase           VARCHAR2(100);
	v_status          VARCHAR2(100);
	v_message         VARCHAR2(1000);
BEGIN
	write_log(' ');
	write_log('Encrypting File from path ' || p_input_filepath || ' name ' || p_input_filename || ' to path ' ||
					  p_output_filepath || ' name ' || p_output_filename);
	
	v_encrypt_req_id := fnd_request.submit_request(application => 'FND',
													program     => 'TAC_ENCRYPT_FILE',
													argument1   => p_input_filepath,
													argument2   => p_input_filename,
													argument3   => p_output_filepath,
													argument4   => p_output_filename,
													argument5   => p_enc_algorithm,
													argument6   => p_enc_password);
	COMMIT;
	write_log('Concurrent Encrypt File is ' || v_encrypt_req_id);
	
	IF (v_encrypt_req_id > 0) THEN
		conc_wait(v_encrypt_req_id, v_phase, v_status, v_message, 60);
		IF (v_status = 'E') THEN
			write_log('Encrypting file from path ' || p_input_filepath || ' name ' || p_input_filename || ' to path ' ||
					  p_output_filepath || ' name ' || p_output_filename || ' failed.');
		ELSE
			write_log('Encrypting file from path ' || p_input_filepath || ' name ' || p_input_filename || ' to path ' ||
					  p_output_filepath || ' name ' || p_output_filename || ' successful.');
		END IF;
	ELSE
		write_log('Encrypting file from path ' || p_input_filepath || ' name ' || p_input_filename || ' to path ' ||
				  p_output_filepath || ' name ' || p_output_filename ||
				  ' failed, No concurrent program [TAC_ENCRYPT_FILE].');
	END IF;
	write_log(' ');
EXCEPTION
	WHEN OTHERS THEN
		write_log('Encrypting file from path ' || p_input_filepath || ' name ' || p_input_filename || ' to path ' ||
				  p_output_filepath || ' name ' || p_output_filename || ' failed with error ' || SQLERRM);
END encrypt_data_file;

----------------------------------------------------------------------------
PROCEDURE send_email
(i_smtp_server		IN	VARCHAR2	:=  'mail-gw.tac.co.th'
,i_smtp_port		IN	VARCHAR2	:=  25
,i_from_email		IN	VARCHAR2	:=  'ERPOperationSupport@dtac.co.th'
,i_to_email			IN	VARCHAR2	-- Separate by ,
,i_cc_email			IN	VARCHAR2	:=  NULL
,i_bcc_email		IN	VARCHAR2	:=  NULL
,i_subject			IN	VARCHAR2
,i_reply_email		IN	VARCHAR2	:=  'occ_interface@dtac.co.th'
,i_priority			IN	NUMBER		:=  3   -- Normal priority
,io_status			IN OUT  VARCHAR2
)
IS
	C_ERROR			 CONSTANT VARCHAR2(40)   :=  '$$ERROR$$';
	NEW_LINE_IN_MSG	 CONSTANT VARCHAR2(10)   :=  '<BR>';
	v_blob			  BLOB;
	v_msg_template	  VARCHAR2(32000) :=
		'Dear All,' || NEW_LINE_IN_MSG ||
		NEW_LINE_IN_MSG ||
		'Please be informed that Allocate Interface to OCC executed with ' || io_status || '.' || NEW_LINE_IN_MSG ||
		NEW_LINE_IN_MSG ||
		'Thank you & Regards,' || NEW_LINE_IN_MSG ||
		'ERP auto job' || NEW_LINE_IN_MSG;
	v_msg				v_msg_template%TYPE;

BEGIN
	io_status   :=  G_NO_ERROR;

	v_msg	:=  v_msg_template;

-- Start Temporary for checking email
	write_log('Sending email info:');
	write_log('SMTP Server: ' || i_smtp_server);
	write_log('SMTP Port: ' || i_smtp_port);
	write_log('From email: ' || i_from_email);
	write_log('To email: ' || i_to_email);
	write_log('CC email: ' || i_cc_email);
	write_log('BCC email: ' || i_bcc_email);
	write_log('Email Subject: ' || i_subject);
	write_log('Email Message:');
	write_log(v_msg);
-- End Temporary for checking email

	mail_tools.sendmail
		(smtp_server		=>  i_smtp_server
		,smtp_server_port	=>  i_smtp_port
		,from_name			=>  i_from_email
		,to_name			=>  i_to_email  -- list of TO email addresses separated by commas (,)
		,cc_name			=>  i_cc_email  -- list of CC email addresses separated by commas (,)
		,bcc_name			=>  i_bcc_email -- list of BCC email addresses separated by commas (,)
		,subject			=>  i_subject
		,message			=>  v_msg
		,priority			=>  i_priority  -- 1-5 1 being the highest priority and 3 normal priority
		);
EXCEPTION
	WHEN OTHERS THEN
		io_status	:=	'Error while sending email : ' || SQLERRM;
END send_email;
----------------------------------------------------------------------------
PROCEDURE sftp_outbound
(i_sftp_username	IN	VARCHAR2
,i_sftp_servername	IN	VARCHAR2
,i_sftp_remote_path	IN	VARCHAR2
,i_filepath			IN	VARCHAR2
,i_filename			IN	VARCHAR2
,o_status			OUT	VARCHAR2)
IS
PRAGMA AUTONOMOUS_TRANSACTION;
	v_sub_req_id	NUMBER;
	v_phase			VARCHAR2(100);
	v_status		VARCHAR2(100);
	v_message		VARCHAR2(1000);
BEGIN
	o_status	:=	G_NO_ERROR;
	write_log(' ');
	write_log('SFTP from File Path ' || i_filepath || ' Filename ' || i_filename);
	
	v_sub_req_id	:=	fnd_request.submit_request
							(application	=>	'SQLAP'
							,program		=>	'TAC_SFTP_OUTBOUND'
							,argument1		=>	i_sftp_username
							,argument2		=>	i_sftp_servername
							,argument3		=>	i_sftp_remote_path
							,argument4		=>	i_filepath
							,argument5		=>	i_filename);
	COMMIT;
	write_log('Concurrent to SFTP File is ' || v_sub_req_id);
	
	IF (v_sub_req_id > 0) THEN
		LOOP
			conc_wait(v_sub_req_id, v_phase, v_status, v_message, 60);
			EXIT WHEN v_phase <> 'Running';
			dbms_lock.sleep(2);
		END LOOP;
		write_log('Concurrent to SFTP File phase = ' || v_phase || ' status = ' || v_status || ' message = ' || v_message);
		IF (v_status LIKE 'E%') THEN	-- Error
			o_status	:=	'Error : Could not sftp file';
			write_log('SFTP from File Path ' || i_filepath || ' Filename ' || i_filename || ' failed.');
		ELSE
			write_log('SFTP from File Path ' || i_filepath || ' Filename ' || i_filename || ' successful.');
		END IF;
	ELSE
		o_status	:=	'Error : Could not sftp file, no program to call';
		write_log('SFTP from File Path ' || i_filepath || ' Filename ' || i_filename ||
					' failed, No concurrent program [TAC_SFTP_OUTBOUND].');
	END IF;
	write_log(' ');
EXCEPTION
	WHEN OTHERS THEN
		o_status	:=	'Error : Could not sftp file';
		write_log('SFTP from File Path ' || i_filepath || ' Filename ' || i_filename ||
					' failed with error ' || SQLERRM);
END sftp_outbound;
----------------------------------------------------------------------------
PROCEDURE move_data_file
(i_source_file	IN	VARCHAR2
,i_dest_path	IN	VARCHAR2)
IS
PRAGMA AUTONOMOUS_TRANSACTION;
	v_movefile_req_id	NUMBER;
	v_phase				VARCHAR2(100);
	v_status			VARCHAR2(100);
	v_message			VARCHAR2(1000);
BEGIN
	write_log(' ');
	write_log('Moving File ' || i_source_file || ' to ' || i_dest_path);
	
	v_movefile_req_id	:=	fnd_request.submit_request
								(application	=>	'FND'
								,program		=>	'TACFND_MVFILE'
								,argument1		=>	i_source_file
								,argument2		=>	i_dest_path);
	COMMIT;
	write_log('Concurrent Move File is ' || v_movefile_req_id);
	
	IF (v_movefile_req_id > 0) THEN
		conc_wait(v_movefile_req_id, v_phase, v_status, v_message, 60);
		IF (v_status = 'E') THEN
			write_log('Move file from ' || i_source_file || ' to ' || i_dest_path || ' failed.');
		ELSE
			write_log('Move file from ' || i_source_file || ' to ' || i_dest_path || ' successful.');
		END IF;
	ELSE
		write_log('Move file from ' || i_source_file || ' to ' || i_dest_path ||
					' failed, No concurrent program [TACFND_MVFILE].');
	END IF;
	write_log(' ');
EXCEPTION
	WHEN OTHERS THEN
		write_log('Move file from ' || i_source_file || ' to ' ||
					i_dest_path || ' failed with error ' || SQLERRM);
END move_data_file;
----------------------------------------------------------------------------
FUNCTION char2date
(i_text		IN	VARCHAR2
,i_format	IN	VARCHAR2)
RETURN DATE
IS
BEGIN
	RETURN TO_DATE(i_text, i_format);
EXCEPTION
	WHEN OTHERS THEN
		write_log('Convert text to date error, value is ' || i_text || ' format is ' || i_format);
		RETURN NULL;
END char2date;
----------------------------------------------------------------------------
PROCEDURE alloc_dealloc
(err_msg			OUT	VARCHAR2
,err_code			OUT	VARCHAR2
,i_interface_type		VARCHAR2
,i_start_date			VARCHAR2
,i_end_date				VARCHAR2
,i_output_dirobj_name	VARCHAR2
,i_file_name_format		VARCHAR2
,i_encrypt_path			VARCHAR2
,i_encrypt_file_suffix	VARCHAR2
,i_encrypt_passwd_key	VARCHAR2
,i_sftp_host			VARCHAR2
,i_sftp_user			VARCHAR2
,i_sftp_remote_path		VARCHAR2
,i_file_path_error		VARCHAR2
,i_file_path_history	VARCHAR2
,i_separator			VARCHAR2	:=	'|'
,i_to_email				VARCHAR2	:=	'Nawatsapon.Thanaratchaikun@dtac.co.th')
--,i_to_email				VARCHAR2	:=	'ERPOperationSupport@dtac.co.th')
IS
	-- 25 Jan 10 Modify Sim Allocation Improvement Project : Yodchai L.
	-- Add allocate for subinventory transfer, inter org transfer , inter org receipt, De-allocate for RMA link to manual allocate
	
/*	c_sales_order_source    CONSTANT VARCHAR2(30) := 'ORDER ENTRY';
	c_sales_order_source_in CONSTANT VARCHAR2(30) := 'ORDER ENTRY INTERNAL';
	c_dft_source            CONSTANT VARCHAR2(30) := 'INVENTORY';
	c_intransit_subinv      CONSTANT VARCHAR2(150) := 'INTRANSIT';
	c_order_internal_source CONSTANT VARCHAR2(30) := 'Internal';
	c_rma_trx_type          CONSTANT VARCHAR2(30) := 'RMA Receipt';
	c_return_order_source   CONSTANT VARCHAR2(30) := 'RETURN';
	C_DATE_SWEEP                               CONSTANT NUMBER  :=  6 ;*/
	v_start_date DATE := TRUNC(to_date(i_start_date, 'YYYY/MM/DD HH24:MI:SS'));
	v_end_date   DATE := LEAST(TRUNC(to_date(i_end_date, 'YYYY/MM/DD HH24:MI:SS'))+0.99999,SYSDATE);
	
	--  v_count                 NUMBER          :=  0;
	v_insert_rec		NUMBER := 0;
	v_flag				VARCHAR2(1) := 'Y';
	v_dealer_number		VARCHAR2(240);
	v_file_date			DATE	:=	SYSDATE;
	v_file_path			VARCHAR2(1000);
	v_file_name			VARCHAR2(100);
	v_encrypt_file_name	VARCHAR2(100);
	
	new_line			CONSTANT VARCHAR2(10)	:=	chr(13) || chr(10);
	v_target_charset	VARCHAR2(1000);
	v_db_charset		VARCHAR2(1000);
	v_encrypt_password	VARCHAR2(1000);
	v_status			VARCHAR2(100);
	v_records			NUMBER	:=	0;
	v_bom_raw			RAW(3);

	CURSOR c_data2sent IS
		SELECT	*
		FROM	tac_occ_erp_allocate_tmp t
		WHERE	t.request_id	=	G_REQUEST_ID
		AND		(i_interface_type	=	'ALL' OR 
				EXISTS (SELECT	'a'
						FROM	tac_occ_erp_allocate_sent s
						WHERE	s.obsolete_record	=	'N'
						AND		transaction_group	=	t.transaction_group
						AND		transaction_id		=	t.transaction_id
						AND		s.request_id_first_sent	=	G_REQUEST_ID)
				)
		ORDER BY t.transaction_group, t.document_date, t.order_number, t.transaction_id;

	v_file_handle		utl_file.file_type;
	v_line_data			VARCHAR2(32767);

BEGIN
	SELECT	MAX(DECODE(parameter,'NLS_CHARACTERSET',VALUE))
			,'UTF8'
	INTO	v_db_charset, v_target_charset
	FROM	v$nls_parameters
	WHERE	parameter IN ('NLS_LANGUAGE','NLS_TERRITORY','NLS_CHARACTERSET');

	v_encrypt_password	:=	NULL;
	FOR rec IN (SELECT p.* FROM tac_encryption_password p WHERE p.key_name = i_encrypt_passwd_key)
	LOOP
		v_encrypt_password	:=	rec.password_value;
	END LOOP;

	write_log('+----------------------------------------------------------------------------+');
	write_log('Start Concurrent export Allocate / De-Allocate to OCC');
	write_log('Start Date: ' ||
			  to_char(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
	write_log('+----------------------------------------------------------------------------+');
	write_log('');
	write_log('Agruments');
	write_log('-----------');
	write_log('i_interface_type =  ' || i_interface_type);
	write_log('i_start_date =  ' || i_start_date);
	write_log('i_end_date =  ' || i_end_date);
	write_log('i_output_dirobj_name =  ' || i_output_dirobj_name);
	write_log('i_file_name_format =  ' || i_file_name_format);
	write_log('i_encrypt_path =  ' || i_encrypt_path);
	write_log('i_encrypt_file_suffix =  ' || i_encrypt_file_suffix);
	write_log('i_encrypt_passwd_key =  ' || i_encrypt_passwd_key);
	write_log('i_sftp_host =  ' || i_sftp_host);
	write_log('i_sftp_user =  ' || i_sftp_user);
	write_log('i_sftp_remote_path =  ' || i_sftp_remote_path);
	write_log('i_file_path_error =  ' || i_file_path_error);
	write_log('i_file_path_history =  ' || i_file_path_history);
	write_log('i_separator =  ' || i_separator);
	write_log('+----------------------------------------------------------------------------+');

	-- validate parameter
	IF i_output_dirobj_name IS NULL THEN
		RAISE utl_file.invalid_path;
	END IF;
	IF i_file_name_format IS NULL THEN
		RAISE utl_file.invalid_filename;
	END IF;
	IF v_encrypt_password IS NULL THEN
		err_msg := 'ERROR: No encryption password key "' || i_encrypt_passwd_key || '" defined in TAC_ENCRYPTION_PASSWORD table, please correct setup';
		write_log(err_msg);
		raise_application_error(-20021, err_msg);
	END IF;

	v_file_name := i_file_name_format || '.DAT';
	v_file_name := REPLACE(v_file_name,
						   '$DATETIME$',
						   to_char(v_file_date, 'YYYYMMDD_HH24MISS'));
	v_file_name := REPLACE(v_file_name,
						   '$DATE$',
						   to_char(v_file_date, 'YYYYMMDD'));

	write_log('+---------------------------------------------------------------------------+');
	write_log('Prepare data to interface.');
	write_log(' ');
	err_code     := '2';

	INSERT INTO tac_occ_erp_allocate_tmp
		(request_id
-- Added 25-Jul-2018
		,inv_org_id
		,warehouse_code
		,ou_code
-- End Added 25-Jul-2018
		,order_number
		,document_date
		,transaction_type
		,partner_code
		,product_code
		,serial_number
		,unit_selling_price
		,qty	-- Added 16-Jul-2018
		,source_name
		,warranty_date
		,expiry_date
		,imei_number
		,telephone_number
		,transaction_group
		,transaction_id
		,process_date)
	-- Non SIM, DEVICE, AIRCARD
	SELECT	g_request_id
-- Added 25-Jul-2018
		,mmt.organization_id
		,mpt.organization_code
		,decode(mpm.organization_code,'Z00','DTAC','P00','PSB','DTN') company_code
-- End Added 25-Jul-2018
		,to_char(ooh.order_number) order_number
		,mmt.transaction_date document_date -- format date  'DD/MM/YYYY HH24:MI:SS'
		,CASE WHEN mmt.transaction_type_id = 33 THEN 'A' ELSE 'D' END transaction_type
		,ship_cas.attribute6 partner_code
		,msi.segment1 product_code
		,NULL	serial_number -- 'SIM': show serial number, 'DEVICE': Item number
		,ool.unit_selling_price unit_selling_price
		,ABS(mmt.transaction_quantity)	-- Added 16-Jul-2018
		,'ERP' source_name
		,NULL warranty_date
		,NULL	/*char2date(msn.attribute2, 'yyyy/mm/dd hh24:mi:ss')*/ expiry_date
		,NULL	imei_number -- 'SIM': show NULL, 'DEVICE': attribute1, 'AIRCARD+SIM' show attribute3 (UD item number)
		,NULL	telephone_number -- 'SIM': show attribute1, 'DEVICE': null, 'AIRCARD+SIM' show attribute1
		,'1. SO Issue & RMA Receipt' transaction_group
		,mmt.transaction_id
		,SYSDATE
	FROM mtl_material_transactions mmt,
	   mtl_transaction_types     mtt,
	   mtl_system_items          msi,
	   fnd_lookup_values         flv,
	   oe_order_lines_all        ool,
	   oe_order_headers_all      ooh,
	   oe_order_sources          oos,
--	   mtl_unit_transactions     mut,
	   hz_cust_accounts          cust_acct,
	   hz_parties                party,
	   hz_cust_site_uses_all     ship_su,
	   hz_cust_acct_sites_all    ship_cas,
	   hz_party_sites            ship_party_site,
	   hz_locations              ship_loc
-- Added 25-Jul-2018
	   ,mtl_parameters			mpt
	   ,mtl_parameters			mpm
-- End Added 25-Jul-2018
--	   mtl_serial_numbers        msn
	WHERE 1 = 1
-- Added 25-Jul-2018
	AND	mpm.organization_code IN ('Z00', 'P00', 'N00')
	AND	mpt.master_organization_id	=	mpm.organization_id
	AND	mpt.organization_id			=	mmt.organization_id
-- End Added 25-Jul-2018
-- change transaction to creation date
	AND mmt.transaction_date BETWEEN v_start_date AND v_end_date
/*	AND mmt.creation_date >= v_start_date
	AND mmt.creation_date <= v_end_date*/
	AND mmt.transaction_type_id IN (33, 15) -- (33: 'Sales order issue',15: 'RMA Receipt')
	AND mtt.transaction_type_id = mmt.transaction_type_id
	AND msi.inventory_item_id = mmt.inventory_item_id
	AND msi.organization_id = mmt.organization_id
	AND flv.lookup_code = msi.item_type
	AND flv.lookup_type = 'ITEM_TYPE'
	AND flv.attribute14 NOT IN ('SIM','DEVICE','AIRCARD')
	AND ool.line_id = mmt.trx_source_line_id
	AND ooh.header_id = ool.header_id
	AND oos.order_source_id = ooh.order_source_id
	AND cust_acct.cust_account_id(+) = ooh.sold_to_org_id
	AND cust_acct.party_id = party.party_id(+)
	AND ooh.ship_to_org_id = ship_su.site_use_id(+)
	AND ship_su.cust_acct_site_id = ship_cas.cust_acct_site_id(+)
	AND ship_cas.party_site_id = ship_party_site.party_site_id
	AND ship_cas.attribute6 IS NOT NULL -- ship_cas.ATTRIBUTE6: Partner_code,
	AND ship_party_site.location_id = ship_loc.location_id
	UNION ALL
	-- Only SIM, DEVICE, AIRCARD
	SELECT	g_request_id
-- Added 25-Jul-2018
		,mmt.organization_id
		,mpt.organization_code
		,decode(mpm.organization_code,'Z00','DTAC','P00','PSB','DTN') company_code
-- End Added 25-Jul-2018
		,to_char(ooh.order_number) order_number
		,mmt.transaction_date document_date -- format date  'DD/MM/YYYY HH24:MI:SS'
		,CASE WHEN mmt.transaction_type_id = 33 THEN 'A' ELSE 'D' END transaction_type
		,ship_cas.attribute6 partner_code
		,msi.segment1 product_code
		,decode(flv.attribute14,'DEVICE',NULL,mut.serial_number) serial_number -- 'SIM': show serial number, 'DEVICE': Item number
		,ool.unit_selling_price unit_selling_price
		,1	-- Added 16-Jul-2018
		,'ERP' source_name
		,NULL warranty_date
		,char2date(msn.attribute2, 'yyyy/mm/dd hh24:mi:ss') expiry_date,
		 decode(flv.attribute14, 'DEVICE', nvl(msn.attribute1, mut.serial_number),
				'AIRCARD', msn.attribute3, NULL) imei_number -- 'SIM': show NULL, 'DEVICE': attribute1, 'AIRCARD+SIM' show attribute3 (UD item number)
		,decode(flv.attribute14, 'DEVICE', NULL, msn.attribute1) telephone_number -- 'SIM': show attribute1, 'DEVICE': null, 'AIRCARD+SIM' show attribute1
		,'1. SO Issue & RMA Receipt' transaction_group
		,mmt.transaction_id
		,SYSDATE
	FROM mtl_material_transactions mmt,
	   mtl_transaction_types     mtt,
	   mtl_system_items          msi,
	   fnd_lookup_values         flv,
	   oe_order_lines_all        ool,
	   oe_order_headers_all      ooh,
	   oe_order_sources          oos,
	   mtl_unit_transactions     mut,
	   hz_cust_accounts          cust_acct,
	   hz_parties                party,
	   hz_cust_site_uses_all     ship_su,
	   hz_cust_acct_sites_all    ship_cas,
	   hz_party_sites            ship_party_site,
	   hz_locations              ship_loc,
	   mtl_serial_numbers        msn
-- Added 25-Jul-2018
	   ,mtl_parameters			mpt
	   ,mtl_parameters			mpm
-- End Added 25-Jul-2018
	WHERE 1 = 1
-- Added 25-Jul-2018
	AND	mpm.organization_code IN ('Z00', 'P00', 'N00')
	AND	mpt.master_organization_id	=	mpm.organization_id
	AND	mpt.organization_id			=	mmt.organization_id
-- End Added 25-Jul-2018
-- change transaction to creation date
	AND mmt.transaction_date BETWEEN v_start_date AND v_end_date
/*	AND mmt.creation_date >= v_start_date
	AND mmt.creation_date <= v_end_date*/
	AND mmt.transaction_type_id IN (33, 15) -- (33: 'Sales order issue',15: 'RMA Receipt')
	AND mtt.transaction_type_id = mmt.transaction_type_id
	AND msi.inventory_item_id = mmt.inventory_item_id
	AND msi.organization_id = mmt.organization_id
	AND flv.lookup_code = msi.item_type
	AND flv.lookup_type = 'ITEM_TYPE'
	AND flv.attribute14 IS NOT NULL
	AND flv.attribute14 IN ('SIM','DEVICE','AIRCARD')
	AND ool.line_id = mmt.trx_source_line_id
	AND mut.transaction_id = mmt.transaction_id
	AND mut.organization_id = mmt.organization_id
	AND ooh.header_id = ool.header_id
	AND oos.order_source_id = ooh.order_source_id
	AND cust_acct.cust_account_id(+) = ooh.sold_to_org_id
	AND cust_acct.party_id = party.party_id(+)
	AND ooh.ship_to_org_id = ship_su.site_use_id(+)
	AND ship_su.cust_acct_site_id = ship_cas.cust_acct_site_id(+)
	AND ship_cas.party_site_id = ship_party_site.party_site_id
	AND ship_cas.attribute6 IS NOT NULL -- ship_cas.ATTRIBUTE6: Partner_code,
	AND ship_party_site.location_id = ship_loc.location_id
	AND mut.inventory_item_id = msn.inventory_item_id
	AND mut.serial_number = msn.serial_number;

	write_log('');
	write_log('+---------------------------------------------------------------------------+');
	v_insert_rec	:=	v_insert_rec + SQL%ROWCOUNT;
	write_log('Insert Complete Sales Order Issue & RMA Receipt : ' || SQL%ROWCOUNT || ' records.');

	-- 2. Sales to Own distribution (Internal Order -> Inventory Org A01,B01,C01,D01,E01)
	-- Yodchai L. 30-Sep-2013 join attribute from mtl_serial_numbers
	INSERT INTO tac_occ_erp_allocate_tmp
		(request_id
-- Added 25-Jul-2018
		,inv_org_id
		,warehouse_code
		,ou_code
-- End Added 25-Jul-2018
		,order_number
		,document_date
		,transaction_type
		,partner_code
		,product_code
		,serial_number
		,unit_selling_price
		,qty	-- Added 16-Jul-2018
		,source_name
		,warranty_date
		,expiry_date
		,imei_number
		,telephone_number
		,transaction_group
		,transaction_id
		,process_date)
	-- Non SIM, DEVICE, AIRCARD
	SELECT g_request_id
-- Added 25-Jul-2018
		,mmt.organization_id
		,mpt.organization_code
		,decode(mpm.organization_code,'Z00','DTAC','P00','PSB','DTN') company_code
-- End Added 25-Jul-2018
		,to_char(ooh.order_number) order_number
		,mmt.transaction_date document_date -- format date  'DD/MM/YYYY HH24:MI:SS'
		,'A' transaction_type
		 -- Fix bug Yodchai L. 20-Sep-13
		,sub_tran.attribute5 partner_code
		,msi.segment1 product_code
		,NULL/*decode(flv.attribute14,'DEVICE',NULL,mut.serial_number)*/ serial_number -- 'SIM': show serial number, 'DEVICE': Item number
		,0 unit_selling_price
		,ABS(mmt.transaction_quantity)	-- Added 16-Jul-2018
		,'ERP' source_name
		,NULL warranty_date
		,NULL	/*char2date(msn.attribute2, 'yyyy/mm/dd hh24:mi:ss')*/ expiry_date
		,NULL	imei_number -- 'SIM': show NULL, 'DEVICE': attribute1, 'AIRCARD+SIM' show attribute3 (UD item number)
		,NULL	telephone_number -- 'SIM': show attribute1, 'DEVICE': null, 'AIRCARD+SIM' show attribute1
		,'2. Internal Order to A01,B01,C01,D01,E01' transaction_group
		,mmt.transaction_id
		,SYSDATE
	FROM mtl_material_transactions mmt,
	   mtl_transaction_types     mtt,
	   mtl_system_items          msi,
	   fnd_lookup_values         flv,
	   oe_order_lines_all        ool,
	   oe_order_headers_all      ooh,
--	   mtl_unit_transactions     mut,
	   mtl_secondary_inventories sub,
	   mtl_secondary_inventories sub_tran
--	   mtl_serial_numbers        msn
-- Added 25-Jul-2018
	   ,mtl_parameters			mpt
	   ,mtl_parameters			mpm
-- End Added 25-Jul-2018
	WHERE 1 = 1
-- Added 25-Jul-2018
	AND	mpm.organization_code IN ('Z00', 'P00', 'N00')
	AND	mpt.master_organization_id	=	mpm.organization_id
	AND	mpt.organization_id			=	mmt.organization_id
-- End Added 25-Jul-2018
-- change transaction to creation date
	AND mmt.transaction_date BETWEEN v_start_date AND v_end_date
/*	AND mmt.creation_date >= v_start_date
	AND mmt.creation_date <= v_end_date*/
	AND mmt.transaction_type_id IN (62) -- 62: Transaction type = 'Int Order Intr Ship', Source = 'Internal Order'
	AND mmt.transaction_type_id = mtt.transaction_type_id
	AND msi.inventory_item_id = mmt.inventory_item_id
	AND msi.organization_id = mmt.organization_id
	AND flv.lookup_code = msi.item_type
	AND flv.lookup_type = 'ITEM_TYPE'
	AND flv.attribute14 NOT IN ('SIM','DEVICE','AIRCARD')
	AND ool.line_id = mmt.trx_source_line_id
	AND ooh.header_id = ool.header_id
	--and mmt.organization_id = org_mast.organization_id
	--and mmt.transfer_organization_id = org_tran.organization_id(+)
	AND mmt.organization_id = sub.organization_id
	AND mmt.subinventory_code = sub.secondary_inventory_name
	AND mmt.transfer_organization_id = sub_tran.organization_id(+)
	AND mmt.transfer_subinventory = sub_tran.secondary_inventory_name(+)
	   -- Fix bug Yodchai L. 07-Aug-13
	AND sub_tran.attribute5 IS NOT NULL
	-- and sub.attribute5 is not null -- don't fix RTR at source
	UNION ALL
	-- Only SIM, DEVICE, AIRCARD
	SELECT g_request_id
-- Added 25-Jul-2018
		,mmt.organization_id
		,mpt.organization_code
		,decode(mpm.organization_code,'Z00','DTAC','P00','PSB','DTN') company_code
-- End Added 25-Jul-2018
		,to_char(ooh.order_number) order_number
		,mmt.transaction_date document_date -- format date  'DD/MM/YYYY HH24:MI:SS'
		,'A' transaction_type
		 -- Fix bug Yodchai L. 20-Sep-13
		,sub_tran.attribute5 partner_code
		,msi.segment1 product_code
		,decode(flv.attribute14,'DEVICE',NULL,mut.serial_number) serial_number -- 'SIM': show serial number, 'DEVICE': Item number
		,0 unit_selling_price
		,1	-- Added 16-Jul-2018
		,'ERP' source_name
		,NULL warranty_date
		,char2date(msn.attribute2, 'yyyy/mm/dd hh24:mi:ss') expiry_date
		,decode(flv.attribute14,'DEVICE',nvl(msn.attribute1, mut.serial_number),
				'AIRCARD',msn.attribute3,NULL) imei_number -- 'SIM': show NULL, 'DEVICE': attribute1, 'AIRCARD+SIM' show attribute3 (UD item number)
		,decode(flv.attribute14,'DEVICE',NULL,msn.attribute1) telephone_number -- 'SIM': show attribute1, 'DEVICE': null, 'AIRCARD+SIM' show attribute1
		,'2. Internal Order to A01,B01,C01,D01,E01' transaction_group
		,mmt.transaction_id
		,SYSDATE
	FROM mtl_material_transactions mmt,
	   mtl_transaction_types     mtt,
	   mtl_system_items          msi,
	   fnd_lookup_values         flv,
	   oe_order_lines_all        ool,
	   oe_order_headers_all      ooh,
	   mtl_unit_transactions     mut,
	   mtl_secondary_inventories sub,
	   mtl_secondary_inventories sub_tran,
	   mtl_serial_numbers        msn
-- Added 25-Jul-2018
	   ,mtl_parameters			mpt
	   ,mtl_parameters			mpm
-- End Added 25-Jul-2018
	WHERE 1 = 1
-- Added 25-Jul-2018
	AND	mpm.organization_code IN ('Z00', 'P00', 'N00')
	AND	mpt.master_organization_id	=	mpm.organization_id
	AND	mpt.organization_id			=	mmt.organization_id
-- End Added 25-Jul-2018
-- change transaction to creation date
	AND mmt.transaction_date BETWEEN v_start_date AND v_end_date
/*	AND mmt.creation_date >= v_start_date
	AND mmt.creation_date <= v_end_date*/
	AND mmt.transaction_type_id IN (62) -- 62: Transaction type = 'Int Order Intr Ship', Source = 'Internal Order'
	AND mmt.transaction_type_id = mtt.transaction_type_id
	AND msi.inventory_item_id = mmt.inventory_item_id
	AND msi.organization_id = mmt.organization_id
	AND flv.lookup_code = msi.item_type
	AND flv.lookup_type = 'ITEM_TYPE'
	AND flv.attribute14 IS NOT NULL
	AND flv.attribute14 IN ('SIM','DEVICE','AIRCARD')
	AND ool.line_id = mmt.trx_source_line_id
	AND mut.transaction_id = mmt.transaction_id
	AND mut.organization_id = mmt.organization_id
	AND mut.inventory_item_id = msn.inventory_item_id
	AND mut.serial_number = msn.serial_number
	AND ooh.header_id = ool.header_id
	--and mmt.organization_id = org_mast.organization_id
	--and mmt.transfer_organization_id = org_tran.organization_id(+)
	AND mmt.organization_id = sub.organization_id
	AND mmt.subinventory_code = sub.secondary_inventory_name
	AND mmt.transfer_organization_id = sub_tran.organization_id(+)
	AND mmt.transfer_subinventory = sub_tran.secondary_inventory_name(+)
	   -- Fix bug Yodchai L. 07-Aug-13
	AND sub_tran.attribute5 IS NOT NULL;
	/*                          and nvl(sub_tran.attribute5, (select min(attribute5)
	from mtl_secondary_inventories s
	where s.organization_id = mmt.transfer_organization_id)) is not null */
	--  and nvl(sub_tran.attribute5, (select min(attribute5) from  mtl_secondary_inventories s where s.ORGANIZATION_ID =mmt.TRANSFER_ORGANIZATION_ID)) <> 5013015380 -- c_wh_rtr_code (destination is not null only)

	v_insert_rec	:=	v_insert_rec + SQL%ROWCOUNT;
	write_log('Insert Complete Internal Order to W/H A01,B01,C01,D01,E01 : ' || SQL%ROWCOUNT || ' records.');

	-- 3. Sales return from own distribution (return from Inventory Org A01,B01,C01,D01,E01 to A06)
	-- Yodchai L. 30-Sep-2013 join attribute from mtl_serial_numbers
	INSERT INTO tac_occ_erp_allocate_tmp
		(request_id
-- Added 25-Jul-2018
		,inv_org_id
		,warehouse_code
		,ou_code
-- End Added 25-Jul-2018
		,order_number
		,document_date
		,transaction_type
		,partner_code
		,product_code
		,serial_number
		,unit_selling_price
		,qty	-- Added 16-Jul-2018
		,source_name
		,warranty_date
		,expiry_date
		,imei_number
		,telephone_number
		,transaction_group
		,transaction_id
		,process_date)
	-- Non SIM, DEVICE, AIRCARD
	SELECT g_request_id
-- Added 25-Jul-2018
		,mmt.organization_id
		,mpt.organization_code
		,decode(mpm.organization_code,'Z00','DTAC','P00','PSB','DTN') company_code
-- End Added 25-Jul-2018
		,mmt.transaction_source_name order_number
		,mmt.transaction_date document_date -- format date  'DD/MM/YYYY HH24:MI:SS'
		,'D' transaction_type
		,sub_tran.attribute5 partner_code
		,msi.segment1 product_code
		-- 'SIM': show serial number, 'DEVICE': Item number
		,NULL/*decode(flv.attribute14,'DEVICE',NULL,mut.serial_number)*/ serial_number
		,0 unit_selling_price
		,ABS(mmt.transaction_quantity)	-- Added 16-Jul-2018
		,'ERP' source_name
		,NULL warranty_date
		,NULL	/*char2date(msn.attribute2, 'yyyy/mm/dd hh24:mi:ss')*/ expiry_date
		-- 'SIM': show NULL, 'DEVICE': attribute1, 'AIRCARD+SIM' show attribute3 (UD item number)
		,NULL	imei_number
		-- 'SIM': show attribute1, 'DEVICE': null, 'AIRCARD+SIM' show attribute1
		,NULL	telephone_number
		,'3. Return from W/H A01,B01,C01,D01,E01 to A06' transaction_group
		,mmt.transaction_id
		,SYSDATE
	FROM mtl_material_transactions mmt,
	   mtl_transaction_types     mtt,
	   mtl_system_items          msi,
	   fnd_lookup_values         flv,
	   mtl_parameters            org_mast,
	   mtl_parameters            org_tran,
--	   mtl_unit_transactions     mut,
	   mtl_secondary_inventories sub,
	   mtl_secondary_inventories sub_tran
--	   mtl_serial_numbers        msn
-- Added 25-Jul-2018
	   ,mtl_parameters			mpt
	   ,mtl_parameters			mpm
-- End Added 25-Jul-2018
	WHERE 1 = 1
-- Added 25-Jul-2018
	AND	mpm.organization_code IN ('Z00', 'P00', 'N00')
	AND	mpt.master_organization_id	=	mpm.organization_id
	AND	mpt.organization_id			=	mmt.organization_id
-- End Added 25-Jul-2018
-- change transaction to creation date
	AND mmt.transaction_date BETWEEN v_start_date AND v_end_date
/*	AND mmt.creation_date >= v_start_date
	AND mmt.creation_date <= v_end_date*/
	AND mmt.transaction_type_id IN (164) --'Receive return from shop (I)'
	AND mtt.transaction_type_id = mmt.transaction_type_id
	AND msi.inventory_item_id = mmt.inventory_item_id
	AND mmt.organization_id = msi.organization_id
	AND flv.lookup_code = msi.item_type
	AND flv.lookup_type = 'ITEM_TYPE'
	AND flv.attribute14 NOT IN ('SIM','DEVICE','AIRCARD')
	AND mmt.organization_id = org_mast.organization_id
	AND mmt.transfer_organization_id = org_tran.organization_id(+)
	AND mmt.organization_id = sub.organization_id
	AND mmt.subinventory_code = sub.secondary_inventory_name
	AND mmt.transaction_source_name LIKE '44%' -- for own distribute beginning with '44'
	AND mmt.transfer_organization_id = sub_tran.organization_id
	   --and substr(mmt.transaction_source_name, 3, 5) = sub_tran.secondary_inventory_name
	   -- Update Yodchai L . To support DTN, PAY, TYP doc no format change 01-Aug-13
	AND CASE WHEN upper(substr(mmt.transaction_source_name,3,1)) BETWEEN 'A' AND 'Z' THEN
			 substr(mmt.transaction_source_name,3,6)
		ELSE substr(mmt.transaction_source_name,3,5) END = sub_tran.secondary_inventory_name
								   
	   --and sub.attribute5 is not null -- (don't concern RTR at destination because this case is return)
	AND sub_tran.attribute5 IS NOT NULL
	UNION ALL
	-- Only SIM, DEVICE, AIRCARD
	SELECT g_request_id
-- Added 25-Jul-2018
		,mmt.organization_id
		,mpt.organization_code
		,decode(mpm.organization_code,'Z00','DTAC','P00','PSB','DTN') company_code
-- End Added 25-Jul-2018
		,mmt.transaction_source_name order_number
		,mmt.transaction_date document_date -- format date  'DD/MM/YYYY HH24:MI:SS'
		,'D' transaction_type
		,sub_tran.attribute5 partner_code
		,msi.segment1 product_code
		-- 'SIM': show serial number, 'DEVICE': Item number
		,decode(flv.attribute14,'DEVICE',NULL,mut.serial_number) serial_number
		,0 unit_selling_price
		,1	-- Added 16-Jul-2018
		,'ERP' source_name
		,NULL warranty_date
		,char2date(msn.attribute2, 'yyyy/mm/dd hh24:mi:ss') expiry_date
		-- 'SIM': show NULL, 'DEVICE': attribute1, 'AIRCARD+SIM' show attribute3 (UD item number)
		,decode(flv.attribute14,'DEVICE',nvl(msn.attribute1, mut.serial_number),'AIRCARD',msn.attribute3,NULL) imei_number
		-- 'SIM': show attribute1, 'DEVICE': null, 'AIRCARD+SIM' show attribute1
		,decode(flv.attribute14,'DEVICE',NULL,msn.attribute1) telephone_number
		,'3. Return from W/H A01,B01,C01,D01,E01 to A06' transaction_group
		,mmt.transaction_id
		,SYSDATE
	FROM mtl_material_transactions mmt,
	   mtl_transaction_types     mtt,
	   mtl_system_items          msi,
	   fnd_lookup_values         flv,
	   mtl_parameters            org_mast,
	   mtl_parameters            org_tran,
	   mtl_unit_transactions     mut,
	   mtl_secondary_inventories sub,
	   mtl_secondary_inventories sub_tran,
	   mtl_serial_numbers        msn
-- Added 25-Jul-2018
	   ,mtl_parameters			mpt
	   ,mtl_parameters			mpm
-- End Added 25-Jul-2018
	WHERE 1 = 1
-- Added 25-Jul-2018
	AND	mpm.organization_code IN ('Z00', 'P00', 'N00')
	AND	mpt.master_organization_id	=	mpm.organization_id
	AND	mpt.organization_id			=	mmt.organization_id
-- End Added 25-Jul-2018
-- change transaction to creation date
	AND mmt.transaction_date BETWEEN v_start_date AND v_end_date
/*	AND mmt.creation_date >= v_start_date
	AND mmt.creation_date <= v_end_date*/
	AND mmt.transaction_type_id IN (164) --'Receive return from shop (I)'
	AND mtt.transaction_type_id = mmt.transaction_type_id
	AND msi.inventory_item_id = mmt.inventory_item_id
	AND mmt.organization_id = msi.organization_id
	AND flv.lookup_code = msi.item_type
	AND flv.lookup_type = 'ITEM_TYPE'
	AND flv.attribute14 IS NOT NULL
	AND flv.attribute14 IN ('SIM','DEVICE','AIRCARD')
	AND mmt.organization_id = org_mast.organization_id
	AND mmt.transfer_organization_id = org_tran.organization_id(+)
	AND mut.transaction_id = mmt.transaction_id
	AND mut.organization_id = mmt.organization_id
	AND mut.inventory_item_id = msn.inventory_item_id
	AND mut.serial_number = msn.serial_number
	AND mmt.organization_id = sub.organization_id
	AND mmt.subinventory_code = sub.secondary_inventory_name
	AND mmt.transaction_source_name LIKE '44%' -- for own distribute beginning with '44'
	AND mmt.transfer_organization_id = sub_tran.organization_id
	   --and substr(mmt.transaction_source_name, 3, 5) = sub_tran.secondary_inventory_name
	   -- Update Yodchai L . To support DTN, PAY, TYP doc no format change 01-Aug-13
	AND CASE WHEN upper(substr(mmt.transaction_source_name,3,1)) BETWEEN 'A' AND 'Z' THEN
			 substr(mmt.transaction_source_name,3,6)
		ELSE substr(mmt.transaction_source_name,3,5) END = sub_tran.secondary_inventory_name
								   
	   --and sub.attribute5 is not null -- (don't concern RTR at destination because this case is return)
	AND sub_tran.attribute5 IS NOT NULL;

	v_insert_rec	:=	v_insert_rec + SQL%ROWCOUNT;
	write_log('Insert Complete Return from Inventory Org A01,B01,C01,D01,E01 to A06 : ' || SQL%ROWCOUNT || ' records.');

	write_log('+---------------------------------------------------------------------------+');
	write_log('');
	write_log('+---------------------------------------------------------------------------+');
	--write_log('Complete : '||TO_CHAR(SQL%ROWCOUNT)||' records.');
	write_log('Insert Complete Total records : ' || v_insert_rec || ' records.');
	write_log('+---------------------------------------------------------------------------+');
	COMMIT;

-- Mark Obsolete for record not send this time
	UPDATE	tac_occ_erp_allocate_sent s
	SET		s.obsolete_record		=	'Y'
			,s.request_id_4obsolete	=	G_REQUEST_ID
	WHERE	obsolete_record		=	'N'
	AND		(transaction_group, transaction_id, transaction_type) IN
			(SELECT	DISTINCT transaction_group, transaction_id, transaction_type
			FROM	(SELECT	order_number
					-- Added 25-Jul-2018
							,inv_org_id
							,warehouse_code
							,ou_code
					-- End Added 25-Jul-2018
							,document_date
							,transaction_type
							,NVL(partner_code,' ')
							,product_code
							,NVL(serial_number,' ')
							,NVL(unit_selling_price,0)
							,source_name
							,NVL(warranty_date,C_DEFAULT_START_DATE)
							,NVL(expiry_date,C_DEFAULT_START_DATE)
							,NVL(imei_number,' ')
							,NVL(telephone_number,' ')
							,transaction_group
							,transaction_id
					FROM	tac_occ_erp_allocate_sent s
					WHERE	s.obsolete_record	=	'N'
					MINUS
					SELECT	order_number
					-- Added 25-Jul-2018
							,inv_org_id
							,warehouse_code
							,ou_code
					-- End Added 25-Jul-2018
							,document_date
							,transaction_type
							,NVL(partner_code,' ')
							,product_code
							,NVL(serial_number,' ')
							,NVL(unit_selling_price,0)
							,source_name
							,NVL(warranty_date,C_DEFAULT_START_DATE)
							,NVL(expiry_date,C_DEFAULT_START_DATE)
							,NVL(imei_number,' ')
							,NVL(telephone_number,' ')
							,transaction_group
							,transaction_id
					FROM	tac_occ_erp_allocate_tmp
					WHERE	request_id	=	G_REQUEST_ID)
			)
	;

	INSERT INTO tac_occ_erp_allocate_sent
		(order_number
-- Added 25-Jul-2018
		,inv_org_id
		,warehouse_code
		,ou_code
-- End Added 25-Jul-2018
		,document_date
		,transaction_type
		,partner_code
		,product_code
		,serial_number
		,unit_selling_price
		,source_name
		,warranty_date
		,expiry_date
		,imei_number
		,telephone_number
		,transaction_group
		,transaction_id
		,request_id_first_sent
		,obsolete_record)
	SELECT	order_number
	-- Added 25-Jul-2018
			,inv_org_id
			,warehouse_code
			,ou_code
	-- End Added 25-Jul-2018
			,document_date
			,transaction_type
			,partner_code
			,product_code
			,serial_number
			,unit_selling_price
			,source_name
			,warranty_date
			,expiry_date
			,imei_number
			,telephone_number
			,transaction_group
			,transaction_id
			,G_REQUEST_ID
			,'N'	-- Not obsolete
	FROM	tac_occ_erp_allocate_tmp
	WHERE	(transaction_group, transaction_id, transaction_type) IN
			(SELECT	transaction_group, transaction_id, transaction_type
			FROM	tac_occ_erp_allocate_tmp
			WHERE	request_id	=	G_REQUEST_ID
			MINUS
			SELECT	transaction_group, transaction_id, transaction_type
			FROM	tac_occ_erp_allocate_sent s
			WHERE	s.obsolete_record	=	'N')
	;

	-- open file for write item master
	write_log('+++++++++++++++++++++++++++++++++++++++++++++++');
	write_log('Create file ' || get_directory_path(i_output_dirobj_name) || '/' ||
			  v_file_name || ' for write data');
	
	v_file_handle := utl_file.fopen(i_output_dirobj_name,
									v_file_name,
									'w');
	BEGIN
		utl_file.fclose(v_file_handle);
	EXCEPTION
		WHEN OTHERS THEN
			NULL;
	END;
	v_file_handle := utl_file.fopen(i_output_dirobj_name,
									v_file_name,
									'w',
									max_linesize);
	v_bom_raw     := hextoraw('EFBBBF');
	utl_file.put_raw(v_file_handle, v_bom_raw);
	
	FOR r_data IN c_data2sent
	LOOP
		v_line_data := clear_spchar(r_data.ou_code, i_separator) ||	-- Added 25-Jul-2018
					   i_separator ||	-- Added 25-Jul-2018
					   clear_spchar(r_data.order_number, i_separator) ||
					   i_separator ||
					   clear_spchar(TO_CHAR(r_data.document_date,'DD/MM/YYYY HH24:MI:SS'), i_separator) ||
					   i_separator ||
					   clear_spchar(r_data.transaction_type, i_separator) ||
					   i_separator ||
					   clear_spchar(r_data.partner_code, i_separator) ||
					   i_separator ||
					   clear_spchar(r_data.product_code, i_separator) ||
					   i_separator ||
					   clear_spchar(r_data.serial_number, i_separator) ||
					   i_separator ||
					   clear_spchar(r_data.unit_selling_price, i_separator) ||
					   i_separator ||
					   clear_spchar(r_data.source_name, i_separator) ||
					   i_separator ||
					   clear_spchar(TO_CHAR(r_data.warranty_date,'DD/MM/YYYY'), i_separator) ||
					   i_separator ||
					   clear_spchar(TO_CHAR(r_data.expiry_date,'DD/MM/YYYY'), i_separator) ||
					   i_separator ||
					   clear_spchar(r_data.imei_number, i_separator) ||
					   i_separator ||
					   clear_spchar(r_data.telephone_number, i_separator) ||
					   i_separator ||
					   clear_spchar(r_data.qty, i_separator);	-- Added 16-Jul-2018
		
		write_line(v_file_handle, CONVERT(v_line_data, v_target_charset, v_db_charset));
--		write_log(v_line_data);
		v_records	:=	v_records + 1;
	END LOOP;
	write_log('+---------------------------------------------------------------------------+');
	
	IF utl_file.is_open(v_file_handle) THEN
		utl_file.fclose(v_file_handle);
	END IF;
	
-- Encrypt file
	v_file_path	:=	get_directory_path(i_output_dirobj_name);
	IF v_file_path LIKE '%/' THEN
		v_file_path	:=	RTRIM(v_file_path,'/');
	END IF;
	v_encrypt_file_name := v_file_name || i_encrypt_file_suffix;

	encrypt_data_file(v_file_path,
				   v_file_name,
				   NVL(i_encrypt_path,v_file_path),
				   v_encrypt_file_name,
				   'AES256-cbc',
				   v_encrypt_password);

-- Write SYNC file
	v_file_name	:=	SUBSTR(v_file_name, 1, LENGTH(v_file_name)-3) || 'SYNC';
	v_file_handle := utl_file.fopen(i_output_dirobj_name,
									v_file_name,
									'w');
	v_bom_raw     := hextoraw('EFBBBF');
	utl_file.put_raw(v_file_handle, v_bom_raw);
	write_line(v_file_handle, TO_CHAR(v_records));
	IF utl_file.is_open(v_file_handle) THEN
		utl_file.fclose(v_file_handle);
	END IF;
	
--	GOTO skip_sync_sftp;	-- skip_sync_sftp

-- Encrypt SYNC file
/*	v_encrypt_file_name := v_file_name || i_encrypt_file_suffix;
	encrypt_data_file(v_file_path,
				   v_file_name,
				   NVL(i_encrypt_path,v_file_path),
				   v_encrypt_file_name,
				   'AES256-cbc',
				   v_encrypt_password);*/

-- SFTP Encrypted DAT file & SYNC file
	sftp_outbound
		(i_sftp_username	=>	i_sftp_user
		,i_sftp_servername	=>	i_sftp_host
		,i_sftp_remote_path	=>	i_sftp_remote_path
		,i_filepath			=>	v_file_path
		,i_filename			=>	REPLACE(v_file_name,'.SYNC','.*')
		,o_status			=>	v_status);
	IF v_status = G_NO_ERROR THEN
		move_data_file(v_file_path || '/' || REPLACE(v_file_name,'.SYNC','.*'), i_file_path_history /*|| '/' || v_file_name*/);
		ERR_CODE	:=	'0';
	ELSE
		move_data_file(v_file_path || '/' || REPLACE(v_file_name,'.SYNC','.*'), i_file_path_error /*|| '/' || v_file_name*/);
		write_log('Warning :');
		write_log('---------------------------------------');
		write_log(v_status);
		write_log(' ');
		write_log('---------------------------------------');
		ERR_MSG		:=	v_status;
		ERR_CODE	:=	'1';

		-- Send email
		send_email  (i_to_email		=>  i_to_email
					,i_cc_email		=>  NULL
					,i_bcc_email	=>  NULL
					,i_subject		=>	'[ERP] OCC Allocate Outbound Interface : Cannot sftp file (Request ID=' || g_request_id || ')'
					,io_status		=>  v_status
					);
		IF v_status <> G_NO_ERROR THEN
			write_log('Error while sending email = ' || v_status);
		END IF;

		COMMIT;
--		ROLLBACK;
		RETURN;
	END IF;

	<<skip_sync_sftp>>
	err_code := 0;
EXCEPTION
	WHEN utl_file.access_denied THEN
		err_msg := 'ACCESS DENIED: Access to the file has been denied by the operating system';
		write_log(err_msg);
		raise_application_error(-20001, err_msg);
	WHEN utl_file.file_open THEN
		err_msg := 'FILE OPEN: File is already open';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.internal_error THEN
		err_msg := 'INTERNAL ERROR: Unhandled internal error in the UTL_FILE package';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.invalid_filehandle THEN
		err_msg := 'INVALID FILE HANDLE: File handle does not exist';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.invalid_filename THEN
		err_msg := 'INVALID FILENAME: A file with the specified name does not exist in the path';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.invalid_maxlinesize THEN
		err_msg := 'INVALID MAXLINESIZE: The MAX_LINESIZE value for FOPEN() is invalid; it should be within the range 1 to 32767';
		write_log(err_msg);
		raise_application_error(-20001, err_msg);
	WHEN value_error THEN
		err_msg := 'VAULE ERROR: GET_LINE value is larger than the buffer';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.invalid_mode THEN
		err_msg := 'INVALID MODE: The open_mode parameter in FOPEN is invalid';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.invalid_operation THEN
		err_msg := 'INVALID OPERATION: File could not be opened or operated on as requested';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.invalid_path THEN
		err_msg := 'INVALID PATH: Specified path does not exist or is not visible to Oracle';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.read_error THEN
		err_msg := 'READ ERROR: Unable to read file';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN no_data_found THEN
		IF utl_file.is_open(v_file_handle) THEN
			utl_file.fclose(v_file_handle);
		END IF;
	WHEN OTHERS THEN
		IF utl_file.is_open(v_file_handle) THEN
			utl_file.fclose(v_file_handle);
		END IF;
		raise_application_error(-20011, SQLCODE || ': ' || SQLERRM);
END alloc_dealloc;
------------------------------------------------------------------------------------------
END tac_occ_allocate_itf_pkg;
/
