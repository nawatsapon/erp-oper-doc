CREATE OR REPLACE PACKAGE BODY GLB_TEXT_OUTPUT_UTIL IS
/*
Date		By				Description
----------	--------------	---------------------------------------------
22/08/2017	TM				Created
*/

g_max_seq			NUMBER;
g_file_format		number_tabtyp;

PROCEDURE replace_var
(io_full_text		IN OUT	VARCHAR2
,i_variable_name	IN		VARCHAR2
,i_replace_value	IN		VARCHAR2
,i_delimiter		IN		VARCHAR2	:=	'$$')
IS
	v_old_value		VARCHAR2(1000)	:=	i_delimiter || i_variable_name || i_delimiter;
BEGIN
	io_full_text	:=	REPLACE(io_full_text, v_old_value, i_replace_value);
END replace_var;

PROCEDURE set_format
(it_col_len		IN	number_tabtyp
,o_status		OUT	VARCHAR2
,i_clear_data	IN	VARCHAR2	:=	'Y')
IS
PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
	IF it_col_len.COUNT = 0 THEN
		o_status	:=	C_ERROR || 'NO_COLUMN_DEFINED';
	ELSIF it_col_len.COUNT <> it_col_len.LAST THEN
		o_status	:=	C_ERROR || 'COLUMN_NOT_CORRECT';
	ELSE
		g_file_format	:=	it_col_len;
		o_status		:=	NO_ERROR;
	END IF;
-- Clear data
	IF i_clear_data = 'Y' THEN
		DELETE	tmp_process_csv  pl
		WHERE	pl.process_id = g_process_id;
	END IF;
	COMMIT;
END set_format;

PROCEDURE add_txt
(it_message			IN	varchar_tabtyp
,i_delimiter		IN	VARCHAR2	:=	'"'
,i_delimiter_end	IN	VARCHAR2	:=	NULL
,i_separator		IN	VARCHAR2	:=	','
,i_line_prefix		IN	VARCHAR2	:=	NULL
,i_line_suffix		IN	VARCHAR2	:=	NULL)
IS
PRAGMA AUTONOMOUS_TRANSACTION;
	CURSOR	get_max_seq IS
		SELECT	NVL(MAX(seq),0) max_seq
		FROM	tmp_process_csv  pl
		WHERE	pl.process_id = g_process_id;
	v_seq				NUMBER	:=	NULL;
	v_index				NUMBER	:=	0;
	tmp_text			VARCHAR2(32767)	:=	NULL;
	v_message			VARCHAR2(32767)	:=	NULL;
	v_col_txt			VARCHAR2(4000);
	v_delimiter_end		VARCHAR2(10)	:=	NVL(i_delimiter_end, i_delimiter);
	t_message			varchar_tabtyp	:=	it_message;
BEGIN
	IF g_max_seq IS NULL THEN
		FOR rec IN get_max_seq LOOP
			g_max_seq := rec.max_seq;
		END LOOP;
	END IF;
	g_max_seq	:=	g_max_seq + 1;
	v_seq		:=	g_max_seq;

	IF i_delimiter IS NOT NULL THEN
		FOR i IN 1..t_message.count LOOP
			t_message(i)	:=	REPLACE(t_message(i),i_delimiter,i_delimiter||i_delimiter);
			IF t_message(i) IS NOT NULL THEN
				-- Text column contains pure numbers beginning with 0, must add prefix '
				IF g_file_format(i) IS NOT NULL AND g_file_format(i) <> -1 AND t_message(i) IS NOT NULL AND LTRIM(RTRIM(TRANSLATE(t_message(i),'0123456789/.-',' '))) IS NULL /*AND t_message(i) LIKE '0%'*/ THEN
					t_message(i)	:=	'''' || t_message(i);
				END IF;
				-- Header of number column contains , in content (when separator is provided)
				IF g_file_format(i) IS NULL AND t_message(i) IS NOT NULL AND i_separator IS NOT NULL AND t_message(i) LIKE '%' || i_separator || '%' THEN
					t_message(i)	:=	i_delimiter||t_message(i)||v_delimiter_end;
				END IF;
			END IF;
		END LOOP;
	END IF;

	FOR i IN 1..t_message.count LOOP
		IF NOT g_file_format.EXISTS(i) THEN
			g_file_format(i)	:=	NULL;
		END IF;
		IF NVL(g_file_format(i),0) <= 0 THEN
			v_col_txt	:=	t_message(i);
		ELSE
			v_col_txt	:=	RPAD(NVL(t_message(i),' '),g_file_format(i));
		END IF;
		-- If column length is null, not put delimiter
		IF g_file_format(i) IS NULL THEN
			tmp_text	:=	v_col_txt;
		ELSE
			tmp_text	:=	i_delimiter||v_col_txt||v_delimiter_end;
		END IF;

		v_message	:=	v_message||i_separator||tmp_text;
	END LOOP;

	-- Remove prefix separator if any
	v_message	:=	SUBSTR(v_message, NVL(LENGTH(i_separator),0) + 1);
	v_message	:=	i_line_prefix || v_message || i_line_suffix;

	IF g_process_id IS NOT NULL THEN
		INSERT INTO tmp_process_csv
			(process_id
			,seq
			,message
			,generate_date)
		VALUES
			(g_process_id
			,NVL(v_seq,1)
			,v_message
			,SYSDATE);
--		dbms_output.put_line('v_message ' || v_message);
	END IF;

	COMMIT;
END add_txt;

PROCEDURE write2file
(i_file_loc		IN	VARCHAR2
,i_file_name	IN	VARCHAR2
,i_charset		IN	VARCHAR2	:=	NULL	-- Add by TM 25-Jun-2018
,o_status		OUT	VARCHAR2)
IS
PRAGMA AUTONOMOUS_TRANSACTION;
	CURSOR Q1 IS
		SELECT pl.*
		FROM tmp_process_csv pl
		WHERE pl.process_id = g_process_id
		ORDER BY pl.seq;
	v_file			UTL_FILE.FILE_TYPE;
	v_lock_file_cmd	VARCHAR2(1000);
	v_file_name		VARCHAR2(1000);
	v_file_loc		VARCHAR2(1000);
	v_message		VARCHAR2(4000);
-- Add by TM 25-Jun-2018
	v_target_charset	VARCHAR2(1000);
	v_db_charset		VARCHAR2(1000);
-- End Add by TM 25-Jun-2018
BEGIN
	v_message := NULL;

-- Add by TM 25-Jun-2018
	SELECT	MAX(DECODE(parameter,'NLS_CHARACTERSET',VALUE))
			,UPPER(i_charset)
	INTO	v_db_charset, v_target_charset
	FROM	v$nls_parameters
	WHERE	parameter IN ('NLS_LANGUAGE','NLS_TERRITORY','NLS_CHARACTERSET');
-- End Add by TM 25-Jun-2018

	v_file_loc	:=	NVL(i_file_loc, v_file_loc);
	v_file_name	:=	NVL(i_file_name, v_file_name);

	v_file	:=	UTL_FILE.fopen	(location	=>	v_file_loc
								,filename	=>	v_file_name
								,open_mode	=>	'w'
								,max_linesize=> 32767);

	IF i_charset = 'UTF8' THEN
		utl_file.put_raw(v_file, hextoraw('EFBBBF'));	-- Header for UTF8 file
	END IF;
	
	FOR indx IN Q1 LOOP
		v_message := indx.message;
--		UTL_FILE.put(file	=>	v_file, buffer	=>	v_message || CHR(13) || CHR(10));
-- Edit by TM 25-Jun-2018
--		UTL_FILE.put_line(file	=>	v_file, buffer	=>	v_message || CHR(13));
		IF i_charset IS NULL THEN
			UTL_FILE.put_line(file	=>	v_file, buffer	=>	v_message || CHR(13));
		ELSE
			UTL_FILE.put_line(v_file, CONVERT(v_message, v_target_charset, v_db_charset));
		END IF;
-- End Edit by TM 25-Jun-2018
--		UTL_FILE.putf(v_file, '%s', v_message || CHR(13) || CHR(10));
--		UTL_FILE.new_line(v_file);
	END LOOP;
	UTL_FILE.fclose(file => v_file);

	o_status	:=	NO_ERROR;

	COMMIT;

--	java_shell('/bin/chmod 774 ' || v_file_loc || '/' || v_file_name);
EXCEPTION
	WHEN utl_file.invalid_path THEN
		ROLLBACK;
		o_status	:=	C_ERROR || 'Can not write file ,Invalid file path ,Output File Path : '||v_file_loc||v_file_name;
	WHEN utl_file.invalid_mode THEN
		ROLLBACK;
		o_status	:=	C_ERROR || 'Can not write file ,Invalid file mode ,Output File Path : '||v_file_loc||v_file_name;
	WHEN utl_file.invalid_filehandle THEN
		ROLLBACK;
		o_status	:=	C_ERROR || 'Can not write file ,Invalid file handle ,Output File Path : '||v_file_loc||v_file_name;
	WHEN utl_file.invalid_operation THEN
		ROLLBACK;
		o_status	:=	C_ERROR || 'Can not write file ,Invaild operation ,Output File Path : '||v_file_loc||v_file_name;
END write2file;

PROCEDURE writelog2file
(i_file_loc		IN	VARCHAR2
,i_file_name	IN	VARCHAR2
,o_status		OUT	VARCHAR2)
IS
PRAGMA AUTONOMOUS_TRANSACTION;
	CURSOR Q1 IS
		SELECT pl.*
		FROM tmp_process_csv pl
		WHERE pl.process_id = g_process_id
		ORDER BY pl.seq;
	v_file			UTL_FILE.FILE_TYPE;
	v_lock_file_cmd	VARCHAR2(1000);
	v_file_name		VARCHAR2(1000);
	v_file_loc		VARCHAR2(1000);
	v_message		VARCHAR2(4000);
BEGIN
	v_message := NULL;

	v_file_loc	:=	NVL(i_file_loc, v_file_loc);
	v_file_name	:=	NVL(i_file_name, v_file_name);

	v_file	:=	UTL_FILE.fopen	(location	=>	v_file_loc
								,filename	=>	v_file_name
								,open_mode	=>	'w'
								,max_linesize=> 32767);

	FOR indx IN Q1 LOOP
		v_message := indx.message;
		UTL_FILE.put_line(file	=>	v_file, buffer	=>	v_message || CHR(13));
	END LOOP;
	UTL_FILE.fclose(file => v_file);

	o_status	:=	NO_ERROR;

	COMMIT;
EXCEPTION
	WHEN utl_file.invalid_path THEN
		ROLLBACK;
		o_status	:=	C_ERROR || 'Can not write file ,Invalid file path ,Output File Path : '||v_file_loc||v_file_name;
	WHEN utl_file.invalid_mode THEN
		ROLLBACK;
		o_status	:=	C_ERROR || 'Can not write file ,Invalid file mode ,Output File Path : '||v_file_loc||v_file_name;
	WHEN utl_file.invalid_filehandle THEN
		ROLLBACK;
		o_status	:=	C_ERROR || 'Can not write file ,Invalid file handle ,Output File Path : '||v_file_loc||v_file_name;
	WHEN utl_file.invalid_operation THEN
		ROLLBACK;
		o_status	:=	C_ERROR || 'Can not write file ,Invaild operation ,Output File Path : '||v_file_loc||v_file_name;
END writelog2file;
----------------------
PROCEDURE write2fileutf8
(i_file_loc		IN	VARCHAR2
,i_file_name	IN	VARCHAR2
,o_status		OUT	VARCHAR2)
IS
PRAGMA AUTONOMOUS_TRANSACTION;
	CURSOR Q1 IS
		SELECT pl.*
		FROM tmp_process_csv pl
		WHERE pl.process_id = g_process_id
		ORDER BY pl.seq;
	v_file			UTL_FILE.FILE_TYPE;
	v_lock_file_cmd	VARCHAR2(1000);
	v_file_name		VARCHAR2(1000);
	v_file_loc		VARCHAR2(1000);
	v_message		VARCHAR2(4000);
BEGIN
	v_message := NULL;

	v_file_loc	:=	NVL(i_file_loc, v_file_loc);
	v_file_name	:=	NVL(i_file_name, v_file_name);

	v_file	:=	UTL_FILE.fopen_nchar	(location	=>	v_file_loc
								,filename	=>	v_file_name
								,open_mode	=>	'w'
								,max_linesize=> 32767);

	FOR indx IN Q1 LOOP
		v_message := indx.message;
		UTL_FILE.put_line_nchar(file	=>	v_file, buffer	=>	v_message || CHR(13));
	END LOOP;
	UTL_FILE.fclose(file => v_file);

	o_status	:=	NO_ERROR;

	COMMIT;

EXCEPTION
	WHEN utl_file.invalid_path THEN
		ROLLBACK;
		o_status	:=	C_ERROR || 'Can not write file ,Invalid file path ,Output File Path : '||v_file_loc||v_file_name;
	WHEN utl_file.invalid_mode THEN
		ROLLBACK;
		o_status	:=	C_ERROR || 'Can not write file ,Invalid file mode ,Output File Path : '||v_file_loc||v_file_name;
	WHEN utl_file.invalid_filehandle THEN
		ROLLBACK;
		o_status	:=	C_ERROR || 'Can not write file ,Invalid file handle ,Output File Path : '||v_file_loc||v_file_name;
	WHEN utl_file.invalid_operation THEN
		ROLLBACK;
		o_status	:=	C_ERROR || 'Can not write file ,Invaild operation ,Output File Path : '||v_file_loc||v_file_name;
END write2fileutf8;
-----------------------
END GLB_TEXT_OUTPUT_UTIL;
