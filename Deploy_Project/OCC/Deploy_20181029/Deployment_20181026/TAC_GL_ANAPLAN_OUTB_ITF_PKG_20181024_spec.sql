SET SCAN OFF;
CREATE OR REPLACE PACKAGE TAC_GL_ANAPLAN_OUTB_ITF_PKG IS

TYPE varchar2_tabtyp IS TABLE OF VARCHAR2(4000) INDEX BY BINARY_INTEGER;

PROCEDURE main_outbound_gl
(ERR_MSG				OUT	VARCHAR2
,ERR_CODE				OUT	VARCHAR2
,i_file_path			IN	VARCHAR2	:=	'/MP/GERP_TEST/ANAPLAN/INBOX'	--\\lynx22\EAITEST\ANAPLAN\INBOX
,i_filename_pattern		IN	VARCHAR2	:=	'YYYYMM"_ERP_GLSummary_$ENTITY$_ANAPLAN_ALL"'
,i_period_date			IN	VARCHAR2
,i_email_address		IN	VARCHAR2	:=	'nidjarin@teammer.com'
,i_gen_bs				IN	VARCHAR2	-- PA_SRS_YES_NO_LOV
,i_gen_pl				IN	VARCHAR2
,i_gen_cogs				IN	VARCHAR2
/*,i_sftp_host			IN	VARCHAR2
,i_sftp_user			IN	VARCHAR2
,i_sftp_remote_path		IN	VARCHAR2
,i_file_path_error		IN	VARCHAR2
,i_file_path_history	IN	VARCHAR2*/
);

PROCEDURE main_outbound_po
(ERR_MSG				OUT	VARCHAR2
,ERR_CODE				OUT	VARCHAR2
,i_file_path			IN	VARCHAR2	:=	'/MP/GERP_TEST/ANAPLAN/INBOX'	--\\lynx22\EAITEST\ANAPLAN\INBOX
,i_filename_pattern		IN	VARCHAR2	:=	'YYYYMM"_ERP_POSummary_ANAPLAN_ALL"'
,i_period_date			IN	VARCHAR2
,i_email_address		IN	VARCHAR2	:=	'nidjarin@teammer.com'
/*,i_sftp_host			IN	VARCHAR2
,i_sftp_user			IN	VARCHAR2
,i_sftp_remote_path		IN	VARCHAR2
,i_file_path_error		IN	VARCHAR2
,i_file_path_history	IN	VARCHAR2*/
);

PROCEDURE main_report_opex
(ERR_MSG				OUT	VARCHAR2
,ERR_CODE				OUT	VARCHAR2
,i_company_code_from	IN	VARCHAR2
,i_company_code_to		IN	VARCHAR2
,i_account_code_from	IN	VARCHAR2
,i_account_code_to		IN	VARCHAR2
,i_rc_code_from			IN	VARCHAR2
,i_rc_code_to			IN	VARCHAR2
,i_period_from			IN	VARCHAR2	-- TH_TCORE_PERIOD
,i_period_to			IN	VARCHAR2	-- TH_TCORE_PERIOD
,i_report_name			IN	VARCHAR2	:=	'OPEX GL Transactions'
/*,i_sftp_host			IN	VARCHAR2
,i_sftp_user			IN	VARCHAR2
,i_sftp_remote_path		IN	VARCHAR2
,i_file_path_error		IN	VARCHAR2
,i_file_path_history	IN	VARCHAR2*/
);

END TAC_GL_ANAPLAN_OUTB_ITF_PKG;
/
