CREATE OR REPLACE PACKAGE tac_occ_product_itf_pkg IS

-- Original Program : tac_discovery_interface_util.export_product_master
-- Author  : TM
-- Created : 20-Jun-2018
-- Purpose : Interface ERP data to OCC

d2c_format        CONSTANT VARCHAR2(50) := 'dd/mm/yyyy';
g_datechar_format CONSTANT VARCHAR2(50) := 'dd/mm/yyyy hh24:mi:ss';
n2c_format        CONSTANT VARCHAR2(20) := '999999999999.9999';
c2dt_format       CONSTANT VARCHAR2(50) := 'yyyy/mm/dd hh24:mi:ss';

max_linesize BINARY_INTEGER := 32767;

g_grn_inbox   CONSTANT VARCHAR2(50) := 'DISC_GRN_INBOX';
g_grn_error   CONSTANT VARCHAR2(50) := 'DISC_GRN_ERROR';
g_grn_history CONSTANT VARCHAR2(50) := 'DISC_GRN_HISTORY';

g_gl_inbox   CONSTANT VARCHAR2(50) := 'DISC_GL_INBOX';
g_gl_error   CONSTANT VARCHAR2(50) := 'DISC_GL_ERROR';
g_gl_history CONSTANT VARCHAR2(50) := 'DISC_GL_HISTORY';

g_log_line VARCHAR2(250) := '+---------------------------------------------------------------------------+';

PROCEDURE export_product_master
(err_msg				OUT VARCHAR2
,err_code				OUT VARCHAR2
,p_interface_type		VARCHAR2
--,p_increment_date		VARCHAR2
,p_output_dirobj_name	VARCHAR2
,p_file_name_format		VARCHAR2
,p_encrypt_path			VARCHAR2
,p_encrypt_file_suffix	VARCHAR2
,p_encrypt_passwd_key	VARCHAR2
,p_sftp_host			VARCHAR2
,p_sftp_user			VARCHAR2
,p_sftp_remote_path		VARCHAR2
,p_file_path_error		VARCHAR2
,p_file_path_history	VARCHAR2
,p_separator			VARCHAR2	:=	'|'
--,p_to_email				VARCHAR2	:=	'Nawatsapon.Thanaratchaikun@dtac.co.th');
,p_to_email				VARCHAR2	:=	'ERPOperationSupport@dtac.co.th');
/*
PROCEDURE export_customer_master
(
	err_msg            OUT VARCHAR2
   ,err_code           OUT VARCHAR2
   ,p_interface_type   VARCHAR2
   ,p_increment_date   VARCHAR2
   ,p_output_path      VARCHAR2
   ,p_file_name_format VARCHAR2
   ,p_separator        VARCHAR2 DEFAULT '|'
);

PROCEDURE export_purchase_order
(
	err_msg            OUT VARCHAR2
   ,err_code           OUT VARCHAR2
   ,p_interface_type   VARCHAR2
   ,p_increment_date   VARCHAR2
   ,p_output_path      VARCHAR2
   ,p_file_name_format VARCHAR2
   ,p_separator        VARCHAR2 DEFAULT '|'
   ,p_org_id_list      VARCHAR2 DEFAULT '102' -- FOrmat = '102,142,144,...''
);

PROCEDURE interface_grn
(
	err_msg                 OUT VARCHAR2
   ,err_code                OUT VARCHAR2
   ,p_company_map_org_id    VARCHAR2
   ,p_file_path             VARCHAR2
   ,p_file_name_format      VARCHAR2
   ,p_file_name             VARCHAR2 DEFAULT NULL
   ,p_delimiter             VARCHAR2 DEFAULT '|'
   ,p_debug_flag            VARCHAR2 DEFAULT 'N'
   ,p_purge_days            NUMBER DEFAULT 45
   ,p_run_interface_process VARCHAR2 DEFAULT 'Y'
);

PROCEDURE interface_gl
(
	err_msg                  OUT VARCHAR2
   ,err_code                 OUT VARCHAR2
   ,p_file_path              VARCHAR2
   ,p_file_name_format       VARCHAR2
   ,p_file_name              VARCHAR2 DEFAULT NULL
   ,p_delimiter              VARCHAR2 DEFAULT '|'
   ,p_debug_flag             VARCHAR2 DEFAULT 'N'
   ,p_purge_days             NUMBER DEFAULT 45
   ,p_insert_interface_table VARCHAR2 DEFAULT 'Y'
);
*/
END tac_occ_product_itf_pkg;
