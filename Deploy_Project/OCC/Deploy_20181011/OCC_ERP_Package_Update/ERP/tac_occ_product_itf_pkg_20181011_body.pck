CREATE OR REPLACE PACKAGE BODY tac_occ_product_itf_pkg IS

-- Original Program : tac_discovery_interface_util.export_product_master
-- Author  : TM
-- Created : 20-Jun-2018
-- Purpose : Interface ERP data to OCC
-- 24-Jul-2018	Add P00 = PSB
-- Last update 11-OCT-2018

error_output_path EXCEPTION;
error_file_name EXCEPTION;

g_org_id   				NUMBER := FND_PROFILE.VALUE('ORG_ID');
g_log_in   				NUMBER := FND_GLOBAL.CONC_LOGIN_ID;
g_user_id   			NUMBER := FND_GLOBAL.USER_ID;
g_request_id  			NUMBER := FND_GLOBAL.CONC_REQUEST_ID;
g_no_error				VARCHAR2(100)	:=	'No Error';

PROCEDURE conc_wait(p_conc_req_id NUMBER) IS
	phase      VARCHAR2(30);
	status     VARCHAR2(30);
	dev_phase  VARCHAR2(30);
	dev_status VARCHAR2(30);
	message    VARCHAR2(1000);
BEGIN
	COMMIT;
	LOOP
		IF fnd_concurrent.wait_for_request
			(p_conc_req_id	,10			,0
			,phase			,status		,dev_phase
			,dev_status		,message) THEN
			NULL;
		END IF;
		EXIT WHEN phase <> 'Running';
		dbms_lock.sleep(2);
	END LOOP;
END conc_wait;

PROCEDURE conc_wait
(
	p_conc_req_id NUMBER
   ,p_phase       OUT NOCOPY VARCHAR2
   ,p_status      OUT NOCOPY VARCHAR2
   ,p_message     OUT NOCOPY VARCHAR2
   ,p_max_wait    NUMBER DEFAULT 60
) IS
	v_phase         VARCHAR2(30);
	v_status        VARCHAR2(30);
	v_dev_phase     VARCHAR2(30);
	v_dev_status    VARCHAR2(30);
	v_message       VARCHAR2(1000);
	v_wait_complete BOOLEAN;
BEGIN
	COMMIT;
	IF p_conc_req_id > 0 THEN
		LOOP
			v_wait_complete := fnd_concurrent.wait_for_request(request_id => p_conc_req_id,
															   INTERVAL   => 10,
															   max_wait   => p_max_wait,
															   -- out arguments
															   phase      => v_phase,
															   status     => v_status,
															   dev_phase  => v_dev_phase,
															   dev_status => v_dev_status,
															   message    => v_message);
			EXIT WHEN v_phase <> 'Running';
			dbms_lock.sleep(2);
		END LOOP;
	END IF;
	p_phase   := v_phase;
	p_status  := v_status;
	p_message := v_message;
END conc_wait;

PROCEDURE write_log(p_msg VARCHAR2) IS
BEGIN
	fnd_file.put_line(fnd_file.log, p_msg);
EXCEPTION
	WHEN OTHERS THEN
		NULL;
END write_log;

PROCEDURE write_output(p_msg VARCHAR2) IS
BEGIN
	fnd_file.put_line(fnd_file.output, p_msg);
EXCEPTION
	WHEN OTHERS THEN
		NULL;
END write_output;

PROCEDURE write_line
(
	p_file_handle IN OUT utl_file.file_type
   ,p_line_buff   IN VARCHAR2
) IS
	user_error VARCHAR2(255); -- to store translated file_error
BEGIN
	
	utl_file.put_line(p_file_handle, p_line_buff);
	utl_file.fflush(p_file_handle);
EXCEPTION
	WHEN utl_file.invalid_filehandle THEN
		user_error := 'Invalid filehandle';
		write_log(user_error);
	WHEN utl_file.invalid_operation THEN
		user_error := 'Invalid operation';
		write_log(user_error);
	WHEN utl_file.write_error THEN
		user_error := 'Write error';
		write_log(user_error);
	WHEN OTHERS THEN
		user_error := 'Error others.';
		write_log(user_error);
END write_line;

PROCEDURE write_line_nchar
(
	p_file_handle IN OUT utl_file.file_type
   ,p_line_buff   IN NVARCHAR2
) IS
	user_error VARCHAR2(255); -- to store translated file_error
BEGIN
	
	utl_file.put_line_nchar(p_file_handle, p_line_buff);
	utl_file.fflush(p_file_handle);
EXCEPTION
	WHEN utl_file.invalid_filehandle THEN
		user_error := 'Invalid filehandle';
		write_log(user_error);
	WHEN utl_file.invalid_operation THEN
		user_error := 'Invalid operation';
		write_log(user_error);
	WHEN utl_file.write_error THEN
		user_error := 'Write error';
		write_log(user_error);
	WHEN OTHERS THEN
		user_error := 'Error others.';
		write_log(user_error);
END write_line_nchar;

FUNCTION clear_spchar
(i_input		IN	VARCHAR2
,i_separator	IN	VARCHAR2)
RETURN VARCHAR2
IS
BEGIN
	RETURN REPLACE(REPLACE(REPLACE(LTRIM(i_input,''''),CHR(10)),CHR(13)),i_separator);
END clear_spchar;

FUNCTION get_directory_path
(p_directory_name	IN	VARCHAR2)
RETURN VARCHAR2 IS
	v_path VARCHAR2(250);
BEGIN
	SELECT	directory_path
	INTO	v_path
	FROM	all_directories
	WHERE	directory_name = p_directory_name;
	
	RETURN v_path;
EXCEPTION
	WHEN OTHERS THEN
		RETURN NULL;
END get_directory_path;

FUNCTION char_to_date
(
	p_date_char   VARCHAR2
   ,p_format_mask VARCHAR2 DEFAULT NULL
) RETURN DATE IS
	v_date DATE;
BEGIN
	IF (p_date_char IS NULL) THEN
		RETURN NULL;
	END IF;
	
	IF (p_format_mask IS NOT NULL) THEN
		v_date := to_date(p_date_char, p_format_mask);
		RETURN v_date;
	END IF;
	
	-- Format Mask = dd/mm/yyyy hh24:mi:ss
	BEGIN
		v_date := to_date(p_date_char, 'dd/mm/yyyy hh24:mi:ss');
		RETURN v_date;
	EXCEPTION
		WHEN OTHERS THEN
			NULL;
	END;
	
	-- Format Mask = dd/mm/yyyy
	BEGIN
		v_date := to_date(p_date_char, 'dd/mm/yyyy');
		RETURN v_date;
	EXCEPTION
		WHEN OTHERS THEN
			NULL;
	END;
	
	-- Format Mask = dd/m/yyyy
	BEGIN
		v_date := to_date(p_date_char, 'dd/m/yyyy');
		RETURN v_date;
	EXCEPTION
		WHEN OTHERS THEN
			NULL;
	END;
	
	-- Format Mask = d/mm/yyyy
	BEGIN
		v_date := to_date(p_date_char, 'd/mm/yyyy');
		RETURN v_date;
	EXCEPTION
		WHEN OTHERS THEN
			NULL;
	END;
	
	-- Format Mask = d/m/yyyy
	BEGIN
		v_date := to_date(p_date_char, 'd/m/yyyy');
		RETURN v_date;
	EXCEPTION
		WHEN OTHERS THEN
			RETURN p_date_char;
	END;
END char_to_date;

PROCEDURE encrypt_data_file
(
	p_input_filepath	VARCHAR2
   ,p_input_filename	VARCHAR2
   ,p_output_filepath	VARCHAR2
   ,p_output_filename	VARCHAR2
   ,p_enc_algorithm		VARCHAR2
   ,p_enc_password		VARCHAR2
) IS
	v_encrypt_req_id	NUMBER;
	v_phase           VARCHAR2(100);
	v_status          VARCHAR2(100);
	v_message         VARCHAR2(1000);
BEGIN
	write_log(' ');
	write_log('Encrypting File from path ' || p_input_filepath || ' name ' || p_input_filename || ' to path ' ||
					  p_output_filepath || ' name ' || p_output_filename);
	
	v_encrypt_req_id := fnd_request.submit_request(application => 'FND',
													program     => 'TAC_ENCRYPT_FILE',
													argument1   => p_input_filepath,
													argument2   => p_input_filename,
													argument3   => p_output_filepath,
													argument4   => p_output_filename,
													argument5   => p_enc_algorithm,
													argument6   => p_enc_password);
	COMMIT;
	write_log('Concurrent Encrypt File is ' || v_encrypt_req_id);
	
	IF (v_encrypt_req_id > 0) THEN
		conc_wait(v_encrypt_req_id, v_phase, v_status, v_message, 60);
		IF (v_status = 'E') THEN
			write_log('Encrypting file from path ' || p_input_filepath || ' name ' || p_input_filename || ' to path ' ||
					  p_output_filepath || ' name ' || p_output_filename || ' failed.');
		ELSE
			write_log('Encrypting file from path ' || p_input_filepath || ' name ' || p_input_filename || ' to path ' ||
					  p_output_filepath || ' name ' || p_output_filename || ' successful.');
		END IF;
	ELSE
		write_log('Encrypting file from path ' || p_input_filepath || ' name ' || p_input_filename || ' to path ' ||
				  p_output_filepath || ' name ' || p_output_filename ||
				  ' failed, No concurrent program [TAC_ENCRYPT_FILE].');
	END IF;
	write_log(' ');
EXCEPTION
	WHEN OTHERS THEN
		write_log('Encrypting file from path ' || p_input_filepath || ' name ' || p_input_filename || ' to path ' ||
				  p_output_filepath || ' name ' || p_output_filename || ' failed with error ' || SQLERRM);
END encrypt_data_file;

----------------------------------------------------------------------------
PROCEDURE send_email
(i_smtp_server		IN	VARCHAR2	:=  'mail-gw.tac.co.th'
,i_smtp_port		IN	VARCHAR2	:=  25
,i_from_email		IN	VARCHAR2	:=  'ERPOperationSupport@dtac.co.th'
,i_to_email			IN	VARCHAR2	-- Separate by ,
,i_cc_email			IN	VARCHAR2	:=  NULL
,i_bcc_email		IN	VARCHAR2	:=  NULL
,i_subject			IN	VARCHAR2
,i_reply_email		IN	VARCHAR2	:=  'occ_interface@dtac.co.th'
,i_priority			IN	NUMBER		:=  3   -- Normal priority
,io_status			IN OUT  VARCHAR2
)
IS
	C_ERROR			 CONSTANT VARCHAR2(40)   :=  '$$ERROR$$';
	NEW_LINE_IN_MSG	 CONSTANT VARCHAR2(10)   :=  '<BR>';
	v_blob			  BLOB;
	v_msg_template	  VARCHAR2(32000) :=
		'Dear All,' || NEW_LINE_IN_MSG ||
		NEW_LINE_IN_MSG ||
		'Please be informed that Product Interface to OCC executed with ' || io_status || '.' || NEW_LINE_IN_MSG ||
		NEW_LINE_IN_MSG ||
		'Thank you & Regards,' || NEW_LINE_IN_MSG ||
		'ERP auto job' || NEW_LINE_IN_MSG;
	v_msg				v_msg_template%TYPE;

BEGIN
	io_status   :=  G_NO_ERROR;

	v_msg	:=  v_msg_template;

-- Start Temporary for checking email
	write_log('Sending email info:');
	write_log('SMTP Server: ' || i_smtp_server);
	write_log('SMTP Port: ' || i_smtp_port);
	write_log('From email: ' || i_from_email);
	write_log('To email: ' || i_to_email);
	write_log('CC email: ' || i_cc_email);
	write_log('BCC email: ' || i_bcc_email);
	write_log('Email Subject: ' || i_subject);
	write_log('Email Message:');
	write_log(v_msg);
-- End Temporary for checking email

	mail_tools.sendmail
		(smtp_server		=>  i_smtp_server
		,smtp_server_port	=>  i_smtp_port
		,from_name			=>  i_from_email
		,to_name			=>  i_to_email  -- list of TO email addresses separated by commas (,)
		,cc_name			=>  i_cc_email  -- list of CC email addresses separated by commas (,)
		,bcc_name			=>  i_bcc_email -- list of BCC email addresses separated by commas (,)
		,subject			=>  i_subject
		,message			=>  v_msg
		,priority			=>  i_priority  -- 1-5 1 being the highest priority and 3 normal priority
		);
EXCEPTION
	WHEN OTHERS THEN
		io_status	:=	'Error while sending email : ' || SQLERRM;
END send_email;
----------------------------------------------------------------------------
PROCEDURE sftp_outbound
(i_sftp_username	IN	VARCHAR2
,i_sftp_servername	IN	VARCHAR2
,i_sftp_remote_path	IN	VARCHAR2
,i_filepath			IN	VARCHAR2
,i_filename			IN	VARCHAR2
,o_status			OUT	VARCHAR2)
IS
PRAGMA AUTONOMOUS_TRANSACTION;
	v_sub_req_id	NUMBER;
	v_phase			VARCHAR2(100);
	v_status		VARCHAR2(100);
	v_message		VARCHAR2(1000);
BEGIN
	o_status	:=	G_NO_ERROR;
	write_log(' ');
	write_log('SFTP from File Path ' || i_filepath || ' Filename ' || i_filename);
	
	v_sub_req_id	:=	fnd_request.submit_request
							(application	=>	'SQLAP'
							,program		=>	'TAC_SFTP_OUTBOUND'
							,argument1		=>	i_sftp_username
							,argument2		=>	i_sftp_servername
							,argument3		=>	i_sftp_remote_path
							,argument4		=>	i_filepath
							,argument5		=>	i_filename);
	COMMIT;
	write_log('Concurrent to SFTP File is ' || v_sub_req_id);
	
	IF (v_sub_req_id > 0) THEN
		LOOP
			conc_wait(v_sub_req_id, v_phase, v_status, v_message, 60);
			EXIT WHEN v_phase <> 'Running';
			dbms_lock.sleep(2);
		END LOOP;
		write_log('Concurrent to SFTP File phase = ' || v_phase || ' status = ' || v_status || ' message = ' || v_message);
		IF (v_status LIKE 'E%') THEN	-- Error
			o_status	:=	'Error : Could not sftp file';
			write_log('SFTP from File Path ' || i_filepath || ' Filename ' || i_filename || ' failed.');
		ELSE
			write_log('SFTP from File Path ' || i_filepath || ' Filename ' || i_filename || ' successful.');
		END IF;
	ELSE
		o_status	:=	'Error : Could not sftp file, no program to call';
		write_log('SFTP from File Path ' || i_filepath || ' Filename ' || i_filename ||
					' failed, No concurrent program [TAC_SFTP_OUTBOUND].');
	END IF;
	write_log(' ');
EXCEPTION
	WHEN OTHERS THEN
		o_status	:=	'Error : Could not sftp file';
		write_log('SFTP from File Path ' || i_filepath || ' Filename ' || i_filename ||
					' failed with error ' || SQLERRM);
END sftp_outbound;
----------------------------------------------------------------------------
PROCEDURE move_data_file
(i_source_file	IN	VARCHAR2
,i_dest_path	IN	VARCHAR2)
IS
PRAGMA AUTONOMOUS_TRANSACTION;
	v_movefile_req_id	NUMBER;
	v_phase				VARCHAR2(100);
	v_status			VARCHAR2(100);
	v_message			VARCHAR2(1000);
BEGIN
	write_log(' ');
	write_log('Moving File ' || i_source_file || ' to ' || i_dest_path);
	
	v_movefile_req_id	:=	fnd_request.submit_request
								(application	=>	'FND'
								,program		=>	'TACFND_MVFILE'
								,argument1		=>	i_source_file
								,argument2		=>	i_dest_path);
	COMMIT;
	write_log('Concurrent Move File is ' || v_movefile_req_id);
	
	IF (v_movefile_req_id > 0) THEN
		conc_wait(v_movefile_req_id, v_phase, v_status, v_message, 60);
		IF (v_status = 'E') THEN
			write_log('Move file from ' || i_source_file || ' to ' || i_dest_path || ' failed.');
		ELSE
			write_log('Move file from ' || i_source_file || ' to ' || i_dest_path || ' successful.');
		END IF;
	ELSE
		write_log('Move file from ' || i_source_file || ' to ' || i_dest_path ||
					' failed, No concurrent program [TACFND_MVFILE].');
	END IF;
	write_log(' ');
EXCEPTION
	WHEN OTHERS THEN
		write_log('Move file from ' || i_source_file || ' to ' ||
					i_dest_path || ' failed with error ' || SQLERRM);
END move_data_file;
----------------------------------------------------------------------------
PROCEDURE export_product_master
(err_msg				OUT VARCHAR2
,err_code				OUT VARCHAR2
,p_interface_type		VARCHAR2
--,p_increment_date		VARCHAR2
,p_output_dirobj_name	VARCHAR2
,p_file_name_format		VARCHAR2
,p_encrypt_path			VARCHAR2
,p_encrypt_file_suffix	VARCHAR2
,p_encrypt_passwd_key	VARCHAR2
,p_sftp_host			VARCHAR2
,p_sftp_user			VARCHAR2
,p_sftp_remote_path		VARCHAR2
,p_file_path_error		VARCHAR2
,p_file_path_history	VARCHAR2
,p_separator			VARCHAR2	:=	'|'
--,p_to_email				VARCHAR2	:=	'Nawatsapon.Thanaratchaikun@dtac.co.th')
,p_to_email				VARCHAR2	:=	'ERPOperationSupport@dtac.co.th')
IS
	v_file_handle		utl_file.file_type;
	v_line_data			VARCHAR2(32767);
	v_file_path			VARCHAR2(1000);
	v_file_name			VARCHAR2(100);
	v_encrypt_file_name	VARCHAR2(100);
	v_date				DATE;
	v_file_date			DATE := SYSDATE;
	v_bom_raw			RAW(3);

	NEW_LINE			CONSTANT VARCHAR2(10)	:=	CHR(13) || CHR(10);
	v_target_charset	VARCHAR2(1000);
	v_db_charset		VARCHAR2(1000);
	v_encrypt_password	VARCHAR2(1000);
	v_status			VARCHAR2(100);
	v_records			NUMBER	:=	0;
	CURSOR c_data2sent IS
		SELECT	*
		FROM	tac_occ_erp_product_tmp t
		WHERE	t.request_id	=	G_REQUEST_ID
		AND		(p_interface_type	=	'ALL' OR 
				EXISTS (SELECT	'a'
						FROM	tac_occ_erp_product_sent s
						WHERE	s.obsolete_record	=	'N'
						AND		ou_code		=	t.ou_code
						AND		product_sku	=	t.product_sku
						AND		s.request_id_first_sent	=	G_REQUEST_ID)
				)
		ORDER BY ou_code, product_sku;
BEGIN
	SELECT	MAX(DECODE(parameter,'NLS_CHARACTERSET',VALUE))
			,'UTF8'
	INTO	v_db_charset, v_target_charset
	FROM	v$nls_parameters
	WHERE	parameter IN ('NLS_LANGUAGE','NLS_TERRITORY','NLS_CHARACTERSET');
	
	v_encrypt_password	:=	NULL;
	FOR rec IN (SELECT p.* FROM tac_encryption_password p WHERE p.key_name = p_encrypt_passwd_key)
	LOOP
		v_encrypt_password	:=	rec.password_value;
	END LOOP;

	write_log(' ');
	write_log('+---------------------------------------------------------------------------+');
	write_log('Start Concurrent export Product Master to OCC ');
	write_log('Start Date: ' ||
			  to_char(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
	write_log(' ');
	write_log('Agruments');
	write_log('-----------');
	write_log('p_interface_type =  ' || p_interface_type);
--	write_log('p_increment_date =  ' || p_increment_date);
	write_log('p_output_dirobj_name =  ' || p_output_dirobj_name);
	write_log('p_file_name_format =  ' || p_file_name_format);
	write_log('p_encrypt_path =  ' || p_encrypt_path);
	write_log('p_encrypt_file_suffix =  ' || p_encrypt_file_suffix);
	write_log('p_encrypt_passwd_key =  ' || p_encrypt_passwd_key);
	write_log('p_sftp_host =  ' || p_sftp_host);
	write_log('p_sftp_user =  ' || p_sftp_user);
	write_log('p_sftp_remote_path =  ' || p_sftp_remote_path);
	write_log('p_file_path_error =  ' || p_file_path_error);
	write_log('p_file_path_history =  ' || p_file_path_history);
	write_log('p_separator =  ' || p_separator);
	write_log('+----------------------------------------------------------------------------+');
	
	-- validate parameter
	IF p_output_dirobj_name IS NULL THEN
		RAISE utl_file.invalid_path;
	END IF;
	IF p_file_name_format IS NULL THEN
		RAISE utl_file.invalid_filename;
	END IF;
	IF v_encrypt_password IS NULL THEN
		err_msg := 'ERROR: No encryption password key "' || p_encrypt_passwd_key || '" defined in TAC_ENCRYPTION_PASSWORD table, please correct setup';
		write_log(err_msg);
		raise_application_error(-20021, err_msg);
	END IF;
/*	IF p_increment_date IS NULL THEN
		v_date      := trunc(SYSDATE) - 1;
		v_file_date := SYSDATE;
	ELSE
		v_date      := trunc(to_date(p_increment_date,
									 'yyyy/mm/dd hh24:mi:ss'));
		v_file_date := v_date;
	END IF;*/
	
	v_file_name := p_file_name_format || '.DAT';
	v_file_name := REPLACE(v_file_name,
						   '$DATETIME$',
						   to_char(v_file_date, 'YYYYMMDD_HH24MISS'));
	v_file_name := REPLACE(v_file_name,
						   '$DATE$',
						   to_char(v_file_date, 'YYYYMMDD'));

	write_log('+---------------------------------------------------------------------------+');
	write_log('Prepare data to interface.');
	write_log(' ');
	err_code     := '2';
	-- query increment item master
	INSERT INTO tac_occ_erp_product_tmp
		(request_id
		,ou_code
		,product_sku
		,product_name
		,flag_serial
		,product_group
		,uom
		,item_status
		,process_date)
	SELECT	g_request_id
			,decode(mpt.organization_code,'Z00','DTAC','P00','PSB','DTN') company_code
			,msi.segment1 product_sku
			,msi.description product_name
			,CASE msi.serial_number_control_code WHEN 1 THEN 'N' ELSE 'Y' END flag_serial
			,msi.item_type	product_group
			,'EA'	uom
			,msi.inventory_item_status_code item_status
			,SYSDATE
	FROM	mtl_system_items_b		msi
			,mtl_parameters			mpt
			,fnd_lookup_values_vl	fv
			,fnd_lookup_types_vl	fl
	WHERE	1 = 1
	AND		mpt.organization_code IN ('Z00', 'P00', 'N00')
	--AND		msi.item_type IN ('DEVICE', 'MOBILE')
	AND		msi.inventory_item_status_code IN ('Active', 'Dead Stock')
	AND		msi.item_type = fv.lookup_code
	AND		fv.lookup_type = fl.lookup_type
	AND		fl.lookup_type LIKE 'ITEM_TYPE'
	AND		fl.application_id = 401
	AND		fv.attribute8	=	'Y'	-- Interface Item to OCC
--	AND		nvl(fv.attribute15, 'N') = 'Y'	-- Interface to DMS
	AND		msi.organization_id = mpt.organization_id
	AND		mpt.organization_id = mpt.master_organization_id
--	AND		(p_interface_type = 'ALL' OR trunc(msi.creation_date) = v_date OR trunc(msi.last_update_date) = v_date)
	;

-- Mark Obsolete for record not send this time
	UPDATE	tac_occ_erp_product_sent s
	SET		s.obsolete_record		=	'Y'
			,s.request_id_4obsolete	=	G_REQUEST_ID
	WHERE	obsolete_record		=	'N'
	AND		(ou_code, product_sku) IN
			(SELECT	DISTINCT ou_code, product_sku
			FROM	(SELECT	ou_code
							,product_sku
							,NVL(product_name,' ')
							,NVL(flag_serial,' ')
							,NVL(product_group,' ')
							,NVL(uom,' ')
							,NVL(item_status,' ')
					FROM	tac_occ_erp_product_sent s
					WHERE	s.obsolete_record	=	'N'
					MINUS
					SELECT	ou_code
							,product_sku
							,NVL(product_name,' ')
							,NVL(flag_serial,' ')
							,NVL(product_group,' ')
							,NVL(uom,' ')
							,NVL(item_status,' ')
					FROM	tac_occ_erp_product_tmp
					WHERE	request_id	=	G_REQUEST_ID)
			)
	;

	INSERT INTO tac_occ_erp_product_sent
		(ou_code
		,product_sku
		,product_name
		,flag_serial
		,product_group
		,uom
		,item_status
		,request_id_first_sent
		,obsolete_record)
	SELECT	ou_code
			,product_sku
			,product_name
			,flag_serial
			,product_group
			,uom
			,item_status
			,G_REQUEST_ID
			,'N'	-- Not obsolete
	FROM	tac_occ_erp_product_tmp
	WHERE	(ou_code, product_sku) IN
			(SELECT	ou_code, product_sku
			FROM	tac_occ_erp_product_tmp
			WHERE	request_id	=	G_REQUEST_ID
			MINUS
			SELECT	ou_code, product_sku
			FROM	tac_occ_erp_product_sent s
			WHERE	s.obsolete_record	=	'N')
	;

	-- open file for write item master
	write_log('+++++++++++++++++++++++++++++++++++++++++++++++');
	write_log('Create file ' || get_directory_path(p_output_dirobj_name) || '/' ||
			  v_file_name || ' for write data');
	
	v_file_handle := utl_file.fopen(p_output_dirobj_name,
									v_file_name,
									'w');
	BEGIN
		utl_file.fclose(v_file_handle);
	EXCEPTION
		WHEN OTHERS THEN
			NULL;
	END;
	v_file_handle := utl_file.fopen(p_output_dirobj_name,
									v_file_name,
									'w',
									max_linesize);
	v_bom_raw     := hextoraw('EFBBBF');
	utl_file.put_raw(v_file_handle, v_bom_raw);
	
	FOR r_data IN c_data2sent
	LOOP
		v_line_data := clear_spchar(r_data.ou_code, p_separator) ||
					   p_separator ||
					   clear_spchar(r_data.product_sku, p_separator) ||
					   p_separator ||
					   clear_spchar(r_data.product_name, p_separator) ||
					   p_separator ||
					   clear_spchar(r_data.flag_serial, p_separator) ||
					   p_separator ||
					   clear_spchar(r_data.product_group, p_separator) ||
					   p_separator ||
					   clear_spchar(r_data.uom, p_separator) ||
					   p_separator ||
					   clear_spchar(r_data.item_status, p_separator);
		
		write_line(v_file_handle, CONVERT(v_line_data, v_target_charset, v_db_charset));
--		write_log(v_line_data);
		v_records	:=	v_records + 1;
	END LOOP;
	write_log('+---------------------------------------------------------------------------+');
	
	IF utl_file.is_open(v_file_handle) THEN
		utl_file.fclose(v_file_handle);
	END IF;

-- Encrypt file
	v_file_path	:=	get_directory_path(p_output_dirobj_name);
	IF v_file_path LIKE '%/' THEN
		v_file_path	:=	RTRIM(v_file_path,'/');
	END IF;
	v_encrypt_file_name := v_file_name || p_encrypt_file_suffix;

-- Move to after SFTP 11-OCT-2018
/*	encrypt_data_file(v_file_path,
				   v_file_name,
				   NVL(p_encrypt_path,v_file_path),
				   v_encrypt_file_name,
				   'AES256-cbc',
				   v_encrypt_password);*/
-- End Move to after SFTP 11-OCT-2018

-- Write SYNC file
	v_file_name	:=	SUBSTR(v_file_name, 1, LENGTH(v_file_name)-3) || 'SYNC';
	v_file_handle := utl_file.fopen(p_output_dirobj_name,
									v_file_name,
									'w');
	v_bom_raw     := hextoraw('EFBBBF');
	utl_file.put_raw(v_file_handle, v_bom_raw);
	write_line(v_file_handle, TO_CHAR(v_records));
	IF utl_file.is_open(v_file_handle) THEN
		utl_file.fclose(v_file_handle);
	END IF;
	
--	GOTO skip_sync_sftp;	-- skip_sync_sftp

-- Encrypt SYNC file
/*	v_encrypt_file_name := v_file_name || p_encrypt_file_suffix;
	encrypt_data_file(v_file_path,
				   v_file_name,
				   NVL(p_encrypt_path,v_file_path),
				   v_encrypt_file_name,
				   'AES256-cbc',
				   v_encrypt_password);*/

-- SFTP Encrypted DAT file & SYNC file
	sftp_outbound
		(i_sftp_username	=>	p_sftp_user
		,i_sftp_servername	=>	p_sftp_host
		,i_sftp_remote_path	=>	p_sftp_remote_path
		,i_filepath			=>	v_file_path
		,i_filename			=>	REPLACE(v_file_name,'.SYNC','.*')
		,o_status			=>	v_status);
	IF v_status = G_NO_ERROR THEN
		move_data_file(v_file_path || '/' || REPLACE(v_file_name,'.SYNC','.*'), p_file_path_history /*|| '/' || v_file_name*/);
-- Move from before SFTP 11-OCT-2018
		v_file_name	:=	SUBSTR(v_encrypt_file_name,1,LENGTH(v_encrypt_file_name)-LENGTH(p_encrypt_file_suffix));
		encrypt_data_file(v_file_path,
					   v_file_name,
					   NVL(p_encrypt_path,v_file_path),
					   v_encrypt_file_name,
					   'AES256-cbc',
					   v_encrypt_password);
-- End Move from before SFTP 11-OCT-2018
		ERR_CODE	:=	'0';
	ELSE
		move_data_file(v_file_path || '/' || REPLACE(v_file_name,'.SYNC','.*'), p_file_path_error /*|| '/' || v_file_name*/);
		write_log('Warning :');
		write_log('---------------------------------------');
		write_log(v_status);
		write_log(' ');
		write_log('---------------------------------------');
		ERR_MSG		:=	v_status;
		ERR_CODE	:=	'1';

		-- Send email
		send_email  (i_to_email		=>  p_to_email
					,i_cc_email		=>  NULL
					,i_bcc_email	=>  NULL
					,i_subject		=>	'[ERP] OCC Product Outbound Interface : ERROR sftp file (Request ID=' || g_request_id || ')'
					,io_status		=>  v_status
					);
		IF v_status <> G_NO_ERROR THEN
			write_log('Error while sending email = ' || v_status);
		END IF;

		COMMIT;
--		ROLLBACK;
		RETURN;
	END IF;

	<<skip_sync_sftp>>
	err_code := 0;
EXCEPTION
	WHEN utl_file.access_denied THEN
		err_msg := 'ACCESS DENIED: Access to the file has been denied by the operating system';
		write_log(err_msg);
		raise_application_error(-20001, err_msg);
	WHEN utl_file.file_open THEN
		err_msg := 'FILE OPEN: File is already open';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.internal_error THEN
		err_msg := 'INTERNAL ERROR: Unhandled internal error in the UTL_FILE package';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.invalid_filehandle THEN
		err_msg := 'INVALID FILE HANDLE: File handle does not exist';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.invalid_filename THEN
		err_msg := 'INVALID FILENAME: A file with the specified name does not exist in the path';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.invalid_maxlinesize THEN
		err_msg := 'INVALID MAXLINESIZE: The MAX_LINESIZE value for FOPEN() is invalid; it should be within the range 1 to 32767';
		write_log(err_msg);
		raise_application_error(-20001, err_msg);
	WHEN value_error THEN
		err_msg := 'VAULE ERROR: GET_LINE value is larger than the buffer';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.invalid_mode THEN
		err_msg := 'INVALID MODE: The open_mode parameter in FOPEN is invalid';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.invalid_operation THEN
		err_msg := 'INVALID OPERATION: File could not be opened or operated on as requested';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.invalid_path THEN
		err_msg := 'INVALID PATH: Specified path does not exist or is not visible to Oracle';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.read_error THEN
		err_msg := 'READ ERROR: Unable to read file';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN no_data_found THEN
		IF utl_file.is_open(v_file_handle) THEN
			utl_file.fclose(v_file_handle);
		END IF;
	WHEN OTHERS THEN
		IF utl_file.is_open(v_file_handle) THEN
			utl_file.fclose(v_file_handle);
		END IF;
		raise_application_error(-20011, SQLCODE || ': ' || SQLERRM);
END export_product_master;
/*
PROCEDURE export_customer_master
(
	err_msg            OUT VARCHAR2
   ,err_code           OUT VARCHAR2
   ,p_interface_type   VARCHAR2
   ,p_increment_date   VARCHAR2
   ,p_output_path      VARCHAR2
   ,p_file_name_format VARCHAR2
   ,p_separator        VARCHAR2 DEFAULT '|'
) IS
	
	v_file_handle  utl_file.file_type;
	v_line_data    NVARCHAR2(32767);
	v_file_name    VARCHAR2(100);
	v_date         DATE;
	v_line_count   NUMBER;
	v_file_date    DATE := SYSDATE;
	v_bom_raw      RAW(3);
	v_put_bom_flag VARCHAR2(10) := 'N';
BEGIN
	write_log(' ');
	write_log('+---------------------------------------------------------------------------+');
	write_log('Start Concurrent export Customer Master to OCC ');
	write_log('Start Date: ' ||
			  to_char(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
	write_log(' ');
	write_log('Agruments');
	write_log('-----------');
	write_log('p_interface_type =  ' || p_interface_type);
	write_log('p_increment_date =  ' || p_increment_date);
	write_log('p_output_path =  ' || p_output_path);
	write_log('p_file_name_format =  ' || p_file_name_format);
	write_log('p_separator =  ' || p_separator);
	write_log('+----------------------------------------------------------------------------+');
	
	-- validate parameter
	IF p_output_path IS NULL THEN
		RAISE utl_file.invalid_path;
	END IF;
	IF p_file_name_format IS NULL THEN
		RAISE utl_file.invalid_filename;
	END IF;
	IF p_increment_date IS NULL THEN
		v_date      := trunc(SYSDATE) - 1;
		v_file_date := SYSDATE;
	ELSE
		v_date      := trunc(to_date(p_increment_date,
									 'yyyy/mm/dd hh24:mi:ss'));
		v_file_date := v_date;
	END IF;
	
	v_file_name := p_file_name_format;
	v_file_name := REPLACE(v_file_name,
						   '$DATETIME$',
						   to_char(v_file_date, 'YYYYMMDD_HH24MISS'));
	v_file_name := REPLACE(v_file_name,
						   '$DATE$',
						   to_char(v_file_date, 'YYYYMMDD'));
	-- open file for write item master
	write_log('+++++++++++++++++++++++++++++++++++++++++++++++');
	write_log('Create file ' || get_directory_path(p_output_path) || '/' ||
			  v_file_name || ' for write data');
	
	v_file_handle := utl_file.fopen_nchar(p_output_path,
										  v_file_name,
										  'w');
	BEGIN
		utl_file.fclose(v_file_handle);
	EXCEPTION
		WHEN OTHERS THEN
			NULL;
	END;
	v_file_handle := utl_file.fopen_nchar(p_output_path,
										  v_file_name,
										  'w',
										  max_linesize);
	v_bom_raw     := hextoraw('EFBBBF');
	-- Move put_raw into loop
	--utl_file.put_raw(v_file_handle, v_bom_raw);
	
	write_log('+---------------------------------------------------------------------------+');
	write_log('Qeury data and write to file.');
	write_log(' ');
	v_line_count := 0;
	err_code     := '2';
	-- query increment item master (creation date)
	FOR r_data IN (SELECT CASE addr.org_id
							  WHEN 102 THEN
							   'DTAC'
							  WHEN 142 THEN
							   'DTN'
							  ELSE
							   'DTAC'
						  END company_code,
						  cust.customer_number customer_no,
						  cust.customer_name customer_name,
						  cust.tax_reference tax_id,
						  bill.location billto_code,
						  decode(bill.location, NULL, NULL, loc.address1) billto_address1,
						  decode(bill.location, NULL, NULL, loc.address2) billto_address2,
						  decode(bill.location, NULL, NULL, loc.address3) billto_address3,
						  decode(bill.location, NULL, NULL, loc.province) billto_province,
						  decode(bill.location,
								 NULL,
								 NULL,
								 loc.postal_code) billto_postal,
						  bill_cr.overall_credit_limit credit_limit,
						  substr(nvl(bill.location, ship.location),
								 1,
								 instr(nvl(bill.location, ship.location),
									   '_') - 1) division,
						  trm.standard_terms_name term,
						  ship.location shipto_code,
						  decode(ship.location, NULL, NULL, loc.address1) shipto_address1,
						  decode(ship.location, NULL, NULL, loc.address2) shipto_address2,
						  decode(ship.location, NULL, NULL, loc.address3) shipto_address3,
						  decode(ship.location, NULL, NULL, loc.province) shipto_province,
						  decode(ship.location,
								 NULL,
								 NULL,
								 loc.postal_code) shipto_postal,
						  addr.attribute6 partner_code,
						  addr.attribute8 parent_partner_code
							  
						  -------------------------------
						 ,
						  cust.creation_date       creation_date_cust,
						  cust.last_update_date    last_update_date_cust,
						  addr.creation_date       creation_date_addr,
						  addr.last_update_date    last_update_date_addr,
						  loc.creation_date        creation_date_loc,
						  loc.last_update_date     last_update_date_loc,
						  bill_cr.creation_date    creation_date_bill_cr,
						  bill_cr.last_update_date last_update_date_bill_cr
				   FROM ar_customers_all_v     cust,
						hz_cust_acct_sites_all addr,
						hz_party_sites         party_site,
						hz_locations           loc
						--  ,hz_loc_assignments loc_assign
					   ,
						hz_cust_site_uses_all  bill,
						hz_cust_site_uses_all  ship,
						hz_cust_profile_amts   bill_cr,
						ar_customer_profiles_v trm
				   WHERE 1 = 1
				   AND cust.customer_id = addr.cust_account_id
				   AND addr.party_site_id = party_site.party_site_id
				   AND party_site.location_id = loc.location_id
						--    and loc.location_id = loc_assign.location_id
				   AND addr.cust_acct_site_id = bill.cust_acct_site_id(+)
				   AND addr.cust_acct_site_id = ship.cust_acct_site_id(+)
							
				   AND bill.site_use_id = bill_cr.site_use_id(+)
				   AND bill.site_use_id = trm.site_use_id(+)
				   AND ((addr.org_id = 102 AND
						 substr(nvl(bill.location, ship.location),
							   1,
							   instr(nvl(bill.location, ship.location),
									 '_') - 1) IN
						 ('H0', 'H3', 'H4', 'H9', 'H10', 'H11', 'H15')) OR
						 (addr.org_id = 142 AND
						 substr(nvl(bill.location, ship.location),
							   1,
							   instr(nvl(bill.location, ship.location),
									 '_') - 1) IN
						 ('N0', 'N3', 'N4', 'N9', 'N10', 'N11', 'N15')))
				   AND 'BILL_TO' = bill.site_use_code(+)
				   AND 'SHIP_TO' = ship.site_use_code(+)
						--and cust.customer_number = '1357'
						--and party_site.party_site_number = 1228
				   AND (trunc(cust.creation_date) =
						 decode(p_interface_type,
							  'ALL',
							  trunc(cust.creation_date),
							  v_date) OR trunc(cust.last_update_date) =
						 decode(p_interface_type,
											  'ALL',
											  trunc(cust.last_update_date),
											  v_date) OR
						 trunc(addr.creation_date) =
						 decode(p_interface_type,
							  'ALL',
							  trunc(addr.creation_date),
							  v_date) OR trunc(addr.last_update_date) =
						 decode(p_interface_type,
											  'ALL',
											  trunc(addr.last_update_date),
											  v_date) OR
						 trunc(loc.creation_date) =
						 decode(p_interface_type,
							  'ALL',
							  trunc(loc.creation_date),
							  v_date) OR trunc(loc.last_update_date) =
						 decode(p_interface_type,
											  'ALL',
											  trunc(loc.last_update_date),
											  v_date) OR
						 trunc(bill_cr.creation_date) =
						 decode(p_interface_type,
							  'ALL',
							  trunc(bill_cr.creation_date),
							  v_date) OR trunc(bill_cr.last_update_date) =
						 decode(p_interface_type,
											  'ALL',
											  trunc(bill_cr.last_update_date),
											  v_date))
				   ORDER BY addr.org_id, cust.customer_number) LOOP
		\*
		v_line_data := replace(r_data.company_code, p_separator, '') || p_separator ||
					   replace(r_data.customer_no, p_separator, '') || p_separator ||
					   replace(r_data.customer_name, p_separator, '') || p_separator ||
						replace(r_data.tax_id, p_separator, '') || p_separator ||
					   replace(r_data.billto_code, p_separator, '') || p_separator ||
					   replace(r_data.billto_address1, p_separator, '') || p_separator ||
					   replace(r_data.billto_address2, p_separator, '') || p_separator ||
					   replace(r_data.billto_address3, p_separator, '') || p_separator ||
					   replace(r_data.billto_province, p_separator, '') || p_separator ||
					   replace(r_data.billto_postal, p_separator, '') || p_separator ||
					   replace(r_data.credit_limit, p_separator, '') || p_separator ||
					   replace(r_data.division, p_separator, '') || p_separator ||
					   replace(r_data.term, p_separator, '') || p_separator ||
					   replace(r_data.shipto_code, p_separator, '') ||p_separator ||
					   replace(r_data.shipto_address1, p_separator, '') || p_separator ||
					   replace(r_data.shipto_address2, p_separator, '') || p_separator ||
					   replace(r_data.shipto_address3, p_separator, '') || p_separator ||
					   replace(r_data.shipto_province, p_separator, '') || p_separator ||
					   replace(r_data.shipto_postal, p_separator, '') || p_separator ||
					   replace(r_data.partner_code, p_separator, '') || p_separator ||
					   replace(r_data.parent_partner_code, p_separator, '');
					   *\
		
		v_line_data := REPLACE(r_data.company_code, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.customer_no, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.customer_name, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.tax_id, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.billto_code, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.billto_address1, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.billto_address2, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.billto_address3, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.billto_province, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.billto_postal, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.credit_limit, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.division, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.term, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.shipto_code, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.shipto_address1, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.shipto_address2, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.shipto_address3, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.shipto_province, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.shipto_postal, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.partner_code, p_separator, '') ||
					   p_separator || REPLACE(r_data.parent_partner_code,
											  p_separator,
											  '');
		
		IF (v_put_bom_flag = 'N') THEN
			v_put_bom_flag := 'Y';
			utl_file.put_raw(v_file_handle, v_bom_raw);
		END IF;
		
		write_line_nchar(v_file_handle, v_line_data);
		write_log(v_line_data);
		v_line_count := v_line_count + 1;
	END LOOP;
	write_log('+---------------------------------------------------------------------------+');
	
	IF utl_file.is_open(v_file_handle) THEN
		utl_file.fclose(v_file_handle);
	END IF;
	err_code := 0;
EXCEPTION
	WHEN utl_file.access_denied THEN
		err_msg := 'ACCESS DENIED: Access to the file has been denied by the operating system';
		write_log(err_msg);
		raise_application_error(-20001, err_msg);
	WHEN utl_file.file_open THEN
		err_msg := 'FILE OPEN: File is already open';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.internal_error THEN
		err_msg := 'INTERNAL ERROR: Unhandled internal error in the UTL_FILE package';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.invalid_filehandle THEN
		err_msg := 'INVALID FILE HANDLE: File handle does not exist';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.invalid_filename THEN
		err_msg := 'INVALID FILENAME: A file with the specified name does not exist in the path';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.invalid_maxlinesize THEN
		err_msg := 'INVALID MAXLINESIZE: The MAX_LINESIZE value for FOPEN() is invalid; it should be within the range 1 to 32767';
		write_log(err_msg);
		raise_application_error(-20001, err_msg);
	WHEN value_error THEN
		err_msg := 'VAULE ERROR: GET_LINE value is larger than the buffer';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.invalid_mode THEN
		err_msg := 'INVALID MODE: The open_mode parameter in FOPEN is invalid';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.invalid_operation THEN
		err_msg := 'INVALID OPERATION: File could not be opened or operated on as requested';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.invalid_path THEN
		err_msg := 'INVALID PATH: Specified path does not exist or is not visible to Oracle';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.read_error THEN
		err_msg := 'READ ERROR: Unable to read file';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN no_data_found THEN
		IF utl_file.is_open(v_file_handle) THEN
			utl_file.fclose(v_file_handle);
		END IF;
	WHEN OTHERS THEN
		IF utl_file.is_open(v_file_handle) THEN
			utl_file.fclose(v_file_handle);
		END IF;
		raise_application_error(-20011, SQLCODE || ': ' || SQLERRM);
END export_customer_master;

PROCEDURE export_purchase_order
(
	err_msg            OUT VARCHAR2
   ,err_code           OUT VARCHAR2
   ,p_interface_type   VARCHAR2
   ,p_increment_date   VARCHAR2
   ,p_output_path      VARCHAR2
   ,p_file_name_format VARCHAR2
   ,p_separator        VARCHAR2 DEFAULT '|'
   ,p_org_id_list      VARCHAR2 DEFAULT '102' -- FOrmat = '102,142,144,...''
) IS
	
	v_file_handle  utl_file.file_type;
	v_line_data    NVARCHAR2(32767);
	v_file_name    VARCHAR2(100);
	v_date         DATE;
	v_line_count   NUMBER;
	v_file_date    DATE := SYSDATE;
	v_bom_raw      RAW(3);
	v_net_qty      NUMBER;
	v_quantity     NUMBER;
	v_canceled     NUMBER;
	v_put_bom_flag VARCHAR2(10) := 'N';
BEGIN
	write_log(' ');
	write_log('+---------------------------------------------------------------------------+');
	write_log('Start Concurrent export Customer Master to OCC ');
	write_log('Start Date: ' ||
			  to_char(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
	write_log(' ');
	write_log('Agruments');
	write_log('-----------');
	write_log('p_interface_type =  ' || p_interface_type);
	write_log('p_increment_date =  ' || p_increment_date);
	write_log('p_output_path =  ' || p_output_path);
	write_log('p_file_name_format =  ' || p_file_name_format);
	write_log('p_separator =  ' || p_separator);
	write_log('p_org_id_list =  ' || p_org_id_list);
	write_log('+----------------------------------------------------------------------------+');
	
	-- validate parameter
	IF p_output_path IS NULL THEN
		RAISE utl_file.invalid_path;
	END IF;
	IF p_file_name_format IS NULL THEN
		RAISE utl_file.invalid_filename;
	END IF;
	IF p_increment_date IS NULL THEN
		v_date      := trunc(SYSDATE) - 1;
		v_file_date := SYSDATE;
	ELSE
		v_date      := trunc(to_date(p_increment_date,
									 'yyyy/mm/dd hh24:mi:ss'));
		v_file_date := v_date;
	END IF;
	
	v_file_name := p_file_name_format;
	v_file_name := REPLACE(v_file_name,
						   '$DATETIME$',
						   to_char(v_file_date, 'YYYYMMDD_HH24MISS'));
	v_file_name := REPLACE(v_file_name,
						   '$DATE$',
						   to_char(v_file_date, 'YYYYMMDD'));
	-- open file for write item master
	write_log('++++++++');
	write_log('Create file ' || get_directory_path(p_output_path) || '/' ||
			  v_file_name || ' for write data');
	
	v_file_handle := utl_file.fopen_nchar(p_output_path,
										  v_file_name,
										  'w');
	BEGIN
		utl_file.fclose(v_file_handle);
	EXCEPTION
		WHEN OTHERS THEN
			NULL;
	END;
	v_file_handle := utl_file.fopen_nchar(p_output_path,
										  v_file_name,
										  'w',
										  max_linesize);
	v_bom_raw     := hextoraw('EFBBBF');
	--utl_file.put_raw(v_file_handle, v_bom_raw);
	
	write_log('+---------------------------------------------------------------------------+');
	write_log('Parameters');
	write_log('++++++++');
	write_log('p_interface_type = ' || p_interface_type);
	write_log('v_date = ' || to_char(v_date, 'dd/mm/yyyy hh24:mi:ss'));
	write_log(' ');
	write_log('+---------------------------------------------------------------------------+');
	write_log('Qeury data and write to file.');
	write_log(' ');
	v_line_count := 0;
	err_code     := '2';
	-- query increment item master (creation date)
	FOR r_data IN (SELECT DISTINCT CASE poh.org_id
									   WHEN 102 THEN
										'DTAC'
									   WHEN 142 THEN
										'DTN'
									   ELSE
										'DTAC'
								   END company_code,
								   poh.segment1 po_number,
								   poh.revision_num release_no,
								   to_char(poh.creation_date,
										   'dd/mm/yyyy hh24:mi:ss') po_date,
								   pv.segment1 supplier_code,
								   pvs.vendor_site_code supplier_site_code,
								   REPLACE(poh.comments, '|', '/') description,
								   poh.currency_code currency_code,
								   pol.line_num po_line_number,
								   msi.segment1 product_sku_code,
								   org.organization_code org_code,
								   '' sub_inven_code,
								   pol.quantity quantity,
								   SUM(poll.quantity_cancelled) over(PARTITION BY poll.po_line_id) cancel_qty,
								   SUM(poll.quantity_received) over(PARTITION BY poll.po_line_id) received_qty,
								   poll.unit_meas_lookup_code uom,
								   pol.unit_price unit_price,
								   to_char(poh.creation_date,
										   'dd/mm/yyyy hh24:mi:ss') creation_date,
								   (SELECT MIN(prl.attribute2)
									FROM po_lines_all             pol1,
										 po_line_locations_all    poll,
										 po_distributions_all     pod,
										 po_req_distributions_all prd,
										 po_requisition_lines_all prl
									WHERE pol1.po_line_id =
										  pol.po_line_id
									AND pol.po_line_id = poll.po_line_id
									AND poll.line_location_id =
										  pod.line_location_id
									AND pod.req_distribution_id =
										  prd.distribution_id
									AND prd.requisition_line_id =
										  prl.requisition_line_id
									AND prl.attribute2 IS NOT NULL) sfa_transaction_code
				   FROM po_headers_all               poh,
						po_lines_all                 pol,
						po_vendors                   pv,
						po_vendor_sites_all          pvs,
						mtl_system_items_b           msi,
						mtl_parameters               mpt,
						org_organization_definitions iorg,
						fnd_lookup_values_vl         fv,
						fnd_lookup_types_vl          fl,
						po_line_locations_all        poll,
						org_organization_definitions org,
						po_line_types                plt
				   WHERE 1 = 1
				   AND msi.item_type IN ('DEVICE', 'MOBILE')
				   AND poh.vendor_id = pv.vendor_id
				   AND poh.org_id = pvs.org_id
				   AND poh.vendor_site_id = pvs.vendor_site_id
				   AND poh.po_header_id = pol.po_header_id
				   AND pol.item_id = msi.inventory_item_id
				   AND msi.organization_id = mpt.organization_id
				   AND mpt.organization_id = mpt.master_organization_id
				   AND msi.organization_id = iorg.organization_id
				   AND poh.org_id = iorg.operating_unit
				   AND msi.item_type = fv.lookup_code
				   AND fv.lookup_type = fl.lookup_type
				   AND fl.lookup_type = 'ITEM_TYPE'
				   AND fl.application_id = 401
				   AND poh.authorization_status = 'APPROVED' -- FDD_150909 : DTAC{'991015007900', ''91015007890'} DTN {8015003047}
				   AND poh.closed_code = 'OPEN'
				   AND pol.po_line_id = poll.po_line_id
				   AND poll.ship_to_organization_id = org.organization_id
						-- Add condition po Line Type = 'Expense' ; FDD_151002
				   AND pol.line_type_id = plt.line_type_id
				   AND plt.line_type = 'Expenses'
							
						--    and poh.segment1 =  '991015007900', ''91015007890', '991007014310' -- '991007014310', 991007014411, 991007015054, 991007015234, 991007016508
						\* -- FDD_150909 : Change to po apprival date
						and (trunc(poh.creation_date) = decode(p_interface_type, 'ALL', trunc(poh.creation_date), v_date) or
						trunc(poh.last_update_date) = decode(p_interface_type, 'ALL', trunc(poh.last_update_date), v_date) or
						trunc(pol.creation_date) = decode(p_interface_type, 'ALL', trunc(pol.creation_date), v_date) or
						trunc(pol.last_update_date) = decode(p_interface_type, 'ALL', trunc(pol.last_update_date), v_date) or
						trunc(poll.creation_date) = decode(p_interface_type, 'ALL', trunc(poll.creation_date), v_date) or
						trunc(poll.last_update_date) = decode(p_interface_type, 'ALL', trunc(poll.last_update_date), v_date))
						*\
				   AND ('ALL' = p_interface_type OR
						 (p_interface_type != 'ALL' AND
						 poh.approved_date >= v_date AND
						 poh.approved_date < (v_date + 1)))
				   AND poh.org_id IN
						 (SELECT substr(org_ids,
										instr(org_ids, ',', 1, lvl) + 1,
										instr(org_ids, ',', 1, lvl + 1) -
										instr(org_ids, ',', 1, lvl) - 1) orderid
						  FROM (SELECT ',' || org_ids || ',' AS org_ids
								FROM (SELECT p_org_id_list AS org_ids
									  FROM dual)),
							   (SELECT LEVEL AS lvl
								FROM dual
								CONNECT BY LEVEL <= 20)
						  WHERE lvl <= length(org_ids) -
								length(REPLACE(org_ids, ',')) - 1)
				   ORDER BY poh.segment1, pol.line_num) LOOP
		v_net_qty := 0;
		IF (r_data.cancel_qty > 0) THEN
			v_net_qty  := nvl(r_data.received_qty, 0);
			v_quantity := 0;
		ELSE
			v_net_qty  := nvl(r_data.quantity, 0);
			v_quantity := nvl(r_data.quantity, 0);
		END IF;
		
		v_line_data := REPLACE(r_data.company_code, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.po_number, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.release_no, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.po_date, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.supplier_code, p_separator, '') ||
					   p_separator || REPLACE(r_data.supplier_site_code,
											  p_separator,
											  '') || p_separator ||
					   REPLACE(r_data.description, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.currency_code, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.po_line_number, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.product_sku_code, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.org_code, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.sub_inven_code, p_separator, '') ||
					   p_separator ||
					   REPLACE(v_quantity, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.cancel_qty, p_separator, '') ||
					   p_separator ||
					   REPLACE(v_net_qty, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.uom, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.unit_price, p_separator, '') ||
					   p_separator ||
					   REPLACE(r_data.creation_date, p_separator, '') ||
					   p_separator || REPLACE(r_data.sfa_transaction_code,
											  p_separator,
											  '');
		
		IF (v_put_bom_flag = 'N') THEN
			v_put_bom_flag := 'Y';
			utl_file.put_raw(v_file_handle, v_bom_raw);
		END IF;
		
		write_line_nchar(v_file_handle, v_line_data);
		write_log(v_line_data);
		v_line_count := v_line_count + 1;
	END LOOP;
	write_log('+---------------------------------------------------------------------------+');
	
	IF utl_file.is_open(v_file_handle) THEN
		utl_file.fclose(v_file_handle);
	END IF;
	err_code := 0;
EXCEPTION
	WHEN utl_file.access_denied THEN
		err_msg := 'ACCESS DENIED: Access to the file has been denied by the operating system';
		write_log(err_msg);
		raise_application_error(-20001, err_msg);
	WHEN utl_file.file_open THEN
		err_msg := 'FILE OPEN: File is already open';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.internal_error THEN
		err_msg := 'INTERNAL ERROR: Unhandled internal error in the UTL_FILE package';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.invalid_filehandle THEN
		err_msg := 'INVALID FILE HANDLE: File handle does not exist';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.invalid_filename THEN
		err_msg := 'INVALID FILENAME: A file with the specified name does not exist in the path';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.invalid_maxlinesize THEN
		err_msg := 'INVALID MAXLINESIZE: The MAX_LINESIZE value for FOPEN() is invalid; it should be within the range 1 to 32767';
		write_log(err_msg);
		raise_application_error(-20001, err_msg);
	WHEN value_error THEN
		err_msg := 'VAULE ERROR: GET_LINE value is larger than the buffer';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.invalid_mode THEN
		err_msg := 'INVALID MODE: The open_mode parameter in FOPEN is invalid';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.invalid_operation THEN
		err_msg := 'INVALID OPERATION: File could not be opened or operated on as requested';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.invalid_path THEN
		err_msg := 'INVALID PATH: Specified path does not exist or is not visible to Oracle';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN utl_file.read_error THEN
		err_msg := 'READ ERROR: Unable to read file';
		write_log(err_msg);
		raise_application_error(-20002, err_msg);
	WHEN no_data_found THEN
		IF utl_file.is_open(v_file_handle) THEN
			utl_file.fclose(v_file_handle);
		END IF;
	WHEN OTHERS THEN
		IF utl_file.is_open(v_file_handle) THEN
			utl_file.fclose(v_file_handle);
		END IF;
		raise_application_error(-20011, SQLCODE || ': ' || SQLERRM);
END export_purchase_order;

FUNCTION validate_grn(p_request_id NUMBER) RETURN VARCHAR2 IS
	v_result VARCHAR2(1000) := NULL;
BEGIN
	IF (p_request_id IS NOT NULL) THEN
		RETURN '';
	ELSE
		RETURN 'ERROR : No request identify.';
	END IF;
EXCEPTION
	WHEN OTHERS THEN
		v_result := 'ERROR : ' || SQLERRM;
		RETURN v_result;
END;

FUNCTION validate_gl(p_request_id NUMBER) RETURN VARCHAR2 IS
	v_result VARCHAR2(1000) := NULL;
BEGIN
	IF (p_request_id IS NOT NULL) THEN
		RETURN '';
	ELSE
		RETURN 'ERROR : No request identify.';
	END IF;
EXCEPTION
	WHEN OTHERS THEN
		v_result := 'ERROR : ' || SQLERRM;
		RETURN v_result;
END;
PROCEDURE interface_grn
(
	err_msg                 OUT VARCHAR2
   ,err_code                OUT VARCHAR2
   ,p_company_map_org_id    VARCHAR2
   ,p_file_path             VARCHAR2
   ,p_file_name_format      VARCHAR2
   ,p_file_name             VARCHAR2 DEFAULT NULL
   ,p_delimiter             VARCHAR2 DEFAULT '|'
   ,p_debug_flag            VARCHAR2 DEFAULT 'N'
   ,p_purge_days            NUMBER DEFAULT 45
   ,p_run_interface_process VARCHAR2 DEFAULT 'Y'
) IS
	v_file_name   VARCHAR2(100);
	v_date        DATE := SYSDATE;
	v_file_handle utl_file.file_type;
	v_line_data   NVARCHAR2(32767);
	v_line_count  NUMBER := 0;
	
	v_rec_data          dtac_grn_interface_temp %ROWTYPE;
	v_insert_error_flag VARCHAR2(1) := 'N';
	
	v_request_id         NUMBER := fnd_profile.value('CONC_REQUEST_ID');
	v_creation_date      DATE := SYSDATE;
	v_debug_flag         VARCHAR2(10);
	v_delimiter          VARCHAR2(10);
	v_company_map_org_id VARCHAR2(500);
	
	v_login_id                 NUMBER := fnd_profile.value('LOGIN_ID');
	v_user_id                  NUMBER := fnd_profile.value('USER_ID');
	v_header_interface_id      NUMBER;
	v_group_interface_id       NUMBER;
	v_interface_transaction_id NUMBER;
	--v_receipt_group_interface_id number;
	v_return2receive_group_id NUMBER;
	v_return2vendor_group_id  NUMBER;
	v_created_by              NUMBER;
	v_employee_id             NUMBER;
	v_locator_id              NUMBER;
	v_item_unit_of_measure    VARCHAR2(25);
	
	v_erp_receipt_num VARCHAR2(50);
	
	v_destination_type_code VARCHAR2(50) := 'INVENTORY';
	v_submit_request_id     NUMBER;
	
	v_run_interface_process VARCHAR2(10) := 'Y';
	
	v_count_receive NUMBER := 0;
	v_count_return  NUMBER := 0;
	
BEGIN
	err_code             := '2';
	v_delimiter          := p_delimiter;
	v_debug_flag         := p_debug_flag;
	v_company_map_org_id := nvl(p_company_map_org_id,
								'DTAC=102,DTN=142');
	
	write_log(' ');
	write_log('+---------------------------------------------------------------------------+');
	write_log('Start Concurrent Goods Receipt Interface from OCC ');
	write_log('Start Date: ' ||
			  to_char(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
	write_log(' ');
	write_log('Agruments');
	write_log('-----------');
	write_log('p_company_map_org_id =  ' || p_company_map_org_id);
	write_log('p_file_path =  ' || p_file_path);
	write_log('p_file_name_format =  ' || p_file_name_format);
	write_log('p_file_name =  ' || p_file_name);
	write_log('p_delimiter =  ' || p_delimiter);
	write_log('p_debug_flag =  ' || p_debug_flag);
	write_log('p_run_interface_process =  ' || p_run_interface_process);
	
	write_log('+----------------------------------------------------------------------------+');
	
	-- Start import data from text file
	v_run_interface_process := nvl(p_run_interface_process, 'Y');
	IF p_file_path IS NULL THEN
		RAISE utl_file.invalid_path;
	END IF;
	
	IF (p_file_name IS NOT NULL) THEN
		v_file_name := p_file_name;
	ELSE
		IF p_file_name_format IS NULL THEN
			RAISE utl_file.invalid_filename;
		END IF;
		v_file_name := REPLACE(p_file_name_format,
							   '$DATE$',
							   TRIM(to_char(v_date, 'yyyymmdd')));
	END IF;
	
	write_log(g_log_line);
	write_log('Import GRN data from file ' ||
			  get_directory_path(g_grn_inbox) || '/' || v_file_name);
	write_log(' ');
	
	BEGIN
		DELETE FROM dtac_grn_interface_temp tmp
		WHERE tmp.interface_date <= (SYSDATE - nvl(p_purge_days, 45));
		COMMIT;
	EXCEPTION
		WHEN OTHERS THEN
			NULL;
	END;
	
	BEGIN
		--v_file_handle := utl_file.fopen(location => p_file_path, filename => v_file_name, open_mode => 'r');
		v_file_handle := utl_file.fopen_nchar(location  => p_file_path,
											  filename  => v_file_name,
											  open_mode => 'r');
		utl_file.fclose(v_file_handle);
	EXCEPTION
		WHEN OTHERS THEN
			write_log(' Error first read : ' || SQLERRM);
			write_log(' ===============');
			write_log('p_file_path = ' || p_file_path);
			write_log('v_file_name = ' || v_file_name);
			write_log(' ===============');
	END;
	--v_file_handle := utl_file.fopen(location => p_file_path, filename => v_file_name, open_mode => 'r');
	
	--write_log('p_file_path = ' || p_file_path);
	--write_log('v_file_name = ' || v_file_name);
	v_file_handle := utl_file.fopen_nchar(location  => p_file_path,
										  filename  => v_file_name,
										  open_mode => 'r');
	
	-- Read first line for header (ignore BOM byte)
	--utl_file.get_line_nchar(file => v_file_handle, buffer => v_line_data);
	--utl_file.get_line(file => v_file_handle, buffer => v_line_data);
	--v_line_data := '';
	
	SAVEPOINT before_insert_temp;
	LOOP
		BEGIN
			
			-- write_log('before get line in loop : ' || v_file_handle);
			--utl_file.get_line(file => v_file_handle, buffer => v_line_data);
			utl_file.get_line_nchar(file   => v_file_handle,
									buffer => v_line_data);
			write_log('befire replace v_line_data: ' || v_line_data);
			v_line_data := REPLACE(REPLACE(v_line_data, chr(10), ''),
								   chr(13),
								   '');
			v_line_data := substr(v_line_data, 2, length(v_line_data));
			write_log('after replace v_line_data: ' || v_line_data);
			IF (v_line_data = 'END') THEN
				--write_log(' END of file');
				EXIT;
			END IF;
			v_line_count := v_line_count + 1;
			
			v_rec_data := split_grn_data(p_line_buff  => v_line_data,
										 p_delimiter  => v_delimiter,
										 p_debug_flag => v_debug_flag);
			BEGIN
				
				v_rec_data.request_id     := v_request_id;
				v_rec_data.interface_date := SYSDATE;
				--write_log('Before Insert temp table line#' || to_char(v_line_count, 90000) || ' [ Data ] =>  ' || v_line_data);
				INSERT INTO dtac_grn_interface_temp
					(company_code,
					 transaction_type,
					 grn_number,
					 po_number,
					 release_number,
					 po_line_number,
					 product_sku_code,
					 invoice_number,
					 rate_date,
					 qty,
					 inventory_org_code,
					 sub_inventory,
					 locator,
					 create_by,
					 creation_date,
					 request_id,
					 interface_date,
					 validate_status,
					 error_msg)
				VALUES
					(v_rec_data.company_code,
					 v_rec_data.transaction_type,
					 v_rec_data.grn_number,
					 v_rec_data.po_number,
					 v_rec_data.release_number,
					 v_rec_data.po_line_number,
					 v_rec_data.product_sku_code,
					 v_rec_data.invoice_number,
					 v_rec_data.rate_date,
					 v_rec_data.qty,
					 v_rec_data.inventory_org_code,
					 v_rec_data.sub_inventory,
					 v_rec_data.locator,
					 v_rec_data.create_by,
					 v_rec_data.creation_date,
					 v_rec_data.request_id,
					 v_rec_data.interface_date,
					 v_rec_data.validate_status,
					 v_rec_data.error_msg);
				
				write_log('Line#' || to_char(v_line_count, 90000) ||
						  ' [ OK] =>  ' || v_line_data);
			EXCEPTION
				WHEN OTHERS THEN
					v_insert_error_flag := 'Y';
					write_log('Line#' || to_char(v_line_count, 90000) ||
							  ' [ERR] =>  ' || v_line_data);
					write_output('Line#' ||
								 to_char(v_line_count, 90000) ||
								 ' [ERR] =>  ' || SQLERRM);
			END;
		EXCEPTION
			WHEN no_data_found THEN
				--write_log('no data found :'|| v_line_data);
				EXIT;
			WHEN OTHERS THEN
				v_insert_error_flag := 'Y';
				write_log('Read data from text file error :  ' ||
						  SQLERRM);
				EXIT;
		END;
	END LOOP;
	-- check open and close
	IF (utl_file.is_open(file => v_file_handle)) THEN
		utl_file.fclose(file => v_file_handle);
	END IF;
	
	IF (v_insert_error_flag = 'Y') THEN
		ROLLBACK TO SAVEPOINT before_insert_temp;
		write_output(' ');
		write_output(' ');
		write_output(g_log_line);
		
		write_log(' ');
		write_log('tac_discovery_interface_util.interface_grn @import_data exception when insert data. See output for error detail.');
		
		err_msg  := 'Error tac_discovery_interface_util.interface_grn @import_data : Some record error during insert to table. See output for detail.';
		err_code := '2';
		move_data_file(get_directory_path(g_grn_inbox) || '/' ||
					   v_file_name,
					   get_directory_path(g_grn_error));
		
		RETURN;
	ELSE
		write_log('commit insert data to temp table');
		COMMIT;
		move_data_file(get_directory_path(g_grn_inbox) || '/' ||
					   v_file_name,
					   get_directory_path(g_grn_history));
	END IF;
	-- End of import data from text file
	
	SAVEPOINT before_insert_interface;
	v_insert_error_flag := 'N';
	-- validate GRN
	IF (validate_grn(v_request_id) IS NOT NULL) THEN
		write_log('Validate : Failed.');
	ELSE
		write_log('Validate : Pass.');
		-- Start insert data from temp table to interface table
		SELECT rcv_interface_groups_s.nextval
		INTO v_group_interface_id
		FROM dual;
		SELECT rcv_interface_groups_s.nextval
		INTO v_return2receive_group_id
		FROM dual;
		SELECT rcv_interface_groups_s.nextval
		INTO v_return2vendor_group_id
		FROM dual;
		
		FOR r_po_grn IN (SELECT tmp.po_number,
								tmp.transaction_type,
								tmp.request_id,
								COUNT(DISTINCT tmp.invoice_number) count_invoice_number
						 FROM dtac_grn_interface_temp tmp
						 WHERE tmp.request_id = v_request_id
						 GROUP BY tmp.po_number,
								  tmp.transaction_type,
								  tmp.request_id) LOOP
			IF (r_po_grn.transaction_type = 'Receipt') THEN
				v_count_receive := v_count_receive + 1;
				SELECT rcv_headers_interface_s.nextval
				INTO v_header_interface_id
				FROM dual;
				--select rcv_interface_groups_s.nextval into v_group_interface_id from dual;
				
				-- RCV Header
				INSERT INTO rcv_headers_interface
					(header_interface_id,
					 group_id,
					 processing_status_code,
					 receipt_source_code,
					 transaction_type,
					 last_update_date,
					 last_updated_by,
					 last_update_login,
					 creation_date,
					 created_by,
					 shipped_date,
					 vendor_id,
					 expected_receipt_date,
					 validation_flag,
					 packing_slip)
					SELECT v_header_interface_id header_interface_id,
						   v_group_interface_id group_id,
						   'PENDING' processing_status_code,
						   'VENDOR' receipt_source_code,
						   'NEW' transaction_type,
						   SYSDATE last_update_date,
						   v_user_id last_updated_by,
						   v_login_id last_update_login,
						   SYSDATE creation_date
						   \* -- change from employe_code to user_lan value
						   ,(select fu.user_id
						   from per_people_f emp
						   ,fnd_user fu
						   where emp.employee_number = tmp.create_by
						   and emp.effective_end_date >= sysdate
						   and emp.person_id = fu.employee_id) created_by*\,
						   fu.user_id created_by,
						   SYSDATE shipped_date,
						   poh.vendor_id vendor_id,
						   SYSDATE expected_receipt_date,
						   'Y' validation_flag,
						   tmp.invoice_number packing_slip
					FROM dtac_grn_interface_temp tmp,
						 po_headers_all          poh,
						 fnd_user                fu
					WHERE 1 = 1
					AND tmp.po_number = poh.segment1
					AND upper(tmp.create_by) = fu.user_name(+)
					AND tmp.transaction_type = r_po_grn.transaction_type
					AND tmp.request_id = r_po_grn.request_id
					AND poh.segment1 = r_po_grn.po_number
					AND rownum = 1;
				-- RCV Transaction
				FOR r_rcv_trx IN (SELECT poh.po_header_id,
										 poh.vendor_id,
										 poh.vendor_site_id,
										 pol.po_line_id,
										 pll.line_location_id,
										 nvl(tmp.release_number,
											 por.release_num) release_num,
										 tmp.po_line_number,
										 pll.shipment_num,
										 pll.ship_to_location_id,
										 (SELECT substr(data_pair,
														instr(data_pair,
															  '=') + 1) org_id
										  FROM (SELECT substr(orderids,
															  instr(orderids,
																	',',
																	1,
																	lvl) + 1,
															  instr(orderids,
																	',',
																	1,
																	lvl + 1) -
															  instr(orderids,
																	',',
																	1,
																	lvl) - 1) data_pair
												FROM (SELECT ',' ||
															 orderids || ',' AS orderids
													  FROM (SELECT v_company_map_org_id AS orderids
															FROM dual)),
													 (SELECT LEVEL AS lvl
													  FROM dual
													  CONNECT BY LEVEL <= 50)
												WHERE lvl <=
													  length(orderids) -
													  length(REPLACE(orderids,
																	 ',')) - 1)
										  WHERE substr(data_pair,
													   1,
													   instr(data_pair,
															 '=') - 1) =
												tmp.company_code) ou_id,
										 pll.ship_to_organization_id ship_to_organization_id,
										 pol.unit_meas_lookup_code,
										 tmp.qty quantity,
										 pol.item_id,
										 msi.inventory_item_id,
										 tmp.product_sku_code item_code,
										 por.po_release_id,
										 nvl(tmp.rate_date,
											 poh.rate_date) rate_date,
										 tmp.grn_number attribute7,
										 mp.organization_code,
										 mp.master_organization_id,
										 msi.organization_id,
										 tmp.create_by,
										 tmp.sub_inventory,
										 tmp.locator,
										 tmp.po_number
								  FROM dtac_grn_interface_temp tmp,
									   po_headers_all          poh,
									   po_releases_all         por,
									   po_lines_all            pol,
									   po_line_locations_all   pll,
									   mtl_parameters          mp,
									   mtl_system_items_b      msi
								  WHERE tmp.po_number = poh.segment1
								  AND poh.po_header_id = pol.po_header_id
								  AND pol.line_num = tmp.po_line_number
								  AND pol.po_line_id = pll.po_line_id
								  AND poh.po_header_id =
										por.po_header_id(+)
								  AND nvl(pll.po_release_id, -99) =
										nvl(por.po_release_id, -99)
								  AND nvl(por.release_num, -99) =
										nvl(tmp.release_number,
										 nvl(por.release_num, -99))
								  AND tmp.inventory_org_code =
										mp.organization_code
								  AND tmp.product_sku_code = msi.segment1
								  AND msi.organization_id =
										mp.master_organization_id
										   
								  AND tmp.po_number = r_po_grn.po_number
								  AND tmp.transaction_type =
										r_po_grn.transaction_type
								  AND tmp.request_id =
										r_po_grn.request_id) LOOP
					SELECT rcv_transactions_interface_s.nextval
					INTO v_interface_transaction_id
					FROM dual;
					BEGIN
						SELECT fu.user_id, fu.employee_id
						INTO v_created_by, v_employee_id
						FROM fnd_user fu
						WHERE fu.user_name = upper(r_rcv_trx.create_by);
					EXCEPTION
						WHEN OTHERS THEN
							v_created_by  := NULL;
							v_employee_id := NULL;
					END;
					
					BEGIN
						SELECT DISTINCT inventory_location_id
						INTO v_locator_id
						FROM mtl_item_locations mil
						WHERE segment1 = r_rcv_trx.locator;
					EXCEPTION
						WHEN OTHERS THEN
							v_locator_id := NULL;
					END;
					BEGIN
						INSERT INTO rcv_transactions_interface
							(interface_transaction_id,
							 group_id,
							 header_interface_id,
							 last_update_date,
							 last_updated_by,
							 creation_date,
							 created_by,
							 transaction_type,
							 transaction_date,
							 processing_status_code,
							 processing_mode_code,
							 transaction_status_code,
							 quantity,
							 unit_of_measure,
							 interface_source_code,
							 item_id,
							 employee_id,
							 auto_transact_code,
							 receipt_source_code,
							 to_organization_id,
							 source_document_code,
							 destination_type_code,
							 deliver_to_location_id,
							 locator_id,
							 subinventory,
							 expected_receipt_date,
							 vendor_id,
							 vendor_site_id,
							 po_header_id,
							 po_release_id,
							 po_line_id,
							 po_line_location_id,
							 shipment_num,
							 validation_flag,
							 currency_conversion_date,
							 substitute_item_id,
							 attribute7)
							SELECT v_interface_transaction_id,
								   v_group_interface_id,
								   v_header_interface_id,
								   SYSDATE last_update_date,
								   v_user_id last_updated_by,
								   SYSDATE creation_date,
								   v_created_by created_by,
								   'RECEIVE' transaction_type,
								   SYSDATE transaction_date,
								   'PENDING' processing_status_code,
								   'BATCH' processing_mode_code,
								   'PENDING' transaction_status_code,
								   r_rcv_trx.quantity quantity,
								   r_rcv_trx.unit_meas_lookup_code unit_of_measure,
								   'RCV' interface_source_code,
								   r_rcv_trx.inventory_item_id item_id,
								   v_employee_id employee_id,
								   'DELIVER' auto_transact_code,
								   'VENDOR' receipt_source_code,
								   r_rcv_trx.ship_to_organization_id to_organization_id,
								   'PO' source_document_code,
								   v_destination_type_code destination_type_code -- INVENTORY, EXPENSE
								  ,
								   r_rcv_trx.ship_to_location_id deliver_to_location_id,
								   v_locator_id locator_id,
								   r_rcv_trx.sub_inventory subinventory,
								   SYSDATE expected_receipt_date,
								   r_rcv_trx.vendor_id vendor_id,
								   r_rcv_trx.vendor_site_id vendor_site_id,
								   r_rcv_trx.po_header_id po_header_id,
								   r_rcv_trx.po_release_id po_release_id,
								   r_rcv_trx.po_line_id po_line_id,
								   r_rcv_trx.line_location_id po_line_location_id,
								   r_rcv_trx.shipment_num shipment_num,
								   'Y' validation_flag,
								   trunc(r_rcv_trx.rate_date, 'dd') currency_conversion_date,
								   NULL substitute_item_id,
								   r_rcv_trx.attribute7
							FROM dual;
					EXCEPTION
						WHEN OTHERS THEN
							v_insert_error_flag := 'Y';
							write_log('[ERR] => ' ||
									  upper(r_po_grn.transaction_type) ||
									  ' : ' || 'PO# ' ||
									  r_rcv_trx.po_number ||
									  ' PO Line# ' ||
									  r_rcv_trx.po_line_number);
							write_output('[ERR] => ' ||
										 upper(r_po_grn.transaction_type) ||
										 ' : ' || 'PO# ' ||
										 r_rcv_trx.po_number ||
										 ' PO Line# ' ||
										 r_rcv_trx.po_line_number ||
										 ' : ' || SQLERRM);
					END;
				END LOOP;
			ELSE
				v_count_return := v_count_return + 1;
				-- RETURN TO RECEIVING
				\*
				A) perform a "RETURN to RECEIVING" for a standard Purchase Order
				through ROI
				TRANSACTION_TYPE = RETURN TO RECEIVING
				PARENT_TRANSACTION_ID = (TRANSACTION_ID for DELIVER transaction)
				*\
				\*
					for r_rtn2rcv_trx in (select distinct rsh.receipt_num
														 ,rt.transaction_id
														 ,rt.transaction_date
														 ,tmp.qty quantity
														 ,rt.unit_of_measure
														 ,rt.shipment_header_id
														 ,rt.shipment_line_id
														 ,rt.source_document_code
														 ,rt.destination_type_code
														 ,rt.employee_id
														 ,rt.parent_transaction_id
														 ,rt.po_header_id
														 ,rt.po_line_id
														 ,rsl.item_id
														 ,rt.po_line_location_id
														 ,rt.po_distribution_id
														 ,rt.deliver_to_person_id
														 ,rt.deliver_to_location_id
														 ,rt.vendor_id
														 ,rt.vendor_site_id
														 ,rt.organization_id
														 ,rt.subinventory
														 ,rt.locator_id
														 ,rt.location_id
														 ,rsh.ship_to_org_id
														 ,(select fu.user_id
														   from per_people_f emp
															   ,fnd_user fu
														   where emp.employee_number = tmp.create_by
																 and emp.effective_end_date >= sysdate
																 and emp.person_id = fu.employee_id) created_by
														  -------------------------------
														 ,tmp.request_id
														 ,tmp.grn_number ax_grn_number
														 ,tmp.po_number
														 ,tmp.po_line_number
														 ,rt.transaction_type
														 ,rsh.receipt_num erp_receipt_num
										  from dtac_grn_interface_temp tmp
											  ,po_headers_all poh
											  ,po_lines_all pol
											  ,rcv_transactions rt
											  ,rcv_shipment_headers rsh
											  ,rcv_shipment_lines rsl
										  where tmp.po_number = poh.segment1
												and tmp.po_line_number = pol.line_num
												and poh.po_header_id = pol.po_header_id
												and tmp.grn_number = rt.attribute7
												and poh.po_header_id = rt.po_header_id
												and rt.transaction_type = 'DELIVER'
												and rt.shipment_header_id = rsh.shipment_header_id
												and rt.shipment_line_id = rsl.shipment_line_id
                            
												and tmp.request_id = r_po_grn.request_id
												and tmp.transaction_type = r_po_grn.transaction_type
												and tmp.po_number = r_po_grn.po_number)
					loop
					  begin
						select min(msi.primary_unit_of_measure) item_unit_of_measure
						into v_item_unit_of_measure
						from mtl_system_items_b msi
						where msi.inventory_item_id = r_rtn2rcv_trx.item_id;
                            
						insert into rcv_transactions_interface
						  (interface_transaction_id
						  ,group_id
						  ,last_update_date
						  ,last_updated_by
						  ,creation_date
						  ,created_by
						  ,last_update_login
						  ,transaction_type
						  ,transaction_date
						  ,processing_status_code
						  ,processing_mode_code
						  ,transaction_status_code
						  ,quantity
						  ,unit_of_measure
						  ,item_id
						  ,employee_id
						  ,shipment_header_id
						  ,shipment_line_id
						  ,receipt_source_code
						  ,vendor_id
						  ,from_organization_id
						  ,from_subinventory
						  ,from_locator_id
						  ,source_document_code
						  ,parent_transaction_id
						  ,po_header_id
						  ,po_line_id
						  ,po_line_location_id
						  ,po_distribution_id
						  ,destination_type_code
						  ,deliver_to_person_id
						  ,location_id
						  ,deliver_to_location_id
						  ,validation_flag)
						values
						  (rcv_transactions_interface_s.nextval --INTERFACE_TRANSACTION_ID
						  ,v_return2receive_group_id --GROUP_ID
						  ,sysdate --LAST_UPDATE_DATE
						  ,v_user_id --LAST_UPDATE_BY
						  ,sysdate --CREATION_DATE
						  ,r_rtn2rcv_trx.created_by --CREATED_BY
						  ,v_login_id --LAST_UPDATE_LOGIN
						  ,'RETURN TO RECEIVING' --TRANSACTION_TYPE
						  ,sysdate --TRANSACTION_DATE
						  ,'PENDING' --PROCESSING_STATUS_CODE
						  ,'BATCH' --PROCESSING_MODE_CODE
						  ,'PENDING' --TRANSACTION_STATUS_CODE
						  ,r_rtn2rcv_trx.quantity --QUANTITY
						  ,nvl(v_item_unit_of_measure, 'EA') --UNIT_OF_MEASURE
						  ,r_rtn2rcv_trx.item_id --ITEM_ID
						  ,r_rtn2rcv_trx.employee_id --EMPLOYEE_ID
						  ,r_rtn2rcv_trx.shipment_header_id --SHIPMENT_HEADER_ID
						  ,r_rtn2rcv_trx.shipment_line_id --SHIPMENT_LINE_ID
						  ,'VENDOR' --RECEIPT_SOURCE_CODE
						  ,r_rtn2rcv_trx.vendor_id --VENDOR_ID
						  ,r_rtn2rcv_trx.organization_id --FROM_ORGANIZATION_ID
						  ,r_rtn2rcv_trx.subinventory --FROM_SUBINVENTORY
						  ,r_rtn2rcv_trx.locator_id --FROM_LOCATOR_ID
						  ,'PO' --SOURCE_DOCUMENT_CODE
						  ,r_rtn2rcv_trx.transaction_id --PARENT_TRANSACTION_ID
						  ,r_rtn2rcv_trx.po_header_id --PO_HEADER_ID
						  ,r_rtn2rcv_trx.po_line_id --PO_LINE_ID
						  ,r_rtn2rcv_trx.po_line_location_id --PO_LINE_LOCATION_ID
						  ,r_rtn2rcv_trx.po_distribution_id --PO_DISTRIBUTION_ID
						  ,r_rtn2rcv_trx.destination_type_code --DESTINATION_TYPE_CODE
						  ,null --DELIVER_TO_PERSON_ID
						  ,null --LOCATION_ID
						  ,null --DELIVER_TO_LOCATION_ID
						  ,'Y' --VALIDATION_FLAG
						   );
					  exception
						when others then
						  v_insert_error_flag := 'Y';
						  write_log('[ERR] => Insert rcv_transactions_interface for return  to receive ' || upper(r_po_grn.transaction_type) || ' : ' ||
									'PO# ' || r_rtn2rcv_trx.po_number || ' PO Line# ' || r_rtn2rcv_trx.po_line_number);
						  write_output('[ERR] =>  Insert rcv_transactions_interface for return to receive ' || upper(r_po_grn.transaction_type) || ' : ' ||
									   'PO# ' || r_rtn2rcv_trx.po_number || ' PO Line# ' || r_rtn2rcv_trx.po_line_number || ' : ' || sqlerrm);
					  end;
					end loop;
				*\
				
				-- RETURN TO VENDOR
				\*
				B) perform a "RETURN to VENDOR" for a standard Purchase Order
				through ROI
				TRANSACTION_TYPE = RETURN TO VENDOR
				PARENT_TRANSACTION_ID = (TRANSACTION_ID for RECEIVE transaction)
				*\
				FOR r_rtn2vendor_trx IN (SELECT DISTINCT rsh.receipt_num,
														 rt.transaction_id,
														 rt.transaction_date,
														 tmp.qty quantity,
														 rt.unit_of_measure,
														 rt.shipment_header_id,
														 rt.shipment_line_id,
														 rt.source_document_code,
														 rt.destination_type_code,
														 rt.employee_id,
														 rt.parent_transaction_id,
														 rt.po_header_id,
														 rt.po_line_id,
														 rsl.item_id,
														 rt.po_line_location_id,
														 rt.po_distribution_id,
														 rt.deliver_to_person_id,
														 rt.deliver_to_location_id,
														 rt.vendor_id,
														 rt.vendor_site_id,
														 rt.organization_id,
														 rt.subinventory,
														 rt.locator_id,
														 rt.location_id,
														 rsh.ship_to_org_id
														 \* Change from employee_code to User_Lan
														 ,(select fu.user_id
														   from per_people_f emp
															   ,fnd_user fu
														   where emp.employee_number = tmp.create_by
																 and emp.effective_end_date >= sysdate
																 and emp.person_id = fu.employee_id) created_by*\,
														 fu.user_id created_by
														 -------------------------------
														,
														 tmp.request_id,
														 tmp.grn_number      ax_grn_number,
														 tmp.po_number,
														 tmp.po_line_number,
														 rt.transaction_type,
														 rsh.receipt_num     erp_receipt_num
										 FROM dtac_grn_interface_temp tmp,
											  po_headers_all          poh,
											  po_lines_all            pol,
											  rcv_transactions        rt,
											  rcv_shipment_headers    rsh,
											  rcv_shipment_lines      rsl,
											  fnd_user                fu
										 WHERE tmp.po_number =
											   poh.segment1
										 AND tmp.po_line_number =
											   pol.line_num
										 AND poh.po_header_id =
											   pol.po_header_id
										 AND tmp.grn_number =
											   rt.attribute7
										 AND poh.po_header_id =
											   rt.po_header_id
										 AND rt.transaction_type =
											   'RECEIVE'
										 AND rt.shipment_header_id =
											   rsh.shipment_header_id
										 AND rt.shipment_line_id =
											   rsl.shipment_line_id
										 AND upper(tmp.create_by) =
											   fu.user_name(+)
												  
										 AND tmp.request_id =
											   r_po_grn.request_id
										 AND tmp.transaction_type =
											   r_po_grn.transaction_type
										 AND tmp.po_number =
											   r_po_grn.po_number) LOOP
					BEGIN
						SELECT MIN(msi.primary_unit_of_measure) item_unit_of_measure
						INTO v_item_unit_of_measure
						FROM mtl_system_items_b msi
						WHERE msi.inventory_item_id =
							  r_rtn2vendor_trx.item_id;
						
						INSERT INTO rcv_transactions_interface
							(interface_transaction_id,
							 group_id,
							 last_update_date,
							 last_updated_by,
							 creation_date,
							 created_by,
							 last_update_login,
							 transaction_type,
							 transaction_date,
							 processing_status_code,
							 processing_mode_code,
							 transaction_status_code,
							 quantity,
							 unit_of_measure,
							 item_id,
							 employee_id,
							 shipment_header_id,
							 shipment_line_id,
							 receipt_source_code,
							 vendor_id,
							 from_organization_id,
							 from_subinventory,
							 from_locator_id,
							 source_document_code,
							 parent_transaction_id,
							 po_header_id,
							 po_line_id,
							 po_line_location_id,
							 po_distribution_id,
							 destination_type_code,
							 deliver_to_person_id,
							 location_id,
							 deliver_to_location_id,
							 validation_flag)
						VALUES
							(rcv_transactions_interface_s.nextval --INTERFACE_TRANSACTION_ID
							,
							 v_return2vendor_group_id --GROUP_ID
							,
							 SYSDATE --LAST_UPDATE_DATE
							,
							 v_user_id --LAST_UPDATE_BY
							,
							 SYSDATE --CREATION_DATE
							,
							 r_rtn2vendor_trx.created_by --CREATED_BY
							,
							 v_login_id --LAST_UPDATE_LOGIN
							,
							 'RETURN TO VENDOR' --TRANSACTION_TYPE
							,
							 SYSDATE --TRANSACTION_DATE
							,
							 'PENDING' --PROCESSING_STATUS_CODE
							,
							 'BATCH' --PROCESSING_MODE_CODE
							,
							 'PENDING' --TRANSACTION_STATUS_CODE
							,
							 r_rtn2vendor_trx.quantity --QUANTITY
							,
							 nvl(v_item_unit_of_measure, 'EA') --UNIT_OF_MEASURE
							,
							 r_rtn2vendor_trx.item_id --ITEM_ID
							,
							 r_rtn2vendor_trx.employee_id --EMPLOYEE_ID
							,
							 r_rtn2vendor_trx.shipment_header_id --SHIPMENT_HEADER_ID
							,
							 r_rtn2vendor_trx.shipment_line_id --SHIPMENT_LINE_ID
							,
							 'VENDOR' --RECEIPT_SOURCE_CODE
							,
							 r_rtn2vendor_trx.vendor_id --VENDOR_ID
							,
							 r_rtn2vendor_trx.organization_id --FROM_ORGANIZATION_ID
							,
							 r_rtn2vendor_trx.subinventory --FROM_SUBINVENTORY
							,
							 r_rtn2vendor_trx.locator_id --FROM_LOCATOR_ID
							,
							 'PO' --SOURCE_DOCUMENT_CODE
							,
							 r_rtn2vendor_trx.transaction_id --PARENT_TRANSACTION_ID
							,
							 r_rtn2vendor_trx.po_header_id --PO_HEADER_ID
							,
							 r_rtn2vendor_trx.po_line_id --PO_LINE_ID
							,
							 r_rtn2vendor_trx.po_line_location_id --PO_LINE_LOCATION_ID
							,
							 r_rtn2vendor_trx.po_distribution_id --PO_DISTRIBUTION_ID
							,
							 r_rtn2vendor_trx.destination_type_code --DESTINATION_TYPE_CODE
							,
							 NULL --DELIVER_TO_PERSON_ID
							,
							 NULL --LOCATION_ID
							,
							 NULL --DELIVER_TO_LOCATION_ID
							,
							 'Y' --VALIDATION_FLAG
							 );
					EXCEPTION
						WHEN OTHERS THEN
							v_insert_error_flag := 'Y';
							write_log('[ERR] => Insert rcv_transactions_interface for return  to vendor ' ||
									  upper(r_po_grn.transaction_type) ||
									  ' : ' || 'PO# ' ||
									  r_rtn2vendor_trx.po_number ||
									  ' PO Line# ' ||
									  r_rtn2vendor_trx.po_line_number);
							write_output('[ERR] =>  Insert rcv_transactions_interface for return to vendor ' ||
										 upper(r_po_grn.transaction_type) ||
										 ' : ' || 'PO# ' ||
										 r_rtn2vendor_trx.po_number ||
										 ' PO Line# ' ||
										 r_rtn2vendor_trx.po_line_number ||
										 ' : ' || SQLERRM);
					END;
				END LOOP;
				
			END IF;
		END LOOP;
		
		IF (v_insert_error_flag = 'Y') THEN
			ROLLBACK TO SAVEPOINT before_insert_interface;
			write_log(' ');
			write_log('tac_discovery_interface_util.interface_grn @ insert interface. See output for error detail.');
			
			err_msg  := 'Error tac_discovery_interface_util.interface_grn @insert interface : Some record error during insert to table. See output for detail.';
			err_code := '2';
		ELSE
			COMMIT;
		END IF;
	END IF;
	
	IF (v_insert_error_flag <> 'Y' AND v_run_interface_process = 'Y') THEN
		-- Receive
		IF (v_count_receive > 0) THEN
			v_submit_request_id := fnd_request.submit_request(application => 'PO',
															  program     => 'RVCTP',
															  description => NULL,
															  start_time  => SYSDATE,
															  sub_request => FALSE,
															  argument1   => 'BATCH',
															  argument2   => v_group_interface_id);
			COMMIT;
			IF v_submit_request_id > 0 THEN
				conc_wait(v_submit_request_id);
			END IF;
		END IF;
		
		IF (v_count_return > 0) THEN
			\*
			-- Return to Receiving
			v_submit_request_id := fnd_request.submit_request(application => 'PO'
															 ,program => 'RVCTP'
															 ,description => null
															 ,start_time => sysdate
															 ,sub_request => false
															 ,argument1 => 'BATCH'
															 ,argument2 => v_return2receive_group_id);
			commit;
			if v_submit_request_id > 0 then
			  conc_wait(v_submit_request_id);
			end if;
			*\
			-- Return to Vendor
			v_submit_request_id := fnd_request.submit_request(application => 'PO',
															  program     => 'RVCTP',
															  description => NULL,
															  start_time  => SYSDATE,
															  sub_request => FALSE,
															  argument1   => 'BATCH',
															  argument2   => v_return2vendor_group_id);
			
			COMMIT;
			IF v_submit_request_id > 0 THEN
				conc_wait(v_submit_request_id);
			END IF;
		END IF;
	END IF;
	
	err_code := '0';
EXCEPTION
	WHEN utl_file.invalid_path THEN
		write_output('Invalid File Location');
		write_log('Invalid File Location');
		raise_application_error(-20052, 'Invalid File Location');
	WHEN utl_file.invalid_operation THEN
		write_output('Invalid File Operation');
		write_log('Invalid Operation');
		raise_application_error(-20054, 'Invalid Operation');
	WHEN utl_file.read_error THEN
		write_output('Read File Error');
		write_log('Read Error');
		raise_application_error(-20055, 'Read Error');
	WHEN utl_file.invalid_maxlinesize THEN
		write_output('Line Size Exceeds 32K');
		write_log('Line Size Exceeds 32K');
		raise_application_error(-20060, 'Line Size Exceeds 32K');
	WHEN utl_file.invalid_filename THEN
		write_output('Invalid File Name');
		write_log('Invalid File Name');
		raise_application_error(-20061, 'Invalid File Name');
	WHEN utl_file.access_denied THEN
		write_output('File Access Denied By');
		write_log('File Access Denied By');
		raise_application_error(-20062, 'File Access Denied By');
	WHEN OTHERS THEN
		IF (utl_file.is_open(file => v_file_handle)) THEN
			utl_file.fclose(file => v_file_handle);
		END IF;
		err_code := '2';
		err_msg  := SQLERRM;
		write_output(SQLERRM);
		raise_application_error(-20011, SQLCODE || ': ' || SQLERRM);
END interface_grn;

PROCEDURE interface_gl
(
	err_msg                  OUT VARCHAR2
   ,err_code                 OUT VARCHAR2
   ,p_file_path              VARCHAR2
   ,p_file_name_format       VARCHAR2
   ,p_file_name              VARCHAR2 DEFAULT NULL
   ,p_delimiter              VARCHAR2 DEFAULT '|'
   ,p_debug_flag             VARCHAR2 DEFAULT 'N'
   ,p_purge_days             NUMBER DEFAULT 45
   ,p_insert_interface_table VARCHAR2 DEFAULT 'Y'
) IS
	v_file_name   VARCHAR2(100);
	v_date        DATE := SYSDATE;
	v_file_handle utl_file.file_type;
	v_line_data   NVARCHAR2(32767);
	v_line_count  NUMBER := 0;
	
	v_rec_data          dtac_gl_interface_temp %ROWTYPE;
	v_insert_error_flag VARCHAR2(1) := 'N';
	
	v_request_id         NUMBER := fnd_profile.value('CONC_REQUEST_ID');
	v_creation_date      DATE := SYSDATE;
	v_debug_flag         VARCHAR2(10);
	v_delimiter          VARCHAR2(10);
	v_company_map_org_id VARCHAR2(500);
	
	v_login_id                 NUMBER := fnd_profile.value('LOGIN_ID');
	v_user_id                  NUMBER := fnd_profile.value('USER_ID');
	v_header_interface_id      NUMBER;
	v_group_interface_id       NUMBER;
	v_interface_transaction_id NUMBER;
	--v_receipt_group_interface_id number;
	v_return2receive_group_id NUMBER;
	v_return2vendor_group_id  NUMBER;
	v_created_by              NUMBER;
	v_employee_id             NUMBER;
	v_submit_request_id       NUMBER;
	
	v_insert_interface_table VARCHAR2(10) := 'Y';
	
	v_count_gl NUMBER := 0;
	
BEGIN
	err_code     := '2';
	v_delimiter  := p_delimiter;
	v_debug_flag := p_debug_flag;
	
	write_log(' ');
	write_log('+---------------------------------------------------------------------------+');
	write_log('Start Concurrent GL Interface from OCC ');
	write_log('Start Date: ' ||
			  to_char(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
	write_log(' ');
	write_log('Agruments');
	write_log('-----------');
	write_log('p_file_path =  ' || p_file_path);
	write_log('p_file_name_format =  ' || p_file_name_format);
	write_log('p_file_name =  ' || p_file_name);
	write_log('p_delimiter =  ' || p_delimiter);
	write_log('p_debug_flag =  ' || p_debug_flag);
	write_log('p_insert_interface_table =  ' ||
			  p_insert_interface_table);
	
	write_log('+----------------------------------------------------------------------------+');
	
	-- Start import data from text file
	v_insert_interface_table := nvl(p_insert_interface_table, 'Y');
	IF p_file_path IS NULL THEN
		RAISE utl_file.invalid_path;
	END IF;
	
	IF (p_file_name IS NOT NULL) THEN
		v_file_name := p_file_name;
	ELSE
		IF p_file_name_format IS NULL THEN
			RAISE utl_file.invalid_filename;
		END IF;
		v_file_name := REPLACE(p_file_name_format,
							   '$DATE$',
							   TRIM(to_char(v_date, 'yyyymmdd')));
	END IF;
	
	write_log(g_log_line);
	write_log('Import GL data from file ' ||
			  get_directory_path(g_gl_inbox) || '/' || v_file_name);
	write_log(' ');
	
	BEGIN
		DELETE FROM dtac_gl_interface_temp tmp
		WHERE tmp.interface_date <= (SYSDATE - nvl(p_purge_days, 45));
		COMMIT;
	EXCEPTION
		WHEN OTHERS THEN
			NULL;
	END;
	
	BEGIN
		--v_file_handle := utl_file.fopen(location => p_file_path, filename => v_file_name, open_mode => 'r');
		
		v_file_handle := utl_file.fopen_nchar(location  => p_file_path,
											  filename  => v_file_name,
											  open_mode => 'r');
		utl_file.fclose(v_file_handle);
	EXCEPTION
		WHEN OTHERS THEN
			write_log(' Error first read : ' || SQLERRM);
			write_log(' ===============');
			write_log('p_file_path = ' || p_file_path);
			write_log('v_file_name = ' || v_file_name);
			write_log(' ===============');
	END;
	--v_file_handle := utl_file.fopen(location => p_file_path, filename => v_file_name, open_mode => 'r');
	v_file_handle := utl_file.fopen_nchar(location  => p_file_path,
										  filename  => v_file_name,
										  open_mode => 'r');
	
	-- Read first line for header (ignore BOM byte)
	utl_file.get_line_nchar(file   => v_file_handle,
							buffer => v_line_data);
	v_line_data := '';
	
	SAVEPOINT before_insert_temp;
	LOOP
		BEGIN
			--utl_file.get_line(file => v_file_handle, buffer => v_line_data);
			utl_file.get_line_nchar(file   => v_file_handle,
									buffer => v_line_data);
			v_line_data := REPLACE(REPLACE(v_line_data, chr(10), ''),
								   chr(13),
								   '');
			IF (v_line_data = 'END') THEN
				EXIT;
			END IF;
			v_line_count := v_line_count + 1;
			
			v_rec_data := split_gl_data(p_line_buff  => v_line_data,
										p_delimiter  => v_delimiter,
										p_debug_flag => v_debug_flag);
			BEGIN
				v_rec_data.request_id     := v_request_id;
				v_rec_data.interface_date := SYSDATE;
				
				INSERT INTO dtac_gl_interface_temp
					(status,
					 set_of_books_id,
					 accounting_date,
					 currency_code,
					 date_created,
					 created_by,
					 actual_flag,
					 user_je_category_name,
					 user_je_source_name,
					 accounted_dr,
					 accounted_cr,
					 currency_conversion_date,
					 currency_conversion_rate,
					 entered_dr,
					 entered_cr,
					 je_line_num,
					 period_name,
					 reference1,
					 reference4,
					 reference5,
					 segment1,
					 segment11,
					 segment10,
					 segment12,
					 segment13,
					 segment14,
					 segment18,
					 segment19,
					 segment21,
					 reference21,
					 request_id,
					 interface_date,
					 validate_status,
					 error_msg)
				VALUES
					(v_rec_data.status,
					 v_rec_data.set_of_books_id,
					 v_rec_data.accounting_date,
					 v_rec_data.currency_code,
					 v_rec_data.date_created,
					 v_rec_data.created_by,
					 v_rec_data.actual_flag,
					 v_rec_data.user_je_category_name,
					 v_rec_data.user_je_source_name,
					 v_rec_data.accounted_dr,
					 v_rec_data.accounted_cr,
					 v_rec_data.currency_conversion_date,
					 v_rec_data.currency_conversion_rate,
					 v_rec_data.entered_dr,
					 v_rec_data.entered_cr,
					 v_rec_data.je_line_num,
					 v_rec_data.period_name,
					 v_rec_data.reference1,
					 v_rec_data.reference4,
					 v_rec_data.reference5,
					 v_rec_data.segment1,
					 v_rec_data.segment11,
					 v_rec_data.segment10,
					 v_rec_data.segment12,
					 v_rec_data.segment13,
					 v_rec_data.segment14,
					 v_rec_data.segment18,
					 v_rec_data.segment19,
					 v_rec_data.segment21,
					 v_rec_data.reference21,
					 v_rec_data.request_id,
					 v_rec_data.interface_date,
					 v_rec_data.validate_status,
					 v_rec_data.error_msg);
				
				write_log('Line#' || to_char(v_line_count, 90000) ||
						  ' [ OK] =>  ' || v_line_data);
			EXCEPTION
				WHEN OTHERS THEN
					v_insert_error_flag := 'Y';
					write_log('Line#' || to_char(v_line_count, 90000) ||
							  ' [ERR] =>  ' || v_line_data);
					write_output('Line#' ||
								 to_char(v_line_count, 90000) ||
								 ' [ERR] =>  ' || SQLERRM);
			END;
		EXCEPTION
			WHEN no_data_found THEN
				EXIT;
			WHEN OTHERS THEN
				v_insert_error_flag := 'Y';
				write_log('Read data from text file error :  ' ||
						  SQLERRM);
				EXIT;
		END;
	END LOOP;
	-- check open and close
	IF (utl_file.is_open(file => v_file_handle)) THEN
		utl_file.fclose(file => v_file_handle);
	END IF;
	
	IF (v_insert_error_flag = 'Y') THEN
		ROLLBACK TO SAVEPOINT before_insert_temp;
		write_output(' ');
		write_output(' ');
		write_output(g_log_line);
		
		write_log(' ');
		write_log('tac_discovery_interface_util.interface_gl @import_data exception when insert data. See output for error detail.');
		
		err_msg  := 'Error tac_discovery_interface_util.interface_gl @import_data : Some record error during insert to table. See output for detail.';
		err_code := '2';
		move_data_file(get_directory_path(g_gl_inbox) || '/' ||
					   v_file_name,
					   get_directory_path(g_gl_error));
		
		RETURN;
	ELSE
		COMMIT;
		move_data_file(get_directory_path(g_gl_inbox) || '/' ||
					   v_file_name,
					   get_directory_path(g_gl_history));
	END IF;
	-- End of import data from text file
	
	SAVEPOINT before_insert_interface;
	v_insert_error_flag := 'N';
	-- validate GL
	IF (validate_grn(v_request_id) IS NOT NULL) THEN
		write_log('Validate : Failed.');
	ELSE
		write_log('Validate : Pass.');
		IF (v_insert_interface_table = 'Y') THEN
			BEGIN
				INSERT INTO gl_interface
					(status,
					 set_of_books_id,
					 accounting_date,
					 currency_code,
					 date_created,
					 created_by,
					 actual_flag,
					 user_je_category_name,
					 user_je_source_name,
					 accounted_dr,
					 accounted_cr,
					 currency_conversion_date,
					 currency_conversion_rate,
					 entered_dr,
					 entered_cr,
					 je_line_num,
					 period_name,
					 reference1,
					 reference4,
					 reference5,
					 segment1,
					 segment11,
					 segment10,
					 segment12,
					 segment13,
					 segment14,
					 segment18,
					 segment19,
					 segment21,
					 reference21)
					SELECT status,
						   set_of_books_id,
						   accounting_date,
						   currency_code,
						   date_created,
						   created_by,
						   actual_flag,
						   user_je_category_name,
						   user_je_source_name,
						   accounted_dr,
						   accounted_cr,
						   currency_conversion_date,
						   currency_conversion_rate,
						   entered_dr,
						   entered_cr,
						   je_line_num,
						   period_name,
						   reference1,
						   reference4,
						   reference5,
						   segment1,
						   segment11,
						   segment10,
						   segment12,
						   segment13,
						   segment14,
						   segment18,
						   segment19,
						   segment21,
						   reference21
					FROM dtac_gl_interface_temp tmp
					WHERE tmp.request_id = v_request_id
					AND tmp.status = 'NEW';
				v_count_gl := SQL%ROWCOUNT;
				COMMIT;
			EXCEPTION
				WHEN OTHERS THEN
					v_insert_error_flag := 'Y';
					ROLLBACK TO SAVEPOINT before_insert_interface;
					write_output(' ');
					write_output(' ');
					write_output(SQLERRM);
					
					write_log(' ');
					write_log('tac_discovery_interface_util.interface_gl @insert interface exception when insert data to gl_interface table. See output for error detail.');
					
					err_msg  := 'Error tac_discovery_interface_util.interface_gl @insert interface : Some record during insert data to gl_interface table. See output for detail.';
					err_code := '2';
					RETURN;
			END;
		END IF;
	END IF;
	
	IF (v_count_gl = v_line_count OR v_insert_interface_table = 'N') THEN
		err_code := '0';
	ELSE
		err_code := '2';
		write_output(' ');
		write_output('ERROR : Total records that insert to gl_interface table are not equal (' ||
					 v_count_gl || '/' || v_line_count ||
					 ' with text file.');
		
		write_log(' ');
		write_log('ERROR : Total records that insert to gl_interface table are not equal (' ||
				  v_count_gl || '/' || v_line_count ||
				  ' with text file.');
	END IF;
	
EXCEPTION
	WHEN utl_file.invalid_path THEN
		write_output('Invalid File Location');
		write_log('Invalid File Location');
		raise_application_error(-20052, 'Invalid File Location');
	WHEN utl_file.invalid_operation THEN
		write_output('Invalid File Operation');
		write_log('Invalid Operation');
		raise_application_error(-20054, 'Invalid Operation');
	WHEN utl_file.read_error THEN
		write_output('Read File Error');
		write_log('Read Error');
		raise_application_error(-20055, 'Read Error');
	WHEN utl_file.invalid_maxlinesize THEN
		write_output('Line Size Exceeds 32K');
		write_log('Line Size Exceeds 32K');
		raise_application_error(-20060, 'Line Size Exceeds 32K');
	WHEN utl_file.invalid_filename THEN
		write_output('Invalid File Name');
		write_log('Invalid File Name');
		raise_application_error(-20061, 'Invalid File Name');
	WHEN utl_file.access_denied THEN
		write_output('File Access Denied By');
		write_log('File Access Denied By');
		raise_application_error(-20062, 'File Access Denied By');
	WHEN OTHERS THEN
		IF (utl_file.is_open(file => v_file_handle)) THEN
			utl_file.fclose(file => v_file_handle);
		END IF;
		err_code := '2';
		err_msg  := SQLERRM;
		write_output(SQLERRM);
		raise_application_error(-20011, SQLCODE || ': ' || SQLERRM);
END interface_gl;
*/
END tac_occ_product_itf_pkg;
/
