#!/usr/bin/ksh

##### Read Parameter #####
#SOURCE_FILE=ERP_INV_ALLOCATE_20180814_180000.DAT
#SOURCE_FILE=ERP_INV_ALLOCATE_20180814_180000.DAT
SOURCE_FILE=$1
DIR_SOURCE_PATH=$2
SOURCE_PATH=/MP/GERP_TEST/DTACONLINE/OUTPUT

# scp ALLOCATE/INBOX/ERP_INV_PRODUCT_20180724_161713.DAT fxthuser@pictor3:/opt/sftproot/SETTLEMENT/home/fxthuser/test/Tem
# ssh fxthuser@pictor3 '/opt/sftproot/SETTLEMENT/home/fxthuser/test/Temp/encrpt.sh /opt/sftproot/SETTLEMENT/home/fxthuser/test/Temp/ERP_INV_ALLOCATE_20180724_161713.DAT'
# scp fxthuser@pictor3:/opt/sftproot/SETTLEMENT/home/fxthuser/test/Temp/ERP_INV_ALLOCATE_20180724_161713.DAT.ENC .

SFTP_FILE_TO_SERVER() {
  DIR_PATH=$1
  (
  echo "
    mput ${SOURCE_PATH}/${DIR_PATH}/INBOX/*.* /discoocc_data_erp/OUTBOUND/${i}/INBOX
    quit
    ") | sftp discoocc@pictor13

  if [ $? -ne 0 ];then
    echo "CRITICAL:Failed to sftp ${SFTP_SOURCE} to pictor13."
    return 1
  fi

  return 0
}

SCP_FILE_TO_SERVER() {
  SCP=/usr/bin/scp

  SOURCE=$1
  DEST=$2

  echo "Performing scp of ${SOURCE} to ${DEST}"
  ${SCP} -p -q ${SOURCE} ${DEST}

  if [ $? -ne 0 ];then
    echo "CRITICAL:Failed to scp ${SOURCE} to dest ${DEST}."
    return 1
  fi

  return 0

}

SSH_SERVER() {
  REMOTE_SERV=fxthuser@pictor3
  SSH=/usr/bin/ssh
  SERV_COMMAND=/opt/sftproot/SETTLEMENT/home/fxthuser/Encrypt/encrypt_all.sh

  echo "Performing ssh ${SERV_COMMAND} on ${REMOTE_SERV}"
  ${SSH} -q ${REMOTE_SERV} 'nohup /opt/sftproot/SETTLEMENT/home/fxthuser/Encrypt/encrypt_all.sh ERPPROD </dev/null &'

  if [ $? -ne 0 ];then
    echo "CRITICAL:Failed to ssh ${SERV_COMMAND} on ${REMOTE_SERV}."
    return 1
  fi

  return 0

}


##### Main Program #####

for i in PRICELIST DISCOUNTCODE;do
  SCP_FILE_TO_SERVER ${SOURCE_PATH}/${i}/INBOX/*.DAT fxthuser@pictor3:/opt/sftproot/SETTLEMENT/home/fxthuser/Encrypt
  if [ $? -ne 0 ];then
     exit 1
  fi

  SSH_SERVER
  if [ $? -ne 0 ];then
     ### echo
     exit 1
  fi

  SCP_FILE_TO_SERVER fxthuser@pictor3:/opt/sftproot/SETTLEMENT/home/fxthuser/Encrypt/*.ENC ${SOURCE_PATH}/${i}/INBOX
  if [ $? -ne 0 ];then
     ### echo
     exit 1
  fi

  mv ${SOURCE_PATH}/${i}/INBOX/*.DAT ${SOURCE_PATH}/${i}/HISTORY

  SFTP_FILE_TO_SERVER ${i}
  if [ $? -ne 0 ];then
     ### echo
     exit 1
  fi

  mv ${SOURCE_PATH}/${i}/INBOX/*.* ${SOURCE_PATH}/${i}/HISTORY

done

exit 0
