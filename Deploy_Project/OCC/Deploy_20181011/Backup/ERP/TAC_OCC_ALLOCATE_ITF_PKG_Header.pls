create or replace PACKAGE tac_occ_allocate_itf_pkg IS

-- Original Program : TAC_IC_ITFC_API.IC116_ITFC
-- Author  : TM
-- Created : 20-Jun-2018
-- Purpose : Interface ERP data to OCC

------------------------------------------------------------------------------------------
FUNCTION char2date
(i_text		IN	VARCHAR2
,i_format	IN	VARCHAR2)
RETURN DATE;
------------------------------------------------------------------------------------------
PROCEDURE alloc_dealloc
(err_msg			OUT	VARCHAR2
,err_code			OUT	VARCHAR2
,i_interface_type		VARCHAR2
,i_start_date			VARCHAR2
,i_end_date				VARCHAR2
,i_output_dirobj_name	VARCHAR2
,i_file_name_format		VARCHAR2
,i_encrypt_path			VARCHAR2
,i_encrypt_file_suffix	VARCHAR2
,i_encrypt_passwd_key	VARCHAR2
,i_sftp_host			VARCHAR2
,i_sftp_user			VARCHAR2
,i_sftp_remote_path		VARCHAR2
,i_file_path_error		VARCHAR2
,i_file_path_history	VARCHAR2
,i_separator			VARCHAR2	:=	'|'
,i_to_email				VARCHAR2	:=	'Nawatsapon.Thanaratchaikun@dtac.co.th');
--,i_to_email				VARCHAR2	:=	'ERPOperationSupport@dtac.co.th');
------------------------------------------------------------------------------------------
END tac_occ_allocate_itf_pkg;