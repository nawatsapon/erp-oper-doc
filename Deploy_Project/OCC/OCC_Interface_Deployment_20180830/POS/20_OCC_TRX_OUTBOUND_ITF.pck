CREATE OR REPLACE PACKAGE OCC_TRX_OUTBOUND_ITF
AUTHID CURRENT_USER
IS
/*
Date		By				Description
----------	--------------	---------------------------------------------
22/06/2018	TM				Created
*/

---**************************************************--

--@@@ AUTO RUNNING PROCESS @@@ --
PROCEDURE main_price_list
(p_ou_code			IN	pb_company.ou_code%TYPE
,p_subinv_code		IN	pb_subinv.subinv_code%TYPE
,p_cutoff_eff_date	IN	VARCHAR2
,p_cutoff_back_days	IN	NUMBER
,p_mode				IN	VARCHAR2
,p_log_back_days	IN	NUMBER
,p_output_path		IN	VARCHAR2
,p_field_separator	IN	VARCHAR2
,p_mail_to			IN	VARCHAR2	-- Send email to ?? upon completion
,N_NO				IN	NUMBER
,N_ID				IN	NUMBER);

PROCEDURE main_discount
(p_ou_code			IN	pb_company.ou_code%TYPE
,p_subinv_code		IN	pb_subinv.subinv_code%TYPE
,p_cutoff_eff_date	IN	VARCHAR2
,p_cutoff_back_days	IN	NUMBER
,p_mode				IN	VARCHAR2
,p_log_back_days	IN	NUMBER
,p_output_path		IN	VARCHAR2
,p_field_separator	IN	VARCHAR2
,p_mail_to			IN	VARCHAR2	-- Send email to ?? upon completion
,N_NO				IN	NUMBER
,N_ID				IN	NUMBER);

END OCC_TRX_OUTBOUND_ITF;
/
CREATE OR REPLACE PACKAGE BODY OCC_TRX_OUTBOUND_ITF
IS
/*
Date		By				Description
----------	--------------  ---------------------------------------------
22/06/2018	TM				Created
*/

C_JOB_NAME			CONSTANT VARCHAR2(1000) :=  'Interface to OCC-';
C_DFLT_START_DATE	CONSTANT DATE	:=	TO_DATE('01/01/0001','dd/mm/yyyy');
C_DFLT_END_DATE		CONSTANT DATE	:=	TO_DATE('31/12/9999','dd/mm/yyyy');
GR_LOG				tac_occ_outb_inf_log%ROWTYPE;
G_CNT_ROWS_EXPORT	NUMBER	:=	0;
G_CNT_ROW_ERROR		NUMBER	:=	0;
G_INF_SEQ			NUMBER	:=	1;
G_PROCESSID			PB_PARAMETER_SETUP.PARAMETER_VALUE%TYPE;

G_ENTITY		VARCHAR2(100);
G_OU_CODE		su_organize.ou_code%TYPE;
G_SUBINV_CODE	pb_subinv.subinv_code%TYPE;
G_MODE			VARCHAR2(10);
G_CUTOFF_BACK_DAYS	NUMBER;
G_CUTOFF_DATE		DATE;
G_LOG_BACK_DAYS	NUMBER;
G_N_NO			NUMBER;
G_N_ID			NUMBER;
G_JOB_BROKEN	VARCHAR2(10);

e_abnormal_end	EXCEPTION;

G_DIRECTORY		VARCHAR2(4000);
G_RESULT_STATUS	VARCHAR2(4000);
G_FILENAME		VARCHAR2(4000);

-- Start Forward Declarations
PROCEDURE write_log
(p_write	tac_occ_outb_inf_log%ROWTYPE);

PROCEDURE remove_process
(i_upd  VARCHAR2
,i_pgm  VARCHAR2);
-- End Forward Declarations

------------------------------------------
PROCEDURE validate
(i_upd	  IN  VARCHAR2
,i_pgm	  IN  VARCHAR2
,P_RESULT   OUT VARCHAR2)
IS
	CNT   NUMBER := 0;
	b_error	BOOLEAN	:=	FALSE;
BEGIN
	P_RESULT := 'COMPLETE';
	
	CNT := 0;

	-->> Initial log
	GR_LOG.PROCESS_ID	:=  G_PROCESSID;
	GR_LOG.INF_UPD_BY	:=  i_upd;
	GR_LOG.INF_UPD_PGM	:=  i_pgm;

	-->> Lock Transaction Pending.
	GR_LOG.STEP				:=  2;
	GR_LOG.STEP_DESC		:=  'Validate data';
	GR_LOG.PROCESS_FLAG		:=  'Prepare Data';
	GR_LOG.INF_LOCK_FLAG	:=  'Lock';

-- Validation

-- End Validation
	IF P_RESULT NOT LIKE 'Complete%' THEN
		REMOVE_PROCESS('AUTO', 'BACK-JOB');
		G_RESULT_STATUS :=  'FAILED';
	END IF;
	GR_LOG	:=	NULL;

END validate;
------------------------------------------
PROCEDURE start_process
(i_upd	  IN  VARCHAR2
,i_pgm	  IN  VARCHAR2)
IS
BEGIN
/*	-->> Initial log
	GR_LOG.PROCESS_ID	:=  G_PROCESSID;
	GR_LOG.INF_UPD_BY	:=  i_upd;
	GR_LOG.INF_UPD_PGM	:=  i_pgm;
	
	-->> Lock Transaction Pending.
	GR_LOG.STEP				:=  1;
	GR_LOG.STEP_DESC		:=  'Lock row';
	GR_LOG.PROCESS_FLAG		:=  'Prepare Data';
	GR_LOG.INF_LOCK_FLAG	:=  'Lock';*/
	
	NULL;	-- No Operation
END start_process;
------------------------------------------
PROCEDURE ending_process
(i_upd	  IN  VARCHAR2
,i_pgm	  IN  VARCHAR2)
IS
BEGIN
	-->> Initial log
/*	GR_LOG	:=	NULL;
	GR_LOG.ENTITY		:=	G_ENTITY;
	GR_LOG.PROCESS_ID	:=  G_PROCESSID;
	GR_LOG.INF_UPD_BY	:=  i_upd;
	GR_LOG.INF_UPD_PGM	:=  i_pgm;
	
	-->> Unlock Transaction Pending.
	GR_LOG.STEP				:=  3;
	GR_LOG.STEP_DESC		:=  'Unlock row';
	GR_LOG.PROCESS_FLAG		:=  'Prepare Data';
	GR_LOG.INF_LOCK_FLAG	:=  'Unlock';
*/	
	NULL;	-- No operation
END ending_process;
------------------------------------------
PROCEDURE remove_process
(i_upd	  IN  VARCHAR2
,i_pgm	  IN  VARCHAR2)
IS
	v_found	 BOOLEAN :=  FALSE;
BEGIN
/*	-->> Initial log
	GR_LOG	:=	NULL;
	GR_LOG.ENTITY			:=	G_ENTITY;
	GR_LOG.PROCESS_ID		:=  G_PROCESSID;
	GR_LOG.INF_UPD_BY		:=  i_upd;
	GR_LOG.INF_UPD_PGM		:=  i_pgm;
	
	-->> Unlock Transaction Pending.
	GR_LOG.STEP				:=  9;
	GR_LOG.STEP_DESC		:=  'Remove process';
	GR_LOG.PROCESS_FLAG		:=  'Warning : Map Error';
	GR_LOG.INF_LOCK_FLAG	:=  'Unlock';
*/
	NULL;	-- No operation
END remove_process;
------------------------------------------
PROCEDURE clear_log
(p_entity	IN	VARCHAR2)
IS
BEGIN
	DELETE  tac_occ_outb_inf_log
	WHERE   inf_upd_date < TRUNC(SYSDATE) - G_LOG_BACK_DAYS
	AND		entity	=	p_entity;

	IF p_entity = 'PRICELIST' THEN
		DELETE  tac_occ_pos_price_list_tmp
		WHERE   process_date < TRUNC(SYSDATE) - G_LOG_BACK_DAYS;
	ELSIF p_entity = 'DISCOUNT' THEN
		DELETE	tac_occ_pos_discount_tmp
		WHERE   process_date < TRUNC(SYSDATE) - G_LOG_BACK_DAYS;
	END IF;
END clear_log;
------------------------------------------
PROCEDURE write_log
(p_write	tac_occ_outb_inf_log%ROWTYPE) IS
BEGIN
	BEGIN
		INSERT INTO tac_occ_outb_inf_log
			(entity
			,process_id
			,process_date
			,step
			,step_desc
			,seq
			,log_code
			,log_desc
			,process_flag
			,inf_lock_flag
			,inf_upd_by
			,inf_upd_date
			,inf_upd_pgm)
		VALUES
			(p_write.entity
			,p_write.process_id
			,p_write.process_date
			,p_write.step
			,p_write.step_desc
			,p_write.seq
			,p_write.log_code
			,p_write.log_desc
			,p_write.process_flag
			,p_write.inf_lock_flag
			,p_write.inf_upd_by
			,p_write.inf_upd_date
			,p_write.inf_upd_pgm);
	END;
END write_log;
------------------------------------------
FUNCTION clear_spchar
(i_input		IN	VARCHAR2
,i_separator	IN	VARCHAR2)
RETURN VARCHAR2
IS
BEGIN
	RETURN REPLACE(REPLACE(REPLACE(LTRIM(i_input,''''),CHR(10)),CHR(13)),i_separator);
END clear_spchar;
------------------------------------------
PROCEDURE prepare_price_list
(i_upd  IN  VARCHAR2
,i_pgm  IN  VARCHAR2)
IS

BEGIN
	G_CNT_ROWS_EXPORT	:=  0;
	
	-->> Initial log
	GR_LOG	:=	NULL;
	GR_LOG.ENTITY			:=	G_ENTITY;
	GR_LOG.PROCESS_ID		:=  G_PROCESSID;
	GR_LOG.INF_UPD_BY		:=  i_upd;
	GR_LOG.INF_UPD_PGM		:=  i_pgm;

	-->> Get data to interface
	GR_LOG.STEP				:=  1;
	GR_LOG.STEP_DESC		:=  'Get Data';
	GR_LOG.PROCESS_FLAG		:=  'Prepare Data';
	GR_LOG.INF_LOCK_FLAG	:=  'Unlock';

-- 1. Get pricelist with OU
	DELETE	tac_occ_pos_price_list_tmp
	WHERE	process_id	=	G_PROCESSID + 0.1;
	
	INSERT INTO tac_occ_pos_price_list_tmp
	SELECT	G_PROCESSID + 0.1		process_id
			,o.ou_code				ou_code
			,p.pricelist_code		pricelist_code
			,NULL					item_code
			,NULL					std_price
			,p.start_date			start_date
			,p.end_date				end_date
			,NVL(p.shop_cond,'F')	shop_cond
			,NVL(p.cust_cond,'F')	cust_cond
			,NULL					subinv_code
			,NULL					cat_code
			,NULL					process_date
	FROM	sa_pricelist p, su_organize o
	WHERE	o.ou_code LIKE G_OU_CODE
	AND		p.status	=	'A'	-- Active
	AND		G_CUTOFF_DATE BETWEEN NVL(p.start_date,C_DFLT_START_DATE) AND NVL(p.end_date,C_DFLT_END_DATE);

	-- Write log
	GR_LOG.PROCESS_DATE	:=  SYSDATE;
	GR_LOG.SEQ			:=  G_INF_SEQ;
	GR_LOG.LOG_CODE		:=  G_PROCESSID || ' 1. Get Pricelist with OU';
	GR_LOG.LOG_DESC		:=  'Get pricelist header, rows = ' || SQL%ROWCOUNT;
	GR_LOG.INF_UPD_DATE	:=  SYSDATE;
	WRITE_LOG(GR_LOG);
	G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;

-- 2. Get pricelist with shop (subinv)
	DELETE	tac_occ_pos_price_list_tmp
	WHERE	process_id	=	G_PROCESSID + 0.2;
	
	INSERT INTO tac_occ_pos_price_list_tmp
	SELECT	G_PROCESSID + 0.2	process_id
			,p.ou_code			ou_code
			,p.pricelist_code	pricelist_code
			,p.item_code		item_code
			,p.std_price		std_price
			,p.start_date		start_date
			,p.end_date			end_date
			,p.shop_cond		shop_cond
			,p.cust_cond		cust_cond
			,p.subinv_code		subinv_code
			,p.cat_code			cat_code
			,NULL				process_date
	FROM	tac_occ_pos_price_list_tmp p
	WHERE	p.process_id	=	G_PROCESSID + 0.1
	AND		p.shop_cond		=	'F';

	-- Write log
	GR_LOG.PROCESS_DATE	:=  SYSDATE;
	GR_LOG.SEQ			:=  G_INF_SEQ;
	GR_LOG.LOG_CODE		:=  G_PROCESSID || ' 2.1 Get Pricelist with Subinv (case ALL)';
	GR_LOG.LOG_DESC		:=  'Get pricelist header, rows = ' || SQL%ROWCOUNT;
	GR_LOG.INF_UPD_DATE	:=  SYSDATE;
	WRITE_LOG(GR_LOG);
	G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;

	INSERT INTO tac_occ_pos_price_list_tmp
	SELECT	G_PROCESSID + 0.2	process_id
			,p.ou_code			ou_code
			,p.pricelist_code	pricelist_code
			,p.item_code		item_code
			,p.std_price		std_price
			,GREATEST(p.start_date,s.start_date)	start_date
			,LEAST(p.end_date,s.end_date)			end_date
			,p.shop_cond		shop_cond
			,p.cust_cond		cust_cond
			,s.subinv_code		subinv_code
			,p.cat_code			cat_code
			,NULL				process_date
	FROM	tac_occ_pos_price_list_tmp p, sa_pricelist_subinv s
	WHERE	p.process_id		=	G_PROCESSID + 0.1
	AND		p.pricelist_code	=	s.pricelist_code
	AND		p.ou_code			=	s.ou_code
	AND		p.shop_cond			IN	('A','N')	-- Allow, Not Allow
	AND		s.subinv_code LIKE G_SUBINV_CODE
	AND		G_CUTOFF_DATE BETWEEN NVL(s.start_date,C_DFLT_START_DATE) AND NVL(s.end_date,C_DFLT_END_DATE);

	-- Write log
	GR_LOG.PROCESS_DATE	:=  SYSDATE;
	GR_LOG.SEQ			:=  G_INF_SEQ;
	GR_LOG.LOG_CODE		:=  G_PROCESSID || ' 2.2 Get Pricelist with Subinv (case Allow / Not Allow)';
	GR_LOG.LOG_DESC		:=  'Get pricelist subinv, rows = ' || SQL%ROWCOUNT;
	GR_LOG.INF_UPD_DATE	:=  SYSDATE;
	WRITE_LOG(GR_LOG);
	G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;

	DELETE	tac_occ_pos_price_list_tmp
	WHERE	process_id	=	G_PROCESSID + 0.1;

-- 3. Get pricelist with category (cust group)
	DELETE	tac_occ_pos_price_list_tmp
	WHERE	process_id	=	G_PROCESSID + 0.3;
	
	INSERT INTO tac_occ_pos_price_list_tmp
	SELECT	G_PROCESSID + 0.3	process_id
			,p.ou_code			ou_code
			,p.pricelist_code	pricelist_code
			,p.item_code		item_code
			,p.std_price		std_price
			,p.start_date		start_date
			,p.end_date			end_date
			,p.shop_cond		shop_cond
			,p.cust_cond		cust_cond
			,p.subinv_code		subinv_code
			,p.cat_code			cat_code
			,NULL				process_date
	FROM	tac_occ_pos_price_list_tmp p
	WHERE	p.process_id	=	G_PROCESSID + 0.2
	AND		p.cust_cond		=	'F'
	AND		G_CUTOFF_DATE BETWEEN NVL(p.start_date,C_DFLT_START_DATE) AND NVL(p.end_date,C_DFLT_END_DATE);

	-- Write log
	GR_LOG.PROCESS_DATE	:=  SYSDATE;
	GR_LOG.SEQ			:=  G_INF_SEQ;
	GR_LOG.LOG_CODE		:=  G_PROCESSID || ' 3.1 Get Pricelist with Cust Category (case ALL)';
	GR_LOG.LOG_DESC		:=  'Get pricelist header, rows = ' || SQL%ROWCOUNT;
	GR_LOG.INF_UPD_DATE	:=  SYSDATE;
	WRITE_LOG(GR_LOG);
	G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;

	INSERT INTO tac_occ_pos_price_list_tmp
	SELECT	G_PROCESSID + 0.3	process_id
			,p.ou_code			ou_code
			,p.pricelist_code	pricelist_code
			,p.item_code		item_code
			,p.std_price		std_price
			,p.start_date		start_date
			,p.end_date			end_date
			,p.shop_cond		shop_cond
			,p.cust_cond		cust_cond
			,p.subinv_code		subinv_code
			,c.cat_code			cat_code
			,NULL				process_date
	FROM	tac_occ_pos_price_list_tmp p, sa_pricelist_cust_group c
	WHERE	p.process_id		=	G_PROCESSID + 0.2
	AND		p.pricelist_code	=	c.pricelist_code
	AND		p.ou_code			=	c.ou_code
	AND		p.cust_cond			IN	('A','N')	-- Allow, Not Allow
	AND		G_CUTOFF_DATE BETWEEN NVL(p.start_date,C_DFLT_START_DATE) AND NVL(p.end_date,C_DFLT_END_DATE);

	-- Write log
	GR_LOG.PROCESS_DATE	:=  SYSDATE;
	GR_LOG.SEQ			:=  G_INF_SEQ;
	GR_LOG.LOG_CODE		:=  G_PROCESSID || ' 3.2 Get Pricelist with Cust Category (case Allow / Not Allow)';
	GR_LOG.LOG_DESC		:=  'Get pricelist cust category, rows = ' || SQL%ROWCOUNT;
	GR_LOG.INF_UPD_DATE	:=  SYSDATE;
	WRITE_LOG(GR_LOG);
	G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;

	DELETE	tac_occ_pos_price_list_tmp
	WHERE	process_id	=	G_PROCESSID + 0.2;

-- 4. Get pricelist lines
	DELETE	tac_occ_pos_price_list_tmp
	WHERE	process_id	=	G_PROCESSID + 0.4;
	
	INSERT INTO tac_occ_pos_price_list_tmp
	SELECT	G_PROCESSID + 0.4	process_id
			,p.ou_code			ou_code
			,p.pricelist_code	pricelist_code
			,i.item_code		item_code
			,i.std_price		std_price
			,GREATEST(p.start_date,i.start_date)	start_date
			,LEAST(p.end_date,i.end_date)			end_date
			,p.shop_cond		shop_cond
			,p.cust_cond		cust_cond
			,p.subinv_code		subinv_code
			,p.cat_code			cat_code
			,NULL				process_date
	FROM	tac_occ_pos_price_list_tmp p, sa_price_item i
	WHERE	p.process_id		=	G_PROCESSID + 0.3
	AND		p.pricelist_code	=	i.pricelist_code
	AND		p.ou_code			=	i.ou_code
	AND		G_CUTOFF_DATE BETWEEN NVL(GREATEST(p.start_date,i.start_date),C_DFLT_START_DATE) AND NVL(LEAST(p.end_date,i.end_date),C_DFLT_END_DATE);

	-- Write log
	GR_LOG.PROCESS_DATE	:=  SYSDATE;
	GR_LOG.SEQ			:=  G_INF_SEQ;
	GR_LOG.LOG_CODE		:=  G_PROCESSID || ' 4. Get Pricelist lines';
	GR_LOG.LOG_DESC		:=  'Get pricelist lines, rows = ' || SQL%ROWCOUNT;
	GR_LOG.INF_UPD_DATE	:=  SYSDATE;
	WRITE_LOG(GR_LOG);
	G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;

	DELETE	tac_occ_pos_price_list_tmp
	WHERE	process_id	=	G_PROCESSID + 0.3;

-- Compare to send data
	DELETE	tac_occ_pos_price_list_tmp
	WHERE	process_id	=	G_PROCESSID;

	IF G_MODE = 'I' THEN
		INSERT INTO tac_occ_pos_price_list_tmp
		SELECT	G_PROCESSID			process_id
				,p.ou_code			ou_code
				,p.pricelist_code	pricelist_code
				,p.item_code		item_code
				,p.std_price		std_price
				,p.start_date		start_date
				,p.end_date			end_date
				,p.shop_cond		shop_cond
				,p.cust_cond		cust_cond
				,p.subinv_code		subinv_code
				,p.cat_code			cat_code
				,SYSDATE			process_date
		FROM	tac_occ_pos_price_list_tmp p
		WHERE	p.pricelist_code IN	(SELECT DISTINCT pricelist_code
									FROM	((SELECT ou_code
													,pricelist_code
													,item_code
													,NVL(std_price,0)
													,NVL(start_date,C_DFLT_START_DATE)
													,NVL(end_date,C_DFLT_END_DATE)
													,NVL(shop_cond,'F')
													,NVL(cust_cond,'F')
													,NVL(subinv_code,' ')
													,NVL(cat_code,' ')
											FROM	tac_occ_pos_price_list_tmp
											WHERE	process_id	=	G_PROCESSID + 0.4
											MINUS
											SELECT	ou_code
													,pricelist_code
													,item_code
													,NVL(std_price,0)
													,NVL(start_date,C_DFLT_START_DATE)
													,NVL(end_date,C_DFLT_END_DATE)
													,NVL(shop_cond,'F')
													,NVL(cust_cond,'F')
													,NVL(subinv_code,' ')
													,NVL(cat_code,' ')
											FROM	tac_occ_pos_price_list_sent s
											WHERE	s.ou_code LIKE G_OU_CODE
											AND		s.subinv_code LIKE G_SUBINV_CODE
											AND		s.obsolete_record	=	'N')
											/*UNION
											(SELECT	ou_code
													,pricelist_code
													,item_code
													,NVL(std_price,0)
													,NVL(start_date,C_DFLT_START_DATE)
													,NVL(end_date,C_DFLT_END_DATE)
													,NVL(shop_cond,'F')
													,NVL(cust_cond,'F')
													,NVL(subinv_code,' ')
													,NVL(cat_code,' ')
											FROM	tac_occ_pos_price_list_sent s
											WHERE	s.ou_code LIKE G_OU_CODE
											AND		s.subinv_code LIKE G_SUBINV_CODE
											AND		s.obsolete_record	=	'N'
											MINUS
											SELECT	ou_code
													,pricelist_code
													,item_code
													,NVL(std_price,0)
													,NVL(start_date,C_DFLT_START_DATE)
													,NVL(end_date,C_DFLT_END_DATE)
													,NVL(shop_cond,'F')
													,NVL(cust_cond,'F')
													,NVL(subinv_code,' ')
													,NVL(cat_code,' ')
											FROM	tac_occ_pos_price_list_tmp
											WHERE	process_id	=	G_PROCESSID + 0.4)*/
											)
									);

		G_CNT_ROWS_EXPORT	:=	SQL%ROWCOUNT;

		-- Write log
		GR_LOG.PROCESS_DATE	:=  SYSDATE;
		GR_LOG.SEQ			:=  G_INF_SEQ;
		GR_LOG.LOG_CODE		:=  G_PROCESSID || ' 5. Compare to data sent (for Incremental mode)';
		GR_LOG.LOG_DESC		:=  'Get pricelist lines to send, rows = ' || G_CNT_ROWS_EXPORT;
		GR_LOG.INF_UPD_DATE	:=  SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;

		DELETE	tac_occ_pos_price_list_tmp
		WHERE	process_id	=	G_PROCESSID + 0.4;
	ELSE
		UPDATE	tac_occ_pos_price_list_tmp
		SET		process_id		=	G_PROCESSID
				,process_date	=	SYSDATE
		WHERE	process_id		=	G_PROCESSID + 0.4;

		G_CNT_ROWS_EXPORT	:=	SQL%ROWCOUNT;
	END IF;

	IF NVL(G_CNT_ROWS_EXPORT, 0) = 0 THEN
		GR_LOG.PROCESS_DATE	:=  SYSDATE;
		GR_LOG.SEQ			:=  G_INF_SEQ;
		GR_LOG.LOG_CODE		:=  G_PROCESSID || '6. SQL%NOTFOUND';
		GR_LOG.LOG_DESC		:=  'No data to export';
		GR_LOG.INF_UPD_DATE	:=  SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;
	END IF;
	COMMIT;
	
-- Update TAC_OCC_POS_PRICE_LIST_SENT
	UPDATE	tac_occ_pos_price_list_sent s
	SET		s.obsolete_record		=	'Y'
			,s.process_id_4obsolete	=	G_PROCESSID
	WHERE	obsolete_record		=	'N'
	AND		(ou_code, pricelist_code, item_code, NVL(subinv_code,' '), NVL(cat_code,' ')) IN
			(SELECT DISTINCT ou_code, pricelist_code, item_code, NVL(subinv_code,' '), NVL(cat_code,' ')
			FROM	(SELECT	ou_code
							,pricelist_code
							,item_code
							,NVL(std_price,0)
							,NVL(start_date,C_DFLT_START_DATE)
							,NVL(end_date,C_DFLT_END_DATE)
							,NVL(shop_cond,'F')
							,NVL(cust_cond,'F')
							,NVL(subinv_code,' ')
							,NVL(cat_code,' ')
					FROM	tac_occ_pos_price_list_sent s
					WHERE	s.ou_code LIKE G_OU_CODE
					AND		s.subinv_code LIKE G_SUBINV_CODE
					AND		s.obsolete_record	=	'N'
					MINUS
					SELECT	ou_code
							,pricelist_code
							,item_code
							,NVL(std_price,0)
							,NVL(start_date,C_DFLT_START_DATE)
							,NVL(end_date,C_DFLT_END_DATE)
							,NVL(shop_cond,'F')
							,NVL(cust_cond,'F')
							,NVL(subinv_code,' ')
							,NVL(cat_code,' ')
					FROM	tac_occ_pos_price_list_tmp
					WHERE	process_id	=	G_PROCESSID)
			);

	-- Write log
	GR_LOG.PROCESS_DATE	:=  SYSDATE;
	GR_LOG.SEQ			:=  G_INF_SEQ;
	GR_LOG.LOG_CODE		:=  G_PROCESSID || ' 7.1 Mark obsolete';
	GR_LOG.LOG_DESC		:=  'Mark obsolete records, rows = ' || SQL%ROWCOUNT;
	GR_LOG.INF_UPD_DATE	:=  SYSDATE;
	WRITE_LOG(GR_LOG);
	G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;

	INSERT INTO tac_occ_pos_price_list_sent
		(ou_code
		,pricelist_code
		,item_code
		,std_price
		,start_date
		,end_date
		,shop_cond
		,cust_cond
		,subinv_code
		,cat_code
		,process_id_first_sent
		,obsolete_record)
	SELECT	ou_code
			,pricelist_code
			,item_code
			,std_price
			,start_date
			,end_date
			,NVL(shop_cond,'F')
			,NVL(cust_cond,'F')
			,subinv_code
			,cat_code
			,G_PROCESSID
			,'N'	-- Not obsolete
	FROM	tac_occ_pos_price_list_tmp
	WHERE	(ou_code, pricelist_code, item_code, NVL(subinv_code,' '), NVL(cat_code,' ')) IN
			(SELECT DISTINCT ou_code, pricelist_code, item_code, NVL(subinv_code,' '), NVL(cat_code,' ')
			FROM	(SELECT	ou_code
							,pricelist_code
							,item_code
							,NVL(std_price,0)
							,NVL(start_date,C_DFLT_START_DATE)
							,NVL(end_date,C_DFLT_END_DATE)
							,NVL(shop_cond,'F')
							,NVL(cust_cond,'F')
							,NVL(subinv_code,' ')
							,NVL(cat_code,' ')
					FROM	tac_occ_pos_price_list_tmp
					WHERE	process_id	=	G_PROCESSID
					MINUS
					SELECT	ou_code
							,pricelist_code
							,item_code
							,NVL(std_price,0)
							,NVL(start_date,C_DFLT_START_DATE)
							,NVL(end_date,C_DFLT_END_DATE)
							,NVL(shop_cond,'F')
							,NVL(cust_cond,'F')
							,NVL(subinv_code,' ')
							,NVL(cat_code,' ')
					FROM	tac_occ_pos_price_list_sent s
					WHERE	s.ou_code LIKE G_OU_CODE
					AND		s.subinv_code LIKE G_SUBINV_CODE
					AND		s.obsolete_record	=	'N')
			)
	;

	-- Write log
	GR_LOG.PROCESS_DATE	:=  SYSDATE;
	GR_LOG.SEQ			:=  G_INF_SEQ;
	GR_LOG.LOG_CODE		:=  G_PROCESSID || ' 7.2 Insert sent records';
	GR_LOG.LOG_DESC		:=  'Insert sent records, rows = ' || SQL%ROWCOUNT;
	GR_LOG.INF_UPD_DATE	:=  SYSDATE;
	WRITE_LOG(GR_LOG);
	G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;

END prepare_price_list;
------------------------------------------
PROCEDURE prepare_discount
(i_upd  IN  VARCHAR2
,i_pgm  IN  VARCHAR2)
IS

BEGIN
	G_CNT_ROWS_EXPORT	:=  0;
	
	-->> Initial log
	GR_LOG	:=	NULL;
	GR_LOG.ENTITY			:=	G_ENTITY;
	GR_LOG.PROCESS_ID		:=  G_PROCESSID;
	GR_LOG.INF_UPD_BY		:=  i_upd;
	GR_LOG.INF_UPD_PGM		:=  i_pgm;

	-->> Get data to interface
	GR_LOG.STEP				:=  1;
	GR_LOG.STEP_DESC		:=  'Get Data';
	GR_LOG.PROCESS_FLAG		:=  'Prepare Data';
	GR_LOG.INF_LOCK_FLAG	:=  'Unlock';

-- 1. Get discount with OU
	DELETE	tac_occ_pos_discount_tmp
	WHERE	process_id	=	G_PROCESSID + 0.1;
	
	INSERT INTO tac_occ_pos_discount_tmp
	SELECT	G_PROCESSID + 0.1		process_id
			,p.discount_code		discount_code
			,p.discount_desc		discount_desc
			,p.discount_level		discount_level
			,p.disc_start			disc_start
			,p.disc_end				disc_end
			,p.disc_perc			disc_perc
			,p.disc_amt				disc_amt
			,NVL(p.shop_cond,'F')	shop_cond
			,NVL(p.cust_cond,'F')	cust_cond
			,p.status				status
			,NULL					subinv_code
			,NULL					cat_code
			,NULL					process_date
	FROM	sa_discount p
	WHERE	p.status	=	'A'	-- Active
	AND		G_CUTOFF_DATE BETWEEN NVL(p.disc_start,C_DFLT_START_DATE) AND NVL(p.disc_end,C_DFLT_END_DATE);

	-- Write log
	GR_LOG.PROCESS_DATE	:=  SYSDATE;
	GR_LOG.SEQ			:=  G_INF_SEQ;
	GR_LOG.LOG_CODE		:=  G_PROCESSID || ' 1. Get Discount header';
	GR_LOG.LOG_DESC		:=  'Get discount header, rows = ' || SQL%ROWCOUNT;
	GR_LOG.INF_UPD_DATE	:=  SYSDATE;
	WRITE_LOG(GR_LOG);
	G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;

-- 2. Get discount with shop (subinv)
	DELETE	tac_occ_pos_discount_tmp
	WHERE	process_id	=	G_PROCESSID + 0.2;
	
	INSERT INTO tac_occ_pos_discount_tmp
	SELECT	G_PROCESSID + 0.2	process_id
			,p.discount_code	discount_code
			,p.discount_desc	discount_desc
			,p.discount_level	discount_level
			,p.disc_start		disc_start
			,p.disc_end			disc_end
			,p.disc_perc		disc_perc
			,p.disc_amt			disc_amt
			,p.shop_cond		shop_cond
			,p.cust_cond		cust_cond
			,p.status			status
			,p.subinv_code		subinv_code
			,p.cat_code			cat_code
			,NULL				process_date
	FROM	tac_occ_pos_discount_tmp p
	WHERE	p.process_id	=	G_PROCESSID + 0.1
	AND		p.shop_cond		=	'F';

	-- Write log
	GR_LOG.PROCESS_DATE	:=  SYSDATE;
	GR_LOG.SEQ			:=  G_INF_SEQ;
	GR_LOG.LOG_CODE		:=  G_PROCESSID || ' 2.1 Get Discount with Subinv (case ALL)';
	GR_LOG.LOG_DESC		:=  'Get discount header, rows = ' || SQL%ROWCOUNT;
	GR_LOG.INF_UPD_DATE	:=  SYSDATE;
	WRITE_LOG(GR_LOG);
	G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;

	INSERT INTO tac_occ_pos_discount_tmp
	SELECT	G_PROCESSID + 0.2	process_id
			,p.discount_code	discount_code
			,p.discount_desc	discount_desc
			,p.discount_level	discount_level
			,GREATEST(p.disc_start,s.start_date)	disc_start
			,LEAST(p.disc_end,s.end_date)			disc_end
			,p.disc_perc		disc_perc
			,p.disc_amt			disc_amt
			,p.shop_cond		shop_cond
			,p.cust_cond		cust_cond
			,p.status			status
			,s.subinv_code		subinv_code
			,p.cat_code			cat_code
			,NULL				process_date
	FROM	tac_occ_pos_discount_tmp p, sa_discount_subinv s
	WHERE	p.process_id		=	G_PROCESSID + 0.1
	AND		p.discount_code		=	s.discount_code
	AND		p.shop_cond			IN	('A','N')	-- Allow, Not Allow
	AND		s.ou_code LIKE G_OU_CODE
	AND		s.subinv_code LIKE G_SUBINV_CODE
	AND		G_CUTOFF_DATE BETWEEN NVL(s.start_date,C_DFLT_START_DATE) AND NVL(s.end_date,C_DFLT_END_DATE);

	-- Write log
	GR_LOG.PROCESS_DATE	:=  SYSDATE;
	GR_LOG.SEQ			:=  G_INF_SEQ;
	GR_LOG.LOG_CODE		:=  G_PROCESSID || ' 2.2 Get Discount with Subinv (case Allow / Not Allow)';
	GR_LOG.LOG_DESC		:=  'Get discount subinv, rows = ' || SQL%ROWCOUNT;
	GR_LOG.INF_UPD_DATE	:=  SYSDATE;
	WRITE_LOG(GR_LOG);
	G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;

	DELETE	tac_occ_pos_discount_tmp
	WHERE	process_id	=	G_PROCESSID + 0.1;

-- 3. Get discount with category (cust group)
	DELETE	tac_occ_pos_discount_tmp
	WHERE	process_id	=	G_PROCESSID + 0.3;
	
	INSERT INTO tac_occ_pos_discount_tmp
	SELECT	G_PROCESSID + 0.3	process_id
			,p.discount_code	discount_code
			,p.discount_desc	discount_desc
			,p.discount_level	discount_level
			,p.disc_start		disc_start
			,p.disc_end			disc_end
			,p.disc_perc		disc_perc
			,p.disc_amt			disc_amt
			,p.shop_cond		shop_cond
			,p.cust_cond		cust_cond
			,p.status			status
			,p.subinv_code		subinv_code
			,p.cat_code			cat_code
			,NULL				process_date
	FROM	tac_occ_pos_discount_tmp p
	WHERE	p.process_id	=	G_PROCESSID + 0.2
	AND		p.cust_cond		=	'F'
	AND		G_CUTOFF_DATE BETWEEN NVL(p.disc_start,C_DFLT_START_DATE) AND NVL(p.disc_end,C_DFLT_END_DATE);

	-- Write log
	GR_LOG.PROCESS_DATE	:=  SYSDATE;
	GR_LOG.SEQ			:=  G_INF_SEQ;
	GR_LOG.LOG_CODE		:=  G_PROCESSID || ' 3.1 Get Discount with Cust Category (case ALL)';
	GR_LOG.LOG_DESC		:=  'Get discount header, rows = ' || SQL%ROWCOUNT;
	GR_LOG.INF_UPD_DATE	:=  SYSDATE;
	WRITE_LOG(GR_LOG);
	G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;

	INSERT INTO tac_occ_pos_discount_tmp
	SELECT	G_PROCESSID + 0.3	process_id
			,p.discount_code	discount_code
			,p.discount_desc	discount_desc
			,p.discount_level	discount_level
			,p.disc_start		disc_start
			,p.disc_end			disc_end
			,p.disc_perc		disc_perc
			,p.disc_amt			disc_amt
			,p.shop_cond		shop_cond
			,p.cust_cond		cust_cond
			,p.status			status
			,p.subinv_code		subinv_code
			,c.cat_code			cat_code
			,NULL				process_date
	FROM	tac_occ_pos_discount_tmp p, sa_discount_cust_group c
	WHERE	p.process_id		=	G_PROCESSID + 0.2
	AND		p.discount_code		=	c.discount_code
	AND		p.cust_cond			IN	('A','N')	-- Allow, Not Allow
	AND		c.ou_code LIKE G_OU_CODE
	AND		G_CUTOFF_DATE BETWEEN NVL(p.disc_start,C_DFLT_START_DATE) AND NVL(p.disc_end,C_DFLT_END_DATE);

	-- Write log
	GR_LOG.PROCESS_DATE	:=  SYSDATE;
	GR_LOG.SEQ			:=  G_INF_SEQ;
	GR_LOG.LOG_CODE		:=  G_PROCESSID || ' 3.2 Get Discount with Cust Category (case Allow / Not Allow)';
	GR_LOG.LOG_DESC		:=  'Get discount cust category, rows = ' || SQL%ROWCOUNT;
	GR_LOG.INF_UPD_DATE	:=  SYSDATE;
	WRITE_LOG(GR_LOG);
	G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;

	DELETE	tac_occ_pos_discount_tmp
	WHERE	process_id	=	G_PROCESSID + 0.2;

-- Compare to send data
	DELETE	tac_occ_pos_discount_tmp
	WHERE	process_id	=	G_PROCESSID;

	IF G_MODE = 'I' THEN
		INSERT INTO tac_occ_pos_discount_tmp
		SELECT	G_PROCESSID			process_id
				,p.discount_code	discount_code
				,p.discount_desc	discount_desc
				,p.discount_level	discount_level
				,p.disc_start		disc_start
				,p.disc_end			disc_end
				,p.disc_perc		disc_perc
				,p.disc_amt			disc_amt
				,p.shop_cond		shop_cond
				,p.cust_cond		cust_cond
				,p.status			status
				,p.subinv_code		subinv_code
				,p.cat_code			cat_code
				,SYSDATE			process_date
		FROM	tac_occ_pos_discount_tmp p
		WHERE	p.discount_code IN	(SELECT DISTINCT discount_code
									FROM	((SELECT discount_code
													,NVL(discount_desc,' ')
													,NVL(discount_level,' ')
													,NVL(disc_start,C_DFLT_START_DATE)
													,NVL(disc_end,C_DFLT_END_DATE)
													,NVL(disc_perc,0)
													,NVL(disc_amt,0)
													,NVL(shop_cond,'F')
													,NVL(cust_cond,'F')
													,NVL(status,'A')
													,NVL(subinv_code,' ')
													,NVL(cat_code,' ')
											FROM	tac_occ_pos_discount_tmp
											WHERE	process_id	=	G_PROCESSID + 0.3
											MINUS
											SELECT	discount_code
													,NVL(discount_desc,' ')
													,NVL(discount_level,' ')
													,NVL(disc_start,C_DFLT_START_DATE)
													,NVL(disc_end,C_DFLT_END_DATE)
													,NVL(disc_perc,0)
													,NVL(disc_amt,0)
													,NVL(shop_cond,'F')
													,NVL(cust_cond,'F')
													,NVL(status,'A')
													,NVL(subinv_code,' ')
													,NVL(cat_code,' ')
											FROM	tac_occ_pos_discount_sent s
											WHERE	s.subinv_code LIKE G_SUBINV_CODE
											AND		s.obsolete_record	=	'N')
											/*UNION
											(SELECT	discount_code
													,NVL(discount_desc,' ')
													,NVL(discount_level,' ')
													,NVL(disc_start,C_DFLT_START_DATE)
													,NVL(disc_end,C_DFLT_END_DATE)
													,NVL(disc_perc,0)
													,NVL(disc_amt,0)
													,NVL(shop_cond,'F')
													,NVL(cust_cond,'F')
													,NVL(status,'A')
													,NVL(subinv_code,' ')
													,NVL(cat_code,' ')
											FROM	tac_occ_pos_discount_sent s
											WHERE	s.subinv_code LIKE G_SUBINV_CODE
											AND		s.obsolete_record	=	'N'
											MINUS
											SELECT	discount_code
													,NVL(discount_desc,' ')
													,NVL(discount_level,' ')
													,NVL(disc_start,C_DFLT_START_DATE)
													,NVL(disc_end,C_DFLT_END_DATE)
													,NVL(disc_perc,0)
													,NVL(disc_amt,0)
													,NVL(shop_cond,'F')
													,NVL(cust_cond,'F')
													,NVL(status,'A')
													,NVL(subinv_code,' ')
													,NVL(cat_code,' ')
											FROM	tac_occ_pos_discount_tmp
											WHERE	process_id	=	G_PROCESSID + 0.3)*/
											)
									);

		G_CNT_ROWS_EXPORT	:=	SQL%ROWCOUNT;

		-- Write log
		GR_LOG.PROCESS_DATE	:=  SYSDATE;
		GR_LOG.SEQ			:=  G_INF_SEQ;
		GR_LOG.LOG_CODE		:=  G_PROCESSID || ' 5. Compare to data sent (for Incremental mode)';
		GR_LOG.LOG_DESC		:=  'Get discount lines to send, rows = ' || G_CNT_ROWS_EXPORT;
		GR_LOG.INF_UPD_DATE	:=  SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;

		DELETE	tac_occ_pos_discount_tmp
		WHERE	process_id	=	G_PROCESSID + 0.4;
	ELSE
		UPDATE	tac_occ_pos_discount_tmp
		SET		process_id		=	G_PROCESSID
				,process_date	=	SYSDATE
		WHERE	process_id		=	G_PROCESSID + 0.3;

		G_CNT_ROWS_EXPORT	:=	SQL%ROWCOUNT;
	END IF;

	IF NVL(G_CNT_ROWS_EXPORT, 0) = 0 THEN
		GR_LOG.PROCESS_DATE	:=  SYSDATE;
		GR_LOG.SEQ			:=  G_INF_SEQ;
		GR_LOG.LOG_CODE		:=  G_PROCESSID || '6. SQL%NOTFOUND';
		GR_LOG.LOG_DESC		:=  'No data to export';
		GR_LOG.INF_UPD_DATE	:=  SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;
	END IF;
	COMMIT;
	
-- Update TAC_OCC_POS_DISCOUNT_SENT
	UPDATE	tac_occ_pos_discount_sent s
	SET		s.obsolete_record	=	'Y'
			,s.process_id_4obsolete	=	G_PROCESSID
	WHERE	obsolete_record		=	'N'
	AND		(discount_code, subinv_code, cat_code) IN
			(SELECT DISTINCT discount_code, subinv_code, cat_code
			FROM	(SELECT	discount_code
							,NVL(discount_desc,' ')
							,NVL(discount_level,' ')
							,NVL(disc_start,C_DFLT_START_DATE)
							,NVL(disc_end,C_DFLT_END_DATE)
							,NVL(disc_perc,0)
							,NVL(disc_amt,0)
							,NVL(shop_cond,'F')
							,NVL(cust_cond,'F')
							,NVL(status,'A')
							,NVL(subinv_code,' ')
							,NVL(cat_code,' ')
					FROM	tac_occ_pos_discount_sent s
					WHERE	NVL(s.subinv_code,' ') LIKE G_SUBINV_CODE
					AND		s.obsolete_record	=	'N'
					MINUS
					SELECT	discount_code
							,NVL(discount_desc,' ')
							,NVL(discount_level,' ')
							,NVL(disc_start,C_DFLT_START_DATE)
							,NVL(disc_end,C_DFLT_END_DATE)
							,NVL(disc_perc,0)
							,NVL(disc_amt,0)
							,NVL(shop_cond,'F')
							,NVL(cust_cond,'F')
							,NVL(status,'A')
							,NVL(subinv_code,' ')
							,NVL(cat_code,' ')
					FROM	tac_occ_pos_discount_tmp
					WHERE	process_id	=	G_PROCESSID)
			);

	-- Write log
	GR_LOG.PROCESS_DATE	:=  SYSDATE;
	GR_LOG.SEQ			:=  G_INF_SEQ;
	GR_LOG.LOG_CODE		:=  G_PROCESSID || ' 7.1 Mark obsolete';
	GR_LOG.LOG_DESC		:=  'Mark obsolete records, rows = ' || SQL%ROWCOUNT;
	GR_LOG.INF_UPD_DATE	:=  SYSDATE;
	WRITE_LOG(GR_LOG);
	G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;

	INSERT INTO tac_occ_pos_discount_sent
		(discount_code
		,discount_desc
		,discount_level
		,disc_start
		,disc_end
		,disc_perc
		,disc_amt
		,shop_cond
		,cust_cond
		,status
		,subinv_code
		,cat_code
		,process_id_first_sent
		,obsolete_record)
	SELECT	discount_code
			,discount_desc
			,discount_level
			,disc_start
			,disc_end
			,disc_perc
			,disc_amt
			,NVL(shop_cond,'F')
			,NVL(cust_cond,'F')
			,status
			,subinv_code
			,cat_code
			,G_PROCESSID
			,'N'	-- Not obsolete
	FROM	tac_occ_pos_discount_tmp
	WHERE	(discount_code,NVL(subinv_code,' '),NVL(cat_code,' ')) IN
			(SELECT DISTINCT discount_code, subinv_code, cat_code
			FROM	(SELECT	discount_code
							,NVL(discount_desc,' ') discount_desc
							,NVL(discount_level,' ')
							,NVL(disc_start,C_DFLT_START_DATE)
							,NVL(disc_end,C_DFLT_END_DATE)
							,NVL(disc_perc,0)
							,NVL(disc_amt,0)
							,NVL(shop_cond,'F')
							,NVL(cust_cond,'F')
							,NVL(status,'A') status
							,NVL(subinv_code,' ')	subinv_code
							,NVL(cat_code,' ')		cat_code
					FROM	tac_occ_pos_discount_tmp
					WHERE	process_id	=	G_PROCESSID
					MINUS
					SELECT	discount_code
							,NVL(discount_desc,' ')
							,NVL(discount_level,' ')
							,NVL(disc_start,C_DFLT_START_DATE)
							,NVL(disc_end,C_DFLT_END_DATE)
							,NVL(disc_perc,0)
							,NVL(disc_amt,0)
							,NVL(shop_cond,'F')
							,NVL(cust_cond,'F')
							,NVL(status,'A')
							,NVL(subinv_code,' ')
							,NVL(cat_code,' ')
					FROM	tac_occ_pos_discount_sent s
					WHERE	s.subinv_code LIKE G_SUBINV_CODE
					AND		s.obsolete_record	=	'N')
			)
	;

	-- Write log
	GR_LOG.PROCESS_DATE	:=  SYSDATE;
	GR_LOG.SEQ			:=  G_INF_SEQ;
	GR_LOG.LOG_CODE		:=  G_PROCESSID || ' 7.2 Insert sent records';
	GR_LOG.LOG_DESC		:=  'Insert sent records, rows = ' || SQL%ROWCOUNT;
	GR_LOG.INF_UPD_DATE	:=  SYSDATE;
	WRITE_LOG(GR_LOG);
	G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;

END prepare_discount;
------------------------------------------
PROCEDURE sent_interface_price_list
(i_upd			IN	  VARCHAR2
,i_pgm			IN	  VARCHAR2
,i_output_path	IN	  VARCHAR2
,io_status		IN OUT  VARCHAR2)
IS
	t_text			glb_text_output_util.varchar_tabtyp;
	t_format		glb_text_output_util.number_tabtyp;
	v_filename		VARCHAR2(100)	:=	G_FILENAME || '.DAT';
	v_filename_sync	VARCHAR2(100)	:=	G_FILENAME || '.SYNC';

	CURSOR c_data2sent IS
		SELECT	*
		FROM	tac_occ_pos_price_list_tmp t
		WHERE	t.process_id	=	G_PROCESSID
		ORDER BY t.pricelist_code, t.ou_code, t.item_code;
BEGIN
	G_INF_SEQ			:=  1;
	G_CNT_ROWS_EXPORT	:=  0;
	
	io_status		:=  glb_text_output_util.NO_ERROR;

	-->> Initial log
	GR_LOG	:=	NULL;
	GR_LOG.ENTITY		:=	G_ENTITY;
	GR_LOG.PROCESS_ID	:=  G_PROCESSID;
	GR_LOG.INF_UPD_BY	:=  i_upd;
	GR_LOG.INF_UPD_PGM	:=  i_pgm;

	-->> Lock Transaction Pending.
	GR_LOG.STEP				:=  2;
	GR_LOG.STEP_DESC		:=  'Generate Interface file';
	GR_LOG.PROCESS_FLAG		:=  'Sent Interface';
	GR_LOG.INF_LOCK_FLAG	:=  'Unlock';

	FOR i IN 1..10
	LOOP
		t_format(i) :=  -1;
	END LOOP;
	glb_text_output_util.set_format
		(it_col_len	=>  t_format
		,o_status	=>  io_status);
	IF io_status <> glb_text_output_util.NO_ERROR THEN
		RAISE e_abnormal_end;
	END IF;

	FOR r_data IN c_data2sent LOOP
		BEGIN
			G_CNT_ROWS_EXPORT	:=  G_CNT_ROWS_EXPORT + 1;

			t_text(1)   :=  clear_spchar(r_data.ou_code,'|');
			t_text(2)   :=  clear_spchar(r_data.pricelist_code,'|');
			t_text(3)   :=  clear_spchar(r_data.item_code,'|');
			t_text(4)   :=  r_data.std_price;
			t_text(5)   :=  TO_CHAR(r_data.start_date,'DD/MM/YYYY');
			t_text(6)   :=  TO_CHAR(r_data.end_date,'DD/MM/YYYY');
			t_text(7)   :=  clear_spchar(r_data.shop_cond,'|');
			t_text(8)   :=  clear_spchar(r_data.cust_cond,'|');
			t_text(9)   :=  clear_spchar(r_data.subinv_code,'|');
			t_text(10)  :=  clear_spchar(r_data.cat_code,'|');
			glb_text_output_util.add_txt
				(it_message		=>  t_text
				,i_delimiter	=>  NULL
				,i_separator	=>  '|'
				);

			GR_LOG.PROCESS_DATE	:=  SYSDATE;
			GR_LOG.SEQ			:=  G_INF_SEQ;
			GR_LOG.LOG_CODE		:=  G_PROCESSID || ' Complete';
			GR_LOG.LOG_DESC		:=  SUBSTR('OU code : ' || r_data.ou_code ||
											' Pricelist code : ' || r_data.pricelist_code ||
											' Item Code : ' || r_data.item_code ||
											' STD Price : ' || r_data.std_price ||
											' Start Date : ' || TO_CHAR(r_data.start_date,'DD/MM/RRRR') ||
											' End Date : ' || TO_CHAR(r_data.end_date,'DD/MM/RRRR') ||
											' Shop Cond : ' || r_data.shop_cond ||
											' Cust Cond : ' || r_data.cust_cond ||
											' Subinv Code : ' || r_data.subinv_code ||
											' Cat Code : ' || r_data.cat_code, 1, 400);
			GR_LOG.INF_UPD_DATE	 :=  SYSDATE;
			WRITE_LOG(GR_LOG);
			G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;
		EXCEPTION
			WHEN OTHERS THEN
				G_CNT_ROW_ERROR		:=  NVL(G_CNT_ROW_ERROR, 0) + 1;
				GR_LOG.PROCESS_DATE	:=  SYSDATE;
				GR_LOG.SEQ			:=  G_INF_SEQ;
				GR_LOG.LOG_CODE		:=  SUBSTR(G_PROCESSID || ' ' || SQLERRM,1,100);
				GR_LOG.LOG_DESC		:=  SUBSTR('OU code : ' || r_data.ou_code ||
											' Pricelist code : ' || r_data.pricelist_code ||
											' Item Code : ' || r_data.item_code ||
											' STD Price : ' || r_data.std_price ||
											' Start Date : ' || TO_CHAR(r_data.start_date,'DD/MM/RRRR') ||
											' End Date : ' || TO_CHAR(r_data.end_date,'DD/MM/RRRR') ||
											' Shop Cond : ' || r_data.shop_cond ||
											' Cust Cond : ' || r_data.cust_cond ||
											' Subinv Code : ' || r_data.subinv_code ||
											' Cat Code : ' || r_data.cat_code, 1, 400);
				GR_LOG.INF_UPD_DATE	 :=  SYSDATE;
				WRITE_LOG(GR_LOG);
				G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;
		END;
	END LOOP;
	---------------------------------------
	IF G_CNT_ROWS_EXPORT = 0 THEN
		GR_LOG.PROCESS_DATE	:=  SYSDATE;
		GR_LOG.SEQ			:=  G_INF_SEQ;
		GR_LOG.LOG_CODE		:=  SUBSTR(G_PROCESSID || ' SQL%NOTFOUND',1,100);
		GR_LOG.LOG_DESC		:=  'No data to export';
		GR_LOG.INF_UPD_DATE	:=  SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;
	END IF;

-- Write file eventhough no data found
	glb_text_output_util.write2file
		(i_file_loc		=>	i_output_path
		,i_file_name	=>	v_filename
		,i_charset		=>	'UTF8'
		,o_status		=>	io_status);
	IF io_status <> glb_text_output_util.NO_ERROR THEN
		RAISE e_abnormal_end;
	END IF;

	t_format.DELETE;
	t_format(1) :=  -1;
	glb_text_output_util.set_format -- Clear previous data
		(it_col_len	=>  t_format
		,o_status	=>  io_status);
	IF io_status <> glb_text_output_util.NO_ERROR THEN
		RAISE e_abnormal_end;
	END IF;

	t_text.DELETE;
	t_text(1)   :=  G_CNT_ROWS_EXPORT;
	glb_text_output_util.add_txt
		(it_message		=>  t_text
		,i_delimiter	=>  NULL
		,i_separator	=>  '|'
		);

	glb_text_output_util.write2file
		(i_file_loc		=>  i_output_path
		,i_file_name	=>  v_filename_sync
		,i_charset		=>	'UTF8'
		,o_status		=>  io_status);
	IF io_status <> glb_text_output_util.NO_ERROR THEN
		RAISE e_abnormal_end;
	END IF;

	COMMIT;
EXCEPTION
	WHEN e_abnormal_end THEN
		GR_LOG.PROCESS_DATE	:=  SYSDATE;
		GR_LOG.SEQ			:=  G_INF_SEQ;
		GR_LOG.LOG_CODE		:=  G_PROCESSID || ' Generate File Error';
		GR_LOG.LOG_DESC		:=  'Auto revert status ' || TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS') ||
								io_status;
		GR_LOG.INF_UPD_DATE	:=  SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;
END sent_interface_price_list;
------------------------------------------
PROCEDURE sent_interface_discount
(i_upd			IN	  VARCHAR2
,i_pgm			IN	  VARCHAR2
,i_output_path	IN	  VARCHAR2
,io_status		IN OUT  VARCHAR2)
IS
	t_text			glb_text_output_util.varchar_tabtyp;
	t_format		glb_text_output_util.number_tabtyp;
	v_filename		VARCHAR2(100)	:=	G_FILENAME || '.DAT';
	v_filename_sync	VARCHAR2(100)	:=	G_FILENAME || '.SYNC';

	CURSOR c_data2sent IS
		SELECT	*
		FROM	tac_occ_pos_discount_tmp t
		WHERE	t.process_id	=	G_PROCESSID
		ORDER BY t.discount_code, t.subinv_code, t.cat_code;
BEGIN
	G_INF_SEQ			:=  1;
	G_CNT_ROWS_EXPORT	:=  0;
	
	io_status		:=  glb_text_output_util.NO_ERROR;

	-->> Initial log
	GR_LOG	:=	NULL;
	GR_LOG.ENTITY		:=	G_ENTITY;
	GR_LOG.PROCESS_ID	:=  G_PROCESSID;
	GR_LOG.INF_UPD_BY	:=  i_upd;
	GR_LOG.INF_UPD_PGM	:=  i_pgm;

	-->> Lock Transaction Pending.
	GR_LOG.STEP				:=  2;
	GR_LOG.STEP_DESC		:=  'Generate Interface file';
	GR_LOG.PROCESS_FLAG		:=  'Sent Interface';
	GR_LOG.INF_LOCK_FLAG	:=  'Unlock';

	FOR i IN 1..11
	LOOP
		t_format(i) :=  -1;
	END LOOP;
	glb_text_output_util.set_format
		(it_col_len	=>  t_format
		,o_status	=>  io_status);
	IF io_status <> glb_text_output_util.NO_ERROR THEN
		RAISE e_abnormal_end;
	END IF;

	FOR r_data IN c_data2sent LOOP
		BEGIN
			G_CNT_ROWS_EXPORT	:=  G_CNT_ROWS_EXPORT + 1;

			t_text(1)   :=  clear_spchar(r_data.discount_code,'|');
			t_text(2)   :=  clear_spchar(r_data.discount_desc,'|');
			t_text(3)   :=  TO_CHAR(r_data.disc_start,'DD/MM/YYYY');
			t_text(4)   :=  TO_CHAR(r_data.disc_end,'DD/MM/YYYY');
			t_text(5)   :=  r_data.disc_perc;
			t_text(6)   :=  r_data.disc_amt;
			t_text(7)   :=  clear_spchar(r_data.shop_cond,'|');
			t_text(8)   :=  clear_spchar(r_data.cust_cond,'|');
			t_text(9)   :=  clear_spchar(r_data.status,'|');
			t_text(10)  :=  clear_spchar(r_data.subinv_code,'|');
			t_text(11)	:=	clear_spchar(r_data.cat_code,'|');
			
			glb_text_output_util.add_txt
				(it_message		=>  t_text
				,i_delimiter	=>  NULL
				,i_separator	=>  '|'
				);

			GR_LOG.PROCESS_DATE	:=  SYSDATE;
			GR_LOG.SEQ			:=  G_INF_SEQ;
			GR_LOG.LOG_CODE		:=  G_PROCESSID || ' Complete';
			GR_LOG.LOG_DESC		:=  SUBSTR('Discount code : ' || r_data.discount_code ||
											' Start Date : ' || TO_CHAR(r_data.disc_start,'DD/MM/RRRR') ||
											' End Date : ' || TO_CHAR(r_data.disc_end,'DD/MM/RRRR') ||
											' Discount % : ' || r_data.disc_perc ||
											' Discount Amt : ' || r_data.disc_amt ||
											' Shop Cond : ' || r_data.shop_cond ||
											' Cust Cond : ' || r_data.cust_cond ||
											' Status : ' || r_data.status ||
											' Subinv Code : ' || r_data.subinv_code ||
											' Cat Code : ' || r_data.cat_code, 1, 400);
			GR_LOG.INF_UPD_DATE	 :=  SYSDATE;
			WRITE_LOG(GR_LOG);
			G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;
		EXCEPTION
			WHEN OTHERS THEN
				G_CNT_ROW_ERROR		:=  NVL(G_CNT_ROW_ERROR, 0) + 1;
				GR_LOG.PROCESS_DATE	:=  SYSDATE;
				GR_LOG.SEQ			:=  G_INF_SEQ;
				GR_LOG.LOG_CODE		:=  SUBSTR(G_PROCESSID || ' ' || SQLERRM,1,100);
				GR_LOG.LOG_DESC		:=  SUBSTR('Discount code : ' || r_data.discount_code ||
											' Start Date : ' || TO_CHAR(r_data.disc_start,'DD/MM/RRRR') ||
											' End Date : ' || TO_CHAR(r_data.disc_end,'DD/MM/RRRR') ||
											' Discount % : ' || r_data.disc_perc ||
											' Discount Amt : ' || r_data.disc_amt ||
											' Shop Cond : ' || r_data.shop_cond ||
											' Cust Cond : ' || r_data.cust_cond ||
											' Status : ' || r_data.status ||
											' Subinv Code : ' || r_data.subinv_code ||
											' Cat Code : ' || r_data.cat_code, 1, 400);
				GR_LOG.INF_UPD_DATE	 :=  SYSDATE;
				WRITE_LOG(GR_LOG);
				G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;
		END;
	END LOOP;
	---------------------------------------
	IF G_CNT_ROWS_EXPORT = 0 THEN
		GR_LOG.PROCESS_DATE	:=  SYSDATE;
		GR_LOG.SEQ			:=  G_INF_SEQ;
		GR_LOG.LOG_CODE		:=  SUBSTR(G_PROCESSID || ' SQL%NOTFOUND',1,100);
		GR_LOG.LOG_DESC		:=  'No data to export';
		GR_LOG.INF_UPD_DATE	:=  SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;
	END IF;

-- Write file eventhough no data found
	glb_text_output_util.write2file
		(i_file_loc		=>	i_output_path
		,i_file_name	=>	v_filename
		,i_charset		=>	'UTF8'
		,o_status		=>	io_status);
	IF io_status <> glb_text_output_util.NO_ERROR THEN
		RAISE e_abnormal_end;
	END IF;

	t_format.DELETE;
	t_format(1) :=  -1;
	glb_text_output_util.set_format -- Clear previous data
		(it_col_len	=>  t_format
		,o_status	=>  io_status);
	IF io_status <> glb_text_output_util.NO_ERROR THEN
		RAISE e_abnormal_end;
	END IF;

	t_text.DELETE;
	t_text(1)   :=  G_CNT_ROWS_EXPORT;
	glb_text_output_util.add_txt
		(it_message		=>  t_text
		,i_delimiter	=>  NULL
		,i_separator	=>  '|'
		);

	glb_text_output_util.write2file
		(i_file_loc		=>  i_output_path
		,i_file_name	=>  v_filename_sync
		,i_charset		=>	'UTF8'
		,o_status		=>  io_status);
	IF io_status <> glb_text_output_util.NO_ERROR THEN
		RAISE e_abnormal_end;
	END IF;

	COMMIT;
EXCEPTION
	WHEN e_abnormal_end THEN
		GR_LOG.PROCESS_DATE	:=  SYSDATE;
		GR_LOG.SEQ			:=  G_INF_SEQ;
		GR_LOG.LOG_CODE		:=  G_PROCESSID || ' Generate File Error';
		GR_LOG.LOG_DESC		:=  'Auto revert status ' || TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS') ||
								io_status;
		GR_LOG.INF_UPD_DATE	:=  SYSDATE;
		WRITE_LOG(GR_LOG);
		G_INF_SEQ   :=  NVL(G_INF_SEQ, 0) + 1;
END sent_interface_discount;
------------------------------------------
PROCEDURE send_email
(i_smtp_server		IN	VARCHAR2	:=  'mail-gw.tac.co.th'
,i_smtp_port		IN	VARCHAR2	:=  25
,i_from_email		IN	VARCHAR2	:=  'ERPOperationSupport@dtac.co.th'
,i_to_email			IN	VARCHAR2	-- Separate by ,
,i_cc_email			IN	VARCHAR2	:=  NULL
,i_bcc_email		IN	VARCHAR2	:=  NULL
,i_subject			IN	VARCHAR2	:=  '[POS] Interface ' || G_ENTITY || ' to OCC - ' || G_RESULT_STATUS
,i_reply_email		IN	VARCHAR2	:=  'OCC_INTERFACE@dtac.co.th'
,i_priority			IN	NUMBER	  :=  3   -- Normal priority
,io_status			IN OUT  VARCHAR2
)
IS
	C_ERROR			 CONSTANT VARCHAR2(40)   :=  '$$ERROR$$';
	NEW_LINE_IN_MSG	 CONSTANT VARCHAR2(10)   :=  '<BR>';
	v_blob			  BLOB;
	v_msg_template	  VARCHAR2(32000) :=
		'Dear All,' || NEW_LINE_IN_MSG ||
		NEW_LINE_IN_MSG ||
		'Please be informed that POS interface ' || G_ENTITY || ' to OCC executed with ' || G_RESULT_STATUS || '.' || 
		CASE WHEN G_RESULT_STATUS='SUCCESS' THEN NULL ELSE '  See more detail in file enclosed.' END || NEW_LINE_IN_MSG ||
		NEW_LINE_IN_MSG ||
		'Job No. : ' || G_N_NO || NEW_LINE_IN_MSG ||
		'Job Status : Complete' || CASE WHEN G_RESULT_STATUS='SUCCESS' THEN NULL ELSE ' with ' || G_RESULT_STATUS END || NEW_LINE_IN_MSG ||
		'Job Broken : ' || G_JOB_BROKEN || NEW_LINE_IN_MSG ||
		NEW_LINE_IN_MSG ||
		'Thank you & Regards,' || NEW_LINE_IN_MSG ||
		'POS auto job' || NEW_LINE_IN_MSG;
	v_msg				v_msg_template%TYPE;
	v_error				VARCHAR2(4000);
	b_has_attachment	BOOLEAN;-- :=  G_RESULT_STATUS <> 'SUCCESS';

-- Start Temporary for checking email
	t_text		glb_text_output_util.varchar_tabtyp;
	t_format	glb_text_output_util.number_tabtyp;
	v_filename	VARCHAR2(4000);
-- End Temporary for checking email

BEGIN
	io_status   :=  glb_text_output_util.NO_ERROR;

	v_msg	:=  v_msg_template;
-- Error => write error HTML file
	IF G_RESULT_STATUS <> 'SUCCESS' THEN
		FOR i IN 1..7
		LOOP
			t_format(i) :=  -1;
		END LOOP;

		glb_text_output_util.set_format
			(it_col_len		=>  t_format
			,o_status		=>  io_status
			,i_clear_data	=>  'Y');
		IF io_status <> glb_text_output_util.NO_ERROR THEN
			RAISE e_abnormal_end;
		END IF;

		-- Headings
		t_text(1)   :=  'Step';
		t_text(2)   :=  'Log Code';
		t_text(3)   :=  'Log Description';
		t_text(4)   :=  'Status';
		t_text(5)   :=  'Process Flag';
		t_text(6)   :=  'INF Lock Flag';
		t_text(7)   :=  'INF Update Date';
		
		glb_text_output_util.add_txt
		(it_message		 =>  t_text
		/*,i_delimiter	  =>  glb_text_output_util.HTML_TBL_HDR_DELIM_B   --'<TH>'
		,i_delimiter_end	=>  glb_text_output_util.HTML_TBL_HDR_DELIM_E   --'</TH>' || glb_bulk_run_report_api.NEW_LINE
		,i_separator		=>  glb_text_output_util.HTML_TBL_SEPARATOR	 --NULL
		,i_line_prefix	  =>  glb_text_output_util.HTML_TBL_HDR_ROW_PREFIX	--'<TABLE BORDER=1><TR>'
		,i_line_suffix	  =>  glb_text_output_util.HTML_TBL_HDR_ROW_SUFFIX*/);	--'</TR>'); 

		FOR r_error IN (SELECT  t.*
						FROM	tac_occ_outb_inf_log t
						WHERE   process_id  =   G_PROCESSID
						ORDER BY inf_upd_date, step, seq)
		LOOP
			t_text(1)   :=  r_error.step;
			t_text(2)   :=  r_error.log_code;
			t_text(3)   :=  r_error.log_desc;
			t_text(4)   :=  G_RESULT_STATUS;
			t_text(5)   :=  r_error.process_flag;
			t_text(6)   :=  r_error.inf_lock_flag;
			t_text(7)   :=  r_error.inf_upd_date;
			
			glb_text_output_util.add_txt
			(it_message			=>  t_text
			/*,i_delimiter		=>  glb_text_output_util.HTML_TBL_DATA_DELIM_B  --'<TH>'
			,i_delimiter_end	=>  glb_text_output_util.HTML_TBL_DATA_DELIM_E  --'</TH>' || glb_bulk_run_report_api.NEW_LINE
			,i_separator		=>  glb_text_output_util.HTML_TBL_SEPARATOR	 --NULL
			,i_line_prefix		=>  glb_text_output_util.HTML_TBL_DATA_ROW_PREFIX   --'<TABLE BORDER=1><TR>'
			,i_line_suffix		=>  glb_text_output_util.HTML_TBL_DATA_ROW_SUFFIX*/);   --'</TR>'); 
		END LOOP;
		glb_text_output_util.write2file
			(i_file_loc		=>  G_DIRECTORY
			,i_file_name	=>  v_filename
			,i_charset		=>	'UTF8'
			,o_status		=>  io_status);
		IF io_status <> glb_text_output_util.NO_ERROR THEN
			RAISE e_abnormal_end;
		END IF;
	END IF;

	IF b_has_attachment THEN
		v_blob  :=  mail_tools.get_local_binary_data
						(p_dir  =>  G_DIRECTORY
						,p_file =>  v_filename);
	ELSE
		v_blob  :=  EMPTY_BLOB();
	END IF;

-- Start Temporary for checking email
/*	t_format.DELETE;
	t_format(1) :=  -1;
	glb_text_output_util.set_format -- Clear previous data
		(it_col_len	 =>  t_format
		,o_status	   =>  io_status);
	IF io_status <> glb_text_output_util.NO_ERROR THEN
		RAISE e_abnormal_end;
	END IF;

	t_text.DELETE;
	t_text(1)   :=  'SMTP Server: ' || i_smtp_server;
	glb_text_output_util.add_txt
		(it_message		 =>  t_text
		,i_delimiter		=>  NULL
		);

	t_text.DELETE;
	t_text(1)   :=  'SMTP Port: ' || i_smtp_port;
	glb_text_output_util.add_txt
		(it_message		 =>  t_text
		,i_delimiter		=>  NULL
		);

	t_text.DELETE;
	t_text(1)   :=  'From email: ' || i_from_email;
	glb_text_output_util.add_txt
		(it_message		 =>  t_text
		,i_delimiter		=>  NULL
		);

	t_text.DELETE;
	t_text(1)   :=  'To email: ' || i_to_email;
	glb_text_output_util.add_txt
		(it_message		 =>  t_text
		,i_delimiter		=>  NULL
		);

	t_text.DELETE;
	t_text(1)   :=  'CC email: ' || i_cc_email;
	glb_text_output_util.add_txt
		(it_message		 =>  t_text
		,i_delimiter		=>  NULL
		);

	t_text.DELETE;
	t_text(1)   :=  'BCC email: ' || i_bcc_email;
	glb_text_output_util.add_txt
		(it_message		 =>  t_text
		,i_delimiter		=>  NULL
		);

	t_text.DELETE;
	t_text(1)   :=  'Email Subject: ' || i_subject;
	glb_text_output_util.add_txt
		(it_message		 =>  t_text
		,i_delimiter		=>  NULL
		);

	IF b_has_attachment THEN
		t_text.DELETE;
		t_text(1)   :=  'Email Attached File: ' || v_filename;
		glb_text_output_util.add_txt
			(it_message		 =>  t_text
			,i_delimiter		=>  NULL
			);
	END IF;

	t_text.DELETE;
	t_text(1)   :=  'Email Message:';
	glb_text_output_util.add_txt
		(it_message		 =>  t_text
		,i_delimiter		=>  NULL
		);

	t_text.DELETE;
	t_text(1)   :=  v_msg;
	glb_text_output_util.add_txt
		(it_message		 =>  t_text
		,i_delimiter		=>  NULL
		);

	glb_text_output_util.write2file
		(i_file_loc		=>  G_DIRECTORY
		,i_file_name	=>  'email_' || TO_CHAR(SYSDATE,'YYYYMMDD_HH24MISSSSS')
		,i_charset		=>	'UTF8'
		,o_status		=>  io_status);
	IF io_status <> glb_text_output_util.NO_ERROR THEN
		RAISE e_abnormal_end;
	END IF;*/
-- End Temporary for checking email

	mail_tools.sendmail
		(smtp_server		=>  i_smtp_server
		,smtp_server_port	=>  i_smtp_port
		,from_name			=>  i_from_email
		,to_name			=>  i_to_email  -- list of TO email addresses separated by commas (,)
		,cc_name			=>  i_cc_email  -- list of CC email addresses separated by commas (,)
		,bcc_name			=>  i_bcc_email -- list of BCC email addresses separated by commas (,)
		,subject			=>  i_subject
		,message			=>  v_msg
		,priority			=>  i_priority  -- 1-5 1 being the highest priority and 3 normal priority
		,filename			=>  v_filename
		,binaryfile			=>  v_blob);
END send_email;
------------------------------------------

--@@@ AUTO RUNNING PROCESS @@@ --
PROCEDURE main_price_list
(p_ou_code			IN	pb_company.ou_code%TYPE
,p_subinv_code		IN	pb_subinv.subinv_code%TYPE
,p_cutoff_eff_date	IN	VARCHAR2
,p_cutoff_back_days	IN	NUMBER
,p_mode				IN	VARCHAR2
,p_log_back_days	IN	NUMBER
,p_output_path		IN	VARCHAR2
,p_field_separator	IN	VARCHAR2
,p_mail_to			IN	VARCHAR2	-- Send email to ?? upon completion
,N_NO				IN	NUMBER
,N_ID				IN	NUMBER)
IS
	V_RESULT	VARCHAR2(400) := 'Default';
	V_WORK		VARCHAR2(1);
	SUC_CNT		NUMBER;
	ERR_CNT		NUMBER;
	M_SEQ		NUMBER;
	v_dir_obj_name	VARCHAR2(100)	:=	'OCC_POS_PRCLST_DIR';
BEGIN
	G_ENTITY	:=	'PRICELIST';
	SELECT  MAX(j.broken)
	INTO	G_JOB_BROKEN
	FROM	all_jobs j
	WHERE   j.job   =   N_NO;

	G_DIRECTORY :=  NULL;   -- Clear value
	G_RESULT_STATUS :=  'SUCCESS';
	
	FOR rec IN (SELECT	directory_name
				FROM	all_directories
				WHERE	directory_path  =   p_output_path
				AND		directory_name LIKE v_dir_obj_name || '%')
	LOOP
		G_DIRECTORY :=  rec.directory_name;
		EXIT;
	END LOOP;
	
	IF G_DIRECTORY IS NULL THEN
		SELECT  NVL(MAX(directory_name),v_dir_obj_name)
		INTO	G_DIRECTORY
		FROM	all_directories
		WHERE   directory_name LIKE v_dir_obj_name || '%';
		
		IF G_DIRECTORY = v_dir_obj_name THEN
			G_DIRECTORY :=  G_DIRECTORY || '_001';
		ELSE
			G_DIRECTORY :=  v_dir_obj_name || '_' || TO_CHAR(TO_NUMBER(SUBSTR(G_DIRECTORY,LENGTH(v_dir_obj_name)+2))+1,'fm000');
		END IF;
		
		EXECUTE IMMEDIATE 'CREATE DIRECTORY ' || G_DIRECTORY || ' AS ''' || p_output_path || '''';
	END IF;
	G_DIRECTORY :=  UPPER(G_DIRECTORY); -- Make sure we always refer directory name with uppercase

-- Set parameters
	G_OU_CODE			:=	NVL(p_ou_code,'%');
	G_SUBINV_CODE		:=	NVL(p_subinv_code,'%');
	G_MODE				:=	NVL(p_mode,'I');	-- I = Incremental, F = Full
	G_CUTOFF_BACK_DAYS	:=	NVL(p_cutoff_back_days,0);
	IF p_cutoff_eff_date IS NULL THEN
		G_CUTOFF_DATE	:=	TRUNC(SYSDATE) + G_CUTOFF_BACK_DAYS;
	ELSE
		G_CUTOFF_DATE	:=	TO_DATE(p_cutoff_eff_date,'DD/MM/RRRR');
	END IF;
	G_LOG_BACK_DAYS	 :=  p_log_back_days;
	G_N_NO			  :=  n_no;
	G_N_ID			  :=  n_id;

-- Clear log
	G_INF_SEQ   :=  1;
	clear_log(G_ENTITY);
	
	G_PROCESSID :=  NULL;   -- Clear value

	-->> Get Process ID Running.
	IF G_PROCESSID IS NULL THEN
		BEGIN
			SELECT  LPAD(NVL(TO_NUMBER(PARAMETER_VALUE), 1), 5, 0)
			INTO	G_PROCESSID
			FROM	PB_PARAMETER_SETUP
			WHERE   PGM_SETUP = 'OCCIFPRC'
			AND	 PARAMETER_NAME = 'OCC Interface Price List Process ID';

			UPDATE  PB_PARAMETER_SETUP
			SET	 PARAMETER_VALUE = TO_CHAR(NVL(TO_NUMBER(PARAMETER_VALUE),0) + 1)
			WHERE   PGM_SETUP = 'OCCIFPRC'
			AND	 PARAMETER_NAME = 'OCC Interface Price List Process ID';
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				G_PROCESSID := '00001';
				INSERT INTO PB_PARAMETER_SETUP
					(PGM_SETUP
					,PARAMETER_NAME
					,PARAMETER_VALUE
					,TRANSFER
					,UPD_BY
					,UPD_DATE
					,UPD_PGM)
				VALUES
					('OCCIFPRC'
					,'OCC Interface Price List Process ID'
					,G_PROCESSID + 1	-- Next process id
					,SYSDATE
					,'AUTO'
					,SYSDATE
					,'BACK-JOB');
		END;
	END IF;

	glb_text_output_util.g_process_id   :=  G_PROCESSID;

	-- CHECK WORK CONDITION
	V_WORK := 'Y';
	--V_WORK := CHECK_JOB_WORK(N_NO);
	
	IF NVL(V_WORK, 'N') = 'Y' THEN
		G_CNT_ROWS_EXPORT	:=	0;
		G_CNT_ROW_ERROR		:=	0;
		
		-- WRITE START PROCESS
		M_SEQ := GET_MONITOR_SEQ(N_ID);
		
		BEGIN
			INSERT INTO JOBS_MONITOR
				(JOB_ID,
				 JOB_SEQ,
				 JOB_NAME,
				 JOB_MESG,
				 JOB_TIME,
				 SUC_CNT,
				 ERR_CNT,
				 UPD_BY,
				 UPD_DATE,
				 UPD_PGM,
				 STATUS)
			VALUES
				(N_ID,
				 M_SEQ,
				 C_JOB_NAME || G_ENTITY,
				 'Start to run : OCC_INTERFACE_' || G_ENTITY,
				 SYSDATE,
				 G_CNT_ROWS_EXPORT,
				 G_CNT_ROW_ERROR,
				 'AUTO',
				 SYSDATE,
				 'BACK-JOB',
				 'Start Run Process ' || G_PROCESSID);
		EXCEPTION
			WHEN OTHERS THEN
				NULL;
		END;
		
/*		start_process('AUTO'
					 ,'BACK-JOB');
		-- @@@ Validate
		validate('AUTO'
				,'BACK-JOB'
				,v_result);
		IF v_result NOT LIKE 'Complete%' THEN
			GOTO end_process;
		END IF;
*/
		-- @@@ Prepare Data @@@ --
		prepare_price_list	('AUTO'
							,'BACK-JOB');
		
/*		ending_process  ('AUTO'
						,'BACK-JOB');
*/		
		--> SENT INTERFACE TO TEXT FILE
		G_FILENAME  :=  'POS_PRICELIST_' || TO_CHAR(SYSDATE,'YYYYMMDD_HHMISS');
		sent_interface_price_list('AUTO'
								,'BACK-JOB'
								,G_DIRECTORY
								,v_result);
		IF v_result <> glb_text_output_util.NO_ERROR THEN
			GOTO end_process;
		END IF;
		GOTO EXIT_PROCESS;
		-- @@@	 End	  @@@ --
		<<END_PROCESS>>
		BEGIN
			-->> Initial log
			GR_LOG.PROCESS_ID	:=  G_PROCESSID;
			GR_LOG.INF_UPD_BY	:=  'AUTO';
			GR_LOG.INF_UPD_PGM	:=  'BACK-JOB';

			-->> Lock Transaction Pending.
			GR_LOG.STEP				:=  6;
			GR_LOG.STEP_DESC		:=  'Error Interface';
			GR_LOG.PROCESS_FLAG		:=  'Error';
			GR_LOG.INF_LOCK_FLAG	:=  'Unlock';

			GR_LOG.PROCESS_DATE	:=  SYSDATE;
			GR_LOG.SEQ			:=  0;
			GR_LOG.LOG_CODE		:=  G_PROCESSID || ' Error Generating file';
			GR_LOG.LOG_DESC		:=  v_result;
			GR_LOG.INF_UPD_DATE	:=  SYSDATE;
			WRITE_LOG(GR_LOG);

			G_RESULT_STATUS :=  'FAILED';
/*			REMOVE_PROCESS  ('AUTO'
							,'BACK-JOB');*/
		END END_PROCESS;
		
		<<EXIT_PROCESS>>
	-- WRITE END PROCESS

		-- Send email
		send_email  (i_to_email		=>  'ERPOperationSupport@dtac.co.th'
					,i_cc_email		=>  NULL
					,i_bcc_email	=>  NULL
					,io_status		=>  v_result
					);
		IF v_result <> glb_text_output_util.NO_ERROR THEN
			GOTO end_process_2;
		END IF;

		BEGIN
			INSERT INTO JOBS_MONITOR
				(JOB_ID,
				 JOB_SEQ,
				 JOB_NAME,
				 JOB_MESG,
				 JOB_TIME,
				 SUC_CNT,
				 ERR_CNT,
				 UPD_BY,
				 UPD_DATE,
				 UPD_PGM,
				 STATUS)
			VALUES
				(N_ID,
				 M_SEQ + 0.1,
				 C_JOB_NAME || G_ENTITY,
				 'Post to OCC_INTERFACE_' || G_ENTITY || ' Complete',
				 SYSDATE,
				 G_CNT_ROWS_EXPORT,
				 G_CNT_ROW_ERROR,
				 'AUTO',
				 SYSDATE,
				 'BACK-JOB',
				 'Complete Process ' || G_PROCESSID);
			GOTO EXIT_PROCESS_2;
		EXCEPTION
			WHEN OTHERS THEN
				NULL;
		END EXIT_PROCESS;

		<<END_PROCESS_2>>
		BEGIN
			-->> Initial log
			GR_LOG.PROCESS_ID	:=  G_PROCESSID;
			GR_LOG.INF_UPD_BY	:=  'AUTO';
			GR_LOG.INF_UPD_PGM	:=  'BACK-JOB';

			-->> Lock Transaction Pending.
			GR_LOG.STEP				:=  7;
			GR_LOG.STEP_DESC		:=  'Error Interface';
			GR_LOG.PROCESS_FLAG		:=  'Error';
			GR_LOG.INF_LOCK_FLAG	:=  'Unlock';

			GR_LOG.PROCESS_DATE	:=  SYSDATE;
			GR_LOG.SEQ			:=  0;
			GR_LOG.LOG_CODE		:=  G_PROCESSID || ' Error Sending Email';
			GR_LOG.LOG_DESC		:=  v_result;
			GR_LOG.INF_UPD_DATE	:=  SYSDATE;
			WRITE_LOG(GR_LOG);

			G_RESULT_STATUS	:=  'FAILED';
/*			REMOVE_PROCESS  ('AUTO'
							,'BACK-JOB');*/
		END END_PROCESS_2;

		<<EXIT_PROCESS_2>>
		--V_WORK := CLEAR_JOB_WORK(N_NO);
		COMMIT;
	END IF;
END main_price_list;
--------
PROCEDURE main_discount
(p_ou_code			IN	pb_company.ou_code%TYPE
,p_subinv_code		IN	pb_subinv.subinv_code%TYPE
,p_cutoff_eff_date	IN	VARCHAR2
,p_cutoff_back_days	IN	NUMBER
,p_mode				IN	VARCHAR2
,p_log_back_days	IN	NUMBER
,p_output_path		IN	VARCHAR2
,p_field_separator	IN	VARCHAR2
,p_mail_to			IN	VARCHAR2	-- Send email to ?? upon completion
,N_NO				IN	NUMBER
,N_ID				IN	NUMBER)
IS
	V_RESULT	VARCHAR2(400) := 'Default';
	V_WORK		VARCHAR2(1);
	SUC_CNT		NUMBER;
	ERR_CNT		NUMBER;
	M_SEQ		NUMBER;
	v_dir_obj_name	VARCHAR2(100)	:=	'OCC_POS_DISCNT_DIR';
BEGIN
	G_ENTITY	:=	'DISCOUNT';
	SELECT  MAX(j.broken)
	INTO	G_JOB_BROKEN
	FROM	all_jobs j
	WHERE   j.job   =   N_NO;

	G_DIRECTORY :=  NULL;   -- Clear value
	G_RESULT_STATUS :=  'SUCCESS';
	
	FOR rec IN (SELECT	directory_name
				FROM	all_directories
				WHERE	directory_path  =   p_output_path
				AND		directory_name LIKE v_dir_obj_name || '%')
	LOOP
		G_DIRECTORY :=  rec.directory_name;
		EXIT;
	END LOOP;
	
	IF G_DIRECTORY IS NULL THEN
		SELECT  NVL(MAX(directory_name),v_dir_obj_name)
		INTO	G_DIRECTORY
		FROM	all_directories
		WHERE   directory_name LIKE v_dir_obj_name || '%';
		
		IF G_DIRECTORY = v_dir_obj_name THEN
			G_DIRECTORY :=  G_DIRECTORY || '_001';
		ELSE
			G_DIRECTORY :=  v_dir_obj_name || '_' || TO_CHAR(TO_NUMBER(SUBSTR(G_DIRECTORY,LENGTH(v_dir_obj_name)+2))+1,'fm000');
		END IF;
		
		EXECUTE IMMEDIATE 'CREATE DIRECTORY ' || G_DIRECTORY || ' AS ''' || p_output_path || '''';
	END IF;
	G_DIRECTORY :=  UPPER(G_DIRECTORY); -- Make sure we always refer directory name with uppercase

-- Set parameters
	G_OU_CODE			:=	NVL(p_ou_code,'%');
	G_SUBINV_CODE		:=	NVL(p_subinv_code,'%');
	G_MODE				:=	NVL(p_mode,'I');	-- I = Incremental, F = Full
	G_CUTOFF_BACK_DAYS	:=	NVL(p_cutoff_back_days,0);
	IF p_cutoff_eff_date IS NULL THEN
		G_CUTOFF_DATE	:=	TRUNC(SYSDATE) + G_CUTOFF_BACK_DAYS;
	ELSE
		G_CUTOFF_DATE	:=	TO_DATE(p_cutoff_eff_date,'DD/MM/RRRR');
	END IF;
	G_LOG_BACK_DAYS	 :=  p_log_back_days;
	G_N_NO			  :=  n_no;
	G_N_ID			  :=  n_id;

-- Clear log
	G_INF_SEQ   :=  1;
	clear_log(G_ENTITY);
	
	G_PROCESSID :=  NULL;   -- Clear value

	-->> Get Process ID Running.
	IF G_PROCESSID IS NULL THEN
		BEGIN
			SELECT  LPAD(NVL(TO_NUMBER(PARAMETER_VALUE), 1), 5, 0)
			INTO	G_PROCESSID
			FROM	PB_PARAMETER_SETUP
			WHERE   PGM_SETUP = 'OCCIFPRC'
			AND	 PARAMETER_NAME = 'OCC Interface Price List Process ID';

			UPDATE  PB_PARAMETER_SETUP
			SET	 PARAMETER_VALUE = TO_CHAR(NVL(TO_NUMBER(PARAMETER_VALUE),0) + 1)
			WHERE   PGM_SETUP = 'OCCIFPRC'
			AND	 PARAMETER_NAME = 'OCC Interface Price List Process ID';
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				G_PROCESSID := '00001';
				INSERT INTO PB_PARAMETER_SETUP
					(PGM_SETUP
					,PARAMETER_NAME
					,PARAMETER_VALUE
					,TRANSFER
					,UPD_BY
					,UPD_DATE
					,UPD_PGM)
				VALUES
					('OCCIFPRC'
					,'OCC Interface Price List Process ID'
					,G_PROCESSID + 1	-- Next process id
					,SYSDATE
					,'AUTO'
					,SYSDATE
					,'BACK-JOB');
		END;
	END IF;

	glb_text_output_util.g_process_id   :=  G_PROCESSID;

	-- CHECK WORK CONDITION
	V_WORK := 'Y';
	--V_WORK := CHECK_JOB_WORK(N_NO);
	
	IF NVL(V_WORK, 'N') = 'Y' THEN
		G_CNT_ROWS_EXPORT	:=	0;
		G_CNT_ROW_ERROR		:=	0;
		
		-- WRITE START PROCESS
		M_SEQ := GET_MONITOR_SEQ(N_ID);
		
		BEGIN
			INSERT INTO JOBS_MONITOR
				(JOB_ID,
				 JOB_SEQ,
				 JOB_NAME,
				 JOB_MESG,
				 JOB_TIME,
				 SUC_CNT,
				 ERR_CNT,
				 UPD_BY,
				 UPD_DATE,
				 UPD_PGM,
				 STATUS)
			VALUES
				(N_ID,
				 M_SEQ,
				 C_JOB_NAME || G_ENTITY,
				 'Start to run : OCC_INTERFACE_' || G_ENTITY,
				 SYSDATE,
				 G_CNT_ROWS_EXPORT,
				 G_CNT_ROW_ERROR,
				 'AUTO',
				 SYSDATE,
				 'BACK-JOB',
				 'Start Run Process ' || G_PROCESSID);
		EXCEPTION
			WHEN OTHERS THEN
				NULL;
		END;
		
/*		start_process('AUTO'
					 ,'BACK-JOB');
		-- @@@ Validate
		validate('AUTO'
				,'BACK-JOB'
				,v_result);
		IF v_result NOT LIKE 'Complete%' THEN
			GOTO end_process;
		END IF;
*/
		-- @@@ Prepare Data @@@ --
		prepare_discount	('AUTO'
							,'BACK-JOB');
		
/*		ending_process  ('AUTO'
						,'BACK-JOB');
*/		
		--> SENT INTERFACE TO TEXT FILE
		G_FILENAME  :=  'POS_DISCOUNT_' || TO_CHAR(SYSDATE,'YYYYMMDD_HHMISS');
		sent_interface_discount	('AUTO'
								,'BACK-JOB'
								,G_DIRECTORY
								,v_result);
		IF v_result <> glb_text_output_util.NO_ERROR THEN
			GOTO end_process;
		END IF;
		GOTO EXIT_PROCESS;
		-- @@@	 End	  @@@ --
		<<END_PROCESS>>
		BEGIN
			-->> Initial log
			GR_LOG.PROCESS_ID	:=  G_PROCESSID;
			GR_LOG.INF_UPD_BY	:=  'AUTO';
			GR_LOG.INF_UPD_PGM	:=  'BACK-JOB';

			-->> Lock Transaction Pending.
			GR_LOG.STEP				:=  6;
			GR_LOG.STEP_DESC		:=  'Error Interface';
			GR_LOG.PROCESS_FLAG		:=  'Error';
			GR_LOG.INF_LOCK_FLAG	:=  'Unlock';

			GR_LOG.PROCESS_DATE	:=  SYSDATE;
			GR_LOG.SEQ			:=  0;
			GR_LOG.LOG_CODE		:=  G_PROCESSID || ' Error Generating file';
			GR_LOG.LOG_DESC		:=  v_result;
			GR_LOG.INF_UPD_DATE	:=  SYSDATE;
			WRITE_LOG(GR_LOG);

			G_RESULT_STATUS :=  'FAILED';
/*			REMOVE_PROCESS  ('AUTO'
							,'BACK-JOB');*/
		END END_PROCESS;
		
		<<EXIT_PROCESS>>
	-- WRITE END PROCESS

		-- Send email
		send_email  (i_to_email		=>  'ERPOperationSupport@dtac.co.th'
					,i_cc_email		=>  NULL
					,i_bcc_email	=>  NULL
					,io_status		=>  v_result
					);
		IF v_result <> glb_text_output_util.NO_ERROR THEN
			GOTO end_process_2;
		END IF;

		BEGIN
			INSERT INTO JOBS_MONITOR
				(JOB_ID,
				 JOB_SEQ,
				 JOB_NAME,
				 JOB_MESG,
				 JOB_TIME,
				 SUC_CNT,
				 ERR_CNT,
				 UPD_BY,
				 UPD_DATE,
				 UPD_PGM,
				 STATUS)
			VALUES
				(N_ID,
				 M_SEQ + 0.1,
				 C_JOB_NAME || G_ENTITY,
				 'Post to OCC_INTERFACE_' || G_ENTITY || ' Complete',
				 SYSDATE,
				 G_CNT_ROWS_EXPORT,
				 G_CNT_ROW_ERROR,
				 'AUTO',
				 SYSDATE,
				 'BACK-JOB',
				 'Complete Process ' || G_PROCESSID);
			GOTO EXIT_PROCESS_2;
		EXCEPTION
			WHEN OTHERS THEN
				NULL;
		END EXIT_PROCESS;

		<<END_PROCESS_2>>
		BEGIN
			-->> Initial log
			GR_LOG.PROCESS_ID	:=  G_PROCESSID;
			GR_LOG.INF_UPD_BY	:=  'AUTO';
			GR_LOG.INF_UPD_PGM	:=  'BACK-JOB';

			-->> Lock Transaction Pending.
			GR_LOG.STEP				:=  7;
			GR_LOG.STEP_DESC		:=  'Error Interface';
			GR_LOG.PROCESS_FLAG		:=  'Error';
			GR_LOG.INF_LOCK_FLAG	:=  'Unlock';

			GR_LOG.PROCESS_DATE	:=  SYSDATE;
			GR_LOG.SEQ			:=  0;
			GR_LOG.LOG_CODE		:=  G_PROCESSID || ' Error Sending Email';
			GR_LOG.LOG_DESC		:=  v_result;
			GR_LOG.INF_UPD_DATE	:=  SYSDATE;
			WRITE_LOG(GR_LOG);

			G_RESULT_STATUS	:=  'FAILED';
/*			REMOVE_PROCESS  ('AUTO'
							,'BACK-JOB');*/
		END END_PROCESS_2;

		<<EXIT_PROCESS_2>>
		--V_WORK := CLEAR_JOB_WORK(N_NO);
		COMMIT;
	END IF;
END main_discount;
-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@--

END OCC_TRX_OUTBOUND_ITF;
/
