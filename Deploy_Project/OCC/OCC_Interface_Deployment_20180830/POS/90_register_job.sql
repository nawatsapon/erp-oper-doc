/*
select * FROM tab WHERE tname LIKE '%PARAM%';

SELECT * FROM jobs_setup;	-- Submit job
SELECT * FROM jobs_proc;	-- Register job
SELECT * FROM PB_PARAMETER_SETUP;
SELECT * FROM sa_param_lists;
SELECT * FROM sa_param_set;
*/

INSERT INTO jobs_proc
(seq, 
job_name, 
job_desc, 
proc_name, 
function_id, 
upd_by, 
upd_date, 
upd_pgm)
VALUES
((SELECT MAX(seq)+1 FROM jobs_proc), 
'OCC : Price List Outbound Interface from POS', 
'Price List Outbound Interface from POS (wait for sftp file by crontab)', 
'OCC_TRX_OUTBOUND_ITF.main_price_list', 
'DTJPPC19', 
'SS-STAFF', 
SYSDATE, 
'DTPBRT14');

INSERT INTO jobs_proc
(seq, 
job_name, 
job_desc, 
proc_name, 
function_id, 
upd_by, 
upd_date, 
upd_pgm)
VALUES
((SELECT MAX(seq)+1 FROM jobs_proc), 
'OCC : Discount Outbound Interface from POS', 
'Discount Outbound Interface from POS (wait for sftp file by crontab)', 
'OCC_TRX_OUTBOUND_ITF.main_discount', 
'DTJPPC19', 
'SS-STAFF', 
SYSDATE, 
'DTPBRT14');
/*
OCC_TRX_OUTBOUND_ITF.main_price_list('TAC','02646','',1,'I',,90,'/MP/GERP_TEST/DTACONLINE/OUTPUT/PRICELIST/INBOX','|','ERPOperationSupport@dtac.co.th',0,0)

DECLARE
	X        NUMBER;
BEGIN
	dbms_job.submit
		(job => x
		,what => 'OCC_TRX_OUTBOUND_ITF.main_price_list(''TAC'',''02646'','''',1,''I'',90,''/MP/GERP_TEST/DTACONLINE/OUTPUT/PRICELIST/INBOX'',''|'',''ERPOperationSupport@dtac.co.th'',0,0);');
END;

select * FROM JOBS_LOG
ORDER BY job_seq DESC;

SELECT * FROM dba_jobs;
*/
