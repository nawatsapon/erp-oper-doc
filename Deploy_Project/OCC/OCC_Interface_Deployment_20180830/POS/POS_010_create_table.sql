CREATE GLOBAL TEMPORARY TABLE tac_occ_pos_price_list_tmp 
(process_id			NUMBER(22,2)
,ou_code			VARCHAR2(3)
,pricelist_code		VARCHAR2(10)
,item_code			VARCHAR2(20)
,std_price			NUMBER(12,2)
,start_date			DATE
,end_date			DATE
,shop_cond			VARCHAR2(1)
,cust_cond			VARCHAR2(1)
,subinv_code		VARCHAR2(10)
,cat_code			VARCHAR2(5)
,process_date		DATE
)
ON COMMIT PRESERVE ROWS;

CREATE INDEX tac_occ_pos_price_list_tmp_n1 ON tac_occ_pos_price_list_tmp (process_id, ou_code, pricelist_code, item_code, subinv_code, cat_code);



CREATE TABLE tac_occ_pos_price_list_sent
(ou_code				VARCHAR2(3)
,pricelist_code			VARCHAR2(10)
,item_code				VARCHAR2(20)
,std_price				NUMBER(12,2)
,start_date				DATE
,end_date				DATE
,shop_cond				VARCHAR2(1)
,cust_cond				VARCHAR2(1)
,subinv_code			VARCHAR2(10)
,cat_code				VARCHAR2(5)
,process_id_first_sent	NUMBER
,obsolete_record		VARCHAR2(1)
,process_id_4obsolete	NUMBER
);

CREATE INDEX tac_occ_pos_price_list_sent_n1 ON tac_occ_pos_price_list_sent (ou_code, pricelist_code, item_code, subinv_code, cat_code);

grant select on tac_occ_pos_price_list_sent to SELECTED_POS_ROLE;



CREATE GLOBAL TEMPORARY TABLE tac_occ_pos_discount_tmp 
(process_id			NUMBER(22,2)
,discount_code		VARCHAR2(10)
,discount_desc		VARCHAR2(100)
,discount_level		VARCHAR2(1)
,disc_start			DATE
,disc_end			DATE
,disc_perc			NUMBER(5,2)
,disc_amt			NUMBER(12,2)
,shop_cond			VARCHAR2(1)
,cust_cond			VARCHAR2(1)
,status				VARCHAR2(1)
,subinv_code		VARCHAR2(10)
,cat_code			VARCHAR2(5)
,process_date		DATE
)
ON COMMIT PRESERVE ROWS;

CREATE INDEX tac_occ_pos_discount_tmp_n1 ON tac_occ_pos_discount_tmp (process_id, discount_code, subinv_code, cat_code);



CREATE TABLE tac_occ_pos_discount_sent
(discount_code			VARCHAR2(10)
,discount_desc			VARCHAR2(100)
,discount_level			VARCHAR2(1)
,disc_start				DATE
,disc_end				DATE
,disc_perc				NUMBER(5,2)
,disc_amt				NUMBER(12,2)
,shop_cond				VARCHAR2(1)
,cust_cond				VARCHAR2(1)
,status					VARCHAR2(1)
,subinv_code			VARCHAR2(10)
,cat_code				VARCHAR2(5)
,process_id_first_sent	NUMBER
,obsolete_record		VARCHAR2(1)
,process_id_4obsolete	NUMBER
);

CREATE INDEX tac_occ_pos_discount_sent_n1 ON tac_occ_pos_discount_sent (discount_code, subinv_code, cat_code);

grant select on tac_occ_pos_discount_sent to SELECTED_POS_ROLE;

-- Create table
create table TAC_OCC_OUTB_INF_LOG
(
  entity	    VARCHAR2(100),
  process_id    NUMBER,
  process_date  DATE,
  step          NUMBER,
  step_desc     VARCHAR2(400),
  seq           NUMBER,
  log_code      VARCHAR2(100),
  log_desc      VARCHAR2(400),
  process_flag  VARCHAR2(20),
  inf_lock_flag VARCHAR2(20),
  inf_upd_by    VARCHAR2(15),
  inf_upd_date  DATE,
  inf_upd_pgm   VARCHAR2(10)/*,
  ou_code       VARCHAR2(3),
  subinv_code   VARCHAR2(10),
  doc_no        VARCHAR2(15)*/
);
-- Add comments to the columns 
/*comment on column TAC_OCC_OUTB_INF_LOG.ou_code
  is 'OU Code';
comment on column TAC_OCC_OUTB_INF_LOG.subinv_code
  is 'Subinventory Code';
comment on column TAC_OCC_OUTB_INF_LOG.doc_no
  is 'Document Number';*/
-- Grant/Revoke object privileges 
grant select on TAC_OCC_OUTB_INF_LOG to SELECTED_POS_ROLE;

CREATE INDEX TAC_OCC_OUTB_INF_LOG_N1 ON TAC_OCC_OUTB_INF_LOG(entity, process_id, step, seq);
