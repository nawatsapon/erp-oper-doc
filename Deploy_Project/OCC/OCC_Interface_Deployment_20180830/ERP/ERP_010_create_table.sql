CREATE GLOBAL TEMPORARY TABLE tac_occ_erp_allocate_tmp 
(request_id				NUMBER
,inv_org_id				NUMBER			-- Added 25-Jul-2018
,warehouse_code			VARCHAR2(30)	-- Added 25-Jul-2018
,ou_code				VARCHAR2(30)	-- Added 25-Jul-2018
,order_number			VARCHAR2(40)
,document_date			DATE
,transaction_type		VARCHAR2(200)
,partner_code			VARCHAR2(40)
,product_code			VARCHAR2(40)
,serial_number			VARCHAR2(40)
,unit_selling_price		NUMBER(22,2)
,qty					NUMBER	-- Added 16-Jul-2018
,source_name			VARCHAR2(200)
,warranty_date			DATE
,expiry_date			DATE
,imei_number			VARCHAR2(100)
,telephone_number		VARCHAR2(100)
,transaction_group		VARCHAR2(50)
,transaction_id			NUMBER
,process_date			DATE
)
ON COMMIT PRESERVE ROWS;

CREATE INDEX tac_occ_erp_allocate_tmp_n1 ON tac_occ_erp_allocate_tmp (request_id, order_number, partner_code, product_code);
CREATE INDEX tac_occ_erp_allocate_tmp_n2 ON tac_occ_erp_allocate_tmp (request_id, transaction_group, transaction_id);



CREATE TABLE tac_occ_erp_allocate_sent
(order_number			VARCHAR2(40)
,inv_org_id				NUMBER			-- Added 25-Jul-2018
,warehouse_code			VARCHAR2(30)	-- Added 25-Jul-2018
,ou_code				VARCHAR2(30)	-- Added 25-Jul-2018
,document_date			DATE
,transaction_type		VARCHAR2(200)
,partner_code			VARCHAR2(40)
,product_code			VARCHAR2(40)
,serial_number			VARCHAR2(40)
,unit_selling_price		NUMBER(22,2)
,qty					NUMBER	-- Added 16-Jul-2018
,source_name			VARCHAR2(200)
,warranty_date			DATE
,expiry_date			DATE
,imei_number			VARCHAR2(100)
,telephone_number		VARCHAR2(100)
,transaction_group		VARCHAR2(50)
,transaction_id			NUMBER
,request_id_first_sent	NUMBER
,obsolete_record		VARCHAR2(1)
,request_id_4obsolete	NUMBER
);

CREATE INDEX tac_occ_erp_allocate_sent_n1 ON tac_occ_erp_allocate_sent (order_number, partner_code, product_code);
CREATE INDEX tac_occ_erp_allocate_sent_n2 ON tac_occ_erp_allocate_sent (transaction_group, transaction_id);



CREATE GLOBAL TEMPORARY TABLE tac_occ_erp_product_tmp 
(request_id			NUMBER
,ou_code			VARCHAR2(10)
,product_sku		VARCHAR2(100)
,product_name		VARCHAR2(1000)
,flag_serial		VARCHAR2(10)
,product_group		VARCHAR2(100)
,uom				VARCHAR2(30)
,item_status		VARCHAR2(100)
,process_date		DATE
)
ON COMMIT PRESERVE ROWS;

CREATE INDEX tac_occ_erp_product_tmp_n1 ON tac_occ_erp_product_tmp (request_id, ou_code, product_sku);



CREATE TABLE tac_occ_erp_product_sent
(ou_code				VARCHAR2(10)
,product_sku			VARCHAR2(100)
,product_name			VARCHAR2(1000)
,flag_serial			VARCHAR2(10)
,product_group			VARCHAR2(100)
,uom					VARCHAR2(30)
,item_status			VARCHAR2(100)
,request_id_first_sent	NUMBER
,obsolete_record		VARCHAR2(1)
,request_id_4obsolete	NUMBER
);

CREATE INDEX tac_occ_erp_product_sent_n1 ON tac_occ_erp_product_sent (ou_code, product_sku);
/*
-- Create table
create table TAC_OCC_OUTB_INF_LOG
(
  entity	    VARCHAR2(100),
  request_id    NUMBER,
  process_date  DATE,
  step          NUMBER,
  step_desc     VARCHAR2(400),
  seq           NUMBER,
  log_code      VARCHAR2(100),
  log_desc      VARCHAR2(400),
  process_flag  VARCHAR2(20),
  inf_lock_flag VARCHAR2(20),
  inf_upd_by    VARCHAR2(15),
  inf_upd_date  DATE,
  inf_upd_pgm   VARCHAR2(10)\*,
  ou_code       VARCHAR2(3),
  subinv_code   VARCHAR2(10),
  doc_no        VARCHAR2(15)*\
);
-- Add comments to the columns 
\*comment on column TAC_OCC_OUTB_INF_LOG.ou_code
  is 'OU Code';
comment on column TAC_OCC_OUTB_INF_LOG.subinv_code
  is 'Subinventory Code';
comment on column TAC_OCC_OUTB_INF_LOG.doc_no
  is 'Document Number';*\

CREATE INDEX TAC_OCC_OUTB_INF_LOG_N1 ON TAC_OCC_OUTB_INF_LOG(entity, request_id, step, seq);
*/
-- Create table
create table TAC_ENCRYPTION_PASSWORD
(
  key_name		    VARCHAR2(100) CONSTRAINT TAC_ENCRYPTION_PASSWORD_PK PRIMARY KEY,
  password_value    VARCHAR2(100) NOT NULL,
  description       VARCHAR2(4000),
  creation_date     DATE DEFAULT SYSDATE,
  created_by        VARCHAR2(100),
  last_update_date  DATE,
  last_updated_by   VARCHAR2(100),
  remark            VARCHAR2(4000)
);

INSERT INTO tac_encryption_password
(key_name, password_value, creation_date, created_by)
VALUES
('ERP2OCC_PASSWD','ERPPROD',SYSDATE,USER);

/*
CREATE OR REPLACE DIRECTORY TAC_OCC_PRODUCT_DIR AS '/MP/GERP_TEST/DTACONLINE/OUTPUT/PRODUCT/INBOX';
CREATE OR REPLACE DIRECTORY TAC_OCC_ALLOCATE_DIR AS '/MP/GERP_TEST/DTACONLINE/OUTPUT/ALLOCATE/INBOX';
*/

--SELECT * FROM all_directories WHERE directory_name LIKE '%OCC%';
--SELECT * FROM tac_encryption_password;
