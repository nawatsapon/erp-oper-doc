create table TAC_VAT_PERIOD_STATUS
(
  org_id                   NUMBER,
  location_id              NUMBER,
  period_name              VARCHAR2(100),
  latest_submit_num_closed NUMBER default -1,
  current_submit_num       NUMBER
);
create unique index TAC_VAT_PERIOD_STATUS_PK on TAC_VAT_PERIOD_STATUS (ORG_ID, LOCATION_ID, PERIOD_NAME);
