create global temporary table tac_ap_data_rpt_csv
(
  process_id    NUMBER not null,
  seq           NUMBER not null,
  message       VARCHAR2(4000),
  generate_date DATE
)
on commit preserve rows;
-- Create/Recreate indexes 
create index tac_ap_data_rpt_csv_idx on tac_ap_data_rpt_csv (process_id, seq);
