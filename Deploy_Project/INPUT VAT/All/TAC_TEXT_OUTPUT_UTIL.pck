CREATE OR REPLACE PACKAGE TAC_TEXT_OUTPUT_UTIL IS
/*
Date		By				Description
----------	--------------	---------------------------------------------
22/08/2017	TM				Created
*/

-- Constants for generating HTML output (used in bulk reporting)
NEW_LINE					CONSTANT VARCHAR2(10)	:=	CHR(13) || CHR(10);

HTML_HDR_DELIM_B			CONSTANT VARCHAR2(1000)	:=	'<B>';
HTML_HDR_DELIM_E			CONSTANT VARCHAR2(1000)	:=	'</B>' || NEW_LINE || '<P>';
HTML_HDR_SEPARATOR			CONSTANT VARCHAR2(1000)	:=	NULL;
HTML_HDR_PREFIX				CONSTANT VARCHAR2(1000)	:=	'<HTML>';

HTML_TBL_HDR_DELIM_B		CONSTANT VARCHAR2(1000)	:=	'<TH>';
HTML_TBL_HDR_DELIM_B_L		CONSTANT VARCHAR2(1000)	:=	'<TH align = "left">';
HTML_TBL_HDR_DELIM_E		CONSTANT VARCHAR2(1000)	:=	'</TH>' || NEW_LINE;
HTML_TBL_SEPARATOR			CONSTANT VARCHAR2(1000)	:=	NULL;
HTML_TBL_HDR_ROW_PREFIX		CONSTANT VARCHAR2(1000)	:=	'<TABLE BORDER=1><TR>';
HTML_TBL_HDR_ROW_PREFIX_NB	CONSTANT VARCHAR2(1000)	:=	'<TABLE BORDER=0><TR>';
HTML_TBL_HDR_ROW_SUFFIX		CONSTANT VARCHAR2(1000)	:=	'</TR>';

HTML_TBL_DATA_DELIM_B		CONSTANT VARCHAR2(1000)	:=	'<TD>';
HTML_TBL_DATA_DELIM_E		CONSTANT VARCHAR2(1000)	:=	'</TD>' || NEW_LINE;
HTML_TBL_DATA_ROW_PREFIX	CONSTANT VARCHAR2(1000)	:=	'<TR>';
HTML_TBL_DATA_ROW_SUFFIX	CONSTANT VARCHAR2(1000)	:=	'</TR>';

HTML_TBL_SUFFIX				CONSTANT VARCHAR2(1000)	:=	'</TABLE>';
HTML_FILE_SUFFIX			CONSTANT VARCHAR2(1000)	:=	'</HTML>';

C_ERROR						CONSTANT VARCHAR2(100)	:=	'Error : ';
NO_ERROR					CONSTANT VARCHAR2(100)	:=	'No Error';

TYPE varchar_tabtyp IS TABLE OF VARCHAR2(4000) INDEX BY BINARY_INTEGER;
TYPE number_tabtyp IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
g_process_id		NUMBER;

PROCEDURE replace_var
(io_full_text		IN OUT	VARCHAR2
,i_variable_name	IN		VARCHAR2
,i_replace_value	IN		VARCHAR2
,i_delimiter		IN		VARCHAR2	:=	'$$');

PROCEDURE set_format
(it_col_len		IN	number_tabtyp
,o_status		OUT	VARCHAR2
,i_clear_data	IN	VARCHAR2	:=	'Y');

PROCEDURE add_txt
(it_message			IN	varchar_tabtyp
,i_delimiter		IN	VARCHAR2	:=	'"'
,i_delimiter_end	IN	VARCHAR2	:=	NULL
,i_separator		IN	VARCHAR2	:=	','
,i_line_prefix		IN	VARCHAR2	:=	NULL
,i_line_suffix		IN	VARCHAR2	:=	NULL);

PROCEDURE write_log
(o_status		OUT	VARCHAR2);

PROCEDURE write_output
(o_status		OUT	VARCHAR2);

END TAC_TEXT_OUTPUT_UTIL; 
/
CREATE OR REPLACE PACKAGE BODY TAC_TEXT_OUTPUT_UTIL IS
/*
Date		By				Description
----------	--------------	---------------------------------------------
22/08/2017	TM				Created
*/

g_max_seq			NUMBER;
g_file_format		number_tabtyp;

PROCEDURE replace_var
(io_full_text		IN OUT	VARCHAR2
,i_variable_name	IN		VARCHAR2
,i_replace_value	IN		VARCHAR2
,i_delimiter		IN		VARCHAR2	:=	'$$')
IS
	v_old_value		VARCHAR2(1000)	:=	i_delimiter || i_variable_name || i_delimiter;
BEGIN
	io_full_text	:=	REPLACE(io_full_text, v_old_value, i_replace_value);
END replace_var;

PROCEDURE set_format
(it_col_len		IN	number_tabtyp
,o_status		OUT	VARCHAR2
,i_clear_data	IN	VARCHAR2	:=	'Y')
IS
PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
	IF it_col_len.COUNT = 0 THEN
		o_status	:=	C_ERROR || 'NO_COLUMN_DEFINED';
	ELSIF it_col_len.COUNT <> it_col_len.LAST THEN
		o_status	:=	C_ERROR || 'COLUMN_NOT_CORRECT';
	ELSE
		g_file_format	:=	it_col_len;
		o_status		:=	NO_ERROR;
	END IF;
-- Clear data
	IF i_clear_data = 'Y' THEN
		DELETE	tac_ap_data_rpt_csv  pl
		WHERE	pl.process_id = g_process_id;
	END IF;
	COMMIT;
END set_format;

PROCEDURE add_txt
(it_message			IN	varchar_tabtyp
,i_delimiter		IN	VARCHAR2	:=	'"'
,i_delimiter_end	IN	VARCHAR2	:=	NULL
,i_separator		IN	VARCHAR2	:=	','
,i_line_prefix		IN	VARCHAR2	:=	NULL
,i_line_suffix		IN	VARCHAR2	:=	NULL)
IS
PRAGMA AUTONOMOUS_TRANSACTION;
	CURSOR	get_max_seq IS
		SELECT	NVL(MAX(seq),0) max_seq
		FROM	tac_ap_data_rpt_csv  pl
		WHERE	pl.process_id = g_process_id;
	v_seq				NUMBER	:=	NULL;
	v_index				NUMBER	:=	0;
	tmp_text			VARCHAR2(32767)	:=	NULL;
	v_message			VARCHAR2(32767)	:=	NULL;
	v_col_txt			VARCHAR2(4000);
	v_delimiter_end		VARCHAR2(10)	:=	NVL(i_delimiter_end, i_delimiter);
	t_message			varchar_tabtyp	:=	it_message;
BEGIN
	IF g_max_seq IS NULL THEN
		FOR rec IN get_max_seq LOOP
			g_max_seq := rec.max_seq;
		END LOOP;
	END IF;
	g_max_seq	:=	g_max_seq + 1;
	v_seq		:=	g_max_seq;

	IF i_delimiter IS NOT NULL THEN
		FOR i IN 1..t_message.count LOOP
			t_message(i)	:=	REPLACE(t_message(i),i_delimiter,i_delimiter||i_delimiter);
			IF t_message(i) IS NOT NULL THEN
				-- Text column contains pure numbers beginning with 0, must add prefix '
				IF g_file_format(i) IS NOT NULL AND g_file_format(i) <> -1 AND t_message(i) IS NOT NULL AND LTRIM(RTRIM(TRANSLATE(t_message(i),'0123456789/.-',' '))) IS NULL /*AND t_message(i) LIKE '0%'*/ THEN
					t_message(i)	:=	'''' || t_message(i);
				END IF;
				-- Header of number column contains , in content (when separator is provided)
				IF g_file_format(i) IS NULL AND t_message(i) IS NOT NULL AND i_separator IS NOT NULL AND t_message(i) LIKE '%' || i_separator || '%' THEN
					t_message(i)	:=	i_delimiter||t_message(i)||v_delimiter_end;
				END IF;
			END IF;
		END LOOP;
	END IF;

	FOR i IN 1..t_message.count LOOP
		IF NOT g_file_format.EXISTS(i) THEN
			g_file_format(i)	:=	NULL;
		END IF;
		IF NVL(g_file_format(i),0) <= 0 THEN
			v_col_txt	:=	t_message(i);
		ELSE
			v_col_txt	:=	RPAD(NVL(t_message(i),' '),g_file_format(i));
		END IF;
		-- If column length is null, not put delimiter
		IF g_file_format(i) IS NULL THEN
			tmp_text	:=	v_col_txt;
		ELSE
			tmp_text	:=	i_delimiter||v_col_txt||v_delimiter_end;
		END IF;

		v_message	:=	v_message||i_separator||tmp_text;
	END LOOP;

	-- Remove prefix separator if any
	v_message	:=	SUBSTR(v_message, NVL(LENGTH(i_separator),0) + 1);
	v_message	:=	i_line_prefix || v_message || i_line_suffix;

	IF g_process_id IS NOT NULL THEN
		INSERT INTO tac_ap_data_rpt_csv
			(process_id
			,seq
			,message
			,generate_date)
		VALUES
			(g_process_id
			,NVL(v_seq,1)
			,v_message
			,SYSDATE);
--		dbms_output.put_line('v_message ' || v_message);
	END IF;

	COMMIT;
END add_txt;

PROCEDURE write_log
(o_status		OUT	VARCHAR2)
IS
PRAGMA AUTONOMOUS_TRANSACTION;
	CURSOR Q1 IS
		SELECT pl.*
		FROM tac_ap_data_rpt_csv pl
		WHERE pl.process_id = g_process_id
		ORDER BY pl.seq;
	v_file			UTL_FILE.FILE_TYPE;
	v_lock_file_cmd	VARCHAR2(1000);
	v_file_name		VARCHAR2(1000);
	v_file_loc		VARCHAR2(1000);
	v_message		VARCHAR2(4000);
BEGIN
	v_message := NULL;

	FOR indx IN Q1 LOOP
		v_message := indx.message;
		fnd_file.put_line(fnd_file.log, v_message);
	END LOOP;
	
	o_status	:=	NO_ERROR;

	COMMIT;
EXCEPTION
	WHEN OTHERS THEN
		ROLLBACK;
		o_status	:=	SQLCODE||' : '||SQLERRM;
END write_log;

PROCEDURE write_output
(o_status		OUT	VARCHAR2)
IS
PRAGMA AUTONOMOUS_TRANSACTION;
	CURSOR Q1 IS
		SELECT pl.*
		FROM tac_ap_data_rpt_csv pl
		WHERE pl.process_id = g_process_id
		ORDER BY pl.seq;
	v_file			UTL_FILE.FILE_TYPE;
	v_lock_file_cmd	VARCHAR2(1000);
	v_file_name		VARCHAR2(1000);
	v_file_loc		VARCHAR2(1000);
	v_message		VARCHAR2(4000);
BEGIN
	v_message := NULL;

	FOR indx IN Q1 LOOP
		v_message := indx.message;
		fnd_file.put_line(fnd_file.output, v_message);
	END LOOP;
	
	o_status	:=	NO_ERROR;

	COMMIT;
EXCEPTION
	WHEN OTHERS THEN
		ROLLBACK;
		o_status	:=	SQLCODE||' : '||SQLERRM;
END write_output;

END TAC_TEXT_OUTPUT_UTIL;
/
