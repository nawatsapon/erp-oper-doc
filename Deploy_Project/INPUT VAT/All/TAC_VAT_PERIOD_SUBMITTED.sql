create table TAC_VAT_PERIOD_SUBMITTED
(
  org_id            NUMBER,
  location_id       NUMBER,
  period_name       VARCHAR2(100),
  submit_num_closed NUMBER
);

create unique index TAC_VAT_PERIOD_SUBMITTED_PK on TAC_VAT_PERIOD_SUBMITTED (ORG_ID, LOCATION_ID, PERIOD_NAME, SUBMIT_NUM_CLOSED);
