SET SCAN OFF;

CREATE OR REPLACE PACKAGE TH_LOAD_LOCALIZATION_V2 IS

  /*==================================================================
    PROCEDURE NAME: TH_LOAD_CANCEL_TAXINV_GOODS

    DESCRIPTION:  This Api for load cancel data from Tax Invoices Service to
                  Thai Localization

    PARAMETERS: P_CONC_REQUEST_ID  IN  NUMBER,
      P_CHECK_ID   IN  NUMBER,

    DESIGN
    REFERENCES:

    CHANGE    Created   09-NOV-2017 TM : VAT & WHT CR
    HISTORY:

  =======================================================================*/
  /*PROCEDURE th_load_cancel_taxinv_goods
  (
    p_conc_request_id IN NUMBER
   ,p_org_id IN NUMBER
  );*/	
  
  /*==================================================================
    PROCEDURE NAME: TH_LOAD_TAXINV_GOODS

    DESCRIPTION:  This Api for load data from Tax Invoices Goods to
                  Thai Localization

    PARAMETERS: P_CONC_REQUEST_ID  IN  NUMBER,
      P_INVOICE_ID   IN  NUMBER,

    DESIGN
    REFERENCES:

    CHANGE    Created   01-OCT-2008 DAMRONG TANTIWIWATSAKUL
    HISTORY:

  =======================================================================*/

  PROCEDURE th_load_taxinv_goods
  (
    p_conc_request_id IN NUMBER
   ,p_invoice_id IN NUMBER
   ,p_org_id IN NUMBER
  ); --add by aeaum 23/09/2009--

  /*==================================================================
    PROCEDURE NAME: TH_LOAD_TAXINV_SERVICE

    DESCRIPTION:  This Api for load data from Tax Invoices Service to
                  Thai Localization

    PARAMETERS: P_CONC_REQUEST_ID  IN  NUMBER,
      P_CHECK_ID   IN  NUMBER,

    DESIGN
    REFERENCES:

    CHANGE    Created   01-OCT-2008 DAMRONG TANTIWIWATSAKUL
    HISTORY:

  =======================================================================*/

  PROCEDURE th_load_taxinv_service
  (
    p_conc_request_id IN NUMBER
   ,p_check_id IN NUMBER
   ,p_org_id IN NUMBER
  ); --add by aeaum 23/09/2009--

  /*==================================================================
    PROCEDURE NAME: TH_LOAD_WHT_A

    DESCRIPTION:  This Api for load data from Withholding Tax Type Auto to
                  Thai Localization

    PARAMETERS: P_CONC_REQUEST_ID  IN  NUMBER,
      P_CHECK_ID   IN  NUMBER,

    DESIGN
    REFERENCES:

    CHANGE    Created   01-OCT-2008 DAMRONG TANTIWIWATSAKUL
    HISTORY:

  =======================================================================*/

  PROCEDURE th_load_wht_a
  (
    p_conc_request_id IN NUMBER
   ,p_check_id IN NUMBER
   ,p_org_id IN NUMBER
  ); --add by aeaum 23/09/2009--

  /*==================================================================
    PROCEDURE NAME: TH_LOAD_WHT_M

    DESCRIPTION:  This Api for load data from Withholding Tax Type Manual to
                  Thai Localization

    PARAMETERS: P_CONC_REQUEST_ID  IN  NUMBER,
      P_INVOICE_ID   IN  NUMBER,

    DESIGN
    REFERENCES:

    CHANGE    Created   01-OCT-2008 DAMRONG TANTIWIWATSAKUL
    HISTORY:

  =======================================================================*/

  PROCEDURE th_load_wht_m
  (
    p_conc_request_id IN NUMBER
   ,p_invoice_id IN NUMBER
   ,p_org_id IN NUMBER
  ); --add by aeaum 23/09/2009--

  /*==================================================================
    PROCEDURE NAME: write_log

    DESCRIPTION:  This Api for write message to log file

    PARAMETERS: param_msg  IN  VARCHAR2

    DESIGN
    REFERENCES:

    CHANGE    Created   01-OCT-2008 DAMRONG TANTIWIWATSAKUL
    HISTORY:

  =======================================================================*/
  PROCEDURE write_log(param_msg VARCHAR2);
        
END TH_LOAD_LOCALIZATION_V2;
/
CREATE OR REPLACE PACKAGE BODY TH_LOAD_LOCALIZATION_V2 IS

	/** V 1.0 by Companion **/
	/** V.2.0 by ptj iCE Consulting **/
	/**       - Insert TH_ACT_TRANSACTION_DATE in WHT Header Table for both Auto  ***/
	/**   - Split WHT Sequence for Automatice (PAY) and Manual ***/
	/**           WHT Sequence for Automatice format TH_WHT_A_ ***/
	/**           WHT Sequence for Manual    format TH_WHT_M_ ***/
	/** V.3.0 by ptj iCE Consulting **/
	/**       - Fix case Void Payment and Cancel Invoice before Tax Invoice load @th_load_taxinv_service **/
	/** V.4.0 by ptj iCE Consulting **/
	/**       - Change  Get Supplier Address  **/
	/** V.4.1 Temporary Fix Bug from V.3.0 @th_load_taxinv_service **/

	/*Added By AP@BAS  on 16-Nov-2012
    /*Added By YVK@iCE on 28-Jun-2016 **/
	/* Alter table th_itemtax_distribution_loaded add field created_by , creation_date
    =====================================================================================
    Function calculate_time use to calculate time used in loop
    =====================================================================================
    */
	PROCEDURE write_output4test(param_msg VARCHAR2);
	/** VAT & WHT CR by TM on 9/11/2017 **/
	FUNCTION calculate_time(p_start_date DATE, p_finish_date DATE)
	RETURN VARCHAR2 IS
		v_days    VARCHAR2(20);
		v_hours   VARCHAR2(20);
		v_minutes VARCHAR2(20);
	BEGIN
	
		IF p_start_date IS NULL OR p_finish_date IS NULL THEN
			RETURN 'Time cannot be empty.';
		ELSE
			SELECT trunc(diff) days,
				   trunc(MOD(diff, trunc(diff)) * 24) hours,
				   round(MOD((MOD(diff, trunc(diff)) * 24), trunc((MOD(diff, trunc(diff)) * 24))) * 60, 1) minutes
			  INTO v_days, v_hours, v_minutes
			  FROM (SELECT p_start_date, p_finish_date, p_finish_date - p_start_date diff
					  FROM dual);
		
			RETURN(v_days || ':' || v_hours || ':' || v_minutes);
		END IF;
	END calculate_time;

	/*================================================================
      PROCEDURE NAME:   reverse_taxinv_goods()
      Created by TM 9/11/2017 : VAT & WHT CR  
    ==================================================================*/
	PROCEDURE reverse_taxinv_goods(p_conc_request_id IN NUMBER,
								   p_invoice_id      IN NUMBER,
								   p_org_id          IN NUMBER) IS
	
		v_tax_id           VARCHAR2(20);
		v_branch_name      VARCHAR2(150);
		v_actual_sup_name  VARCHAR2(250);
		v_vendor_site_code      VARCHAR2(30);
		v_vendor_id        NUMBER;
		v_cancel_flag      VARCHAR2(1) := 'N';
		v_amt_by_tax       NUMBER;
		v_comfirmed_flag   VARCHAR2(1);
		v_tran_cancel_flag VARCHAR2(1) := 'Y';
	
		user_id NUMBER;
		-- Cursor for Load Data to Table : th_taxinvoices_header and th_taxinvoices_line
		CURSOR my_cursor IS
			SELECT ai.invoice_id transaction_id,
				   ai.invoice_num transaction_number,
				   ai.invoice_date transaction_date,
				   trunc(aid.accounting_date) gl_date,
				   'G' type_tax,
				   atc.name tax_code,
				   atc.tax_recovery_rate tax_rate,
				   hla.location_id,
				   decode(ai.doc_sequence_id, NULL, ai.voucher_num, ai.doc_sequence_value) voucher_number,
				   NULL bank_acct_number,
				   NULL bank_acct_name,
				   aid.project_id,
				   pv.vendor_id supplier_id,
				   pv.vendor_name supplier_name,
				   user_id created_by,
				   SYSDATE creation_date,
				   user_id last_updated_by,
				   SYSDATE last_update_date,
				   user_id last_update_login,
				   ai.org_id,
				   aid.attribute4 actual_sup_number,
				   aid.attribute5 actual_site_code,
				   ai.vendor_site_id supplier_site_id
			  FROM ap_invoices              ai,
				   ap_invoice_distributions aid,
				   ap_tax_codes             atc,
				   po_vendors               pv,
				   hr_locations             hla,
				   ap_ae_lines_all          xap
			 WHERE ai.invoice_id = aid.invoice_id
			   AND aid.tax_code_id = atc.tax_id
			   AND atc.global_attribute20 = 'N'
			   AND aid.line_type_lookup_code = 'ITEM'
			   AND EXISTS (SELECT 'x'
					  FROM ap_invoice_distributions
					 WHERE invoice_id = ai.invoice_id
					   AND line_type_lookup_code = 'TAX')
			   AND ai.vendor_id = pv.vendor_id
			   AND EXISTS (SELECT 'x'
					  FROM th_itemtax_distribution_loaded tidl
					 WHERE tidl.invoice_type = 'TAXINV'
					   AND tidl.invoice_distribution_id = aid.invoice_distribution_id)
			   AND NOT EXISTS
			 (SELECT 'x'
					  FROM th_itemtax_distribution_loaded tidl
					 WHERE tidl.invoice_type = 'TAXCCINV'
					   AND tidl.invoice_distribution_id = aid.invoice_distribution_id)
			   AND atc.global_attribute19 = hla.location_id
			   AND xap.source_table = 'AP_INVOICE_DISTRIBUTIONS'
			   AND xap.source_id = aid.invoice_distribution_id
			   AND ai.invoice_id = p_invoice_id
			   AND ai.org_id = p_org_id
			 GROUP BY ai.invoice_id,
					  ai.invoice_num,
					  ai.invoice_date,
					  trunc(aid.accounting_date),
					  atc.name,
					  hla.location_id,
					  decode(ai.doc_sequence_id, NULL, ai.voucher_num, ai.doc_sequence_value),
					  aid.project_id,
					  pv.vendor_id,
					  pv.vendor_name,
					  atc.tax_recovery_rate,
					  ai.org_id,
					  aid.attribute4,
					  aid.attribute5,
					  ai.vendor_site_id
			 ORDER BY decode(ai.doc_sequence_id, NULL, ai.voucher_num, ai.doc_sequence_value),
					  ai.invoice_id,
					  atc.name,
					  aid.project_id;
	
		my_rec my_cursor%ROWTYPE;
		-- Cursor for Load Data to Table : th_itemtax_distribution_loaded
		CURSOR my_cursor_dis IS
			SELECT DISTINCT ai.invoice_id,
							aid.invoice_distribution_id invoice_distribution_id,
							ai.org_id,
							user_id                     created_by,
							SYSDATE                     creation_date
			  FROM ap_invoices              ai,
				   ap_invoice_distributions aid,
				   ap_tax_codes             atc,
				   po_vendors               pv,
				   hr_locations             hla,
				   ap_ae_lines_all          xap
			 WHERE ai.invoice_id = aid.invoice_id
			   AND aid.tax_code_id = atc.tax_id
			   AND atc.global_attribute20 = 'N'
			   AND aid.line_type_lookup_code IN ('TAX', 'ITEM')
			   AND EXISTS (SELECT 'x'
					  FROM ap_invoice_distributions
					 WHERE invoice_id = ai.invoice_id
					   AND line_type_lookup_code = 'TAX')
			   AND ai.vendor_id = pv.vendor_id
			   AND EXISTS (SELECT 'x'
					  FROM th_itemtax_distribution_loaded tidl
					 WHERE tidl.invoice_type = 'TAXINV'
					   AND tidl.invoice_distribution_id = aid.invoice_distribution_id)
			   AND NOT EXISTS
			 (SELECT 'x'
					  FROM th_itemtax_distribution_loaded tidl
					 WHERE tidl.invoice_type = 'TAXCCINV'
					   AND tidl.invoice_distribution_id = aid.invoice_distribution_id)
			   AND xap.source_table = 'AP_INVOICE_DISTRIBUTIONS'
			   AND xap.source_id = aid.invoice_distribution_id
			   AND atc.global_attribute19 = hla.location_id
			   AND ai.invoice_id = p_invoice_id
			   AND ai.org_id = p_org_id;
	
		my_rec_dis                    my_cursor_dis%ROWTYPE;
		v_head_id                     NUMBER;
		v_line_id                     NUMBER;
		base_amt                      NUMBER;
		rec_tax                       NUMBER;
		non_rec_tax                   NUMBER;
		tax_amt                       NUMBER;
		xproject_id                   NUMBER;
		xbproject                     BOOLEAN;
		v_insert                      BOOLEAN;
		v_nperiod                     VARCHAR(10);
		v_ai_invoice_id               NUMBER(15);
		v_ai_invoice_amount           NUMBER;
		v_ai_payment_status_flag      VARCHAR2(1 BYTE);
		v_ai_invoice_type_lookup_code VARCHAR2(25 BYTE);
	
	BEGIN
		user_id   := fnd_profile.value('USER_ID');
		xbproject := FALSE;
		v_insert  := FALSE;
		OPEN my_cursor;
		LOOP
			FETCH my_cursor
				INTO my_rec;
			EXIT WHEN my_cursor%NOTFOUND;
			SELECT th_taxinvh_s.nextval INTO v_head_id FROM dual;
			SELECT th_taxinvl_s.nextval INTO v_line_id FROM dual;
			rec_tax     := 0;
			non_rec_tax := 0;
			tax_amt     := 0;
			base_amt    := 0;
			write_log('Invoice Number : ' || my_rec.transaction_number);
			-- Calculate BASE AMOUNT
			write_log('10 begin : Calculate BASE AMOUNT');
			BEGIN
				SELECT nvl(SUM(nvl(xap.accounted_dr, 0) - nvl(xap.accounted_cr, 0)), 0) * (-1)
				  INTO base_amt
				  FROM ap_invoices              ai,
					   ap_invoice_distributions aid,
					   ap_tax_codes             atc,
					   hr_locations             hla,
					   ap_ae_lines_all          xap
				 WHERE ai.invoice_id = aid.invoice_id
				   AND aid.tax_code_id = atc.tax_id
				   AND atc.global_attribute20 = 'N'
				   AND aid.line_type_lookup_code = 'ITEM'
				   AND EXISTS (SELECT 'x'
						  FROM th_itemtax_distribution_loaded tidl
						 WHERE tidl.invoice_type = 'TAXINV'
						   AND tidl.invoice_distribution_id = aid.invoice_distribution_id)
				   AND NOT EXISTS
				 (SELECT 'x'
						  FROM th_itemtax_distribution_loaded tidl
						 WHERE tidl.invoice_type = 'TAXCCINV'
						   AND tidl.invoice_distribution_id = aid.invoice_distribution_id)
				   AND nvl(aid.project_id, 0) = nvl(my_rec.project_id, 0)
				   AND ai.invoice_id = my_rec.transaction_id
				   AND atc.global_attribute19 = hla.location_id
				   AND atc.name = my_rec.tax_code
				   AND xap.source_id = aid.invoice_distribution_id
				   AND xap.source_table = 'AP_INVOICE_DISTRIBUTIONS'
				   AND trunc(aid.accounting_date) = my_rec.gl_date
				   AND ai.org_id = p_org_id;
			EXCEPTION
				WHEN no_data_found THEN
					base_amt := 0;
			END;
			write_log('10 end : Calculate BASE AMOUNT');
			-- Calculate RECOVERABLE RATE AMOUNT
			write_log('20 begin : Calculate RECOVERABLE RATE AMOUNT');
			BEGIN
				SELECT nvl(SUM(nvl(aca1.allocated_base_amount, 0)), 0) * (-1)
				  INTO rec_tax
				  FROM ap_invoices              ai,
					   ap_invoice_distributions aid,
					   ap_tax_codes             atc,
					   hr_locations             hla,
					   ap_ae_lines_all          xap,
					   ap_chrg_allocations_all  aca1,
					   ap_invoice_distributions aid1,
					   ap_tax_codes             atc1
				 WHERE ai.invoice_id = aid.invoice_id
				   AND aid.tax_code_id = atc.tax_id
				   AND atc.global_attribute20 = 'N'
				   AND aid.line_type_lookup_code = 'TAX'
				   AND EXISTS (SELECT 'x'
						  FROM th_itemtax_distribution_loaded tidl
						 WHERE tidl.invoice_type = 'TAXINV'
						   AND tidl.invoice_distribution_id = aid.invoice_distribution_id)
				   AND NOT EXISTS
				 (SELECT 'x'
						  FROM th_itemtax_distribution_loaded tidl
						 WHERE tidl.invoice_type = 'TAXCCINV'
						   AND tidl.invoice_distribution_id = aid.invoice_distribution_id)
				   AND ai.invoice_id = my_rec.transaction_id
				   AND atc.global_attribute19 = hla.location_id
				   AND xap.source_id = aid.invoice_distribution_id
				   AND atc.name = my_rec.tax_code
				   AND aid.tax_recoverable_flag = 'Y'
				   AND xap.source_table = 'AP_INVOICE_DISTRIBUTIONS'
				   AND aca1.charge_dist_id = aid.invoice_distribution_id
				   AND aca1.item_dist_id = aid1.invoice_distribution_id
				   AND aid1.tax_code_id = atc1.tax_id
				   AND atc1.name = my_rec.tax_code
				   AND nvl(aid1.project_id, 0) = nvl(my_rec.project_id, 0)
				   AND aca1.item_dist_id IN
					   (SELECT invoice_distribution_id
						FROM ap_invoice_distributions aid,
							   ap_tax_codes             atc
						 WHERE aid.invoice_id = my_rec.transaction_id
						   AND atc.name = my_rec.tax_code
						   AND nvl(aid.project_id, 0) = nvl(my_rec.project_id, 0)
						   AND aid.tax_code_id = atc.tax_id
						   AND aid.line_type_lookup_code = 'ITEM')
				   AND trunc(aid.accounting_date) = my_rec.gl_date
				   AND ai.org_id = p_org_id;
			EXCEPTION
				WHEN no_data_found THEN
					rec_tax := 0;
			END;
			write_log('20 end : Calculate RECOVERABLE RATE AMOUNT');
			-- Calculate NON RECOVERABLE RATE AMOUNT
			write_log('30 begin : Calculate NON RECOVERABLE RATE AMOUNT');
			BEGIN
				SELECT nvl(SUM(nvl(xap.accounted_dr, 0) - nvl(xap.accounted_cr, 0)), 0) * (-1)
				  INTO non_rec_tax
				  FROM ap_invoices              ai,
					   ap_invoice_distributions aid,
					   ap_tax_codes             atc,
					   hr_locations             hla,
					   ap_ae_lines_all          xap
				 WHERE ai.invoice_id = aid.invoice_id
				   AND aid.tax_code_id = atc.tax_id
				   AND atc.global_attribute20 = 'N'
				   AND aid.line_type_lookup_code = 'TAX'
				   AND EXISTS (SELECT 'x'
						  FROM th_itemtax_distribution_loaded tidl
						 WHERE tidl.invoice_type = 'TAXINV'
						   AND tidl.invoice_distribution_id = aid.invoice_distribution_id)
				   AND NOT EXISTS
				 (SELECT 'x'
						  FROM th_itemtax_distribution_loaded tidl
						 WHERE tidl.invoice_type = 'TAXCCINV'
						   AND tidl.invoice_distribution_id = aid.invoice_distribution_id)
				   AND nvl(aid.project_id, 0) = nvl(my_rec.project_id, 0)
				   AND ai.invoice_id = my_rec.transaction_id
				   AND atc.global_attribute19 = hla.location_id
				   AND xap.source_id = aid.invoice_distribution_id
				   AND atc.name = my_rec.tax_code
				   AND aid.tax_recoverable_flag = 'N'
				   AND xap.source_table = 'AP_INVOICE_DISTRIBUTIONS'
				   AND trunc(aid.accounting_date) = my_rec.gl_date
				   AND ai.org_id = p_org_id;
			EXCEPTION
				WHEN no_data_found THEN
					non_rec_tax := 0;
			END;
			write_log('30 end : Calculate NON RECOVERABLE RATE AMOUNT');
			tax_amt := rec_tax + non_rec_tax;
			-- Find Period Name from GL DATE
			write_log('40 begin : Find Period Name from GL DATE');
			SELECT period_name
			  INTO v_nperiod
			  FROM gl_periods_v
			 WHERE period_set_name = 'Accounting' -- 'TCORE_Calendar'
			   AND (trunc(my_rec.gl_date) BETWEEN start_date AND end_date)
			   AND adjustment_period_flag = 'N'
			 ORDER BY start_date;
			write_log('40 end : Find Period Name from GL DATE');
		
			-- find vat_registeration_number / Branch Name from supplier_site table
			-- 45
			v_tax_id          := NULL;
			v_branch_name     := NULL;
			v_actual_sup_name := NULL;
			v_vendor_id       := NULL;
			v_vendor_site_code	:= NULL;
		
			IF my_rec.actual_sup_number IS NULL THEN
				BEGIN
					SELECT pv.vendor_name,
						   pv.vendor_id,
						   NVL(pvs.vat_registration_num, pv.vat_registration_num),
						   pvs.global_attribute14,
						   pvs.vendor_site_code
					  INTO v_actual_sup_name,
						   v_vendor_id,
						   v_tax_id,
						   v_branch_name,
						   v_vendor_site_code
					  FROM po_vendors pv, po_vendor_sites_all pvs
					 WHERE pv.vendor_id = my_rec.supplier_id
					   AND pv.vendor_id = pvs.vendor_id
					   AND pvs.vendor_site_id = my_rec.supplier_site_id
					   AND pvs.org_id = my_rec.org_id;
				EXCEPTION
					WHEN OTHERS THEN
						NULL;
				END;
			ELSE
				BEGIN
					SELECT pv.vendor_name, pv.vendor_id, pv.vat_registration_num
					  INTO v_actual_sup_name, v_vendor_id, v_tax_id
					  FROM po_vendors pv
					 WHERE pv.segment1 = my_rec.actual_sup_number;
				
					IF my_rec.actual_site_code IS NOT NULL THEN
						v_vendor_site_code	:=	my_rec.actual_site_code;
						SELECT NVL(pvs.vat_registration_num, v_tax_id), pvs.global_attribute14
						  INTO v_tax_id, v_branch_name
						  FROM po_vendor_sites_all pvs
						 WHERE pvs.vendor_id = v_vendor_id
						   AND pvs.vendor_site_code = my_rec.actual_site_code
						   AND pvs.org_id = my_rec.org_id;
					END IF;
				EXCEPTION
					WHEN OTHERS THEN
						NULL;
				END;
			END IF;
			v_branch_name := NVL(v_branch_name, '�ӹѡ�ҹ�˭�');
			v_vendor_site_code	:=	NVL(v_vendor_site_code,'-');
		
			v_insert := TRUE;
			-- Insert Data to table : th_taxinvoices_header
			write_log('50 begin : Insert Reverse Data to table : th_taxinvoices_header');
			INSERT INTO th_taxinvoices_header
				(th_tih_id,
				 th_tih_number,
				 th_tih_transaction_id,
				 th_tih_transaction_number,
				 th_tih_transaction_date,
				 th_tih_type_tax,
				 th_tih_tax_code,
				 th_tih_location_id,
				 th_tih_voucher_number,
				 th_tih_bank_acct_number,
				 th_tih_bank_acct_name,
				 th_tih_non_rec_amt,
				 th_tih_rec_amt,
				 th_tih_rec_rate,
				 th_tih_base_amt,
				 attribute1,
				 created_by,
				 creation_date,
				 last_updated_by,
				 last_update_date,
				 last_update_login,
				 org_id,
				 cancel_flag,
				 tran_cancel_flag,
				 tran_cancel_date)
			VALUES
				(v_head_id,
				 v_head_id,
				 my_rec.transaction_id,
				 my_rec.transaction_number,
				 my_rec.transaction_date,
				 my_rec.type_tax,
				 my_rec.tax_code,
				 my_rec.location_id,
				 my_rec.voucher_number,
				 my_rec.bank_acct_number,
				 my_rec.bank_acct_name,
				 non_rec_tax,
				 rec_tax,
				 my_rec.tax_rate,
				 base_amt,
				 p_conc_request_id,
				 my_rec.created_by,
				 my_rec.creation_date,
				 my_rec.last_updated_by,
				 my_rec.last_update_date,
				 my_rec.last_update_login,
				 my_rec.org_id,
				 v_cancel_flag,
				 v_tran_cancel_flag,
				 TRUNC(SYSDATE));
			write_log('50 end : Insert Reverse Data to table : th_taxinvoices_header');
			-- Insert Data to table : th_taxinvoices_line
			write_log('60 begin : Insert Reverse Data to table : th_taxinvoices_line');
			INSERT INTO th_taxinvoices_line
				(th_til_id,
				 th_tih_id,
				 th_til_mode,
				 th_til_period_name,
				 th_til_tax_inv_id,
				 th_til_location_id,
				 th_til_tax_inv_number,
				 th_til_tax_inv_date,
				 th_til_supplier_name,
				 th_til_project_id,
				 th_til_base_amt,
				 th_til_tax_amt,
				 th_til_non_rec_amt,
				 th_til_rec_amt,
				 attribute1,
				 created_by,
				 creation_date,
				 last_updated_by,
				 last_update_date,
				 last_update_login,
				 org_id,
				 attribute29,
				 attribute30,
				 attribute28)
			VALUES
				(v_line_id,
				 v_head_id,
				 'A',
				 v_nperiod,
				 my_rec.transaction_id,
				 my_rec.location_id,
				 my_rec.transaction_number,
				 my_rec.transaction_date,
				 v_actual_sup_name,
				 my_rec.project_id,
				 base_amt,
				 tax_amt,
				 non_rec_tax,
				 rec_tax,
				 p_conc_request_id,
				 my_rec.created_by,
				 my_rec.creation_date,
				 my_rec.last_updated_by,
				 my_rec.last_update_date,
				 my_rec.last_update_login,
				 my_rec.org_id,
				 substr(v_tax_id, 1, 13),
				 substr(v_branch_name, 1, 20),
				 v_vendor_site_code);
			write_log('60 end : Insert Reverse Data to table : th_taxinvoices_line');
		END LOOP;
		CLOSE my_cursor;
		-- Insert Data to table : th_itemtax_distribution_loaded
		-- For Check Data Loaded
		IF v_insert THEN
			write_log('70 begin : Insert Reverse Data to table : th_itemtax_distribution_loaded');
			OPEN my_cursor_dis;
			LOOP
				FETCH my_cursor_dis
					INTO my_rec_dis;
				EXIT WHEN my_cursor_dis%NOTFOUND;
				INSERT INTO th_itemtax_distribution_loaded
					(invoice_distribution_id,
					 invoice_type,
					 org_id,
					 created_by,
					 creation_date)
				VALUES
					(my_rec_dis.invoice_distribution_id,
					 'TAXCCINV',
					 my_rec_dis.org_id,
					 my_rec_dis.created_by,
					 my_rec_dis.creation_date);
			END LOOP;
			CLOSE my_cursor_dis;
			write_log('70 end : Insert Reverse Data to table : th_itemtax_distribution_loaded');
		END IF;
		COMMIT;
	EXCEPTION
		WHEN OTHERS THEN
			raise_application_error(-20101, 'Saving Error!');
			ROLLBACK;
	END reverse_taxinv_goods;

	/*================================================================
      PROCEDURE NAME:   cancel_taxinv_goods()
      Created by TM 9/11/2017 : VAT & WHT CR  
    ==================================================================*/
	PROCEDURE cancel_taxinv_goods(p_conc_request_id IN NUMBER,
								  p_invoice_id      IN NUMBER,
								  p_org_id          IN NUMBER,
								  p_user_id         IN NUMBER,
								  p_reverse_flag    IN VARCHAR2,
								  p_type_tax        IN VARCHAR2,
								  p_tax_code        IN VARCHAR2,
								  o_comfirmed_flag  OUT VARCHAR2) IS
	
		v_head_id NUMBER;
	BEGIN
		write_output4test('Start Process cancel invoice goods');
		-- get confirmed flag from all transaction of invoice
		BEGIN
			SELECT NVL(MAX(DECODE(ttl.th_til_comfirmed_date, NULL, 'N', 'Y')), 'F') comfirmed_flag
			  INTO o_comfirmed_flag
			  FROM th_taxinvoices_header tth, th_taxinvoices_line ttl
			 WHERE tth.th_tih_id = ttl.th_tih_id
			   AND tth.th_tih_transaction_id = p_invoice_id
			   AND tth.org_id = p_org_id
			   AND tth.th_tih_type_tax = p_type_tax
			   AND tth.th_tih_tax_code = p_tax_code;
		EXCEPTION
			WHEN OTHERS THEN
				o_comfirmed_flag := 'F';
		END;
		write_output4test('p_invoice_id ' || p_invoice_id ||
						  ' ,comfirmed_flag ' || o_comfirmed_flag);
		-- cancel transaction only active transaction (tran_cancel_flag = 'N') 
		IF o_comfirmed_flag = 'N' THEN
			UPDATE th_taxinvoices_header tth
			   SET cancel_flag      = 'Y',
				   tran_cancel_flag = 'Y',
				   tran_cancel_date = TRUNC(SYSDATE)
			 WHERE tth.th_tih_transaction_id = p_invoice_id
			   AND tth.org_id = p_org_id
			   AND tth.th_tih_type_tax = p_type_tax
			   AND tth.th_tih_tax_code = p_tax_code
			   AND NVL(tth.tran_cancel_flag, 'N') = 'N';
		ELSIF o_comfirmed_flag = 'Y' THEN
			UPDATE th_taxinvoices_header tth
			   SET tran_cancel_flag = 'Y',
				   tran_cancel_date = TRUNC(SYSDATE)
			 WHERE tth.th_tih_transaction_id = p_invoice_id
			   AND tth.org_id = p_org_id
			   AND tth.th_tih_type_tax = p_type_tax
			   AND tth.th_tih_tax_code = p_tax_code
			   AND NVL(tth.tran_cancel_flag, 'N') = 'N';
		
			/*IF p_reverse_flag = 'Y' THEN
				write_output4test('Call revesr invoice');
				reverse_taxinv_goods(p_conc_request_id => p_conc_request_id,
									 p_invoice_id      => p_invoice_id,
									 p_org_id          => p_org_id);
			END IF;*/
		END IF;
		write_output4test('End Process cancel invoice goods');
		COMMIT;
	EXCEPTION
		WHEN OTHERS THEN
			raise_application_error(-20101, 'Saving Error!');
			ROLLBACK;
	END cancel_taxinv_goods;
	
	/*================================================================
      PROCEDURE NAME:   cancel_taxinv_goods()
      Created by TM 9/11/2017 : VAT & WHT CR  
    ==================================================================*/
	PROCEDURE cancel_taxinv_service(p_conc_request_id IN NUMBER,
								  p_check_id      IN NUMBER,
								  p_org_id          IN NUMBER,
								  p_user_id         IN NUMBER,
								  p_type_tax        IN VARCHAR2,
								  p_tax_code        IN VARCHAR2,
								  o_comfirmed_flag  OUT VARCHAR2) IS
	
		v_head_id NUMBER;
	BEGIN
		write_output4test('Start Process cancel invoice service');
		-- get confirmed flag from all transaction of invoice
		BEGIN
			SELECT NVL(MAX(DECODE(ttl.th_til_comfirmed_date, NULL, 'N', 'Y')), 'F') comfirmed_flag
			  INTO o_comfirmed_flag
			  FROM th_taxinvoices_header tth, th_taxinvoices_line ttl
			 WHERE tth.th_tih_id = ttl.th_tih_id
			   AND tth.th_tih_transaction_id = p_check_id
			   AND tth.org_id = p_org_id
			   AND tth.th_tih_type_tax = p_type_tax
			   AND tth.th_tih_tax_code = p_tax_code;
		EXCEPTION
			WHEN OTHERS THEN
				o_comfirmed_flag := 'F';
		END;
		write_output4test('p_check_id ' || p_check_id ||
						  ' ,comfirmed_flag ' || o_comfirmed_flag);
		-- cancel transaction only active transaction (tran_cancel_flag = 'N') 
		IF o_comfirmed_flag = 'N' THEN
			UPDATE th_taxinvoices_header tth
			   SET cancel_flag      = 'Y',
				   tran_cancel_flag = 'Y',
				   tran_cancel_date = TRUNC(SYSDATE)
			 WHERE tth.th_tih_transaction_id = p_check_id
			   AND tth.org_id = p_org_id
			   AND tth.th_tih_type_tax = p_type_tax
			   AND tth.th_tih_tax_code = p_tax_code
			   AND NVL(tth.tran_cancel_flag, 'N') = 'N';
		ELSIF o_comfirmed_flag = 'Y' THEN
			UPDATE th_taxinvoices_header tth
			   SET tran_cancel_flag = 'Y',
				   tran_cancel_date = TRUNC(SYSDATE)
			 WHERE tth.th_tih_transaction_id = p_check_id
			   AND tth.org_id = p_org_id
			   AND tth.th_tih_type_tax = p_type_tax
			   AND tth.th_tih_tax_code = p_tax_code
			   AND NVL(tth.tran_cancel_flag, 'N') = 'N';	
		END IF;
		write_output4test('End Process cancel invoice goods');
		COMMIT;
	EXCEPTION
		WHEN OTHERS THEN
			raise_application_error(-20101, 'Saving Error!');
			ROLLBACK;
	END cancel_taxinv_service;

	/*================================================================
      PROCEDURE NAME:   th_load_cancel_taxinv_goods()
    ==================================================================*/
	/*PROCEDURE th_load_cancel_taxinv_goods(p_conc_request_id IN NUMBER,
										  p_org_id          IN NUMBER) IS
		v_start_process  DATE := TRUNC(SYSDATE);
		v_last_cancel    DATE;
		user_id          NUMBER;
		v_comfirmed_flag VARCHAR2(1);
		CURSOR c_invoice IS
			SELECT ai.invoice_id, 'G' type_tax, atc.name tax_code
			  FROM ap_invoices              ai,
				   ap_invoice_distributions aid,
				   ap_tax_codes             atc
			 WHERE ai.invoice_id = aid.invoice_id
				  --  and To_Char(Trunc(ai.Creation_Date), 'yyyymmdd') >= '20100301' 
			   AND aid.tax_code_id = atc.tax_id
			   AND atc.global_attribute20 = 'N'
			   AND aid.line_type_lookup_code = 'ITEM'
			   AND EXISTS (SELECT 'x'
					  FROM ap_invoice_distributions
					 WHERE invoice_id = ai.invoice_id
					   AND line_type_lookup_code = 'TAX')
			   AND EXISTS
			 		(SELECT 'x'
					  FROM th_taxinvoices_header
					 WHERE th_tih_transaction_id = ai.invoice_id
					   AND th_tih_type_tax = 'G'
					   AND th_tih_tax_code = atc.name
					   AND NVL(tran_cancel_flag, 'N') = 'N')
			   AND ai.cancelled_date >= v_last_cancel
			   AND ai.cancelled_date <= v_start_process
			   AND ai.ORG_ID = p_org_id			 
			 GROUP BY ai.invoice_id, atc.name;
	BEGIN
		write_output4test('Start Process');
		user_id := fnd_profile.value('USER_ID');
		BEGIN
			SELECT last_update_date
			  INTO v_last_cancel
			  FROM tac_thload_collection
			 WHERE process = 'CANCEL_GOODS';
		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				INSERT INTO TAC_THLOAD_COLLECTION
					(process, last_update_date)
				VALUES
					('CANCEL_GOODS', TO_DATE('1/3/2010', 'DD/MM/YYYY'));
				v_last_cancel := TO_DATE('1/3/2010', 'DD/MM/YYYY');
		END;
		FOR r_invoice IN c_invoice LOOP
			write_output4test('call cancel invoice ' ||
							  r_invoice.invoice_id || ' ,tax code ' ||
							  r_invoice.tax_code);
			cancel_taxinv_goods(p_conc_request_id => p_conc_request_id,
								p_invoice_id      => r_invoice.invoice_id,
								p_org_id          => p_org_id,
								p_user_id         => user_id,
								p_reverse_flag    => 'Y',
								p_type_tax        => r_invoice.type_tax,
								p_tax_code        => r_invoice.tax_code,
								o_comfirmed_flag  => v_comfirmed_flag);
		END LOOP;
		BEGIN
			UPDATE tac_thload_collection
			   SET last_update_date = v_start_process,
				   last_updated_by  = user_id
			 WHERE process = 'CANCEL_GOODS';
		EXCEPTION
			WHEN OTHERS THEN
				NULL;
		END;
		write_output4test('End Process');
		COMMIT;
	EXCEPTION
		WHEN OTHERS THEN
			raise_application_error(-20101, 'Saving Error!');
			ROLLBACK;
	END th_load_cancel_taxinv_goods;*/
	/*================================================================
      PROCEDURE NAME:   th_load_taxinv_goods()
    ==================================================================*/
	PROCEDURE th_load_taxinv_goods(p_conc_request_id IN NUMBER,
								   p_invoice_id      IN NUMBER,
								   p_org_id          IN NUMBER) IS
	
		--added by AP@BAS on 25-Dec-2014
		v_tax_id          VARCHAR2(20);
		v_branch_name     VARCHAR2(150);
		v_actual_sup_name VARCHAR2(250);
		----
		-- Add by TM : VAT & WHT CR
		v_vendor_id        NUMBER;
		v_cancel_flag      VARCHAR2(1);
		v_amt_by_tax       NUMBER;
		v_comfirmed_flag   VARCHAR2(1);
		v_tran_cancel_flag VARCHAR2(1) := 'Y';
		v_vendor_site_code VARCHAR2(30);
		----
		user_id NUMBER;
		-- Cursor for Load Data to Table : th_taxinvoices_header and th_taxinvoices_line
		CURSOR my_cursor IS
			SELECT ai.invoice_id transaction_id,
				   ai.invoice_num transaction_number,
				   ai.invoice_date transaction_date,
				   trunc(aid.accounting_date) gl_date,
				   'G' type_tax,
				   atc.name tax_code,
				   atc.tax_recovery_rate tax_rate,
				   hla.location_id,
				   decode(ai.doc_sequence_id, NULL, ai.voucher_num, ai.doc_sequence_value) voucher_number,
				   NULL bank_acct_number,
				   NULL bank_acct_name,
				   aid.project_id,
				   pv.vendor_id supplier_id,
				   pv.vendor_name supplier_name,
				   user_id created_by,
				   SYSDATE creation_date,
				   user_id last_updated_by,
				   SYSDATE last_update_date,
				   user_id last_update_login,
				   ai.org_id,
				   -- added by AP@BAS on 25-Dec-2014 : for link to Supplier Site.Tax Registration Number, Supplier Site.(WHT FF)Branch Number
				   aid.attribute4 actual_sup_number,
				   aid.attribute5 actual_site_code,
				   -- Add by TM 9/11/2017 : VAT & WHT CR				  
				   ai.vendor_site_id supplier_site_id,
				   ap_invoices_pkg.get_approval_status(ai.invoice_id, ai.invoice_amount, ai.payment_status_flag, ai.invoice_type_lookup_code) approval_status
			-- End Add by TM 9/11/2017 : VAT & WHT CR
			  FROM ap_invoices              ai,
				   ap_invoice_distributions aid,
				   ap_tax_codes             atc,
				   po_vendors               pv,
				   hr_locations             hla,
				   ap_ae_lines_all          xap
			 WHERE ai.invoice_id = aid.invoice_id
			   AND aid.tax_code_id = atc.tax_id
			   AND atc.global_attribute20 = 'N'
			   AND aid.line_type_lookup_code = 'ITEM'
				  /* Edit by TM 9/11/2017 : VAT & WHT CR*/
				  /*AND ai.invoice_id IN (SELECT invoice_id
                  FROM ap_invoice_distributions
                  WHERE invoice_id = ai.invoice_id
                        AND line_type_lookup_code = 'TAX')*/
			   AND EXISTS (SELECT 'x'
					  FROM ap_invoice_distributions
					 WHERE invoice_id = ai.invoice_id
					   AND line_type_lookup_code = 'TAX')
				  /* End Edit by TM 9/11/2017 : VAT & WHT CR*/
			   AND ai.vendor_id = pv.vendor_id
			   AND ap_invoices_pkg.get_posting_status(ai.invoice_id) = 'Y'
			   AND ap_invoices_pkg.get_approval_status(ai.invoice_id, ai.invoice_amount, ai.payment_status_flag, ai.invoice_type_lookup_code) IN
				   ('APPROVED', 'CANCELLED')
				  /* Edit by TM 9/11/2017 : VAT & WHT CR
                  and (aid.invoice_distribution_id, 'TAXINV') not in
                  (select tidl.invoice_distribution_id
                             ,tidl.invoice_type
                       from th_itemtax_distribution_loaded tidl
                       where tidl.invoice_type = 'TAXINV'
                             and tidl.invoice_distribution_id = aid.invoice_distribution_id)*/
			   AND NOT EXISTS
			 		(SELECT 'x'
					 FROM th_itemtax_distribution_loaded tidl
					 WHERE tidl.invoice_type = 'TAXINV'
					   AND tidl.invoice_distribution_id = aid.invoice_distribution_id)
			   AND NOT EXISTS
			 		(SELECT 'x'
					 FROM th_itemtax_distribution_loaded tidl
					 	,ap_invoice_payments      aip
					 WHERE tidl.invoice_type = 'PAYINV'
					 AND tidl.invoice_distribution_id = aip.invoice_payment_id
					 AND ai.invoice_id = aip.invoice_id)	   
				/* End Edit by TM 9/11/2017*/
			   AND atc.global_attribute19 = hla.location_id
			   AND xap.source_table = 'AP_INVOICE_DISTRIBUTIONS'
			   AND xap.source_id = aid.invoice_distribution_id
			   AND ai.invoice_id = p_invoice_id
			   AND ai.org_id = p_org_id ----add by aeaum 23/09/2009------
			--------------------Comment by aeaum 23/09/2009------------------
			-- AND ai.invoice_id != 4170984               /*  BUG invoice INS 09-0101  */
			-------------------------------------------------------------------------
			 GROUP BY ai.invoice_id,
					  ai.invoice_num,
					  ai.invoice_date,
					  trunc(aid.accounting_date),
					  atc.name,
					  hla.location_id,
					  decode(ai.doc_sequence_id, NULL, ai.voucher_num, ai.doc_sequence_value),
					  aid.project_id,
					  pv.vendor_id,
					  pv.vendor_name,
					  atc.tax_recovery_rate,
					  ai.org_id,
					  aid.attribute4,
					  aid.attribute5
					  /* Add by TM 9/11/2017 : VAT & WHT CR*/,
					  ai.invoice_amount,
					  ai.payment_status_flag,
					  ai.invoice_type_lookup_code,
					  ai.vendor_site_id
			/* End Add by TM 9/11/2017*/
			 ORDER BY decode(ai.doc_sequence_id, NULL, ai.voucher_num, ai.doc_sequence_value),
					  ai.invoice_id,
					  atc.name,
					  aid.project_id;
	
		my_rec my_cursor%ROWTYPE;
		-- Cursor for Load Data to Table : th_itemtax_distribution_loaded
		CURSOR my_cursor_dis IS
			SELECT DISTINCT ai.invoice_id,
							aid.invoice_distribution_id invoice_distribution_id,
							ai.org_id,
							user_id                     created_by --yvk20160607
						   ,SYSDATE                     creation_date --yvk20160607
			  FROM ap_invoices              ai,
				   ap_invoice_distributions aid,
				   ap_tax_codes             atc,
				   po_vendors               pv,
				   hr_locations             hla,
				   ap_ae_lines_all          xap
			 WHERE ai.invoice_id = aid.invoice_id
			   AND aid.tax_code_id = atc.tax_id
			   AND atc.global_attribute20 = 'N'
			   AND aid.line_type_lookup_code IN ('TAX', 'ITEM')
				  /* Edit by TM 9/11/2017 : VAT & WHT CR*/
				  /*AND ai.invoice_id IN (SELECT invoice_id
                  FROM ap_invoice_distributions
                  WHERE invoice_id = ai.invoice_id
                        AND line_type_lookup_code = 'TAX')*/
			   AND EXISTS (SELECT 'x'
					  FROM ap_invoice_distributions
					 WHERE invoice_id = ai.invoice_id
					   AND line_type_lookup_code = 'TAX')
				  /* End Edit by TM 9/11/2017 : VAT & WHT CR*/
			   AND ai.vendor_id = pv.vendor_id
			   AND ap_invoices_pkg.get_posting_status(ai.invoice_id) = 'Y'
			   AND ap_invoices_pkg.get_approval_status(ai.invoice_id, ai.invoice_amount, ai.payment_status_flag, ai.invoice_type_lookup_code) IN
				   ('APPROVED', 'CANCELLED')
				  /* Edit by TM 9/11/2017 : VAT & WHT CR
                  AND (aid.invoice_distribution_id, 'TAXINV') NOT IN
                  (SELECT tidl.invoice_distribution_id
                             ,tidl.invoice_type
                       FROM th_itemtax_distribution_loaded tidl
                       WHERE tidl.invoice_type = 'TAXINV'
                             AND tidl.invoice_distribution_id = aid.invoice_distribution_id)*/
			   AND NOT EXISTS
			 		(SELECT 'x'
					 FROM th_itemtax_distribution_loaded tidl
					 WHERE tidl.invoice_type = 'TAXINV'
					 AND tidl.invoice_distribution_id = aid.invoice_distribution_id)
			   AND NOT EXISTS
			 		(SELECT 'x'
					 FROM th_itemtax_distribution_loaded tidl
					 	,ap_invoice_payments      aip
					 WHERE tidl.invoice_type = 'PAYINV'
					 AND tidl.invoice_distribution_id = aip.invoice_payment_id
					 AND ai.invoice_id = aip.invoice_id)		 
			   /* End Edit by TM 9/11/2017*/
			   AND xap.source_table = 'AP_INVOICE_DISTRIBUTIONS'
			   AND xap.source_id = aid.invoice_distribution_id
			   AND atc.global_attribute19 = hla.location_id
			   AND ai.invoice_id = p_invoice_id
			   AND ai.org_id = p_org_id; ----add by aeaum 23/09/2009------
		---comment by aeaum 23/09/2009----
		--AND ai.invoice_id != 4170984  ;    /*  BUG invoice INS 09-0101  */
		----------------------------------
		my_rec_dis                    my_cursor_dis%ROWTYPE;
		v_head_id                     NUMBER;
		v_line_id                     NUMBER;
		base_amt                      NUMBER;
		rec_tax                       NUMBER;
		non_rec_tax                   NUMBER;
		tax_amt                       NUMBER;
		xproject_id                   NUMBER;
		xbproject                     BOOLEAN;
		v_insert                      BOOLEAN;
		v_nperiod                     VARCHAR(10);
		v_ai_invoice_id               NUMBER(15);
		v_ai_invoice_amount           NUMBER;
		v_ai_payment_status_flag      VARCHAR2(1 BYTE);
		v_ai_invoice_type_lookup_code VARCHAR2(25 BYTE);
	
	BEGIN
		write_output4test('Start Process');
		write_output4test('step 1 get user_id');
		user_id   := fnd_profile.value('USER_ID');
		xbproject := FALSE;
		v_insert  := FALSE;
		write_output4test('open cursor my_cursor');
		OPEN my_cursor;
		LOOP
			FETCH my_cursor
				INTO my_rec;
			EXIT WHEN my_cursor%NOTFOUND;
			/* Comment by TM 9/11/2017 : VAT & WHT CR*/
			/*SELECT th_taxinvh_s.nextval INTO v_head_id FROM dual;
            SELECT th_taxinvl_s.nextval INTO v_line_id FROM dual;*/
			/* End Comment by TM 9/11/2017 : VAT & WHT CR*/
			rec_tax     := 0;
			non_rec_tax := 0;
			tax_amt     := 0;
			base_amt    := 0;
			write_log('Invoice Number : ' || my_rec.transaction_number);
			-- Calculate BASE AMOUNT
			write_log('10 begin : Calculate BASE AMOUNT');
			BEGIN
				SELECT nvl(SUM(nvl(xap.accounted_dr, 0) - nvl(xap.accounted_cr, 0)), 0)
				  INTO base_amt
				  FROM ap_invoices              ai,
					   ap_invoice_distributions aid,
					   ap_tax_codes             atc,
					   hr_locations             hla,
					   ap_ae_lines_all          xap
				 WHERE ai.invoice_id = aid.invoice_id
				   AND aid.tax_code_id = atc.tax_id
				   AND atc.global_attribute20 = 'N'
				   AND aid.line_type_lookup_code = 'ITEM'
					  /* Comment by TM 9/11/2017 : VAT & WHT CR
                      AND ap_invoices_pkg.get_posting_status(ai.invoice_id) = 'Y'
                      AND ap_invoices_pkg.get_approval_status(ai.invoice_id, ai.invoice_amount, ai.payment_status_flag, ai.invoice_type_lookup_code) IN
                      ('APPROVED', 'CANCELLED')*/
					  /* Edit by TM 9/11/2017 : VAT & WHT CR
                      AND (aid.invoice_distribution_id, 'TAXINV') NOT IN
                      (SELECT tidl.invoice_distribution_id
                                 ,tidl.invoice_type
                           FROM th_itemtax_distribution_loaded tidl
                           WHERE tidl.invoice_type = 'TAXINV'
                                 AND tidl.invoice_distribution_id = aid.invoice_distribution_id)*/
				   AND NOT EXISTS
				 		(SELECT 'x'
						 FROM th_itemtax_distribution_loaded tidl
						 WHERE tidl.invoice_type = 'TAXINV'
						 AND tidl.invoice_distribution_id = aid.invoice_distribution_id)
					  /* End Edit by TM 9/11/2017*/
				   AND nvl(aid.project_id, 0) = nvl(my_rec.project_id, 0)
				   AND ai.invoice_id = my_rec.transaction_id
				   AND atc.global_attribute19 = hla.location_id
				   AND atc.name = my_rec.tax_code
				   AND xap.source_id = aid.invoice_distribution_id
				   AND xap.source_table = 'AP_INVOICE_DISTRIBUTIONS'
				   AND trunc(aid.accounting_date) = my_rec.gl_date
				   AND ai.org_id = p_org_id; ----add by aeaum 23/09/2009------
			EXCEPTION
				WHEN no_data_found THEN
					base_amt := 0;
			END;
			write_log('10 end : Calculate BASE AMOUNT');
			-- Calculate RECOVERABLE RATE AMOUNT
			write_log('20 begin : Calculate RECOVERABLE RATE AMOUNT');
			BEGIN
				SELECT nvl(SUM(nvl(aca1.allocated_base_amount, 0)), 0)
				  INTO rec_tax
				  FROM ap_invoices              ai,
					   ap_invoice_distributions aid,
					   ap_tax_codes             atc,
					   hr_locations             hla,
					   ap_ae_lines_all          xap,
					   ap_chrg_allocations_all  aca1,
					   ap_invoice_distributions aid1,
					   ap_tax_codes             atc1
				 WHERE ai.invoice_id = aid.invoice_id
				   AND aid.tax_code_id = atc.tax_id
				   AND atc.global_attribute20 = 'N'
				   AND aid.line_type_lookup_code = 'TAX'
					  /* Comment by TM 9/11/2017 : VAT & WHT CR
                      AND ap_invoices_pkg.get_posting_status(ai.invoice_id) = 'Y'
                      AND ap_invoices_pkg.get_approval_status(ai.invoice_id, ai.invoice_amount, ai.payment_status_flag, ai.invoice_type_lookup_code) IN
                      ('APPROVED', 'CANCELLED')*/
					  /* Edit by TM 9/11/2017 : VAT & WHT CR
                      AND (aid.invoice_distribution_id, 'TAXINV') NOT IN
                      (SELECT tidl.invoice_distribution_id
                                 ,tidl.invoice_type
                           FROM th_itemtax_distribution_loaded tidl
                           WHERE tidl.invoice_type = 'TAXINV'
                                 AND tidl.invoice_distribution_id = aid.invoice_distribution_id)*/
				   AND NOT EXISTS
				 		(SELECT 'x'
						 FROM th_itemtax_distribution_loaded tidl
						 WHERE tidl.invoice_type = 'TAXINV'
						 AND tidl.invoice_distribution_id = aid.invoice_distribution_id)
					  /* End Edit by TM 9/11/2017*/
				   AND ai.invoice_id = my_rec.transaction_id
				   AND atc.global_attribute19 = hla.location_id
				   AND xap.source_id = aid.invoice_distribution_id
				   AND atc.name = my_rec.tax_code
				   AND aid.tax_recoverable_flag = 'Y'
				   AND xap.source_table = 'AP_INVOICE_DISTRIBUTIONS'
				   AND aca1.charge_dist_id = aid.invoice_distribution_id
				   AND aca1.item_dist_id = aid1.invoice_distribution_id
				   AND aid1.tax_code_id = atc1.tax_id
				   AND atc1.name = my_rec.tax_code
				   AND nvl(aid1.project_id, 0) = nvl(my_rec.project_id, 0)
				   AND aca1.item_dist_id IN
					   (SELECT invoice_distribution_id
						  FROM ap_invoice_distributions aid,
							   ap_tax_codes             atc
						 WHERE aid.invoice_id = my_rec.transaction_id
						   AND atc.name = my_rec.tax_code
						   AND nvl(aid.project_id, 0) = nvl(my_rec.project_id, 0)
						   AND aid.tax_code_id = atc.tax_id
						   AND aid.line_type_lookup_code = 'ITEM')
				   AND trunc(aid.accounting_date) = my_rec.gl_date
				   AND ai.org_id = p_org_id; ----add by aeaum 23/09/2009------
			EXCEPTION
				WHEN no_data_found THEN
					rec_tax := 0;
			END;
			write_log('20 end : Calculate RECOVERABLE RATE AMOUNT');
			-- Calculate NON RECOVERABLE RATE AMOUNT
			write_log('30 begin : Calculate NON RECOVERABLE RATE AMOUNT');
			BEGIN
				SELECT nvl(SUM(nvl(xap.accounted_dr, 0) - nvl(xap.accounted_cr, 0)), 0)
				  INTO non_rec_tax
				  FROM ap_invoices              ai,
					   ap_invoice_distributions aid,
					   ap_tax_codes             atc,
					   hr_locations             hla,
					   ap_ae_lines_all          xap
				 WHERE ai.invoice_id = aid.invoice_id
				   AND aid.tax_code_id = atc.tax_id
				   AND atc.global_attribute20 = 'N'
				   AND aid.line_type_lookup_code = 'TAX'
					  /* Comment by TM 9/11/2017 : VAT & WHT CR
                      AND ap_invoices_pkg.get_posting_status(ai.invoice_id) = 'Y'
                      AND ap_invoices_pkg.get_approval_status(ai.invoice_id, ai.invoice_amount, ai.payment_status_flag, ai.invoice_type_lookup_code) IN
                      ('APPROVED', 'CANCELLED')*/
					  /* Edit by TM 9/11/2017 : VAT & WHT CR*/
					  /*AND (aid.invoice_distribution_id, 'TAXINV') NOT IN
                      (SELECT tidl.invoice_distribution_id
                                 ,tidl.invoice_type
                           FROM th_itemtax_distribution_loaded tidl
                           WHERE tidl.invoice_type = 'TAXINV'
                                 AND tidl.invoice_distribution_id = aid.invoice_distribution_id)*/
				   AND NOT EXISTS
				 		(SELECT 'x'
						 FROM th_itemtax_distribution_loaded tidl
						 WHERE tidl.invoice_type = 'TAXINV'
						 AND tidl.invoice_distribution_id = aid.invoice_distribution_id)
					  /* End Edit by TM 9/11/2017*/
				   AND nvl(aid.project_id, 0) = nvl(my_rec.project_id, 0)
				   AND ai.invoice_id = my_rec.transaction_id
				   AND atc.global_attribute19 = hla.location_id
				   AND xap.source_id = aid.invoice_distribution_id
				   AND atc.name = my_rec.tax_code
				   AND aid.tax_recoverable_flag = 'N'
				   AND xap.source_table = 'AP_INVOICE_DISTRIBUTIONS'
				   AND trunc(aid.accounting_date) = my_rec.gl_date
				   AND ai.org_id = p_org_id; ----add by aeaum 23/09/2009------
			EXCEPTION
				WHEN no_data_found THEN
					non_rec_tax := 0;
			END;
			write_log('30 end : Calculate NON RECOVERABLE RATE AMOUNT');
			tax_amt := rec_tax + non_rec_tax;
			-- Find Period Name from GL DATE
			write_log('40 begin : Find Period Name from GL DATE');
			SELECT period_name
			  INTO v_nperiod
			  FROM gl_periods_v
			 WHERE period_set_name = 'Accounting' -- 'TCORE_Calendar'
			   AND (trunc(my_rec.gl_date) BETWEEN start_date AND end_date)
			   AND adjustment_period_flag = 'N'
			 ORDER BY start_date;
			write_log('40 end : Find Period Name from GL DATE');
		
			-- added by AP@BAS on 25-Dec-2014 : find vat_registeration_number / Branch Name from supplier_site table
			-- 45
			v_tax_id          := NULL;
			v_branch_name     := NULL;
			v_actual_sup_name := NULL;
			/* Edit by TM on 9/11/2017 : VAT & WHT CR*/
			v_vendor_id := NULL;
			v_vendor_site_code := NULL;
			/*IF my_rec.actual_sup_number IS NOT NULL AND my_rec.actual_site_code IS NOT NULL THEN
              BEGIN
                SELECT pv.vendor_name
                      ,pvs.vat_registration_num
                      ,pvs.global_attribute14
                INTO v_actual_sup_name
                    ,v_tax_id
                    ,v_branch_name
                FROM po_vendors pv
                    ,po_vendor_sites_all pvs
                WHERE pv.vendor_id = pvs.vendor_id
                      AND pv.segment1 = my_rec.actual_sup_number
                      AND pvs.vendor_site_code = my_rec.actual_site_code
                      AND pvs.org_id = my_rec.org_id;
              EXCEPTION
                WHEN OTHERS THEN
                  NULL;
              END;
            END IF;*/
			IF my_rec.actual_sup_number IS NULL THEN
				BEGIN
					SELECT pv.vendor_name, pv.vendor_id, pvs.vat_registration_num, pvs.global_attribute14 ,pvs.vendor_site_code
					  INTO v_actual_sup_name, v_vendor_id, v_tax_id, v_branch_name ,v_vendor_site_code
					  FROM po_vendors pv, po_vendor_sites_all pvs
					 WHERE pv.vendor_id = my_rec.supplier_id
					   AND pv.vendor_id = pvs.vendor_id
					   AND pvs.vendor_site_id = my_rec.supplier_site_id
					   AND pvs.org_id = my_rec.org_id;
				EXCEPTION
					WHEN OTHERS THEN
						NULL;
				END;
			ELSE
				BEGIN
					SELECT pv.vendor_name, pv.vendor_id, pv.vat_registration_num
					  INTO v_actual_sup_name, v_vendor_id, v_tax_id
					  FROM po_vendors pv
					 WHERE pv.segment1 = my_rec.actual_sup_number;
				
					IF my_rec.actual_site_code IS NOT NULL THEN
						v_vendor_site_code	:=	my_rec.actual_site_code;
						SELECT pvs.vat_registration_num, pvs.global_attribute14
						  INTO v_tax_id, v_branch_name
						  FROM po_vendor_sites_all pvs
						 WHERE pvs.vendor_id = v_vendor_id
						   AND pvs.vendor_site_code = my_rec.actual_site_code
						   AND pvs.org_id = my_rec.org_id;   
					END IF;
				EXCEPTION
					WHEN OTHERS THEN
						NULL;
				END;
			END IF;
			v_branch_name := NVL(v_branch_name, '�ӹѡ�ҹ�˭�');
			v_vendor_site_code	:=	NVL(v_vendor_site_code,'-');
			/* End Edit by TM on 9/11/2017 : VAT & WHT CR    */
			/* Add by TM on 9/11/2017 : VAT & WHT CR*/
			-- Check cancel 
			v_cancel_flag      := 'N';
			v_tran_cancel_flag := 'N';
			v_comfirmed_flag   := 'N';
			IF my_rec.approval_status = 'CANCELLED' THEN
				cancel_taxinv_goods(p_conc_request_id => p_conc_request_id,
									p_invoice_id      => my_rec.transaction_id,
									p_org_id          => p_org_id,
									p_user_id         => user_id,
									p_reverse_flag    => 'Y',
									p_type_tax        => my_rec.type_tax,
									p_tax_code        => my_rec.tax_code,
									o_comfirmed_flag  => v_comfirmed_flag);
				IF v_comfirmed_flag = 'N' THEN
					v_cancel_flag := 'Y';
				ELSE
					v_cancel_flag := 'N';
				END IF;
				v_tran_cancel_flag := 'Y';
			ELSE
				BEGIN
					SELECT SUM(aid.amount)
					  INTO v_amt_by_tax
					  FROM ap_invoice_distributions aid, ap_tax_codes atc
					 WHERE aid.tax_code_id = atc.tax_id
					   AND aid.line_type_lookup_code = 'ITEM'
					   AND atc.global_attribute20 = 'N'
					   AND aid.invoice_id = my_rec.transaction_id;
				
					IF v_amt_by_tax = 0 THEN
						cancel_taxinv_goods(p_conc_request_id => p_conc_request_id,
											p_invoice_id      => my_rec.transaction_id,
											p_org_id          => p_org_id,
											p_user_id         => user_id,
											p_reverse_flag    => 'N',
											p_type_tax        => my_rec.type_tax,
											p_tax_code        => my_rec.tax_code,
											o_comfirmed_flag  => v_comfirmed_flag);
						IF v_comfirmed_flag = 'N' THEN
							v_cancel_flag := 'Y';
						ELSE
							v_cancel_flag := 'N';
						END IF;
						v_tran_cancel_flag := 'Y';
					END IF;
				EXCEPTION
					WHEN OTHERS THEN
						NULL;
				END;
			END IF;
			write_output4test('Comfirmed_flag ' || v_comfirmed_flag ||
							  ' ,cancel_flag ' || v_cancel_flag ||
							  ' ,tran_cancel_flag ' || v_tran_cancel_flag);
			/* End Add by TM on 9/11/2017 : VAT & WHT CR*/
			/* Edit by TM 9/11/2017 : VAT & WHT CR*/
			IF v_comfirmed_flag <> 'F' THEN
				SELECT th_taxinvh_s.nextval INTO v_head_id FROM dual;
				SELECT th_taxinvl_s.nextval INTO v_line_id FROM dual;
				/* End Edit by TM 9/11/2017 : VAT & WHT CR*/
				v_insert := TRUE;
				-- Insert Data to table : th_taxinvoices_header
				write_log('50 begin : Insert Data to table : th_taxinvoices_header');
				INSERT INTO th_taxinvoices_header
					(th_tih_id,
					 th_tih_number,
					 th_tih_transaction_id,
					 th_tih_transaction_number,
					 th_tih_transaction_date,
					 th_tih_type_tax,
					 th_tih_tax_code,
					 th_tih_location_id,
					 th_tih_voucher_number,
					 th_tih_bank_acct_number,
					 th_tih_bank_acct_name,
					 th_tih_non_rec_amt,
					 th_tih_rec_amt,
					 th_tih_rec_rate,
					 th_tih_base_amt,
					 attribute1,
					 created_by,
					 creation_date,
					 last_updated_by,
					 last_update_date,
					 last_update_login,
					 org_id,
					 --Add by TM on 9/11/2017 : VAT & WHT CR					
					 cancel_flag,
					 tran_cancel_flag,
					 tran_cancel_date
					 -- End Add by TM on 9/11/2017 : VAT & WHT CR
					 )
				VALUES
					(v_head_id,
					 v_head_id,
					 my_rec.transaction_id,
					 my_rec.transaction_number,
					 my_rec.transaction_date,
					 my_rec.type_tax,
					 my_rec.tax_code,
					 my_rec.location_id,
					 my_rec.voucher_number,
					 my_rec.bank_acct_number,
					 my_rec.bank_acct_name,
					 non_rec_tax,
					 rec_tax,
					 my_rec.tax_rate,
					 base_amt,
					 p_conc_request_id,
					 my_rec.created_by,
					 my_rec.creation_date,
					 my_rec.last_updated_by,
					 my_rec.last_update_date,
					 my_rec.last_update_login,
					 my_rec.org_id,
					 --Add by TM on 9/11/2017 : VAT & WHT CR					
					 v_cancel_flag,
					 v_tran_cancel_flag,
					 TRUNC(SYSDATE)
					 -- End Add by TM on 9/11/2017 : VAT & WHT CR
					 );
				write_log('50 end : Insert Data to table : th_taxinvoices_header');
				-- Insert Data to table : th_taxinvoices_line
				write_log('60 begin : Insert Data to table : th_taxinvoices_line');
				INSERT INTO th_taxinvoices_line
					(th_til_id,
					 th_tih_id,
					 th_til_mode,
					 th_til_period_name,
					 th_til_tax_inv_id,
					 th_til_location_id,
					 th_til_tax_inv_number,
					 th_til_tax_inv_date,
					 th_til_supplier_name,
					 th_til_project_id,
					 th_til_base_amt,
					 th_til_tax_amt,
					 th_til_non_rec_amt,
					 th_til_rec_amt,
					 attribute1,
					 created_by,
					 creation_date,
					 last_updated_by,
					 last_update_date,
					 last_update_login,
					 org_id,
					 -- added by AP@BAS on 25-Dec-2014					
					 attribute29,
					 attribute30,
					 attribute28)
				VALUES
					(v_line_id,
					 v_head_id,
					 'A',
					 v_nperiod,
					 my_rec.transaction_id,
					 my_rec.location_id,
					 my_rec.transaction_number,
					 my_rec.transaction_date,
					 /* Edit by TM on 9/11/2017 : VAT & WHT CR   */
					 --,my_rec.supplier_name					
					 v_actual_sup_name
					 /* End Edit by TM on 9/11/2017 : VAT & WHT CR   */,
					 my_rec.project_id,
					 base_amt,
					 tax_amt,
					 non_rec_tax,
					 rec_tax,
					 p_conc_request_id,
					 my_rec.created_by,
					 my_rec.creation_date,
					 my_rec.last_updated_by,
					 my_rec.last_update_date,
					 my_rec.last_update_login,
					 my_rec.org_id,
					 -- added by AP@BAS on 25-Dec-2014					
					 substr(v_tax_id, 1, 13),
					 substr(v_branch_name, 1, 20),
					 v_vendor_site_code);
				write_log('60 end : Insert Data to table : th_taxinvoices_line');
			END IF; /* Edit by TM 9/11/2017 : VAT & WHT CR*/
		END LOOP;
		CLOSE my_cursor;
		-- Insert Data to table : th_itemtax_distribution_loaded
		-- For Check Data Loaded
		IF v_insert THEN
			write_log('70 begin : Insert Data to table : th_itemtax_distribution_loaded');
			OPEN my_cursor_dis;
			LOOP
				FETCH my_cursor_dis
					INTO my_rec_dis;
				EXIT WHEN my_cursor_dis%NOTFOUND;
				INSERT INTO th_itemtax_distribution_loaded
					(invoice_distribution_id,
					 invoice_type,
					 org_id,
					 created_by,
					 creation_date)
				VALUES
					(my_rec_dis.invoice_distribution_id,
					 'TAXINV',
					 my_rec_dis.org_id,
					 my_rec_dis.created_by,
					 my_rec_dis.creation_date);
				--Add by TM on 9/11/2017 : VAT & WHT CR
				IF v_tran_cancel_flag = 'Y' THEN
					INSERT INTO th_itemtax_distribution_loaded
						(invoice_distribution_id,
						 invoice_type,
						 org_id,
						 created_by,
						 creation_date)
					VALUES
						(my_rec_dis.invoice_distribution_id,
						 'TAXCCINV',
						 my_rec_dis.org_id,
						 my_rec_dis.created_by,
						 my_rec_dis.creation_date);
				END IF;
				--End Add by TM on 9/11/2017 : VAT & WHT CR
			END LOOP;
			CLOSE my_cursor_dis;
			write_log('70 end : Insert Data to table : th_itemtax_distribution_loaded');
		END IF;
		COMMIT;
	EXCEPTION
		WHEN OTHERS THEN
			raise_application_error(-20101, 'Saving Error!');
			ROLLBACK;
	END th_load_taxinv_goods;

	/*================================================================
      PROCEDURE NAME:   th_load_taxinv_service()
    ==================================================================*/
	PROCEDURE th_load_taxinv_service(p_conc_request_id IN NUMBER,
									 p_check_id        IN NUMBER,
									 p_org_id          IN NUMBER) IS
	
		user_id                       NUMBER := 0;
		v_head_id                     NUMBER := 0;
		v_line_id                     NUMBER := 0;
		v_base_amt                    NUMBER := 0;
		v_non_rec_tax                 NUMBER := 0;
		v_rec_tax                     NUMBER := 0;
		v_non_rec_amt                 NUMBER := 0;
		v_pi_result                   NUMBER(13, 10) := 0;
		v_rec_amt                     NUMBER := 0;
		v_tax_amt                     NUMBER := 0;
		v_payment_amt                 NUMBER := 0;
		v_invoice_amt                 NUMBER := 0;
		v_project_id                  NUMBER := 0;
		v_bproject                    BOOLEAN;
		v_insert                      BOOLEAN;
		v_paid_percent                NUMBER := 0;
		v_paid_base_inv               NUMBER := 0;
		v_paid_rec_tax                NUMBER := 0;
		v_paid_nonrec_tax             NUMBER := 0;
		v_check_base_inv              NUMBER := 0;
		v_check_rec_tax               NUMBER := 0;
		v_check_nonrec_tax            NUMBER := 0;
		v_pay_act                     NUMBER := 0;
		v_query                       VARCHAR2(800);
		cursor1                       INTEGER;
		rows_processed                INTEGER;
		v_index                       NUMBER := 0;
		v_ai_invoice_id               NUMBER(15) := 0;
		v_ai_invoice_amount           NUMBER := 0;
		v_ai_payment_status_flag      VARCHAR2(1 BYTE);
		v_ai_invoice_type_lookup_code VARCHAR2(25 BYTE);
	
		--added by AP@BAS on 25-Dec-2014
		v_tax_id          VARCHAR2(20);
		v_branch_name     VARCHAR2(150);
		v_actual_sup_name VARCHAR2(250);
		-- Add by TM : VAT & WHT CR
		v_vendor_id   NUMBER;
		v_cancel_flag VARCHAR2(1);
		v_skip_flag   VARCHAR2(1);
		v_comfirmed_flag   VARCHAR2(1);
		v_tran_cancel_flag VARCHAR2(1) := 'Y';
		v_vendor_site_code VARCHAR2(30);
		--
		----- Added by Korn.iCE on 29-APR-2010.
		----- Due to Step 20, 30 the query will take so long time, then I create parameters for keep a current value and will compare with next record.
		----- If the both value are similar then should not need query again.
		v_comp_trans_id   NUMBER(15) := 0;
		v_comp_invoice_id NUMBER(15) := 0;
	
		----- Cursor for Load Data to Table : th_taxinvoices_header
		CURSOR cursor_head IS
			SELECT ac.check_id transaction_id,
				   ac.check_number transaction_number,
				   ac.check_date transaction_date,
				   'S' type_tax,
				   atc.name tax_code,
				   ac.doc_sequence_value voucher_number,
				   atc.tax_recovery_rate,
				   hla.location_id location_id,
				   aba.bank_account_num bank_acct_number,
				   ac.bank_account_name bank_acct_name,
				   ac.vendor_name supplier_name,
				   user_id created_by,
				   SYSDATE creation_date,
				   user_id last_updated_by,
				   SYSDATE last_update_date,
				   user_id last_update_login,
				   ai.org_id,
				   ac.status_lookup_code payment_status -- Add by TM 9/11/2017 : VAT & WHT CR
			  FROM ap_invoices              ai,
				   ap_invoice_distributions aid,
				   ap_tax_codes             atc,
				   ap_invoice_payments      aip,
				   ap_checks                ac,
				   ap_bank_accounts         aba,
				   hr_locations             hla,
				   ap_ae_lines_all          xap
			 WHERE ai.invoice_id = aid.invoice_id
			   AND aid.tax_code_id = atc.tax_id
			   AND atc.global_attribute20 = 'Y'
			   AND aid.line_type_lookup_code = 'ITEM'
				/* Edit by TM 9/11/2017 : VAT & WHT CR*/
				  /*AND ai.invoice_id IN (SELECT invoice_id
                  FROM ap_invoice_distributions
                  WHERE invoice_id = ai.invoice_id
                        AND line_type_lookup_code = 'TAX')*/
			   AND EXISTS (SELECT 'x'
					  FROM ap_invoice_distributions
					 WHERE invoice_id = ai.invoice_id
					   AND line_type_lookup_code = 'TAX')
			   AND ap_checks_pkg.get_posting_status(ac.check_id) = 'Y'
			   AND ((ac.status_lookup_code = 'VOIDED') 
			   OR  (ac.status_lookup_code <> 'VOIDED'
			   AND ap_invoices_pkg.get_posting_status(ai.invoice_id) = 'Y'
			   AND ap_invoices_pkg.get_approval_status(ai.invoice_id, ai.invoice_amount, ai.payment_status_flag, ai.invoice_type_lookup_code) IN
				   ('APPROVED', 'CANCELLED') -- V.3.0
				   ))
				  /*AND ((aid.invoice_distribution_id, 'TAXINV') NOT IN
                  (SELECT tidl.invoice_distribution_id
                              ,tidl.invoice_type
                        FROM th_itemtax_distribution_loaded tidl
                        WHERE tidl.invoice_type = 'TAXINV'
                              AND tidl.invoice_distribution_id = aid.invoice_distribution_id) OR*/
				  /*(aip.invoice_payment_id, 'PAYINV') NOT IN
                  (SELECT tidld.invoice_distribution_id
                              ,tidld.invoice_type
                        FROM th_itemtax_distribution_loaded tidld
                        WHERE tidld.invoice_type = 'PAYINV'
                              AND tidld.invoice_distribution_id = aip.invoice_payment_id))*/
			   AND NOT EXISTS
			 		(SELECT 'x'
					 FROM th_itemtax_distribution_loaded tidl
					 WHERE tidl.invoice_type = 'PAYINV'
					 AND tidl.invoice_distribution_id = aip.invoice_payment_id)
				  /* End Edit by TM 9/11/2017*/
			   AND ai.invoice_id = aip.invoice_id
			   AND aip.check_id = ac.check_id
			   AND ac.bank_account_id = aba.bank_account_id
			   AND atc.global_attribute19 = hla.location_id
			   AND xap.source_table = 'AP_INVOICE_DISTRIBUTIONS'
			   AND xap.source_id = aid.invoice_distribution_id
			   AND ac.check_id = p_check_id
			   AND ai.org_id = p_org_id
			 GROUP BY ac.check_id,
					  ac.check_date,
					  atc.name,
					  hla.location_id,
					  ac.doc_sequence_value,
					  aba.bank_account_num,
					  ac.bank_account_name,
					  ac.vendor_name,
					  atc.tax_recovery_rate,
					  ac.check_number,
					  ai.org_id,
					  ac.status_lookup_code -- Add by TM 9/11/2017 : VAT & WHT CR
			 ORDER BY ac.doc_sequence_value;
	
		rec_head cursor_head%ROWTYPE;
	
		----- Cursor for Load Data to Table : th_taxinvoices_line
		CURSOR cursor_invoice IS
			SELECT ac.check_id transaction_id,
				   ac.check_number transaction_number,
				   ac.check_date transaction_date,
				   ai.invoice_id,
				   ai.invoice_num,
				   'S' type_tax,
				   atc.name tax_code,
				   ac.doc_sequence_value voucher_number,
				   atc.tax_recovery_rate,
				   hla.location_id location_id,
				   aba.bank_account_num bank_acct_number,
				   ac.bank_account_name bank_acct_name,
				   aid.project_id,
				   ---Edit by aeaum 18/01/2009----
				   --ac.vendor_name supplier_name,				  
				   nvl(aid.attribute10, ac.vendor_name) supplier_name,
				   -- Comment by TM 9/11/2017 : VAT & WHT CR
				   /*,aid.attribute9 base_amount
                   ,aid.attribute8 tax_amount */
				   ---------------------------------				  
				   user_id   created_by,
				   SYSDATE   creation_date,
				   user_id   last_updated_by,
				   SYSDATE   last_update_date,
				   user_id   last_update_login,
				   ai.org_id,
				   -- added by AP@BAS on 25-Dec-2014 : for link to Supplier Site.Tax Registration Number, Supplier Site.(WHT FF)Branch Number
 				   aid.attribute4 actual_sup_number,
				   aid.attribute5 actual_site_code,
				   -- Add by TM 9/11/2017 : VAT & WHT CR
				   ai.vendor_id supplier_id,
				   ai.vendor_site_id supplier_site_id,
				   NVL(atc.tax_rate, 0) tax_rate
			-- End Add by TM 9/11/2017 : VAT & WHT CR
			  FROM ap_invoices              ai,
				   ap_invoice_distributions aid,
				   ap_tax_codes             atc,
				   ap_invoice_payments      aip,
				   ap_checks                ac,
				   ap_bank_accounts         aba,
				   hr_locations             hla,
				   ap_ae_lines_all          xap
			 WHERE ai.invoice_id = aid.invoice_id
			   AND aid.tax_code_id = atc.tax_id
			   AND atc.global_attribute20 = 'Y'
			   AND aid.line_type_lookup_code = 'ITEM'
			   /* Edit by TM 9/11/2017 : VAT & WHT CR*/
				  /*AND ai.invoice_id IN (SELECT invoice_id
                  FROM ap_invoice_distributions
                  WHERE invoice_id = ai.invoice_id
                        AND line_type_lookup_code = 'TAX')*/
			   AND EXISTS (SELECT 'x'
					  FROM ap_invoice_distributions
					 WHERE invoice_id = ai.invoice_id
					   AND line_type_lookup_code = 'TAX')
			   AND ap_checks_pkg.get_posting_status(ac.check_id) = 'Y'
			   AND ((ac.status_lookup_code = 'VOIDED') 
			   OR  (ac.status_lookup_code <> 'VOIDED'
			   AND ap_invoices_pkg.get_posting_status(ai.invoice_id) = 'Y'
			   AND ap_invoices_pkg.get_approval_status(ai.invoice_id, ai.invoice_amount, ai.payment_status_flag, ai.invoice_type_lookup_code) IN
				   ('APPROVED', 'CANCELLED') -- V.3.0
				   ))
				  /*AND ((aip.invoice_payment_id, 'PAYINV') NOT IN
                  (SELECT tidl.invoice_distribution_id
                              ,tidl.invoice_type
                        FROM th_itemtax_distribution_loaded tidl
                        WHERE tidl.invoice_type = 'PAYINV'
                              AND tidl.invoice_distribution_id = aip.invoice_payment_id))*/
			   AND NOT EXISTS
			 		(SELECT 'x'
					 FROM th_itemtax_distribution_loaded tidl
					 WHERE tidl.invoice_type = 'PAYINV'
					 AND tidl.invoice_distribution_id = aip.invoice_payment_id)
			   /* End Edit by TM 9/11/2017*/
			   AND ai.invoice_id = aip.invoice_id
			   AND aip.check_id = ac.check_id
			   AND ac.bank_account_id = aba.bank_account_id
			   AND atc.global_attribute19 = hla.location_id
			   AND xap.source_table = 'AP_INVOICE_DISTRIBUTIONS'
			   AND xap.source_id = aid.invoice_distribution_id
			   AND ac.check_id = p_check_id
			   AND ai.org_id = p_org_id
			 GROUP BY ac.check_id,
					  ac.check_date,
					  ai.invoice_id,
					  ai.invoice_num,
					  atc.name,
					  hla.location_id,
					  ac.doc_sequence_value,
					  aba.bank_account_num,
					  ac.bank_account_name,
					  aid.project_id,
					  ----- Edit by aeaum 17/01/2009 -----
					  --ac.vendor_name,
					  nvl(aid.attribute10, ac.vendor_name),
					  -- Comment by TM 9/11/2017 : VAT & WHT CR
					  /*,aid.attribute9
                      ,aid.attribute8*/
					  ------------------------------------
					  atc.tax_recovery_rate,
					  ac.check_number,
					  ai.org_id,
					  aid.attribute4,
					  aid.attribute5,
					  -- Add by TM 9/11/2017 : VAT & WHT CR					 
					  ai.vendor_id,
					  ai.vendor_site_id,
					  NVL(atc.tax_rate, 0)
					  -- Add by TM 9/11/2017 : VAT & WHT CR
			 ORDER BY ac.doc_sequence_value;
	
		rec_invoice cursor_invoice%ROWTYPE;
	
		----- Cursor for Load Data to Table : th_itemtax_distribution_loaded
		CURSOR cursor_invoice_dis IS
			SELECT DISTINCT ai.invoice_id,
							ac.check_id,
							aid.invoice_distribution_id,
							aip.invoice_payment_id,
							ai.org_id,
							user_id                     created_by, --yvk20160607
							SYSDATE                     creation_date --yvk20160607
			  FROM ap_invoices              ai,
				   ap_invoice_distributions aid,
				   ap_tax_codes             atc,
				   ap_invoice_payments      aip,
				   ap_checks                ac,
				   ap_bank_accounts         aba,
				   hr_locations             hla,
				   ap_ae_lines_all          xap
			 WHERE ai.invoice_id = aid.invoice_id
			   AND aid.tax_code_id = atc.tax_id
			   AND atc.global_attribute20 = 'Y'
			   AND aid.line_type_lookup_code = 'ITEM'
			   /* Edit by TM 9/11/2017 : VAT & WHT CR*/
				  /*AND ai.invoice_id IN (SELECT invoice_id
                  FROM ap_invoice_distributions
                  WHERE invoice_id = ai.invoice_id
                        AND line_type_lookup_code = 'TAX')*/
			   AND EXISTS (SELECT 'x'
					  FROM ap_invoice_distributions
					 WHERE invoice_id = ai.invoice_id
					   AND line_type_lookup_code = 'TAX')
			   AND ap_checks_pkg.get_posting_status(ac.check_id) = 'Y'
			   AND ((ac.status_lookup_code = 'VOIDED') 
			   OR  (ac.status_lookup_code <> 'VOIDED'
			   AND ap_invoices_pkg.get_posting_status(ai.invoice_id) = 'Y'
			   AND ap_invoices_pkg.get_approval_status(ai.invoice_id, ai.invoice_amount, ai.payment_status_flag, ai.invoice_type_lookup_code) IN
				   ('APPROVED', 'CANCELLED') -- V.3.0
				   ))
				  /*AND ((aip.invoice_payment_id, 'PAYINV') NOT IN
                  (SELECT tidl.invoice_distribution_id
                              ,tidl.invoice_type
                        FROM th_itemtax_distribution_loaded tidl
                        WHERE tidl.invoice_type = 'PAYINV'
                              AND tidl.invoice_distribution_id = aip.invoice_payment_id))*/
			   AND NOT EXISTS
			 		(SELECT 'x'
					 FROM th_itemtax_distribution_loaded tidl
					 WHERE tidl.invoice_type = 'PAYINV'
					 AND tidl.invoice_distribution_id = aip.invoice_payment_id)
			   /* End Edit by TM 9/11/2017 : VAT & WHT CR*/
			   AND ai.invoice_id = aip.invoice_id
			   AND aip.check_id = ac.check_id
			   AND ac.bank_account_id = aba.bank_account_id
			   AND atc.global_attribute19 = hla.location_id
			   AND xap.source_table = 'AP_INVOICE_DISTRIBUTIONS'
			   AND xap.source_id = aid.invoice_distribution_id
			   AND ac.check_id = p_check_id
			   AND ai.org_id = p_org_id;
	
		rec_invoice_dis cursor_invoice_dis%ROWTYPE;
	
		v_nperiod VARCHAR(10);
	
		--added by AP@BAS on 16-Nov-2012 for capture time
		v_start_time_loop1  DATE;
		v_finish_time_loop1 DATE;
		v_start_time_loop2  DATE;
		v_finish_time_loop2 DATE;
		v_start_time_loop3  DATE;
		v_finish_time_loop3 DATE;
		v_start_time_loop4  DATE;
		v_finish_time_loop4 DATE;
		v_start_time_loop5  DATE;
		v_finish_time_loop5 DATE;
	BEGIN
		write_output4test('Start Process');
		write_log('Reqiest ID: ' || p_conc_request_id || ' ##Check ID: ' ||
				  p_check_id || ' ##Org ID: ' || p_org_id);
	
		v_insert := FALSE;
		write_output4test('step 1 get user_id');
		user_id := fnd_profile.value('USER_ID');
	
		v_start_time_loop1 := SYSDATE;
		write_log('     [' ||
				  to_char(v_start_time_loop1, 'dd-mm-yy hh24:mi:ss') || ']' ||
				  ' cursor cursor_head.' || ' : ' ||
				  'Fetch cursor cursor_head');
	
		OPEN cursor_head;
		LOOP
			FETCH cursor_head
				INTO rec_head;
			EXIT WHEN cursor_head%NOTFOUND;
		
			write_log('Check Number : ' || rec_head.transaction_number);
			/* Add by TM 9/11/2017 : VAT & WHT CR*/
			v_cancel_flag      := 'N';
			v_tran_cancel_flag := 'N';
			v_comfirmed_flag   := 'N';
			IF rec_head.payment_status = 'VOIDED' THEN
				cancel_taxinv_service(p_conc_request_id => p_conc_request_id,
									p_check_id      => rec_head.transaction_id,
									p_org_id          => p_org_id,
									p_user_id         => user_id,
									p_type_tax        => rec_head.type_tax,
									p_tax_code        => rec_head.tax_code,
									o_comfirmed_flag  => v_comfirmed_flag);
				IF v_comfirmed_flag = 'F' THEN
					v_skip_flag := 'Y';
				ELSIF v_comfirmed_flag = 'N' THEN
					v_skip_flag := 'N';
					v_cancel_flag := 'Y';
				ELSE
					v_skip_flag := 'N';
					v_cancel_flag := 'N';
				END IF;
				v_tran_cancel_flag := 'Y';				
			ELSE
				v_skip_flag   := 'N';
				v_cancel_flag := 'N';
				v_tran_cancel_flag := 'N';	
			END IF;
			write_output4test('skip_flag ' || v_skip_flag);
			IF v_skip_flag = 'N' THEN
				/* End Add by TM 9/11/2017 : VAT & WHT CR*/
				SELECT th_taxinvh_s.nextval INTO v_head_id FROM dual;
			
				v_start_time_loop2 := SYSDATE;
				write_log('          [' ||
						  to_char(v_start_time_loop2,
								  'dd-mm-yy hh24:mi:ss') || ']' ||
						  ' cursor cursor_invoice.' || ' : ' ||
						  'Fetch cursor cursor_invoice');
			
				OPEN cursor_invoice;
				LOOP
					FETCH cursor_invoice
						INTO rec_invoice;
					EXIT WHEN cursor_invoice%NOTFOUND;
					write_output4test('Invoice Number : ' ||
									  rec_invoice.invoice_num);
					IF nvl(rec_head.transaction_id, 0) = nvl(rec_invoice.transaction_id, 0) AND
					   nvl(rec_head.tax_code, '0') = nvl(rec_invoice.tax_code, '0') AND
					   nvl(rec_head.voucher_number, '0') = nvl(rec_invoice.voucher_number, '0') AND
					   nvl(rec_head.location_id, 0) = nvl(rec_invoice.location_id, 0) AND
					   nvl(rec_head.bank_acct_number, '0') = nvl(rec_invoice.bank_acct_number, '0')
					----Comment by aeaum 18/01/2009----
					-- AND NVL (rec_head.supplier_name, '0') = NVL (rec_invoice.supplier_name, '0')
					--------------------------------------------------------------
					 THEN
						SELECT th_taxinvl_s.nextval INTO v_line_id FROM dual;
					
						----- Calculate PAYMENT AMOUNT
						write_log('10: Calculate PAYMENT AMOUNT');
						BEGIN
							SELECT SUM(decode(acct_line_type,
											  'AWT',
											  nvl(xapas.accounted_cr, 0) - nvl(xapas.accounted_dr, 0),
											  nvl(xapas.accounted_dr, 0) - nvl(xapas.accounted_cr, 0)))
							  INTO v_payment_amt
							  FROM xla_ap_pay_ael_sl_v xapas
							 WHERE xapas.trx_hdr_id =
								   rec_head.transaction_id
							   AND applied_to_trx_hdr_number_disp = rec_invoice.invoice_num
							   AND applied_to_trx_hdr_number_disp IS NOT NULL							   
							   /* Edit by TM 9/11/2017 : VAT & WHT CR*/
							   /*AND ((xapas.ael_id, 'PAYACTINV') NOT IN (SELECT tidl.invoice_distribution_id
                                        ,tidl.invoice_type
                                  FROM th_itemtax_distribution_loaded tidl
                                  WHERE tidl.invoice_type = 'PAYACTINV'
                                        AND tidl.invoice_distribution_id = xapas.ael_id))*/
								AND ((rec_head.payment_status = 'VOIDED' AND xapas.accounting_event_type = 'PAYMENT CANCELLATION')
								OR	(rec_head.payment_status <> 'VOIDED' AND xapas.accounting_event_type <> 'PAYMENT CANCELLATION'))
								  /* End Edit by TM 9/11/2017*/
							   AND org_id = p_org_id;
						EXCEPTION
							WHEN no_data_found THEN
								v_payment_amt := 0;
							WHEN OTHERS THEN
								write_log('10 Error: ' || SQLCODE || '-' || SQLERRM);
								v_payment_amt := 0;
						END;
					
						----- Calculate INVOICE AMOUNT
						write_log('20: Calculate INVOICE AMOUNT (Transaction ID: ' || rec_head.transaction_id ||
								  ' Invoice ID: ' || rec_invoice.invoice_id);
						IF (v_comp_trans_id <> rec_head.transaction_id) OR
						   (v_comp_invoice_id <> rec_invoice.invoice_id) THEN
						
							v_comp_trans_id   := rec_head.transaction_id;
							v_comp_invoice_id := rec_invoice.invoice_id;
						
							BEGIN
								SELECT nvl(SUM(decode(sign(aip.amount), -1,
													  (decode(ap_invoices_pkg.get_approval_status(ai.invoice_id, ai.invoice_amount, ai.payment_status_flag, ai.invoice_type_lookup_code),
															  'CANCELLED',
															  nvl(xap.accounted_dr, 0),
															  nvl(xap.accounted_dr, 0) - nvl(xap.accounted_cr, 0))) * (-1),
													  (decode(ap_invoices_pkg.get_approval_status(ai.invoice_id, ai.invoice_amount, ai.payment_status_flag, ai.invoice_type_lookup_code),
															  'CANCELLED',
															  nvl(xap.accounted_dr, 0),
															  nvl(xap.accounted_dr, 0) - nvl(xap.accounted_cr, 0))))),
										   0)
								  INTO v_invoice_amt
								  FROM ap_invoices              ai,
									   ap_invoice_distributions aid,
									   ap_invoice_payments      aip,
									   ap_checks                ac,
									   ap_ae_lines_all          xap
								 WHERE ai.invoice_id = aid.invoice_id
								   AND aid.line_type_lookup_code IN ('ITEM', 'TAX')
								   /* Edit by TM 9/11/2017 : VAT & WHT CR*/
									  /*AND ap_invoices_pkg.get_posting_status(ai.invoice_id) = 'Y'
                                      AND ap_checks_pkg.get_posting_status(ac.check_id) = 'Y'
                                      AND ap_invoices_pkg.get_approval_status(ai.invoice_id, ai.invoice_amount, ai.payment_status_flag, ai.invoice_type_lookup_code) IN
                                      ('APPROVED', 'CANCELLED') -- V.3.0*/
									  /*AND ((aip.invoice_payment_id, 'PAYINV') NOT IN
                                      (SELECT tidl.invoice_distribution_id
                                                  ,tidl.invoice_type
                                            FROM th_itemtax_distribution_loaded tidl
                                            WHERE tidl.invoice_type = 'PAYINV'
                                                  AND tidl.invoice_distribution_id = aip.invoice_payment_id))*/
								   AND NOT EXISTS
								 		(SELECT 'x'
										 FROM th_itemtax_distribution_loaded tidl
										 WHERE tidl.invoice_type = 'PAYINV'
										 AND tidl.invoice_distribution_id = aip.invoice_payment_id)
								   /* End Edit by TM 9/11/2017*/
								   AND ai.invoice_id = aip.invoice_id
								   AND aip.check_id = ac.check_id
								   AND ac.check_id = rec_head.transaction_id
								   AND ai.invoice_id = rec_invoice.invoice_id
								   AND xap.source_id = aid.invoice_distribution_id
								   AND xap.source_table = 'AP_INVOICE_DISTRIBUTIONS'
								   AND ai.org_id = p_org_id;
							EXCEPTION
								WHEN no_data_found THEN
									v_invoice_amt := 0;
								WHEN OTHERS THEN
									write_log('20 Error : ' || SQLCODE || '-' ||
											  SQLERRM);
									v_invoice_amt := 0;
							END;
						END IF;
					
						----- Calculate BASE AMOUNT
						write_log('30: Calculate BASE AMOUNT');
						BEGIN
							-----Edit by aeaum 18/01/2009----
							-- Comment by TM 9/11/2017 : VAT & WHT CR
							/*IF rec_invoice.base_amount IS NOT NULL THEN
                              v_base_amt := rec_invoice.base_amount;
                            ELSE*/
							BEGIN
								SELECT nvl(SUM(decode(sign(aip.amount), -1,
													  (decode(ap_invoices_pkg.get_approval_status(ai.invoice_id, ai.invoice_amount, ai.payment_status_flag, ai.invoice_type_lookup_code),
															  'CANCELLED',
															  nvl(xap.accounted_dr, 0),
															  nvl(xap.accounted_dr, 0) - nvl(xap.accounted_cr, 0))) * (-1),
													  (decode(ap_invoices_pkg.get_approval_status(ai.invoice_id, ai.invoice_amount, ai.payment_status_flag, ai.invoice_type_lookup_code),
															  'CANCELLED',
															  nvl(xap.accounted_dr, 0),
															  nvl(xap.accounted_dr, 0) - nvl(xap.accounted_cr, 0))))),
										   0)
								  INTO v_base_amt
								  FROM ap_invoices              ai,
									   ap_invoice_distributions aid,
									   ap_tax_codes             atc,
									   ap_invoice_payments      aip,
									   ap_checks                ac,
									   ap_bank_accounts         aba,
									   hr_locations             hla,
									   ap_ae_lines_all          xap
								 WHERE ai.invoice_id = aid.invoice_id
								   AND nvl(aid.tax_code_id, 0) = atc.tax_id(+)
								   AND atc.global_attribute20(+) = 'Y'
								   AND aid.line_type_lookup_code = 'ITEM'
								   /* Edit by TM 9/11/2017 : VAT & WHT CR*/
									  /*AND ap_invoices_pkg.get_posting_status(ai.invoice_id) = 'Y'
                                      AND ap_checks_pkg.get_posting_status(ac.check_id) = 'Y'
                                      AND ap_invoices_pkg.get_approval_status(ai.invoice_id, ai.invoice_amount, ai.payment_status_flag, ai.invoice_type_lookup_code) IN
                                      ('APPROVED', 'CANCELLED') -- V.3.0*/
									  /*AND ((aid.invoice_distribution_id, 'TAXINV') NOT IN
                                      (SELECT tidl.invoice_distribution_id
                                                  ,tidl.invoice_type
                                            FROM th_itemtax_distribution_loaded tidl
                                            WHERE tidl.invoice_type = 'TAXINV'
                                                  AND tidl.invoice_distribution_id = aid.invoice_distribution_id) OR*/
									   /*(aip.invoice_payment_id, 'PAYINV') NOT IN
                                       (SELECT tidld.invoice_distribution_id
                                                   ,tidld.invoice_type
                                             FROM th_itemtax_distribution_loaded tidld
                                             WHERE tidld.invoice_type = 'PAYINV'
                                                   AND tidld.invoice_distribution_id = aip.invoice_payment_id))*/
								   AND NOT EXISTS
										(SELECT 'x'
										   FROM th_itemtax_distribution_loaded tidl
										  WHERE tidl.invoice_type = 'PAYINV'
											AND tidl.invoice_distribution_id = aip.invoice_payment_id)
									  /* End Edit by TM 9/11/2017*/
								   AND ai.invoice_id = aip.invoice_id
								   AND aip.check_id = ac.check_id
								   AND atc.name = rec_head.tax_code
								   AND aba.bank_account_num = rec_head.bank_acct_number
								   AND nvl(aid.project_id, 0) = nvl(rec_invoice.project_id, 0)
								   AND ac.bank_account_id = aba.bank_account_id
								   AND atc.global_attribute19 = hla.location_id
								   AND xap.source_id = aid.invoice_distribution_id
								   AND xap.source_table = 'AP_INVOICE_DISTRIBUTIONS'
								   AND ac.check_id = rec_head.transaction_id
								   AND ai.invoice_id = rec_invoice.invoice_id
								   AND ai.org_id = p_org_id
								   AND nvl(aid.attribute10, ac.vendor_name) = rec_invoice.supplier_name
								   AND NVL(aid.attribute4,'###') = NVL(rec_invoice.actual_sup_number,'###')
								   AND NVL(aid.attribute5,'###') = NVL(rec_invoice.actual_site_code,'###')
								   AND ai.vendor_id = rec_invoice.supplier_id
								   AND ai.vendor_site_id = rec_invoice.supplier_site_id
								   ;
							EXCEPTION
								WHEN no_data_found THEN
									v_base_amt := 0;
								WHEN OTHERS THEN
									write_log('30 Error: ' || SQLCODE || '-' ||
											  SQLERRM);
									v_base_amt := 0;
							END;
							/*END IF;*/
						END;
					
						----- Calculate for Partial Payment
						write_log('40: Calculate for Partial Payment');
						IF nvl(v_invoice_amt, 0) <> 0 THEN
							v_paid_percent := nvl(v_payment_amt, 0) /
											  nvl(v_invoice_amt, 0);
						ELSE
							v_paid_percent := 0;
						END IF;
					
						v_paid_base_inv := round(nvl(v_base_amt, 0) *
												 nvl(v_paid_percent, 0),
												 2);
						/* Edit by TM 9/11/2017 : VAT & WHT CR*/
						/*v_paid_rec_tax :=  ----- Edit by aeaum 18/01/2009 -----
                         nvl(rec_invoice.tax_amount
                                             ,(round(round(round(round(nvl(v_paid_base_inv, 0) * 7, 2) / 100, 2) * nvl(rec_head.tax_recovery_rate, 0), 2) / 100, 2)));
                        \*
                        ROUND (ROUND (ROUND (ROUND (v_paid_base_inv * 7, 2) / 100, 2) * rec_head.tax_recovery_rate, 2) / 100, 2);
                        *\*/
						v_paid_rec_tax := round(round(round(round(nvl(v_paid_base_inv,0) * rec_invoice.tax_rate,2) / 100,2) *
													  nvl(rec_head.tax_recovery_rate,0),2) / 100,2);
					
						--------------------------------------------------
						/*v_paid_nonrec_tax := round(round(round(round(nvl(v_paid_base_inv, 0) * 7, 2) / 100, 2) * (100 - nvl(rec_head.tax_recovery_rate, 0)), 2) / 100
                        ,2);*/
						v_paid_nonrec_tax := round(round(round(round(nvl(v_paid_base_inv,0) * rec_invoice.tax_rate,2) / 100,2) *
														 (100 - nvl(rec_head.tax_recovery_rate,0)),2) / 100,2);
						/* End Edit by TM 9/11/2017 : VAT & WHT CR*/
						----------add by aeaum 28/09/2009----------
						SELECT period_name
						  INTO v_nperiod
						  FROM gl_periods_v
						 WHERE period_set_name = 'Accounting' -- 'TCORE_Calendar'
						   AND (trunc(rec_invoice.transaction_date) BETWEEN
							   start_date AND end_date)
						   AND adjustment_period_flag = 'N'
						 ORDER BY start_date;
						-------------------------------------------------------
					
						-- added by AP@BAS on 25-Dec-2014 : find vat_registeration_number / Branch Name from supplier_site table
						-- 45
						v_tax_id          := NULL;
						v_branch_name     := NULL;
						v_actual_sup_name := NULL;
						/* Edit by TM on 9/11/2017 : VAT & WHT CR*/
						v_vendor_id := NULL;
						v_vendor_site_code := NULL;
						/*IF rec_invoice.actual_sup_number IS NOT NULL AND rec_invoice.actual_site_code IS NOT NULL THEN
                          BEGIN
                            SELECT pv.vendor_name
                                  ,pvs.vat_registration_num
                                  ,pvs.global_attribute14
                            INTO v_actual_sup_name
                                ,v_tax_id
                                ,v_branch_name
                            FROM po_vendors pv
                                ,po_vendor_sites_all pvs
                            WHERE pv.vendor_id = pvs.vendor_id
                                  AND pv.segment1 = rec_invoice.actual_sup_number
                                  AND pvs.vendor_site_code = rec_invoice.actual_site_code
                                  AND pvs.org_id = rec_head.org_id;
                        
                          EXCEPTION
                            WHEN OTHERS THEN
                              NULL;
                          END;
                        END IF;*/
						IF rec_invoice.actual_sup_number IS NULL THEN
							BEGIN
								SELECT pv.vendor_name, pv.vendor_id, pvs.vat_registration_num, pvs.global_attribute14 ,pvs.vendor_site_code
								  INTO v_actual_sup_name, v_vendor_id, v_tax_id, v_branch_name ,v_vendor_site_code
								  FROM po_vendors          pv,
									   po_vendor_sites_all pvs
								 WHERE pv.vendor_id = rec_invoice.supplier_id
								   AND pv.vendor_id = pvs.vendor_id
								   AND pvs.vendor_site_id = rec_invoice.supplier_site_id
								   AND pvs.org_id = rec_head.org_id;
							EXCEPTION
								WHEN OTHERS THEN
									NULL;
							END;
						ELSE
							BEGIN
								SELECT pv.vendor_name, pv.vendor_id, pv.vat_registration_num
								  INTO v_actual_sup_name, v_vendor_id, v_tax_id
								  FROM po_vendors pv
								 WHERE pv.segment1 = rec_invoice.actual_sup_number;
							
								IF rec_invoice.actual_site_code IS NOT NULL THEN
									v_vendor_site_code	:=	rec_invoice.actual_site_code;
									SELECT pvs.vat_registration_num, pvs.global_attribute14
									  INTO v_tax_id, v_branch_name
									  FROM po_vendor_sites_all pvs
									 WHERE pvs.vendor_id = v_vendor_id
									   AND pvs.vendor_site_code = rec_invoice.actual_site_code
									   AND pvs.org_id = rec_head.org_id;	   
								END IF;
							EXCEPTION
								WHEN OTHERS THEN
									NULL;
							END;
						END IF;
						v_branch_name := NVL(v_branch_name,'�ӹѡ�ҹ�˭�');
						v_vendor_site_code	:=	NVL(v_vendor_site_code,'-');
						/* End Edit by TM on 9/11/2017 : VAT & WHT CR */
					
						----- Insert Data to table : th_taxinvoices_line
						write_log('50: Insert Data to table : th_taxinvoices_line');
						BEGIN
							INSERT INTO th_taxinvoices_line
								(th_til_id,
								 th_tih_id,
								 th_til_mode,
								 th_til_period_name,
								 th_til_tax_inv_id,
								 th_til_tax_inv_number,
								 th_til_tax_inv_date,
								 th_til_supplier_name,
								 th_til_project_id,
								 th_til_base_amt,
								 th_til_tax_amt,
								 th_til_non_rec_amt,
								 th_til_rec_amt,
								 attribute1,
								 created_by,
								 creation_date,
								 last_updated_by,
								 last_update_date,
								 last_update_login,
								 th_til_location_id,
								 org_id,
								 -- added by AP@BAS on 25-Dec-2014								
								 attribute29,
								 attribute30,
								 attribute28)
							VALUES
								(v_line_id,
								 v_head_id,
								 'A',
								 --v_nperiod, -- Commented by Korn.iCE on 20-JAN-2009.
								 NULL, -- Added by Korn.iCE on 20-JAN-2009.
								 rec_invoice.invoice_id,
								 --rec_invoice.invoice_num, -- Commented by Korn.iCE on 20-JAN-2009.
								 'N/A', -- Added by Korn.iCE on 20-JAN-2009.
								 rec_head.transaction_date,
								 ---Edit by aeaum 17/01/2009---
								 --rec_head.supplier_name,
								 --,rec_invoice.supplier_name								
								 v_actual_sup_name, -- Edit by TM 9/11/2017 : VAT & WHT CR 
								 ------------------------------								
								 rec_invoice.project_id,
								 v_paid_base_inv,
								 v_paid_rec_tax + v_paid_nonrec_tax,
								 v_paid_nonrec_tax,
								 v_paid_rec_tax,
								 p_conc_request_id,
								 rec_head.created_by,
								 rec_head.creation_date,
								 rec_head.last_updated_by,
								 rec_head.last_update_date,
								 rec_head.last_update_login,
								 rec_head.location_id,
								 rec_head.org_id,
								 --added by AP@BAS on 25-Dec-2014								
								 substr(v_tax_id, 1, 13),
								 substr(v_branch_name, 1, 20),
								 v_vendor_site_code);
						EXCEPTION
							WHEN OTHERS THEN
								write_log('50 Error: Cannot Insert TH_TaxInvoices_Line: ' ||
										  SQLCODE || '-' || SQLERRM);
						END;
					
						v_check_base_inv   := nvl(v_check_base_inv, 0) + nvl(v_paid_base_inv, 0);
						v_check_rec_tax    := nvl(v_check_rec_tax, 0) + nvl(v_paid_rec_tax, 0);
						v_check_nonrec_tax := nvl(v_check_nonrec_tax, 0) + nvl(v_paid_nonrec_tax, 0);
					END IF;
				END LOOP;
				CLOSE cursor_invoice;
				v_finish_time_loop2 := SYSDATE;
				write_log('          [' || to_char(v_finish_time_loop2, 'dd-mm-yy hh24:mi:ss') || ']' ||
						  ' cursor cursor_invoice.' || ' : ' || 'Close cursor cursor_invoice');
				write_log('          cursor_invoice used time (d/h/m) = ' ||
						  calculate_time(v_start_time_loop2, v_finish_time_loop2));
			
				v_insert := TRUE;
				----- Insert Data to table : th_taxinvoices_header
				write_log('60: Insert Data to table : th_taxinvoices_header');
				BEGIN
					INSERT INTO th_taxinvoices_header
						(th_tih_id,
						 th_tih_number,
						 th_tih_transaction_id,
						 th_tih_transaction_number,
						 th_tih_transaction_date,
						 th_tih_type_tax,
						 th_tih_tax_code,
						 th_tih_location_id,
						 th_tih_voucher_number,
						 th_tih_bank_acct_number,
						 th_tih_bank_acct_name,
						 th_tih_non_rec_amt,
						 th_tih_rec_amt,
						 th_tih_rec_rate,
						 th_tih_base_amt,
						 attribute1,
						 created_by,
						 creation_date,
						 last_updated_by,
						 last_update_date,
						 last_update_login,
						 org_id,
						 --Add by TM on 9/11/2017 : VAT & WHT CR						
						 cancel_flag,
						 tran_cancel_flag,
						 tran_cancel_date
						 -- End Add by TM on 9/11/2017 : VAT & WHT CR
						 )
					VALUES
						(v_head_id,
						 v_head_id,
						 rec_head.transaction_id,
						 rec_head.transaction_number,
						 rec_head.transaction_date,
						 rec_head.type_tax,
						 rec_head.tax_code,
						 rec_head.location_id,
						 rec_head.voucher_number,
						 rec_head.bank_acct_number,
						 rec_head.bank_acct_name,
						 v_check_nonrec_tax,
						 v_check_rec_tax,
						 rec_head.tax_recovery_rate,
						 v_check_base_inv,
						 p_conc_request_id,
						 rec_head.created_by,
						 rec_head.creation_date,
						 rec_head.last_updated_by,
						 rec_head.last_update_date,
						 rec_head.last_update_login,
						 rec_head.org_id,
						 --Add by TM on 9/11/2017 : VAT & WHT CR						
						 v_cancel_flag,
						 v_tran_cancel_flag,
						 trunc(SYSDATE)
						 -- End Add by TM on 9/11/2017 : VAT & WHT CR
						 );
				EXCEPTION
					WHEN OTHERS THEN
						write_log('60 Error: Cannot Insert TH_TaxInvoices_Header: ' ||
								  SQLCODE || '-' || SQLERRM);
				END;
			END IF; -- v_skip_flag = 'N' /* End Add by TM 9/11/2017 : VAT & WHT CR*/
		END LOOP;
		CLOSE cursor_head;
		v_finish_time_loop1 := SYSDATE;
		write_log('     [' ||
				  to_char(v_finish_time_loop1, 'dd-mm-yy hh24:mi:ss') || ']' ||
				  ' cursor cursor_head.' || ' : ' || 'Close cursor cursor_head');
		write_log('     cursor_head used time (d/h/m) = ' ||
				  calculate_time(v_start_time_loop1, v_finish_time_loop1));
	
		----- Insert Data to table : th_itemtax_distribution_loaded
		----- For Check Data Loaded
		IF v_insert THEN
			write_log('70: Insert Data to table : th_itemtax_distribution_loaded');
		
			v_start_time_loop1 := SYSDATE;
			write_log('     [' ||
					  to_char(v_start_time_loop1, 'dd-mm-yy hh24:mi:ss') || ']' ||
					  ' cursor cursor_head.' || ' : ' || 'Fetch cursor cursor_head');
			OPEN cursor_head;
			LOOP
				FETCH cursor_head
					INTO rec_head;
				EXIT WHEN cursor_head%NOTFOUND;
			
				v_start_time_loop2 := SYSDATE;
				write_log('          [' ||
						  to_char(v_start_time_loop2, 'dd-mm-yy hh24:mi:ss') || ']' ||
						  ' cursor cursor_invoice.' || ' : ' || 'Fetch cursor cursor_invoice');
				OPEN cursor_invoice;
				LOOP
					FETCH cursor_invoice
						INTO rec_invoice;
					EXIT WHEN cursor_invoice%NOTFOUND;
					BEGIN
						INSERT INTO th_itemtax_distribution_loaded
							(invoice_distribution_id,
							 invoice_type,
							 org_id,
							 created_by,
							 creation_date) --yvk20160607
							SELECT ael_id,
								   'PAYACTINV',
								   org_id,
								   user_id    created_by --yvk20160607
								  ,SYSDATE    creation_date --yvk20160607
							  FROM xla_ap_pay_ael_sl_v xapas
							 WHERE xapas.trx_hdr_id = rec_head.transaction_id
							   AND applied_to_trx_hdr_number_disp = rec_invoice.invoice_num
							   AND applied_to_trx_hdr_number_disp IS NOT NULL
							   AND ((xapas.ael_id, 'PAYACTINV') NOT IN
								   (SELECT tidl.invoice_distribution_id, tidl.invoice_type
									   FROM th_itemtax_distribution_loaded tidl
									  WHERE tidl.invoice_type = 'PAYACTINV'
										AND tidl.invoice_distribution_id = xapas.ael_id))
							   AND org_id = p_org_id;
					EXCEPTION
						WHEN OTHERS THEN
							write_log('71 Error: Cannot insert TH_ItemTax_Distribution_Loaded: ' ||
									  SQLCODE || '-' || SQLERRM);
					END;
				END LOOP;
				CLOSE cursor_invoice;
				v_finish_time_loop2 := SYSDATE;
				write_log('     [' ||
						  to_char(v_finish_time_loop2, 'dd-mm-yy hh24:mi:ss') || ']' ||
						  ' cursor cursor_invoice.' || ' : ' || 'Close cursor cursor_invoice');
				write_log('          cursor_invoice used time (d/h/m) = ' ||
						  calculate_time(v_start_time_loop2, v_finish_time_loop2));
			END LOOP;
			CLOSE cursor_head;
			v_finish_time_loop1 := SYSDATE;
			write_log('     [' ||
					  to_char(v_start_time_loop1, 'dd-mm-yy hh24:mi:ss') || ']' ||
					  ' cursor cursor_invoice_dis.' || ' : ' || 'Close cursor cursor_head');
			write_log('     cursor_invoice used time (d/h/m) = ' ||
					  calculate_time(v_start_time_loop1, v_finish_time_loop1));
		
			v_start_time_loop1 := SYSDATE;
			write_log('     [' ||
					  to_char(v_start_time_loop1, 'dd-mm-yy hh24:mi:ss') || ']' ||
					  ' cursor cursor_invoice_dis.' || ' : ' || 'Fetch cursor cursor_invoice_dis');
			OPEN cursor_invoice_dis;
			LOOP
				FETCH cursor_invoice_dis
					INTO rec_invoice_dis;
				EXIT WHEN cursor_invoice_dis%NOTFOUND;
				BEGIN
					INSERT INTO th_itemtax_distribution_loaded
						(invoice_distribution_id,
						 invoice_type,
						 org_id,
						 created_by,
						 creation_date)
					VALUES
						(rec_invoice_dis.invoice_distribution_id,
						 'TAXINV',
						 rec_invoice_dis.org_id,
						 rec_invoice_dis.created_by,
						 rec_invoice_dis.creation_date);
				EXCEPTION
					WHEN OTHERS THEN
						write_log('72 Error: Cannot insert TH_ItemTax_Distribution_Loaded: ' ||
								  SQLCODE || '-' || SQLERRM);
				END;
			
				BEGIN
					INSERT INTO th_itemtax_distribution_loaded
						(invoice_distribution_id,
						 invoice_type,
						 org_id,
						 created_by,
						 creation_date)
					VALUES
						(rec_invoice_dis.invoice_payment_id,
						 'PAYINV',
						 rec_invoice_dis.org_id,
						 rec_invoice_dis.created_by,
						 rec_invoice_dis.creation_date);
				EXCEPTION
					WHEN OTHERS THEN
						write_log('73 Error: Cannot insert TH_ItemTax_Distribution_Loaded: ' ||
								  SQLCODE || '-' || SQLERRM);
				END;
			END LOOP;
			CLOSE cursor_invoice_dis;
			v_finish_time_loop1 := SYSDATE;
			write_log('     [' ||
					  to_char(v_start_time_loop1, 'dd-mm-yy hh24:mi:ss') || ']' ||
					  ' cursor cursor_invoice_dis.' || ' : ' ||
					  'Close cursor cursor_invoice_dis');
			write_log('     cursor_invoice_dis used time (d/h/m) = ' ||
					  calculate_time(v_start_time_loop1,
									 v_finish_time_loop1));
		END IF;
		COMMIT;
	
	EXCEPTION
		WHEN OTHERS THEN
			raise_application_error(-20101, 'Saving Error!');
			ROLLBACK;
		
	END th_load_taxinv_service;

	/*================================================================
      PROCEDURE NAME:   th_load_wht_a()
    ==================================================================*/
	PROCEDURE th_load_wht_a(p_conc_request_id IN NUMBER,
							p_check_id        IN NUMBER,
							p_org_id          IN NUMBER) IS
		user_id                       NUMBER;
		v_head_id                     NUMBER;
		v_line_id                     NUMBER;
		v_base_amt                    NUMBER;
		v_non_rec_tax                 NUMBER;
		v_rec_tax                     NUMBER;
		v_non_rec_amt                 NUMBER;
		v_pi_result                   NUMBER(13, 10);
		v_rec_amt                     NUMBER;
		v_tax_amt                     NUMBER;
		v_payment_amt                 NUMBER;
		v_invoice_amt                 NUMBER;
		v_project_id                  NUMBER;
		v_bproject                    BOOLEAN;
		v_insert                      BOOLEAN;
		v_paid_percent                NUMBER;
		v_paid_base_inv               NUMBER;
		v_paid_rec_tax                NUMBER;
		v_paid_nonrec_tax             NUMBER;
		v_check_base_inv              NUMBER;
		v_check_rec_tax               NUMBER;
		v_check_nonrec_tax            NUMBER;
		v_period                      VARCHAR2(10);
		v_seq_name                    VARCHAR2(50);
		v_query                       VARCHAR2(800);
		cursor1                       INTEGER;
		v_wht_num                     NUMBER;
		rows_processed                INTEGER;
		v_check_base                  NUMBER;
		v_line_base                   NUMBER;
		v_inv_base                    NUMBER;
		v_inv_tax                     NUMBER;
		v_line_tax                    NUMBER;
		v_check_tax                   NUMBER;
		v_pay_act                     NUMBER;
		v_base_amt_line               NUMBER;
		v_tax_amt_line                NUMBER;
		v_nperiod                     VARCHAR2(10);
		v_ai_invoice_id               NUMBER(15);
		v_ai_invoice_amount           NUMBER;
		v_ai_payment_status_flag      VARCHAR2(1 BYTE);
		v_ai_invoice_type_lookup_code VARCHAR2(25 BYTE);
	
		-- Cursor for Load Data to Table : th_wht_header
		CURSOR cursor_head IS
			SELECT ac.check_id,
				   ac.check_number doc_number,
				   ac.check_date doc_date,
				   'PAY' doc_type,
				   'Bank Account' doc_relate,
				   hla.location_id location_id,
				   ac.doc_sequence_value voucher_number,
				   to_char(ac.check_date, 'MON-YYYY') period_name,
				   atc.global_attribute16 pnd,
				   user_id created_by,
				   SYSDATE creation_date,
				   user_id last_updated_by,
				   SYSDATE last_update_date,
				   user_id last_update_login,
				   trunc(aip.accounting_date) accounting_date,
				   ai.org_id -- V2.0
			  FROM ap_checks                ac,
				   ap_invoice_payments      aip,
				   ap_invoices              ai,
				   ap_invoice_distributions aid,
				   ap_tax_codes             atc,
				   hr_locations             hla,
				   po_vendors               pv,
				   po_vendor_sites          pvs,
				   ap_ae_lines_all          xap
			 WHERE ac.check_id = aip.check_id
			   AND aip.invoice_id = ai.invoice_id
			   AND ai.invoice_id = aid.invoice_id
			   AND aid.line_type_lookup_code = 'AWT'
			   AND nvl(aid.awt_flag, 'A') = 'A'
			   AND aid.tax_code_id = atc.tax_id
			   AND atc.global_attribute19 = hla.location_id
			   AND ac.vendor_id = pv.vendor_id
			   AND ac.vendor_site_id = pvs.vendor_site_id
			   AND ((aip.invoice_payment_id, 'PAYWHT') NOT IN
				   (SELECT tidl.invoice_distribution_id, tidl.invoice_type
					   FROM th_itemtax_distribution_loaded tidl
					  WHERE tidl.invoice_type = 'PAYWHT'
						AND tidl.invoice_distribution_id =
							aip.invoice_payment_id))
			   AND ap_invoices_pkg.get_posting_status(ai.invoice_id) = 'Y'
			   AND ap_checks_pkg.get_posting_status(ac.check_id) = 'Y'
			   AND ap_invoices_pkg.get_approval_status(ai.invoice_id,
													   ai.invoice_amount,
													   ai.payment_status_flag,
													   ai.invoice_type_lookup_code) =
				   'APPROVED'
			   AND xap.source_table = 'AP_INVOICE_DISTRIBUTIONS'
			   AND xap.source_id = aid.invoice_distribution_id
			   AND ac.check_id = p_check_id
			   AND ai.org_id = p_org_id ----add by aeaum 23/09/2009------
			 GROUP BY ac.doc_sequence_value,
					  ac.check_id,
					  ac.check_number,
					  ac.check_date,
					  'PAY',
					  'Bank Account',
					  hla.location_id,
					  to_char(ac.check_date, 'MON-YYYY'),
					  atc.global_attribute16,
					  trunc(aip.accounting_date),
					  ai.org_id -- V2.0
			 ORDER BY ac.doc_sequence_value;
	
		rec_head cursor_head%ROWTYPE;
	
		-- Cursor for Load Data to Table : th_wht_line
		CURSOR cursor_line IS
			SELECT ac.check_id,
				   ac.check_number doc_number,
				   ac.check_date doc_date,
				   'PAY' doc_type,
				   'Bank Account' doc_relate,
				   hla.location_id location_id,
				   ac.doc_sequence_value voucher_number,
				   to_char(ac.check_date, 'MON-YYYY') period_name,
				   atc.global_attribute16 pnd,
				   atc.name wht_code,
				   nvl(atc.global_attribute18, pv.global_attribute17) revenue_type,
				   nvl(atc.global_attribute17, pv.global_attribute18) revenue_name,
				   nvl(pvs.global_attribute18, ac.vendor_name) supplier_name,
				   nvl(pvs.global_attribute17, ac.address_line1) supplier_address1,
				   decode(nvl(length(rtrim(ltrim(pvs.global_attribute17))),
							  0),
						  0,
						  ac.address_line2,
						  '') supplier_address2, -- V4.0
				   decode(nvl(length(rtrim(ltrim(pvs.global_attribute17))),
							  0),
						  0,
						  ac.address_line3,
						  '') supplier_address3, -- V4.0
				   nvl(pvs.global_attribute19, pv.vat_registration_num) supplier_tax_id,
				   pvs.global_attribute20 supplier_branch_no,
				   '' agent_name,
				   '' agent_address,
				   '' agent_tax_id,
				   '' agent_branch_no,
				   hla.attribute15 resp_name,
				   hla.address_line_1 resp_address1,
				   hla.address_line_2 resp_address2,
				   hla.address_line_3 || ' ' || hla.region_2 || ' ' ||
				   hla.postal_code resp_address3,
				   hla.attribute1 resp_tax_id,
				   hla.attribute14 resp_branch_no,
				   awt_rate.tax_rate,
				   user_id created_by,
				   SYSDATE creation_date,
				   user_id last_updated_by,
				   SYSDATE last_update_date,
				   user_id last_update_login,
				   ai.org_id
			  FROM ap_checks                ac,
				   ap_invoice_payments      aip,
				   ap_invoices              ai,
				   ap_invoice_distributions aid,
				   ap_tax_codes             atc,
				   hr_locations             hla,
				   po_vendors               pv,
				   po_vendor_sites          pvs,
				   ap_ae_lines_all          xap,
				   ap_awt_tax_rates_all     awt_rate
			 WHERE ac.check_id = aip.check_id
			   AND aip.invoice_id = ai.invoice_id
			   AND ai.invoice_id = aid.invoice_id
			   AND aid.line_type_lookup_code = 'AWT'
			   AND nvl(aid.awt_flag, 'A') = 'A'
			   AND aid.tax_code_id = atc.tax_id
			   AND atc.global_attribute19 = hla.location_id
			   AND ac.vendor_id = pv.vendor_id
			   AND awt_rate.tax_name = atc.name
			   AND SYSDATE >= nvl(awt_rate.start_date, '01-JAN-1900')
			   AND SYSDATE <= nvl(awt_rate.end_date, '01-JAN-2900')
			   AND awt_rate.rate_type = 'STANDARD'
			   AND ac.vendor_site_id = pvs.vendor_site_id
			   AND ((aid.invoice_distribution_id, 'WHT') NOT IN
				   (SELECT tidla.invoice_distribution_id,
							tidla.invoice_type
					   FROM th_itemtax_distribution_loaded tidla
					  WHERE tidla.invoice_type = 'WHT'
						AND tidla.invoice_distribution_id =
							aid.invoice_distribution_id) OR
				   (aip.invoice_payment_id, 'PAYWHT') NOT IN
				   (SELECT tidlb.invoice_distribution_id,
							tidlb.invoice_type
					   FROM th_itemtax_distribution_loaded tidlb
					  WHERE tidlb.invoice_type = 'PAYWHT'
						AND tidlb.invoice_distribution_id =
							aip.invoice_payment_id))
			   AND ap_invoices_pkg.get_posting_status(ai.invoice_id) = 'Y'
			   AND ap_checks_pkg.get_posting_status(ac.check_id) = 'Y'
			   AND ap_invoices_pkg.get_approval_status(ai.invoice_id,
													   ai.invoice_amount,
													   ai.payment_status_flag,
													   ai.invoice_type_lookup_code) =
				   'APPROVED'
			   AND xap.source_table = 'AP_INVOICE_DISTRIBUTIONS'
			   AND xap.source_id = aid.invoice_distribution_id
			   AND ac.check_id = p_check_id
			   AND ai.org_id = p_org_id ----add by aeaum 23/09/2009------
			 GROUP BY ac.check_id,
					  ac.check_number,
					  hla.location_id,
					  ac.check_date,
					  atc.global_attribute16,
					  atc.name,
					  nvl(atc.global_attribute18, pv.global_attribute17),
					  nvl(atc.global_attribute17, pv.global_attribute18),
					  nvl(pvs.global_attribute18, ac.vendor_name),
					  nvl(pvs.global_attribute17, ac.address_line1),
					  decode(nvl(length(rtrim(ltrim(pvs.global_attribute17))),
								 0),
							 0,
							 ac.address_line2,
							 ''),
					  decode(nvl(length(rtrim(ltrim(pvs.global_attribute17))),
								 0),
							 0,
							 ac.address_line3,
							 ''),
					  nvl(pvs.global_attribute19, pv.vat_registration_num),
					  pvs.global_attribute20,
					  hla.attribute15,
					  hla.address_line_1,
					  hla.address_line_2,
					  hla.address_line_3 || ' ' || hla.region_2 || ' ' ||
					  hla.postal_code,
					  hla.attribute1,
					  hla.attribute14,
					  ac.doc_sequence_value,
					  awt_rate.tax_rate,
					  ai.org_id
			 ORDER BY ac.doc_sequence_value;
	
		rec_line cursor_line%ROWTYPE;
	
		-- Cursor for Load Data from Invoice to Table : th_wht_line
		CURSOR cursor_invoice IS
			SELECT ac.check_id,
				   ai.invoice_num,
				   ai.invoice_id,
				   aip.invoice_payment_id,
				   ac.check_number doc_number,
				   ac.check_date doc_date,
				   'PAY' doc_type,
				   'Bank Account' doc_relate,
				   hla.location_id location_id,
				   ac.doc_sequence_value voucher_number,
				   to_char(ac.check_date, 'MON-YYYY') period_name,
				   atc.global_attribute16 pnd,
				   atc.name wht_code,
				   nvl(atc.global_attribute18, pv.global_attribute17) revenue_type,
				   nvl(atc.global_attribute17, pv.global_attribute18) revenue_name,
				   nvl(pvs.global_attribute18, ac.vendor_name) supplier_name,
				   nvl(pvs.global_attribute17, ac.address_line1) supplier_address1,
				   decode(nvl(length(rtrim(ltrim(pvs.global_attribute17))),
							  0),
						  0,
						  ac.address_line2,
						  '') supplier_address2, -- V4.0
				   decode(nvl(length(rtrim(ltrim(pvs.global_attribute17))),
							  0),
						  0,
						  ac.address_line3,
						  '') supplier_address3, -- V4.0
				   nvl(pvs.global_attribute19, pv.vat_registration_num) supplier_tax_id,
				   pvs.global_attribute20 supplier_branch_no,
				   '' agent_name,
				   '' agent_address,
				   '' agent_tax_id,
				   '' agent_branch_no,
				   hla.attribute15 resp_name,
				   hla.address_line_1 resp_address1,
				   hla.address_line_2 resp_address2,
				   hla.address_line_3 || ' ' || hla.region_2 || ' ' ||
				   hla.postal_code resp_address3,
				   hla.attribute1 resp_tax_id,
				   hla.attribute14 resp_branch_no,
				   user_id created_by,
				   SYSDATE creation_date,
				   user_id last_updated_by,
				   SYSDATE last_update_date,
				   user_id last_update_login,
				   ai.org_id
			  FROM ap_checks                ac,
				   ap_invoice_payments      aip,
				   ap_invoices              ai,
				   ap_invoice_distributions aid,
				   ap_tax_codes             atc,
				   hr_locations             hla,
				   po_vendors               pv,
				   po_vendor_sites          pvs,
				   ap_ae_lines_all          xap
			 WHERE ac.check_id = aip.check_id
			   AND aip.invoice_id = ai.invoice_id
			   AND ai.invoice_id = aid.invoice_id
			   AND aid.line_type_lookup_code = 'AWT'
			   AND nvl(aid.awt_flag, 'A') = 'A'
			   AND aid.tax_code_id = atc.tax_id
			   AND atc.global_attribute19 = hla.location_id
			   AND ac.vendor_id = pv.vendor_id
			   AND ac.vendor_site_id = pvs.vendor_site_id
			   AND ((aip.invoice_payment_id, 'PAYWHT') NOT IN
				   (SELECT tidl.invoice_distribution_id, tidl.invoice_type
					   FROM th_itemtax_distribution_loaded tidl
					  WHERE tidl.invoice_type = 'PAYWHT'
						AND tidl.invoice_distribution_id =
							aip.invoice_payment_id))
			   AND ap_invoices_pkg.get_posting_status(ai.invoice_id) = 'Y'
			   AND ap_checks_pkg.get_posting_status(ac.check_id) = 'Y'
			   AND ap_invoices_pkg.get_approval_status(ai.invoice_id,
													   ai.invoice_amount,
													   ai.payment_status_flag,
													   ai.invoice_type_lookup_code) =
				   'APPROVED'
			   AND xap.source_table = 'AP_INVOICE_DISTRIBUTIONS'
			   AND xap.source_id = aid.invoice_distribution_id
			   AND ac.check_id = p_check_id
			   AND ai.org_id = p_org_id ----add by aeaum 23/09/2009------
			 GROUP BY ac.check_id,
					  ac.check_number,
					  hla.location_id,
					  ac.check_date,
					  atc.global_attribute16,
					  atc.name,
					  nvl(atc.global_attribute18, pv.global_attribute17),
					  nvl(atc.global_attribute17, pv.global_attribute18),
					  nvl(pvs.global_attribute18, ac.vendor_name),
					  nvl(pvs.global_attribute17, ac.address_line1),
					  decode(nvl(length(rtrim(ltrim(pvs.global_attribute17))),
								 0),
							 0,
							 ac.address_line2,
							 ''),
					  decode(nvl(length(rtrim(ltrim(pvs.global_attribute17))),
								 0),
							 0,
							 ac.address_line3,
							 ''),
					  nvl(pvs.global_attribute19, pv.vat_registration_num),
					  pvs.global_attribute20,
					  hla.attribute15,
					  hla.address_line_1,
					  hla.address_line_2,
					  hla.address_line_3 || ' ' || hla.region_2 || ' ' ||
					  hla.postal_code,
					  hla.attribute1,
					  hla.attribute14,
					  ac.doc_sequence_value,
					  ai.invoice_num,
					  ai.invoice_id,
					  aip.invoice_payment_id,
					  ai.org_id
			 ORDER BY ac.doc_sequence_value;
	
		rec_invoice cursor_invoice%ROWTYPE;
	
		-- Cursor for Load Data to Table : th_itemtax_distribution_loaded
		CURSOR cursor_invoice_dis IS
			SELECT DISTINCT ai.invoice_id,
							ac.check_id,
							aid.invoice_distribution_id,
							aip.invoice_payment_id,
							ai.org_id,
							user_id                     created_by --yvk20160607
						   ,
							SYSDATE                     creation_date --yvk20160607
			  FROM ap_checks                ac,
				   ap_invoice_payments      aip,
				   ap_invoices              ai,
				   ap_invoice_distributions aid,
				   ap_tax_codes             atc,
				   hr_locations             hla,
				   po_vendors               pv,
				   po_vendor_sites          pvs,
				   ap_ae_lines_all          xap
			 WHERE ac.check_id = aip.check_id
			   AND aip.invoice_id = ai.invoice_id
			   AND ai.invoice_id = aid.invoice_id
			   AND aid.line_type_lookup_code = 'AWT'
			   AND nvl(aid.awt_flag, 'A') = 'A'
			   AND aid.tax_code_id = atc.tax_id
			   AND atc.global_attribute19 = hla.location_id
			   AND ac.vendor_id = pv.vendor_id
			   AND ac.vendor_site_id = pvs.vendor_site_id
			   AND ((aip.invoice_payment_id, 'PAYWHT') NOT IN
				   (SELECT tidl.invoice_distribution_id, tidl.invoice_type
					   FROM th_itemtax_distribution_loaded tidl
					  WHERE tidl.invoice_type = 'PAYWHT'
						AND tidl.invoice_distribution_id =
							aip.invoice_payment_id))
			   AND ap_invoices_pkg.get_posting_status(ai.invoice_id) = 'Y'
			   AND ap_checks_pkg.get_posting_status(ac.check_id) = 'Y'
			   AND ap_invoices_pkg.get_approval_status(ai.invoice_id,
													   ai.invoice_amount,
													   ai.payment_status_flag,
													   ai.invoice_type_lookup_code) =
				   'APPROVED'
			   AND xap.source_table = 'AP_INVOICE_DISTRIBUTIONS'
			   AND xap.source_id = aid.invoice_distribution_id
			   AND ac.check_id = p_check_id
			   AND ai.org_id = p_org_id; ----add by aeaum 23/09/2009------
	
		rec_invoice_dis cursor_invoice_dis%ROWTYPE;
		error_whtno EXCEPTION;
	BEGIN
		v_insert := FALSE;
		user_id  := fnd_profile.value('USER_ID');
		OPEN cursor_head;
		LOOP
			FETCH cursor_head
				INTO rec_head;
			EXIT WHEN cursor_head%NOTFOUND;
			SELECT th_whth_s.nextval INTO v_head_id FROM dual;
			v_check_base := 0;
			v_check_tax  := 0;
			write_log('Check Number : ' || rec_head.doc_number);
			OPEN cursor_line;
			LOOP
				FETCH cursor_line
					INTO rec_line;
				EXIT WHEN cursor_line%NOTFOUND;
				v_line_base := 0;
				v_line_tax  := 0;
				IF nvl(rec_head.check_id, 0) = nvl(rec_line.check_id, 0) AND
				   nvl(rec_head.voucher_number, '0') =
				   nvl(rec_line.voucher_number, '0') AND
				   nvl(rec_head.pnd, '0') = nvl(rec_line.pnd, '0') AND
				   nvl(rec_head.location_id, 0) =
				   nvl(rec_line.location_id, 0) THEN
					SELECT th_whtl_s.nextval INTO v_line_id FROM dual;
					v_inv_base := 0;
					v_inv_tax  := 0;
					OPEN cursor_invoice;
					LOOP
						FETCH cursor_invoice
							INTO rec_invoice;
						EXIT WHEN cursor_invoice%NOTFOUND;
						IF nvl(rec_invoice.check_id, 0) =
						   nvl(rec_line.check_id, 0) AND
						   nvl(rec_invoice.voucher_number, '0') =
						   nvl(rec_line.voucher_number, '0') AND
						   nvl(rec_invoice.pnd, '0') =
						   nvl(rec_line.pnd, '0') AND
						   nvl(rec_invoice.location_id, 0) =
						   nvl(rec_line.location_id, 0) AND
						   nvl(rec_invoice.wht_code, '0') =
						   nvl(rec_line.wht_code, '0') THEN
							-- Calculate PAYMENT AMOUNT
							write_log('10 begin : Calculate PAYMENT AMOUNT');
							BEGIN
								SELECT nvl(SUM(decode(acct_line_type,
													  'AWT',
													  nvl(xapas.accounted_cr,
														  0) -
													  nvl(xapas.accounted_dr,
														  0),
													  nvl(xapas.accounted_dr,
														  0) -
													  nvl(xapas.accounted_cr,
														  0))),
										   0)
								  INTO v_payment_amt
								  FROM xla_ap_pay_ael_sl_v xapas
								 WHERE xapas.trx_hdr_id = rec_head.check_id
								   AND applied_to_trx_hdr_number_disp =
									   rec_invoice.invoice_num
								   AND applied_to_trx_hdr_number_disp IS NOT NULL
								   AND ((xapas.ael_id, 'PAYACTWHT') NOT IN
									   (SELECT tidl.invoice_distribution_id,
												tidl.invoice_type
										   FROM th_itemtax_distribution_loaded tidl
										  WHERE tidl.invoice_type =
												'PAYACTWHT'
											AND tidl.invoice_distribution_id =
												xapas.ael_id))
								   AND org_id = p_org_id; ----add by aeaum 23/09/2009------
							EXCEPTION
								WHEN no_data_found THEN
									v_payment_amt := 0;
							END;
							write_log('10 end : Calculate PAYMENT AMOUNT');
							-- Calculate INVOICE AMOUNT
							write_log('20 begin : Calculate INVOICE AMOUNT');
							BEGIN
							
								SELECT nvl(SUM(decode(sign(aip.amount),
													  -1,
													  (nvl(xap.accounted_dr,
														   0) -
													  nvl(xap.accounted_cr,
														   0)) * (-1),
													  (nvl(xap.accounted_dr,
														   0) -
													  nvl(xap.accounted_cr,
														   0)))),
										   0)
								  INTO v_invoice_amt
								  FROM ap_invoices              ai,
									   ap_invoice_distributions aid,
									   ap_invoice_payments      aip,
									   ap_checks                ac,
									   ap_ae_lines_all          xap
								 WHERE ai.invoice_id = aid.invoice_id
								   AND aid.line_type_lookup_code IN
									   ('ITEM', 'TAX')
								   AND ap_invoices_pkg.get_posting_status(ai.invoice_id) = 'Y'
								   AND ap_checks_pkg.get_posting_status(ac.check_id) = 'Y'
								   AND ap_invoices_pkg.get_approval_status(ai.invoice_id,
																		   ai.invoice_amount,
																		   ai.payment_status_flag,
																		   ai.invoice_type_lookup_code) =
									   'APPROVED'
								   AND ((aip.invoice_payment_id, 'PAYWHT') NOT IN
									   (SELECT tidl.invoice_distribution_id,
												tidl.invoice_type
										   FROM th_itemtax_distribution_loaded tidl
										  WHERE tidl.invoice_type = 'PAYWHT'
											AND tidl.invoice_distribution_id =
												aip.invoice_payment_id))
								   AND ai.invoice_id = aip.invoice_id
								   AND aip.check_id = ac.check_id
								   AND ac.check_id = rec_head.check_id
								   AND ai.invoice_id =
									   rec_invoice.invoice_id
								   AND xap.source_id =
									   aid.invoice_distribution_id
								   AND xap.source_table =
									   'AP_INVOICE_DISTRIBUTIONS'
								   AND ai.org_id = p_org_id; ----add by aeaum 23/09/2009------
							EXCEPTION
								WHEN no_data_found THEN
									v_invoice_amt := 0;
							END;
							write_log('20 end : Calculate INVOICE AMOUNT');
							-- Calculate BASE AMOUNT
							write_log('30 begin : Calculate BASE AMOUNT');
							BEGIN
								SELECT nvl(SUM(decode(sign(aip.amount),
													  -1,
													  (nvl(xap.accounted_dr,
														   0) -
													  nvl(xap.accounted_cr,
														   0)) * (-1),
													  (nvl(xap.accounted_dr,
														   0) -
													  nvl(xap.accounted_cr,
														   0)))),
										   0)
								  INTO v_base_amt
								  FROM ap_invoices              ai,
									   ap_invoice_distributions aid,
									   ap_awt_group_taxes_all   atc,
									   ap_invoice_payments      aip,
									   ap_checks                ac,
									   ap_bank_accounts         aba,
									   ap_ae_lines_all          xap
								 WHERE ai.invoice_id = aid.invoice_id
								   AND nvl(aid.awt_group_id, 0) =
									   atc.group_id
								   AND aid.line_type_lookup_code IN
									   ('ITEM')
								   AND ap_invoices_pkg.get_posting_status(ai.invoice_id) = 'Y'
								   AND ap_checks_pkg.get_posting_status(ac.check_id) = 'Y'
								   AND ap_invoices_pkg.get_approval_status(ai.invoice_id,
																		   ai.invoice_amount,
																		   ai.payment_status_flag,
																		   ai.invoice_type_lookup_code) =
									   'APPROVED'
								   AND ((aip.invoice_payment_id, 'PAYWHT') NOT IN
									   (SELECT tidl.invoice_distribution_id,
												tidl.invoice_type
										   FROM th_itemtax_distribution_loaded tidl
										  WHERE tidl.invoice_type = 'PAYWHT'
											AND tidl.invoice_distribution_id =
												aip.invoice_payment_id))
								   AND ai.invoice_id = aip.invoice_id
								   AND aip.check_id = ac.check_id
								   AND ac.bank_account_id =
									   aba.bank_account_id
								   AND xap.source_id =
									   aid.invoice_distribution_id
								   AND xap.source_table =
									   'AP_INVOICE_DISTRIBUTIONS'
								   AND ac.check_id = rec_head.check_id
								   AND atc.tax_name = rec_invoice.wht_code
								   AND ai.invoice_id =
									   rec_invoice.invoice_id
								   AND ai.org_id = p_org_id; ----add by aeaum 23/09/2009------
							EXCEPTION
								WHEN no_data_found THEN
									v_base_amt := 0;
							END;
							write_log('30 end : Calculate BASE AMOUNT');
							-- Calculate for Partial Payment
							write_log('40 begin : Calculate for Partial Payment');
							IF v_invoice_amt <> 0 THEN
								v_paid_percent := v_payment_amt /
												  v_invoice_amt;
							ELSE
								v_paid_percent := 0;
							END IF;
							v_inv_base  := round(v_base_amt *
												 v_paid_percent,
												 2);
							v_inv_tax   := round(round(v_base_amt *
													   v_paid_percent,
													   2) *
												 (rec_line.tax_rate / 100),
												 2);
							v_line_base := v_line_base + v_inv_base;
							v_line_tax  := v_line_tax + v_inv_tax;
							write_log('40 end : Calculate for Partial Payment');
						END IF;
					END LOOP;
					CLOSE cursor_invoice;
					-- Insert Data to table : th_wht_line
					write_log('50 begin : Insert Data to table : th_wht_line');
					INSERT INTO th_wht_line
						(th_whtl_id,
						 th_whth_id,
						 th_whtl_wt_code,
						 th_whtl_revenue_type,
						 th_whtl_revenue_name,
						 th_whtl_base_amt,
						 th_whtl_tax_amt,
						 th_whtl_supplier_name,
						 th_whtl_supplier_address1,
						 th_whtl_supplier_address2,
						 th_whtl_supplier_address3,
						 th_whtl_supplier_tax_id,
						 th_whtl_supplier_branch_no,
						 th_whtl_agent_name,
						 th_whtl_agent_address1,
						 th_whtl_agent_address2,
						 th_whtl_agent_address3,
						 th_whtl_agent_tax_id,
						 th_whtl_agent_branch_no,
						 th_whtl_resp_name,
						 th_whtl_resp_address1,
						 th_whtl_resp_address2,
						 th_whtl_resp_address3,
						 th_whtl_resp_tax_id,
						 th_whtl_resp_branch_no,
						 attribute1,
						 created_by,
						 creation_date,
						 last_updated_by,
						 last_update_date,
						 last_update_login,
						 org_id)
					VALUES
						(v_line_id,
						 v_head_id,
						 rec_line.wht_code,
						 rec_line.revenue_type,
						 rec_line.revenue_name,
						 v_line_base,
						 v_line_tax * (-1),
						 rec_line.supplier_name,
						 rec_line.supplier_address1,
						 rec_line.supplier_address2,
						 rec_line.supplier_address3,
						 rec_line.supplier_tax_id,
						 rec_line.supplier_branch_no,
						 rec_line.agent_name,
						 rec_line.agent_address,
						 NULL,
						 NULL,
						 rec_line.agent_tax_id,
						 rec_line.agent_branch_no,
						 rec_line.resp_name,
						 rec_line.resp_address1,
						 rec_line.resp_address2,
						 rec_line.resp_address3,
						 rec_line.resp_tax_id,
						 rec_line.resp_branch_no,
						 p_conc_request_id,
						 rec_line.created_by,
						 rec_line.creation_date,
						 rec_line.last_updated_by,
						 rec_line.last_update_date,
						 rec_line.last_update_login,
						 rec_line.org_id);
					write_log('50 end : Insert Data to table : th_wht_line');
					v_check_base := v_check_base + v_line_base;
					v_check_tax  := v_check_tax + v_line_tax;
				END IF;
			END LOOP;
			CLOSE cursor_line;
			-- Find Period Name from CHECK DATE
			write_log('60 begin : Find Period Name from CHECK DATE');
			SELECT period_name
			  INTO v_nperiod
			  FROM gl_periods_v
			 WHERE period_set_name = 'Accounting' --'TCORE_Calendar'
			   AND (trunc(rec_head.doc_date) BETWEEN start_date AND
				   end_date)
			   AND adjustment_period_flag = 'N'
			 ORDER BY start_date;
			write_log('60 end : Find Period Name from CHECK DATE');
			-- Create WHT Number
			write_log('70 begin : Create WHT Number');
			v_period := substr(rec_head.period_name, 1, 3) ||
						substr(rec_head.period_name, 7, 2);
			v_query  :=    -- 'select th_wht_'   -- V1.0
			 'select th_wht_a_' -- V2.0
						|| v_period || '_' || rec_head.pnd ||
						'_s.nextval from dual';
			BEGIN
				cursor1 := dbms_sql.open_cursor;
				dbms_sql.parse(cursor1, v_query, 2);
				dbms_sql.define_column(cursor1, 1, v_wht_num);
				rows_processed := dbms_sql.execute(cursor1);
				IF dbms_sql.fetch_rows(cursor1) > 0 THEN
					dbms_sql.column_value(cursor1, 1, v_wht_num);
				ELSE
					EXIT;
				END IF;
				dbms_sql.close_cursor(cursor1);
			EXCEPTION
				WHEN OTHERS THEN
					write_log('70 : Can not create WHT Number , ' ||
							  rec_head.period_name || ' , ' ||
							  rec_head.pnd);
					ROLLBACK;
					RAISE error_whtno;
					IF dbms_sql.is_open(cursor1) THEN
						dbms_sql.close_cursor(cursor1);
					END IF;
					EXIT;
			END;
			write_log('70 end : Create WHT Number');
			v_insert := TRUE;
			-- Insert Data to table : th_wht_header
			write_log('80 begin : Insert Data to table : th_wht_header');
			INSERT INTO th_wht_header
				(th_whth_id,
				 th_whth_doc_id,
				 th_whth_doc_number,
				 th_whth_doc_date,
				 th_whth_type,
				 th_whth_related_document,
				 th_whth_location_id,
				 th_whth_base_amt,
				 th_whth_tax_amt,
				 th_whth_voucher_number,
				 th_whth_period_name,
				 th_whth_wht_number,
				 th_phor_ngor_dor,
				 attribute1,
				 created_by,
				 creation_date,
				 last_updated_by,
				 last_update_date,
				 last_update_login,
				 th_act_transaction_date,
				 org_id -- V2.0
				 )
			VALUES
				(v_head_id,
				 rec_head.check_id,
				 rec_head.doc_number,
				 rec_head.doc_date,
				 rec_head.doc_type,
				 rec_head.doc_relate,
				 rec_head.location_id,
				 v_check_base,
				 v_check_tax * (-1),
				 rec_head.voucher_number,
				 v_nperiod,
				 v_wht_num,
				 rec_head.pnd,
				 p_conc_request_id,
				 rec_head.created_by,
				 rec_head.creation_date,
				 rec_head.last_updated_by,
				 rec_head.last_update_date,
				 rec_head.last_update_login,
				 rec_head.accounting_date,
				 rec_head.org_id -- V2.0
				 );
			write_log('80 end : Insert Data to table : th_wht_header');
		END LOOP;
		CLOSE cursor_head;
		-- Insert Data to table : th_itemtax_distribution_loaded
		-- For Check Data Loaded
		IF v_insert THEN
			write_log('90 begin : Insert Data to table : th_itemtax_distribution_loaded');
			OPEN cursor_head;
			LOOP
				FETCH cursor_head
					INTO rec_head;
				EXIT WHEN cursor_head%NOTFOUND;
				OPEN cursor_invoice;
				LOOP
					FETCH cursor_invoice
						INTO rec_invoice;
					EXIT WHEN cursor_invoice%NOTFOUND;
					BEGIN
						INSERT INTO th_itemtax_distribution_loaded
							(invoice_distribution_id,
							 invoice_type,
							 org_id,
							 created_by,
							 creation_date)
							SELECT ael_id,
								   'PAYACTWHT',
								   org_id,
								   user_id    created_by --yvk20160607
								  ,
								   SYSDATE    creation_date --yvk20160607
							  FROM xla_ap_pay_ael_sl_v xapas
							 WHERE xapas.trx_hdr_id = rec_head.check_id
							   AND applied_to_trx_hdr_number_disp =
								   rec_invoice.invoice_num
							   AND applied_to_trx_hdr_number_disp IS NOT NULL
							   AND ((xapas.ael_id, 'PAYACTWHT') NOT IN
								   (SELECT tidl.invoice_distribution_id,
											tidl.invoice_type
									   FROM th_itemtax_distribution_loaded tidl
									  WHERE tidl.invoice_type = 'PAYACTWHT'
										AND tidl.invoice_distribution_id =
											xapas.ael_id))
							   AND org_id = p_org_id; ----add by aeaum 23/09/2009------
					EXCEPTION
						WHEN OTHERS THEN
							NULL;
					END;
				END LOOP;
				CLOSE cursor_invoice;
			END LOOP;
			CLOSE cursor_head;
			OPEN cursor_invoice_dis;
			LOOP
				FETCH cursor_invoice_dis
					INTO rec_invoice_dis;
				EXIT WHEN cursor_invoice_dis%NOTFOUND;
				INSERT INTO th_itemtax_distribution_loaded
					(invoice_distribution_id,
					 invoice_type,
					 org_id,
					 created_by,
					 creation_date)
				VALUES
					(rec_invoice_dis.invoice_distribution_id,
					 'WHT',
					 rec_invoice_dis.org_id,
					 rec_invoice_dis.created_by,
					 rec_invoice_dis.creation_date);
				INSERT INTO th_itemtax_distribution_loaded
					(invoice_distribution_id,
					 invoice_type,
					 org_id,
					 created_by,
					 creation_date)
				VALUES
					(rec_invoice_dis.invoice_payment_id,
					 'PAYWHT',
					 rec_invoice_dis.org_id,
					 rec_invoice_dis.created_by,
					 rec_invoice_dis.creation_date);
			END LOOP;
			CLOSE cursor_invoice_dis;
			write_log('90 end : Insert Data to table : th_itemtax_distribution_loaded');
		END IF;
		COMMIT;
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
	END th_load_wht_a;
	/*================================================================
      PROCEDURE NAME:   th_load_wht_m()
    ==================================================================*/
	PROCEDURE th_load_wht_m(p_conc_request_id IN NUMBER,
							p_invoice_id      IN NUMBER,
							p_org_id          IN NUMBER) IS
		user_id                       NUMBER;
		v_head_id                     NUMBER;
		v_line_id                     NUMBER;
		v_base_amt                    NUMBER;
		v_non_rec_tax                 NUMBER;
		v_rec_tax                     NUMBER;
		v_non_rec_amt                 NUMBER;
		v_pi_result                   NUMBER(13, 10);
		v_rec_amt                     NUMBER;
		v_tax_amt                     NUMBER;
		v_payment_amt                 NUMBER;
		v_invoice_amt                 NUMBER;
		v_project_id                  NUMBER;
		v_bproject                    BOOLEAN;
		v_insert                      BOOLEAN;
		v_paid_percent                NUMBER;
		v_paid_base_inv               NUMBER;
		v_paid_rec_tax                NUMBER;
		v_paid_nonrec_tax             NUMBER;
		v_check_base_inv              NUMBER;
		v_check_rec_tax               NUMBER;
		v_check_nonrec_tax            NUMBER;
		v_period                      VARCHAR2(10);
		v_seq_name                    VARCHAR2(50);
		v_query                       VARCHAR2(800);
		cursor1                       INTEGER;
		v_wht_num                     NUMBER;
		rows_processed                INTEGER;
		v_check_base                  NUMBER;
		v_line_base                   NUMBER;
		v_inv_base                    NUMBER;
		v_inv_tax                     NUMBER;
		v_line_tax                    NUMBER;
		v_check_tax                   NUMBER;
		v_pay_act                     NUMBER;
		v_base_amt_line               NUMBER;
		v_tax_amt_line                NUMBER;
		v_nperiod                     VARCHAR2(10);
		v_ai_invoice_id               NUMBER(15);
		v_ai_invoice_amount           NUMBER;
		v_ai_payment_status_flag      VARCHAR2(1 BYTE);
		v_ai_invoice_type_lookup_code VARCHAR2(25 BYTE);
		v_invoice_date                DATE;
		-- Cursor for Load Data to Table : th_wht_header
		CURSOR my_cursor_manual_head IS
			SELECT ai.invoice_id,
				   ai.invoice_num doc_number,
				   trunc(aid.accounting_date) doc_date,
				   'MANUAL' doc_type,
				   'Invoice' doc_relate,
				   hla.location_id location_id,
				   decode(ai.doc_sequence_id,
						  NULL,
						  ai.voucher_num,
						  ai.doc_sequence_value) voucher,
				   trunc(aid.accounting_date) gl_date,
				   to_char(aid.accounting_date, 'MON-YYYY') period_name,
				   atc.global_attribute16 pnd,
				   atc.name tax_code,
				   user_id created_by,
				   SYSDATE creation_date,
				   user_id last_updated_by,
				   SYSDATE last_update_date,
				   user_id last_update_login,
				   trunc(ai.invoice_date) invoice_date,
				   ai.org_id -- V2.0
			  FROM ap_invoice_distributions aid,
				   ap_invoices              ai,
				   ap_tax_codes             atc,
				   po_vendors               pv,
				   hr_locations             hla,
				   po_vendor_sites          pvs,
				   ap_ae_lines_all          xap
			 WHERE aid.invoice_id = ai.invoice_id
			   AND aid.line_type_lookup_code = 'AWT'
			   AND aid.awt_flag = 'M'
			   AND aid.tax_code_id = atc.tax_id
			   AND atc.global_attribute19 = hla.location_id
			   AND ai.vendor_id = pv.vendor_id
			   AND ai.vendor_site_id = pvs.vendor_site_id
			   AND ap_invoices_pkg.get_posting_status(ai.invoice_id) = 'Y'
			   AND ap_invoices_pkg.get_approval_status(ai.invoice_id,
													   ai.invoice_amount,
													   ai.payment_status_flag,
													   ai.invoice_type_lookup_code) IN
				   ('APPROVED', 'CANCELLED')
			   AND xap.source_table = 'AP_INVOICE_DISTRIBUTIONS'
			   AND xap.source_id = aid.invoice_distribution_id
			   AND (aid.invoice_distribution_id, 'WHT') NOT IN
				   (SELECT tidl.invoice_distribution_id, tidl.invoice_type
					  FROM th_itemtax_distribution_loaded tidl
					 WHERE tidl.invoice_type = 'WHT'
					   AND tidl.invoice_distribution_id =
						   aid.invoice_distribution_id)
			   AND ai.invoice_id = p_invoice_id
			   AND ai.org_id = p_org_id ----add by aeaum 23/09/2009------
			 GROUP BY ai.invoice_id,
					  ai.invoice_num,
					  trunc(ai.invoice_date), -- V2.0
					  to_char(aid.accounting_date, 'MON-YYYY'),
					  'MANUAL',
					  'Invoice',
					  hla.location_id,
					  decode(ai.doc_sequence_id,
							 NULL,
							 ai.voucher_num,
							 ai.doc_sequence_value),
					  trunc(aid.accounting_date),
					  atc.global_attribute16,
					  atc.name,
					  ai.org_id
			 ORDER BY decode(ai.doc_sequence_id,
							 NULL,
							 ai.voucher_num,
							 ai.doc_sequence_value);
	
		my_rec_manual_head my_cursor_manual_head%ROWTYPE;
	
		-- Cursor for Load Data to Table : th_wht_line
		CURSOR my_cursor_manual IS
			SELECT ai.invoice_id,
				   ai.invoice_num doc_number,
				   trunc(aid.accounting_date) doc_date,
				   'MANUAL' doc_type,
				   'Invoice' doc_relate,
				   hla.location_id location_id,
				   decode(ai.doc_sequence_id,
						  NULL,
						  ai.voucher_num,
						  ai.doc_sequence_value) voucher,
				   trunc(aid.accounting_date) gl_date,
				   to_char(aid.accounting_date, 'MON-YYYY') period_name,
				   atc.global_attribute16 pnd,
				   atc.name wht_code,
				   nvl(atc.global_attribute18, pv.global_attribute17) revenue_type,
				   nvl(atc.global_attribute17, pv.global_attribute18) revenue_name,
				   pv.vendor_name supplier_name,
				   pvs.address_line1 supplier_address1,
				   pvs.address_line2 supplier_address2,
				   pvs.address_line3 || ' ' || pvs.address_lines_alt || ' ' ||
				   pvs.city || ' ' || pvs.zip supplier_address3,
				   pv.vat_registration_num supplier_tax_id,
				   pvs.global_attribute20 supplier_branch_no,
				   '' agent_name,
				   '' agent_address,
				   '' agent_tax_id,
				   '' agent_branch_no,
				   hla.attribute15 resp_name,
				   hla.address_line_1 resp_address1,
				   hla.address_line_2 resp_address2,
				   hla.address_line_3 || ' ' || hla.region_2 || ' ' ||
				   hla.postal_code resp_address3,
				   hla.attribute1 resp_tax_id,
				   hla.attribute14 resp_branch_no,
				   user_id created_by,
				   SYSDATE creation_date,
				   user_id last_updated_by,
				   SYSDATE last_update_date,
				   user_id last_update_login,
				   ai.org_id
			  FROM ap_invoice_distributions aid,
				   ap_invoices              ai,
				   ap_tax_codes             atc,
				   po_vendors               pv,
				   hr_locations             hla,
				   po_vendor_sites          pvs,
				   ap_ae_lines_all          xap
			 WHERE aid.invoice_id = ai.invoice_id
			   AND aid.line_type_lookup_code = 'AWT'
			   AND aid.awt_flag = 'M'
			   AND aid.tax_code_id = atc.tax_id
			   AND atc.global_attribute19 = hla.location_id
			   AND ai.vendor_id = pv.vendor_id
			   AND ai.vendor_site_id = pvs.vendor_site_id
			   AND ap_invoices_pkg.get_posting_status(ai.invoice_id) = 'Y'
			   AND ap_invoices_pkg.get_approval_status(ai.invoice_id,
													   ai.invoice_amount,
													   ai.payment_status_flag,
													   ai.invoice_type_lookup_code) IN
				   ('APPROVED', 'CANCELLED')
			   AND xap.source_table = 'AP_INVOICE_DISTRIBUTIONS'
			   AND xap.source_id = aid.invoice_distribution_id
			   AND (aid.invoice_distribution_id, 'WHT') NOT IN
				   (SELECT tidl.invoice_distribution_id, tidl.invoice_type
					  FROM th_itemtax_distribution_loaded tidl
					 WHERE tidl.invoice_type = 'WHT'
					   AND tidl.invoice_distribution_id =
						   aid.invoice_distribution_id)
			   AND ai.invoice_id = p_invoice_id
			   AND ai.org_id = p_org_id ----add by aeaum 23/09/2009------
			 GROUP BY ai.invoice_id,
					  ai.invoice_num,
					  trunc(aid.accounting_date),
					  'MANUAL',
					  'Invoice',
					  hla.location_id,
					  decode(ai.doc_sequence_id,
							 NULL,
							 ai.voucher_num,
							 ai.doc_sequence_value),
					  to_char(aid.accounting_date, 'MON-YYYY'),
					  atc.global_attribute16,
					  atc.name,
					  nvl(atc.global_attribute18, pv.global_attribute17),
					  nvl(atc.global_attribute17, pv.global_attribute18),
					  pv.vendor_name,
					  pvs.address_line1,
					  pvs.address_line2,
					  pvs.address_line3 || ' ' || pvs.address_lines_alt || ' ' ||
					  pvs.city || ' ' || pvs.zip,
					  pv.vat_registration_num,
					  pvs.global_attribute20,
					  hla.attribute15,
					  hla.address_line_1,
					  hla.address_line_2,
					  hla.address_line_3 || ' ' || hla.region_2 || ' ' ||
					  hla.postal_code,
					  hla.attribute1,
					  hla.attribute14,
					  ai.org_id
			 ORDER BY decode(ai.doc_sequence_id,
							 NULL,
							 ai.voucher_num,
							 ai.doc_sequence_value);
	
		my_rec_manual my_cursor_manual%ROWTYPE;
		-- Cursor for Load Data to Table : th_itemtax_distribution_loaded
		CURSOR my_cursor_manual_dis IS
			SELECT ai.invoice_id,
				   aid.invoice_distribution_id,
				   ai.org_id,
				   user_id                     created_by --yvk20160607
				  ,
				   SYSDATE                     creation_date --yvk20160607
			  FROM ap_invoice_distributions aid,
				   ap_invoices              ai,
				   ap_tax_codes             atc,
				   po_vendors               pv,
				   hr_locations             hla,
				   po_vendor_sites          pvs,
				   ap_ae_lines_all          xap
			 WHERE aid.invoice_id = ai.invoice_id
			   AND aid.line_type_lookup_code IN ('AWT', 'ITEM')
			   AND ai.invoice_id IN
				   (SELECT ind.invoice_id
					  FROM ap_invoice_distributions ind
					 WHERE ind.invoice_id = ai.invoice_id
					   AND ind.line_type_lookup_code = 'AWT'
					   AND ind.awt_flag = 'M')
			   AND aid.awt_flag = 'M'
			   AND aid.tax_code_id = atc.tax_id
			   AND atc.global_attribute19 = hla.location_id
			   AND ai.vendor_id = pv.vendor_id
			   AND ai.vendor_site_id = pvs.vendor_site_id
			   AND ap_invoices_pkg.get_posting_status(ai.invoice_id) = 'Y'
			   AND ap_invoices_pkg.get_approval_status(ai.invoice_id,
													   ai.invoice_amount,
													   ai.payment_status_flag,
													   ai.invoice_type_lookup_code) IN
				   ('APPROVED', 'CANCELLED')
			   AND xap.source_table = 'AP_INVOICE_DISTRIBUTIONS'
			   AND xap.source_id = aid.invoice_distribution_id
			   AND (aid.invoice_distribution_id, 'WHT') NOT IN
				   (SELECT tidl.invoice_distribution_id, tidl.invoice_type
					  FROM th_itemtax_distribution_loaded tidl
					 WHERE tidl.invoice_type = 'WHT'
					   AND tidl.invoice_distribution_id =
						   aid.invoice_distribution_id)
			   AND ai.invoice_id = p_invoice_id
			   AND ai.org_id = p_org_id ----add by aeaum 23/09/2009------
			 GROUP BY ai.invoice_id, aid.invoice_distribution_id, ai.org_id
			UNION ALL
			SELECT ai.invoice_id,
				   aid.invoice_distribution_id,
				   ai.org_id,
				   user_id                     created_by --yvk20160607
				  ,
				   SYSDATE                     creation_date --yvk20160607
			  FROM ap_invoice_distributions aid,
				   ap_invoices              ai,
				   ap_tax_codes             atc,
				   po_vendors               pv,
				   hr_locations             hla,
				   po_vendor_sites          pvs,
				   ap_ae_lines_all          xap,
				   ap_awt_groups            aag
			 WHERE aid.invoice_id = ai.invoice_id
			   AND aid.line_type_lookup_code = 'ITEM'
			   AND ai.invoice_id IN
				   (SELECT ind.invoice_id
					  FROM ap_invoice_distributions ind
					 WHERE ind.invoice_id = ai.invoice_id
					   AND ind.line_type_lookup_code = 'AWT'
					   AND ind.awt_flag = 'M')
			   AND aid.tax_code_id = atc.tax_id(+)
			   AND aid.awt_group_id = aag.group_id
			   AND atc.global_attribute19 = hla.location_id(+)
			   AND ai.vendor_id = pv.vendor_id
			   AND ai.vendor_site_id = pvs.vendor_site_id
			   AND ap_invoices_pkg.get_posting_status(ai.invoice_id) = 'Y'
			   AND ap_invoices_pkg.get_approval_status(ai.invoice_id,
													   ai.invoice_amount,
													   ai.payment_status_flag,
													   ai.invoice_type_lookup_code) IN
				   ('APPROVED', 'CANCELLED')
			   AND xap.source_table = 'AP_INVOICE_DISTRIBUTIONS'
			   AND xap.source_id = aid.invoice_distribution_id
			   AND ai.invoice_id = p_invoice_id
			   AND (aid.invoice_distribution_id, 'WHT') NOT IN
				   (SELECT tidl.invoice_distribution_id, tidl.invoice_type
					  FROM th_itemtax_distribution_loaded tidl
					 WHERE tidl.invoice_type = 'WHT'
					   AND tidl.invoice_distribution_id =
						   aid.invoice_distribution_id)
			   AND ai.org_id = p_org_id; ----add by aeaum 23/09/2009------
	
		my_rec_manual_dis my_cursor_manual_dis%ROWTYPE;
		error_whtno EXCEPTION;
	BEGIN
		v_insert := FALSE;
		user_id  := fnd_profile.value('USER_ID');
		OPEN my_cursor_manual_head;
		LOOP
			FETCH my_cursor_manual_head
				INTO my_rec_manual_head;
			EXIT WHEN my_cursor_manual_head%NOTFOUND;
			SELECT th_whth_s.nextval INTO v_head_id FROM dual;
			write_log('Invoice Number : ' || my_rec_manual_head.doc_number);
			-- Calculate BASE AMOUNT
			write_log('10 begin : Calculate BASE AMOUNT');
			BEGIN
				SELECT nvl(SUM(nvl(xap.accounted_dr, 0) -
							   nvl(xap.accounted_cr, 0)),
						   0)
				  INTO v_base_amt
				  FROM ap_invoice_distributions aid,
					   ap_invoices              ai,
					   ap_tax_codes             atc,
					   po_vendors               pv,
					   hr_locations             hla,
					   po_vendor_sites          pvs,
					   ap_ae_lines_all          xap,
					   ap_awt_groups            aag
				 WHERE aid.invoice_id = ai.invoice_id
				   AND aid.line_type_lookup_code = 'ITEM'
				   AND aid.tax_code_id = atc.tax_id(+)
				   AND aid.awt_group_id = aag.group_id
				   AND atc.global_attribute19 = hla.location_id(+)
				   AND ai.vendor_id = pv.vendor_id
				   AND ai.vendor_site_id = pvs.vendor_site_id
				   AND ap_invoices_pkg.get_posting_status(ai.invoice_id) = 'Y'
				   AND ap_invoices_pkg.get_approval_status(ai.invoice_id,
														   ai.invoice_amount,
														   ai.payment_status_flag,
														   ai.invoice_type_lookup_code) IN
					   ('APPROVED', 'CANCELLED')
				   AND xap.source_table = 'AP_INVOICE_DISTRIBUTIONS'
				   AND xap.source_id = aid.invoice_distribution_id
				   AND (aid.invoice_distribution_id, 'WHT') NOT IN
					   (SELECT tidl.invoice_distribution_id,
							   tidl.invoice_type
						  FROM th_itemtax_distribution_loaded tidl
						 WHERE tidl.invoice_type = 'WHT'
						   AND tidl.invoice_distribution_id =
							   aid.invoice_distribution_id)
				   AND aag.name = my_rec_manual_head.tax_code
				   AND trunc(aid.accounting_date) =
					   trunc(my_rec_manual_head.doc_date)
				   AND ai.invoice_id = my_rec_manual_head.invoice_id
				   AND ai.org_id = p_org_id; ----add by aeaum 23/09/2009------
			EXCEPTION
				WHEN no_data_found THEN
					v_base_amt := 0;
			END;
			write_log('10 end : Calculate BASE AMOUNT');
			-- Calculate TAX AMOUNT
			write_log('20 begin : Calculate TAX AMOUNT');
			BEGIN
				SELECT nvl(SUM(nvl(xap.accounted_dr, 0) -
							   nvl(xap.accounted_cr, 0)),
						   0)
				  INTO v_tax_amt
				  FROM ap_invoice_distributions aid,
					   ap_invoices              ai,
					   ap_tax_codes             atc,
					   po_vendors               pv,
					   hr_locations             hla,
					   po_vendor_sites          pvs,
					   ap_ae_lines_all          xap
				 WHERE aid.invoice_id = ai.invoice_id
				   AND aid.line_type_lookup_code = 'AWT'
				   AND aid.awt_flag = 'M'
				   AND aid.tax_code_id = atc.tax_id
				   AND atc.global_attribute19 = hla.location_id
				   AND ai.vendor_id = pv.vendor_id
				   AND ai.vendor_site_id = pvs.vendor_site_id
				   AND ap_invoices_pkg.get_posting_status(ai.invoice_id) = 'Y'
				   AND ap_invoices_pkg.get_approval_status(ai.invoice_id,
														   ai.invoice_amount,
														   ai.payment_status_flag,
														   ai.invoice_type_lookup_code) IN
					   ('APPROVED', 'CANCELLED')
				   AND xap.source_table = 'AP_INVOICE_DISTRIBUTIONS'
				   AND xap.source_id = aid.invoice_distribution_id
				   AND (aid.invoice_distribution_id, 'WHT') NOT IN
					   (SELECT tidl.invoice_distribution_id,
							   tidl.invoice_type
						  FROM th_itemtax_distribution_loaded tidl
						 WHERE tidl.invoice_type = 'WHT'
						   AND tidl.invoice_distribution_id =
							   aid.invoice_distribution_id)
				   AND atc.global_attribute16 = my_rec_manual_head.pnd
				   AND trunc(aid.accounting_date) =
					   trunc(my_rec_manual_head.doc_date)
				   AND ai.invoice_id = my_rec_manual_head.invoice_id
				   AND atc.name = my_rec_manual_head.tax_code
				   AND ai.org_id = p_org_id; ----add by aeaum 23/09/2009------
			EXCEPTION
				WHEN no_data_found THEN
					v_tax_amt := 0;
			END;
			write_log('20 end : Calculate TAX AMOUNT');
			-- Find Period Name from GL DATE
			write_log('30 begin : Find Period Name from GL DATE');
			SELECT period_name
			  INTO v_nperiod
			  FROM gl_periods_v
			 WHERE period_set_name = 'Accounting' --'TCORE_Calendar'
			   AND (trunc(my_rec_manual_head.gl_date) BETWEEN start_date AND
				   end_date)
			   AND adjustment_period_flag = 'N'
			 ORDER BY start_date;
			write_log('30 end : Find Period Name from GL DATE');
		
			-----Edit by aeaum 23/11/2009------
			-- Create WHT Number
			--*** V2.0 Disable for Manual
			write_log('40 begin : Create WHT Number');
			v_period := substr(my_rec_manual_head.period_name, 1, 3) ||
						substr(my_rec_manual_head.period_name, 7, 2);
			v_query  :=    -- 'select th_wht_'   -- V1.0
			-----Edit by aeaum 23/11/2009------
			--'select th_wht_m_'      -- V2.0
			 'select th_wht_a_'
					   ----------------------------------
						|| v_period || '_' || my_rec_manual_head.pnd ||
						'_s.nextval from dual';
			BEGIN
				cursor1 := dbms_sql.open_cursor;
				dbms_sql.parse(cursor1, v_query, 2);
				dbms_sql.define_column(cursor1, 1, v_wht_num);
				rows_processed := dbms_sql.execute(cursor1);
				IF dbms_sql.fetch_rows(cursor1) > 0 THEN
					dbms_sql.column_value(cursor1, 1, v_wht_num);
				ELSE
					EXIT;
				END IF;
				dbms_sql.close_cursor(cursor1);
			EXCEPTION
				WHEN OTHERS THEN
					write_log('40 : Can not create WHT Number , ' ||
							  my_rec_manual_head.period_name || ' , ' ||
							  my_rec_manual_head.pnd);
					RAISE error_whtno;
					ROLLBACK;
					IF dbms_sql.is_open(cursor1) THEN
						dbms_sql.close_cursor(cursor1);
					END IF;
					EXIT;
			END;
			write_log('40 end : Create WHT Number');
			---------------------------------------------------------------
			v_insert := TRUE;
			-- Insert Data to table : th_wht_header
			write_log('50 begin : Insert Data to table : th_wht_header');
			INSERT INTO th_wht_header
				(th_whth_id,
				 th_whth_doc_id,
				 th_whth_doc_number,
				 th_whth_doc_date,
				 th_whth_type,
				 th_whth_related_document,
				 th_whth_location_id,
				 th_whth_base_amt,
				 th_whth_tax_amt,
				 th_whth_voucher_number,
				 th_whth_period_name,
				 ---add by aeaum 23/11/2009---
				 th_whth_wht_number, -- V2.0
				 -----------------------------
				 th_phor_ngor_dor,
				 attribute1,
				 created_by,
				 creation_date,
				 last_updated_by,
				 last_update_date,
				 last_update_login,
				 th_act_transaction_date,
				 org_id -- V2.0
				 )
			VALUES
				(v_head_id,
				 my_rec_manual_head.invoice_id,
				 my_rec_manual_head.doc_number,
				 my_rec_manual_head.doc_date,
				 my_rec_manual_head.doc_type,
				 my_rec_manual_head.doc_relate,
				 my_rec_manual_head.location_id,
				 v_base_amt,
				 v_tax_amt,
				 my_rec_manual_head.voucher,
				 v_nperiod,
				 ---add by aeaum 23/11/2009---
				 v_wht_num, -- V2.0
				 -----------------------------
				 my_rec_manual_head.pnd,
				 p_conc_request_id,
				 my_rec_manual_head.created_by,
				 my_rec_manual_head.creation_date,
				 my_rec_manual_head.last_updated_by,
				 my_rec_manual_head.last_update_date,
				 my_rec_manual_head.last_update_login,
				 my_rec_manual_head.invoice_date,
				 my_rec_manual_head.org_id -- V2.0
				 );
			write_log('50 end : Insert Data to table : th_wht_header');
			OPEN my_cursor_manual;
			LOOP
				FETCH my_cursor_manual
					INTO my_rec_manual;
				EXIT WHEN my_cursor_manual%NOTFOUND;
				IF my_rec_manual_head.invoice_id = my_rec_manual.invoice_id AND
				   my_rec_manual_head.tax_code = my_rec_manual.wht_code AND
				   my_rec_manual_head.doc_date = my_rec_manual.doc_date THEN
					SELECT th_whtl_s.nextval INTO v_line_id FROM dual;
					-- Calculate BASE AMOUNT LINE
					write_log('60 begin : Calculate BASE AMOUNT LINE');
					BEGIN
						SELECT nvl(SUM(nvl(xap.accounted_dr, 0) -
									   nvl(xap.accounted_cr, 0)),
								   0)
						  INTO v_base_amt_line
						  FROM ap_invoice_distributions aid,
							   ap_invoices              ai,
							   ap_tax_codes             atc,
							   po_vendors               pv,
							   hr_locations             hla,
							   po_vendor_sites          pvs,
							   ap_ae_lines_all          xap,
							   ap_awt_groups            aag
						 WHERE aid.invoice_id = ai.invoice_id
						   AND aid.line_type_lookup_code = 'ITEM'
						   AND aid.tax_code_id = atc.tax_id(+)
						   AND aid.awt_group_id = aag.group_id
						   AND atc.global_attribute19 = hla.location_id(+)
						   AND ai.vendor_id = pv.vendor_id
						   AND ai.vendor_site_id = pvs.vendor_site_id
						   AND ap_invoices_pkg.get_posting_status(ai.invoice_id) = 'Y'
						   AND ap_invoices_pkg.get_approval_status(ai.invoice_id,
																   ai.invoice_amount,
																   ai.payment_status_flag,
																   ai.invoice_type_lookup_code) IN
							   ('APPROVED', 'CANCELLED')
						   AND xap.source_table =
							   'AP_INVOICE_DISTRIBUTIONS'
						   AND xap.source_id = aid.invoice_distribution_id
						   AND (aid.invoice_distribution_id, 'WHT') NOT IN
							   (SELECT tidl.invoice_distribution_id,
									   tidl.invoice_type
								  FROM th_itemtax_distribution_loaded tidl
								 WHERE tidl.invoice_type = 'WHT'
								   AND tidl.invoice_distribution_id =
									   aid.invoice_distribution_id)
						   AND ai.invoice_id = my_rec_manual.invoice_id
						   AND trunc(aid.accounting_date) =
							   trunc(my_rec_manual.doc_date)
						   AND aag.name = my_rec_manual.wht_code
						   AND ai.org_id = p_org_id; ----add by aeaum 23/09/2009------
					EXCEPTION
						WHEN no_data_found THEN
							v_base_amt_line := 0;
					END;
					write_log('60 end : Calculate BASE AMOUNT LINE');
					-- Calculate TAX AMOUNT LINE
					write_log('70 begin : Calculate TAX AMOUNT LINE');
					BEGIN
						SELECT nvl(SUM(nvl(xap.accounted_dr, 0) -
									   nvl(xap.accounted_cr, 0)),
								   0)
						  INTO v_tax_amt_line
						  FROM ap_invoice_distributions aid,
							   ap_invoices              ai,
							   ap_tax_codes             atc,
							   po_vendors               pv,
							   hr_locations             hla,
							   po_vendor_sites          pvs,
							   ap_ae_lines_all          xap
						 WHERE aid.invoice_id = ai.invoice_id
						   AND aid.line_type_lookup_code = 'AWT'
						   AND aid.awt_flag = 'M'
						   AND aid.tax_code_id = atc.tax_id
						   AND atc.global_attribute19 = hla.location_id
						   AND ai.vendor_id = pv.vendor_id
						   AND ai.vendor_site_id = pvs.vendor_site_id
						   AND ap_invoices_pkg.get_posting_status(ai.invoice_id) = 'Y'
						   AND ap_invoices_pkg.get_approval_status(ai.invoice_id,
																   ai.invoice_amount,
																   ai.payment_status_flag,
																   ai.invoice_type_lookup_code) IN
							   ('APPROVED', 'CANCELLED')
						   AND xap.source_table =
							   'AP_INVOICE_DISTRIBUTIONS'
						   AND xap.source_id = aid.invoice_distribution_id
						   AND (aid.invoice_distribution_id, 'WHT') NOT IN
							   (SELECT tidl.invoice_distribution_id,
									   tidl.invoice_type
								  FROM th_itemtax_distribution_loaded tidl
								 WHERE tidl.invoice_type = 'WHT'
								   AND tidl.invoice_distribution_id =
									   aid.invoice_distribution_id)
						   AND ai.invoice_id = my_rec_manual.invoice_id
						   AND trunc(aid.accounting_date) =
							   trunc(my_rec_manual.doc_date)
						   AND atc.name = my_rec_manual.wht_code
						   AND ai.org_id = p_org_id; ----add by aeaum 23/09/2009------
					EXCEPTION
						WHEN no_data_found THEN
							v_tax_amt_line := 0;
					END;
					write_log('70 end : Calculate TAX AMOUNT LINE');
					-- Insert Data to table : th_wht_line
					write_log('80 begin : Insert Data to table : th_wht_line');
					INSERT INTO th_wht_line
						(th_whtl_id,
						 th_whth_id,
						 th_whtl_wt_code,
						 th_whtl_revenue_type,
						 th_whtl_revenue_name,
						 th_whtl_base_amt,
						 th_whtl_tax_amt,
						 th_whtl_supplier_name,
						 th_whtl_supplier_address1,
						 th_whtl_supplier_address2,
						 th_whtl_supplier_address3,
						 th_whtl_supplier_tax_id,
						 th_whtl_supplier_branch_no,
						 th_whtl_agent_name,
						 th_whtl_agent_address1,
						 th_whtl_agent_address2,
						 th_whtl_agent_address3,
						 th_whtl_agent_tax_id,
						 th_whtl_agent_branch_no,
						 th_whtl_resp_name,
						 th_whtl_resp_address1,
						 th_whtl_resp_address2,
						 th_whtl_resp_address3,
						 th_whtl_resp_tax_id,
						 th_whtl_resp_branch_no,
						 attribute1,
						 created_by,
						 creation_date,
						 last_updated_by,
						 last_update_date,
						 last_update_login,
						 org_id)
					VALUES
						(v_line_id,
						 v_head_id,
						 my_rec_manual.wht_code,
						 my_rec_manual.revenue_type,
						 my_rec_manual.revenue_name,
						 v_base_amt_line,
						 v_tax_amt_line,
						 my_rec_manual.supplier_name,
						 my_rec_manual.supplier_address1,
						 my_rec_manual.supplier_address2,
						 my_rec_manual.supplier_address3,
						 my_rec_manual.supplier_tax_id,
						 my_rec_manual.supplier_branch_no,
						 my_rec_manual.agent_name,
						 my_rec_manual.agent_address,
						 NULL,
						 NULL,
						 my_rec_manual.agent_tax_id,
						 my_rec_manual.agent_branch_no,
						 my_rec_manual.resp_name,
						 my_rec_manual.resp_address1,
						 my_rec_manual.resp_address2,
						 my_rec_manual.resp_address3,
						 my_rec_manual.resp_tax_id,
						 my_rec_manual.resp_branch_no,
						 p_conc_request_id,
						 my_rec_manual.created_by,
						 my_rec_manual.creation_date,
						 my_rec_manual.last_updated_by,
						 my_rec_manual.last_update_date,
						 my_rec_manual.last_update_login,
						 my_rec_manual.org_id);
					write_log('80 end : Insert Data to table : th_wht_line');
				END IF;
			END LOOP;
			CLOSE my_cursor_manual;
		END LOOP;
		CLOSE my_cursor_manual_head;
		-- Insert Data to table : th_itemtax_distribution_loaded
		-- For Check Data Loaded
		IF v_insert THEN
			write_log('90 begin : Insert Data to table : th_itemtax_distribution_loaded');
			OPEN my_cursor_manual_dis;
			LOOP
				FETCH my_cursor_manual_dis
					INTO my_rec_manual_dis;
				EXIT WHEN my_cursor_manual_dis%NOTFOUND;
				INSERT INTO th_itemtax_distribution_loaded
					(invoice_distribution_id,
					 invoice_type,
					 org_id,
					 created_by,
					 creation_date)
				VALUES
					(my_rec_manual_dis.invoice_distribution_id,
					 'WHT',
					 my_rec_manual_dis.org_id,
					 my_rec_manual_dis.created_by,
					 my_rec_manual_dis.creation_date);
			END LOOP;
			CLOSE my_cursor_manual_dis;
			write_log('90 end : Insert Data to table : th_itemtax_distribution_loaded');
		END IF;
		COMMIT;
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
	END th_load_wht_m;
	/*================================================================
      PROCEDURE NAME:     write_log()
    ==================================================================*/
	PROCEDURE write_log(param_msg VARCHAR2) IS
	BEGIN
		--fnd_file.put_line(fnd_file.log, param_msg);
		dbms_output.put_line(param_msg);
	END write_log;
	PROCEDURE write_output4test(param_msg VARCHAR2) IS
	BEGIN
		dbms_output.put_line(param_msg);
	END write_output4test;
END TH_LOAD_LOCALIZATION_V2;
/
