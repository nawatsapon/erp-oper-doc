UPDATE th_taxinvoices_line l 
SET l.submit_period_name = l.th_til_period_name
	,l.submit_num		= NVL(l.submit_num,0)
WHERE l.submit_period_name IS NULL
AND l.th_til_comfirmed_date IS NOT NULL
COMMIT;

INSERT INTO tac_vat_period_submitted
(org_id ,location_id ,period_name ,submit_num_closed)
SELECT h.org_id,l.th_til_location_id,l.th_til_period_name,NVL(l.submit_num,0) submit_num
FROM th_taxinvoices_line l
	,th_taxinvoices_header h
WHERE l.th_tih_id = h.th_tih_id	
AND l.th_til_comfirmed_date IS NOT NULL
AND NOT EXISTS (SELECT 'x' FROM tac_vat_period_submitted s
				WHERE s.org_id = h.org_id
				AND s.location_id = l.th_til_location_id
				AND s.period_name = l.th_til_period_name
				AND s.submit_num_closed = NVL(l.submit_num,0))
GROUP BY h.org_id,l.th_til_location_id,l.th_til_period_name,NVL(l.submit_num,0);
COMMIT;		
		
INSERT INTO tac_vat_period_status
(org_id ,location_id ,period_name ,latest_submit_num_closed ,current_submit_num)
SELECT h.org_id,l.th_til_location_id,l.th_til_period_name,MAX(NVL(l.submit_num,0)),MAX(NVL(l.submit_num,0))+1
FROM th_taxinvoices_line l
	,th_taxinvoices_header h
WHERE l.th_tih_id = h.th_tih_id	
AND l.th_til_comfirmed_date IS NOT NULL
AND NOT EXISTS (SELECT 'x' FROM tac_vat_period_status s
				WHERE s.org_id = h.org_id
				AND s.location_id = l.th_til_location_id
				AND s.period_name = l.th_til_period_name)
GROUP BY h.org_id,l.th_til_location_id,l.th_til_period_name;					
COMMIT;	
