create or replace package TAC_PAYMENT_ACTION_PKG is
/*Change History
 Date      Author        Version   Description
 --------- -----------    ------   ------------------------------------------------
 06-Nov-17  Pinyo P.    1.00   Created
*/
----------------------------------------------------------------------------
PROCEDURE clear_data(i_request_id	IN	NUMBER);
----------------------------------------------------------------------------
PROCEDURE VALIDATE_PAYMENT(
  i_check_id         IN       NUMBER,
  i_request_id		   IN       NUMBER
);
----------------------------------------------------------------------------
PROCEDURE VALIDATE_PAYMENT_BATCH(
  i_checkrun_id           IN       NUMBER,
  i_request_id		   IN       NUMBER
);
----------------------------------------------------------------------------
PROCEDURE VOID_PAYMENT_REPORT(
  err_msg              OUT      VARCHAR2,
  err_code             OUT      VARCHAR2,
  i_request_id         IN       NUMBER
);
----------------------------------------------------------------------------
PROCEDURE submit_conc
(i_request_id   IN 	NUMBER,
 o_request_id		OUT NUMBER);
----------------------------------------------------------------------------
end TAC_PAYMENT_ACTION_PKG;
/
create or replace package body TAC_PAYMENT_ACTION_PKG is
/*Change History
 Date      Author        Version   Description
 --------- -----------    ------   ------------------------------------------------
 06-Nov-17  Pinyo P.    1.00   Created
*/
----------------------------------------------------------------------------

----------------------------------------------------------------------------
PROCEDURE write_log (param_msg VARCHAR2 ) IS
BEGIN
 fnd_file.put_line(fnd_file.log, param_msg);
END write_log;
----------------------------------------------------------------------------
PROCEDURE write_output (param_msg VARCHAR2)
IS
BEGIN
  fnd_file.put_line (fnd_file.output, param_msg);
END write_output;
----------------------------------------------------------------------------
PROCEDURE clear_data(i_request_id	IN	NUMBER) IS
BEGIN
	DELETE	TAC_VALIDATE_PAYMENT
	WHERE	request_id = i_request_id;
  --
	DELETE	TAC_VALIDATE_PAYMENT
	WHERE creation_date < SYSDATE -2;

	COMMIT;
END;
----------------------------------------------------------------------------
PROCEDURE INSERT_TAC_VALIDATE_PAYMENT(
 io_tac_validate_payment IN OUT TAC_VALIDATE_PAYMENT%ROWTYPE
) IS 
BEGIN
  INSERT INTO TAC_VALIDATE_PAYMENT
  (
  request_id, 
  check_id, 
  document_num, 
  document_date, 
  msg_error, 
  creation_date, 
  created_by
  ) VALUES (
  io_tac_validate_payment.request_id, 
  io_tac_validate_payment.check_id, 
  io_tac_validate_payment.document_num, 
  io_tac_validate_payment.document_date,
  io_tac_validate_payment.msg_error, 
  SYSDATE, 
  io_tac_validate_payment.created_by
  );
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
	ROLLBACK;
END; 
----------------------------------------------------------------------------
PROCEDURE submit_conc
(i_request_id   IN 	NUMBER,
 o_request_id		OUT NUMBER)
IS
BEGIN
		o_request_id := FND_REQUEST.submit_request (
    	'SQLAP' ,				--  application IN varchar2 default NULL,
    	'TAC_VOID_PAYMENT_REPORT'  ,		-- program     IN varchar2 default NULL,
    	NULL,	-- description IN varchar2 default NULL,
    	NULL,	-- start_time  IN varchar2 default NULL,
    	NULL,	-- sub_request IN boolean  default FALSE,
    	i_request_id,	--1
    	CHR(0),		--2
    	CHR(0),	--3
		CHR(0),	--4
		CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),   --5--10
        CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),	--11-20
        CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),	--21-30
        CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),	--31-40
        CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),	--41-50
        CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),	--51-60
        CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),	--61-70
        CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),	--71-80
        CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),	--81-90
        CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0));	--91-100

		COMMIT;

END;
---------------------------------------------------------------------------
PROCEDURE VALIDATE_PAYMENT(
  i_check_id         IN       NUMBER,
  i_request_id		   IN       NUMBER
) IS

  v_tac_validate_payment TAC_VALIDATE_PAYMENT%ROWTYPE;
  v_request_id         NUMBER;
  v_submit_request_id        NUMBER;
  t_Token_tbl				tac_fnd_util_pkg.Token_Tbl_Type;
  v_msg					VARCHAR2(4000);
  v_check_date			VARCHAR2(100);
  
  CURSOR c_check_period IS 
	SELECT
	ttl.* 
	FROM TH_TAXINVOICES_HEADER tth
	,TH_TAXINVOICES_LINE ttl
	WHERE tth.TH_TIH_TRANSACTION_ID = i_check_id
	AND tth.TH_TIH_TYPE_TAX = 'S'
	AND tth.th_tih_id = ttl.th_tih_id
	AND ttl.th_til_period_name IS NOT NULL;

				  
BEGIN
  v_msg := NULL;
 
  FOR r IN c_check_period LOOP		
      t_Token_tbl.DELETE;
			v_msg := tac_fnd_util_pkg.get_message
			(i_message_name		=>	'TAC_AP_INV_NOVOID_4RELEASED'
			,i_app_short_name	=>	'SQLAP'
			,it_Token_tbl			=>	t_Token_tbl);
      
	  v_tac_validate_payment := NULL;
      v_tac_validate_payment.request_id := i_request_id;
      v_tac_validate_payment.check_id := i_check_id;
      v_tac_validate_payment.msg_error := v_msg;
      INSERT_TAC_VALIDATE_PAYMENT(v_tac_validate_payment);
	  EXIT;
  END LOOP;
  
  BEGIN
	  SELECT TO_CHAR(t.check_date,'MM-YYYY')
	  INTO v_check_date
	  FROM  ap_checks_all t
	  WHERE t.check_id = i_check_id;
	  EXCEPTION WHEN OTHERS THEN v_check_date := '';
  END;	  
  IF v_check_date <> TO_CHAR(SYSDATE,'MM-YYYY') THEN 
		t_Token_tbl.DELETE;
		v_msg := tac_fnd_util_pkg.get_message
		(i_message_name		=>	'TAC_AP_PMT_NOVOID_ACROSS_MONTH'
		,i_app_short_name	=>	'SQLAP'
		,it_Token_tbl			=>	t_Token_tbl);
      
	  v_tac_validate_payment := NULL;
      v_tac_validate_payment.request_id := i_request_id;
      v_tac_validate_payment.check_id := i_check_id;
      v_tac_validate_payment.msg_error := v_msg;
      INSERT_TAC_VALIDATE_PAYMENT(v_tac_validate_payment);
  END IF;
  	  
  COMMIT;  
END;
----------------------------------------------------------------------------
PROCEDURE VALIDATE_PAYMENT_BATCH(
  i_checkrun_id           IN       NUMBER,
  i_request_id		   IN       NUMBER
) IS
  
  CURSOR c_batch IS 
  SELECT t.* 
  FROM ap_checks_all t
  WHERE t.checkrun_id = i_checkrun_id
  ORDER BY t.check_id;
  
BEGIN
    
  FOR r IN c_batch LOOP  
    VALIDATE_PAYMENT(r.check_id, i_request_id);
  END LOOP;
  
END;
----------------------------------------------------------------------------
PROCEDURE VOID_PAYMENT_REPORT(
  err_msg              OUT      VARCHAR2,
  err_code             OUT      VARCHAR2,
  i_request_id         IN       NUMBER
) IS
  CURSOR c_q1 IS 
  SELECT t.msg_error
         ,t.check_id
         ,ai.check_number  document_num 
		 ,(SELECT ais.checkrun_name
			FROM  AP_INV_SELECTION_CRITERIA_ALL ais
			WHERE ais.checkrun_id = ai.checkrun_id) batch_name 
  FROM TAC_VALIDATE_PAYMENT t,
       ap_checks_all ai
  WHERE t.request_id = i_request_id
  AND  t.check_id = ai.check_id
  ORDER BY batch_name, ai.check_number, t.msg_error;
BEGIN
  IF i_request_id IS NOT NULL THEN 
    write_output(RPAD('Batch Name',50,' ')||'  '||RPAD('Document Number',30,' ')||'  '||'Error Message');
    FOR r IN c_q1 LOOP
      write_output(RPAD(r.batch_name,50,' ')||'  '||RPAD(r.document_num,30,' ')||'  '||r.msg_error);
    END LOOP;  
  ELSE
      write_log('Request ID not Found.');
  END IF;
  NULL;
END;    
----------------------------------------------------------------------------
end TAC_PAYMENT_ACTION_PKG;
/
