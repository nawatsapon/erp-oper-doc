create or replace package xxtac_fnd_util_pkg IS
/*Change History
 Date      Author        Version   Description
 --------- -----------    ------   ------------------------------------------------
 06-Nov-17  Pinyo P.    1.00   Created
*/

TYPE Token_Rec_Type IS RECORD
(token_code          VARCHAR2(100)
,token_value    VARCHAR2(4000)
);

TYPE Token_Tbl_Type IS TABLE OF Token_Rec_Type
    INDEX BY BINARY_INTEGER;

FUNCTION get_message
(i_message_name    VARCHAR2
,i_app_short_name  VARCHAR2
,i_language      VARCHAR2  :=  NULL
,it_Token_tbl    Token_Tbl_Type)
RETURN VARCHAR2;

FUNCTION get_message_personalize
(i_message_name    VARCHAR2
,i_app_short_name  VARCHAR2
,i_token_code1    VARCHAR2  :=  NULL
,i_token_value1    VARCHAR2  :=  NULL
,i_token_code2    VARCHAR2  :=  NULL
,i_token_value2    VARCHAR2  :=  NULL
,i_token_code3    VARCHAR2  :=  NULL
,i_token_value3    VARCHAR2  :=  NULL
,i_token_code4    VARCHAR2  :=  NULL
,i_token_value4    VARCHAR2  :=  NULL
,i_token_code5    VARCHAR2  :=  NULL
,i_token_value5    VARCHAR2  :=  NULL
,i_token_code6    VARCHAR2  :=  NULL
,i_token_value6    VARCHAR2  :=  NULL
,i_token_code7    VARCHAR2  :=  NULL
,i_token_value7    VARCHAR2  :=  NULL
,i_token_code8    VARCHAR2  :=  NULL
,i_token_value8    VARCHAR2  :=  NULL
,i_token_code9    VARCHAR2  :=  NULL
,i_token_value9    VARCHAR2  :=  NULL
,i_token_code10    VARCHAR2  :=  NULL
,i_token_value10  VARCHAR2  :=  NULL
,i_token_code11    VARCHAR2  :=  NULL
,i_token_value11  VARCHAR2  :=  NULL
,i_token_code12    VARCHAR2  :=  NULL
,i_token_value12  VARCHAR2  :=  NULL
,i_token_code13    VARCHAR2  :=  NULL
,i_token_value13  VARCHAR2  :=  NULL
,i_token_code14    VARCHAR2  :=  NULL
,i_token_value14  VARCHAR2  :=  NULL
,i_token_code15    VARCHAR2  :=  NULL
,i_token_value15  VARCHAR2  :=  NULL
,i_token_code16    VARCHAR2  :=  NULL
,i_token_value16  VARCHAR2  :=  NULL
,i_token_code17    VARCHAR2  :=  NULL
,i_token_value17  VARCHAR2  :=  NULL
,i_token_code18    VARCHAR2  :=  NULL
,i_token_value18  VARCHAR2  :=  NULL
,i_token_code19    VARCHAR2  :=  NULL
,i_token_value19  VARCHAR2  :=  NULL
,i_token_code20    VARCHAR2  :=  NULL
,i_token_value20  VARCHAR2  :=  NULL)
RETURN VARCHAR2;

end xxtac_fnd_util_pkg;
/
create or replace package body xxtac_fnd_util_pkg IS
/*Change History
 Date      Author        Version   Description
 --------- -----------    ------   ------------------------------------------------
 06-Nov-17  Pinyo P.    1.00   Created
*/
------------------------------------------------------------------------------------------
FUNCTION get_message
(i_message_name    VARCHAR2
,i_app_short_name  VARCHAR2
,i_language      VARCHAR2  :=  NULL
,it_Token_tbl    Token_Tbl_Type)
RETURN VARCHAR2 IS
  CURSOR get_msg IS
    SELECT message_text
        FROM fnd_new_messages m, fnd_application a
        WHERE m.message_name   =   i_message_name
        AND m.language_code    =  NVL(i_language,userenv('LANG'))
        AND a.application_short_name  =  i_app_short_name
        AND m.application_id = a.application_id;

  v_message      VARCHAR2(2000);
  v_symbol      VARCHAR2(1)    :=  '&';
BEGIN
  FOR r_msg IN get_msg LOOP
    v_message  :=  r_msg.message_text;
    EXIT;
  END LOOP;
  IF v_message IS NULL THEN
    v_message  :=  i_message_name;
  ELSE
      FOR i IN 1..it_Token_tbl.COUNT LOOP
        v_message  :=  REPLACE(v_message,v_symbol||it_Token_tbl(i).token_code,it_Token_tbl(i).token_value);
      END LOOP;
  END IF;
  RETURN   v_message;
END get_message;
------------------------------------------------------------------------------------------
FUNCTION get_message_personalize
(i_message_name    VARCHAR2
,i_app_short_name  VARCHAR2
,i_token_code1    VARCHAR2  :=  NULL
,i_token_value1    VARCHAR2  :=  NULL
,i_token_code2    VARCHAR2  :=  NULL
,i_token_value2    VARCHAR2  :=  NULL
,i_token_code3    VARCHAR2  :=  NULL
,i_token_value3    VARCHAR2  :=  NULL
,i_token_code4    VARCHAR2  :=  NULL
,i_token_value4    VARCHAR2  :=  NULL
,i_token_code5    VARCHAR2  :=  NULL
,i_token_value5    VARCHAR2  :=  NULL
,i_token_code6    VARCHAR2  :=  NULL
,i_token_value6    VARCHAR2  :=  NULL
,i_token_code7    VARCHAR2  :=  NULL
,i_token_value7    VARCHAR2  :=  NULL
,i_token_code8    VARCHAR2  :=  NULL
,i_token_value8    VARCHAR2  :=  NULL
,i_token_code9    VARCHAR2  :=  NULL
,i_token_value9    VARCHAR2  :=  NULL
,i_token_code10    VARCHAR2  :=  NULL
,i_token_value10  VARCHAR2  :=  NULL
,i_token_code11    VARCHAR2  :=  NULL
,i_token_value11  VARCHAR2  :=  NULL
,i_token_code12    VARCHAR2  :=  NULL
,i_token_value12  VARCHAR2  :=  NULL
,i_token_code13    VARCHAR2  :=  NULL
,i_token_value13  VARCHAR2  :=  NULL
,i_token_code14    VARCHAR2  :=  NULL
,i_token_value14  VARCHAR2  :=  NULL
,i_token_code15    VARCHAR2  :=  NULL
,i_token_value15  VARCHAR2  :=  NULL
,i_token_code16    VARCHAR2  :=  NULL
,i_token_value16  VARCHAR2  :=  NULL
,i_token_code17    VARCHAR2  :=  NULL
,i_token_value17  VARCHAR2  :=  NULL
,i_token_code18    VARCHAR2  :=  NULL
,i_token_value18  VARCHAR2  :=  NULL
,i_token_code19    VARCHAR2  :=  NULL
,i_token_value19  VARCHAR2  :=  NULL
,i_token_code20    VARCHAR2  :=  NULL
,i_token_value20  VARCHAR2  :=  NULL)
RETURN VARCHAR2 IS
  t_Token_tbl    Token_Tbl_Type;
  v_message    VARCHAR2(2000);
BEGIN
    t_Token_tbl.DELETE;
  IF i_token_code1 IS NOT NULL THEN
        t_Token_tbl(1).token_code     :=  i_token_code1;
        t_Token_tbl(1).token_value     :=  i_token_value1;
  END IF;
  IF i_token_code2 IS NOT NULL THEN
        t_Token_tbl(2).token_code     :=  i_token_code2;
        t_Token_tbl(2).token_value     :=  i_token_value2;
  END IF;
  IF i_token_code3 IS NOT NULL THEN
        t_Token_tbl(3).token_code     :=  i_token_code3;
        t_Token_tbl(3).token_value     :=  i_token_value3;
  END IF;
  IF i_token_code4 IS NOT NULL THEN
        t_Token_tbl(4).token_code     :=  i_token_code4;
        t_Token_tbl(4).token_value     :=  i_token_value4;
  END IF;
  IF i_token_code5 IS NOT NULL THEN
        t_Token_tbl(5).token_code     :=  i_token_code5;
        t_Token_tbl(5).token_value     :=  i_token_value5;
  END IF;
  IF i_token_code6 IS NOT NULL THEN
        t_Token_tbl(6).token_code     :=  i_token_code6;
        t_Token_tbl(6).token_value     :=  i_token_value6;
  END IF;
  IF i_token_code7 IS NOT NULL THEN
        t_Token_tbl(7).token_code     :=  i_token_code7;
        t_Token_tbl(7).token_value     :=  i_token_value7;
  END IF;
  IF i_token_code8 IS NOT NULL THEN
        t_Token_tbl(8).token_code     :=  i_token_code8;
        t_Token_tbl(8).token_value     :=  i_token_value8;
  END IF;
  IF i_token_code9 IS NOT NULL THEN
        t_Token_tbl(9).token_code     :=  i_token_code9;
        t_Token_tbl(9).token_value     :=  i_token_value9;
  END IF;
  IF i_token_code10 IS NOT NULL THEN
        t_Token_tbl(10).token_code     :=  i_token_code10;
        t_Token_tbl(10).token_value   :=  i_token_value10;
  END IF;
  IF i_token_code11 IS NOT NULL THEN
        t_Token_tbl(11).token_code     :=  i_token_code11;
        t_Token_tbl(11).token_value   :=  i_token_value11;
  END IF;
  IF i_token_code12 IS NOT NULL THEN
        t_Token_tbl(12).token_code     :=  i_token_code12;
        t_Token_tbl(12).token_value   :=  i_token_value12;
  END IF;
  IF i_token_code13 IS NOT NULL THEN
        t_Token_tbl(13).token_code     :=  i_token_code13;
        t_Token_tbl(13).token_value   :=  i_token_value13;
  END IF;
  IF i_token_code14 IS NOT NULL THEN
        t_Token_tbl(14).token_code     :=  i_token_code14;
        t_Token_tbl(14).token_value   :=  i_token_value14;
  END IF;
  IF i_token_code15 IS NOT NULL THEN
        t_Token_tbl(15).token_code     :=  i_token_code15;
        t_Token_tbl(15).token_value   :=  i_token_value15;
  END IF;
  IF i_token_code16 IS NOT NULL THEN
        t_Token_tbl(16).token_code     :=  i_token_code16;
        t_Token_tbl(16).token_value   :=  i_token_value16;
  END IF;
  IF i_token_code17 IS NOT NULL THEN
        t_Token_tbl(17).token_code     :=  i_token_code17;
        t_Token_tbl(17).token_value   :=  i_token_value17;
  END IF;
  IF i_token_code18 IS NOT NULL THEN
        t_Token_tbl(18).token_code     :=  i_token_code18;
        t_Token_tbl(18).token_value   :=  i_token_value18;
  END IF;
  IF i_token_code19 IS NOT NULL THEN
        t_Token_tbl(19).token_code     :=  i_token_code19;
        t_Token_tbl(19).token_value   :=  i_token_value19;
  END IF;
  IF i_token_code20 IS NOT NULL THEN
        t_Token_tbl(20).token_code     :=  i_token_code20;
        t_Token_tbl(20).token_value   :=  i_token_value20;
  END IF;
    v_message  :=  get_message(i_message_name    =>  i_message_name
                              ,i_app_short_name    =>  i_app_short_name
                              ,it_Token_tbl      =>  t_Token_tbl);
  RETURN   v_message;
END get_message_personalize;
------------------------------------------------------------------------------------------
FUNCTION get_profile
(i_profile_name     IN  VARCHAR2
,i_user_id         IN  NUMBER     :=  NULL
,i_responsibility_id   IN  NUMBER     :=  NULL
,i_application_id     IN  NUMBER     :=  NULL
,i_org_id         IN  NUMBER    :=  NULL
,i_server_id       IN  NUMBER     :=  NULL)
RETURN VARCHAR2 IS
  v_profile_value                 VARCHAR2(255);
BEGIN
  v_profile_value  :=  fnd_profile.value_specific
                (name         => i_profile_name
                  ,user_id       => i_user_id
                  ,responsibility_id   => i_responsibility_id
                  ,application_id   => i_application_id
                  ,org_id       => i_org_id
                  ,server_id       => i_server_id);
  RETURN v_profile_value;
END get_profile;
------------------------------------------------------------------------------------------
end xxtac_fnd_util_pkg;
/
