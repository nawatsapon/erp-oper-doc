-- Create table
create table TAC_TMP_VALIDATE_INVOICE
(
  request_id            NUMBER,
  invoice_id            NUMBER,
  line_type_lookup_code VARCHAR2(100),
  name                  VARCHAR2(100),
  tax_rate              NUMBER,
  amount                NUMBER,
  vat_amt               NUMBER,
  found_flag            VARCHAR2(100),
  creation_date         DATE default SYSDATE,
  created_by            NUMBER
);
-- Create/Recreate indexes 
create index TAC_TMP_VALIDATE_INVOICE_ID on TAC_TMP_VALIDATE_INVOICE (REQUEST_ID, INVOICE_ID, LINE_TYPE_LOOKUP_CODE, NAME, TAX_RATE);
