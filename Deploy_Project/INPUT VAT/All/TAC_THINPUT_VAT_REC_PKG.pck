CREATE OR REPLACE PACKAGE TAC_THINPUT_VAT_REC_PKG IS

PROCEDURE main_generate_file
(ERR_MSG				OUT	VARCHAR2
,ERR_CODE				OUT	VARCHAR2
,P_TAX_PERIOD_FROM			IN VARCHAR2
,P_TAX_PERIOD_TO			IN VARCHAR2
,P_VAT_TAX_CODE				IN VARCHAR2
,P_FROM_INVOICE_DATE		IN VARCHAR2
,P_DEFAULT_SUBMIT_PERIOD	IN VARCHAR2
,P_SHOW_UNACCT_INV			IN VARCHAR2
);

END TAC_THINPUT_VAT_REC_PKG;
/
CREATE OR REPLACE PACKAGE BODY TAC_THINPUT_VAT_REC_PKG IS
/*Change History
 Date      	Author       	Version 	Description
 --------- 	-------------	--------- 	------------------------------------------------
 20-Nov-17 	TM.    			1.00 		Created

*/
ABNORMAL_END      		EXCEPTION;
g_org_id   				NUMBER := FND_PROFILE.VALUE('ORG_ID');
g_log_in   				NUMBER := FND_GLOBAL.CONC_LOGIN_ID;
g_user_id   			NUMBER := FND_GLOBAL.USER_ID;
g_request_id  			NUMBER := FND_GLOBAL.CONC_REQUEST_ID;
g_no_error				VARCHAR2(100)	:=	'No Error';
e_abnormal_end		EXCEPTION;
---------------------------------------------------------------------------
PROCEDURE write_log (param_msg VARCHAR2 ) IS
BEGIN
  ---fnd_file.put_line(fnd_file.log, param_msg);
  fnd_file.put_line(fnd_file.log, TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS')||' '||param_msg);
END write_log;
----------------------------------------------------------------------------
FUNCTION update_string (i_column IN VARCHAR2)
  RETURN VARCHAR2
IS
  v_return   VARCHAR2 (4000);
BEGIN
  IF i_column IS NULL
  THEN
     v_return := '';
  ELSE
     v_return := REPLACE (i_column, '', '');
  END IF;

  RETURN ''||v_return||'';
END;
----------------------------------------------------------------------------
PROCEDURE generate_csv
(P_TAX_PERIOD_FROM			IN VARCHAR2
,P_TAX_PERIOD_TO			IN VARCHAR2
,P_VAT_TAX_CODE				IN VARCHAR2
,P_FROM_INVOICE_DATE		IN VARCHAR2
,P_DEFAULT_SUBMIT_PERIOD	IN VARCHAR2
,P_SHOW_UNACCT_INV			IN VARCHAR2
,o_status		OUT	VARCHAR2)
IS
/*--------------------------- File Format -----------------------------------
No.	Field Name				Data Type	Sample Data	Description
1	No.						Numeric		1
2	Pay Group				Text		S-CTB-CHQ-A-COUNTER
3	Liability Acct			Text
4	Invoice Date			Date		01-OCT-14
5	AP Voucher				Text		2014072080
6	Invoice Num				Text		2014/0105
7	Description				Text		P.10/14 ������ Mr.Anders Renchil
8	CMS Linkage Value		Text		CMSA2560/10
9	Status					Text		APPROVED
10	Posting Status			Text		Yes
11	Invoice Amt 	 		Numeric		120,000.00
12	Vendor Num				Text		177803
13	Vendor Name				Text		Garden View Terrace Co.,Ltd.
14	Requester				Text		�ʴ����� Requester
15	Division				Text		�ʴ� division �Ѩ�غѹ�ͧ Requester
16	AP's GL Date			Date		01-OCT-14
17	Month					Numeric		10
18	Year					Numeric		2014
19	Line's GL Date			Date		01-OCT-14
20	Month					Numeric		10
21	Year					Numeric		2014
22 	WHT Group				Text		'WHT1'
23	Base Amt				Numeric		25,570.09
24	WHT Amt					Numeric		1,278.50
25	Tax Amt					Numeric		1,789.91
26	Tax Code				Numeric		S-VAT
27	No.						Numeric		1
28	Payment Date			Date		01-OCT-14
29	Payment Voucher			Text		2014002222
30	Document Number			Text		�ʴ� Payment's
31	Status					Text		APPROVED
32	Posting Status			Text		Yes
33	Paid Amt				Numeric		26,081.50
34	No.						Numeric		1
35	Tax Code				Text		S-VAT
36	Base Amt				Numeric		25,570.09
37	Non-Rec Amt				Numeric		0
38	Rec Amt					Numeric		1,278.50
39	No.						Numeric		1
40	Legal Loc.				Text		TAC-HO
41	Cat. Code				Text		IN10
42	Tax Invoice No.			Text		17020042
43	Tax Invoice Date		Date		01-OCT-14
44	Tax Period				Text		OCT-14
45	Submit#					Numeric		0
46	Submit Period			Text		OCT-14
47	Base Amt				Numeric		25,570.09
48	Tax Amt					Numeric		1,278.50
49	Last Update Date		Date		01-OCT-14
50	Last Update By			Text		released_by
----------------------------------------------------------------------*/
	CURSOR c_trx IS
	SELECT
		invoice_id							Invoice_Id
		,pay_group							Invoice_Pay_Group
		,liability_acct						Invoice_Liability_Acct
		,TO_CHAR(invoice_date,'DD-MON-YY')	Invoice_Date
		,ap_voucher							Invoice_AP_Voucher
		,invoice_num						Invoice_Num
		,REPLACE(REPLACE(description,CHR(13),''),CHR(10),'')						Invoice_Description
		,CMS_Linkage_Value					Invoice_CMS_Linkage_Value
		,invoice_status						Invoice_Status
		,posting_status						Invoice_Posting_Status
		,Invoice_Amt 	 					Invoice_Amt
		,vendor_num							Invoice_Vendor_Num
		,vendor_name						Invoice_Vendor_Name
		,requester							Invoice_Requester
		,division							Invoice_Division
		,TO_CHAR(ap_gl_date,'DD-MON-YY')	Invoice_AP_GL_Date
		,ap_gl_month						Invoice_AP_GL_Month
		,ap_gl_year							Invoice_AP_GL_Year
		,TO_CHAR(line_gl_date,'DD-MON-YY')	Invoice_Line_GL_Date
		,line_gl_month						Invoice_Line_GL_Month
		,line_gl_year						Invoice_Line_GL_Year
		,WHT_Group							Invoice_WHT_Group
		,Base_Amt							Invoice_Base_Amt
		,WHT_Amt							Invoice_WHT_Amt
		,Tax_Amt							Invoice_Tax_Amt
		,tax_code							Invoice_Tax_Code
		,Payment_Id							Payment_Id
		,TO_CHAR(Payment_Date,'DD-MON-YY')	Payment_Date
		,Payment_Voucher					Payment_Voucher
		,Payment_Document_Number			Payment_Document_Number
		,Payment_Status_Disp				Payment_Status
		,Payment_Posting_Status				Payment_Posting_Status
		,Payment_Base_Amt					Payment_Paid_Amt
		,TTH_Id								TTH_Id
		,TTH_Tax_Code						TTH_Tax_Code
		,TTH_Base_Amt						TTH_Base_Amt
		,TTH_NonRec_Amt						TTH_NonRec_Amt
		,TTH_Rec_Amt						TTH_Rec_Amt
		,TTL_Id								TTL_Id
		,TTL_Legal_Loc						TTL_Legal_Loc
		,TTL_Cat_Code						TTL_Cat_Code
		,TTL_Tax_Invoice_No					TTL_Tax_Invoice_No
		,TO_CHAR(TTL_Tax_Invoice_Date,'DD-MON-YY')		TTL_Tax_Invoice_Date
		,TTL_Tax_Period						TTL_Tax_Period
		,TTL_Submit							TTL_Submit
		,TTL_Submit_Period					TTL_Submit_Period
		,TTL_Base_Amt						TTL_Base_Amt
		,TTL_Tax_Amt						TTL_Tax_Amt
		,TO_CHAR(TTL_Last_Update_Date,'DD-MON-YY')		TTL_Last_Update_Date
		,TTL_Last_Update_By					TTL_Last_Update_By
		,Invoice_Dist_Id					Invoice_Dist_Id
		,Invoice_Payment_Id					Invoice_Payment_Id
	FROM tac_thinput_vat_rec_tth
	WHERE process_id = g_request_id
	ORDER BY min_data_group
			,ap_voucher
			,SUBSTR(data_group,1,5)
			,Payment_Voucher
			,SUBSTR(data_group,1,8)
			,TTH_Tax_Code
			,data_group
			,TTL_Legal_Loc
			,TTL_Tax_Period
			,TTL_Submit
			,TTL_Submit_Seq_Num
			,TTL_Tax_Invoice_No;

	t_text			tac_text_output_util.varchar_tabtyp;
	t_format		tac_text_output_util.number_tabtyp;

	v_invoice_seq			NUMBER;
	v_payment_seq			NUMBER;
	v_tth_seq				NUMBER;
	v_ttl_seq				NUMBER;

	v_prev_invoice_id			NUMBER	:=	-999;
	v_prev_invoice_Dist_id		NUMBER	:=	-999;
	v_prev_invoice_payment_id	NUMBER	:=	-999;
	v_prev_tth_id				NUMBER	:=	-999;

	v_invoice_amt			NUMBER;
	v_payment_amt			NUMBER;
	v_org_name				VARCHAR2(1000);
BEGIN
	o_status	:=	G_NO_ERROR;
	BEGIN
		SELECT b.name 
		INTO v_org_name
		FROM hr_operating_units b
		WHERE b.organization_id = g_org_id;
	EXCEPTION WHEN OTHERS THEN
		v_org_name	:=	NULL;
	END;
	-- Format Header
	FOR i IN 1..50
	LOOP
		t_format(i)	:=	0;
	END LOOP;
	tac_text_output_util.set_format
		(it_col_len		=>	t_format
		,o_status		=>	o_status
		,i_clear_data	=>	'Y');
	IF o_status <> tac_text_output_util.NO_ERROR THEN
		RAISE e_abnormal_end;
	END IF;
	-- Write Parameters
	t_text(1)	:= 'Parameters : ';
	tac_text_output_util.add_txt(it_message	=>	t_text);
	t_text(1)	:= '1. Organization : ';
	t_text(2)	:= v_org_name||' ('||g_org_id||')';
	tac_text_output_util.add_txt(it_message	=>	t_text);
	t_text(1)	:= '2. Tax Period From : ';
	t_text(2)	:= P_TAX_PERIOD_FROM;
	tac_text_output_util.add_txt(it_message	=>	t_text);
	t_text(1)	:= '3. Tax Period To : ';
	t_text(2)	:= P_TAX_PERIOD_TO;
	tac_text_output_util.add_txt(it_message	=>	t_text);
	t_text(1)	:= '4. VAT Tax Code : ';
	t_text(2)	:= P_VAT_TAX_CODE;
	tac_text_output_util.add_txt(it_message	=>	t_text);
	t_text(1)	:= '5. From Invoice Date : ';
	t_text(2)	:= P_FROM_INVOICE_DATE;
	tac_text_output_util.add_txt(it_message	=>	t_text);
	t_text(1)	:= '6. Default Submit Period (case Submit# > 0) : ';
	t_text(2)	:= P_DEFAULT_SUBMIT_PERIOD;
	tac_text_output_util.add_txt(it_message	=>	t_text);
	t_text(1)	:= '7. Show Unaccounted Invoices? : ';
	t_text(2)	:= P_SHOW_UNACCT_INV;
	tac_text_output_util.add_txt(it_message	=>	t_text);
	-- Write Header
	t_text(1)	:=	'No.';
	t_text(2)	:=	'Pay Group';
	t_text(3)	:=	'Liability Acct';
	t_text(4)	:=	'Invoice Date';
	t_text(5)	:=	'AP Voucher';
	t_text(6)	:=	'Invoice Num';
	t_text(7)	:=	'Description';
	t_text(8)	:=	'CMS Linkage Value';
	t_text(9)	:=	'Status';
	t_text(10)	:=	'Posting Status';
	t_text(11)	:=	'Invoice Amt';
	t_text(12)	:=	'Vendor Num';
	t_text(13)	:=	'Vendor Name';
	t_text(14)	:=	'Requester';
	t_text(15)	:=	'Division';
	t_text(16)	:=	'AP''s GL Date';
	t_text(17)	:=	'Month';
	t_text(18)	:=	'Year';
	t_text(19)	:=	'Line''s GL Date';
	t_text(20)	:=	'Month';
	t_text(21)	:=	'Year';
	t_text(22)	:=	'WHT Group';
	t_text(23)	:=	'Base Amt';
	t_text(24)	:=	'WHT Amt';
	t_text(25)	:=	'Tax Amt';
	t_text(26)	:=	'Tax Code';
	t_text(27)	:=	'No.';
	t_text(28)	:=	'Payment Date';
	t_text(29)	:=	'Payment Voucher';
	t_text(30)	:=	'Document Number';
	t_text(31)	:=	'Status';
	t_text(32)	:=	'Posting Status';
	t_text(33)	:=	'Paid Amt';
	t_text(34)	:=	'No.';
	t_text(35)	:=	'Tax Code';
	t_text(36)	:=	'Base Amt';
	t_text(37)	:=	'Non-Rec Amt';
	t_text(38)	:=	'Rec Amt';
	t_text(39)	:=	'No.';
	t_text(40)	:=	'Legal Loc.';
	t_text(41)	:=	'Cat. Code';
	t_text(42)	:=	'Tax Invoice No.';
	t_text(43)	:=	'Tax Invoice Date';
	t_text(44)	:=	'Tax Period';
	t_text(45)	:=	'Submit#';
	t_text(46)	:=	'Submit Period';
	t_text(47)	:=	'Base Amt';
	t_text(48)	:=	'Tax Amt';
	t_text(49)	:=	'Last Update Date';
	t_text(50)	:=	'Last Update By';
	tac_text_output_util.add_txt(it_message	=>	t_text);

	-- Format Line
	t_format(1)		:=	-1;		--Invoice_seq
	t_format(2)		:=	0;		--Invoice_Pay_Group
	t_format(3)		:=	0;		--Invoice_Liability_Acct
	t_format(4)		:=	-1;		--Invoice_Date
	t_format(5)		:=	0;		--Invoice_AP_Voucher
	t_format(6)		:=	0;		--Invoice_Num
	t_format(7)		:=	0;		--Invoice_Description
	t_format(8)		:=	0;		--Invoice_CMS_Linkage_Value
	t_format(9)		:=	0;		--Invoice_Status
	t_format(10)	:=	0;		--Invoice_Posting_Status
	t_format(11)	:=	-1;		--Invoice_Amt
	t_format(12)	:=	0;		--Invoice_Vendor_Num
	t_format(13)	:=	0;		--Invoice_Vendor_Name
	t_format(14)	:=	0;		--Invoice_Requester
	t_format(15)	:=	0;		--Invoice_Division
	t_format(16)	:=	-1;		--Invoice_AP_GL_Date
	t_format(17)	:=	-1;		--Invoice_AP_GL_Month
	t_format(18)	:=	-1;		--Invoice_AP_GL_Year
	t_format(19)	:=	-1;		--Invoice_Line_GL_Date
	t_format(20)	:=	-1;		--Invoice_Line_GL_Month
	t_format(21)	:=	-1;		--Invoice_Line_GL_Year
	t_format(22)	:=	0;		--Invoice_WHT_Group
	t_format(23)	:=	-1;		--Invoice_Base_Amt
	t_format(24)	:=	-1;		--Invoice_WHT_Amt
	t_format(25)	:=	-1;		--Invoice_Tax_Amt
	t_format(26)	:=	0;		--Invoice_Tax_Code
	t_format(27)	:=	-1;		--Payment_Seq
	t_format(28)	:=	-1;		--Payment_Date
	t_format(29)	:=	0;		--Payment_Voucher
	t_format(30)	:=	0;		--Payment_Document_Number
	t_format(31)	:=	0;		--Payment_Status
	t_format(32)	:=	0;		--Payment_Posting_Status
	t_format(33)	:=	-1;		--Payment_Paid_Amt
	t_format(34)	:=	-1;		--TTH_Seq
	t_format(35)	:=	0;		--TTH_Tax_Code
	t_format(36)	:=	-1;		--TTH_Base_Amt
	t_format(37)	:=	-1;		--TTH_NonRec_Amt
	t_format(38)	:=	-1;		--TTH_Rec_Amt
	t_format(39)	:=	-1;		--TTL_Seq
	t_format(40)	:=	0;		--TTL_Legal_Loc
	t_format(41)	:=	0;		--TTL_Cat_Code
	t_format(42)	:=	0;		--TTL_Tax_Invoice_No
	t_format(43)	:=	-1;		--TTL_Tax_Invoice_Date
	t_format(44)	:=	0;		--TTL_Tax_Period
	t_format(45)	:=	-1;		--TTL_Submit
	t_format(46)	:=	0;		--TTL_Submit_Period
	t_format(47)	:=	-1;		--TTL_Base_Amt
	t_format(48)	:=	-1;		--TTL_Tax_Amt
	t_format(49)	:=	-1;		--TTL_Last_Update_Date
	t_format(50)	:=	0;		--TTL_Last_Update_By
	tac_text_output_util.set_format
		(it_col_len		=>	t_format
		,o_status		=>	o_status
		,i_clear_data	=>	'N');
	IF o_status <> tac_text_output_util.NO_ERROR THEN
		RAISE e_abnormal_end;
	END IF;
	-- Write Line
	v_invoice_seq	:=	0;
	FOR r_trx IN c_trx LOOP
		t_text.DELETE;
		-- ����¹ Invoice
		IF r_trx.Invoice_id <> v_prev_invoice_id THEN
			v_invoice_seq	:=	v_invoice_seq + 1;
			v_payment_seq	:=	1;
			v_tth_seq		:=	1;
			v_ttl_seq		:=	1;
			t_text(11)	:=	r_trx.Invoice_Amt;
			t_text(23)	:=	r_trx.Invoice_Base_Amt;
			t_text(24)	:=	r_trx.Invoice_WHT_Amt;
			t_text(25)	:=	r_trx.Invoice_Tax_Amt;
			t_text(33)	:=	r_trx.Payment_Paid_Amt;
			t_text(36)	:=	r_trx.TTH_Base_Amt;
			t_text(37)	:=	r_trx.TTH_NonRec_Amt;
			t_text(38)	:=	r_trx.TTH_Rec_Amt;
		ELSE
			t_text(11)	:=	NULL;
			IF r_trx.Invoice_Dist_Id <> v_prev_invoice_Dist_id THEN
				v_payment_seq	:=	1;
				v_tth_seq		:=	1;
				v_ttl_seq		:=	1;
				t_text(23)	:=	r_trx.Invoice_Base_Amt;
				t_text(24)	:=	r_trx.Invoice_WHT_Amt;
				t_text(25)	:=	r_trx.Invoice_Tax_Amt;
				t_text(33)	:=	r_trx.Payment_Paid_Amt;
				t_text(36)	:=	r_trx.TTH_Base_Amt;
				t_text(37)	:=	r_trx.TTH_NonRec_Amt;
				t_text(38)	:=	r_trx.TTH_Rec_Amt;
			ELSE
				t_text(23)	:=	NULL;
				t_text(24)	:=	NULL;
				t_text(25)	:=	NULL;
				IF r_trx.Invoice_Payment_Id <> v_prev_invoice_payment_id THEN
					v_payment_seq	:=	v_payment_seq+1;
					v_tth_seq		:=	1;
					v_ttl_seq		:=	1;
					t_text(33)	:=	r_trx.Payment_Paid_Amt;
					t_text(36)	:=	r_trx.TTH_Base_Amt;
					t_text(37)	:=	r_trx.TTH_NonRec_Amt;
					t_text(38)	:=	r_trx.TTH_Rec_Amt;
				ELSE
					t_text(33)	:=	NULL;
					IF r_trx.TTH_id <> v_prev_tth_id THEN
						v_tth_seq	:=	v_tth_seq+1;
						v_ttl_seq	:=	1;
						t_text(36)	:=	r_trx.TTH_Base_Amt;
						t_text(37)	:=	r_trx.TTH_NonRec_Amt;
						t_text(38)	:=	r_trx.TTH_Rec_Amt;
					ELSE
						v_ttl_seq	:=	v_ttl_seq+1;
						t_text(36)	:=	NULL;
						t_text(37)	:=	NULL;
						t_text(38)	:=	NULL;
					END IF;
				END IF;
			END IF;
		END IF;

		t_text(1)	:=	v_invoice_seq;
		t_text(2)	:=	r_trx.Invoice_Pay_Group;
		t_text(3)	:=	r_trx.Invoice_Liability_Acct;
		t_text(4)	:=	r_trx.Invoice_Date;
		t_text(5)	:=	r_trx.Invoice_AP_Voucher;
		t_text(6)	:=	r_trx.Invoice_Num;
		t_text(7)	:=	r_trx.Invoice_Description;
		t_text(8)	:=	r_trx.Invoice_CMS_Linkage_Value;
		t_text(9)	:=	r_trx.Invoice_Status;
		t_text(10)	:=	r_trx.Invoice_Posting_Status;
		t_text(12)	:=	r_trx.Invoice_Vendor_Num;
		t_text(13)	:=	r_trx.Invoice_Vendor_Name;
		t_text(14)	:=	r_trx.Invoice_Requester;
		t_text(15)	:=	r_trx.Invoice_Division;
		t_text(16)	:=	r_trx.Invoice_AP_GL_Date;
		t_text(17)	:=	r_trx.Invoice_AP_GL_Month;
		t_text(18)	:=	r_trx.Invoice_AP_GL_Year;
		t_text(19)	:=	r_trx.Invoice_Line_GL_Date;
		t_text(20)	:=	r_trx.Invoice_Line_GL_Month;
		t_text(21)	:=	r_trx.Invoice_Line_GL_Year;
		t_text(22)	:=	r_trx.Invoice_WHT_Group;
		t_text(26)	:=	r_trx.Invoice_Tax_Code;
		IF r_trx.Payment_Document_Number IS NULL THEN
			t_text(27)	:= 	NULL;
			t_text(28)	:=	NULL;
			t_text(29)	:=	NULL;
			t_text(30)	:=	NULL;
			t_text(31)	:=	NULL;
			t_text(32)	:=	NULL;
		ELSE
			t_text(27)	:=	v_payment_seq;
			t_text(28)	:=	r_trx.Payment_Date;
			t_text(29)	:=	r_trx.Payment_Voucher;
			t_text(30)	:=	r_trx.Payment_Document_Number;
			t_text(31)	:=	r_trx.Payment_Status;
			t_text(32)	:=	r_trx.Payment_Posting_Status;
		END IF;

		IF r_trx.TTH_Tax_Code IS NULL THEN
			t_text(34)	:= 	NULL;
			t_text(35)	:=	NULL;
		ELSE
			t_text(34)	:=	v_tth_seq;
			t_text(35)	:=	r_trx.TTH_Tax_Code;
		end IF;

		IF r_trx.TTL_Legal_Loc IS NULL THEN
			t_text(39)	:= 	NULL;
			t_text(40)	:=	NULL;
			t_text(41)	:=	NULL;
			t_text(42)	:=	NULL;
			t_text(43)	:=	NULL;
			t_text(44)	:=	NULL;
			t_text(45)	:=	NULL;
			t_text(46)	:=	NULL;
			t_text(47)	:=	NULL;
			t_text(48)	:=	NULL;
			t_text(49)	:=	NULL;
			t_text(50)	:=	NULL;
		ELSE
			t_text(39)	:=	v_ttl_seq;
			t_text(40)	:=	r_trx.TTL_Legal_Loc;
			t_text(41)	:=	r_trx.TTL_Cat_Code;
			t_text(42)	:=	r_trx.TTL_Tax_Invoice_No;
			t_text(43)	:=	r_trx.TTL_Tax_Invoice_Date;
			t_text(44)	:=	r_trx.TTL_Tax_Period;
			t_text(45)	:=	r_trx.TTL_Submit;
			t_text(46)	:=	r_trx.TTL_Submit_Period;
			t_text(47)	:=	r_trx.TTL_Base_Amt;
			t_text(48)	:=	r_trx.TTL_Tax_Amt;
			t_text(49)	:=	r_trx.TTL_Last_Update_Date;
			t_text(50)	:=	r_trx.TTL_Last_Update_By;
		END IF;
		tac_text_output_util.add_txt(it_message	=>	t_text);

		v_prev_invoice_id			:= r_trx.Invoice_Id;
		v_prev_invoice_Dist_id		:= r_trx.Invoice_Dist_Id;
		v_prev_invoice_payment_id	:= r_trx.Invoice_Payment_Id;
		v_prev_tth_id				:= r_trx.TTH_Id;
	END LOOP;
	IF v_invoice_seq = 0 THEN
		t_text.DELETE;
		t_text(2)	:=	'*********** No Data ***********';
		tac_text_output_util.add_txt(it_message	=>	t_text);
	END IF;
	tac_text_output_util.write_output
		(o_status			=>	o_status);
EXCEPTION
	WHEN e_abnormal_end THEN
		NULL;
	WHEN OTHERS THEN
		o_status		:=	SQLERRM;
END generate_csv;
----------------------------------------------------------------------------
PROCEDURE gen_data
(P_TAX_PERIOD_FROM			IN VARCHAR2
,P_TAX_PERIOD_TO			IN VARCHAR2
,P_VAT_TAX_CODE				IN VARCHAR2
,P_FROM_INVOICE_DATE		IN VARCHAR2
,P_DEFAULT_SUBMIT_PERIOD	IN VARCHAR2
,P_SHOW_UNACCT_INV			IN VARCHAR2
) IS
	v_from_invoice_date		DATE :=	TRUNC(TO_DATE(P_FROM_INVOICE_DATE,'YYYY/MM/DD HH24:MI:SS'));
	v_period_from_date		DATE := TO_DATE('1-'||REPLACE(P_TAX_PERIOD_FROM,'ADJ','DEC'),'DD-MON-YY');
	v_period_end_date		DATE := LAST_DAY(TO_DATE('1-'||REPLACE(P_TAX_PERIOD_TO,'ADJ','DEC'),'DD-MON-YY'));
BEGIN
	write_log('Parameters');
	write_log('----------');
	write_log('P_TAX_PERIOD_FROM : '||P_TAX_PERIOD_FROM);
	write_log('P_TAX_PERIOD_TO : '||P_TAX_PERIOD_TO);
	write_log('P_VAT_TAX_CODE : '||P_VAT_TAX_CODE);
	write_log('P_FROM_INVOICE_DATE : '||P_FROM_INVOICE_DATE);
	write_log('P_DEFAULT_SUBMIT_PERIOD : '||P_DEFAULT_SUBMIT_PERIOD);
	write_log('P_SHOW_UNACCT_INV : '||P_SHOW_UNACCT_INV);
	write_log('org_id : '||g_org_id);
	write_log('period_from_date : '||v_period_from_date);
	write_log('period_end_date : '||v_period_end_date);
	write_log(' ');
	-- Clear data
	DELETE tac_thinput_vat_rec_inv WHERE process_id = g_request_id;
	DELETE tac_thinput_vat_rec_pm WHERE process_id = g_request_id;
	DELETE tac_thinput_vat_rec_tth WHERE process_id = g_request_id;

	-- Invoice Info --------------------------------------------------------------------------------------------
	write_log('     Get Invoice Info : '||to_char(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
	INSERT /*+ append nologging */ INTO tac_thinput_vat_rec_inv
		(process_id,
		invoice_id,
		pay_group,
		liability_acct,
		invoice_date,
		ap_voucher,
		invoice_num,
		description,
		CMS_Linkage_Value,
		invoice_status,
		posting_status,
		vendor_num,
		vendor_name,
		vendor_site,
		vendor_branch,
		requester,
		division,
		ap_gl_date,
		ap_gl_month,
		ap_gl_year,		
		line_gl_date, 
		line_gl_month, 
		line_gl_year, 
		wht_group,
		type_tax,
		tax_code,
		Invoice_Amt,
		Base_Amt,
		Vat_Tax_Amt,
		Vat_Base_Amt,
		Invoice_WHT_Amt,
		doc_category_code,
		Invoice_Dist_Id)
	SELECT /*+ RULE */ 
		g_request_id
		,ai.invoice_id									Invoice_Id
		,decode( upper(ai.pay_group_lookup_code), 'EMPLOYEE', 'NON-TRADE', 'PETTY CASH', 'NON-TRADE', 'EXPENSE', 'NON-TRADE', ai.pay_group_lookup_code)				Invoice_Pay_Group
		,gcc.concatenated_segments					Invoice_Liability_Acct
		,ai.invoice_date							Invoice_Date
		,nvl(ai.doc_sequence_value, ai.voucher_num)						Invoice_AP_Voucher
		,ai.invoice_num 							Invoice_Num
		,MAX(aid.description) 						description
		,aid.global_attribute3						CMS_Linkage_Value
		,ap_invoices_pkg.get_approval_status(ai.invoice_id, ai.invoice_amount, ai.payment_status_flag, ai.invoice_type_lookup_code)							Invoice_Status
		,decode(ap_invoices_pkg.get_posting_status(ai.invoice_id),'Y','Yes','N','No','Partial')								Invoice_Posting_Status
		,NVL(pvd.segment1,pv.segment1)				Invoice_Vendor_Num
		,NVL(pvd.vendor_name,pv.vendor_name)		Invoice_Vendor_Name
		,CASE aid.attribute4 WHEN NULL THEN pvs.vendor_site_code
							ELSE CASE aid.attribute5 WHEN NULL THEN '-' ELSE aid.attribute5 END END Invoice_Vendor_Site
		,NVL(CASE aid.attribute4 WHEN NULL THEN pvs.global_attribute14 ELSE
			CASE aid.attribute5 WHEN NULL THEN NULL ELSE 
				(SELECT pvsd.global_attribute14
	  			FROM po_vendor_sites_all pvsd
	 			WHERE pvsd.vendor_id = pvd.vendor_id
	   			AND pvsd.vendor_site_code = aid.attribute5
	   			AND pvsd.org_id = 102) END END, '�ӹѡ�ҹ�˭�')	Invoice_Vendor_Branch
		,(SELECT full_name
		FROM per_all_people_f pap
		WHERE person_id = ai.requester_id
		AND rownum = 1)							Invoice_Requester  --ai.requester_id
		,NULL									Invoice_Division
		,ai.gl_date								Invoice_AP_GL_Date
		,TO_CHAR(ai.gl_date,'MM')				Invoice_AP_GL_Month
		,TO_CHAR(ai.gl_date,'YYYY')				Invoice_AP_GL_Year
		,aid.accounting_date					Invoice_Line_GL_Date
		,TO_CHAR(aid.accounting_date,'MM')		Invoice_Line_GL_Month
		,TO_CHAR(aid.accounting_date,'YYYY')	Invoice_Line_GL_Year
		,awt.name								Invoice_WHT_Group
		,DECODE(atc.global_attribute20,'N','G','Y','S',atc.global_attribute20)		Invoice_Type_Tax
		,atc.name 								Invoice_Tax_Code
		,(SELECT sum(aidt.amount)
		FROM ap_invoice_distributions aidt
		WHERE ai.invoice_id = aidt.invoice_id
		AND aidt.line_type_lookup_code IN ('ITEM','TAX'))	Invoice_Amt
		,sum(aid.amount) 	 						Base_Amt
		,(SELECT sum(aidt.amount)
		FROM ap_invoice_distributions aidt
			,ap_tax_codes atct
		WHERE ai.invoice_id = aidt.invoice_id
		AND aidt.line_type_lookup_code = 'TAX'
		AND aidt.tax_code_id = atct.tax_id
		AND atct.global_attribute20 = atc.global_attribute20
		AND atct.name = atc.name)	Vat_Tax_Amt
		,(SELECT sum(aidt.amount)
		FROM ap_invoice_distributions aidt
			,ap_tax_codes atct
		WHERE ai.invoice_id = aidt.invoice_id
		AND aidt.line_type_lookup_code = 'ITEM'
		AND aidt.tax_code_id = atct.tax_id
		AND atct.global_attribute20 = atc.global_attribute20
		AND atct.name = atc.name)	Vat_Base_Amt
		,(SELECT sum(aidt.amount)
		FROM ap_invoice_distributions aidt
			,ap_tax_codes atct
		WHERE aidt.invoice_id = ai.invoice_id
		AND aidt.line_type_lookup_code = 'AWT'
		AND aidt.tax_code_id = atct.tax_id
		AND atct.name = awt.name)	WHT_Amt
		,ai.Doc_Category_Code
		,MIN(aid.invoice_distribution_id)
	FROM ap_invoices_all ai
		,ap_invoice_distributions_all aid
		,ap_tax_codes atc
		,po_vendors pv
		,po_vendors pvd
		,gl_code_combinations_kfv gcc
		,ap_awt_groups awt
		,po_vendor_sites_all pvs
	WHERE ai.invoice_id = aid.invoice_id
	AND aid.line_type_lookup_code = 'ITEM'
	AND aid.tax_code_id = atc.tax_id
	AND ai.vendor_id = pv.vendor_id
	AND pvd.segment1(+) = aid.attribute4
	AND gcc.code_combination_id = ai.accts_pay_code_combination_id
	AND aid.awt_group_id = awt.group_id(+)
	AND pvs.vendor_site_id = ai.vendor_site_id
	AND pvs.org_id = ai.org_id
	AND ai.org_id = g_org_id
	AND ai.gl_date >= v_from_invoice_date
	AND ai.gl_date <= v_period_end_date
	AND (P_SHOW_UNACCT_INV = 'Y'
	OR (P_SHOW_UNACCT_INV = 'N' AND ap_invoices_pkg.get_posting_status(ai.invoice_id) <> 'N'))
	AND (P_VAT_TAX_CODE IS NULL 
	OR (P_VAT_TAX_CODE = 'S-VAT' AND atc.global_attribute20 = 'Y')
	OR (P_VAT_TAX_CODE = 'G-VAT' AND atc.global_attribute20 = 'N'))
	GROUP BY
		ai.invoice_id
		,decode( upper(ai.pay_group_lookup_code), 'EMPLOYEE', 'NON-TRADE', 'PETTY CASH', 'NON-TRADE', 'EXPENSE', 'NON-TRADE', ai.pay_group_lookup_code)
		,gcc.concatenated_segments
		,ai.invoice_date
		,nvl(ai.doc_sequence_value, ai.voucher_num)
		,ai.invoice_num
		,aid.global_attribute3
		,ai.invoice_amount
		,ai.payment_status_flag
		,ai.invoice_type_lookup_code
		,NVL(pvd.segment1,pv.segment1)
		,NVL(pvd.vendor_name,pv.vendor_name)
		,ai.requester_id
		,ai.gl_date
		,aid.accounting_date
		,TO_CHAR(aid.accounting_date,'MM')
		,TO_CHAR(aid.accounting_date,'YYYY')
		,awt.name
		,atc.name
		,atc.global_attribute20
		,ai.Doc_Category_Code
		,pvd.vendor_id,aid.attribute5,pvs.vendor_site_code
		,aid.attribute4,pvs.global_attribute14;
	COMMIT;
	-- Update Division
	write_log('     Update Division : '||to_char(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
	FOR rec IN (SELECT ai.invoice_id,ai.ap_gl_date,prh.attribute2 division
				FROM ap_invoice_distributions_all aid
					,po_distributions_all pod
					,po_req_distributions_all prd
					,po_requisition_lines_all prl
					,po_requisition_headers_all prh
					,tac_thinput_vat_rec_inv ai
				WHERE aid.po_distribution_id = pod.po_distribution_id
				AND pod.req_distribution_id  = prd.distribution_id
				AND prd.requisition_line_id  = prl.requisition_line_id
				AND prl.requisition_header_id = prh.requisition_header_id
				AND aid.invoice_id 		= ai.invoice_id
				AND aid.accounting_date = ai.ap_gl_date
				AND ai.process_id = g_request_id
				GROUP BY ai.invoice_id,ai.ap_gl_date,prh.attribute2)
	LOOP
		UPDATE tac_thinput_vat_rec_inv
		SET division = DECODE(division,NULL,rec.division,division||','||rec.division)
		WHERE invoice_id = rec.invoice_id
		AND ap_gl_date = rec.ap_gl_date
		AND process_id = g_request_id;
	END LOOP;
	COMMIT;
	-- Calc Amount -------------------------------------
	UPDATE tac_thinput_vat_rec_inv
	SET tax_amt = DECODE(vat_base_amt,0,0,ROUND((base_amt*vat_tax_amt/vat_base_amt),2))
		,wht_amt = DECODE(vat_base_amt,0,0,ROUND((base_amt*invoice_wht_amt/vat_base_amt),2))
	WHERE process_id = g_request_id;
	COMMIT;
	-- Payment Info --------------------------------------------------------------------------------------------
	write_log('     Get Payment Info : '||to_char(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
	INSERT /*+ append nologging */ INTO tac_thinput_vat_rec_pm
		(process_id,
		invoice_id,
		pay_group,
		liability_acct,
		invoice_date,
		ap_voucher,
		invoice_num,
		description,
		CMS_Linkage_Value,
		invoice_status,
		posting_status,
		vendor_num,
		vendor_name, 
		vendor_site,
		Vendor_Branch,
		requester,
		division,
		ap_gl_date,
		ap_gl_month,
		ap_gl_year,
		line_gl_date, 
		line_gl_month, 
		line_gl_year, 
		wht_group,
		type_tax,
		tax_code,
		Invoice_Amt,
		Base_Amt,
		Vat_Tax_Amt,
		Vat_Base_Amt,
		Invoice_WHT_Amt,
		Tax_Amt,
		WHT_Amt,
		Payment_Id,
		Payment_Date,
		Payment_Voucher,
		Payment_Document_Number,
		Payment_Status,
		Payment_Status_Disp,
		Payment_Posting_Status,
		Payment_Amt,
		Payment_Base_Amt,
		Doc_Category_Code,
		Invoice_Dist_Id,
		Invoice_Payment_Id
		)
	SELECT /*+ RULE */ 
		ai.process_id,
		ai.invoice_id,
		ai.pay_group,
		ai.liability_acct,
		ai.invoice_date,
		ai.ap_voucher,
		ai.invoice_num,
		ai.description,
		ai.cms_linkage_value,
		ai.invoice_status,
		ai.posting_status,
		ai.vendor_num,
		ai.vendor_name,
		ai.vendor_site,
		ai.Vendor_Branch,
		ai.requester,
		ai.division,
		ai.ap_gl_date,
		ai.ap_gl_month,
		ai.ap_gl_year,
		ai.line_gl_date, 
		ai.line_gl_month, 
		ai.line_gl_year, 
		ai.wht_group,
		ai.type_tax,
		ai.tax_code,
		ai.Invoice_Amt,
		ai.Base_Amt,
		ai.Vat_Tax_Amt,
		ai.Vat_Base_Amt,
		ai.Invoice_WHT_Amt,
		ai.Tax_Amt,
		ai.WHT_Amt
		,ac.check_id				Payment_Id
		,ac.check_date				Payment_Date
		,ac.doc_sequence_value		Payment_Voucher
		,ac.check_number			Payment_Document_Number
		,ac.status_lookup_code		Payment_Status
		,pm_sts.displayed_field		Payment_Status_Disp
		,decode(ap_checks_pkg.get_posting_status(ac.check_id),'Y','Yes','N','No','Partial')	Payment_Posting_Status
		,aip.amount					Payment_Amt
		,DECODE((vat_base_amt+vat_tax_amt),0,0,ROUND(((base_amt+tax_amt)*aip.amount/(vat_base_amt+vat_tax_amt)),2))	Payment_Base_Amt
		,ai.Doc_Category_Code
		,ai.Invoice_Dist_Id
		,aip.Invoice_Payment_Id
	FROM tac_thinput_vat_rec_inv ai
		,ap_invoice_payments_all aip
		,ap_checks_all ac
		,ap_lookup_codes  pm_sts
	WHERE ai.process_id = g_request_id
	AND ai.invoice_id = aip.invoice_id
	AND aip.check_id = ac.check_id
	AND pm_sts.lookup_type(+) = 'CHECK STATE'
	AND pm_sts.lookup_code(+) = ac.status_lookup_code
	AND ac.check_date <= v_period_end_date
	AND (ai.invoice_status <> 'CANCELLED'
	OR (ai.invoice_status = 'CANCELLED' AND ac.status_lookup_code <> 'VOIDED'));
	COMMIT;
	-- Not Payment -----------------------------------
	write_log('     Get Not Payment : '||to_char(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
	INSERT /*+ append nologging */ INTO tac_thinput_vat_rec_pm
		(process_id,
		invoice_id,
		pay_group,
		liability_acct,
		invoice_date,
		ap_voucher,
		invoice_num,
		description,
		CMS_Linkage_Value,
		invoice_status,
		posting_status,
		vendor_num,
		vendor_name,
		vendor_site,
		vendor_branch,
		requester,
		division,
		ap_gl_date,
		ap_gl_month,
		ap_gl_year,
		line_gl_date, 
		line_gl_month, 
		line_gl_year, 
		wht_group,
		type_tax,
		tax_code,
		Invoice_Amt,
		Base_Amt,
		Vat_Tax_Amt,
		Vat_Base_Amt,
		Invoice_WHT_Amt,
		Tax_Amt,
		WHT_Amt,
		Payment_Id,
		Payment_Date,
		Payment_Voucher,
		Payment_Document_Number,
		Payment_Status,
		Payment_Status_Disp,
		Payment_Posting_Status,
		Payment_Amt,
		Payment_Base_Amt,
		Doc_Category_Code,
		Invoice_Dist_Id,
		Invoice_Payment_Id
		)
	SELECT /*+ RULE */ 
		ai.process_id,
		ai.invoice_id,
		ai.pay_group,
		ai.liability_acct,
		ai.invoice_date,
		ai.ap_voucher,
		ai.invoice_num,
		ai.description,
		ai.cms_linkage_value,
		ai.invoice_status,
		ai.posting_status,
		ai.vendor_num,
		ai.vendor_name,
		ai.vendor_site,
		ai.vendor_branch,
		ai.requester,
		ai.division,
		ai.ap_gl_date,
		ai.ap_gl_month,
		ai.ap_gl_year,
		ai.line_gl_date, 
		ai.line_gl_month, 
		ai.line_gl_year, 
		ai.wht_group,
		ai.type_tax,
		ai.tax_code,
		ai.Invoice_Amt,
		ai.Base_Amt,
		ai.Vat_Tax_Amt,
		ai.Vat_Base_Amt,
		ai.Invoice_WHT_Amt,
		ai.Tax_Amt,
		ai.WHT_Amt
		,NULL	Payment_Id
		,NULL	Payment_Date
		,NULL	Payment_Voucher
		,NULL	Payment_Document_Number
		,NULL	Payment_Status
		,NULL	Payment_Status_Disp
		,NULL	Payment_Posting_Status
		,NULL	Payment_Amt
		,NULL	Payment_Base_Amt
		,ai.Doc_Category_Code
		,ai.Invoice_Dist_Id
		,NULL	Invoice_Payment_Id
	FROM tac_thinput_vat_rec_inv ai
	WHERE ai.process_id = g_request_id
	AND NOT EXISTS (SELECT  /*+ RULE */ 'x'
					FROM tac_thinput_vat_rec_pm aip
					WHERE ai.invoice_id = aip.invoice_id
					AND aip.process_id = g_request_id);
	COMMIT;
	-- Tax Invoice --------------------------------------------------------------------------------------------
	IF NVL(P_VAT_TAX_CODE,'G-VAT') = 'G-VAT' THEN
		write_log('     Get Tax Invoice - G-Vat : '||to_char(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
		-- G-VAT ---------------------------------------
		INSERT /*+ append nologging */ INTO tac_thinput_vat_rec_tth
			(process_id,
			invoice_id,
			pay_group,
			liability_acct,
			invoice_date,
			ap_voucher,
			invoice_num,
			description,
			CMS_Linkage_Value,
			invoice_status,
			posting_status,
			vendor_num,
			vendor_name,
			vendor_site,
			vendor_branch,
			requester,
			division,
			ap_gl_date,
			ap_gl_month,
			ap_gl_year,
			line_gl_date, 
			line_gl_month, 
			line_gl_year, 
			wht_group,
			type_tax,
			tax_code,
			Invoice_Amt,
			Base_Amt,
			Vat_Tax_Amt,
			Vat_Base_Amt,
			Invoice_WHT_Amt,
			Tax_Amt,
			WHT_Amt,
			Payment_Id,
			Payment_Date,
			Payment_Voucher,
			Payment_Document_Number,
			Payment_Status,
			Payment_Status_Disp,
			Payment_Posting_Status,
			Payment_Amt,
			Payment_Base_Amt,
			Doc_Category_Code,
			Invoice_Dist_Id,
			Invoice_Payment_Id,
			TTH_Id,
			TTH_Tax_Code,
			TTH_Base_Amt,
			TTH_NonRec_Amt,
			TTH_Rec_Amt,
			TTL_Id,
			TTL_Legal_Loc,
			TTL_Cat_Code,
			TTL_Tax_Invoice_No,
			TTL_Tax_Invoice_Date,
			TTL_Tax_Period,
			TTL_Submit,
			TTL_Submit_Period,
			TTL_Base_Amt,
			TTL_Tax_Amt,
			TTL_nonrec_amt,
			TTL_rec_amt,
			TTL_Last_Update_Date,
			TTL_Last_Update_By,
			TTL_Submit_Seq_Num
			)
		SELECT /*+ RULE */  
			ai.process_id,
			ai.invoice_id,
			ai.pay_group,
			ai.liability_acct,
			ai.invoice_date,
			ai.ap_voucher,
			ai.invoice_num,
			ai.description,
			ai.cms_linkage_value,
			ai.invoice_status,
			ai.posting_status,
			ai.vendor_num,
			ai.vendor_name,
			ai.vendor_site,
			ai.vendor_branch,
			ai.requester,
			ai.division,
			ai.ap_gl_date,
			ai.ap_gl_month,
			ai.ap_gl_year,
			ai.line_gl_date, 
			ai.line_gl_month, 
			ai.line_gl_year, 
			ai.wht_group,
			ai.type_tax,
			ai.tax_code,
			ai.Invoice_Amt,
			ai.Base_Amt,
			ai.Vat_Tax_Amt,
			ai.Vat_Base_Amt,
			ai.Invoice_WHT_Amt,
			ai.Tax_Amt,
			ai.WHT_Amt,
			ai.Payment_Id,
			ai.Payment_Date,
			ai.Payment_Voucher,
			ai.Payment_Document_Number,
			ai.Payment_Status,
			ai.Payment_Status_Disp,
			ai.Payment_Posting_Status,
			ai.Payment_Amt,
			ai.Payment_Base_Amt,
			ai.Doc_Category_Code,
			ai.Invoice_Dist_Id,
			ai.Invoice_Payment_Id
			,tth.th_tih_id				TTH_Id
			,tth.th_tih_tax_code		TTH_Tax_Code
			,tth.th_tih_base_amt		TTH_Base_Amt
			,tth.th_tih_non_rec_amt		TTH_NonRec_Amt
			,tth.th_tih_rec_amt			TTH_Rec_Amt
			,ttl.th_til_id				TTL_Id
			,(SELECT location_code FROM hr_locations h WHERE h.location_id = ttl.th_til_location_id)	TTL_Legal_Loc
			,ai.Doc_Category_Code	TTL_Cat_Code
			,ttl.th_til_tax_inv_number	TTL_Tax_Invoice_No
			,ttl.th_til_tax_inv_date	TTL_Tax_Invoice_Date
			,ttl.th_til_period_name		TTL_Tax_Period
			,ttl.submit_num				TTL_Submit
			,ttl.submit_period_name		TTL_Submit_Period
			,ttl.th_til_base_amt		TTL_Base_Amt
			,ttl.th_til_tax_amt			TTL_Tax_Amt
			,ttl.th_til_non_rec_amt		TTL_non_rec_amt
			,ttl.th_til_rec_amt			TTL_rec_Amt
			,ttl.released_date			TTL_Last_Update_Date
			,(SELECT user_name FROM Fnd_user WHERE user_id = ttl.released_by)	TTL_Last_Update_By
			,ttl.submit_seq_num			TTL_Submit_Seq_Num
		FROM tac_thinput_vat_rec_pm ai
			,th_taxinvoices_header tth
			,th_taxinvoices_line ttl
		WHERE ai.process_id = g_request_id
		AND ai.type_tax = 'G'
		AND ai.type_tax = tth.th_tih_type_tax
		AND ai.tax_code = tth.th_tih_tax_code
		AND ai.invoice_id = tth.th_tih_transaction_id
		AND tth.th_tih_id = ttl.th_tih_id
		AND (ai.invoice_status <> 'CANCELLED'
		OR (ai.invoice_status = 'CANCELLED' AND tth.cancel_flag <> 'Y'))
		AND (ttl.th_til_period_name IS NULL
		OR	(ttl.th_til_period_name IS NOT NULL
			AND (	(NVL(ttl.submit_num,0) = 0
					AND TO_DATE('1-'||REPLACE(NVL(ttl.submit_period_name,ttl.th_til_period_name),'ADJ','DEC'),'DD-MON-YY') BETWEEN v_period_from_date AND v_period_end_date
					)
				OR (NVL(ttl.submit_num,0) <> 0)
					AND TO_DATE('1-'||REPLACE(NVL(ttl.submit_period_name,'SEP-17'),'ADJ','DEC'),'DD-MON-YY') BETWEEN v_period_from_date AND v_period_end_date
				)
			)
		);
		COMMIT;
		-- G-VAT No Tax Invoice ------------------------
		write_log('     Get Tax Invoice - G-Vat No Payment,No Tax Invoice : '||to_char(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
		INSERT /*+ append nologging */ INTO  tac_thinput_vat_rec_tth
			(process_id,
			invoice_id,
			pay_group,
			liability_acct,
			invoice_date,
			ap_voucher,
			invoice_num,
			description,
			CMS_Linkage_Value,
			invoice_status,
			posting_status,
			vendor_num,
			vendor_name,
			vendor_site,
			vendor_branch,
			requester,
			division,
			ap_gl_date,
			ap_gl_month,
			ap_gl_year,
			line_gl_date, 
			line_gl_month, 
			line_gl_year, 
			wht_group,
			type_tax,
			tax_code,
			Invoice_Amt,
			Base_Amt,
			Vat_Tax_Amt,
			Vat_Base_Amt,
			Invoice_WHT_Amt,
			Tax_Amt,
			WHT_Amt,
			Payment_Id,
			Payment_Date,
			Payment_Voucher,
			Payment_Document_Number,
			Payment_Status,
			Payment_Status_Disp,
			Payment_Posting_Status,
			Payment_Amt,
			Payment_Base_Amt,
			Doc_Category_Code,
			Invoice_Dist_Id,
			Invoice_Payment_Id,
			TTH_Id,
			TTH_Tax_Code,
			TTH_Base_Amt,
			TTH_NonRec_Amt,
			TTH_Rec_Amt,
			TTL_Id,
			TTL_Legal_Loc,
			TTL_Cat_Code,
			TTL_Tax_Invoice_No,
			TTL_Tax_Invoice_Date,
			TTL_Tax_Period,
			TTL_Submit,
			TTL_Submit_Period,
			TTL_Base_Amt,
			TTL_Tax_Amt,
			TTL_Last_Update_Date,
			TTL_Last_Update_By,
			TTL_Submit_Seq_Num
			)
		SELECT /*+ RULE */
			ai.process_id,
			ai.invoice_id,
			ai.pay_group,
			ai.liability_acct,
			ai.invoice_date,
			ai.ap_voucher,
			ai.invoice_num,
			ai.description,
			ai.cms_linkage_value,
			ai.invoice_status,
			ai.posting_status,
			ai.vendor_num,
			ai.vendor_name,
			ai.vendor_site,
			ai.vendor_branch,
			ai.requester,
			ai.division,
			ai.ap_gl_date,
			ai.ap_gl_month,
			ai.ap_gl_year,
			ai.line_gl_date, 
			ai.line_gl_month, 
			ai.line_gl_year, 
			ai.wht_group,
			ai.type_tax,
			ai.tax_code,
			ai.Invoice_Amt,
			ai.Base_Amt,
			ai.Vat_Tax_Amt,
			ai.Vat_Base_Amt,
			ai.Invoice_WHT_Amt,
			ai.Tax_Amt,
			ai.WHT_Amt,
			ai.Payment_Id,
			ai.Payment_Date,
			ai.Payment_Voucher,
			ai.Payment_Document_Number,
			ai.Payment_Status,
			ai.Payment_Status_Disp,
			ai.Payment_Posting_Status,
			ai.Payment_Amt,
			ai.Payment_Base_Amt,
			ai.Doc_Category_Code,
			ai.Invoice_Dist_Id,
			ai.Invoice_Payment_Id
			,NULL		TTH_Id
			,NULL		TTH_Tax_Code
			,NULL		TTH_Base_Amt
			,NULL		TTH_NonRec_Amt
			,NULL		TTH_Rec_Amt
			,NULL		TTL_Id
			,NULL		TTL_Legal_Loc
			,NULL		TTL_Cat_Code
			,NULL		TTL_Tax_Invoice_No
			,NULL		TTL_Tax_Invoice_Date
			,NULL		TTL_Tax_Period
			,NULL		TTL_Submit
			,NULL		TTL_Submit_Period
			,NULL		TTL_Base_Amt
			,NULL		TTL_Tax_Amt
			,NULL		TTL_Last_Update_Date
			,NULL		TTL_Last_Update_By
			,NULL		TTL_Submit_Seq_Num
		FROM tac_thinput_vat_rec_pm ai
		WHERE ai.process_id = g_request_id
		AND ai.type_tax = 'G'
		AND ai.invoice_status <> 'CANCELLED'
		AND ai.Invoice_Payment_Id IS NULL
		AND NOT EXISTS (SELECT  /*+ RULE */ 'x'
						FROM tac_thinput_vat_rec_tth tth
						WHERE ai.invoice_id = tth.invoice_id
						AND ai.type_tax = 'G'
						AND tth.process_id = g_request_id);
		COMMIT;
		write_log('     Get Tax Invoice - G-Vat No Tax Invoice : '||to_char(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
		INSERT /*+ append nologging */ INTO  tac_thinput_vat_rec_tth
			(process_id,
			invoice_id,
			pay_group,
			liability_acct,
			invoice_date,
			ap_voucher,
			invoice_num,
			description,
			CMS_Linkage_Value,
			invoice_status,
			posting_status,
			vendor_num,
			vendor_name,
			vendor_site,
			vendor_branch,
			requester,
			division,
			ap_gl_date,
			ap_gl_month,
			ap_gl_year,
			line_gl_date, 
			line_gl_month, 
			line_gl_year, 
			wht_group,
			type_tax,
			tax_code,
			Invoice_Amt,
			Base_Amt,
			Vat_Tax_Amt,
			Vat_Base_Amt,
			Invoice_WHT_Amt,
			Tax_Amt,
			WHT_Amt,
			Payment_Id,
			Payment_Date,
			Payment_Voucher,
			Payment_Document_Number,
			Payment_Status,
			Payment_Status_Disp,
			Payment_Posting_Status,
			Payment_Amt,
			Payment_Base_Amt,
			Doc_Category_Code,
			Invoice_Dist_Id,
			Invoice_Payment_Id,
			TTH_Id,
			TTH_Tax_Code,
			TTH_Base_Amt,
			TTH_NonRec_Amt,
			TTH_Rec_Amt,
			TTL_Id,
			TTL_Legal_Loc,
			TTL_Cat_Code,
			TTL_Tax_Invoice_No,
			TTL_Tax_Invoice_Date,
			TTL_Tax_Period,
			TTL_Submit,
			TTL_Submit_Period,
			TTL_Base_Amt,
			TTL_Tax_Amt,
			TTL_Last_Update_Date,
			TTL_Last_Update_By,
			TTL_Submit_Seq_Num
			)
		SELECT /*+ RULE */
			ai.process_id,
			ai.invoice_id,
			ai.pay_group,
			ai.liability_acct,
			ai.invoice_date,
			ai.ap_voucher,
			ai.invoice_num,
			ai.description,
			ai.cms_linkage_value,
			ai.invoice_status,
			ai.posting_status,
			ai.vendor_num,
			ai.vendor_name,
			ai.vendor_site,
			ai.vendor_branch,
			ai.requester,
			ai.division,
			ai.ap_gl_date,
			ai.ap_gl_month,
			ai.ap_gl_year,
			ai.line_gl_date, 
			ai.line_gl_month, 
			ai.line_gl_year, 
			ai.wht_group,
			ai.type_tax,
			ai.tax_code,
			ai.Invoice_Amt,
			ai.Base_Amt,
			ai.Vat_Tax_Amt,
			ai.Vat_Base_Amt,
			ai.Invoice_WHT_Amt,
			ai.Tax_Amt,
			ai.WHT_Amt,
			ai.Payment_Id,
			ai.Payment_Date,
			ai.Payment_Voucher,
			ai.Payment_Document_Number,
			ai.Payment_Status,
			ai.Payment_Status_Disp,
			ai.Payment_Posting_Status,
			ai.Payment_Amt,
			ai.Payment_Base_Amt,
			ai.Doc_Category_Code,
			ai.Invoice_Dist_Id,
			ai.Invoice_Payment_Id
			,NULL		TTH_Id
			,NULL		TTH_Tax_Code
			,NULL		TTH_Base_Amt
			,NULL		TTH_NonRec_Amt
			,NULL		TTH_Rec_Amt
			,NULL		TTL_Id
			,NULL		TTL_Legal_Loc
			,NULL		TTL_Cat_Code
			,NULL		TTL_Tax_Invoice_No
			,NULL		TTL_Tax_Invoice_Date
			,NULL		TTL_Tax_Period
			,NULL		TTL_Submit
			,NULL		TTL_Submit_Period
			,NULL		TTL_Base_Amt
			,NULL		TTL_Tax_Amt
			,NULL		TTL_Last_Update_Date
			,NULL		TTL_Last_Update_By
			,NULL		TTL_Submit_Seq_Num
		FROM tac_thinput_vat_rec_pm ai
		WHERE ai.process_id = g_request_id
		AND ai.type_tax = 'G'
		AND ai.invoice_status <> 'CANCELLED'
		AND ai.Invoice_Payment_Id IS NOT NULL
		AND NOT EXISTS (SELECT  /*+ RULE */ 'x'
						FROM tac_thinput_vat_rec_tth tth
						WHERE ai.Invoice_Payment_Id = tth.Invoice_Payment_Id
						AND ai.type_tax = 'G'
						AND tth.process_id = g_request_id);
		COMMIT;
	END IF;		
	-- S-VAT ---------------------------------------
	IF NVL(P_VAT_TAX_CODE,'S-VAT') = 'S-VAT' THEN
		write_log('     Get Tax Invoice - S-Vat : '||to_char(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
		INSERT /*+ append nologging */ INTO tac_thinput_vat_rec_tth
			(process_id,
			invoice_id,
			pay_group,
			liability_acct,
			invoice_date,
			ap_voucher,
			invoice_num,
			description,
			CMS_Linkage_Value,
			invoice_status,
			posting_status,
			vendor_num,
			vendor_name,
			vendor_site,
			vendor_branch,
			requester,
			division,
			ap_gl_date,
			ap_gl_month,
			ap_gl_year,
			line_gl_date, 
			line_gl_month, 
			line_gl_year, 
			wht_group,
			type_tax,
			tax_code,
			Invoice_Amt,
			Base_Amt,
			Vat_Tax_Amt,
			Vat_Base_Amt,
			Invoice_WHT_Amt,
			Tax_Amt,
			WHT_Amt,
			Payment_Id,
			Payment_Date,
			Payment_Voucher,
			Payment_Document_Number,
			Payment_Status,
			Payment_Status_Disp,
			Payment_Posting_Status,
			Payment_Amt,
			Payment_Base_Amt,
			Doc_Category_Code,
			Invoice_Dist_Id,
			Invoice_Payment_Id,
			TTH_Id,
			TTH_Tax_Code,
			TTH_Base_Amt,
			TTH_NonRec_Amt,
			TTH_Rec_Amt,
			TTL_Id,
			TTL_Legal_Loc,
			TTL_Cat_Code,
			TTL_Tax_Invoice_No,
			TTL_Tax_Invoice_Date,
			TTL_Tax_Period,
			TTL_Submit,
			TTL_Submit_Period,
			TTL_Base_Amt,
			TTL_Tax_Amt,
			TTL_nonrec_amt,
			TTL_rec_amt,
			TTL_Last_Update_Date,
			TTL_Last_Update_By,
			TTL_Submit_Seq_Num
			)
		SELECT  /*+ RULE */ 
			ai.process_id,
			ai.invoice_id,
			ai.pay_group,
			ai.liability_acct,
			ai.invoice_date,
			ai.ap_voucher,
			ai.invoice_num,
			ai.description,
			ai.cms_linkage_value,
			ai.invoice_status,
			ai.posting_status,
			ai.vendor_num,
			ai.vendor_name,
			ai.vendor_site,
			ai.vendor_branch,
			ai.requester,
			ai.division,
			ai.ap_gl_date,
			ai.ap_gl_month,
			ai.ap_gl_year,
			ai.line_gl_date, 
			ai.line_gl_month, 
			ai.line_gl_year, 
			ai.wht_group,
			ai.type_tax,
			ai.tax_code,
			ai.Invoice_Amt,
			ai.Base_Amt,
			ai.Vat_Tax_Amt,
			ai.Vat_Base_Amt,
			ai.Invoice_WHT_Amt,
			ai.Tax_Amt,
			ai.WHT_Amt,
			ai.Payment_Id,
			ai.Payment_Date,
			ai.Payment_Voucher,
			ai.Payment_Document_Number,
			ai.Payment_Status,
			ai.Payment_Status_Disp,
			ai.Payment_Posting_Status,
			ai.Payment_Amt,
			ai.Payment_Base_Amt,
			ai.Doc_Category_Code,
			ai.Invoice_Dist_Id,
			ai.Invoice_Payment_Id
			,tth.th_tih_id				TTH_Id
			,tth.th_tih_tax_code		TTH_Tax_Code
			,tth.th_tih_base_amt		TTH_Base_Amt
			,tth.th_tih_non_rec_amt		TTH_NonRec_Amt
			,tth.th_tih_rec_amt			TTH_Rec_Amt
			,ttl.th_til_id				TTL_Id
			,(SELECT location_code FROM hr_locations h WHERE h.location_id = ttl.th_til_location_id)	TTL_Legal_Loc
			,ai.Doc_Category_Code		TTL_Cat_Code
			,ttl.th_til_tax_inv_number	TTL_Tax_Invoice_No
			,ttl.th_til_tax_inv_date	TTL_Tax_Invoice_Date
			,ttl.th_til_period_name		TTL_Tax_Period
			,ttl.submit_num				TTL_Submit
			,ttl.submit_period_name		TTL_Submit_Period
			,ttl.th_til_base_amt		TTL_Base_Amt
			,ttl.th_til_tax_amt			TTL_Tax_Amt
			,ttl.th_til_non_rec_amt		TTL_nonrec_amt
			,ttl.th_til_rec_amt			TTL_rec_Amt
			,NVL(ttl.released_date,ttl.last_update_date)			TTL_Last_Update_Date
			,(SELECT user_name FROM Fnd_user WHERE user_id = NVL(ttl.released_by,ttl.last_updated_by))	TTL_Last_Update_By
			,ttl.submit_seq_num			TTL_Submit_Seq_Num
		FROM tac_thinput_vat_rec_pm ai
			,th_taxinvoices_header tth
			,th_taxinvoices_line ttl
		WHERE ai.process_id = g_request_id
		AND ai.type_tax = 'S'
		AND ai.type_tax = tth.th_tih_type_tax
		AND ai.tax_code = tth.th_tih_tax_code
		AND ai.Payment_Id = tth.th_tih_transaction_id
		AND tth.th_tih_id = ttl.th_tih_id
		AND ai.invoice_id = ttl.th_til_tax_inv_id
		AND ai.vendor_name = ttl.th_til_supplier_name
		AND ai.Vendor_Branch = ttl.attribute30
		AND (ttl.attribute28 IS NULL OR ai.vendor_site = ttl.attribute28)
		AND (ai.Payment_Status <> 'VOIDED'
		OR (ai.Payment_Status = 'VOIDED' AND tth.cancel_flag <> 'Y'
		AND ((SIGN(ai.Payment_Amt) = 1 AND SIGN(tth.th_tih_base_amt) = 1)
		OR	(SIGN(ai.Payment_Amt) <> 1 AND SIGN(tth.th_tih_base_amt) <> 1))
			))
		AND (ttl.th_til_period_name IS NULL
		OR	(ttl.th_til_period_name IS NOT NULL
			AND (	(NVL(ttl.submit_num,0) = 0
					AND TO_DATE('1-'||REPLACE(NVL(ttl.submit_period_name,ttl.th_til_period_name),'ADJ','DEC'),'DD-MON-YY') BETWEEN v_period_from_date AND v_period_end_date
					)
				OR (NVL(ttl.submit_num,0) <> 0)
					AND TO_DATE('1-'||REPLACE(NVL(ttl.submit_period_name,P_DEFAULT_SUBMIT_PERIOD),'ADJ','DEC'),'DD-MON-YY') BETWEEN v_period_from_date AND v_period_end_date
				)
			)
		);
		COMMIT;
		-- UPdate S-Vat TTH Amount
	/*	UPDATE tac_thinput_vat_rec_tth ai
		SET TTH_Base_Amt	= (SELECT SUM(TTL_Base_Amt) FROM tac_thinput_vat_rec_tth a
								WHERE a.process_id = g_request_id AND a.type_tax = 'S'
								AND ai.invoice_id = a.invoice_id AND ai.Invoice_Payment_Id = a.Invoice_Payment_Id
								AND ai.Invoice_Dist_Id = a.Invoice_Dist_Id AND ai.tth_id = a.tth_id
								)
			,TTH_NonRec_Amt	= (SELECT SUM(TTL_NonRec_Amt) FROM tac_thinput_vat_rec_tth a
								WHERE a.process_id = g_request_id AND a.type_tax = 'S'
								AND ai.invoice_id = a.invoice_id AND ai.Invoice_Payment_Id = a.Invoice_Payment_Id
								AND ai.Invoice_Dist_Id = a.Invoice_Dist_Id AND ai.tth_id = a.tth_id
								)
			,TTH_Rec_Amt	=  (SELECT SUM(TTL_Rec_Amt) FROM tac_thinput_vat_rec_tth a
								WHERE a.process_id = g_request_id AND a.type_tax = 'S'
								AND ai.invoice_id = a.invoice_id AND ai.Invoice_Payment_Id = a.Invoice_Payment_Id
								AND ai.Invoice_Dist_Id = a.Invoice_Dist_Id AND ai.tth_id = a.tth_id
								)
		WHERE ai.process_id = g_request_id
		AND ai.type_tax = 'S';
		COMMIT;
	*/
		FOR rec IN (SELECT	a.invoice_id, a.invoice_payment_id, a.invoice_dist_id, a.tth_id
							,SUM(ttl_base_amt) total_base, SUM(ttl_nonrec_amt) total_nonrec, SUM(ttl_rec_amt) total_rec
					FROM	tac_thinput_vat_rec_tth a
					WHERE	a.process_id = g_request_id
					AND		a.type_tax = 'S'
					GROUP BY a.invoice_id, a.invoice_payment_id, a.invoice_dist_id, a.tth_id)
		LOOP
			UPDATE tac_thinput_vat_rec_tth ai
			SET TTH_Base_Amt	= rec.total_base
				,TTH_NonRec_Amt	= rec.total_nonrec
				,TTH_Rec_Amt	= rec.total_rec
			WHERE	ai.process_id			=	g_request_id
			AND		ai.invoice_id			=	rec.invoice_id
			AND		ai.Invoice_Payment_Id	=	rec.Invoice_Payment_Id
			AND		ai.Invoice_Dist_Id		=	rec.Invoice_Dist_Id
			AND		ai.tth_id				=	rec.tth_id
			AND		ai.type_tax				=	'S';
			COMMIT;
		END LOOP;
		-- S-VAT No Tax Invoice ------------------------
		write_log('     Get Tax Invoice - S-Vat No Payment,No Tax Invoice : '||to_char(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
		INSERT  /*+ append nologging */ INTO tac_thinput_vat_rec_tth
			(process_id,
			invoice_id,
			pay_group,
			liability_acct,
			invoice_date,
			ap_voucher,
			invoice_num,
			description,
			CMS_Linkage_Value,
			invoice_status,
			posting_status,
			vendor_num,
			vendor_name,
			vendor_site,
			vendor_branch,
			requester,
			division,
			ap_gl_date,
			ap_gl_month,
			ap_gl_year,
			line_gl_date, 
			line_gl_month, 
			line_gl_year, 
			wht_group,
			type_tax,
			tax_code,
			Invoice_Amt,
			Base_Amt,
			Vat_Tax_Amt,
			Vat_Base_Amt,
			Invoice_WHT_Amt,
			Tax_Amt,
			WHT_Amt,
			Payment_Id,
			Payment_Date,
			Payment_Voucher,
			Payment_Document_Number,
			Payment_Status,
			Payment_Status_Disp,
			Payment_Posting_Status,
			Payment_Amt,
			Payment_Base_Amt,
			Doc_Category_Code,
			Invoice_Dist_Id,
			Invoice_Payment_Id,
			TTH_Id,
			TTH_Tax_Code,
			TTH_Base_Amt,
			TTH_NonRec_Amt,
			TTH_Rec_Amt,
			TTL_Id,
			TTL_Legal_Loc,
			TTL_Cat_Code,
			TTL_Tax_Invoice_No,
			TTL_Tax_Invoice_Date,
			TTL_Tax_Period,
			TTL_Submit,
			TTL_Submit_Period,
			TTL_Base_Amt,
			TTL_Tax_Amt,
			TTL_Last_Update_Date,
			TTL_Last_Update_By,
			TTL_Submit_Seq_Num
			)
		SELECT  /*+ RULE */ 
			ai.process_id,
			ai.invoice_id,
			ai.pay_group,
			ai.liability_acct,
			ai.invoice_date,
			ai.ap_voucher,
			ai.invoice_num,
			ai.description,
			ai.cms_linkage_value,
			ai.invoice_status,
			ai.posting_status,
			ai.vendor_num,
			ai.vendor_name,
			ai.vendor_site,
			ai.vendor_branch,
			ai.requester,
			ai.division,
			ai.ap_gl_date,
			ai.ap_gl_month,
			ai.ap_gl_year,
			ai.line_gl_date, 
			ai.line_gl_month, 
			ai.line_gl_year, 
			ai.wht_group,
			ai.type_tax,
			ai.tax_code,
			ai.Invoice_Amt,
			ai.Base_Amt,
			ai.Vat_Tax_Amt,
			ai.Vat_Base_Amt,
			ai.Invoice_WHT_Amt,
			ai.Tax_Amt,
			ai.WHT_Amt,
			ai.Payment_Id,
			ai.Payment_Date,
			ai.Payment_Voucher,
			ai.Payment_Document_Number,
			ai.Payment_Status,
			ai.Payment_Status_Disp,
			ai.Payment_Posting_Status,
			ai.Payment_Amt,
			ai.Payment_Base_Amt,
			ai.Doc_Category_Code,
			ai.Invoice_Dist_Id,
			ai.Invoice_Payment_Id
			,NULL		TTH_Id
			,NULL		TTH_Tax_Code
			,NULL		TTH_Base_Amt
			,NULL		TTH_NonRec_Amt
			,NULL		TTH_Rec_Amt
			,NULL		TTL_Id
			,NULL		TTL_Legal_Loc
			,NULL		TTL_Cat_Code
			,NULL		TTL_Tax_Invoice_No
			,NULL		TTL_Tax_Invoice_Date
			,NULL		TTL_Tax_Period
			,NULL		TTL_Submit
			,NULL		TTL_Submit_Period
			,NULL		TTL_Base_Amt
			,NULL		TTL_Tax_Amt
			,NULL		TTL_Last_Update_Date
			,NULL		TTL_Last_Update_By
			,NULL		TTL_Submit_Seq_Num
		FROM tac_thinput_vat_rec_pm ai
		WHERE ai.process_id = g_request_id
		AND ai.type_tax = 'S'
		AND ai.Invoice_Payment_Id IS NULL
		AND ai.invoice_status <> 'CANCELLED'
		AND NOT EXISTS (SELECT  /*+ RULE */  'x'
						FROM tac_thinput_vat_rec_tth tth
						WHERE ai.invoice_id = tth.invoice_id
						AND ai.type_tax = 'S'
						AND tth.process_id = g_request_id);
		COMMIT;
		write_log('     Get Tax Invoice - S-Vat No Tax Invoice : '||to_char(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
		INSERT  /*+ append nologging */ INTO tac_thinput_vat_rec_tth
			(process_id,
			invoice_id,
			pay_group,
			liability_acct,
			invoice_date,
			ap_voucher,
			invoice_num,
			description,
			CMS_Linkage_Value,
			invoice_status,
			posting_status,
			vendor_num,
			vendor_name,
			vendor_site,
			vendor_branch,
			requester,
			division,
			ap_gl_date,
			ap_gl_month,
			ap_gl_year,
			line_gl_date, 
			line_gl_month, 
			line_gl_year, 
			wht_group,
			type_tax,
			tax_code,
			Invoice_Amt,
			Base_Amt,
			Vat_Tax_Amt,
			Vat_Base_Amt,
			Invoice_WHT_Amt,
			Tax_Amt,
			WHT_Amt,
			Payment_Id,
			Payment_Date,
			Payment_Voucher,
			Payment_Document_Number,
			Payment_Status,
			Payment_Status_Disp,
			Payment_Posting_Status,
			Payment_Amt,
			Payment_Base_Amt,
			Doc_Category_Code,
			Invoice_Dist_Id,
			Invoice_Payment_Id,
			TTH_Id,
			TTH_Tax_Code,
			TTH_Base_Amt,
			TTH_NonRec_Amt,
			TTH_Rec_Amt,
			TTL_Id,
			TTL_Legal_Loc,
			TTL_Cat_Code,
			TTL_Tax_Invoice_No,
			TTL_Tax_Invoice_Date,
			TTL_Tax_Period,
			TTL_Submit,
			TTL_Submit_Period,
			TTL_Base_Amt,
			TTL_Tax_Amt,
			TTL_Last_Update_Date,
			TTL_Last_Update_By,
			TTL_Submit_Seq_Num
			)
		SELECT  /*+ RULE */ 
			ai.process_id,
			ai.invoice_id,
			ai.pay_group,
			ai.liability_acct,
			ai.invoice_date,
			ai.ap_voucher,
			ai.invoice_num,
			ai.description,
			ai.cms_linkage_value,
			ai.invoice_status,
			ai.posting_status,
			ai.vendor_num,
			ai.vendor_name,
			ai.vendor_site,
			ai.vendor_branch,
			ai.requester,
			ai.division,
			ai.ap_gl_date,
			ai.ap_gl_month,
			ai.ap_gl_year,
			ai.line_gl_date, 
			ai.line_gl_month, 
			ai.line_gl_year, 
			ai.wht_group,
			ai.type_tax,
			ai.tax_code,
			ai.Invoice_Amt,
			ai.Base_Amt,
			ai.Vat_Tax_Amt,
			ai.Vat_Base_Amt,
			ai.Invoice_WHT_Amt,
			ai.Tax_Amt,
			ai.WHT_Amt,
			ai.Payment_Id,
			ai.Payment_Date,
			ai.Payment_Voucher,
			ai.Payment_Document_Number,
			ai.Payment_Status,
			ai.Payment_Status_Disp,
			ai.Payment_Posting_Status,
			ai.Payment_Amt,
			ai.Payment_Base_Amt,
			ai.Doc_Category_Code,
			ai.Invoice_Dist_Id,
			ai.Invoice_Payment_Id
			,NULL		TTH_Id
			,NULL		TTH_Tax_Code
			,NULL		TTH_Base_Amt
			,NULL		TTH_NonRec_Amt
			,NULL		TTH_Rec_Amt
			,NULL		TTL_Id
			,NULL		TTL_Legal_Loc
			,NULL		TTL_Cat_Code
			,NULL		TTL_Tax_Invoice_No
			,NULL		TTL_Tax_Invoice_Date
			,NULL		TTL_Tax_Period
			,NULL		TTL_Submit
			,NULL		TTL_Submit_Period
			,NULL		TTL_Base_Amt
			,NULL		TTL_Tax_Amt
			,NULL		TTL_Last_Update_Date
			,NULL		TTL_Last_Update_By
			,NULL		TTL_Submit_Seq_Num
		FROM tac_thinput_vat_rec_pm ai
		WHERE ai.process_id = g_request_id
		AND ai.type_tax = 'S'
		AND ai.Invoice_Payment_Id IS NOT NULL
		AND ai.Payment_Status <> 'VOIDED'
		AND NOT EXISTS (SELECT  /*+ RULE */  'x'
						FROM tac_thinput_vat_rec_tth tth
						WHERE ai.Invoice_Payment_Id = tth.Invoice_Payment_Id
						AND ai.type_tax = 'S'
						AND tth.process_id = g_request_id);
		COMMIT;
	END IF;	
	------------------------------------------------------------------------------------------------------------
	write_log('     Calc Data Group : '||to_char(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
	UPDATE tac_thinput_vat_rec_tth
	SET data_group = DECODE(ttl_tax_period,NULL,'30','40')
	WHERE process_id = g_request_id;

	UPDATE tac_thinput_vat_rec_tth
	SET data_group = DECODE(tth_id,NULL,'20','30')||'.' || data_group
	WHERE process_id = g_request_id;

	UPDATE tac_thinput_vat_rec_tth
	SET data_group = DECODE(invoice_payment_id,NULL,'10','20')||'.' || data_group
	WHERE process_id = g_request_id;

	FOR rec IN (SELECT invoice_id, MIN(data_group) min_grp
				FROM tac_thinput_vat_rec_tth
				WHERE process_id = g_request_id
				GROUP BY invoice_id)
	LOOP
		UPDATE tac_thinput_vat_rec_tth
		SET min_data_group = rec.min_grp
		WHERE invoice_id = rec.invoice_id
		AND process_id = g_request_id;
		COMMIT;
	END LOOP;
	COMMIT;
	------------------------------------------------------------------------------------------------------------
END gen_data;
----------------------------------------------------------------------------
PROCEDURE main_generate_file
(ERR_MSG				OUT	VARCHAR2
,ERR_CODE				OUT	VARCHAR2
,P_TAX_PERIOD_FROM			IN VARCHAR2
,P_TAX_PERIOD_TO			IN VARCHAR2
,P_VAT_TAX_CODE				IN VARCHAR2
,P_FROM_INVOICE_DATE		IN VARCHAR2
,P_DEFAULT_SUBMIT_PERIOD	IN VARCHAR2
,P_SHOW_UNACCT_INV			IN VARCHAR2
) IS
	v_status		VARCHAR2(2000);
BEGIN
	write_log('----------------------------------------------------------------------------');
	write_log('Start Process : '||to_char(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
	tac_text_output_util.g_process_id	:=	g_request_id;
	v_status := G_NO_ERROR;
	-- Generate text file
	write_log('Call Generate Data : '||to_char(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
	gen_data
			(P_TAX_PERIOD_FROM			=> P_TAX_PERIOD_FROM
			,P_TAX_PERIOD_TO			=> P_TAX_PERIOD_TO
			,P_VAT_TAX_CODE				=> P_VAT_TAX_CODE
			,P_FROM_INVOICE_DATE		=> P_FROM_INVOICE_DATE
			,P_DEFAULT_SUBMIT_PERIOD	=> P_DEFAULT_SUBMIT_PERIOD
			,P_SHOW_UNACCT_INV			=> P_SHOW_UNACCT_INV
			);
	-- Generate text file
	write_log('Call Generate CSV : '||to_char(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
	generate_csv
			(P_TAX_PERIOD_FROM			=> P_TAX_PERIOD_FROM
			,P_TAX_PERIOD_TO			=> P_TAX_PERIOD_TO
			,P_VAT_TAX_CODE				=> P_VAT_TAX_CODE
			,P_FROM_INVOICE_DATE		=> P_FROM_INVOICE_DATE
			,P_DEFAULT_SUBMIT_PERIOD	=> P_DEFAULT_SUBMIT_PERIOD
			,P_SHOW_UNACCT_INV			=> P_SHOW_UNACCT_INV
			,o_status					=> v_status);
	write_log('Call Generate CSV status : '||v_status);

	IF v_status = G_NO_ERROR THEN
		ERR_CODE	:=	'0';
	ELSE
		write_log('Error :');
		write_log('---------------------------------------');
		write_log(v_status);
		write_log(' ');
		write_log('---------------------------------------');
		ERR_MSG		:=	v_status;
		ERR_CODE	:=	'2';
		RETURN;
	END IF;

	write_log('End Process : '||to_char(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
	write_log('----------------------------------------------------------------------------');
	COMMIT;
EXCEPTION
	WHEN OTHERS THEN
		ERR_MSG		:=	SQLERRM;
		ERR_CODE	:=	'2';
END main_generate_file;
----------------------------------------------------------------------------
END TAC_THINPUT_VAT_REC_PKG;
/
