create table TAC_VALIDATE_INVOICE
(
  request_id    NUMBER,
  invoice_id    NUMBER,
  invoice_num   VARCHAR2(100),
  invoice_date  DATE,
  DISTRIBUTION_LINE_NUMBER	NUMBER,
  msg_error     VARCHAR2(4000),
  creation_date DATE default SYSDATE,
  created_by    NUMBER
);
create index TAC_VALIDATE_INVOICE_ID1 on TAC_VALIDATE_INVOICE (REQUEST_ID);
create index TAC_VALIDATE_INVOICE_ID2 on TAC_VALIDATE_INVOICE (CREATION_DATE);
