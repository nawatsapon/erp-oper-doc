-- Create table
create global temporary table TAC_THINPUT_VAT_REC_PM
(
  process_id              NUMBER not null,
  invoice_id              NUMBER not null,
  pay_group               VARCHAR2(4000),
  liability_acct          VARCHAR2(4000),
  invoice_date            DATE,
  ap_voucher              VARCHAR2(4000),
  invoice_num             VARCHAR2(4000),
  description             VARCHAR2(4000),
  cms_linkage_value       VARCHAR2(4000),
  invoice_status          VARCHAR2(4000),
  posting_status          VARCHAR2(4000),
  vendor_num              VARCHAR2(4000),
  vendor_name             VARCHAR2(4000),
  vendor_site             VARCHAR2(4000),
  vendor_branch           VARCHAR2(4000),
  requester               VARCHAR2(4000),
  division                VARCHAR2(4000),
  ap_gl_date              DATE,
  ap_gl_month             NUMBER,
  ap_gl_year              NUMBER,
  line_gl_date            DATE,
  line_gl_month           NUMBER,
  line_gl_year            NUMBER,
  wht_group               VARCHAR2(4000),
  type_tax                VARCHAR2(4000),
  tax_code                VARCHAR2(4000),
  invoice_amt             NUMBER,
  invoice_wht_amt         NUMBER,
  vat_base_amt            NUMBER,
  vat_tax_amt             NUMBER,
  base_amt                NUMBER,
  tax_amt                 NUMBER,
  wht_amt                 NUMBER,
  payment_id              NUMBER,
  payment_date            DATE,
  payment_voucher         VARCHAR2(4000),
  payment_document_number VARCHAR2(4000),
  payment_status          VARCHAR2(4000),
  payment_status_disp     VARCHAR2(4000),
  payment_posting_status  VARCHAR2(4000),
  payment_amt             NUMBER,
  payment_base_amt        NUMBER,
  doc_category_code       VARCHAR2(4000),
  invoice_dist_id         NUMBER,
  invoice_payment_id      NUMBER
)
on commit preserve rows;
-- Create/Recreate indexes 
create index TAC_THINPUT_VAT_REC_PM_IDX1 on TAC_THINPUT_VAT_REC_PM (PROCESS_ID);
create index TAC_THINPUT_VAT_REC_PM_IDX2 on TAC_THINPUT_VAT_REC_PM (PROCESS_ID, INVOICE_ID);
create index TAC_THINPUT_VAT_REC_PM_IDX3 on TAC_THINPUT_VAT_REC_PM (PROCESS_ID, PAYMENT_ID);
create index TAC_THINPUT_VAT_REC_PM_IDX4 on TAC_THINPUT_VAT_REC_PM (PROCESS_ID, INVOICE_ID, PAYMENT_ID);
create index TAC_THINPUT_VAT_REC_PM_IDX5 on TAC_THINPUT_VAT_REC_PM (PROCESS_ID, TYPE_TAX);
