create or replace package TAC_INVOICE_ACTION_PKG is
/*Change History
 Date      Author        Version   Description
 --------- -----------    ------   ------------------------------------------------
 06-Nov-17  Pinyo P.    1.00   Created
*/
----------------------------------------------------------------------------
PROCEDURE clear_data(i_request_id	IN	NUMBER);
----------------------------------------------------------------------------
PROCEDURE VALIDATE_INVOICE(
  i_invoice_id         IN       NUMBER,
  i_request_id		   IN       NUMBER
);
----------------------------------------------------------------------------
PROCEDURE VALIDATE_INVOICE_BATCH(
  i_batch_id           IN       NUMBER,
  i_request_id		   IN       NUMBER
);
----------------------------------------------------------------------------
PROCEDURE VALIDATE_INVOICE_REPORT(
  err_msg              OUT      VARCHAR2,
  err_code             OUT      VARCHAR2,
  i_request_id         IN       NUMBER
);
----------------------------------------------------------------------------
PROCEDURE submit_conc
(i_request_id   IN 	NUMBER,
 o_request_id		OUT NUMBER);
----------------------------------------------------------------------------
end TAC_INVOICE_ACTION_PKG;
/
create or replace package body TAC_INVOICE_ACTION_PKG is
/*Change History
 Date      Author        Version   Description
 --------- -----------    ------   ------------------------------------------------
 06-Nov-17  Pinyo P.    1.00   Created
*/
----------------------------------------------------------------------------

----------------------------------------------------------------------------
PROCEDURE write_log (param_msg VARCHAR2 ) IS
BEGIN
 fnd_file.put_line(fnd_file.log, param_msg);
END write_log;
----------------------------------------------------------------------------
PROCEDURE write_output (param_msg VARCHAR2)
IS
BEGIN
  fnd_file.put_line (fnd_file.output, param_msg);
END write_output;
----------------------------------------------------------------------------
PROCEDURE clear_data(i_request_id	IN	NUMBER) IS
BEGIN
	DELETE	TAC_VALIDATE_INVOICE
	WHERE	request_id = i_request_id;
  --
	DELETE	TAC_VALIDATE_INVOICE
	WHERE creation_date < SYSDATE -2;

	COMMIT;
END;
----------------------------------------------------------------------------
PROCEDURE INSERT_TAC_VALIDATE_INVOICE(
 io_tac_validate_invoice IN OUT TAC_VALIDATE_INVOICE%ROWTYPE
) IS 
BEGIN
  INSERT INTO TAC_VALIDATE_INVOICE
  (
  request_id, 
  invoice_id, 
  invoice_num, 
  invoice_date, 
  distribution_line_number,
  msg_error, 
  creation_date, 
  created_by
  ) VALUES (
  io_tac_validate_invoice.request_id, 
  io_tac_validate_invoice.invoice_id, 
  io_tac_validate_invoice.invoice_num, 
  io_tac_validate_invoice.invoice_date, 
  io_tac_validate_invoice.distribution_line_number,
  io_tac_validate_invoice.msg_error, 
  SYSDATE, 
  io_tac_validate_invoice.created_by
  );
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
	ROLLBACK;
END; 
----------------------------------------------------------------------------
PROCEDURE submit_conc
(i_request_id   IN 	NUMBER,
 o_request_id		OUT NUMBER)
IS
BEGIN
		o_request_id := FND_REQUEST.submit_request (
    	'SQLAP' ,				--  application IN varchar2 default NULL,
    	'TAC_VALIDATE_INVOICE_REPORT'  ,		-- program     IN varchar2 default NULL,
    	NULL,	-- description IN varchar2 default NULL,
    	NULL,	-- start_time  IN varchar2 default NULL,
    	NULL,	-- sub_request IN boolean  default FALSE,
    	i_request_id,	--1
    	CHR(0),		--2
    	CHR(0),	--3
		CHR(0),	--4
		CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),   --5--10
        CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),	--11-20
        CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),	--21-30
        CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),	--31-40
        CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),	--41-50
        CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),	--51-60
        CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),	--61-70
        CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),	--71-80
        CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),	--81-90
        CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0),CHR(0));	--91-100

		COMMIT;

END;
---------------------------------------------------------------------------
PROCEDURE VALIDATE_INVOICE(
  i_invoice_id         IN       NUMBER,
  i_request_id		   IN       NUMBER
) IS
  v_g_vat_item         NUMBER;
  v_g_vat_tax          NUMBER;
  v_s_vat_item         NUMBER;
  v_s_vat_tax          NUMBER;
  v_tac_validate_invoice TAC_VALIDATE_INVOICE%ROWTYPE;
  v_request_id         NUMBER;
  v_submit_request_id        NUMBER;
  t_Token_tbl				tac_fnd_util_pkg.Token_Tbl_Type;
  v_msg					VARCHAR2(4000);
  
  v_account_code		VARCHAR2(100);
  v_acc_cnt				NUMBER;
  
  v_vat_amt				NUMBER;
  v_tolerance		    NUMBER;
  v_found				BOOLEAN;
  v_segment1 		gl_code_combinations_kfv.segment1%TYPE;
  v_segment11 		gl_code_combinations_kfv.segment11%TYPE;
  v_segment12 		gl_code_combinations_kfv.segment12%TYPE;
  v_segment13 		gl_code_combinations_kfv.segment13%TYPE;
  v_segment14 		gl_code_combinations_kfv.segment14%TYPE;
  v_segment18 		gl_code_combinations_kfv.segment18%TYPE;
  v_segment21 		gl_code_combinations_kfv.segment21%TYPE;
  v_segment19 		gl_code_combinations_kfv.segment19%TYPE;
  
  vi_segment1 		gl_code_combinations_kfv.segment1%TYPE;
  vi_segment11 		gl_code_combinations_kfv.segment11%TYPE;
  vi_segment12 		gl_code_combinations_kfv.segment12%TYPE;
  vi_segment13 		gl_code_combinations_kfv.segment13%TYPE;
  vi_segment14 		gl_code_combinations_kfv.segment14%TYPE;
  vi_segment18 		gl_code_combinations_kfv.segment18%TYPE;
  vi_segment21 		gl_code_combinations_kfv.segment21%TYPE;
  vi_segment19 		gl_code_combinations_kfv.segment19%TYPE;
  
  v_set_of_books_id   	NUMBER := fnd_profile.value('GL_SET_OF_BKS_ID');
  v_chart_of_accounts_id	NUMBER;
 
  CURSOR c_check_acc IS 
	  SELECT aid.invoice_id,
		aid.DIST_CODE_COMBINATION_ID,
		atc.TAX_CODE_COMBINATION_ID,
		aid.DISTRIBUTION_LINE_NUMBER,
		aid.LINE_TYPE_LOOKUP_CODE,
		aid.AMOUNT
      FROM ap_invoice_distributions aid
        ,ap_tax_codes atc
      WHERE aid.tax_code_id = atc.tax_id(+)
	  AND DECODE(aid.reversal_flag,'Y','Y',DECODE(aid.parent_reversal_id,NULL,'N','Y')) <> 'Y'
      AND aid.invoice_id = i_invoice_id
 	  ORDER BY aid.DISTRIBUTION_LINE_NUMBER;
	  
	  
  CURSOR c_check_line IS
	  SELECT 	 
		t.rowid row_id,
		t.line_type_lookup_code,
		t.name,
		t.tax_rate,
		t.amount,
		t.vat_amt
		FROM TAC_TMP_VALIDATE_INVOICE t
		WHERE t.request_id = i_request_id
		AND t.invoice_id = i_invoice_id
		AND t.line_type_lookup_code = 'ITEM'
		ORDER BY t.line_type_lookup_code;

  CURSOR c_check_line_tax(i_name IN VARCHAR2,i_tax_rate IN NUMBER) IS
	  SELECT 	 
		t.rowid row_id,
		t.line_type_lookup_code,
		t.name,
		t.tax_rate,
		t.amount,
		t.vat_amt
		FROM TAC_TMP_VALIDATE_INVOICE t
		WHERE t.request_id = i_request_id
		AND t.invoice_id = i_invoice_id
		AND t.line_type_lookup_code = 'TAX'
		AND t.name = i_name
		AND t.tax_rate = i_tax_rate
		ORDER BY t.name;

   CURSOR c_check_line_tax_found IS
	  SELECT 	 
		t.rowid row_id,
		t.line_type_lookup_code,
		t.name,
		t.tax_rate,
		t.amount,
		t.vat_amt
		FROM TAC_TMP_VALIDATE_INVOICE t
		WHERE t.request_id = i_request_id
		AND t.invoice_id = i_invoice_id
		AND t.line_type_lookup_code = 'TAX'
		AND NVL(t.found_flag,'N') <> 'Y'
		ORDER BY t.line_type_lookup_code;
				  
BEGIN
  
	BEGIN
	SELECT g.chart_of_accounts_id
	INTO v_chart_of_accounts_id
	FROM gl_sets_of_books  g
	WHERE g.set_of_books_id = v_set_of_books_id;
	EXCEPTION WHEN OTHERS THEN NULL;
	END;		

  v_msg := NULL;
  v_g_vat_item := 0;
  v_g_vat_tax  := 0;
  v_s_vat_item := 0;
  v_s_vat_tax  := 0;
  
  BEGIN
      SELECT SUM(DECODE(aid.line_type_lookup_code,'ITEM',DECODE(atc.global_attribute20,'N',aid.amount,0),0))  g_vat_item
      ,SUM(DECODE(aid.line_type_lookup_code,'TAX',DECODE(atc.global_attribute20,'N',aid.amount,0),0))  g_vat_tax
      ,SUM(DECODE(aid.line_type_lookup_code,'ITEM',DECODE(atc.global_attribute20,'Y',aid.amount,0),0))  s_vat_item
      ,SUM(DECODE(aid.line_type_lookup_code,'TAX',DECODE(atc.global_attribute20,'Y',aid.amount,0),0))  s_vat_tax
      INTO  v_g_vat_item,
        	  v_g_vat_tax,
            v_s_vat_item,
            v_s_vat_tax
      FROM ap_invoice_distributions aid
        ,ap_tax_codes atc
      WHERE aid.tax_code_id = atc.tax_id
	  AND atc.name NOT LIKE 'U-VAT%'	-- Skip checking for Unclaimable VAT
      AND aid.invoice_id = i_invoice_id;
      EXCEPTION WHEN OTHERS THEN NULL;
  END;
  IF ((v_g_vat_item + v_g_vat_tax) > 0) AND
     ((v_s_vat_item + v_s_vat_tax) > 0) THEN 
      
      t_Token_tbl.DELETE;
			v_msg := tac_fnd_util_pkg.get_message
			(i_message_name		=>	'TAC_AP_INV_MIX_VAT_TYPE'
			,i_app_short_name	=>	'SQLAP'
			,it_Token_tbl			=>	t_Token_tbl);
      
	  v_tac_validate_invoice := NULL;
      v_tac_validate_invoice.request_id := i_request_id;
      v_tac_validate_invoice.invoice_id := i_invoice_id;
      v_tac_validate_invoice.msg_error := v_msg;
      INSERT_TAC_VALIDATE_INVOICE(v_tac_validate_invoice);
  END IF;
	  
  ----
  
  FOR r IN c_check_acc LOOP
	  IF r.line_type_lookup_code = 'TAX' THEN 
		BEGIN  
  		  SELECT segment1,segment11,segment12,segment13,segment14,segment18,segment21,segment19
		  INTO v_segment1,v_segment11,v_segment12,v_segment13,v_segment14,v_segment18,v_segment21,v_segment19
		  FROM gl_code_combinations_kfv gcc
		  WHERE gcc.chart_of_accounts_id =  v_chart_of_accounts_id
		  AND gcc.code_combination_id = r.TAX_CODE_COMBINATION_ID;
		  EXCEPTION WHEN OTHERS THEN NULL;
		END;  
		
		BEGIN  
  		  SELECT segment1,segment11,segment12,segment13,segment14,segment18,segment21,segment19
		  INTO vi_segment1,vi_segment11,vi_segment12,vi_segment13,vi_segment14,vi_segment18,vi_segment21,vi_segment19
		  FROM gl_code_combinations_kfv gcc
		  WHERE gcc.chart_of_accounts_id =  v_chart_of_accounts_id
		  AND gcc.code_combination_id = r.DIST_CODE_COMBINATION_ID;
		  EXCEPTION WHEN OTHERS THEN NULL;
		END; 
		  
		  IF  v_segment1 <> vi_segment1 OR
			  v_segment11 <> vi_segment11 OR
			  v_segment12 <> vi_segment12 OR
			  v_segment13 <> vi_segment13 OR
			  v_segment14 <> vi_segment14 OR
			  v_segment18 <> vi_segment18 OR
			  v_segment21 <> vi_segment21 OR
			  v_segment19 <> vi_segment19 
			  THEN
		  
			   t_Token_tbl.DELETE;
				v_msg := tac_fnd_util_pkg.get_message
				(i_message_name		=>	'TAC_AP_INV_WRONG_CCID4TAX'
				,i_app_short_name	=>	'SQLAP'
				,it_Token_tbl			=>	t_Token_tbl);
			    
				v_tac_validate_invoice := NULL;  
				v_tac_validate_invoice.request_id := i_request_id;
				v_tac_validate_invoice.invoice_id := i_invoice_id;
				v_tac_validate_invoice.distribution_line_number := r.distribution_line_number;
				v_tac_validate_invoice.msg_error := v_msg;
				INSERT_TAC_VALIDATE_INVOICE(v_tac_validate_invoice);
		  END IF;	  
	  END IF;	  
	  
  	  IF r.line_type_lookup_code = 'ITEM' THEN 
		  v_account_code := NULL;
		  BEGIN
			SELECT gcc.segment11
			INTO v_account_code
			FROM gl_code_combinations_kfv gcc
			WHERE gcc.code_combination_id = r.DIST_CODE_COMBINATION_ID
			AND gcc.chart_of_accounts_id =  v_chart_of_accounts_id;
			EXCEPTION WHEN OTHERS THEN NULL;
		  END;	
			
		  IF v_account_code IS NOT NULL THEN     	
			v_acc_cnt := 0;
		  	BEGIN
				SELECT NVL(count(gcc.segment11),0)
				INTO v_acc_cnt
				FROM ap_tax_codes atc
				,gl_code_combinations_kfv gcc
				WHERE atc.tax_type <> 'AWT'
				AND atc.TAX_CODE_COMBINATION_ID = gcc.code_combination_id
				AND gcc.chart_of_accounts_id =  v_chart_of_accounts_id
				AND gcc.segment11 = v_account_code;
				EXCEPTION WHEN OTHERS THEN v_acc_cnt := 0;
			 END;
			 
			 IF v_acc_cnt > 0 THEN 
				t_Token_tbl.DELETE;
					v_msg := tac_fnd_util_pkg.get_message
					(i_message_name		=>	'TAC_AP_INV_WRONG_CCID4NONTAX'
					,i_app_short_name	=>	'SQLAP'
					,it_Token_tbl			=>	t_Token_tbl);
					
				v_tac_validate_invoice := NULL;				      
				v_tac_validate_invoice.request_id := i_request_id;
				v_tac_validate_invoice.invoice_id := i_invoice_id;
				v_tac_validate_invoice.distribution_line_number := r.distribution_line_number;
				v_tac_validate_invoice.msg_error := v_msg;
				INSERT_TAC_VALIDATE_INVOICE(v_tac_validate_invoice);
			 END IF;
			 
		  END IF;	 
	  END IF;	  
	  
  END LOOP;
  ----
  v_tolerance := NVL(fnd_profile.VALUE('TAC_AP_INV_TAX_TOLERANCE'),1); --1
  BEGIN
	  INSERT INTO TAC_TMP_VALIDATE_INVOICE
	  (request_id, invoice_id, line_type_lookup_code,  name,  tax_rate,  amount, vat_amt )
	  SELECT 
	  i_request_id, 
	  i_invoice_id,
	  aid.line_type_lookup_code,
	  atc.name,
	  atc.tax_rate,
	  SUM(aid.amount) amount,
	  ROUND(NVL(atc.tax_rate,0)*SUM(aid.amount)/100,2) vat_amt
      FROM ap_invoice_distributions aid
        ,ap_tax_codes atc
      WHERE aid.tax_code_id = atc.tax_id
	  AND atc.TAX_TYPE <> 'AWT'
      AND aid.invoice_id = i_invoice_id
	  GROUP BY
	  aid.line_type_lookup_code,
	  atc.name,
	  atc.tax_rate;
	  EXCEPTION WHEN OTHERS THEN NULL;
  END;
  
  FOR r IN c_check_line LOOP
	  v_vat_amt := 0;
	  v_found := TRUE;
	  FOR rec IN c_check_line_tax(r.name,r.tax_rate) LOOP
		  v_found := FALSE;	
		  BEGIN
			UPDATE TAC_TMP_VALIDATE_INVOICE tt
			SET tt.found_flag = 'Y'
			WHERE tt.rowid = rec.row_id;
			EXCEPTION WHEN OTHERS THEN NULL;
		  END;	  
		  
		  IF ABS(r.vat_amt - rec.amount) > v_tolerance THEN 
				t_Token_tbl.DELETE;
					v_msg := tac_fnd_util_pkg.get_message
					(i_message_name		=>	'TAC_AP_INV_TAX_DIFF_OVER_TLR'
					,i_app_short_name	=>	'SQLAP'
					,it_Token_tbl			=>	t_Token_tbl);
				
				v_tac_validate_invoice := NULL;					      
				v_tac_validate_invoice.request_id := i_request_id;
				v_tac_validate_invoice.invoice_id := i_invoice_id;
				v_tac_validate_invoice.msg_error := v_msg;
				INSERT_TAC_VALIDATE_INVOICE(v_tac_validate_invoice);

		  END IF;	  
	  END LOOP;
	  
	  IF (v_found AND (r.vat_amt <> 0)) THEN 
		  t_Token_tbl.DELETE;
					v_msg := tac_fnd_util_pkg.get_message
					(i_message_name		=>	'TAC_AP_TAX_WITH_NO_TAX'
					,i_app_short_name	=>	'SQLAP'
					,it_Token_tbl			=>	t_Token_tbl);
					
				v_tac_validate_invoice := NULL;									      
				v_tac_validate_invoice.request_id := i_request_id;
				v_tac_validate_invoice.invoice_id := i_invoice_id;
				v_tac_validate_invoice.msg_error := v_msg||'('||r.name||')';
				INSERT_TAC_VALIDATE_INVOICE(v_tac_validate_invoice);
	  END IF;
	  
  END LOOP;	  
  
  FOR rt IN c_check_line_tax_found LOOP
	t_Token_tbl.DELETE;
		v_msg := tac_fnd_util_pkg.get_message
		(i_message_name		=>	'TAC_AP_TAX_WITH_NO_ITEM'
		,i_app_short_name	=>	'SQLAP'
		,it_Token_tbl			=>	t_Token_tbl);
		
	v_tac_validate_invoice := NULL;											      
	v_tac_validate_invoice.request_id := i_request_id;
	v_tac_validate_invoice.invoice_id := i_invoice_id;
	v_tac_validate_invoice.msg_error := v_msg||'('||rt.name||')';
	INSERT_TAC_VALIDATE_INVOICE(v_tac_validate_invoice);		  
  END LOOP;
  ----  
  COMMIT;  
END;
----------------------------------------------------------------------------
PROCEDURE VALIDATE_INVOICE_BATCH(
  i_batch_id           IN       NUMBER,
  i_request_id		   IN       NUMBER
) IS
  
  CURSOR c_batch IS 
  SELECT t.* 
  FROM ap_invoices_all t
  WHERE t.batch_id = i_batch_id
  ORDER BY t.invoice_id;
  
BEGIN
    
  FOR r IN c_batch LOOP  
    VALIDATE_INVOICE(r.invoice_id, i_request_id);
  END LOOP;
  
END;
----------------------------------------------------------------------------
PROCEDURE VALIDATE_INVOICE_REPORT(
  err_msg              OUT      VARCHAR2,
  err_code             OUT      VARCHAR2,
  i_request_id         IN       NUMBER
) IS
  CURSOR c_q1 IS 
  SELECT t.msg_error
         ,t.invoice_id
         ,ai.invoice_num  invoice_num 
		 ,t.DISTRIBUTION_LINE_NUMBER 
		 ,(SELECT ab.batch_name 
		  FROM AP_BATCHES_ALL  ab
		  WHERE ab.batch_id = ai.batch_id) batch_name
  FROM TAC_VALIDATE_INVOICE t,
       ap_invoices_all ai
  WHERE t.request_id = i_request_id
  AND  t.invoice_id = ai.invoice_id
  ORDER BY batch_name,ai.invoice_num, NVL(t.DISTRIBUTION_LINE_NUMBER,0), t.msg_error;
BEGIN
  IF i_request_id IS NOT NULL THEN 
    write_output(RPAD('Batch Name',50,' ')||'  '||RPAD('Invoice Number',30,' ')||'  '||RPAD('Distribution Num',18,' ')||'  '||'Error Message');
    FOR r IN c_q1 LOOP
      write_output(RPAD(r.batch_name,50,' ')||'  '||RPAD(r.invoice_num,30,' ')||'  '||RPAD(NVL(TO_CHAR(r.distribution_line_number),' '),18,' ')||'  '||r.msg_error);
    END LOOP;  
  ELSE
      write_log('Request ID not Found.');
  END IF;
  NULL;
END;    
----------------------------------------------------------------------------
end TAC_INVOICE_ACTION_PKG;
/
