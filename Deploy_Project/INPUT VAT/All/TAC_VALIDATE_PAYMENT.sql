create table TAC_VALIDATE_PAYMENT
(
  request_id    NUMBER,
  check_id    NUMBER,
  document_num   VARCHAR2(100),
  document_date  DATE,
  msg_error     VARCHAR2(4000),
  creation_date DATE default SYSDATE,
  created_by    NUMBER
);
create index TAC_VALIDATE_PAYMENT_ID1 on TAC_VALIDATE_PAYMENT (REQUEST_ID);
create index TAC_VALIDATE_PAYMENT_ID2 on TAC_VALIDATE_PAYMENT (CREATION_DATE);
