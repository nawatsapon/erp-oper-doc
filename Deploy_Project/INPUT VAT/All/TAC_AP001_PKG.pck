CREATE OR REPLACE PACKAGE TAC_AP001_PKG IS

PROCEDURE update_comfirmed_data
(p_org				IN NUMBER
,p_org_loc			IN NUMBER
,p_entity_from		IN VARCHAR2
,p_period			IN VARCHAR2	
,p_submit_num		IN NUMBER
,p_submit_period	IN VARCHAR2
,p_requested_by		IN NUMBER
,p_order_by			IN VARCHAR2
,o_error_msg		OUT VARCHAR2);

END TAC_AP001_PKG;
/
CREATE OR REPLACE PACKAGE BODY TAC_AP001_PKG IS
/*Change History
 Date      	Author       	Version 	Description
 --------- 	-------------	--------- 	------------------------------------------------
 17-Nov-17 	TM.    			1.00 		Created
 
*/
ABNORMAL_END      		EXCEPTION;
G_COND_REQUEST_ID       NUMBER;
G_NO_ERROR				VARCHAR2(100)	:=	'NO_ERROR';
----------------------------------------------------------------------------
PROCEDURE update_comfirmed_data
(p_org				IN NUMBER
,p_org_loc			IN NUMBER
,p_entity_from		IN VARCHAR2
,p_period			IN VARCHAR2	
,p_submit_num		IN NUMBER
,p_submit_period	IN VARCHAR2
,p_requested_by		IN NUMBER
,p_order_by			IN VARCHAR2
,o_error_msg		OUT VARCHAR2)
IS
	TYPE CurTyp  IS REF CURSOR;
	c_cursor		CurTyp;
	v_stmt      	VARCHAR2(32767);
	v_seq			NUMBER;
	v_th_til_id		NUMBER;
	v_upd_row		NUMBER;	
BEGIN
	o_error_msg := NULL;
	-- Dynamic SQL statement with placeholder:
	v_stmt := 'SELECT dt.th_til_id'||
			' FROM th_taxinvoices_header hd'||
			' ,th_taxinvoices_line dt'||
			' WHERE hd.th_tih_id = dt.th_tih_id'|| 
			' AND hd.th_tih_location_id = '||p_org_loc||
			' AND dt.th_til_location_id = '||p_entity_from||
			' AND dt.th_til_period_name = '''||p_period||''''||
			' AND NVL(dt.submit_num,0) = '||NVL(p_submit_num,0)||
			' AND dt.th_til_comfirmed_date is NULL';
	if p_order_by is null then
		v_stmt := v_stmt||' ORDER BY hd.th_tih_voucher_number';
	elsif upper(p_order_by)  = 'INVOICE DATE' then
		v_stmt := v_stmt||' Order By ,dt.th_til_tax_inv_date';
	elsif upper(p_order_by)  = 'UPDATE DATE' then
		v_stmt := v_stmt||' Order By dt.last_update_date';
	elsif upper(p_order_by)  = 'UPDATE BY & UPDATE DATE' then
		v_stmt := v_stmt||' Order By dt.last_updated_by ,dt.last_update_date';	
	end if;										
	-- Open cursor & specify bind argument in USING clause:
	OPEN c_cursor FOR v_stmt;		
	-- Fetch rows from result set one at a time:
	v_seq := 0;
	LOOP
		FETCH c_cursor INTO v_th_til_id;
		EXIT WHEN c_cursor%NOTFOUND;
		v_seq := v_seq + 1;
		UPDATE th_taxinvoices_line	
		SET	submit_period_name = NVL(p_submit_period,p_period),
			submit_seq_num = v_seq,
			released_by = last_updated_by,
			released_date = last_update_date,
			th_til_comfirmed_date = SYSDATE,
			last_updated_by = p_requested_by,
			last_update_date = SYSDATE
		WHERE th_til_id = v_th_til_id;
	END LOOP;		
	-- Close cursor:
	CLOSE c_cursor;

	IF v_seq = 0 THEN 
		o_error_msg := 'No record found for process, please check Period and Submit# parameter.';
	ELSE		
		BEGIN
			update tac_vat_period_status
			set latest_submit_num_closed = NVL(p_submit_num,0)
			   ,current_submit_num = DECODE(GREATEST(NVL(current_submit_num,0),NVL(p_submit_num,0)),NVL(p_submit_num,0),NVL(p_submit_num,0)+1,NVL(current_submit_num,0))
			where org_id = p_org
			and location_id = p_org_loc
			and period_name = p_period;
			v_upd_row := SQL%ROWCOUNT;
		EXCEPTION
			WHEN OTHERS THEN
				v_upd_row := 0;
		END;
		IF v_upd_row = 0 THEN
			insert into tac_vat_period_status
			(org_id ,location_id ,period_name ,latest_submit_num_closed ,current_submit_num)
			values
			(p_org ,p_org_loc ,p_period ,nvl(p_submit_num,0) ,nvl(p_submit_num,0)+1);
		END IF;
		insert into tac_vat_period_submitted
		(org_id ,location_id ,period_name ,submit_num_closed)
		values
		(p_org ,p_org_loc ,p_period ,nvl(p_submit_num,0));
		
	END IF;
END update_comfirmed_data;
----------------------------------------------------------------------------
END TAC_AP001_PKG;
/
