CREATE OR REPLACE PACKAGE TAC_AP_INTERFACE_UTIL_DTCV2 IS

-- ***** Last Updated by TM 01-MAR-2017 *****

    -- Added by AP@BAS on 07-Feb-2013
    g_dtn_org_id NUMBER := 142;
    g_psb_org_id NUMBER := 218; --TEST=238, PROD=218
    --


    PROCEDURE insert_invoice(ir_invoice IN ap_invoices_interface%ROWTYPE);

    PROCEDURE insert_invoice_line(ir_invoice_line IN ap_invoice_lines_interface%ROWTYPE);

    PROCEDURE update_template
    (
        i_temp_id IN NUMBER
       ,i_next_period IN VARCHAR2
       ,i_next_payment IN NUMBER
       ,i_period_rem IN NUMBER
       ,i_line_amount IN NUMBER
    );

    PROCEDURE interface_ap
    (
        err_msg OUT VARCHAR2
       ,err_code OUT VARCHAR2
       ,i_org_id IN NUMBER
       ,i_batch IN VARCHAR2
       ,i_group IN VARCHAR2
    );

    PROCEDURE inf_ap_recurring
    (
        err_msg OUT VARCHAR2
       ,err_code OUT VARCHAR2
       ,i_inv_batch IN VARCHAR2
       ,i_gl_date_char IN VARCHAR2
       ,i_doc_category_code IN VARCHAR2
       ,i_exch_rate_type IN VARCHAR2
       ,i_exch_date IN DATE
       ,i_exch_rate IN NUMBER
    );

    PROCEDURE insert_dtac_ap_banks_to_sts
    (
        ir_dtac_ap_banks_to_sts IN dtac_ap_banks_to_sts%ROWTYPE
       ,o_err OUT VARCHAR2
    );

    PROCEDURE get_bank_from_flex_val
    (
        err_msg IN OUT VARCHAR2
       ,err_code IN OUT VARCHAR2
    );


    -- Added by AP@BAS on 07-Feb-2013
    PROCEDURE update_temp_data (p_group_id VARCHAR2);

END TAC_AP_INTERFACE_UTIL_DTCV2;
/
CREATE OR REPLACE PACKAGE BODY TAC_AP_INTERFACE_UTIL_DTCV2 IS

-- ***** Last Updated by TM 01-MAR-2017 *****
-- Added by TM 15-Feb-2017 : To get setup instead of hardcoding
	g_error_flag	BOOLEAN;
-- End Added by TM 15-Feb-2017 : To get setup instead of hardcoding

-- Added by TM 24-Nov-2017 : Source Reference to GLOBAL_ATTRIBUTE3

    ----------------------------------------------------------------------------------------------------
    PROCEDURE write_log(param_msg VARCHAR2) IS
    BEGIN
        fnd_file.put_line(fnd_file.log, param_msg);
    END write_log;

    PROCEDURE write_out(param_msg VARCHAR2) IS
    BEGIN
        fnd_file.put_line(fnd_file.output, param_msg);
    END write_out;

    PROCEDURE delete_temp_table(p_group_id VARCHAR2, p_old_days NUMBER DEFAULT 45) IS
    PRAGMA AUTONOMOUS_TRANSACTION;
    BEGIN
        DELETE FROM dtac_ap_invoices_temp_int
        WHERE eai_crtd_dttm <= (SYSDATE -p_old_days)
            AND SOURCE = 'STS';
        COMMIT;

    EXCEPTION
        WHEN OTHERS THEN
            NULL;
    END delete_temp_table;
    ---------------------------------------------------------------------------------------


    -- Added by AP@BAS on 07-Feb-2013 : update temp data for source STS only
    ---------------------------------------------------------------------------------------
    PROCEDURE update_temp_data (p_group_id VARCHAR2) IS
    PRAGMA AUTONOMOUS_TRANSACTION;
-- Added by TM 15-Feb-2017 : To get setup instead of hardcoding
		CURSOR c_config IS
			SELECT * FROM fnd_lookup_values
			WHERE lookup_type = 'TAC_OU_VSITE_MAP'
--			AND attribute1 = 'Y'
			ORDER BY lookup_code;
		v_rows_updated		NUMBER	:=	0;
-- End Added by TM 15-Feb-2017 : To get setup instead of hardcoding
    BEGIN
-- Editted by TM 15-Feb-2017 : To get setup instead of hardcoding
/*
       -- Update record where DIST_CODE_CONCATINATED start with '08'
       UPDATE dtac_ap_invoices_temp_int a
       SET doc_category_code = 'DTACN-IN'
           ,reporting_entity = '10050'
           ,vendor_site_code = 'DTN'
           ,act_vendor_site_code = 'N' || to_char(to_number(substr(act_vendor_site_code, 3, 999)),'fm900')
       WHERE dist_code_concatinated LIKE '08%'
           AND status = 'NEW'
           AND SOURCE = 'STS'
           AND group_id = p_group_id
         AND act_vendor_site_code LIKE 'H%';

       -- Update record where DIST_CODE_CONCATINATED start with '12'
       UPDATE dtac_ap_invoices_temp_int a
       SET doc_category_code = 'INPB'
           ,reporting_entity = '10094'
           ,vendor_site_code = 'PSB'
           ,act_vendor_site_code = 'P' || to_char(to_number(substr(act_vendor_site_code, 3, 999)),'fm900')
       WHERE dist_code_concatinated LIKE '12%'
           AND SOURCE = 'STS'
           AND status = 'NEW'
           AND group_id = p_group_id
         AND act_vendor_site_code LIKE 'H%';
*/
		g_error_flag	:=	FALSE;
		FOR r_data IN	(SELECT	COUNT(*) total_rows, COUNT(DECODE(status,'NEW',0,1)) processed_rows
						FROM	dtac_ap_invoices_temp_int
						WHERE	SOURCE		=	'STS'
						AND		group_id	=	p_group_id)
		LOOP
			FOR r_config IN c_config
			LOOP
				IF r_config.attribute1 = 'Y' THEN
					UPDATE	dtac_ap_invoices_temp_int a
					SET		doc_category_code		=	r_config.attribute7
							,reporting_entity		=	r_config.attribute5
							,vendor_site_code		=	r_config.attribute6
							,act_vendor_site_code	=	r_config.attribute3 || to_char(to_number(substr(act_vendor_site_code, 3)),'fm999900')
					WHERE dist_code_concatinated LIKE r_config.attribute4 || '%'
					AND status = 'NEW'
					AND SOURCE = 'STS'
					AND group_id = p_group_id
					AND act_vendor_site_code LIKE r_config.attribute2;
					v_rows_updated	:=	v_rows_updated + SQL%ROWCOUNT;	-- Count rows updated
				ELSE
					SELECT	v_rows_updated + COUNT(*)
					INTO	v_rows_updated
					FROM	dtac_ap_invoices_temp_int
					WHERE	dist_code_concatinated LIKE r_config.attribute4 || '%'
					AND		status = 'NEW'
					AND		SOURCE = 'STS'
					AND		group_id = p_group_id
					AND		act_vendor_site_code LIKE r_config.attribute2;
				END IF;
			END LOOP;
			IF r_data.total_rows <> v_rows_updated AND r_data.processed_rows = 0 THEN
				g_error_flag	:=	TRUE;
				write_log('Legal Entity specified in Distribution code combination or Line Supplier Site Code is not included in the TAC_OU_VSITE_MAP lookup DFF defined.');
			END IF;
		END LOOP;
-- End Editted by TM 15-Feb-2017 : To get setup instead of hardcoding

    COMMIT;
    EXCEPTION
       WHEN OTHERS THEN
           write_log('TAC_AP_INTERFACE_UTIL.update_temp_data error.');
    END update_temp_data;
    ---------------------------------------------------------------------------------------


    ---------------------------------------------------------------------------------------
    PROCEDURE conc_wait(param_req NUMBER) IS
        v_result VARCHAR2(1);
        phase VARCHAR2(30);
        status VARCHAR2(30);
        dev_phase VARCHAR2(30);
        dev_status VARCHAR2(30);
        message VARCHAR2(1000);
    BEGIN
        COMMIT;

        IF fnd_concurrent.wait_for_request(param_req, 10, 0, phase, status, dev_phase, dev_status, message) THEN
            NULL;
        END IF;
    END conc_wait;
    --conc_wait Add by Naj 19-jan-2009

    ---------------------------------------------------------------------------------------
    PROCEDURE insert_invoice(ir_invoice IN ap_invoices_interface%ROWTYPE) IS
    BEGIN
        INSERT INTO ap_invoices_interface
            (invoice_id
            ,invoice_num
            ,invoice_type_lookup_code
            ,invoice_date
            ,po_number
            ,vendor_id
            ,vendor_num
            ,vendor_name
            ,vendor_site_id
            ,vendor_site_code
            ,invoice_amount
            ,invoice_currency_code
            ,exchange_rate
            ,exchange_rate_type
            ,exchange_date
            ,terms_id
            ,terms_name
            ,description
            ,awt_group_id
            ,awt_group_name
            ,last_update_date
            ,last_updated_by
            ,last_update_login
            ,creation_date
            ,created_by
            ,attribute_category
            ,attribute1
            ,attribute2
            ,attribute3
            ,attribute4
            ,attribute5
            ,attribute6
            ,attribute7
            ,attribute8
            ,attribute9
            ,attribute10
            ,attribute11
            ,attribute12
            ,attribute13
            ,attribute14
            ,attribute15
            ,global_attribute_category
            ,global_attribute1
            ,global_attribute2
            ,global_attribute3
            ,global_attribute4
            ,global_attribute5
            ,global_attribute6
            ,global_attribute7
            ,global_attribute8
            ,global_attribute9
            ,global_attribute10
            ,global_attribute11
            ,global_attribute12
            ,global_attribute13
            ,global_attribute14
            ,global_attribute15
            ,global_attribute16
            ,global_attribute17
            ,global_attribute18
            ,global_attribute19
            ,global_attribute20
            ,status
            ,SOURCE
            ,group_id
            ,request_id
            ,payment_cross_rate_type
            ,payment_cross_rate_date
            ,payment_cross_rate
            ,payment_currency_code
            ,workflow_flag
            ,doc_category_code
            ,voucher_num
            ,payment_method_lookup_code
            ,pay_group_lookup_code
            ,goods_received_date
            ,invoice_received_date
            ,gl_date
            ,accts_pay_code_combination_id
            ,ussgl_transaction_code
            ,exclusive_payment_flag
            ,org_id
            ,amount_applicable_to_discount
            ,prepay_num
            ,prepay_dist_num
            ,prepay_apply_amount
            ,prepay_gl_date
            ,invoice_includes_prepay_flag
            ,no_xrate_base_amount
            ,vendor_email_address
            ,terms_date
            ,requester_id
            ,ship_to_location
            ,external_doc_ref)
        VALUES
            (ir_invoice.invoice_id
            ,ir_invoice.invoice_num
            ,ir_invoice.invoice_type_lookup_code
            ,ir_invoice.invoice_date
            ,ir_invoice.po_number
            ,ir_invoice.vendor_id
            ,ir_invoice.vendor_num
            ,ir_invoice.vendor_name
            ,ir_invoice.vendor_site_id
            ,ir_invoice.vendor_site_code
            ,ir_invoice.invoice_amount
            ,ir_invoice.invoice_currency_code
            ,ir_invoice.exchange_rate
            ,ir_invoice.exchange_rate_type
            ,ir_invoice.exchange_date
            ,ir_invoice.terms_id
            ,ir_invoice.terms_name
            ,ir_invoice.description
            ,ir_invoice.awt_group_id
            ,ir_invoice.awt_group_name
            ,ir_invoice.last_update_date
            ,ir_invoice.last_updated_by
            ,ir_invoice.last_update_login
            ,ir_invoice.creation_date
            ,ir_invoice.created_by
            ,ir_invoice.attribute_category
            ,ir_invoice.attribute1
            ,ir_invoice.attribute2
            ,ir_invoice.attribute3
            ,ir_invoice.attribute4
            ,ir_invoice.attribute5
            ,ir_invoice.attribute6
            ,ir_invoice.attribute7
            ,ir_invoice.attribute8
            ,ir_invoice.attribute9
            ,ir_invoice.attribute10
            ,ir_invoice.attribute11
            ,ir_invoice.attribute12
            ,ir_invoice.attribute13
            ,ir_invoice.attribute14
            ,ir_invoice.attribute15
            ,ir_invoice.global_attribute_category
            ,ir_invoice.global_attribute1
            ,ir_invoice.global_attribute2
            ,ir_invoice.global_attribute3
            ,ir_invoice.global_attribute4
            ,ir_invoice.global_attribute5
            ,ir_invoice.global_attribute6
            ,ir_invoice.global_attribute7
            ,ir_invoice.global_attribute8
            ,ir_invoice.global_attribute9
            ,ir_invoice.global_attribute10
            ,ir_invoice.global_attribute11
            ,ir_invoice.global_attribute12
            ,ir_invoice.global_attribute13
            ,ir_invoice.global_attribute14
            ,ir_invoice.global_attribute15
            ,ir_invoice.global_attribute16
            ,ir_invoice.global_attribute17
            ,ir_invoice.global_attribute18
            ,ir_invoice.global_attribute19
            ,ir_invoice.global_attribute20
            ,ir_invoice.status
            ,ir_invoice.source
            ,ir_invoice.group_id
            ,ir_invoice.request_id
            ,ir_invoice.payment_cross_rate_type
            ,ir_invoice.payment_cross_rate_date
            ,ir_invoice.payment_cross_rate
            ,ir_invoice.payment_currency_code
            ,ir_invoice.workflow_flag
            ,ir_invoice.doc_category_code
            ,ir_invoice.voucher_num
            ,ir_invoice.payment_method_lookup_code
            ,ir_invoice.pay_group_lookup_code
            ,ir_invoice.goods_received_date
            ,ir_invoice.invoice_received_date
            ,ir_invoice.gl_date
            ,ir_invoice.accts_pay_code_combination_id
            ,ir_invoice.ussgl_transaction_code
            ,ir_invoice.exclusive_payment_flag
            ,ir_invoice.org_id
            ,ir_invoice.amount_applicable_to_discount
            ,ir_invoice.prepay_num
            ,ir_invoice.prepay_dist_num
            ,ir_invoice.prepay_apply_amount
            ,ir_invoice.prepay_gl_date
            ,ir_invoice.invoice_includes_prepay_flag
            ,ir_invoice.no_xrate_base_amount
            ,ir_invoice.vendor_email_address
            ,ir_invoice.terms_date
            ,ir_invoice.requester_id
            ,ir_invoice.ship_to_location
            ,ir_invoice.external_doc_ref);
    END insert_invoice;

    ------------------------------------------------------------------------------
    PROCEDURE insert_invoice_line(ir_invoice_line IN ap_invoice_lines_interface%ROWTYPE) IS
    BEGIN
        INSERT INTO ap_invoice_lines_interface
            (invoice_id
            ,invoice_line_id
            ,line_number
            ,line_type_lookup_code
            ,line_group_number
            ,amount
            ,accounting_date
            ,description
            ,amount_includes_tax_flag
            ,prorate_across_flag
            ,tax_code
            ,tax_code_id
            ,tax_code_override_flag
            ,tax_recovery_rate
            ,tax_recovery_override_flag
            ,tax_recoverable_flag
            ,final_match_flag
            ,po_header_id
            ,po_line_id
            ,po_line_location_id
            ,po_distribution_id
            ,po_unit_of_measure
            ,inventory_item_id
            ,quantity_invoiced
            ,unit_price
            ,distribution_set_id
            ,dist_code_concatenated
            ,dist_code_combination_id
            ,awt_group_id
            ,attribute_category
            ,attribute1
            ,attribute2
            ,attribute3
            ,attribute4
            ,attribute5
            ,attribute6
            ,attribute7
            ,attribute8
            ,attribute9
            ,attribute10
            ,attribute11
            ,attribute12
            ,attribute13
            ,attribute14
            ,attribute15
            ,global_attribute_category
            ,global_attribute1
            ,global_attribute2
            ,global_attribute3
            ,global_attribute4
            ,global_attribute5
            ,global_attribute6
            ,global_attribute7
            ,global_attribute8
            ,global_attribute9
            ,global_attribute10
            ,global_attribute11
            ,global_attribute12
            ,global_attribute13
            ,global_attribute14
            ,global_attribute15
            ,global_attribute16
            ,global_attribute17
            ,global_attribute18
            ,global_attribute19
            ,global_attribute20
            ,po_release_id
            ,balancing_segment
            ,cost_center_segment
            ,account_segment
            ,project_id
            ,task_id
            ,expenditure_type
            ,expenditure_item_date
            ,expenditure_organization_id
            ,project_accounting_context
            ,pa_addition_flag
            ,pa_quantity
            ,stat_amount
            ,type_1099
            ,income_tax_region
            ,assets_tracking_flag
            ,price_correction_flag
            ,ussgl_transaction_code
            ,receipt_number
            ,match_option
            ,rcv_transaction_id
            ,creation_date
            ,created_by
            ,last_update_date
            ,last_updated_by
            ,last_update_login
            ,org_id
            ,award_id
            ,price_correct_inv_num)
        VALUES
            (ir_invoice_line.invoice_id
            ,ir_invoice_line.invoice_line_id
            ,ir_invoice_line.line_number
            ,ir_invoice_line.line_type_lookup_code
            ,ir_invoice_line.line_group_number
            ,ir_invoice_line.amount
            ,ir_invoice_line.accounting_date
            ,ir_invoice_line.description
            ,ir_invoice_line.amount_includes_tax_flag
            ,ir_invoice_line.prorate_across_flag
            ,ir_invoice_line.tax_code
            ,ir_invoice_line.tax_code_id
            ,ir_invoice_line.tax_code_override_flag
            ,ir_invoice_line.tax_recovery_rate
            ,ir_invoice_line.tax_recovery_override_flag
            ,ir_invoice_line.tax_recoverable_flag
            ,ir_invoice_line.final_match_flag
            ,ir_invoice_line.po_header_id
            ,ir_invoice_line.po_line_id
            ,ir_invoice_line.po_line_location_id
            ,ir_invoice_line.po_distribution_id
            ,ir_invoice_line.po_unit_of_measure
            ,ir_invoice_line.inventory_item_id
            ,ir_invoice_line.quantity_invoiced
            ,ir_invoice_line.unit_price
            ,ir_invoice_line.distribution_set_id
            ,ir_invoice_line.dist_code_concatenated
            ,ir_invoice_line.dist_code_combination_id
            ,ir_invoice_line.awt_group_id
            ,ir_invoice_line.attribute_category
            ,ir_invoice_line.attribute1
            ,ir_invoice_line.attribute2
            ,ir_invoice_line.attribute3
            ,ir_invoice_line.attribute4
            ,ir_invoice_line.attribute5
            ,ir_invoice_line.attribute6
            ,ir_invoice_line.attribute7
            ,ir_invoice_line.attribute8
            ,ir_invoice_line.attribute9
            ,ir_invoice_line.attribute10
            ,ir_invoice_line.attribute11
            ,ir_invoice_line.attribute12
            ,ir_invoice_line.attribute13
            ,ir_invoice_line.attribute14
            ,ir_invoice_line.attribute15
            ,ir_invoice_line.global_attribute_category
            ,ir_invoice_line.global_attribute1
            ,ir_invoice_line.global_attribute2
            ,ir_invoice_line.global_attribute3
            ,ir_invoice_line.global_attribute4
            ,ir_invoice_line.global_attribute5
            ,ir_invoice_line.global_attribute6
            ,ir_invoice_line.global_attribute7
            ,ir_invoice_line.global_attribute8
            ,ir_invoice_line.global_attribute9
            ,ir_invoice_line.global_attribute10
            ,ir_invoice_line.global_attribute11
            ,ir_invoice_line.global_attribute12
            ,ir_invoice_line.global_attribute13
            ,ir_invoice_line.global_attribute14
            ,ir_invoice_line.global_attribute15
            ,ir_invoice_line.global_attribute16
            ,ir_invoice_line.global_attribute17
            ,ir_invoice_line.global_attribute18
            ,ir_invoice_line.global_attribute19
            ,ir_invoice_line.global_attribute20
            ,ir_invoice_line.po_release_id
            ,ir_invoice_line.balancing_segment
            ,ir_invoice_line.cost_center_segment
            ,ir_invoice_line.account_segment
            ,ir_invoice_line.project_id
            ,ir_invoice_line.task_id
            ,ir_invoice_line.expenditure_type
            ,ir_invoice_line.expenditure_item_date
            ,ir_invoice_line.expenditure_organization_id
            ,ir_invoice_line.project_accounting_context
            ,ir_invoice_line.pa_addition_flag
            ,ir_invoice_line.pa_quantity
            ,ir_invoice_line.stat_amount
            ,ir_invoice_line.type_1099
            ,ir_invoice_line.income_tax_region
            ,ir_invoice_line.assets_tracking_flag
            ,ir_invoice_line.price_correction_flag
            ,ir_invoice_line.ussgl_transaction_code
            ,ir_invoice_line.receipt_number
            ,ir_invoice_line.match_option
            ,ir_invoice_line.rcv_transaction_id
            ,ir_invoice_line.creation_date
            ,ir_invoice_line.created_by
            ,ir_invoice_line.last_update_date
            ,ir_invoice_line.last_updated_by
            ,ir_invoice_line.last_update_login
            ,ir_invoice_line.org_id
            ,ir_invoice_line.award_id
            ,ir_invoice_line.price_correct_inv_num);
    END insert_invoice_line;

    ------------------------------------------------------------------------------
    --Modified by AP@BAS on 07-Feb-2013 : add parameter i_acc_ou_segment
    PROCEDURE update_status
    (
        i_inv_num VARCHAR2
        ,i_acc_ou_segment VARCHAR2 := '01'
       ,i_line_num NUMBER := NULL
       ,i_status VARCHAR2
    ) IS
        PRAGMA AUTONOMOUS_TRANSACTION;
    BEGIN
        UPDATE dtac_ap_invoices_temp_int
        SET status = i_status
        WHERE 1=1
            AND invoice_num = i_inv_num
            -- Added by AP@BAS on 07-Feb-2013 : add criteria to filter record for update
            AND dist_code_concatinated LIKE i_acc_ou_segment||'.%'
            --
              AND line_number = nvl(i_line_num, line_number);
        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            write_log('Can not update interface status!');
            ROLLBACK;
    END update_status;

    ------------------------------------------------------------------------------
    PROCEDURE update_template
    (
        i_temp_id IN NUMBER
       ,i_next_period IN VARCHAR2
       ,i_next_payment IN NUMBER
       ,i_period_rem IN NUMBER
       ,i_line_amount IN NUMBER
    ) IS
    BEGIN
        UPDATE ap_recurring_payments_all rp
        SET rp.next_period = i_next_period
           ,rp.next_payment = i_next_payment
           ,rp.num_of_periods_rem = i_period_rem
           ,rp.released_amount = rp.released_amount + i_line_amount
        WHERE rp.recurring_payment_id = i_temp_id;
    EXCEPTION
        WHEN OTHERS THEN
            fnd_file.put_line(fnd_file.log, 'Can not update recurring template ');
    END update_template;

    ------------------------------------------------------------------------------
    --added by AP@BAS 07-Feb-2013 : mapping org_id to acc_ou_code
    ------------------------------------------------------------------------------
    FUNCTION get_mapping_org_id_acccode(p_org_id NUMBER) RETURN VARCHAR2 IS
    BEGIN
-- Editted by TM 15-Feb-2017 : To get setup instead of hardcoding
/*
       IF (p_org_id = 142) THEN
           RETURN '08';
        ELSIF (p_org_id = 218) THEN
           RETURN '12';
        ELSIF (p_org_id = 238) THEN
           RETURN '12';
        ELSE
           RETURN '01';
       END IF;
*/
		FOR rec IN (SELECT * FROM fnd_lookup_values WHERE lookup_type = 'TAC_OU_VSITE_MAP' AND attribute8 = p_org_id)
		LOOP
			IF rec.attribute4 IS NOT NULL THEN
				RETURN rec.attribute4;
			END IF;
		END LOOP;
		RETURN '01';	-- Default value
-- End Editted by TM 15-Feb-2017 : To get setup instead of hardcoding
    EXCEPTION
       WHEN OTHERS THEN
           RETURN '01';
    END get_mapping_org_id_acccode;
    ------------------------------------------------------------------------------
-- Added by TM 15-Feb-2017 : To get setup instead of hardcoding
    FUNCTION get_mapping_acccode_org_id(p_acc_code VARCHAR2, p_org_id NUMBER) RETURN NUMBER IS
    BEGIN
		FOR rec IN (SELECT * FROM fnd_lookup_values WHERE lookup_type = 'TAC_OU_VSITE_MAP' AND attribute4 = p_acc_code)
		LOOP
			IF rec.attribute8 IS NOT NULL THEN
				RETURN rec.attribute8;
			END IF;
		END LOOP;
		RETURN p_org_id;	-- Default value
    EXCEPTION
       WHEN OTHERS THEN
           RETURN p_org_id;
    END get_mapping_acccode_org_id;
-- End Added by TM 15-Feb-2017 : To get setup instead of hardcoding
    ------------------------------------------------------------------------------
    PROCEDURE interface_ap
    (
        err_msg OUT VARCHAR2
       ,err_code OUT VARCHAR2
       ,i_org_id IN NUMBER
       ,i_batch IN VARCHAR2
       ,i_group IN VARCHAR2
    ) IS

       v_mapped_org_id_acc_code VARCHAR2(10);
        CURSOR c_inv IS
            SELECT t.vendor_num
                  ,t.group_id
                  ,t.source
                  ,t.invoice_type_lookup_code
                  ,t.vendor_site_code
                  ,t.invoice_num
                  ,t.invoice_date
                  ,t.doc_category_code
                  ,t.invoice_currency_code
                  ,0 invoice_amount
                  ,t.gl_date
                  ,t.invoice_description
                  ,t.terms_name
                  ,t.payment_method_lookup_code
                  ,t.employee_number

                  -- Added by AP@BAS on 07-Feb-2013 : for separate transaction by OU (01, 08, 12)
                  ,substr(t.dist_code_concatinated,1, instr(t.dist_code_concatinated,'.',1)-1) acc_ou_segment
                  --
                  ,COUNT('x') num_of_dist
            FROM dtac_ap_invoices_temp_int t
            WHERE t.status = 'NEW'
                  AND t.group_id = i_group
                  AND t.dist_code_concatinated LIKE  v_mapped_org_id_acc_code
            GROUP BY t.vendor_num
                    ,t.group_id
                    ,t.source
                    ,t.invoice_type_lookup_code
                    ,t.vendor_site_code
                    ,t.invoice_num
                    ,t.invoice_date
                    ,t.doc_category_code
                    ,t.invoice_currency_code
                    ,t.gl_date
                    ,t.invoice_description
                    ,t.terms_name
                    ,t.payment_method_lookup_code
                    ,t.employee_number
                    -- Added by AP@BAS on 07-Feb-2013
                    ,substr(t.dist_code_concatinated,1, instr(t.dist_code_concatinated,'.',1)-1);

        -- Modify by AP@BAS on 07-Feb-2013 : add parameter i_acc_ou_segment
        CURSOR c_line(i_inv_num IN VARCHAR2, i_acc_ou_segment VARCHAR2 DEFAULT '01') IS
            SELECT l.group_id
                  ,l.source
                  ,l.invoice_type_lookup_code
                  ,l.vendor_num
                  ,l.vendor_site_code
                  ,l.invoice_num
                  ,l.invoice_date
                  ,l.doc_category_code
                  ,l.invoice_currency_code
                  ,l.invoice_amount
                  ,l.gl_date
                  ,l.invoice_description
                  ,l.terms_name
                  ,l.payment_method_lookup_code
                  ,l.employee_number
                  ,l.line_number
                  ,l.line_type_lookup_code
                  ,l.line_amount
                  ,l.line_tax_code
                  ,l.awt_group_name
                  ,l.dist_code_concatinated
                  ,l.line_description
                  ,l.act_vendor_num
                  ,l.act_vendor_site_code
                  ,l.reporting_entity
                  ,l.tax_invoice_date
                  ,l.tax_invoice_num
                  ,l.tax_acct_period
                  ,l.wht_acct_period
                  ,l.phor_ngor_dor
                  ,l.wht_condition
                  ,l.wht_revenue_type
                  ,l.wht_revenue_name
                  ,l.project_number
                  ,l.project_task
                  ,l.expenditure_type
                  ,l.expenditure_organization
                  ,l.status
                  ,l.eai_crtd_dttm
                  ,l.payee
                  ,l.mail_to_name
                  ,l.mail_to_address1
                  ,l.mail_to_address2
                  ,l.mail_to_address3
                  ,l.mail_to_address4
                  ,l.receipt_number
                  ,l.match_option
                  ,l.bill_placement_date
                  ,l.po_number
                  ,l.resend_flag
                  ,l.error_msg
                  ,l.seqno
                  ,ppa.project_id --,pt.task_id
                  ,hou.organization_id
-- Add by TM 01/03/2017 : To validate project expire
				  ,ppa.start_date project_start_date
				  ,ppa.completion_date project_completion_date
-- End Add by TM 01/03/2017 : To validate project expire
            FROM dtac_ap_invoices_temp_int l
                ,pa_projects_all ppa --,PA_TASKS pt
                ,hr_organization_units hou
            WHERE l.invoice_num = i_inv_num
                  AND l.status = 'NEW'
                  AND l.dist_code_concatinated LIKE i_acc_ou_segment||'.%'
                  AND l.project_number = ppa.segment1(+)
                  AND l.expenditure_organization = hou.name(+)
            ORDER BY l.line_number;

        r_invoice ap_invoices_interface%ROWTYPE;
        r_invoice_line ap_invoice_lines_interface%ROWTYPE;
        v_group VARCHAR2(80);
        v_source VARCHAR2(80);

        v_rate NUMBER;
        v_tax_ccid NUMBER;
        v_line_num NUMBER;
        v_site_auto_calc_flag VARCHAR2(10);
        v_supp_name VARCHAR2(100);
        v_period VARCHAR2(30);
        v_proj_id NUMBER(20);
        v_task_id NUMBER(20);
        v_expen_type VARCHAR(100);
        v_expen_org NUMBER(20);
        v_flag BOOLEAN := FALSE;
        v_chk_line BOOLEAN := FALSE;
        v_validate_error BOOLEAN := FALSE;
        ap_req_id NUMBER;
        c_ap_req_id NUMBER;
        v_i_group VARCHAR2(80);
        v_ap_req_id NUMBER;
        v_line_count NUMBER := 0;
        v_site po_vendor_sites%ROWTYPE;
        v_save_pnt VARCHAR2(100);
        v_tax_rate NUMBER;
        v_requester_id NUMBER;

        v_count NUMBER := 0; -- Added by Korn.iCE on 20-OCT-2009.
        v_total_line_tax_amt NUMBER := 0; -- Added by Korn.iCE on 20-OCT-2009.

        v_line_status_info VARCHAR2(32000);
        v_line_error VARCHAR2(32000);
        v_header_error VARCHAR2(32000);

        v_count_invalid_header NUMBER := 0;
        v_inv_line_error_list VARCHAR2(10000);
        V_DEBUG NUMBER := 0 ;
    BEGIN
        --Init
        SAVEPOINT b4_process;
        write_log(' ');
        write_log('+----------------------------------------------------------------------------+');
        write_log('Start Concurrent AP Interface from STS ');
        write_log('Start Date: ' || to_char(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
        write_log(' ');
        write_log('Agruments');
        write_log('-----------');
        write_log('Org ID: ' || i_org_id);
        write_log('Batch Name: ' || i_batch);
        write_log('Group: ' || i_group);
        write_log('+----------------------------------------------------------------------------+');

        /*
        ** added by AP@BAS on 30-Aug-2012
        ** for delete old transaction with 45 days
        */
        delete_temp_table(45);
        /*
        ** end added
        */

V_DEBUG := 10;

        -- Added by AP@BAS on 07-Feb-2013 : Update data depend on combination code (08.xxx, 12.xxx)
        update_temp_data(i_group);
-- Added by TM 15-Feb-2017 : To get setup instead of hardcoding
		IF g_error_flag THEN
            err_code := 2;
            err_msg := 'Invoice information invalid. See log for detail.';
            RETURN;
		END IF;
-- End Added by TM 15-Feb-2017 : To get setup instead of hardcoding
        --

        --- Start added by Korn.iCE on 18-NOV-2009.
        --- Cause: System will create many records in AP_Chrg_Allocations_All and take a long time for run Invoice Validation.
        --- Purpose: Due to system can automatically calcucate tax by allocation then need not to import Line Tax for STS Invoice.
        SELECT COUNT(*), SUM(line_amount)
        INTO v_count, v_total_line_tax_amt
        FROM wmuser.dtac_ap_invoices_temp_int
        WHERE 1=1
              AND group_id = i_group
              AND status = 'NEW'
              AND line_type_lookup_code = 'TAX';
        BEGIN
            DELETE FROM wmuser.dtac_ap_invoices_temp_int
            WHERE 1=1
                  AND group_id = i_group
                  AND status = 'NEW'
                  AND line_type_lookup_code = 'TAX';

            COMMIT;
            write_log('Delete ' || v_count || ' Line(s) TAX for STS Invoice.');
            write_log('Total Line TAX Amount = ' || v_total_line_tax_amt);

        EXCEPTION
            WHEN OTHERS THEN
                write_log('!!! Package TAC_AP_INTERFACE_UTIL.Interface_AP');
                write_log('!!! Error MSG: ' || SQLCODE || ' - ' || SQLERRM);
        END;
        write_log('+----------------------------------------------------------------------------+');
        
V_DEBUG := 20;

        -- Modigy BY AP@BAS on 07-Feb-2013 : add group by substr(t.dist_code_concatinated,1, instr(t.dist_code_concatinated,'.',1)-1)
        /*
        ** Modify by AP@BAS on 27-Aug-2012
        ** for check duplicate invoice information e.g. invoice description, employee number.
        */
        write_log('');
        v_mapped_org_id_acc_code := get_mapping_org_id_acccode(i_org_id) ||'.%';
        write_log('v_mapped_org_id_acc_code = ' || v_mapped_org_id_acc_code);
        FOR r_inv_h IN (
            SELECT t.vendor_num
                  ,t.vendor_site_code
                  ,t.invoice_num
                  ,substr(t.dist_code_concatinated,1, instr(t.dist_code_concatinated,'.',1)-1) acc_ou_segment
                  ,COUNT(DISTINCT t.invoice_date) dup_invoice_date
                  ,COUNT(DISTINCT t.doc_category_code) dup_doc_category_code
                  ,COUNT(DISTINCT t.invoice_currency_code) dup_invoice_currency_code
                  ,COUNT(DISTINCT t.gl_date) dup_gl_date
                  ,COUNT(DISTINCT t.invoice_description) dup_invoice_description
                  ,COUNT(DISTINCT t.terms_name) dup_terms_name
                  ,COUNT(DISTINCT t.payment_method_lookup_code) dup_payment_method_lookup_code
                  ,COUNT(DISTINCT t.employee_number) dup_employee_number
            FROM dtac_ap_invoices_temp_int t
            WHERE t.status = 'NEW'
                  AND t.group_id = i_group
            GROUP BY t.vendor_num
                  ,t.vendor_site_code
                  ,t.invoice_num
                  ,substr(t.dist_code_concatinated,1, instr(t.dist_code_concatinated,'.',1)-1)
            HAVING COUNT(DISTINCT t.invoice_date) > 1
                  OR COUNT(DISTINCT t.doc_category_code) > 1
                  OR COUNT(DISTINCT t.invoice_currency_code) > 1
                  OR COUNT(DISTINCT t.gl_date) > 1
                  OR COUNT(DISTINCT t.invoice_description) > 1
                  OR COUNT(DISTINCT t.terms_name) > 1
                  OR COUNT(DISTINCT t.payment_method_lookup_code) >1
                  OR COUNT(DISTINCT t.employee_number) > 1
        )LOOP
        
V_DEBUG := 30;

            v_count_invalid_header := v_count_invalid_header + 1;
            v_header_error := 'Invoice number ('|| r_inv_h.invoice_num || ') error with ';
            IF r_inv_h.dup_invoice_date > 1 THEN
                v_header_error := v_header_error || '|' || 'multiple invoice date,';
            END IF;

            IF r_inv_h.dup_doc_category_code > 1 THEN
                v_header_error := v_header_error || '|' || 'multiple document category,';
            END IF;

            IF r_inv_h.dup_invoice_currency_code > 1 THEN
                v_header_error := v_header_error || '|' || 'multiple currency code,';
            END IF;

            IF r_inv_h.dup_gl_date > 1 THEN
                v_header_error := v_header_error || '|' || 'multiple GL date,';
            END IF;

            IF r_inv_h.dup_invoice_description > 1 THEN
                v_header_error := v_header_error || '|' || 'multiple invoice description,';
            END IF;

            IF r_inv_h.dup_terms_name > 1 THEN
                v_header_error := v_header_error || '|' || 'multiple term,';
            END IF;

            IF r_inv_h.dup_payment_method_lookup_code > 1 THEN
                v_header_error := v_header_error || '|' || 'multiple payment method,';
            END IF;

            IF r_inv_h.dup_employee_number > 1 THEN
                v_header_error := v_header_error || '|' || 'multiple requester,';
            END IF;
            write_log(v_header_error);
        END LOOP;

V_DEBUG := 40;

        IF v_count_invalid_header > 0 THEN
            write_log('');
            err_code := 2;
            err_msg := 'Invoice information invalid. See log for detail.';
            RETURN;
        END IF;
        v_header_error := '';
        /*
        ** End modify by AP@BAS on 27-Aug-2012
        */

        FOR r_inv IN c_inv
        LOOP
            v_save_pnt := r_inv.invoice_num || r_inv.vendor_num;
            SAVEPOINT v_save_pnt;

            r_invoice := NULL;
            v_flag := FALSE;
            
V_DEBUG := 50;

            write_log('Invoice Number : ' || r_inv.invoice_num);
            v_group := r_inv.group_id;
            v_source := r_inv.source;
            /** AP_INVOICES_INTERFACE **/
            --SEQ
            SELECT ap_invoices_interface_s.nextval INTO r_invoice.invoice_id FROM dual;

            --Supplier
            BEGIN
                SELECT v.vendor_id INTO r_invoice.vendor_id
                FROM po_vendors v
                WHERE v.segment1 = r_inv.vendor_num;
            EXCEPTION
                WHEN no_data_found THEN
                    v_flag := TRUE;
                    write_log('*Invalid Supplier : ' || r_inv.vendor_num);
                    err_msg := '*** DATA ERROR : Interface data is invalid!';
                    err_code := '1';
            END;
V_DEBUG := 60;
            --Site---
            BEGIN
                SELECT
                 vs.*
                INTO v_site
                FROM po_vendor_sites vs
                WHERE vs.vendor_site_code = r_inv.vendor_site_code
                      AND vs.vendor_id = r_invoice.vendor_id;
            EXCEPTION
                WHEN no_data_found THEN
                    v_flag := TRUE;

                    write_log('*Invalid Supplier Site : ' || r_inv.vendor_site_code);
                    write_log('*Supplier Number : ' || r_inv.vendor_num);
                    err_msg := '*** DATA ERROR : Interface data is invalid!';
                    err_code := '1';
                    v_site := NULL;
            END;
            
V_DEBUG := 70;

            r_invoice.vendor_site_id := v_site.vendor_site_id;
            r_invoice.accts_pay_code_combination_id := v_site.accts_pay_code_combination_id;
            v_site_auto_calc_flag := v_site.auto_tax_calc_flag;

            --Term
            IF r_inv.terms_name IS NOT NULL THEN
                BEGIN
                    SELECT t.name, t.creation_date
                    INTO r_invoice.terms_name, r_invoice.terms_date
                    FROM ap_terms t
                    WHERE t.term_id = r_inv.terms_name;
                EXCEPTION
                    WHEN no_data_found THEN
                        v_flag := TRUE;
                        write_log('*Invalid Term Name : ' || r_inv.terms_name);
                        err_msg := '*** DATA ERROR : Interface data is invalid!';
                        err_code := '1';
                END;
            END IF;
            v_requester_id := 0;
            
V_DEBUG := 80;

            BEGIN
                SELECT person_id
                INTO v_requester_id
                FROM per_people_f
                WHERE employee_number = to_char(r_inv.employee_number)
                      AND ((trunc(effective_start_date) <= trunc(SYSDATE)) AND (trunc(effective_end_date) >= trunc(SYSDATE)));
            EXCEPTION
                WHEN OTHERS THEN
                    v_flag := TRUE;
                    write_log('*Invalid Employee Number : ' || r_inv.employee_number);
                    err_msg := '*** DATA ERROR : Interface data is invalid!';
                    err_code := '1';
                    v_requester_id := 0;
            END;

V_DEBUG := 90;

            IF r_inv.gl_date IS NULL THEN
                r_invoice.gl_date := trunc(SYSDATE);
            ELSE
                r_invoice.gl_date := r_inv.gl_date;
            END IF;
            -- Validate Open GL Periods

            -- modified by AP@BAS on 07-Feb-2013 : org_id depend on dist_code_concatinated
-- Editted by TM 15-Feb-2017 : To get setup instead of hardcoding
/*
            IF (r_inv.acc_ou_segment='08') THEN
                --DTN
                r_invoice.org_id := g_dtn_org_id;
            ELSIF(r_inv.acc_ou_segment='12') THEN
                --PSB
                r_invoice.org_id := g_psb_org_id;
            ELSE
                r_invoice.org_id := i_org_id;
            END IF;
*/
			r_invoice.org_id	:=	get_mapping_acccode_org_id(r_inv.acc_ou_segment,i_org_id);
-- End Editted by TM 15-Feb-2017 : To get setup instead of hardcoding
            ---


            r_invoice.invoice_num := r_inv.invoice_num;
            r_invoice.invoice_type_lookup_code := r_inv.invoice_type_lookup_code; --'STANDARD';
            r_invoice.invoice_date := r_inv.invoice_date;
            r_invoice.vendor_num := r_inv.vendor_num;
            r_invoice.vendor_site_code := r_inv.vendor_site_code;
            r_invoice.invoice_amount := r_inv.invoice_amount;
            r_invoice.invoice_currency_code := r_inv.invoice_currency_code;
            r_invoice.source := r_inv.source;
            r_invoice.group_id := r_inv.group_id;
            r_invoice.doc_category_code := r_inv.doc_category_code;
            r_invoice.description := r_inv.invoice_description;
            r_invoice.terms_id := r_inv.terms_name;
            r_invoice.payment_method_lookup_code := r_inv.payment_method_lookup_code;
            r_invoice.creation_date := SYSDATE;
            r_invoice.last_update_date := SYSDATE;
            r_invoice.created_by := fnd_global.user_id;
            r_invoice.last_updated_by := fnd_global.user_id;
            r_invoice.request_id := fnd_global.conc_request_id;
            r_invoice.attribute_category := 'JA.TH.APXINWKB.DISTRIBUTIONS';
            r_invoice.global_attribute_category := 'JA.TH.APXINWKB.DISTRIBUTIONS';


            r_invoice.requester_id := v_requester_id;
            
V_DEBUG := 100;

            IF v_flag THEN
                update_status(i_inv_num => r_inv.invoice_num
                    , i_acc_ou_segment=>r_inv.acc_ou_segment
                    ,  i_status => 'REJECTED'
                    );
                v_validate_error := TRUE;
                
V_DEBUG := 110;

            ELSE
            
V_DEBUG := 120;
                v_line_num := 0;
                r_invoice.invoice_amount := 0;
                v_chk_line := FALSE;

                -- Modify by AP@BAS on 07-Feb-2013 : add parameter nvl(r_inv.acc_ou_segment,'01')
                write_log('r_inv.acc_ou_segment = ' || r_inv.acc_ou_segment);
                FOR r_line IN c_line(r_invoice.invoice_num, nvl(r_inv.acc_ou_segment,'01'))
                LOOP
					v_task_id	:=	NULL;	-- Add by TM 01/03/2017 : Clear Task ID
                    v_line_num := v_line_num + 1;
                    r_invoice_line := NULL;
                    v_chk_line := FALSE;

                    v_line_status_info := 'Row#' || v_line_num || ' |Invoice line#' ||r_line.line_number;

                    /** AP_INVOICE_LINES_INTERFACE **/
                    SELECT ap_invoice_lines_interface_s.nextval INTO r_invoice_line.invoice_line_id FROM dual;
                    -- Validate data for Withholding Tax
                    IF r_line.awt_group_name IS NOT NULL THEN
                        BEGIN
                            SELECT ag.group_id INTO r_invoice_line.awt_group_id
                            FROM ap_awt_groups ag
                            WHERE ag.name = r_line.awt_group_name;
                        EXCEPTION
                            WHEN no_data_found THEN
                                v_chk_line := TRUE;
                                v_line_status_info := v_line_status_info || '|Invalid Withholding Tax Group (' || r_line.awt_group_name || ')';
                                -- Uncomment by TM 01/03/2017
								write_log('*Invalid Withholding Tax Group : ' || r_line.awt_group_name);
                                err_msg := '*** DATA ERROR : Interface data is invalid!';
                                err_code := '1';
                        END;
                        
V_DEBUG := 130;
                        
                        IF r_line.wht_condition IS NULL THEN
                            v_line_status_info := v_line_status_info || '|WHT Condition is NULL';
                            --Uncomment by TM 01/03/2017
							write_log('*Incomplete Withholding Tax Information - WHT Condition is NULL');
                            v_chk_line := TRUE;
                            err_msg := '*** DATA ERROR : Interface data is invalid!';
                            err_code := '1';
                        END IF;
                        IF r_line.wht_revenue_type IS NULL THEN
                            v_line_status_info := v_line_status_info || '|WHT Revenue Type is NULL';
                            --Uncomment by TM 01/03/2017
							write_log('*Incomplete Withholding Tax Information - WHT Revenue Type is NULL');
                            v_chk_line := TRUE;
                            err_msg := '*** DATA ERROR : Interface data is invalid!';
                            err_code := '1';
                        END IF;
                        IF r_line.wht_revenue_name IS NULL THEN
                            v_line_status_info := v_line_status_info || '|WHT Revenue Name NULL';
                            --Uncomment by TM 01/03/2017
							write_log('*Incomplete Withholding Tax Information - WHT Revenue Name NULL');
                            v_chk_line := TRUE;
                            err_msg := '*** DATA ERROR : Interface data is invalid!';
                            err_code := '1';
                        END IF;
                        IF r_line.phor_ngor_dor IS NULL THEN
                            v_line_status_info := v_line_status_info || '|Phor Ngor Dor is NULL';
                            --Uncomment by TM 01/03/2017
							write_log('*Incomplete Withholding Tax Information - Phor Ngor Dor is NULL');
                            v_chk_line := TRUE;
                            err_msg := '*** DATA ERROR : Interface data is invalid!';
                            err_code := '1';
                        END IF;
                    END IF;

V_DEBUG := 140;
                    r_invoice_line.amount := r_line.line_amount;

                    IF r_line.line_tax_code IS NOT NULL THEN
                        BEGIN
                            SELECT atc.tax_code_combination_id, atc.tax_id, atc.tax_rate
                            INTO v_tax_ccid, r_invoice_line.tax_code_id, v_rate
                            FROM ap_tax_codes_all atc -- change from ap_tax_codes to ap_tax_codes_all
                            WHERE atc.enabled_flag = 'Y'
                                  AND atc.name = r_line.line_tax_code
                                  AND nvl(atc.start_date, SYSDATE) <= SYSDATE
                                  AND nvl(atc.inactive_date, SYSDATE) >= SYSDATE
                                  -- added by AP@BAS on 07-Feb-2013 : add criteria org_id
                                  AND atc.org_id = r_invoice.org_id;
                        EXCEPTION
                            WHEN OTHERS THEN
                                v_chk_line := TRUE;
                                v_line_status_info := v_line_status_info || '|Invalid Tax Code (' || r_line.line_tax_code || ')';
                                --Uncomment by TM 01/03/2017
								write_log('*Invalid Tax Code : ' || r_line.line_tax_code);
                                err_msg := '*** DATA ERROR : Interface data is invalid!';
                                err_code := '1';
                        END;
                        r_invoice_line.tax_code := r_line.line_tax_code;
                        r_invoice_line.tax_code_override_flag := 'N';
                        r_invoice_line.tax_recoverable_flag := 'Y';
V_DEBUG := 150;
                    ELSE
                        r_invoice_line.tax_code := NULL;
                        r_invoice_line.tax_recovery_rate := NULL;
V_DEBUG := 160;
                    END IF;
                    v_supp_name := NULL;

                    ---- Validation Actual Vendor
                    BEGIN
                        SELECT pv.vendor_name
                              ,rpad(pvs.address_line1 || ' ' || pvs.address_line2 || ' ' || pvs.address_line3 || ' ' || pvs.address_line4, 150)
                              ,rpad(pvs.city || ' ' || pvs.province || ' ' || pvs.zip, 150)
                              ,decode(r_line.phor_ngor_dor, '53', pvs.vat_registration_num, NULL)
                              ,decode(r_line.phor_ngor_dor, '3', pvs.vat_registration_num, NULL)
                        INTO v_supp_name
                            ,r_invoice_line.global_attribute8
                            ,r_invoice_line.global_attribute7
                            ,r_invoice_line.global_attribute6
                            ,r_invoice_line.global_attribute5
                        FROM po_vendors pv, po_vendor_sites_all pvs -- change from po_vendor_sites to po_vendor_sites_all
                        WHERE pv.segment1 = r_line.act_vendor_num
                              AND pv.vendor_id = pvs.vendor_id
                              AND pvs.vendor_site_code = r_line.act_vendor_site_code
                              -- added by AP@BAS on 07-Feb-2013 : add criteria org_id
                              AND pvs.org_id = r_invoice.org_id;
                    EXCEPTION
                        WHEN OTHERS THEN
                            v_supp_name := NULL;
                            v_chk_line := TRUE;
                            v_line_status_info := v_line_status_info || '|Actual Vendor or Site Code not found (' ||r_line.act_vendor_num || '/' || r_line.act_vendor_site_code || '])';
                            --Uncomment by TM 01/03/2017
							write_log('*Actual Vendor or Site Code not found :[Invoice-Line=' || r_invoice.invoice_num || '-' || to_char(r_line.line_number) || ',' || r_line.act_vendor_num || '/' || r_line.act_vendor_site_code || ']');
                            --Uncomment by TM 01/03/2017
							write_log('*Actual Vendor Number : ' || r_line.act_vendor_num);
                            --Uncomment by TM 01/03/2017
							write_log('*Actual Vendor Site Code : ' || r_line.act_vendor_site_code);

                            err_msg := '*** DATA ERROR : Interface data is invalid!';
                            err_code := '1';
                    END;
V_DEBUG := 170;
-- Added by TM 15-Feb-2017 : To accept NULL value in Project Code
					IF (r_line.project_number IS NULL OR r_line.project_task IS NULL) AND r_line.project_number || r_line.project_task IS NOT NULL THEN
						v_validate_error := TRUE;
						v_flag := TRUE;
						v_line_status_info := v_line_status_info || '|Invalid Project (' || r_line.project_number || ')';
						write_log('*Invalid Project/Task : ' || r_line.project_number || '/' || r_line.project_task);
						err_msg := '*** DATA ERROR : Interface data is invalid!';
						err_code := '1';
					END IF;
					
					IF TRUNC(SYSDATE) NOT BETWEEN NVL(r_line.project_start_date,TRUNC(SYSDATE)) AND NVL(r_line.project_completion_date,TRUNC(SYSDATE)) THEN
						v_validate_error := TRUE;
						v_flag := TRUE;
						v_line_status_info := v_line_status_info || '|Invalid Project Duration for Project#(' || r_line.project_number || ')';
						write_log('*Invalid Project Duration for Project# : ' || r_line.project_number);
						err_msg := '*** DATA ERROR : Interface data is invalid!';
						err_code := '1';
					END IF;
					
					IF r_line.organization_id IS NULL THEN
						v_validate_error := TRUE;
						v_flag := TRUE;
						v_line_status_info := v_line_status_info || '|Project Owner Organization is mismatched for Project#(' || r_line.project_number || ')';
						write_log('*Invalid Project Owner Organization for Project# : ' || r_line.project_number);
						err_msg := '*** DATA ERROR : Interface data is invalid!';
						err_code := '1';
					END IF;
-- End Added by TM 15-Feb-2017 : To accept NULL value in Project Code

-- Editted by TM 15-Feb-2017 : To accept NULL value in Project Code
--                    IF r_line.project_id IS NULL AND r_line.project_task IS NULL AND r_line.expenditure_type IS NULL AND
                    IF r_line.project_number IS NULL AND r_line.project_id IS NULL AND r_line.project_task IS NULL AND r_line.expenditure_type IS NULL AND
-- End Editted by TM 15-Feb-2017 : To accept NULL value in Project Code
                       r_line.organization_id IS NULL
                     THEN
                        v_validate_error := FALSE;
V_DEBUG := 180;
                    ELSE

                        -- Project Number begin add
-- Editted by TM 15-Feb-2017 : To accept NULL value in Project Code
--                        IF r_line.project_id IS NULL THEN
                        IF r_line.project_id IS NULL AND r_line.project_number IS NOT NULL THEN
-- End Editted by TM 15-Feb-2017 : To accept NULL value in Project Code
                            v_validate_error := TRUE;
                            v_flag := TRUE;
                            v_line_status_info := v_line_status_info || '|Invalid Project (' || r_line.project_number || ')';
                            write_log('*Invalid Project : ' || r_line.project_number);
                            err_msg := '*** DATA ERROR : Interface data is invalid!';
                            err_code := '1';
                            -- END;
                        END IF; ------end add

                        -- Task Number begin add
                        IF r_line.project_id IS NOT NULL THEN
                            BEGIN
                                SELECT pt.task_id
                                INTO v_task_id
                                FROM (SELECT * FROM pa_tasks pt
                                WHERE pt.project_id(+) = r_line.project_id) pt --,PA_PROJECTS_ALL ppa
                                WHERE pt.task_number = r_line.project_task;
                                -- and pt.project_id = r_line.project_id;

                            EXCEPTION
                                WHEN no_data_found THEN
                                    v_validate_error := TRUE;
                                    v_flag := TRUE;
                                    v_line_status_info := v_line_status_info || '|Invalid Task (' || r_line.project_task || ')';
                                    write_log('*Invalid Task : ' || r_line.project_task);
                                    err_msg := '*** DATA ERROR : Interface data is invalid!';
                                    err_code := '1';
                            END;
V_DEBUG := 190;
-- Commented by TM 15-Feb-2017 : To accept NULL value in Project Code
/*
                        ELSE
                            v_validate_error := TRUE;
                            v_line_status_info := v_line_status_info || '|Invalid Task (' || r_line.project_task || ')';
                            write_log('*Invalid Task : ' || r_line.project_task);
                            err_msg := '*** DATA ERROR : Interface data is invalid!';
                            err_code := '1';
*/
-- End Commented by TM 15-Feb-2017 : To accept NULL value in Project Code
                        END IF; ---end add
V_DEBUG := 200;
                        --  Expenditure Type begin add
                        BEGIN
                            SELECT pet.expenditure_type
                            INTO v_expen_type
                            FROM pa_expenditure_types pet
                            WHERE pet.expenditure_type = r_line.expenditure_type;
                        EXCEPTION
                            WHEN no_data_found THEN
                                v_validate_error := TRUE;
                                v_flag := TRUE;
                                v_line_status_info := v_line_status_info || '|Invalid Expenditure Type (' || r_line.expenditure_type || ')';
                                --Uncomment by TM 01/03/2017
								write_log('*Invalid Expenditure Type : ' || r_line.expenditure_type);
                                err_msg := '*** DATA ERROR : Interface data is invalid!';
                                err_code := '1';
                        END;
                        --END IF; ---end add
V_DEBUG := 210;
                        --  Expenditure Org begin add
                        IF r_line.organization_id IS NULL THEN
                            BEGIN
                                SELECT hou.organization_id
                                INTO v_expen_org
                                FROM hr_organization_units hou, dtac_ap_invoices_temp_int l
                                WHERE hou.organization_id = r_line.organization_id
                                      AND hou.name(+) = l.expenditure_organization;
                            EXCEPTION
                                WHEN no_data_found THEN
                                    v_validate_error := TRUE;
                                    v_flag := TRUE;
                                    v_line_status_info := v_line_status_info || '|Invalid Expenditure Organization (' || r_line.expenditure_organization || ')';
                                    --Uncomment by TM 01/03/2017
									write_log('*Invalid Expenditure Organization : ' || r_line.expenditure_organization);
                                    err_msg := '*** DATA ERROR : Interface data is invalid!';
                                    err_code := '1';
                            END;
                        END IF; ---end add
V_DEBUG := 220;
                    END IF;

                    -----
                    r_invoice_line.invoice_id := r_invoice.invoice_id;
                    -- modify by AP@BAS on 07-Feb-2013 : new assign value from i_org_id to r_invoice.org_id
                    --r_invoice_line.org_id := i_org_id;
                    r_invoice_line.org_id := r_invoice.org_id;


                    r_invoice_line.line_number := v_line_num;
                    r_invoice_line.accounting_date := r_line.gl_date;
                    r_invoice_line.project_id := r_line.project_id; ---iCE-Tae add
                    r_invoice_line.task_id := v_task_id; --r_line.task_id; ---iCE-Tae add
                    r_invoice_line.expenditure_type := r_line.expenditure_type; ---iCE-Tae add
                    r_invoice_line.expenditure_organization_id := r_line.organization_id; ---iCE-Tae add
                    r_invoice_line.expenditure_item_date := SYSDATE; ---iCE-Tae add
                    r_invoice_line.line_type_lookup_code := r_line.line_type_lookup_code; --'ITEM';
                    r_invoice_line.description := v_supp_name || ',' || r_line.line_description;
					r_invoice_line.global_attribute3 := r_line.line_description; ----Add By TM 24-Nov-2017
                    r_invoice_line.dist_code_concatenated := r_line.dist_code_concatinated;
                    r_invoice_line.awt_group_name := r_line.awt_group_name;
                    r_invoice_line.attribute4 := r_line.act_vendor_num;
                    r_invoice_line.attribute5 := r_line.act_vendor_site_code;
                    r_invoice_line.global_attribute_category := 'JA.TH.APXINWKB.DISTRIBUTIONS';

                    IF r_invoice_line.global_attribute16 IS NULL THEN
                        r_invoice_line.global_attribute16 := r_inv.source;
                    END IF;
                    IF r_line.tax_acct_period IS NULL THEN
                        r_invoice_line.attribute13 := v_period;
                    ELSE
                        r_invoice_line.attribute13 := r_line.tax_acct_period;
                    END IF;

                    r_invoice_line.attribute15 := r_line.reporting_entity;
                    r_invoice_line.global_attribute9 := v_supp_name;


                    r_invoice_line.attribute_category := 'JA.TH.APXINWKB.DISTRIBUTIONS';


                    r_invoice_line.global_attribute20 := r_line.reporting_entity;

                    IF r_line.awt_group_name IS NOT NULL THEN
                        r_invoice_line.global_attribute12 := r_line.wht_revenue_name;
                        r_invoice_line.global_attribute13 := r_line.wht_revenue_type;
                        r_invoice_line.global_attribute14 := r_line.wht_condition;
                        r_invoice_line.global_attribute15 := r_line.phor_ngor_dor;
                        r_invoice_line.global_attribute18 := r_line.wht_acct_period;
                    END IF;

                    r_invoice_line.created_by := fnd_global.user_id;
                    r_invoice_line.creation_date := SYSDATE;
V_DEBUG := 230;
                    IF v_chk_line THEN
                        --ROLLBACK TO SAVEPOINT rejected;

                        --write_log ('update dtac_ap_invoices_temp_int set status = ''PROCESSED'' where invoice_num = ''' || r_invoice.invoice_num || ''' and line_number='|| r_invoice_line.line_number ||';');
                        v_line_status_info := v_line_status_info || '| Rejected';
                        /*
                        ** remark and modify #2 by AP@BAS on 24-Aug-2012
                        ** because used r_invoice_line.line_number that running record count
                        ** fixed change r_invoice_line.line_number to r_line.line_number
                        --update_status(i_inv_num => r_invoice.invoice_num, i_line_num => r_invoice_line.line_number, i_status => 'REJECTED');
                        */
                        update_status(i_inv_num => r_invoice.invoice_num
                            , i_acc_ou_segment=>r_inv.acc_ou_segment
                            , i_line_num => r_line.line_number
                            , i_status => 'REJECTED');

                        v_validate_error := TRUE;
V_DEBUG := 240;
                    ELSE
                        IF r_line.line_type_lookup_code = 'ITEM' AND r_line.line_tax_code IS NOT NULL THEN
                            BEGIN
                                SELECT tax_rate
                                INTO v_tax_rate
                                FROM ap_tax_codes_all
                                WHERE NAME = r_line.line_tax_code
                                        -- modify by AP@BAS on 07-Feb-2013 : change criteria from i_org_id to r_invoice.org_id
                                      --and org_id = i_org_id
                                      AND org_id = r_invoice.org_id
                                      AND ((enabled_flag = 'Y') OR (enabled_flag IS NULL))
                                      AND ((trunc(start_date) <= trunc(SYSDATE)) AND (trunc(nvl(inactive_date, SYSDATE)) >= trunc(SYSDATE)));
                            EXCEPTION
                                WHEN OTHERS THEN
                                    v_tax_rate := 0;
                            END;

                            r_invoice_line.attribute9 := r_invoice_line.amount;
                            r_invoice_line.attribute8 := round(r_invoice_line.amount * (v_tax_rate / 100), 2);
                            r_invoice_line.attribute10 := v_supp_name;
                            
V_DEBUG := 250;

                        END IF;
                        /*
                        ** Modify by AP@BAS on 18-Sep-2012
                        ** not insert insert_invoice_line if have error accured atlease one error
                        --write_out('Invoice Number' || ' ' || (r_invoice.invoice_num) || ' ' || 'Line' || ' ' || (r_invoice_line.line_number) || ' ' ||'Source:' || ' ' || (r_invoice_line.global_attribute16));
                          insert_invoice_line(ir_invoice_line => r_invoice_line); -- ITEM
                        */
                        IF v_flag = FALSE THEN
                            insert_invoice_line(ir_invoice_line => r_invoice_line); -- ITEM
V_DEBUG := 260;
                        END IF;
                        -- End modify
                        --
                        r_invoice.invoice_amount := nvl(r_invoice.invoice_amount, 0) + r_invoice_line.amount; -- For Invoice Header

                        v_line_status_info := v_line_status_info || '| Processed';

                        --write_log ('update dtac_ap_invoices_temp_int set status = ''PROCESSED'' where invoice_num = ''' || r_invoice.invoice_num || ''' and line_number='|| r_invoice_line.line_number ||';');
                        /*
                        ** remark and modify #2 by AP@BAS on 24-Aug-2012
                        ** because used r_invoice_line.line_number that running record count
                        ** fixed change r_invoice_line.line_number to r_line.line_number
                        --update_status(i_inv_num => r_invoice.invoice_num, i_line_num => r_invoice_line.line_number, i_status => 'PROCESSED');
                        */
                        update_status(i_inv_num => r_invoice.invoice_num
                          , i_acc_ou_segment=>r_inv.acc_ou_segment
                            , i_line_num => r_line.line_number
                            , i_status => 'PROCESSED');

                        v_line_count := v_line_count + 1;
V_DEBUG := 270;
                    END IF;
                    IF v_chk_line THEN
                        v_flag := TRUE;
                    END IF;

                    write_log(v_line_status_info);
                END LOOP; --Line
            END IF;
V_DEBUG := 280;
            IF v_chk_line = FALSE THEN
                insert_invoice(ir_invoice => r_invoice);
                write_log('Invoice ID: ' || r_invoice.invoice_id);
            END IF;

            IF v_flag = TRUE THEN
                 write_log('Rollback transaction of invoice number ' || r_inv.invoice_num);
                ROLLBACK TO SAVEPOINT v_save_pnt;
            END IF;
V_DEBUG := 290;
        END LOOP; -- Header

        IF (v_line_count <= 0 ) THEN
          write_log('No invoice line');
        END IF;
V_DEBUG := 300;
        IF NOT v_validate_error AND v_line_count > 0 THEN
            COMMIT; -- SAVE

            BEGIN
                ap_req_id := fnd_request.submit_request(application => 'SQLAP'
                        , program => 'APXIIMPT'
                        , argument1 => v_source
                        , argument2 => i_group
                        , argument3 => i_batch --to_char(v_source_header_id),
                        , argument4 => NULL
                        , argument5 => NULL
                        , argument6 => NULL
                        , argument7 => 'N');
            END;

            v_i_group := i_group;
            v_ap_req_id := ap_req_id;
V_DEBUG := 310;
            COMMIT;
            --Add by Naj 19-jan-2009
            IF ap_req_id > 0 THEN
                write_log('Payable Interface Start Request ID: ' || ap_req_id);
                conc_wait(ap_req_id);

                c_ap_req_id := fnd_request.submit_request(application => 'SQLAP', program => 'TACAP016_V3_EXTD', argument1 => v_ap_req_id, argument2 => v_i_group);
            ELSE
                write_log('Can not Submit Payable Open Interface ' || to_char(SYSDATE, 'DD/MM/YYYY HH24:MI:SS') || ' ------');
            END IF;
            --Add by Naj 19-jan-2009
        END IF; -- Ctrl flag
V_DEBUG := 320;
        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            write_log('TAC_AP_INTERFACE_UTIL.interface_ap exception .');
            write_log('Error MSG: ' || SQLCODE || ' - ' || SQLERRM);
            write_log('V_DEBUG: ' || V_DEBUG );
    END interface_ap;

    ------------------------------------------------------------------------------
    PROCEDURE inf_ap_recurring
    (
        err_msg OUT VARCHAR2
       ,err_code OUT VARCHAR2
       ,i_inv_batch IN VARCHAR2
       ,i_gl_date_char IN VARCHAR2
       ,i_doc_category_code IN VARCHAR2
       ,i_exch_rate_type IN VARCHAR2
       ,i_exch_date IN DATE
       ,i_exch_rate IN NUMBER
    ) IS

        CURSOR c_header(i_gl_date IN DATE) IS
            SELECT DISTINCT rp.recurring_pay_num recurring_pay_num
                           ,rp.vendor_id vendor_id
                           ,rp.next_period next_period
                           ,rp.special_period_name1 special_period_name1
                           ,rp.special_period_name2 special_period_name2
                           ,rp.special_payment_amount1 special_payment_amount1
                           ,rp.special_payment_amount2 special_payment_amount2
                           ,rp.awt_group_id awt_group_id
                           ,rp.description description
                           ,rp.terms_id terms_id
                           ,rp.payment_method_lookup_code payment_method_lookup_code
                           ,rp.invoice_currency_code invoice_currency_code
                           ,rp.num_of_periods_rem num_of_periods_rem
                           ,rp.org_id org_id
                           ,rp.next_payment next_payment
                           ,rp.accts_pay_code_combination_id accts_pay_code_combination_id
                           ,t.name terms
                           ,t.creation_date terms_date
                           ,g.name awt_group_name
                           ,v.segment1 vendor_num
                           ,v.vendor_name vendor_name
                           ,vs.vendor_site_id vendor_site_id
                           ,vs.vendor_site_code vendor_site_code
                           ,rp.pay_group_lookup_code pay_group
                           ,rp.hold_lookup_code hold_lookup_code
                           ,rp.rec_pay_period_type rec_pay_period_type
                           ,rp.recurring_payment_id recurring_payment_id
                           ,rp.first_period first_period
                           ,rp.first_amount first_amount
                           ,rp.increment_percent increment_percent
                           ,nvl(rp.num_of_periods, 0) - nvl(rp.num_of_periods_rem, 0) current_period
            FROM ap_recurring_payments rp, po_vendors v, po_vendor_sites vs, ap_awt_groups g, ap_terms t
            WHERE rp.vendor_id = v.vendor_id
                  AND rp.vendor_site_id = vs.vendor_site_id
                  AND rp.awt_group_id = g.group_id(+)
                  AND rp.terms_id = t.term_id
                  AND rp.num_of_periods_rem > 0
                 --AND rp.RECURRING_PAY_NUM  = 'TM#010'
                 --NVL(rp.expiry_date,TO_DATE('31/12/9999','DD/MM/YYYY'))
                  AND rp.next_period IN (SELECT aop.period_name
                                         FROM ap_other_periods aop, gl_period_statuses gps, ap_system_parameters asp
                                         WHERE aop.module = 'RECURRING PAYMENTS'
                                               AND aop.application_id = 200
                                               AND gps.application_id = 200
                                               AND gps.set_of_books_id = asp.set_of_books_id
                                               AND nvl(gps.adjustment_period_flag, 'N') = 'N'
                                               AND i_gl_date BETWEEN gps.start_date AND gps.end_date
                                              /*M comment*/
                                              --AND aop.end_date BETWEEN gps.start_date AND gps.end_date
                                               AND gps.closing_status = 'O'
                                               AND aop.period_type = rp.rec_pay_period_type
                                               AND nvl(rp.expiry_date, to_date('31/12/9999', 'DD/MM/YYYY')) >= aop.end_date
                                               AND i_gl_date BETWEEN aop.start_date AND aop.end_date);

        --## Em Add Check Expire Date 2006/12/2006

        CURSOR c_line(i_pay_num IN VARCHAR2) IS
        -- ## EM start edit 2006/12/04
            SELECT arp.*
                  ,ads.distribution_set_name
                  ,adsl.dist_code_combination_id
                  ,adsl.vat_code line_vat_code
                  ,adsl.description line_description
                  ,adsl.percent_distribution line_percent
            FROM ap_recurring_payments arp, ap_distribution_sets ads, ap_distribution_set_lines adsl
            WHERE arp.recurring_pay_num = i_pay_num
                  AND arp.distribution_set_id = ads.distribution_set_id
                  AND ads.distribution_set_id = adsl.distribution_set_id;
        -- End edit    2006/12/04

        CURSOR c_chk(req_id IN NUMBER, inv_num IN VARCHAR2, vendor_id IN NUMBER) IS
            SELECT h.*
            FROM ap_invoices_interface h
            WHERE h.request_id = req_id
                  AND h.vendor_id = vendor_id
                  AND h.invoice_num = inv_num;
        --AND   h.status      = 'PROCESSED';

        CURSOR c_get_period_start_date(i_period_name IN VARCHAR2) IS
            SELECT op.* FROM ap_other_periods op WHERE op.period_name = i_period_name;

        CURSOR c_get_next_period(i_period_name IN VARCHAR2) IS
            SELECT op.*
            FROM ap_other_periods op
            WHERE op.start_date > (SELECT sop.start_date FROM ap_other_periods sop WHERE sop.period_name = i_period_name)
            ORDER BY op.period_year, op.period_num;

        CURSOR c_get_all_period(i_period_start_date IN DATE, i_period_end_date IN DATE) IS
            SELECT op.* FROM ap_other_periods op WHERE op.start_date BETWEEN i_period_start_date AND i_period_end_date;

        TYPE r_temp_type IS RECORD(
             temp_id              NUMBER
            ,invoice_num          VARCHAR2(100)
            ,next_period          VARCHAR2(100)
            ,next_payment         NUMBER
            ,num_remain           NUMBER
            ,line_amount          NUMBER
            ,recurring_payment_id NUMBER
            ,vendor_id            NUMBER
            ,invoice_batch        VARCHAR2(100));

        TYPE r_temp_rec IS TABLE OF r_temp_type INDEX BY BINARY_INTEGER;

        r_temp r_temp_rec;
        idx INTEGER := 1;
        i_gl_date DATE := to_date(i_gl_date_char, 'YYYY/MM/DD HH24:MI:SS');
        r_invoice ap_invoices_interface%ROWTYPE;
        r_invoice_line ap_invoice_lines_interface%ROWTYPE;
        v_source VARCHAR2(80) := 'RECURRING INVOICE';
        v_invoice_type VARCHAR2(100) := 'STANDARD';
        v_line_num NUMBER;
        v_ctrl_flag VARCHAR2(1) := 'N';
        ap_req_id NUMBER;
        c_ap_req_id NUMBER;
        v_next_period_name VARCHAR2(100);
        v_next_period_num NUMBER;
        v_next_period_year NUMBER;

        v_rate NUMBER;
        v_site_auto_calc_flag VARCHAR2(30);
        v_sys_param ap_system_parameters%ROWTYPE;

        lc_wait BOOLEAN;
        lc_phase VARCHAR2(100);
        lc_status VARCHAR2(100);
        lc_dev_phase VARCHAR2(100);
        lc_dev_status VARCHAR2(100);
        lc_message VARCHAR2(1000);
        lc_phase_code VARCHAR2(1) := NULL;
        lc_status_code VARCHAR2(1) := NULL;
        v_count NUMBER;
        v_status_inf VARCHAR2(100);
        v_group_id VARCHAR2(100);

        v_period_start_date DATE;
        v_period_end_date DATE;
        v_next_amt NUMBER;
        v_invoice_rate_dis_set NUMBER;
        v_invoice_tax_dis_set NUMBER;
        v_tax_code_combination_id NUMBER;
        v_tax_description VARCHAR2(2000);
    BEGIN

        write_log('+----------------------------------------------------------------------------+');
        write_log('Start Concurrent AP Recurring Invoice Interface. ');
        write_log('Start Date: ' || to_char(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
        write_log(' ');
        write_log('Agruments');
        write_log('-----------');
        write_log('Invoice Batch : ' || i_inv_batch);
        write_log('GL Date : ' || to_char(i_gl_date, 'DD/MM/YYYY'));
        write_log('Document Category code: ' || i_doc_category_code);
        write_log('Exchange Type : ' || i_exch_rate_type);
        write_log('Exchange Date : ' || to_char(i_exch_date, 'DD/MM/YYYY'));
        write_log('Exchange Rate : ' || to_char(i_exch_rate));
        write_log('+----------------------------------------------------------------------------+');

        IF nvl(i_exch_rate_type, '**') != '**' THEN
            IF nvl(i_exch_rate_type, '**') != 'User' AND i_exch_date IS NULL THEN
                write_log('*Exchange Rate Type is not User please input exchange date');
                err_msg := 'Exchange Rate Type is not User please input exchange date';
                err_code := '1';
                RETURN;
            ELSIF nvl(i_exch_rate_type, '**') = 'User' AND i_exch_date IS NULL THEN
                write_log('*Exchange Rate Type is User please input exchange rate');
                err_msg := '*Exchange Rate Type is User please input exchange rate';
                err_code := '1';
                RETURN;
            ELSE
                -- Not in gl_daily_conversion_
                /* write_log('*Exchange Rate Type is User please input exchange rate') ;
                err_msg        := '*Exchange Rate Type is User please input exchange rate';
                ERR_CODE    := '1' ;
                RETURN ;      */
                NULL;
            END IF;
        END IF;

        BEGIN
            SELECT COUNT(*) INTO v_count FROM ap_batches b WHERE upper(b.batch_name) = upper(i_inv_batch);

            IF v_count > 0 THEN
                write_log('*Invoice Batch already exist: ' || i_inv_batch);
                err_msg := '*** DATA ERROR : Interface data is invalid';
                err_code := '1';
                RETURN;
            END IF;
        EXCEPTION
            WHEN OTHERS THEN
                RETURN;
        END;

        DECLARE
            v_row_count NUMBER;
        BEGIN
            SELECT COUNT(*)
            INTO v_row_count
            FROM gl_period_statuses gps
            WHERE trunc(i_gl_date) BETWEEN trunc(gps.start_date) AND trunc(gps.end_date)
                  AND gps.closing_status = 'O'
                  AND gps.application_id = 200;

            IF v_row_count = 0 THEN
                write_log('*GL Date not in open period: ' || i_inv_batch);
                err_msg := '*** DATA ERROR : Interface data is invalid';
                err_code := '1';
                RETURN;
            END IF;
        EXCEPTION
            WHEN OTHERS THEN
                RETURN;
        END;

        BEGIN
            SELECT * --batch_control_flag
            INTO v_sys_param --v_batch_ctrl_flag
            FROM ap_system_parameters;

        EXCEPTION
            WHEN OTHERS THEN
                --v_batch_ctrl_flag := NULL ;
                v_sys_param := NULL;
        END;

        r_temp.delete;

        SAVEPOINT b4_process;

        BEGIN
            SELECT rtrim(ltrim(to_char(ap_invoices_interface_s.nextval))) INTO v_group_id FROM dual;
        END;

        FOR r_head IN c_header(i_gl_date)
        LOOP
            write_log('Invoice Number: ' || r_head.recurring_pay_num || '-' || r_head.next_period);
            r_invoice := NULL;

            SELECT ap_invoices_interface_s.nextval INTO r_invoice.invoice_id FROM dual;
            -- Validate inactive Supplier.
            IF r_head.vendor_id IS NOT NULL THEN
                DECLARE
                    v_act_vendor NUMBER;
                BEGIN
                    SELECT COUNT(*) INTO v_act_vendor FROM po_vendors_active_ap_v WHERE vendor_id = r_head.vendor_id;

                    IF v_act_vendor = 0 THEN
                        write_log('*Supplier is inactive: ' || r_head.vendor_id);
                        err_msg := '*** DATA ERROR : Interface data is invalid';
                        err_code := '1';
                    END IF;
                END;
            END IF;
            -- Validate Inactive Supplier Site.
            /*        DECLARE
            v_act_vendor_site    NUMBER;*/
            BEGIN

                SELECT pvs.auto_tax_calc_flag
                INTO v_site_auto_calc_flag
                FROM po_vendor_sites pvs
                WHERE pvs.vendor_site_id = r_head.vendor_site_id
                      AND SYSDATE < nvl(inactive_date, SYSDATE + 1);

            EXCEPTION
                WHEN no_data_found THEN
                    write_log('*Supplier site is inactive : ' || r_head.vendor_site_id);
                    err_msg := '*** DATA ERROR : Interface data is invalid';
                    err_code := '1';
            END;

            IF r_head.num_of_periods_rem < 1 THEN
                write_log('*Invoice over remaining : ' || r_head.recurring_pay_num);
                err_msg := '*** DATA ERROR : Interface data is invalid';
                err_code := '1';
            END IF;

            -- Assign AP_INVOICES_INTERFACE
            r_invoice.source := v_source;
            r_invoice.group_id := v_group_id;
            r_invoice.invoice_type_lookup_code := v_invoice_type;
            r_invoice.vendor_id := r_head.vendor_id;
            r_invoice.vendor_num := r_head.vendor_num;
            r_invoice.vendor_name := r_head.vendor_name;
            r_invoice.vendor_site_id := r_head.vendor_site_id;
            r_invoice.vendor_site_code := r_head.vendor_site_code;
            r_invoice.invoice_date := i_gl_date;
            r_invoice.invoice_num := r_head.recurring_pay_num || '-' || r_head.next_period;
            r_invoice.invoice_currency_code := r_head.invoice_currency_code;
            r_invoice.org_id := r_head.org_id;
            r_invoice.invoice_amount := r_head.next_payment; -- Update Invoice Amount, by Nu --
            r_invoice.awt_group_id := r_head.awt_group_id;
            r_invoice.awt_group_name := r_head.awt_group_name;
            r_invoice.gl_date := i_gl_date;
            r_invoice.description := r_head.description;
            r_invoice.terms_id := r_head.terms_id;
            r_invoice.terms_name := r_head.terms;
            r_invoice.terms_date := r_head.terms_date;
            r_invoice.pay_group_lookup_code := r_head.pay_group;
            r_invoice.doc_category_code := i_doc_category_code;
            r_invoice.payment_method_lookup_code := r_head.payment_method_lookup_code;
            r_invoice.exchange_rate_type := i_exch_rate_type;
            r_invoice.exchange_date := i_exch_date;
            r_invoice.exchange_rate := i_exch_rate;
            r_invoice.accts_pay_code_combination_id := r_head.accts_pay_code_combination_id;
            r_invoice.created_by := fnd_global.user_id;
            r_invoice.creation_date := SYSDATE;

            insert_invoice(ir_invoice => r_invoice); --Insert ap interface

            v_line_num := 0;
            -- ## Em add 2006/12/04=>LOOP for check expire date
            --    FOR r_check IN c_check_expiration (r_head.RECURRING_PAY_NUM)
            --    LOOP
            FOR r_line IN c_line(r_head.recurring_pay_num)
            LOOP

                r_invoice_line := NULL;
                v_line_num := v_line_num + 1;

                -- Assign AP_INVOICE_LINES_INTERFACE
                SELECT ap_invoice_lines_interface_s.nextval INTO r_invoice_line.invoice_line_id FROM dual;

                r_invoice_line.invoice_id := r_invoice.invoice_id;
                r_invoice_line.line_number := v_line_num;
                r_invoice_line.line_type_lookup_code := 'ITEM';
                r_invoice_line.awt_group_id := r_invoice.awt_group_id;
                r_invoice_line.awt_group_name := r_invoice.awt_group_name;
                r_invoice_line.org_id := r_line.org_id;

                -- r_invoice_line.awt_group_id                     :=    r_invoice.awt_group_id;
                r_invoice_line.created_by := fnd_global.user_id;
                r_invoice_line.creation_date := SYSDATE;
                --## Em start edit 2006/12/04
                --r_invoice_line.dist_code_combination_id :=    r_line.accts_pay_code_combination_id;
                r_invoice_line.dist_code_combination_id := r_line.dist_code_combination_id;
                -- ## End edit 2006/12/04
                --------------------------------
                -- Find from distribution Set
                --------------------------------
                --write_log('check vat code:'||r_line.line_vat_code);

                -- Line Tax Em add /*04/01/2006*/
                IF r_line.line_vat_code IS NOT NULL AND
                   (nvl(v_sys_param.auto_tax_calc_flag, 'N') = 'L' OR nvl(v_site_auto_calc_flag, 'L') IN ('Y', 'L')) THEN
                    BEGIN
                        SELECT atc.tax_id, atc.tax_rate, atc.tax_code_combination_id, atc.description
                        INTO r_invoice_line.tax_code_id, v_rate, v_tax_code_combination_id, v_tax_description
                        FROM ap_tax_codes atc
                        WHERE atc.enabled_flag = 'Y'
                             -- Em edit 2006/12/04
                             --AND    atc.NAME                            = v_vat_code
                              AND atc.name = r_line.line_vat_code
                              AND nvl(atc.start_date, SYSDATE) <= SYSDATE
                              AND nvl(atc.inactive_date, SYSDATE) >= SYSDATE;
                    EXCEPTION
                        WHEN OTHERS THEN
                            write_log('*Invalid Tax Code : ' || r_line.line_vat_code);
                            err_msg := '*** DATA ERROR : Interface data is invalid!';
                            err_code := '1';
                    END;

                    r_invoice_line.description := r_line.line_description;
                    r_invoice_line.tax_code := r_line.line_vat_code;
                    r_invoice_line.amount_includes_tax_flag := 'N';
                    --EM clear value
                    v_invoice_rate_dis_set := 0;
                    v_invoice_tax_dis_set := 0;
                    v_invoice_rate_dis_set := round(r_invoice.invoice_amount /*/((v_rate/100)+1)*/
                                                    * (r_line.line_percent / 100), 2); -- compute exclude vat
                    v_invoice_tax_dis_set := round(v_invoice_rate_dis_set * (v_rate) / (100 + v_rate), 2); -- compute exclude vat
                    r_invoice_line.amount := v_invoice_rate_dis_set - v_invoice_tax_dis_set;
                    --642 =  1070 * 60/100
                    --42 =  (642 * 7)/107
                    --600 = 642 - 42
                    insert_invoice_line(ir_invoice_line => r_invoice_line); -- Insert Invoioce line => 'ITEM'

                    ------------- TAX -------------
                    v_line_num := v_line_num + 1;

                    SELECT ap_invoice_lines_interface_s.nextval INTO r_invoice_line.invoice_line_id FROM dual;

                    r_invoice_line.invoice_id := r_invoice.invoice_id;
                    r_invoice_line.line_number := v_line_num;
                    r_invoice_line.line_type_lookup_code := 'TAX'; --TAX
                    -- Em edit 2006/12/04
                    --r_invoice_line.amount                        :=    ROUND((r_line.next_payment*(v_rate/100))*(v_percent/100),2);
                    --r_invoice_line.amount                        :=    ROUND((r_line.next_payment*(v_rate/100))*(r_line.line_percent/100),2);
                    r_invoice_line.amount := v_invoice_tax_dis_set;
                    r_invoice_line.awt_group_id := NULL;
                    r_invoice_line.awt_group_name := NULL;
                    r_invoice_line.org_id := r_line.org_id;
                    r_invoice_line.amount_includes_tax_flag := 'N';
                    --  r_invoice_line.awt_group_id                :=    NULL;
                    r_invoice_line.created_by := fnd_global.user_id;
                    r_invoice_line.creation_date := SYSDATE;
                    --    r_invoice_line.dist_code_combination_id :=    r_line.accts_pay_code_combination_id;
                    r_invoice_line.dist_code_combination_id := v_tax_code_combination_id;
                    -- EM edit 2006/12/04
                    --r_invoice_line.description                 :=    v_line_desc;
                    --    r_invoice_line.description                :=    r_line.line_description;
                    r_invoice_line.description := v_tax_description;
                    r_invoice_line.tax_code := r_line.line_vat_code;

                    insert_invoice_line(ir_invoice_line => r_invoice_line); --Insert Invoice line => 'TAX'

                ELSIF r_line.line_vat_code IS NOT NULL AND
                      (nvl(v_sys_param.auto_tax_calc_flag, 'N') = 'N' OR nvl(v_site_auto_calc_flag, 'L') IN ('Y', 'N')) THEN
                    BEGIN
                        SELECT atc.tax_id, atc.tax_rate, atc.tax_code_combination_id, atc.description
                        INTO r_invoice_line.tax_code_id, v_rate, v_tax_code_combination_id, v_tax_description
                        FROM ap_tax_codes atc
                        WHERE atc.enabled_flag = 'Y'
                             -- Em edit 2006/12/04
                             --AND    atc.NAME                            = v_vat_code
                              AND atc.name = r_line.line_vat_code
                              AND nvl(atc.start_date, SYSDATE) <= SYSDATE
                              AND nvl(atc.inactive_date, SYSDATE) >= SYSDATE;
                    EXCEPTION
                        WHEN OTHERS THEN
                            write_log('*Invalid Tax Code : ' || r_line.line_vat_code);
                            err_msg := '*** DATA ERROR : Interface data is invalid!';
                            err_code := '1';
                    END;

                    r_invoice_line.description := r_line.line_description;
                    r_invoice_line.tax_code := r_line.line_vat_code;
                    r_invoice_line.amount_includes_tax_flag := 'N';
                    r_invoice_line.amount := round(r_invoice.invoice_amount / ((v_rate / 100) + 1) * (r_line.line_percent / 100), 2); -- compute exclude vat

                    insert_invoice_line(ir_invoice_line => r_invoice_line); -- Insert Invoice line => 'ITEM'

                    ------------- TAX -------------
                    v_line_num := v_line_num + 1;

                    SELECT ap_invoice_lines_interface_s.nextval INTO r_invoice_line.invoice_line_id FROM dual;

                    r_invoice_line.invoice_id := r_invoice.invoice_id;
                    r_invoice_line.line_number := v_line_num;
                    r_invoice_line.line_type_lookup_code := 'TAX'; --TAX
                    -- Em edit 2006/12/04
                    --r_invoice_line.amount                        :=    ROUND((r_line.next_payment*(v_rate/100))*(v_percent/100),2);
                    r_invoice_line.amount := round((r_line.next_payment * (v_rate / 100)) * (r_line.line_percent / 100), 2);
                    r_invoice_line.awt_group_id := NULL;
                    r_invoice_line.awt_group_name := NULL;
                    r_invoice_line.org_id := r_line.org_id;
                    r_invoice_line.amount_includes_tax_flag := 'N';
                    --  r_invoice_line.awt_group_id                :=    r_invoice.awt_group_id;
                    r_invoice_line.created_by := fnd_global.user_id;
                    r_invoice_line.creation_date := SYSDATE;
                    --    r_invoice_line.dist_code_combination_id :=    r_line.accts_pay_code_combination_id;
                    r_invoice_line.dist_code_combination_id := v_tax_code_combination_id;
                    -- EM edit 2006/12/04
                    --r_invoice_line.description                 :=    v_line_desc;
                    r_invoice_line.description := v_tax_description;
                    r_invoice_line.tax_code := r_line.line_vat_code;
                    insert_invoice_line(ir_invoice_line => r_invoice_line); --Insert Invoice line => 'TAX'
                ELSE
                    r_invoice_line.amount_includes_tax_flag := 'Y';
                    -- EM edit 2006/12/04
                    --    r_invoice_line.amount                        :=    r_line.next_payment;
                    --    r_invoice_line.description                     :=    v_line_desc;
                    --    r_invoice_line.tax_code                        :=    v_vat_code;
                    r_invoice_line.amount := round(r_line.next_payment * (r_line.line_percent / 100), 2);
                    r_invoice_line.description := r_line.line_description;
                    r_invoice_line.tax_code := r_line.line_vat_code;
                    insert_invoice_line(ir_invoice_line => r_invoice_line); --Insert Invoice line => 'ITEM'
                END IF;
                --IF r_line.num_of_periods_rem <> 0 THEN
            --Next period

            END LOOP; -- Line
            --    END LOOP; -- Check Expire Date
            v_ctrl_flag := 'Y';

            IF 1 = 1 THEN

                BEGIN

                    ap_recurring_invoices_pkg.ap_get_next_period(p_period_type => r_head.rec_pay_period_type, p_current_period_name => r_head.next_period, p_next_period_name => v_next_period_name, p_next_period_num => v_next_period_num, p_next_period_year => v_next_period_year, p_calling_sequence => '1.0');
                EXCEPTION
                    WHEN OTHERS THEN
                        fnd_file.put_line(fnd_file.log, 'Get Next Period Err ' || SQLERRM);
                END;
                /*
                 Begin
                   Update ap_recurring_payments_all  arp
                   Set    arp.next_period = ''
                   ,      arp.num_of_periods_rem = 0
                   WHERE  arp.recurring_payment_id = 1 ;
                 End;
                */

                -- Get Next Payment, update by nu --
                IF v_next_period_name = r_head.special_period_name1 THEN
                    v_next_amt := r_head.special_payment_amount1;
                ELSIF v_next_period_name = r_head.special_period_name2 THEN
                    v_next_amt := r_head.special_payment_amount2;
                ELSE
                    IF nvl(r_head.increment_percent, 0) = 0 THEN
                        v_next_amt := r_head.first_amount;
                    ELSE
                        -- get start date for first period --
                        FOR r_first_prd IN c_get_period_start_date(r_head.first_period)
                        LOOP
                            v_period_start_date := r_first_prd.start_date;
                        END LOOP;
                        -- get start date for next period --
                        FOR r_next_prd IN c_get_period_start_date(v_next_period_name)
                        LOOP
                            v_period_end_date := r_next_prd.start_date;
                        END LOOP;
                        -- get invoice amt by multiply increment percent --
                        v_next_amt := nvl(r_head.first_amount, 0);
                        FOR r_all_period IN c_get_all_period(v_period_start_date, v_period_end_date)
                        LOOP
                            IF r_all_period.period_name = r_head.first_period THEN
                                v_next_amt := nvl(r_head.first_amount, 0);
                            ELSIF (r_head.special_period_name1 IS NULL OR r_all_period.period_name <> r_head.special_period_name1) AND
                                  (r_head.special_period_name2 IS NULL OR r_all_period.period_name <> r_head.special_period_name2) THEN
                                v_next_amt := v_next_amt + (v_next_amt * nvl(r_head.increment_percent, 0) / 100);
                            END IF;
                        END LOOP;
                    END IF;
                END IF;
                -- End Invoice Amount --

                r_temp(idx).invoice_num := r_invoice.invoice_num;
                r_temp(idx).vendor_id := r_invoice.vendor_id;
                r_temp(idx).temp_id := r_head.recurring_payment_id;
                r_temp(idx).recurring_payment_id := r_head.recurring_payment_id;
                r_temp(idx).next_period := v_next_period_name;
                r_temp(idx).next_payment := v_next_amt; --r_head.next_payment;
                r_temp(idx).line_amount := v_next_amt; --r_head.Next_Payment;
                r_temp(idx).invoice_batch := i_inv_batch;

                IF r_head.num_of_periods_rem <> 0 THEN
                    r_temp(idx).num_remain := r_head.num_of_periods_rem - 1;
                END IF;

                idx := idx + 1;

            END IF;
        END LOOP; -- Head

        --write_log(' Ctrl flag '||v_ctrl_flag);

        IF v_ctrl_flag = 'Y' THEN
            COMMIT; -- SAVE

            BEGIN
                ap_req_id := fnd_request.submit_request(application => 'SQLAP', program => 'APXIIMPT', argument1 => v_source, argument2 => v_group_id, argument3 => i_inv_batch --to_char(v_source_header_id),
                                                       , argument4 => NULL, argument5 => NULL, argument6 => NULL, argument7 => 'N', argument8 => NULL, argument9 => NULL, argument10 => 'N');
            END;

            COMMIT;

            IF ap_req_id > 0 THEN
                --FND_FILE.put_line(fnd_file.log,' -- AP Req_id '||ap_req_id );
                ap_req_id := ap_req_id;
            ELSE
                fnd_file.put_line(fnd_file.log, ' Can not Submit Payable Open Interface ' || to_char(SYSDATE, 'DD/MM/YYYY HH24:MI:SS') || ' ------');
            END IF;

            --END IF ;  -- Ctrl flag

            --------------------------------------------
            ---- Check concurrent request is completed.
            --------------------------------------------
            --    fnd_conc_global.set_req_globals(conc_status => 'PAUSED', request_data =>lc_req_data);
            lc_wait := fnd_concurrent.wait_for_request(request_id => ap_req_id, INTERVAL => 1, phase => lc_phase, status => lc_status, dev_phase => lc_dev_phase, dev_status => lc_dev_status, message => lc_message);

            BEGIN
                SELECT fcr.phase_code, fcr.status_code
                INTO lc_phase_code, lc_status_code
                FROM fnd_concurrent_requests fcr
                WHERE fcr.request_id = ap_req_id;
            EXCEPTION
                WHEN no_data_found THEN
                    NULL;
            END;
            --begin

            --  declare

            IF lc_phase_code = 'C' AND lc_status_code = 'C' THEN
                FOR i IN r_temp.first .. r_temp.last
                LOOP
                    --            FOR r_chk IN c_chk(ap_req_id, r_temp(i).invoice_num,r_temp(i).vendor_id)
                    --            LOOP
                    BEGIN
                        SELECT h.status
                        INTO v_status_inf
                        FROM ap_invoices_interface h
                        WHERE h.source = 'RECURRING INVOICE' --h.request_id    =    v_req_id
                              AND h.vendor_id = r_temp(i).vendor_id
                              AND h.invoice_num = r_temp(i).invoice_num
                              AND h.group_id = v_group_id
                              AND h.request_id = ap_req_id;
                    EXCEPTION
                        WHEN OTHERS THEN
                            fnd_file.put_line(fnd_file.log, 'Table Interface Not found ');
                            v_status_inf := NULL;
                    END;

                    IF v_status_inf = 'PROCESSED' THEN
                        update_template(i_temp_id => r_temp(i).temp_id, i_next_period => r_temp(i).next_period, i_next_payment => r_temp(i)
                                                          .next_payment, i_period_rem => r_temp(i).num_remain, i_line_amount => r_temp(i).line_amount);
                        BEGIN
                            UPDATE ap_invoices_all ai
                            SET ai.recurring_payment_id = r_temp(i).recurring_payment_id
                            WHERE ai.invoice_num = r_temp(i).invoice_num
                                  AND ai.vendor_id = r_temp(i).vendor_id;
                        EXCEPTION
                            WHEN OTHERS THEN
                                NULL;
                        END;
                    END IF;
                END LOOP;
            END IF;

            --write_log('+----------------------------------------------------------------------------+');
            --write_log('Recurring Invoices Generated Report.');

            BEGIN
                ap_req_id := fnd_request.submit_request(application => 'SQLAP', program => 'TACAP037', argument1 => i_inv_batch, argument2 => to_char(i_gl_date, 'YYYY/MM/DD HH24:MI:SS'), argument3 => to_char(SYSDATE, 'YYYY/MM/DD HH24:MI:SS'), argument4 => to_char(ap_req_id), argument5 => v_group_id);
            END;

            COMMIT;

            IF ap_req_id > 0 THEN
                --FND_FILE.put_line(fnd_file.log,' -- TACAP037 Req_id '||ap_req_id );
                ap_req_id := ap_req_id;
            ELSE
                fnd_file.put_line(fnd_file.log, ' Can not Submit Recurring Invoices Generated Report ' || to_char(SYSDATE, 'DD/MM/YYYY HH24:MI:SS') ||
                                   ' ------');
            END IF;
            write_log('+----------------------------------------------------------------------------+');
        END IF; -- Control Flag

        --FND_FILE.put_line(fnd_file.log,' -- End Process AP Recuuring '||TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SS')||' ------');
    END inf_ap_recurring;

    ------------------------------------------------------------------------------
    PROCEDURE insert_dtac_ap_banks_to_sts
    (
        ir_dtac_ap_banks_to_sts IN dtac_ap_banks_to_sts%ROWTYPE
       ,o_err OUT VARCHAR2
    ) IS
    BEGIN
        --validate_dtac_ap_banks_to_sts()
        BEGIN
            INSERT INTO dtac_ap_banks_to_sts
                (seqn, bank_code, bank_name, creation_date, eai_stts, eai_last_chng_dttm)
            VALUES
                (ir_dtac_ap_banks_to_sts.seqn
                ,ir_dtac_ap_banks_to_sts.bank_code
                ,ir_dtac_ap_banks_to_sts.bank_name
                ,ir_dtac_ap_banks_to_sts.creation_date
                ,ir_dtac_ap_banks_to_sts.eai_stts
                ,ir_dtac_ap_banks_to_sts.eai_last_chng_dttm);

        EXCEPTION
            WHEN OTHERS THEN
                -- fnd_file.put_line(fnd_file.output
                --                    ,' Bank Code : '||ir_dtac_ap_banks_to_sts.bank_code||' => '||SQLERRM  ) ;
                o_err := SQLERRM;
        END;
    END;

    ---------------------------------------------------------------------------------------
    PROCEDURE get_bank_from_flex_val
    (
        err_msg IN OUT VARCHAR2
       ,err_code IN OUT VARCHAR2
    ) IS
        CURSOR c_get_fnd_flex_value IS
            SELECT ffv.flex_value bank_code, ffv.description bank_name
            FROM fnd_flex_values_vl ffv, fnd_flex_value_sets ffvs
            WHERE ffv.flex_value_set_id = ffvs.flex_value_set_id
                  AND ffvs.flex_value_set_name = 'TAC_AP_SUPPLIER_BANK'
                 -- Add by TM.Pornnicha --
                  AND ffv.enabled_flag = 'Y'
                  AND ffv.attribute1 = 'Y'
                  AND trunc(SYSDATE) BETWEEN nvl(ffv.start_date_active, trunc(SYSDATE)) AND nvl(ffv.end_date_active, trunc(SYSDATE));

        seq NUMBER := 0;
        v_err VARCHAR2(300);
        r_dtac_ap_banks_to_sts dtac_ap_banks_to_sts%ROWTYPE;
    BEGIN
        SAVEPOINT before_del_banks;

        DELETE dtac_ap_banks_to_sts;
        --    RAISE err_test ;
        FOR rec IN c_get_fnd_flex_value
        LOOP
            IF seq = 0 THEN
                seq := 0;
                --Write_Log('--------------- Banks List In DTAC_AP_BANKS_TO_STS ----------------------') ;
            END IF;
            seq := seq + 1;
            r_dtac_ap_banks_to_sts.seqn := seq;
            r_dtac_ap_banks_to_sts.bank_code := rec.bank_code;
            r_dtac_ap_banks_to_sts.bank_name := rec.bank_name;
            r_dtac_ap_banks_to_sts.creation_date := SYSDATE;
            insert_dtac_ap_banks_to_sts(r_dtac_ap_banks_to_sts, v_err);
            --fnd_file.put_line(fnd_file.output,seq||' :'||rec.bank_code||' '||v_err) ;
            IF v_err IS NOT NULL THEN
                err_code := '2'; -- Warning
            END IF;
        END LOOP;
        write_log('--------------------------------------------------------------------------');
        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK TO before_del_banks;
            err_msg := SQLERRM;
            err_code := SQLCODE;
    END get_bank_from_flex_val;
    ----------------------------------------------------------------------------------------------

END TAC_AP_INTERFACE_UTIL_DTCV2;
/
