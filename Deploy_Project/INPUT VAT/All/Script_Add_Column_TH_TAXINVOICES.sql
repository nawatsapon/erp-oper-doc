alter table TH_TAXINVOICES_HEADER add cancel_flag varchar2(1) default 'N';
ALTER TABLE th_taxinvoices_header ADD tran_cancel_flag VARCHAR2(1) DEFAULT 'N';
ALTER TABLE th_taxinvoices_header ADD tran_cancel_date DATE;


alter table TH_TAXINVOICES_LINE add submit_num number;
alter table TH_TAXINVOICES_LINE add submit_period_name varchar2(100);
alter table TH_TAXINVOICES_LINE add submit_seq_num number;
alter table TH_TAXINVOICES_LINE add released_by number;
alter table TH_TAXINVOICES_LINE add released_date date;
