CREATE OR REPLACE PROCEDURE SP_GEN_INV_SIMCHARGE
(P_SUBINV VARCHAR2, 
 P_DATE_FROM DATE, 
 P_DATE_TO DATE,
 P_CUSTCODE VARCHAR2, 
 P_PAYMENT_CODE VARCHAR2,
 P_VAT_CODE VARCHAR2)  
IS
   V_DATE_TO       DATE;

  CURSOR CUR_SERIAL IS
  SELECT A.TRANS_ID, A.CARD_NUMBER
          ,A.LOCATION SUBINV_CODE
         ,A.SALES_PERSON
         ,A.PRICELIST_CODE
         ,A.OU_CODE
  FROM SA_SALES_INTERFACE A
  WHERE A.TRANS_TYPE = 'SIMCHARGE'
  AND A.STATUS IN ('P')--P means Processing, Get only this status because at begin we will update all to P(Processing)
  AND A.LOCATION  = P_SUBINV
  AND A.TRANS_DATE >=P_DATE_FROM
  AND A.TRANS_DATE <V_DATE_TO;

  CURSOR CUR_SERIAL_DUP IS
  SELECT MIN(A.TRANS_ID) TRANS_ID, COUNT(*) , CARD_NUMBER
  FROM SA_SALES_INTERFACE A
  WHERE A.TRANS_TYPE = 'SIMCHARGE'
  AND A.STATUS = 'P'
  AND A.LOCATION  = P_SUBINV
  AND A.TRANS_DATE >=P_DATE_FROM
  AND A.TRANS_DATE <V_DATE_TO
  GROUP BY A.CARD_NUMBER 
  HAVING COUNT(*) > 1;

  CURSOR CUR_SERIAL_STATUS(P_CARD_NUMBER VARCHAR2) IS
  SELECT S.CURRENT_STATUS, S.CURRENT_ORGANIZATION_ID, S.CURRENT_SUBINVENTORY_CODE, 
         S.CURRENT_LOCATOR_ID, L.SEGMENT1 LOCATOR_CODE, S.INVENTORY_ITEM_ID,
         S.SERIAL_NUMBER
  FROM MTL_SERIAL_NUMBERS S, MTL_ITEM_LOCATIONS L
  WHERE SUBSTR(S.SERIAL_NUMBER,LENGTH(S.SERIAL_NUMBER)-16+1,16) = P_CARD_NUMBER
  AND S.CURRENT_LOCATOR_ID = L.INVENTORY_LOCATION_ID(+)
  ORDER BY S.CURRENT_STATUS;


  --- VARIABLE FOR GENERATE INVOICE

  V_FOUND_INFO    VARCHAR2(1);
  V_PRICELIST     SA_TRANSACTION.PRICELIST_CODE%TYPE;
  V_SUBINV_TYP_CODE VARCHAR2(10);

  CURSOR C_HEADER IS
  SELECT  I.LOCATION SUBINV_CODE
          ,I.OU_CODE
          ,SUM(UNIT_PRICE) SUM_AMT
          ,I.SALES_PERSON
  FROM    SA_SALES_INTERFACE I
  WHERE    I.TRANS_TYPE   = 'SIMCHARGE'
  AND      I.STATUS = 'P'  -- STATUS P IS PROCESSING
  AND      I.LOCATION  = P_SUBINV   -- Fix bug Yodchai L. 2014-Dec-04 9:09 , User run same time with others SHOP
  GROUP BY I.LOCATION
          ,I.OU_CODE
          ,I.SALES_PERSON
  ORDER BY TRUNC(SYSDATE),I.LOCATION,I.SALES_PERSON; 

  CURSOR C_DETAIL(
         P_OU_CODE SA_TRANSACTION.OU_CODE%TYPE, 
         P_SUBINV_CODE SA_TRANSACTION.SUBINV_CODE%TYPE,
         P_SALES_PERSON SA_TRANSACTION.SALESPERSON%TYPE
  )  IS
  SELECT  I.LOCATION SUBINV_CODE
          ,I.OU_CODE
          ,I.ITEM_CODE
          ,I.LOCATOR_CODE
          ,COUNT(*) SERIAL_CNT
          ,I.UNIT_PRICE UNITPRICE
          ,SUM(I.UNIT_PRICE) SUM_LINE_AMT
          --,V_PRICELIST PRICE_LIST_CODE
          ,PRICELIST_CODE PRICE_LIST_CODE
  FROM    SA_SALES_INTERFACE I 
  WHERE    I.TRANS_TYPE   = 'SIMCHARGE'
  AND      I.LOCATION = P_SUBINV_CODE
  AND      I.OU_CODE = P_OU_CODE    -- Add Yodchai L. 2014-Dec-04 9:09 , To prevent bug
  AND      I.STATUS = 'P'  -- STATUS P IS PROCESSING
  AND      I.SALES_PERSON =P_SALES_PERSON
  GROUP BY I.LOCATION 
          ,I.OU_CODE
          ,I.ITEM_CODE
          ,I.LOCATOR_CODE
          ,I.UNIT_PRICE
          ,I.PRICELIST_CODE
  ORDER BY I.ITEM_CODE,I.LOCATOR_CODE;
  
 CURSOR C_DETAIL_CHECK  IS
 SELECT  I.LOCATION SUBINV_CODE
          ,I.OU_CODE
          ,I.ITEM_CODE
          ,I.LOCATOR_CODE
          ,I.SERIAL
          ,I.TRANS_ID
  FROM    SA_SALES_INTERFACE I 
  WHERE   I.TRANS_TYPE = 'SIMCHARGE'
  AND I.STATUS IN ('P') --P means Processing, Get only this status because at begin we will update all to P(Processing)
  AND I.LOCATION  = P_SUBINV
  AND I.TRANS_DATE >=P_DATE_FROM
  AND I.TRANS_DATE <V_DATE_TO;


  CURSOR C_SN(P_OU_CODE SA_TRANSACTION.OU_CODE%TYPE, 
              P_SUBINV_CODE SA_TRANSACTION.SUBINV_CODE%TYPE,
              P_ITEM_CODE SA_TRANS_DTL.ITEM_CODE%TYPE,
              P_LOCATOR_CODE SA_TRANS_DTL.LOCATOR_CODE%TYPE,
              P_SALES_PERSON SA_TRANSACTION.SALESPERSON%TYPE,
              P_PRICELIST_CODE SA_TRANSACTION.PRICELIST_CODE%TYPE
              )  IS
  SELECT  I.SERIAL
          ,I.TRANS_ID
  FROM    SA_SALES_INTERFACE I
  WHERE    I.TRANS_TYPE   = 'SIMCHARGE'
  AND      I.LOCATION = P_SUBINV_CODE
  AND      I.OU_CODE = P_OU_CODE
  AND      I.ITEM_CODE = P_ITEM_CODE
  AND      I.LOCATOR_CODE = P_LOCATOR_CODE
  AND      I.PRICELIST_CODE = P_PRICELIST_CODE
  AND      I.STATUS = 'P'  -- STATUS P IS PROCESSING
  AND      I.SALES_PERSON =P_SALES_PERSON  -- Add Yodchai L. 2014-Dec-04 9:09 , To prevent bug
  ORDER BY I.SERIAL;

  V_TOTAL_AMT			SA_TRANSACTION.TOTAL_AMT%TYPE;
  V_VAT_AMT				SA_TRANSACTION.VAT_AMT%TYPE;
  V_NET_AMT				SA_TRANSACTION.NET_AMT%TYPE;
  V_VAT_CODE			SA_TRANSACTION.VAT_CODE%TYPE;
  V_VAT_RATE			SA_TRANSACTION.VAT_RATE%TYPE;

  V_DOC_NO				SA_TRANSACTION.DOC_NO%TYPE;	
  V_DOC_DATE			SA_TRANSACTION.DOC_DATE%TYPE;	
  V_STYPE					SA_TRANSACTION.STYPE_CODE%TYPE;
  V_CHANNEL				SA_TRANSACTION.CHANNEL_CODE%TYPE;
  V_SALES					SA_TRANSACTION.SALESPERSON%TYPE;
  V_UNIT_PRICE		SA_TRANS_DTL.UNITPRICE%TYPE;
  V_PAYMENT_CODE       SA_TRANS_PAY.PAYMENT_CODE%TYPE;
  V_CUSTCODE      SA_TRANSACTION.CUST_CODE%TYPE;
  V_BRANCH_CODE   SA_TRANSACTION.BRANCH_CODE%TYPE;
  V_TAX_ID        SA_TRANSACTION.TAX_ID%TYPE;
  V_NAME          SA_TRANSACTION.CUST_NAME%TYPE;
  V_ADDR 					SA_TRANSACTION.CUST_ADD%TYPE;
  V_AMPHUR 				SA_TRANSACTION.CUST_AMPHUR%TYPE;
  V_PROVINCE 			SA_TRANSACTION.CUST_PROVINCE%TYPE;
  V_POSTAL 				SA_TRANSACTION.CUST_POSTAL%TYPE; 
  V_TEL 					SA_TRANSACTION.CUST_TEL%TYPE;
  V_COUNT         NUMBER;
  V_PROGRAM_ID    VARCHAR2(20);
  V_IV_TRANS_TYPE   PB_PARAMETER_SETUP.PARAMETER_VALUE%TYPE;
  V_LINE_SEQ      NUMBER;
  V_SN_SEQ        NUMBER;
  V_USER          SA_TRANSACTION.UPD_BY%TYPE;
  --V_OU_CODE       SA_TRANSACTION.OU_CODE%TYPE;
  V_REC_COUNT     NUMBER;
  tmpNum          NUMBER;

  
      
  TMP_OU_CODE 			  SA_SALES_INTERFACE.OU_CODE%TYPE;
  TMP_SUBINV_CODE 		SA_SALES_INTERFACE.LOCATION%TYPE; 
  TMP_SALESPERSON 					SA_SALES_INTERFACE.SALES_PERSON%TYPE;
  TMP_PRICELIST_CODE 					SA_SALES_INTERFACE.PRICELIST_CODE%TYPE;
  TMP_ITEM_CODE 					SA_SALES_INTERFACE.ITEM_CODE%TYPE;
  TMP_EXIST_IN_MTL_INF    NUMBER;
  

BEGIN
   
   INSERT INTO JOBS_MONITOR(JOB_ID, JOB_SEQ, JOB_NAME, JOB_MESG, JOB_TIME, SUC_CNT, ERR_CNT, UPD_BY, UPD_DATE, UPD_PGM, STATUS)
   VALUES (601, 0, 'SP_GEN_INV_SIMCHARGE', 'Start Process ' || P_SUBINV || ' '  || to_char(P_DATE_FROM,'dd/mm/yyyy HH24:MI:SS'), SYSDATE, 0, 0, 'Jobs', SYSDATE, 'Jobs','Start Process');
   COMMIT;

-- Add by TM 27-Jan-2019
	SP_GEN_PRE_PROCESS
		(P_JOB_ID		=>	601
		,P_JOB_SEQ		=>	0.1
		,P_JOB_NAME		=>	'SP_GEN_INV_SIMCHARGE'
		,P_UPD_PGM		=>	'Jobs'
		,P_TRANS_TYPE	=>	'SIMCHARGE'
		-- for SP_GEN_INV_SIMCHARGE
		,P_SUBINV		=>	P_SUBINV
		,P_DATE_FROM	=>	P_DATE_FROM
		,P_DATE_TO		=>	P_DATE_TO);
-- End Add by TM 27-Jan-2019

   V_DATE_TO := P_DATE_TO+1;
   
   -- Add by Yodchai L. 2014-Dec-14 9:49 to prevent two process running on same subinventory
   SELECT COUNT(*) INTO V_COUNT FROM SA_SALES_INTERFACE 
   WHERE STATUS='P'
    AND TRANS_TYPE = 'SIMCHARGE'
    AND LOCATION  = P_SUBINV;

   IF V_COUNT > 0 THEN
     
     INSERT INTO JOBS_MONITOR(JOB_ID, JOB_SEQ, JOB_NAME, JOB_MESG, JOB_TIME, SUC_CNT, ERR_CNT, UPD_BY, UPD_DATE, UPD_PGM, STATUS)
     VALUES (601, 1, 'SP_GEN_INV_SIMCHARGE', 'Error There are another process is running for SUBINV: ' || P_SUBINV || ' '  || to_char(P_DATE_FROM,'dd/mm/yyyy HH24:MI:SS'), SYSDATE, 0, 0, 'Jobs', SYSDATE, 'Jobs','Error and Exit');
     COMMIT;

      RAISE_APPLICATION_ERROR
              (-20000
               , 'There are another process is running for SUBINV: ' || P_SUBINV); 
   END IF;
   
  UPDATE SA_SALES_INTERFACE SET STATUS='P', 
         ERR_DESC = NULL,
         OU_CODE = CASE WHEN COMP_CODE ='10' THEN 'TAC' ELSE 'DTN' END
  WHERE TRANS_TYPE = 'SIMCHARGE'
  AND LOCATION  = P_SUBINV
  AND STATUS IN ( 'R','E','P')
  AND TRANS_DATE >=P_DATE_FROM
  AND TRANS_DATE <V_DATE_TO;
  
  COMMIT;
  
  -- Duplicate card by Location
  FOR DUP IN CUR_SERIAL_DUP LOOP
      UPDATE  SA_SALES_INTERFACE SET  STATUS='I',
              ERR_DESC = 'DUPLICATE CARD_NUMBER' 
      WHERE STATUS='P'
      AND LOCATION  = P_SUBINV
      AND CARD_NUMBER = DUP.CARD_NUMBER
      AND TRANS_ID <> DUP.TRANS_ID;
  END LOOP;
  
  COMMIT;
   
-- BEGIN CHECK PARAMETER
  V_PROGRAM_ID := 'AUTO_SC';
  V_IV_TRANS_TYPE   := PB_PARAMETER_SETUP_PKG.GET_PARAMETER_VALUE('DTSART15','SALES');
  V_PAYMENT_CODE := P_PAYMENT_CODE;
--  V_PRICELIST := P_PRICELIST_CODE;
  V_CUSTCODE := P_CUSTCODE;
  V_CHANNEL := 'SHP';
  V_STYPE := '001';
  V_SALES := 'POS';
  V_DOC_DATE := SYSDATE;
  V_USER := 'POS';
	V_VAT_CODE := P_VAT_CODE; --'07'
	SELECT SA_VAT_PKG.GET_VAT_RATE(V_VAT_CODE) INTO V_VAT_RATE FROM dual;
  

   IF USERENV('LANG')<> 'US' THEN
      RAISE_APPLICATION_ERROR
              (-20000
               , 'Invalid Language USER ENV. Language must be US'); 
   END IF;

   SELECT COUNT(*) INTO V_COUNT FROM SA_VAT WHERE VAT_CODE = P_VAT_CODE;
   IF V_COUNT <=0 THEN
      RAISE_APPLICATION_ERROR
              (-20000
               , 'Invalid VAT_CODE:' || P_VAT_CODE); 
   END IF;
/*
   SELECT COUNT(*) INTO V_COUNT FROM SA_PRICELIST WHERE PRICELIST_CODE = P_PRICELIST_CODE;
   IF V_COUNT <=0 THEN
      RAISE_APPLICATION_ERROR
              (-20000
               , 'Invalid PRICELIST_CODE:' || P_PRICELIST_CODE); 
   END IF;
*/
   SELECT COUNT(*) INTO V_COUNT FROM SA_PAYMENT WHERE PAYMENT_CODE = P_PAYMENT_CODE;
   IF V_COUNT <=0 THEN
      RAISE_APPLICATION_ERROR
              (-20000
               , 'Invalid PAYMENT_CODE:' || P_PAYMENT_CODE); 
   END IF;
   
   SELECT COUNT(*) INTO V_COUNT FROM CI_CUSTOMER WHERE OU_CODE = 'TAC' AND CUST_CODE=P_CUSTCODE;
   IF V_COUNT <=0 THEN
      RAISE_APPLICATION_ERROR
              (-20000
               , 'Invalid CUSTOMER_CODE:' || P_CUSTCODE || ' OU : TAC' ); 
   END IF;
   
   SELECT COUNT(*) INTO V_COUNT FROM CI_CUSTOMER WHERE OU_CODE = 'DTN' AND CUST_CODE=P_CUSTCODE;
   IF V_COUNT <=0 THEN
      RAISE_APPLICATION_ERROR
              (-20000
               , 'Invalid CUSTOMER_CODE:' || P_CUSTCODE || ' OU : DTN' ); 
   END IF;
   
  V_REC_COUNT := 0;
  
  FOR REC_S IN CUR_SERIAL LOOP
      
      V_REC_COUNT := V_REC_COUNT+1;
      V_FOUND_INFO := 'N';
      --V_OU_CODE :=NULL;
      
      FOR REC_INFO IN CUR_SERIAL_STATUS(REC_S.CARD_NUMBER) LOOP
        
          V_FOUND_INFO := 'Y';
          /*
          V_OU_CODE := CASE WHEN REC_INFO.CURRENT_ORGANIZATION_ID in (298,299,318,143,278,258) THEN 'DTN'
                        ELSE 'TAC'
                       END;
          */           
          SELECT NVL(MIN(SUBINV_TYP_CODE),'I') INTO V_SUBINV_TYP_CODE 
          FROM PB_SUBINV 
          WHERE OU_CODE = REC_S.OU_CODE AND SUBINV_CODE = REC_INFO.CURRENT_SUBINVENTORY_CODE;
          
          /* 13-JUN-15 YL Prevent auto gen serial that just create invoice*/
          SELECT COUNT(*) INTO TMP_EXIST_IN_MTL_INF
          FROM MTL_SERIAL_NUMBERS_INTERFACE
          WHERE FM_SERIAL_NUMBER = REC_INFO.SERIAL_NUMBER;

          UPDATE SA_SALES_INTERFACE 
          SET 
              CUST_ADDR2 = CASE WHEN CUST_ADDR2 IS NULL THEN LOCATION
                                ELSE CUST_ADDR2
                           END
              --,LOCATION = REC_INFO.CURRENT_SUBINVENTORY_CODE
              ,LOCATOR_CODE = REC_INFO.LOCATOR_CODE
              ,ITEM_CODE = (SELECT MIN(ITEM_CODE)
                            FROM IN_ITEM 
                            WHERE OA_ITEM = REC_INFO.INVENTORY_ITEM_ID
                            AND OU_CODE = REC_S.OU_CODE)
              --,OU_CODE = V_OU_CODE   Use follow what CCB send 10=TAC , 20=DTN
              ,STATUS = CASE WHEN TMP_EXIST_IN_MTL_INF > 0 THEN 'E' 
                             WHEN REC_INFO.CURRENT_STATUS =3 AND V_SUBINV_TYP_CODE = 'S' THEN 'P' 
                             WHEN REC_INFO.CURRENT_STATUS =3 AND V_SUBINV_TYP_CODE <> 'S' THEN 'E' 
                             WHEN REC_INFO.CURRENT_STATUS =4 THEN 'F'
                             ELSE 'E'
                        END 
              ,ERR_DESC = CASE WHEN TMP_EXIST_IN_MTL_INF > 0 THEN  'THIS SERIAL IS IN INTERFACE TABLE MTL_SERIAL_NUMBERS_INTERFACE'
                             WHEN REC_INFO.CURRENT_STATUS =3 AND V_SUBINV_TYP_CODE = 'S' THEN NULL 
                             WHEN REC_INFO.CURRENT_STATUS =3 AND V_SUBINV_TYP_CODE <> 'S' THEN 'THIS SERIAL IS NOT IN SHOP V_SUBINV_TYP_CODE=' || V_SUBINV_TYP_CODE
                             WHEN REC_INFO.CURRENT_STATUS =4 THEN 'IGNORE THIS SERIAL BECAUSE STATUS IS OUT OF STOCK'
                             WHEN REC_INFO.CURRENT_STATUS =5 THEN 'THIS SERIAL IS IN INTRA-SIT'
                             ELSE 'CURRENT STATUS IS NOT IN INVENTORY.(' || TO_CHAR(REC_INFO.CURRENT_STATUS) || '),SUBINV:' || REC_INFO.CURRENT_SUBINVENTORY_CODE
                        END 
              ,ERR_DATE = CASE WHEN REC_INFO.CURRENT_STATUS =3 AND V_SUBINV_TYP_CODE = 'S' THEN NULL 
                             ELSE SYSDATE
                        END
              ,SERIAL = CASE WHEN REC_INFO.CURRENT_STATUS IN (3,4,5) THEN REC_INFO.SERIAL_NUMBER
                             ELSE NULL
                        END 
              
          WHERE TRANS_ID = REC_S.TRANS_ID;
          COMMIT;
          EXIT; -- Do only one loop
          
      END LOOP;
      
      IF V_FOUND_INFO = 'N' THEN
           UPDATE SA_SALES_INTERFACE
           SET STATUS = 'E'
              ,ERR_DESC = 'SERIAL IS NOT FOUND IN INVENTORY' 
              ,ERR_DATE = SYSDATE
           WHERE TRANS_ID = REC_S.TRANS_ID;
           --CONTINUE;
           GOTO continue_loop;
      END IF;
      
      --UPDATE STATUS E FOR LINE THAT CANNOT FIND ITEM_CODE
       UPDATE SA_SALES_INTERFACE
       SET STATUS = 'E'
          ,ERR_DESC = 'ITEM CODE IS NOT FOUND IN ITEM MASTER' 
          ,ERR_DATE = SYSDATE
       WHERE TRANS_ID = REC_S.TRANS_ID
       AND STATUS <> 'E'
       AND ITEM_CODE IS NULL;
       
      IF SQL%ROWCOUNT = 1 THEN
        GOTO continue_loop;
      END IF;
       
      --UPDATE PRICE GET BY PRICELIST_CODE
      SELECT COUNT(*),MIN(STD_PRICE) INTO V_COUNT, V_UNIT_PRICE
      FROM SA_PRICE_ITEM 
      WHERE ITEM_CODE = (SELECT ITEM_CODE FROM SA_SALES_INTERFACE WHERE TRANS_ID = REC_S.TRANS_ID)
      AND OU_CODE = REC_S.OU_CODE --V_OU_CODE
      AND PRICELIST_CODE = REC_S.PRICELIST_CODE;
            
       IF V_COUNT <= 0 THEN
                          
           UPDATE SA_SALES_INTERFACE
           SET STATUS = 'E'
              ,ERR_DESC = 'CANNOT FOUND THIS ITEM CODE IN PRICE LIST :'  || V_PRICELIST
              ,ERR_DATE = SYSDATE
           WHERE TRANS_ID = REC_S.TRANS_ID
           AND STATUS <> 'E';
           --CONTINUE;
           GOTO continue_loop;
           
      ELSE
           UPDATE SA_SALES_INTERFACE
           SET UNIT_PRICE = V_UNIT_PRICE
           WHERE TRANS_ID = REC_S.TRANS_ID;

      END IF;
      
      /* Check Sale Person that need to be exist in POS and in SU_USER and having permission in this subinventory*/
      SELECT COUNT(*) INTO V_COUNT
      FROM SU_USER 
      WHERE USER_ID = REC_S.SALES_PERSON;
      
      IF V_COUNT <= 0 THEN
                          
           UPDATE SA_SALES_INTERFACE
           SET STATUS = 'E'
              ,ERR_DESC = 'USER DOES NOT EXIST :'  || REC_S.SALES_PERSON
              ,ERR_DATE = SYSDATE
           WHERE TRANS_ID = REC_S.TRANS_ID
           AND STATUS <> 'E';
           --CONTINUE;
           GOTO continue_loop;
      END IF;
     
      -- Check User must has SALES_PERSON Code
      SELECT COUNT(*) INTO V_COUNT
      FROM SA_SALESPERSON 
      WHERE PERSON_CODE = REC_S.SALES_PERSON;
      
      IF V_COUNT <= 0 THEN
                          
           UPDATE SA_SALES_INTERFACE
           SET STATUS = 'E'
              ,ERR_DESC = 'SALESPERSON DOES NOT EXIST :'  || REC_S.SALES_PERSON
              ,ERR_DATE = SYSDATE
           WHERE TRANS_ID = REC_S.TRANS_ID
           AND STATUS <> 'E';
           --CONTINUE;
           GOTO continue_loop;

      END IF;
      
      -- Check User must has permission in that subinv

      SELECT OU_CODE, LOCATION, SALES_PERSON ,PRICELIST_CODE, TMP_ITEM_CODE
      INTO TMP_OU_CODE, TMP_SUBINV_CODE, TMP_SALESPERSON, TMP_PRICELIST_CODE, TMP_ITEM_CODE
      FROM SA_SALES_INTERFACE
      WHERE TRANS_ID = REC_S.TRANS_ID;
      
      SELECT COUNT(*) INTO V_COUNT
      FROM PB_USER_SUBINVEN 
      WHERE USER_ID = TMP_SALESPERSON
      AND OU_CODE = TMP_OU_CODE
      AND SUBINV_CODE = TMP_SUBINV_CODE;
      
      IF V_COUNT <= 0 THEN
                          
           UPDATE SA_SALES_INTERFACE
           SET STATUS = 'E'
              ,ERR_DESC = 'USER: ' || TMP_SALESPERSON  ||' DOES NOT PERMISSION ON SUBINVENTORY :'  || TMP_SUBINV_CODE || 'OU:' || TMP_OU_CODE
              ,ERR_DATE = SYSDATE
           WHERE TRANS_ID = REC_S.TRANS_ID
           AND STATUS <> 'E';
           GOTO continue_loop;

      END IF;
      
      /*
      --Check pricelist per item
      SELECT COUNT(*) INTO V_COUNT
      FROM SA_PRICE_ITEM 
      WHERE OU_CODE = TMP_OU_CODE
      AND PRICELIST_CODE = TMP_PRICELIST_CODE
      AND ITEM_CODE = TMP_ITEM_CODE;
      
      IF V_COUNT <= 0 THEN
                          
           UPDATE SA_SALES_INTERFACE
           SET STATUS = 'E'
              ,ERR_DESC = 'NO PRICE IN PRICELIST_CODE:' || TMP_PRICELIST_CODE  ||', ITEM_CODE :'  || TMP_ITEM_CODE || ' OU:' || TMP_OU_CODE
              ,ERR_DATE = SYSDATE
           WHERE TRANS_ID = REC_S.TRANS_ID
           AND STATUS <> 'E';
           GOTO continue_loop;

      END IF;
      */
      
      
      <<continue_loop>>
      NULL;

  END LOOP;
  
  
  FOR REC_S_CHK IN C_DETAIL_CHECK LOOP
        -- This procedure will validate agaist serail that just save but not yet interface
        tmpNum:= TRN_CHK_STOCK.get_available_qty(REC_S_CHK.OU_CODE, 
                                                REC_S_CHK.SUBINV_CODE,  
                                                REC_S_CHK.LOCATOR_CODE, 
                                                REC_S_CHK.ITEM_CODE, 
                                                REC_S_CHK.SERIAL, '%','AVL');
        IF nvl(tmpNum,0) <= 0 THEN
              UPDATE SA_SALES_INTERFACE SET STATUS='E'
                      ,ERR_DESC =  'This serial is not available.' 
                      ,ERR_DATE = SYSDATE
              WHERE TRANS_ID = REC_S_CHK.TRANS_ID;
        END IF;
  END LOOP
  
  
  COMMIT;  
  
-- BEGIN GENERATE INVOICE

   INSERT INTO JOBS_MONITOR(JOB_ID, JOB_SEQ, JOB_NAME, JOB_MESG, JOB_TIME, SUC_CNT, ERR_CNT, UPD_BY, UPD_DATE, UPD_PGM, STATUS)
   VALUES (601, 1, 'SP_GEN_INV_SIMCHARGE', 'End checking,Start Gen Invoice ' || P_SUBINV, SYSDATE, V_REC_COUNT, 0, 'Jobs', SYSDATE, 'Jobs','End checking,Start Gen Invoice');
   COMMIT;
   
  V_REC_COUNT :=0;
  FOR REC_H IN C_HEADER LOOP
      
      INSERT INTO JOBS_MONITOR(JOB_ID, JOB_SEQ, JOB_NAME, JOB_MESG, JOB_TIME, SUC_CNT, ERR_CNT, UPD_BY, UPD_DATE, UPD_PGM, STATUS)
      VALUES (601, 1, 'SP_GEN_INV_SIMCHARGE', 'Gen Invoice Begin in Loop C_HEADER ' || P_SUBINV, SYSDATE, V_REC_COUNT, 0, 'Jobs', SYSDATE, 'Jobs','End checking,Start Gen Invoice');
      
      V_REC_COUNT := V_REC_COUNT +1;
      V_SALES := REC_H.SALES_PERSON;
      V_USER := REC_H.SALES_PERSON;
       
			SELECT ROUND((100/(100+V_VAT_RATE))* REC_H.SUM_AMT,2) INTO V_TOTAL_AMT FROM Dual; --Exclude VAT
			
			V_VAT_AMT       := REC_H.SUM_AMT - V_TOTAL_AMT;
			V_NET_AMT       := REC_H.SUM_AMT;
      
      V_DOC_NO := PB_RUNNING_PKG.GET_RUNNING(REC_H.OU_CODE,
                               PB_PARAMETER_SETUP_PKG.GET_PARAMETER_VALUE('DTSART14','IV_PREFIX'),
                               REC_H.SUBINV_CODE,SYSDATE,
                               V_USER,V_PROGRAM_ID);
      
        SELECT  SUBSTR((CI_PRE_NAME_PKG.GET_NAME(PRE_CODE)||' '||FIRST_NAME||' '||LAST_NAME),1,200) CUST_NAME
                ,				CUR_ADD
                ,				CUR_AMPHUR
                ,				CUR_PROVINCE
                ,				CUR_POSTAL
                ,				CUR_TEL
                ,       BRANCH_CODE
                ,       DOC_NO
        INTO		V_NAME
                ,				V_ADDR
                ,				V_AMPHUR
                ,				V_PROVINCE
                ,				V_POSTAL
                ,				V_TEL
                ,       V_BRANCH_CODE
                ,       V_TAX_ID
        FROM 		CI_CUSTOMER
        WHERE		OU_CODE 		= REC_H.OU_CODE
        AND			CUST_CODE		= V_CUSTCODE;
        
  		   INSERT INTO SA_TRANSACTION
  					 (	
  					 		OU_CODE, 				SUBINV_CODE, 			DOC_NO, 				DOC_TYPE, 			DOC_DATE,
  					  	PRICELIST_CODE, CUST_CODE, 				STYPE_CODE, 		CHANNEL_CODE, 	STATUS, 
  					  	TOTAL_AMT, 			DISC_BAHT, 				SALESPERSON, 		VAT_AMT, 				VAT_RATE, 
  					  	NET_AMT, 				UPD_BY, 					VAT_CODE, 			UPD_DATE, 			UPD_PGM, 
  					  	POS_NO,					CASHIER_CODE,			HIRE_PURCHASE,	REPRINT,				HP_PERIODS,
  					  	CN_FLAG,				CASHIER_POINT,  	TEAM_HEADER,		AREA_MGR,				SHOP_MGR,				  	
  					  	SERIAL_FLG, 		CUST_ADD, 				CUST_AMPHUR, 		CUST_PROVINCE, 	CUST_POSTAL, 
  					  	CUST_TEL, 			CUST_NAME,        DISC_PERC,       BRANCH_CODE,    TAX_ID,
                GROSS_AMT,      REMARK
  					 )
  					 VALUES
  					 (
  					 		REC_H.OU_CODE,		    REC_H.SUBINV_CODE,		V_DOC_NO,			1,						SYSDATE,
  					 		V_PRICELIST,	        V_CUSTCODE,			      V_STYPE,			V_CHANNEL,		'N',
  					 		V_TOTAL_AMT,					0,										V_SALES,			V_VAT_AMT,		V_VAT_RATE,
  					 		V_NET_AMT,					V_USER,		          	V_VAT_CODE,				SYSDATE,			V_PROGRAM_ID,
  					 		NULL,			            NULL,		              'N',					0,						1,
  					 		'N',									NULL,	                NULL,	        NULL,	        NULL,  					 		
  					 		'Y',									V_ADDR,               V_AMPHUR,		  V_PROVINCE,	  V_POSTAL,
  					 		V_TEL,								V_NAME,           		0,		        V_BRANCH_CODE,    V_TAX_ID,
                REC_H.SUM_AMT,        'Auto Gen by Auto Invoice(SIM Charge)'
  					 );
             
              /********** INSERT HEADER TO IN_TRAN ***********/
              PTI_TRANSFER.POS_TO_INV('A',REC_H.OU_CODE, REC_H.SUBINV_CODE,
                                      '1', V_DOC_NO,
                                      V_IV_TRANS_TYPE,  
                                      V_USER, V_PROGRAM_ID);
                                      
							V_LINE_SEQ := 1;
              
              INSERT INTO JOBS_MONITOR(JOB_ID, JOB_SEQ, JOB_NAME, JOB_MESG, JOB_TIME, SUC_CNT, ERR_CNT, UPD_BY, UPD_DATE, UPD_PGM, STATUS)
              VALUES (601, 1, 'SP_GEN_INV_SIMCHARGE', 'Gen Invoice Before C_DETAIL', SYSDATE, V_REC_COUNT, 0, 'Jobs', SYSDATE, 'Jobs','End checking,Start Gen Invoice');
      
							FOR l IN C_DETAIL(REC_H.OU_CODE, REC_H.SUBINV_CODE,REC_H.SALES_PERSON) LOOP
                
                  INSERT INTO JOBS_MONITOR(JOB_ID, JOB_SEQ, JOB_NAME, JOB_MESG, JOB_TIME, SUC_CNT, ERR_CNT, UPD_BY, UPD_DATE, UPD_PGM, STATUS)
                  VALUES (601, 1, 'SP_GEN_INV_SIMCHARGE', 'Gen Invoice in Loop C_DETAIL', SYSDATE, V_REC_COUNT, 0, 'Jobs', SYSDATE, 'Jobs','End checking,Start Gen Invoice');
                  
									INSERT INTO SA_TRANS_DTL
									 (	
									 		OU_CODE, 				SUBINV_CODE, 				DOC_NO, 				DOC_TYPE, 			SEQ_NO,
											STYPE_CODE, 		BARCODE, 						ITEM_CODE, 			LOCATOR_CODE, 	PRICE_LIST_CODE, 
											QTY, 						UNITPRICE, 					AMOUNT, 				STD_PRICE, 			UPD_BY, 
											UPD_DATE, 			UPD_PGM,          	
                      UCOST,          
                      GROSS_AMT,      
                      DISC_AMT,       DISC_TOT
									 )
									 VALUES
									 (
									 		REC_H.OU_CODE,     REC_H.SUBINV_CODE, 	  V_DOC_NO,				'1',							V_LINE_SEQ,
									 		'001',							l.ITEM_CODE,					l.ITEM_CODE,		l.LOCATOR_CODE,		l.PRICE_LIST_CODE,
									 		l.SERIAL_CNT,			  l.UNITPRICE,					l.SUM_LINE_AMT,	l.UNITPRICE,			V_USER,
									 		SYSDATE,					  V_PROGRAM_ID,        
                      SA_ITEM_PKG.GET_UNIT_COST(REC_H.OU_CODE, REC_H.SUBINV_CODE,l.LOCATOR_CODE,l.ITEM_CODE), 	
                      l.SUM_LINE_AMT,
                      0,0
									 );
                   
                  INSERT INTO JOBS_MONITOR(JOB_ID, JOB_SEQ, JOB_NAME, JOB_MESG, JOB_TIME, SUC_CNT, ERR_CNT, UPD_BY, UPD_DATE, UPD_PGM, STATUS)
                  VALUES (601, 1, 'SP_GEN_INV_SIMCHARGE', 'Gen Invoice in Loop C_DETAIL After insert detail ' || P_SUBINV, SYSDATE, V_REC_COUNT, 0, 'Jobs', SYSDATE, 'Jobs','End checking,Start Gen Invoice');

					        		/********** INSERT DETAIL TO IN_TRAN ***********/ 
					            PTI_TRANSFER.POS_TO_INV('A',REC_H.OU_CODE,REC_H.SUBINV_CODE,
								                              '1',V_DOC_NO,V_IV_TRANS_TYPE,
							                              V_USER,V_PROGRAM_ID,l.ITEM_CODE,
							                              l.LOCATOR_CODE,l.ITEM_CODE,V_LINE_SEQ);
            
                   
											V_SN_SEQ := 1;
                      
                      INSERT INTO JOBS_MONITOR(JOB_ID, JOB_SEQ, JOB_NAME, JOB_MESG, JOB_TIME, SUC_CNT, ERR_CNT, UPD_BY, UPD_DATE, UPD_PGM, STATUS)
                      VALUES (601, 1, 'SP_GEN_INV_SIMCHARGE', 'Gen Invoice Befor C_SN ' || P_SUBINV, SYSDATE, V_REC_COUNT, 0, 'Jobs', SYSDATE, 'Jobs','End checking,Start Gen Invoice');

                      
											FOR s IN C_SN(REC_H.OU_CODE, REC_H.SUBINV_CODE, l.ITEM_CODE, l.LOCATOR_CODE,REC_H.SALES_PERSON,l.PRICE_LIST_CODE) LOOP

                                INSERT INTO JOBS_MONITOR(JOB_ID, JOB_SEQ, JOB_NAME, JOB_MESG, JOB_TIME, SUC_CNT, ERR_CNT, UPD_BY, UPD_DATE, UPD_PGM, STATUS)
                                VALUES (601, 1, 'SP_GEN_INV_SIMCHARGE', 'Gen Invoice in loop C_SN', SYSDATE, V_REC_COUNT, 0, 'Jobs', SYSDATE, 'Jobs','End checking,Start Gen Invoice');

                                
									  		  	   INSERT INTO SA_TRANS_SN_LOT  --SA_AUIV_TRANS_SN_LOT
									  		  	        ( 
									  		  	          OU_CODE,             SUBINV_CODE,          DOC_NO,             DOC_TYPE,      SEQ_NO, 
									  		  	          ITEM_CODE,           SEQ_SN_LOT,           LOCATOR_CODE,       SERIAL,        QTY, 
									  		  	          UPD_BY,              UPD_DATE,             UPD_PGM
									  		  	        )
									  		  	   VALUES 
									  		  	        (
									  		  	          REC_H.OU_CODE,        REC_H.SUBINV_CODE, 			V_DOC_NO,						'1',						V_LINE_SEQ,
									  		  	          l.ITEM_CODE,         V_SN_SEQ,             l.LOCATOR_CODE,        s.SERIAL,      1,
									  		  	          V_USER,                SYSDATE,				 		V_PROGRAM_ID
									  		  	        );
									  		  	        
                         UPDATE SA_SALES_INTERFACE 
                         SET INVOICE_NO =V_DOC_NO, 
                             INVOICE_DATE=V_DOC_DATE,
                             STATUS='C',
                             ERR_DESC=NULL
                         WHERE TRANS_ID = s.TRANS_ID;
               
													V_SN_SEQ := V_SN_SEQ+1;
											END LOOP; -- CUR_SN

				         /********** INSERT SERIAL TO IN_TRAN***********/ 
			           PTI_TRANSFER.PTI_EDIT_SN_LOT(REC_H.OU_CODE,REC_H.SUBINV_CODE,
			                                     '1',V_DOC_NO,
			                                     V_IV_TRANS_TYPE,
			                                     l.ITEM_CODE,l.LOCATOR_CODE,V_USER,V_PROGRAM_ID,V_LINE_SEQ);
			                                     
									V_LINE_SEQ := V_LINE_SEQ+1;
                  
                   
               END LOOP; --C_DETAIL
               

              -- Insert Payment
              INSERT INTO SA_TRANS_PAY
               (	
                  OU_CODE, 				SUBINV_CODE, 			DOC_NO, 				DOC_TYPE, 			PAYMENT_CODE,
                  PAY_AMT, 				BANK_CODE,			UPD_BY, 					UPD_DATE, 			UPD_PGM,
                  PAY_NET,				REF_NO,					CREDIT_CARD)
               VALUES
               (  REC_H.OU_CODE,         REC_H.SUBINV_CODE,		V_DOC_NO,							'1',					V_PAYMENT_CODE,
                  REC_H.SUM_AMT,  				NULL,					      V_USER,	 		          SYSDATE,			V_PROGRAM_ID,		
                  REC_H.SUM_AMT,					NULL,					       NULL);


    END LOOP; -- C_HEADER
  
    COMMIT;
    
   INSERT INTO JOBS_MONITOR(JOB_ID, JOB_SEQ, JOB_NAME, JOB_MESG, JOB_TIME, SUC_CNT, ERR_CNT, UPD_BY, UPD_DATE, UPD_PGM, STATUS)
   VALUES (601, 2, 'SP_GEN_INV_SIMCHARGE', 'End Gen Invoice ' || P_SUBINV, SYSDATE, V_REC_COUNT, 0, 'Jobs', SYSDATE, 'Jobs','End Process');
   COMMIT;
  
END SP_GEN_INV_SIMCHARGE;
/
