CREATE OR REPLACE PACKAGE APPS.XCUST_DTSCF_INF_PKG
AS
   /******************************************************************************
      NAME:       XCUST_DTSCF_INF_PKG
      PURPOSE:

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        28/09/2015      ktpice       1. Created this package.
   ******************************************************************************/


   PROCEDURE MAIN_PO_UPLOAD (errbuf              OUT VARCHAR2,
                             retcode             OUT NUMBER,
                             P_ORG_ID         IN     NUMBER,
                             P_LAST_DATE      IN     VARCHAR2,
                             P_VOUCHER_FR     IN     VARCHAR2,
                             P_VOUCHER_TO     IN     VARCHAR2,
                             P_FH_REC_TYPE    IN     VARCHAR2,
                             P_BH_REC_TYPE    IN     VARCHAR2,
                             P_PO_REC_TYPE    IN     VARCHAR2,
                             P_CM_REC_TYPE    IN     VARCHAR2,
                             P_AT_REC_TYPE    IN     VARCHAR2,
                             P_BC_REC_TYPE    IN     VARCHAR2,
                             P_FC_REC_TYPE    IN     VARCHAR2,
                             P_DEST_NAME      IN     VARCHAR2,
                             P_CONC_TYPE      IN     VARCHAR2,
                             P_USER_ID        IN     NUMBER);
                             
  procedure get_dir_list( p_directory in varchar2 );
  
                             
  PROCEDURE MAIN_BRA_DOWNLOAD (errbuf              OUT VARCHAR2,
                             retcode               OUT NUMBER,
                             P_FILE_NAME         IN     VARCHAR2,
                             P_REPLACE           IN     VARCHAR2,
                             P_USER_ID           IN     NUMBER);
                             
  PROCEDURE MAIN_MONITOR  (errbuf              OUT VARCHAR2,
                             retcode               OUT NUMBER,
                             P_FOLDER_NAME         IN     VARCHAR2,
                             P_REQUEST_ID          IN     NUMBER,
                             P_PROGRAM             IN     VARCHAR2);
                             
 PROCEDURE MAIN_CLEAR_MONITOR  (errbuf              OUT VARCHAR2,
                             retcode               OUT NUMBER,
                             P_FOLDER_NAME         IN     VARCHAR2,
                             P_DAY          IN     NUMBER);
   
END XCUST_DTSCF_INF_PKG;
/
