CREATE OR REPLACE PACKAGE BODY APPS.XCUST_DTSCF_INF_PKG
AS
   /******************************************************************************
      NAME:       XCUST_DTSCF_INF_PKG
      PURPOSE:

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        28/09/2015     ktpice       1. Created this package body.
   ******************************************************************************/
   PROCEDURE WRITE_LOG (pCHAR IN VARCHAR2, pTYPE IN VARCHAR2)
   IS
   BEGIN
      IF UPPER (pTYPE) = 'LOG'
      THEN
         fnd_file.put_LINE (FND_FILE.LOG, pCHAR);
      ELSE
         fnd_file.put_line (FND_FILE.OUTPUT, pCHAR);
      END IF;

      DBMS_OUTPUT.put_line (Pchar);
   exception when others then
       DBMS_OUTPUT.put_line (sqlerrm);
   END WRITE_LOG;
   
   

   PROCEDURE MAIN_PO_UPLOAD (errbuf              OUT VARCHAR2,
                             retcode             OUT NUMBER,
                             P_ORG_ID         IN     NUMBER,
                             P_LAST_DATE      IN     VARCHAR2,
                             P_VOUCHER_FR     IN     VARCHAR2,
                             P_VOUCHER_TO     IN     VARCHAR2,
                             P_FH_REC_TYPE    IN     VARCHAR2,
                             P_BH_REC_TYPE    IN     VARCHAR2,
                             P_PO_REC_TYPE    IN     VARCHAR2,
                             P_CM_REC_TYPE    IN     VARCHAR2,
                             P_AT_REC_TYPE    IN     VARCHAR2,
                             P_BC_REC_TYPE    IN     VARCHAR2,
                             P_FC_REC_TYPE    IN     VARCHAR2,
                             P_DEST_NAME      IN     VARCHAR2,
                             P_CONC_TYPE      IN     VARCHAR2,
                             P_USER_ID        IN     NUMBER)
   IS
      
      CURSOR B1                                                   --MAIN BATCH
      IS
           SELECT   DISTINCT
                    V.VENDOR_NUMBER, 
                    --NVL(V.VENDOR_NAME_ALT,NVL(V.VENDOR_NAME,' ')) VENDOR_NAME,
                    NVL(replace(convert(nvl(V.VENDOR_NAME_ALT,V.VENDOR_NAME),'us7ascii','utf8'),'?',''),' ')  VENDOR_NAME,
                    AP.INVOICE_CURRENCY_CODE
             FROM   ap_invoices_all AP, AP_VENDORS_V V        
            WHERE       AP.VENDOR_ID = V.VENDOR_ID
                    AND AP.CANCELLED_DATE IS NULL
                    AND AP.WFAPPROVAL_STATUS IN ('NOT REQUIRED')
                    AND AP_INVOICES_PKG.GET_POSTING_STATUS (AP.INVOICE_ID) = 'Y'
                    AND AP_INVOICES_PKG.GET_APPROVAL_STATUS (AP.INVOICE_ID,
                                                 AP.INVOICE_AMOUNT,
                                                 AP.PAYMENT_STATUS_FLAG,
                                                 AP.INVOICE_TYPE_LOOKUP_CODE) ='APPROVED'
                    AND AP.PAYMENT_STATUS_FLAG = 'N'
                     AND CASE P_CONC_TYPE
                        WHEN 'R' THEN 0
                        ELSE AP.INVOICE_ID
                     END NOT IN
                           (SELECT   NVL(BUYER_UNIQUE_DOC_ID,0)
                              FROM   TAC_SCF_INVOICE_TBL
                            WHERE RECORD_TYPE_CODE IN ('PO','CM'))
                    AND  V.VENDOR_NUMBER IN (SELECT   FLV.LOOKUP_CODE
                                                FROM   FND_LOOKUP_VALUES_VL FLV
                                                WHERE   FLV.LOOKUP_TYPE = 'TAC_SCF_SUPPLIER'
                                                AND FLV.ENABLED_FLAG = 'Y'
                                                AND (FLV.END_DATE_ACTIVE IS NULL
                                                OR FLV.END_DATE_ACTIVE > SYSDATE))
                    AND AP.PAY_GROUP_LOOKUP_CODE IN (SELECT   FLV.LOOKUP_CODE
                                                FROM   FND_LOOKUP_VALUES_VL FLV
                                                WHERE   FLV.LOOKUP_TYPE = 'TAC_SCF_PAYGROUP'
                                                AND FLV.ENABLED_FLAG = 'Y'
                                                AND (FLV.END_DATE_ACTIVE IS NULL
                                                OR FLV.END_DATE_ACTIVE > SYSDATE))
                    --PARAMETER
                    AND AP.ORG_ID = P_ORG_ID
                    AND TO_CHAR (AP.LAST_UPDATE_DATE, 'YYYYMMDD') =
                          TO_CHAR (
                             TO_DATE (
                                NVL (
                                   P_LAST_DATE,
                                   TO_CHAR (AP.LAST_UPDATE_DATE,
                                            'YYYY/MM/DD HH24:MI:SS')
                                ),
                                'YYYY/MM/DD HH24:MI:SS'
                             ),
                             'YYYYMMDD'
                          )
                    /*AND AP.INVOICE_NUM BETWEEN NVL (P_INVOICE_FR,
                                                    AP.INVOICE_NUM)
                                           AND  NVL (P_INVOICE_TO,
                                                     AP.INVOICE_NUM)*/
                      AND NVL(AP.DOC_SEQUENCE_VALUE,NVL(AP.VOUCHER_NUM,'-999')) BETWEEN NVL (P_VOUCHER_FR,
                                                    NVL(AP.DOC_SEQUENCE_VALUE,NVL(AP.VOUCHER_NUM,'-999')))
                                           AND  NVL (P_VOUCHER_TO,
                                                     NVL(AP.DOC_SEQUENCE_VALUE,NVL(AP.VOUCHER_NUM,'-999')))
                                                     
                                                     
        ORDER BY V.VENDOR_NUMBER;



      CURSOR C1 (
         P_VENDOR_NUMBER                 VARCHAR2,
         P_CURRENCY VARCHAR2
      )
      IS
        SELECT    AP.INVOICE_ID,
                  AP.INVOICE_NUM,
                  NVL(AP.DOC_SEQUENCE_VALUE,AP.VOUCHER_NUM) VOUCHER_NUM,
                  AP.INVOICE_CURRENCY_CODE,
                  V.VENDOR_NUMBER,
                  --SUBSTR( NVL(V.VENDOR_NAME_ALT,NVL(V.VENDOR_NAME,' ')),1,35) VENDOR_NAME,
                  NVL(SUBSTR(replace(convert( NVL(V.VENDOR_NAME_ALT,V.VENDOR_NAME),'us7ascii','utf8'),'?',''),1,35),' ') VENDOR_NAME,
                  TO_CHAR (AP.INVOICE_DATE, 'YYMMDD') INVOICE_DATE,
                  ROUND(NVL (AP.INVOICE_AMOUNT, 0),2) AMOUNT,
                  --replace(to_char(round(NVL (AP.INVOICE_AMOUNT, 0),2),'FM999999999999.90'),'.','') AMOUNT,
                  --to_char(NVL (AP.INVOICE_AMOUNT, 0),'FM999999999990.90') AMOUNT,
                  --SUBSTR (NVL(AP.DESCRIPTION,' '), 1, 50) DESCRIPTION,
                  NVL(SUBSTR (replace(convert(AP.DESCRIPTION,'us7ascii','utf8'),'?',''), 1, 50),' ') DESCRIPTION,
                  AP.AWT_GROUP_ID,
                  AP.INVOICE_TYPE_LOOKUP_CODE INVOICE_TYPE,
                  AP.LAST_UPDATE_DATE
           FROM   ap_invoices_all AP, AP_VENDORS_V V
          WHERE       AP.VENDOR_ID = V.VENDOR_ID
                  AND AP.CANCELLED_DATE IS NULL
                  AND AP.WFAPPROVAL_STATUS IN ('NOT REQUIRED')
                  AND AP_INVOICES_PKG.GET_POSTING_STATUS (AP.INVOICE_ID) = 'Y'
                  AND AP_INVOICES_PKG.GET_APPROVAL_STATUS (AP.INVOICE_ID,
                                                 AP.INVOICE_AMOUNT,
                                                 AP.PAYMENT_STATUS_FLAG,
                                                 AP.INVOICE_TYPE_LOOKUP_CODE) ='APPROVED'
                  AND AP.PAYMENT_STATUS_FLAG = 'N'
                  AND CASE P_CONC_TYPE
                        WHEN 'R' THEN 0
                        ELSE AP.INVOICE_ID
                  END NOT IN
                           (SELECT   NVL(BUYER_UNIQUE_DOC_ID,0)
                              FROM   TAC_SCF_INVOICE_TBL
                              WHERE RECORD_TYPE_CODE IN ('PO','CM'))
                  AND  V.VENDOR_NUMBER IN (SELECT   FLV.LOOKUP_CODE
                                                FROM   FND_LOOKUP_VALUES_VL FLV
                                                WHERE   FLV.LOOKUP_TYPE = 'TAC_SCF_SUPPLIER'
                                                AND FLV.ENABLED_FLAG = 'Y'
                                                AND (FLV.END_DATE_ACTIVE IS NULL
                                                OR FLV.END_DATE_ACTIVE > SYSDATE))
                  --PARAMETER
                  AND AP.ORG_ID = P_ORG_ID
                  AND TO_CHAR (AP.LAST_UPDATE_DATE, 'YYYYMMDD') =
                        TO_CHAR (
                           TO_DATE (
                              NVL (
                                 P_LAST_DATE,
                                 TO_CHAR (AP.LAST_UPDATE_DATE,
                                          'YYYY/MM/DD HH24:MI:SS')
                              ),
                              'YYYY/MM/DD HH24:MI:SS'
                           ),
                           'YYYYMMDD'
                        )
                 /* AND AP.INVOICE_NUM BETWEEN NVL (P_INVOICE_FR,
                                                  AP.INVOICE_NUM)
                                         AND  NVL (P_INVOICE_TO,
                                                   AP.INVOICE_NUM)*/
                   AND NVL(AP.DOC_SEQUENCE_VALUE,NVL(AP.VOUCHER_NUM,'-999')) BETWEEN NVL (P_VOUCHER_FR,
                                                    NVL(AP.DOC_SEQUENCE_VALUE,NVL(AP.VOUCHER_NUM,'-999')))
                                           AND  NVL (P_VOUCHER_TO,
                                                     NVL(AP.DOC_SEQUENCE_VALUE,NVL(AP.VOUCHER_NUM,'-999')))
                  AND V.VENDOR_NUMBER = P_VENDOR_NUMBER
                  AND AP.INVOICE_CURRENCY_CODE = P_CURRENCY
         ORDER BY AP.INVOICE_NUM;


      CURSOR ATT (
         P_INVOICE_ID                 NUMBER
      )
      IS
           SELECT   AP.INVOICE_NUM,
                    TO_CHAR(D.ACCOUNTING_DATE,'YYMMDD') APPLIED_DATE,
                    --NVL(AP.DESCRIPTION,' ') DESCRIPTION,
                    NVL(replace(convert(AP.DESCRIPTION,'us7ascii','utf8'),'?',''),' ') DESCRIPTION,
                    NVL(SUM (D.AMOUNT),0) AMOUNT
                    --replace(to_char(round(NVL (SUM (D.AMOUNT), 0),2),'FM999999999999.90'),'.','') AMOUNT
                    --to_char(round(NVL (SUM (D.AMOUNT), 0),2),'FM999999999999.90') AMOUNT
             FROM   ap_invoices_all AP, AP_INVOICE_DISTRIBUTIONS_ALL D
            WHERE   AP.INVOICE_ID = D.PARENT_INVOICE_ID
                    AND D.INVOICE_ID = P_INVOICE_ID                  --1210849
         GROUP BY   AP.INVOICE_NUM, D.ACCOUNTING_DATE, AP.DESCRIPTION;
         
         
         
         CURSOR INI IS
         SELECT   DISTINCT
                    V.VENDOR_NUMBER, 
                    --NVL(V.VENDOR_NAME_ALT,NVL(V.VENDOR_NAME,' ')) VENDOR_NAME,
                    NVL(replace(convert(nvl(V.VENDOR_NAME_ALT,V.VENDOR_NAME),'us7ascii','utf8'),'?',''),' ')  VENDOR_NAME,
                    AP.INVOICE_CURRENCY_CODE,
                    AP.INVOICE_NUM
             FROM   ap_invoices_all AP, AP_VENDORS_V V        
            WHERE       AP.VENDOR_ID = V.VENDOR_ID
                    AND AP.CANCELLED_DATE IS NULL
                    and upper(ap.WFAPPROVAL_STATUS) = 'INITIATED'                                                                
                    /*AND AP.WFAPPROVAL_STATUS IN ('NOT REQUIRED')
                    AND AP_INVOICES_PKG.GET_POSTING_STATUS (AP.INVOICE_ID) = 'Y'
                    AND AP_INVOICES_PKG.GET_APPROVAL_STATUS (AP.INVOICE_ID,
                                                 AP.INVOICE_AMOUNT,
                                                 AP.PAYMENT_STATUS_FLAG,
                                                 AP.INVOICE_TYPE_LOOKUP_CODE) ='APPROVED'*/
                    AND AP.PAYMENT_STATUS_FLAG = 'N'
                     AND CASE P_CONC_TYPE
                        WHEN 'R' THEN 0
                        ELSE AP.INVOICE_ID
                     END NOT IN
                           (SELECT   NVL(BUYER_UNIQUE_DOC_ID,0)
                              FROM   TAC_SCF_INVOICE_TBL
                            WHERE RECORD_TYPE_CODE IN ('PO','CM'))
                    AND  V.VENDOR_NUMBER IN (SELECT   FLV.LOOKUP_CODE
                                                FROM   FND_LOOKUP_VALUES_VL FLV
                                                WHERE   FLV.LOOKUP_TYPE = 'TAC_SCF_SUPPLIER'
                                                AND FLV.ENABLED_FLAG = 'Y'
                                                AND (FLV.END_DATE_ACTIVE IS NULL
                                                OR FLV.END_DATE_ACTIVE > SYSDATE))
                    AND AP.PAY_GROUP_LOOKUP_CODE IN (SELECT   FLV.LOOKUP_CODE
                                                FROM   FND_LOOKUP_VALUES_VL FLV
                                                WHERE   FLV.LOOKUP_TYPE = 'TAC_SCF_PAYGROUP'
                                                AND FLV.ENABLED_FLAG = 'Y'
                                                AND (FLV.END_DATE_ACTIVE IS NULL
                                                OR FLV.END_DATE_ACTIVE > SYSDATE))
                    --PARAMETER
                    AND AP.ORG_ID = P_ORG_ID
                    AND TO_CHAR (AP.LAST_UPDATE_DATE, 'YYYYMMDD') =
                          TO_CHAR (
                             TO_DATE (
                                NVL (
                                   P_LAST_DATE,
                                   TO_CHAR (AP.LAST_UPDATE_DATE,
                                            'YYYY/MM/DD HH24:MI:SS')
                                ),
                                'YYYY/MM/DD HH24:MI:SS'
                             ),
                             'YYYYMMDD'
                          )
                    /*AND AP.INVOICE_NUM BETWEEN NVL (P_INVOICE_FR,
                                                    AP.INVOICE_NUM)
                                           AND  NVL (P_INVOICE_TO,
                                                     AP.INVOICE_NUM)*/
                      AND NVL(AP.DOC_SEQUENCE_VALUE,NVL(AP.VOUCHER_NUM,'-999')) BETWEEN NVL (P_VOUCHER_FR,
                                                    NVL(AP.DOC_SEQUENCE_VALUE,NVL(AP.VOUCHER_NUM,'-999')))
                                           AND  NVL (P_VOUCHER_TO,
                                                     NVL(AP.DOC_SEQUENCE_VALUE,NVL(AP.VOUCHER_NUM,'-999')))
                                                     
                                                     
        ORDER BY V.VENDOR_NUMBER;
                 

      V_SCF_SUPPLIER         VARCHAR2 (240);
      V_SCF_ORG_ID           VARCHAR2 (240);
      V_SCF_ORG_NAME         VARCHAR2 (25);

      fh                     NUMBER := 0;
      bh                     NUMBER := 0;
      bc                     NUMBER := 0;
      c                      NUMBER := 0;
      p                      NUMBER := 0;
      a                      NUMBER := 0;
      po                     NUMBER := 0;
      cm                     NUMBER := 0;
      ath                    NUMBER := 0;
      e                      NUMBER := 0;
      init                    NUMBER := 0;
     

      
      V_BUYER_ID             VARCHAR2 (240);
      V_BUYER_NUMBER         VARCHAR2 (240);
      V_ID_MODIFIER          NUMBER;
      V_CONC_REQ_ID          NUMBER := fnd_global.conc_request_id;
      V_FILE_NAME            VARCHAR2 (240);
      V_FILE_NAME_MONITOR    VARCHAR2 (240);
      CNT_FILE               NUMBER;
      V_BATCH_NUMBER         NUMBER := 0;
      V_CHR_BATCH_NUMBER     VARCHAR2 (7);
      V_TAX_RATE             NUMBER := 0;
      V_TAX_AMOUNT           NUMBER := 0;
      V_PO_AMOUNT            NUMBER := 0;
      V_PO_CM_AMOUNT         NUMBER := 0;
      V_PO_CM_AMT_TBL        NUMBER := 0;
      V_MATURITY_DATE        VARCHAR2 (11);
      V_REC_TYPE             VARCHAR2 (2);
      V_ATT_DOC_ID           NUMBER := 0;
      V_ENTITY_CNT           NUMBER := 0;
      V_TOTAL_PO_AMT         NUMBER := 0;
      V_TOTAL_CM_AMT         NUMBER := 0;
      V_TOTAL_OB_AMT_BATCH   NUMBER := 0;
      V_TOTAL_OB_AMT         NUMBER := 0;
      V_RECORD_CNT           NUMBER := 0;
      V_MSG_ERR              VARCHAR2(2000);
      V_PATH                 VARCHAR2(240);
      
      
      V_INTEGRATION_NAME   VARCHAR2(100) := 'SCF001';
      v_dir                VARCHAR2 (5120) := 'TAC_SCF_INT_OUT';
      v_dir_monitor        VARCHAR2 (5120) := 'TAC_SCF_MONITOR_OUT';
      
      
      in_FILE        UTL_FILE.FILE_TYPE;
      datarow        VARCHAR2 (32767);
      
      V_TRAN_START_DATE    DATE ;
      V_TRAN_END_DATE      DATE ;
      V_LOAD_START_DATE  DATE ;
      V_LOAD_END_DATE    DATE ;
      
      l_success NUMBER;    
      v_errbuf  VARCHAR2(1000);
      v_retcode NUMBER;
      
      

   BEGIN
   
      begin
        DBMS_APPLICATION_INFO.set_client_info (P_ORG_ID);
      end;
      
      delete from TAC_SCF_LOG_TBL where CREATION_DATE < SYSDATE-5;
      COMMIT;
      
      V_TRAN_START_DATE := SYSDATE;
      
      
      write_log (' ', 'out');
      write_log (' ', 'out');
      
      IF P_CONC_TYPE = 'R' THEN
        write_log ('Program : TAC : Interface Payment Obligation (Rerun)', 'out');
        write_log ('Parameter :', 'out');
        write_log ('    Org_id = ' || P_ORG_ID, 'out');
        write_log (
         '    Last Update Date = '
         || TO_CHAR (TO_DATE (P_LAST_DATE, 'YYYY/MM/DD HH24:MI:SS'),
                     'DD Mon YYYY'),
         'out'
      );
        write_log ('    Voucher Number From = ' || P_VOUCHER_FR, 'out');
        write_log ('    Voucher Number To = ' || P_VOUCHER_TO, 'out');
      
      ELSE
          write_log ('Program : TAC : Interface Payment Obligation', 'out');
          write_log ('Parameter :', 'out');
          write_log ('    Org_id = ' || P_ORG_ID, 'out');
          write_log (
         '    Last Update Date = '
         || TO_CHAR (TO_DATE (P_LAST_DATE, 'YYYY/MM/DD HH24:MI:SS'),
                     'DD Mon YYYY'),
         'out'
         );
      
      END IF;
      
      write_log (' ', 'out');
      write_log (
         'Concurrent Start : ' || TO_CHAR (SYSDATE, 'DD Mon YYYY HH24.Mi'),
         'out'
         );
      write_log ('------------------------------------------------------------------------------------------------', 'out');
         
      INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Program : TAC : Interface Payment Obligation',SYSDATE,'N');
      INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Parameter :',SYSDATE,'N');
      INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'    Org_id = ' || P_ORG_ID,SYSDATE,'N');
      INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'    Last Update Date = '
         || TO_CHAR (TO_DATE (P_LAST_DATE, 'YYYY/MM/DD HH24:MI:SS'),'DD Mon YYYY'),SYSDATE,'N');
      INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,' ',SYSDATE,'N');
      INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Concurrent Start : ' || TO_CHAR (SYSDATE, 'DD Mon YYYY HH24.Mi'),SYSDATE,'N');
      INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'------------------------------------------------------------------------------------------------',SYSDATE,'N');
      COMMIT;
      
      
      
      BEGIN
        select DIRECTORY_PATH 
        INTO V_PATH
        from all_directories 
        where directory_name = V_DIR;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
         
            V_PATH := null;

      END;
      
       BEGIN
            SELECT   SUBSTR(FLV.MEANING,1,25)
              INTO   V_SCF_ORG_NAME
              FROM   FND_LOOKUP_VALUES_VL FLV
             WHERE       P_ORG_ID = FLV.LOOKUP_CODE
                     AND FLV.LOOKUP_TYPE = 'TAC_SCF_ORG_NAME'
                     AND FLV.ENABLED_FLAG = 'Y'
                     AND (FLV.END_DATE_ACTIVE IS NULL
                          OR FLV.END_DATE_ACTIVE > SYSDATE);
       EXCEPTION
            WHEN OTHERS
            THEN
               V_SCF_ORG_NAME := NULL;

       END;
       

      
      
      
     IF V_PATH IS NULL 
     THEN
            
            e := e + 1;
            write_log ('Error ' , 'out');
            write_log (' ', 'out');
            write_log ('Error SCF001-003 : Not found path (ERP) (It Operartion)', 'out');
            write_log (' ', 'out');
            write_log ('------------------------------------------------------------------------------------------------', 'out');
           
            INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error ',SYSDATE,'N');
            INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,' ',SYSDATE,'N');
            INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error SCF001-003 : Not found path (ERP) (It Operartion)',SYSDATE,'N');
            INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,' ',SYSDATE,'N');
            INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'------------------------------------------------------------------------------------------------',SYSDATE,'N');
           
          BEGIN
            INSERT INTO TAC_SCF_MONITOR 
            VALUES(NVL(V_SCF_ORG_NAME,P_ORG_ID),
            9999,   
            '9999',
            '9999',
            V_INTEGRATION_NAME,
            SYSDATE,
            SYSDATE,
            SYSDATE,
            SYSDATE,
            'E',
            'SCF001-003',
            'Not found path (ERP) (It Operartion)',
            V_CONC_REQ_ID);
            
            COMMIT;
          EXCEPTION WHEN OTHERS THEN 
             write_log('ERROR INSERT TAC_SCF_MONITOR '||SQLERRM,'LOG');
          END;
      
      
     ELSE 


       BEGIN
         SELECT   MEANING, DESCRIPTION
           INTO   V_BUYER_ID, V_BUYER_NUMBER
           FROM   FND_LOOKUP_VALUES_VL
          WHERE   LOOKUP_CODE = P_ORG_ID AND LOOKUP_TYPE = 'TAC_SCF_ORG_ID';
       EXCEPTION
         WHEN OTHERS
         THEN
             V_BUYER_ID := null;
             V_BUYER_NUMBER := null;
       END;
      
      

       BEGIN
         SELECT   NVL(MAX (FIELD_ID_MODIFIER) + 1,0)
           INTO   V_ID_MODIFIER
           FROM   TAC_SCF_INVOICE_TBL
         WHERE   RECORD_TYPE_CODE = 'FH' 
                 AND NVL(STATUS,'P') <> 'E'
                 AND REPLACE(BUYER_ID,' ','') = V_BUYER_ID
                 AND TO_CHAR (CREATION_DATE, 'YYYYMMDD') =
                     TO_CHAR (SYSDATE, 'YYYYMMDD');
       EXCEPTION
         WHEN OTHERS
         THEN
            V_ID_MODIFIER := 0;
       END;

      
       V_FILE_NAME :=
            'PO'
         || '_'
         || V_BUYER_ID
         || '_'
         || TO_CHAR (SYSDATE, 'YYYYMMDD')
         || '_'
         || V_ID_MODIFIER
         || '.txt';
         
         
       BEGIN
         SELECT   count(*)
           INTO   CNT_FILE
           FROM   TAC_SCF_INVOICE_TBL
         WHERE   NVL(STATUS,'P') <> 'E'
                 AND REPLACE(BUYER_ID,' ','') = V_BUYER_ID
                 AND TO_CHAR (CREATION_DATE, 'YYYYMMDD') =
                     TO_CHAR (SYSDATE, 'YYYYMMDD')
                 AND FILE_NAME = V_FILE_NAME;
       EXCEPTION
         WHEN OTHERS
         THEN
            CNT_FILE := 1;
       END; 
       
       
       IF CNT_FILE > 0 then
       
              e := e + 1;
       
            write_log ('Error ' , 'out');
            write_log (' ', 'out');
            write_log ('Error SCF001-007 : Duplicate File name. (It Operartion)', 'out');
            write_log (' ', 'out');
            write_log ('------------------------------------------------------------------------------------------------', 'out');
           
            INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error ',SYSDATE,'N');
            INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,' ',SYSDATE,'N');
            INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error SCF001-007 : Duplicate File name. (It Operartion)',SYSDATE,'N');
            INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,' ',SYSDATE,'N');
            INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'------------------------------------------------------------------------------------------------',SYSDATE,'N');
           
              BEGIN
                INSERT INTO TAC_SCF_MONITOR 
                VALUES(NVL(V_SCF_ORG_NAME,P_ORG_ID),
                9999,   
                '9999',
                '9999',
                V_INTEGRATION_NAME,
                SYSDATE,
                SYSDATE,
                SYSDATE,
                SYSDATE,
                'E',
                'SCF001-007',
                'Duplicate File name. (It Operartion)',
                V_CONC_REQ_ID);
            
                COMMIT;
              EXCEPTION WHEN OTHERS THEN 
                write_log('ERROR INSERT TAC_SCF_MONITOR '||SQLERRM,'LOG');
              END;
       
       
       
       END IF;
         
 

      /***  INSERT FILE HEADER RECORD ***/
        BEGIN
          fh := fh + 1;
         INSERT INTO TAC_SCF_INVOICE_TBL (RECORD_TYPE_CODE,
                                          BUYER_ID,
                                          BUYER_REF_NO,
                                          FILE_CREATE_DATE,
                                          FILE_CREATE_TIME,
                                          FIELD_ID_MODIFIER,
                                          DESTINATION_NAME,
                                          ORIGIN_NAME,
                                          REFERENCE_CODE,
                                          CONC_REQ_ID,
                                          CREATION_DATE,
                                          CREATED_BY,
                                          LAST_UPDATE_DATE,
                                          LAST_UPDATED_BY,
                                          LAST_UPDATE_LOGIN,
                                          FILE_NAME)
           VALUES   (P_FH_REC_TYPE,
                     RPAD(V_BUYER_ID,15,' '),
                     RPAD(V_BUYER_NUMBER,15,' '),
                     TO_CHAR (SYSDATE, 'YYMMDD'),
                     TO_CHAR (SYSDATE, 'HHMM'),
                     SUBSTR(V_ID_MODIFIER,1,1),
                     RPAD(P_DEST_NAME,20,' '),
                     RPAD(V_SCF_ORG_NAME,25,' '),
                     RPAD(V_CONC_REQ_ID,15,' '), --V_CONC_REQ_ID || '_' || V_FILE_NAME, too long
                     V_CONC_REQ_ID,
                     SYSDATE,
                     P_USER_ID,
                     SYSDATE,
                     P_USER_ID,
                     P_USER_ID,
                     V_FILE_NAME);

         COMMIT;
         
          datarow := P_FH_REC_TYPE||
                     RPAD(V_BUYER_ID,15,' ')||
                     RPAD(V_BUYER_NUMBER,15,' ')||
                     TO_CHAR (SYSDATE, 'YYMMDD')||
                     TO_CHAR (SYSDATE, 'HHMM')||
                     SUBSTR(V_ID_MODIFIER,1,1)||
                     RPAD(P_DEST_NAME,20,' ')||
                     RPAD(V_SCF_ORG_NAME,25,' ')||
                     RPAD(V_CONC_REQ_ID,15,' ')
                     ;
                     
            in_FILE := UTL_FILE.FOPEN(v_dir, v_file_name, 'A'); 
            UTL_FILE.PUT_LINE (in_FILE, datarow);
            UTL_FILE.FCLOSE(in_File);

        EXCEPTION
         WHEN OTHERS
         THEN
         
           fh := fh - 1;
         
            IF e = 0 then
              write_log ('Error ' , 'out');
              write_log (' ', 'out');
              
              INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error ',SYSDATE,'N');
              INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,' ',SYSDATE,'N');
              COMMIT;
            end if;
            e := e + 1;
              
              
            write_log ('Error SCF001-005 : Cannot Write File(ERP) (It Operartion) ', 'OUT');
            write_log (' ', 'out');
            
            INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error SCF001-005 : Cannot Write File(ERP) (It Operartion) ',SYSDATE,'N');
            INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,' ',SYSDATE,'N');
            COMMIT;
           
        END;

    
      
      
      FOR REC_B1 IN B1
      LOOP
      
         c    := 0;
         p    := 0;
         a    := 0;
      
        
         V_BATCH_NUMBER := V_BATCH_NUMBER + 1;
         V_CHR_BATCH_NUMBER := LPAD(V_BATCH_NUMBER,7,0);
         bh := bh + 1;

         V_SCF_SUPPLIER := NULL;
         V_SCF_ORG_ID := NULL;
  

         V_MSG_ERR := null;



         BEGIN
            SELECT   FLV.LOOKUP_CODE
              INTO   V_SCF_ORG_ID
              FROM   FND_LOOKUP_VALUES_VL FLV
             WHERE       P_ORG_ID = FLV.LOOKUP_CODE
                     AND FLV.LOOKUP_TYPE = 'TAC_SCF_ORG_ID'
                     AND FLV.ENABLED_FLAG = 'Y'
                     AND (FLV.END_DATE_ACTIVE IS NULL
                          OR FLV.END_DATE_ACTIVE > SYSDATE);
         EXCEPTION
            WHEN OTHERS
            THEN
               V_SCF_ORG_ID := NULL;

         END;


        


         IF V_SCF_ORG_ID IS NULL 
         THEN
            bh := bh - 1;
         
            IF e = 0 then
              write_log ('Error ' , 'out');
              write_log (' ', 'out');
              
              INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error ',SYSDATE,'N');
              INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,' ',SYSDATE,'N');
              COMMIT;
            end if;
              e := e + 1;
              
             
              
              IF V_SCF_ORG_ID IS NULL THEN
                write_log ('Org ID : ' || P_ORG_ID, 'out');
                write_log ('Error SCF001-006 : Not Found Org_id in Look up " TAC_SCF_ORG_ID" (It Operartion)', 'out');
                write_log (' ', 'out');
              
                INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Org ID : ' || P_ORG_ID,SYSDATE,'N');
                INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error SCF001-006 : Not Found Org_id in Look up " TAC_SCF_ORG_ID" (It Operartion) ',SYSDATE,'N');
                INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,' ',SYSDATE,'N');
                
                
               BEGIN
                INSERT INTO TAC_SCF_MONITOR 
                VALUES(NVL(V_SCF_ORG_NAME,P_ORG_ID),
                99999,
                '99999',
                '99999',
                V_INTEGRATION_NAME,
                SYSDATE,
                SYSDATE,
                SYSDATE,
                SYSDATE,
                'E',
                'SCF001-006',
                'Not Found Org_id in Look up " TAC_SCF_ORG_ID" (It Operartion)',
                V_CONC_REQ_ID);
            
                COMMIT;
                
               EXCEPTION WHEN OTHERS THEN 
                   write_log('ERROR INSERT TAC_SCF_MONITOR '||SQLERRM,'LOG');
               END;

              END IF;
             
         ELSE
            /***  INSERT BATCH HEADER RECORD ***/
            BEGIN
               INSERT INTO TAC_SCF_INVOICE_TBL (RECORD_TYPE_CODE,
                                                BUYER_CURRENCY_CODE,
                                                BUYER_DISC_DATA,
                                                SUPPLIER_REF_NO,
                                                SUPPLIER_NAME,
                                                BATCH_NUMBER,
                                                CONC_REQ_ID,
                                                CREATION_DATE,
                                                CREATED_BY,
                                                LAST_UPDATE_DATE,
                                                LAST_UPDATED_BY,
                                                LAST_UPDATE_LOGIN,
                                                FILE_NAME)
                 VALUES   ( P_BH_REC_TYPE,
                           RPAD(REC_B1.INVOICE_CURRENCY_CODE,25,' '),
                           RPAD(' ',20,' '),
                           RPAD(REC_B1.VENDOR_NUMBER,35,' '),
                           RPAD(REC_B1.VENDOR_NAME,35,' '),
                           V_CHR_BATCH_NUMBER,
                           V_CONC_REQ_ID,
                           SYSDATE,
                           P_USER_ID,
                           SYSDATE,
                           P_USER_ID,
                           P_USER_ID,
                           V_FILE_NAME  );

               COMMIT;
               
               
               datarow :=  P_BH_REC_TYPE||
                           RPAD(REC_B1.INVOICE_CURRENCY_CODE,25,' ')||
                           RPAD(' ',20,' ')||
                           RPAD(REC_B1.VENDOR_NUMBER,35,' ')||
                           RPAD(REC_B1.VENDOR_NAME,35,' ')||
                           V_CHR_BATCH_NUMBER
                           ;
                     
                in_FILE := UTL_FILE.FOPEN(v_dir, v_file_name, 'A'); 
                UTL_FILE.PUT_LINE (in_FILE, datarow);
                UTL_FILE.FCLOSE(in_File);
            EXCEPTION
               WHEN OTHERS
               THEN
                
                 bh := bh - 1;  
                IF e = 0 then
                    write_log ('Error ' , 'out');
                    write_log (' ', 'out');
              
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error ',SYSDATE,'N');
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,' ',SYSDATE,'N');
                    COMMIT;
                end if;
            
                    e := e + 1;
              
              
                    write_log ('Batch Number:'|| V_CHR_BATCH_NUMBER , 'OUT');
                    write_log ('Error SCF001-005 : Cannot Write File(ERP) (It Operartion) ', 'OUT');
                    write_log (' ', 'out');
                    
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Batch Number:'|| V_CHR_BATCH_NUMBER,SYSDATE,'N');
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error SCF001-005 : Cannot Write File(ERP) (It Operartion) ' ,SYSDATE,'N');
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,' ',SYSDATE,'N');
                    COMMIT;

            END;



            
            FOR REC_C1 IN C1 (REC_B1.VENDOR_NUMBER,REC_B1.INVOICE_CURRENCY_CODE)
            LOOP

               V_TAX_RATE :=0;
               
               BEGIN
                  SELECT   TAX_RATE
                    INTO   V_TAX_RATE
                    FROM   AP_AWT_TAX_RATES_ALL
                   WHERE   TAX_RATE_ID = REC_C1.AWT_GROUP_ID;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     V_TAX_RATE := 0;
               END;
               
               
                BEGIN
                   SELECT AMOUNT 
                   INTO V_PO_AMOUNT
                   FROM AP_INVOICE_DISTRIBUTIONS_ALL
                   WHERE LINE_TYPE_LOOKUP_CODE = 'ITEM'
                   AND INVOICE_ID=REC_C1.INVOICE_ID;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     V_PO_AMOUNT := 0;
               END;
               
               
               BEGIN
                  SELECT AMOUNT 
                   INTO V_TAX_AMOUNT
                   FROM AP_INVOICE_DISTRIBUTIONS_ALL
                   WHERE LINE_TYPE_LOOKUP_CODE = 'TAX'
                   AND INVOICE_ID=REC_C1.INVOICE_ID;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     V_TAX_AMOUNT := 0;
               END;

               V_PO_CM_AMOUNT := 0;
               V_REC_TYPE := NULL;
               V_ATT_DOC_ID := 0;

               IF REC_C1.INVOICE_TYPE = 'CREDIT'
               THEN
                  V_PO_CM_AMOUNT := --abs(NVL (REC_C1.AMOUNT, 0));
                  abs(NVL (REC_C1.AMOUNT, 0) - ( NVL (V_PO_AMOUNT, 0) * (NVL (V_TAX_RATE, 0)/100)));
                  V_REC_TYPE := P_CM_REC_TYPE;
                  V_PO_CM_AMT_TBL := --abs(NVL (REC_C1.AMOUNT, 0));
                  abs(NVL (REC_C1.AMOUNT, 0) - ( NVL (V_PO_AMOUNT, 0) * (NVL (V_TAX_RATE, 0)/100)));

                  BEGIN
                     SELECT   LPAD (COUNT (DISTINCT PARENT_INVOICE_ID), 3, 0)
                       INTO   V_ATT_DOC_ID
                       FROM   AP_INVOICE_DISTRIBUTIONS_ALL
                      WHERE   INVOICE_ID = REC_C1.INVOICE_ID;        --1210849
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        V_ATT_DOC_ID := LPAD (0, 3, 0);
                  END;
               ELSE
                  V_PO_CM_AMOUNT :=
                    NVL (REC_C1.AMOUNT, 0) - ( NVL (V_PO_AMOUNT, 0) * (NVL (V_TAX_RATE, 0)/100));
                  V_REC_TYPE := P_PO_REC_TYPE;
                  V_ATT_DOC_ID := LPAD (0, 3, 0);
                  
                  V_PO_CM_AMT_TBL :=
                     NVL (REC_C1.AMOUNT, 0) - ( NVL (V_PO_AMOUNT, 0) * (NVL (V_TAX_RATE, 0)/100));
               END IF;


               V_MATURITY_DATE := null;
               BEGIN
                  SELECT   TO_CHAR (DUE_DATE, 'YYMMDD')
                    INTO   V_MATURITY_DATE
                    FROM   AP_PAYMENT_SCHEDULES_ALL
                   WHERE   INVOICE_ID = REC_C1.INVOICE_ID;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     V_MATURITY_DATE := NULL;
               END;
               
               
               IF V_REC_TYPE = 'CM' THEN
                 c := c + 1;
                 cm := cm + 1;
               ELSE
                 p := p + 1;
                 po := po + 1;
               
               END IF;
               
                 --  write_log('amt :' || V_PO_CM_AMT_TBL ,'out');
               
               /***  INSERT PO DETAIL RECORD ***/
               BEGIN
                  INSERT INTO TAC_SCF_INVOICE_TBL (RECORD_TYPE_CODE,
                                                   BUYER_ID,
                                                   BUYER_REF_NO,
                                                   SUPPLIER_REF_NO,
                                                   SUPPLIER_NAME,
                                                   ORIGINAL_INVOICE_DATE,
                                                   PO_CM_AMOUNT,
                                                   EFFECTIVE_DATE,
                                                   DESCRIPTION,
                                                   PO_CM_REFERENCE,
                                                   BUYER_UNIQUE_DOC_ID,
                                                   BUYER_DUEDATE,
                                                   ATTACH_DOC_ID,
                                                   FILE_CREATE_DATE,
                                                   FILE_CREATE_TIME,
                                                   FIELD_ID_MODIFIER,
                                                   DESTINATION_NAME,
                                                   ORIGIN_NAME,
                                                   REFERENCE_CODE,
                                                   BUYER_CURRENCY_CODE,
                                                   BATCH_NUMBER,
                                                   INVOICE_LAST_UPDATE_DATE,
                                                   VOUCHER_NUMBER,
                                                   CONC_REQ_ID,
                                                   CREATION_DATE,
                                                   CREATED_BY,
                                                   LAST_UPDATE_DATE,
                                                   LAST_UPDATED_BY,
                                                   LAST_UPDATE_LOGIN,
                                                   FILE_NAME)
                    VALUES   (V_REC_TYPE,
                              RPAD(V_BUYER_ID,15,' '),
                              RPAD(V_BUYER_NUMBER,15,' '),
                              RPAD(REC_C1.VENDOR_NUMBER,35,' '),
                              RPAD(REC_C1.VENDOR_NAME,35,' '),
                              REC_C1.INVOICE_DATE,
                              V_PO_CM_AMT_TBL,
                              V_MATURITY_DATE,
                              RPAD(REC_C1.DESCRIPTION,50,' '),
                              RPAD(REC_C1.INVOICE_NUM,50,' '),
                              RPAD(REC_C1.INVOICE_ID,100,' '),
                              V_MATURITY_DATE,
                              LPAD(V_ATT_DOC_ID,3,0) ,
                              TO_CHAR (SYSDATE, 'YYMMDD'),
                              TO_CHAR (SYSDATE, 'HHMM'),
                              SUBSTR(V_ID_MODIFIER,1,1),
                              RPAD(P_DEST_NAME,20,' '),
                              RPAD(V_SCF_ORG_NAME,25,' '),
                              RPAD(V_CONC_REQ_ID,15,' '),
                              RPAD(REC_C1.INVOICE_CURRENCY_CODE,25,' '),
                              V_CHR_BATCH_NUMBER,
                              REC_C1.LAST_UPDATE_DATE,
                              REC_C1.VOUCHER_NUM,
                              V_CONC_REQ_ID,
                              SYSDATE,
                              P_USER_ID,
                              SYSDATE,
                              P_USER_ID,
                              P_USER_ID,
                              V_FILE_NAME);

                  COMMIT;
                  
                  
                  datarow :=  V_REC_TYPE||
                              RPAD(V_BUYER_ID,15,' ')||
                              RPAD(REC_C1.VENDOR_NUMBER,35,' ')||
                              REC_C1.INVOICE_DATE||
                              LPAD(REPLACE(TO_CHAR(V_PO_CM_AMOUNT,'FM999999999990.90'),'.',''),18,0)||
                              V_MATURITY_DATE||
                              RPAD(REC_C1.DESCRIPTION,50,' ')||
                              RPAD(REC_C1.INVOICE_NUM,50,' ')||
                              RPAD(REC_C1.INVOICE_ID,100,' ')||
                              V_MATURITY_DATE||
                              LPAD(V_ATT_DOC_ID,3,0) 
                           ;
                     
                in_FILE := UTL_FILE.FOPEN(v_dir, v_file_name, 'A'); 
                UTL_FILE.PUT_LINE (in_FILE, datarow);
                UTL_FILE.FCLOSE(in_File);
                
               BEGIN 
                INSERT INTO TAC_SCF_MONITOR 
                VALUES(NVL(V_SCF_ORG_NAME,P_ORG_ID),
                REC_C1.INVOICE_ID,
                REC_C1.INVOICE_NUM,
                REC_C1.VOUCHER_NUM,
                V_INTEGRATION_NAME,
                SYSDATE,
                SYSDATE,
                SYSDATE,
                SYSDATE,
                'S',
                '',
                '',
                V_CONC_REQ_ID);
               EXCEPTION WHEN OTHERS THEN
                   write_log('ERROR INSERT TAC_SCF_MONITOR '||SQLERRM,'LOG');
               END;
            
                COMMIT;
                
               EXCEPTION
                  WHEN OTHERS
                  THEN
                  
                      IF V_REC_TYPE = 'CM' THEN
                        c := c - 1;
                        cm := cm - 1;
                      ELSE
                        p := p - 1;
                        po := po - 1;
               
                      END IF;
                  
                      IF e = 0 then
                        write_log ('Error ' , 'out');
                        write_log (' ', 'out');
              
                        INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error ',SYSDATE,'N');
                        INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,' ',SYSDATE,'N');
                        COMMIT;
                      end if;
            
                    e := e + 1;
                    
              
                    write_log('Invoice Number ' || REC_C1.INVOICE_NUM,'out');
                    write_log('Error SCF001-005 : Cannot Write File(ERP) (It Operartion)  ','out');
                    write_log('','out');
                    
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Invoice Number ' || REC_C1.INVOICE_NUM,SYSDATE,'N');
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error SCF001-005 : Cannot Write File(ERP) (It Operartion)',SYSDATE,'N');
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'',SYSDATE,'N');
                    
                   BEGIN 
                    INSERT INTO TAC_SCF_MONITOR 
                    VALUES(NVL(V_SCF_ORG_NAME,P_ORG_ID),
                    REC_C1.INVOICE_ID,
                    REC_C1.INVOICE_NUM,
                    REC_C1.VOUCHER_NUM,
                    V_INTEGRATION_NAME,
                    SYSDATE,
                    SYSDATE,
                    SYSDATE,
                    SYSDATE,
                    'E',
                    'Error SCF001-005',
                    'Cannot Write File(ERP) (It Operartion)',
                    V_CONC_REQ_ID);
                    COMMIT;
                   EXCEPTION WHEN OTHERS THEN
                       write_log('ERROR INSERT TAC_SCF_MONITOR '||SQLERRM,'LOG');
                   END;
                    
                     
               END;



               

               FOR REC_A1 IN ATT (REC_C1.INVOICE_ID)
               LOOP
                  a := a + 1;
                  ath := ath+1;



                  /***  INSERT ATTACHMENT RECORD ***/
                  BEGIN
                     INSERT INTO TAC_SCF_INVOICE_TBL (RECORD_TYPE_CODE,
                                                      DOC_TYPE,
                                                      DOC_REF_NO,
                                                      APPLIED_DATE,
                                                      NOTE,
                                                      ATTACH_AMOUNT,
                                                      CONC_REQ_ID,
                                                      CREATION_DATE,
                                                      CREATED_BY,
                                                      LAST_UPDATE_DATE,
                                                      LAST_UPDATED_BY,
                                                      LAST_UPDATE_LOGIN,
                                                      FILE_NAME)
                       VALUES   (P_AT_REC_TYPE,
                                 'INV',
                                 RPAD(REC_A1.INVOICE_NUM,100,' '),
                                 REC_A1.APPLIED_DATE,
                                 RPAD(REC_A1.DESCRIPTION,250,' '),
                                 abs(nvl(REC_A1.AMOUNT,0)),
                                 V_CONC_REQ_ID,
                                 SYSDATE,
                                 P_USER_ID,
                                 SYSDATE,
                                 P_USER_ID,
                                 P_USER_ID,
                                 V_FILE_NAME);

                     COMMIT;
                     
                     datarow :=  P_AT_REC_TYPE||
                                 'INV'||
                                 RPAD(REC_A1.INVOICE_NUM,100,' ')||
                                 REC_A1.APPLIED_DATE||
                                 RPAD(REC_A1.DESCRIPTION,250,' ')||
                                 LPAD(REPLACE(TO_CHAR(REC_A1.AMOUNT * (-1),'FM999999999990.90'),'.',''),18,0);
                                 
                     in_FILE := UTL_FILE.FOPEN(v_dir, v_file_name, 'A'); 
                     UTL_FILE.PUT_LINE (in_FILE, datarow);
                     UTL_FILE.FCLOSE(in_File);
                     
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                       
                       a := a - 1;
                       ath := ath - 1; 
                       
                      IF e = 0 then
                        write_log ('Error ' , 'out');
                        write_log (' ', 'out');
              
                        INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error ',SYSDATE,'N');
                        INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,' ',SYSDATE,'N');
                        --COMMIT;
                      end if;
            
                    e := e + 1;
              
                    write_log('Attachment Document Reference Number ' || REC_A1.INVOICE_NUM,'out');
                    write_log('Error SCF001-005 : Cannot Write File(ERP) (It Operartion) ','out');
                    write_log('','out');
                    
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Attachment Document Reference Number ' || REC_A1.INVOICE_NUM,SYSDATE,'N');
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error SCF001-005 : Cannot Write File(ERP) (It Operartion) ',SYSDATE,'N');
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'',SYSDATE,'N');
                    COMMIT;
                  END;
               END LOOP;
            END LOOP;


           
           V_TOTAL_OB_AMT_BATCH := 0;
           
            
            BEGIN
               SELECT   NVL(SUM (PO_CM_AMOUNT),0)
                 INTO   V_TOTAL_OB_AMT_BATCH
                 FROM   TAC_SCF_INVOICE_TBL
                WHERE   BATCH_NUMBER = V_CHR_BATCH_NUMBER
                        AND FILE_NAME = V_FILE_NAME
                        AND TO_CHAR (CREATION_DATE, 'YYYYMMDD') =
                              TO_CHAR (SYSDATE, 'YYYYMMDD');
            EXCEPTION
               WHEN OTHERS
               THEN
                  V_TOTAL_OB_AMT_BATCH := 0;
            END;
            
            
            
            IF V_TOTAL_OB_AMT_BATCH < 0 THEN
               V_TOTAL_OB_AMT_BATCH := V_TOTAL_OB_AMT_BATCH * (-1);
            END IF;


            V_ENTITY_CNT := p + c + a;
            
            
           /***  INSERT BATCH CONTRAL RECORD ***/
            BEGIN
                bc := bc + 1;
               INSERT INTO TAC_SCF_INVOICE_TBL (RECORD_TYPE_CODE,
                                                ENTRY_DETAIL_COUNT,
                                                TOTAL_OBLIGATION_AMOUNT,
                                                SUPPLIER_REF_NO,
                                                BATCH_NUMBER,
                                                CONC_REQ_ID,
                                                CREATION_DATE,
                                                CREATED_BY,
                                                LAST_UPDATE_DATE,
                                                LAST_UPDATED_BY,
                                                LAST_UPDATE_LOGIN,
                                                FILE_NAME)
                 VALUES   (P_BC_REC_TYPE,
                           LPAD(V_ENTITY_CNT,6,0),
                           V_TOTAL_OB_AMT_BATCH,
                           RPAD(REC_B1.VENDOR_NUMBER,35,' '),
                           V_CHR_BATCH_NUMBER,
                           V_CONC_REQ_ID,
                           SYSDATE,
                           P_USER_ID,
                           SYSDATE,
                           P_USER_ID,
                           P_USER_ID,
                           V_FILE_NAME);

               COMMIT;
               
               datarow := P_BC_REC_TYPE||
                           LPAD(V_ENTITY_CNT,6,0)||
                           LPAD(REPLACE(TO_CHAR(V_TOTAL_OB_AMT_BATCH,'FM999999999990.90'),'.',''),22,0)||
                           RPAD(REC_B1.VENDOR_NUMBER,35,' ')||
                           V_CHR_BATCH_NUMBER
                           ;
                                 
                     in_FILE := UTL_FILE.FOPEN(v_dir, v_file_name, 'A'); 
                     UTL_FILE.PUT_LINE (in_FILE, datarow);
                     UTL_FILE.FCLOSE(in_File);
            EXCEPTION
               WHEN OTHERS
               THEN
                       bc := bc - 1;
                       
                      IF e = 0 then
                        write_log ('Error ' , 'out');
                        write_log (' ', 'out');
              
                        INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error ',SYSDATE,'N');
                        INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,' ',SYSDATE,'N');
                        --COMMIT;
                      end if;
            
                    e := e + 1;
              
                    write_log('Batch Number ' || V_CHR_BATCH_NUMBER,'out');
                    write_log('Error SCF001-005 : Cannot Write File(ERP) (It Operartion) ','out');
                    write_log('','out');
                    
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Batch Number ' || V_CHR_BATCH_NUMBER,SYSDATE,'N');
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error SCF001-005 : Cannot Write File(ERP) (It Operartion) ',SYSDATE,'N');
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'',SYSDATE,'N');
                    --COMMIT;
            END;
         END IF;
      END LOOP;  -- END LOOP MAIN BATCH
      
     

      IF bh = 0 and e = 0
      THEN
         
        
         write_log ('Error ' , 'out');
         write_log (' ', 'out');
         write_log ('Error SCF001-001: No data Found.', 'out');
         write_log ('------------------------------------------------------------------------------------------------', 'out');
         
        INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error ',SYSDATE,'N');
        INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,' ',SYSDATE,'N');
        INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error SCF001-001: No data Found.',SYSDATE,'N');
        INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'------------------------------------------------------------------------------------------------',SYSDATE,'N');
        commit;
        
         BEGIN
            INSERT INTO TAC_SCF_MONITOR 
            VALUES(NVL(V_SCF_ORG_NAME,P_ORG_ID),
            9999,   
            '9999',
            '9999',
            V_INTEGRATION_NAME,
            SYSDATE,
            SYSDATE,
            SYSDATE,
            SYSDATE,
            'E',
            'SCF001-001',
            'No data Found.',
            V_CONC_REQ_ID);
            
            COMMIT;
         EXCEPTION WHEN OTHERS THEN 
             write_log('ERROR INSERT TAC_SCF_MONITOR '||SQLERRM,'LOG');
         END;
      
      
        UTL_FILE.FREMOVE (v_dir, v_file_name);
        DELETE TAC_SCF_INVOICE_TBL WHERE FILE_NAME = V_FILE_NAME;
        commit;

      ELSE
         /*** INSERT FILE CONTRAL RECORD ***/
         BEGIN
            SELECT   NVL(SUM (TOTAL_OBLIGATION_AMOUNT),0)
              INTO   V_TOTAL_OB_AMT
              FROM   TAC_SCF_INVOICE_TBL
             WHERE   FILE_NAME = V_FILE_NAME
                     AND TO_CHAR (CREATION_DATE, 'YYYYMMDD') =
                           TO_CHAR (SYSDATE, 'YYYYMMDD');
         EXCEPTION
            WHEN OTHERS
            THEN
               V_TOTAL_OB_AMT := 0;
         END;
         
         
         IF V_TOTAL_OB_AMT < 0 THEN
           V_TOTAL_OB_AMT :=  V_TOTAL_OB_AMT * (-1);
         END IF;

         V_RECORD_CNT := fh + 1 + bh + bc + po + cm + ath;                          --FH+FC+BH+BC+(PO+CM+AT)

         BEGIN
            INSERT INTO TAC_SCF_INVOICE_TBL (RECORD_TYPE_CODE,
                                             BATCH_COUNT,
                                             RECORD_COUNT,
                                             TOTAL_OBLIGATION_AMOUNT,
                                             CONC_REQ_ID,
                                             CREATION_DATE,
                                             CREATED_BY,
                                             LAST_UPDATE_DATE,
                                             LAST_UPDATED_BY,
                                             LAST_UPDATE_LOGIN,
                                             FILE_NAME)
              VALUES   (P_FC_REC_TYPE,
                        LPAD(bh,6,0),
                        LPAD(V_RECORD_CNT,6,0),
                        V_TOTAL_OB_AMT,
                        V_CONC_REQ_ID,
                        SYSDATE,
                        P_USER_ID,
                        SYSDATE,
                        P_USER_ID,
                        P_USER_ID,
                        V_FILE_NAME);

            COMMIT;
            
             datarow := P_FC_REC_TYPE||
                        LPAD(bh,6,0)||
                        LPAD(V_RECORD_CNT,6,0)||
                        LPAD(REPLACE(TO_CHAR(V_TOTAL_OB_AMT,'FM999999999990.90'),'.',''),22,0);
                                 
                     in_FILE := UTL_FILE.FOPEN(v_dir, v_file_name, 'A'); 
                     UTL_FILE.PUT_LINE (in_FILE, datarow);
                     UTL_FILE.FCLOSE(in_File);
         EXCEPTION
            WHEN OTHERS
            THEN
            
               
                IF e = 0 then
                    write_log ('Error ' , 'out');
                    write_log (' ', 'out');
              
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error ',SYSDATE,'N');
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,' ',SYSDATE,'N');
                    --COMMIT;
                end if;
            
                    e := e + 1;
                         
                    write_log('Error SCF001-005 : Cannot Write File(ERP) (It Operartion)  ' ,'out');
                    write_log('','out');

                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error SCF001-005 : Cannot Write File(ERP) (It Operartion) ' ,SYSDATE,'N');
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'',SYSDATE,'N');
                    --COMMIT;
               
         END;
      END IF;
      
      
           V_TOTAL_PO_AMT := 0;
           V_TOTAL_CM_AMT := 0;
           
            
            BEGIN
               SELECT   NVL(SUM (PO_CM_AMOUNT),0)
                 INTO   V_TOTAL_PO_AMT
                 FROM   TAC_SCF_INVOICE_TBL
                WHERE   RECORD_TYPE_CODE = 'PO'
                        AND FILE_NAME = V_FILE_NAME
                        AND TO_CHAR (CREATION_DATE, 'YYYYMMDD') =
                              TO_CHAR (SYSDATE, 'YYYYMMDD');
            EXCEPTION
               WHEN OTHERS
               THEN
                  V_TOTAL_PO_AMT := 0;
            END;
            
             BEGIN
               SELECT   NVL(SUM (PO_CM_AMOUNT),0)
                 INTO   V_TOTAL_CM_AMT
                 FROM   TAC_SCF_INVOICE_TBL
                WHERE   RECORD_TYPE_CODE = 'CM'
                        AND FILE_NAME = V_FILE_NAME
                        AND TO_CHAR (CREATION_DATE, 'YYYYMMDD') =
                              TO_CHAR (SYSDATE, 'YYYYMMDD');
            EXCEPTION
               WHEN OTHERS
               THEN
                  V_TOTAL_CM_AMT := 0;
            END;
            
       FOR N IN INI
       LOOP
           
           
          
                IF init = 0 then
                    write_log ('Initiated not pass ' , 'out');
                    write_log (' ', 'out');
              
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Initiated not pass ',SYSDATE,'N');
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,' ',SYSDATE,'N');
                    --COMMIT;
                end if;
                    
                    write_log('Invoice ::  '|| N.INVOICE_NUM ,'out');
                    
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error SCF001-005 : Cannot Write File(ERP) (It Operartion) ' ,SYSDATE,'N');
                    
         
           init := init + 1;
       
       END LOOP;
            
       if e > 0 then
           write_log ('------------------------------------------------------------------------------------------------', 'out');
           
           INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'------------------------------------------------------------------------------------------------',SYSDATE,'N');
     
         UTL_FILE.FREMOVE (v_dir, v_file_name);
         
         UPDATE TAC_SCF_INVOICE_TBL 
         SET STATUS= 'E'
         WHERE FILE_NAME = V_FILE_NAME;
         COMMIT;
         
       end if;
       
     
      
     END IF; /* end if not fund path*/
     
      IF bh = 0 and e = 0
      THEN null;
      ELSE
       write_log ('File Name : ' || V_FILE_NAME , 'out');
      END IF;
      write_log ('Total Batch : ' || bh || ' Batch', 'out');
      write_log ('Total POs in File : ' || lpad(po,4,' ') ||' Records   Value : ' || LPAD(TO_CHAR(V_TOTAL_PO_AMT,'FM999,999,999,990.90'),20,' '),'out');
      write_log ('Total CMs in File : ' || lpad(cm,4,' ') ||' Records   Value : ' || LPAD(TO_CHAR(V_TOTAL_CM_AMT,'FM999,999,999,990.90'),20,' '),'out');
      write_log ('------------------------------------------------------------------------------------------------', 'out');
      write_log ('Concurrent End : ' || TO_CHAR (SYSDATE, 'DD Mon YYYY HH24.Mi'),'out');
      
      
      INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'File Name : ' || V_FILE_NAME,SYSDATE,'N');
      INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Total Batch : ' || bh || ' Batch',SYSDATE,'N');
      INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Total POs in File : ' || lpad(po,4,' ') ||' Records   Value : ' || LPAD(TO_CHAR(V_TOTAL_PO_AMT,'FM999,999,999,990.90'),20,' '),SYSDATE,'N');
      INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Total CMs in File : ' || lpad(cm,4,' ') ||' Records   Value : ' || LPAD(TO_CHAR(V_TOTAL_CM_AMT,'FM999,999,999,990.90'),20,' '),SYSDATE,'N');
      INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'------------------------------------------------------------------------------------------------',SYSDATE,'N');
      INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Concurrent End : ' || TO_CHAR (SYSDATE, 'DD Mon YYYY HH24.Mi'),SYSDATE,'N');
      commit;
      
      V_TRAN_END_DATE := SYSDATE;
      
      
      
      
      /*SFTP file to Prime*/
      V_LOAD_START_DATE := SYSDATE;
      V_LOAD_END_DATE := SYSDATE;
      
      
      
      
      UPDATE TAC_SCF_MONITOR
      SET TRAN_START_DATE = V_TRAN_START_DATE,
      TRAN_END_DATE = V_TRAN_END_DATE,
      LOAD_START_DATE = V_LOAD_START_DATE,
      LOAD_END_DATE = V_LOAD_END_DATE
      WHERE REQUEST_ID = V_CONC_REQ_ID;
      COMMIT;
      
             /** generate monitor text file**/
              MAIN_MONITOR  (errbuf         => V_ERRBUF,
                             retcode   => V_RETCODE,
                             P_FOLDER_NAME  => v_dir_monitor,
                             P_REQUEST_ID  => V_CONC_REQ_ID,
                             P_PROGRAM    => V_INTEGRATION_NAME);
      
      
      /** submit alert**/
      l_success :=
        fnd_request.submit_request('ALR', -- Application Short name of the Concurrent Program/or for Alerts it is ALR
        'ALECDC', -- hard coded Program Short Name for Alerts
        'TAC : Alert Supply Chain Finance Integration', -- Description of the Program.--- alert_name col from table alr_alerts  for your alert name'
        SYSDATE, -- Submitted date. Always give the SYSDATE.
        FALSE, ---); --, -- Always give the FLASE.
        200,--- application_id col from table alr_alerts  for 'your alert name'
        124023, --- alert_id col from table alr_alerts  for 'your alert name'
        'A');  
        
   EXCEPTION WHEN OTHERS THEN
   
   WRITE_LOG('Others Error : '|| SQLERRM,'Out');  
   INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Others Error : '|| SQLERRM,SYSDATE,'N');

      
   END;
   
    procedure get_dir_list( p_directory in varchar2 )
    as language java
    name 'DirList.getList( java.lang.String )';
 
   PROCEDURE MAIN_BRA_DOWNLOAD (errbuf              OUT VARCHAR2,
                             retcode               OUT NUMBER,
                             P_FILE_NAME         IN     VARCHAR2,
                             P_REPLACE           IN     VARCHAR2,
                             P_USER_ID           IN     NUMBER)
   IS
   
   
   CURSOR F1(P_FILENAME VARCHAR2)
   IS
   
       SELECT FILENAME
       FROM TAC_SCF_INT_IN
       WHERE UPPER(FILENAME) LIKE UPPER(NVL(P_FILENAME,FILENAME))||'%'
       AND FILENAME NOT IN ('LOADED','ERROR','TAC_SCF_INT_IN')
       --AND UPPER(FILENAME) like '%.TXT'
       ;
       
   CURSOR TMP
   IS
   SELECT DISTINCT COL,ERROR_CODE,ERROR_MESSAGE
   FROM TAC_SCF_MONITOR_TMP;
       
   V_INTEGRATION_NAME VARCHAR2(50) := 'SCF002';
   
   V_DIR            VARCHAR2 (5120) := 'TAC_SCF_INT_IN';
   V_DIR_LOADED     VARCHAR2 (5120) := 'TAC_SCF_INT_IN_LOADED';
   V_DIR_ERROR      VARCHAR2 (5120) := 'TAC_SCF_INT_IN_ERROR';
   v_dir_monitor    VARCHAR2 (5120) := 'TAC_SCF_MONITOR_IN';
   
   V_PATH          VARCHAR2 (5120);
   V_PATH_LOADED     VARCHAR2 (5120);
   V_PATH_ERROR      VARCHAR2 (5120);
   in_file        UTL_FILE.file_type;
   datarow        VARCHAR2 (32767);
   V_CONC_REQ_ID          NUMBER := fnd_global.conc_request_id;
   
   V_FILE_NAME    VARCHAR2 (1000);
   RF             TAC_SCF_BRA_TBL%ROWTYPE;
   RB             TAC_SCF_BRA_BATCH_TBL%ROWTYPE;
   DT             TAC_SCF_BRA_DETAIL_TBL%ROWTYPE;
   RA             TAC_SCF_BRA_ATTATCH_TBL%ROWTYPE;
   RC             TAC_SCF_BRA_BATCH_CT_TBL%ROWTYPE;
   FR             TAC_SCF_BRA_FILE_CT_TBL%ROWTYPE;
   
   REC_TYPE       VARCHAR2(2);
   V_MSG          VARCHAR2(2000);
   v_msg_tmp      VARCHAR2(2000);
   V_ERROR_CODE_TMP VARCHAR2(2000);
   V_ERROR_MSG_TMP   VARCHAR2(2000);
   V_FLAG         VARCHAR2(1);
   
   V_CNT_PO       NUMBER := 0;
   V_CNT_CM       NUMBER := 0;
   V_CNT_ATT      NUMBER := 0;
   V_CNT_FH       NUMBER := 0;
   V_CNT_BATCH    NUMBER := 0;
   V_CNT_BATCH_CT NUMBER := 0;
   V_CNT_FCT      NUMBER := 0;
   V_BATCH        NUMBER := 0;
   V_REC          NUMBER := 0;
   
   V_SUM_DT       NUMBER := 0;
   V_SUM_PO_AMT   NUMBER := 0;
   V_SUM_CM_AMT   NUMBER := 0;
   V_SUM_PO_ORI   NUMBER := 0;
   V_SUM_CM_ORI   NUMBER := 0;
   V_SUM_PO_TRADE   NUMBER := 0;
   V_SUM_CM_TRADE   NUMBER := 0;
   V_SUM_PAY_AMT  NUMBER := 0;
   
   V_CNT_FILE     NUMBER := 0;
   e              NUMBER := 0;
   
   V_TRAN_START_DATE    DATE ;
   V_TRAN_END_DATE      DATE ;
   V_LOAD_START_DATE  DATE ;
   V_LOAD_END_DATE    DATE ;   
   
   V_VOUCHER_NUM   VARCHAR(50);
   l_success number;
   v_errbuf   VARCHAR2(1000);
   v_retcode  NUMBER;
   
   V_BATCH_NUMBER VARCHAR2(250);
   FUNCTION GET_AMOUNT(P_AMOUNT VARCHAR2,P_COLUMN VARCHAR2) RETURN NUMBER
   IS
   
   AMT NUMBER;
   
   BEGIN
     IF NVL(P_AMOUNT,0) = 0 THEN
       AMT :=  0;
     ELSE 
       AMT := substr(P_AMOUNT,1,length(P_AMOUNT)-2)||'.'|| substr(P_AMOUNT,-2);
     END IF;
     
     RETURN AMT;
   EXCEPTION WHEN OTHERS THEN
       
       V_FLAG := 'E';
       --v_msg_tmp := P_AMOUNT || ': Error SCF001-005 : Amount Number Format Not Correct (IT Operation)';
       --if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
       --write_log(v_msg_tmp|| sqlerrm,'log');
       
       v_msg_tmp := '=> '||P_COLUMN||' : Error SCF002-011 : Amount Number Format Not Correct (IT Operation)';
       if v_msg is null 
       then v_msg:=v_msg_tmp; 
       else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); 
       end if;
       
       INSERT INTO TAC_SCF_MONITOR_TMP VALUES('SCF002-011','Amount Number Format Not Correct (IT Operation)',P_COLUMN);
       COMMIT;
       RETURN P_AMOUNT;
     
    END; 
     
   
   FUNCTION VALIDATE_DATE(P_DATA VARCHAR2,P_COLUMN VARCHAR2) RETURN DATE
    IS
   
    V_DATE DATE;
   
    BEGIN
      IF REPLACE(P_DATA,'000000',NULL) IS NULL THEN
         V_DATE := NULL;
      ELSE
         V_DATE := TO_DATE(P_DATA,'YYMMDD');
      END IF;
      

      RETURN V_DATE;
      
    EXCEPTION WHEN OTHERS THEN
       
       V_FLAG := 'E';
       v_msg_tmp := '=> '||P_COLUMN||' : Error SCF002-003 : Date Format not correct (IT Operation) ';
       if v_msg is null 
       then v_msg:=v_msg_tmp; 
       else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); 
       end if;
       
       INSERT INTO TAC_SCF_MONITOR_TMP VALUES('SCF002-003','Date Format not correct (IT Operation)',P_COLUMN);
       COMMIT;
       
       
       --write_log(P_DATA || v_msg_tmp|| sqlerrm,'log');
       
       RETURN NULL;
     
    END;
    
   FUNCTION VALIDATE_NUMBER(P_DATA VARCHAR2,P_COLUMN VARCHAR2) RETURN NUMBER
    IS
   
    V_NUMBER NUMBER;
   
    BEGIN
      V_NUMBER := TO_NUMBER(replace(P_DATA,' ',0));

      RETURN V_NUMBER;
      
    EXCEPTION WHEN OTHERS THEN
       
       V_FLAG := 'E';
       v_msg_tmp := '=> '||P_COLUMN || ' : Error SCF002-005 : Number Format Not Correct (IT Operation)';
       if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
       --write_log(P_DATA || v_msg_tmp|| sqlerrm,'log');
       
       INSERT INTO TAC_SCF_MONITOR_TMP VALUES('SCF002-005','Number Format not correct (IT Operation)',P_COLUMN);
       COMMIT;
       
       RETURN null;
    END;
    
    FUNCTION VALIDATE_INVOICE(P_DATA VARCHAR2,P_COLUMN VARCHAR2) RETURN VARCHAR2
    IS
   
    V_INVOICE VARCHAR2(100);
   
    BEGIN
      SELECT distinct BUYER_UNIQUE_DOC_ID
      INTO V_INVOICE
      FROM TAC_SCF_INVOICE_TBL
      WHERE BUYER_UNIQUE_DOC_ID = P_DATA;
      
      RETURN P_DATA;
      
    EXCEPTION WHEN OTHERS THEN
       
       V_FLAG := 'E';
       v_msg_tmp := '=> '||P_COLUMN || ' : Error SCF002-004 : Not found Invoice in ERP (User)';
       if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
       --write_log(P_DATA || v_msg_tmp || sqlerrm,'log');
       
        INSERT INTO TAC_SCF_MONITOR_TMP VALUES('SCF002-004','Not found Invoice in ERP (User)',P_COLUMN);
       COMMIT;
       RETURN P_DATA;
    END;
    
   
    
    FUNCTION VALIDATE_DOC_REF(P_DATA VARCHAR2,P_COLUMN VARCHAR2) RETURN NUMBER
    IS
   
    V_DOC VARCHAR2(100);
   
    BEGIN
      SELECT DOC_REF_NO
      INTO V_DOC
      FROM TAC_SCF_INVOICE_TBL
      WHERE TRIM(DOC_REF_NO) = TRIM(P_DATA);

      RETURN P_DATA;
      
    EXCEPTION WHEN OTHERS THEN
       
       V_FLAG := 'E';
       v_msg_tmp := '=> '||P_COLUMN || ' : Error SCF002-004 : Not found Attached Document Reference in ERP (User)';
       if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
       
       INSERT INTO TAC_SCF_MONITOR_TMP VALUES('SCF002-004','Not found Attached Document Reference in ERP (User)',P_COLUMN);
       COMMIT;
       --write_log(v_msg_tmp || sqlerrm,'log');
       RETURN P_DATA;
    END;
    
    
   
   --============================   MAIN ========================================--  
   
   BEGIN
   
        /*SFTP file to Prime*/
      V_LOAD_START_DATE := SYSDATE;
      V_LOAD_END_DATE := SYSDATE;
   
        
   
   
      delete from TAC_SCF_LOG_TBL where CREATION_DATE < SYSDATE-5;
      COMMIT;
      
      V_TRAN_START_DATE := SYSDATE;
      
      write_log (' ', 'out');
      write_log (' ', 'out');
      write_log ('Program : TAC : Interface Buyer Remittance Advice', 'out');
      write_log ('Parameter :', 'out');
      write_log ('    FileName = ' || P_FILE_NAME, 'out');
      write_log ('    Replace = ' || P_REPLACE,'out');
      write_log (' ', 'out');
      write_log ('Concurrent Start : ' || TO_CHAR (SYSDATE, 'DD Mon YYYY HH24.Mi'),'out');
     
         
      INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Program : TAC : Interface Buyer Remittance Advice',SYSDATE,'N');
      INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Parameter :',SYSDATE,'N');
      INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'    FileName = ' || P_FILE_NAME,SYSDATE,'N');
      INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'    Replace = ' || P_REPLACE,SYSDATE,'N');
      INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,' ',SYSDATE,'N');
      INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Concurrent Start : ' || TO_CHAR (SYSDATE, 'DD Mon YYYY HH24.Mi'),SYSDATE,'N');
      COMMIT;

    --V_PATH := NULL;
    BEGIN
        SELECT DIRECTORY_PATH 
        INTO V_PATH
        from all_directories 
        where directory_name =V_DIR;
    EXCEPTION WHEN OTHERS THEN
        V_PATH := NULL;
    END;
    
    
      
   IF V_PATH IS NULL 
     THEN
            IF e = 0 then
              write_log ('Error ' , 'out');
              write_log ('------------------------------------------------------------------------------------------------', 'out');
              
              INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error ',SYSDATE,'N');
              INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'------------------------------------------------------------------------------------------------',SYSDATE,'N');
           
              COMMIT;
              
            end if;
            
            e := e + 1;
            
            write_log ('Error SCF002-010 : Not found Folder (ERP)', 'out');
            write_log (' ', 'out');
            
           
            
            INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error SCF002-010 : Not found Folder (ERP)',SYSDATE,'N');
            INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,' ',SYSDATE,'N');
            
          BEGIN
            INSERT INTO TAC_SCF_MONITOR 
            VALUES('DTAC',
            9999,   
            '9999',
            '9999',
            V_INTEGRATION_NAME,
            SYSDATE,
            SYSDATE,
            SYSDATE,
            SYSDATE,
            'E',
            'SCF002-010',
            'Not found Folder (ERP)',
            V_CONC_REQ_ID);
            
            COMMIT;
          EXCEPTION WHEN OTHERS THEN 
             write_log('ERROR INSERT TAC_SCF_MONITOR '||SQLERRM,'LOG');
          END;
      
      
   ELSE 
    delete from dir_list;
    commit;
    get_dir_list( V_PATH );   
    
    
    DELETE FROM TAC_SCF_INT_IN;
    COMMIT;
    INSERT INTO TAC_SCF_INT_IN (SELECT * FROM DIR_LIST);
    COMMIT;

   FOR RECF1 IN F1(P_FILE_NAME)
   LOOP
   
      V_FILE_NAME := RECF1.FILENAME;
      e := 0;
     BEGIN
               write_log('','out');
               write_log ('====================================================================', 'out');
               write_log('File Name : '|| v_file_name,'out');
               
               INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'',SYSDATE,'N');
               INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'File Name : '|| v_file_name,SYSDATE,'N');
               INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,
               '====================================================================',SYSDATE,'N');
      
               
               /**CHECK DUPPLICATE FILE**/
             IF P_REPLACE = 'N' THEN
               
                  BEGIN
                    SELECT DIRECTORY_PATH 
                    INTO V_PATH_LOADED
                    from all_directories 
                    where directory_name =V_DIR_LOADED;
                  EXCEPTION WHEN OTHERS THEN
                    V_PATH_LOADED := NULL;
                  END;
      
  
                delete from dir_list;
                commit;
                get_dir_list( V_PATH_LOADED ); 
                
                
                DELETE FROM TAC_SCF_INT_IN_LOADED;
                COMMIT;
                INSERT INTO TAC_SCF_INT_IN_LOADED (SELECT * FROM DIR_LIST);
                COMMIT;  
               
               
                 BEGIN
                    SELECT COUNT(*) 
                    INTO V_CNT_FILE
                    FROM TAC_SCF_INT_IN_LOADED
                    WHERE FILENAME = V_FILE_NAME;
                 EXCEPTION WHEN OTHERS THEN
                    V_CNT_FILE := 0;
                 END;
               
               
                 IF V_CNT_FILE > 0 THEN
                  UTL_FILE.FCOPY (V_DIR_LOADED,V_FILE_NAME,V_DIR_ERROR,V_FILE_NAME);
                  UTL_FILE.FREMOVE (V_DIR,V_FILE_NAME);
                    
                  
                    IF E = 0 THEN
                       WRITE_LOG('Error Record','Out');
                       WRITE_LOG('----------------------------------------------------------------------------------------------------------------------------------------','Out');
                       
                       INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error Record',SYSDATE,'N');
                       INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,
                       '----------------------------------------------------------------------------------------------------------------------------------------',SYSDATE,'N');
                    END IF;
                    e := e+1;
                    
                    WRITE_LOG('SCF002-002 : File have already exist in ERP (IT Operation)','Out');
                    write_log ('', 'out');

                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'SCF002-002 : File have already exist in ERP (IT Operation)',SYSDATE,'N');
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'',SYSDATE,'N');
                    COMMIT;
                    
                     BEGIN
                        INSERT INTO TAC_SCF_MONITOR 
                        VALUES('DTAC',
                        9999,   
                        '9999',
                        '9999',
                        V_INTEGRATION_NAME,
                        SYSDATE,
                        SYSDATE,
                        SYSDATE,
                        SYSDATE,
                        'E',
                        'SCF002-002',
                        'File have already exist in ERP (IT Operation)',
                        V_CONC_REQ_ID);
            
                        COMMIT;
                    EXCEPTION WHEN OTHERS THEN 
                        write_log('ERROR INSERT TAC_SCF_MONITOR '||SQLERRM,'LOG');
                    END;
                    
                  
                  
                 END IF;
                 
                 
                 
             -- write_log('P_REPLACE ' || P_REPLACE,'log');
             -- write_log('V_CNT_FILE ' || V_CNT_FILE,'log');
             -- write_log('e ' || e,'log');
             END IF;
             
             IF (P_REPLACE = 'Y') OR 
             (P_REPLACE = 'N' AND e = 0) THEN
              
               
               write_log('open file','log');
               in_file := UTL_FILE.fopen (v_dir, V_FILE_NAME, 'R');
               datarow := NULL;


               LOOP
               
                  V_MSG    := null;
                  V_FLAG   :=  'S';
                  REC_TYPE := NULL;
                  
                  DELETE FROM TAC_SCF_MONITOR_TMP;
                  COMMIT;
   
   
                  UTL_FILE.get_line (in_file, datarow);
                 

                  IF datarow IS NULL
                  THEN
                     WRITE_LOG ('END FILE', 'Log');
                     EXIT;
                  END IF;
                  
                  REC_TYPE := SUBSTR(datarow,1,2);
                  
                 -- write_log('record type '|| REC_TYPE,'log');
                  
                 IF REC_TYPE = 'RF' THEN 
                  
                 
                  RF.RECORD_TYPE_CODE := SUBSTR(datarow,1,2);  
                  RF.BUYER_IDENTIFICATION := SUBSTR(datarow,3,15);
                  RF.BUYER_NAME := SUBSTR(datarow,18,25);
                  RF.FILE_CREATION_DATE := VALIDATE_DATE(SUBSTR(datarow,43,6),'FILE_CREATION_DATE');
                  RF.FILE_CREATION_TIME := SUBSTR(datarow,49,4);
                  RF.FIELD_ID_MODIFIER := VALIDATE_NUMBER(SUBSTR(datarow,53,1),'FIELD_ID_MODIFIER');
                  RF.FILLER := SUBSTR(datarow,55,326);
                  
                  IF V_MSG IS NOT NULL THEN
                    IF E = 0 THEN
                       WRITE_LOG('Error Record','Out');
                       WRITE_LOG('----------------------------------------------------------------------------------------------------------------------------------------','Out');
                       
                       INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error Record',SYSDATE,'N');
                       INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,
                       '----------------------------------------------------------------------------------------------------------------------------------------',SYSDATE,'N');
                    END IF;
                    e := e+1;
                    
                    WRITE_LOG('File Header Record','Out');
                    WRITE_LOG(V_MSG,'Out');
                    write_log ('', 'out');
                    
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'File Header Record',SYSDATE,'N');
                    
                     FOR REC_T IN TMP
                     LOOP
                         BEGIN
                           
                             INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,
                             '=> '|| REC_T.COL || ' : ' || REC_T.ERROR_CODE || ' : ' || REC_T.ERROR_MESSAGE,SYSDATE,'N');
            
                                    COMMIT;
                         EXCEPTION WHEN OTHERS THEN 
                                write_log('ERROR INSERT TAC_SCF_LOG_TBL '||SQLERRM,'LOG');
                         END;
                     
                     END LOOP;
                     
                    
                    
                    --INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,V_MSG,SYSDATE,'N');
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'',SYSDATE,'N');
                    
                    
                  END IF;
              
                     
                     BEGIN
                        INSERT INTO TAC_SCF_BRA_TBL (RECORD_TYPE_CODE,
                                                        BUYER_IDENTIFICATION,
                                                        BUYER_NAME,
                                                        FILE_CREATION_DATE,
                                                        FILE_CREATION_TIME,
                                                        FIELD_ID_MODIFIER,
                                                        FILLER,
                                                        CONC_REQ_ID,
                                                        CREATION_DATE,
                                                        CREATED_BY,
                                                        LAST_UPDATE_DATE,
                                                        LAST_UPDATED_BY,
                                                        LAST_UPDATE_LOGIN,
                                                        FILE_NAME,
                                                        FLAG,
                                                        ERR_MSG)
                          VALUES   (RF.RECORD_TYPE_CODE,
                                    RF.BUYER_IDENTIFICATION,
                                    RF.BUYER_NAME,
                                    RF.FILE_CREATION_DATE,
                                    RF.FILE_CREATION_TIME,
                                    RF.FIELD_ID_MODIFIER,
                                    RF.FILLER,
                                    V_CONC_REQ_ID,
                                    SYSDATE,
                                    P_USER_ID,
                                    SYSDATE,
                                    P_USER_ID,
                                    P_USER_ID,
                                    V_FILE_NAME,
                                    V_FLAG,
                                    V_MSG
                                    );
                                    
                       COMMIT;
                     EXCEPTION
                       
                        WHEN OTHERS
                        THEN
                           WRITE_LOG ('cannot insert TAC_SCF_BRA_TBL : ' || SQLERRM,' Log');
                     END;
                     
                     
                 ELSIF REC_TYPE = 'RB' THEN 
                  
                  V_BATCH_NUMBER := NULL;
                  RB.RECORD_TYPE_CODE := SUBSTR(datarow,1,2);
                  RB.PAYMENT_REFERNCE_NUM := SUBSTR(datarow,3,12);
                  RB.SUPPLIER_NUMBER := SUBSTR(datarow,13,35);
                  RB.SUPPLIER_NAME := SUBSTR(datarow,50,25);
                  RB.FI_NAME := SUBSTR(datarow,75,25);
                  RB.BATCH_NUMBER := SUBSTR(datarow,100,7);
                  RB.CURRENCY_CODE := SUBSTR(datarow,107,25);
                  RB.SPECIAL_USE_FIELD1 :=  SUBSTR(datarow,132,20);
                  RB.SPECIAL_USE_FIELD2 :=  SUBSTR(datarow,152,100);
                  RB.FILLER :=  SUBSTR(datarow,252,128);
                  V_BATCH_NUMBER := RB.BATCH_NUMBER;
                  
                  IF V_MSG IS NOT NULL THEN
                  
                    IF E = 0 THEN
                       WRITE_LOG('Error Record','Out');
                       WRITE_LOG('----------------------------------------------------------------------------------------------------------------------------------------','Out');
                       
                       INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error Record',SYSDATE,'N');
                       INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,
                       '----------------------------------------------------------------------------------------------------------------------------------------',SYSDATE,'N');
                    END IF;
                    e := e+1;
                    
                    WRITE_LOG('Batch Header Record','out');
                    WRITE_LOG(V_MSG,'out');
                    write_log ('', 'out');
                    
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Batch Header Record',SYSDATE,'N');
                    
                    FOR REC_T IN TMP
                     LOOP
                         BEGIN
                           
                             INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,
                             '=> '|| REC_T.COL || ' : ' || REC_T.ERROR_CODE || ' : ' || REC_T.ERROR_MESSAGE,SYSDATE,'N');
            
                                    COMMIT;
                         EXCEPTION WHEN OTHERS THEN 
                                write_log('ERROR INSERT TAC_SCF_LOG_TBL '||SQLERRM,'LOG');
                         END;
                     
                    END LOOP;
                    
                    --INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,V_MSG,SYSDATE,'N');
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'',SYSDATE,'N');
                  END IF;
                  
                  
                 

                     BEGIN
                        INSERT INTO TAC_SCF_BRA_BATCH_TBL (RECORD_TYPE_CODE,
                                                        PAYMENT_REFERNCE_NUM,
                                                        SUPPLIER_NUMBER,
                                                        SUPPLIER_NAME,
                                                        FI_NAME,
                                                        BATCH_NUMBER,
                                                        CURRENCY_CODE,
                                                        SPECIAL_USE_FIELD1,
                                                        SPECIAL_USE_FIELD2,
                                                        FILLER,
                                                        CONC_REQ_ID,
                                                        CREATION_DATE,
                                                        CREATED_BY,
                                                        LAST_UPDATE_DATE,
                                                        LAST_UPDATED_BY,
                                                        LAST_UPDATE_LOGIN,
                                                        FILE_NAME,
                                                        FLAG)
                          VALUES   (RB.RECORD_TYPE_CODE ,
                                    RB.PAYMENT_REFERNCE_NUM ,
                                    RB.SUPPLIER_NUMBER ,
                                    RB.SUPPLIER_NAME ,
                                    RB.FI_NAME ,
                                    RB.BATCH_NUMBER,
                                    RB.CURRENCY_CODE,
                                    RB.SPECIAL_USE_FIELD1,
                                    RB.SPECIAL_USE_FIELD2,
                                    RB.FILLER,
                                    V_CONC_REQ_ID,
                                    SYSDATE,
                                    P_USER_ID,
                                    SYSDATE,
                                    P_USER_ID,
                                    P_USER_ID,
                                    V_FILE_NAME,
                                    V_FLAG
                                    );
                                    
                       COMMIT;
                     EXCEPTION
                       
                        WHEN OTHERS
                        THEN
                           WRITE_LOG ('cannot insert TAC_SCF_BRA_BATCH_TBL : ' || SQLERRM,' Log');
                     END;
                     
                 ELSIF REC_TYPE = 'CM' THEN 
                  
                 
                 
                  DT.RECORD_TYPE_CODE := SUBSTR(datarow,1,2);
                  DT.INVOICE_NUMBER := SUBSTR(datarow,3,50);
                  DT.TRANSACTION_DATE := VALIDATE_DATE(SUBSTR(datarow,53,6),'TRANSACTION_DATE');
                  DT.ORIGINAL_INVOICE_DATE := VALIDATE_DATE(SUBSTR(datarow,59,6),'ORIGINAL_INVOICE_DATE');
                  DT.ORIGINAL_EFFECTIVE_DATE := VALIDATE_DATE(SUBSTR(datarow,65,6),'ORIGINAL_EFFECTIVE_DATE');
                  DT.ACTUAL_EFFECTIVE_DATE := VALIDATE_DATE(SUBSTR(datarow,71,6),'ACTUAL_EFFECTIVE_DATE');
                  DT.ORIGINAL_VALUE := GET_AMOUNT(VALIDATE_NUMBER(SUBSTR(datarow,77,18),'ORIGINAL_VALUE'),'ORIGINAL_VALUE');
                  DT.NET_AFTER_DEDUCT :=  GET_AMOUNT(VALIDATE_NUMBER(SUBSTR(datarow,95,18),'NET_AFTER_DEDUCT'),'NET_AFTER_DEDUCT');
                  DT.APPLY_AMT :=  GET_AMOUNT(VALIDATE_NUMBER(SUBSTR(datarow,113,18),'APPLY_AMT'),'APPLY_AMT');
                  DT.PAYMENT_VALUE :=  GET_AMOUNT(VALIDATE_NUMBER(SUBSTR(datarow,131,18),'PAYMENT_VALUE'),'PAYMENT_VALUE');
                  DT.INVOICE_ID :=  VALIDATE_INVOICE(SUBSTR(datarow,149,100),'INVOICE_ID');
                  DT.BUYER_FORCAST_DUEDATE :=  VALIDATE_DATE(SUBSTR(datarow,249,6),'BUYER_FORCAST_DUEDATE');
                  DT.FILLER :=  SUBSTR(datarow,255,100);
                  DT.BATCH_NUMBER := V_BATCH_NUMBER;
                  
                  
                  
                  BEGIN
                     
                         SELECT NVL(DOC_SEQUENCE_VALUE,NVL(VOUCHER_NUM,'-999'))
                         INTO V_VOUCHER_NUM
                         FROM AP_INVOICES_ALL
                         WHERE INVOICE_ID = DT.INVOICE_ID;
                     
                  EXCEPTION WHEN OTHERS THEN
                        V_VOUCHER_NUM := NULL;
                  END;

                  
                  IF V_MSG IS NOT NULL THEN
                  
                    IF E = 0 THEN
                       WRITE_LOG('Error Record','Out');
                       WRITE_LOG('----------------------------------------------------------------------------------------------------------------------------------------','Out');
                       
                       INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error Record',SYSDATE,'N');
                       INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,
                       '----------------------------------------------------------------------------------------------------------------------------------------',SYSDATE,'N');
                    END IF;
                    e := e+1;
                    
                    WRITE_LOG('Invoice Number = '|| DT.INVOICE_NUMBER ,'Out');
                    WRITE_LOG(V_MSG,'Out');
                    write_log ('', 'out');
                    
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Invoice Number = '|| DT.INVOICE_NUMBER ,SYSDATE,'N');
                    
                    FOR REC_T IN TMP
                     LOOP
                         BEGIN
                            INSERT INTO TAC_SCF_MONITOR 
                            VALUES('DTAC',
                                    NVL(REPLACE(DT.INVOICE_ID,' ',''),9999),   
                                    NVL(REPLACE(DT.INVOICE_NUMBER,' ',''),'9999'),
                                    NVL(V_VOUCHER_NUM,'9999'),
                                    V_INTEGRATION_NAME,
                                    SYSDATE,
                                    SYSDATE,
                                    SYSDATE,
                                    SYSDATE,
                                    'E',
                                    REC_T.ERROR_CODE,
                                    REC_T.ERROR_MESSAGE,
                                    V_CONC_REQ_ID);
                                    
                             INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,
                             '=> '|| REC_T.COL || ' : ' || REC_T.ERROR_CODE || ' : ' || REC_T.ERROR_MESSAGE,SYSDATE,'N');
            
                                    COMMIT;
                         EXCEPTION WHEN OTHERS THEN 
                                write_log('ERROR INSERT TAC_SCF_MONITOR '||SQLERRM,'LOG');
                         END;
                     
                     END LOOP;
                    
                    
                    --INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,V_MSG,SYSDATE,'N');
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'',SYSDATE,'N');
                    
                     
                     
                  ELSE
                  
                     BEGIN
                            INSERT INTO TAC_SCF_MONITOR 
                            VALUES('DTAC',
                                    NVL(REPLACE(DT.INVOICE_ID,' ',''),9999),   
                                    NVL(REPLACE(DT.INVOICE_NUMBER,' ',''),'9999'),
                                    NVL(V_VOUCHER_NUM,'9999'),
                                    V_INTEGRATION_NAME,
                                    SYSDATE,
                                    SYSDATE,
                                    SYSDATE,
                                    SYSDATE,
                                    'S',
                                    '',
                                    '',
                                    V_CONC_REQ_ID);
            
                                    COMMIT;
                     EXCEPTION WHEN OTHERS THEN 
                                write_log('ERROR INSERT TAC_SCF_MONITOR '||SQLERRM,'LOG');
                     END;
                       

                    
                    
                  END IF;
                  


                     BEGIN
                        INSERT INTO TAC_SCF_BRA_DETAIL_TBL (RECORD_TYPE_CODE,
                                                        INVOICE_NUMBER,
                                                        TRANSACTION_DATE,
                                                        ORIGINAL_INVOICE_DATE,
                                                        ORIGINAL_EFFECTIVE_DATE,
                                                        ACTUAL_EFFECTIVE_DATE,
                                                        ORIGINAL_VALUE,
                                                        NET_AFTER_DEDUCT,
                                                        APPLY_AMT,
                                                        PAYMENT_VALUE,
                                                        INVOICE_ID,
                                                        BUYER_FORCAST_DUEDATE,
                                                        FILLER,
                                                        CONC_REQ_ID,
                                                        CREATION_DATE,
                                                        CREATED_BY,
                                                        LAST_UPDATE_DATE,
                                                        LAST_UPDATED_BY,
                                                        LAST_UPDATE_LOGIN,
                                                        FILE_NAME,
                                                        FLAG,
                                                        ERR_MSG,
                                                        TRADE_VALUE,
                                                        BATCH_NUMBER)
                          VALUES   (DT.RECORD_TYPE_CODE,
                                    DT.INVOICE_NUMBER,
                                    DT.TRANSACTION_DATE,
                                    DT.ORIGINAL_INVOICE_DATE,
                                    DT.ORIGINAL_EFFECTIVE_DATE,
                                    DT.ACTUAL_EFFECTIVE_DATE,
                                    DT.ORIGINAL_VALUE * (-1),
                                    DT.NET_AFTER_DEDUCT,
                                    DT.APPLY_AMT,
                                    DT.PAYMENT_VALUE * (-1),
                                    DT.INVOICE_ID,
                                    DT.BUYER_FORCAST_DUEDATE,
                                    DT.FILLER,
                                    V_CONC_REQ_ID,
                                    SYSDATE,
                                    P_USER_ID,
                                    SYSDATE,
                                    P_USER_ID,
                                    P_USER_ID,
                                    V_FILE_NAME,
                                    V_FLAG,
                                    V_MSG,
                                    0,
                                    DT.BATCH_NUMBER
                                    );
                                    
                       COMMIT;
                     EXCEPTION
                       
                        WHEN OTHERS
                        THEN
                           WRITE_LOG ('cannot insert TAC_SCF_BRA_DETAIL_TBL : ' || SQLERRM,' Log');
                     END;
                     

                 
                 ELSIF REC_TYPE = 'PO' THEN 
                 
                 
                 
                  
                  DT.RECORD_TYPE_CODE := SUBSTR(datarow,1,2);
                  --WRITE_LOG ('1','log');
                  DT.INVOICE_NUMBER := SUBSTR(datarow,3,50);
                  --WRITE_LOG ('2','log');
                  DT.TRANSACTION_DATE := VALIDATE_DATE(SUBSTR(datarow,53,6),'TRANSACTION_DATE');
                  --WRITE_LOG ('3','log');
                  DT.PO_DATE_SETTLED := VALIDATE_DATE(SUBSTR(datarow,59,6),'PO_DATE_SETTLED');
                  --WRITE_LOG ('4','log');
                  DT.ORIGINAL_EFFECTIVE_DATE := VALIDATE_DATE(SUBSTR(datarow,65,6),'ORIGINAL_EFFECTIVE_DATE');
                 -- WRITE_LOG ('5','log');
                  DT.ORIGINAL_MATURITY_DATE := VALIDATE_DATE(SUBSTR(datarow,71,6),'ORIGINAL_MATURITY_DATE');
                  --WRITE_LOG ('6','log');
                  DT.MATURITY_DATE := VALIDATE_DATE(SUBSTR(datarow,77,6),'MATURITY_DATE');
                 -- WRITE_LOG ('7','log');
                  DT.ORIGINAL_VALUE := GET_AMOUNT(VALIDATE_NUMBER(SUBSTR(datarow,83,18),'ORIGINAL_VALUE'),'ORIGINAL_VALUE');
                  --WRITE_LOG ('8','log');
                  DT.NET_AFTER_DEDUCT := GET_AMOUNT(VALIDATE_NUMBER(SUBSTR(datarow,101,18),'NET_AFTER_DEDUCT'),'NET_AFTER_DEDUCT');
                 -- WRITE_LOG ('9','log');
                  DT.PAYMENT_VALUE :=  GET_AMOUNT(VALIDATE_NUMBER(SUBSTR(datarow,119,18),'PAYMENT_VALUE'),'PAYMENT_VALUE');
                 -- WRITE_LOG ('10','log');
                  DT.DISCOUNT_FEE :=  GET_AMOUNT(VALIDATE_NUMBER(SUBSTR(datarow,137,18),'DISCOUNT_FEE'),'DISCOUNT_FEE');
                 -- WRITE_LOG ('11','log');
                  DT.TRADE_VALUE :=  GET_AMOUNT(VALIDATE_NUMBER(SUBSTR(datarow,155,18),'TRADE_VALUE'),'TRADE_VALUE');
                 -- WRITE_LOG ('12','log');
                  DT.SERVICE_PROVIDER_FEE := GET_AMOUNT(VALIDATE_NUMBER(SUBSTR(datarow,173,18),'SERVICE_PROVIDER_FEE'),'SERVICE_PROVIDER_FEE');
                 -- WRITE_LOG ('13','log');
                  DT.COMMUNITY_FEE :=  GET_AMOUNT(VALIDATE_NUMBER(SUBSTR(datarow,191,18),'COMMUNITY_FEE'),'COMMUNITY_FEE');
                  --WRITE_LOG ('14','log');
                  DT.FI_MARGIN :=  GET_AMOUNT(VALIDATE_NUMBER(SUBSTR(datarow,209,18),'FI_MARGIN'),'FI_MARGIN');
                 --WRITE_LOG ('15','log');
                  DT.INVOICE_ID :=  VALIDATE_INVOICE(SUBSTR(datarow,227,100),'INVOICE_ID');
                  --WRITE_LOG ('16','log');
                  DT.BUYER_FORCAST_DUEDATE :=  VALIDATE_DATE(SUBSTR(datarow,327,6),'BUYER_FORCAST_DUEDATE');
                  --WRITE_LOG ('17','log');
                  DT.BUYER_DISCOUNT_FEE :=  GET_AMOUNT(VALIDATE_NUMBER(SUBSTR(datarow,333,18),'BUYER_DISCOUNT_FEE'),'BUYER_DISCOUNT_FEE');
                  --WRITE_LOG ('18','log');
                  DT.PO_CERTIFIED_VALUE :=  GET_AMOUNT(VALIDATE_NUMBER(SUBSTR(datarow,351,18),'PO_CERTIFIED_VALUE'),'PO_CERTIFIED_VALUE');
                  --WRITE_LOG ('19','log');
                  DT.FILLER :=  SUBSTR(datarow,369,100);
                  --WRITE_LOG ('20','log');
                  DT.BATCH_NUMBER := V_BATCH_NUMBER;
                     BEGIN
                     
                         SELECT NVL(DOC_SEQUENCE_VALUE,NVL(VOUCHER_NUM,'-999'))
                         INTO V_VOUCHER_NUM
                         FROM AP_INVOICES_ALL
                         WHERE INVOICE_ID = DT.INVOICE_ID;
                     
                     EXCEPTION WHEN OTHERS THEN
                        V_VOUCHER_NUM := NULL;
                     END;

                  
                  IF V_MSG IS NOT NULL THEN
                  
                    IF E = 0 THEN
                       WRITE_LOG('Error Record','Out');
                       WRITE_LOG('----------------------------------------------------------------------------------------------------------------------------------------','Out');
                       
                       INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error Record',SYSDATE,'N');
                       INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,
                       '----------------------------------------------------------------------------------------------------------------------------------------',SYSDATE,'N');
                    END IF;
                    e := e+1;
                    
                    WRITE_LOG('Invoice Number = '|| DT.INVOICE_NUMBER ,'Out');
                    WRITE_LOG(V_MSG,'Out');
                    write_log ('', 'out');
                    
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Invoice Number = '|| DT.INVOICE_NUMBER,SYSDATE,'N');
                    
                     FOR REC_T IN TMP
                     LOOP
                         BEGIN
                            INSERT INTO TAC_SCF_MONITOR 
                            VALUES('DTAC',
                                    NVL(REPLACE(DT.INVOICE_ID,' ',''),9999),   
                                    NVL(REPLACE(DT.INVOICE_NUMBER,' ',''),'9999'),
                                    NVL(V_VOUCHER_NUM,'9999'),
                                    V_INTEGRATION_NAME,
                                    SYSDATE,
                                    SYSDATE,
                                    SYSDATE,
                                    SYSDATE,
                                    'E',
                                    REC_T.ERROR_CODE,
                                    REC_T.COL || ' : ' ||REC_T.ERROR_MESSAGE,
                                    V_CONC_REQ_ID);
                                    
                             INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,
                             '=> '|| REC_T.COL || ' : ' || REC_T.ERROR_CODE || ' : ' || REC_T.ERROR_MESSAGE,SYSDATE,'N');
            
                                    COMMIT;
                         EXCEPTION WHEN OTHERS THEN 
                                write_log('ERROR INSERT TAC_SCF_MONITOR '||SQLERRM,'LOG');
                         END;
                     
                     END LOOP;
                     
                     
                    --INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,V_MSG,SYSDATE,'N');
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'',SYSDATE,'N');
                    
                    
                  ELSE
                  
                         BEGIN
                            INSERT INTO TAC_SCF_MONITOR 
                            VALUES('DTAC',
                                    NVL(REPLACE(DT.INVOICE_ID,' ',''),9999),   
                                    NVL(REPLACE(DT.INVOICE_NUMBER,' ',''),'9999'),
                                    NVL(V_VOUCHER_NUM,'9999'),
                                    V_INTEGRATION_NAME,
                                    SYSDATE,
                                    SYSDATE,
                                    SYSDATE,
                                    SYSDATE,
                                    'S',
                                    '',
                                    '',
                                    V_CONC_REQ_ID);
            
                                    COMMIT;
                         EXCEPTION WHEN OTHERS THEN 
                                write_log('ERROR INSERT TAC_SCF_MONITOR '||SQLERRM,'LOG');
                         END;
                  END IF;
                 

                     BEGIN
                        INSERT INTO TAC_SCF_BRA_DETAIL_TBL (RECORD_TYPE_CODE,
                                                        INVOICE_NUMBER,
                                                        TRANSACTION_DATE,
                                                        PO_DATE_SETTLED,
                                                        ORIGINAL_EFFECTIVE_DATE,
                                                        ORIGINAL_MATURITY_DATE,
                                                        MATURITY_DATE,
                                                        ORIGINAL_VALUE,
                                                        NET_AFTER_DEDUCT,
                                                        PAYMENT_VALUE,
                                                        DISCOUNT_FEE,
                                                        TRADE_VALUE,
                                                        SERVICE_PROVIDER_FEE,
                                                        COMMUNITY_FEE,
                                                        FI_MARGIN,
                                                        INVOICE_ID,
                                                        BUYER_FORCAST_DUEDATE,
                                                        BUYER_DISCOUNT_FEE,
                                                        PO_CERTIFIED_VALUE,
                                                        FILLER,
                                                        CONC_REQ_ID,
                                                        CREATION_DATE,
                                                        CREATED_BY,
                                                        LAST_UPDATE_DATE,
                                                        LAST_UPDATED_BY,
                                                        LAST_UPDATE_LOGIN,
                                                        FILE_NAME,
                                                        FLAG,
                                                        ERR_MSG,
                                                        BATCH_NUMBER)
                          VALUES   (DT.RECORD_TYPE_CODE,
                                    DT.INVOICE_NUMBER,
                                    DT.TRANSACTION_DATE,
                                    DT.PO_DATE_SETTLED,
                                    DT.ORIGINAL_EFFECTIVE_DATE,
                                    DT.ORIGINAL_MATURITY_DATE,
                                    DT.MATURITY_DATE,
                                    DT.ORIGINAL_VALUE,
                                    DT.NET_AFTER_DEDUCT,
                                    DT.PAYMENT_VALUE,
                                    DT.DISCOUNT_FEE,
                                    DT.TRADE_VALUE,
                                    DT.SERVICE_PROVIDER_FEE,
                                    DT.COMMUNITY_FEE,
                                    DT.FI_MARGIN,
                                    DT.INVOICE_ID,
                                    DT.BUYER_FORCAST_DUEDATE,
                                    DT.BUYER_DISCOUNT_FEE,
                                    DT.PO_CERTIFIED_VALUE,
                                    DT.FILLER,
                                    V_CONC_REQ_ID,
                                    SYSDATE,
                                    P_USER_ID,
                                    SYSDATE,
                                    P_USER_ID,
                                    P_USER_ID,
                                    V_FILE_NAME,
                                    V_FLAG,
                                    V_MSG,
                                    DT.BATCH_NUMBER
                                    );
                                    
                       COMMIT;
                     EXCEPTION
                       
                        WHEN OTHERS
                        THEN
                           WRITE_LOG ('cannot insert TAC_SCF_BRA_DETAIL_TBL : ' || SQLERRM,' Log');
                     END;
                     

                     
                     
                 ELSIF REC_TYPE = 'RA' THEN 
                  
                  RA.RECORD_TYPE := SUBSTR(datarow,1,2);
                  RA.DOCUMENT_TYPE := SUBSTR(datarow,3,3);
                  RA.DOC_REFERENCE := VALIDATE_DOC_REF(SUBSTR(datarow,6,100),'DOC_REFERENCE');
                  RA.ATTATCH_DOC_DATE := VALIDATE_DATE(SUBSTR(datarow,106,6),'ATTATCH_DOC_DATE');
                  RA.ATTATCH_DESCRIPTION := SUBSTR(datarow,112,250);
                  RA.ATTACH_AMOUNT := GET_AMOUNT(VALIDATE_NUMBER(SUBSTR(datarow,362,18),'ATTACH_AMOUNT'),'ATTACH_AMOUNT');
                  
                  IF V_MSG IS NOT NULL THEN
                  
                    IF E = 0 THEN
                       WRITE_LOG('Error Record','Out');
                       WRITE_LOG('----------------------------------------------------------------------------------------------------------------------------------------','Out');
                       
                       INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error Record',SYSDATE,'N');
                       INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,
                       '----------------------------------------------------------------------------------------------------------------------------------------',SYSDATE,'N');
                    END IF;
                    e := e+1;
                    
                    WRITE_LOG('Attached Document Number = '|| RA.DOC_REFERENCE ,'Out');
                    WRITE_LOG(V_MSG,'Out');
                    write_log ('', 'out');
                    
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Attached Document Number = '|| RA.DOC_REFERENCE ,SYSDATE,'N');
                    
                    FOR REC_T IN TMP
                     LOOP
                         BEGIN
                           
                             INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,
                             '=> '|| REC_T.COL || ' : ' || REC_T.ERROR_CODE || ' : ' || REC_T.ERROR_MESSAGE,SYSDATE,'N');
            
                                    COMMIT;
                         EXCEPTION WHEN OTHERS THEN 
                                write_log('ERROR INSERT TAC_SCF_LOG_TBL '||SQLERRM,'LOG');
                         END;
                     
                    END LOOP;
                    
                    --INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,V_MSG,SYSDATE,'N');
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'',SYSDATE,'N');
                  END IF;
                  
                     BEGIN
                        INSERT INTO TAC_SCF_BRA_ATTATCH_TBL (RECORD_TYPE,
                                                        DOCUMENT_TYPE,
                                                        DOC_REFERENCE,
                                                        ATTATCH_DOC_DATE,
                                                        ATTATCH_DESCRIPTION,
                                                        ATTACH_AMOUNT,
                                                        CONC_REQ_ID,
                                                        CREATION_DATE,
                                                        CREATED_BY,
                                                        LAST_UPDATE_DATE,
                                                        LAST_UPDATED_BY,
                                                        LAST_UPDATE_LOGIN,
                                                        FILE_NAME,
                                                        FLAG,
                                                        ERR_MSG)
                          VALUES   (RA.RECORD_TYPE,
                                    RA.DOCUMENT_TYPE,
                                    RA.DOC_REFERENCE,
                                    RA.ATTATCH_DOC_DATE,
                                    RA.ATTATCH_DESCRIPTION,
                                    RA.ATTACH_AMOUNT,
                                    V_CONC_REQ_ID,
                                    SYSDATE,
                                    P_USER_ID,
                                    SYSDATE,
                                    P_USER_ID,
                                    P_USER_ID,
                                    V_FILE_NAME,
                                    V_FLAG,
                                    V_MSG
                                    );
                                    
                       COMMIT;
                     EXCEPTION
                       
                        WHEN OTHERS
                        THEN
                           WRITE_LOG ('cannot insert TAC_SCF_BRA_ATTATCH_TBL : ' || SQLERRM,' Log');
                     END;  
                     
                     
                     
                     
                 ELSIF REC_TYPE = 'RC' THEN 
                  
                 
                  RC.RECORD_TYPE := SUBSTR(datarow,1,2);
                  RC.DETAIL_COUNT := VALIDATE_NUMBER(SUBSTR(datarow,3,6),'DETAIL_COUNT');
                  RC.PAYMENT_AMOUNT := GET_AMOUNT(VALIDATE_NUMBER(SUBSTR(datarow,9,22),'PAYMENT_AMOUNT'),'PAYMENT_AMOUNT');
                  RC.VENDOR_NUM := SUBSTR(datarow,31,35);
                  RC.BATCH_NUMBER := SUBSTR(datarow,66,7);
                  RC.FILLER := SUBSTR(datarow,73,307);
                  
                  IF V_MSG IS NOT NULL THEN
                  
                    IF E = 0 THEN
                       WRITE_LOG('Error Record','Out');
                       WRITE_LOG('----------------------------------------------------------------------------------------------------------------------------------------','Out');
                       
                       INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error Record',SYSDATE,'N');
                       INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,
                       '----------------------------------------------------------------------------------------------------------------------------------------',SYSDATE,'N');
                    END IF;
                    e := e+1;
                    
                    WRITE_LOG('Batch Control' ,'Out');
                    WRITE_LOG(V_MSG,'Out');
                    write_log ('', 'out');
                    
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Batch Control',SYSDATE,'N');
                    
                    FOR REC_T IN TMP
                     LOOP
                         BEGIN
                           
                             INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,
                             '=> '|| REC_T.COL || ' : ' || REC_T.ERROR_CODE || ' : ' || REC_T.ERROR_MESSAGE,SYSDATE,'N');
            
                                    COMMIT;
                         EXCEPTION WHEN OTHERS THEN 
                                write_log('ERROR INSERT TAC_SCF_LOG_TBL '||SQLERRM,'LOG');
                         END;
                     
                    END LOOP;
                    
                    --INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,V_MSG,SYSDATE,'N');
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'',SYSDATE,'N');
                    
                    
                  END IF;
               
                  
                 

                     BEGIN
                        INSERT INTO TAC_SCF_BRA_BATCH_CT_TBL (RECORD_TYPE,
                                                        DETAIL_COUNT,
                                                        PAYMENT_AMOUNT,
                                                        VENDOR_NUM,
                                                        BATCH_NUMBER,
                                                        FILLER,
                                                        CONC_REQ_ID,
                                                        CREATION_DATE,
                                                        CREATED_BY,
                                                        LAST_UPDATE_DATE,
                                                        LAST_UPDATED_BY,
                                                        LAST_UPDATE_LOGIN,
                                                        FILE_NAME,
                                                        FLAG,
                                                        ERR_MSG)
                          VALUES   (RC.RECORD_TYPE,
                                    RC.DETAIL_COUNT,
                                    RC.PAYMENT_AMOUNT,
                                    RC.VENDOR_NUM,
                                    RC.BATCH_NUMBER,
                                    RC.FILLER,
                                    V_CONC_REQ_ID,
                                    SYSDATE,
                                    P_USER_ID,
                                    SYSDATE,
                                    P_USER_ID,
                                    P_USER_ID,
                                    V_FILE_NAME,
                                    V_FLAG,
                                    V_MSG
                                    );
                                    
                       COMMIT;
                     EXCEPTION
                       
                        WHEN OTHERS
                        THEN
                           WRITE_LOG ('cannot insert TAC_SCF_BRA_BATCH_CT_TBL : ' || SQLERRM,' Log');
                     END;
                     
                 ELSIF REC_TYPE = 'FR' THEN 
                 
                
                  FR.RECORD_TYPE := SUBSTR(datarow,1,2);
                  FR.BATCH_COUNT := VALIDATE_NUMBER(SUBSTR(datarow,3,6),'BATCH_COUNT');
                  FR.RECORD_COUNT := VALIDATE_NUMBER(SUBSTR(datarow,9,6),'RECORD_COUNT');
                  FR.PAYMENT_AMOUNT := GET_AMOUNT(VALIDATE_NUMBER(SUBSTR(datarow,15,22),'PAYMENT_AMOUNT'),'PAYMENT_AMOUNT');
                  FR.FILLER := SUBSTR(datarow,37,307);
                  
                  IF V_MSG IS NOT NULL THEN
                  
                    IF E = 0 THEN
                       WRITE_LOG('Error Record','Out');
                       WRITE_LOG('----------------------------------------------------------------------------------------------------------------------------------------','Out');
                       
                       INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error Record',SYSDATE,'N');
                       INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,
                       '----------------------------------------------------------------------------------------------------------------------------------------',SYSDATE,'N');
                    END IF;
                    e := e+1;
                    
                    WRITE_LOG('File Control' ,'Out');
                    WRITE_LOG(V_MSG,'Out');
                    write_log ('', 'out');
                    
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'File Control',SYSDATE,'N');
                    
                    FOR REC_T IN TMP
                    LOOP
                         BEGIN
                           
                             INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,
                             '=> '|| REC_T.COL || ' : ' || REC_T.ERROR_CODE || ' : ' || REC_T.ERROR_MESSAGE,SYSDATE,'N');
            
                                    COMMIT;
                         EXCEPTION WHEN OTHERS THEN 
                                write_log('ERROR INSERT TAC_SCF_LOG_TBL '||SQLERRM,'LOG');
                         END;
                     
                    END LOOP;
                    --INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,V_MSG,SYSDATE,'N');
                    INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'',SYSDATE,'N');
                    
                    
                  END IF;
                
                 


                     BEGIN
                        INSERT INTO TAC_SCF_BRA_FILE_CT_TBL (RECORD_TYPE,
                                                        BATCH_COUNT,
                                                        RECORD_COUNT,
                                                        PAYMENT_AMOUNT,
                                                        FILLER,
                                                        CONC_REQ_ID,
                                                        CREATION_DATE,
                                                        CREATED_BY,
                                                        LAST_UPDATE_DATE,
                                                        LAST_UPDATED_BY,
                                                        LAST_UPDATE_LOGIN,
                                                        FILE_NAME,
                                                        FLAG,
                                                        ERR_MSG)
                          VALUES   (FR.RECORD_TYPE,
                                    FR.BATCH_COUNT,
                                    FR.RECORD_COUNT,
                                    FR.PAYMENT_AMOUNT,
                                    FR.FILLER,
                                    V_CONC_REQ_ID,
                                    SYSDATE,
                                    P_USER_ID,
                                    SYSDATE,
                                    P_USER_ID,
                                    P_USER_ID,
                                    V_FILE_NAME,
                                    V_FLAG,
                                    V_MSG
                                    );
                                    
                       COMMIT;
                     EXCEPTION
                       
                        WHEN OTHERS
                        THEN
                           WRITE_LOG ('cannot insert TAC_SCF_BRA_FILE_CT_TBL : ' || SQLERRM,' Log');
                     END;  
                     
                 END IF;  
                  
                  
               END LOOP;
               
                
               
             END IF;


               COMMIT;
               
               
             
             
             
               
     EXCEPTION 
               WHEN no_data_found
               THEN
                  WRITE_LOG ('end file', 'LOG');
                  /*** Move file to directory loaded*/
           UTL_FILE.FCOPY (V_DIR,V_FILE_NAME,V_DIR_LOADED,V_FILE_NAME);
           UTL_FILE.FREMOVE (V_DIR,V_FILE_NAME);
     
               WHEN OTHERS
               THEN
                  WRITE_LOG ('Others Error :' || sqlerrm, 'Out');
                  INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Others Error : '|| SQLERRM,SYSDATE,'N');
                  
                   /*** Move file to directory ERROR*/
           UTL_FILE.FCOPY (V_DIR,V_FILE_NAME,V_DIR_ERROR,V_FILE_NAME);
           UTL_FILE.FREMOVE (V_DIR,V_FILE_NAME);
     END;
     
   
     
     
   V_CNT_PO        := 0;
   V_CNT_CM        := 0;
   V_CNT_ATT       := 0; 
   V_CNT_FH        := 0;
   V_CNT_BATCH     := 0;
   V_CNT_BATCH_CT  := 0;
   V_CNT_FCT       := 0;
   V_BATCH         := 0;
   V_REC           := 0;
   
   V_SUM_DT        := 0;
   V_SUM_PO_AMT    := 0;
   V_SUM_CM_AMT    := 0;
   V_SUM_PO_ORI    := 0;
   V_SUM_CM_ORI    := 0;
   V_SUM_PAY_AMT   := 0;
     
     
    
     
     BEGIN
        SELECT COUNT(*)  
        INTO V_CNT_ATT
        FROM TAC_SCF_BRA_ATTATCH_TBL
        WHERE FILE_NAME = V_FILE_NAME
        AND CONC_REQ_ID=V_CONC_REQ_ID
        AND FLAG = 'S';
     EXCEPTION
               WHEN OTHERS
               THEN
              V_CNT_ATT := 0;
     END;
     
     BEGIN
        SELECT SUM(ORIGINAL_VALUE),SUM(PAYMENT_VALUE),SUM(TRADE_VALUE) ,COUNT(*)
        INTO V_SUM_PO_ORI,V_SUM_PO_AMT,V_SUM_PO_TRADE,V_CNT_PO
        FROM TAC_SCF_BRA_DETAIL_TBL
        WHERE FILE_NAME = V_FILE_NAME
        AND CONC_REQ_ID=V_CONC_REQ_ID
        AND RECORD_TYPE_CODE = 'PO'
        AND FLAG = 'S'
        GROUP BY RECORD_TYPE_CODE;
     EXCEPTION
               WHEN OTHERS
               THEN
              V_SUM_PO_AMT := 0;
              V_CNT_PO := 0;
     END;
     
     BEGIN
        SELECT SUM(ORIGINAL_VALUE),SUM(PAYMENT_VALUE),SUM(TRADE_VALUE) ,COUNT(*)
        INTO V_SUM_CM_ORI,V_SUM_CM_AMT,V_SUM_CM_TRADE,V_CNT_CM
        FROM TAC_SCF_BRA_DETAIL_TBL
        WHERE FILE_NAME = V_FILE_NAME
        AND CONC_REQ_ID=V_CONC_REQ_ID
        AND RECORD_TYPE_CODE = 'CM'
        AND FLAG = 'S'
        GROUP BY RECORD_TYPE_CODE;
     EXCEPTION
               WHEN OTHERS
               THEN
              V_SUM_CM_AMT := 0;
              V_CNT_CM := 0;
     END;
     
     BEGIN 
        SELECT NVL(SUM(DETAIL_COUNT),0) S_DT,NVL(SUM(PAYMENT_AMOUNT),0) S_AMT
        INTO V_SUM_DT,V_SUM_PAY_AMT
        FROM TAC_SCF_BRA_BATCH_CT_TBL
        WHERE FILE_NAME = V_FILE_NAME
        AND CONC_REQ_ID=V_CONC_REQ_ID
        AND FLAG = 'S';
     EXCEPTION
               WHEN OTHERS
               THEN
              V_SUM_DT := 0;
              V_SUM_PAY_AMT := 0;
     END;
     
     IF (V_CNT_PO+V_CNT_CM+V_CNT_ATT) != V_SUM_DT THEN
     
        write_log('Error SCF002-006 :  Entry Detail Count  Not Correct (User)','Out');
        INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error SCF002-006 :  Entry Detail Count  Not Correct (User)',SYSDATE,'N');
        
        
        BEGIN
            INSERT INTO TAC_SCF_MONITOR 
            VALUES('DTAC',
            9999,   
            '9999',
            '9999',
            V_INTEGRATION_NAME,
            SYSDATE,
            SYSDATE,
            SYSDATE,
            SYSDATE,
            'E',
            'SCF002-006',
            'Entry Detail Count  Not Correct (User)',
            V_CONC_REQ_ID);
            
            COMMIT;
        EXCEPTION WHEN OTHERS THEN 
             write_log('ERROR INSERT TAC_SCF_MONITOR '||SQLERRM,'LOG');
        END;
     END IF;
      
     
     
     
     
     
     IF (V_SUM_PO_AMT+V_SUM_CM_AMT)!= V_SUM_PAY_AMT THEN
     
        write_log('Error SCF002-007 :  Batch Total Payment Not Correct (User)','Out');
        INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error SCF002-007 :  Batch Total Payment Not Correct (User)',SYSDATE,'N');
        
        
        BEGIN
            INSERT INTO TAC_SCF_MONITOR 
            VALUES('DTAC',
            9999,   
            '9999',
            '9999',
            V_INTEGRATION_NAME,
            SYSDATE,
            SYSDATE,
            SYSDATE,
            SYSDATE,
            'E',
            'SCF002-007',
            'Batch Total Payment Not Correct (User)',
            V_CONC_REQ_ID);
            
            COMMIT;
        EXCEPTION WHEN OTHERS THEN 
            write_log('ERROR INSERT TAC_SCF_MONITOR '||SQLERRM,'LOG');
        END;
     END IF;
     
     
     BEGIN
        SELECT COUNT(*)  
        INTO V_CNT_BATCH
        FROM TAC_SCF_BRA_BATCH_TBL
        WHERE FILE_NAME = V_FILE_NAME
        AND CONC_REQ_ID=V_CONC_REQ_ID
        AND FLAG = 'S';
     EXCEPTION
               WHEN OTHERS
               THEN
              V_CNT_BATCH := 0;
     END;
     
     
     
     BEGIN
        SELECT BATCH_COUNT , RECORD_COUNT
        INTO V_BATCH, V_REC
        FROM TAC_SCF_BRA_FILE_CT_TBL
        WHERE FILE_NAME = V_FILE_NAME
        AND CONC_REQ_ID=V_CONC_REQ_ID
        AND FLAG = 'S';
     EXCEPTION
               WHEN OTHERS
               THEN
              V_BATCH := 0;
              V_REC := 0;
     END;
     
     BEGIN
        SELECT COUNT(*)  
        INTO V_CNT_BATCH_CT
        FROM TAC_SCF_BRA_BATCH_CT_TBL
        WHERE FILE_NAME = V_FILE_NAME
        AND CONC_REQ_ID=V_CONC_REQ_ID
        AND FLAG = 'S';
     EXCEPTION
               WHEN OTHERS
               THEN
              V_CNT_BATCH_CT := 0;
     END;
     
     BEGIN
        SELECT COUNT(*)  
        INTO V_CNT_FH
        FROM TAC_SCF_BRA_TBL
        WHERE FILE_NAME = V_FILE_NAME
        AND CONC_REQ_ID=V_CONC_REQ_ID
        AND FLAG = 'S';
     EXCEPTION
               WHEN OTHERS
               THEN
              V_CNT_FH := 0;
     END;
     
     BEGIN
        SELECT COUNT(*)  
        INTO V_CNT_FCT
        FROM TAC_SCF_BRA_FILE_CT_TBL
        WHERE FILE_NAME = V_FILE_NAME
        AND CONC_REQ_ID=V_CONC_REQ_ID
        AND FLAG = 'S';
     EXCEPTION
               WHEN OTHERS
               THEN
              V_CNT_FCT := 0;
     END;
     
     
     
     IF V_CNT_BATCH != V_BATCH THEN
      write_log('Error SCF002-008 :  Batch Count Not Correct (User)','Out');
      INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error SCF002-008 :  Batch Count Not Correct (User)',SYSDATE,'N');
       
      BEGIN
            INSERT INTO TAC_SCF_MONITOR 
            VALUES('DTAC',
            9999,   
            '9999',
            '9999',
            V_INTEGRATION_NAME,
            SYSDATE,
            SYSDATE,
            SYSDATE,
            SYSDATE,
            'E',
            'SCF002-008',
            'Batch Count Not Correct (User)',
            V_CONC_REQ_ID);
            
            COMMIT;
      EXCEPTION WHEN OTHERS THEN 
             write_log('ERROR INSERT TAC_SCF_MONITOR '||SQLERRM,'LOG');
      END;
     END IF;
     
     IF ( V_CNT_FH+V_CNT_PO+V_CNT_CM+V_CNT_ATT+V_CNT_BATCH+V_CNT_BATCH_CT+V_CNT_FCT ) != V_REC  THEN
      write_log('Error SCF002-009 :  Record Count Not Correct (User)','Out');
      INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Error SCF002-009 :  Record Count Not Correct (User)',SYSDATE,'N');
      
      BEGIN
            INSERT INTO TAC_SCF_MONITOR 
            VALUES('DTAC',
            9999,   
            '9999',
            '9999',
            V_INTEGRATION_NAME,
            SYSDATE,
            SYSDATE,
            SYSDATE,
            SYSDATE,
            'E',
            'SCF002-009',
            'Record Count Not Correct (User)',
            V_CONC_REQ_ID);
            
            COMMIT;
          EXCEPTION WHEN OTHERS THEN 
             write_log('ERROR INSERT TAC_SCF_MONITOR '||SQLERRM,'LOG');
          END;
     END IF;
     
      
      WRITE_LOG('----------------------------------------------------------------------------------------------------------------------------------------','Out');  
      write_log ('Total Batch : ' || V_CNT_BATCH || ' Batch', 'out');
      write_log ('Total POs in File : ' || lpad(V_CNT_PO,4,' ') ||' Records   Value(Original) : ' || LPAD(TO_CHAR(V_SUM_PO_ORI,'FM999,999,999,990.90'),20,' ')
      ||'  ,Payment Value : ' || LPAD(TO_CHAR(V_SUM_PO_AMT,'FM999,999,999,990.90'),20,' ')
      ||'  ,Trade Value : ' || LPAD(TO_CHAR(V_SUM_PO_TRADE,'FM999,999,999,990.90'),20,' '),'out');
      write_log ('Total CMs in File : ' || lpad(V_CNT_CM,4,' ') ||' Records   Value(Original) : ' || LPAD(TO_CHAR(V_SUM_CM_ORI,'FM999,999,999,990.90'),20,' ')
      ||'  ,Payment Value : ' || LPAD(TO_CHAR(V_SUM_CM_AMT,'FM999,999,999,990.90'),20,' ')
      ||'  ,Trade Value : ' || LPAD(TO_CHAR(V_SUM_CM_TRADE,'FM999,999,999,990.90'),20,' '),'out');
      
      
      INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'----------------------------------------------------------------------------------------------------------------------------------------',SYSDATE,'N');
      INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Total Batch : ' || V_CNT_BATCH || ' Batch',SYSDATE,'N');
      INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Total POs in File : ' || lpad(V_CNT_PO,4,' ') ||' Records   Value(Original) : ' || LPAD(TO_CHAR(V_SUM_PO_ORI,'FM999,999,999,990.90'),20,' ')
      ||'  ,Payment Value : ' || LPAD(TO_CHAR(V_SUM_PO_AMT,'FM999,999,999,990.90'),20,' ')
      ||'  ,Trade Value : ' || LPAD(TO_CHAR(V_SUM_PO_TRADE,'FM999,999,999,990.90'),20,' '),SYSDATE,'N');
      INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Total CMs in File : ' || lpad(V_CNT_CM,4,' ') ||' Records   Value(Original) : ' || LPAD(TO_CHAR(V_SUM_CM_ORI,'FM999,999,999,990.90'),20,' ')
      ||'  ,Payment Value : ' || LPAD(TO_CHAR(V_SUM_CM_AMT,'FM999,999,999,990.90'),20,' ')
      ||'  ,Trade Value : ' || LPAD(TO_CHAR(V_SUM_CM_TRADE,'FM999,999,999,990.90'),20,' '),SYSDATE,'N');
      commit;
     
     
   END LOOP;  --=====  end loop folder
   
  END IF; --=== END NOT FOUND PATH
  
   write_log ('====================================================================', 'out');
   write_log ('Concurrent End : ' || TO_CHAR (SYSDATE, 'DD Mon YYYY HH24.Mi'),'out');
      
   
   INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,
   '====================================================================',SYSDATE,'N');
   INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Concurrent End : ' || TO_CHAR (SYSDATE, 'DD Mon YYYY HH24.Mi'),SYSDATE,'N');
   COMMIT; 
   
   
   
      V_TRAN_END_DATE := SYSDATE;
      
      
      UPDATE TAC_SCF_MONITOR
      SET TRAN_START_DATE = V_TRAN_START_DATE,
      TRAN_END_DATE = V_TRAN_END_DATE,
      LOAD_START_DATE = V_LOAD_START_DATE,
      LOAD_END_DATE = V_LOAD_END_DATE
      WHERE REQUEST_ID = V_CONC_REQ_ID;
      COMMIT;
      
      
         /** generate monitor text file**/
         
        BEGIN
         MAIN_MONITOR  (errbuf         => V_ERRBUF,
                             retcode   => V_RETCODE,
                             P_FOLDER_NAME  => v_dir_monitor,
                             P_REQUEST_ID  => V_CONC_REQ_ID,
                             P_PROGRAM    => V_INTEGRATION_NAME);
        EXCEPTION WHEN OTHERS THEN
           NULL;
        END;
      

      /** submit Alert**/ 
      l_success :=
        fnd_request.submit_request('ALR', -- Application Short name of the Concurrent Program/or for Alerts it is ALR
        'ALECDC', -- hard coded Program Short Name for Alerts
        'TAC : Alert Supply Chain Finance Integration', -- Description of the Program.--- alert_name col from table alr_alerts  for your alert name'
        SYSDATE, -- Submitted date. Always give the SYSDATE.
        FALSE, ---); --, -- Always give the FLASE.
        200,--- application_id col from table alr_alerts  for 'your alert name'
        124023, --- alert_id col from table alr_alerts  for 'your alert name'
        'A');  
     
   EXCEPTION WHEN OTHERS THEN
   
   WRITE_LOG('Others Error : '|| SQLERRM,'Out');  
   INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Others Error : '|| SQLERRM,SYSDATE,'N');

   END;
   
   
   PROCEDURE MAIN_MONITOR  (errbuf              OUT VARCHAR2,
                             retcode               OUT NUMBER,
                             P_FOLDER_NAME         IN     VARCHAR2,
                             P_REQUEST_ID          IN     NUMBER,
                             P_PROGRAM             IN     VARCHAR2)
                             IS
                             
                             
   CURSOR M1 IS
      SELECT distinct COMPANY,
      INVOICE_ID,
      INVOICE_NUM,
      VOUCHER_NUM,
      INTEGRATION_NAME,
      TRAN_START_DATE,
      TRAN_END_DATE,
      LOAD_START_DATE,
      LOAD_END_DATE,
      LOAD_STATUS,
      ERROR_CODE,
      ERROR_MESSAGE,
      REQUEST_ID
      FROM TAC_SCF_MONITOR
      WHERE REQUEST_ID = P_REQUEST_ID
      AND INTEGRATION_NAME = P_PROGRAM
      --ORDER BY INVOICE_NUM
      ;
      
      
      
   in_file        UTL_FILE.file_type;
   datarow        VARCHAR2 (32767);
   V_FILE_NAME_MONITOR    VARCHAR2 (1000);
   
   BEGIN
     IF P_PROGRAM='SCF001' THEN
      V_FILE_NAME_MONITOR :=
            'SCF_POOUTBOUND'
         || '_'
         || TO_CHAR (SYSDATE, 'DDMMYYYY')
         || '_'
         || TO_CHAR (SYSDATE, 'HH24:MI:SS')
         || '.txt';
     ELSE
       V_FILE_NAME_MONITOR :=
            'SCF_POINBOUND'
         || '_'
         || TO_CHAR (SYSDATE, 'DDMMYYYY')
         || '_'
         || TO_CHAR (SYSDATE, 'HH24:MI:SS')
         || '.txt';
     
     
     END IF;
   
      FOR REC_M1 IN M1
      LOOP
      
        datarow :=  REC_M1.COMPANY||'|'||
            REC_M1.INVOICE_ID||'|'||
            REC_M1.INVOICE_NUM||'|'||
            REC_M1.VOUCHER_NUM||'|'||
            REC_M1.INTEGRATION_NAME||'|'||
            TO_CHAR(REC_M1.TRAN_START_DATE,'ddmmyyyy,hh24:mi:ss') ||'|'||
            TO_CHAR(REC_M1.TRAN_END_DATE,'ddmmyyyy,hh24:mi:ss')||'|'||
            TO_CHAR(REC_M1.LOAD_START_DATE,'ddmmyyyy,hh24:mi:ss')||'|'||
            TO_CHAR(REC_M1.LOAD_END_DATE,'ddmmyyyy,hh24:mi:ss')||'|'||
            REC_M1.LOAD_STATUS||'|'||
            REC_M1.ERROR_CODE||'|'||
            REC_M1.ERROR_MESSAGE||'|'||
            REC_M1.REQUEST_ID;
      
      in_FILE := UTL_FILE.FOPEN(P_FOLDER_NAME, V_FILE_NAME_MONITOR, 'A'); 
      UTL_FILE.PUT_LINE (in_FILE, datarow);
      UTL_FILE.FCLOSE(in_File);
      
      END LOOP; 
   END;
   
   PROCEDURE MAIN_CLEAR_MONITOR  (errbuf              OUT VARCHAR2,
                             retcode               OUT NUMBER,
                             P_FOLDER_NAME         IN     VARCHAR2,
                             P_DAY          IN     NUMBER)
    IS
    
    CURSOR C1 IS
    SELECT FILENAME 
    FROM dir_list
    WHERE TO_DATE(SUBSTR(FILENAME,INSTR(FILENAME,'_',1,2)+1,8),'DDMMYYYY') <= SYSDATE - P_DAY
    --AND ROWNUM =1
    ;
    
     --v_dir_monitor    VARCHAR2 (5120) := 'TAC_SCF_MONITOR_IN';
     V_PATH  VARCHAR2(250);
    
    BEGIN
    
        BEGIN
          SELECT DIRECTORY_PATH 
          INTO V_PATH
          from all_directories 
          where directory_name =P_FOLDER_NAME;
        EXCEPTION WHEN OTHERS THEN
          V_PATH := NULL;
        END;
    
        
        delete from dir_list;
        commit;
        get_dir_list(V_PATH); 
        
        FOR REC1 IN C1
        LOOP
        
          UTL_FILE.FREMOVE (P_FOLDER_NAME,REC1.FILENAME);
        
        END LOOP;
    END;
END XCUST_DTSCF_INF_PKG;
/
