create or replace PACKAGE BODY TAC_GL_ANAPLAN_OUTB_ITF_PKG IS
/*Change History
 Date      	Author       	Version 	Description
 --------- 	-------------	--------- 	------------------------------------------------
 27-Aug-18 	TM.    			1.00 		Created
 26-Sep-18	TM.				1.01		Fix amount field
 27-Sep-18	TM.				1.02		Prevent duplicate lookup value in TAC_GL_ACCOUNT_OPEX lookup type
*/
ABNORMAL_END      		EXCEPTION;
/*g_org_id   				NUMBER := FND_PROFILE.VALUE('ORG_ID');
g_log_in   				NUMBER := FND_GLOBAL.CONC_LOGIN_ID;
g_user_id   			NUMBER := FND_GLOBAL.USER_ID;*/
g_request_id  			NUMBER := FND_GLOBAL.CONC_REQUEST_ID;
g_no_error				VARCHAR2(100)	:=	'No Error';
DELIMITER_DATA		CONSTANT VARCHAR2(10)	:=	',';
DELIMITER_CTRL		CONSTANT VARCHAR2(10)	:=	'|';
DIRECTORY_PREFIX	CONSTANT VARCHAR2(20)	:=	'GL_ANAPLAN';

C_DATE_FORMAT		CONSTANT VARCHAR2(20)	:=	'DD/MM/YYYY';

TYPE varchar_by_varchar_tabtyp IS TABLE OF VARCHAR2(1000) INDEX BY VARCHAR2(100);
TYPE nested_varchar_rectyp IS RECORD
(err_code		VARCHAR2(100)
,err_desc		VARCHAR2(1000)
,err_detail		varchar2_tabtyp);
TYPE nested_varchar_tabtyp IS TABLE OF nested_varchar_rectyp INDEX BY BINARY_INTEGER;
CT_ERROR			varchar_by_varchar_tabtyp;

C_DFLT_DATE_START	CONSTANT DATE	:=	TO_DATE('01/01/0001','dd/mm/yyyy');
C_DFLT_DATE_END		CONSTANT DATE	:=	TO_DATE('31/12/9999','dd/mm/yyyy');

---------------------------------------------------------------------------
PROCEDURE conc_wait
(p_conc_req_id	IN	NUMBER)
IS
	phase		VARCHAR2(30);
	status		VARCHAR2(30);
	dev_phase	VARCHAR2(30);
	dev_status	VARCHAR2(30);
	message		VARCHAR2(1000);
BEGIN
	COMMIT;
	LOOP
		IF fnd_concurrent.wait_for_request
			(p_conc_req_id	,10			,0
			,phase			,status		,dev_phase
			,dev_status		,message) THEN
			NULL;
		END IF;
		EXIT WHEN phase <> 'Running';
		dbms_lock.sleep(2);
	END LOOP;
END conc_wait;

PROCEDURE conc_wait
(p_conc_req_id	IN			NUMBER
,p_phase		OUT NOCOPY	VARCHAR2
,p_status		OUT NOCOPY	VARCHAR2
,p_message		OUT NOCOPY	VARCHAR2
,p_max_wait		IN			NUMBER	:=	60)
IS
	v_phase			VARCHAR2(30);
	v_status		VARCHAR2(30);
	v_dev_phase		VARCHAR2(30);
	v_dev_status	VARCHAR2(30);
	v_message		VARCHAR2(1000);
	v_wait_complete	BOOLEAN;
BEGIN
	COMMIT;
	IF p_conc_req_id > 0 THEN
		LOOP
			v_wait_complete	:=	fnd_concurrent.wait_for_request
									(request_id	=>	p_conc_req_id
									,INTERVAL	=>	10
									,max_wait	=>	p_max_wait
									-- out arguments
									,phase		=>	v_phase
									,status		=>	v_status
									,dev_phase	=>	v_dev_phase
									,dev_status	=>	v_dev_status
									,message	=>	v_message);
			EXIT WHEN v_phase <> 'Running';
			dbms_lock.sleep(2);
		END LOOP;
	END IF;
	p_phase		:=	v_phase;
	p_status	:=	v_status;
	p_message	:=	v_message;
END conc_wait;
---------------------------------------------------------------------------
PROCEDURE write_log (param_msg VARCHAR2 ) IS
BEGIN
  ---fnd_file.put_line(fnd_file.log, param_msg);
  fnd_file.put_line(fnd_file.log, TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS')||' '||param_msg);
--dbms_output.put_line(TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS')||' '||param_msg);
END write_log;
---------------------------------------------------------------------------
PROCEDURE send_email
(i_smtp_server		IN	VARCHAR2	:=  'mail-gw.tac.co.th'
,i_smtp_port		IN	VARCHAR2	:=  25
,i_from_email		IN	VARCHAR2	:=  'ERPOperationSupport@dtac.co.th'
,i_to_email			IN	VARCHAR2	-- Separate by ,
,i_cc_email			IN	VARCHAR2	:=  NULL
,i_bcc_email		IN	VARCHAR2	:=  NULL
,i_subject			IN	VARCHAR2
,i_program_name		IN	VARCHAR2
,i_file_path		IN	VARCHAR2
,i_filename_pattern	IN	VARCHAR2
,i_period_date		IN	VARCHAR2
,i_email_address	IN	VARCHAR2
,i_gen_bs			IN	VARCHAR2	:=	NULL
,i_gen_pl			IN	VARCHAR2	:=	NULL
,i_gen_cogs			IN	VARCHAR2	:=	NULL
,it_error			IN	nested_varchar_tabtyp
,i_start_time		IN	DATE
,i_end_time			IN	DATE
--,i_reply_email		IN	VARCHAR2	:=  'gl2anaplan_outb_itf@dtac.co.th'
,i_priority			IN	NUMBER	  :=  3   -- Normal priority
,io_status			IN OUT  VARCHAR2
)
IS
	C_ERROR				CONSTANT VARCHAR2(40)   :=  '$$ERROR$$';
	NEW_LINE_IN_MSG		CONSTANT VARCHAR2(10)   :=  '<BR>';
	v_msg_template		VARCHAR2(32000) :=
		'Program : ' || i_program_name || NEW_LINE_IN_MSG ||
		'  Parameter : ' || NEW_LINE_IN_MSG ||
		'    File Path : ' || i_file_path || NEW_LINE_IN_MSG ||
		'    File Name Pattern : ' || i_filename_pattern || NEW_LINE_IN_MSG ||
		'    Period Date : ' || i_period_date || NEW_LINE_IN_MSG ||
		'    Email Address : ' || i_email_address || NEW_LINE_IN_MSG ||
		'    $$OTHER_PARAM$$ ' ||
		NEW_LINE_IN_MSG ||
		'  Concurrent Start : ' || TO_CHAR(i_start_time,'DD Mon YYYY HH24.MI.SS') || NEW_LINE_IN_MSG ||
		'  --------------------------------------------------------------------------' || NEW_LINE_IN_MSG ||
		'  Error' || NEW_LINE_IN_MSG ||
		'  --------------------------------------------------------------------------' || NEW_LINE_IN_MSG ||
		NEW_LINE_IN_MSG ||
		'  Error' || NEW_LINE_IN_MSG ||
		NEW_LINE_IN_MSG || '<TABLE BORDER=1>$$ERROR$$</TABLE>' || NEW_LINE_IN_MSG ||
		NEW_LINE_IN_MSG ||
		'  --------------------------------------------------------------------------' || NEW_LINE_IN_MSG ||
		'  Concurrent End : ' || TO_CHAR(i_end_time,'DD Mon YYYY HH24.MI.SS') || NEW_LINE_IN_MSG
		;
	v_msg				CLOB;
	v_other_param		VARCHAR2(32000)	:=
		'    Gen BS file? : ' || i_gen_bs || NEW_LINE_IN_MSG ||
		'    Gen PL file? : ' || i_gen_pl || NEW_LINE_IN_MSG ||
		'    Gen COGS file? : ' || i_gen_cogs || NEW_LINE_IN_MSG;

BEGIN
	io_status   :=  G_NO_ERROR;

	v_msg	:=  v_msg_template;

-- Column Heading
	IF it_error.COUNT > 0 THEN
		write_log(RPAD('Error Code',12) || RPAD('Description',40) || 'Error Detail');
		v_msg	:=	REPLACE(v_msg,'$$ERROR$$','<TR><TH>Error Code</TH><TH>Description</TH><TH>Error Detail</TH></TR>' || CHR(10) || '$$ERROR$$');
	END IF;

-- Data
	FOR i IN 1..it_error.COUNT
	LOOP
		FOR j IN 1..it_error(i).err_detail.COUNT
		LOOP
			write_log(RPAD(it_error(i).err_code,12) || RPAD(it_error(i).err_desc,40) || it_error(i).err_detail(j));
			v_msg	:=	REPLACE(v_msg,'$$ERROR$$','<TR><TD>' || it_error(i).err_code || '</TD><TD>' || it_error(i).err_desc || '</TD><TD>' || it_error(i).err_detail(j) || '</TD></TR>' || CHR(10) || '$$ERROR$$');
		END LOOP;
	END LOOP;

	v_msg	:=	REPLACE(v_msg,'$$ERROR$$',NULL);

-- Other param
	IF i_gen_bs || i_gen_pl || i_gen_cogs IS NULL THEN
		v_msg	:=	REPLACE(v_msg,'$$OTHER_PARAM$$',NULL);
	ELSE
		v_msg	:=	REPLACE(v_msg,'$$OTHER_PARAM$$',v_other_param);
	END IF;

-- Start Temporary for checking email
	write_log('Sending email info:');
	write_log('SMTP Server:   ' || i_smtp_server);
	write_log('SMTP Port:     ' || i_smtp_port);
	write_log('From email:    ' || i_from_email);
	write_log('To email:      ' || i_to_email);
	write_log('CC email:      ' || i_cc_email);
	write_log('BCC email:     ' || i_bcc_email);
	write_log('Email Subject: ' || i_subject);
	write_log('Email Message: ');
	write_log(v_msg);
-- End Temporary for checking email

	mail_tools.sendmail
		(smtp_server		=>  i_smtp_server
		,smtp_server_port	=>  i_smtp_port
		,from_name			=>  i_from_email
		,to_name			=>  i_to_email  -- list of TO email addresses separated by commas (,)
		,cc_name			=>  i_cc_email  -- list of CC email addresses separated by commas (,)
		,bcc_name			=>  i_bcc_email -- list of BCC email addresses separated by commas (,)
		,subject			=>  i_subject
		,message			=>  v_msg
		,priority			=>  i_priority  -- 1-5 1 being the highest priority and 3 normal priority
		);
EXCEPTION
	WHEN OTHERS THEN
		io_status	:=	'Error while sending email : ' || SQLERRM;
END send_email;
---------------------------------------------------------------------------
PROCEDURE send_email_success
(i_smtp_server		IN	VARCHAR2	:=  'mail-gw.tac.co.th'
,i_smtp_port		IN	VARCHAR2	:=  25
,i_from_email		IN	VARCHAR2	:=  'ERPOperationSupport@dtac.co.th'
,i_to_email			IN	VARCHAR2	-- Separate by ,
,i_cc_email			IN	VARCHAR2	:=  NULL
,i_bcc_email		IN	VARCHAR2	:=  NULL
,i_subject			IN	VARCHAR2
,i_program_name		IN	VARCHAR2
,i_file_path		IN	VARCHAR2
,i_filename_pattern	IN	VARCHAR2
,i_period_date		IN	VARCHAR2
,i_email_address	IN	VARCHAR2
,i_gen_bs			IN	VARCHAR2	:=	NULL
,i_gen_pl			IN	VARCHAR2	:=	NULL
,i_gen_cogs			IN	VARCHAR2	:=	NULL
,i_bs_rows			IN	NUMBER	:=	NULL
,i_pl_rows			IN	NUMBER	:=	NULL
,i_cogs_rows		IN	NUMBER	:=	NULL
,i_po_rows			IN	NUMBER	:=	NULL
,i_bs_amount		IN	NUMBER	:=	NULL
,i_pl_amount		IN	NUMBER	:=	NULL
,i_cogs_amount		IN	NUMBER	:=	NULL
,i_po_amount		IN	NUMBER	:=	NULL
,i_bs_filename		IN	VARCHAR2	:=	NULL
,i_pl_filename		IN	VARCHAR2	:=	NULL
,i_cogs_filename	IN	VARCHAR2	:=	NULL
,i_po_filename		IN	VARCHAR2	:=	NULL
,i_start_time		IN	DATE
,i_end_time			IN	DATE
--,i_reply_email		IN	VARCHAR2	:=  'gl2anaplan_outb_itf@dtac.co.th'
,i_priority			IN	NUMBER	  :=  3   -- Normal priority
,io_status			IN OUT  VARCHAR2
)
IS
	NEW_LINE_IN_MSG		CONSTANT VARCHAR2(10)   :=  '<BR>';
	v_msg_template		VARCHAR2(32000) :=
		'Program : ' || i_program_name || NEW_LINE_IN_MSG ||
		'  Parameter : ' || NEW_LINE_IN_MSG ||
		'    File Path : ' || i_file_path || NEW_LINE_IN_MSG ||
		'    File Name Pattern : ' || i_filename_pattern || NEW_LINE_IN_MSG ||
		'    Period Date : ' || i_period_date || NEW_LINE_IN_MSG ||
		'    Email Address : ' || i_email_address || NEW_LINE_IN_MSG ||
		'    $$OTHER_PARAM$$ ' ||
		NEW_LINE_IN_MSG ||
		'  Concurrent Start : ' || TO_CHAR(i_start_time,'DD Mon YYYY HH24.MI.SS') || NEW_LINE_IN_MSG ||
		'  --------------------------------------------------------------------------' || NEW_LINE_IN_MSG ||
		'  Completed' || NEW_LINE_IN_MSG ||
		'  --------------------------------------------------------------------------' || NEW_LINE_IN_MSG ||
		NEW_LINE_IN_MSG ||
		'  --------------------------------------------------------------------------' || NEW_LINE_IN_MSG ||
		'$$FILE_INFO$$' || NEW_LINE_IN_MSG ||
		'  Concurrent End : ' || TO_CHAR(i_end_time,'DD Mon YYYY HH24.MI.SS') || NEW_LINE_IN_MSG
		;
	v_file_info_template	VARCHAR2(32000)	:=
		'  Data File Name : $$FILENAME$$.csv' || NEW_LINE_IN_MSG ||
		'  Control File Name : $$FILENAME$$.ctl' || NEW_LINE_IN_MSG ||
		NEW_LINE_IN_MSG ||
		'  Total Records : $$ROWS$$' || NEW_LINE_IN_MSG ||
		'  Total Amount  : $$AMOUNT$$' || NEW_LINE_IN_MSG ||
		'  --------------------------------------------------------------------------' || NEW_LINE_IN_MSG ||
		NEW_LINE_IN_MSG
		;
	v_file_info			VARCHAR2(32000);
	v_msg				CLOB;
	v_other_param		VARCHAR2(32000)	:=
		'    Gen BS file? : ' || i_gen_bs || NEW_LINE_IN_MSG ||
		'    Gen PL file? : ' || i_gen_pl || NEW_LINE_IN_MSG ||
		'    Gen COGS file? : ' || i_gen_cogs || NEW_LINE_IN_MSG;

BEGIN
	io_status   :=  G_NO_ERROR;

	v_msg	:=  v_msg_template;

-- File Info
	IF i_bs_rows IS NOT NULL THEN
		v_file_info	:=	v_file_info_template;
		v_file_info	:=	REPLACE(v_file_info,'$$FILENAME$$',i_bs_filename);
		v_file_info	:=	REPLACE(v_file_info,'$$ROWS$$',TO_CHAR(i_bs_rows,'fm999,999,999,990'));
		v_file_info	:=	REPLACE(v_file_info,'$$AMOUNT$$',TO_CHAR(i_bs_amount,'fm999,999,999,999,999,999,990.00'));
		
		v_msg	:=	REPLACE(v_msg,'$$FILE_INFO$$',v_file_info || '$$FILE_INFO$$');
	END IF;

	IF i_pl_rows IS NOT NULL THEN
		v_file_info	:=	v_file_info_template;
		v_file_info	:=	REPLACE(v_file_info,'$$FILENAME$$',i_pl_filename);
		v_file_info	:=	REPLACE(v_file_info,'$$ROWS$$',TO_CHAR(i_pl_rows,'fm999,999,999,990'));
		v_file_info	:=	REPLACE(v_file_info,'$$AMOUNT$$',TO_CHAR(i_pl_amount,'fm999,999,999,999,999,999,990.00'));
		
		v_msg	:=	REPLACE(v_msg,'$$FILE_INFO$$',v_file_info || '$$FILE_INFO$$');
	END IF;

	IF i_cogs_rows IS NOT NULL THEN
		v_file_info	:=	v_file_info_template;
		v_file_info	:=	REPLACE(v_file_info,'$$FILENAME$$',i_cogs_filename);
		v_file_info	:=	REPLACE(v_file_info,'$$ROWS$$',TO_CHAR(i_cogs_rows,'fm999,999,999,990'));
		v_file_info	:=	REPLACE(v_file_info,'$$AMOUNT$$',TO_CHAR(i_cogs_amount,'fm999,999,999,999,999,999,990.00'));
		
		v_msg	:=	REPLACE(v_msg,'$$FILE_INFO$$',v_file_info || '$$FILE_INFO$$');
	END IF;

	IF i_po_rows IS NOT NULL THEN
		v_file_info	:=	v_file_info_template;
		v_file_info	:=	REPLACE(v_file_info,'$$FILENAME$$',i_po_filename);
		v_file_info	:=	REPLACE(v_file_info,'$$ROWS$$',TO_CHAR(i_po_rows,'fm999,999,999,990'));
		v_file_info	:=	REPLACE(v_file_info,'$$AMOUNT$$',TO_CHAR(i_po_amount,'fm999,999,999,999,999,999,990.00'));
		
		v_msg	:=	REPLACE(v_msg,'$$FILE_INFO$$',v_file_info || '$$FILE_INFO$$');
	END IF;

	v_msg	:=	REPLACE(v_msg,'$$FILE_INFO$$',NULL);

-- Other param
	IF i_gen_bs || i_gen_pl || i_gen_cogs IS NULL THEN
		v_msg	:=	REPLACE(v_msg,'$$OTHER_PARAM$$',NULL);
	ELSE
		v_msg	:=	REPLACE(v_msg,'$$OTHER_PARAM$$',v_other_param);
	END IF;

-- Start Temporary for checking email
	write_log('Sending email info:');
	write_log('SMTP Server:   ' || i_smtp_server);
	write_log('SMTP Port:     ' || i_smtp_port);
	write_log('From email:    ' || i_from_email);
	write_log('To email:      ' || i_to_email);
	write_log('CC email:      ' || i_cc_email);
	write_log('BCC email:     ' || i_bcc_email);
	write_log('Email Subject: ' || i_subject);
	write_log('Email Message: ');
	write_log(v_msg);
-- End Temporary for checking email

	mail_tools.sendmail
		(smtp_server		=>  i_smtp_server
		,smtp_server_port	=>  i_smtp_port
		,from_name			=>  i_from_email
		,to_name			=>  i_to_email  -- list of TO email addresses separated by commas (,)
		,cc_name			=>  i_cc_email  -- list of CC email addresses separated by commas (,)
		,bcc_name			=>  i_bcc_email -- list of BCC email addresses separated by commas (,)
		,subject			=>  i_subject
		,message			=>  v_msg
		,priority			=>  i_priority  -- 1-5 1 being the highest priority and 3 normal priority
		);
EXCEPTION
	WHEN OTHERS THEN
		io_status	:=	'Error while sending email : ' || SQLERRM;
END send_email_success;
----------------------------------------------------------------------------
FUNCTION replace_txt
(i_text		IN	VARCHAR2)
RETURN VARCHAR2
IS
	v_text	VARCHAR2(32767)	:=	i_text;
BEGIN
	v_text	:=	REPLACE(v_text,CHR(13) || CHR(10),' ');
	v_text	:=	REPLACE(v_text,CHR(13),' ');
	v_text	:=	REPLACE(v_text,CHR(10),' ');
	v_text	:=	REPLACE(v_text,CHR(9),' ');
	RETURN v_text;
END replace_txt;
----------------------------------------------------------------------------
FUNCTION get_dirobj
(i_path			IN	VARCHAR2
,i_dir_prefix	IN	VARCHAR2)
RETURN VARCHAR2
IS
	v_dirobj	VARCHAR2(250);
BEGIN
	SELECT	MAX(directory_name)
	INTO	v_dirobj
	FROM	all_directories
	WHERE	directory_path	=	i_path;

	IF v_dirobj IS NULL THEN
		SELECT  MAX(directory_name)
		INTO	v_dirobj
		FROM	all_directories
		WHERE   directory_name LIKE i_dir_prefix || '\_%' ESCAPE '\';
		
		IF v_dirobj = UPPER(i_dir_prefix) THEN
			v_dirobj :=  v_dirobj || '_001';
		ELSE
			v_dirobj :=  UPPER(i_dir_prefix) || '_' || TO_CHAR(NVL(TO_NUMBER(SUBSTR(v_dirobj,LENGTH(i_dir_prefix)+2)),0)+1,'fm000');
		END IF;
		
		EXECUTE IMMEDIATE 'CREATE DIRECTORY ' || v_dirobj || ' AS ''' || i_path || '''';
	END IF;
	
	RETURN v_dirobj;
END get_dirobj;
----------------------------------------------------------------------------
PROCEDURE move_data_file
(i_source_file	IN	VARCHAR2
,i_dest_path	IN	VARCHAR2)
IS
PRAGMA AUTONOMOUS_TRANSACTION;
	v_movefile_req_id	NUMBER;
	v_phase				VARCHAR2(100);
	v_status			VARCHAR2(100);
	v_message			VARCHAR2(1000);
BEGIN
	write_log(' ');
	write_log('Moving File ' || i_source_file || ' to ' || i_dest_path);
	
	v_movefile_req_id	:=	fnd_request.submit_request
								(application	=>	'FND'
								,program		=>	'TACFND_MVFILE'
								,argument1		=>	i_source_file
								,argument2		=>	i_dest_path);
	COMMIT;
	write_log('Concurrent Move File is ' || v_movefile_req_id);
	
	IF (v_movefile_req_id > 0) THEN
		conc_wait(v_movefile_req_id, v_phase, v_status, v_message, 60);
		IF (v_status = 'E') THEN
			write_log('Move file from ' || i_source_file || ' to ' || i_dest_path || ' failed.');
		ELSE
			write_log('Move file from ' || i_source_file || ' to ' || i_dest_path || ' successful.');
		END IF;
	ELSE
		write_log('Move file from ' || i_source_file || ' to ' || i_dest_path ||
					' failed, No concurrent program [TACFND_MVFILE].');
	END IF;
	write_log(' ');
EXCEPTION
	WHEN OTHERS THEN
		write_log('Move file from ' || i_source_file || ' to ' ||
					i_dest_path || ' failed with error ' || SQLERRM);
END move_data_file;
----------------------------------------------------------------------------
PROCEDURE sftp_outbound
(i_sftp_username	IN	VARCHAR2
,i_sftp_servername	IN	VARCHAR2
,i_sftp_remote_path	IN	VARCHAR2
,i_filepath			IN	VARCHAR2
,i_filename			IN	VARCHAR2
,o_status			OUT	VARCHAR2)
IS
PRAGMA AUTONOMOUS_TRANSACTION;
	v_sub_req_id	NUMBER;
	v_phase			VARCHAR2(100);
	v_status		VARCHAR2(100);
	v_message		VARCHAR2(1000);
BEGIN
	o_status	:=	G_NO_ERROR;
	write_log(' ');
	write_log('SFTP from File Path ' || i_filepath || ' Filename ' || i_filename);
	
	v_sub_req_id	:=	fnd_request.submit_request
							(application	=>	'SQLAP'
							,program		=>	'TAC_SFTP_OUTBOUND'
							,argument1		=>	i_sftp_username
							,argument2		=>	i_sftp_servername
							,argument3		=>	i_sftp_remote_path
							,argument4		=>	i_filepath
							,argument5		=>	i_filename);
	COMMIT;
	write_log('Concurrent to SFTP File is ' || v_sub_req_id);
	
	IF (v_sub_req_id > 0) THEN
		LOOP
			conc_wait(v_sub_req_id, v_phase, v_status, v_message, 60);
			EXIT WHEN v_phase <> 'Running';
			dbms_lock.sleep(2);
		END LOOP;
		write_log('Concurrent to SFTP File phase = ' || v_phase || ' status = ' || v_status || ' message = ' || v_message);
		IF (v_status LIKE 'E%') THEN	-- Error
			o_status	:=	'Error : Could not sftp file';
			write_log('SFTP from File Path ' || i_filepath || ' Filename ' || i_filename || ' failed.');
		ELSE
			write_log('SFTP from File Path ' || i_filepath || ' Filename ' || i_filename || ' successful.');
		END IF;
	ELSE
		o_status	:=	'Error : Could not sftp file, no program to call';
		write_log('SFTP from File Path ' || i_filepath || ' Filename ' || i_filename ||
					' failed, No concurrent program [TAC_SFTP_OUTBOUND].');
	END IF;
	write_log(' ');
EXCEPTION
	WHEN OTHERS THEN
		o_status	:=	'Error : Could not sftp file';
		write_log('SFTP from File Path ' || i_filepath || ' Filename ' || i_filename ||
					' failed with error ' || SQLERRM);
END sftp_outbound;
----------------------------------------------------------------------------
PROCEDURE gen_gl_bs
(i_program_name		IN	VARCHAR2
,i_period_name		IN	VARCHAR2
,i_period_end_date	IN	DATE
,i_directory		IN	VARCHAR2
,i_filename_format	IN	VARCHAR2
,o_filename			OUT	VARCHAR2
,o_status			OUT	VARCHAR2
,o_rows				OUT	NUMBER
,o_amount			OUT	NUMBER)
IS
/*--------------------------- File Format -----------------------------------
No.	Field Name				M/O	Data Type		Description
Data record
1	Company Code			M	VARCHAR2(10)	Company Code (mapping for ANAPLAN)
2	Account Code			M	VARCHAR2(10)	Account Code
3	Date					M	DATE			YYYYMMDD
4	Amount					M	NUMBER		 	Dr Amount
----------------------------------------------------------------------*/
	CURSOR c_trx IS
		SELECT /*+ ORDERED */	lv_books.attribute3		company_code
				,gcc.segment11			account_code
				,i_period_end_date		gl_date
				,NVL(SUM(bl.begin_balance_dr),0) + NVL(SUM(bl.period_net_dr),0) - NVL(SUM(bl.begin_balance_cr),0) - NVL(SUM(bl.period_net_cr),0)	net_amount
		FROM	fnd_lookup_values		lv_books
				,gl_balances			bl
				,gl_code_combinations	gcc
				,fnd_lookup_values		lv_acct
		WHERE	lv_books.lookup_type	=	'TAC_GL_ANAPLAN_COMPANY'
		AND		bl.set_of_books_id		=	TO_NUMBER(lv_books.attribute1)
		AND		bl.period_name			=	i_period_name
		AND		gcc.segment1 || ''		=	lv_books.attribute2
		AND		gcc.code_combination_id	=	bl.code_combination_id
		AND		gcc.summary_flag		=	'N'
		AND		bl.currency_code		=	'THB'
		AND		bl.actual_flag			=	'A'
		AND		lv_acct.lookup_type		=	'TAC_GL_ACCOUNT_BLS'
		AND		(lv_acct.attribute1 || lv_acct.attribute2 IS NULL OR gcc.segment11 BETWEEN lv_acct.attribute1 AND NVL(lv_acct.attribute2,lv_acct.attribute1))	-- Account
		AND		(lv_acct.attribute3 IS NULL OR gcc.account_type = lv_acct.attribute3) -- Asset, Liability, Owners Equity
		AND		i_period_end_date BETWEEN NVL(lv_books.start_date_active,C_DFLT_DATE_START) AND NVL(lv_books.end_date_active,C_DFLT_DATE_END)
		AND		i_period_end_date BETWEEN NVL(lv_acct.start_date_active,C_DFLT_DATE_START) AND NVL(lv_acct.end_date_active,C_DFLT_DATE_END)
		AND		lv_books.enabled_flag	=	'Y'
		AND		lv_acct.enabled_flag	=	'Y'
		GROUP BY lv_books.attribute3	--	company_code
				,gcc.segment11	--	account_code
				,bl.period_name
--		HAVING NVL(SUM(bl.begin_balance_dr),0) + NVL(SUM(bl.period_net_dr),0) - NVL(SUM(bl.begin_balance_cr),0) - NVL(SUM(bl.period_net_cr),0) <> 0	-- Non-zero only
		ORDER BY bl.period_name DESC, company_code, account_code;

	t_text			tac_text_output_util.varchar_tabtyp;
	t_format		tac_text_output_util.number_tabtyp;

	v_filename		VARCHAR2(100)	:=	TO_CHAR(TO_DATE(i_period_name,'MON-YY'),NVL(i_filename_format,'YYYYMM"_ERP_GLSummary_BS_ANAPLAN_ALL"'));
	v_rows			NUMBER	:=	0;
BEGIN
	o_status	:=	G_NO_ERROR;
	o_amount	:=	0;

	-- Format Header
	t_format.DELETE;
	t_text.DELETE;
	FOR i IN 1..4
	LOOP
		t_format(i)	:=	NULL;
	END LOOP;
	tac_text_output_util.set_format
		(it_col_len		=>	t_format
		,o_status		=>	o_status
		,i_clear_data	=>	'Y');
	IF o_status <> tac_text_output_util.NO_ERROR THEN
		RAISE abnormal_end;
	END IF;
	-- Write Header
	t_text(1)	:= 'Company Code';
	t_text(2)	:= 'Account Code';
	t_text(3)	:= 'Date';
	t_text(4)	:= 'Amount';
	tac_text_output_util.add_txt(it_message	=>	t_text, i_separator => DELIMITER_DATA);

	-- Data
	FOR r_trx IN c_trx
	LOOP
		t_text(1)	:= r_trx.company_code;
		t_text(2)	:= r_trx.account_code;
		t_text(3)	:= TO_CHAR(r_trx.gl_date,'YYYYMMDD');
		t_text(4)	:= r_trx.net_amount;
		tac_text_output_util.add_txt(it_message	=>	t_text, i_separator => DELIMITER_DATA);
		v_rows		:=	v_rows + 1;
		o_amount	:=	o_amount + NVL(t_text(4),0);
	END LOOP;

	tac_text_output_util.write_output
		(o_status		=>	o_status
		,i_directory	=>	i_directory
		,i_filename		=>	v_filename || '.csv'
		,i_skip_output	=>	TRUE
		,i_charset		=>	'UTF8');

	IF o_status <> tac_text_output_util.NO_ERROR THEN
		RAISE abnormal_end;
	END IF;


-- Control File
	t_format.DELETE;
	t_text.DELETE;
	FOR i IN 1..2
	LOOP
		t_format(i)	:=	NULL;
	END LOOP;
	tac_text_output_util.set_format
		(it_col_len		=>	t_format
		,o_status		=>	o_status
		,i_clear_data	=>	'Y');
	IF o_status <> tac_text_output_util.NO_ERROR THEN
		RAISE abnormal_end;
	END IF;
	t_text(1)	:=	v_filename || '.csv';
	t_text(2)	:=	v_rows;
	tac_text_output_util.add_txt(it_message	=>	t_text, i_separator => DELIMITER_CTRL);

	tac_text_output_util.write_output
		(o_status		=>	o_status
		,i_directory	=>	i_directory
		,i_filename		=>	v_filename || '.ctl'
		,i_skip_output	=>	TRUE
		,i_charset		=>	'UTF8');

	IF o_status <> tac_text_output_util.NO_ERROR THEN
		RAISE abnormal_end;
	END IF;

	o_filename	:=	v_filename;
	o_rows		:=	v_rows;

EXCEPTION
	WHEN abnormal_end THEN
		NULL;
	WHEN OTHERS THEN
		o_status		:=	'Error gen_gl_bs : ' || SQLERRM;
END gen_gl_bs;
----------------------------------------------------------------------------
PROCEDURE gen_gl_pl
(i_program_name		IN	VARCHAR2
,i_period_name		IN	VARCHAR2
,i_period_end_date	IN	DATE
,i_directory		IN	VARCHAR2
,i_filename_format	IN	VARCHAR2
,o_filename			OUT	VARCHAR2
,o_status			OUT	VARCHAR2
,o_rows				OUT	NUMBER
,o_amount			OUT	NUMBER)
IS
/*--------------------------- File Format -----------------------------------
No.	Field Name				M/O	Data Type		Description
Data record
1	Company Code			M	VARCHAR2(10)	Company Code (mapping for ANAPLAN)
2	Account Code			M	VARCHAR2(10)	Account Code
3	Date					M	DATE			YYYYMMDD
4	Amount					M	NUMBER		 	Dr Amount
----------------------------------------------------------------------*/
	CURSOR c_trx IS
		SELECT /*+ ORDERED */	lv_books.attribute3		company_code
				,gcc.segment11			account_code
				,i_period_end_date		gl_date
				,NVL(SUM(bl.period_net_cr),0) - NVL(SUM(bl.period_net_dr),0)	net_amount
		FROM	fnd_lookup_values		lv_books
				,gl_balances			bl
				,gl_code_combinations	gcc
				,fnd_lookup_values		lv_acct
		WHERE	lv_books.lookup_type	=	'TAC_GL_ANAPLAN_COMPANY'
		AND		bl.set_of_books_id		=	TO_NUMBER(lv_books.attribute1)
		AND		bl.period_name			=	i_period_name
		AND		gcc.segment1 || ''		=	lv_books.attribute2
		AND		gcc.code_combination_id	=	bl.code_combination_id
		AND		gcc.summary_flag		=	'N'
		AND		bl.currency_code		=	'THB'
		AND		bl.actual_flag			=	'A'
		AND		lv_acct.lookup_type		=	'TAC_GL_ACCOUNT_P&L'
		AND		(lv_acct.attribute1 || lv_acct.attribute2 IS NULL OR gcc.segment11 BETWEEN lv_acct.attribute1 AND NVL(lv_acct.attribute2,lv_acct.attribute1))	-- Account
		AND		(lv_acct.attribute3 IS NULL OR gcc.account_type = lv_acct.attribute3) -- Revenue, Expense
		AND		i_period_end_date BETWEEN NVL(lv_books.start_date_active,C_DFLT_DATE_START) AND NVL(lv_books.end_date_active,C_DFLT_DATE_END)
		AND		i_period_end_date BETWEEN NVL(lv_acct.start_date_active,C_DFLT_DATE_START) AND NVL(lv_acct.end_date_active,C_DFLT_DATE_END)
		AND		lv_books.enabled_flag	=	'Y'
		AND		lv_acct.enabled_flag	=	'Y'
		GROUP BY lv_books.attribute3	--	company_code
				,gcc.segment11	--	account_code
				,bl.period_name
--		HAVING NVL(SUM(bl.period_net_cr),0) - NVL(SUM(bl.period_net_dr),0) <> 0	-- Non-zero only
		ORDER BY bl.period_name DESC, company_code, account_code;

	t_text			tac_text_output_util.varchar_tabtyp;
	t_format		tac_text_output_util.number_tabtyp;

	v_filename		VARCHAR2(100)	:=	TO_CHAR(TO_DATE(i_period_name,'MON-YY'),NVL(i_filename_format,'YYYYMM"_ERP_GLSummary_PL_ANAPLAN_ALL"'));
	v_rows			NUMBER	:=	0;
BEGIN
	o_status	:=	G_NO_ERROR;
	o_amount	:=	0;

	-- Format Header
	t_format.DELETE;
	t_text.DELETE;
	FOR i IN 1..4
	LOOP
		t_format(i)	:=	NULL;
	END LOOP;
	tac_text_output_util.set_format
		(it_col_len		=>	t_format
		,o_status		=>	o_status
		,i_clear_data	=>	'Y');
	IF o_status <> tac_text_output_util.NO_ERROR THEN
		RAISE abnormal_end;
	END IF;
	-- Write Header Record
	t_text(1)	:= 'Company Code';
	t_text(2)	:= 'Account Code';
	t_text(3)	:= 'Date';
	t_text(4)	:= 'Amount';
	tac_text_output_util.add_txt(it_message	=>	t_text, i_separator => DELIMITER_DATA);

	-- Data
	FOR r_trx IN c_trx
	LOOP
		t_text(1)	:= r_trx.company_code;
		t_text(2)	:= r_trx.account_code;
		t_text(3)	:= TO_CHAR(r_trx.gl_date,'YYYYMMDD');
		t_text(4)	:= r_trx.net_amount;
		tac_text_output_util.add_txt(it_message	=>	t_text, i_separator => DELIMITER_DATA);
		v_rows	:=	v_rows + 1;
		o_amount	:=	o_amount + NVL(t_text(4),0);
	END LOOP;

	tac_text_output_util.write_output
		(o_status		=>	o_status
		,i_directory	=>	i_directory
		,i_filename		=>	v_filename || '.csv'
		,i_skip_output	=>	TRUE
		,i_charset		=>	'UTF8');

	IF o_status <> tac_text_output_util.NO_ERROR THEN
		RAISE abnormal_end;
	END IF;

-- Control File
	t_format.DELETE;
	t_text.DELETE;
	FOR i IN 1..2
	LOOP
		t_format(i)	:=	NULL;
	END LOOP;
	tac_text_output_util.set_format
		(it_col_len		=>	t_format
		,o_status		=>	o_status
		,i_clear_data	=>	'Y');
	IF o_status <> tac_text_output_util.NO_ERROR THEN
		RAISE abnormal_end;
	END IF;
	t_text(1)	:=	v_filename || '.csv';
	t_text(2)	:=	v_rows;
	tac_text_output_util.add_txt(it_message	=>	t_text, i_separator => DELIMITER_CTRL);

	tac_text_output_util.write_output
		(o_status		=>	o_status
		,i_directory	=>	i_directory
		,i_filename		=>	v_filename || '.ctl'
		,i_skip_output	=>	TRUE
		,i_charset		=>	'UTF8');

	IF o_status <> tac_text_output_util.NO_ERROR THEN
		RAISE abnormal_end;
	END IF;

	o_filename	:=	v_filename;
	o_rows		:=	v_rows;

EXCEPTION
	WHEN abnormal_end THEN
		NULL;
	WHEN OTHERS THEN
		o_status		:=	'Error gen_gl_pl : ' || SQLERRM;
END gen_gl_pl;
----------------------------------------------------------------------------
PROCEDURE gen_gl_cogs
(i_program_name		IN	VARCHAR2
,i_period_name		IN	VARCHAR2
,i_period_end_date	IN	DATE
,i_directory		IN	VARCHAR2
,i_filename_format	IN	VARCHAR2
,o_filename			OUT	VARCHAR2
,o_status			OUT	VARCHAR2
,o_rows				OUT	NUMBER
,o_amount			OUT	NUMBER)
IS
/*--------------------------- File Format -----------------------------------
No.	Field Name				M/O	Data Type		Description
Data record
1	Company Code			M	VARCHAR2(10)	Company Code (mapping for ANAPLAN)
2	Account Code			M	VARCHAR2(10)	Account Code
3	Date					M	DATE			YYYYMMDD
4	Amount					M	NUMBER		 	Dr Amount
----------------------------------------------------------------------*/
	CURSOR c_trx IS
		SELECT /*+ ORDERED */	lv_books.attribute3		company_code
				,gcc.segment11			account_code
				,gcc.segment10			rc_code
				,i_period_end_date		gl_date
				,NVL(SUM(bl.period_net_dr),0) - NVL(SUM(bl.period_net_cr),0)	net_amount
		FROM	fnd_lookup_values		lv_books
				,gl_balances			bl
				,gl_code_combinations	gcc
				,fnd_lookup_values		lv_acct
		WHERE	lv_books.lookup_type	=	'TAC_GL_ANAPLAN_COMPANY'
		AND		bl.set_of_books_id		=	TO_NUMBER(lv_books.attribute1)
		AND		bl.period_name			=	i_period_name
		AND		gcc.segment1 || ''		=	lv_books.attribute2
		AND		gcc.code_combination_id	=	bl.code_combination_id
		AND		gcc.summary_flag		=	'N'
		AND		bl.currency_code		=	'THB'
		AND		bl.actual_flag			=	'A'
		AND		lv_acct.lookup_type		=	'TAC_GL_ACCOUNT_COGS'
		AND		(lv_acct.attribute1 || lv_acct.attribute2 IS NULL OR gcc.segment11 BETWEEN lv_acct.attribute1 AND NVL(lv_acct.attribute2,lv_acct.attribute1))	-- Account
		AND		(lv_acct.attribute3 IS NULL OR gcc.account_type = lv_acct.attribute3) -- Expense
		AND		i_period_end_date BETWEEN NVL(lv_books.start_date_active,C_DFLT_DATE_START) AND NVL(lv_books.end_date_active,C_DFLT_DATE_END)
		AND		i_period_end_date BETWEEN NVL(lv_acct.start_date_active,C_DFLT_DATE_START) AND NVL(lv_acct.end_date_active,C_DFLT_DATE_END)
		AND		lv_books.enabled_flag	=	'Y'
		AND		lv_acct.enabled_flag	=	'Y'
		GROUP BY lv_books.attribute3	--	company_code
				,gcc.segment11	--	account_code
				,gcc.segment10	--	rc_code
				,bl.period_name
		HAVING NVL(SUM(bl.period_net_dr),0) - NVL(SUM(bl.period_net_cr),0) <> 0	-- Non-zero only
		ORDER BY bl.period_name DESC, company_code, account_code, rc_code;

	t_text			tac_text_output_util.varchar_tabtyp;
	t_format		tac_text_output_util.number_tabtyp;

	v_filename		VARCHAR2(100)	:=	TO_CHAR(TO_DATE(i_period_name,'MON-YY'),NVL(i_filename_format,'YYYYMM"_ERP_GLSummary_COGS_ANAPLAN_ALL"'));
	v_rows			NUMBER	:=	0;
BEGIN
	o_status	:=	G_NO_ERROR;
	o_amount	:=	0;

	-- Format Header
	t_format.DELETE;
	t_text.DELETE;
	FOR i IN 1..5
	LOOP
		t_format(i)	:=	NULL;
	END LOOP;
	tac_text_output_util.set_format
		(it_col_len		=>	t_format
		,o_status		=>	o_status
		,i_clear_data	=>	'Y');
	IF o_status <> tac_text_output_util.NO_ERROR THEN
		RAISE abnormal_end;
	END IF;
	-- Write Header Record
	t_text(1)	:= 'Company Code';
	t_text(2)	:= 'Account Code';
	t_text(3)	:= 'RC Code';
	t_text(4)	:= 'Date';
	t_text(5)	:= 'Amount';
	tac_text_output_util.add_txt(it_message	=>	t_text, i_separator => DELIMITER_DATA);

	-- Data
	FOR r_trx IN c_trx
	LOOP
		t_text(1)	:= r_trx.company_code;
		t_text(2)	:= r_trx.account_code;
		t_text(3)	:= r_trx.rc_code;
		t_text(4)	:= TO_CHAR(r_trx.gl_date,'YYYYMMDD');
		t_text(5)	:= r_trx.net_amount;
		tac_text_output_util.add_txt(it_message	=>	t_text, i_separator => DELIMITER_DATA);
		v_rows	:=	v_rows + 1;
		o_amount	:=	o_amount + NVL(t_text(5),0);
	END LOOP;

	tac_text_output_util.write_output
		(o_status		=>	o_status
		,i_directory	=>	i_directory
		,i_filename		=>	v_filename || '.csv'
		,i_skip_output	=>	TRUE
		,i_charset		=>	'UTF8');

	IF o_status <> tac_text_output_util.NO_ERROR THEN
		RAISE abnormal_end;
	END IF;

-- Control File
	t_format.DELETE;
	t_text.DELETE;
	FOR i IN 1..2
	LOOP
		t_format(i)	:=	NULL;
	END LOOP;
	tac_text_output_util.set_format
		(it_col_len		=>	t_format
		,o_status		=>	o_status
		,i_clear_data	=>	'Y');
	IF o_status <> tac_text_output_util.NO_ERROR THEN
		RAISE abnormal_end;
	END IF;
	t_text(1)	:=	v_filename || '.csv';
	t_text(2)	:=	v_rows;
	tac_text_output_util.add_txt(it_message	=>	t_text, i_separator => DELIMITER_CTRL);

	tac_text_output_util.write_output
		(o_status		=>	o_status
		,i_directory	=>	i_directory
		,i_filename		=>	v_filename || '.ctl'
		,i_skip_output	=>	TRUE
		,i_charset		=>	'UTF8');

	IF o_status <> tac_text_output_util.NO_ERROR THEN
		RAISE abnormal_end;
	END IF;

	o_filename	:=	v_filename;
	o_rows		:=	v_rows;

EXCEPTION
	WHEN abnormal_end THEN
		NULL;
	WHEN OTHERS THEN
		o_status		:=	'Error gen_gl_cogs : ' || SQLERRM;
END gen_gl_cogs;
----------------------------------------------------------------------------
PROCEDURE gen_po
(i_program_name		IN	VARCHAR2
,i_period_name		IN	VARCHAR2
,i_period_end_date	IN	DATE
,i_directory		IN	VARCHAR2
,i_filename_format	IN	VARCHAR2
,o_filename			OUT	VARCHAR2
,o_status			OUT	VARCHAR2
,o_rows				OUT	NUMBER
,o_amount			OUT	NUMBER)
IS
/*--------------------------- File Format -----------------------------------
No.	Field Name				M/O	Data Type		Description
Data record
1	Company Code			M	VARCHAR2(10)	Company Code (mapping for ANAPLAN)
2	Account Code			M	VARCHAR2(10)	Account Code
3	Date					M	DATE			YYYYMMDD
4	Amount					M	NUMBER		 	Dr Amount
----------------------------------------------------------------------*/

	CURSOR c_trx IS
		SELECT	/*+ INDEX(pap,PA_PROJECTS_U3) */
				pap.segment1				project_code
/*				,gcpr.concatenated_segments pr_combine_account
				,gcpr.segment11             pr_account
				,gcpr.segment10             pr_cost_center
				,gcpr.segment21             pr_system_code
				,gcpo.concatenated_segments po_combine_account
				,gcpo.segment11             po_account
				,gcpo.segment10             po_cost_center
				,gcpo.segment21             po_system_code
				,DECODE(pd.req_distribution_id,NULL,NVL(pd.req_header_reference_num, porh.segment1),porh.segment1)	pr_number
				,porh.authorization_status									pr_status
				,TRUNC(NVL(pord.gl_encumbered_date, pord.creation_date))	pr_date
				,pord.expenditure_type										pr_expenditure_type
				,DECODE(pd.req_distribution_id,NULL,NVL(pd.req_line_reference_num, porl.line_num),porl.line_num)	pr_line_num
				,pord.distribution_num										pr_distribution_num
				,DECODE(porl.cancel_flag,'Y',0,
						DECODE(plt.order_type_lookup_code,
								'FIXED PRICE',NVL(porl.amount,0),
								'RATE',NVL(porl.amount,0),
								CASE
									WHEN NVL(porl.rate, 1) = 1 THEN
										(porl.quantity * porl.unit_price)
									ELSE
										NVL(porl.attribute1,porl.quantity) *
										NVL(porl.attribute2,porl.unit_price) * NVL(porl.rate,1)
									END)
						) pr_amount
				,DECODE(porl.closed_code,'FINALLY CLOSED',0,
						DECODE(pd.po_distribution_id,NULL,
								DECODE(pd.req_distribution_id,NULL,
										DECODE(porl.cancel_flag,'Y',0,
											NVL(DECODE(plt.order_type_lookup_code,
														'FIXED PRICE', NVL(porl.amount,0),
														'RATE',NVL(porl.amount,0),
														CASE
															WHEN NVL(porl.rate, 1) = 1 THEN
																(porl.quantity * porl.unit_price)
															ELSE
																NVL(porl.attribute1,porl.quantity) *
																NVL(porl.attribute2,porl.unit_price) * NVL(porl.rate,1)
															END
														) -
											   (((pd.quantity_ordered -
												DECODE(plt.order_type_lookup_code,
														'RATE', NVL(pd.amount_cancelled,0),
														'FIXED PRICE', NVL(pd.amount_cancelled,0),
														NVL(pd.quantity_cancelled,0))) *
														NVL(TO_NUMBER(DECODE(plt.order_type_lookup_code,
																	  'AMOUNT', NULL, NVL(pll.price_override,0)))
															,1)
												 ) * NVL(ph.rate,1)
												),
												(DECODE(plt.order_type_lookup_code,
														'FIXED PRICE', NVL(porl.amount,0),
														'RATE', NVL(porl.amount,0),
														CASE
															WHEN NVL(porl.rate, 1) = 1 THEN
																(porl.quantity * porl.unit_price)
															ELSE
																NVL(porl.attribute1, porl.quantity) *
																NVL(porl.attribute2, porl.unit_price) * NVL(porl.rate, 1)
															END
														)
												))),
									0),
							 0)
						)	pr_outstanding_amount
				,ph.segment1	po_number
				,DECODE(pd.gl_cancelled_date, NULL, ph.authorization_status, 'CANCELLED') po_approval_status
				,pll.closed_code po_closure_status
				,TRUNC(NVL(pd.gl_encumbered_date, pd.creation_date))	po_date
				,pl.line_num			po_line_num
				,pll.shipment_num		shipment
				,pd.distribution_num	po_distribution_num
				,ph.currency_code		po_currency
				,DECODE(ph.currency_code, NULL, NULL, NVL(ph.rate,1))	po_rate*/
-- Support Finally Closed
/*				,SUM(DECODE(pd.gl_cancelled_date, NULL,
						  ((NVL(pd.quantity_ordered,0) -
						  DECODE(plt.order_type_lookup_code,
								'RATE', NVL(pd.amount_cancelled, 0),
								'FIXED PRICE', NVL(pd.amount_cancelled, 0),
								NVL(pd.quantity_cancelled, 0)
								)
							) *
							NVL(TO_NUMBER(DECODE(plt.order_type_lookup_code, 'AMOUNT', NULL,
												NVL(pll.price_override, 0)
												)), 1)
							) * NVL(ph.rate, 1),
						  0)
					)										po_amount*/
				,SUM(DECODE(pd.gl_cancelled_date, NULL,
						  DECODE(pll.closed_code, 'FINALLY CLOSED', pd.quantity_delivered,
							  ((NVL(pd.quantity_ordered,0) -
							  DECODE(plt.order_type_lookup_code,
									'RATE', NVL(pd.amount_cancelled, 0),
									'FIXED PRICE', NVL(pd.amount_cancelled, 0),
									NVL(pd.quantity_cancelled, 0)
									)
								) *
								NVL(TO_NUMBER(DECODE(plt.order_type_lookup_code, 'AMOUNT', NULL,
													NVL(pll.price_override, 0)
													)), 1)
								))
						  * NVL(ph.rate, 1),
						  0)
					)										po_amount
-- End Support Finally Closed
				,SUM(DECODE(pd.gl_cancelled_date, NULL,
						  DECODE(pll.closed_code, 'FINALLY CLOSED', 0,
								 DECODE(pll.match_option,
										'P',
										((((NVL(pd.quantity_ordered, 0) -
										DECODE(plt.order_type_lookup_code,
												   'RATE',
												   NVL(pd.amount_cancelled, 0),
												   'FIXED PRICE',
												   NVL(pd.amount_cancelled, 0),
												   NVL(pd.quantity_cancelled, 0))) *
										NVL(TO_NUMBER(DECODE(plt.order_type_lookup_code,
																'AMOUNT',
																NULL,
																NVL(pll.price_override,
																	0))),
											   1)) * NVL(ph.rate, 1)) -
										(NVL(pd.amount_billed, 0) * NVL(ph.rate, 1))),
										'R',
										(((NVL(pd.quantity_ordered, 0) -
										DECODE(plt.order_type_lookup_code,
												  'RATE',
												  NVL(pd.amount_cancelled, 0),
												  'FIXED PRICE',
												  NVL(pd.amount_cancelled, 0),
												  NVL(pd.quantity_cancelled, 0))) *
										NVL(TO_NUMBER(DECODE(plt.order_type_lookup_code,
															   'AMOUNT',
															   NULL,
															   NVL(pll.price_override,
																   0))),
											  1) * NVL(ph.rate, 1)) -
										(NVL((SELECT SUM(DECODE(transaction_type,
																'RETURN TO VENDOR',
																- (NVL(source_doc_quantity,
																	  0) *
																  NVL(po_unit_price, 1) *
																  NVL(currency_conversion_rate,
																	  1)),
																NVL(source_doc_quantity,
																	0) *
																NVL(po_unit_price, 1) *
																NVL(currency_conversion_rate,
																	1))) receipt_amount_thai
											  FROM rcv_transactions
											  WHERE po_distribution_id =
													pd.po_distribution_id
											  AND NVL(transaction_type, 'NULL') IN
													('RECEIVE',
													 'CORRECT',
													 'MATCH',
													 'RETURN TO VENDOR',
													 'NULL')
											  AND NVL(destination_type_code,
													 'RECEIVING') = 'RECEIVING'
											  GROUP BY po_distribution_id),
											  0))),
										0)),
						  0)
					)			po_outstanding_amount
/*				,((SELECT	MAX(full_name)
					FROM	per_all_people_f ppf
					WHERE	ppf.person_id	=	pd.deliver_to_person_id))	requester_name
				,((SELECT	paf.ass_attribute2
					FROM	per_assignments_f paf
					WHERE	paf.person_id	=	pd.deliver_to_person_id
					AND SYSDATE BETWEEN paf.effective_start_date AND paf.effective_end_date)) requester_division
				,porh.description				pr_header_description
				,rsh.receipt_num				receipt_num
				,TRUNC(rtxns.transaction_date)	receipt_date
				,rtxns.currency_code			receipt_currency
				,DECODE(rtxns.currency_code, NULL, NULL, NVL(currency_conversion_rate,1))	receipt_rate
				,DECODE(rtxns.transaction_type,
						'RETURN TO VENDOR', -(NVL(rtxns.source_doc_quantity,0) * NVL(rtxns.po_unit_price,0)),
						NVL(rtxns.source_doc_quantity,0) * NVL(rtxns.po_unit_price,0))
					* NVL(rtxns.currency_conversion_rate,1)			receipt_amount_thai
				,ai1.invoice_num invoice_num
				,TRUNC(ai1.invoice_date) invoice_date
				,TRUNC(aid.accounting_date) gl_date
				,NVL(aid.amount,0) * NVL(ai1.exchange_rate,1)		invoice_amount_thai
				,g_request_id					request_id
				,pap.project_id					project_id
				,pord.distribution_id			pr_distribution_id
				,pd.po_distribution_id			po_distribution_id
				,rtxns.transaction_id			transaction_id
				,aid.invoice_distribution_id	invoice_distribution_id
				,pll.match_option				match_option
				,'NEW'							validate_status
				,pap.name						project_name
				,mc.segment1 || '.' || mc.segment2 || '.' || mc.segment3 || '.' || mc.segment4	po_category
				,SYSDATE*/
		FROM	po_req_distributions_all		pord
				,po_requisition_lines_all		porl
				,po_requisition_headers_all		porh
				,po_distributions_all			pd
				,po_line_locations_all			pll
				,po_lines_all					pl
				,po_line_types					plt
				,po_headers_all					ph
/*				,rcv_transactions				rtxns
				,rcv_shipment_headers			rsh
				,rcv_shipment_lines				rsl
				,ap_invoice_distributions_all	aid
				,ap_invoices_all				ai1
				,mtl_categories					mc
				,gl_code_combinations_kfv		gcpr
				,gl_code_combinations_kfv		gcpo*/
				,pa_projects_all				pap
				,fnd_lookup_values				lv
		--
		WHERE	lv.lookup_type	=	'TAC_PO_PROJECT_ANAPLAN'
		AND		lv.enabled_flag	=	'Y'
		AND		i_period_end_date BETWEEN NVL(lv.start_date_active,C_DFLT_DATE_START) AND NVL(lv.end_date_active,C_DFLT_DATE_END)
		AND		pord.project_id				=	pap.project_id
		AND		pord.distribution_id		=	pd.req_distribution_id(+)
		AND		pord.requisition_line_id	=	porl.requisition_line_id(+)
		AND		porl.requisition_header_id	=	porh.requisition_header_id(+)
		AND		pd.po_header_id				=	ph.po_header_id(+)
		AND		pd.line_location_id			=	pll.line_location_id(+)
		AND		pll.po_line_id				=	pl.po_line_id(+)
		AND		pl.line_type_id				=	plt.line_type_id(+)
/*		AND		rtxns.po_distribution_id(+)	=	pd.po_distribution_id
		AND		rtxns.shipment_line_id		=	rsl.shipment_line_id(+)
		AND		rsl.shipment_header_id		=	rsh.shipment_header_id(+)
		AND		rtxns.transaction_id		=	aid.rcv_transaction_id(+)
		AND		aid.invoice_id				=	ai1.invoice_id(+)
		AND		NVL(rtxns.transaction_type, 'NULL') IN
			  ('RECEIVE', 'CORRECT', 'MATCH', 'RETURN TO VENDOR', 'NULL')
		AND		NVL(rtxns.destination_type_code, 'RECEIVING') = 'RECEIVING'
		AND		pl.category_id = mc.category_id(+)
			 -- account info added by AP@BAS
		AND		pord.code_combination_id = gcpr.code_combination_id(+)
		AND		pd.code_combination_id = gcpo.code_combination_id(+)*/
		AND		NVL(porl.modified_by_agent_flag, 'N') = 'N'
		AND		pap.segment1 BETWEEN lv.attribute1 AND NVL(lv.attribute2,lv.attribute1)
		AND		ph.creation_date	<=	TRUNC(i_period_end_date) + 0.99999	-- PO Date
		GROUP BY pap.segment1;

	t_text			tac_text_output_util.varchar_tabtyp;
	t_format		tac_text_output_util.number_tabtyp;

	v_filename		VARCHAR2(100)	:=	TO_CHAR(TO_DATE(i_period_name,'MON-YY'),NVL(i_filename_format,'YYYYMM"_ERP_GLSummary_COGS_ANAPLAN_ALL"'));
	v_rows			NUMBER	:=	0;
BEGIN
	o_status	:=	G_NO_ERROR;
	o_amount	:=	0;

	-- Format Header
	t_format.DELETE;
	t_text.DELETE;
	FOR i IN 1..3
	LOOP
		t_format(i)	:=	NULL;
	END LOOP;
	tac_text_output_util.set_format
		(it_col_len		=>	t_format
		,o_status		=>	o_status
		,i_clear_data	=>	'Y');
	IF o_status <> tac_text_output_util.NO_ERROR THEN
		RAISE abnormal_end;
	END IF;
	-- Write Header Record
	t_text(1)	:= 'Project Code';
	t_text(2)	:= 'PO Amount (THB)';
	t_text(3)	:= 'Date';
	tac_text_output_util.add_txt(it_message	=>	t_text, i_separator => DELIMITER_DATA);

	-- Data
	FOR r_trx IN c_trx
	LOOP
		t_text(1)	:= r_trx.project_code;
		t_text(2)	:= r_trx.po_amount;
		t_text(3)	:= TO_CHAR(i_period_end_date,'YYYYMMDD');
		tac_text_output_util.add_txt(it_message	=>	t_text, i_separator => DELIMITER_DATA);
		v_rows	:=	v_rows + 1;
		o_amount	:=	o_amount + NVL(t_text(2),0);
	END LOOP;

	tac_text_output_util.write_output
		(o_status		=>	o_status
		,i_directory	=>	i_directory
		,i_filename		=>	v_filename || '.csv'
		,i_skip_output	=>	TRUE
		,i_charset		=>	'UTF8');

	IF o_status <> tac_text_output_util.NO_ERROR THEN
		RAISE abnormal_end;
	END IF;

-- Control File
	t_format.DELETE;
	t_text.DELETE;
	FOR i IN 1..2
	LOOP
		t_format(i)	:=	NULL;
	END LOOP;
	tac_text_output_util.set_format
		(it_col_len		=>	t_format
		,o_status		=>	o_status
		,i_clear_data	=>	'Y');
	IF o_status <> tac_text_output_util.NO_ERROR THEN
		RAISE abnormal_end;
	END IF;
	t_text(1)	:=	v_filename || '.csv';
	t_text(2)	:=	v_rows;
	tac_text_output_util.add_txt(it_message	=>	t_text, i_separator => DELIMITER_CTRL);

	tac_text_output_util.write_output
		(o_status		=>	o_status
		,i_directory	=>	i_directory
		,i_filename		=>	v_filename || '.ctl'
		,i_skip_output	=>	TRUE
		,i_charset		=>	'UTF8');

	IF o_status <> tac_text_output_util.NO_ERROR THEN
		RAISE abnormal_end;
	END IF;

	o_filename	:=	v_filename;
	o_rows		:=	v_rows;

EXCEPTION
	WHEN abnormal_end THEN
		NULL;
	WHEN OTHERS THEN
		o_status		:=	'Error gen_po : ' || SQLERRM;
END gen_po;
----------------------------------------------------------------------------
PROCEDURE get_ap_data
(l_source               IN VARCHAR2
,l_category             IN VARCHAR2
,l_ael_line_id          IN VARCHAR2
,l_acct_line_type_name  OUT VARCHAR2
,l_trx_line_number      OUT NUMBER
,l_currency_code        OUT VARCHAR2
,l_trx_number_displayed OUT VARCHAR2
,l_entered_dr           OUT NUMBER
,l_entered_cr           OUT NUMBER
,l_accounted_dr         OUT NUMBER
,l_accounted_cr         OUT NUMBER
,l_trx_hdr_id           OUT NUMBER
,l_third_party_name     OUT VARCHAR2
,l_doc_sequence_name    OUT VARCHAR2
,l_doc_sequence_value   OUT VARCHAR2)
IS
BEGIN
	IF l_source = 'Payables' AND l_category IN ('Reconciled Payments', 'Cross Currency', 'Payments') THEN
		SELECT	l1.displayed_field acct_line_type_name
				,d.distribution_line_number trx_line_number
				,ael.currency_code currency_code
				,to_char(c.check_number) trx_number_displayed
				,ael.entered_dr entered_dr
				,ael.entered_cr entered_cr
				,ael.accounted_dr accounted_dr
				,ael.accounted_cr accounted_cr
				,c.check_id trx_hdr_id
				,v.vendor_name third_party_name
				,fd.name doc_sequence_name
				,ael.subledger_doc_sequence_value doc_sequence_value
		INTO	l_acct_line_type_name
				,l_trx_line_number
				,l_currency_code
				,l_trx_number_displayed
				,l_entered_dr
				,l_entered_cr
				,l_accounted_dr
				,l_accounted_cr
				,l_trx_hdr_id
				,l_third_party_name
				,l_doc_sequence_name
				,l_doc_sequence_value
		FROM	ap_lookup_codes					l1
				,fnd_document_sequences			fd
				,po_vendors						v
				,ap_checks_all					c
				,ap_accounting_events_all		ae
				,ap_ae_headers_all				aeh
				,ap_invoice_distributions_all	d
				,ap_ae_lines_all				ael
		WHERE	l1.lookup_code					=	ael.ae_line_type_code
		AND		l1.lookup_type					=	'AE LINE TYPE'
		AND		ae.source_table					=	'AP_CHECKS'
		AND		ae.accounting_event_id			=	aeh.accounting_event_id
		AND		aeh.ae_header_id				=	ael.ae_header_id
		AND		c.check_id						=	ae.source_id
		AND		d.invoice_distribution_id(+)	=	DECODE(ael.source_table, 'AP_INVOICE_DISTRIBUTIONS', ael.source_id, NULL)
		AND		ael.subledger_doc_sequence_id	=	fd.doc_sequence_id(+)
		AND		ael.third_party_id				=	v.vendor_id
		AND		ael.ae_line_id					=	l_ael_line_id
		UNION ALL
		SELECT	l1.displayed_field			acct_line_type_name
				,to_number(NULL)			trx_line_number
				,ael.currency_code			currency_code
				,to_char(c.check_number)	trx_number_displayed
				,ael.entered_dr				entered_dr
				,ael.entered_cr				entered_cr
				,ael.accounted_dr			accounted_dr
				,ael.accounted_cr			accounted_cr
				,c.check_id					trx_hdr_id
				,v.vendor_name				third_party_name
				,fd.name					doc_sequence_name
				,ael.subledger_doc_sequence_value	doc_sequence_value
		FROM	ap_lookup_codes				l1
				,fnd_document_sequences		fd
				,po_vendors					v
				,ap_accounting_events_all	ae
				,ap_checks					c
				,ap_ae_headers_all			aeh
				,ap_ae_lines_all			ael
		WHERE	l1.lookup_code			=	ael.ae_line_type_code
		AND		l1.lookup_type			=	'AE LINE TYPE'
		AND		ael.third_party_id		=	v.vendor_id
		AND		ael.subledger_doc_sequence_id	=	fd.doc_sequence_id(+)
		AND		c.check_id				=	ae.source_id
		AND		ae.source_table			=	'AP_PAYMENT_HISTORY'
		AND		ae.accounting_event_id	=	aeh.accounting_event_id
		AND		aeh.ae_header_id		=	ael.ae_header_id
		AND		ael.ae_line_id			=	l_ael_line_id;
	ELSIF (l_source = 'Payables' AND l_category = 'Purchase Invoices') THEN
		SELECT	l1.displayed_field			acct_line_type_name
				,d.distribution_line_number	trx_line_number
				,ael.currency_code			currency_code
				,i.invoice_num				trx_number_displayed
				,ael.entered_dr				entered_dr
				,ael.entered_cr				entered_cr
				,ael.accounted_dr			accounted_dr
				,ael.accounted_cr			accounted_cr
				,i.invoice_id				trx_hdr_id
				,v.vendor_name				third_party_name
				,fd.name					doc_sequence_name
				,ael.subledger_doc_sequence_value	doc_sequence_value
		INTO	l_acct_line_type_name
				,l_trx_line_number
				,l_currency_code
				,l_trx_number_displayed
				,l_entered_dr
				,l_entered_cr
				,l_accounted_dr
				,l_accounted_cr
				,l_trx_hdr_id
				,l_third_party_name
				,l_doc_sequence_name
				,l_doc_sequence_value
		FROM	ap_lookup_codes				l1
				,fnd_document_sequences		fd
				,po_vendors					v
				,ap_accounting_events_all	ae
				,ap_ae_headers_all			aeh
				,ap_invoice_distributions_all	d
				,ap_invoices_all			i
				,ap_ae_lines_all			ael
		WHERE	l1.lookup_code	=	ael.ae_line_type_code
		AND		l1.lookup_type	=	'AE LINE TYPE'
		AND		d.invoice_distribution_id(+)	=	DECODE(ael.source_table, 'AP_INVOICE_DISTRIBUTIONS', ael.source_id, NULL)
		AND		ael.subledger_doc_sequence_id	=	fd.doc_sequence_id(+)
		AND		ael.third_party_id		=	v.vendor_id
		AND		i.invoice_id			=	ae.source_id
		AND		ae.accounting_event_id	=	aeh.accounting_event_id
		AND		ae.source_table			=	'AP_INVOICES'
		AND		aeh.ae_header_id		=	ael.ae_header_id
		AND		ael.ae_line_id			=	l_ael_line_id;
	END IF;
END get_ap_data;
----------------------------------------------------------------------------
PROCEDURE get_line_num
(i_jh_je_source				IN	VARCHAR2
,i_jc_je_category_name		IN	VARCHAR2
,i_reference_3				IN	VARCHAR2
,i_reference_10				IN	VARCHAR2
,i_ae_line_id				IN	NUMBER
,i_gl_sl_link_id			IN	NUMBER
,i_jh_currency_code			IN	VARCHAR2
,i_sob_currency_code		IN	VARCHAR2
,o_line_number				OUT	NUMBER)
IS
     l_amount                   NUMBER;
     l_acct_line_type           VARCHAR2(80);
     l_acct_line_type_name      VARCHAR2(80);
     l_trx_line_number          NUMBER;
     l_trx_line_type_name       VARCHAR2(100);
     l_currency_code            VARCHAR2(30);
     l_trx_number_displayed     VARCHAR2(80);
     l_entered_dr               NUMBER;
     l_entered_cr               NUMBER;
     l_accounted_dr             NUMBER;
     l_accounted_cr             NUMBER;
     l_trx_hdr_id               NUMBER;

     l_ael_id                   NUMBER;
     l_accounting_line_number   NUMBER;
     l_acct_line_type_code      VARCHAR2(80);
     l_source_tabe              VARCHAR2(150);
     l_source_id                NUMBER;
     l_third_party_id           NUMBER;
     l_doc_sequence_id          NUMBER;
     l_doc_sequence_value       VARCHAR2(240);
     l_doc_sequence_name        VARCHAR2(240);
     l_org_id                   NUMBER;
     l_third_party_name         VARCHAR2(240);
     l_je_header_id             NUMBER;
     l_je_line_num              NUMBER;
     l_Set_of_books_id          NUMBER;
     l_trx_line_type            VARCHAR2(150);

BEGIN
	o_line_number	:=	NULL;
	IF i_jh_je_source IN ('Receivables', 'Payables') THEN
		IF i_jh_je_source='Receivables' AND i_reference_3 IS NOT NULL THEN
			BEGIN
				GL_GET_AR_DATA.GET_AR_DATA
				(i_reference_3,
				i_reference_10,
				i_jc_je_category_name,
				l_acct_line_type ,
				l_acct_line_type_name,
				l_trx_line_number  ,
				l_trx_line_type_name ,
				l_currency_code  ,
				l_trx_number_displayed  ,
				l_entered_dr    ,
				l_entered_cr   ,
				l_accounted_dr  ,
				l_accounted_cr ,
				l_trx_hdr_id
				);
			EXCEPTION
				WHEN no_data_found THEN
					write_log('Error while get_line_num : ' || gl_message.get_message('SHRD0090','Y','ROUTINE','GL_GET_AR_DATA.GET_AR_DATA'));
				WHEN too_many_rows THEN
					write_log('Error while get_line_num : ' || gl_message.get_message('GL_PLL_ROUTINE_ERROR','Y','ROUTINE','GL_GET_AR_DATA.GET_AR_DATA'));
					RETURN;
			END;
		ELSIF i_jh_je_source='Payables' AND i_ae_line_id IS NOT NULL AND i_gl_sl_link_id IS NOT NULL THEN
			BEGIN
				GET_AP_DATA(
					i_jh_je_source,
					i_jc_je_category_name,
					i_ae_line_id,
					l_acct_line_type_name,
					l_trx_line_number,
					l_currency_code,
					l_trx_number_displayed,
					l_entered_dr,
					l_entered_cr,
					l_accounted_dr,
					l_accounted_cr,
					l_trx_hdr_id,
					l_third_party_name,
					l_doc_sequence_name,
					l_doc_sequence_value );

			EXCEPTION
				WHEN no_data_found THEN
					write_log('Error while get_line_num : ' || gl_message.get_message('SHRD0090','Y','ROUTINE','GL_GET_DRLDN_DATA.GET_AP_DATA'));
				WHEN too_many_rows THEN
					write_log('Error while get_line_num : ' || gl_message.get_message('GL_PLL_ROUTINE_ERROR','Y','ROUTINE','GL_GET_DRLDN_DATA.GET_AP_DATA'));
					RETURN;
			END;
		END IF;

/*		IF i_jh_currency_code = i_sob_currency_code THEN
			IF l_accounted_dr IS NOT NULL THEN
				l_amount	:=	l_accounted_dr;
			ELSIF l_accounted_cr IS NOT NULL THEN
				l_amount	:=	-l_accounted_cr;
			END IF;
		ELSE
			IF l_entered_dr IS NOT NULL THEN
				l_amount	:=	l_entered_dr;
			ELSIF l_entered_cr IS NOT NULL THEN
				l_amount	:=	-l_entered_cr;
			END IF;
		END IF;

		IF l_amount IS NULL THEN
			l_amount	:=	0;
		END IF;*/

		IF l_trx_line_number IS NULL THEN
			l_trx_line_number	:=	0;
		END IF;

/*		IF i_jh_je_source = 'Receivables' THEN
			l_return_value :=
				substr(rpad(to_char(l_trx_line_number),15),1,15)||
				substr(rpad(to_char(l_amount),38),1,38)||
				substr(rpad(l_acct_line_type_name,320),1,80)||
				substr(rpad(l_currency_code,60),1,15);
		ELSIF i_jh_je_source = 'Payables' THEN
			l_return_value :=
				substr(rpad(to_char(l_trx_line_number),15),1,15)||
				substr(rpad(to_char(l_amount),38),1,38)||
				substr(rpad(l_acct_line_type_name,320),1,80)||
				substr(rpad(l_currency_code,60),1,15)||
				substr(rpad(l_third_party_name,400),1,100)||
				substr(rpad(l_doc_sequence_name,400),1,100)||
				substr(rpad(l_doc_sequence_value,400),1,100);
		END IF;
	ELSE
		l_return_value :=
			substr(rpad(to_char(0),15),1,15)||
			substr(rpad(to_char(0),38),1,38)||
			substr(rpad(l_acct_line_type_name,320),1,80)||
			substr(rpad(l_currency_code,60),1,15);*/
	END IF; 

	o_line_number	:=	l_trx_line_number;
END get_line_num;
----------------------------------------------------------------------------
PROCEDURE get_pr_po_info
(i_jh_je_source				IN	VARCHAR2
,i_jc_je_category_name		IN	VARCHAR2
,i_org_id					IN	NUMBER
,i_ir_je_header_id			IN	NUMBER
,i_ir_je_line_num			IN	NUMBER
,i_jh_currency_code			IN	VARCHAR2
,i_sob_currency_code		IN	VARCHAR2
,i_actual_flag				IN	VARCHAR2
,o_po_number				OUT	VARCHAR2
,o_po_line_no				OUT	NUMBER
,o_po_line_distribution_no	OUT	NUMBER
,o_po_create_date			OUT	DATE
,o_po_encumber_date			OUT	DATE
,o_po_distribution_amount	OUT	NUMBER
,o_pr_number				OUT	VARCHAR2
,o_pr_line_no				OUT	NUMBER
,o_pr_line_distribution_no	OUT	NUMBER
,o_pr_create_date			OUT	DATE
,o_pr_gl_date				OUT	DATE
,o_pr_distribution_amount	OUT	NUMBER
,o_remark					OUT	VARCHAR2
,o_pr_header_description	OUT	VARCHAR2
,o_pr_line_description		OUT	VARCHAR2
,o_po_header_description	OUT	VARCHAR2
,o_po_line_description		OUT	VARCHAR2)
IS
	-- matching flag
	v_matching_flag VARCHAR2(1) := 'Y';
	v_max_len_description NUMBER := 500;
BEGIN
	-- reset value of placeholder item;
/*	o_po_number := '';
	o_po_line_no := '';
	o_po_line_distribution_no := '';
	o_po_create_date_txt := '';
	o_po_encumber_date_txt := '';
	o_po_distribution_amount := '';
	
	o_pr_number := '';
	o_pr_line_no := '';
	o_pr_line_distribution_no := '';
	o_pr_create_date_txt := '';
	o_pr_gl_date_txt := '';
	o_pr_distribution_amount := '';
	
	o_remark := '';
	
	o_pr_header_description := '';
	o_pr_line_description := '';
	o_po_header_description := '';
	o_po_line_description := '';*/
	--srw.message(1000, 'Process wkfind_po_pr_informationsFormula');
	--srw.message(1000, 'Find PR PO Information : C_ORG_ID = ' || :C_ORG_ID || ', Source = ' || :source || ', Category = ' || i_jc_je_category_name || ', je_header_id = ' || :je_header_id || ', je_line_num = ' || :je_line_num);
	
	IF i_jh_je_source = 'Payables' THEN
		IF (i_jc_je_category_name = 'Purchase Invoices') THEN
			dbms_application_info.set_client_info(i_org_id);
			
			IF i_actual_flag = 'E' THEN
				-- user view := 'XLA_AP_INV_ENC_GL_V' for find informations
				BEGIN
					SELECT DISTINCT
						poh.segment1				po_number
						,pol.line_num				po_line_num
						,pod.distribution_num		po_line_distribution_num
						,poh.creation_date			po_date
						,pod.gl_encumbered_date		po_gl_encumbered_date
						,DECODE(i_jh_currency_code, i_sob_currency_code, NVL(pod.rate,1) * pod.quantity_ordered * pol.unit_price, pod.quantity_ordered * pol.unit_price)	po_distribution_amount

						,prh.segment1				pr_number
						,prl.line_num				pr_line_num
						,prd.distribution_num		pr_line_distribution_num
						,prh.creation_date			pr_create_date
						,prd.gl_encumbered_date		pr_gl_encumbered_date
						,DECODE(i_jh_currency_code, i_sob_currency_code, NVL(prl.unit_price,0) * NVL(prd.req_line_quantity,0) ,prl.currency_unit_price * NVL(prd.req_line_quantity,0))	pr_distribution_amount

						,DECODE(aid.rcv_transaction_id ,NULL, DECODE(aid.po_distribution_id, NULL,'Direct Invoice','Match PO'),'Match Receive') invoice_match_option
					
						,SUBSTR(poh.comments, 1, v_max_len_description)			po_header_description
						,SUBSTR(pol.item_description, 1, v_max_len_description)	po_line_description
						,SUBSTR(prh.description, 1, v_max_len_description)		pr_header_description
						,SUBSTR(NVL(prl.justification, prh.note_to_authorizer), 1, v_max_len_description)	pr_line_description
					INTO o_po_number
						,o_po_line_no
						,o_po_line_distribution_no
						,o_po_create_date
						,o_po_encumber_date
						,o_po_distribution_amount
						
						,o_pr_number 
						,o_pr_line_no 
						,o_pr_line_distribution_no 
						,o_pr_create_date
						,o_pr_gl_date
						,o_pr_distribution_amount
						
						,o_remark
						
						,o_po_header_description
						,o_po_line_description
						,o_pr_header_description
						,o_pr_line_description
					FROM xla_ap_inv_enc_gl_v xla
						,ap_invoice_distributions_all aid
						,po_distributions_all pord
						,po_distributions_all pod
						,po_lines_all pol
						,po_headers_all poh
						
						,po_req_distributions_all prd
						,po_requisition_lines_all prl
						,po_requisition_headers_all prh
					WHERE 1 = 1
					AND xla.je_header_id		=	i_ir_je_header_id	-- 840125
					AND xla.je_line_num			=	i_ir_je_line_num	-- 9
					AND xla.trx_number_displayed IS NOT NULL
					AND xla.trx_line_number IS NOT NULL
					AND xla.trx_hdr_id			=	aid.invoice_id
					AND xla.trx_line_number		=	aid.distribution_line_number
					AND aid.po_distribution_id	=	pord.po_distribution_id
					AND pod.po_distribution_id	=	DECODE(pord.distribution_type
														,'STANDARD',pord.po_distribution_id
														,'BLANKET',pord.po_distribution_id
														,pord.source_distribution_id)
					AND pod.po_line_id			=	pol.po_line_id
					AND pol.po_header_id		=	poh.po_header_id
					AND pod.req_distribution_id	=	prd.distribution_id
					AND prd.requisition_line_id	=	prl.requisition_line_id
					AND prl.requisition_header_id	=	prh.requisition_header_id;

	
				EXCEPTION
					WHEN TOO_MANY_ROWS THEN
						NULL;
						--srw.message(1000,i_jc_je_category_name || '(XLA_AP_INV_ENC_GL_V) :: je_header_id=' ||:je_header_id || ', je_line_num='|| :je_line_num || ' :: TOO_MANY_ROWS');
					WHEN NO_DATA_FOUND THEN
						--srw.message(1000,i_jc_je_category_name || '(XLA_AP_INV_ENC_GL_V) :: je_header_id=' ||:je_header_id || ', je_line_num='|| :je_line_num || ' :: NO_DATA_FOUND');
						-- AP@BAS on 03-APR-2012
						-- Find from ap invoice (Expense Report)
						v_matching_flag := 'N';
						
						DECLARE
							v_invoice_id NUMBER;
							v_dist_code_combination_id NUMBER;
							v_inv_dist_description VARCHAR2(250);
							v_inv_dist_start_expense_date DATE;
							v_inv_dist_amount NUMBER;
						BEGIN
							SELECT 'Expense Report'
								,ai.description
								,NVL(aid.justification, aid.description)
								,aid.invoice_id
								,aid.dist_code_combination_id
								,aid.description
								,aid.start_expense_date
								,aid.amount
							INTO
								o_remark
								,o_pr_header_description
								,o_pr_line_description
								,v_invoice_id
								,v_dist_code_combination_id
								,v_inv_dist_description
								,v_inv_dist_start_expense_date
								,v_inv_dist_amount
							FROM xla_ap_inv_ael_gl_v xla
								,ap_invoice_distributions_all aid
								,ap_invoices_all ai
							WHERE xla.trx_hdr_id			=	aid.invoice_id
							AND xla.trx_line_number			=	aid.distribution_line_number
							AND aid.invoice_id				=	ai.invoice_id
							AND ai.invoice_type_lookup_code	=	'EXPENSE REPORT'
							AND xla.je_header_id			=	i_ir_je_header_id	-- 133141
							AND xla.je_line_num				=	i_ir_je_line_num	-- 399
							;
								
							v_matching_flag := 'E';
						EXCEPTION
							WHEN OTHERS THEN
								NULL;
						END;
					WHEN OTHERS THEN
						--srw.message(1000,i_jc_je_category_name || '(XLA_AP_INV_ENC_GL_V) :: je_header_id=' ||:je_header_id || ', je_line_num='|| :je_line_num || ' :: others');
						NULL;
				END;
			ELSE
				-- user view := 'XLA_AP_INV_AEL_GL_V' for find informations
				BEGIN
					SELECT DISTINCT
						poh.segment1				po_number
						,pol.line_num				po_line_num
						,pod.distribution_num		po_line_distribution_num
						,poh.creation_date			po_date
						,pod.gl_encumbered_date		po_gl_encumbered_date
						,DECODE(i_jh_currency_code, i_sob_currency_code, NVL(pod.rate,1) * pod.quantity_ordered * pol.unit_price, pod.quantity_ordered * pol.unit_price) po_distribution_amount
						
						,prh.segment1				pr_number
						,prl.line_num				pr_line_num
						,prd.distribution_num		pr_line_distribution_num
						,prh.creation_date			pr_create_date
						,prd.gl_encumbered_date		pr_gl_encumbered_date
						,DECODE(i_jh_currency_code, i_sob_currency_code, NVL(prl.unit_price,0) * NVL(prd.req_line_quantity,0) ,prl.currency_unit_price * NVL(prd.req_line_quantity,0)) pr_distribution_amount
						
						,DECODE(aid.rcv_transaction_id ,NULL,DECODE(aid.po_distribution_id, NULL,'Direct Invoice','Match PO'),'Match Receive') invoice_match_option
						
						,SUBSTR(poh.comments, 1, v_max_len_description)			po_header_description
						,SUBSTR(pol.item_description, 1, v_max_len_description)	po_line_description
						,SUBSTR(prh.description, 1, v_max_len_description)		pr_header_description
						,SUBSTR(NVL(prl.justification, prh.note_to_authorizer), 1, v_max_len_description)	pr_line_description
					INTO
						o_po_number
						,o_po_line_no
						,o_po_line_distribution_no
						,o_po_create_date
						,o_po_encumber_date
						,o_po_distribution_amount
						
						,o_pr_number 
						,o_pr_line_no 
						,o_pr_line_distribution_no 
						,o_pr_create_date
						,o_pr_gl_date
						,o_pr_distribution_amount
						
						,o_remark
						
						,o_po_header_description
						,o_po_line_description
						,o_pr_header_description
						,o_pr_line_description
					FROM xla_ap_inv_ael_gl_v xla
						,ap_invoice_distributions_all aid
						,po_distributions_all pord
						,po_distributions_all pod
						,po_lines_all pol
						,po_headers_all poh
						
						,po_req_distributions_all prd
						,po_requisition_lines_all prl
						,po_requisition_headers_all prh
					WHERE 1 = 1
					AND xla.je_header_id	=	i_ir_je_header_id	-- 840125
					AND xla.je_line_num		=	i_ir_je_line_num	-- 9
					AND xla.trx_number_displayed IS NOT NULL
					AND xla.trx_line_number IS NOT NULL
					AND xla.trx_hdr_id				=	aid.invoice_id
					AND xla.trx_line_number			=	aid.distribution_line_number
					AND aid.po_distribution_id		=	pord.po_distribution_id
					AND pod.po_distribution_id		=	DECODE(pord.distribution_type
															,'STANDARD',pord.po_distribution_id
															,'BLANKET',pord.po_distribution_id
															,pord.source_distribution_id)
					AND pod.po_line_id				=	pol.po_line_id
					AND pol.po_header_id			=	poh.po_header_id
					AND pod.req_distribution_id		=	prd.distribution_id
					AND prd.requisition_line_id		=	prl.requisition_line_id
					AND prl.requisition_header_id	=	prh.requisition_header_id;
				EXCEPTION
					WHEN TOO_MANY_ROWS THEN
						NULL;
						--srw.message(1000,i_jc_je_category_name || '(XLA_AP_INV_AEL_GL_V) :: je_header_id=' ||:je_header_id || ', je_line_num='|| :je_line_num || ' :: TOO_MANY_ROWS');
					WHEN NO_DATA_FOUND THEN
						--srw.message(1000,'(XLA_AP_INV_AEL_GL_V) no data found');
						v_matching_flag := 'N';
						-- AP@BAS on 03-APR-2012
						-- Find from ap invoice (Expense Report)
						DECLARE
							v_invoice_id					NUMBER;
							v_dist_code_combination_id		NUMBER;
							v_inv_dist_description			VARCHAR2(250);
							v_inv_dist_start_expense_date	DATE;
							v_inv_dist_amount				NUMBER;
						BEGIN
							SELECT 'Expense Report'
								,ai.description
								,NVL(aid.justification, aid.description)
								,aid.invoice_id
								,aid.dist_code_combination_id
								,aid.description
								,aid.start_expense_date
								,aid.amount
							INTO o_remark
								,o_pr_header_description
								,o_pr_line_description
								,v_invoice_id
								,v_dist_code_combination_id
								,v_inv_dist_description
								,v_inv_dist_start_expense_date
								,v_inv_dist_amount
							FROM xla_ap_inv_ael_gl_v xla
								,ap_invoice_distributions_all aid
								,ap_invoices_all ai
							WHERE xla.trx_hdr_id			=	aid.invoice_id
							AND xla.trx_line_number			=	aid.distribution_line_number
							AND aid.invoice_id				=	ai.invoice_id
							AND ai.invoice_type_lookup_code	=	'EXPENSE REPORT'
							AND xla.je_header_id			=	i_ir_je_header_id	-- 133141
							AND xla.je_line_num				=	i_ir_je_line_num	-- 399
							;

							v_matching_flag		:=	'E';
						EXCEPTION
							WHEN OTHERS THEN
								NULL;
						END;
					WHEN OTHERS THEN				
						NULL;
				END;
			END IF;
		ELSE
			NULL;
		END IF;

		--srw.message(1000,'v_matching_flag = ' || v_matching_flag);
		-- find matching information from other distribution line
		IF v_matching_flag = 'N' THEN
			BEGIN
				o_remark	:=	'Direct Invoice';
				SELECT
				    NVL(MAX(DECODE(aid.rcv_transaction_id ,NULL,
				    DECODE(aid.po_distribution_id, NULL,'Direct Invoice','Direct Invoice with Match PO'),'Direct Invoice with Match Receive')
				    ),'Direct Invoice')		invoice_match_option
				INTO o_remark
				FROM xla_ap_inv_ael_gl_v xla
					,ap_invoice_distributions_all aid
				WHERE 1 = 1
				AND xla.je_header_id	=	i_ir_je_header_id	-- 840125
				AND xla.je_line_num		=	i_ir_je_line_num	-- 218
				AND xla.trx_number_displayed IS NOT NULL
				AND xla.trx_line_number IS NOT NULL
				AND xla.trx_hdr_id		=	aid.invoice_id
				AND aid.line_type_lookup_code	=	'ITEM'
				AND aid.po_distribution_id IS NOT NULL
				AND xla.trx_line_number	<>	aid.distribution_line_number;
			EXCEPTION
				WHEN NO_DATA_FOUND THEN
					--srw.message(1000, 'Direct Invoice : No Data Found!');
					o_remark := 'Direct Invoice';
				WHEN OTHERS THEN
					--srw.message(1000, 'Direct Invoice : exception others!');
					--srw.message(1000, 'SQLERRM : ' || sqlerrm);
					NULL;
			END;
		END IF;
		
	ELSIF i_jh_je_source = 'Purchasing' THEN
		IF i_jc_je_category_name = 'Receiving' THEN -- world.trx_group = TRX
			-- user view XLA_PO_AEL_GL_V
			-- reference5 = receiving transction_id
			-- reference3 = po_distribution_id
			-- reference4 = po_number
			-- reference2 = po_header_id
			BEGIN
				SELECT DISTINCT
					poh.segment1			po_number
					,pol.line_num			po_line_num
					,pod.distribution_num	po_line_distribution_num
					,poh.creation_date		po_date
					,pod.gl_encumbered_date	po_gl_encumbered_date
					,DECODE(i_jh_currency_code, i_sob_currency_code, NVL(pod.rate,1) * pod.quantity_ordered * pol.unit_price, pod.quantity_ordered * pol.unit_price) po_distribution_amount
						
					,prh.segment1			pr_number
					,prl.line_num			pr_line_num
					,prd.distribution_num	pr_line_distribution_num
					,prh.creation_date		pr_create_date
					,prd.gl_encumbered_date	pr_gl_encumbered_date
					,DECODE(i_jh_currency_code, i_sob_currency_code, NVL(prl.unit_price,0) * NVL(prd.req_line_quantity,0) ,prl.currency_unit_price * NVL(prd.req_line_quantity,0)) pr_distribution_amount
						
					,'' invoice_match_option
						
					/*
					,poh.comments po_header_description
					,pol.item_description po_line_description
					,prh.description pr_header_description
					,NVL(prl.justification, prh.note_to_authorizer) pr_line_description
					*/
						
					,SUBSTR(poh.comments, 1, v_max_len_description)			po_header_description
					,SUBSTR(pol.item_description, 1, v_max_len_description)	po_line_description
					,SUBSTR(prh.description, 1, v_max_len_description)		pr_header_description
					,SUBSTR(NVL(prl.justification, prh.note_to_authorizer), 1, v_max_len_description)	pr_line_description
				INTO
					o_po_number
					,o_po_line_no
					,o_po_line_distribution_no
					,o_po_create_date
					,o_po_encumber_date
					,o_po_distribution_amount
						
					,o_pr_number 
					,o_pr_line_no 
					,o_pr_line_distribution_no 
					,o_pr_create_date
					,o_pr_gl_date
					,o_pr_distribution_amount
						
					,o_remark
						
					,o_po_header_description
					,o_po_line_description
					,o_pr_header_description
					,o_pr_line_description
				FROM xla_po_ael_gl_v xla
					,rcv_transactions rt
					,po_distributions_all pord
					,po_distributions_all pod
					,po_lines_all pol
					,po_headers_all poh
						
					,po_req_distributions_all prd
					,po_requisition_lines_all prl
					,po_requisition_headers_all prh
				WHERE 1 = 1
				AND xla.je_header_id		=	i_ir_je_header_id	-- 677694
				AND xla.je_line_num			=	i_ir_je_line_num	-- 1
				AND xla.trx_hdr_id			=	rt.transaction_id
				AND rt.po_distribution_id	=	pord.po_distribution_id
				AND pod.po_distribution_id	=	DECODE(pord.distribution_type
													,'STANDARD',pord.po_distribution_id
													,'BLANKET',pord.po_distribution_id
													,pord.source_distribution_id)
				AND pod.po_line_id			=	pol.po_line_id
				AND pol.po_header_id		=	poh.po_header_id
				AND pod.req_distribution_id	=	prd.distribution_id
				AND prd.requisition_line_id	=	prl.requisition_line_id
				AND prl.requisition_header_id	=	prh.requisition_header_id;
			EXCEPTION
			  WHEN TOO_MANY_ROWS THEN
			  	NULL;
			  WHEN NO_DATA_FOUND THEN
			  	NULL;
			  	--o_po_number := :reference_4;
			END;
		ELSIF i_jc_je_category_name='Requisitions' THEN -- world.trx_group = REQ
			-- user view XLA_REQ_ENC_AEL_GL_V if reference1 and reference2 is null
			NULL;
		ELSIF i_jc_je_category_name='Purchases' THEN -- world.trx_group = PUR
			-- user view XLA_PO_ENC_AEL_GL_V if reference1 and reference2 is null
			NULL;
		ELSE
			-- user view XLA_PO_AEL_GL_V
			NULL;
		END IF;
	END IF;

	-- rounding amount
	o_pr_distribution_amount := round(o_pr_distribution_amount, 2);
	o_po_distribution_amount := round(o_po_distribution_amount, 2);
	
EXCEPTION
	WHEN OTHERS THEN
		write_log('get_pr_po_info : ' || SUBSTR(SQLERRM,1 , 240));
END get_pr_po_info;
----------------------------------------------------------------------------
PROCEDURE gen_opex
(i_program_name			IN	VARCHAR2
,i_report_name			IN	VARCHAR2
,i_company_code_from	IN	VARCHAR2
,i_company_code_to		IN	VARCHAR2
,i_account_code_from	IN	VARCHAR2
,i_account_code_to		IN	VARCHAR2
,i_rc_code_from			IN	VARCHAR2
,i_rc_code_to			IN	VARCHAR2
,i_period_from			IN	VARCHAR2
,i_period_to			IN	VARCHAR2
,o_status				OUT	VARCHAR2)
IS
	v_date_from			DATE;
	v_date_to			DATE;

	CURSOR c_trx IS
-- 27-Sep-18	TM.				1.02		Prevent duplicate lookup value in TAC_GL_ACCOUNT_OPEX lookup type
--		SELECT	gcc.segment1				company_code
		SELECT DISTINCT	gcc.segment1		company_code
-- End 27-Sep-18	TM.				1.02		Prevent duplicate lookup value in TAC_GL_ACCOUNT_OPEX lookup type
				,lv_books.attribute3		company_code_anaplan
				,gcc.segment11				account_code
				,gcc.segment10				rc_code
				,gcc.segment13				project_internal_code
-- project_code
				,CASE WHEN jh.je_source = 'Purchasing' AND jc.je_category_name = 'Receiving' THEN
					(SELECT	MAX(pp.segment1)
					FROM	po_distributions_all	pod
							,po_headers_all			poh
--							,po_line_locations_all	poll
							,po_lines_all			pol
							,pa_projects_all		pp
					WHERE	pod.po_header_id		=	poh.po_header_id
					AND		pol.po_header_id		=	poh.po_header_id
					AND		pol.po_line_id			=	pod.po_line_id
/*					AND		poll.po_header_id		=	poh.po_header_id
					AND		poll.po_line_id			=	pol.po_line_id
					AND		pod.line_location_id	=	poll.line_location_id	*/
					AND		pod.po_distribution_id	=	ir.reference_3
					AND		poh.segment1			=	ir.reference_4
					AND		pp.project_id			=	NVL(pod.project_id, pol.project_id))
				WHEN jh.je_source = 'Payables' AND jc.je_category_name = 'Purchase Invoices' AND ael.ae_line_id IS NOT NULL AND ir.gl_sl_link_id IS NOT NULL THEN
					(SELECT	MAX(pp.segment1)
					FROM	ap_invoice_distributions_all	aid
							,pa_projects_all				pp
					WHERE	aid.invoice_id					=	TO_NUMBER(ir.reference_2)
					AND		aid.distribution_line_number	=	NVL(TO_NUMBER(ir.reference_3),aid.distribution_line_number)
					AND		pp.project_id					=	aid.project_id)
				ELSE
					NULL
				END			project_code
				,jl.effective_date			acc_date
				,jh.je_source				je_source
				,jc.user_je_category_name	category
				,ir.je_line_num				je_line_num
-- doc_number
				,CASE WHEN jh.je_source = 'Receivables' AND jc.je_category_name = 'Trade Receipts' THEN
					TO_CHAR(ir.subledger_doc_sequence_value)
				WHEN jh.je_source = 'Receivables' THEN
					ir.reference_4
				WHEN jh.je_source = 'Manual' THEN
					TO_CHAR(jh.doc_sequence_value)
				WHEN jh.je_source IN ('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '21', 'Consolidation', 'Recurring') THEN
					TO_CHAR(jh.doc_sequence_value)
				WHEN jh.je_source = 'Payables' AND ael.ae_line_id IS NOT NULL AND ir.gl_sl_link_id IS NOT NULL THEN 
					(SELECT	MAX(DECODE(ae.source_table,
								'AP_INVOICES',(SELECT TO_CHAR(doc_sequence_value) FROM ap_invoices_all WHERE invoice_id = ae.source_id),
								'AP_CHECKS'  ,(SELECT TO_CHAR(doc_sequence_value) FROM ap_checks_all   WHERE check_id   = ae.source_id))
							)
--					INTO sr_table, sr_id
					FROM	ap_ae_headers_all			aeh
							,ap_accounting_events_all	ae
					WHERE	aeh.ae_header_id		=	ael.ae_header_id
					AND		ae.accounting_event_id	=	aeh.accounting_event_id)
				WHEN jh.je_source = 'Payables' AND ir.reference_6 = 'AP Invoices' THEN
					NVL(ir.reference_5, ' ')
				WHEN jh.je_source = 'Payables' THEN
					ir.reference_4
				WHEN jh.je_source = 'Purchasing' AND jc.je_category_name = 'Receiving' AND ir.reference_5 IS NOT NULL THEN
					(SELECT	NVL(MAX(rsh.receipt_num), '***Unknown***')
					FROM	rcv_transactions rt, rcv_shipment_headers rsh
					WHERE	rsh.shipment_header_id	=	rt.shipment_header_id
					AND		rt.transaction_id		=	TO_NUMBER(RTRIM(LTRIM(ir.reference_5))))
				WHEN jh.je_source = 'Purchasing' AND jc.je_category_name = 'Purchases' THEN
					ir.reference_4
				WHEN jh.je_source = 'Purchasing' THEN
					ir.reference_5
				ELSE
					NULL
				END			doc_number
-- reference_no
				,CASE WHEN jh.je_source = 'Receivables' AND jc.je_category_name = 'Trade Receipts' THEN
					ir.reference_5
				WHEN jh.je_source = 'Receivables' AND jc.je_category_name IN ('Sales Invoices','Credit Memos','Debit Memos') THEN
					(SELECT	MAX(ct.ct_reference)
					FROM	ra_customer_trx_all ct
							,ra_cust_trx_types_all ctt
							,hz_cust_accounts rac_bill
					WHERE	ct.cust_trx_type_id				=	ctt.cust_trx_type_id
					AND		ct.bill_to_customer_id			=	rac_bill.cust_account_id
					AND		TO_CHAR(ct.doc_sequence_value)	=	ir.reference_4
					AND		rac_bill.account_number			=	ir.reference_5
					AND		ctt.type	=	DECODE(jc.je_category_name, 'Sales Invoices', 'INV', 'Credit Memos', 'CM', 'Debit Memos', 'DM'))
				WHEN jh.je_source = 'Receivables' AND jc.je_category_name = 'Credit Memo Applications' THEN
					(SELECT	MAX(ct.ct_reference)
					FROM	ra_customer_trx_all ct
					WHERE	ct.trx_number			=	ir.reference_4
					AND		ct.bill_to_customer_id	=	ir.reference_7)
				WHEN jh.je_source = 'Receivables' AND jc.je_category_name = 'Adjustment' THEN
					(SELECT	MAX(ct.ct_reference)
					FROM	ra_customer_trx_all ct
							,ar_adjustments_all	adj
					WHERE	TO_CHAR(ct.doc_sequence_value)	=	ir.reference_4
					AND		ct.customer_trx_id		=	adj.customer_trx_id
					AND		adj.adjustment_id		=	TO_NUMBER(ir.reference_2)
					AND		adj.posting_control_id	=	TO_NUMBER(ir.reference_1)
					AND		adj.adjustment_number	=	ir.reference_5)
				WHEN jh.je_source = 'Payables' AND ael.ae_line_id IS NOT NULL AND ir.gl_sl_link_id IS NOT NULL AND jc.je_category_name = 'Payments' THEN 
					(SELECT	MAX(TO_CHAR(doc_sequence_value))
					FROM	ap_invoices_all		ai
					WHERE	ai.invoice_id	=	ir.reference_2)
				WHEN jh.je_source = 'Payables' THEN
					ir.reference_5
				WHEN jh.je_source = 'Purchasing' AND jc.je_category_name = 'Receiving' THEN
					ir.reference_4
				WHEN jh.je_source = 'Purchasing' AND jc.je_category_name = 'Purchases' AND ir.reference_4 IS NOT NULL THEN
					(SELECT	MAX(DECODE(pod.req_distribution_id
								,NULL,pod.req_header_reference_num
								,porh.segment1))
					FROM	po_headers_all			poh
							,po_lines_all			pol
							,po_line_locations_all	pll
							,po_releases_all		por
							,po_distributions_all	pod
							,po_req_distributions_all	pord
							,po_requisition_lines_all	porl
							,po_requisition_headers_all	porh
					WHERE	poh.po_header_id			=	pod.po_header_id
					AND		pod.po_line_id				=	pol.po_line_id(+)
					AND		pod.line_location_id		=	pll.line_location_id(+)
					AND		pod.po_release_id			=	por.po_release_id(+)
					AND		pod.req_distribution_id		=	pord.distribution_id(+)
					AND		pord.requisition_line_id	=	porl.requisition_line_id(+)
					AND		porl.requisition_header_id	=	porh.requisition_header_id(+)
					AND		poh.segment1				=	ir.reference_4)
				ELSE
					NULL
				END			reference_no
-- Vendor / Customer
				,CASE WHEN jh.je_source = 'Payables' AND ael.ae_line_id IS NOT NULL AND ir.gl_sl_link_id IS NOT NULL THEN 
					(SELECT	MAX(v.vendor_name)
					FROM	po_vendors v
					WHERE	v.vendor_id	=	ael.third_party_id)
				WHEN jh.je_source = 'Payables' THEN
					NVL(ir.reference_1, ' ')
				WHEN jh.je_source = 'Receivables' AND ir.reference_7 IS NULL THEN 
					NULL
				WHEN jh.je_source = 'Receivables' THEN
					(SELECT	MAX(c.customer_name)
					FROM	ra_customers c
					WHERE	c.customer_id	=	TO_NUMBER(ir.reference_7))
				WHEN jh.je_source = 'Purchasing' AND jc.je_category_name = 'Receiving' AND ir.reference_5 IS NOT NULL THEN
					(SELECT NVL(MAX(pov.vendor_name),'**Unknown**') supplier
					FROM	rcv_transactions rt, po_vendors pov
					WHERE	rt.vendor_id		=	pov.vendor_id
					AND		rt.transaction_id	=	TO_NUMBER(RTRIM(LTRIM(ir.reference_5))))
				ELSE
					NULL
				END			vendor_customer
-- PO Category
				,CASE WHEN jh.je_source = 'Purchasing' AND jc.je_category_name = 'Receiving' THEN
					(SELECT	MAX(mca.segment1 || '.' || mca.segment2 || '.' || mca.segment3 || '.' || mca.segment4) c_flex_cat
					FROM	mtl_categories			mca
							,po_distributions_all	pod
							,po_line_locations_all	poll
							,po_lines_all			pol
							,po_headers_all			poh
					WHERE	pol.category_id			=	mca.category_id
					AND		pol.po_header_id		=	poh.po_header_id
					AND		poll.po_header_id		=	poh.po_header_id
					AND		poll.po_line_id			=	pol.po_line_id
					AND		pod.po_header_id		=	poh.po_header_id
					AND		pod.line_location_id	=	poll.line_location_id
					AND		poh.segment1			=	ir.reference_4)
				WHEN jh.je_source = 'Purchasing' THEN
					NULL
				WHEN jh.je_source = 'Payables' AND ael.ae_line_id IS NOT NULL AND ir.gl_sl_link_id IS NOT NULL AND jc.je_category_name = 'Purchase Invoices' THEN 
					(SELECT	MAX(mca.segment1 || '.' || mca.segment2 || '.' || mca.segment3 || '.' || mca.segment4) c_flex_cat
					FROM	mtl_categories				mca
							,po_distributions_all		pod
							,po_line_locations_all		poll
							,po_lines_all				pol
							,po_headers_all				poh
							,ap_invoice_distributions_all	apid
					WHERE	pol.category_id			=	mca.category_id
					AND		pol.po_header_id		=	poh.po_header_id
					AND		poll.po_header_id		=	poh.po_header_id
					AND		poll.po_line_id			=	pol.po_line_id
					AND		pod.po_header_id		=	poh.po_header_id
					AND		pod.line_location_id	=	poll.line_location_id
					AND		pod.po_distribution_id	=	apid.po_distribution_id
					AND		apid.invoice_id			=	TO_NUMBER(ir.reference_2))
				WHEN jh.je_source = 'Payables' THEN
					NULL
				WHEN jh.je_source = 'Assets' AND jc.je_category_name = 'Addition' THEN 
					NULL
				WHEN jh.je_source = 'Assets' THEN 
					NULL
				ELSE
					NULL
				END			po_category
-- Journal Batch, Journal Name, Journal Line desc, Amount DR & CR
				,NVL(jb.name,'None')	journal_batch_name
				,jh.name				journal_name
				,REPLACE(REPLACE(SUBSTR(jl.description, 1, 240), chr(10), ''),',')	journal_line_desc
-- Edit 26-Sep-18	TM.				1.01		Fix amount field
/*				,DECODE(jh.currency_code, sob.currency_code, jl.accounted_dr, jl.entered_dr)	amount_dr
				,DECODE(jh.currency_code, sob.currency_code, jl.accounted_cr, jl.entered_cr)	amount_cr*/
				,DECODE(jh.currency_code, sob.currency_code, jl.entered_dr, jl.accounted_dr)	amount_dr
				,DECODE(jh.currency_code, sob.currency_code, jl.entered_cr, jl.accounted_cr)	amount_cr
-- End Edit 26-Sep-18	TM.				1.01		Fix amount field
-- PO Number, PO Create Date, PR Number, PR Create Date, Header Description, Justification
--				,CASE WHEN jh.je_source = 'Payables' AND jc.je_category_name = 'Purchase Invoices' AND jh.actual_flag = 'E' THEN 
				,jh.je_source			jh_je_source
				,jc.je_category_name	jc_je_category_name
				,jh.actual_flag			jh_actual_flag
				,jh.currency_code		jh_currency_code
				,sob.currency_code		sob_currency_code
-- Org ID
				,CASE WHEN jh.je_source = 'Receivables' AND jc.je_category_name = 'Trade Receipts' AND ir.reference_10 = 'AR_RECEIVABLE_APPLICATIONS' THEN
					(SELECT	MAX(org_id)
					FROM	ar_receivable_applications_all
					WHERE	cash_receipt_id				=	TO_NUMBER(SUBSTR(ir.reference_2,1,INSTR(ir.reference_2,'C')-1))
					AND		receivable_application_id	=	TO_NUMBER(SUBSTR(ir.reference_2,INSTR(ir.reference_2,'C')+1,LENGTH(ir.reference_2))))
				WHEN jh.je_source = 'Receivables' AND jc.je_category_name = 'Trade Receipts' AND ir.reference_10 = 'AR_CASH_RECEIPT_HISTORY' THEN
					(SELECT	MAX(org_id)
					FROM	ar_cash_receipt_history_all
					WHERE	cash_receipt_id				=	TO_NUMBER(SUBSTR(ir.reference_2,1,INSTR(ir.reference_2,'C')-1))
					AND		cash_receipt_history_id		=	TO_NUMBER(SUBSTR(ir.reference_2,INSTR(ir.reference_2,'C')+1,LENGTH(ir.reference_2))))
				WHEN jh.je_source = 'Receivables' AND jc.je_category_name = 'Credit Memo Applications' THEN
					(SELECT	MAX(org_id)
					FROM	ar_receivable_applications_all
					WHERE	receivable_application_id	=	TO_NUMBER(ir.reference_2))
				WHEN jh.je_source = 'Receivables' AND jc.je_category_name = 'Adjustment' THEN
					(SELECT	MAX(org_id)
					FROM	ar_adjustments_all
					WHERE	adjustment_id	=	TO_NUMBER(ir.reference_2))
				WHEN jh.je_source = 'Receivables' THEN
					(SELECT	MAX(org_id)
					FROM	ra_cust_trx_line_gl_dist_all
					WHERE	cust_trx_line_gl_dist_id	=	TO_NUMBER(ir.reference_3))
				WHEN jh.je_source = 'Payables' AND ael.ae_line_id IS NOT NULL AND ir.gl_sl_link_id IS NOT NULL THEN
					(SELECT MAX(DECODE(ae.source_table
--								,'AP_INVOICES', (SELECT MAX(org_id) FROM ap_invoices_all WHERE invoice_id = ae.source_id)
								,'AP_CHECKS', (SELECT MAX(org_id) FROM ap_checks_all WHERE check_id = ae.source_id)
								,(SELECT MAX(org_id) FROM ap_invoices_all WHERE invoice_id = ae.source_id))
							)
					FROM	ap_ae_headers_all			aeh
							,ap_accounting_events_all	ae
					WHERE	aeh.ae_header_id		=	ael.ae_header_id
					AND		ae.accounting_event_id	=	aeh.accounting_event_id)
				WHEN jh.je_source = 'Purchasing' AND ir.reference_5 IS NOT NULL THEN
					(SELECT	MAX(organization_id)
					FROM	rcv_transactions
					WHERE	transaction_id	=	TO_NUMBER(RTRIM(LTRIM(ir.reference_5))))
				ELSE
					NULL
				END			org_id

				,ael.ae_line_id			ael_ae_line_id
				,ir.gl_sl_link_id		ir_gl_sl_link_id
				
				,jh.doc_sequence_value	jh_doc_sequence_value
				,js.user_je_source_name	js_user_source
				,ir.je_header_id		ir_je_header_id
				,jh.je_batch_id			jh_je_batch_id
				,ds.name				doc_seq_name
				,jl.je_line_num			jl_je_line_num
				,jl.accounted_dr		jl_accounted_dr
				,jl.accounted_cr		jl_accounted_cr
				,jl.entered_dr			jl_entered_dr
				,jl.entered_cr			jl_entered_cr
				,jh.external_reference	jh_external_reference
				,jl.reference_2			jl_reference_2
				,jl.reference_3			jl_reference_3
				,jl.period_name			jl_period_name
				,ir.reference_1			ir_reference_1
				,ir.reference_2			ir_reference_2
				,ir.reference_3			ir_reference_3
				,ir.reference_4			ir_reference_4
				,ir.reference_5			ir_reference_5
				,ir.reference_6			ir_reference_6
				,ir.reference_7			ir_reference_7
				,ir.reference_8			ir_reference_8
				,ir.reference_9			ir_reference_9
				,ir.reference_10		ir_reference_10
				,ir.subledger_doc_sequence_id		ir_subledger_doc_seq_id
				,ir.subledger_doc_sequence_value	ir_subledger_doc_seq_value
				,jh.doc_sequence_id		jh_doc_sequence_id
				,ael.source_id			ael_source_id
		FROM	fnd_lookup_values		lv_books
				,fnd_lookup_values		lv_acct
				,gl_code_combinations	gcc
				,gl_sets_of_books		sob
				,gl_je_lines			jl
				,gl_je_headers			jh
				,gl_je_batches			jb
				,gl_je_sources			js
				,gl_je_categories		jc
				,gl_import_references	ir
				,ap_ae_lines_all		ael
				,fnd_document_sequences	ds
		WHERE	lv_books.lookup_type	=	'TAC_GL_ANAPLAN_COMPANY'
		AND		lv_books.enabled_flag	=	'Y'
		AND		v_date_to BETWEEN NVL(lv_books.start_date_active,v_date_to) AND NVL(lv_books.end_date_active,v_date_to)	-- 27-Sep-18
		AND		jl.set_of_books_id		=	TO_NUMBER(lv_books.attribute1)
		AND		jl.set_of_books_id		=	sob.set_of_books_id
		AND		gcc.segment1			=	lv_books.attribute2
		AND		gcc.summary_flag		=	'N'
		AND		gcc.code_combination_id	=	jl.code_combination_id
		AND		lv_acct.lookup_type		=	'TAC_GL_ACCOUNT_OPEX'
		AND		lv_acct.enabled_flag	=	'Y'
		AND		v_date_to BETWEEN NVL(lv_acct.start_date_active,v_date_to) AND NVL(lv_acct.end_date_active,v_date_to)	-- 27-Sep-18
		AND		(lv_acct.attribute1 || lv_acct.attribute2 IS NULL OR gcc.segment11 BETWEEN lv_acct.attribute1 AND NVL(lv_acct.attribute2,lv_acct.attribute1))	-- Account
		AND		(lv_acct.attribute3 IS NULL OR gcc.account_type = lv_acct.attribute3)	-- Acct Type
		AND		gcc.segment1 BETWEEN NVL(i_company_code_from,' ') AND NVL(i_company_code_to,CHR(255))	-- Company Code
		AND		gcc.segment11 BETWEEN NVL(i_account_code_from,' ') AND NVL(i_account_code_to,CHR(255))	-- Account Code
		AND		gcc.segment10 BETWEEN NVL(i_rc_code_from,' ') AND NVL(i_rc_code_to,CHR(255))	-- RC Code
		AND		jl.effective_date BETWEEN v_date_from AND v_date_to
		AND		jl.status || ''			=	'P'
		AND		jh.status				=	'P'
		AND		jh.actual_flag			=	'A'
		AND		jh.je_header_id			=	jl.je_header_id + 0
		AND		jh.currency_code		!=	'STAT'	-- Non-STAT currency
--		AND		jh.je_source || '' LIKE DECODE(:p_source, '', '%', :p_source)
--		AND		jh.je_category || '' LIKE DECODE(:p_category, '', '%', :p_category)
		AND		jb.je_batch_id			=	jh.je_batch_id + 0
		AND		jb.average_journal_flag	=	'N'
		AND		js.je_source_name		=	jh.je_source
		AND		jc.je_category_name		=	jh.je_category
		AND		ir.je_header_id(+)		=	jl.je_header_id
		AND		ir.je_line_num(+)		=	jl.je_line_num
		AND		(ir.rowid IS NULL OR jh.je_source IN ('Payables', 'Receivables') OR
				ir.rowid IN (SELECT	MAX(ir2.rowid)
							FROM	gl_import_references ir2
							WHERE	ir2.je_header_id = jl.je_header_id
							AND		ir2.je_line_num = jl.je_line_num))
		AND		ael.gl_sl_link_id(+)	=	ir.gl_sl_link_id
		AND		ds.doc_sequence_id(+)	=	ir.subledger_doc_sequence_id
		AND		jl.effective_date IS NOT NULL
		ORDER BY jh.name
				,jc.je_category_name
				,jl.effective_date
				,ir.je_line_num
				,ds.name;

	v_po_number					VARCHAR2(4000);
	v_po_line_no				NUMBER;
	v_po_line_distribution_no	NUMBER;
	v_po_create_date			DATE;
	v_po_encumber_date			DATE;
	v_po_distribution_amount	NUMBER;
	v_pr_number					VARCHAR2(4000);
	v_pr_line_no				NUMBER;
	v_pr_line_distribution_no	NUMBER;
	v_pr_create_date			DATE;
	v_pr_gl_date				DATE;
	v_pr_distribution_amount	NUMBER;
	v_remark					VARCHAR2(4000);
	v_pr_header_description		VARCHAR2(4000);
	v_pr_line_description		VARCHAR2(4000);
	v_po_header_description		VARCHAR2(4000);
	v_po_line_description		VARCHAR2(4000);

	v_line_number	NUMBER;

	t_text			tac_text_output_util.varchar_tabtyp;
	t_format		tac_text_output_util.number_tabtyp;

BEGIN
	o_status	:=	G_NO_ERROR;

	write_log('Log in gen_opex subprogram :');

	SELECT	MAX(DECODE(p.period_name,i_period_from,p.start_date))	date_from
			,MAX(DECODE(p.period_name,i_period_to,p.end_date))		date_to
	INTO	v_date_from, v_date_to
	FROM	gl_periods p
	WHERE	period_set_name				=	'Accounting'
	AND		p.adjustment_period_flag	=	'N'
	AND		period_name IN (i_period_from, i_period_to);

	write_log('Date From (derived from i_period_from) : '||v_date_from);
	write_log('Date To   (derived from i_period_to)   : '||v_date_to);

	-- Format Report Headings
	t_format.DELETE;
	t_text.DELETE;
	FOR i IN 1..1
	LOOP
		t_format(i)	:=	0;
		t_text(i)	:=	NULL;
	END LOOP;
	tac_text_output_util.set_format
		(it_col_len		=>	t_format
		,o_status		=>	o_status
		,i_clear_data	=>	'Y');
	IF o_status <> tac_text_output_util.NO_ERROR THEN
		RAISE abnormal_end;
	END IF;

	-- Write Report Headings
	t_text(1)	:=	'TAC : BPT OPEX Report';
	tac_text_output_util.add_txt(it_message	=>	t_text, i_separator => DELIMITER_CTRL);

	t_text(1)	:=	'';
	tac_text_output_util.add_txt(it_message	=>	t_text, i_separator => DELIMITER_CTRL);

	-- Format Report Parameters
	t_format.DELETE;
	t_text.DELETE;
	FOR i IN 1..3
	LOOP
		t_format(i)	:=	0;
		t_text(i)	:=	NULL;
	END LOOP;
	tac_text_output_util.set_format
		(it_col_len		=>	t_format
		,o_status		=>	o_status
		,i_clear_data	=>	'N');
	IF o_status <> tac_text_output_util.NO_ERROR THEN
		RAISE abnormal_end;
	END IF;

	t_text(1)	:=	'Request ID:';
	t_text(2)	:=	g_request_id;
	tac_text_output_util.add_txt(it_message	=>	t_text, i_separator => DELIMITER_CTRL);

	-- Write Report Parameters
	t_text(1)	:=	'Parameter Name';
	t_text(2)	:=	'From';
	t_text(3)	:=	'To';
	tac_text_output_util.add_txt(it_message	=>	t_text, i_separator => DELIMITER_CTRL);

	t_text(1)	:=	'Company Code:';
	t_text(2)	:=	NVL(i_company_code_from,'');
	t_text(3)	:=	NVL(i_company_code_to,'');
	tac_text_output_util.add_txt(it_message	=>	t_text, i_separator => DELIMITER_CTRL);

	t_text(1)	:=	'Account Code:';
	t_text(2)	:=	NVL(i_account_code_from,'');
	t_text(3)	:=	NVL(i_account_code_to,'');
	tac_text_output_util.add_txt(it_message	=>	t_text, i_separator => DELIMITER_CTRL);

	t_text(1)	:=	'RC Code:';
	t_text(2)	:=	NVL(i_rc_code_from,'');
	t_text(3)	:=	NVL(i_rc_code_to,'');
	tac_text_output_util.add_txt(it_message	=>	t_text, i_separator => DELIMITER_CTRL);

	t_text(1)	:=	'Period Name:';
	t_text(2)	:=	NVL(i_period_from,'');
	t_text(3)	:=	NVL(i_period_to,'');
	tac_text_output_util.add_txt(it_message	=>	t_text, i_separator => DELIMITER_CTRL);

-- Blank line
	t_format.DELETE;
	t_text.DELETE;
	FOR i IN 1..1
	LOOP
		t_format(i)	:=	0;
		t_text(i)	:=	NULL;
	END LOOP;
	tac_text_output_util.set_format
		(it_col_len		=>	t_format
		,o_status		=>	o_status
		,i_clear_data	=>	'N');
	IF o_status <> tac_text_output_util.NO_ERROR THEN
		RAISE abnormal_end;
	END IF;
	tac_text_output_util.add_txt(it_message	=>	t_text, i_separator => DELIMITER_CTRL);

	-- Format Column Headings
	t_format.DELETE;
	t_text.DELETE;
	FOR i IN 1..25
	LOOP
		t_format(i)	:=	NULL;
	END LOOP;
	tac_text_output_util.set_format
		(it_col_len		=>	t_format
		,o_status		=>	o_status
		,i_clear_data	=>	'N');
	IF o_status <> tac_text_output_util.NO_ERROR THEN
		RAISE abnormal_end;
	END IF;

	-- Write Column Headings
	t_text(1)	:=	'Company Code';
	t_text(2)	:=	'Company (ANAPLAN)';
	t_text(3)	:=	'GL';
	t_text(4)	:=	'RC';
	t_text(5)	:=	'Internal Code';
	t_text(6)	:=	'Project Code';
	t_text(7)	:=	'Period-Month';
	t_text(8)	:=	'Period-Year';
	t_text(9)	:=	'Source';
	t_text(10)	:=	'Category';
	t_text(11)	:=	'Line#';
	t_text(12)	:=	'Doc. Number';
	t_text(13)	:=	'Reference No.';
	t_text(14)	:=	'Vendor/Customer';
	t_text(15)	:=	'PO Category';
	t_text(16)	:=	'Journal Batch';
	t_text(17)	:=	'Journal Name';
	t_text(18)	:=	'Journal Line Description';
	t_text(19)	:=	'Amount';
	t_text(20)	:=	'PO Number';
	t_text(21)	:=	'PO Create Date';
	t_text(22)	:=	'PR Number';
	t_text(23)	:=	'PR Create Date';
	t_text(24)	:=	'Header Description';
	t_text(25)	:=	'Justification';
	tac_text_output_util.add_txt(it_message	=>	t_text, i_separator => DELIMITER_CTRL);

	-- Write Data Record
	FOR r_trx IN c_trx
	LOOP
		get_pr_po_info
			(i_jh_je_source				=>	r_trx.jh_je_source
			,i_jc_je_category_name		=>	r_trx.jc_je_category_name
			,i_org_id					=>	r_trx.org_id
			,i_ir_je_header_id			=>	r_trx.ir_je_header_id
			,i_ir_je_line_num			=>	r_trx.je_line_num
			,i_jh_currency_code			=>	r_trx.jh_currency_code
			,i_sob_currency_code		=>	r_trx.sob_currency_code
			,i_actual_flag				=>	r_trx.jh_actual_flag
			,o_po_number				=>	v_po_number
			,o_po_line_no				=>	v_po_line_no
			,o_po_line_distribution_no	=>	v_po_line_distribution_no
			,o_po_create_date			=>	v_po_create_date
			,o_po_encumber_date			=>	v_po_encumber_date
			,o_po_distribution_amount	=>	v_po_distribution_amount
			,o_pr_number				=>	v_pr_number
			,o_pr_line_no				=>	v_pr_line_no
			,o_pr_line_distribution_no	=>	v_pr_line_distribution_no
			,o_pr_create_date			=>	v_pr_create_date
			,o_pr_gl_date				=>	v_pr_gl_date
			,o_pr_distribution_amount	=>	v_pr_distribution_amount
			,o_remark					=>	v_remark
			,o_pr_header_description	=>	v_pr_header_description
			,o_pr_line_description		=>	v_pr_line_description
			,o_po_header_description	=>	v_po_header_description
			,o_po_line_description		=>	v_po_line_description);
		
		get_line_num
			(i_jh_je_source			=>	r_trx.jh_je_source
			,i_jc_je_category_name	=>	r_trx.jc_je_category_name
			,i_reference_3			=>	r_trx.ir_reference_3
			,i_reference_10			=>	r_trx.ir_reference_10
			,i_ae_line_id			=>	r_trx.ael_ae_line_id
			,i_gl_sl_link_id		=>	r_trx.ir_gl_sl_link_id
			,i_jh_currency_code		=>	r_trx.jh_currency_code
			,i_sob_currency_code	=>	r_trx.sob_currency_code
			,o_line_number			=>	v_line_number);

		t_text(1)	:=	r_trx.company_code;				-- Company Code
		t_text(2)	:=	r_trx.company_code_anaplan;		-- Company (ANAPLAN)
		t_text(3)	:=	r_trx.account_code;				-- Account Code (GL)
		t_text(4)	:=	r_trx.rc_code;					-- RC Code
		t_text(5)	:=	r_trx.project_internal_code;	-- Internal Code
		t_text(6)	:=	r_trx.project_code;				-- Project Code
		t_text(7)	:=	TO_CHAR(r_trx.acc_date,'MM');	-- Period-Month
		t_text(8)	:=	TO_CHAR(r_trx.acc_date,'YYYY');	-- Period-Year
		t_text(9)	:=	r_trx.je_source;			-- Source
		t_text(10)	:=	r_trx.jc_je_category_name;	-- Category
		t_text(11)	:=	r_trx.jl_je_line_num; /*v_line_number*/	-- Line#
		t_text(12)	:=	r_trx.doc_number;			-- Doc. Number
		t_text(13)	:=	replace_txt(r_trx.reference_no);	-- Reference No.
		t_text(14)	:=	replace_txt(r_trx.vendor_customer);	-- Vendor/Customer
		t_text(15)	:=	r_trx.po_category;			-- PO Category
		t_text(16)	:=	replace_txt(r_trx.journal_batch_name);	-- Journal Batch
		t_text(17)	:=	replace_txt(r_trx.journal_name);		-- Journal Name
		t_text(18)	:=	replace_txt(r_trx.journal_line_desc);	-- Journal Line Description
		t_text(19)	:=	NVL(r_trx.amount_dr,0) - NVL(r_trx.amount_cr,0);	-- Amount
		t_text(20)	:=	v_po_number;				-- PO Number
		t_text(21)	:=	TO_CHAR(v_po_create_date, C_DATE_FORMAT);	-- PO Create Date
		t_text(22)	:=	v_pr_number;				-- PR Number
		t_text(23)	:=	TO_CHAR(v_pr_create_date, C_DATE_FORMAT);	-- PR Create Date
		t_text(24)	:=	replace_txt(v_pr_header_description);	-- Header Description
		t_text(25)	:=	replace_txt(v_pr_line_description);		-- Justification

		tac_text_output_util.add_txt(it_message	=>	t_text, i_separator => DELIMITER_CTRL);
	END LOOP;

	tac_text_output_util.write_output
		(o_status		=>	o_status
--		,i_directory	=>	i_directory
--		,i_filename		=>	v_filename || '.csv'
		,i_skip_output	=>	FALSE
		,i_charset		=>	'UTF8');

	IF o_status <> tac_text_output_util.NO_ERROR THEN
		RAISE abnormal_end;
	END IF;

EXCEPTION
	WHEN abnormal_end THEN
		NULL;
	WHEN OTHERS THEN
		o_status		:=	'Error gen_opex : ' || SQLERRM;
END gen_opex;
----------------------------------------------------------------------------
PROCEDURE validate_gl
(i_period_name		IN	VARCHAR2
,i_period_end_date	IN	DATE
,i_gen_bs			IN	VARCHAR2	-- PA_SRS_YES_NO_LOV
,i_gen_pl			IN	VARCHAR2
,i_gen_cogs			IN	VARCHAR2
,o_status			OUT	VARCHAR2
,ot_error			OUT	nested_varchar_tabtyp)
IS
	CURSOR c_validate IS
		SELECT	lv_bks.attribute2	legal_entity
				,lv_bks.attribute3	company_code	-- in ANAPLAN
				,(SELECT	sob.name
				  FROM		gl_sets_of_books sob
				  WHERE		sob.set_of_books_id	=	TO_NUMBER(lv_bks.attribute1))	sob_name
				,ps.period_name
				,DECODE(ps.closing_status,'O','Open','F','Future','N','Never Opened',ps.closing_status)	period_status_dsp
		FROM	gl_period_statuses	ps
				,fnd_lookup_values	lv_bks
		WHERE	lv_bks.lookup_type	=	'TAC_GL_ANAPLAN_COMPANY'
		AND		lv_bks.enabled_flag	=	'Y'
		AND		i_period_end_date BETWEEN NVL(lv_bks.start_date_active,C_DFLT_DATE_START) AND NVL(lv_bks.end_date_active,C_DFLT_DATE_END)
		AND		ps.application_id	=	101	-- GL
		AND		ps.set_of_books_id	=	TO_NUMBER(lv_bks.attribute1)
		AND		ps.period_name		=	i_period_name
		-- O=Open, C=Closed, F=Future, N=Never Opened, P=Permanently Closed, W=Close Pending (Waiting - For AR Only)
		AND		ps.closing_status	NOT IN	('C','P')	-- Closed, Permanently Closed
		ORDER BY legal_entity, company_code, TO_DATE(REPLACE(ps.period_name,'ADJ','DEC'),'MON-YY'), ps.period_name DESC;

	t_error		varchar2_tabtyp;
	r_prev		fnd_lookup_values%ROWTYPE;
BEGIN
	o_status	:=	G_NO_ERROR;

	write_log('     Validate Data : '||TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
	
	ot_error.DELETE;

	t_error.DELETE;
-- Skip Validate Closed Period
/*	FOR rec IN c_validate
	LOOP
		t_error(t_error.COUNT+1)	:=	'Period : ' || RPAD(rec.period_name,15) || '  Period Status : ' || RPAD(rec.period_status_dsp,22) || '  SOB : ' || RPAD(rec.sob_name,40);
	END LOOP;*/
	
	IF t_error.COUNT > 0 THEN
		o_status	:=	'Error validate_gl';
--write_log('ot_error.COUNT = ' || ot_error.COUNT);
		ot_error(ot_error.COUNT+1)	:=	NULL;
		ot_error(ot_error.COUNT).err_code		:=	'EV-001';
		ot_error(ot_error.COUNT).err_desc		:=	CT_ERROR(ot_error(ot_error.COUNT).err_code);
		ot_error(ot_error.COUNT).err_detail		:=	t_error;
	END IF;
/*write_log('10, t_error.COUNT = ' || t_error.COUNT);
write_log('10, ot_error.COUNT = ' || ot_error.COUNT);*/

	-- Validate Setup
	t_error.DELETE;
	FOR rec IN (SELECT	lv.*
				FROM	fnd_lookup_values lv
				WHERE	lv.lookup_type	IN	('TAC_GL_ACCOUNT_BLS','TAC_GL_ACCOUNT_P&L','TAC_GL_ACCOUNT_COGS')
				AND		lv.enabled_flag	=	'Y'
				AND		i_period_end_date BETWEEN NVL(lv.start_date_active,C_DFLT_DATE_START) AND NVL(lv.end_date_active,C_DFLT_DATE_END)
				AND		(lv.attribute1 IS NULL AND lv.attribute3 IS NULL)
				ORDER BY lv.lookup_type, lv.lookup_code)
	LOOP
		IF rec.lookup_type <> r_prev.lookup_type THEN
			o_status	:=	'Error validate_gl';
			ot_error(ot_error.COUNT+1)	:=	NULL;
			IF r_prev.lookup_type = 'TAC_GL_ACCOUNT_BLS' THEN
				ot_error(ot_error.COUNT).err_code		:=	'EV-003';
			ELSIF r_prev.lookup_type = 'TAC_GL_ACCOUNT_P&L' THEN
				ot_error(ot_error.COUNT).err_code		:=	'EV-004';
			ELSIF r_prev.lookup_type = 'TAC_GL_ACCOUNT_COGS' THEN
				ot_error(ot_error.COUNT).err_code		:=	'EV-005';
			END IF;
			ot_error(ot_error.COUNT).err_desc		:=	CT_ERROR(ot_error(ot_error.COUNT).err_code);
			ot_error(ot_error.COUNT).err_detail		:=	t_error;
			t_error.DELETE;
		END IF;
		t_error(t_error.COUNT+1)	:=	'Lookup Code : ' || rec.lookup_code;
		r_prev	:=	rec;
	END LOOP;
--write_log('30, t_error.COUNT = ' || t_error.COUNT);
	IF t_error.COUNT > 0 THEN
		o_status	:=	'Error validate_gl';
		ot_error(ot_error.COUNT+1)	:=	NULL;
		IF r_prev.lookup_type = 'TAC_GL_ACCOUNT_BLS' THEN
			ot_error(ot_error.COUNT).err_code		:=	'EV-003';
		ELSIF r_prev.lookup_type = 'TAC_GL_ACCOUNT_P&L' THEN
			ot_error(ot_error.COUNT).err_code		:=	'EV-004';
		ELSIF r_prev.lookup_type = 'TAC_GL_ACCOUNT_COGS' THEN
			ot_error(ot_error.COUNT).err_code		:=	'EV-005';
		END IF;
		ot_error(ot_error.COUNT).err_desc		:=	CT_ERROR(ot_error(ot_error.COUNT).err_code);
		ot_error(ot_error.COUNT).err_detail		:=	t_error;
	END IF;
--write_log('40, ot_error.COUNT = ' || ot_error.COUNT);

	t_error.DELETE;
	IF 'Y' IN (i_gen_bs, i_gen_pl, i_gen_cogs) THEN
		NULL;
	ELSE
		t_error(t_error.COUNT+1)	:=	'Gen BS : ' || i_gen_bs || '  Gen PL : ' || i_gen_pl || '  Gen COGS : ' || i_gen_cogs;
	END IF;
	
	IF t_error.COUNT > 0 THEN
		o_status	:=	'Error validate_gl';
--write_log('ot_error.COUNT = ' || ot_error.COUNT);
		ot_error(ot_error.COUNT+1)	:=	NULL;
		ot_error(ot_error.COUNT).err_code		:=	'EV-007';
		ot_error(ot_error.COUNT).err_desc		:=	CT_ERROR(ot_error(ot_error.COUNT).err_code);
		ot_error(ot_error.COUNT).err_detail		:=	t_error;
	END IF;
/*write_log('10, t_error.COUNT = ' || t_error.COUNT);
write_log('10, ot_error.COUNT = ' || ot_error.COUNT);*/

EXCEPTION
	WHEN OTHERS THEN
		o_status		:=	'Error validate_gl : ' || SQLERRM;
		write_log('     Validate Data : '||o_status);
END validate_gl;
----------------------------------------------------------------------------
PROCEDURE validate_po
(i_period_name		IN	VARCHAR2
,i_period_end_date	IN	DATE
,o_status			OUT	VARCHAR2
,ot_error			OUT	nested_varchar_tabtyp)
IS
	t_error		varchar2_tabtyp;
	r_prev		fnd_lookup_values%ROWTYPE;
BEGIN
	o_status	:=	G_NO_ERROR;

	write_log('     Validate Data : '||TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
	
	ot_error.DELETE;

	-- Validate Setup
	t_error.DELETE;
	FOR rec IN (SELECT	lv.*
				FROM	fnd_lookup_values lv
				WHERE	lv.lookup_type	=	'TAC_PO_PROJECT_ANAPLAN'
				AND		lv.enabled_flag	=	'Y'
				AND		i_period_end_date BETWEEN NVL(lv.start_date_active,C_DFLT_DATE_START) AND NVL(lv.end_date_active,C_DFLT_DATE_END)
				AND		lv.attribute1 IS NULL
				ORDER BY lv.lookup_code)
	LOOP
		t_error(t_error.COUNT+1)	:=	'Lookup Code : ' || rec.lookup_code;
	END LOOP;
	IF t_error.COUNT > 0 THEN
		o_status	:=	'Error validate_po';
		ot_error(ot_error.COUNT+1)	:=	NULL;
		ot_error(ot_error.COUNT).err_code		:=	'EV-006';
		ot_error(ot_error.COUNT).err_desc		:=	CT_ERROR(ot_error(ot_error.COUNT).err_code);
		ot_error(ot_error.COUNT).err_detail		:=	t_error;
	END IF;

EXCEPTION
	WHEN OTHERS THEN
		o_status		:=	'Error validate_po : ' || SQLERRM;
		write_log('     Validate Data : '||o_status);
END validate_po;
----------------------------------------------------------------------------
PROCEDURE validate_opex
(i_date_from	IN	DATE
,i_date_to		IN	DATE
,o_status		OUT	VARCHAR2
,ot_error		OUT	nested_varchar_tabtyp)
IS
	t_error		varchar2_tabtyp;
	r_prev		fnd_lookup_values%ROWTYPE;
BEGIN
	o_status	:=	G_NO_ERROR;

	write_log('     Validate Data : '||TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
	
	ot_error.DELETE;

	-- Validate Setup
	t_error.DELETE;
	FOR rec IN (SELECT	lv.*
				FROM	fnd_lookup_values lv
				WHERE	lv.lookup_type	=	'TAC_GL_ACCOUNT_OPEX'
				AND		lv.enabled_flag	=	'Y'
				AND		NVL(lv.start_date_active,C_DFLT_DATE_START)	<=	i_date_to
				AND		NVL(lv.end_date_active,C_DFLT_DATE_END)		>=	i_date_from
				AND		lv.attribute1 IS NULL AND lv.attribute3 IS NULL
				ORDER BY lv.lookup_code)
	LOOP
		t_error(t_error.COUNT+1)	:=	'Lookup Code : ' || rec.lookup_code;
	END LOOP;
	IF t_error.COUNT > 0 THEN
		o_status	:=	'Error validate_opex';
		ot_error(ot_error.COUNT+1)	:=	NULL;
		ot_error(ot_error.COUNT).err_code		:=	'EV-002';
		ot_error(ot_error.COUNT).err_desc		:=	CT_ERROR(ot_error(ot_error.COUNT).err_code);
		ot_error(ot_error.COUNT).err_detail		:=	t_error;
	END IF;

EXCEPTION
	WHEN OTHERS THEN
		o_status		:=	'Error validate_opex : ' || SQLERRM;
		write_log('     Validate Data : '||o_status);
END validate_opex;
----------------------------------------------------------------------------
PROCEDURE main_outbound_gl
(ERR_MSG				OUT	VARCHAR2
,ERR_CODE				OUT	VARCHAR2
,i_file_path			IN	VARCHAR2	:=	'/MP/GERP_TEST/ANAPLAN/INBOX'	--\\lynx22\EAITEST\ANAPLAN\INBOX
,i_filename_pattern		IN	VARCHAR2	:=	'YYYYMM"_ERP_GLSummary_$ENTITY$_ANAPLAN_ALL"'
,i_period_date			IN	VARCHAR2
,i_email_address		IN	VARCHAR2	:=	'nidjarin@teammer.com'
,i_gen_bs				IN	VARCHAR2	-- PA_SRS_YES_NO_LOV
,i_gen_pl				IN	VARCHAR2
,i_gen_cogs				IN	VARCHAR2
/*,i_sftp_host			IN	VARCHAR2
,i_sftp_user			IN	VARCHAR2
,i_sftp_remote_path		IN	VARCHAR2
,i_file_path_error		IN	VARCHAR2
,i_file_path_history	IN	VARCHAR2*/
)
IS
	v_status			VARCHAR2(2000);
	t_error				nested_varchar_tabtyp;
	C_PROGRAM_NAME		CONSTANT VARCHAR2(1000)	:=	'TAC : BPT Outbound Interface - GL';
	v_period_date		DATE	:=	TO_DATE(i_period_date,'YYYY/MM/DD HH24:MI:SSSSS');
	v_period_name		VARCHAR2(30);
	v_period_end_date	DATE;
	v_filename_bs		VARCHAR2(100);
	v_filename_pl		VARCHAR2(100);
	v_filename_cogs		VARCHAR2(100);
	b_success			BOOLEAN	:=	TRUE;
	v_error_code		VARCHAR2(100);
	v_bs_rows			NUMBER;
	v_pl_rows			NUMBER;
	v_cogs_rows			NUMBER;
	v_bs_amount			NUMBER;
	v_pl_amount			NUMBER;
	v_cogs_amount		NUMBER;
	v_start_time		DATE	:=	SYSDATE;
BEGIN
	write_log('----------------------------------------------------------------------------');
	write_log('Start Process : '||TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
	write_log('----------------------------------------------------------------------------');
	write_log('Parameters');
	write_log('----------');
	write_log('i_file_path        : '||i_file_path);
	write_log('i_filename_pattern : '||i_filename_pattern);
	write_log('i_period_date      : '||i_period_date);
	write_log('i_email_address    : '||i_email_address);
	write_log('i_gen_bs           : '||i_gen_bs);
	write_log('i_gen_pl           : '||i_gen_pl);
	write_log('i_gen_cogs         : '||i_gen_cogs);
	write_log(' ');
	write_log(' ');

	SELECT	p.period_name, p.end_date
	INTO	v_period_name, v_period_end_date
	FROM	gl_periods p
	WHERE	period_set_name				=	'Accounting'
	AND		p.adjustment_period_flag	=	'N'
	AND		v_period_date BETWEEN p.start_date AND p.end_date;

	tac_text_output_util.g_process_id	:=	g_request_id;
	v_status := G_NO_ERROR;
	-- Generate text file
	write_log('Call Validate Data : '||TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
	validate_gl	(i_period_name		=>	v_period_name
				,i_period_end_date	=>	v_period_end_date
				,i_gen_bs			=>	i_gen_bs
				,i_gen_pl			=>	i_gen_pl
				,i_gen_cogs			=>	i_gen_cogs
				,o_status			=>	v_status
				,ot_error			=>	t_error);
	IF v_status <> G_NO_ERROR THEN
		write_log('Validation Error :');
		write_log('---------------------------------------');
		write_log(v_status);
		FOR i IN 1..t_error.COUNT
		LOOP
			write_log(t_error(i).err_code || ' : ' || t_error(i).err_desc);
/*			FOR j IN 1..t_error(i).err_detail.COUNT
			LOOP
				write_log(t_error(i).err_code || ' : ' || t_error(i).err_desc || ' : ' || t_error(i).err_detail(j));
			END LOOP;*/
		END LOOP;
		write_log(' ');
		write_log('---------------------------------------');
		ERR_MSG		:=	v_status;
		ERR_CODE	:=	'2';	-- Error

		-- Send email
		send_email  (i_to_email		=>  i_email_address
					,i_cc_email		=>  NULL
					,i_bcc_email	=>  NULL
					,i_subject		=>	'ERP : ' || C_PROGRAM_NAME || ' Request ID : ' || g_request_id || ' - VALIDATION FAILED'
					,i_program_name	=>	C_PROGRAM_NAME
					,i_file_path		=>	i_file_path
					,i_filename_pattern	=>	i_filename_pattern
					,i_period_date		=>	i_period_date
					,i_email_address	=>	i_email_address
					,i_gen_bs			=>	i_gen_bs
					,i_gen_pl			=>	i_gen_pl
					,i_gen_cogs			=>	i_gen_cogs
					,it_error		=>	t_error
					,i_start_time	=>	v_start_time
					,i_end_time		=>	SYSDATE
					,io_status		=>  v_status
					);
		IF v_status <> G_NO_ERROR THEN
			write_log('Error while sending email = ' || v_status);
		END IF;
		RETURN;
	END IF;
	-- Generate text file
	IF i_gen_bs = 'Y' THEN
		write_log('Call Generate Output for BS : '||TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
		gen_gl_bs
			(i_program_name		=>	C_PROGRAM_NAME
			,i_period_name		=>	v_period_name
			,i_period_end_date	=>	v_period_end_date
			,i_directory		=>	get_dirobj(i_file_path, DIRECTORY_PREFIX)
			,i_filename_format	=>	REPLACE(i_filename_pattern,'$ENTITY$','BS')
			,o_filename			=>	v_filename_bs
			,o_status			=>	v_status
			,o_rows				=>	v_bs_rows
			,o_amount			=>	v_bs_amount);
		IF v_status <> G_NO_ERROR THEN
			b_success	:=	FALSE;
			ERR_MSG		:=	v_status;
			ERR_CODE	:=	'2';	-- Error

			t_error.DELETE;
			t_error(t_error.COUNT+1)				:=	NULL;
			t_error(t_error.COUNT).err_code			:=	'FG-001';
			t_error(t_error.COUNT).err_desc			:=	CT_ERROR(t_error(t_error.COUNT).err_code);
			t_error(t_error.COUNT).err_detail(1)	:=	v_status;

			-- Send email
			send_email  (i_to_email		=>  i_email_address
						,i_cc_email		=>  NULL
						,i_bcc_email	=>  NULL
						,i_subject		=>	'ERP : ' || C_PROGRAM_NAME || ' Request ID : ' || g_request_id || ' - BS FILE GENERATION FAILED'
						,i_program_name	=>	C_PROGRAM_NAME
						,i_file_path		=>	i_file_path
						,i_filename_pattern	=>	i_filename_pattern
						,i_period_date		=>	i_period_date
						,i_email_address	=>	i_email_address
						,i_gen_bs			=>	i_gen_bs
						,i_gen_pl			=>	i_gen_pl
						,i_gen_cogs			=>	i_gen_cogs
						,it_error		=>	t_error
						,i_start_time	=>	v_start_time
						,i_end_time		=>	SYSDATE
						,io_status		=>  v_status
						);
			IF v_status <> G_NO_ERROR THEN
				write_log('Error while sending email = ' || v_status);
			END IF;
			RAISE abnormal_end;
		END IF;
		write_log('Call Generate Output for BS status : '||v_status);
		write_log('BS Outbound Interface Filename : '||v_filename_bs);
		write_log('BS Outbound Interface Filepath : '||i_file_path);
		IF v_status = G_NO_ERROR THEN
			write_log('Data File Name : ' || v_filename_bs || '.csv');
			write_log('Control File Name : ' || v_filename_bs || '.ctl');
			write_log('Total Records : ' || TO_CHAR(v_bs_rows,'fm999,999,999,990'));
			write_log('Total Amount : ' || TO_CHAR(v_bs_amount,'fm999,999,999,999,999,999,990.00'));
		END IF;
		write_log(' ');
	END IF;

	IF i_gen_pl = 'Y' THEN
		write_log('Call Generate Output for P&L : '||TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
		gen_gl_pl
			(i_program_name		=>	C_PROGRAM_NAME
			,i_period_name		=>	v_period_name
			,i_period_end_date	=>	v_period_end_date
			,i_directory		=>	get_dirobj(i_file_path, DIRECTORY_PREFIX)
			,i_filename_format	=>	REPLACE(i_filename_pattern,'$ENTITY$','PL')
			,o_filename			=>	v_filename_pl
			,o_status			=>	v_status
			,o_rows				=>	v_pl_rows
			,o_amount			=>	v_pl_amount);
		IF v_status <> G_NO_ERROR THEN
			b_success	:=	FALSE;
			ERR_MSG		:=	v_status;
			ERR_CODE	:=	'2';	-- Error

			t_error.DELETE;
			t_error(t_error.COUNT+1)				:=	NULL;
			t_error(t_error.COUNT).err_code			:=	'FG-001';
			t_error(t_error.COUNT).err_desc			:=	CT_ERROR(t_error(t_error.COUNT).err_code);
			t_error(t_error.COUNT).err_detail(1)	:=	v_status;

			-- Send email
			send_email  (i_to_email		=>  i_email_address
						,i_cc_email		=>  NULL
						,i_bcc_email	=>  NULL
						,i_subject		=>	'ERP : ' || C_PROGRAM_NAME || ' Request ID : ' || g_request_id || ' - PL FILE GENERATION FAILED'
						,i_program_name	=>	C_PROGRAM_NAME
						,i_file_path		=>	i_file_path
						,i_filename_pattern	=>	i_filename_pattern
						,i_period_date		=>	i_period_date
						,i_email_address	=>	i_email_address
						,i_gen_bs			=>	i_gen_bs
						,i_gen_pl			=>	i_gen_pl
						,i_gen_cogs			=>	i_gen_cogs
						,it_error		=>	t_error
						,i_start_time	=>	v_start_time
						,i_end_time		=>	SYSDATE
						,io_status		=>  v_status
						);
			IF v_status <> G_NO_ERROR THEN
				write_log('Error while sending email = ' || v_status);
			END IF;
			RAISE abnormal_end;
		END IF;
		write_log('Call Generate Output for P&L status : '||v_status);
		write_log('P&L Outbound Interface Filename : '||v_filename_pl);
		write_log('P&L Outbound Interface Filepath : '||i_file_path);
		IF v_status = G_NO_ERROR THEN
			write_log('Data File Name : ' || v_filename_pl || '.csv');
			write_log('Control File Name : ' || v_filename_pl || '.ctl');
			write_log('Total Records : ' || TO_CHAR(v_pl_rows,'fm999,999,999,990'));
			write_log('Total Amount : ' || TO_CHAR(v_pl_amount,'fm999,999,999,999,999,999,990.00'));
		END IF;
		write_log(' ');
	END IF;

	IF i_gen_cogs = 'Y' THEN
		write_log('Call Generate Output for COGS : '||TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
		gen_gl_cogs
			(i_program_name		=>	C_PROGRAM_NAME
			,i_period_name		=>	v_period_name
			,i_period_end_date	=>	v_period_end_date
			,i_directory		=>	get_dirobj(i_file_path, DIRECTORY_PREFIX)
			,i_filename_format	=>	REPLACE(i_filename_pattern,'$ENTITY$','COGS')
			,o_filename			=>	v_filename_cogs
			,o_status			=>	v_status
			,o_rows				=>	v_cogs_rows
			,o_amount			=>	v_cogs_amount);
		IF v_status <> G_NO_ERROR THEN
			b_success	:=	FALSE;
			ERR_MSG		:=	v_status;
			ERR_CODE	:=	'2';	-- Error

			t_error.DELETE;
			t_error(t_error.COUNT+1)				:=	NULL;
			t_error(t_error.COUNT).err_code			:=	'FG-001';
			t_error(t_error.COUNT).err_desc			:=	CT_ERROR(t_error(t_error.COUNT).err_code);
			t_error(t_error.COUNT).err_detail(1)	:=	v_status;

			-- Send email
			send_email  (i_to_email		=>  i_email_address
						,i_cc_email		=>  NULL
						,i_bcc_email	=>  NULL
						,i_subject		=>	'ERP : ' || C_PROGRAM_NAME || ' Request ID : ' || g_request_id || ' - COGS FILE GENERATION FAILED'
						,i_program_name	=>	C_PROGRAM_NAME
						,i_file_path		=>	i_file_path
						,i_filename_pattern	=>	i_filename_pattern
						,i_period_date		=>	i_period_date
						,i_email_address	=>	i_email_address
						,i_gen_bs			=>	i_gen_bs
						,i_gen_pl			=>	i_gen_pl
						,i_gen_cogs			=>	i_gen_cogs
						,it_error		=>	t_error
						,i_start_time	=>	v_start_time
						,i_end_time		=>	SYSDATE
						,io_status		=>  v_status
						);
			IF v_status <> G_NO_ERROR THEN
				write_log('Error while sending email = ' || v_status);
			END IF;
			RAISE abnormal_end;
		END IF;
		write_log('Call Generate Output for COGS status : '||v_status);
		write_log('COGS Outbound Interface Filename : '||v_filename_cogs);
		write_log('COGS Outbound Interface Filepath : '||i_file_path);
		IF v_status = G_NO_ERROR THEN
			write_log('Data File Name : ' || v_filename_cogs || '.csv');
			write_log('Control File Name : ' || v_filename_cogs || '.ctl');
			write_log('Total Records : ' || TO_CHAR(v_cogs_rows,'fm999,999,999,990'));
			write_log('Total Amount : ' || TO_CHAR(v_cogs_amount,'fm999,999,999,999,999,999,990.00'));
		END IF;
		write_log(' ');
	END IF;

	IF b_success AND v_status = G_NO_ERROR THEN
		ERR_CODE	:=	'0';
-- Skip sending email for Success case
/*		send_email_success  (i_to_email			=>  i_email_address
							,i_cc_email			=>  NULL
							,i_bcc_email		=>  NULL
							,i_subject			=>	'ERP : ' || C_PROGRAM_NAME || ' Request ID : ' || g_request_id || ' - SUCCESS'
							,i_program_name		=>	C_PROGRAM_NAME
							,i_file_path		=>	i_file_path
							,i_filename_pattern	=>	i_filename_pattern
							,i_period_date		=>	i_period_date
							,i_email_address	=>	i_email_address
							,i_gen_bs			=>	i_gen_bs
							,i_gen_pl			=>	i_gen_pl
							,i_gen_cogs			=>	i_gen_cogs
							,i_bs_rows			=>	v_bs_rows
							,i_pl_rows			=>	v_pl_rows
							,i_cogs_rows		=>	v_cogs_rows
							,i_bs_amount		=>	v_bs_amount
							,i_pl_amount		=>	v_pl_amount
							,i_cogs_amount		=>	v_cogs_amount
							,i_bs_filename		=>	v_filename_bs
							,i_pl_filename		=>	v_filename_pl
							,i_cogs_filename	=>	v_filename_cogs
							,i_start_time		=>	v_start_time
							,i_end_time		=>	SYSDATE
							,io_status		=>  v_status
							);*/
	ELSE
		write_log('Error :');
		write_log('---------------------------------------');
		write_log(v_status);
		write_log(' ');
		write_log('---------------------------------------');
		ERR_MSG		:=	v_status;
		ERR_CODE	:=	'2';	-- Error

		RETURN;
	END IF;

	write_log('End Process : '||TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
	write_log('----------------------------------------------------------------------------');
EXCEPTION
	WHEN abnormal_end THEN
		ERR_MSG		:=	v_status;
		ERR_CODE	:=	'2';
		ROLLBACK;
	WHEN OTHERS THEN
		ERR_MSG		:=	SQLERRM;
		ERR_CODE	:=	'2';
END main_outbound_gl;
----------------------------------------------------------------------------
PROCEDURE main_outbound_po
(ERR_MSG				OUT	VARCHAR2
,ERR_CODE				OUT	VARCHAR2
,i_file_path			IN	VARCHAR2	:=	'/MP/GERP_TEST/ANAPLAN/INBOX'	--\\lynx22\EAITEST\ANAPLAN\INBOX
,i_filename_pattern		IN	VARCHAR2	:=	'YYYYMM"_ERP_POSummary_ANAPLAN_ALL"'
,i_period_date			IN	VARCHAR2
,i_email_address		IN	VARCHAR2	:=	'nidjarin@teammer.com'
/*,i_sftp_host			IN	VARCHAR2
,i_sftp_user			IN	VARCHAR2
,i_sftp_remote_path		IN	VARCHAR2
,i_file_path_error		IN	VARCHAR2
,i_file_path_history	IN	VARCHAR2*/
)
IS
	v_status			VARCHAR2(2000);
	t_error				nested_varchar_tabtyp;
	C_PROGRAM_NAME		CONSTANT VARCHAR2(1000)	:=	'TAC : BPT Outbound Interface - PO';
	v_period_date		DATE	:=	TO_DATE(i_period_date,'YYYY/MM/DD HH24:MI:SSSSS');
	v_period_name		VARCHAR2(30);
	v_period_end_date	DATE;
	v_filename_po		VARCHAR2(100);
	b_success			BOOLEAN	:=	TRUE;
	v_error_code		VARCHAR2(100);
	v_po_rows			NUMBER;
	v_po_amount			NUMBER;
	v_start_time		DATE	:=	SYSDATE;
BEGIN
	write_log('----------------------------------------------------------------------------');
	write_log('Start Process : '||TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
	write_log('----------------------------------------------------------------------------');
	write_log('Parameters');
	write_log('----------');
	write_log('i_file_path        : '||i_file_path);
	write_log('i_filename_pattern : '||i_filename_pattern);
	write_log('i_period_date      : '||i_period_date);
	write_log('i_email_address    : '||i_email_address);
	write_log(' ');
	write_log(' ');

	SELECT	p.period_name, p.end_date
	INTO	v_period_name, v_period_end_date
	FROM	gl_periods p
	WHERE	period_set_name				=	'Accounting'
	AND		p.adjustment_period_flag	=	'N'
	AND		v_period_date BETWEEN p.start_date AND p.end_date;

	tac_text_output_util.g_process_id	:=	g_request_id;
	v_status := G_NO_ERROR;
	-- Generate text file
	write_log('Call Validate Data : '||TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
	validate_po	(i_period_name		=>	v_period_name
				,i_period_end_date	=>	v_period_end_date
				,o_status			=>	v_status
				,ot_error			=>	t_error);
	IF v_status <> G_NO_ERROR THEN
		write_log('Validation Error :');
		write_log('---------------------------------------');
		write_log(v_status);
		FOR i IN 1..t_error.COUNT
		LOOP
			write_log(t_error(i).err_code || ' : ' || t_error(i).err_desc);
/*			FOR j IN 1..t_error(i).err_detail.COUNT
			LOOP
				write_log(t_error(i).err_code || ' : ' || t_error(i).err_desc || ' : ' || t_error(i).err_detail(j));
			END LOOP;*/
		END LOOP;
		write_log(' ');
		write_log('---------------------------------------');
		ERR_MSG		:=	v_status;
		ERR_CODE	:=	'2';	-- Error

		-- Send email
		send_email  (i_to_email		=>  i_email_address
					,i_cc_email		=>  NULL
					,i_bcc_email	=>  NULL
					,i_subject		=>	'ERP : ' || C_PROGRAM_NAME || ' Request ID : ' || g_request_id || ' - VALIDATION FAILED'
					,i_program_name	=>	C_PROGRAM_NAME
					,i_file_path		=>	i_file_path
					,i_filename_pattern	=>	i_filename_pattern
					,i_period_date		=>	i_period_date
					,i_email_address	=>	i_email_address
					,it_error		=>	t_error
					,i_start_time	=>	v_start_time
					,i_end_time		=>	SYSDATE
					,io_status		=>  v_status
					);
		IF v_status <> G_NO_ERROR THEN
			write_log('Error while sending email = ' || v_status);
		END IF;
		RETURN;
	END IF;
	-- Generate text file
	write_log('Call Generate Output for PO : '||TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
	gen_po
		(i_period_name		=>	v_period_name
		,i_period_end_date	=>	v_period_end_date
		,i_program_name		=>	C_PROGRAM_NAME
		,i_directory		=>	get_dirobj(i_file_path, DIRECTORY_PREFIX)
		,i_filename_format	=>	i_filename_pattern
		,o_filename			=>	v_filename_po
		,o_status			=>	v_status
		,o_rows				=>	v_po_rows
		,o_amount			=>	v_po_amount);
	IF v_status <> G_NO_ERROR THEN
		b_success	:=	FALSE;
		ERR_MSG		:=	v_status;
		ERR_CODE	:=	'2';	-- Error

		t_error.DELETE;
		t_error(t_error.COUNT+1)				:=	NULL;
		t_error(t_error.COUNT).err_code			:=	'FG-001';
		t_error(t_error.COUNT).err_desc			:=	CT_ERROR(t_error(t_error.COUNT).err_code);
		t_error(t_error.COUNT).err_detail(1)	:=	v_status;

		-- Send email
		send_email  (i_to_email		=>  i_email_address
					,i_cc_email		=>  NULL
					,i_bcc_email	=>  NULL
					,i_subject		=>	'ERP : ' || C_PROGRAM_NAME || ' Request ID : ' || g_request_id || ' - PO FILE GENERATION FAILED'
					,i_program_name	=>	C_PROGRAM_NAME
					,i_file_path		=>	i_file_path
					,i_filename_pattern	=>	i_filename_pattern
					,i_period_date		=>	i_period_date
					,i_email_address	=>	i_email_address
					,it_error		=>	t_error
					,i_start_time	=>	v_start_time
					,i_end_time		=>	SYSDATE
					,io_status		=>  v_status
					);
		IF v_status <> G_NO_ERROR THEN
			write_log('Error while sending email = ' || v_status);
		END IF;
		RAISE abnormal_end;
	END IF;
	write_log('Call Generate Output for BS status : '||v_status);
	write_log('BS Outbound Interface Filename : '||v_filename_po);
	write_log('BS Outbound Interface Filepath : '||i_file_path);
	IF v_status = G_NO_ERROR THEN
		write_log('Data File Name : ' || v_filename_po || '.csv');
		write_log('Control File Name : ' || v_filename_po || '.ctl');
		write_log('Total Records : ' || TO_CHAR(v_po_rows,'fm999,999,999,990'));
		write_log('Total Amount : ' || TO_CHAR(v_po_amount,'fm999,999,999,999,999,999,990.00'));
	END IF;
	write_log(' ');

	IF b_success AND v_status = G_NO_ERROR THEN
		ERR_CODE	:=	'0';
-- Skip sending email for Success case
/*		send_email_success  (i_to_email			=>  i_email_address
							,i_cc_email			=>  NULL
							,i_bcc_email		=>  NULL
							,i_subject			=>	'ERP : ' || C_PROGRAM_NAME || ' Request ID : ' || g_request_id || ' - SUCCESS'
							,i_program_name		=>	C_PROGRAM_NAME
							,i_file_path		=>	i_file_path
							,i_filename_pattern	=>	i_filename_pattern
							,i_period_date		=>	i_period_date
							,i_email_address	=>	i_email_address
							,i_po_rows			=>	v_po_rows
							,i_po_amount		=>	v_po_amount
							,i_po_filename		=>	v_filename_po
							,i_start_time		=>	v_start_time
							,i_end_time			=>	SYSDATE
							,io_status			=>  v_status
							);*/
	ELSE
		write_log('Error :');
		write_log('---------------------------------------');
		write_log(v_status);
		write_log(' ');
		write_log('---------------------------------------');
		ERR_MSG		:=	v_status;
		ERR_CODE	:=	'2';	-- Error

		COMMIT;
--		ROLLBACK;
		RETURN;
	END IF;

	write_log('End Process : '||TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
	write_log('----------------------------------------------------------------------------');
	COMMIT;
EXCEPTION
	WHEN abnormal_end THEN
		ERR_MSG		:=	v_status;
		ERR_CODE	:=	'2';
		ROLLBACK;
	WHEN OTHERS THEN
		ERR_MSG		:=	SQLERRM;
		ERR_CODE	:=	'2';
END main_outbound_po;
----------------------------------------------------------------------------
PROCEDURE main_report_opex
(ERR_MSG				OUT	VARCHAR2
,ERR_CODE				OUT	VARCHAR2
,i_company_code_from	IN	VARCHAR2
,i_company_code_to		IN	VARCHAR2
,i_account_code_from	IN	VARCHAR2
,i_account_code_to		IN	VARCHAR2
,i_rc_code_from			IN	VARCHAR2
,i_rc_code_to			IN	VARCHAR2
,i_period_from			IN	VARCHAR2	-- TH_TCORE_PERIOD
,i_period_to			IN	VARCHAR2	-- TH_TCORE_PERIOD
,i_report_name			IN	VARCHAR2	:=	'OPEX GL Transactions'
/*,i_sftp_host			IN	VARCHAR2
,i_sftp_user			IN	VARCHAR2
,i_sftp_remote_path		IN	VARCHAR2
,i_file_path_error		IN	VARCHAR2
,i_file_path_history	IN	VARCHAR2*/
)
IS
	v_status			VARCHAR2(2000);
	t_error				nested_varchar_tabtyp;
	C_PROGRAM_NAME		CONSTANT VARCHAR2(1000)	:=	'TAC : BPT OPEX Report';
	v_filename			VARCHAR2(100);
	b_success			BOOLEAN	:=	TRUE;
	v_error_code		VARCHAR2(100);
BEGIN
	write_log('----------------------------------------------------------------------------');
	write_log('Start Report : '||TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
	write_log('----------------------------------------------------------------------------');
	write_log('Parameters');
	write_log('----------');
	write_log('i_company_code_from : '||i_company_code_from);
	write_log('i_company_code_to   : '||i_company_code_to);
	write_log('i_account_code_from : '||i_account_code_from);
	write_log('i_account_code_to   : '||i_account_code_to);
	write_log('i_rc_code_from      : '||i_rc_code_from);
	write_log('i_rc_code_to        : '||i_rc_code_to);
	write_log('i_period_from       : '||i_period_from);
	write_log('i_period_to         : '||i_period_to);
	write_log(' ');
	write_log(' ');
	tac_text_output_util.g_process_id	:=	g_request_id;
	v_status := G_NO_ERROR;
	
	-- Validate parameters
	ERR_CODE	:=	'0';	-- Success
	IF i_company_code_from > i_company_code_to THEN
		write_log('ERROR : Company Code From > To');
		ERR_CODE	:=	'2';
	END IF;

	IF i_account_code_from > i_account_code_to THEN
		write_log('ERROR : Account Code From > To');
		ERR_CODE	:=	'2';
	END IF;

	IF i_rc_code_from > i_rc_code_to THEN
		write_log('ERROR : RC Code From > To');
		ERR_CODE	:=	'2';
	END IF;

	FOR rec IN (SELECT	MAX(DECODE(p.period_name,i_period_from,p.start_date))	date_from
						,MAX(DECODE(p.period_name,i_period_to,p.end_date))		date_to
				FROM	gl_periods p
				WHERE	period_set_name				=	'Accounting'
				AND		p.adjustment_period_flag	=	'N'
				AND		period_name IN (i_period_from, i_period_to))
	LOOP
		IF rec.date_from > rec.date_to THEN
			write_log('ERROR : Period From > To');
			ERR_CODE	:=	'2';
		ELSE
		-- Validate Lookup Config
			write_log('Call Validate Data : '||TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
			validate_opex	(i_date_from	=>	rec.date_from
							,i_date_to		=>	rec.date_to
							,o_status		=>	v_status
							,ot_error		=>	t_error);
			IF v_status <> G_NO_ERROR THEN
				write_log('Validation Error :');
				write_log('---------------------------------------');
				write_log(v_status);
				FOR i IN 1..t_error.COUNT
				LOOP
					write_log(t_error(i).err_code || ' : ' || t_error(i).err_desc);
		/*			FOR j IN 1..t_error(i).err_detail.COUNT
					LOOP
						write_log(t_error(i).err_code || ' : ' || t_error(i).err_desc || ' : ' || t_error(i).err_detail(j));
					END LOOP;*/
				END LOOP;
				write_log(' ');
				write_log('---------------------------------------');
				ERR_MSG		:=	v_status;
				ERR_CODE	:=	'2';	-- Error
			END IF;
		END IF;
	END LOOP;

	IF ERR_CODE = '2' THEN
		ERR_MSG		:=	'ERROR : Incorrect parameter value(s), please correct and re-submit report';
	ELSE
		-- Generate text file
		write_log('Call Generate Output for OPEX : '||TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
		gen_opex
			(i_program_name			=>	C_PROGRAM_NAME
			,i_report_name			=>	i_report_name
			,i_company_code_from	=>	i_company_code_from
			,i_company_code_to		=>	i_company_code_to
			,i_account_code_from	=>	i_account_code_from
			,i_account_code_to		=>	i_account_code_to
			,i_rc_code_from			=>	i_rc_code_from
			,i_rc_code_to			=>	i_rc_code_to
			,i_period_from			=>	i_period_from
			,i_period_to			=>	i_period_to
			,o_status				=>	v_status);
		write_log('Call Generate Output for OPEX status : '||v_status);

		IF v_status = G_NO_ERROR THEN
			ERR_CODE	:=	'0';
		ELSE
			write_log('Error :');
			write_log('---------------------------------------');
			write_log(v_status);
			write_log(' ');
			write_log('---------------------------------------');
			ERR_MSG		:=	v_status;
			ERR_CODE	:=	'2';	-- Error

			RETURN;
		END IF;
	END IF;

	write_log('End Report : '||TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));
	write_log('----------------------------------------------------------------------------');
EXCEPTION
	WHEN abnormal_end THEN
		ERR_MSG		:=	v_status;
		ERR_CODE	:=	'2';
		ROLLBACK;
	WHEN OTHERS THEN
		ERR_MSG		:=	SQLERRM;
		ERR_CODE	:=	'2';
END main_report_opex;
----------------------------------------------------------------------------
BEGIN
	CT_ERROR('EV-001')	:=	'GL Period was not closed.';
	CT_ERROR('EV-002')	:=	'Setup incorrect for lookup type "TAC_GL_ACCOUNT_OPEX", either Account Code or Account Type must be specified.';
	CT_ERROR('EV-003')	:=	'Setup incorrect for lookup type "TAC_GL_ACCOUNT_BLS", either Account Code or Account Type must be specified.';
	CT_ERROR('EV-004')	:=	'Setup incorrect for lookup type "TAC_GL_ACCOUNT_P&L", either Account Code or Account Type must be specified.';
	CT_ERROR('EV-005')	:=	'Setup incorrect for lookup type "TAC_GL_ACCOUNT_COGS", either Account Code or Account Type must be specified.';
	CT_ERROR('EV-006')	:=	'Setup incorrect for lookup type "TAC_PO_PROJECT_ANAPLAN", Project Code must be specified.';
	CT_ERROR('EV-007')	:=	'Parameter error, at least 1 file must be generated (BS, PL or COGS).';
	CT_ERROR('FG-001')	:=	'File generation error';

END TAC_GL_ANAPLAN_OUTB_ITF_PKG;