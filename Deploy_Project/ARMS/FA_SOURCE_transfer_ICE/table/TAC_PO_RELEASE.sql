DROP VIEW APPS.TAC_PO_RELEASE;

/* Formatted on 4/7/2017 14:05:42 (QP5 v5.287) */
CREATE OR REPLACE FORCE VIEW APPS.TAC_PO_RELEASE
(
   ORG_ID,
   PO_HEADER_ID,
   LINE_NUM,
   PO_NUMBER
)
AS
   SELECT DISTINCT
          poh.org_id,
          POH.PO_HEADER_ID,
          pol.line_num,
             POH.SEGMENT1
          || DECODE (pra.RELEASE_NUM, NULL, NULL, '-')
          || pra.RELEASE_NUM
             po_number
     FROM PO_HEADERS_ALL POH,
          PO_LINES_ALL POL,
          PO_LINE_LOCATIONS_ALL PLL,
          PO_RELEASES_ALL PRA
    WHERE     1 = 1
          AND POH.PO_HEADER_ID = POL.PO_HEADER_ID
          AND POL.PO_LINE_ID = PLL.PO_LINE_ID
          AND POL.PO_HEADER_ID = PLL.PO_HEADER_ID
          AND PLL.PO_HEADER_ID = PRA.PO_HEADER_ID(+)
          AND PLL.PO_RELEASE_ID = PRA.PO_RELEASE_ID(+)
--and POH.SEGMENT1 like '1007002256%'
;
