
DECLARE
  l_schema VARCHAR2(30) := 'APPS'; -- Adjust as required.
BEGIN
   -- dbms_java.grant_permission( 'APPS', 'SYS:java.io.FilePermission', '/bin/sh', 'execute' );
  DBMS_JAVA.grant_permission(l_schema, 'java.io.FilePermission', '<<ALL FILES>>', 'read ,write, execute, delete');
  DBMS_JAVA.grant_permission(l_schema, 'SYS:java.lang.RuntimePermission', 'writeFileDescriptor', '');
  DBMS_JAVA.grant_permission(l_schema, 'SYS:java.lang.RuntimePermission', 'readFileDescriptor', '');
END;