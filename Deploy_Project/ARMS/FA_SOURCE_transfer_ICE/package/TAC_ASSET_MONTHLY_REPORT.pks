CREATE OR REPLACE PACKAGE APPS.TAC_ASSET_MONTHLY_REPORT AUTHID CURRENT_USER AS
--**********************************************************
-- FILE NAME: TAC_ASSET_MONTHLY.pls
-- DESCRIPTION: TAC Asset Monthly Report
-- AUTHOR     : RAPEEPORN  FONGPETCH
-- DATE       : 29-MAR-2017
--Initial Revision.
--
--**********************************************************
FUNCTION GET_FA_ASSET_ID(i_book         IN  FA_DEPRN_DETAIL.BOOK_TYPE_CODE%TYPE,
                        i_po_num        IN  PO_HEADERS_ALL.SEGMENT1%TYPE,
                        i_po_line       IN  PO_LINES_ALL.LINE_NUM%TYPE
                        ) RETURN NUMBER;
                        
FUNCTION GET_FA_COST(i_book         IN  FA_DEPRN_DETAIL.BOOK_TYPE_CODE%TYPE,
                        i_po_num        IN  PO_HEADERS_ALL.SEGMENT1%TYPE,
                        i_po_line       IN  PO_LINES_ALL.LINE_NUM%TYPE
                        ) RETURN NUMBER;

PROCEDURE GENERTAE_REPORT(o_errbuf        OUT VARCHAR2
                         ,o_retcode       OUT NUMBER
                         ,p_org_id        IN  NUMBER 
                         ,p_project       IN  NUMBER
                         ,p_book          IN  VARCHAR2 
                         
                         );
                        
END TAC_ASSET_MONTHLY_REPORT;
/