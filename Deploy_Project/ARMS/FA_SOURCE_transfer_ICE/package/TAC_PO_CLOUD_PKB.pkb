CREATE OR REPLACE PACKAGE BODY APPS.TAC_PO_CLOUD_PKB IS
/******************************************************************************
   NAME:       TAC_PO_CLOUD_PKB
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        3/30/2017   Kitiyakorn       1. Created this package.
******************************************************************************/
    PROCEDURE WRITE_LOG(pCHAR IN VARCHAR2,pTYPE IN VARCHAR2) IS
    BEGIN
       IF upper(pTYPE) = 'LOG' THEN
           fnd_file.put_LINE(FND_FILE.LOG,pCHAR );
       ELSE
           fnd_file.put_line(FND_FILE.OUTPUT,pCHAR );
       END IF;
    END WRITE_LOG;
    
    PROCEDURE get_directory_list(p_directory IN VARCHAR2) AS
        LANGUAGE JAVA NAME 'PocloudListDirectory.getList( java.lang.String )';
    
    procedure write_text_collect(p_text         IN tTemptextRec,
                                 gWriteText     IN OUT tTemptextTab) is

     BEGIN
     
         gWriteText.EXTEND;
         gWriteText(gWriteText.count).PO_NUMBER                := p_text.PO_NUMBER;
         gWriteText(gWriteText.count).PO_LINE_NUMBER           := p_text.PO_LINE_NUMBER; 
         gWriteText(gWriteText.count).PROJECT_CODE             := p_text.PROJECT_CODE; 
         gWriteText(gWriteText.count).CLOUD_VALUE_REGISTER     := p_text.CLOUD_VALUE_REGISTER;
         gWriteText(gWriteText.count).CLOUD_QTY_REGISTER       := p_text.CLOUD_QTY_REGISTER;
         gWriteText(gWriteText.count).BOQ_STATUS               := p_text.BOQ_STATUS;
         gWriteText(gWriteText.count).CREATION_DATE            := p_text.CREATION_DATE;
         gWriteText(gWriteText.count).CREATED_BY               := p_text.CREATED_BY;
         gWriteText(gWriteText.count).LAST_UPDATE_DATE         := p_text.LAST_UPDATE_DATE;
         gWriteText(gWriteText.count).LAST_UPDATED_BY          := p_text.LAST_UPDATED_BY;
         gWriteText(gWriteText.count).FILE_NAME                := p_text.FILE_NAME;
         gWriteText(gWriteText.count).STATUS                   := p_text.STATUS;
         gWriteText(gWriteText.count).COUNT_VALIDATE_STATUS    := p_text.COUNT_VALIDATE_STATUS;
         gWriteText(gWriteText.count).ERROR_MESSAGE            := p_text.ERROR_MESSAGE;
         
     end write_text_collect;

    PROCEDURE validate_text_process(p_text         IN     tTemptextRec,
                                  pWriteText     IN OUT tTemptextTab,
                                  p_status       OUT    number) is
        v_status                varchar2(1);
        v_error                 varchar2(500);
        v_count_status          number := 0;  
        l_po_line_num           number;
        l_cloud_value           number;
        l_cloud_qty             number;
        l_chk_digit             number;
        l_chk_project           number;
        l_project_code          varchar2(25);
        l_STATUS                varchar2(1);
        l_PO_NUMBER             varchar2(25);
        l_PO_LINE_NUMBER        number;
        l_CLOUD_VALUE_REGISTER  number;
        l_CLOUD_QTY_REGISTER    number;
        
        vPO_HEADER_ID  number;
    BEGIN
        --IF gWriteCollect.COUNT > 0 THEN
            --FOR i in 1..gWriteCollect.COUNT LOOP
                ---po number
                IF p_text.PO_NUMBER IS NULL THEN
                    v_status := 'N';
                    v_count_status := v_count_status + 1;
                    --v_error := 'PO_NUMBER not found in text file '||p_text.FILE_NAME; 
                    v_error := 'Error FA013-006: PO_NUMBER is null '; 
                ELSE
                    BEGIN
                        select length(p_text.PO_NUMBER)
                        into l_chk_digit
                        from dual;
                    END;
                    
                    IF l_chk_digit > 25 THEN
                        null;
                        /*v_status := 'N';
                        v_count_status := v_count_status + 1;
                       -- v_error := v_error||'/'||' longer than 25 digit of PO_NUMBER '||p_text.PO_LINE_NUMBER||' file name '||p_text.FILE_NAME;
                        v_error := v_error||','||'Error FA013-0010: longer than 25 digit of PO_NUMBER ';
                        */
                    ELSE
                        /*BEGIN
                            select segment1
                            into l_PO_NUMBER   ---
                            from po_headers_all
                            where segment1 = p_text.PO_NUMBER;
                        EXCEPTION WHEN NO_DATA_FOUND THEN
                            l_PO_NUMBER := null;
                            v_status := 'N';
                            v_count_status := v_count_status + 1;
                            --v_error := 'Error FA013-002 :PO Number  does not  exists  in erp '||p_text.PO_NUMBER || ' file name '||p_text.FILE_NAME;
                            v_error := v_error||','||'Error FA013-002: PO Number  does not  exists  in erp '; 
                            WHEN OTHERS THEN
                                l_PO_NUMBER := null;
                                v_status := 'N';
                                v_count_status := v_count_status + 1;
                                --v_error := v_error||'/'||' Error FA013-002 :PO Number  does not  exists  in erp '||p_text.PO_NUMBER|| ' file name '||p_text.FILE_NAME||' Error '|| sqlerrm;  
                                v_error := v_error||','||'Error FA013-002: PO Number  does not  exists  in erp ';   
                        END;*/
                        --ktp 20170624
                        vPO_HEADER_ID := null;
                        BEGIN
                            SELECT po_header_id,segment1
                            INTO vPO_HEADER_ID,l_PO_NUMBER
                            FROM PO_HEADERS_ALL 
                            WHERE SEGMENT1 = p_text.PO_NUMBER;
                        EXCEPTION  WHEN OTHERS THEN
                            null ;
                        END;
                        
                        
                        if vPO_HEADER_ID is null then    
                          begin 
                            SELECT poh.po_header_id,poh.segment1
                            INTO vPO_HEADER_ID,l_PO_NUMBER
                            FROM PO_HEADERS_ALL POH,
                            PO_LINES_ALL POL,
                            PO_LINE_LOCATIONS_ALL  PLL,
                            PO_RELEASES_ALL        PRA
                            WHERE POH.SEGMENT1 || decode(pra.RELEASE_NUM,null,null,'-') || pra.RELEASE_NUM = p_text.PO_NUMBER
                            AND POH.PO_HEADER_ID =  POL.PO_HEADER_ID
                            AND POL.PO_LINE_ID = PLL.PO_LINE_ID
                            AND PLL.PO_HEADER_ID = PRA.PO_HEADER_ID(+)
                            AND PLL.PO_RELEASE_ID = PRA.PO_RELEASE_ID(+);
                          EXCEPTION  WHEN OTHERS THEN
                            null ;
                          END;
                        end if;
                        
                        if vPO_HEADER_ID is null then
                                vPO_HEADER_ID := null;
                                l_PO_NUMBER := null;
                                v_status := 'N';
                                v_count_status := v_count_status + 1;
                                --v_error := v_error||'/'||' Error FA013-002 :PO Number  does not  exists  in erp '||p_text.PO_NUMBER|| ' file name '||p_text.FILE_NAME||' Error '|| sqlerrm;  
                                v_error := v_error||','||'Error FA013-002: PO Number  does not  exists  in erp ';   
                        end if;
                        
                        
                    END IF;
                END IF;
                
                ---po line
                IF p_text.PO_LINE_NUMBER IS NULL THEN
                    v_status := 'N';
                    v_count_status := v_count_status + 1;
                   -- v_error := v_error||'/'||' PO_LINE_NUMBER not found in text file '||p_text.FILE_NAME; 
                    v_error := v_error||','||'Error FA013-006: PO_LINE_NUMBER is null ';
                ELSE
                    BEGIN
                        select to_number(p_text.PO_LINE_NUMBER)
                        into l_po_line_num
                        from dual;
                    EXCEPTION WHEN OTHERS THEN
                        l_po_line_num := null;
                        v_status := 'N';
                        v_count_status := v_count_status + 1;
                        --v_error := v_error||'/'||' wrong format PO_LINE_NUMBER '||p_text.PO_LINE_NUMBER||' file name '||p_text.FILE_NAME; 
                        v_error := v_error||','||'Error FA013-011: wrong format PO_LINE_NUMBER '; 
                    END;
                    
                    IF l_po_line_num is not null THEN
                        BEGIN
                           /* select pol.line_num
                            into l_PO_LINE_NUMBER    ---
                            from po_headers_all poh,
                                 po_lines_all pol
                            where pol.po_header_id = poh.po_header_id
                            and poh.segment1 = l_PO_NUMBER
                            and pol.line_num = l_po_line_num;
                            */
                            select L.line_num
                            into l_PO_LINE_NUMBER 
                            FROM PO_HEADERS_ALL H,PO_LINES_ALL L 
                            WHERE H.PO_HEADER_ID = L.PO_HEADER_ID
                            AND H.PO_HEADER_ID = vPO_HEADER_ID
                            AND L.LINE_NUM = l_po_line_num;
                        EXCEPTION WHEN NO_DATA_FOUND THEN
                            l_PO_LINE_NUMBER := null;
                            v_status := 'N';
                            v_count_status := v_count_status + 1;
                            --v_error := v_error||'/'||'Error FA013-003: PO Line Number  does not  exists  in erp '||p_text.PO_LINE_NUMBER||' file name '||p_text.FILE_NAME;
                            v_error := v_error||','||'Error FA013-003: PO Line Number  does not  exists  in erp '; 
                            WHEN OTHERS THEN
                                l_PO_LINE_NUMBER := null;
                                v_status := 'N';
                                v_count_status := v_count_status + 1;
                               -- v_error := v_error||'/'||'Error FA013-003: PO Line Number  does not  exists  in erp '||p_text.PO_LINE_NUMBER||' file name '||p_text.FILE_NAME||' Error '|| sqlerrm;   
                                v_error := v_error||','||'Error FA013-003: PO Line Number  does not  exists  in erp ';   
                        END;
                        
                    END IF;
                END IF;
                
                --project code
                IF p_text.PROJECT_CODE IS NULL THEN
                    null;  
                    --not require
                   /* v_status := 'N';
                    v_count_status := v_count_status + 1;
                    --v_error := v_error||'/'||' PROJECT_CODE not found in text file '||p_text.FILE_NAME; 
                    v_error := v_error||','||'Error FA013-006: PROJECT_CODE is null '; 
                    */
                ELSE
                    BEGIN
                        select length(p_text.PROJECT_CODE)
                        into l_chk_digit
                        from dual;
                    END;
                    
                    IF l_chk_digit > 25 THEN
                        v_status := 'N';
                        v_count_status := v_count_status + 1;
                        --v_error := v_error||'/'||' longer than 25 digit of PROJECT_CODE '||p_text.PROJECT_CODE||' file name '||p_text.FILE_NAME;
                        v_error := v_error||','||'Error FA013-0010: longer than 25 digit of PROJECT_CODE ';
                    ELSE
                        BEGIN
                            select segment1
                            into l_project_code
                            from pa_projects_all
                            where segment1 = p_text.PROJECT_CODE;
                        EXCEPTION WHEN NO_DATA_FOUND THEN
                            l_project_code := null;
                            v_status := 'N';
                            v_count_status := v_count_status + 1;
                            --v_error := v_error||'/'||'Error FA013-003: Project Code does not  exists  in erp '||p_text.PROJECT_CODE||' file name '||p_text.FILE_NAME;
                            v_error := v_error||','||'Error FA013-003: Project Code does not  exists  in erp ';
                            WHEN OTHERS THEN
                                 l_project_code := null;
                                 v_status := 'N';
                                 v_count_status := v_count_status + 1;
                                 --v_error := v_error||'/'||'Error FA013-003: Project Code does not  exists  in erp '||p_text.PROJECT_CODE||' file name '||p_text.FILE_NAME||' Error '|| sqlerrm;
                                 v_error := v_error||','||'Error FA013-003: Project Code does not exists in erp ';
                        END;
                    END IF;
                    
                    IF l_project_code is not null THEN 
                        BEGIN
                            select distinct 1
                            into l_chk_project
                            from PO_DISTRIBUTIONS_ALL pod,
                                 PO_LINES_ALL pol,
                                 PO_HEADERS_ALL po,
                                 PA_PROJECTS_ALL pa
                            where pod.PO_HEADER_ID = pol.PO_HEADER_ID
                            --and   pod.PO_HEADER_ID = 102839
                            and   pod.PO_LINE_ID   = pol.PO_LINE_ID
                            and   pol.PO_HEADER_ID = po.PO_HEADER_ID
                            and   pod.PROJECT_ID   = pa.PROJECT_ID
                            and   po.SEGMENT1      = l_PO_NUMBER
                            and   pol.LINE_NUM     = l_PO_LINE_NUMBER
                            and   pa.SEGMENT1      = l_project_code;
                        EXCEPTION WHEN NO_DATA_FOUND THEN
                            v_status := 'N';
                            v_count_status := v_count_status + 1;
                           -- v_error := v_error||'/'||'Error FA013-004: Invalid Project Code '||p_text.PROJECT_CODE||' po line '||p_text.PO_LINE_NUMBER;
                            v_error := v_error||','||'Error FA013-004: Invalid Project Code';
                            WHEN OTHERS THEN
                                 v_status := 'N';
                                 v_count_status := v_count_status + 1;
                                 --v_error := v_error||'/'||'Error FA013-004: Invalid Project Code '||p_text.PROJECT_CODE||' po line '||p_text.PO_LINE_NUMBER||' Error '|| sqlerrm;
                                 v_error := v_error||','||'Error FA013-004: Invalid Project Code ';
                        END;
                    END IF;
                END IF;
                
                ---cloud value register
                IF p_text.CLOUD_VALUE_REGISTER IS NULL THEN
                    /*v_status := 'Y';
                    v_error := v_error||'/'||' CLOUD_VALUE_REGISTER not found in text file and allready assign value 0 '||p_text.FILE_NAME; 
                    l_CLOUD_VALUE_REGISTER := 0;  ---
                    */
                    v_count_status := v_count_status + 1;
                    v_status := 'N';
                    v_error := v_error||','||'Error FA013-006: CLOUD_VALUE_REGISTER is null'; 
                    
                ELSE
                    BEGIN
                        select to_number(p_text.CLOUD_VALUE_REGISTER)
                        into l_CLOUD_VALUE_REGISTER --l_cloud_value
                        from dual;
                    EXCEPTION WHEN OTHERS THEN
                        /*l_cloud_value := 0;
                        v_error := v_error||'/'||' wrong format CLOUD_VALUE_REGISTERr and allready assign value 0 '||p_text.CLOUD_VALUE_REGISTER|| ' file name '||p_text.FILE_NAME;
                        l_CLOUD_VALUE_REGISTER := 0;  ---
                        v_status := 'Y';  
                        */
                        v_count_status := v_count_status + 1;
                        v_error := v_error||','||'Error FA013-011: wrong format CLOUD_VALUE_REGISTER';
                        v_status := 'N';  
                    END;
                END IF;
                
                --cloud qty register
                IF p_text.CLOUD_QTY_REGISTER IS NULL THEN
                    --v_status := 'Y';                    
                    --v_error := v_error||'/'||' CLOUD_QTY_REGISTER not found in text file and allready assign value 0 '||p_text.FILE_NAME;                     
                    --l_CLOUD_QTY_REGISTER := 0;  ---
                    v_count_status := v_count_status + 1;
                    v_status := 'N';
                    v_error := v_error||','||'Error FA013-006: CLOUD_QTY_REGISTER is null'; 
                ELSE
                    BEGIN
                        select to_number(p_text.CLOUD_QTY_REGISTER)
                        into l_CLOUD_QTY_REGISTER --l_cloud_qty
                        from dual;
                    EXCEPTION WHEN OTHERS THEN
                        /*l_cloud_qty := 0;
                        l_CLOUD_QTY_REGISTER := 0;  --
                        v_status := 'Y';
                        v_error := v_error||'/'||' wrong format CLOUD_QTY_REGISTER and allready assign value 0 '||p_text.CLOUD_QTY_REGISTER||' file name '||p_text.FILE_NAME;  
                        */
                        v_count_status := v_count_status + 1;
                        v_status := 'N';
                        v_error := v_error||','||'Error FA013-011: wrong format CLOUD_QTY_REGISTER';  
                    END;
                END IF;
                
                --boq status
                IF p_text.BOQ_STATUS IS NULL THEN
                    v_status := 'N';
                    v_count_status := v_count_status + 1;
                   -- v_error := v_error||'/'||' BOQ_STATUS not found in text file '||p_text.FILE_NAME; 
                    v_error := v_error||','||'Error FA013-006: BOQ_STATUS is null '; 
                ELSE
                   /* BEGIN
                        select length(p_text.BOQ_STATUS)
                        into l_chk_digit
                        from dual;
                    END;
                    
                    IF l_chk_digit > 30 THEN
                        v_status := 'N';
                        v_count_status := v_count_status + 1;
                        v_error := v_error||'/'||' longer than 25 digit of BOQ_STATUS '||p_text.BOQ_STATUS||' file name '||p_text.FILE_NAME;
                    END IF;*/
                    
                    if p_text.BOQ_STATUS not in ('Y','N') then
                        v_status := 'N';
                        v_count_status := v_count_status + 1;
                        v_error := v_error||','||'Error FA013-011: wrong format BOQ STATUS ';
                    end if;
    
                END IF;
                
                IF v_count_status > 0 THEN
                    l_STATUS := 'E';   ---
                ELSE
                    l_STATUS := 'S';   ---
                END IF;
                
                pWriteText.EXTEND;
                pWriteText(pWriteText.count).PO_NUMBER                := l_PO_NUMBER;
                pWriteText(pWriteText.count).PO_LINE_NUMBER           := l_PO_LINE_NUMBER; 
                pWriteText(pWriteText.count).PROJECT_CODE             := l_PROJECT_CODE; 
                pWriteText(pWriteText.count).CLOUD_VALUE_REGISTER     := l_CLOUD_VALUE_REGISTER;
                pWriteText(pWriteText.count).CLOUD_QTY_REGISTER       := l_CLOUD_QTY_REGISTER;
                pWriteText(pWriteText.count).BOQ_STATUS               := p_text.BOQ_STATUS;
                pWriteText(pWriteText.count).CREATION_DATE            := p_text.CREATION_DATE;
                pWriteText(pWriteText.count).CREATED_BY               := p_text.CREATED_BY;
                pWriteText(pWriteText.count).LAST_UPDATE_DATE         := p_text.LAST_UPDATE_DATE;
                pWriteText(pWriteText.count).LAST_UPDATED_BY          := p_text.LAST_UPDATED_BY;
                pWriteText(pWriteText.count).FILE_NAME                := p_text.FILE_NAME;
                pWriteText(pWriteText.count).STATUS                   := l_STATUS;
                pWriteText(pWriteText.count).COUNT_VALIDATE_STATUS    := v_count_status;
                pWriteText(pWriteText.count).ERROR_MESSAGE            := v_error;
                
                p_status := v_count_status;
                --l_ERROR_MESSAGE := v_error;
            --END LOOP;
        --END IF;
    END validate_text_process;
   
    PROCEDURE validate_process(gWriteCollect     IN OUT tTemptextTab,v_count_status  OUT NUMBER) IS
        v_status            varchar2(1);
        v_error             varchar2(500);
        --v_count_status      number := 0;  
        l_po_line_num       number;
        l_cloud_value       number;
        l_cloud_qty         number;
        l_chk_digit         number;
        l_chk_project       number;
        l_project_code      varchar2(25);
        vPO_HEADER_ID       number;
    BEGIN
        IF gWriteCollect.COUNT > 0 THEN
            FOR i in 1..gWriteCollect.COUNT LOOP
                ---po number
                IF gWriteCollect(i).PO_NUMBER IS NULL THEN
                    v_status := 'N';
                    v_count_status := v_count_status + 1;
                    v_error := 'PO_NUMBER not found in text file '||gWriteCollect(i).FILE_NAME; 
                ELSE
                    BEGIN
                        select length(gWriteCollect(i).PO_NUMBER)
                        into l_chk_digit
                        from dual;
                    END;
                    
                    IF l_chk_digit > 25 THEN
                        /*v_status := 'N';
                        v_count_status := v_count_status + 1;
                        v_error := v_error||'/'||' longer than 25 digit of PO_NUMBER '||gWriteCollect(i).PO_LINE_NUMBER||' file name '||gWriteCollect(i).FILE_NAME;
                         */
                         null;
                    ELSE
                        /*BEGIN
                            select segment1
                            into gWriteCollect(i).PO_NUMBER
                            from po_headers_all
                            where segment1 = gWriteCollect(i).PO_NUMBER;
                        EXCEPTION WHEN NO_DATA_FOUND THEN
                            v_status := 'N';
                            v_count_status := v_count_status + 1;
                            v_error := 'Error FA013-002 :PO Number  does not  exists  in erp '||gWriteCollect(i).PO_NUMBER || ' file name '||gWriteCollect(i).FILE_NAME;
                            WHEN OTHERS THEN
                                v_status := 'N';
                                v_count_status := v_count_status + 1;
                                v_error := v_error||'/'||' Error FA013-002 :PO Number  does not  exists  in erp '||gWriteCollect(i).PO_NUMBER|| ' file name '||gWriteCollect(i).FILE_NAME||' Error '|| sqlerrm;   
                        END;*/
                        
                        --ktp 20170624
                        vPO_HEADER_ID := null;
                        BEGIN
                            SELECT po_header_id,segment1
                            INTO vPO_HEADER_ID,gWriteCollect(i).PO_NUMBER
                            FROM PO_HEADERS_ALL 
                            WHERE SEGMENT1 = gWriteCollect(i).PO_NUMBER;
                        EXCEPTION  WHEN OTHERS THEN
                            null ;
                        END;
                        
                        
                        if vPO_HEADER_ID is null then    
                          begin 
                            SELECT poh.po_header_id,poh.segment1
                            INTO vPO_HEADER_ID,gWriteCollect(i).PO_NUMBER
                            FROM PO_HEADERS_ALL POH,
                            PO_LINES_ALL POL,
                            PO_LINE_LOCATIONS_ALL  PLL,
                            PO_RELEASES_ALL        PRA
                            WHERE POH.SEGMENT1 || decode(pra.RELEASE_NUM,null,null,'-') || pra.RELEASE_NUM = gWriteCollect(i).PO_NUMBER
                            AND POH.PO_HEADER_ID =  POL.PO_HEADER_ID
                            AND POL.PO_LINE_ID = PLL.PO_LINE_ID
                            AND PLL.PO_HEADER_ID = PRA.PO_HEADER_ID(+)
                            AND PLL.PO_RELEASE_ID = PRA.PO_RELEASE_ID(+);
                          EXCEPTION  WHEN OTHERS THEN
                            null ;
                          END;
                        end if;
                        
                        if vPO_HEADER_ID is null then
                            v_status := 'N';
                            v_count_status := v_count_status + 1;
                            v_error := 'Error FA013-002 :PO Number  does not  exists  in erp '||gWriteCollect(i).PO_NUMBER || ' file name '||gWriteCollect(i).FILE_NAME;
                            
                        end if;
                        
                        
                    END IF;
                END IF;
                
                ---po line
                IF gWriteCollect(i).PO_LINE_NUMBER IS NULL THEN
                    v_status := 'N';
                    v_count_status := v_count_status + 1;
                    v_error := v_error||'/'||' PO_LINE_NUMBER not found in text file '||gWriteCollect(i).FILE_NAME; 
                ELSE
                    BEGIN
                        select to_number(gWriteCollect(i).PO_LINE_NUMBER)
                        into l_po_line_num
                        from dual;
                    EXCEPTION WHEN OTHERS THEN
                        l_po_line_num := null;
                        v_status := 'N';
                        v_count_status := v_count_status + 1;
                        v_error := v_error||'/'||' wrong format PO_LINE_NUMBER '||gWriteCollect(i).PO_LINE_NUMBER||' file name '||gWriteCollect(i).FILE_NAME; 
                    END;
                    
                    IF l_po_line_num is not null THEN
                        BEGIN
                           /* select pol.line_num
                            into gWriteCollect(i).PO_NUMBER
                            from po_headers_all poh,
                                 po_lines_all pol
                            where pol.po_header_id = poh.po_header_id
                            and poh.segment1 = gWriteCollect(i).PO_NUMBER
                            and pol.line_num = l_po_line_num;*/
                            
                          select L.line_num
                            into gWriteCollect(i).PO_LINE_NUMBER 
                            FROM PO_HEADERS_ALL H,PO_LINES_ALL L 
                            WHERE H.PO_HEADER_ID = L.PO_HEADER_ID
                            AND H.PO_HEADER_ID = vPO_HEADER_ID
                            AND L.LINE_NUM = l_po_line_num;
                        EXCEPTION WHEN NO_DATA_FOUND THEN
                            v_status := 'N';
                            v_count_status := v_count_status + 1;
                            v_error := v_error||'/'||'Error FA013-003: PO Line Number  does not  exists  in erp '||gWriteCollect(i).PO_LINE_NUMBER||' file name '||gWriteCollect(i).FILE_NAME;
                            WHEN OTHERS THEN
                                v_status := 'N';
                                v_count_status := v_count_status + 1;
                                v_error := v_error||'/'||'Error FA013-003: PO Line Number  does not  exists  in erp '||gWriteCollect(i).PO_LINE_NUMBER||' file name '||gWriteCollect(i).FILE_NAME||' Error '|| sqlerrm;   
                        END;
                    END IF;
                END IF;
                
                --project code
                IF gWriteCollect(i).PROJECT_CODE IS NULL THEN
                    v_status := 'N';
                    v_count_status := v_count_status + 1;
                    v_error := v_error||'/'||' PROJECT_CODE not found in text file '||gWriteCollect(i).FILE_NAME; 
                ELSE
                    BEGIN
                        select length(gWriteCollect(i).PROJECT_CODE)
                        into l_chk_digit
                        from dual;
                    END;
                    
                    IF l_chk_digit > 25 THEN
                        v_status := 'N';
                        v_count_status := v_count_status + 1;
                        v_error := v_error||'/'||' longer than 25 digit of PROJECT_CODE '||gWriteCollect(i).PROJECT_CODE||' file name '||gWriteCollect(i).FILE_NAME;
                    ELSE
                        BEGIN
                            select segment1
                            into l_project_code
                            from pa_projects_all
                            where segment1 = gWriteCollect(i).PROJECT_CODE;
                        EXCEPTION WHEN NO_DATA_FOUND THEN
                            v_status := 'N';
                            v_count_status := v_count_status + 1;
                            v_error := v_error||'/'||'Error FA013-003: Project Code does not  exists  in erp '||gWriteCollect(i).PROJECT_CODE||' file name '||gWriteCollect(i).FILE_NAME;
                            WHEN OTHERS THEN
                                 v_status := 'N';
                                 v_count_status := v_count_status + 1;
                                 v_error := v_error||'/'||'Error FA013-003: Project Code does not  exists  in erp '||gWriteCollect(i).PROJECT_CODE||' file name '||gWriteCollect(i).FILE_NAME||' Error '|| sqlerrm;
                        END;
                        
                        BEGIN
                            select distinct 1
                            into l_chk_project
                            from PO_DISTRIBUTIONS_ALL pod,
                                 PO_LINES_ALL pol,
                                 PO_HEADERS_ALL po,
                                 PA_PROJECTS_ALL pa
                            where pod.PO_HEADER_ID = pol.PO_HEADER_ID
                            and   pod.PO_HEADER_ID = 102839
                            and   pod.PO_LINE_ID   = pol.PO_LINE_ID
                            and   pol.PO_HEADER_ID = po.PO_HEADER_ID
                            and   pod.PROJECT_ID   = pa.PROJECT_ID
                            and   po.SEGMENT1      = gWriteCollect(i).PO_NUMBER
                            and   pol.LINE_NUM     = to_number(gWriteCollect(i).PO_LINE_NUMBER)
                            and   pa.SEGMENT1      = gWriteCollect(i).PROJECT_CODE;
                        EXCEPTION WHEN NO_DATA_FOUND THEN
                            v_status := 'N';
                            v_count_status := v_count_status + 1;
                            v_error := v_error||'/'||'Error FA013-004: Invalid Project Code '||gWriteCollect(i).PROJECT_CODE||' po line '||gWriteCollect(i).PO_LINE_NUMBER;
                            WHEN OTHERS THEN
                                 v_status := 'N';
                                 v_count_status := v_count_status + 1;
                                 v_error := v_error||'/'||'Error FA013-004: Invalid Project Code '||gWriteCollect(i).PROJECT_CODE||' po line '||gWriteCollect(i).PO_LINE_NUMBER||' Error '|| sqlerrm;
                        END;
                    END IF;
                END IF;
                
                ---cloud value register
                IF gWriteCollect(i).CLOUD_VALUE_REGISTER IS NULL THEN
                    v_status := 'Y';
                    v_error := v_error||'/'||' CLOUD_VALUE_REGISTER not found in text file and allready assign value 0 '||gWriteCollect(i).FILE_NAME; 
                    gWriteCollect(i).CLOUD_VALUE_REGISTER := 0;
                ELSE
                    BEGIN
                        select to_number(gWriteCollect(i).CLOUD_VALUE_REGISTER)
                        into l_cloud_value
                        from dual;
                    EXCEPTION WHEN OTHERS THEN
                        l_cloud_value := 0;
                        v_error := v_error||'/'||' wrong format CLOUD_VALUE_REGISTERr and allready assign value 0 '||gWriteCollect(i).CLOUD_VALUE_REGISTER|| ' file name '||gWriteCollect(i).FILE_NAME;
                        gWriteCollect(i).CLOUD_VALUE_REGISTER := 0;
                        v_status := 'Y';  
                    END;
                END IF;
                
                --cloud qty register
                IF gWriteCollect(i).CLOUD_QTY_REGISTER IS NULL THEN
                    v_status := 'Y';
                    v_error := v_error||'/'||' CLOUD_QTY_REGISTER not found in text file and allready assign value 0 '||gWriteCollect(i).FILE_NAME; 
                    gWriteCollect(i).CLOUD_QTY_REGISTER := 0;
                ELSE
                    BEGIN
                        select to_number(gWriteCollect(i).CLOUD_QTY_REGISTER)
                        into l_cloud_qty
                        from dual;
                    EXCEPTION WHEN OTHERS THEN
                        l_cloud_qty := 0;
                        gWriteCollect(i).CLOUD_QTY_REGISTER := 0;
                        v_status := 'Y';
                        v_error := v_error||'/'||' wrong format CLOUD_QTY_REGISTER and allready assign value 0 '||gWriteCollect(i).CLOUD_QTY_REGISTER||' file name '||gWriteCollect(i).FILE_NAME;  
                    END;
                END IF;
                
                --boq status
                IF gWriteCollect(i).BOQ_STATUS IS NULL THEN
                    v_status := 'N';
                    v_count_status := v_count_status + 1;
                    v_error := v_error||'/'||' BOQ_STATUS not found in text file '||gWriteCollect(i).FILE_NAME; 
                ELSE
                    BEGIN
                        select length(gWriteCollect(i).BOQ_STATUS)
                        into l_chk_digit
                        from dual;
                    END;
                    
                    IF l_chk_digit > 30 THEN
                        v_status := 'N';
                        v_count_status := v_count_status + 1;
                        v_error := v_error||'/'||' longer than 25 digit of BOQ_STATUS '||gWriteCollect(i).BOQ_STATUS||' file name '||gWriteCollect(i).FILE_NAME;
                    END IF;
                END IF;
                IF v_count_status > 0 THEN
                    gWriteCollect(i).STATUS := 'E';
                ELSE
                    gWriteCollect(i).STATUS := 'S';
                END IF;
                gWriteCollect(i).COUNT_VALIDATE_STATUS := v_count_status;
                gWriteCollect(i).ERROR_MESSAGE := v_error;
            END LOOP;
        END IF;
    END validate_process;
    
    PROCEDURE INSERT_PROCESS(gWriteText     IN OUT tTemptextTab,p_status_insert  OUT VARCHAR2) is
        l_chk_dup       number := 0;
    BEGIN
        IF gWriteText.count > 0 THEN
             FOR i in 1..gWriteText.count LOOP
                 BEGIN
                    select distinct 1
                    into l_chk_dup
                    from TAC_PO_CLOUD_INFO
                    where PO_NUMBER             = gWriteText(i).PO_NUMBER
                    and PO_LINE_NUMBER          = gWriteText(i).PO_LINE_NUMBER;
                 EXCEPTION WHEN NO_DATA_FOUND THEN
                    l_chk_dup := 0;
                    WHEN OTHERS THEN
                        l_chk_dup := 0;
                 END;
                
                 /*write_log('l_chk_dup' || l_chk_dup,'log');
                 write_log('PO_NUMBER=' || gWriteText(i).PO_NUMBER,'log');
                 write_log('PO_LINE_NUMBER=' || gWriteText(i).PO_LINE_NUMBER,'log');
                 write_log('PROJECT_CODE=' || gWriteText(i).PROJECT_CODE,'log');
                 write_log('CLOUD_VALUE_REGISTER=' || gWriteText(i).CLOUD_VALUE_REGISTER,'log');
                 write_log('CLOUD_QTY_REGISTER=' || gWriteText(i).CLOUD_QTY_REGISTER,'log');
                 write_log('BOQ_STATUS=' || gWriteText(i).BOQ_STATUS,'log');*/

                 
                 IF l_chk_dup = 0 THEN
                     insert into apps.TAC_PO_CLOUD_INFO(PO_NUMBER,
                                                        PO_LINE_NUMBER,
                                                        PROJECT_CODE,
                                                        CLOUD_VALUE_REGISTER,
                                                        CLOUD_QTY_REGISTER,
                                                        BOQ_STATUS,
                                                        CREATION_DATE,
                                                        CREATED_BY,
                                                        LAST_UPDATE_DATE,
                                                        LAST_UPDATED_BY
                                                        )
                                                 values (gWriteText(i).PO_NUMBER,
                                                         gWriteText(i).PO_LINE_NUMBER,
                                                         gWriteText(i).PROJECT_CODE,
                                                         gWriteText(i).CLOUD_VALUE_REGISTER,
                                                         gWriteText(i).CLOUD_QTY_REGISTER,
                                                         gWriteText(i).BOQ_STATUS,
                                                         sysdate,
                                                         FND_PROFILE.VALUE('USER_ID'),
                                                         sysdate,
                                                         FND_PROFILE.VALUE('USER_ID')
                                                        );
                 ELSIF l_chk_dup = 1 THEN
                    update apps.TAC_PO_CLOUD_INFO
                    set PROJECT_CODE           = gWriteText(i).PROJECT_CODE,
                        CLOUD_VALUE_REGISTER   = gWriteText(i).CLOUD_VALUE_REGISTER,
                        CLOUD_QTY_REGISTER     = gWriteText(i).CLOUD_QTY_REGISTER,
                        BOQ_STATUS             = gWriteText(i).BOQ_STATUS,
                        LAST_UPDATE_DATE       = sysdate,
                        LAST_UPDATED_BY        = FND_PROFILE.VALUE('USER_ID')
                    where PO_NUMBER = gWriteText(i).PO_NUMBER
                    and PO_LINE_NUMBER = gWriteText(i).PO_LINE_NUMBER;
                 END IF;
             END LOOP;
             COMMIT;
             p_status_insert := 'S';
        END IF;
    EXCEPTION WHEN OTHERS THEN
        p_status_insert := 'U';
        write_log('Canot insert or update ' || sqlerrm,'LOG');
    END;
    
    PROCEDURE write_log_process(gWriteText     IN tTemptextTab,
                                p_concurrent   IN VARCHAR2,
                                p_request_id   IN NUMBER,
                                p_filename     IN VARCHAR2,
                                p_host_name    IN VARCHAR2,
                                p_starttime    IN DATE,
                                p_total        IN NUMBER,
                                p_validate     IN NUMBER) is
        l_count_rec    number := 0;  
        l_complete number := 0;   
        l_error number := 0;
    BEGIN
        IF gWriteText.count > 0 THEN
            --write_log('Begin date time|End date time|Host|Job name|Dest file|Num record|Po number|Po line number|Project code|Status|Error message','LOG');
            FOR i in 1..gWriteText.count LOOP
                l_count_rec := l_count_rec + 1;
                write_log(p_starttime||'|'||sysdate||'|'||p_host_name||'|'||p_concurrent||'|'||p_filename||'|'||l_count_rec||'|'||
                          gWriteText(i).PO_NUMBER||'|'||gWriteText(i).PO_LINE_NUMBER||'|'||gWriteText(i).PROJECT_CODE||'|'||
                          gWriteText(i).STATUS||'|'||gWriteText(i).ERROR_MESSAGE,'LOG');
                          
                if upper(gWriteText(i).STATUS) = 'E' then
                   l_error := l_error +1;
                end if;
            END LOOP;
            
            l_complete := nvl(p_total,0) - nvl(l_error,0);
            write_log(' ','LOG');
            write_log('======================================================================================================================================','LOG');
            write_log('TOTAL RECORD = '||p_total,'LOG');
            write_log('COMPLETED = '|| l_complete,'LOG');
            --write_log('ERROR = '||p_validate,'LOG');
            write_log('ERROR = '||l_error,'LOG');
        END IF;
    EXCEPTION WHEN OTHERS THEN
        null;
    end write_log_process;
    
    FUNCTION get_host_name RETURN VARCHAR2 IS
        l_host_name     varchar2(20);
    BEGIN
        select UTL_INADDR.GET_HOST_NAME into l_host_name from dual;
        return l_host_name;
    END;
    
    FUNCTION FileExists(p_DirName in varchar2,p_FileName in varchar2) RETURN NUMBER IS
      l_fexists boolean;
      l_flen   number;
      l_bsize  number;
      l_res    number(1);
    BEGIN
      l_res := 0;
      utl_file.fgetattr(upper(p_DirName), p_FileName, l_fexists, l_flen, l_bsize);
      IF l_fexists THEN
        l_res := 1;
      ELSE
        l_res := 0;
      END IF; 
      RETURN l_res;
    END FileExists;
    
    FUNCTION get_concurrent_id(l_request_id    in number) RETURN NUMBER IS
        p_request_id   number;
    BEGIN
        select a.REQUEST_ID
        into p_request_id
        from fnd_concurrent_requests a
        where a.REQUEST_ID = l_request_id;
        return p_request_id;
    EXCEPTION WHEN NO_DATA_FOUND THEN
        return null; 
        WHEN OTHERS THEN
        return null; 
    END;
    
    PROCEDURE get_concurrent_path(l_request_id    in number,l_path_name out  varchar2,l_file_name  out varchar2) IS
    BEGIN
        select trim(substr(LOGFILE_NAME,1,instr(LOGFILE_NAME,'/',-1,1)-1)) LOGFILE_PATH,trim(substr(LOGFILE_NAME,instr(LOGFILE_NAME,'/',-1,1)+1)) LOGFILE_NAME
        into l_path_name,l_file_name
        from fnd_concurrent_requests a ,
             fnd_concurrent_programs_vl b
        where a.REQUEST_ID = l_request_id;
    EXCEPTION WHEN NO_DATA_FOUND THEN
        l_path_name := null; 
        l_file_name := null;
        WHEN OTHERS THEN
        l_path_name := null; 
        l_file_name := null;
    END get_concurrent_path;
    
    PROCEDURE manage_file(p_base_directory   IN VARCHAR2,p_destination_directory   IN VARCHAR2,p_file_name   IN VARCHAR2) IS
    BEGIN
        UTL_FILE.FCOPY (p_base_directory,p_file_name,p_destination_directory,p_file_name);
        UTL_FILE.FREMOVE (p_base_directory,p_file_name);
    EXCEPTION WHEN OTHERS THEN
        write_log('Error manage_file '||sqlerrm,'LOG');
    END manage_file;
    
    PROCEDURE copy_log_file_process(p_base_directory   IN VARCHAR2,p_destination_directory   IN VARCHAR2,p_file_name   IN VARCHAR2) IS
    BEGIN
        UTL_FILE.FCOPY (p_base_directory,p_file_name,p_destination_directory,p_file_name);
    EXCEPTION WHEN OTHERS THEN
        write_log('Error manage_file '||sqlerrm,'LOG');
    END copy_log_file_process;
    
    
    FUNCTION extrack_data(p_buffer IN varchar2
                        ,p_delimiter IN varchar2
                        ,p_pos_data IN number
                        ) RETURN varchar2 IS
 v_pos_start number;
 v_pos_end number;
 v_subsrt_len number;
 v_data varchar2(255);
 
BEGIN
    IF p_pos_data = 1 THEN
        v_pos_start := 1;
    ELSE
        v_pos_start := INSTR(p_buffer, p_delimiter, 1, p_pos_data - 1) + 1;
    END IF;

        v_pos_end := INSTR(p_buffer, p_delimiter, 1, p_pos_data);

    IF v_pos_end = 0 THEN
        v_subsrt_len := LENGTH(p_buffer);
    ELSE
        v_subsrt_len := v_pos_end - v_pos_start;
    END IF;
        
        v_data := SUBSTR(p_buffer, v_pos_start, v_subsrt_len);

    
    RETURN REPLACE(REPLACE(TRIM(v_data), CHR(10), ''),CHR(13),'');

EXCEPTION
 WHEN OTHERS THEN
   --write_log('[ERR] : @extrack_data : POS' || p_pos_data || ' : ' || SQLERRM,'log');
   RETURN NULL;
END extrack_data;
    
    PROCEDURE TAC_PO_CLOUD_PROCESS(errbuf            OUT VARCHAR2,
                                   retcode           OUT NUMBER,
                                   p_pocloud_inbox   IN  VARCHAR2,
                                   p_pocloud_archive IN  VARCHAR2,
                                   p_pocloud_error   IN  VARCHAR2) IS
        CURSOR C1 IS
        select filename
        from tac_pocloud_inbox
        order by filename;
        
        l_destFile              utl_file.file_type;
        l_path                  dba_directories.directory_path%TYPE;
        G_POCLOUD_INBOX         VARCHAR2(100) := p_pocloud_inbox; --'TAC_FA_POCLOUD_INBOX';
        G_POCLOUD_ARCHIVE       VARCHAR2(100) := p_pocloud_archive; --'TAC_FA_POCLOUD_ARCHIVE';
        G_POCLOUD_ERROR         VARCHAR2(100) := p_pocloud_error; --'TAC_FA_POCLOUD_ERROR';
        
        l_file                  varchar2(32767); 
        l_dilimited             varchar2(1) := '|';
        pointer_fr              number;
        pointer_to              number;
        
        l_concurrent            varchar2(100) := 'TAC: Interface PO Cloud Value';
        l_request_id            number;
        l_host_name             varchar2(20) := get_host_name;
        l_text_collect          tTemptextRec;--TAC_PO_CLOUD_INFO%ROWTYPE; 
        l_WriteText             tTemptextTab := tTemptextTab();                   
        l_status                varchar2(1) := null;
        l_user_id               number;
        l_starttime             date;
        l_total                 number;
        l_count_status          number;
        l_chk_file_path         varchar2(1) := null;
        l_error_message         varchar2(500) := null;
        l_status_insert         varchar2(1) := null;
        l_dir_inbox varchar2(100);
        l_dir_archive varchar2(100);
        l_dir_error varchar2(100);
        v_cnt_inbox number := 0;
        
    BEGIN
    
      --  write_log('start process','log');
        l_error_message := null;
        l_request_id := fnd_global.CONC_REQUEST_ID;
        l_user_id    := to_number(FND_PROFILE.VALUE('USER_ID'));
        l_starttime  := SYSDATE;
        
      --  write_log('before get_directory_list','log');
        DELETE FROM TAC_POCLOUD_INBOX;
        COMMIT;
        
       begin
          get_directory_list(G_POCLOUD_INBOX);
       exception when others then
           l_error_message := l_error_message||' /'||'Error FA013-007 : Not found Path '||G_POCLOUD_INBOX;
          
       end;

       INSERT INTO TAC_POCLOUD_INBOX (SELECT * FROM tac_pocloud_dir_list);
       COMMIT;
       
      l_dir_inbox := null;  
      l_dir_archive := null;  
      l_dir_error := null;      
 
      BEGIN
                select DIRECTORY_NAME --'Y'
                into l_dir_inbox --l_chk_file_path
                from dba_directories
                where DIRECTORY_PATH = G_POCLOUD_INBOX;
            EXCEPTION WHEN NO_DATA_FOUND THEN
                l_chk_file_path := null;
                l_dir_inbox := null;
                l_error_message := l_error_message||' /'||'Error FA013-007 : Not found Path '||G_POCLOUD_INBOX;
            END;
            
            BEGIN
                select DIRECTORY_NAME --'Y'
                into l_dir_archive --l_chk_file_path
                from dba_directories
                where DIRECTORY_PATH = G_POCLOUD_ARCHIVE;
            EXCEPTION WHEN NO_DATA_FOUND THEN
                l_chk_file_path := null;
                l_dir_archive := null;
                l_error_message := l_error_message||' /'||'Error FA013-007 : Not found Path '||G_POCLOUD_ARCHIVE;
            END;
            
            BEGIN
                select DIRECTORY_NAME --'Y'
                into l_dir_error --l_chk_file_path
                from dba_directories
                where DIRECTORY_PATH = G_POCLOUD_ERROR;
            EXCEPTION WHEN NO_DATA_FOUND THEN
                l_chk_file_path := null;
                l_dir_error := null;
                l_error_message := l_error_message||' /'||'Error FA013-007 : Not found Path '||G_POCLOUD_ERROR;
            END;
     

     if l_error_message is not null then
           
           l_error_message := sysdate || '|'
                   || sysdate || '|'
                   || l_host_name || '|'
                   || l_concurrent || '|'
                   || '' || '|'
                   || '' || '|'
                   || '' || '|'
                   || '' || '|'
                   || '' || '|'
                   || 'E' || '|'
                   || l_error_message
                   ;
          write_log(l_error_message,'LOG');
     else    

      begin 
       select count(*)
       into v_cnt_inbox 
       from TAC_POCLOUD_INBOX;
      exception when others then
         v_cnt_inbox := 0;
      end;
       
       
       
            
                IF v_cnt_inbox = 0 THEN
                   l_error_message := l_error_message||' /'|| 'Error FA013-008 : Zero File '
                   ;
                   
                end if;
                
                if l_error_message is not null then
                
                   l_error_message := sysdate || '|'
                   || sysdate || '|'
                   || l_host_name || '|'
                   || l_concurrent || '|'
                   || '' || '|'
                   || '' || '|'
                   || '' || '|'
                   || '' || '|'
                   || '' || '|'
                   || 'E' || '|'
                   || l_error_message
                   ;
                   
                   write_log(l_error_message,'LOG');
                
                end if;
       
        
       -- write_log('before loop','log');
        FOR i in c1 LOOP
            l_file := null;
            pointer_fr := 0;
            pointer_to := 0;
            l_total    := 0;
            l_count_status := 0;
            l_error_message := null;

            
          --  write_log('before file existing = ' || i.filename,'log');
            
            --IF FileExists(G_POCLOUD_INBOX,i.filename) = 0 THEN
            IF FileExists(l_dir_inbox,i.filename) = 0 THEN
                l_error_message := l_error_message||' /'||'Error FA013-008 : Zero File '||i.filename;
            
                IF l_error_message is not null THEN
                   write_log(l_concurrent,'LOG');
                   write_log(' ','LOG');
                   write_log(l_error_message,'LOG');
                ELSE
                   write_log_process(l_WriteText,l_concurrent,l_request_id,i.filename,l_host_name,l_starttime,l_total,l_count_status);
                END IF;
            
            else
            
              -- write_log('before file fopen','log');
              --  l_destFile := UTL_FILE.FOPEN(G_POCLOUD_INBOX,i.filename,'R');
              l_destFile := UTL_FILE.FOPEN(l_dir_inbox,i.filename,'R');
                 l_file := null;
               
               
            
            
            IF UTL_FILE.IS_OPEN(l_destFile) THEN
                LOOP
                    BEGIN
                       
                        --write_log('before read file','log');  
                       begin                       
                        UTL_FILE.get_line(l_destFile,l_file);
                        l_file := REPLACE(REPLACE(l_file, CHR(10), ''), CHR(13), '');
                       exception when others then
                          --write_log('error while get line','log');
                          EXIT;
                       end;
                        
                        /*IF l_file IS NULL THEN
                            --write_log('end file','log');  
                            --l_error_message := l_error_message||' /'||'Error FA013-009 : Cannot Read File  '||i.filename;
                            EXIT;
                        end if;
                        */
                        
                        IF l_file IS NULL THEN
                            null;
                        else
                          
                          begin
                            --write_log('l_total','log');
                            
                            l_total := l_total + 1;
                            
                            /*
                            pointer_fr := 1;
                            pointer_to := instr(l_file,l_dilimited,pointer_fr);
                            l_text_collect.PO_NUMBER               := substr(l_file,pointer_fr,pointer_to - pointer_fr);
                            pointer_to := instr(l_file,l_dilimited,pointer_to + 1);
                            l_text_collect.PO_LINE_NUMBER           := substr(l_file,pointer_to + 1,pointer_to - (pointer_to + 1));
                            pointer_to := instr(l_file,l_dilimited,pointer_to + 1);
                            l_text_collect.PROJECT_CODE             := substr(l_file,pointer_to + 1,pointer_to - (pointer_to + 1));
                            pointer_to := instr(l_file,l_dilimited,pointer_to + 1);
                            l_text_collect.CLOUD_VALUE_REGISTER     := substr(l_file,pointer_to + 1,pointer_to - (pointer_to + 1));
                            pointer_to := instr(l_file,l_dilimited,pointer_to + 1);
                            l_text_collect.CLOUD_QTY_REGISTER       := substr(l_file,pointer_to + 1,pointer_to - (pointer_to + 1));
                            pointer_to := instr(l_file,l_dilimited,pointer_to + 1);
                            l_text_collect.BOQ_STATUS               := substr(l_file,pointer_to + 1,pointer_to - (pointer_to + 1));
                             
                            pointer_to := instr(l_file,l_dilimited,pointer_to + 1);
                            l_text_collect.CREATION_DATE            := substr(l_file,pointer_to + 1,pointer_to - (pointer_to + 1));
                            pointer_to := instr(l_file,l_dilimited,pointer_to + 1);
                            l_text_collect.CREATED_BY               := substr(l_file,pointer_to + 1,pointer_to - (pointer_to + 1));
                            pointer_to := instr(l_file,l_dilimited,pointer_to + 1);
                            l_text_collect.LAST_UPDATE_DATE         := substr(l_file,pointer_to + 1,pointer_to - (pointer_to + 1));
                            pointer_to := instr(l_file,l_dilimited,pointer_to + 1);
                            l_text_collect.LAST_UPDATED_BY          := substr(l_file,pointer_to + 1,pointer_to - (pointer_to + 1));
                            */
                            
                            
                            
                           
                            l_text_collect.PO_NUMBER                := extrack_data(l_file,l_dilimited, 1);
                            l_text_collect.PO_LINE_NUMBER           := extrack_data(l_file,l_dilimited, 2);
                            l_text_collect.PROJECT_CODE             := extrack_data(l_file,l_dilimited, 3);
                            l_text_collect.CLOUD_VALUE_REGISTER     := extrack_data(l_file,l_dilimited, 4);
                            l_text_collect.CLOUD_QTY_REGISTER       := extrack_data(l_file,l_dilimited, 5);
                            l_text_collect.BOQ_STATUS               := extrack_data(l_file,l_dilimited, 6);
                            l_text_collect.CREATION_DATE            := sysdate;                            
                            l_text_collect.CREATED_BY               := FND_PROFILE.VALUE('USER_ID');                            
                            l_text_collect.LAST_UPDATE_DATE         := sysdate;                            
                            l_text_collect.LAST_UPDATED_BY          := FND_PROFILE.VALUE('USER_ID');                           
                            l_text_collect.FILE_NAME                := i.filename;
                            validate_text_process(l_text_collect,l_WriteText,l_count_status);
                            
                          
                            /*write_log('PO_NUMBER=' || l_text_collect.PO_NUMBER,'log');
                            write_log('PO_LINE_NUMBER=' || l_text_collect.PO_LINE_NUMBER,'log');
                            write_log('PROJECT_CODE=' || l_text_collect.PROJECT_CODE,'log');
                            write_log('CLOUD_VALUE_REGISTER=' || l_text_collect.CLOUD_VALUE_REGISTER,'log');
                            write_log('CLOUD_QTY_REGISTER=' || l_text_collect.CLOUD_QTY_REGISTER,'log');
                            write_log('boq status=' || l_text_collect.BOQ_STATUS,'log');*/
                            
                            --write_text_collect(l_text_collect,l_WriteText);
                            --validate_process(l_WriteText,l_count_status);
                          exception when others then
                              l_count_status := l_count_status + 1;
                              --write_log('error while l_text_collect','log');
                              l_error_message := sysdate || '|'
                                                || sysdate || '|'
                                                || l_host_name || '|'
                                                || l_concurrent || '|'
                                                || '' || '|'
                                                || '' || '|'
                                                || '' || '|'
                                                || '' || '|'
                                                || '' || '|'
                                                || 'E' || '|'
                                                || 'Error FA013-009 : Cannot Read File';
                   
                             -- write_log(l_error_message,'LOG');
                              
                              exit;
                          end; --end  l_text_collect
                       end if;  --end null record  
                    END;
                END LOOP;
                
            END IF;
           
            
          --  write_log('before l_count_status','log');
            
            IF l_count_status = 0 THEN
                 --20170622 delete before insert
                 delete from TAC_PO_CLOUD_INFO;
                 commit;
                 
                insert_process(l_WriteText,l_status_insert);
                IF l_status_insert = 'S' THEN
                    --manage_file(G_POCLOUD_INBOX,G_POCLOUD_ARCHIVE,i.filename);
                    manage_file(l_dir_inbox,l_dir_archive,i.filename);
                ELSE
                    --manage_file(G_POCLOUD_INBOX,G_POCLOUD_ERROR,i.filename);
                    manage_file(l_dir_inbox,l_dir_error,i.filename);
                END IF;
            ELSE
                --manage_file(G_POCLOUD_INBOX,G_POCLOUD_ERROR,i.filename);
                manage_file(l_dir_inbox,l_dir_error,i.filename);
            END IF;
            
            IF l_error_message is not null THEN
                --write_log(l_concurrent,'LOG');
                --write_log(' ','LOG');
                write_log(l_error_message,'LOG');
            ELSE
                write_log_process(l_WriteText,l_concurrent,l_request_id,i.filename,l_host_name,l_starttime,l_total,l_count_status);
            END IF;
          
           end if;
            
        END LOOP;
        
     end if; -- not found path   
        
        
    EXCEPTION WHEN OTHERS THEN
        write_log(l_concurrent,'LOG');
        write_log(' ','LOG');
        write_log('Exception','LOG');
        write_log(sqlerrm,'LOG');
        --errbuf := sqlerrm;
        retcode := 2;
    END TAC_PO_CLOUD_PROCESS;

PROCEDURE TAC_COPY_LOG_PROCESS(errbuf            OUT VARCHAR2,
                               retcode           OUT NUMBER,
                               p_request_id      IN  NUMBER,
                               p_destination     IN  VARCHAR2) IS
                               
        G_COPY_DESTINATION      varchar2(100) := p_destination;   --'TAC_COPY_LOG_SPLUNK';
        l_concurrent            varchar2(100) := 'TAC: Copy Log File To Splunk';
        l_request_id            number;
        l_user_id               number;
        l_starttime             date;
        l_chk_file_path         varchar2(1) := null;
        l_error_message         varchar2(500) := null;
        
        l_concurrent_id         number;
        l_concurrent_path       varchar2(150);
        l_concurrent_file_name  varchar2(150);
        v_count_status          number := 0;
        
    BEGIN
        l_request_id := fnd_global.CONC_REQUEST_ID;
        l_user_id    := to_number(FND_PROFILE.VALUE('USER_ID'));
        l_starttime  := SYSDATE;
        
        l_concurrent_id := get_concurrent_id(p_request_id);
        IF l_concurrent_id is null THEN
            v_count_status := v_count_status + 1;
            l_error_message := l_error_message||' /'||'Error GLOBAL002 : Not found Concurrent Requet ID '||p_request_id;
        END IF;
        
        get_concurrent_path(p_request_id,l_concurrent_path,l_concurrent_file_name);
        IF l_concurrent_path is null THEN
            v_count_status := v_count_status + 1;
            l_error_message := l_error_message||' /'||'Error GLOBAL001 : Not found Log File '||p_request_id;
        END IF;
        
        IF l_concurrent_file_name is null THEN
            v_count_status := v_count_status + 1;
            l_error_message := l_error_message||' /'||'Error GLOBAL001 : Not found Log File '||p_request_id;
        END IF;
        
        BEGIN
            select 'Y'
            into l_chk_file_path
            from dba_directories
            where DIRECTORY_PATH = G_COPY_DESTINATION;
        EXCEPTION WHEN NO_DATA_FOUND THEN
            l_chk_file_path := null;
            v_count_status := v_count_status + 1;
            l_error_message := l_error_message||' /'||'Error GLOBAL003 : Not found Folder '||G_COPY_DESTINATION;
        END;
            
        IF l_concurrent_path is not null and l_concurrent_file_name is not null THEN
            IF FileExists(l_concurrent_path,l_concurrent_file_name) = 0 THEN
                v_count_status := v_count_status + 1;
                l_error_message := l_error_message||' /'||'Error FA013-008 : Zero File '||l_concurrent_file_name;
            END IF;
        END IF;

        IF v_count_status = 0 THEN
            copy_log_file_process(l_concurrent_path,G_COPY_DESTINATION,l_concurrent_file_name);
        END IF;
        
        ----write log
        IF v_count_status = 0 THEN
            write_log(l_concurrent,'LOG');
            write_log(' ','LOG');
            write_log(' ','LOG');
            write_log('Parameter','LOG');
            write_log('Folder Splunk = '||G_COPY_DESTINATION,'LOG');
            write_log('RequestID = '||p_request_id,'LOG');
            write_log(' ','LOG');
            write_log('Status = '||'Complete','LOG');
        END IF;

    EXCEPTION WHEN OTHERS THEN
        write_log(l_concurrent,'LOG');
        write_log(' ','LOG');
        write_log(' ','LOG');
        write_log('Parameter','LOG');
        write_log('Folder Splunk = '||G_COPY_DESTINATION,'LOG');
        write_log('RequestID = '||p_request_id,'LOG');
        write_log(' ','LOG');
        write_log('Status = '||'Error','LOG');
        write_log(' ','LOG');
        write_log('Error Message','LOG');
        write_log('= = > '||l_concurrent_file_name,'LOG');
        write_log('= = > '||sqlerrm,'LOG');
        retcode := 2;
    END TAC_COPY_LOG_PROCESS;

END TAC_PO_CLOUD_PKB;
/

