CREATE OR REPLACE PACKAGE BODY APPS.TAC_FA_REPORT_PKG is

PROCEDURE WRITE_LOG (pCHAR IN VARCHAR2, pTYPE IN VARCHAR2)
   IS
   BEGIN
      IF UPPER (pTYPE) = 'LOG'
      THEN
         fnd_file.put_LINE (FND_FILE.LOG, pCHAR);
      ELSE
         fnd_file.put_line (FND_FILE.OUTPUT, pCHAR);
      END IF;

      DBMS_OUTPUT.put_line (Pchar);
   exception when others then
       DBMS_OUTPUT.put_line (sqlerrm);
   END WRITE_LOG;
   
FUNCTION CHECK_POADD_RP(P_PO_NUMBER IN VARCHAR2,
                        P_PO_LINE IN VARCHAR2
                         ) RETURN varchar2
is

V_DISPLAY VARCHAR2(1);

BEGIN

V_DISPLAY := null;

    BEGIN
        SELECT DISPLAY_REPORT
        INTO V_DISPLAY
        FROM TAC_PO_ADDITION_INFO
        WHERE PO_NUMBER = P_PO_NUMBER
        AND PO_LINE_NUMBER = P_PO_LINE
        AND TO_DATE(PERIOD_NAME,'MON-YY') = (SELECT MAX(to_date(PERIOD_NAME,'MON-YY'))
                                                FROM TAC_PO_ADDITION_INFO
                                                WHERE PO_NUMBER = P_PO_NUMBER
                                                AND PO_LINE_NUMBER = P_PO_LINE)
        ;
        /*AND LAST_UPDATE_DATE = (SELECT MAX(LAST_UPDATE_DATE)
                        FROM TAC_PO_ADDITION_INFO
                        WHERE PO_NUMBER = P_PO_NUMBER
                        AND PO_LINE_NUMBER = P_PO_LINE);*/
    EXCEPTION WHEN OTHERS THEN
       NULL;
    END;
                         
/*IF V_DISPLAY = 'Y'  THEN
   RETURN 'Y';
ELSE
   RETURN 'N';
END IF;
*/
RETURN V_DISPLAY;

EXCEPTION WHEN OTHERS THEN
    RETURN 'N';
END;

FUNCTION CHECK_DISPLAY_PROJECT(P_PO_NUMBER IN VARCHAR2,
                        P_PO_LINE IN VARCHAR2
                         ) RETURN varchar2
                         
is

V_DISPLAY VARCHAR2(1);

BEGIN

V_DISPLAY := null;

    BEGIN
        SELECT DISPLAY_PROJECT
        INTO V_DISPLAY
        FROM TAC_PO_ADDITION_INFO
        WHERE PO_NUMBER = P_PO_NUMBER
        AND PO_LINE_NUMBER = P_PO_LINE
        AND TO_DATE(PERIOD_NAME,'MON-YY') = (SELECT MAX(to_date(PERIOD_NAME,'MON-YY'))
                                                FROM TAC_PO_ADDITION_INFO
                                                WHERE PO_NUMBER = P_PO_NUMBER
                                                AND PO_LINE_NUMBER = P_PO_LINE)
        ;
    EXCEPTION WHEN OTHERS THEN
       NULL;
    END;
                         
RETURN V_DISPLAY;

EXCEPTION WHEN OTHERS THEN
    RETURN 'N';
END;

FUNCTION GET_SYSTEM_CODE(P_REQ_DIST_ID NUMBER) RETURN VARCHAR2
IS

V_SYSTEM_CODE VARCHAR2(100);


BEGIN


SELECT DISTINCT GLCC.SEGMENT21
INTO V_SYSTEM_CODE
FROM PO_REQUISITION_LINES_ALL PRL,
PO_REQ_DISTRIBUTIONS_ALL PRD,
GL_CODE_COMBINATIONS_KFV GLCC
WHERE PRL.REQUISITION_LINE_ID = PRD.REQUISITION_LINE_ID
AND PRD.DISTRIBUTION_ID = P_REQ_DIST_ID
AND PRD.CODE_COMBINATION_ID = GLCC.CODE_COMBINATION_ID
AND ROWNUM = 1;


 RETURN (V_SYSTEM_CODE);

EXCEPTION WHEN OTHERS THEN

RETURN NULL;

END;

FUNCTION GET_TYPE_OF_WORK(P_REQ_DIST_ID NUMBER,
                          P_PO_NUMBER VARCHAR2,
                          P_PO_LINE NUMBER) RETURN VARCHAR2

IS

V_TYPE_OF_WORK1 VARCHAR2(3);
V_TYPE_OF_WORK VARCHAR2(3);
v_po_header_id number;
v_po_line_id number;
v_line_num number;


BEGIN

    BEGIN
       SELECT TYPE_OF_WORK
       INTO V_TYPE_OF_WORK1
       FROM TAC_PO_ADDITION_INFO
       WHERE PO_NUMBER = P_PO_NUMBER
       AND PO_LINE_NUMBER = P_PO_LINE
       AND TO_DATE(PERIOD_NAME,'MON-YY') = (SELECT MAX(to_date(PERIOD_NAME,'MON-YY'))
                                                FROM TAC_PO_ADDITION_INFO
                                                WHERE PO_NUMBER = P_PO_NUMBER
                                                AND PO_LINE_NUMBER = P_PO_LINE);
     EXCEPTION WHEN OTHERS THEN
         V_TYPE_OF_WORK1 := null;
     END;


SELECT DISTINCT DECODE(upper(PRL.ATTRIBUTE7) ,'YES','t','NO','e','','u')
INTO V_TYPE_OF_WORK
FROM PO_REQUISITION_LINES_ALL PRL,
PO_REQ_DISTRIBUTIONS_ALL PRD
WHERE PRL.REQUISITION_LINE_ID = PRD.REQUISITION_LINE_ID
AND PRD.DISTRIBUTION_ID = P_REQ_DIST_ID;


  if V_TYPE_OF_WORK1 is not null then
      RETURN nvl(V_TYPE_OF_WORK1,'u');
  else
      RETURN nvl(V_TYPE_OF_WORK,'u');
  end if;

EXCEPTION WHEN OTHERS THEN

RETURN NULL;

END;

FUNCTION GET_PR_DESC(P_REQ_DIST_ID NUMBER) RETURN VARCHAR2
IS

V_DESC VARCHAR2(250);

BEGIN

        SELECT DISTINCT PRH.DESCRIPTION
        INTO V_DESC
        FROM PO_REQUISITION_HEADERS_ALL PRH,
        PO_REQUISITION_LINES_ALL PRL,
        PO_REQ_DISTRIBUTIONS_ALL PRD
        WHERE PRH.REQUISITION_HEADER_ID = PRL.REQUISITION_HEADER_ID
        AND PRL.REQUISITION_LINE_ID = PRD.REQUISITION_LINE_ID
        AND PRD.DISTRIBUTION_ID = P_REQ_DIST_ID;
        
        RETURN V_DESC;

EXCEPTION WHEN OTHERS THEN

RETURN NULL;

END;

FUNCTION GET_ACCOUNT_WIP(P_HEADER_ID IN NUMBER,
                         P_LINE_ID IN NUMBER,
                         P_PROJECT_ID IN NUMBER) RETURN VARCHAR2 IS
                         
V_ACCOUNT_WIP VARCHAR2(20);

BEGIN
   
if P_PROJECT_ID is not null then
    SELECT 
    case when GLCC.SEGMENT1 = '01' then
       NVL(SUBSTR(POD.EXPENDITURE_TYPE,1,5),GLCC.SEGMENT11) 
    else --NVL(SUBSTR(POD.EXPENDITURE_TYPE,1,5),GLCC.SEGMENT11)
       decode(NVL(SUBSTR(POD.EXPENDITURE_TYPE,1,5),GLCC.SEGMENT11),'17030','17028','18003','17028','18003','17028',NVL(SUBSTR(POD.EXPENDITURE_TYPE,1,5),GLCC.SEGMENT11)) 
    end ACCOUNT_WIP
    INTO V_ACCOUNT_WIP
    FROM PO_HEADERS_ALL POH,
     PO_LINES_ALL POL,
     PO_DISTRIBUTIONS_ALL  POD,
     GL_CODE_COMBINATIONS_KFV GLCC    
WHERE POH.PO_HEADER_ID = POL.PO_HEADER_ID
AND POL.PO_HEADER_ID = POD.PO_HEADER_ID
AND POL.PO_LINE_ID = POD.PO_LINE_ID 
AND POD.PO_HEADER_ID = P_HEADER_ID
AND POD.PO_LINE_ID = P_LINE_ID
AND POD.PROJECT_ID = P_PROJECT_ID
AND POD.BUDGET_ACCOUNT_ID = GLCC.CODE_COMBINATION_ID(+)
/*AND ( case when GLCC.SEGMENT1 = '01' then
       NVL(SUBSTR(POD.EXPENDITURE_TYPE,1,5),GLCC.SEGMENT11) 
    else 
       decode(NVL(SUBSTR(POD.EXPENDITURE_TYPE,1,5),GLCC.SEGMENT11),'17030','17028','18003','17028','18003','17028',NVL(SUBSTR(POD.EXPENDITURE_TYPE,1,5),GLCC.SEGMENT11)) 
    end 
    IN (select lookup_code from fnd_lookup_values Where lookup_type = 'TAC_DTN_ASSET_REP_ACCOUNT')

    --or  TAC_FA_REPORT_PKG.CHECK_POADD_RP(POH.SEGMENT1,POL.LINE_NUM,to_char(POH.APPROVED_DATE,'MON-YY')) = 'Y' 
    )*/
 AND ROWNUM=1
; 

else  --- no project ----
  SELECT 
    case when GLCC.SEGMENT1 = '01' then
       NVL(SUBSTR(POD.EXPENDITURE_TYPE,1,5),GLCC.SEGMENT11) 
    else --NVL(SUBSTR(POD.EXPENDITURE_TYPE,1,5),GLCC.SEGMENT11)
       decode(NVL(SUBSTR(POD.EXPENDITURE_TYPE,1,5),GLCC.SEGMENT11),'17030','17028','18003','17028','18003','17028',NVL(SUBSTR(POD.EXPENDITURE_TYPE,1,5),GLCC.SEGMENT11)) 
    end ACCOUNT_WIP
    INTO V_ACCOUNT_WIP
    FROM PO_HEADERS_ALL POH,
     PO_LINES_ALL POL,
     PO_DISTRIBUTIONS_ALL  POD,
     GL_CODE_COMBINATIONS_KFV GLCC    
WHERE POH.PO_HEADER_ID = POL.PO_HEADER_ID
AND POL.PO_HEADER_ID = POD.PO_HEADER_ID
AND POL.PO_LINE_ID = POD.PO_LINE_ID 
AND POD.PO_HEADER_ID = P_HEADER_ID
AND POD.PO_LINE_ID = P_LINE_ID
--AND POD.PROJECT_ID = P_PROJECT_ID
AND POD.BUDGET_ACCOUNT_ID = GLCC.CODE_COMBINATION_ID(+)
 /*AND ( case when GLCC.SEGMENT1 = '01' then
       NVL(SUBSTR(POD.EXPENDITURE_TYPE,1,5),GLCC.SEGMENT11) 
    else 
       decode(NVL(SUBSTR(POD.EXPENDITURE_TYPE,1,5),GLCC.SEGMENT11),'17030','17028','18003','17028','18003','17028',NVL(SUBSTR(POD.EXPENDITURE_TYPE,1,5),GLCC.SEGMENT11)) 
    end 
    IN (select lookup_code from fnd_lookup_values Where lookup_type = 'TAC_DTN_ASSET_REP_ACCOUNT')

   -- or  TAC_FA_REPORT_PKG.CHECK_POADD_RP(POH.SEGMENT1,POL.LINE_NUM,to_char(POH.APPROVED_DATE,'MON-YY')) = 'Y' 
    )*/
 AND ROWNUM=1
; 

end if;

RETURN (V_ACCOUNT_WIP);

EXCEPTION WHEN OTHERS THEN

    RETURN NULL;

END;

FUNCTION GET_RELEASE_NUM(/*P_PO_NUMBER VARCHAR2,
                          P_PO_LINE NUMBER,
                          P_APPROVED_DATE_FR VARCHAR2,
                          P_APPROVED_DATE_TO VARCHAR2,*/
                          P_RELEASE_ID NUMBER) RETURN VARCHAR2
                          IS
                          
  V_RELEASE_NUM VARCHAR2(10);
 BEGIN
 
 
/*select DISTINCT '-'||pra.release_num
     INTO V_RELEASE_NUM
from PO_HEADERS_ALL          POH
       ,PO_LINES_ALL            POL
       ,PO_LINE_LOCATIONS_ALL  PLL
       ,PO_RELEASES_ALL        PRA
where POH.PO_HEADER_ID =  POL.PO_HEADER_ID
AND POH.AUTHORIZATION_STATUS = 'APPROVED'
AND POL.PO_LINE_ID = PLL.PO_LINE_ID
AND PLL.PO_HEADER_ID = PRA.PO_HEADER_ID
AND PLL.PO_RELEASE_ID = PRA.PO_RELEASE_ID
AND POH.SEGMENT1 = P_PO_NUMBER
and pol.line_num = P_PO_LINE
AND PRA.PO_RELEASE_ID = P_RELEASE_ID
and (case nvl(pra.RELEASE_NUM,0) when 0 then TRUNC(poh.APPROVED_DATE )
     else TRUNC(pra.APPROVED_DATE ) end >=  TRUNC(TO_DATE(P_APPROVED_DATE_FR,'YYYY/MM/DD HH24:MI:SS')) 
     or P_APPROVED_DATE_FR is null)
and (case nvl(pra.RELEASE_NUM,0) when 0 then TRUNC(poh.APPROVED_DATE )
     else TRUNC(pra.APPROVED_DATE ) end <=  TRUNC(TO_DATE(P_APPROVED_DATE_TO,'YYYY/MM/DD HH24:MI:SS')) 
     or P_APPROVED_DATE_TO is null)
;*/

select DISTINCT '-'||pra.release_num
     INTO V_RELEASE_NUM
from PO_RELEASES_ALL        PRA
where PRA.PO_RELEASE_ID = P_RELEASE_ID
;
 
 RETURN (V_RELEASE_NUM);
 
 EXCEPTION WHEN OTHERS THEN
    RETURN NULL;
 END; 
                         
FUNCTION GET_APPROVED_DATE(/*P_PO_NUMBER VARCHAR2,
                          P_PO_LINE NUMBER,
                          P_APPROVED_DATE_FR VARCHAR2,
                          P_APPROVED_DATE_TO VARCHAR2,*/
                          P_RELEASE_ID NUMBER) RETURN DATE
                          IS
 V_APPROVED_DATE DATE;
                          
 BEGIN
 
 /*select DISTINCT case nvl(pra.RELEASE_NUM,0) when 0 then TRUNC(poh.APPROVED_DATE )
     else TRUNC(pra.APPROVED_DATE ) end  APP_DATE
     INTO V_APPROVED_DATE
from PO_HEADERS_ALL          POH
       ,PO_LINES_ALL            POL
       ,PO_LINE_LOCATIONS_ALL  PLL
       ,PO_RELEASES_ALL        PRA
where POH.PO_HEADER_ID =  POL.PO_HEADER_ID
AND POH.AUTHORIZATION_STATUS = 'APPROVED'
AND POL.PO_LINE_ID = PLL.PO_LINE_ID
AND PLL.PO_HEADER_ID = PRA.PO_HEADER_ID
AND PLL.PO_RELEASE_ID = PRA.PO_RELEASE_ID
AND POH.SEGMENT1 = P_PO_NUMBER
and pol.line_num = P_PO_LINE
AND PRA.PO_RELEASE_ID = P_RELEASE_ID
and (case nvl(pra.RELEASE_NUM,0) when 0 then TRUNC(poh.APPROVED_DATE )
     else TRUNC(pra.APPROVED_DATE ) end >=  TRUNC(TO_DATE(P_APPROVED_DATE_FR,'YYYY/MM/DD HH24:MI:SS')) 
     or P_APPROVED_DATE_FR is null)
and (case nvl(pra.RELEASE_NUM,0) when 0 then TRUNC(poh.APPROVED_DATE )
     else TRUNC(pra.APPROVED_DATE ) end <=  TRUNC(TO_DATE(P_APPROVED_DATE_TO,'YYYY/MM/DD HH24:MI:SS')) 
     or P_APPROVED_DATE_TO is null)
;
*/

select DISTINCT TRUNC(pra.APPROVED_DATE ) APP_DATE
     INTO V_APPROVED_DATE
from PO_RELEASES_ALL        PRA
where PRA.PO_RELEASE_ID = P_RELEASE_ID
;

RETURN TRUNC(V_APPROVED_DATE);
 
 
 EXCEPTION WHEN OTHERS THEN
   RETURN NULL;
 END;                         
                          
                          

FUNCTION GET_DIS_QTY(P_HEADER_ID IN NUMBER,
                    P_LINE_ID IN NUMBER,
                    P_RELEASE_ID IN NUMBER) RETURN NUMBER  IS
                    
   V_DIS_QTY NUMBER;
BEGIN

V_DIS_QTY  := 0;

IF P_RELEASE_ID IS NOT NULL THEN
    SELECT SUM(PLL.QUANTITY)
    INTO V_DIS_QTY
    FROM PO_LINE_LOCATIONS_ALL PLL   
    WHERE PLL.PO_HEADER_ID = P_HEADER_ID
    AND PLL.PO_LINE_ID = P_LINE_ID
    AND PLL.PO_RELEASE_ID = P_RELEASE_ID;
    
ELSE  ------SUM FROM PO DISTRIBUTION-----

    SELECT SUM(NVL(QUANTITY_ORDERED,0))
    INTO V_DIS_QTY
    FROM PO_HEADERS_ALL POH,
     PO_LINES_ALL POL,
     PO_DISTRIBUTIONS_ALL  POD,
     GL_CODE_COMBINATIONS_KFV GLCC    
    WHERE POH.PO_HEADER_ID = POL.PO_HEADER_ID
    AND POL.PO_HEADER_ID = POD.PO_HEADER_ID
    AND POL.PO_LINE_ID = POD.PO_LINE_ID 
    AND POD.PO_HEADER_ID = P_HEADER_ID
    AND POD.PO_LINE_ID = P_LINE_ID
    AND POD.BUDGET_ACCOUNT_ID = GLCC.CODE_COMBINATION_ID(+)
    AND ( GET_ACCOUNT_WIP(POH.PO_HEADER_ID,POL.PO_LINE_ID,POD.PROJECT_ID) is not null
         --IN (select lookup_code from fnd_lookup_values Where lookup_type = 'TAC_DTN_ASSET_REP_ACCOUNT')

         or  TAC_FA_REPORT_PKG.CHECK_POADD_RP(POH.SEGMENT1,POL.LINE_NUM) = 'Y' 
    )
    AND  NVL(TAC_FA_REPORT_PKG.CHECK_POADD_RP(POH.SEGMENT1,POL.LINE_NUM),'Y') != 'N' 
;

END IF;

RETURN(V_DIS_QTY);

EXCEPTION WHEN OTHERS THEN

RETURN(0);

END;

FUNCTION GET_GRN_QTY(P_HEADER_ID IN NUMBER,
                    P_LINE_ID IN NUMBER,
                    P_po_release_id IN NUMBER)  RETURN NUMBER IS
                    
   V_GRN_QTY NUMBER;
BEGIN

SELECT sum(case TRANSACTION_TYPE when 'RETURN TO VENDOR'
       then nvl(QUANTITY,0) * -1
       else nvl(QUANTITY,0) end) V_GRN_QTY
       INTO V_GRN_QTY
FROM RCV_TRANSACTIONS     
WHERE PO_HEADER_ID = P_HEADER_ID
AND PO_LINE_ID = P_LINE_ID
and TRANSACTION_TYPE in ('RECEIVE','RETURN TO VENDOR') 
and CASE NVL(P_po_release_id,1) WHEN 1
     THEN 1
     ELSE P_po_release_id END = NVL(po_release_id,1)
;

RETURN NVL(V_GRN_QTY,0);

EXCEPTION WHEN OTHERS THEN

RETURN(0);

END;

FUNCTION GET_GRN_AMT_PO(P_PO_NUMBER IN VARCHAR2,
                    P_PO_LINE IN VARCHAR2) RETURN NUMBER
IS

V_GRN_PO_AMT NUMBER;
BEGIN
      SELECT SUM(NVL(GRN_ADJUST,0)) GRN_AMT
      INTO V_GRN_PO_AMT
      FROM TAC_PO_ADDITION_INFO
      WHERE PO_NUMBER = P_PO_NUMBER
      AND PO_LINE_NUMBER = P_PO_LINE
      AND TO_DATE(PERIOD_NAME,'MON-YY') = (SELECT MAX(to_date(PERIOD_NAME,'MON-YY'))
                                                FROM TAC_PO_ADDITION_INFO
                                                WHERE PO_NUMBER = P_PO_NUMBER
                                                AND PO_LINE_NUMBER = P_PO_LINE)
      ;
      
      RETURN nvl(V_GRN_PO_AMT,0);
 EXCEPTION WHEN NO_DATA_FOUND THEN
      RETURN 0;
 WHEN OTHERS THEN
      RETURN 0;
END;

FUNCTION GET_GRN_AMT_INV(P_PO_NUMBER IN VARCHAR2,
                    P_PO_LINE IN VARCHAR2
                    --P_RELEASE_NUM IN VARCHAR2
                    ) RETURN NUMBER
IS

V_GRN_INV_AMT NUMBER;
V_PO_NUMBER VARCHAR2(30);
V_PO_LINE VARCHAR2(30);
BEGIN
      V_GRN_INV_AMT := 0;
      V_PO_NUMBER := null;
      V_PO_LINE := null;

     BEGIN
      SELECT DISTINCT ATTRIBUTE1,ATTRIBUTE2
      INTO V_PO_NUMBER,V_PO_LINE
      FROM AP_INVOICE_DISTRIBUTIONS_all
      WHERE ATTRIBUTE1 = P_PO_NUMBER
      AND ATTRIBUTE2 = P_PO_LINE
      ;
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END;
    
      
     
     IF V_PO_NUMBER IS NOT NULL 
       AND V_PO_LINE IS NOT NULL THEN
      
            SELECT SUM(AMOUNT) INV_AMT
            INTO V_GRN_INV_AMT
            FROM AP_INVOICE_DISTRIBUTIONS_all
            WHERE ATTRIBUTE1 = V_PO_NUMBER
            AND ATTRIBUTE2 = V_PO_LINE
            group by ATTRIBUTE1 ,
            ATTRIBUTE2;
            
            /*IF SUBSTR(P_RELEASE_NUM,2) IS NOT NULL THEN
                SELECT SUM(AMOUNT) INV_AMT
                INTO V_GRN_INV_AMT
                FROM AP_INVOICE_DISTRIBUTIONS_all
                WHERE ATTRIBUTE1 = V_PO_NUMBER
                AND ATTRIBUTE2 = V_PO_LINE
                AND ATTRIBUTE3 = SUBSTR(P_RELEASE_NUM,2)
                group by ATTRIBUTE1 ,
                ATTRIBUTE2
                ,ATTRIBUTE3;
            END IF;
            */
     END if;
      
      RETURN nvl(V_GRN_INV_AMT,0);
 EXCEPTION WHEN NO_DATA_FOUND THEN
      RETURN 0;
 WHEN OTHERS THEN
      RETURN 0;
END;

FUNCTION GET_GRN_AMT_GL(P_PO_NUMBER IN VARCHAR2,
                    P_PO_LINE IN VARCHAR2,
                    P_RELEASE_NUM IN VARCHAR2,
                    P_RELEASE_ID IN NUMBER) RETURN NUMBER
                    
IS

     cursor c1 is
     SELECT aid.INVOICE_ID,aid.DISTRIBUTION_LINE_NUMBER
     FROM ap_invoice_distributions_ALL aid
     WHERE ATTRIBUTE1 = P_PO_NUMBER||P_RELEASE_NUM
     AND ATTRIBUTE2 = P_PO_LINE
     ;
     
     cursor c2 is
      SELECT distinct aid.INVOICE_ID,aid.po_distribution_id,aid.DISTRIBUTION_LINE_NUMBER
     FROM ap_invoice_distributions_ALL aid,
     PO_DISTRIBUTIONS_all pod,
     po_headers_all poh,
     po_lines_all pol
     where POD.PO_DISTRIBUTION_ID = AID.PO_DISTRIBUTION_ID
     and pod.po_header_id = poh.po_header_id
     and pod.po_line_id = pol.po_line_id
     and pol.po_header_id = poh.po_header_id
     and poh.segment1 = P_PO_NUMBER
     and pol.line_num = P_PO_LINE
     and aid.po_distribution_id is not null;
     
V_GRN_GL_AMT NUMBER;
V_GRN_GL_AMT1 NUMBER;
V_GRN_GL_AMT2 NUMBER;
V_GRN_GL_AMT3 NUMBER;
V_INVOICE_ID1 NUMBER;
V_INVOICE_ID2 NUMBER;

v_dis_line_num1 number;
v_po_distribution_id2 number;
v_dis_line_num2 number;
I NUMBER;

BEGIN

V_GRN_GL_AMT := 0;
V_GRN_GL_AMT1 := 0;
V_GRN_GL_AMT2 := 0;
V_GRN_GL_AMT2 := 0;
V_INVOICE_ID1 := null;
V_INVOICE_ID2 := null;
v_dis_line_num1 := null;
v_po_distribution_id2 := null;
v_dis_line_num2 := null;
I := 0;

   

  for rec1 in c1 
  loop
  I := i+1;
    BEGIN
       V_GRN_GL_AMT := 0;
       select  sum(NVL(jl.accounted_dr,0)-NVL(jl.accounted_cr,0)) grn_amt
       INTO V_GRN_GL_AMT
        from     GL_JE_LINES	    jl
         ,GL_JE_HEADERS 	jh
         ,GL_CODE_COMBINATIONS_KFV GLCC
         --,xla_ap_inv_enc_gl_v xla
         ,GL_IMPORT_REFERENCES xla
         --,ap_invoice_distributions_ALL aid         
         --,po_headers_all poh
		 --,po_lines_all pol
		 --,PO_DISTRIBUTIONS_all pod

        where  jl.status             = 'P'
        and jl.code_combination_id 	= GLCC.code_combination_id
        and jh.status                = 'P' 
        and jh.actual_flag           = 'A'
        and jh.je_header_id          = jl.je_header_id
        and jh.je_source             = 'Payables' 
        and jh.je_category      	 = 'Purchase Invoices'
        and jl.effective_date        is not null   
        and xla.je_header_id         = jl.je_header_id  -- 721504
        and xla.je_line_num          = jl.je_line_num   -- 4
        /*and xla.trx_hdr_id           = aid.invoice_id(+)
        and xla.trx_line_number      = aid.distribution_line_number(+)
        and aid.po_header_id         = poh.po_header_id(+)
        and aid.po_header_id         = pol.po_header_id(+)
        and aid.po_line_number       = pol.line_num(+)*/
        and xla.REFERENCE_2      = TO_CHAR(rec1.invoice_id) --rec1.invoice_id --to_char(V_INVOICE_ID1) -- TO_CHAR(aid.invoice_id)
        and xla.REFERENCE_3      = TO_CHAR(rec1.distribution_line_number)  -- rec1.distribution_line_number --to_char(v_dis_line_num1) -- TO_CHAR(aid.distribution_line_number)  
        
       -- and aid.attribute1 = P_PO_NUMBER --poh.segment1
       -- and aid.attribute2 = P_PO_LINE --pol.line_num
       -- and poh.po_header_id = pod.po_header_id
       -- and pol.po_line_id = pod.po_line_id
        --and POD.BUDGET_ACCOUNT_ID = GLCC.CODE_COMBINATION_ID
     --   and TAC_FA_REPORT_PKG.GET_ACCOUNT_WIP(POH.PO_HEADER_ID,POL.PO_LINE_ID) = glcc.SEGMENT11
        
         AND glcc.SEGMENT11 BETWEEN '17000' AND '19999' 
        /*and (glcc.SEGMENT11 BETWEEN '15005' AND '15005'
            OR glcc.SEGMENT11 BETWEEN '17000' AND '19999'
            OR glcc.SEGMENT11 BETWEEN '70003' AND '70003'
            OR glcc.SEGMENT11 BETWEEN '73000' AND '73999')*/
 
       -- AND poh.segment1 = P_PO_NUMBER
       -- AND pol.line_num = P_PO_LINE       
       -- AND AID.INVOICE_ID = V_INVOICE_ID1 --'1337167'
              
        --group by AID.INVOICE_ID
        --poh.segment1,pol.line_num    
        group by xla.REFERENCE_2,xla.REFERENCE_3        
        ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
       V_GRN_GL_AMT := 0;
     WHEN OTHERS THEN
       V_GRN_GL_AMT := 0;
    END;
    
    V_GRN_GL_AMT1 := nvl(V_GRN_GL_AMT1,0) + nvl(V_GRN_GL_AMT,0);
   -- V_GRN_GL_AMT1 := rec1.distribution_line_number; --NVL(V_GRN_GL_AMT1,0) + NVL(rec1.distribution_line_number,0);
  end loop;

   
    
    
        
    IF P_RELEASE_ID IS NULL --- CASE STANDARD----
    THEN
      begin  
        SELECT sum(NVL(jl.accounted_dr,0)-NVL(jl.accounted_cr,0)) grn_amt
        INTO V_GRN_GL_AMT2
        FROM
        GL_CODE_COMBINATIONS_KFV GLCC,
        po_headers_all poh,
        po_lines_all pol,
        PO_DISTRIBUTIONS_ALL POD,
        RCV_TRANSACTIONS RCT,
        PO_LOOKUP_CODES PL2,
        GL_IMPORT_REFERENCES R,
        RCV_RECEIVING_SUB_LEDGER  RRS,
        GL_JE_HEADERS jh,
        GL_JE_LINES	  jl
        WHERE jl.status             = 'P'
      and jl.code_combination_id 	= GLCC.code_combination_id
      and jh.status                = 'P' 
      and jh.actual_flag           = 'A'
      and jh.je_header_id          = jl.je_header_id
      and jh.je_source             = 'Purchasing' 
      and jh.je_category      	 = 'Receiving'
      and jl.effective_date        is not null   
      AND jh.JE_HEADER_ID = R.JE_HEADER_ID
      AND JL.JE_LINE_NUM = R.JE_LINE_NUM
      AND jh.ACTUAL_FLAG = RRS.ACTUAL_FLAG
      AND RRS.GL_SL_LINK_ID = R.GL_SL_LINK_ID
      AND RRS.RCV_TRANSACTION_ID = R.REFERENCE_5
      AND RCT.TRANSACTION_ID = RRS.RCV_TRANSACTION_ID
      AND RRS.REFERENCE3 = to_char(POD.PO_DISTRIBUTION_ID)
      AND PL2.LOOKUP_CODE = RCT.TRANSACTION_TYPE
      AND PL2.LOOKUP_TYPE = 'RCV TRANSACTION TYPE'
      AND R.GL_SL_LINK_TABLE = 'RSL'
      and RRS.CODE_COMBINATION_ID = GLCC.CODE_COMBINATION_ID
     
      and poh.po_header_id = pol.po_header_id      
      and pol.po_header_id = pod.po_header_id
      and pol.po_line_id = pod.po_line_id
      --and POD.BUDGET_ACCOUNT_ID = GLCC.CODE_COMBINATION_ID
      --and TAC_FA_REPORT_PKG.GET_ACCOUNT_WIP(POH.PO_HEADER_ID,POL.PO_LINE_ID) = glcc.SEGMENT11
      AND glcc.SEGMENT11 BETWEEN '17000' AND '19999' 
      /*and (glcc.SEGMENT11 BETWEEN '15005' AND '15005'
            OR glcc.SEGMENT11 BETWEEN '17000' AND '19999'
            OR glcc.SEGMENT11 BETWEEN '70003' AND '70003'
            OR glcc.SEGMENT11 BETWEEN '73000' AND '73999') */
            
      AND poh.segment1 = P_PO_NUMBER
      AND pol.line_num = P_PO_LINE  
      
      group by poh.segment1,pol.line_num
      ;
      
    EXCEPTION WHEN NO_DATA_FOUND THEN
       V_GRN_GL_AMT2 := 0;
     WHEN OTHERS THEN
       V_GRN_GL_AMT2 := 0;
    END;
    
    ELSE  ------CASE RELEASE-------------------
    
       begin  
        SELECT sum(NVL(jl.accounted_dr,0)-NVL(jl.accounted_cr,0)) grn_amt
        INTO V_GRN_GL_AMT2
        FROM
        GL_CODE_COMBINATIONS_KFV GLCC,
        po_headers_all poh,
        po_lines_all pol,
        PO_DISTRIBUTIONS_ALL POD,
        PO_LINE_LOCATIONS_ALL PLL,--
        RCV_TRANSACTIONS RCT,
        PO_LOOKUP_CODES PL2,
        GL_IMPORT_REFERENCES R,
        RCV_RECEIVING_SUB_LEDGER  RRS,
        GL_JE_HEADERS jh,
        GL_JE_LINES	  jl
        WHERE jl.status             = 'P'
      and jl.code_combination_id 	= GLCC.code_combination_id
      and jh.status                = 'P' 
      and jh.actual_flag           = 'A'
      and jh.je_header_id          = jl.je_header_id
      and jh.je_source             = 'Purchasing' 
      and jh.je_category      	 = 'Receiving'
      and jl.effective_date        is not null   
      AND jh.JE_HEADER_ID = R.JE_HEADER_ID
      AND JL.JE_LINE_NUM = R.JE_LINE_NUM
      AND jh.ACTUAL_FLAG = RRS.ACTUAL_FLAG
      AND RRS.GL_SL_LINK_ID = R.GL_SL_LINK_ID
      AND RRS.RCV_TRANSACTION_ID = R.REFERENCE_5
      AND RCT.TRANSACTION_ID = RRS.RCV_TRANSACTION_ID
      AND RRS.REFERENCE3 = to_char(POD.PO_DISTRIBUTION_ID)
      AND PL2.LOOKUP_CODE = RCT.TRANSACTION_TYPE
      AND PL2.LOOKUP_TYPE = 'RCV TRANSACTION TYPE'
      AND R.GL_SL_LINK_TABLE = 'RSL'
      and RRS.CODE_COMBINATION_ID = GLCC.CODE_COMBINATION_ID
     
      and poh.po_header_id = pol.po_header_id      
      and pol.po_header_id = pod.po_header_id
      and pol.po_line_id = pod.po_line_id
      --and POD.BUDGET_ACCOUNT_ID = GLCC.CODE_COMBINATION_ID
      --and TAC_FA_REPORT_PKG.GET_ACCOUNT_WIP(POH.PO_HEADER_ID,POL.PO_LINE_ID) = glcc.SEGMENT11
      AND glcc.SEGMENT11 BETWEEN '17000' AND '19999' 
      /*and (glcc.SEGMENT11 BETWEEN '15005' AND '15005'
            OR glcc.SEGMENT11 BETWEEN '17000' AND '19999'
            OR glcc.SEGMENT11 BETWEEN '70003' AND '70003'
            OR glcc.SEGMENT11 BETWEEN '73000' AND '73999') */
            
      AND poh.segment1 = P_PO_NUMBER
      AND pol.line_num = P_PO_LINE  
      
      --------------------CASE RELEASE----------------------
     and PLL.po_release_id = P_RELEASE_ID
     AND POD.line_location_id = PLL.line_location_id
     AND POD.PO_HEADER_ID = PLL.PO_HEADER_ID
     AND POD.PO_LINE_ID = PLL.PO_LINE_ID
      group by poh.segment1,pol.line_num
      ;
      
    EXCEPTION WHEN NO_DATA_FOUND THEN
       V_GRN_GL_AMT2 := 0;
     WHEN OTHERS THEN
       V_GRN_GL_AMT2 := 0;
    END;
    
    
    END IF;
     
    
    
    
    for rec2 in c2 
    loop
      begin
       
       V_GRN_GL_AMT := 0;
       
       select sum(NVL(jl.accounted_dr,0)-NVL(jl.accounted_cr,0)) grn_amt
       INTO V_GRN_GL_AMT
        from GL_CODE_COMBINATIONS_KFV GLCC
         ,GL_JE_HEADERS 	jh
         ,GL_JE_LINES	    jl
         ,GL_IMPORT_REFERENCES xla
         --,ap_invoice_distributions_ALL aid         
		 --,PO_DISTRIBUTIONS_all pod
        where  1=1
        and jl.status             = 'P'
        and jl.code_combination_id 	= GLCC.code_combination_id
        and jh.status                = 'P' 
        and jh.actual_flag           = 'A'
        and jh.je_header_id          = jl.je_header_id
        and jh.je_source             = 'Payables' 
        and jh.je_category      	 = 'Purchase Invoices'
        and jl.effective_date        is not null   
        and xla.je_header_id         = jl.je_header_id  -- 721504
        and xla.je_line_num          = jl.je_line_num   -- 4
        and xla.REFERENCE_2      = TO_CHAR(rec2.invoice_id) --to_char(V_INVOICE_ID2) -- TO_CHAR(aid.invoice_id)
        and xla.REFERENCE_3      = TO_CHAR(rec2.distribution_line_number) --to_char(v_dis_line_num2) -- TO_CHAR(aid.distribution_line_number)       
       -- AND POD.PO_DISTRIBUTION_ID = AID.PO_DISTRIBUTION_ID
        AND glcc.SEGMENT11 BETWEEN '17000' AND '19999' 
       /*and (glcc.SEGMENT11 BETWEEN '15005' AND '15005'
            OR glcc.SEGMENT11 BETWEEN '17000' AND '19999'
            OR glcc.SEGMENT11 BETWEEN '70003' AND '70003'
            OR glcc.SEGMENT11 BETWEEN '73000' AND '73999')  */   
            
           -- AND AID.INVOICE_ID = V_INVOICE_ID2 --'1337167'  
           -- and aid.po_distribution_id = v_po_distribution_id            
            group by xla.REFERENCE_2,xla.REFERENCE_3      
        ;
        
      exception WHEN NO_DATA_FOUND THEN
         V_GRN_GL_AMT := 0;
                when others then
         V_GRN_GL_AMT := 0;
      end;
      
      V_GRN_GL_AMT3 := nvl(V_GRN_GL_AMT3,0) + nvl(V_GRN_GL_AMT,0);
    end loop;
   -- end if;
    
    
    

    
 RETURN NVL(V_GRN_GL_AMT1,0)  
 + NVL(V_GRN_GL_AMT2,0) 
 + NVL(V_GRN_GL_AMT3,0)
 ;

EXCEPTION WHEN OTHERS THEN
   RETURN 0;
END;

FUNCTION GET_REGIST_AMT_FA(P_PO_NUMBER IN VARCHAR2,
                    P_PO_LINE IN VARCHAR2) RETURN NUMBER
IS

V_REGIS_AMT  NUMBER;

BEGIN
        SELECT SUM(fb.cost) REGIS_AMT 
        INTO V_REGIS_AMT
        FROM FA_ASSET_INVOICES ai,
        FA_BOOKS fb,
        fa_additions_b fab
        WHERE ai.asset_id = fb.asset_id
        and ai.asset_id = fab.asset_id
        and fb.DATE_INEFFECTIVE is null
        and ai.PO_NUMBER = P_PO_NUMBER
        and fab.ATTRIBUTE5 = P_PO_LINE
        --and ai.ATTRIBUTE1 = P_PO_LINE
        ;

        
      RETURN nvl(V_REGIS_AMT,0);
EXCEPTION WHEN NO_DATA_FOUND THEN 
      RETURN 0;
 WHEN OTHERS THEN 
      RETURN 0;
END; 

FUNCTION GET_REGIST_AMT_CLOUD(P_PO_NUMBER IN VARCHAR2,
                    P_PO_LINE IN VARCHAR2) RETURN NUMBER
IS
V_REGIST_AMT NUMBER;

BEGIN
      SELECT SUM(CLOUD_VALUE_REGISTER) REGIST_AMT
      INTO V_REGIST_AMT
      FROM TAC_PO_CLOUD_INFO
      WHERE PO_NUMBER = P_PO_NUMBER
      AND PO_LINE_NUMBER = P_PO_LINE;
      
      RETURN nvl(V_REGIST_AMT,0);
EXCEPTION WHEN NO_DATA_FOUND THEN
      RETURN 0;
 WHEN OTHERS THEN
      RETURN 0;
END;

FUNCTION GET_REGIST_QTY_FA(P_PO_NUMBER IN VARCHAR2,
                    P_PO_LINE IN VARCHAR2) RETURN NUMBER
IS
V_REGIST_QTY NUMBER;

BEGIN

        SELECT sum(FB.attribute4)  
        into V_REGIST_QTY
        FROM FA_ASSET_INVOICES ai,
        FA_ADDITIONS_B fb,
        fa_additions_b fab
        WHERE ai.asset_id = fb.asset_id
        and ai.asset_id = fab.asset_id
        and ai.PO_NUMBER = P_PO_NUMBER
        and fab.ATTRIBUTE5 = P_PO_LINE
        --and ai.ATTRIBUTE1 = P_PO_LINE
        ;
        
        return (nvl(V_REGIST_QTY,0));

EXCEPTION WHEN NO_DATA_FOUND THEN
      RETURN 0;
 WHEN OTHERS THEN
      RETURN 0;
END;

FUNCTION GET_REGIST_QTY_CLOUD(P_PO_NUMBER IN VARCHAR2,
                    P_PO_LINE IN VARCHAR2) RETURN NUMBER
IS
V_REGIST_QTY NUMBER;

BEGIN
      SELECT SUM(CLOUD_QTY_REGISTER) REGIST_AMT
      INTO V_REGIST_QTY
      FROM TAC_PO_CLOUD_INFO
      WHERE PO_NUMBER = P_PO_NUMBER
      AND PO_LINE_NUMBER = P_PO_LINE;
      
      RETURN nvl(V_REGIST_QTY,0);
EXCEPTION WHEN NO_DATA_FOUND THEN
      RETURN 0;
 WHEN OTHERS THEN
      RETURN 0;
END;



FUNCTION GET_PROGRESS_AMT_PO(P_PO_NUMBER IN VARCHAR2,
                    P_PO_LINE IN VARCHAR2) RETURN NUMBER
 IS
V_PROGRESS_AMT NUMBER := 0;

    BEGIN
        SELECT SUM(PROGRESS_AMOUNT) PROGRESS_AMT 
        INTO V_PROGRESS_AMT
        FROM TAC_PO_ADDITION_INFO
        WHERE PO_NUMBER = P_PO_NUMBER
        and PO_LINE_NUMBER = P_PO_LINE
        ;
        
        return nvl(V_PROGRESS_AMT,0);
    EXCEPTION WHEN NO_DATA_FOUND THEN 
      return(0);
     WHEN OTHERS THEN 
      return(0);
    END; 
 
FUNCTION GET_PROGRESS_QTY_PO(P_PO_NUMBER IN VARCHAR2,
                    P_PO_LINE IN VARCHAR2) RETURN NUMBER
 IS
V_PROGRESS_QTY NUMBER := 0;

    BEGIN
        SELECT SUM(PROGRESS_QTY) PROGRESS_AMT 
        INTO V_PROGRESS_QTY
        FROM TAC_PO_ADDITION_INFO
        WHERE PO_NUMBER = P_PO_NUMBER
        and PO_LINE_NUMBER = P_PO_LINE
        ;
        
        return nvl(V_PROGRESS_QTY,0);
    EXCEPTION WHEN NO_DATA_FOUND THEN 
      return(0);
     WHEN OTHERS THEN 
      return(0);
    END;


end TAC_FA_REPORT_PKG;
/