CREATE OR REPLACE PACKAGE APPS.TAC_WIP_ACC_REPORT_PKG is
FUNCTION check_wip_accrued_report(p_project_id IN NUMBER,
                                  p_po_header_id IN NUMBER,
                                  p_po_line_id IN NUMBER,
                                  p_rate number  ) RETURN varchar2; 
---
FUNCTION CHECK_POADD_RP(P_PO_NUMBER IN VARCHAR2,
                        P_PO_LINE IN NUMBER,
                        P_PERIOD IN VARCHAR2
                         ) RETURN varchar2; 
FUNCTION CHECK_DISPLAY_PROJECT(P_PO_NUMBER IN VARCHAR2,
                        P_PO_LINE IN VARCHAR2,
                        P_PERIOD IN VARCHAR2
                         ) RETURN varchar2;    
FUNCTION CHECK_FULL_RECEIVE(P_PO_HEADER_ID IN NUMBER,
                        P_PO_LINE_ID IN NUMBER
                         ) RETURN varchar2;
FUNCTION GET_LAST_RECEIVE_DATE(P_PO_HEADER_ID NUMBER,
                               P_PO_LINE_ID NUMBER) RETURN DATE; 
FUNCTION GET_REGIST_DATE(P_PO_NUMBER IN VARCHAR2,
                    P_PO_LINE IN VARCHAR2,
                    P_PERIOD IN VARCHAR2) RETURN DATE;
FUNCTION GET_GRN_AMT_PO(P_PO_NUMBER IN VARCHAR2,
                    P_PO_LINE IN VARCHAR2,
                    P_PERIOD IN VARCHAR2) RETURN NUMBER;
FUNCTION GET_GRN_AMT_INV(P_PO_NUMBER IN VARCHAR2,
                    P_PO_LINE IN VARCHAR2,
                    P_PERIOD IN VARCHAR2) RETURN NUMBER;
FUNCTION GET_GRN_AMT_GL(P_PO_NUMBER IN VARCHAR2,
                    P_PO_LINE IN VARCHAR2,
                    P_RELEASE_NUM IN VARCHAR2,
                    P_RELEASE_ID IN NUMBER,
                    P_PERIOD IN VARCHAR2) RETURN NUMBER;
FUNCTION GET_REGIST_AMT_FA(P_PO_NUMBER IN VARCHAR2,
                    P_PO_LINE IN VARCHAR2,
                    P_PERIOD IN VARCHAR2) RETURN NUMBER;
FUNCTION GET_REGIST_AMT_CLOUD(P_PO_NUMBER IN VARCHAR2,
                    P_PO_LINE IN VARCHAR2,
                    P_PERIOD IN VARCHAR2) RETURN NUMBER;

FUNCTION GET_PROGRESS_AMT_PO(P_PO_NUMBER IN VARCHAR2,
                    P_PO_LINE IN VARCHAR2,
                    P_PERIOD IN VARCHAR2) RETURN NUMBER;

FUNCTION GET_AMOUNT(P_PO_NUMBER IN VARCHAR2,
                    P_PO_LINE IN VARCHAR2,
                    P_TYPE_OF_WORK VARCHAR2,
                    P_TYPE_REPORT VARCHAR2,
                    P_PERIOD IN VARCHAR2) RETURN NUMBER;   
 
procedure gen_wip_aging (P_REQUEST_ID IN NUMBER,
                    P_ORG_ID IN NUMBER,
                    P_PO_NO_ID IN NUMBER,
                    P_PO_LINE IN NUMBER,
                    P_PROJECT_NO IN VARCHAR2,
                    PIN_PERIOD IN VARCHAR2 );  
                    
procedure gen_accrued_aging (P_REQUEST_ID IN NUMBER,
                    P_ORG_ID IN NUMBER,
                    P_PO_NO_ID IN NUMBER,
                    P_PO_LINE IN NUMBER,
                    P_PROJECT_NO IN VARCHAR2,
                    PIN_PERIOD IN VARCHAR2 );                   

                                                                                  
                                                     
  
end TAC_WIP_ACC_REPORT_PKG;
/