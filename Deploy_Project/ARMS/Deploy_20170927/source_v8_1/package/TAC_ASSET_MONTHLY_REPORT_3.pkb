CREATE OR REPLACE PACKAGE BODY APPS.TAC_ASSET_MONTHLY_REPORT AS
--**********************************************************
-- FILE NAME: TAC_ASSET_MONTHLY.plb
-- DESCRIPTION: TAC Asset Monthly Report
-- AUTHOR     : RAPEEPORN  FONGPETCH
-- DATE       : 29-MAR-2017
--Initial Revision.
--
--**********************************************************

g_program                       VARCHAR2(2000) := 'xxup.plsql.XXEL_SHORTAGE_PLAN_REPORT';
g_step                          VARCHAR2(2000); 

g_ErrorMsg                      VARCHAR2(2000);
ExitWithError                   EXCEPTION;


TYPE accumm_tab IS TABLE OF VARCHAR2(50); -- element type is not record type 

  accumm_code  accumm_tab := accumm_tab(17106, 17107,17118,17126, 17134,17135,17143,17144,17145,17146,17147,17148,17149,18002,19114,19115);     
          
--------------------------------------------------------------------------------------------------------------------
PROCEDURE write_log(i_msg  IN  VARCHAR2) IS
BEGIN
  Fnd_File.PUT_LINE(Fnd_File.LOG,i_msg);
END write_log;
--------------------------------------------------------------------------------------------------------------------
PROCEDURE write_output(i_msg  IN  VARCHAR2) IS
BEGIN
  Fnd_File.PUT_LINE(Fnd_File.OUTPUT,i_msg);
END write_output;
--------------------------------------------------------------------------------------------------------------------
PROCEDURE write_unxp(i_module  IN  VARCHAR2, i_msg  IN  VARCHAR2) IS
BEGIN
  IF FND_LOG.LEVEL_UNEXPECTED >= FND_LOG.G_CURRENT_RUNTIME_LEVEL THEN
    FND_LOG.STRING(FND_LOG.LEVEL_UNEXPECTED,i_module,i_msg);
  END IF;
END write_unxp;
--------------------------------------------------------------------------------------------------------------------
PROCEDURE write_error(i_module  IN  VARCHAR2, i_msg  IN  VARCHAR2) IS
BEGIN
  
  IF FND_LOG.LEVEL_ERROR >= FND_LOG.G_CURRENT_RUNTIME_LEVEL THEN
    FND_LOG.STRING(FND_LOG.LEVEL_ERROR,i_module,i_msg);
  END IF;
END write_error;
--------------------------------------------------------------------------------------------------------------------
PROCEDURE write_excpt(i_module  IN  VARCHAR2, i_msg  IN  VARCHAR2) IS
BEGIN
  IF FND_LOG.LEVEL_EXCEPTION >= FND_LOG.G_CURRENT_RUNTIME_LEVEL THEN
    FND_LOG.STRING(FND_LOG.LEVEL_EXCEPTION,i_module,i_msg);
  END IF;
END write_excpt;
--------------------------------------------------------------------------------------------------------------------
PROCEDURE write_event(i_module  IN  VARCHAR2, i_msg  IN  VARCHAR2) IS
BEGIN
  IF FND_LOG.LEVEL_EVENT >= FND_LOG.G_CURRENT_RUNTIME_LEVEL THEN
    FND_LOG.STRING(FND_LOG.LEVEL_EVENT,i_module,i_msg);
  END IF;
END write_event;
--------------------------------------------------------------------------------------------------------------------
PROCEDURE write_proc(i_module  IN  VARCHAR2, i_msg  IN  VARCHAR2) IS
BEGIN
  IF FND_LOG.LEVEL_PROCEDURE >= FND_LOG.G_CURRENT_RUNTIME_LEVEL THEN
    FND_LOG.STRING(FND_LOG.LEVEL_PROCEDURE,i_module,i_msg);
  END IF;
END write_proc;
--------------------------------------------------------------------------------------------------------------------
PROCEDURE write_stmn(i_module  IN  VARCHAR2, i_msg  IN  VARCHAR2) IS
BEGIN
  IF FND_LOG.LEVEL_STATEMENT >= FND_LOG.G_CURRENT_RUNTIME_LEVEL THEN
    FND_LOG.STRING(FND_LOG.LEVEL_STATEMENT,i_module,i_msg);
  END IF;
END write_stmn;
--------------------------------------------------------------------------------------------------------------------

---Get GL Period 
--**********************************************************
FUNCTION GET_GL_PERIOD ( i_period   IN  VARCHAR2        ) RETURN NUMBER IS
  v_module   VARCHAR2(200) :=  g_program||'.'||'GET_GL_PERIOD';
  o_period   NUMBER;

BEGIN

  write_proc(v_module, 'BEGIN');
  
  begin
   select period_year||lpad(period_num ,2,0)
   into o_period
   from gl_periods 
   where period_set_name = 'Accounting'
   and adjustment_period_flag = 'N'
   and to_date (sysdate, 'dd/mm/rrrr') between start_date and end_date;
   exception when no_data_found then
     o_period := '';
  end;
     
  return(o_period);   
  
  write_proc(v_module, 'END');
     
END GET_GL_PERIOD;       

--Get CUT OFF Period 
--**********************************************************
FUNCTION GET_CUTOFF_PERIOD (i_period   IN  DATE) RETURN DATE IS
  v_module          VARCHAR2(200) :=  g_program||'.'||'GET_CUTOFF_PERIOD';
  lCutoffPeriod     DATE;

BEGIN

  write_proc(v_module, 'BEGIN');
  
  begin
    select to_date(MEANING)
    into lCutoffPeriod
    from fnd_lookup_values 
    Where lookup_type = 'TAC_DTN_CUTOFF'
    and lookup_code = to_char(i_period,'MON-YY');
   exception when no_data_found then
     lCutoffPeriod := last_day(trunc(sysdate));
  end;
     
  return lCutoffPeriod;   
  
  write_proc(v_module, 'END');
     
END GET_CUTOFF_PERIOD;   


FUNCTION GET_CUTOFF_LAST_PERIOD (i_period   IN  DATE) RETURN DATE IS
  v_module          VARCHAR2(200) :=  g_program||'.'||'GET_CUTOFF_PERIOD';
  lCutoffPeriod     DATE;

BEGIN

  write_proc(v_module, 'BEGIN');
  
  begin
    select to_date(MEANING)
    into lCutoffPeriod
    from fnd_lookup_values 
    Where lookup_type = 'TAC_DTN_CUTOFF'
    and lookup_code = to_char(trunc(add_months(SYSDATE,-1)),'MON-YY');
   exception when no_data_found then
     lCutoffPeriod := add_months(last_day(trunc(sysdate)),-1);
  end;
     
  return lCutoffPeriod;   
  
  write_proc(v_module, 'END');
     
END GET_CUTOFF_LAST_PERIOD;   

--**********************************************************
---Get Period  NAME 
--**********************************************************
FUNCTION GET_PERIOD_NAME (i_period   IN  date        ) RETURN varchar2 IS
  v_module   VARCHAR2(200) :=  g_program||'.'||'GET_PERIOD_NAME';
  o_period   VARCHAR2(50);

BEGIN

  write_proc(v_module, 'BEGIN');
  
  begin
   select PERIOD_NAME
   into o_period
   from gl_periods 
   where period_set_name = 'Accounting'
   and adjustment_period_flag = 'N'
   and to_date (sysdate, 'dd/mm/rrrr') between start_date and end_date;
   exception when no_data_found then
     o_period := '';
  end;
     
  return(o_period);   
  
  write_proc(v_module, 'END');
     
END GET_PERIOD_NAME;       
--**********************************************************

--Get Period Counter
--**********************************************************
FUNCTION GET_PERIOD_COUNTER ( i_book   IN  VARCHAR2 ) RETURN NUMBER IS
  v_module          VARCHAR2(200) :=  g_program||'.'||'GET_PERIOD_COUNTER';
  o_period_counter  NUMBER;

BEGIN
  write_proc(v_module, 'BEGIN');

  BEGIN
    
    select distinct period_counter
    into  o_period_counter
    from   FA_DEPRN_PERIODS
    where  --book_type_code = i_book
      period_name    =  to_char(sysdate,'MON-YY');
   
  EXCEPTION WHEN NO_DATA_FOUND THEN
   o_period_counter := '';
  END;

  RETURN o_period_counter;

  write_proc(v_module, 'END');
  
END  GET_PERIOD_COUNTER ;

 
--**********************************************************
--get grn amount

FUNCTION GET_GRN_AMT_GL(P_PO_NUMBER IN VARCHAR2,
                        P_PO_LINE IN VARCHAR2,
                        P_RELEASE_NUM IN VARCHAR2,
                        P_RELEASE_ID IN NUMBER,
                        i_begin_end  IN  VARCHAR2 ) RETURN NUMBER
                    
IS

 cursor c1 is
     SELECT aid.INVOICE_ID,aid.DISTRIBUTION_LINE_NUMBER
     FROM ap_invoice_distributions_ALL aid
     WHERE ATTRIBUTE1 = P_PO_NUMBER||P_RELEASE_NUM
     AND ATTRIBUTE2 = P_PO_LINE
     ;
     
     cursor c2 is
      SELECT distinct aid.INVOICE_ID,aid.po_distribution_id,aid.DISTRIBUTION_LINE_NUMBER
     FROM ap_invoice_distributions_ALL aid,
     PO_DISTRIBUTIONS_all pod,
     po_headers_all poh,
     po_lines_all pol
     where POD.PO_DISTRIBUTION_ID = AID.PO_DISTRIBUTION_ID
     and pod.po_header_id = poh.po_header_id
     and pod.po_line_id = pol.po_line_id
     and pol.po_header_id = poh.po_header_id
     and poh.segment1 = P_PO_NUMBER
     and pol.line_num = P_PO_LINE
     and aid.po_distribution_id is not null;

V_GRN_GL_AMT  NUMBER;
V_GRN_GL_AMT1 NUMBER;
V_GRN_GL_AMT2 NUMBER;
V_GRN_GL_AMT3 NUMBER;
V_INVOICE_ID NUMBER;
--v_period     number;
v_last_period   DATE;
v_period        DATE;

BEGIN

V_GRN_GL_AMT1 := 0;
V_GRN_GL_AMT2 := 0;
V_INVOICE_ID := null;


  
   v_last_period := GET_CUTOFF_LAST_PERIOD(SYSDATE);
   v_period      := GET_CUTOFF_PERIOD(SYSDATE);
   

   
   
   for rec_gl1 in c1
     loop
     BEGIN
        V_GRN_GL_AMT := 0;
        select  sum(NVL(jl.accounted_dr,0)-NVL(jl.accounted_cr,0)) grn_amt
        INTO V_GRN_GL_AMT
       
        from  GL_JE_LINES	    jl
         ,GL_JE_HEADERS 	jh
         ,GL_CODE_COMBINATIONS_KFV GLCC
         ,GL_IMPORT_REFERENCES xla
         --,ap_invoice_distributions_ALL aid         

        where  jl.status             = 'P'
        and jl.code_combination_id 	= GLCC.code_combination_id
        and jh.status                = 'P' 
        and jh.actual_flag           = 'A'
        and jh.je_header_id          = jl.je_header_id
        and jh.je_source             = 'Payables' 
        and jh.je_category      	 = 'Purchase Invoices'
        and jl.effective_date        is not null   
        and xla.je_header_id         = jl.je_header_id  -- 721504
        and xla.je_line_num          = jl.je_line_num   -- 4
        and xla.REFERENCE_2      = TO_CHAR(rec_gl1.invoice_id)
        and xla.REFERENCE_3      = TO_CHAR(rec_gl1.distribution_line_number)  
        
        AND glcc.SEGMENT11 BETWEEN '17000' AND '19999'
         and ( case when i_begin_end = 'B' and jl.EFFECTIVE_DATE <= v_last_period
          then 1
          when  i_begin_end = 'E' and jl.EFFECTIVE_DATE <= v_period
          then 1
          end ) = 1 
        /*AND AID.ATTRIBUTE1 = REC1.PO_NO
        AND AID.ATTRIBUTE2 = REC1.PO_LINE_NO*/
        
        group by --AID.ATTRIBUTE1,AID.ATTRIBUTE2 ,
        TRUNC(JL.EFFECTIVE_DATE)       
        ;
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END;
     
     V_GRN_GL_AMT1 := nvl(V_GRN_GL_AMT1,0) + nvl(V_GRN_GL_AMT,0);
     
     end loop;
   
  if P_RELEASE_ID is null then ---------- case standard -----------------------
    begin  
        SELECT sum(NVL(jl.accounted_dr,0)-NVL(jl.accounted_cr,0)) grn_amt
        INTO V_GRN_GL_AMT2
        FROM
        GL_CODE_COMBINATIONS_KFV GLCC,
        po_headers_all poh,
        po_lines_all pol,
        PO_DISTRIBUTIONS_ALL POD,
        RCV_TRANSACTIONS RCT,
        PO_LOOKUP_CODES PL2,
        GL_IMPORT_REFERENCES R,
        RCV_RECEIVING_SUB_LEDGER  RRS,
        GL_JE_HEADERS jh,
        GL_JE_LINES	  jl
        WHERE jl.status             = 'P'
      and jl.code_combination_id 	= GLCC.code_combination_id
      and jh.status                = 'P' 
      and jh.actual_flag           = 'A'
      and jh.je_header_id          = jl.je_header_id
      and jh.je_source             = 'Purchasing' 
      and jh.je_category      	 = 'Receiving'
      and jl.effective_date        is not null   
      AND jh.JE_HEADER_ID = R.JE_HEADER_ID
      AND JL.JE_LINE_NUM = R.JE_LINE_NUM
      AND jh.ACTUAL_FLAG = RRS.ACTUAL_FLAG
      AND RRS.GL_SL_LINK_ID = R.GL_SL_LINK_ID
      AND RRS.RCV_TRANSACTION_ID = R.REFERENCE_5
      AND RCT.TRANSACTION_ID = RRS.RCV_TRANSACTION_ID
      AND RRS.REFERENCE3 = to_char(POD.PO_DISTRIBUTION_ID)
      AND PL2.LOOKUP_CODE = RCT.TRANSACTION_TYPE
      AND PL2.LOOKUP_TYPE = 'RCV TRANSACTION TYPE'
      AND R.GL_SL_LINK_TABLE = 'RSL'
      and RRS.CODE_COMBINATION_ID = GLCC.CODE_COMBINATION_ID
     
      and poh.po_header_id = pol.po_header_id      
      and pol.po_header_id = pod.po_header_id
      and pol.po_line_id = pod.po_line_id
      --and POD.BUDGET_ACCOUNT_ID = GLCC.CODE_COMBINATION_ID
      --and TAC_FA_REPORT_PKG.GET_ACCOUNT_WIP(POH.PO_HEADER_ID,POL.PO_LINE_ID) = glcc.SEGMENT11
      AND glcc.SEGMENT11 BETWEEN '17000' AND '19999' 
      and ( case when i_begin_end = 'B' and jl.EFFECTIVE_DATE <= v_last_period
          then 1
          when  i_begin_end = 'E' and jl.EFFECTIVE_DATE <= v_period
          then 1
          end ) = 1 
            
      AND poh.segment1 = P_PO_NUMBER
      AND pol.line_num = P_PO_LINE  
      
      group by poh.segment1,pol.line_num
      ;
      
    EXCEPTION WHEN NO_DATA_FOUND THEN
       V_GRN_GL_AMT2 := 0;
     WHEN OTHERS THEN
       V_GRN_GL_AMT2 := 0;
    END;
    
  else
  
        begin  
        SELECT sum(NVL(jl.accounted_dr,0)-NVL(jl.accounted_cr,0)) grn_amt
        INTO V_GRN_GL_AMT2
        FROM
        GL_CODE_COMBINATIONS_KFV GLCC,
        po_headers_all poh,
        po_lines_all pol,
        PO_DISTRIBUTIONS_ALL POD,
        PO_LINE_LOCATIONS_ALL PLL,--
        RCV_TRANSACTIONS RCT,
        PO_LOOKUP_CODES PL2,
        GL_IMPORT_REFERENCES R,
        RCV_RECEIVING_SUB_LEDGER  RRS,
        GL_JE_HEADERS jh,
        GL_JE_LINES	  jl
        WHERE jl.status             = 'P'
      and jl.code_combination_id 	= GLCC.code_combination_id
      and jh.status                = 'P' 
      and jh.actual_flag           = 'A'
      and jh.je_header_id          = jl.je_header_id
      and jh.je_source             = 'Purchasing' 
      and jh.je_category      	 = 'Receiving'
      and jl.effective_date        is not null   
      AND jh.JE_HEADER_ID = R.JE_HEADER_ID
      AND JL.JE_LINE_NUM = R.JE_LINE_NUM
      AND jh.ACTUAL_FLAG = RRS.ACTUAL_FLAG
      AND RRS.GL_SL_LINK_ID = R.GL_SL_LINK_ID
      AND RRS.RCV_TRANSACTION_ID = R.REFERENCE_5
      AND RCT.TRANSACTION_ID = RRS.RCV_TRANSACTION_ID
      AND RRS.REFERENCE3 = to_char(POD.PO_DISTRIBUTION_ID)
      AND PL2.LOOKUP_CODE = RCT.TRANSACTION_TYPE
      AND PL2.LOOKUP_TYPE = 'RCV TRANSACTION TYPE'
      AND R.GL_SL_LINK_TABLE = 'RSL'
      and RRS.CODE_COMBINATION_ID = GLCC.CODE_COMBINATION_ID
     
      and poh.po_header_id = pol.po_header_id      
      and pol.po_header_id = pod.po_header_id
      and pol.po_line_id = pod.po_line_id
      --and POD.BUDGET_ACCOUNT_ID = GLCC.CODE_COMBINATION_ID
      --and TAC_FA_REPORT_PKG.GET_ACCOUNT_WIP(POH.PO_HEADER_ID,POL.PO_LINE_ID) = glcc.SEGMENT11
      AND glcc.SEGMENT11 BETWEEN '17000' AND '19999' 
      and ( case when i_begin_end = 'B' and jl.EFFECTIVE_DATE <= v_last_period
          then 1
          when  i_begin_end = 'E' and jl.EFFECTIVE_DATE <= v_period
          then 1
          end ) = 1 
            
      AND poh.segment1 = P_PO_NUMBER
      AND pol.line_num = P_PO_LINE  
       --CASE RELEASE
     and PLL.po_release_id = P_RELEASE_ID
     AND POD.line_location_id = PLL.line_location_id
     AND POD.PO_HEADER_ID = PLL.PO_HEADER_ID
     AND POD.PO_LINE_ID = PLL.PO_LINE_ID
      
      group by poh.segment1,pol.line_num
      ;
      
    EXCEPTION WHEN NO_DATA_FOUND THEN
       V_GRN_GL_AMT2 := 0;
     WHEN OTHERS THEN
       V_GRN_GL_AMT2 := 0;
    END;
  
  
  
  end if;
    
    
    for rec2 in c2 
    loop
      begin
       
       V_GRN_GL_AMT := 0;
       select sum(NVL(jl.accounted_dr,0)-NVL(jl.accounted_cr,0)) grn_amt
       INTO V_GRN_GL_AMT
        from GL_CODE_COMBINATIONS_KFV GLCC
         ,GL_JE_HEADERS 	jh
         ,GL_JE_LINES	    jl
         ,GL_IMPORT_REFERENCES xla
         --,ap_invoice_distributions_ALL aid         
		 --,PO_DISTRIBUTIONS_all pod
        where  1=1
        and jl.status             = 'P'
        and jl.code_combination_id 	= GLCC.code_combination_id
        and jh.status                = 'P' 
        and jh.actual_flag           = 'A'
        and jh.je_header_id          = jl.je_header_id
        and jh.je_source             = 'Payables' 
        and jh.je_category      	 = 'Purchase Invoices'
        and jl.effective_date        is not null   
        and xla.je_header_id         = jl.je_header_id  -- 721504
        and xla.je_line_num          = jl.je_line_num   -- 4
        and xla.REFERENCE_2      = TO_CHAR(rec2.invoice_id) --to_char(V_INVOICE_ID2) -- TO_CHAR(aid.invoice_id)
        and xla.REFERENCE_3      = TO_CHAR(rec2.distribution_line_number) --to_char(v_dis_line_num2) -- TO_CHAR(aid.distribution_line_number)       
       -- AND POD.PO_DISTRIBUTION_ID = AID.PO_DISTRIBUTION_ID
        AND glcc.SEGMENT11 BETWEEN '17000' AND '19999' 
        and ( case when i_begin_end = 'B' and jl.EFFECTIVE_DATE <= v_last_period
          then 1
          when  i_begin_end = 'E' and jl.EFFECTIVE_DATE <= v_period
          then 1
          end ) = 1 
         
            group by xla.REFERENCE_2,xla.REFERENCE_3      
        ;
        
      exception WHEN NO_DATA_FOUND THEN
         V_GRN_GL_AMT := 0;
                when others then
         V_GRN_GL_AMT := 0;
      end;
      
      V_GRN_GL_AMT3 := nvl(V_GRN_GL_AMT3,0) + nvl(V_GRN_GL_AMT,0);
    end loop;

 RETURN NVL(V_GRN_GL_AMT1,0) + NVL(V_GRN_GL_AMT2,0) + nvl(V_GRN_GL_AMT3,0);

EXCEPTION WHEN OTHERS THEN
   RETURN 0;
END;

FUNCTION GET_CAPITALIZE(P_PO_NUMBER IN VARCHAR2,
                        P_PO_LINE IN VARCHAR2
                        ,i_begin_end  IN  VARCHAR2 ) RETURN NUMBER
                    
IS

v_last_period DATE;
v_period number;
V_REGIS_AMT number;
 

BEGIN

--v_last_period := GET_CUTOFF_LAST_PERIOD(SYSDATE);
--v_period      := GET_CUTOFF_PERIOD(SYSDATE);

v_period := GET_GL_PERIOD(SYSDATE);


    BEGIN
      V_REGIS_AMT := 0;
        
      SELECT --ROUND(SUM(fb.original_cost),2) REGIS_AMT
      ROUND(SUM(fb.cost),2) REGIS_AMT
      into V_REGIS_AMT
      from FA_ASSET_INVOICES fai,
      FA_BOOKS fb,
      FA_ADDITIONS_B FAB
      where 1=1
      and fai.asset_id = fb.asset_id
      and fb.asset_id = fab.asset_id
      --and fb.DATE_INEFFECTIVE is null
      --AND FB.RETIREMENT_PENDING_FLAG = 'NO'
      and fai.PO_NUMBER = P_PO_NUMBER     
      AND FAB.ATTRIBUTE5 = P_PO_LINE
     --and fai.ATTRIBUTE1 = P_PO_LINE
      AND UPPER(fb.BOOK_TYPE_CODE) NOT LIKE '%TAX%'
      AND FB.TRANSACTION_HEADER_ID_IN = ( SELECT MAX(FB1.TRANSACTION_HEADER_ID_IN) 
                                    FROM APPS.FA_BOOKS FB1,
                                     FA_TRANSACTION_HISTORY_TRX_V FT
                                    WHERE FB1.ASSET_ID = FT.ASSET_ID
                                    AND FB1.TRANSACTION_HEADER_ID_IN = ft.TRANSACTION_HEADER_ID
                                    and FB1.ASSET_ID=FB.ASSET_ID 
                                    AND FB1.BOOK_TYPE_CODE = FB.BOOK_TYPE_CODE 
         and ( case when i_begin_end = 'B' and to_number(to_char(TO_DATE(ft.PERIOD_ENTERED,'MON-YY'),'YYYYMM')) <= v_period - 1
          then 1
          when  i_begin_end = 'E' and to_number(to_char(TO_DATE(ft.PERIOD_ENTERED,'MON-YY'),'YYYYMM')) <= v_period
          then 1
          end ) = 1 
                    )
     /* and ( case when i_begin_end = 'B' and to_number(to_char(nvl(TO_DATE(FAB.ATTRIBUTE9,'YYYY/MM/DD HH24:MI:SS'),FAB.CREATION_DATE),'YYYYMM')) <= v_period - 1
          then 1
          when  i_begin_end = 'E' and to_number(to_char(nvl(TO_DATE(FAB.ATTRIBUTE9,'YYYY/MM/DD HH24:MI:SS'),FAB.CREATION_DATE),'YYYYMM')) <= v_period
          then 1
          end ) = 1 
          */
     
      ;
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END;
     
  
     
     return nvl(V_REGIS_AMT,0);
     
 END;


--**********************************************************

FUNCTION GET_AMT_FA_NO_ACC ( i_po_num        IN  varchar2
                        ,i_po_line_num   IN  NUMBER 
                        ,i_begin_end     IN  VARCHAR2) RETURN NUMBER IS
                        
  v_module          VARCHAR2(200) :=  g_program||'.'||'GET_AMT_CLOUD';
  v_fa_cloud  NUMBER;
  v_fa_writeoff NUMBER;
  v_period number;
BEGIN
  write_proc(v_module, 'BEGIN');
  v_period := GET_GL_PERIOD(SYSDATE);
  v_fa_cloud := 0;
  v_fa_writeoff := 0;

 
  
  
  begin
      SELECT SUM(COST_RETIRED) C
            INTO v_fa_writeoff 
            FROM FA_RETIREMENTS           FAR,
            FA_ASSET_INVOICES       FAI,
            FA_ADDITIONS_B FAB
            WHERE  FAR.ASSET_ID =  FAI.ASSET_ID
            AND FAI.ASSET_ID = FAB.ASSET_ID
            AND FAI.PO_NUMBER = i_po_num
            AND FAB.ATTRIBUTE5 = TO_CHAR(i_po_line_num)
            --AND FAI.ATTRIBUTE1 = TO_CHAR(i_po_line_num)
            AND FAR.STATUS = 'PROCESSED'
            AND UPPER(FAR.BOOK_TYPE_CODE) NOT LIKE '%TAX%'
            and ( CASE WHEN i_begin_end = 'B' AND TO_NUMBER(to_char(FAR.CREATION_DATE,'YYYYMM')) <=  v_period-1
            THEN 1
            WHEN  i_begin_end = 'E' AND TO_NUMBER(to_char(FAR.CREATION_DATE,'YYYYMM'))  <= v_period
            THEN 1
            END ) = 1 
          
         /*   and ( case when i_begin_end = 'B' and to_number(to_char(nvl(TO_DATE(FAB.ATTRIBUTE9,'YYYY/MM/DD HH24:MI:SS'),FAB.CREATION_DATE),'YYYYMM')) <= v_period - 1
          then 1
          when  i_begin_end = 'E' and to_number(to_char(nvl(TO_DATE(FAB.ATTRIBUTE9,'YYYY/MM/DD HH24:MI:SS'),FAB.CREATION_DATE),'YYYYMM')) <= v_period
          then 1
          end ) = 1 
          */
           /* and ( CASE WHEN i_begin_end = 'B' AND TO_NUMBER(to_char(DATE_RETIRED,'YYYYMM')) <=  v_period-1
            THEN 1
            WHEN  i_begin_end = 'E' AND TO_NUMBER(to_char(DATE_RETIRED,'YYYYMM'))  <= v_period
            THEN 1
            END ) = 1 
            */
           ;
  exception when others then
     null;
  end;

  RETURN nvl(v_fa_cloud,0) + nvl(v_fa_writeoff,0);

  write_proc(v_module, 'END');
  
END  GET_AMT_FA_NO_ACC ;

--**********************************************************

--Get Beginning  Ending GRN,Progress of Service,Capitalize
--Column M-O, DQ-DS
--**********************************************************
PROCEDURE GET_BEGIN_END_GRN( i_po_num           IN  VARCHAR2
                            ,i_po_line_num      IN  NUMBER 
                            ,i_po_release_num   in  varchar2
                            ,i_release_id       in  number
                            ,i_begin_end        IN  VARCHAR2 
                            ,o_grn              OUT NUMBER  
                            ,o_progress_amt     OUT NUMBER  
                            ,o_capitalize       OUT NUMBER ) IS
v_module           VARCHAR2(200) :=  g_program||'.'||'GET_BEGIN_END_GRN';
v_period_counter   NUMBER;
v_grn_adj          NUMBER;
v_grn_adj2          NUMBER;
v_grn_adj3          NUMBER;
v_grn_amt          NUMBER; 
v_period           NUMBER;
V_CUTOFF_DATE_B      date;
V_CUTOFF_DATE_E      date;
v_capitalize1 number;
v_capitalize2 number;
v_capitalize3 NUMBER;

BEGIN  
  write_proc(v_module, 'BEGIN');
  g_step  :=  'GET_BEGIN_END_GRN';
  write_stmn(v_module,g_step);
  
    v_period := GET_GL_PERIOD(SYSDATE);
  
    
    V_CUTOFF_DATE_B := GET_CUTOFF_LAST_PERIOD(SYSDATE);
    V_CUTOFF_DATE_E := GET_CUTOFF_PERIOD(SYSDATE);
    
  --=====================================   GET GRN ========================================--
  
   begin
        select NVL(sum(GRN_ADJUST),0)                 
          ,NVL(sum(PROGRESS_AMOUNT),0)  
        into v_grn_adj , o_progress_amt 
        from TAC_PO_ADDITION_INFO t
        where t.PO_NUMBER = i_po_num ||i_po_release_num
        and t.PO_LINE_NUMBER  = i_po_line_num 
        and ( case when i_begin_end = 'B' and to_number(to_char(to_date(PERIOD_NAME,'MON-YY'),'YYYYMM')) <= v_period-1
          then 1
          when  i_begin_end = 'E' and to_number(to_char(to_date(PERIOD_NAME,'MON-YY'),'YYYYMM')) <= v_period
          then 1
          end ) = 1  ;
   exception when no_data_found then
      v_grn_adj := 0;
      o_progress_amt := 0;
   end; 
   
   
   BEGIN
        
        SELECT SUM(AMOUNT) AMOUNT
            into v_grn_adj2
            FROM AP_INVOICE_DISTRIBUTIONS_all
            WHERE ATTRIBUTE1 = i_po_num||i_po_release_num
            AND ATTRIBUTE2 = i_po_line_num
            and ( case when i_begin_end = 'B' and TRUNC(ACCOUNTING_DATE) <= TRUNC(V_CUTOFF_DATE_B)
            then 1
            when  i_begin_end = 'E' and TRUNC(ACCOUNTING_DATE) <= TRUNC(V_CUTOFF_DATE_E)
            then 1
            end ) = 1 
           
            ;
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END;


   v_grn_amt := GET_GRN_AMT_GL(i_po_num,
                               i_po_line_num,
                               i_po_release_num,
                               i_release_id,
                               i_begin_end);
    

    o_grn := NVL(v_grn_adj,0) + NVL(v_grn_adj2,0)+ NVL(v_grn_amt,0);    --M ,DQ

    
  ---=========================================   GET CAPITALIZE =======================================================---
   begin
    select nvl(CLOUD_VALUE_REGISTER,0) 
    into v_capitalize1  
    from TAC_PO_CLOUD_INFO  
    where  PO_NUMBER = i_po_num || i_po_release_num
    and PO_LINE_NUMBER  = i_po_line_num 
    and ( CASE WHEN i_begin_end = 'B' AND to_number(to_char(to_date(PERIOD_NAME,'MON-YY'),'YYYYMM')) =  v_period-1
          THEN 1
          WHEN  i_begin_end = 'E' AND to_number(to_char(to_date(PERIOD_NAME,'MON-YY'),'YYYYMM'))  = v_period
          THEN 1
         END ) = 1  
    /* and ( CASE WHEN i_begin_end = 'B' AND to_number(to_char(to_date(PERIOD_NAME,'MON-YY'),'YYYYMM')) =  (select max(to_number(to_char(to_date(PERIOD_NAME,'MON-YY'),'YYYYMM'))) 
                                                      from TAC_PO_CLOUD_INFO  
                                                      where  PO_NUMBER = i_po_num || i_po_release_num
                                                      and PO_LINE_NUMBER  = i_po_line_num 
                                                      AND to_number(to_char(to_date(PERIOD_NAME,'MON-YY'),'YYYYMM')) <= v_period-1)
          THEN 1
          WHEN  i_begin_end = 'E' AND to_number(to_char(to_date(PERIOD_NAME,'MON-YY'),'YYYYMM')) =  (select max(to_number(to_char(to_date(PERIOD_NAME,'MON-YY'),'YYYYMM'))) 
                                                      from TAC_PO_CLOUD_INFO  
                                                      where  PO_NUMBER = i_po_num || i_po_release_num
                                                      and PO_LINE_NUMBER  = i_po_line_num 
                                                      AND to_number(to_char(to_date(PERIOD_NAME,'MON-YY'),'YYYYMM')) <= v_period)
          THEN 1
         END ) = 1   
         */  
         ;
    exception when no_data_found then
       v_capitalize1 := 0;
   end;
   
   v_capitalize2 := GET_CAPITALIZE(i_po_num||i_po_release_num
                ,i_po_line_num
                ,i_begin_end);
                
                
   v_capitalize3 := GET_AMT_FA_NO_ACC (i_po_num||i_po_release_num
                                        ,i_po_line_num
                                        ,i_begin_end) ;
                           
                
   o_capitalize := NVL(v_capitalize1,0)+NVL(v_capitalize2,0)+NVL(v_capitalize3,0);
   
   
 
 write_proc(v_module, 'END');

END GET_BEGIN_END_GRN; 
--**********************************************************

--Get Beginning Beginning WIP
--Column V,AQ
--**********************************************************
PROCEDURE GET_BEGINNING_WIP (i_po_num        IN  VARCHAR2
                            ,i_po_line_num   IN  NUMBER 
                            --,o_wip           OUT NUMBER  
                            ,o_wip_code      OUT VARCHAR2    ) IS
v_module          VARCHAR2(200) :=  g_program||'.'||'GET_BEGINNING_WIP';
v_period_counter   NUMBER;

BEGIN       
  write_proc(v_module, 'BEGIN');
  g_step  :=  'GET_BEGINNING_WIP';
  write_stmn(v_module,g_step); 
  
   begin   
 
    select TAC_FA_REPORT_PKG.GET_ACCOUNT_WIP(POD.PO_HEADER_ID,
                                          POD.PO_LINE_ID,
                                          POD.PROJECT_ID) 
     into o_wip_code
     from PO_DISTRIBUTIONS_ALL POD  
     where POD.PO_HEADER_ID  = i_po_num 
     and   POD.PO_LINE_ID  = i_po_line_num 
     and rownum =1
    ;
     
             
 exception when no_data_found then
    --o_wip := 0 ;
    o_wip_code := null;
  end;
  
  /*IF o_wip IS NULL THEN
    o_wip := 0;
  END IF;
  */
 write_proc(v_module, 'END');

END GET_BEGINNING_WIP; 
--**********************************************************

--Get Ending WIP 
--Column DZ-EU
--********************************************************** 
PROCEDURE GET_ENDING_WIP (i_po_num        IN  NUMBER
                            ,i_po_line_num   IN  NUMBER 
                            --,o_wip           OUT NUMBER  
                            ,o_wip_code      OUT VARCHAR2) IS
                            
                            
v_module           VARCHAR2(200) :=  g_program||'.'||'GET_ENDING_WIP';
v_period_counter   NUMBER;

BEGIN       
  write_proc(v_module, 'BEGIN');
  g_step  :=  'GET_ENDING_WIP';
  write_stmn(v_module,g_step); 
  

  
  begin   

     select TAC_FA_REPORT_PKG.GET_ACCOUNT_WIP(POD.PO_HEADER_ID,
                                          POD.PO_LINE_ID,
                                          POD.PROJECT_ID) 
     into o_wip_code
     from PO_DISTRIBUTIONS_ALL POD  
     where POD.PO_HEADER_ID  = i_po_num 
     and   POD.PO_LINE_ID  = i_po_line_num 
     and rownum =1
    ;
             
 exception when no_data_found then
    --o_wip := 0 ;
    o_wip_code := null;
  end;
  
  /*IF o_wip IS NULL THEN
     o_wip := null;
  END IF;*/
  
  write_proc(v_module, 'END');
  
END GET_ENDING_WIP; 
--**********************************************************
--Get Beginning Accum Depre
--Column P,BJ - BY , FN-GC
--**********************************************************
PROCEDURE GET_BEGINNING_ACCUM_DEPRE( --i_asset_id      IN  NUMBER
                                    --,i_book          IN  VARCHAR2 
                                    i_po_num        IN  varchar2
                                    ,i_po_line_num   IN  NUMBER 
                                    ,i_begin_end     IN  VARCHAR2
                                    ,o_accum_dp      OUT NUMBER  
                                    --,o_accum_code    OUT VARCHAR2   
                                     ) IS
v_module           VARCHAR2(200) :=  g_program||'.'||'GET_BEGINNING_ACCUM_DEPRE';
v_period_counter   NUMBER;

BEGIN    
   
  write_proc(v_module, 'BEGIN');
  g_step  :=  'GET_BEGINNING_ACCUM_DEPRE';
  write_stmn(v_module,g_step);
  
   
  v_period_counter:= GET_PERIOD_COUNTER ( '' );

   --P, Begining BJ-BY , Ending FN-GC
   begin
        
  select sum(DD.DEPRN_RESERVE) o_accum_dp   
         into o_accum_dp
   from FA_DEPRN_DETAIL        DD 
       ,FA_ASSET_INVOICES FAI
       ,FA_ADDITIONS_B FAB
   where dd.DEPRN_SOURCE_CODE = 'D' 
   and DD.ASSET_ID = FAI.ASSET_ID
   AND FAI.ASSET_ID = FAB.ASSET_ID
  and FAI.PO_NUMBER = i_po_num
  and FAB.ATTRIBUTE5 = i_po_line_num
  --and FAI.ATTRIBUTE1 = i_po_line_num
  and ( case when i_begin_end = 'B' and DD.PERIOD_COUNTER = (SELECT MAX(PERIOD_COUNTER) 
                                                                FROM FA_DEPRN_SUMMARY
                                                                WHERE ASSET_ID = dd.ASSET_ID --12413324
                                                                AND DEPRN_SOURCE_CODE = 'DEPRN'
                                                                and PERIOD_COUNTER <= v_period_counter  - 1
                                                                ) 
          then 1
          when  i_begin_end = 'E' and DD.PERIOD_COUNTER = (SELECT MAX(PERIOD_COUNTER) 
                                                                FROM FA_DEPRN_SUMMARY
                                                                WHERE ASSET_ID = dd.ASSET_ID --12413324
                                                                AND DEPRN_SOURCE_CODE = 'DEPRN'
                                                                and PERIOD_COUNTER <= v_period_counter
                                                                )             --Ending FN-GC
          then 1
          end ) = 1 
 /* and ( case when i_begin_end = 'B' and DD.PERIOD_COUNTER <= v_period_counter  - 1    --Beginning BJ-BY 
          then 1
          when  i_begin_end = 'E' 
                and DD.PERIOD_COUNTER > v_period_counter  - 1
                and DD.PERIOD_COUNTER <= v_period_counter             --Ending FN-GC
          then 1
          end ) = 1 */
   ;
             
 exception when no_data_found then
    o_accum_dp := 0 ;
    --o_accum_code := null;
  end;
  
  IF o_accum_dp IS NULL THEN
    o_accum_dp := 0;
  END IF;

 write_proc(v_module, 'END');

END GET_BEGINNING_ACCUM_DEPRE; 

--**********************************************************
--Generate Report 
--**********************************************************
PROCEDURE GENERTAE_REPORT( o_errbuf        OUT VARCHAR2
                          ,o_retcode       OUT NUMBER 
                          ,p_org_id        IN  NUMBER 
                          ,p_project       IN  NUMBER
                          ,p_book          IN  VARCHAR2
                          ,P_GROUP_OU      IN  VARCHAR2 )
IS
  v_module  VARCHAR2(2000) := g_program||'.'||'GENERTAE_REPORT';
  v_errmsg  VARCHAR2(2000);
  v_return  VARCHAR2(5);     
  
   
  v_BGRN        NUMBER;
  v_BPROGRESS   NUMBER;
  v_BCAP        NUMBER;
  v_EGRN        NUMBER;
  v_EPROGRESS   NUMBER;
  v_ECAP        NUMBER;
  v_BTYPE_OF_WORK NUMBER;
  v_ETYPE_OF_WORK NUMBER;  
  
  --Beginning WIP
  b_wip        NUMBER;-- := 0;
  b_wip_code   GL_CODE_COMBINATIONS.SEGMENT11%TYPE;
  b_wip_17004  NUMBER;-- := 0;  
  b_wip_17006  NUMBER;-- := 0;  
  b_wip_17010  NUMBER;-- := 0;  
  b_wip_17022  NUMBER;-- := 0;
  b_wip_17024  NUMBER;-- := 0; 
  b_wip_17028  NUMBER;-- := 0;
  b_wip_17029  NUMBER;-- := 0;
  b_wip_17030  NUMBER;-- := 0; 
  b_wip_17031  NUMBER;-- := 0;
  b_wip_18001  NUMBER;-- := 0; 
  b_wip_18003  NUMBER;-- := 0;
  b_wip_18008  NUMBER;-- := 0;  
  b_wip_18009  NUMBER;-- := 0;
  b_wip_18013  NUMBER;-- := 0;
  b_wip_19011  NUMBER;-- := 0;  
  b_wip_19107  NUMBER;-- := 0;
  b_wip_19108  NUMBER;-- := 0;
  b_wip_19111  NUMBER;-- := 0;
  b_wip_73001  NUMBER;-- := 0;
  b_wip_73002  NUMBER;-- := 0;
  b_wip_73016  NUMBER;-- := 0;
  b_wip_no     NUMBER;-- := 0;
  b_wip_accrual number;
   --Ending WIP
  e_wip        NUMBER;-- := 0;
  e_wip_code   GL_CODE_COMBINATIONS.SEGMENT11%TYPE;
  e_wip_17004  NUMBER;-- := 0;  
  e_wip_17006  NUMBER;-- := 0;  
  e_wip_17010  NUMBER;-- := 0;  
  e_wip_17022  NUMBER;-- := 0;
  e_wip_17024  NUMBER;-- := 0; 
  e_wip_17028  NUMBER;-- := 0;
  e_wip_17029  NUMBER;-- := 0;
  e_wip_17030  NUMBER;-- := 0; 
  e_wip_17031  NUMBER;-- := 0;
  e_wip_18001  NUMBER;-- := 0; 
  e_wip_18003  NUMBER;-- := 0;
  e_wip_18008  NUMBER;-- := 0;  
  e_wip_18009  NUMBER;-- := 0;
  e_wip_18013  NUMBER;-- := 0;
  e_wip_19011  NUMBER;-- := 0;  
  e_wip_19107  NUMBER;-- := 0;
  e_wip_19108  NUMBER;-- := 0;
  e_wip_19111  NUMBER;-- := 0;
  e_wip_73001  NUMBER;-- := 0;
  e_wip_73002  NUMBER;-- := 0;
  e_wip_73016  NUMBER;-- := 0;
  e_wip_no     NUMBER;-- := 0;
  e_wip_accrual number;
  --Beginning FA
  b_fa         NUMBER;-- := 0;
  bfa_code     GL_CODE_COMBINATIONS.SEGMENT11%TYPE;
  b_fa_17004   NUMBER;-- := 0; 
  b_fa_17006   NUMBER;-- := 0;
  b_fa_17007   NUMBER;-- := 0;
  b_fa_17009   NUMBER;-- := 0;
  b_fa_17018   NUMBER;-- := 0;
  b_fa_17026   NUMBER;-- := 0;
  b_fa_17034   NUMBER;-- := 0;
  b_fa_17035   NUMBER;-- := 0;
  b_fa_17043   NUMBER;-- := 0;
  b_fa_17044   NUMBER;-- := 0;
  b_fa_17045   NUMBER;-- := 0;
  b_fa_17046   NUMBER;-- := 0;
  b_fa_17047   NUMBER;-- := 0;
  b_fa_17048   NUMBER;-- := 0;
  b_fa_17049   NUMBER;-- := 0;
  b_fa_17050   NUMBER;-- := 0;
  b_fa_18021   NUMBER;-- := 0;
  b_fa_19107   NUMBER;-- := 0;
  b_fa_19108   NUMBER;-- := 0;
  b_fa_no      NUMBER;
  b_fa_no_acc   NUMBER;
 
  --Ending FA
  e_fa         NUMBER;-- := 0;
  efa_code     GL_CODE_COMBINATIONS.SEGMENT11%TYPE;
  e_fa_17004   NUMBER;-- := 0; 
  e_fa_17006   NUMBER;-- := 0;
  e_fa_17007   NUMBER;-- := 0;
  e_fa_17009   NUMBER;-- := 0;
  e_fa_17018   NUMBER;-- := 0;
  e_fa_17026   NUMBER;-- := 0;
  e_fa_17034   NUMBER;-- := 0;
  e_fa_17035   NUMBER;-- := 0;
  e_fa_17043   NUMBER;-- := 0;
  e_fa_17044   NUMBER;-- := 0;
  e_fa_17045   NUMBER;-- := 0;
  e_fa_17046   NUMBER;-- := 0;
  e_fa_17047   NUMBER;-- := 0;
  e_fa_17048   NUMBER;-- := 0;
  e_fa_17049   NUMBER;-- := 0;
  e_fa_17050   NUMBER;-- := 0;
  e_fa_18021   NUMBER;-- := 0;
  e_fa_19107   NUMBER;-- := 0;
  e_fa_19108   NUMBER;-- := 0;
  e_fa_no      NUMBER;
  e_fa_no_acc   NUMBER;
   
  ---Beginnng Accumm Depre
  b_accum_dp       NUMBER;-- := 0;
  b_accum_code     GL_CODE_COMBINATIONS.SEGMENT11%TYPE;
  b_accum_dp17106  NUMBER;-- := 0;
  b_accum_dp17107  NUMBER;-- := 0;
  b_accum_dp17118  NUMBER;-- := 0;    
  b_accum_dp17126  NUMBER;-- := 0;  
  b_accum_dp17134  NUMBER;-- := 0;
  b_accum_dp17135  NUMBER;-- := 0;
  b_accum_dp17143  NUMBER;-- := 0;
  b_accum_dp17144  NUMBER;-- := 0;
  b_accum_dp17145  NUMBER;-- := 0; 
  b_accum_dp17146  NUMBER;-- := 0;  
  b_accum_dp17147  NUMBER;-- := 0;
  b_accum_dp17148  NUMBER;-- := 0; 
  b_accum_dp17149  NUMBER;-- := 0;
  b_accum_dp18002  NUMBER;-- := 0; 
  b_accum_dp19114  NUMBER;-- := 0;  
  b_accum_dp19115  NUMBER;-- := 0; 
  --Ending Accumm Depre
  e_accum_dp       NUMBER;-- := 0;
  e_accum_code     GL_CODE_COMBINATIONS.SEGMENT11%TYPE;
  e_accum_dp17106  NUMBER;-- := 0;
  e_accum_dp17107  NUMBER;-- := 0;
  e_accum_dp17118  NUMBER;-- := 0;    
  e_accum_dp17126  NUMBER;-- := 0;  
  e_accum_dp17134  NUMBER;-- := 0;
  e_accum_dp17135  NUMBER;-- := 0;
  e_accum_dp17143  NUMBER;-- := 0;
  e_accum_dp17144  NUMBER;-- := 0;
  e_accum_dp17145  NUMBER;-- := 0; 
  e_accum_dp17146  NUMBER;-- := 0;  
  e_accum_dp17147  NUMBER;-- := 0;
  e_accum_dp17148  NUMBER;-- := 0; 
  e_accum_dp17149  NUMBER;-- := 0;
  e_accum_dp18002  NUMBER;-- := 0; 
  e_accum_dp19114  NUMBER;-- := 0;  
  e_accum_dp19115  NUMBER;-- := 0; 
  
  --Depre
  v_dp       FA_DEPRN_DETAIL.DEPRN_AMOUNT%TYPE;
  v_dp_code   GL_CODE_COMBINATIONS.SEGMENT11%TYPE;
  v_dp_50301  NUMBER;-- := 0; 
  v_dp_55706  NUMBER;-- := 0; 
  v_dp_55707  NUMBER;-- := 0; 
  v_dp_55709  NUMBER;-- := 0; 
  v_dp_55726  NUMBER;-- := 0; 
  v_dp_55734  NUMBER;-- := 0; 
  v_dp_55735  NUMBER;-- := 0; 
  v_dp_55803  NUMBER;-- := 0; 
  v_dp_55804  NUMBER;-- := 0; 
  v_dp_no     NUMBER;-- := 0; 
  
  --ktp 20170505
  v_strim_code varchar2(80);
  V_PERIOD_NAME VARCHAR2(50);
  b_booked_amt number;
  e_booked_amt number;
  
  b_prepaid number;
  e_prepaid number;
  
  b_prepaid_accrual number;
  e_prepaid_accrual number;
  v_amt_cloud NUMBER;
  
  
  
v_MGRN number;
    v_MPROGRESS number;
    v_MCAP number;
    m_accum_dp number;
    m_booked_amt number;
    m_prepaid number;
    m_prepaid_accrual number;
    m_wip_accrual number;
    m_wip_17028 number;
    m_wip_17029 number;
    m_wip_18003 number;
    m_wip_19011 number;
    m_wip_73001 number;
    m_wip_73002 number;
    
    m_fa_17004 number;
    m_fa_17006 number;
    m_fa_17007 number;
    m_fa_17009 number;
    m_fa_17018 number;
    m_fa_17026 number;    
    m_fa_17034 number;
    m_fa_17035 number;
    m_fa_17043 number;
    m_fa_17044 number;
    m_fa_17045 number;
    m_fa_17046 number;
    m_fa_17047 number;
    m_fa_17048 number;
    m_fa_17049 number;
    m_fa_17050 number;
    m_fa_19107 number;
    m_fa_19108 number;
    m_fa_no number;
    
    P_REQUEST_ID  NUMBER;
    
    v_ou number;
 
--ktp 20170918   
cursor ou is
SELECT ou.organization_id OU,name
FROM hr_operating_units  ou
where ou.organization_id = nvl(P_ORG_ID,ou.organization_id)
AND NAME LIKE P_GROUP_OU||'%';
   
  CURSOR CUR_DATA(pin_request_id number) IS
  select distinct  COMPANY,
  PRO_GROUP,
  DIVISION,
  PROJECT_CODE,
  PROJECT_NAME,
  PO_NUM,
  RELEASE_NUM,
  LINE_NUM,
  PO_RELEASE_ID,
  ITEM_DESC,
  VENDOR_NAME,
  TYPE_OF_WORK,
  TYPE_OF_WORK_LAST,
  PO_STATUS,
  PO_HEADER_ID,
  PO_LINE_ID
   from TAC_ASSET_MONTHLY_TEMP
   where request_id = pin_request_id
   order by  po_num,line_num ;
    /*SELECT distinct
        (SELECT ou.NAME
            FROM hr_operating_units  ou
            WHERE organization_id = POH.ORG_ID )  COMPANY     --A      
          ,PPA.ATTRIBUTE1         PRO_GROUP  --B
          ,(select replace(replace(FV.description,CHR(10),''),CHR(9),'') 
            from FND_FLEX_VALUE_SETS FS, 
            FND_FLEX_VALUES_VL FV
            where FV.FLEX_VALUE = PPA.ATTRIBUTE2
            AND FS.FLEX_VALUE_SET_ID = FV.FLEX_VALUE_SET_ID
            AND FS.FLEX_VALUE_SET_NAME = 'DTN Group') DIVISION    --C 
          ,PPA.SEGMENT1           PROJECT_CODE   --D   
          ,PPA.NAME               PROJECT_NAME--E    
          ,POH.SEGMENT1           PO_NUM                  --F
          ,TAC_FA_REPORT_PKG.GET_RELEASE_NUM(PLL.PO_RELEASE_ID) release_num
          ,POL.LINE_NUM           LINE_NUM                --G
          ,PLL.PO_RELEASE_ID
          ,replace(replace(POL.ITEM_DESCRIPTION,CHR(10),''),CHR(9),'')   ITEM_DESC   --H 
          ,PV.VENDOR_NAME         VENDOR_NAME --K 
          ,nvl(TAC_FA_REPORT_PKG.GET_TYPE_OF_WORK(pod.REQ_DISTRIBUTION_ID,POH.SEGMENT1,POL.LINE_NUM),'u') TYPE_OF_WORK  --I
          ,nvl(TAC_WIP_ACC_REPORT_PKG.GET_TYPE_OF_WORK_LASTPERIOD(pod.REQ_DISTRIBUTION_ID,POH.SEGMENT1,POL.LINE_NUM),'u') TYPE_OF_WORK_LAST
          ,POH.CLOSED_CODE        PO_STATUS   --J    
          ,POD.PO_HEADER_ID  
          ,POL.PO_LINE_ID  
    FROM PO_HEADERS_ALL           POH
        ,PO_LINES_ALL             POL
        ,PO_LINE_LOCATIONS_ALL    PLL
        ,PO_VENDORS               PV
        ,PO_DISTRIBUTIONS_ALL     POD  
        --,GL_CODE_COMBINATIONS     GCC     
        ,PA_PROJECTS_ALL          PPA  
    WHERE POH.PO_HEADER_ID      = POL.PO_HEADER_ID
    AND POH.VENDOR_ID           = PV.VENDOR_ID
    AND POH.PO_HEADER_ID        = POD.PO_HEADER_ID  
    AND POL.PO_LINE_ID          = POD.PO_LINE_ID  
    and POD.LINE_LOCATION_ID    = pll.LINE_LOCATION_ID
    AND POL.PO_HEADER_ID        = PLL.PO_HEADER_ID
    AND POL.PO_LINE_ID          = PLL.PO_LINE_ID
    --AND POD.CODE_COMBINATION_ID = GCC.CODE_COMBINATION_ID 
    AND POD.PROJECT_ID          = PPA.PROJECT_ID(+)
    AND POH.AUTHORIZATION_STATUS = 'APPROVED'
    AND NVL(POL.CANCEL_FLAG,'N') != 'Y' 
    ---------PARAMETER---------------------------
    AND POH.ORG_ID = P_ORG_ID
    AND (TAC_FA_REPORT_PKG.GET_ACCOUNT_WIP(POH.PO_HEADER_ID,POL.PO_LINE_ID,POD.PROJECT_ID) --is not null
    IN (select lookup_code from fnd_lookup_values Where lookup_type = 'TAC_DTN_ASSET_REP_ACCOUNT')
          or  TAC_FA_REPORT_PKG.CHECK_POADD_RP(POH.SEGMENT1,POL.LINE_NUM) = 'Y' 
        )
     AND  NVL(TAC_FA_REPORT_PKG.CHECK_POADD_RP(POH.SEGMENT1,POL.LINE_NUM),'Y') != 'N'
     AND  NVL(TAC_FA_REPORT_PKG.CHECK_DISPLAY_PROJECT(POH.SEGMENT1,POL.LINE_NUM),'Y') != 'N'
     
   -- AND POH.SEGMENT1 = '8012000317' 
   -- AND POL.LINE_NUM = 1
    AND ( PPA.PROJECT_ID    =  P_PROJECT  OR P_PROJECT IS NULL )
    ORDER BY POH.SEGMENT1         
            ,POL.LINE_NUM;
            */
 
  
       
 cursor c_deprn(P_PO_NUMBER varchar2
               ,P_PO_LINE number
               ,p_period_counter number)
 is
 select sum(DD.DEPRN_AMOUNT) o_dp
          ,GCC.SEGMENT11  o_dp_code   
   from FA_DEPRN_DETAIL        DD 
       ,FA_DISTRIBUTION_HISTORY dh
       ,GL_CODE_COMBINATIONS  GCC
       ,FA_ASSET_INVOICES FAI
       ,FA_ADDITIONS_B FAB
   where dd.asset_id = dh.asset_id  
   AND DD.DISTRIBUTION_ID = DH.DISTRIBUTION_ID
   and DEPRN_SOURCE_CODE = 'D'
   --AND DD.DEPRN_EXPENSE_CCID = GCC.CODE_COMBINATION_ID  
   AND DH.CODE_COMBINATION_ID = GCC.CODE_COMBINATION_ID  
   and DD.ASSET_ID = FAI.ASSET_ID
   and FAI.ASSET_ID = FAB.ASSET_ID  
  and FAI.PO_NUMBER = P_PO_NUMBER
  and FAB.ATTRIBUTE5 = P_PO_LINE
  --and FAI.ATTRIBUTE1 = P_PO_LINE
  and DD.PERIOD_COUNTER < p_period_counter
  group by GCC.SEGMENT11;
  

  cursor c_fa(P_PO_NUMBER varchar2
              ,P_PO_LINE number
              ,P_PERIOD number
              ,I_BEGIN_END VARCHAR2)
  is
  SELECT  
  FCBB.ASSET_COST_ACCT
  ,ROUND(SUM(fb.cost),2) cost_fa
  --,ROUND(SUM(fb.original_cost),2) cost_fa
  FROM FA_CATEGORY_BOOKS FCBB,
  FA_ASSET_HISTORY FAH,
  FA_ASSET_INVOICES FAI,
  FA_ADDITIONS_B FAB,
  FA_BOOKS fb
  WHERE FCBB.CATEGORY_ID  = FAH.CATEGORY_ID
  AND FAH.ASSET_ID = FAI.ASSET_ID
  and fab.asset_id = fai.asset_id
  and FAI.PO_NUMBER = P_PO_NUMBER --'8015000011'
  --and FAI.ATTRIBUTE1 = i_po_line_num
  AND FAB.ATTRIBUTE5 = P_PO_LINE 
  and fai.asset_id = fb.asset_id
  and fb.asset_id = fab.asset_id
  and fah.DATE_INEFFECTIVE is null
  --and fb.DATE_INEFFECTIVE is null
  --AND FB.RETIREMENT_PENDING_FLAG = 'NO'
  and fb.book_type_code = fcbb.book_type_code
  AND UPPER(fb.BOOK_TYPE_CODE) NOT LIKE '%TAX%'
  AND FB.TRANSACTION_HEADER_ID_IN = ( SELECT MAX(FB1.TRANSACTION_HEADER_ID_IN) 
                                    FROM APPS.FA_BOOKS FB1,
                                     FA_TRANSACTION_HISTORY_TRX_V FT
                                    WHERE FB1.ASSET_ID = FT.ASSET_ID
                                    AND FB1.TRANSACTION_HEADER_ID_IN = ft.TRANSACTION_HEADER_ID
                                    and FB1.ASSET_ID=FB.ASSET_ID 
                                    AND FB1.BOOK_TYPE_CODE = FB.BOOK_TYPE_CODE 
         and ( case when i_begin_end = 'B' and to_number(to_char(TO_DATE(ft.PERIOD_ENTERED,'MON-YY'),'YYYYMM')) <= p_period - 1
          then 1
          when  i_begin_end = 'E' and to_number(to_char(TO_DATE(ft.PERIOD_ENTERED,'MON-YY'),'YYYYMM')) <= p_period
          then 1
          end ) = 1 
                    )
  /*and ( case when i_begin_end = 'B' and to_number(to_char(nvl(TO_DATE(FAB.ATTRIBUTE9,'YYYY/MM/DD HH24:MI:SS'),FAB.CREATION_DATE),'YYYYMM')) <= p_period - 1
          then 1
          when  i_begin_end = 'E' and to_number(to_char(nvl(TO_DATE(FAB.ATTRIBUTE9,'YYYY/MM/DD HH24:MI:SS'),FAB.CREATION_DATE),'YYYYMM')) <= p_period
          then 1
          end ) = 1 */
  group by FCBB.ASSET_COST_ACCT
  
  union all
  
  select '00000' ASSET_COST_ACCT
    ,nvl(CLOUD_VALUE_REGISTER,0) COST_FA
    from TAC_PO_CLOUD_INFO  
    where  PO_NUMBER = P_PO_NUMBER
    and PO_LINE_NUMBER  = P_PO_LINE 
    and ( CASE WHEN i_begin_end = 'B' AND to_number(to_char(to_date(PERIOD_NAME,'MON-YY'),'YYYYMM')) =  p_period - 1
          THEN 1
          WHEN  i_begin_end = 'E' AND to_number(to_char(to_date(PERIOD_NAME,'MON-YY'),'YYYYMM'))  = p_period
          THEN 1
         END ) = 1 
   /* and ( CASE WHEN i_begin_end = 'B' AND to_number(to_char(to_date(PERIOD_NAME,'MON-YY'),'YYYYMM')) = (select max(to_number(to_char(to_date(PERIOD_NAME,'MON-YY'),'YYYYMM'))) 
                                                      from TAC_PO_CLOUD_INFO  
                                                      where  PO_NUMBER = P_PO_NUMBER
                                                      and PO_LINE_NUMBER  = P_PO_LINE 
                                                      AND to_number(to_char(to_date(PERIOD_NAME,'MON-YY'),'YYYYMM')) <= p_period-1)
          THEN 1
          WHEN  i_begin_end = 'E' AND to_number(to_char(to_date(PERIOD_NAME,'MON-YY'),'YYYYMM')) = (select max(to_number(to_char(to_date(PERIOD_NAME,'MON-YY'),'YYYYMM'))) 
                                                      from TAC_PO_CLOUD_INFO  
                                                      where  PO_NUMBER = P_PO_NUMBER
                                                      and PO_LINE_NUMBER  = P_PO_LINE 
                                                      AND to_number(to_char(to_date(PERIOD_NAME,'MON-YY'),'YYYYMM')) <= p_period)
          THEN 1
         END ) = 1 
         */
  ;
      
BEGIN


P_REQUEST_ID := fnd_global.conc_request_id;

DELETE FROM TAC_ASSET_MONTHLY_TEMP
WHERE REQUEST_ID < P_REQUEST_ID - 500;
COMMIT;
--ktp 20170915
for o in ou
 loop
   begin
    
    v_ou := null;
    
    IF P_ORG_ID IS NULL 
       AND P_GROUP_OU IS NULL
       AND O.NAME NOT LIKE 'TAC OU%' 
       AND O.NAME NOT LIKE 'DTAC-N' THEN
        v_ou := O.OU;
    ELSIF P_ORG_ID IS NULL 
      AND P_GROUP_OU IS NOT NULL THEN
        v_ou := O.OU;
    ELSIF P_ORG_ID IS NOT NULL THEN
        v_ou := P_ORG_ID;
    END IF;
    
    ----======================================= main cursor ===========================================----

  insert into TAC_ASSET_MONTHLY_TEMP
  (COMPANY,
  PRO_GROUP,
  DIVISION,
  PROJECT_CODE,
  PROJECT_NAME,
  PO_NUM,
  RELEASE_NUM,
  LINE_NUM,
  PO_RELEASE_ID,
  ITEM_DESC,
  VENDOR_NAME,
  TYPE_OF_WORK,
  TYPE_OF_WORK_LAST,
  PO_STATUS,
  PO_HEADER_ID,
  PO_LINE_ID,
  REQUEST_ID)
    SELECT 
        (SELECT ou.NAME
            FROM hr_operating_units  ou
            WHERE organization_id = POH.ORG_ID )  COMPANY     --A      
          ,PPA.ATTRIBUTE1         PRO_GROUP  --B
          ,(select replace(replace(FV.description,CHR(10),''),CHR(9),'') 
            from FND_FLEX_VALUE_SETS FS, 
            FND_FLEX_VALUES_VL FV
            where FV.FLEX_VALUE = PPA.ATTRIBUTE2
            AND FS.FLEX_VALUE_SET_ID = FV.FLEX_VALUE_SET_ID
            AND FS.FLEX_VALUE_SET_NAME = 'DTN Group') DIVISION    --C 
          ,PPA.SEGMENT1           PROJECT_CODE   --D   
          ,PPA.NAME               PROJECT_NAME--E    
          ,POH.SEGMENT1           PO_NUM                  --F
          ,TAC_FA_REPORT_PKG.GET_RELEASE_NUM(PLL.PO_RELEASE_ID) release_num
          ,POL.LINE_NUM           LINE_NUM                --G
          ,PLL.PO_RELEASE_ID
          ,replace(replace(POL.ITEM_DESCRIPTION,CHR(10),''),CHR(9),'')   ITEM_DESC   --H 
          ,PV.VENDOR_NAME         VENDOR_NAME --K 
          ,nvl(TAC_FA_REPORT_PKG.GET_TYPE_OF_WORK(pod.REQ_DISTRIBUTION_ID,POH.SEGMENT1,POL.LINE_NUM),'u') TYPE_OF_WORK  --I
          ,nvl(TAC_WIP_ACC_REPORT_PKG.GET_TYPE_OF_WORK_LASTPERIOD(pod.REQ_DISTRIBUTION_ID,POH.SEGMENT1,POL.LINE_NUM),'u') TYPE_OF_WORK_LAST
          ,POH.CLOSED_CODE        PO_STATUS   --J    
          ,POD.PO_HEADER_ID  
          ,POL.PO_LINE_ID  
          ,P_REQUEST_ID
    FROM PO_HEADERS_ALL           POH
        ,PO_LINES_ALL             POL
        ,PO_LINE_LOCATIONS_ALL    PLL
        ,PO_VENDORS               PV
        ,PO_DISTRIBUTIONS_ALL     POD  
        --,GL_CODE_COMBINATIONS     GCC     
        ,PA_PROJECTS_ALL          PPA  
    WHERE POH.PO_HEADER_ID      = POL.PO_HEADER_ID
    AND POH.VENDOR_ID           = PV.VENDOR_ID
    AND POH.PO_HEADER_ID        = POD.PO_HEADER_ID  
    AND POL.PO_LINE_ID          = POD.PO_LINE_ID  
    and POD.LINE_LOCATION_ID    = pll.LINE_LOCATION_ID
    AND POL.PO_HEADER_ID        = PLL.PO_HEADER_ID
    AND POL.PO_LINE_ID          = PLL.PO_LINE_ID
    --AND POD.CODE_COMBINATION_ID = GCC.CODE_COMBINATION_ID 
    AND POD.PROJECT_ID          = PPA.PROJECT_ID(+)
    AND POH.AUTHORIZATION_STATUS = 'APPROVED'
    AND NVL(POL.CANCEL_FLAG,'N') != 'Y' 
    ---------PARAMETER---------------------------
    AND POH.ORG_ID = v_ou --ktp 20170915 P_ORG_ID
    AND (TAC_FA_REPORT_PKG.GET_ACCOUNT_WIP(POH.PO_HEADER_ID,POL.PO_LINE_ID,POD.PROJECT_ID) --is not null
    IN (select lookup_code from fnd_lookup_values Where lookup_type = 'TAC_DTN_ASSET_REP_ACCOUNT')
          or  TAC_FA_REPORT_PKG.CHECK_POADD_RP(POH.SEGMENT1,POL.LINE_NUM) = 'Y' 
        )
     AND  NVL(TAC_FA_REPORT_PKG.CHECK_POADD_RP(POH.SEGMENT1,POL.LINE_NUM),'Y') != 'N'
     AND  NVL(TAC_FA_REPORT_PKG.CHECK_DISPLAY_PROJECT(POH.SEGMENT1,POL.LINE_NUM),'Y') != 'N'
 --   ORDER BY POH.SEGMENT1          ,POL.LINE_NUM;
    ;
    
    commit;
    
  end;
 end loop;
    
    
    
  write_proc(v_module, 'BEGIN'); 
   fnd_file.put_line(fnd_file.output,'TAC Asset Monthly Report');
   fnd_file.put_line(fnd_file.output,'Request ID:'||fnd_global.conc_request_id);
   fnd_file.put_line(fnd_file.output,'Date/Time:'||to_char(sysdate,'DD-MON-RRRR HH24:MI:SS'));
   
  fnd_file.put_line(fnd_file.output,
         --A-L
         'Company'||'|'||'Group'||'|'||'Division'||'|'||'Project'||'|'||'Project Name'||'|'||'PO'||'|'||'PO Line'||'|'||
         'Item Description'||'|'||'Type of Work' ||'|'||'PO Closure Status'||'|'||'Vendor Name'||'|'||'Strim Code'
         --M-O
         ||'|'||'Beginning GRN'||'|'||'Beginning Progress of Service'||'|'||'Beginning Capitalize'
         --P-S
         ||'|'||'Beginning Accum Depre'||'|'||'Beginning CAPEX Booking'||'|'||'Beginning Prepaid'||'|'||'Beginning Prepaid Accrual'
         --T,U
         ||'|'||'Beginning WIP Account'||'|'||'Beginning WIP Accrual'
         --Beginning WIP : V-AQ
         ||'|'||'Beginning WIP 17004'||'|'||'Beginning WIP 17006'||'|'||'Beginning WIP 17010'||'|'||'Beginning WIP 17022'||'|'||'Beginning WIP 17024'||'|'||'Beginning WIP 17028'
         ||'|'||'Beginning WIP 17029'||'|'||'Beginning WIP 17030'||'|'||'Beginning WIP 17031'||'|'||'Beginning WIP 18001'||'|'||'Beginning WIP 18003'||'|'||'Beginning WIP 18008'
         ||'|'||'Beginning WIP 18009'||'|'||'Beginning WIP 18013'||'|'||'Beginning WIP 19011'||'|'||'Beginning WIP 19107'||'|'||'Beginning WIP 19108'||'|'||'Beginning WIP 19111'
         ||'|'||'Beginning WIP 73001'||'|'||'Beginning WIP 73002'||'|'||'Beginning WIP 73016'||'|'||'Beginning WIP No account'
         --Beginning FA : AR-BI
         ||'|'||'Beginning FA 17004'||'|'||'Beginning FA 17006'||'|'||'Beginning FA 17007'||'|'||'Beginning FA 17009'||'|'||'Beginning FA 17018'||'|'||'Beginning FA 17026'
         ||'|'||'Beginning FA 17034'||'|'||'Beginning FA 17035'||'|'||'Beginning FA 17043'||'|'||'Beginning FA 17044'||'|'||'Beginning FA 17045'||'|'||'Beginning FA 17046'
         ||'|'||'Beginning FA 17047'||'|'||'Beginning FA 17048'||'|'||'Beginning FA 17049'||'|'||'Beginning FA 17050'||'|'||'Beginning FA 19107'||'|'||'Beginning FA 19108'||'|'||'Beginning FA No account'
         --Beginning Accum Depre : BJ-BY
         /*
         ||'|'||'Beginning Accum Depre 17106'||'|'||'Beginning Accum Depre 17107'||'|'||'Beginning Accum Depre 17118'||'|'||'Beginning Accum Depre 17126'||'|'||'Beginning Accum Depre 17134'
         ||'|'||'Beginning Accum Depre 17135'||'|'||'Beginning Accum Depre 17143'||'|'||'Beginning Accum Depre 17144'||'|'||'Beginning Accum Depre 17145'||'|'||'Beginning Accum Depre 17146'
         ||'|'||'Beginning Accum Depre 17147'||'|'||'Beginning Accum Depre 17148'||'|'||'Beginning Accum Depre 17149'||'|'||'Beginning Accum Depre 18002'||'|'||'Beginning Accum Depre 19114'
         ||'|'||'Beginning Accum Depre 19115'
         */
         --BZ-CG
         ||'|'||'Movement GRN'||'|'||'Movement Progress of Service'||'|'||'Movement Capitalize'||'|'||'Movement Accum Depre'||'|'||'Movement CAPEX Booking'||'|'||'Movement Prepaid'
         ||'|'||'Movement Prepaid Accrual'||'|'||'Movement WIP Accrual'
         --Movement WIP : CH-CM
         ||'|'||'Movement WIP 17028'||'|'||'Movement WIP 17029'||'|'||'Movement WIP 18003'||'|'||'Movement WIP 19011'||'|'||'Movement WIP 73001'||'|'||'Movement WIP No Account' --'Movement WIP 73002'
         --Movement FA :CN-CY
         ||'|'||'Movement FA 17004'||'|'||'Movement FA 17006'||'|'||'Movement FA 17007'||'|'||'Movement FA 17009'||'|'||'Movement FA 17018'||'|'||'Movement FA 17026'
         ||'|'||'Movement FA 17034'||'|'||'Movement FA 17035'||'|'||'Movement FA 17043'||'|'||'Movement FA 17044'||'|'||'Movement FA 17045'||'|'||'Movement FA 17046'
         ||'|'||'Movement FA 17047'||'|'||'Movement FA 17048'||'|'||'Movement FA 17049'||'|'||'Movement FA 17050'||'|'||'Movement FA 19107'||'|'||'Movement FA 19108'||'|'||'Movement FA No Account'    
         
         --Movement Accum Depre  :DA-DP
         /*
         ||'|'||'Accum Depre 17106'||'|'||'Accum Depre 17107'||'|'||'Accum Depre 17118'||'|'||'Accum Depre 17126'||'|'||'Accum Depre 17134'||'|'||'Accum Depre 17135'||'|'||'Accum Depre 17143'
         ||'|'||'Accum Depre 17144'||'|'||'Accum Depre 17145'||'|'||'Accum Depre 17146'||'|'||'Accum Depre 17147'||'|'||'Accum Depre 17148'||'|'||'Accum Depre 17149'||'|'||'Accum Depre 18002'
         ||'|'||'Accum Depre 19114'||'|'||'Accum Depre 19115'
         */
         --DQ-DS
         ||'|'||'Ending GRN'||'|'||'Ending Progress of Service'||'|'||'Ending Capitalize'
         --DT-DW
         ||'|'||'Ending Accum Depre'||'|'||'Ending CAPEX Booking'||'|'||'Ending Prepaid'||'|'||'Ending Prepaid Accrual'
         --DX-DY
         ||'|'||'Ending WIP Account'||'|'||'Ending WIP Accrual'
         --Ending WIP : DZ-EU
         ||'|'||'Ending WIP 17004'||'|'||'Ending WIP 17006'||'|'||'Ending WIP 17010'||'|'||'Ending WIP 17022'||'|'||'Ending WIP 17024'||'|'||'Ending WIP 17028'||'|'||'Ending WIP 17029'
         ||'|'||'Ending WIP 17030'||'|'||'Ending WIP 17031'||'|'||'Ending WIP 18001'||'|'||'Ending WIP 18003'||'|'||'Ending WIP 18008'||'|'||'Ending WIP 18009'||'|'||'Ending WIP 18013'
         ||'|'||'Ending WIP 19011'||'|'||'Ending WIP 19107'||'|'||'Ending WIP 19108'||'|'||'Ending WIP 19111'||'|'||'Ending WIP 73001'||'|'||'Ending WIP 73002'||'|'||'Ending WIP 73016'
         ||'|'||'Ending WIP No account'
         --Ending FA : EV-FM
         ||'|'||'Ending FA 17004'||'|'||'Ending FA 17006'||'|'||'Ending FA 17007'||'|'||'Ending FA 17009'||'|'||'Ending FA 17018'||'|'||'Ending FA 17026'
         ||'|'||'Ending FA 17034'||'|'||'Ending FA 17035'||'|'||'Ending FA 17043'||'|'||'Ending FA 17044'||'|'||'Ending FA 17045'||'|'||'Ending FA 17046'
         ||'|'||'Ending FA 17047'||'|'||'Ending FA 17048'||'|'||'Ending FA 17049'||'|'||'Ending FA 17050'||'|'||'Ending FA 19107'||'|'||'Ending FA 19108'||'|'||'Ending FA No Account'
         --Ending Accum Depre : FN-GC
         /*
         ||'|'||'Ending Accum Depre 17106'||'|'||'Ending Accum Depre 17107'||'|'||'Ending Accum Depre 17118'||'|'||'Ending Accum Depre 17126'||'|'||'Ending Accum Depre 17134'
         ||'|'||'Ending Accum Depre 17135'||'|'||'Ending Accum Depre 17143'||'|'||'Ending Accum Depre 17144'||'|'||'Ending Accum Depre 17145'||'|'||'Ending Accum Depre 17146'
         ||'|'||'Ending Accum Depre 17147'||'|'||'Ending Accum Depre 17148'||'|'||'Ending Accum Depre 17149'||'|'||'Ending Accum Depre 18002'||'|'||'Ending Accum Depre 19114'
         ||'|'||'Ending Accum Depre 19115'  
         */
         --Depre GD-GM
         ||'|'||'Depre 50301'||'|'||'Depre 55706'||'|'||'Depre 55707'||'|'||'Depre 55709'||'|'||'Depre 55726'||'|'||'Depre 55734'||'|'||'Depre 55735'
         ||'|'||'Depre 55803'||'|'||'Depre 55804'||'|'||'Depre No Account'   
         --GN     
         ||'|'||'Amount Not in FA Module' );
         
         
 
                         
  <<loop_data>>
  FOR i IN CUR_DATA(P_REQUEST_ID)
  LOOP    
    
    write_log('in loop ');
    V_PERIOD_NAME := GET_PERIOD_NAME(sysdate);
    --Column L
    v_strim_code := null;
    begin
       select strim_code
       into v_strim_code
       from TAC_PO_ADDITION_INFO
       where po_number = i.PO_NUM||i.release_num
       and po_line_number = i.line_num
       and period_name = V_PERIOD_NAME;
    exception when others then
       v_strim_code := null;
    end;  
    
   
    write_log('2 ');
    
    
  
    --Column M-O
   GET_BEGIN_END_GRN(i_po_num        => i.PO_NUM
                    ,i_po_line_num    => i.LINE_NUM 
                    ,i_po_release_num => i.release_num
                    ,i_release_id     => i.po_release_id
                    ,i_begin_end      => 'B'
                    ,o_grn            => v_BGRN 
                    ,o_progress_amt   => v_BPROGRESS
                    ,o_capitalize     => v_BCAP);
                    
    write_log('3 ');
    --Column DQ-DS
   GET_BEGIN_END_GRN(i_po_num      => i.PO_NUM
                    ,i_po_line_num    => i.LINE_NUM 
                    ,i_po_release_num => i.release_num
                    ,i_release_id     => i.po_release_id
                    ,i_begin_end      => 'E'
                    ,o_grn            => v_EGRN
                    ,o_progress_amt   => v_EPROGRESS
                    ,o_capitalize     => v_ECAP);
    write_log('4 ');
    
     b_booked_amt := 0;
     b_wip := 0;
     b_wip_accrual := 0;
     b_prepaid := 0;
     b_prepaid_accrual := 0;
   
     IF upper(i.TYPE_OF_WORK_LAST) in ('E','U') THEN       
        b_booked_amt := nvl(GREATEST(v_BGRN,v_BCAP),0);       
     ELSIF upper(i.TYPE_OF_WORK_LAST) = 'S' THEN       
        b_booked_amt := nvl(GREATEST(v_BCAP,v_BGRN,v_BPROGRESS),0); 
     ELSIF upper(i.TYPE_OF_WORK_LAST) = 'T' THEN
        b_booked_amt := nvl(v_BCAP,0);
     ELSIF upper(i.TYPE_OF_WORK_LAST) = 'O' THEN
        b_booked_amt := 0; 
     ELSE
        b_booked_amt := 0;           
     END IF;
     
     --==  wip ==--
     IF upper(i.TYPE_OF_WORK_LAST) IN ('E','S','U') THEN
        b_wip :=  NVL(b_booked_amt,0) - NVL(v_BCAP,0);
     ELSE
        b_wip := 0;
     END IF;
     
     --==  wip accrual ==--
     IF upper(i.TYPE_OF_WORK_LAST) IN ('E','U') THEN 
      b_wip_accrual :=  NVL(v_BCAP,0) - NVL(v_BGRN,0); 
    ELSIF  upper(i.TYPE_OF_WORK_LAST) IN ('S') THEN
      b_wip_accrual :=  NVL(b_booked_amt,0) - NVL(v_BGRN,0);
    ELSE
      b_wip_accrual := 0;
    END IF;

     --==  prepaid ==--
     IF upper(i.TYPE_OF_WORK_LAST) = 'T' THEN
        b_prepaid:= NVL(v_BGRN,0) - NVL(v_BCAP,0);
     ELSE
        b_prepaid := 0;
     END IF;
     
     --==  prepaid accrual ==--
     IF upper(i.TYPE_OF_WORK_LAST) IN ('T') THEN
        b_PREPAID_ACCRUAL := NVL(v_BCAP,0) - NVL(v_BGRN,0);
     ELSE
        b_PREPAID_ACCRUAL := 0;
     END IF;
     
     --==================== set 0 ===============--
     if b_booked_amt < 0 then
       b_booked_amt := 0;
     end if;
     
     if b_wip < 0 then
       b_wip := 0;
     end if;
     
     if b_wip_accrual < 0 then
       b_wip_accrual := 0;
     end if;
     
     if b_prepaid < 0 then
       b_prepaid := 0;
     end if;
     
     if b_PREPAID_ACCRUAL < 0 then
       b_PREPAID_ACCRUAL := 0;
     end if;
    
    
    
    
    
    --Beginning WIP V-AQ   
    b_wip_code := null; 
    GET_BEGINNING_WIP(i_po_num       => i.po_header_id --i.PO_NUM 
                     ,i_po_line_num  => i.po_line_id --i.LINE_NUM  
                     --,o_wip          => b_wip
                     ,o_wip_code     => b_wip_code) ;
                     
    write_log('5 ');  
    
  b_wip_17004  := 0;  
  b_wip_17006  := 0; 
  b_wip_17010  := 0; 
  b_wip_17022  := 0; 
  b_wip_17024  := 0; 
  b_wip_17028  := 0; 
  b_wip_17029  := 0; 
  b_wip_17030  := 0; 
  b_wip_17031  := 0; 
  b_wip_18001  := 0; 
  b_wip_18003  := 0; 
  b_wip_18008  := 0; 
  b_wip_18009  := 0; 
  b_wip_18013  := 0; 
  b_wip_19011  := 0; 
  b_wip_19107  := 0; 
  b_wip_19108  := 0; 
  b_wip_19111  := 0; 
  b_wip_73001  := 0; 
  b_wip_73002  := 0; 
  b_wip_73016  := 0; 
  b_wip_no     := 0; 
            
    if b_wip_code  = 17004  then
       b_wip_17004  := b_wip;
       b_wip_no  := b_wip;  
    elsif b_wip_code  = 17006  then
       b_wip_17006  := b_wip;
       b_wip_no  := b_wip;  
    elsif b_wip_code  = 17010  then
       b_wip_17010  := b_wip;
       b_wip_no  := b_wip;  
    elsif b_wip_code  = 17022  then
       b_wip_17022  := b_wip;
       b_wip_no  := b_wip;
    elsif b_wip_code  = 17024  then
       b_wip_17024  := b_wip;
       b_wip_no  := b_wip;  
    elsif b_wip_code  = 17028  then
       b_wip_17028  := b_wip;
    elsif b_wip_code  = 17029  then
       b_wip_17029  := b_wip;  
    elsif b_wip_code  = 17030  then
       b_wip_17030  := b_wip; 
       b_wip_no  := b_wip; 
    elsif b_wip_code  = 17031  then
       b_wip_17031  := b_wip;
       b_wip_no  := b_wip;
    elsif b_wip_code  = 18001  then
       b_wip_18001  := b_wip;
       b_wip_no  := b_wip;  
    elsif b_wip_code  = 18003  then
       b_wip_18003  := b_wip;
    elsif b_wip_code  = 18008  then
       b_wip_18008  := b_wip;
       b_wip_no  := b_wip;  
    elsif b_wip_code  = 18009  then
       b_wip_18009  := b_wip;
       b_wip_no  := b_wip;  
    elsif b_wip_code  = 18013  then
       b_wip_18013  := b_wip;
       b_wip_no  := b_wip;
    elsif b_wip_code  = 19011  then
       b_wip_19011  := b_wip;  
    elsif b_wip_code  = 19107  then
       b_wip_19107  := b_wip;
       b_wip_no  := b_wip;
    elsif b_wip_code  = 19108  then
       b_wip_19108  := b_wip;
       b_wip_no  := b_wip;
    elsif b_wip_code  = 19111  then
       b_wip_19111  := b_wip;
       b_wip_no  := b_wip;
    elsif b_wip_code  = 73001  then
       b_wip_73001  := b_wip;
    elsif b_wip_code  = 73002  then  -- 20170821 Change Movement WIP 73002 to Movement WIP No Account in monthly report 
       b_wip_73002  := b_wip;
       b_wip_no  := b_wip;   
    elsif b_wip_code  = 73016  then
       b_wip_73016  := b_wip;
       b_wip_no  := b_wip;
    else 
       b_wip_no  := b_wip;
    end if;
    write_log('6 ');
   --Ending WIP : DZ-EU   
   e_booked_amt := 0;
   e_wip := 0;
   e_wip_accrual := 0;
   e_prepaid := 0;
   e_prepaid_accrual := 0;
   
   IF upper(i.TYPE_OF_WORK) in ('E','U') THEN       
        e_booked_amt := nvl(GREATEST(v_EGRN,v_ECAP),0);       
     ELSIF upper(i.TYPE_OF_WORK) = 'S' THEN       
        e_booked_amt := nvl(GREATEST(v_ECAP,v_EGRN,v_EPROGRESS),0); 
     ELSIF upper(i.TYPE_OF_WORK) = 'T' THEN
        e_booked_amt := nvl(v_ECAP,0);
     ELSIF upper(i.TYPE_OF_WORK) = 'O' THEN
        e_booked_amt := 0; 
     ELSE
        e_booked_amt := 0;           
     END IF;
     
     --==  wip ==--
     IF upper(i.TYPE_OF_WORK) IN ('E','S','U') THEN
        e_wip :=  NVL(e_booked_amt,0) - NVL(v_ECAP,0);
     ELSE
        e_wip := 0;
     END IF;
     
     --==  wip accrual ==--
     IF upper(i.TYPE_OF_WORK) IN ('E','U') THEN 
      e_wip_accrual :=  NVL(v_ECAP,0) - NVL(v_EGRN,0); 
    ELSIF  upper(i.TYPE_OF_WORK) IN ('S') THEN
      e_wip_accrual :=  NVL(e_booked_amt,0) - NVL(v_EGRN,0);
    ELSE
      e_wip_accrual := 0;
    END IF;
    
    --==  prepaid ==--
     IF upper(i.TYPE_OF_WORK) = 'T' THEN
        e_prepaid:= NVL(v_EGRN,0) - NVL(v_ECAP,0);
     ELSE
        e_prepaid := 0;
     END IF;
     
     --==  prepaid accrual ==--
     IF upper(i.TYPE_OF_WORK) IN ('T') THEN
        e_PREPAID_ACCRUAL := NVL(v_ECAP,0) - NVL(v_EGRN,0);
     ELSE
        e_PREPAID_ACCRUAL := 0;
     END IF;
     
     --==================== set 0 ===============--
     if e_booked_amt < 0 then
       e_booked_amt := 0;
     end if;
     
     if e_wip < 0 then
       e_wip := 0;
     end if;
     
     if e_wip_accrual < 0 then
       e_wip_accrual := 0;
     end if;
     
     if e_prepaid < 0 then
       e_prepaid := 0;
     end if;
     
     if e_PREPAID_ACCRUAL < 0 then
       e_PREPAID_ACCRUAL := 0;
     end if;
    

   
   e_wip_code := null;
   GET_ENDING_WIP(i_po_num       => i.po_header_id --i.PO_NUM 
                     ,i_po_line_num  => i.po_line_id --i.LINE_NUM  
                     --,o_wip          => e_wip
                     ,o_wip_code     => e_wip_code) ;              
    write_log('7 ');
    

  e_wip_17004  := 0;  
  e_wip_17006  := 0; 
  e_wip_17010  := 0; 
  e_wip_17022  := 0; 
  e_wip_17024  := 0; 
  e_wip_17028  := 0; 
  e_wip_17029  := 0; 
  e_wip_17030  := 0; 
  e_wip_17031  := 0; 
  e_wip_18001  := 0; 
  e_wip_18003  := 0; 
  e_wip_18008  := 0; 
  e_wip_18009  := 0; 
  e_wip_18013  := 0; 
  e_wip_19011  := 0; 
  e_wip_19107  := 0; 
  e_wip_19108  := 0; 
  e_wip_19111  := 0; 
  e_wip_73001  := 0; 
  e_wip_73002  := 0; 
  e_wip_73016  := 0; 
  e_wip_no     := 0; 
  
   if e_wip_code  = 17004  then
       e_wip_17004  := e_wip;
       e_wip_no  := e_wip;  
    elsif e_wip_code  = 17006  then
       e_wip_17006  := e_wip;
       e_wip_no  := e_wip;  
    elsif e_wip_code  = 17010  then
       b_wip_17010  := e_wip;
       e_wip_no  := e_wip;  
    elsif e_wip_code  = 17022  then
       b_wip_17022  := e_wip;
       e_wip_no  := e_wip;
    elsif e_wip_code  = 17024  then
       e_wip_17024  := e_wip;
       e_wip_no  := e_wip;  
    elsif e_wip_code  = 17028  then
       e_wip_17028  := e_wip;
    elsif e_wip_code  = 17029  then
       e_wip_17029  := e_wip;  
    elsif e_wip_code  = 17030  then
       e_wip_17030  := e_wip;
       e_wip_no  := e_wip;  
    elsif e_wip_code  = 17031  then
       e_wip_17031  := e_wip;
       e_wip_no  := e_wip;
    elsif e_wip_code  = 18001  then
       e_wip_18001  := e_wip; 
       e_wip_no  := e_wip; 
    elsif e_wip_code  = 18003  then
       e_wip_18003  := e_wip;
    elsif e_wip_code  = 18008  then
       e_wip_18008  := e_wip;
       e_wip_no  := e_wip;  
    elsif e_wip_code  = 18009  then
       e_wip_18009  := e_wip; 
       e_wip_no  := e_wip; 
    elsif e_wip_code  = 18013  then
       e_wip_18013  := e_wip;
       e_wip_no  := e_wip;
    elsif e_wip_code  = 19011  then
       e_wip_19011  := e_wip;  
    elsif e_wip_code  = 19107  then
       e_wip_19107  := e_wip;
       e_wip_no  := e_wip;
    elsif e_wip_code  = 19108  then
       e_wip_19108  := e_wip;
       e_wip_no  := e_wip;
    elsif e_wip_code  = 19111  then
       e_wip_19111  := e_wip;
       e_wip_no  := e_wip;
    elsif e_wip_code  = 73001  then
       e_wip_73001  := e_wip;
    elsif e_wip_code  = 73002  then  -- 20170821 Change Movement WIP 73002 to Movement WIP No Account in monthly report 
       e_wip_73002  := e_wip;
       e_wip_no  := e_wip;
    elsif e_wip_code  = 73016  then
       e_wip_73016  := e_wip;
       e_wip_no  := e_wip;
    else 
       e_wip_no  := e_wip;
    end if; 
    write_log('8 ');            
   --Beginning FA AR-BI
   
   b_fa := null;
   bfa_code := null;
   

  b_fa_17004   := 0; 
  b_fa_17006   := 0;
  b_fa_17007   := 0;
  b_fa_17009   := 0;
  b_fa_17018   := 0;
  b_fa_17026   := 0;
  b_fa_17034   := 0;
  b_fa_17035   := 0;
  b_fa_17043   := 0;
  b_fa_17044   := 0;
  b_fa_17045   := 0;
  b_fa_17046   := 0;
  b_fa_17047   := 0;
  b_fa_17048   := 0;
  b_fa_17049   := 0;
  b_fa_17050   := 0;
  b_fa_18021   := 0;
  b_fa_19107   := 0;
  b_fa_19108   := 0;
  b_fa_no      := 0;
                   
    for rec_bfa in c_fa(i.PO_NUM||i.release_num
                        ,i.LINE_NUM
                        ,GET_GL_PERIOD(SYSDATE)
                        ,'B')
    loop
                     
    --b_fa := v_BCAP;
  b_fa := null;
  bfa_code := null;
  
  b_fa := rec_bfa.cost_fa;
  bfa_code := rec_bfa.ASSET_COST_ACCT;
    
 
    write_log('9 ');  
   if bfa_code = 17004 then              
     b_fa_17004 := b_fa;
   elsif bfa_code = 17006 then 
     b_fa_17006 := b_fa;
   elsif bfa_code = 17007 then 
     b_fa_17007 := b_fa;
   elsif bfa_code = 17008 then 
     b_fa_17018 := b_fa;
   elsif bfa_code = 17026 then 
     b_fa_17026 := b_fa;
   elsif bfa_code = 17034 then 
     b_fa_17034 := b_fa;
   elsif bfa_code = 17035 then 
     b_fa_17035 := b_fa;
   elsif bfa_code = 17043 then 
     b_fa_17043 := b_fa;
   elsif bfa_code = 17044  then 
     b_fa_17044 := b_fa;
   elsif bfa_code = 17045 then 
     b_fa_17045 := b_fa;
   elsif bfa_code = 17046  then 
     b_fa_17046 := b_fa;
   elsif bfa_code = 17047 then 
     b_fa_17047 := b_fa;
   elsif bfa_code = 17048 then 
     b_fa_17048 := b_fa;
   elsif bfa_code = 17049 then 
     b_fa_17049 := b_fa;
   elsif bfa_code = 17050 then 
     b_fa_17050 := b_fa;
   --elsif bfa_code = 18021 then 
   --  b_fa_18021 := b_fa;
   elsif bfa_code = 19107 then 
     b_fa_19107 := b_fa;
   elsif bfa_code = 19108 then 
     b_fa_19108:= b_fa;
   else 
     b_fa_no := nvl(b_fa,0);
   end if;                        

   -- WRITE_LOG('b_fa_17007 ' || b_fa_17007);
   --Ending FA EV - FM
   
   end loop;
   
   write_log('10 '); 
   write_log('b_fa_no '|| b_fa_no); 
    
   b_fa_no_acc   := 0;
  
   b_fa_no_acc := GET_AMT_FA_NO_ACC(i.PO_NUM ||i.release_num
                              ,i.LINE_NUM 
                              ,i_begin_end    => 'B');
                              
   b_fa_no := nvl(b_fa_no,0) + nvl(b_fa_no_acc,0);
   write_log('b_fa_no ' || b_fa_no); 
   
   ----==================================================================================================---
  e_fa_17004   := 0; 
  e_fa_17006   := 0;
  e_fa_17007   := 0;
  e_fa_17009   := 0;
  e_fa_17018   := 0;
  e_fa_17026   := 0;
  e_fa_17034   := 0;
  e_fa_17035   := 0;
  e_fa_17043   := 0;
  e_fa_17044   := 0;
  e_fa_17045   := 0;
  e_fa_17046   := 0;
  e_fa_17047   := 0;
  e_fa_17048   := 0;
  e_fa_17049   := 0;
  e_fa_17050   := 0;
  e_fa_18021   := 0;
  e_fa_19107   := 0;
  e_fa_19108   := 0;
  e_fa_no      := 0;
  
   for rec_efa in c_fa(i.PO_NUM||i.release_num
                        ,i.LINE_NUM
                        ,GET_GL_PERIOD(SYSDATE)
                        ,'E')
    loop
    
   e_fa := null;
   efa_code := null;
   
                    
   -- e_fa := v_ECAP;
    
  
  
    e_fa := rec_efa.cost_fa;
    efa_code := rec_efa.asset_cost_acct;
    
    
    write_log('11 ');    
   if efa_code = 17004 then              
     e_fa_17004 := e_fa;
   elsif efa_code = 17006 then 
     e_fa_17006 := e_fa;
   elsif efa_code = 17007 then 
     e_fa_17007 := e_fa;
   elsif efa_code = 17008 then 
     e_fa_17018 := e_fa;
   elsif efa_code = 17026 then 
     e_fa_17026 := e_fa;
   elsif efa_code = 17034 then 
     e_fa_17034 := e_fa;
   elsif efa_code = 17035 then 
     e_fa_17035 := e_fa;
   elsif efa_code = 17043 then 
     e_fa_17043 := e_fa;
   elsif efa_code = 17044  then 
     e_fa_17044 := e_fa;
   elsif efa_code = 17045 then 
     e_fa_17045 := e_fa;
   elsif efa_code = 17046  then 
     e_fa_17046 := e_fa;
   elsif efa_code = 17047 then 
     e_fa_17047 := e_fa;
   elsif efa_code = 17048 then 
     e_fa_17048 := e_fa;
   elsif efa_code = 17049 then 
     e_fa_17049 := e_fa;
   elsif efa_code = 17050 then 
     e_fa_17050 := e_fa;
   --elsif efa_code = 18021 then 
   --  e_fa_18021 := e_fa;
   elsif efa_code = 19107 then 
     e_fa_19107 := e_fa;
   elsif efa_code = 19108 then 
     e_fa_19108:= e_fa;
   else 
     e_fa_no := nvl(e_fa,0);
   end if;
   
   write_log('e_fa '|| e_fa);
   write_log('e_fa_17007 '|| e_fa_17007);
   write_log('e_fa_17007 '|| e_fa_17007);
   
   
 END LOOP;
 
 
    e_fa_no_acc   := 0;
  
    e_fa_no_acc := GET_AMT_FA_NO_ACC(i.PO_NUM ||i.release_num
                              ,i.LINE_NUM 
                              ,i_begin_end    => 'E');
                              
                              
   e_fa_no := nvl(e_fa_no,0) + nvl(e_fa_no_acc,0);
 
 --==================================================================================--
   
   
    write_log('12 ');                                 
   --Beginning Accum BJ - BY 
   GET_BEGINNING_ACCUM_DEPRE(--i_asset_id   =>   i.asset_id   
                             --,i_book       =>   P_BOOK  
                              i_po_num       => i.PO_NUM||i.release_num 
                             ,i_po_line_num  => i.LINE_NUM
                             ,i_begin_end  =>   'B'
                             ,o_accum_dp   =>   b_accum_dp       
                             --,o_accum_code =>   b_accum_code 
                             );
    /* 
    write_log('13 ');
   if b_accum_code = 17106  then
       b_accum_dp17106  := b_accum_dp;  
    elsif b_accum_code = 17107 then 
       b_accum_dp17107  := b_accum_dp;  
    elsif b_accum_code = 17118 then
    b_accum_dp17118  := b_accum_dp;     
       elsif b_accum_code = 17126 then
    b_accum_dp17126  := b_accum_dp;  
      elsif b_accum_code = 17134 then
    b_accum_dp17134  := b_accum_dp;  
      elsif b_accum_code = 17135 then
    b_accum_dp17135  := b_accum_dp;  
      elsif b_accum_code = 17143 then
    b_accum_dp17143  := b_accum_dp;  
      elsif b_accum_code = 17144 then
    b_accum_dp17144  := b_accum_dp;  
      elsif b_accum_code = 17145 then
    b_accum_dp17145  := b_accum_dp;  
      elsif b_accum_code = 17146 then
    b_accum_dp17146  := b_accum_dp;  
      elsif b_accum_code = 17147 then
    b_accum_dp17147  := b_accum_dp;  
      elsif b_accum_code = 17148 then
    b_accum_dp17148  := b_accum_dp;  
      elsif b_accum_code = 17149 then
    b_accum_dp17149 := b_accum_dp;  
      elsif b_accum_code = 18002 then
    b_accum_dp18002  := b_accum_dp;  
      elsif b_accum_code = 19114 then
    b_accum_dp19114  := b_accum_dp;  
      elsif b_accum_code = 19115 then
    b_accum_dp19115  := b_accum_dp;  
     else null;  
    end if;*/
    write_log('14 ');
    --Ending Accum FN - GC
    
    GET_BEGINNING_ACCUM_DEPRE( --i_asset_id   =>   i.asset_id   
                             --,i_book       =>   P_BOOK  
                             i_po_num       => i.PO_NUM ||i.release_num 
                             ,i_po_line_num  => i.LINE_NUM
                             ,i_begin_end  =>  'E'
                             ,o_accum_dp   =>   e_accum_dp       
                             --,o_accum_code =>   e_accum_code 
                             );
                             
    /*
    write_log('15 ');
    if e_accum_code = 17106  then
       e_accum_dp17106  := e_accum_dp;  
    elsif e_accum_code = 17107 then 
       e_accum_dp17107  := e_accum_dp;  
    elsif e_accum_code = 17118 then
      e_accum_dp17118  := e_accum_dp;     
    elsif b_accum_code = 17126 then
       e_accum_dp17126  := e_accum_dp;  
    elsif b_accum_code = 17134 then
      e_accum_dp17134  := e_accum_dp;  
    elsif e_accum_code = 17135 then
      e_accum_dp17135  := e_accum_dp;  
    elsif e_accum_code = 17143 then
      e_accum_dp17143  := e_accum_dp;  
      elsif b_accum_code = 17144 then
    e_accum_dp17144  := e_accum_dp;  
      elsif b_accum_code = 17145 then
    e_accum_dp17145  := e_accum_dp;  
      elsif b_accum_code = 17146 then
    e_accum_dp17146  := e_accum_dp;  
      elsif b_accum_code = 17147 then
    e_accum_dp17147  := e_accum_dp;  
      elsif b_accum_code = 17148 then
    e_accum_dp17148  := e_accum_dp;  
      elsif b_accum_code = 17149 then
    e_accum_dp17149 := e_accum_dp;  
      elsif b_accum_code = 18002 then
    e_accum_dp18002  := e_accum_dp;  
      elsif b_accum_code = 19114 then
    e_accum_dp19114  := e_accum_dp;  
      elsif b_accum_code = 19115 then
    e_accum_dp19115  := e_accum_dp;  
     else null;  
    end if;
    */
    
    write_log('16 ');
    /*GET_DEPRE( i_asset_id   => i.asset_id   
              ,i_book       => P_BOOK 
              ,o_dp         => v_dp
              ,o_dp_code    => v_dp_code 
              ) ;*/
    write_log('17 ');
    
     v_dp := null;
     v_dp_code := null;
     
     v_dp_50301  := 0; 
     v_dp_55706  := 0; 
    v_dp_55707  := 0; 
    v_dp_55709  := 0; 
    v_dp_55726  := 0; 
    v_dp_55734  := 0; 
    v_dp_55735  := 0; 
    v_dp_55803  := 0; 
    v_dp_55804  := 0; 
    v_dp_no     := 0; 
    
   for deprn in c_deprn (i.PO_NUM ||i.release_num 
                         ,i.LINE_NUM 
                         ,GET_PERIOD_COUNTER ( '' )
                         )
   loop 
   

      v_dp := deprn.o_dp;
      v_dp_code := deprn.o_dp_code;
      
  
      
    if v_dp_code = '50301' then
       v_dp_50301  :=  v_dp;
    elsif v_dp_code = '55706' then
       v_dp_55706  :=  v_dp;
    elsif v_dp_code = '55707' then
       v_dp_55707  :=  v_dp;
    elsif v_dp_code = '55709' then
       v_dp_55709  :=  v_dp;
    elsif v_dp_code = '55726' then
       v_dp_55726  :=  v_dp;
    elsif v_dp_code = '55734' then
       v_dp_55734  :=  v_dp;
    elsif v_dp_code = '55735' then
       v_dp_55735  :=  v_dp;   
    elsif v_dp_code = '55803' then
       v_dp_55803  :=  v_dp;
    elsif v_dp_code = '55804' then
       v_dp_55804  :=  v_dp; 
    else  v_dp_no  :=  v_dp; 
    end if;
    
    END LOOP;
    
    v_amt_cloud := TAC_FA_REPORT_PKG.GET_REGIST_AMT_CLOUD(i.PO_NUM ||i.release_num ,i.LINE_NUM);
    
    v_MGRN :=0;
    v_MPROGRESS :=0;
    v_MCAP :=0;
    m_accum_dp :=0;
    m_booked_amt :=0;
    m_prepaid :=0;
    m_prepaid_accrual :=0;
    m_wip_accrual :=0;
    m_wip_17028 :=0;
    m_wip_17029 :=0;
    m_wip_18003 :=0;
    m_wip_19011 :=0;
    m_wip_73001 :=0;
    m_wip_73002 :=0;
    
    m_fa_17004 :=0;
    m_fa_17006 :=0;
    m_fa_17007 :=0;
    m_fa_17009 :=0;
    m_fa_17018 :=0;
    m_fa_17026 :=0;    
    m_fa_17034 :=0;
    m_fa_17035 :=0;
    m_fa_17043 :=0;
    m_fa_17044 :=0;
    m_fa_17045 :=0;
    m_fa_17046 :=0;
    m_fa_17047 :=0;
    m_fa_17048 :=0;
    m_fa_17049 :=0;
    m_fa_17050 :=0;
    m_fa_19107 :=0;
    m_fa_19108 :=0;
    m_fa_no :=0;
    
    
    --================  Movement =============================---
   /*  if v_EGRN = 0 then
        v_MGRN := 0;
     else
        v_MGRN := (v_EGRN - v_BGRN);
     end if;
     -----------------------------------------
     if v_EPROGRESS = 0 then
        v_MPROGRESS := 0;
     else
        v_MPROGRESS := (v_EPROGRESS - v_BPROGRESS);
     end if;
     -----------------------------------------
     if v_ECAP = 0 then
        v_MCAP := 0;
     else
        v_MCAP := (v_ECAP - v_BCAP);
     end if;
     -----------------------------------------
     if e_accum_dp = 0 then
        m_accum_dp := 0;
     else
        m_accum_dp := (e_accum_dp - b_accum_dp);
     end if;
     -----------------------------------------
     if e_booked_amt = 0 then
        m_booked_amt := 0;
     else
        m_booked_amt := (e_booked_amt - b_booked_amt);
     end if;
     -----------------------------------------
     if e_prepaid = 0 then
        m_prepaid := 0;
     else
        m_prepaid := (e_prepaid - b_prepaid);
     end if;
     -----------------------------------------
     if e_prepaid_accrual = 0 then
        m_prepaid_accrual := 0;
     else
        m_prepaid_accrual := (e_prepaid_accrual - b_prepaid_accrual);
     end if;
     -----------------------------------------
     if e_wip_accrual = 0 then
        m_wip_accrual := 0;
     else
        m_wip_accrual := (e_wip_accrual - b_wip_accrual);
     end if;
     -----------------------------------------
     if e_wip_17028 = 0 then
        m_wip_17028 := 0;
     else
        m_wip_17028 := (e_wip_17028 - b_wip_17028);
     end if;
     -----------------------------------------
     if e_wip_17029 = 0 then
        m_wip_17029 := 0;
     else
        m_wip_17029 := (e_wip_17029 - b_wip_17029);
     end if;
     -----------------------------------------
     if e_wip_18003 = 0 then
        m_wip_18003 := 0;
     else
        m_wip_18003 := (e_wip_18003 - b_wip_18003);
     end if;
     -----------------------------------------
     if e_wip_19011 = 0 then
        m_wip_19011 := 0;
     else
        m_wip_19011 := (e_wip_19011 - b_wip_19011);
     end if;
     -----------------------------------------
     if e_wip_73001 = 0 then
        m_wip_73001 := 0;
     else
        m_wip_73001 := (e_wip_73001 - b_wip_73001);
     end if;
     -----------------------------------------
     if e_wip_73002 = 0 then
        m_wip_73002 := 0;
     else
        m_wip_73002 := (e_wip_73002 - b_wip_73002) ;
     end if;
     -----------------------------------------
     if e_fa_17004 = 0 then
        m_fa_17004 := 0;
     else
        m_fa_17004 := (e_fa_17004 - b_fa_17004);
     end if;
     -----------------------------------------
     if e_fa_17006 = 0 then
        m_fa_17006 := 0;
     else
        m_fa_17006 := (e_fa_17006 - b_fa_17006);
     end if;
     -----------------------------------------
     if e_fa_17007 = 0 then
        m_fa_17007 := 0;
     else
        m_fa_17007 := (e_fa_17007 - b_fa_17007);
     end if;
     -----------------------------------------
     if e_fa_17009 = 0 then
        m_fa_17009 := 0;
     else
        m_fa_17009 := (e_fa_17009 - b_fa_17009);
     end if;
     -----------------------------------------
     if e_fa_17018 = 0 then
        m_fa_17018 := 0;
     else
        m_fa_17018 := (e_fa_17018 - b_fa_17018);
     end if;
     -----------------------------------------
     if e_fa_17026 = 0 then
        m_fa_17026 := 0;
     else
        m_fa_17026 := (e_fa_17026 - b_fa_17026);
     end if;
     -----------------------------------------
     if e_fa_17034 = 0 then
        m_fa_17034 := 0;
     else
        m_fa_17034 := (e_fa_17034 - b_fa_17034);
     end if;
     -----------------------------------------
     if e_fa_17035 = 0 then
        m_fa_17035 := 0;
     else
        m_fa_17035 := (e_fa_17035 - b_fa_17035);
     end if;
     -----------------------------------------
     if e_fa_17043 = 0 then
        m_fa_17043 := 0;
     else
        m_fa_17043 := (e_fa_17043 - b_fa_17043);
     end if;
     -----------------------------------------
     if e_fa_17044 = 0 then
        m_fa_17044 := 0;
     else
        m_fa_17044 := (e_fa_17044 - b_fa_17044);
     end if;
     -----------------------------------------
     if e_fa_17045 = 0 then
        m_fa_17045 := 0;
     else
        m_fa_17045 := (e_fa_17045 - b_fa_17045);
     end if;
     -----------------------------------------
     if e_fa_17046 = 0 then
        m_fa_17046 := 0;
     else
        m_fa_17046 := (e_fa_17046 - b_fa_17046);
     end if;
     -----------------------------------------
     if e_fa_17047 = 0 then
        m_fa_17047 := 0;
     else
        m_fa_17047 := (e_fa_17047 - b_fa_17047);
     end if;
     -----------------------------------------
     if e_fa_17048 = 0 then
        m_fa_17048 := 0;
     else
        m_fa_17048 := (e_fa_17048 - b_fa_17048);
     end if;
     -----------------------------------------
     if e_fa_17049 = 0 then
        m_fa_17049 := 0;
     else
        m_fa_17049 := (e_fa_17049 - b_fa_17049);
     end if;
     -----------------------------------------
     if e_fa_17050 = 0 then
        m_fa_17050 := 0;
     else
        m_fa_17050 := (e_fa_17050 - b_fa_17050);
     end if;
     -----------------------------------------
     if e_fa_19107 = 0 then
        m_fa_19107 := 0;
     else
        m_fa_19107 := (e_fa_19107  - b_fa_19107);
     end if;
     -----------------------------------------
     if e_fa_19108 = 0 then
        m_fa_19108 := 0;
     else
        m_fa_19108 := (e_fa_19108  - b_fa_19108);
     end if;
     -----------------------------------------
     if e_fa_no = 0 then
        m_fa_no := 0;
     else
        m_fa_no := (e_fa_no  - b_fa_no);
     end if;
     -----------------------------------------
     
     */


  
    
    write_log('18 ');
   fnd_file.put_line(fnd_file.output,
         --A-L
         i.COMPANY||'|'||i.PRO_GROUP||'|'||i.DIVISION||'|'||i.PROJECT_CODE||'|'||i.PROJECT_NAME||'|'||i.PO_NUM||i.release_num||'|'||i.LINE_NUM
         ||'|'||i.ITEM_DESC||'|'||i.TYPE_OF_WORK||'|'||i.PO_STATUS||'|'||i.VENDOR_NAME||'|'||V_STRIM_CODE 
         --M,N,O
         ||'|'||v_BGRN||'|'||v_BPROGRESS||'|'||v_BCAP
        -- ||'|'||'M'||'|'||'N'||'|'||'O'
         --P,Q,R,S
        ||'|'||b_accum_dp||'|'||b_booked_amt||'|'||b_prepaid||'|'||b_prepaid_accrual
        -- ||'|'||'P'||'|'||'Q'||'|'||'R'||'|'||'S'
          --T,U
         --||'|'||i.BWIP_ACC||'|'||i.BWIP_ACCRUAL   
         ||'|'||b_wip_code||'|'||B_WIP_ACCRUAL 
         --Beginning WIP : V-AQ
         ||'|'||b_wip_17004||'|'||b_wip_17006||'|'||b_wip_17010||'|'||b_wip_17022||'|'||b_wip_17024||'|'||b_wip_17028||'|'||b_wip_17029
         ||'|'||b_wip_17030||'|'||b_wip_17031||'|'||b_wip_18001||'|'||b_wip_18003||'|'||b_wip_18008||'|'||b_wip_18009||'|'||b_wip_18013
         ||'|'||b_wip_19011||'|'||b_wip_19107||'|'||b_wip_19108||'|'||b_wip_19111||'|'||b_wip_73001||'|'||b_wip_73002||'|'||b_wip_73016
         ||'|'||b_wip_no 
         --Beginning FA : AR-BI  
         ||'|'||b_fa_17004||'|'||b_fa_17006||'|'||b_fa_17007||'|'||b_fa_17009||'|'||b_fa_17018||'|'||b_fa_17026
         ||'|'||b_fa_17034||'|'||b_fa_17035||'|'||b_fa_17043||'|'||b_fa_17044||'|'||b_fa_17045||'|'||b_fa_17046
         ||'|'||b_fa_17047||'|'||b_fa_17048||'|'||b_fa_17049||'|'||b_fa_17050||'|'||b_fa_19107||'|'||b_fa_19108||'|'||b_fa_no       
         --Beginning Accum Depre : BJ-BY        
         /*
         ||'|'||b_accum_dp17106||'|'||b_accum_dp17107||'|'||b_accum_dp17118||'|'||b_accum_dp17126||'|'||b_accum_dp17134
         ||'|'||b_accum_dp17135||'|'||b_accum_dp17143||'|'||b_accum_dp17144||'|'||b_accum_dp17145||'|'||b_accum_dp17146
         ||'|'||b_accum_dp17147||'|'||b_accum_dp17148||'|'||b_accum_dp17149||'|'||b_accum_dp18002||'|'||b_accum_dp19114
         ||'|'||b_accum_dp19115  
         */         
         --Movement GRN BZ:Column M - DQ
         ||'|'||(v_EGRN - v_BGRN)
         --||'|'||v_mGRN
         --Movemen Progress of Service CA:Column N - DR
         ||'|'||(v_EPROGRESS - v_BPROGRESS)
         --||'|'||v_MPROGRESS
         --Movement Capitalize CB:Column O - DS
         ||'|'||(v_ECAP - v_BCAP)
         --||'|'||v_MCAP
         --Movement Accum Depre CC:Column P - DT
         ||'|'||(e_accum_dp - b_accum_dp) 
         --||'|'||m_accum_dp              
         --Movement CAPEX Booking CD:Column Q - DU
         ||'|'||(e_booked_amt - b_booked_amt)
         --||'|'||m_booked_amt
         --Movement Prepaid CE:R - DV
         ||'|'||(e_prepaid - b_prepaid)
         --||'|'||m_prepaid
         --Movement Prepaid Accrual CF:Column S - DW
         ||'|'||(e_prepaid_accrual - b_prepaid_accrual)
         --||'|'||m_prepaid_accrual
         --Movement WIP Accrual CG:Column T - DZ         
         ||'|'||(e_wip_accrual - b_wip_accrual)
         --||'|'||m_wip_accrual
         --Movement WIP CH-CM
         ||'|'||(e_wip_17028 - b_wip_17028)
         --||'|'||m_wip_17028
         --Column EG - AC
         ||'|'||(e_wip_17029 - b_wip_17029)
         --||'|'||m_wip_17029
         --Column EK - AG
         ||'|'||(e_wip_18003 - b_wip_18003)
         --||'|'||m_wip_18003
         --Column EO - AK
         ||'|'||(e_wip_19011 - b_wip_19011)
         --||'|'||m_wip_19011
         --Column ES - AO
         ||'|'||(e_wip_73001 - b_wip_73001)
         --||'|'||m_wip_73001
         --Column ET - AP 
         ||'|'||(e_wip_no - b_wip_no)
         --||'|'||(e_wip_73002 - b_wip_73002)  -- 20170821 Change Movement WIP 73002 to Movement WIP No Account in monthly report 
         --||'|'||m_wip_73002
         --Movement FA:CN-CZ
         --Column EX - AT
         ||'|'||(e_fa_17004 - b_fa_17004)
         --||'|'||m_fa_17004
         ||'|'||(e_fa_17006 - b_fa_17006)
         --||'|'||m_fa_17006
         --Column EY - AU
         ||'|'||(e_fa_17007 - b_fa_17007)
         --||'|'||m_fa_17007
         --Column FB - AX
         ||'|'||(e_fa_17009  - b_fa_17009)
         ||'|'||(e_fa_17018  - b_fa_17018)
         ||'|'||(e_fa_17026  - b_fa_17026)
         --||'|'||m_fa_17009
         --||'|'||m_fa_17018
         --||'|'||m_fa_17026
         --Column FC - AY
         ||'|'||(e_fa_17034  - b_fa_17034)
         --||'|'||m_fa_17034
         --Column FD - AZ
         ||'|'||(e_fa_17035  - b_fa_17035)
         --||'|'||m_fa_17035
         --Column FE - BA
         ||'|'||(e_fa_17043  - b_fa_17043)
         --||'|'||m_fa_17043
         --Column FF - BB
         ||'|'||(e_fa_17044  - b_fa_17044)
         --||'|'||m_fa_17044
         --Column FG - BC
         ||'|'||(e_fa_17045  - b_fa_17045)
         --||'|'||m_fa_17045
         --Column FH - BD
         ||'|'||(e_fa_17046  - b_fa_17046)
         --||'|'||m_fa_17046
         --Column FI - BE
         ||'|'||(e_fa_17047  - b_fa_17047)
         --||'|'||m_fa_17047
         --Column FJ - BF
         ||'|'||(e_fa_17048  - b_fa_17048)
         ||'|'||(e_fa_17049  - b_fa_17049)
         ||'|'||(e_fa_17050  - b_fa_17050)
         --||'|'||m_fa_17048
         --||'|'||m_fa_17049
         --||'|'||m_fa_17050
         --Column FL - BH
         ||'|'||(e_fa_19107  - b_fa_19107)  
         --||'|'||m_fa_19107       
         --Column FM - BJ    
         ||'|'||(e_fa_19108  - b_fa_19108) 
         ||'|'||(e_fa_no  - b_fa_no)
         --||'|'||m_fa_19108
         --||'|'||m_fa_no
         --Movement Accum Depre:DA-DP
         -------------------------------------------------------------
         /*
         --Column FN - BJ
         ||'|'||(e_accum_dp17106 - b_accum_dp17106)
         --Column FO - BK
         ||'|'||(e_accum_dp17107 - b_accum_dp17107)
         --Column FP - BL
         ||'|'||(e_accum_dp17118 - b_accum_dp17118) 
         --Column FQ - BM
         ||'|'||(e_accum_dp17126 - b_accum_dp17126)
         --Column FR - BN
         ||'|'||(e_accum_dp17134 - b_accum_dp17134)
         --Column FS - BO
         ||'|'||(e_accum_dp17135 - b_accum_dp17135)
         --Column FT - BP
         ||'|'||(e_accum_dp17143 - b_accum_dp17143)
         --Column FU - BQ
         ||'|'||(e_accum_dp17144 - b_accum_dp17144)
         --Column FV - BR
         ||'|'||(e_accum_dp17145 - b_accum_dp17145)
         --Column FW - BS
         ||'|'||(e_accum_dp17146 - b_accum_dp17146)
         --Column FX - BT
         ||'|'||(e_accum_dp17147 - b_accum_dp17147)
         --Column FY - BU
         ||'|'||(e_accum_dp17148 - b_accum_dp17148)
         --Column FZ - BV
         ||'|'||(e_accum_dp17149 - b_accum_dp17149)
         --Column GA - BW
         ||'|'||(e_accum_dp18002 - b_accum_dp18002)
         --Column GB - BX
         ||'|'||(e_accum_dp19114 - b_accum_dp19114)
         --Column GC - BY
         ||'|'||(e_accum_dp19115 - b_accum_dp19115) 
         */    
         --DQ,DR,DS
         ||'|'||v_EGRN||'|'||v_EPROGRESS||'|'||v_ECAP
         --DT
         ||'|'||e_accum_dp
         --DU,DV,DW
         ||'|'||e_booked_amt||'|'||e_prepaid||'|'||e_prepaid_accrual
         --||'|'||v_ETYPE_OF_WORK||'|'||v_ETYPE_OF_WORK||'|'||v_ETYPE_OF_WORK   
         --||'|'||'DU'||'|'||'DV'||'|'||'DW'    
         --DX-DY
         --||'|'||i.BWIP_ACC||'|'|| e_wip --i.BWIP_ACCRUAL 
         ||'|'||e_wip_code||'|'|| e_wip_accrual         
         --Ending WIP : DZ-EU
         ||'|'||e_wip_17004||'|'||e_wip_17006||'|'||e_wip_17010||'|'||e_wip_17022||'|'||e_wip_17024||'|'||e_wip_17028||'|'||e_wip_17029
         ||'|'||e_wip_17030||'|'||e_wip_17031||'|'||e_wip_18001||'|'||e_wip_18003||'|'||e_wip_18008||'|'||e_wip_18009||'|'||e_wip_18013
         ||'|'||e_wip_19011||'|'||e_wip_19107||'|'||e_wip_19108||'|'||e_wip_19111||'|'||e_wip_73001||'|'||e_wip_73002||'|'||e_wip_73016
         ||'|'||e_wip_no 
         --Ending FA : EV-FM  
        ||'|'||e_fa_17004||'|'||e_fa_17006||'|'||e_fa_17007||'|'||e_fa_17009||'|'||e_fa_17018||'|'||e_fa_17026
         ||'|'||e_fa_17034||'|'||e_fa_17035||'|'||e_fa_17043||'|'||e_fa_17044||'|'||e_fa_17045||'|'||e_fa_17046
         ||'|'||e_fa_17047||'|'||e_fa_17048||'|'||e_fa_17049||'|'||e_fa_17050||'|'||e_fa_19107||'|'||e_fa_19108||'|'||e_fa_no     
           
         --Ending Accum Depre : FN-GC
         /*
         ||'|'||e_accum_dp17106||'|'||e_accum_dp17107||'|'||e_accum_dp17118||'|'||e_accum_dp17126||'|'||e_accum_dp17134
         ||'|'||e_accum_dp17135||'|'||e_accum_dp17143||'|'||e_accum_dp17144||'|'||e_accum_dp17145||'|'||e_accum_dp17146
         ||'|'||e_accum_dp17147||'|'||e_accum_dp17148||'|'||e_accum_dp17149||'|'||e_accum_dp18002||'|'||e_accum_dp19114
         ||'|'||e_accum_dp19115  
         */
         --Depre GD-GM
         ||'|'||v_dp_50301||'|'||v_dp_55706||'|'||v_dp_55707||'|'||v_dp_55709||'|'||v_dp_55726||'|'||v_dp_55734||'|'||v_dp_55735
         ||'|'||v_dp_55803||'|'||v_dp_55804||'|'||v_dp_no           
         --GN
         --||'|'||(i.BWIP_ACCRUAL - v_EGRN - i.COST) 
         --||'|'||(B_WIP_ACCRUAL - v_EGRN - i.COST ) 
         ||'|'|| v_amt_cloud
                    );
    write_log('19 ');
  END LOOP loop_data;
    write_log('20 ');
  write_proc(v_module, 'END'); 

EXCEPTION
  WHEN fnd_api.g_exc_error THEN
      write_log('21 ');
    ROLLBACK;
    o_errbuf  :=  SUBSTR(v_errmsg,1,2000);
    write_error(v_module,o_errbuf);
   -- o_retcode :=  g_FailureCompletionCode;
  WHEN OTHERS THEN
      write_log('22 ');
    ROLLBACK;
    o_errbuf  :=  SUBSTR(SQLERRM||' @ '||g_step,1,2000);
    write_unxp(v_module,o_errbuf);
    --o_retcode :=  g_FailureCompletionCode;   
    
END GENERTAE_REPORT;  
--**********************************************************

END TAC_ASSET_MONTHLY_REPORT ;
/