CREATE OR REPLACE PACKAGE APPS.TAC_ASSET_MONTHLY_REPORT AUTHID CURRENT_USER AS
--**********************************************************
-- FILE NAME: TAC_ASSET_MONTHLY.pls
-- DESCRIPTION: TAC Asset Monthly Report
-- AUTHOR     : RAPEEPORN  FONGPETCH
-- DATE       : 29-MAR-2017
--Initial Revision.
--
--**********************************************************

PROCEDURE GENERTAE_REPORT(o_errbuf        OUT VARCHAR2
                         ,o_retcode       OUT NUMBER
                         ,p_org_id        IN  NUMBER 
                         ,p_project       IN  NUMBER
                         ,p_book          IN  VARCHAR2 
                         ,P_GROUP_OU      IN  VARCHAR2
                         );
                        
END TAC_ASSET_MONTHLY_REPORT;
/