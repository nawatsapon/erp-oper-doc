create or replace PACKAGE      TAC_WIP_ACC_REPORT_PKG is
FUNCTION check_wip_accrued_report(p_project_id IN NUMBER,
                                  p_po_header_id IN NUMBER,
                                  p_po_line_id IN NUMBER,
                                  p_rate number  ) RETURN varchar2; 
---
FUNCTION CHECK_POADD_RP(P_PO_NUMBER IN VARCHAR2,
                        P_PO_LINE IN NUMBER,
                        P_PERIOD IN VARCHAR2
                         ) RETURN varchar2; 
FUNCTION CHECK_DISPLAY_PROJECT(P_PO_NUMBER IN VARCHAR2,
                        P_PO_LINE IN VARCHAR2,
                        P_PERIOD IN VARCHAR2
                         ) RETURN varchar2;    
FUNCTION CHECK_FULL_RECEIVE(P_PO_HEADER_ID IN NUMBER,
                        P_PO_LINE_ID IN NUMBER
                         ) RETURN varchar2;
FUNCTION GET_LAST_RECEIVE_DATE(P_PO_HEADER_ID NUMBER,
                               P_PO_LINE_ID NUMBER) RETURN DATE;  
FUNCTION GET_TYPE_OF_WORK_LASTPERIOD(P_REQ_DIST_ID NUMBER,
                          P_PO_NUMBER VARCHAR2,
                          P_PO_LINE NUMBER) RETURN VARCHAR2;
procedure gen_wip_aging (errbuf             OUT VARCHAR2,
                         retcode            OUT NUMBER,
                         PIN_REQUEST_ID     IN NUMBER,
                         P_ORG_ID           IN NUMBER,
                         P_PROJECT_NO       IN VARCHAR2,
                         P_PO_NO_ID         IN NUMBER,
                         P_PO_LINE          IN NUMBER,
                         PIN_PERIOD         IN VARCHAR2,
                         P_GROUP_OU         IN VARCHAR2);  
                    
procedure gen_accrued_aging (errbuf             OUT VARCHAR2,
                            retcode             OUT NUMBER,
                            PIN_REQUEST_ID      IN NUMBER,
                            P_ORG_ID            IN NUMBER,
                            P_PO_NO_ID          IN NUMBER,
                            P_PO_LINE           IN NUMBER,
                            P_PROJECT_NO        IN VARCHAR2,
                            PIN_PERIOD          IN VARCHAR2,
                            P_GROUP_OU          IN VARCHAR2);                   

                                                                                  
                                                     
  
end TAC_WIP_ACC_REPORT_PKG;
