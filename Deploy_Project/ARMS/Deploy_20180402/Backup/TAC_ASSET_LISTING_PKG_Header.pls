create or replace PACKAGE      TAC_ASSET_LISTING_PKG is

 
procedure gen_report (errbuf               OUT VARCHAR2,
                         retcode              OUT NUMBER,
                         P_BOOK IN VARCHAR2,
                         P_PERIOD_FR IN VARCHAR2,
                         P_PERIOD_TO IN VARCHAR2,
                         P_PROJECT IN VARCHAR2,
                         P_PO IN VARCHAR2 ,
                         P_LOCATION IN VARCHAR2,
                         P_CATEGORY IN VARCHAR2,                        
                         P_OWNER IN NUMBER

                         );  
                    

end TAC_ASSET_LISTING_PKG;
