create or replace PACKAGE BODY      TAC_ASSET_LISTING_PKG is

PROCEDURE WRITE_LOG (pCHAR IN VARCHAR2, pTYPE IN VARCHAR2)
   IS
   BEGIN
      IF UPPER (pTYPE) = 'LOG'
      THEN
         fnd_file.put_LINE (FND_FILE.LOG, pCHAR);
      ELSE
         fnd_file.put_line (FND_FILE.OUTPUT, pCHAR);
      END IF;

      DBMS_OUTPUT.put_line (Pchar);
   exception when others then
       DBMS_OUTPUT.put_line (sqlerrm);
   END WRITE_LOG;
   
procedure CF_ADJUST(P_PERIOD IN date,--VARCHAR2,
                         P_ASSET_ID IN NUMBER,
                         O_USEFUL_LIFE OUT NUMBER,
                         O_DATE_STRAT_LIFE OUT VARCHAR2,
                         O_DATE_END_LIFE OUT VARCHAR2
                         )   IS

V_TRANSACTION_HEADER_ID NUMBER;  
V_PERIOD VARCHAR2(20);                       
BEGIN

	V_TRANSACTION_HEADER_ID := NULL;
	V_PERIOD := P_PERIOD;
	
	IF V_PERIOD IS NULL THEN 
		V_PERIOD := TO_CHAR(SYSDATE,'MON-YY');
	END IF;
  
  BEGIN
   /*SELECT MAX(ft.TRANSACTION_HEADER_ID)
   INTO V_TRANSACTION_HEADER_ID
   FROM --FA_TRANSACTION_HEADERS FT
   FA_TRANSACTION_HISTORY_TRX_V FT
   ,FA_BOOKS B
   WHERE B.ASSET_ID = FT.ASSET_ID
   AND B.TRANSACTION_HEADER_ID_IN= ft.TRANSACTION_HEADER_ID
   AND B.ASSET_ID = P_ASSET_ID
   --AND LAST_DAY(ft.TRANSACTION_DATE_ENTERED) <= last_day(to_date(V_PERIOD,'MON-YY'))
   AND LAST_DAY(TO_DATE(ft.PERIOD_ENTERED,'MON-YY')) <= last_day(to_date(V_PERIOD,'MON-YY'))
   ;*/
   
   
   /*SELECT MAX(ft.TRANSACTION_HEADER_ID)
   INTO V_TRANSACTION_HEADER_ID
   FROM FA_TRANSACTION_HEADERS FT
   WHERE FT.ASSET_ID = P_ASSET_ID
   AND ft.DATE_EFFECTIVE <= P_PERIOD
   ;*/
   
    --ktp 20171010 fixed issue no.18
   SELECT MAX(ft.TRANSACTION_HEADER_ID)
   INTO V_TRANSACTION_HEADER_ID
   FROM FA_TRANSACTION_HEADERS FT
   ,FA_BOOKS B
   WHERE B.ASSET_ID = FT.ASSET_ID
   AND B.TRANSACTION_HEADER_ID_IN= ft.TRANSACTION_HEADER_ID
   AND B.ASSET_ID = P_ASSET_ID
   AND ft.DATE_EFFECTIVE <= P_PERIOD
   ;

  EXCEPTION WHEN OTHERS THEN
    V_TRANSACTION_HEADER_ID := NULL;
  END;
  
  IF V_TRANSACTION_HEADER_ID IS NULL THEN
  	     BEGIN
         /*SELECT MAX(ft.TRANSACTION_HEADER_ID)
         INTO V_TRANSACTION_HEADER_ID
         FROM --FA_TRANSACTION_HEADERS FT
         FA_TRANSACTION_HISTORY_TRX_V FT
         ,FA_BOOKS B
         WHERE B.ASSET_ID = FT.ASSET_ID
        AND B.TRANSACTION_HEADER_ID_IN= ft.TRANSACTION_HEADER_ID
        AND B.ASSET_ID = P_ASSET_ID
        ;*/
        
         /*SELECT MAX(ft.TRANSACTION_HEADER_ID)
         INTO V_TRANSACTION_HEADER_ID
         FROM FA_TRANSACTION_HEADERS FT
         WHERE FT.ASSET_ID = P_ASSET_ID
        ;*/
         --ktp 20171010 fixed issue no.18
        SELECT MAX(ft.TRANSACTION_HEADER_ID)
         INTO V_TRANSACTION_HEADER_ID
         FROM FA_TRANSACTION_HEADERS FT
         ,FA_BOOKS B
         WHERE B.ASSET_ID = FT.ASSET_ID
        AND B.TRANSACTION_HEADER_ID_IN= ft.TRANSACTION_HEADER_ID
        AND B.ASSET_ID = P_ASSET_ID
        ;

        EXCEPTION WHEN OTHERS THEN
          V_TRANSACTION_HEADER_ID := NULL;
        END;
  	
  END IF;
  
	
  
  	O_USEFUL_LIFE := NULL;
    O_DATE_STRAT_LIFE := NULL;
    O_DATE_END_LIFE := NULL;

    
 BEGIN 
  select fnd_number.canonical_to_number(NVL(TRUNC(BKS.life_in_months /12),0)|| '.'||NVL(MOD(BKS.life_in_months,12),0))   USE_LIFT_LAST   --AI
   --ktp 20171010 fixed issue no.19
  ,to_char(BKS.DATE_PLACED_IN_SERVICE,'DD-MON-YYYY')  START_DATE_LAST     --AJ 
  ,TO_CHAR(ADD_MONTHS (BKS.DATE_PLACED_IN_SERVICE  ,BKS.life_in_months  ) -1 ,'DD-MON-YYYY' )   END_DATE_LAST   --AK
  --,BKS.DATE_PLACED_IN_SERVICE
  --,ADD_MONTHS (BKS.DATE_PLACED_IN_SERVICE  ,BKS.life_in_months  ) -1

  INTO O_USEFUL_LIFE
  ,O_DATE_STRAT_LIFE
  ,O_DATE_END_LIFE

  from FA_BOOKS BKS
  where ASSET_ID = P_ASSET_ID  
  AND TRANSACTION_HEADER_ID_IN = V_TRANSACTION_HEADER_ID;
  
 EXCEPTION WHEN OTHERS THEN
 	O_USEFUL_LIFE := NULL;
    O_DATE_STRAT_LIFE := NULL;
    O_DATE_END_LIFE := NULL;
 END;
 

EXCEPTION WHEN OTHERS THEN
    NULL;
END;



procedure CF_ADJUST_LAST(P_PERIOD IN date, --VARCHAR2,
                         P_ASSET_ID IN NUMBER,
                         O_USE_LIFT_LAST OUT NUMBER,
                         O_START_DATE_LAST OUT VARCHAR2,
                         O_END_DATE_LAST OUT VARCHAR2,
                         O_AMOUNT_LAST OUT NUMBER
                         )   IS

V_TRANSACTION_HEADER_ID NUMBER;  
V_PERIOD VARCHAR2(20);                       
BEGIN

	V_TRANSACTION_HEADER_ID := NULL;
	V_PERIOD := P_PERIOD;
	
	IF V_PERIOD IS NULL THEN 
		V_PERIOD := TO_CHAR(SYSDATE,'MON-YY');
	END IF;
  
  BEGIN
   /*SELECT MAX(ft.TRANSACTION_HEADER_ID)
   INTO V_TRANSACTION_HEADER_ID
   FROM --FA_TRANSACTION_HEADERS FT
   FA_TRANSACTION_HISTORY_TRX_V FT
   ,FA_BOOKS B
   WHERE B.ASSET_ID = FT.ASSET_ID
   AND B.TRANSACTION_HEADER_ID_IN = ft.TRANSACTION_HEADER_ID
   AND B.ASSET_ID = P_ASSET_ID
   --AND LAST_DAY(ft.TRANSACTION_DATE_ENTERED) < last_day(to_date(V_PERIOD,'MON-YY'))
   AND LAST_DAY(TO_DATE(ft.PERIOD_ENTERED,'MON-YY')) < last_day(to_date(V_PERIOD,'MON-YY'))
   ;*/
   
   /*SELECT MAX(ft.TRANSACTION_HEADER_ID)
   INTO V_TRANSACTION_HEADER_ID
   FROM FA_TRANSACTION_HEADERS FT
   WHERE FT.ASSET_ID = P_ASSET_ID
    AND ft.DATE_EFFECTIVE < P_PERIOD
   ;*/
   --ktp 20171010 fixed issue no.18
   SELECT MAX(ft.TRANSACTION_HEADER_ID)
   INTO V_TRANSACTION_HEADER_ID
   FROM FA_TRANSACTION_HEADERS FT
   ,FA_BOOKS B
   WHERE B.ASSET_ID = FT.ASSET_ID
   AND B.TRANSACTION_HEADER_ID_IN= ft.TRANSACTION_HEADER_ID
   AND B.ASSET_ID = P_ASSET_ID
   AND ft.DATE_EFFECTIVE < P_PERIOD
   ;


  EXCEPTION WHEN OTHERS THEN
    V_TRANSACTION_HEADER_ID := NULL;
  END;
  

  
  	O_USE_LIFT_LAST := NULL;
    O_START_DATE_LAST := NULL;
    O_END_DATE_LAST := NULL;
    O_AMOUNT_LAST := NULL;

    
  BEGIN 
  select fnd_number.canonical_to_number(NVL(TRUNC(BKS.life_in_months /12),0)|| '.'||NVL(MOD(BKS.life_in_months,12),0))   USE_LIFT_LAST   --AI
   --ktp 20171010 fixed issue no.19
  ,to_char(BKS.DATE_PLACED_IN_SERVICE,'DD-MON-YYYY')  START_DATE_LAST     --AJ 
  ,TO_CHAR(ADD_MONTHS (BKS.DATE_PLACED_IN_SERVICE  ,BKS.life_in_months  ) -1 ,'DD-MON-YYYY' )   END_DATE_LAST   --AK
  --,BKS.DATE_PLACED_IN_SERVICE
  --,ADD_MONTHS (BKS.DATE_PLACED_IN_SERVICE  ,BKS.life_in_months  ) -1  
  ,BKS.COST      AMOUNT_LAST     --AM
  INTO O_USE_LIFT_LAST
  ,O_START_DATE_LAST
  ,O_END_DATE_LAST
  ,O_AMOUNT_LAST
  from FA_BOOKS BKS
  where ASSET_ID = P_ASSET_ID  
  AND TRANSACTION_HEADER_ID_IN = V_TRANSACTION_HEADER_ID;
 EXCEPTION WHEN OTHERS THEN
 	O_USE_LIFT_LAST := NULL;
    O_START_DATE_LAST := NULL;
    O_END_DATE_LAST := NULL;
    O_AMOUNT_LAST := NULL;
 END;
 

EXCEPTION WHEN OTHERS THEN
    NULL;
END;


procedure CF_FDS(P_PERIOD_COUNTER IN NUMBER,
                         P_ASSET_ID IN NUMBER,
                         P_BOOK_TYPE_CODE IN VARCHAR2,
                         O_PERIOD_COUNTER OUT NUMBER,
                         O_YTD_DEPRN OUT NUMBER,
                         O_DEPRN_ACCUM OUT NUMBER,
                         O_ACC_ACCUM OUT VARCHAR2,
                         O_ACC_DEPRE OUT VARCHAR2
                         )   IS
                                                
                         
BEGIN

   -- WRITE_LOG('update FDS_PERIOD_COUNTER '|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'log');
    /*update TAC_ASSET_LISTING AL
    set FDS_ASSET_ID = (SELECT YTD.ASSET_ID
                        FROM FA_DEPRN_SUMMARY ytd
                        WHERE     YTD.DEPRN_SOURCE_CODE = 'DEPRN'
                        AND YTD.PERIOD_COUNTER = (SELECT MAX (PERIOD_COUNTER)
                                                    FROM FA_DEPRN_SUMMARY
                                                    WHERE     ASSET_ID = YTD.ASSET_ID                   --12413324
                                                    AND DEPRN_SOURCE_CODE = 'DEPRN'
                                                    AND PERIOD_COUNTER <= P_PERIOD_COUNTER)
                        AND YTD.ASSET_ID = AL.ASSET_ID
                        AND YTD.BOOK_TYPE_CODE = AL.BOOK_TYPE_CODE);*/
                        
   /*update TAC_ASSET_LISTING AL
    set FDS_PERIOD_COUNTER = (SELECT MAX (PERIOD_COUNTER)
                               FROM FA_DEPRN_SUMMARY
                               WHERE  ASSET_ID = AL.ASSET_ID 
                               AND BOOK_TYPE_CODE = AL.BOOK_TYPE_CODE                 
                               AND DEPRN_SOURCE_CODE = 'DEPRN'
                                AND PERIOD_COUNTER <= P_PERIOD_COUNTER);
            
     COMMIT;
     
     WRITE_LOG('update YTD_DEPRN '|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'log');
     update TAC_ASSET_LISTING AL
     set YTD_DEPRN = (SELECT YTD.YTD_DEPRN
                        FROM FA_DEPRN_SUMMARY ytd
                        WHERE YTD.DEPRN_SOURCE_CODE = 'DEPRN'                       
                        AND YTD.ASSET_ID = AL.ASSET_ID
                        AND YTD.BOOK_TYPE_CODE = AL.BOOK_TYPE_CODE
                        AND YTD.PERIOD_COUNTER = AL.FDS_PERIOD_COUNTER)
     WHERE FDS_PERIOD_COUNTER IS NOT NULL;
            
     COMMIT;
     
     
     WRITE_LOG('update DEPRN_ACCUM '|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'log');
     update TAC_ASSET_LISTING AL
     set DEPRN_ACCUM = (SELECT YTD.DEPRN_RESERVE
                        FROM FA_DEPRN_SUMMARY ytd
                        WHERE YTD.DEPRN_SOURCE_CODE = 'DEPRN'                        
                        AND YTD.ASSET_ID = AL.ASSET_ID
                        AND YTD.BOOK_TYPE_CODE = AL.BOOK_TYPE_CODE
                        AND YTD.PERIOD_COUNTER = AL.FDS_PERIOD_COUNTER)
     WHERE FDS_PERIOD_COUNTER IS NOT NULL;
            
     COMMIT;*/

    BEGIN
        SELECT --YTD.ASSET_ID,
        YTD.YTD_DEPRN, 
        YTD.DEPRN_RESERVE,
        YTD.PERIOD_COUNTER
        INTO --O_FDS_ASSET_ID,
        O_YTD_DEPRN,
        O_DEPRN_ACCUM,
        O_PERIOD_COUNTER
        FROM FA_DEPRN_SUMMARY ytd
        WHERE     YTD.DEPRN_SOURCE_CODE = 'DEPRN'
        AND YTD.PERIOD_COUNTER = (SELECT MAX (PERIOD_COUNTER)
                                    FROM FA_DEPRN_SUMMARY
                                    WHERE  ASSET_ID = P_ASSET_ID                   --12413324
                                    AND BOOK_TYPE_CODE = P_BOOK_TYPE_CODE
                                    AND DEPRN_SOURCE_CODE = 'DEPRN'
                                    AND PERIOD_COUNTER <= P_PERIOD_COUNTER)
        AND YTD.ASSET_ID = P_ASSET_ID
        AND YTD.BOOK_TYPE_CODE = P_BOOK_TYPE_CODE;
      
   EXCEPTION WHEN OTHERS THEN
   
        NULL;
   END;
   
   
  -- WRITE_LOG('update ACC_ACCUM '|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'log');
    /* update TAC_ASSET_LISTING AL
     set ACC_ACCUM = (SELECT G.SEGMENT11  DEPRN_EXPENSE_ACCT          
                        FROM FA_DEPRN_DETAIL D,
                        GL_CODE_COMBINATIONS G
                        WHERE G.CODE_COMBINATION_ID = D.DEPRN_EXPENSE_CCID
                        AND D.ASSET_ID = AL.ASSET_ID
                        AND D.BOOK_TYPE_CODE = AL.BOOK_TYPE_CODE
                        AND D.PERIOD_COUNTER = AL.FDS_PERIOD_COUNTER
                        AND ROWNUM = 1)
     WHERE FDS_PERIOD_COUNTER IS NOT NULL;
            
     COMMIT;
     
    
    WRITE_LOG('update ACC_DEPRE '|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'log'); 
     update TAC_ASSET_LISTING AL
     set ACC_DEPRE = (SELECT SEGMENT11 DEPRN_RESERVE_ACCT          
                        FROM FA_DEPRN_DETAIL D,
                        GL_CODE_COMBINATIONS G
                        WHERE  G.CODE_COMBINATION_ID = D.DEPRN_RESERVE_CCID
                        AND D.ASSET_ID = AL.ASSET_ID
                        AND D.BOOK_TYPE_CODE = AL.BOOK_TYPE_CODE
                        AND D.PERIOD_COUNTER = AL.FDS_PERIOD_COUNTER
                        AND ROWNUM = 1)
     WHERE FDS_PERIOD_COUNTER IS NOT NULL;
            
     COMMIT;
     */
   
   
   BEGIN
   
           SELECT (SELECT SEGMENT11 FROM GL_CODE_COMBINATIONS
                    WHERE CODE_COMBINATION_ID = D.DEPRN_EXPENSE_CCID) DEPRN_EXPENSE_ACCT
                 ,(SELECT SEGMENT11 FROM GL_CODE_COMBINATIONS
                    WHERE CODE_COMBINATION_ID = D.DEPRN_RESERVE_CCID) DEPRN_RESERVE_ACCT
           INTO O_ACC_ACCUM
           ,O_ACC_DEPRE
           FROM FA_DEPRN_DETAIL D
           WHERE  D.ASSET_ID = P_ASSET_ID
            AND D.BOOK_TYPE_CODE = P_BOOK_TYPE_CODE
            AND PERIOD_COUNTER = O_PERIOD_COUNTER
           /*AND D.PERIOD_COUNTER = (SELECT MAX(PERIOD_COUNTER)
                                 FROM FA_DEPRN_DETAIL
                                 WHERE ASSET_ID = D.ASSET_ID
                                 AND DEPRN_EXPENSE_CCID IS NOT NULL
                                 AND DEPRN_RESERVE_CCID IS NOT NULL 
                                 and PERIOD_COUNTER <= P_PERIOD_COUNTER)*/
         ;
                                 

  EXCEPTION WHEN OTHERS THEN
   
        NULL;
 END;
 
 
 /*WRITE_LOG('update ACC_ASSET '|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'log'); 
  update TAC_ASSET_LISTING AL
  set ACC_ASSET = ''
  WHERE NVL(AL.FDS_PERIOD_COUNTER,0) = 0;
  
  COMMIT;
  */
  

                


END;


procedure CF_FDS_LAST(P_PERIOD_COUNTER IN NUMBER,
                         P_ASSET_ID IN NUMBER,
                         P_BOOK_TYPE_CODE IN VARCHAR2,
                         O_PERIOD_COUNTER OUT NUMBER,
                         --O_YTD_DEPRN OUT NUMBER,
                         O_DEPRN_ACCUM OUT NUMBER,
                         O_ACC_ACCUM OUT VARCHAR2,
                         O_ACC_DEPRE OUT VARCHAR2
                         )   IS
                         
                       
BEGIN

   -- WRITE_LOG('update FDS_LAST_PERIOD_COUNTER '|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'log');
    /*update TAC_ASSET_LISTING AL
    set FDS_ASSET_ID = (SELECT YTD.ASSET_ID
                        FROM FA_DEPRN_SUMMARY ytd
                        WHERE     YTD.DEPRN_SOURCE_CODE = 'DEPRN'
                        AND YTD.PERIOD_COUNTER = (SELECT MAX (PERIOD_COUNTER)
                                                    FROM FA_DEPRN_SUMMARY
                                                    WHERE     ASSET_ID = YTD.ASSET_ID                   --12413324
                                                    AND DEPRN_SOURCE_CODE = 'DEPRN'
                                                    AND PERIOD_COUNTER <= P_PERIOD_COUNTER)
                        AND YTD.ASSET_ID = AL.ASSET_ID
                        AND YTD.BOOK_TYPE_CODE = AL.BOOK_TYPE_CODE);*/
                        
    /*update TAC_ASSET_LISTING AL
    set FDS_LAST_PERIOD_COUNTER = (SELECT MAX (PERIOD_COUNTER)
                                    FROM FA_DEPRN_SUMMARY
                                    WHERE  ASSET_ID = AL.ASSET_ID 
                                    AND BOOK_TYPE_CODE = AL.BOOK_TYPE_CODE                 
                                    AND DEPRN_SOURCE_CODE = 'DEPRN'
                                    AND PERIOD_COUNTER <= P_PERIOD_COUNTER - 1);
            
     COMMIT;
     
     
     
     WRITE_LOG('update DEPRN_ACCUM_LAST '|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'log');
     update TAC_ASSET_LISTING AL
     set DEPRN_ACCUM_LAST = (SELECT YTD.DEPRN_RESERVE
                        FROM FA_DEPRN_SUMMARY ytd
                        WHERE YTD.DEPRN_SOURCE_CODE = 'DEPRN'                        
                        AND YTD.ASSET_ID = AL.ASSET_ID
                        AND YTD.BOOK_TYPE_CODE = AL.BOOK_TYPE_CODE
                        AND YTD.PERIOD_COUNTER = AL.FDS_LAST_PERIOD_COUNTER)
     WHERE FDS_LAST_PERIOD_COUNTER IS NOT NULL;
            
     COMMIT;*/

    BEGIN
        SELECT --YTD.ASSET_ID,
        --YTD.YTD_DEPRN, 
        YTD.DEPRN_RESERVE,
        YTD.PERIOD_COUNTER
        INTO --O_FDS_ASSET_ID,
        --O_YTD_DEPRN,
        O_DEPRN_ACCUM,
        O_PERIOD_COUNTER
        FROM FA_DEPRN_SUMMARY ytd
        WHERE     YTD.DEPRN_SOURCE_CODE = 'DEPRN'
            AND YTD.PERIOD_COUNTER = (SELECT MAX (PERIOD_COUNTER)
                                    FROM FA_DEPRN_SUMMARY
                                    WHERE  ASSET_ID = P_ASSET_ID                   --12413324
                                    AND BOOK_TYPE_CODE = P_BOOK_TYPE_CODE
                                    AND DEPRN_SOURCE_CODE = 'DEPRN'
                                    AND PERIOD_COUNTER <= P_PERIOD_COUNTER -1 ) -- 20171020 kitiya fix bug
            AND YTD.ASSET_ID = P_ASSET_ID
            AND YTD.BOOK_TYPE_CODE = P_BOOK_TYPE_CODE;
      
   EXCEPTION WHEN OTHERS THEN
   
        NULL;
   END;
   
   
 /*  WRITE_LOG('update ACC_ACCUM_LAST '|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'log');
   update TAC_ASSET_LISTING AL
     set ACC_ACCUM_LAST = (SELECT G.SEGMENT11  DEPRN_EXPENSE_ACCT          
                        FROM FA_DEPRN_DETAIL D,
                        GL_CODE_COMBINATIONS G
                        WHERE G.CODE_COMBINATION_ID = D.DEPRN_EXPENSE_CCID
                        AND D.ASSET_ID = AL.ASSET_ID
                        AND D.BOOK_TYPE_CODE = AL.BOOK_TYPE_CODE
                        AND D.PERIOD_COUNTER = AL.FDS_LAST_PERIOD_COUNTER
                        AND ROWNUM = 1)
    WHERE FDS_LAST_PERIOD_COUNTER IS NOT NULL;
            
     COMMIT;
     
    
    WRITE_LOG('update ACC_DEPRE_LAST '|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'log'); 
     update TAC_ASSET_LISTING AL
     set ACC_DEPRE_LAST = (SELECT SEGMENT11 DEPRN_RESERVE_ACCT          
                        FROM FA_DEPRN_DETAIL D,
                        GL_CODE_COMBINATIONS G
                        WHERE  G.CODE_COMBINATION_ID = D.DEPRN_RESERVE_CCID
                        AND D.ASSET_ID = AL.ASSET_ID
                        AND D.BOOK_TYPE_CODE = AL.BOOK_TYPE_CODE
                        AND D.PERIOD_COUNTER = AL.FDS_LAST_PERIOD_COUNTER
                        AND ROWNUM = 1)
    WHERE FDS_LAST_PERIOD_COUNTER IS NOT NULL;
            
     COMMIT;
   */
   
 BEGIN
   
           SELECT (SELECT SEGMENT11 FROM GL_CODE_COMBINATIONS
                    WHERE CODE_COMBINATION_ID = D.DEPRN_EXPENSE_CCID) DEPRN_EXPENSE_ACCT
                 ,(SELECT SEGMENT11 FROM GL_CODE_COMBINATIONS
                    WHERE CODE_COMBINATION_ID = D.DEPRN_RESERVE_CCID) DEPRN_RESERVE_ACCT
           INTO O_ACC_ACCUM
           ,O_ACC_DEPRE
           FROM FA_DEPRN_DETAIL D
           WHERE  D.ASSET_ID = P_ASSET_ID
            AND D.BOOK_TYPE_CODE = P_BOOK_TYPE_CODE
            AND PERIOD_COUNTER = O_PERIOD_COUNTER
           /*AND PERIOD_COUNTER = (SELECT MAX(PERIOD_COUNTER)
                                 FROM FA_DEPRN_DETAIL
                                 WHERE ASSET_ID = D.ASSET_ID
                                 AND DEPRN_EXPENSE_CCID IS NOT NULL
                                 AND DEPRN_RESERVE_CCID IS NOT NULL 
                                 and PERIOD_COUNTER <= P_PERIOD_COUNTER)*/
                                 ;
                                 

  EXCEPTION WHEN OTHERS THEN
   
        NULL;
 END;
 
 
 
 
 /*WRITE_LOG('update ACC_ASSET_LAST '|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'log'); 
  update TAC_ASSET_LISTING AL
  set ACC_ASSET_LAST = ''
  WHERE NVL(AL.FDS_LAST_PERIOD_COUNTER,0) = 0;
  
  COMMIT;
  */


END;
   
procedure gen_report (errbuf               OUT VARCHAR2,
                         retcode              OUT NUMBER,
                         P_BOOK IN VARCHAR2,
                         P_PERIOD IN VARCHAR2,
                         P_PROJECT IN VARCHAR2,
                         P_PO IN VARCHAR2 ,
                         P_LOCATION IN VARCHAR2,
                         P_CATEGORY IN VARCHAR2,                        
                         P_OWNER IN NUMBER

                         )   IS
                         
                         
CURSOR C1 (P_REQUEST_ID NUMBER)
          /*(LP_PERIOD VARCHAR2,
           LP_PERIOD_COUNTER NUMBER
           --,P_WHERE VARCHAR2
            )*/
IS

SELECT DISTINCT DATE_REGIST,
    ASSET_ID,
    ASSET_NUMBER,
    BOOK_TYPE_CODE,
    ACC_COMPANY,
    PROJECT_NUM,
    PO_NUMBER,
    PO_LINE,
    BOQ_ITEM,
    QTY_PO,
    AMOUNT_THB,    
    VENDOR_NAME,
    IDENTIFICATION,
    ASSET_GENERIC,
    FA_CAT,
    CAT_DESC,
    BRAND,
    MODEL_NUMBER,
    SERIAL,
    OWNER,
    CIM,
    ACC_ASSET,
    F_REMARK,
    ACC_ASSET_LAST,
    tag_number,
    LOC,
    LOC_NAME,
    LOCATION_ID,
    USEFUL_LIFE,
    DATE_STRAT_LIFE,
    DATE_END_LIFE,
    DEPRE,
    WRITEOFF_DATE,
    DEPRN_LAST
FROM TAC_ASSET_LISTING
WHERE BOOK_TYPE_CODE = P_BOOK
AND REQUEST_ID = P_REQUEST_ID
ORDER BY ASSET_NUMBER;



LP_PERIOD_COUNTER number;
LP_PERIOD_OPEN_DATE date;
LP_PERIOD_CLOSE_DATE date;
LP_PERIOD  varchar2(20);
LP_REQUEST_ID NUMBER;
V_USEFUL_LIFE NUMBER;
V_DATE_STRAT_LIFE VARCHAR2(20);
V_DATE_END_LIFE VARCHAR2(20);
V_USE_LIFT_LAST  NUMBER;
V_START_DATE_LAST  VARCHAR2(20);
V_END_DATE_LAST  VARCHAR2(20);
V_AMOUNT_LAST  NUMBER;
V_LOC_NAME VARCHAR2(200);
V_VENDOR_NAME VARCHAR2(240);
V_DEPRE NUMBER;
V_DEPRN_LAST NUMBER;
V_WRITEOFF_DATE DATE;

V_PERIOD_COUNTER NUMBER;
V_YTD_DEPRN NUMBER;
V_DEPRN_ACCUM NUMBER;
V_ACC_ASSET   VARCHAR2(240);
V_ACC_ACCUM VARCHAR2(240);
V_ACC_DEPRE VARCHAR2(240);


V_PERIOD_COUNTER_LAST NUMBER;
V_YTD_DEPRN_LAST NUMBER;
V_DEPRN_ACCUM_LAST NUMBER;
V_ACC_ASSET_LAST   VARCHAR2(240);
V_ACC_ACCUM_LAST VARCHAR2(240);
V_ACC_DEPRE_LAST VARCHAR2(240);


                         
 BEGIN
 
  WRITE_LOG('start process '|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'log');
 
 
 

    LP_PERIOD_COUNTER := null;
    LP_PERIOD := null;
    LP_REQUEST_ID := NULL;
    
    
    LP_REQUEST_ID := fnd_global.conc_request_id;
    
    
 DELETE FROM TAC_ASSET_LISTING
 WHERE REQUEST_ID <= LP_REQUEST_ID - 500
 ;
 COMMIT;
 
    
        if P_PERIOD is not null then
           
          begin
            select period_counter,PERIOD_OPEN_DATE,nvl(PERIOD_CLOSE_DATE,sysdate)
            into  LP_PERIOD_COUNTER,LP_PERIOD_OPEN_DATE,LP_PERIOD_CLOSE_DATE
            from   FA_DEPRN_PERIODS
            where  book_type_code = P_BOOK
            and   period_name    = P_PERIOD;
    
            LP_PERIOD := P_PERIOD;
          exception when others then
            LP_PERIOD_COUNTER := null;
            LP_PERIOD := null;
          end;
  
        ELSE
		  begin
            select period_name
            into  LP_PERIOD
            from   FA_DEPRN_PERIODS
            where  book_type_code = P_BOOK
            AND period_counter = (SELECT MAX(period_counter)
                                from   FA_DEPRN_PERIODS
                                where  book_type_code = P_BOOK);
                                
          exception when others then
            LP_PERIOD := null;
          end;                     
          
          begin                
            select period_counter,PERIOD_OPEN_DATE,nvl(PERIOD_CLOSE_DATE,sysdate)
            into  LP_PERIOD_COUNTER,LP_PERIOD_OPEN_DATE,LP_PERIOD_CLOSE_DATE
            from   FA_DEPRN_PERIODS
            where  book_type_code = P_BOOK
            and   period_name    = LP_PERIOD;
          exception when others then
            LP_PERIOD_COUNTER := null;
          end;
                          
   
        end if;
    

WRITE_LOG('insert TAC_ASSET_LISTING '|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'log');
    
    INSERT INTO TAC_ASSET_LISTING
    (ASSET_ID,
    BOOK_TYPE_CODE,
    DATE_REGIST,
    ACC_COMPANY,
    PROJECT_NUM,
    PO_NUMBER,
    PO_LINE,
    BOQ_ITEM,
    QTY_PO,
    AMOUNT_THB,
    IDENTIFICATION,
    ASSET_GENERIC,
    FA_CAT,
    CAT_DESC,
    BRAND,
    MODEL_NUMBER,
    SERIAL,
    OWNER,
    CIM,
    ACC_ASSET,
    ASSET_NUMBER,
    F_REMARK,
    ACC_ASSET_LAST,
    tag_number,
    LOC,
    LOCATION_ID,
    REQUEST_ID
    )
    SELECT --distinct
 FB.asset_id
,fb.BOOK_TYPE_CODE
,nvl(TO_CHAR(TO_DATE(FAB.ATTRIBUTE9,'YYYY/MM/DD HH24:MI:SS'),'DD-MON-YYYY'),TO_CHAR(FAB.CREATION_DATE,'DD-MON-YYYY')) DATE_REGIST 
,DHCC.SEGMENT1  ACC_COMPANY 
,FAI.PROJECT_NUM 
,FAI.PO_NUMBER   
,FAB.ATTRIBUTE5  PO_LINE  
,FAI.BOQ_ITEM  BOQ_ITEM  
 --,FAB.ATTRIBUTE4  QTY_PO      -- dev
 ,FAB.ATTRIBUTE10   QTY_PO      -- prod 
 -- ,FB.ORIGINAL_COST  AMOUNT_THB 
 ,fb.cost  AMOUNT_THB                                       
 ,replace(replace(FAT.DESCRIPTION,CHR(10),''),CHR(9),'')    IDENTIFICATION   
 ,FAI.generic_name   ASSET_GENERIC  
 --,fccb.cate_code FA_CAT   
 ,FAB.ATTRIBUTE_CATEGORY_CODE FA_CAT
 ,replace(replace(FCT.DESCRIPTION,CHR(10),''),CHR(9),'')    CAT_DESC
 ,fab.attribute1 BRAND 
 ,FAB.MODEL_NUMBER    
 ,FAB.SERIAL_NUMBER SERIAL   
 ,PAPF.FULL_NAME     OWNER   
 ,FAB.PROPERTY_TYPE_CODE   CIM 
 ,FCBB.ASSET_COST_ACCT   ACC_ASSET      
,FAB.ASSET_NUMBER         
,FAB.ATTRIBUTE3  F_REMARK
,FCBB.ASSET_COST_ACCT  ACC_ASSET_LAST 
,fab.tag_number
--,FCBB.CATEGORY_ID
--,TO_CHAR(FAB.CREATION_DATE,'MON-YY')  create_period_name
,FL.ATTRIBUTE1 LOC
,FDH.LOCATION_ID
,LP_REQUEST_ID
FROM  FA_ADDITIONS_B          FAB
     ,FA_ADDITIONS_TL         FAT
     ,FA_BOOKS                FB
     ,FA_CATEGORIES_TL FCT
     ,FA_CATEGORY_BOOKS       FCBB    
     ,(SELECT I.PO_NUMBER  
             ,I.PO_VENDOR_ID
             ,I.ASSET_ID
             ,I.PROJECT_ID 
             ,(SELECT PPA.SEGMENT1 
              FROM APPS.PA_PROJECTS_ALL PPA 
              WHERE  PPA.PROJECT_ID = I.PROJECT_ID
              and rownum = 1) PROJECT_NUM
             ,I.ATTRIBUTE7 generic_name
             ,I.ATTRIBUTE4 BOQ_ITEM
        FROM  FA_ASSET_INVOICES I)   FAI
     ,FA_DISTRIBUTION_HISTORY FDH
     ,FA_LOCATIONS FL  
     --,FA_ASSET_HISTORY         FAH
     ,GL_CODE_COMBINATIONS     DHCC
     --,FA_DEPRN_PERIODS         FDP
     ,PER_ALL_PEOPLE_F        PAPF                               

WHERE  FAB.ASSET_ID = FAT.ASSET_ID
AND FAB.ASSET_ID = FB.ASSET_ID
AND FAB.ASSET_ID = FAI.ASSET_ID(+)
--AND FAH.ASSET_ID = FAB.ASSET_ID
--AND FAH.CATEGORY_ID = FCBB.CATEGORY_ID
--AND FAB.ASSET_CATEGORY_ID = FAH.CATEGORY_ID
AND FAB.ASSET_CATEGORY_ID = FCBB.CATEGORY_ID --new
AND FAB.ASSET_CATEGORY_ID = FCT.CATEGORY_ID --new
--KTP 
--AND FAB.ATTRIBUTE_CATEGORY_CODE = FCCB.CATE_CODE
--AND FCCB.CATEGORY_ID = FCBB.CATEGORY_ID
AND FB.BOOK_TYPE_CODE  =  FCBB.BOOK_TYPE_CODE 
AND FAB.ASSET_ID = FDH.ASSET_ID
AND FDH.LOCATION_ID = FL.LOCATION_ID
AND PAPF.PERSON_ID(+)  = FDH.ASSIGNED_TO 
AND FB.TRANSACTION_HEADER_ID_IN = ( SELECT MAX(TRANSACTION_HEADER_ID_IN) 
                                    FROM APPS.FA_BOOKS FB1 
                                    WHERE FB1.ASSET_ID=FB.ASSET_ID 
                                    AND FB1.BOOK_TYPE_CODE = FB.BOOK_TYPE_CODE 
                                    )
 -----------------------------------------------------------------------------
 AND FDH.DATE_INEFFECTIVE IS NULL   
 AND FDH.CODE_COMBINATION_ID = DHCC.CODE_COMBINATION_ID

 -----------------------------------------------------------------------------
 --AND FB.ASSET_ID = FDS.ASSET_ID(+)
 --AND FB.BOOK_TYPE_CODE = FDS.BOOK_TYPE_CODE(+)

-------KTP-------
 --AND FB.ASSET_ID = FDS_LAST.ASSET_ID(+) 
 --AND FB.BOOK_TYPE_CODE = FDS_LAST.BOOK_TYPE_CODE(+) 


---------PARAMETER---------------------------
and nvl(TO_DATE(FAB.ATTRIBUTE9,'YYYY/MM/DD HH24:MI:SS'),FAB.CREATION_DATE) <= LAST_DAY(to_date(LP_PERIOD,'MON-YY'))
AND FB.BOOK_TYPE_CODE   =  P_BOOK
--KTP

AND (FAI.PO_NUMBER LIKE P_PO || '%' OR P_PO IS NULL )
AND ( FAI.PROJECT_NUM LIKE P_PROJECT || '%' OR P_PROJECT IS NULL )
--AND (fccb.cate_code LIKE :P_CATEGORY || '%' OR :P_CATEGORY   IS NULL )
AND (FAB.ATTRIBUTE_CATEGORY_CODE LIKE P_CATEGORY || '%' OR P_CATEGORY   IS NULL )
AND (FL.ATTRIBUTE1 LIKE P_LOCATION || '%' OR P_LOCATION   IS NULL )
AND (FDH.ASSIGNED_TO  =  P_OWNER OR  P_OWNER IS NULL)

/*AND (FAI.PO_NUMBER = P_PO OR P_PO IS NULL )
AND (FAI.PROJECT_NUM = P_PROJECT  OR P_PROJECT IS NULL )
AND (FAB.ATTRIBUTE_CATEGORY_CODE = P_CATEGORY  OR P_CATEGORY   IS NULL )
AND (FL.ATTRIBUTE1 = P_LOCATION  OR P_LOCATION   IS NULL )
AND (FDH.ASSIGNED_TO  =  P_OWNER OR  P_OWNER IS NULL)
*/
;

COMMIT;
        
     WRITE_LOG('TAC Asset Listing Report','OUT');  
     WRITE_LOG('Request ID : ' || LP_REQUEST_ID,'OUT');
     WRITE_LOG('Date/Time : '|| TO_CHAR(sysdate,'DD/Mon/YYYY'),'OUT');
     WRITE_LOG('Book :  ' || P_BOOK,'OUT');
     WRITE_LOG('Period : ' || LP_PERIOD,'OUT');
     
   		 					
     WRITE_LOG('','OUT');
     WRITE_LOG('Date Registration' || '|' ||'Asset Number' || '|' ||'Tag Number' || '|' ||'Company' || '|' ||'Project' || '|' ||'PO No.' 
               || '|' ||'PO Line' || '|' ||'BOQ Item' || '|' ||'QTY PO/BOQ Register' || '|' ||'QTY Real' || '|' ||'Amount (THB)' 
               || '|' ||'Vendor' || '|' ||'Location' || '|' ||'Location Name' || '|' || 'Identification' || '|' ||'Asset Part Generic Name'
               || '|' ||'FA Category'|| '|' ||'FA Category Description'|| '|' ||'Brand'|| '|' ||'Model'|| '|' ||'Serial'|| '|' ||'Owner'
               || '|' ||'CIM'|| '|' ||'Useful life'|| '|' ||'Date Start of Useful life'|| '|' ||'Date End of Useful Life'|| '|' ||'Depre'
               || '|' ||'YTD Depre'|| '|' ||'Depre Accum'|| '|' ||'Account Code for Asset'|| '|' ||'Account Code for Accum Depre'
               || '|' ||'Account Code  for Depre'|| '|' ||'Disposal or Write off Date'|| '|' ||'Remark'|| '|' ||'Useful life Last Period'
               || '|' ||'Useful life Start date Last Period'|| '|' ||'Useful Life End Date Last Period'|| '|' ||'Amount(THB) Last Period'
               || '|' ||'Depre  Last Period'|| '|' ||'Depre Accum Last Period'|| '|' ||'Account FA Last Period'
               || '|' ||'Account Accum Depre Last Period'|| '|' ||'Account Depre Last Period','OUT');
        
        
              
      WRITE_LOG('update loc_name '|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'log');
                         
        begin
	
           -- v_loc_name := '';
	 					
              
            update TAC_ASSET_LISTING al
            set LOC_NAME = (SELECT --replace(replace(ffvt.DESCRIPTION,CHR(10),''),CHR(9),'') LOC_NAME
                            ffvt.DESCRIPTION LOC_NAME
            FROM fnd_flex_value_sets fvs,
                fnd_flex_values fv,
                fnd_flex_values_tl ffvt,
                FA_LOCATIONS FL
            WHERE     fvs.FLEX_VALUE_SET_ID = fv.FLEX_VALUE_SET_ID
                AND fv.flex_value_id = ffvt.flex_value_id
                AND fvs.FLEX_VALUE_SET_NAME = 'TAC_FA_LOCATION_NAME'
                AND fv.PARENT_FLEX_VALUE_LOW = FL.SEGMENT1
                AND fv.flex_value = FL.SEGMENT2
                AND FL.LOCATION_ID = al.LOCATION_ID
                AND ROWNUM = 1)
            WHERE BOOK_TYPE_CODE = P_BOOK
            AND REQUEST_ID = LP_REQUEST_ID;
                
                commit;
        EXCEPTION WHEN OTHERS THEN
             NULL;
        end;
        
      
        WRITE_LOG('update vendor_name '|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'log');
   
        begin
            
            update TAC_ASSET_LISTING al
            set  VENDOR_NAME = (SELECT DISTINCT  PV.VENDOR_NAME
                                FROM APPS.PO_VENDORS PV,po_headers_all p 
                                WHERE PV.VENDOR_ID  = P.VENDOR_ID 
                                and p.segment1 = al.po_number)
            WHERE BOOK_TYPE_CODE = P_BOOK
            AND REQUEST_ID = LP_REQUEST_ID;
            
            commit;
  
  
        EXCEPTION WHEN OTHERS THEN
             NULL;
        end;
        
        
         WRITE_LOG('update depre '|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'log');
        V_DEPRE := null;
        begin 

            update TAC_ASSET_LISTING al
            set DEPRE = (SELECT DEPRN_AMOUNT --sum(DEPRN_AMOUNT)
                        FROM FA_DEPRN_SUMMARY 
                        WHERE ASSET_ID = al.ASSET_ID
                        AND BOOK_TYPE_CODE = al.BOOK_TYPE_CODE
                        AND DEPRN_SOURCE_CODE = 'DEPRN'
                        AND  PERIOD_COUNTER = LP_PERIOD_COUNTER )
            WHERE  EXISTS (SELECT 'X' 
                        FROM FA_DEPRN_SUMMARY 
                        WHERE ASSET_ID = al.ASSET_ID
                        AND BOOK_TYPE_CODE = al.BOOK_TYPE_CODE
                        AND DEPRN_SOURCE_CODE = 'DEPRN'
                        AND  PERIOD_COUNTER = LP_PERIOD_COUNTER)
                  AND BOOK_TYPE_CODE = P_BOOK
                  AND REQUEST_ID = LP_REQUEST_ID;
            
            commit;
        exception when others then 
            null;
        end; 
  
  
         WRITE_LOG('update deprn_last ' || to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'log');
        V_DEPRN_LAST := null;
        begin       

           update TAC_ASSET_LISTING al
            set DEPRN_LAST = (SELECT DEPRN_AMOUNT --sum(DEPRN_AMOUNT)
                                FROM FA_DEPRN_SUMMARY 
                                WHERE ASSET_ID = al.ASSET_ID
                                AND BOOK_TYPE_CODE = al.BOOK_TYPE_CODE
                                AND DEPRN_SOURCE_CODE = 'DEPRN'
                                AND  PERIOD_COUNTER = LP_PERIOD_COUNTER -1)
             WHERE BOOK_TYPE_CODE = P_BOOK
             AND REQUEST_ID = LP_REQUEST_ID;
            /*WHERE  EXISTS (SELECT 'X' 
                        FROM FA_DEPRN_SUMMARY 
                        WHERE ASSET_ID = al.ASSET_ID
                        AND BOOK_TYPE_CODE = al.BOOK_TYPE_CODE
                        AND DEPRN_SOURCE_CODE = 'DEPRN'
                        AND  PERIOD_COUNTER = LP_PERIOD_COUNTER - 1)*/
            
            commit;
  
        exception when others then 
            null;
        end;
   
  
         WRITE_LOG('update writeoff '|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'log');
        V_WRITEOFF_DATE := null;
        begin

            update TAC_ASSET_LISTING al
            set WRITEOFF_DATE = (SELECT to_char(MAX(FAR.DATE_RETIRED),'DD-MON-YYYY') 
                                FROM FA_RETIREMENTS           FAR   
                                WHERE FAR.ASSET_ID = al.ASSET_ID
                                AND FAR.STATUS = 'PROCESSED' )
            WHERE BOOK_TYPE_CODE = P_BOOK
            AND REQUEST_ID = LP_REQUEST_ID;          
            commit;
        exception when others then 
            null;
        end;
        
        
        /* WRITE_LOG('update fds '|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'log');
         CF_FDS(P_PERIOD_COUNTER => LP_PERIOD_COUNTER,
                         P_ASSET_ID => NULL,
                         P_BOOK_TYPE_CODE => NULL,
                         O_FDS_ASSET_ID => V_FDS_ASSET_ID,
                         O_YTD_DEPRN => V_YTD_DEPRN,
                         O_DEPRN_ACCUM => V_DEPRN_ACCUM,
                         O_ACC_ACCUM => V_ACC_ACCUM,
                         O_ACC_DEPRE => V_ACC_DEPRE);*/
        
 
      --  WRITE_LOG('update fds_last '|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'log');                
       /* CF_FDS_LAST(P_PERIOD_COUNTER => LP_PERIOD_COUNTER,
                         P_ASSET_ID => NULL,
                         P_BOOK_TYPE_CODE => NULL,
                         O_FDS_ASSET_ID => V_FDS_ASSET_ID_LAST,
                         O_YTD_DEPRN => V_YTD_DEPRN_LAST,
                         O_DEPRN_ACCUM => V_DEPRN_ACCUM_LAST,
                         O_ACC_ACCUM => V_ACC_ACCUM_LAST,
                         O_ACC_DEPRE => V_ACC_DEPRE_LAST);*/
                         
                         

        
   WRITE_LOG('START LOOP '|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'log');
   
   FOR REC1 IN C1 (LP_REQUEST_ID)--(LP_PERIOD,LP_PERIOD_COUNTER)
   LOOP
   
   
           --  WRITE_LOG('update adjust '|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'log');
              CF_ADJUST(P_PERIOD => LP_PERIOD_CLOSE_DATE, --LP_PERIOD,
                         P_ASSET_ID => REC1.ASSET_ID,
                         O_USEFUL_LIFE => V_USEFUL_LIFE,
                         O_DATE_STRAT_LIFE => V_DATE_STRAT_LIFE,
                         O_DATE_END_LIFE => V_DATE_END_LIFE
                         );
                         
             --  WRITE_LOG('update adjust_last '|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'log');         
               CF_ADJUST_LAST(P_PERIOD => LP_PERIOD_OPEN_DATE, --LP_PERIOD,
                         P_ASSET_ID => REC1.ASSET_ID,
                         O_USE_LIFT_LAST => V_USE_LIFT_LAST,
                         O_START_DATE_LAST => V_START_DATE_LAST,
                         O_END_DATE_LAST => V_END_DATE_LAST,
                         O_AMOUNT_LAST => V_AMOUNT_LAST
                         );
                         
                         
                         
                         
                  CF_FDS(P_PERIOD_COUNTER => LP_PERIOD_COUNTER,
                         P_ASSET_ID => REC1.ASSET_ID,
                         P_BOOK_TYPE_CODE => REC1.BOOK_TYPE_CODE,
                         O_PERIOD_COUNTER => V_PERIOD_COUNTER,
                         O_YTD_DEPRN => V_YTD_DEPRN,
                         O_DEPRN_ACCUM => V_DEPRN_ACCUM,
                         O_ACC_ACCUM => V_ACC_ACCUM,
                         O_ACC_DEPRE => V_ACC_DEPRE);
               

                IF NVL(V_PERIOD_COUNTER,0) = 0 THEN 
                   V_ACC_ASSET := '';  
                ELSE 
                   V_ACC_ASSET := REC1.ACC_ASSET;                
                END IF;  
                

                
             CF_FDS_LAST(P_PERIOD_COUNTER => LP_PERIOD_COUNTER,
                         P_ASSET_ID => REC1.ASSET_ID,
                         P_BOOK_TYPE_CODE => REC1.BOOK_TYPE_CODE,
                         O_PERIOD_COUNTER => V_PERIOD_COUNTER_LAST,
                         --O_YTD_DEPRN => V_YTD_DEPRN_LAST,
                         O_DEPRN_ACCUM => V_DEPRN_ACCUM_LAST,
                         O_ACC_ACCUM => V_ACC_ACCUM_LAST,
                         O_ACC_DEPRE => V_ACC_DEPRE_LAST);
                         
                    
                IF NVL(V_PERIOD_COUNTER_LAST,0) = 0 THEN 
                   V_ACC_ASSET_LAST := '';  
                ELSE 
                   V_ACC_ASSET_LAST := REC1.ACC_ASSET_LAST;                
                END IF;  
                
                
                         
                         
              
     --  WRITE_LOG('update write_log '|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'log');
       
      WRITE_LOG(rec1.DATE_REGIST || '|' ||rec1.ASSET_NUMBER|| '|' ||rec1.TAG_NUMBER || '|' ||rec1.ACC_COMPANY || '|' ||rec1.PROJECT_NUM 
      || '|' ||rec1.PO_NUMBER|| '|' ||rec1.PO_LINE || '|' ||rec1.BOQ_ITEM || '|' ||rec1.QTY_PO || '|' ||'' || '|' ||rec1.AMOUNT_THB 
               || '|' ||rec1.vendor_name || '|' ||rec1.Loc || '|' ||replace(replace(rec1.Loc_Name,CHR(10),''),CHR(9),'') || '|' || rec1.Identification 
               || '|' ||rec1.ASSET_GENERIC || '|' ||rec1.fa_cat|| '|' ||rec1.CAT_DESC|| '|' ||rec1.Brand|| '|' ||rec1.MODEL_NUMBER
               || '|' ||rec1.Serial|| '|' ||rec1.Owner|| '|' ||rec1.CIM|| '|' ||v_USEFUL_LIFE || '|' ||v_DATE_STRAT_LIFE|| '|' ||v_DATE_END_LIFE
               || '|' ||rec1.Depre|| '|' ||V_YTD_Deprn|| '|' ||V_DEPRN_ACCUM|| '|' ||V_ACC_ASSET|| '|' ||V_ACC_ACCUM
               || '|' ||V_ACC_DEPRE|| '|' ||rec1.WRITEOFF_DATE|| '|' ||rec1.F_Remark|| '|' ||v_USE_LIFT_LAST
               || '|' ||v_START_DATE_LAST|| '|' ||v_END_DATE_LAST|| '|' ||v_AMOUNT_LAST
               || '|' ||REC1.DEPRN_LAST|| '|' ||V_DEPRN_ACCUM_LAST|| '|' ||V_ACC_ASSET_LAST
               || '|' ||V_ACC_ACCUM_LAST|| '|' ||V_ACC_DEPRE_LAST,'OUT');    
   
   END LOOP;
   
    WRITE_LOG('FINISH LOOP '|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'),'log');

 EXCEPTION WHEN OTHERS THEN
    NULL;
 END;
 
 
end TAC_ASSET_LISTING_PKG;