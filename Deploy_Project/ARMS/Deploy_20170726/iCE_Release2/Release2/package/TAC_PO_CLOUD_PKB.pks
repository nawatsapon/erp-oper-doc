CREATE OR REPLACE PACKAGE APPS.TAC_PO_CLOUD_PKB is
/******************************************************************************
   NAME:       TAC_PO_CLOUD_PKB
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        3/30/2017   Kitiyakorn       1. Created this package.
   2.0        7/9/2017    Kittiya          2. Edit this package.
******************************************************************************/
  /*
  G_POCLOUD_INBOX        VARCHAR2(100) := 'TAC_FA_POCLOUD_INBOX';
  G_POCLOUD_ARCHIVE      VARCHAR2(100) := 'TAC_FA_POCLOUD_ARCHIVE';
  G_POCLOUD_ERROR        VARCHAR2(100) := 'TAC_FA_POCLOUD_ERROR';
  */
  TYPE tTemptextRec IS RECORD(PERIOD_NAME            VARCHAR2(150),
                             PO_NUMBER               VARCHAR2(150),
                             PO_LINE_NUMBER          VARCHAR2(150),
                             PROJECT_CODE            VARCHAR2(150),
                             CLOUD_VALUE_REGISTER    VARCHAR2(150),
                             CLOUD_QTY_REGISTER      VARCHAR2(150),
                             BOQ_STATUS              VARCHAR2(150),
                             CREATION_DATE           VARCHAR2(150),
                             CREATED_BY              VARCHAR2(150),
                             LAST_UPDATE_DATE        VARCHAR2(150),
                             LAST_UPDATED_BY         VARCHAR2(150),
                             FILE_NAME               VARCHAR2(150),
                             STATUS                  VARCHAR2(1),
                             COUNT_VALIDATE_STATUS   NUMBER,
                             ERROR_MESSAGE           VARCHAR2(5000)
                             ); 
  TYPE tTemptextTab IS TABLE OF tTemptextRec; 
  
  TYPE tTemp_text IS TABLE OF TAC_PO_CLOUD_INFO%rowtype;
  
  PROCEDURE WRITE_LOG(pCHAR IN VARCHAR2,pTYPE IN VARCHAR2);  
                      
  PROCEDURE TAC_PO_CLOUD_PROCESS(errbuf            OUT VARCHAR2,
                                 retcode           OUT NUMBER,
                                 p_pocloud_inbox   IN  VARCHAR2,
                                 p_pocloud_archive IN  VARCHAR2,
                                 p_pocloud_error   IN  VARCHAR2);
                                 
    PROCEDURE TAC_COPY_LOG_PROCESS(errbuf            OUT VARCHAR2,
                                   retcode           OUT NUMBER,
                                   p_request_id      IN  NUMBER,
                                   p_destination     IN  VARCHAR2);
END TAC_PO_CLOUD_PKB;
/

