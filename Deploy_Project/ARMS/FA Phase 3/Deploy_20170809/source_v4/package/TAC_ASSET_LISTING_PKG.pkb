CREATE OR REPLACE PACKAGE BODY APPS.TAC_ASSET_LISTING_PKG is

PROCEDURE WRITE_LOG (pCHAR IN VARCHAR2, pTYPE IN VARCHAR2)
   IS
   BEGIN
      IF UPPER (pTYPE) = 'LOG'
      THEN
         fnd_file.put_LINE (FND_FILE.LOG, pCHAR);
      ELSE
         fnd_file.put_line (FND_FILE.OUTPUT, pCHAR);
      END IF;

      DBMS_OUTPUT.put_line (Pchar);
   exception when others then
       DBMS_OUTPUT.put_line (sqlerrm);
   END WRITE_LOG;
   
procedure CF_ADJUST(P_PERIOD IN VARCHAR2,
                         P_ASSET_ID IN NUMBER,
                         O_USEFUL_LIFE OUT NUMBER,
                         O_DATE_STRAT_LIFE OUT VARCHAR2,
                         O_DATE_END_LIFE OUT VARCHAR2
                         )   IS

V_TRANSACTION_HEADER_ID NUMBER;  
V_PERIOD VARCHAR2(20);                       
BEGIN

	V_TRANSACTION_HEADER_ID := NULL;
	V_PERIOD := P_PERIOD;
	
	IF V_PERIOD IS NULL THEN 
		V_PERIOD := TO_CHAR(SYSDATE,'MON-YY');
	END IF;
  
  BEGIN
   SELECT MAX(ft.TRANSACTION_HEADER_ID)
   INTO V_TRANSACTION_HEADER_ID
   FROM --FA_TRANSACTION_HEADERS FT
   FA_TRANSACTION_HISTORY_TRX_V FT
   ,FA_BOOKS B
   WHERE B.ASSET_ID = FT.ASSET_ID
   AND B.TRANSACTION_HEADER_ID_IN= ft.TRANSACTION_HEADER_ID
   AND B.ASSET_ID = P_ASSET_ID
   --AND LAST_DAY(ft.TRANSACTION_DATE_ENTERED) <= last_day(to_date(V_PERIOD,'MON-YY'))
   AND LAST_DAY(TO_DATE(ft.PERIOD_ENTERED,'MON-YY')) <= last_day(to_date(V_PERIOD,'MON-YY'));

  EXCEPTION WHEN OTHERS THEN
    V_TRANSACTION_HEADER_ID := NULL;
  END;
  
  IF V_TRANSACTION_HEADER_ID IS NULL THEN
  	     BEGIN
         SELECT MAX(ft.TRANSACTION_HEADER_ID)
         INTO V_TRANSACTION_HEADER_ID
         FROM --FA_TRANSACTION_HEADERS FT
         FA_TRANSACTION_HISTORY_TRX_V FT
         ,FA_BOOKS B
         WHERE B.ASSET_ID = FT.ASSET_ID
        AND B.TRANSACTION_HEADER_ID_IN= ft.TRANSACTION_HEADER_ID
        AND B.ASSET_ID = P_ASSET_ID
        ;

        EXCEPTION WHEN OTHERS THEN
          V_TRANSACTION_HEADER_ID := NULL;
        END;
  	
  END IF;
  
	
  
  	O_USEFUL_LIFE := NULL;
    O_DATE_STRAT_LIFE := NULL;
    O_DATE_END_LIFE := NULL;

    
 BEGIN 
  select fnd_number.canonical_to_number(NVL(TRUNC(BKS.life_in_months /12),0)|| '.'||NVL(MOD(BKS.life_in_months,12),0))   USE_LIFT_LAST   --AI
  ,to_char(BKS.DATE_PLACED_IN_SERVICE,'DD-Mon-YYYY')  START_DATE_LAST     --AJ 
  ,TO_CHAR(ADD_MONTHS (BKS.DATE_PLACED_IN_SERVICE  ,BKS.life_in_months  ) -1 ,'DD-Mon-YYYY' )   END_DATE_LAST   --AK

  INTO O_USEFUL_LIFE
  ,O_DATE_STRAT_LIFE
  ,O_DATE_END_LIFE

  from FA_BOOKS BKS
  where ASSET_ID = P_ASSET_ID  
  AND TRANSACTION_HEADER_ID_IN = V_TRANSACTION_HEADER_ID;
  
 EXCEPTION WHEN OTHERS THEN
 	O_USEFUL_LIFE := NULL;
    O_DATE_STRAT_LIFE := NULL;
    O_DATE_END_LIFE := NULL;
 END;
 

EXCEPTION WHEN OTHERS THEN
    NULL;
END;



procedure CF_ADJUST_LAST(P_PERIOD IN VARCHAR2,
                         P_ASSET_ID IN NUMBER,
                         O_USE_LIFT_LAST OUT NUMBER,
                         O_START_DATE_LAST OUT VARCHAR2,
                         O_END_DATE_LAST OUT VARCHAR2,
                         O_AMOUNT_LAST OUT NUMBER
                         )   IS

V_TRANSACTION_HEADER_ID NUMBER;  
V_PERIOD VARCHAR2(20);                       
BEGIN

	V_TRANSACTION_HEADER_ID := NULL;
	V_PERIOD := P_PERIOD;
	
	IF V_PERIOD IS NULL THEN 
		V_PERIOD := TO_CHAR(SYSDATE,'MON-YY');
	END IF;
  
  BEGIN
   SELECT MAX(ft.TRANSACTION_HEADER_ID)
   INTO V_TRANSACTION_HEADER_ID
   FROM --FA_TRANSACTION_HEADERS FT
   FA_TRANSACTION_HISTORY_TRX_V FT
   ,FA_BOOKS B
   WHERE B.ASSET_ID = FT.ASSET_ID
   AND B.TRANSACTION_HEADER_ID_IN = ft.TRANSACTION_HEADER_ID
   AND B.ASSET_ID = P_ASSET_ID
   --AND LAST_DAY(ft.TRANSACTION_DATE_ENTERED) < last_day(to_date(V_PERIOD,'MON-YY'))
   AND LAST_DAY(TO_DATE(ft.PERIOD_ENTERED,'MON-YY')) < last_day(to_date(V_PERIOD,'MON-YY'))
   ;

  EXCEPTION WHEN OTHERS THEN
    V_TRANSACTION_HEADER_ID := NULL;
  END;
  

  
  	O_USE_LIFT_LAST := NULL;
    O_START_DATE_LAST := NULL;
    O_END_DATE_LAST := NULL;
    O_AMOUNT_LAST := NULL;

    
  BEGIN 
  select fnd_number.canonical_to_number(NVL(TRUNC(BKS.life_in_months /12),0)|| '.'||NVL(MOD(BKS.life_in_months,12),0))   USE_LIFT_LAST   --AI
  ,to_char(BKS.DATE_PLACED_IN_SERVICE,'DD-Mon-YYYY')  START_DATE_LAST     --AJ 
  ,TO_CHAR(ADD_MONTHS (BKS.DATE_PLACED_IN_SERVICE  ,BKS.life_in_months  ) -1 ,'DD-Mon-YYYY' )   END_DATE_LAST   --AK
  ,BKS.COST      AMOUNT_LAST     --AM
  INTO O_USE_LIFT_LAST
  ,O_START_DATE_LAST
  ,O_END_DATE_LAST
  ,O_AMOUNT_LAST
  from FA_BOOKS BKS
  where ASSET_ID = P_ASSET_ID  
  AND TRANSACTION_HEADER_ID_IN = V_TRANSACTION_HEADER_ID;
 EXCEPTION WHEN OTHERS THEN
 	O_USE_LIFT_LAST := NULL;
    O_START_DATE_LAST := NULL;
    O_END_DATE_LAST := NULL;
    O_AMOUNT_LAST := NULL;
 END;
 

EXCEPTION WHEN OTHERS THEN
    NULL;
END;
   
procedure gen_report (errbuf               OUT VARCHAR2,
                         retcode              OUT NUMBER,
                         P_BOOK IN VARCHAR2,
                         P_PERIOD IN VARCHAR2,
                         P_PROJECT IN VARCHAR2,
                         P_PO IN VARCHAR2 ,
                         P_LOCATION IN VARCHAR2,
                         P_CATEGORY IN VARCHAR2,                        
                         P_OWNER IN NUMBER

                         )   IS
                         
                         
CURSOR C1 (LP_PERIOD VARCHAR2,
           LP_PERIOD_COUNTER NUMBER
            )
IS

SELECT distinct
FB.asset_id
,fb.BOOK_TYPE_CODE
,nvl(TO_CHAR(TO_DATE(FAB.ATTRIBUTE9,'YYYY/MM/DD HH24:MI:SS'),'DD-MON-YY'),TO_CHAR(FAB.CREATION_DATE,'DD-MON-YY')) DATE_REGIST 
,FAB.CREATION_DATE
,DHCC.SEGMENT1  ACC_COMPANY 
,FAI.PROJECT_NUM 
,FAI.PO_NUMBER   
,FAB.ATTRIBUTE5  PO_LINE  
,FAI.BOQ_ITEM  BOQ_ITEM  
 --,FAB.ATTRIBUTE4  QTY_PO      -- dev
 ,FAB.ATTRIBUTE10   QTY_PO      -- prod
 ,FAB.PARENT_ASSET_ID   
 -- ,FB.ORIGINAL_COST  AMOUNT_THB 
 ,fb.cost  AMOUNT_THB                                       
 ,replace(replace(FAT.DESCRIPTION,CHR(10),''),CHR(9),'')    IDENTIFICATION   
 ,FAI.generic_name   ASSET_GENERIC  
 ,fccb.cate_code FA_CAT   
 ,replace(replace(FCCB.DESCRIPTION,CHR(10),''),CHR(9),'')    CAT_DESC
 ,fab.attribute1 BRAND 
 ,FAB.MODEL_NUMBER    
 ,FAB.SERIAL_NUMBER SERIAL   
 ,PAPF.FULL_NAME     OWNER   
 ,FAB.PROPERTY_TYPE_CODE   CIM 
 ,FDS.YTD_DEPRN
 ,FDS.DEPRN_RESERVE     DEPRN_ACCUM    
 ,CASE NVL(FDS.ASSET_ID,0) WHEN 0 THEN ''  
  ELSE FCBB.ASSET_COST_ACCT END  ACC_ASSET   
 ,FDS.DEPRN_RESERVE_ACCT   ACC_ACCUM     
 ,FDS.DEPRN_EXPENSE_ACCT   ACC_DEPRE       
 , 'Y'   REGISTER_FA   
,FAB.ASSET_NUMBER         
,FAB.ATTRIBUTE3  F_REMARK
 ,FDS_LAST.DEPRN_RESERVE DEPRN_ACCUM_LAST   
 ,CASE NVL(FDS_LAST.ASSET_ID,0) WHEN 0 THEN ''  
  ELSE FCBB.ASSET_COST_ACCT END  ACC_ASSET_LAST         
 ,FDS_LAST.DEPRN_RESERVE_ACCT   ACC_ACCUM_LAST     
 ,FDS_LAST.DEPRN_EXPENSE_ACCT   ACC_DEPRE_LAST  
,fab.tag_number
,FCBB.CATEGORY_ID
,TO_CHAR(FAB.CREATION_DATE,'MON-YY')  create_period_name
,CASE WHEN FAI.PO_NUMBER  IS NOT NULL  AND ( FAB.CREATION_DATE <= TO_DATE(LP_PERIOD,'MM/RRRR') ) THEN
        'Y'
        ELSE NULL
        END   REGISTER_FA_LAST 
,FL.ATTRIBUTE1 LOC
,FDH.LOCATION_ID

FROM  FA_ADDITIONS_B          FAB
     ,FA_ADDITIONS_TL         FAT
     ,FA_BOOKS                FB
     ,(SELECT FCT.CATEGORY_ID
       ,FCB.SEGMENT1||'-'||FCB.SEGMENT2 CATE_CODE
       ,FCT.DESCRIPTION
       FROM FA_CATEGORIES_TL        FCT,
       FA_CATEGORIES_B         FCB
       WHERE FCT.CATEGORY_ID = FCB.CATEGORY_ID) FCCB
     ,FA_CATEGORY_BOOKS       FCBB    
     ,(SELECT I.PO_NUMBER  
             ,I.PO_VENDOR_ID
             ,I.ASSET_ID
             ,I.PROJECT_ID 
             ,(SELECT DISTINCT PPA.SEGMENT1 
              FROM APPS.PA_PROJECTS_ALL PPA 
              WHERE  PPA.PROJECT_ID = I.PROJECT_ID) PROJECT_NUM
             ,I.ATTRIBUTE7 generic_name
             ,I.ATTRIBUTE4 BOQ_ITEM
        FROM  FA_ASSET_INVOICES I)   FAI
     ,FA_DISTRIBUTION_HISTORY FDH
     ,FA_LOCATIONS FL  
     ,FA_ASSET_HISTORY         FAH
     ,GL_CODE_COMBINATIONS     DHCC
     --,FA_DEPRN_PERIODS         FDP
     ,PER_ALL_PEOPLE_F        PAPF   
     
          ,(SELECT YTD.ASSET_ID,YTD.YTD_DEPRN,YTD.DEPRN_RESERVE,YTD.BOOK_TYPE_CODE     
,YTD.PERIOD_COUNTER YTD_PERIOD
,(SELECT SEGMENT11 FROM GL_CODE_COMBINATIONS
              WHERE CODE_COMBINATION_ID = DD.DEPRN_EXPENSE_CCID) DEPRN_EXPENSE_ACCT
              ,(SELECT SEGMENT11 FROM GL_CODE_COMBINATIONS
              WHERE CODE_COMBINATION_ID = DD.DEPRN_RESERVE_CCID) DEPRN_RESERVE_ACCT
           
           /* ,CASE NVL(RET.DATE_RETIRED,SYSDATE) WHEN SYSDATE THEN (SELECT SEGMENT11 FROM GL_CODE_COMBINATIONS
              WHERE CODE_COMBINATION_ID = DD.DEPRN_EXPENSE_CCID) 
              ELSE '' END DEPRN_EXPENSE_ACCT
            ,CASE NVL(RET.DATE_RETIRED,SYSDATE) WHEN SYSDATE THEN  (SELECT SEGMENT11 FROM GL_CODE_COMBINATIONS
              WHERE CODE_COMBINATION_ID = DD.DEPRN_RESERVE_CCID) 
              ELSE '' END DEPRN_RESERVE_ACCT*/

            FROM FA_DEPRN_SUMMARY ytd,
           (SELECT ASSET_ID,distribution_id,BOOK_TYPE_CODE,PERIOD_COUNTER,DEPRN_EXPENSE_CCID,DEPRN_RESERVE_CCID
           FROM FA_DEPRN_DETAIL D
           WHERE PERIOD_COUNTER = (SELECT MAX(PERIOD_COUNTER)
                                 FROM FA_DEPRN_DETAIL
                                 WHERE ASSET_ID = D.ASSET_ID
                                 AND DEPRN_EXPENSE_CCID IS NOT NULL
                                 AND DEPRN_RESERVE_CCID IS NOT NULL 
                                 and PERIOD_COUNTER <= LP_PERIOD_COUNTER
                                 )) DD,
            (SELECT MAX(FAR.DATE_RETIRED ) DATE_RETIRED,FAR.ASSET_ID 
             FROM FA_RETIREMENTS           FAR               
             WHERE  FAR.STATUS = 'PROCESSED' 
             GROUP BY FAR.ASSET_ID) RET
            
            WHERE YTD.DEPRN_SOURCE_CODE = 'DEPRN'            
            AND YTD.ASSET_ID = DD.ASSET_ID(+)
            and YTD.BOOK_TYPE_CODE = DD.BOOK_TYPE_CODE(+)  
            AND YTD.ASSET_ID = RET.ASSET_ID(+) 
                               
            AND YTD.PERIOD_COUNTER = (SELECT MAX(PERIOD_COUNTER) FROM FA_DEPRN_SUMMARY
                                  WHERE ASSET_ID = YTD.ASSET_ID --12413324
                                  AND DEPRN_SOURCE_CODE = 'DEPRN'
                                  and PERIOD_COUNTER <= LP_PERIOD_COUNTER
                                  )
                                  ) FDS
                                  
      ,(SELECT YTD.ASSET_ID,YTD.YTD_DEPRN,YTD.DEPRN_RESERVE,YTD.BOOK_TYPE_CODE     
,YTD.PERIOD_COUNTER YTD_PERIOD
,(SELECT SEGMENT11 FROM GL_CODE_COMBINATIONS
              WHERE CODE_COMBINATION_ID = DD.DEPRN_EXPENSE_CCID) DEPRN_EXPENSE_ACCT
              ,(SELECT SEGMENT11 FROM GL_CODE_COMBINATIONS
              WHERE CODE_COMBINATION_ID = DD.DEPRN_RESERVE_CCID) DEPRN_RESERVE_ACCT
           
            /*,CASE NVL(RET.DATE_RETIRED,SYSDATE) WHEN SYSDATE THEN (SELECT SEGMENT11 FROM GL_CODE_COMBINATIONS
              WHERE CODE_COMBINATION_ID = DD.DEPRN_EXPENSE_CCID) 
              ELSE '' END DEPRN_EXPENSE_ACCT
            ,CASE NVL(RET.DATE_RETIRED,SYSDATE) WHEN SYSDATE THEN  (SELECT SEGMENT11 FROM GL_CODE_COMBINATIONS
              WHERE CODE_COMBINATION_ID = DD.DEPRN_RESERVE_CCID) 
              ELSE '' END DEPRN_RESERVE_ACCT*/

            FROM FA_DEPRN_SUMMARY ytd,
           (SELECT ASSET_ID,distribution_id,BOOK_TYPE_CODE,PERIOD_COUNTER,DEPRN_EXPENSE_CCID,DEPRN_RESERVE_CCID
           FROM FA_DEPRN_DETAIL D
           WHERE PERIOD_COUNTER = (SELECT MAX(PERIOD_COUNTER)
                                 FROM FA_DEPRN_DETAIL
                                 WHERE ASSET_ID = D.ASSET_ID
                                 AND DEPRN_EXPENSE_CCID IS NOT NULL
                                 AND DEPRN_RESERVE_CCID IS NOT NULL 
                                 and PERIOD_COUNTER <= LP_PERIOD_COUNTER - 1
                                 )) DD,
            (SELECT MAX(FAR.DATE_RETIRED ) DATE_RETIRED,FAR.ASSET_ID 
             FROM FA_RETIREMENTS           FAR               
             WHERE  FAR.STATUS = 'PROCESSED' 
             GROUP BY FAR.ASSET_ID) RET
            
            WHERE YTD.DEPRN_SOURCE_CODE = 'DEPRN'            
            AND YTD.ASSET_ID = DD.ASSET_ID(+)
            and YTD.BOOK_TYPE_CODE = DD.BOOK_TYPE_CODE(+)  
            AND YTD.ASSET_ID = RET.ASSET_ID(+) 
                               
            AND YTD.PERIOD_COUNTER = (SELECT MAX(PERIOD_COUNTER) FROM FA_DEPRN_SUMMARY
                                  WHERE ASSET_ID = YTD.ASSET_ID --12413324
                                  AND DEPRN_SOURCE_CODE = 'DEPRN'
                                  and PERIOD_COUNTER <= LP_PERIOD_COUNTER - 1
                                  )
                                  ) FDS_LAST
                                  
            
   
WHERE  FAB.ASSET_ID = FAT.ASSET_ID
AND FAB.ASSET_ID = FB.ASSET_ID
AND FAB.ASSET_ID = FAI.ASSET_ID(+)
AND FAH.ASSET_ID = FAB.ASSET_ID
AND FAH.CATEGORY_ID = FCBB.CATEGORY_ID
AND FAB.ASSET_CATEGORY_ID = FAH.CATEGORY_ID
--KTP 
--AND FAB.ATTRIBUTE_CATEGORY_CODE = FCCB.CATE_CODE
AND FCCB.CATEGORY_ID = FCBB.CATEGORY_ID
AND FB.BOOK_TYPE_CODE  =  FCBB.BOOK_TYPE_CODE 
AND FAB.ASSET_ID = FDH.ASSET_ID
AND FDH.LOCATION_ID = FL.LOCATION_ID
AND PAPF.PERSON_ID(+)  = FDH.ASSIGNED_TO 
AND FB.TRANSACTION_HEADER_ID_IN = ( SELECT MAX(TRANSACTION_HEADER_ID_IN) 
                                    FROM APPS.FA_BOOKS FB1 
                                    WHERE FB1.ASSET_ID=FB.ASSET_ID 
                                    AND FB1.BOOK_TYPE_CODE = FB.BOOK_TYPE_CODE 
                                    )
 -----------------------------------------------------------------------------
 AND FDH.DATE_INEFFECTIVE IS NULL   
 AND FDH.CODE_COMBINATION_ID = DHCC.CODE_COMBINATION_ID

 -----------------------------------------------------------------------------
 AND FB.ASSET_ID = FDS.ASSET_ID(+)
 AND FB.BOOK_TYPE_CODE = FDS.BOOK_TYPE_CODE(+)

-------KTP-------
 AND FB.ASSET_ID = FDS_LAST.ASSET_ID(+) 
 AND FB.BOOK_TYPE_CODE = FDS_LAST.BOOK_TYPE_CODE(+) 


---------PARAMETER---------------------------
and nvl(TO_DATE(FAB.ATTRIBUTE9,'YYYY/MM/DD HH24:MI:SS'),FAB.CREATION_DATE) <= LAST_DAY(to_date(LP_PERIOD,'MON-YY'))
AND FB.BOOK_TYPE_CODE   =  P_BOOK
--KTP
AND (FAI.PO_NUMBER LIKE P_PO || '%' OR P_PO IS NULL )
AND ( FAI.PROJECT_NUM LIKE P_PROJECT || '%' OR P_PROJECT IS NULL )
AND (fccb.cate_code LIKE P_CATEGORY || '%' OR P_CATEGORY   IS NULL )
AND (FL.ATTRIBUTE1 LIKE P_LOCATION || '%' OR P_LOCATION   IS NULL )
AND (FDH.ASSIGNED_TO  =  P_OWNER OR  P_OWNER IS NULL)

ORDER BY FAB.ASSET_NUMBER;



LP_PERIOD_COUNTER number;
LP_PERIOD  varchar2(20);
LP_REQUEST_ID NUMBER;
V_USEFUL_LIFE NUMBER;
V_DATE_STRAT_LIFE VARCHAR2(20);
V_DATE_END_LIFE VARCHAR2(20);
V_USE_LIFT_LAST  NUMBER;
V_START_DATE_LAST  VARCHAR2(20);
V_END_DATE_LAST  VARCHAR2(20);
V_AMOUNT_LAST  NUMBER;
V_LOC_NAME VARCHAR2(200);
V_VENDOR_NAME VARCHAR2(240);
V_DEPRE NUMBER;
V_DEPRN_LAST NUMBER;
V_WRITEOFF_DATE DATE;


                         
 BEGIN
 
    LP_PERIOD_COUNTER := null;
    LP_PERIOD := null;
    LP_REQUEST_ID := NULL;
    
    
    LP_REQUEST_ID := fnd_global.conc_request_id;
 
    
        if P_PERIOD is not null then
           
          begin
            select period_counter
            into  LP_PERIOD_COUNTER
            from   FA_DEPRN_PERIODS
            where  book_type_code = P_BOOK
            and   period_name    = P_PERIOD;
    
            LP_PERIOD := P_PERIOD;
          exception when others then
            LP_PERIOD_COUNTER := null;
            LP_PERIOD := null;
          end;
  
        ELSE
		  begin
            select period_name
            into  LP_PERIOD
            from   FA_DEPRN_PERIODS
            where  book_type_code = P_BOOK
            AND period_counter = (SELECT MAX(period_counter)
                                from   FA_DEPRN_PERIODS
                                where  book_type_code = P_BOOK);
                                
          exception when others then
            LP_PERIOD := null;
          end;                     
          
          begin                
            select period_counter
            into  LP_PERIOD_COUNTER
            from   FA_DEPRN_PERIODS
            where  book_type_code = P_BOOK
            and   period_name    = LP_PERIOD;
          exception when others then
            LP_PERIOD_COUNTER := null;
          end;
                          
   
        end if;
        
     WRITE_LOG('TAC Asset Listing Report','OUT');  
     WRITE_LOG('Request ID : ' || LP_REQUEST_ID,'OUT');
     WRITE_LOG('Date/Time : '|| TO_CHAR(sysdate,'DD/Mon/YYYY'),'OUT');
     WRITE_LOG('Book :  ' || P_BOOK,'OUT');
     WRITE_LOG('Period : ' || LP_PERIOD,'OUT');
     
   		 					
     WRITE_LOG('','OUT');
     WRITE_LOG('Date Registration' || '|' ||'Asset Number' || '|' ||'Tag Number' || '|' ||'Company' || '|' ||'Project' || '|' ||'PO No.' 
               || '|' ||'PO Line' || '|' ||'BOQ Item' || '|' ||'QTY PO/BOQ Register' || '|' ||'QTY Real' || '|' ||'Amount (THB)' 
               || '|' ||'Vendor' || '|' ||'Location' || '|' ||'Location Name' || '|' || 'Identification' || '|' ||'Asset Part Generic Name'
               || '|' ||'FA Category'|| '|' ||'FA Category Description'|| '|' ||'Brand'|| '|' ||'Model'|| '|' ||'Serial'|| '|' ||'Owner'
               || '|' ||'CIM'|| '|' ||'Useful life'|| '|' ||'Date Start of Useful life'|| '|' ||'Date End of Useful Life'|| '|' ||'Depre'
               || '|' ||'YTD Depre'|| '|' ||'Depre Accum'|| '|' ||'Account Code for Asset'|| '|' ||'Account Code for Accum Depre'
               || '|' ||'Account Code  for Depre'|| '|' ||'Disposal or Write off Date'|| '|' ||'Remark'|| '|' ||'Useful life Last Period'
               || '|' ||'Useful life Start date Last Period'|| '|' ||'Useful Life End Date Last Period'|| '|' ||'Amount(THB) Last Period'
               || '|' ||'Depre  Last Period'|| '|' ||'Depre Accum Last Period'|| '|' ||'Account FA Last Period'
               || '|' ||'Account Accum Depre Last Period'|| '|' ||'Account Depre Last Period','OUT');
        
        
        
   FOR REC1 IN C1(LP_PERIOD,LP_PERIOD_COUNTER)
   LOOP
   
               CF_ADJUST(P_PERIOD => LP_PERIOD,
                         P_ASSET_ID => REC1.ASSET_ID,
                         O_USEFUL_LIFE => V_USEFUL_LIFE,
                         O_DATE_STRAT_LIFE => V_DATE_STRAT_LIFE,
                         O_DATE_END_LIFE => V_DATE_END_LIFE
                         );
                         
                         
              CF_ADJUST_LAST(P_PERIOD => LP_PERIOD,
                         P_ASSET_ID => REC1.ASSET_ID,
                         O_USE_LIFT_LAST => V_USE_LIFT_LAST,
                         O_START_DATE_LAST => V_START_DATE_LAST,
                         O_END_DATE_LAST => V_END_DATE_LAST,
                         O_AMOUNT_LAST => V_AMOUNT_LAST
                         );
                         
                         
                         
        begin
	
            v_loc_name := '';
	 					
            
            SELECT DISTINCT replace(replace(ffvt.DESCRIPTION,CHR(10),''),CHR(9),'') LOC_NAME
            INTO V_LOC_NAME
            FROM fnd_flex_value_sets fvs,
                fnd_flex_values fv,
                fnd_flex_values_tl ffvt,
                FA_LOCATIONS FL
            WHERE     fvs.FLEX_VALUE_SET_ID = fv.FLEX_VALUE_SET_ID
                AND fv.flex_value_id = ffvt.flex_value_id
                AND fvs.FLEX_VALUE_SET_NAME LIKE '%TAC_FA_LOCATION_NAME%'
                AND fv.PARENT_FLEX_VALUE_LOW = FL.SEGMENT1
                AND fv.flex_value = FL.SEGMENT2
                AND FL.LOCATION_ID = REC1.LOCATION_ID;     
        EXCEPTION WHEN OTHERS THEN
             NULL;
        end;
        
        
        begin
	
            v_vendor_name := '';
	
            SELECT DISTINCT  PV.VENDOR_NAME
            INTO V_VENDOR_NAME
            FROM APPS.PO_VENDORS PV,po_headers_all p 
            WHERE PV.VENDOR_ID  = P.VENDOR_ID 
            and p.segment1 = rec1.po_number
            ;
  
  
        EXCEPTION WHEN OTHERS THEN
             NULL;
        end;
        
        
        V_DEPRE := null;
        begin 
            SELECT sum(DEPRN_AMOUNT)
            INTO V_DEPRE
            FROM FA_DEPRN_SUMMARY 
            WHERE ASSET_ID = REC1.ASSET_ID
            AND BOOK_TYPE_CODE = REC1.BOOK_TYPE_CODE
            AND DEPRN_SOURCE_CODE = 'DEPRN'
            AND  PERIOD_COUNTER = LP_PERIOD_COUNTER 
            ;
        exception when others then 
            null;
        end; 
  
  
        V_DEPRN_LAST := null;
        begin       
            SELECT sum(DEPRN_AMOUNT)
            INTO V_DEPRN_LAST
            FROM FA_DEPRN_SUMMARY 
            WHERE ASSET_ID = REC1.ASSET_ID
            AND BOOK_TYPE_CODE = REC1.BOOK_TYPE_CODE
            AND DEPRN_SOURCE_CODE = 'DEPRN'
            AND  PERIOD_COUNTER = LP_PERIOD_COUNTER -1
            ; 
  
        exception when others then 
            null;
        end;
   
  
        V_WRITEOFF_DATE := null;
        begin
            SELECT MAX(FAR.DATE_RETIRED) 
            into V_WRITEOFF_DATE
            FROM FA_RETIREMENTS           FAR   
            WHERE FAR.ASSET_ID = REC1.ASSET_ID
            AND FAR.STATUS = 'PROCESSED' 
            ;
        exception when others then 
            null;
        end;
        
        
   


      WRITE_LOG(rec1.DATE_REGIST || '|' ||rec1.ASSET_NUMBER|| '|' ||rec1.TAG_NUMBER || '|' ||rec1.ACC_COMPANY || '|' ||rec1.PROJECT_NUM 
      || '|' ||rec1.PO_NUMBER|| '|' ||rec1.PO_LINE || '|' ||rec1.BOQ_ITEM || '|' ||rec1.QTY_PO || '|' ||'' || '|' ||rec1.AMOUNT_THB 
               || '|' ||v_vendor_name || '|' ||rec1.Loc || '|' ||v_Loc_Name || '|' || rec1.Identification || '|' ||rec1.ASSET_GENERIC
               || '|' ||rec1.fa_cat|| '|' ||rec1.CAT_DESC|| '|' ||rec1.Brand|| '|' ||rec1.MODEL_NUMBER|| '|' ||rec1.Serial|| '|' ||rec1.Owner
               || '|' ||rec1.CIM|| '|' ||v_USEFUL_LIFE|| '|' ||v_DATE_STRAT_LIFE|| '|' ||v_DATE_END_LIFE|| '|' ||v_Depre
               || '|' ||rec1.YTD_Deprn|| '|' ||rec1.DEPRN_ACCUM|| '|' ||rec1.ACC_ASSET|| '|' ||rec1.ACC_ACCUM
               || '|' ||rec1.ACC_DEPRE|| '|' ||v_WRITEOFF_DATE|| '|' ||rec1.F_Remark|| '|' ||v_USE_LIFT_LAST
               || '|' ||v_START_DATE_LAST|| '|' ||v_END_DATE_LAST|| '|' ||v_AMOUNT_LAST
               || '|' ||v_DEPRN_LAST|| '|' ||rec1.DEPRN_ACCUM_LAST|| '|' ||rec1.ACC_ASSET_LAST
               || '|' ||rec1.ACC_ACCUM_LAST|| '|' ||rec1.ACC_DEPRE_LAST,'OUT');    
   
   END LOOP;

 EXCEPTION WHEN OTHERS THEN
    NULL;
 END;
 
 
end TAC_ASSET_LISTING_PKG;
/