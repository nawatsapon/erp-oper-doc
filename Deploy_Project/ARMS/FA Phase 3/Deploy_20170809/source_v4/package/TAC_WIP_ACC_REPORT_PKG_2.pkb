CREATE OR REPLACE PACKAGE BODY APPS.TAC_WIP_ACC_REPORT_PKG is

PROCEDURE WRITE_LOG (pCHAR IN VARCHAR2, pTYPE IN VARCHAR2)
   IS
   BEGIN
      IF UPPER (pTYPE) = 'LOG'
      THEN
         fnd_file.put_LINE (FND_FILE.LOG, pCHAR);
      ELSE
         fnd_file.put_line (FND_FILE.OUTPUT, pCHAR);
      END IF;

      DBMS_OUTPUT.put_line (Pchar);
   exception when others then
       DBMS_OUTPUT.put_line (sqlerrm);
   END WRITE_LOG;
   
FUNCTION check_wip_accrued_report(p_project_id IN NUMBER,
                                  p_po_header_id IN NUMBER,
                                  p_po_line_id IN NUMBER,
                                  p_rate number  ) RETURN varchar2
is

v_po_no varchar2(30);
v_po_line_no varchar2(30);
v_rec_amount number;
v_fa_cost number;
v_report varchar2(20);
begin

    begin

        select poh.segment1,pol.line_num
        into v_po_no, v_po_line_no
        from po_headers_all poh,
        po_lines_all pol
        where poh.po_header_id = pol.po_header_id
        and poh.po_header_id = p_po_header_id
        and pol.po_line_id = p_po_line_id; 

    exception when others then
        v_po_no := null;
        v_po_line_no := null;
    end;
    
    
    begin    
        SELECT sum( NVL(PLL.QUANTITY_RECEIVED,0) * NVL(P_RATE,1)) AMOUNT
        into v_rec_amount
        FROM     PO_LINE_LOCATIONS_ALL PLL,
        PO_DISTRIBUTIONS_all pod
        WHERE PLL.LINE_LOCATION_ID  = POD.LINE_LOCATION_ID 
        and pod.project_id = p_project_id
        and pll.po_header_id = p_po_header_id
        and pll.po_line_id = p_po_line_id;
    exception when others then
        v_rec_amount := 0;
    end ;
    
    
    begin
        SELECT ROUND(SUM(fb.cost),2) REGIS_AMT 
        INTO v_fa_cost
        FROM FA_ASSET_INVOICES ai,
        FA_BOOKS fb,
        fa_additions_b fab
        WHERE ai.asset_id = fb.asset_id
        and ai.asset_id = fab.asset_id
        and fb.DATE_INEFFECTIVE is null
        and ai.PROJECT_ID = P_PROJECT_ID
        and ai.PO_NUMBER = V_PO_NO
        and fab.ATTRIBUTE5 = V_PO_LINE_NO
        --and ai.ATTRIBUTE1 = V_PO_LINE_NO
        AND UPPER(fb.BOOK_TYPE_CODE) NOT LIKE '%TAX%'
        ;
    exception when others then
       v_fa_cost := null;
    end;
    
    if nvl(v_rec_amount,0) < nvl(v_fa_cost,0) then
       v_report := 'FA009';
    
    elsif v_fa_cost is null then
       v_report := 'FA008';
    end if;
    
      
    return v_report;


end;
FUNCTION CHECK_POADD_RP(P_PO_NUMBER IN VARCHAR2,
                        P_PO_LINE IN NUMBER,
                        P_PERIOD IN VARCHAR2
                         ) RETURN varchar2
is

V_DISPLAY VARCHAR2(1);


BEGIN

V_DISPLAY := null;

    BEGIN
        SELECT  distinct DISPLAY_REPORT
        INTO V_DISPLAY
        FROM TAC_PO_ADDITION_INFO
        WHERE PO_NUMBER = P_PO_NUMBER
        AND PO_LINE_NUMBER = P_PO_LINE
        AND LAST_DAY(TO_DATE(PERIOD_NAME,'MON-YY')) = LAST_DAY(TO_DATE(P_PERIOD,'MON-YY')) 
        ;
        /*AND LAST_UPDATE_DATE = (SELECT MAX(LAST_UPDATE_DATE)
                        FROM TAC_PO_ADDITION_INFO
                        WHERE PO_NUMBER = P_PO_NUMBER
                        AND PO_LINE_NUMBER = P_PO_LINE);*/
    EXCEPTION WHEN OTHERS THEN
       V_DISPLAY := NULL;
    END;
/*     
IF (V_DISPLAY = 'Y')  THEN
   RETURN 'Y';
ELSE
   RETURN 'N';
END IF;
*/
    RETURN V_DISPLAY;
EXCEPTION WHEN OTHERS THEN
    RETURN 'N';
END;

FUNCTION CHECK_DISPLAY_PROJECT(P_PO_NUMBER IN VARCHAR2,
                        P_PO_LINE IN VARCHAR2,
                        P_PERIOD IN VARCHAR2
                         ) RETURN varchar2
                         
is

V_DISPLAY VARCHAR2(1);


BEGIN

V_DISPLAY := null;

    BEGIN
        SELECT distinct DISPLAY_PROJECT
        INTO V_DISPLAY
        FROM TAC_PO_ADDITION_INFO
        WHERE PO_NUMBER = P_PO_NUMBER
        AND PO_LINE_NUMBER = P_PO_LINE
        AND LAST_DAY(TO_DATE(PERIOD_NAME,'MON-YY')) = LAST_DAY(TO_DATE(P_PERIOD,'MON-YY')) 
        ;
        /*AND LAST_UPDATE_DATE = (SELECT MAX(LAST_UPDATE_DATE)
                        FROM TAC_PO_ADDITION_INFO
                        WHERE PO_NUMBER = P_PO_NUMBER
                        AND PO_LINE_NUMBER = P_PO_LINE);*/
    EXCEPTION WHEN OTHERS THEN
       V_DISPLAY := NULL;
    END;

    RETURN V_DISPLAY;
EXCEPTION WHEN OTHERS THEN
    RETURN 'N';
END;
                         
FUNCTION CHECK_FULL_RECEIVE(P_PO_HEADER_ID IN NUMBER,
                        P_PO_LINE_ID IN NUMBER
                         ) RETURN varchar2
IS

V_RECEIVE_AMT VARCHAR2(1);

V_QUANTITY number;
v_QUANTITY_CANCELLED number;
v_QUANTITY_BILLED number;
v_QUANTITY_RECEIVED number;
                        
BEGIN

    V_RECEIVE_AMT := 'N';    
                  
    BEGIN
       /* SELECT 'Y'
        INTO V_RECEIVE_AMT
        FROM     PO_LINE_LOCATIONS_ALL PLL,
        PO_DISTRIBUTIONS_all pod
        WHERE pll.po_header_id = p_po_header_id
        and pll.po_line_id = p_po_line_id
        GROUP BY   PLL.PO_HEADER_ID,pll.po_line_id
                 HAVING   NVL (SUM (PLL.QUANTITY), 0)
                          - NVL (SUM (PLL.QUANTITY_CANCELLED), 0) =
                             NVL (SUM (PLL.QUANTITY_BILLED), 0);*/
                             
     SELECT SUM (PLL.QUANTITY)
            ,SUM (PLL.QUANTITY_CANCELLED)
            ,SUM (PLL.QUANTITY_BILLED)
            ,sum(pll.QUANTITY_RECEIVED)
        INTO V_QUANTITY
        ,v_QUANTITY_CANCELLED
        ,v_QUANTITY_BILLED
        ,v_QUANTITY_RECEIVED
        FROM     PO_LINE_LOCATIONS_ALL PLL,
        PO_DISTRIBUTIONS_all pod
        WHERE pll.po_header_id = pod.po_header_id
        and pll.po_line_id = pod.po_line_id
        and pll.po_header_id = p_po_header_id
        and pll.po_line_id = p_po_line_id
        GROUP BY   PLL.PO_HEADER_ID,pll.po_line_id
        ;
    EXCEPTION WHEN OTHERS THEN
        V_QUANTITY := 0;
        v_QUANTITY_CANCELLED := 0;
        v_QUANTITY_BILLED := 0;
        v_QUANTITY_RECEIVED := 0;
    END; 
    
    
    if nvl(V_QUANTITY,0) - nvl(v_QUANTITY_CANCELLED,0) = nvl(v_QUANTITY_RECEIVED,0) then
      RETURN 'Y';
    else
      RETURN 'N';
    end if; 

EXCEPTION WHEN OTHERS THEN
    RETURN 'N';
END;

FUNCTION GET_LAST_RECEIVE_DATE(P_PO_HEADER_ID NUMBER,
                               P_PO_LINE_ID NUMBER) RETURN DATE
                               
IS
V_RCV_DATE DATE;
BEGIN

select MAX(RT.TRANSACTION_DATE) RCV_DATE
INTO V_RCV_DATE
from RCV_SHIPMENT_HEADERS RSH,
RCV_SHIPMENT_LINES RSL,
RCV_TRANSACTIONS RT
where RSH.SHIPMENT_HEADER_ID = RSL.SHIPMENT_HEADER_ID
AND RSH.SHIPMENT_HEADER_ID = RT.SHIPMENT_HEADER_ID
AND RSL.SHIPMENT_LINE_ID = RT.SHIPMENT_LINE_ID
and RT.transaction_type = 'RECEIVE'
AND RSL.PO_HEADER_ID = P_PO_HEADER_ID
AND RSL.PO_LINE_ID = P_PO_LINE_ID
;

RETURN V_RCV_DATE;

EXCEPTION WHEN OTHERS THEN
   RETURN NULL;
END;


procedure gen_wip_aging (errbuf               OUT VARCHAR2,
                         retcode              OUT NUMBER,
                         PIN_REQUEST_ID IN NUMBER,
                         P_ORG_ID IN NUMBER,
                         P_PROJECT_NO IN VARCHAR2,
                         P_PO_NO_ID IN NUMBER,
                         P_PO_LINE IN NUMBER,
                         PIN_PERIOD IN VARCHAR2 )
                    
 IS
 
cursor c1(pPERIOD VARCHAR2) is
select distinct --FV.DESCRIPTION COMPANY,
(SELECT ou.NAME
            FROM hr_operating_units  ou
            WHERE organization_id = POH.ORG_ID )  COMPANY ,
PA.PROJECT_ID,
PA.SEGMENT1 PROJECT_CODE,
PA.NAME PROJECT_NAME,
POH.SEGMENT1 PO_NO,
TAC_FA_REPORT_PKG.GET_RELEASE_NUM(PLL.PO_RELEASE_ID) release_no,
POL.LINE_NUM PO_LINE_NO
,PLL.PO_RELEASE_ID
,poh.org_id
,poh.po_header_id
,pol.po_line_id
,NVL(POL.UNIT_PRICE,0) UNIT_PRICE
,NVL(POH.RATE,1)  RATE
/*,TAC_WIP_ACC_REPORT_PKG.GET_LAST_RECEIVE_DATE(POH.PO_HEADER_ID,
                       POL.PO_LINE_ID) LAST_RECEIVE_DATE*/
--,pol.attribute1 type_of_work
,NVL(TAC_FA_REPORT_PKG.GET_TYPE_OF_WORK(pod.REQ_DISTRIBUTION_ID,POH.SEGMENT1,POL.LINE_NUM),'U') TYPE_OF_WORK 
,CASE WHEN upper(NVL(TAC_FA_REPORT_PKG.GET_TYPE_OF_WORK(pod.REQ_DISTRIBUTION_ID,POH.SEGMENT1,POL.LINE_NUM),'U')) IN ('E','S','U')  THEN 'WIP'
      WHEN upper(NVL(TAC_FA_REPORT_PKG.GET_TYPE_OF_WORK(pod.REQ_DISTRIBUTION_ID,POH.SEGMENT1,POL.LINE_NUM),'U')) IN ('T') THEN 'PREPAID'
 END WIP_PREPAID


from po_headers_ALL poh,
PO_LINES_ALL POL,
PO_LINE_LOCATIONS_ALL  PLL,
PO_DISTRIBUTIONS_all pod,
/*GL_CODE_COMBINATIONS_KFV GLCC,
FND_FLEX_VALUE_SETS FS, 
FND_FLEX_VALUES_VL FV,*/
PA_PROJECTS_ALL PA

WHERE 1=1
AND POH.PO_HEADER_ID = POD.PO_HEADER_ID
AND POH.PO_HEADER_ID = POL.PO_HEADER_ID
AND POL.PO_LINE_ID = POD.PO_LINE_ID
AND POH.PO_HEADER_ID = PLL.PO_HEADER_ID
AND POL.PO_LINE_ID = PLL.PO_LINE_ID
and pod.LINE_LOCATION_ID = pll.LINE_LOCATION_ID

----AND POD.CODE_COMBINATION_ID = GLCC.CODE_COMBINATION_ID
/*AND POD.BUDGET_ACCOUNT_ID = GLCC.CODE_COMBINATION_ID
AND FV.FLEX_VALUE_MEANING = GLCC.SEGMENT1
AND FS.FLEX_VALUE_SET_ID = FV.FLEX_VALUE_SET_ID
AND FS.FLEX_VALUE_SET_NAME = 'TAC_COA_LEGAL_ENTITY'*/
AND POD.PROJECT_ID = PA.PROJECT_ID(+)
AND POH.AUTHORIZATION_STATUS IN ('APPROVED')
AND NVL(POL.CANCEL_FLAG,'N') != 'Y' 
                                                               
AND (TAC_FA_REPORT_PKG.GET_ACCOUNT_WIP(POH.PO_HEADER_ID,POL.PO_LINE_ID,POD.PROJECT_ID) --IS NOT NULL
    IN (select lookup_code from fnd_lookup_values Where lookup_type = 'TAC_DTN_ASSET_REP_ACCOUNT')
                                                               
     OR TAC_FA_REPORT_PKG.CHECK_POADD_RP(POH.SEGMENT1|| TAC_FA_REPORT_PKG.GET_RELEASE_NUM(PLL.PO_RELEASE_ID),
                                     POL.LINE_NUM) = 'Y'
                                     )
 AND NVL(TAC_FA_REPORT_PKG.CHECK_POADD_RP(POH.SEGMENT1|| TAC_FA_REPORT_PKG.GET_RELEASE_NUM(PLL.PO_RELEASE_ID),
                                     POL.LINE_NUM),'Y') != 'N'
 AND  NVL(TAC_FA_REPORT_PKG.CHECK_DISPLAY_PROJECT(POH.SEGMENT1|| TAC_FA_REPORT_PKG.GET_RELEASE_NUM(PLL.PO_RELEASE_ID),POL.LINE_NUM),'Y') != 'N'
                                     
  /*AND TAC_WIP_ACC_REPORT_PKG.GET_AMOUNT(POH.SEGMENT1,
                              POL.LINE_NUM,
                              NVL(TAC_FA_REPORT_PKG.GET_TYPE_OF_WORK(pod.REQ_DISTRIBUTION_ID,POH.SEGMENT1,POL.LINE_NUM),'U'),
                              'FA008',
                              P_PERIOD) > 0
                              */
                              

     
--=== parameter ===--
AND POH.ORG_ID = P_ORG_ID
/*AND (PA.SEGMENT1 = P_PROJECT_NO
     or P_PROJECT_NO is null
     or PA.SEGMENT1 is null)*/
and case when nvl(P_PROJECT_NO,'X') != 'X' and PA.SEGMENT1 = P_PROJECT_NO then 1
         when nvl(P_PROJECT_NO,'X') = 'X' and POD.PROJECT_ID is null then 1
         when nvl(P_PROJECT_NO,'X') = 'X' then 1
         end = 1
AND POH.PO_HEADER_ID= NVL(P_PO_NO_ID,POH.PO_HEADER_ID)
AND POL.LINE_NUM = NVL(P_PO_LINE,POL.LINE_NUM)

--AND POH.po_header_id=316786 
--AND POH.SEGMENT1 = '8017300026' --'991017001740'
--AND POL.LINE_NUM = 2

ORDER BY PA.SEGMENT1 ,
POH.SEGMENT1,
POL.LINE_NUM

;


CURSOR C_AMT(P_PONO VARCHAR2,P_POLINE NUMBER,P_REQUEST_ID NUMBER) IS
SELECT PO_NUMBER,PO_LINE
    ,AMT_TYPE
    ,nvl(RCV_DATE,sysdate) RCV_DATE
    ,SUM(AMOUNT) AMOUNT
     FROM TAC_WIP_AGING
     WHERE PO_NUMBER = P_PONO
     AND PO_LINE = P_POLINE
     AND REQUEST_ID = P_REQUEST_ID
     GROUP BY PO_NUMBER,PO_LINE
     ,AMT_TYPE
     ,RCV_DATE
     HAVING SUM(AMOUNT) != 0 
     ORDER BY AMT_TYPE,RCV_DATE DESC;
     
     
     
cursor C_GL1(p_pono varchar2,p_poline number)
IS 
select  aid.invoice_id,aid.distribution_line_number
        from ap_invoice_distributions_ALL aid
        where ATTRIBUTE1 = p_pono
        and ATTRIBUTE2 = p_poline;
        
  
cursor C_GL2(p_pono varchar2,p_poline number)
IS       
 SELECT distinct aid.INVOICE_ID,aid.po_distribution_id,aid.DISTRIBUTION_LINE_NUMBER
     FROM ap_invoice_distributions_ALL aid,
     PO_DISTRIBUTIONS_all pod,
     po_headers_all poh,
     po_lines_all pol
     where POD.PO_DISTRIBUTION_ID = AID.PO_DISTRIBUTION_ID
     and pod.po_header_id = poh.po_header_id
     and pod.po_line_id = pol.po_line_id
     and pol.po_header_id = poh.po_header_id
     and poh.segment1 = p_pono
     and pol.line_num = p_poline
     and aid.po_distribution_id is not null;
     
 cursor C_CLOUD(p_pono varchar2,p_poline number,pin_period varchar2)
 IS      
  SELECT PO_NUMBER
              ,PO_LINE_NUMBER
              ,last_day(to_date(PERIOD_NAME,'MON-YY')) REGIST_DATE
              ,SUM(CLOUD_VALUE_REGISTER) REGIST_AMT
      FROM TAC_PO_CLOUD_INFO
      WHERE PO_NUMBER = p_pono
      AND PO_LINE_NUMBER = p_poline
      and last_day(to_date(PERIOD_NAME,'MON-YY')) <= last_day(to_date(pin_PERIOD,'MON-YY'))
      GROUP BY PO_NUMBER,PO_LINE_NUMBER,last_day(to_date(PERIOD_NAME,'MON-YY'))
      order by last_day(to_date(PERIOD_NAME,'MON-YY'))
      ;
      

P_REQUEST_ID NUMBER;
V_CUTOFF_DATE  DATE;
V_GRN_AMT NUMBER;
V_REGIS_AMT NUMBER;
V_PROGRESS_AMT NUMBER;
V_BOOKING_AMT NUMBER;
V_AMOUNT NUMBER;

V_AGING NUMBER;
V_AG1 NUMBER;
V_AG2 NUMBER;
V_AG3 NUMBER;
V_AG4 NUMBER;
V_AG5 NUMBER;
V_AG6 NUMBER;
V_AG7 NUMBER;
V_AG_UNDEFY NUMBER;
V_LAST_DATE DATE;
V_AMT_AG NUMBER;
V_AMT_AGING NUMBER;
V_RECAMT  NUMBER;

V_CHECK VARCHAR2(1);



V_PO_NUMBER VARCHAR2(30);
V_PO_LINE NUMBER;
V_AMT_TYPE VARCHAR2(20);
P_PERIOD VARCHAR2(20);

V_MM_AMT NUMBER;
V_CURRENT_AMT NUMBER;
i number;





 
 BEGIN
 
    P_REQUEST_ID := nvl(PIN_REQUEST_ID,fnd_global.conc_request_id);
 
    -- write_log('step 1 - ' || to_char(sysdate,'dd/mm/yy hh24:mi:ss'),'log');
     P_PERIOD := NVL(PIN_PERIOD,to_char(sysdate,'MON-YY'));
 
      BEGIN
        select TO_DATE(MEANING,'DD-MON-YYYY') CUTOFF_DATE
        INTO V_CUTOFF_DATE
        from fnd_lookup_values 
        Where lookup_type = 'TAC_DTN_CUTOFF'
        AND LOOKUP_CODE = P_PERIOD;
    EXCEPTION WHEN OTHERS THEN
        V_CUTOFF_DATE := LAST_DAY(SYSDATE-30);
    END;
    
    
    DELETE FROM TAC_WIP_AGING
    WHERE nvl(REQUEST_ID,0) < 100;
   --  COMMIT; -- comment by Joe
    DELETE FROM TAC_WIP_AGING_TEMP
    WHERE nvl(REQUEST_ID,0) < 100;
    COMMIT;
    
    
    /* WRITE_LOG('Request ID : ' || P_REQUEST_ID,'OUT');
     WRITE_LOG('Date/Time: '|| sysdate,'OUT');
     WRITE_LOG('','OUT');
     WRITE_LOG('Company' || '|' ||'Project Code' || '|' ||'Project Name' || '|' ||'PO No.' || '|' ||'Line No.' || '|' ||'WIP/Prepaid' || '|' ||
               '<= 1 Month' || '|' ||'1-6 Months' || '|' ||'6-12 Months' || '|' ||'12-24 Months' || '|' ||'24-36 Months' || '|' ||
               '36-48 Months' || '|' ||'> 48 Months' || '|' || 'Unidentified Aging' || '|' ||'Grand Total','OUT');
    
    write_log('step 2 - ' || to_char(sysdate,'dd/mm/yy hh24:mi:ss'),'log');
    */
    FOR REC1 IN C1 (P_PERIOD)
    LOOP
    
    DELETE FROM TAC_WIP_AGING
    WHERE nvl(REQUEST_ID,0) = P_REQUEST_ID;
    commit;
    
     
    -- write_log('step 3 - ' || to_char(sysdate,'dd/mm/yy hh24:mi:ss'),'log');
     BEGIN
        INSERT INTO TAC_WIP_AGING(PO_NUMBER,PO_LINE,RCV_DATE,AMOUNT,AMT_TYPE,REQUEST_ID)
        SELECT PO_NUMBER
        ,PO_LINE_NUMBER
        ,LAST_DAY(TO_DATE(PERIOD_NAME,'MON-YY')) RCV_DATE
        ,SUM(GRN_ADJUST) GRN_AMT
        ,'GRN'
        ,P_REQUEST_ID
        FROM TAC_PO_ADDITION_INFO
        WHERE PO_NUMBER = REC1.PO_NO||REC1.RELEASE_NO
        AND PO_LINE_NUMBER = REC1.PO_LINE_NO
        AND LAST_DAY(TO_DATE(PERIOD_NAME,'MON-YY')) <= LAST_DAY(TO_DATE(P_PERIOD,'MON-YY')) 
        GROUP BY PO_NUMBER,PO_LINE_NUMBER,LAST_DAY(TO_DATE(PERIOD_NAME,'MON-YY'));
        --COMMIT;  --comment by joe
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END;
     COMMIT;  --Add by Joe
        

    -- write_log('step 4 - ' || to_char(sysdate,'dd/mm/yy hh24:mi:ss'),'log');
     BEGIN
        INSERT INTO TAC_WIP_AGING(PO_NUMBER,PO_LINE,RCV_DATE,AMOUNT,AMT_TYPE,REQUEST_ID)
        SELECT ATTRIBUTE1
              ,ATTRIBUTE2
              ,TRUNC(ACCOUNTING_DATE) RCV_DATE
              ,SUM(AMOUNT) AMOUNT
              ,'GRN'
              ,P_REQUEST_ID
            FROM AP_INVOICE_DISTRIBUTIONS_all
            WHERE ATTRIBUTE1 = REC1.PO_NO||REC1.RELEASE_NO
            AND ATTRIBUTE2 = REC1.PO_LINE_NO
            AND TRUNC(ACCOUNTING_DATE) <= TRUNC(V_CUTOFF_DATE)
            group by ATTRIBUTE1 ,
            ATTRIBUTE2,
            TRUNC(ACCOUNTING_DATE);
            COMMIT;
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END;
     
     
     for rec_gl1 in c_gl1(REC1.PO_NO||REC1.RELEASE_NO,REC1.PO_LINE_NO)
     loop 
     
    -- write_log('step 5 - ' || to_char(sysdate,'dd/mm/yy hh24:mi:ss'),'log');
     BEGIN
        INSERT INTO TAC_WIP_AGING(PO_NUMBER,PO_LINE,RCV_DATE,AMOUNT,AMT_TYPE,REQUEST_ID)
        select  
        REC1.PO_NO||REC1.RELEASE_NO,REC1.PO_LINE_NO
        --AID.ATTRIBUTE1,AID.ATTRIBUTE2
        ,TRUNC(JL.EFFECTIVE_DATE) RCV_DATE
        ,sum(NVL(jl.accounted_dr,0)-NVL(jl.accounted_cr,0)) grn_amt
        ,'GRN'
        ,P_REQUEST_ID
        from  GL_JE_LINES        jl
         ,GL_JE_HEADERS     jh
         ,GL_CODE_COMBINATIONS_KFV GLCC
         ,GL_IMPORT_REFERENCES xla        

        where  jl.status             = 'P'
        and jl.code_combination_id     = GLCC.code_combination_id
        and jh.status                = 'P' 
        and jh.actual_flag           = 'A'
        and jh.je_header_id          = jl.je_header_id
        and jh.je_source             = 'Payables' 
        and jh.je_category           = 'Purchase Invoices'
        and jl.effective_date        is not null   
        and xla.je_header_id         = jl.je_header_id  -- 721504
        and xla.je_line_num          = jl.je_line_num   -- 4
        and xla.REFERENCE_2      = TO_CHAR(rec_gl1.invoice_id)
        and xla.REFERENCE_3      = TO_CHAR(rec_gl1.distribution_line_number)  
        
        AND glcc.SEGMENT11 BETWEEN '17000' AND '19999'
        AND TRUNC(JL.EFFECTIVE_DATE) <= TRUNC(V_CUTOFF_DATE)
        
        group by TRUNC(JL.EFFECTIVE_DATE)       
        ;
        --COMMIT;  --Comment by Joe
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END;
     
     END LOOP;
     COMMIT;  -- Add by Joe

     
    
    IF REC1.PO_RELEASE_ID IS NULL 
    --- CASE STANDARD
    THEN 
    
   -- write_log('step 6 - ' || to_char(sysdate,'dd/mm/yy hh24:mi:ss'),'log');
     BEGIN
        INSERT INTO TAC_WIP_AGING(PO_NUMBER,PO_LINE,RCV_DATE,AMOUNT,AMT_TYPE,REQUEST_ID)
        select  REC1.PO_NO||REC1.RELEASE_NO
        ,pol.line_num
        ,TRUNC(JL.EFFECTIVE_DATE) rcv_date
        ,sum(NVL(jl.accounted_dr,0)-NVL(jl.accounted_cr,0)) grn_amt
        ,'GRN'
        ,P_REQUEST_ID
        FROM
        GL_CODE_COMBINATIONS_KFV GLCC,
        po_headers_all poh,
        po_lines_all pol,
        PO_DISTRIBUTIONS_ALL POD,
        RCV_TRANSACTIONS RCT,
        PO_LOOKUP_CODES PL2,
        GL_IMPORT_REFERENCES R,
        RCV_RECEIVING_SUB_LEDGER  RRS,
        GL_JE_HEADERS jh,
        GL_JE_LINES      jl
        WHERE jl.status             = 'P'
      and jl.code_combination_id     = GLCC.code_combination_id
      and jh.status                = 'P' 
      and jh.actual_flag           = 'A'
      and jh.je_header_id          = jl.je_header_id
      and jh.je_source             = 'Purchasing' 
      and jh.je_category           = 'Receiving'
      and jl.effective_date        is not null   
      AND jh.JE_HEADER_ID = R.JE_HEADER_ID
      AND JL.JE_LINE_NUM = R.JE_LINE_NUM
      AND jh.ACTUAL_FLAG = RRS.ACTUAL_FLAG
      AND RRS.GL_SL_LINK_ID = R.GL_SL_LINK_ID
      AND RRS.RCV_TRANSACTION_ID = R.REFERENCE_5
      AND RCT.TRANSACTION_ID = RRS.RCV_TRANSACTION_ID
      AND RRS.REFERENCE3 = to_char(POD.PO_DISTRIBUTION_ID)
      AND PL2.LOOKUP_CODE = RCT.TRANSACTION_TYPE
      AND PL2.LOOKUP_TYPE = 'RCV TRANSACTION TYPE'
      AND R.GL_SL_LINK_TABLE = 'RSL'
      and RRS.CODE_COMBINATION_ID = GLCC.CODE_COMBINATION_ID
     
      and poh.po_header_id = pol.po_header_id      
      and pol.po_header_id = pod.po_header_id
      and pol.po_line_id = pod.po_line_id
      AND glcc.SEGMENT11 BETWEEN '17000' AND '19999'      
      AND poh.segment1 = REC1.PO_NO
      AND pol.line_num = REC1.PO_LINE_NO 
      AND TRUNC(JL.EFFECTIVE_DATE) <= TRUNC(v_CUTOFF_DATE)
      
      group by poh.segment1,pol.line_num,TRUNC(JL.EFFECTIVE_DATE)
      ; 
      COMMIT;
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END;
     
     ELSE  ------------------------  CASE RELEASE ----------------------------
    -- write_log('step 7 - ' || to_char(sysdate,'dd/mm/yy hh24:mi:ss'),'log');
     BEGIN
        INSERT INTO TAC_WIP_AGING(PO_NUMBER,PO_LINE,RCV_DATE,AMOUNT,AMT_TYPE,REQUEST_ID)
        select REC1.PO_NO||REC1.RELEASE_NO
        ,pol.line_num
        ,TRUNC(JL.EFFECTIVE_DATE) rcv_date
        ,sum(NVL(jl.accounted_dr,0)-NVL(jl.accounted_cr,0)) grn_amt
        ,'GRN'
        ,P_REQUEST_ID
        FROM
        GL_CODE_COMBINATIONS_KFV GLCC,
        po_headers_all poh,
        po_lines_all pol,
        PO_DISTRIBUTIONS_ALL POD,
        PO_LINE_LOCATIONS_ALL PLL,--
        RCV_TRANSACTIONS RCT,
        PO_LOOKUP_CODES PL2,
        GL_IMPORT_REFERENCES R,
        RCV_RECEIVING_SUB_LEDGER  RRS,
        GL_JE_HEADERS jh,
        GL_JE_LINES      jl
        WHERE jl.status             = 'P'
      and jl.code_combination_id     = GLCC.code_combination_id
      and jh.status                = 'P' 
      and jh.actual_flag           = 'A'
      and jh.je_header_id          = jl.je_header_id
      and jh.je_source             = 'Purchasing' 
      and jh.je_category           = 'Receiving'
      and jl.effective_date        is not null   
      AND jh.JE_HEADER_ID = R.JE_HEADER_ID
      AND JL.JE_LINE_NUM = R.JE_LINE_NUM
      AND jh.ACTUAL_FLAG = RRS.ACTUAL_FLAG
      AND RRS.GL_SL_LINK_ID = R.GL_SL_LINK_ID
      AND RRS.RCV_TRANSACTION_ID = R.REFERENCE_5
      AND RCT.TRANSACTION_ID = RRS.RCV_TRANSACTION_ID
      AND RRS.REFERENCE3 = to_char(POD.PO_DISTRIBUTION_ID)
      AND PL2.LOOKUP_CODE = RCT.TRANSACTION_TYPE
      AND PL2.LOOKUP_TYPE = 'RCV TRANSACTION TYPE'
      AND R.GL_SL_LINK_TABLE = 'RSL'
      and RRS.CODE_COMBINATION_ID = GLCC.CODE_COMBINATION_ID
     
      and poh.po_header_id = pol.po_header_id      
      and pol.po_header_id = pod.po_header_id
      and pol.po_line_id = pod.po_line_id
      AND glcc.SEGMENT11 BETWEEN '17000' AND '19999'      
      AND poh.segment1 = REC1.PO_NO
      AND pol.line_num = REC1.PO_LINE_NO 
      AND TRUNC(JL.EFFECTIVE_DATE) <= TRUNC(v_CUTOFF_DATE)
      
      --------------------CASE RELEASE----------------------
     and PLL.po_release_id = REC1.PO_RELEASE_ID
     AND POD.line_location_id = PLL.line_location_id
     AND POD.PO_HEADER_ID = PLL.PO_HEADER_ID
     AND POD.PO_LINE_ID = PLL.PO_LINE_ID
      
      group by poh.segment1,pol.line_num,TRUNC(JL.EFFECTIVE_DATE)
      ; 
      COMMIT;
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END;   
     END IF;
     
     
 
     
    for rec_gl2 in c_gl2 (REC1.PO_NO,REC1.PO_LINE_NO)
    loop
    --write_log('step 8 - ' || to_char(sysdate,'dd/mm/yy hh24:mi:ss'),'log');
     BEGIN
        INSERT INTO TAC_WIP_AGING(PO_NUMBER,PO_LINE,RCV_DATE,AMOUNT,AMT_TYPE,REQUEST_ID)
        select  --poh.segment1,pol.line_num
        REC1.PO_NO||REC1.RELEASE_NO,REC1.PO_LINE_NO
        ,TRUNC(JL.EFFECTIVE_DATE) RCV_DATE
        ,sum(NVL(jl.accounted_dr,0)-NVL(jl.accounted_cr,0)) grn_amt
        ,'GRN'
        ,P_REQUEST_ID
        from GL_CODE_COMBINATIONS_KFV GLCC
         ,GL_JE_HEADERS     jh
         ,GL_JE_LINES        jl
         ,GL_IMPORT_REFERENCES xla
    
        where  1=1
        and jl.status             = 'P'
        and jl.code_combination_id     = GLCC.code_combination_id
        and jh.status                = 'P' 
        and jh.actual_flag           = 'A'
        and jh.je_header_id          = jl.je_header_id
        and jh.je_source             = 'Payables' 
        and jh.je_category           = 'Purchase Invoices'
        and jl.effective_date        is not null   
        and xla.je_header_id         = jl.je_header_id  -- 721504
        and xla.je_line_num          = jl.je_line_num   -- 4
        and xla.REFERENCE_2      = TO_CHAR(rec_gl2.invoice_id)
        and xla.REFERENCE_3      = TO_CHAR(rec_gl2.distribution_line_number)       
        AND glcc.SEGMENT11 BETWEEN '17000' AND '19999'
     
        AND TRUNC(JL.EFFECTIVE_DATE) <= TRUNC(V_CUTOFF_DATE)         
        group by --poh.segment1,pol.line_num ,
        TRUNC(JL.EFFECTIVE_DATE)  
        ;
        --COMMIT;  --Comment by Joe
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END;
     
     end loop;
     COMMIT;  --Add by Joe
     
     
     
     BEGIN
     --write_log('step 9 - ' || to_char(sysdate,'dd/mm/yy hh24:mi:ss'),'log');
        INSERT INTO TAC_WIP_AGING(PO_NUMBER,PO_LINE,RCV_DATE,AMOUNT,AMT_TYPE,REQUEST_ID)
        SELECT fai.PO_NUMBER
              ,fab.ATTRIBUTE5
              --,last_day(to_date(pdp.PERIOD_NAME,'MON-YY')) regis_date
              ,nvl(TO_DATE(FAB.ATTRIBUTE9,'YYYY/MM/DD HH24:MI:SS'),FAB.CREATION_DATE) regis_date
              ,ROUND(SUM(fb.cost),2) REGIS_AMT
              ,'REGISTER'
              ,P_REQUEST_ID
      
      from FA_ASSET_INVOICES fai,
      FA_BOOKS fb,
      fa_additions_b fab
      where 1=1
      and fai.asset_id = fb.asset_id
      and fb.asset_id = fab.asset_id
      and fb.DATE_INEFFECTIVE is null
      --AND FB.RETIREMENT_PENDING_FLAG = 'NO'
      and fai.PO_NUMBER = REC1.PO_NO||REC1.RELEASE_NO
      and fab.ATTRIBUTE5 = REC1.PO_LINE_NO
      --and fai.ATTRIBUTE1 = REC1.PO_LINE_NO      
      --and last_day(to_date(pdp.PERIOD_NAME,'MON-YY')) <= last_day(to_date(P_PERIOD,'MON-YY')) 
      and nvl(TO_DATE(FAB.ATTRIBUTE9,'YYYY/MM/DD HH24:MI:SS'),FAB.CREATION_DATE) <= last_day(to_date(P_PERIOD,'MON-YY'))
      AND UPPER(fb.BOOK_TYPE_CODE) NOT LIKE '%TAX%'
      group by fai.PO_NUMBER,fab.ATTRIBUTE5
      --,last_day(to_date(pdp.PERIOD_NAME,'MON-YY'))
      ,nvl(TO_DATE(FAB.ATTRIBUTE9,'YYYY/MM/DD HH24:MI:SS'),FAB.CREATION_DATE)
      
      ;
      COMMIT;
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END;
     
     
    -- write_log('step 10 - ' || to_char(sysdate,'dd/mm/yy hh24:mi:ss'),'log');
     BEGIN
     
        INSERT INTO TAC_WIP_AGING(PO_NUMBER,PO_LINE,RCV_DATE,AMOUNT,AMT_TYPE,REQUEST_ID)
        SELECT FAI.PO_NUMBER
              ,fab.ATTRIBUTE5
              ,nvl(TO_DATE(FAR.DATE_RETIRED,'YYYY/MM/DD HH24:MI:SS'),FAR.DATE_RETIRED) regis_date
              ,SUM(COST_RETIRED) REGIST_AMT
              ,'REGISTER'
              ,P_REQUEST_ID
            FROM FA_RETIREMENTS           FAR,
            FA_ASSET_INVOICES       FAI,
            FA_ADDITIONS_B FAB
            WHERE  FAR.ASSET_ID =  FAI.ASSET_ID
            AND FAI.ASSET_ID = FAB.ASSET_ID
            and fai.PO_NUMBER = REC1.PO_NO||REC1.RELEASE_NO
            and fab.ATTRIBUTE5 = REC1.PO_LINE_NO
            --AND FAI.ATTRIBUTE1 = TO_CHAR(i_po_line_num)
            AND FAR.STATUS = 'PROCESSED'
            and nvl(TO_DATE(FAR.DATE_RETIRED,'YYYY/MM/DD HH24:MI:SS'),FAR.DATE_RETIRED) <= last_day(to_date(P_PERIOD,'MON-YY'))
            group by fai.PO_NUMBER,fab.ATTRIBUTE5
            ,nvl(TO_DATE(FAR.DATE_RETIRED,'YYYY/MM/DD HH24:MI:SS'),FAR.DATE_RETIRED)
            ;
            COMMIT;
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END; 
     
    V_MM_AMT := 0;
    V_CURRENT_AMT := 0;
    for rec_cl in c_cloud (REC1.PO_NO||REC1.RELEASE_NO,REC1.PO_LINE_NO,p_period)
    loop 
    
        V_MM_AMT := nvl(rec_cl.REGIST_AMT,0) - nvl(V_CURRENT_AMT,0); 
        V_CURRENT_AMT := nvl(rec_cl.REGIST_AMT,0);
    
   -- write_log('step 11 - ' || to_char(sysdate,'dd/mm/yy hh24:mi:ss'),'log');
     BEGIN
        INSERT INTO TAC_WIP_AGING(PO_NUMBER,PO_LINE,RCV_DATE,AMOUNT,AMT_TYPE,REQUEST_ID)
        SELECT rec_cl.PO_NUMBER
              ,rec_cl.PO_LINE_NUMBER
              ,rec_cl.REGIST_DATE
              ,V_MM_AMT
              ,'REGISTER'
              ,P_REQUEST_ID
        FROM dual;
        -- COMMIT;  -- Comment by Joe
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END;
     end loop;
     
     COMMIT; -- Add by Joe
     
    -- write_log('step 12 - ' || to_char(sysdate,'dd/mm/yy hh24:mi:ss'),'log');
     BEGIN
        INSERT INTO TAC_WIP_AGING(PO_NUMBER,PO_LINE,RCV_DATE,AMOUNT,AMT_TYPE,REQUEST_ID)
        SELECT PO_NUMBER
              ,PO_LINE_NUMBER
              ,LAST_DAY(TO_DATE(PERIOD_NAME,'MON-YY')) RCV_DATE
              ,SUM(PROGRESS_AMOUNT) PROGRESS_AMT 
              ,'PROGRESS'
              ,P_REQUEST_ID
        FROM TAC_PO_ADDITION_INFO
        WHERE PO_NUMBER = REC1.PO_NO||REC1.RELEASE_NO
        and PO_LINE_NUMBER = REC1.PO_LINE_NO
        AND LAST_DAY(TO_DATE(PERIOD_NAME,'MON-YY')) <= LAST_DAY(TO_DATE(P_PERIOD,'MON-YY')) 
        GROUP BY PO_NUMBER,PO_LINE_NUMBER,LAST_DAY(TO_DATE(PERIOD_NAME,'MON-YY'));
        COMMIT;
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END;
     
    --=============================================================================================--
    
    V_AGING := 0;
    V_AG1 := 0;
    V_AG2 := 0;
    V_AG3 := 0;
    V_AG4 := 0;
    V_AG5 := 0;
    V_AG6 := 0;
    V_AG7 := 0;
    V_AG_UNDEFY := 0; 
    V_GRN_AMT := 0;
    V_REGIS_AMT := 0;
    V_PROGRESS_AMT := 0;
    V_AMT_AG := 0;
    V_AMT_AGING := 0;
    V_AMOUNT := 0;
    V_RECAMT := 0;
    
    --write_log('step 13 - ' || to_char(sysdate,'dd/mm/yy hh24:mi:ss'),'log');
    BEGIN
        SELECT SUM(AMOUNT) AMOUNT
        INTO V_GRN_AMT
        FROM TAC_WIP_AGING
        WHERE PO_NUMBER = REC1.PO_NO||REC1.RELEASE_NO
        AND PO_LINE = REC1.PO_LINE_NO
        AND AMT_TYPE = 'GRN'
        AND REQUEST_ID = P_REQUEST_ID
        GROUP BY PO_NUMBER,PO_LINE;
     EXCEPTION WHEN OTHERS THEN
       NULL;
     END;
     
    -- write_log('step 14 - ' || to_char(sysdate,'dd/mm/yy hh24:mi:ss'),'log');
     BEGIN
        SELECT SUM(AMOUNT) AMOUNT
        INTO V_REGIS_AMT
        FROM TAC_WIP_AGING
        WHERE PO_NUMBER = REC1.PO_NO||REC1.RELEASE_NO
        AND PO_LINE = REC1.PO_LINE_NO
        AND AMT_TYPE = 'REGISTER'
        AND REQUEST_ID = P_REQUEST_ID
        GROUP BY PO_NUMBER,PO_LINE;
     EXCEPTION WHEN OTHERS THEN
       NULL;
     END;
     
    -- write_log('step 15 - ' || to_char(sysdate,'dd/mm/yy hh24:mi:ss'),'log');
     BEGIN
        SELECT SUM(AMOUNT) AMOUNT
        INTO V_PROGRESS_AMT
        FROM TAC_WIP_AGING
        WHERE PO_NUMBER = REC1.PO_NO||REC1.RELEASE_NO
        AND PO_LINE = REC1.PO_LINE_NO
        AND AMT_TYPE = 'PROGRESS'
        AND REQUEST_ID = P_REQUEST_ID
        GROUP BY PO_NUMBER,PO_LINE;
     EXCEPTION WHEN OTHERS THEN
       NULL;
     END;
     
     IF upper(REC1.TYPE_OF_WORK) IN ('E','U') THEN       
        V_BOOKING_AMT :=  GREATEST(V_GRN_AMT,V_REGIS_AMT);       
     ELSIF upper(REC1.TYPE_OF_WORK) = 'S' THEN       
        V_BOOKING_AMT := GREATEST(V_PROGRESS_AMT,V_GRN_AMT,V_REGIS_AMT);           
     END IF;
     
       
     IF upper(REC1.TYPE_OF_WORK) IN ('E','S','U') THEN
        V_AMOUNT := NVL(V_BOOKING_AMT,0) - NVL(V_REGIS_AMT,0);
     ELSIF upper(REC1.TYPE_OF_WORK) = 'T' THEN
        V_AMOUNT := NVL(V_GRN_AMT,0) - NVL(V_REGIS_AMT,0);
     ELSE
        V_AMOUNT := 0;
     END IF;


    
  if nvl(V_AMOUNT,0) > 0 then 
    
   FOR RECAMT IN C_AMT(REC1.PO_NO||REC1.RELEASE_NO,REC1.PO_LINE_NO,P_REQUEST_ID)
   LOOP  
   
       -- write_log('step 16 - ' || to_char(sysdate,'dd/mm/yy hh24:mi:ss'),'log');
   
        V_LAST_DATE := NULL;
        V_AGING := NULL;
        
        V_LAST_DATE := RECAMT.RCV_DATE;
        IF V_LAST_DATE IS NOT NULL THEN
            V_AGING := trunc(last_day(sysdate-30)) - trunc(V_LAST_DATE);
        END IF;
        
        IF upper(REC1.TYPE_OF_WORK) IN ('E','U','T','S') and V_GRN_AMT > V_REGIS_AMT 
            and RECAMT.AMT_TYPE in ('GRN','REGISTER')        
        THEN  
        
          if (upper(REC1.TYPE_OF_WORK) IN ('S') 
               AND V_GRN_AMT < V_PROGRESS_AMT) then
               
                  null;
          else
            

                V_RECAMT := 0;
            
                if V_REGIS_AMT < 0  then     
                    if  RECAMT.AMT_TYPE in ('GRN') then 
                        V_RECAMT := NVL(RECAMT.AMOUNT,0);
                    elsif RECAMT.AMT_TYPE in ('REGISTER') then
                        V_RECAMT := NVL(RECAMT.AMOUNT,0) * -1;
                    end if;
            
                else
                    if  RECAMT.AMT_TYPE in ('GRN') then 
                        V_RECAMT := NVL(RECAMT.AMOUNT,0);
                    end if;
                end if;
            
                V_AMT_AGING := NVL(V_AMT_AGING,0) + V_RECAMT;
            
           -- V_AMT_AG := V_RECAMT;
            
            IF V_AMT_AGING <= V_AMOUNT THEN
              /*IF V_RECAMT < 0 THEN
                V_AMT_AG := V_RECAMT - V_REGIS_AMT; 
                V_AMT_AGING := NVL(V_AMT_AGING,0) - V_RECAMT
                V_AMT_AGING := NVL(V_AMT_AGING,0) + V_AMT_AG;
              ELSE
                V_AMT_AG := V_RECAMT;
              END IF;*/
              V_AMT_AG := V_RECAMT;
            ELSE 
                V_AMT_AG := V_AMT_AGING - V_AMOUNT;
                V_AMT_AG := V_RECAMT - V_AMT_AG;
                V_AMT_AGING := V_AMOUNT;
                
            END IF;
            
            
                IF V_AGING <= 30 THEN
                    V_AG1 := NVL(V_AG1,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 31 AND V_AGING <= 180 THEN
                    V_AG2 := NVL(V_AG2,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 181 AND V_AGING <= 365 THEN
                    V_AG3 := NVL(V_AG3,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 366 AND V_AGING <= 730 THEN
                    V_AG4 := NVL(V_AG4,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 731 AND V_AGING <= 1095 THEN
                    V_AG5 := NVL(V_AG5,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 1096 AND V_AGING <= 1460 THEN
                    V_AG6 := NVL(V_AG6,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 1461 THEN
                    V_AG7 := NVL(V_AG7,0) + NVL(V_AMT_AG,0);
                else
                    V_AG_UNDEFY := NVL(V_AG_UNDEFY,0) + NVL(V_AMT_AG,0); 
                END IF;
                
                
           
          END IF;
        END IF;
        
        
        ----------------------------------------------------------------------------------------------------------
        IF upper(REC1.TYPE_OF_WORK) IN ('S') THEN  
          IF V_PROGRESS_AMT > V_REGIS_AMT 
            AND V_PROGRESS_AMT > V_GRN_AMT 
            AND V_AMT_AGING < V_AMOUNT
             AND RECAMT.AMT_TYPE in ('PROGRESS','REGISTER') THEN 
            
            V_RECAMT := 0;
            
           if V_REGIS_AMT < 0  then     
            if  RECAMT.AMT_TYPE in ('PROGRESS') then 
                V_RECAMT := NVL(RECAMT.AMOUNT,0);
            elsif RECAMT.AMT_TYPE in ('REGISTER') then
                V_RECAMT := NVL(RECAMT.AMOUNT,0) * -1;
            end if;
            
           else
            if  RECAMT.AMT_TYPE in ('PROGRESS') then 
                V_RECAMT := NVL(RECAMT.AMOUNT,0);
            end if;
           end if;
              
            V_AMT_AGING := NVL(V_AMT_AGING,0) + V_RECAMT;
           -- V_AMT_AG := V_RECAMT;
            
            IF V_AMT_AGING <= V_AMOUNT THEN
              V_AMT_AG := V_RECAMT;
            ELSE 
                V_AMT_AG := V_AMT_AGING - V_AMOUNT;
                V_AMT_AG := V_RECAMT - V_AMT_AG;
                V_AMT_AGING := V_AMOUNT;
                
            END IF;
            
            
                IF V_AGING <= 30 THEN
                    V_AG1 := NVL(V_AG1,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 31 AND V_AGING <= 180 THEN
                    V_AG2 := NVL(V_AG2,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 181 AND V_AGING <= 365 THEN
                    V_AG3 := NVL(V_AG3,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 366 AND V_AGING <= 730 THEN
                    V_AG4 := NVL(V_AG4,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 731 AND V_AGING <= 1095 THEN
                    V_AG5 := NVL(V_AG5,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 1096 AND V_AGING <= 1460 THEN
                    V_AG6 := NVL(V_AG6,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 1461 THEN
                    V_AG7 := NVL(V_AG7,0) + NVL(V_AMT_AG,0);
                else
                    V_AG_UNDEFY := NVL(V_AG_UNDEFY,0) + NVL(V_AMT_AG,0); 
                END IF;
                
            
          END IF;
        END IF;
        
        
        
        ----------------------------------------------------------------------------------------------------
        IF (upper(REC1.TYPE_OF_WORK) IN ('T') 
            and V_GRN_AMT < V_REGIS_AMT )
        THEN  
             
            V_RECAMT := 0;
                
           if V_REGIS_AMT < 0  then     
            if  RECAMT.AMT_TYPE in ('GRN') then 
                V_RECAMT := NVL(RECAMT.AMOUNT,0);
            elsif RECAMT.AMT_TYPE in ('REGISTER') then
                V_RECAMT := NVL(RECAMT.AMOUNT,0) * -1;
            end if;
            
           else
            if  RECAMT.AMT_TYPE in ('GRN') then 
                V_RECAMT := NVL(RECAMT.AMOUNT,0);
            end if;
           end if;
            
            V_AMT_AGING := NVL(V_AMT_AGING,0) + V_RECAMT;
            
           -- V_AMT_AG := V_RECAMT;
            
            IF V_AMT_AGING <= V_AMOUNT THEN
              V_AMT_AG := V_RECAMT;
            ELSE 
                V_AMT_AG := V_AMT_AGING - V_AMOUNT;
                V_AMT_AG := V_RECAMT - V_AMT_AG;
                V_AMT_AGING := V_AMOUNT;
                
            END IF;
            
            
                IF V_AGING <= 30 THEN
                    V_AG1 := NVL(V_AG1,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 31 AND V_AGING <= 180 THEN
                    V_AG2 := NVL(V_AG2,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 181 AND V_AGING <= 365 THEN
                    V_AG3 := NVL(V_AG3,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 366 AND V_AGING <= 730 THEN
                    V_AG4 := NVL(V_AG4,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 731 AND V_AGING <= 1095 THEN
                    V_AG5 := NVL(V_AG5,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 1096 AND V_AGING <= 1460 THEN
                    V_AG6 := NVL(V_AG6,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 1461 THEN
                    V_AG7 := NVL(V_AG7,0) + NVL(V_AMT_AG,0);
                else
                    V_AG_UNDEFY := NVL(V_AG_UNDEFY,0) + NVL(V_AMT_AG,0); 
                END IF;

         END IF;
        
    
     
     END LOOP; ---LOOP AMOUNT
     
     

     
     i := nvl(i,0) + 1;
     
    -- write_log('step 17 - ' || to_char(sysdate,'dd/mm/yy hh24:mi:ss'),'log');
     
     INSERT INTO TAC_WIP_AGING_TEMP
     (COMPANY
     ,PROJECT_ID
     ,PROJECT_CODE
     ,PROJECT_NAME
     ,PO_HEADER_ID
     ,PO_LINE_ID
     ,PO_NO
     ,PO_LINE_NO
     ,TYPE_OF_WORK
     ,WIP_PREPAID
     ,AG1
     ,AG2
     ,AG3
     ,AG4
     ,AG5
     ,AG6
     ,AG7
     ,AG_UNDEFY
     ,AMOUNT_TOTAL
     ,REQUEST_ID
     ,CREATION_DATE)
     VALUES(REC1.COMPANY
     ,REC1.PROJECT_ID
     ,REC1.PROJECT_CODE
     ,REC1.PROJECT_NAME
     ,REC1.PO_HEADER_ID
     ,REC1.PO_LINE_ID
     ,REC1.PO_NO||REC1.RELEASE_NO
     ,REC1.PO_LINE_NO
     ,REC1.TYPE_OF_WORK
     ,REC1.WIP_PREPAID
     ,V_AG1
     ,V_AG2
     ,V_AG3
     ,V_AG4
     ,V_AG5
     ,V_AG6
     ,V_AG7
     ,V_AG_UNDEFY
     ,V_AMOUNT
     ,P_REQUEST_ID
     ,SYSDATE);
     
     if mod(i,100) = 0 then
        COMMIT;
     end if;
     
     
     
     
     /* WRITE_LOG(REC1.COMPANY||'|'||
                REC1.PROJECT_CODE||'|'||
                REC1.PROJECT_NAME||'|'||
                REC1.PO_NO||'|'||
                REC1.PO_LINE_NO||'|'||
                REC1.WIP_PREPAID||'|'||
                V_AG1||'|'||
                V_AG2||'|'||
                V_AG3||'|'||
                V_AG4||'|'||
                V_AG5||'|'||
                V_AG6||'|'||
                V_AG7||'|'||
                V_AG_UNDEFY||'|'||
                V_AMOUNT,'OUT');*/
     
   end if; --- amount_total > 0
          
          
  END LOOP;
  
  
  INSERT INTO TAC_WIP_AGING_TEMP
     (COMPANY
     ,REQUEST_ID
     ,CREATION_DATE)
     VALUES('END'
     ,P_REQUEST_ID
     ,SYSDATE);
     COMMIT;
     

 
 EXCEPTION WHEN OTHERS THEN
    NULL;
 END;
 
 procedure gen_accrued_aging (errbuf               OUT VARCHAR2,
                         retcode              OUT NUMBER,
                         PIN_REQUEST_ID IN NUMBER,
                    P_ORG_ID IN NUMBER,
                    P_PO_NO_ID IN NUMBER,
                    P_PO_LINE IN NUMBER,
                    P_PROJECT_NO IN VARCHAR2,
                    PIN_PERIOD IN VARCHAR2)
                    
 IS
 
cursor c1(pPERIOD VARCHAR2) is
select distinct --FV.DESCRIPTION COMPANY,
(SELECT ou.NAME
            FROM hr_operating_units  ou
            WHERE organization_id = POH.ORG_ID )  COMPANY ,
PA.PROJECT_ID,
PA.SEGMENT1 PROJECT_CODE,
PA.NAME PROJECT_NAME,
POH.SEGMENT1 PO_NO,
TAC_FA_REPORT_PKG.GET_RELEASE_NUM(PLL.PO_RELEASE_ID) RELEASE_NO,
POL.LINE_NUM PO_LINE_NO
,PLL.PO_RELEASE_ID
,poh.org_id
,poh.po_header_id
,pol.po_line_id
,NVL(POL.UNIT_PRICE,0) UNIT_PRICE
,NVL(POH.RATE,1)  RATE
/*,TAC_WIP_ACC_REPORT_PKG.GET_REGIST_DATE(POH.SEGMENT1,
                       POL.LINE_NUM,
                       :CF_PERIOD) LAST_RECEIVE_DATE*/
--,pol.attribute1 type_of_work
,nvl(TAC_FA_REPORT_PKG.GET_TYPE_OF_WORK(pod.REQ_DISTRIBUTION_ID,POH.SEGMENT1,POL.LINE_NUM),'u') TYPE_OF_WORK 
,CASE WHEN upper(nvl(TAC_FA_REPORT_PKG.GET_TYPE_OF_WORK(pod.REQ_DISTRIBUTION_ID,POH.SEGMENT1,POL.LINE_NUM),'u')) IN ('E','S','U') THEN 'WIP'
      WHEN upper(nvl(TAC_FA_REPORT_PKG.GET_TYPE_OF_WORK(pod.REQ_DISTRIBUTION_ID,POH.SEGMENT1,POL.LINE_NUM),'u')) IN ('T') THEN 'PREPAID'
 END WIP_PREPAID

from po_headers_ALL poh,
PO_LINES_ALL POL,
PO_LINE_LOCATIONS_ALL PLL,
PO_DISTRIBUTIONS_all pod,
/*GL_CODE_COMBINATIONS_KFV GLCC,
FND_FLEX_VALUE_SETS FS, 
FND_FLEX_VALUES_VL FV,*/
PA_PROJECTS_ALL PA


WHERE 1=1
AND POH.PO_HEADER_ID = POD.PO_HEADER_ID
AND POH.PO_HEADER_ID = POL.PO_HEADER_ID
AND POL.PO_LINE_ID = POD.PO_LINE_ID
AND POH.PO_HEADER_ID = PLL.PO_HEADER_ID
AND POL.PO_LINE_ID = PLL.PO_LINE_ID
and pod.LINE_LOCATION_ID = pll.LINE_LOCATION_ID
/*AND POD.CODE_COMBINATION_ID = GLCC.CODE_COMBINATION_ID
AND FV.FLEX_VALUE_MEANING = GLCC.SEGMENT1
AND FS.FLEX_VALUE_SET_ID = FV.FLEX_VALUE_SET_ID
AND FS.FLEX_VALUE_SET_NAME = 'TAC_COA_LEGAL_ENTITY'*/
AND POD.PROJECT_ID = PA.PROJECT_ID(+)
AND POH.AUTHORIZATION_STATUS IN ('APPROVED')
AND NVL(POL.CANCEL_FLAG,'N') != 'Y' 

/*AND (POH.CLOSED_CODE IN ('FINALLY CLOSED','CLOSED') 
   OR POL.CLOSED_CODE IN ('FINALLY CLOSED','CLOSED') )*/

AND (TAC_FA_REPORT_PKG.GET_ACCOUNT_WIP(POH.PO_HEADER_ID,POL.PO_LINE_ID,POD.PROJECT_ID) --is not null
    IN (select lookup_code from fnd_lookup_values Where lookup_type = 'TAC_DTN_ASSET_REP_ACCOUNT')
                                                               
     OR TAC_FA_REPORT_PKG.CHECK_POADD_RP(POH.SEGMENT1|| TAC_FA_REPORT_PKG.GET_RELEASE_NUM(PLL.PO_RELEASE_ID),
     POL.LINE_NUM) = 'Y' 
     )
     AND NVL(TAC_FA_REPORT_PKG.CHECK_POADD_RP(POH.SEGMENT1|| TAC_FA_REPORT_PKG.GET_RELEASE_NUM(PLL.PO_RELEASE_ID),
     POL.LINE_NUM),'Y') != 'N'
     AND  NVL(TAC_FA_REPORT_PKG.CHECK_DISPLAY_PROJECT(POH.SEGMENT1|| TAC_FA_REPORT_PKG.GET_RELEASE_NUM(PLL.PO_RELEASE_ID),POL.LINE_NUM),'Y') != 'N'
  /*AND TAC_WIP_ACC_REPORT_PKG.GET_AMOUNT(POH.SEGMENT1,
                              POL.LINE_NUM,
                              NVL(TAC_FA_REPORT_PKG.GET_TYPE_OF_WORK(pod.REQ_DISTRIBUTION_ID,POH.SEGMENT1,POL.LINE_NUM),'U'),
                              'FA009',
                              :CF_PERIOD) > 0*/
                                     
--=== parameter ===--
AND POH.ORG_ID = P_ORG_ID
/*AND (PA.SEGMENT1 = P_PROJECT_NO
     or P_PROJECT_NO is null
     or PA.SEGMENT1 is null)*/
and case when nvl(P_PROJECT_NO,'X') != 'X' and PA.SEGMENT1 = P_PROJECT_NO then 1
         when nvl(P_PROJECT_NO,'X') = 'X' and POD.PROJECT_ID is null then 1
         when nvl(P_PROJECT_NO,'X') = 'X' then 1
         end = 1
AND POH.PO_HEADER_ID= NVL(P_PO_NO_ID,POH.PO_HEADER_ID)
AND POL.LINE_NUM = NVL(P_PO_LINE,POL.LINE_NUM)


--AND POH.po_header_id=316786 
--AND POH.SEGMENT1 = '8015007677'
--AND POL.LINE_NUM = 10
--in (select po_number from FA_MASS_ADDITIONS)

ORDER BY PA.SEGMENT1 ,
POH.SEGMENT1,
POL.LINE_NUM
;


CURSOR C_AMT(P_PONO VARCHAR2,P_POLINE NUMBER,P_REQUEST_ID NUMBER) IS
SELECT PO_NUMBER,PO_LINE
    ,AMT_TYPE
    ,nvl(RCV_DATE,sysdate) RCV_DATE
    ,SUM(AMOUNT) AMOUNT
     FROM TAC_ACCRUED_AGING
     WHERE PO_NUMBER = P_PONO
     AND PO_LINE = P_POLINE
     AND REQUEST_ID = P_REQUEST_ID
     GROUP BY PO_NUMBER,PO_LINE
     ,AMT_TYPE
     ,RCV_DATE
     ORDER BY AMT_TYPE,RCV_DATE DESC;
     
     
cursor C_GL1(p_pono varchar2,p_poline number)
IS 
select  aid.invoice_id,aid.distribution_line_number
        from ap_invoice_distributions_ALL aid
        where ATTRIBUTE1 = p_pono
        and ATTRIBUTE2 = p_poline;
        
  
cursor C_GL2(p_pono varchar2,p_poline number)
IS       
 SELECT distinct aid.INVOICE_ID,aid.po_distribution_id,aid.DISTRIBUTION_LINE_NUMBER
     FROM ap_invoice_distributions_ALL aid,
     PO_DISTRIBUTIONS_all pod,
     po_headers_all poh,
     po_lines_all pol
     where POD.PO_DISTRIBUTION_ID = AID.PO_DISTRIBUTION_ID
     and pod.po_header_id = poh.po_header_id
     and pod.po_line_id = pol.po_line_id
     and pol.po_header_id = poh.po_header_id
     and poh.segment1 = p_pono
     and pol.line_num = p_poline
     and aid.po_distribution_id is not null;
     
     
cursor C_CLOUD(p_pono varchar2,p_poline number,pin_period varchar2)
 IS      
  SELECT PO_NUMBER
              ,PO_LINE_NUMBER
              ,last_day(to_date(PERIOD_NAME,'MON-YY')) REGIST_DATE
              ,SUM(CLOUD_VALUE_REGISTER) REGIST_AMT
      FROM TAC_PO_CLOUD_INFO
      WHERE PO_NUMBER = p_pono
      AND PO_LINE_NUMBER = p_poline
      and last_day(to_date(PERIOD_NAME,'MON-YY')) <= last_day(to_date(pin_PERIOD,'MON-YY'))
      GROUP BY PO_NUMBER,PO_LINE_NUMBER,last_day(to_date(PERIOD_NAME,'MON-YY'))
      order by last_day(to_date(PERIOD_NAME,'MON-YY'))
      ;

P_REQUEST_ID NUMBER;
V_CUTOFF_DATE  DATE;
V_GRN_AMT NUMBER;
V_REGIS_AMT NUMBER;
V_PROGRESS_AMT NUMBER;
V_BOOKING_AMT NUMBER;
V_AMOUNT NUMBER;

V_AGING NUMBER;
V_AG1 NUMBER;
V_AG2 NUMBER;
V_AG3 NUMBER;
V_AG4 NUMBER;
V_AG5 NUMBER;
V_AG6 NUMBER;
V_AG7 NUMBER;
V_AG_UNDEFY NUMBER;
V_LAST_DATE DATE;
V_AMT_AG NUMBER;
V_AMT_AGING NUMBER;
V_RECAMT NUMBER;

V_CHECK VARCHAR2(1);



V_PO_NUMBER VARCHAR2(30);
V_PO_LINE NUMBER;
V_AMT_TYPE VARCHAR2(20);
P_PERIOD VARCHAR2(20);

V_MM_AMT NUMBER;
V_CURRENT_AMT NUMBER;


 
 BEGIN
 
    P_PERIOD := NVL(PIN_PERIOD,to_char(sysdate,'MON-YY'));
    P_REQUEST_ID := nvl(PIN_REQUEST_ID,fnd_global.conc_request_id);
       
 
    BEGIN
        select TO_DATE(MEANING,'DD-MON-YYYY') CUTOFF_DATE
        INTO V_CUTOFF_DATE
        from fnd_lookup_values 
        Where lookup_type = 'TAC_DTN_CUTOFF'
        AND LOOKUP_CODE = P_PERIOD;
    EXCEPTION WHEN OTHERS THEN
        V_CUTOFF_DATE := LAST_DAY(SYSDATE-30);
    END;
    
    
    DELETE FROM TAC_ACCRUED_AGING
    WHERE NVL(REQUEST_ID,0) < 100;

    DELETE FROM TAC_ACCRUED_AGING_TEMP
    WHERE NVL(REQUEST_ID,0) < 100;
    COMMIT;
    
    
    FOR REC1 IN C1(P_PERIOD) 
    LOOP
    
    DELETE FROM TAC_ACCRUED_AGING
    WHERE NVL(REQUEST_ID,0) = P_REQUEST_ID;
    
     BEGIN
        INSERT INTO TAC_ACCRUED_AGING(PO_NUMBER,PO_LINE,RCV_DATE,AMOUNT,AMT_TYPE,REQUEST_ID)
        SELECT PO_NUMBER
        ,PO_LINE_NUMBER
        ,LAST_DAY(TO_DATE(PERIOD_NAME,'MON-YY')) RCV_DATE
        ,SUM(GRN_ADJUST) GRN_AMT
        ,'GRN'
        ,P_REQUEST_ID
        FROM TAC_PO_ADDITION_INFO
        WHERE PO_NUMBER = REC1.PO_NO||REC1.RELEASE_NO
        AND PO_LINE_NUMBER = REC1.PO_LINE_NO
        AND LAST_DAY(TO_DATE(PERIOD_NAME,'MON-YY')) <= LAST_DAY(TO_DATE(P_PERIOD,'MON-YY')) 
        GROUP BY PO_NUMBER,PO_LINE_NUMBER,LAST_DAY(TO_DATE(PERIOD_NAME,'MON-YY'));
        COMMIT;
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END;
     
        V_CHECK := NULL;
        

     
     BEGIN
        INSERT INTO TAC_ACCRUED_AGING(PO_NUMBER,PO_LINE,RCV_DATE,AMOUNT,AMT_TYPE,REQUEST_ID)
        SELECT ATTRIBUTE1
              ,ATTRIBUTE2
              ,TRUNC(ACCOUNTING_DATE) RCV_DATE
              ,SUM(AMOUNT) AMOUNT
              ,'GRN'
              ,P_REQUEST_ID
            FROM AP_INVOICE_DISTRIBUTIONS_all
            WHERE ATTRIBUTE1 = REC1.PO_NO||REC1.RELEASE_NO
            AND ATTRIBUTE2 = REC1.PO_LINE_NO
            AND TRUNC(ACCOUNTING_DATE) <= TRUNC(V_CUTOFF_DATE)
            group by ATTRIBUTE1 ,
            ATTRIBUTE2,
            TRUNC(ACCOUNTING_DATE);
            COMMIT;
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END;
     
     for rec_gl1 in c_gl1(REC1.PO_NO||REC1.RELEASE_NO,REC1.PO_LINE_NO)
     loop
     BEGIN
        INSERT INTO TAC_ACCRUED_AGING(PO_NUMBER,PO_LINE,RCV_DATE,AMOUNT,AMT_TYPE,REQUEST_ID)
        select  --AID.ATTRIBUTE1,AID.ATTRIBUTE2
        REC1.PO_NO||REC1.RELEASE_NO,REC1.PO_LINE_NO
        ,TRUNC(JL.EFFECTIVE_DATE) RCV_DATE
        ,sum(NVL(jl.accounted_dr,0)-NVL(jl.accounted_cr,0)) grn_amt
        ,'GRN'
        ,P_REQUEST_ID
       
        from  GL_JE_LINES        jl
         ,GL_JE_HEADERS     jh
         ,GL_CODE_COMBINATIONS_KFV GLCC
         ,GL_IMPORT_REFERENCES xla        

        where  jl.status             = 'P'
        and jl.code_combination_id     = GLCC.code_combination_id
        and jh.status                = 'P' 
        and jh.actual_flag           = 'A'
        and jh.je_header_id          = jl.je_header_id
        and jh.je_source             = 'Payables' 
        and jh.je_category           = 'Purchase Invoices'
        and jl.effective_date        is not null   
        and xla.je_header_id         = jl.je_header_id  -- 721504
        and xla.je_line_num          = jl.je_line_num   -- 4
        and xla.REFERENCE_2      = TO_CHAR(rec_gl1.invoice_id)
        and xla.REFERENCE_3      = TO_CHAR(rec_gl1.distribution_line_number)  
        
        AND glcc.SEGMENT11 BETWEEN '17000' AND '19999'
        AND TRUNC(JL.EFFECTIVE_DATE) <= TRUNC(V_CUTOFF_DATE)

        
        group by TRUNC(JL.EFFECTIVE_DATE)       
        ;
        COMMIT;
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END;
     
     end loop;
     

     
    IF REC1.PO_RELEASE_ID IS NULL 
    
    THEN 
     BEGIN
        INSERT INTO TAC_ACCRUED_AGING(PO_NUMBER,PO_LINE,RCV_DATE,AMOUNT,AMT_TYPE,REQUEST_ID)
        select  REC1.PO_NO||REC1.RELEASE_NO 
        ,pol.line_num
        ,TRUNC(JL.EFFECTIVE_DATE) rcv_date
        ,sum(NVL(jl.accounted_dr,0)-NVL(jl.accounted_cr,0)) grn_amt
        ,'GRN'
        ,P_REQUEST_ID
        FROM
        GL_CODE_COMBINATIONS_KFV GLCC,
        po_headers_all poh,
        po_lines_all pol,
        PO_DISTRIBUTIONS_ALL POD,
        RCV_TRANSACTIONS RCT,
        PO_LOOKUP_CODES PL2,
        GL_IMPORT_REFERENCES R,
        RCV_RECEIVING_SUB_LEDGER  RRS,
        GL_JE_HEADERS jh,
        GL_JE_LINES      jl
        WHERE jl.status             = 'P'
      and jl.code_combination_id     = GLCC.code_combination_id
      and jh.status                = 'P' 
      and jh.actual_flag           = 'A'
      and jh.je_header_id          = jl.je_header_id
      and jh.je_source             = 'Purchasing' 
      and jh.je_category           = 'Receiving'
      and jl.effective_date        is not null   
      AND jh.JE_HEADER_ID = R.JE_HEADER_ID
      AND JL.JE_LINE_NUM = R.JE_LINE_NUM
      AND jh.ACTUAL_FLAG = RRS.ACTUAL_FLAG
      AND RRS.GL_SL_LINK_ID = R.GL_SL_LINK_ID
      AND RRS.RCV_TRANSACTION_ID = R.REFERENCE_5
      AND RCT.TRANSACTION_ID = RRS.RCV_TRANSACTION_ID
      AND RRS.REFERENCE3 = to_char(POD.PO_DISTRIBUTION_ID)
      AND PL2.LOOKUP_CODE = RCT.TRANSACTION_TYPE
      AND PL2.LOOKUP_TYPE = 'RCV TRANSACTION TYPE'
      AND R.GL_SL_LINK_TABLE = 'RSL'
      and RRS.CODE_COMBINATION_ID = GLCC.CODE_COMBINATION_ID
     
      and poh.po_header_id = pol.po_header_id      
      and pol.po_header_id = pod.po_header_id
      and pol.po_line_id = pod.po_line_id
      AND glcc.SEGMENT11 BETWEEN '17000' AND '19999'   
      

      AND poh.segment1 = REC1.PO_NO
      AND pol.line_num = REC1.PO_LINE_NO 
      AND TRUNC(JL.EFFECTIVE_DATE) <= TRUNC(v_CUTOFF_DATE)
      
      group by poh.segment1,pol.line_num,TRUNC(JL.EFFECTIVE_DATE)
      ; 
      COMMIT;
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END;
     
    ELSE  ------------------------  CASE RELEASE ----------------------------
     
     BEGIN
        INSERT INTO TAC_ACCRUED_AGING(PO_NUMBER,PO_LINE,RCV_DATE,AMOUNT,AMT_TYPE,REQUEST_ID)
        select  REC1.PO_NO||REC1.RELEASE_NO
        ,pol.line_num
        ,TRUNC(JL.EFFECTIVE_DATE) rcv_date
        ,sum(NVL(jl.accounted_dr,0)-NVL(jl.accounted_cr,0)) grn_amt
        ,'GRN'
        ,P_REQUEST_ID
        FROM
        GL_CODE_COMBINATIONS_KFV GLCC,
        po_headers_all poh,
        po_lines_all pol,
        PO_DISTRIBUTIONS_ALL POD,
        PO_LINE_LOCATIONS_ALL PLL,--
        RCV_TRANSACTIONS RCT,
        PO_LOOKUP_CODES PL2,
        GL_IMPORT_REFERENCES R,
        RCV_RECEIVING_SUB_LEDGER  RRS,
        GL_JE_HEADERS jh,
        GL_JE_LINES      jl
        WHERE jl.status             = 'P'
      and jl.code_combination_id     = GLCC.code_combination_id
      and jh.status                = 'P' 
      and jh.actual_flag           = 'A'
      and jh.je_header_id          = jl.je_header_id
      and jh.je_source             = 'Purchasing' 
      and jh.je_category           = 'Receiving'
      and jl.effective_date        is not null   
      AND jh.JE_HEADER_ID = R.JE_HEADER_ID
      AND JL.JE_LINE_NUM = R.JE_LINE_NUM
      AND jh.ACTUAL_FLAG = RRS.ACTUAL_FLAG
      AND RRS.GL_SL_LINK_ID = R.GL_SL_LINK_ID
      AND RRS.RCV_TRANSACTION_ID = R.REFERENCE_5
      AND RCT.TRANSACTION_ID = RRS.RCV_TRANSACTION_ID
      AND RRS.REFERENCE3 = to_char(POD.PO_DISTRIBUTION_ID)
      AND PL2.LOOKUP_CODE = RCT.TRANSACTION_TYPE
      AND PL2.LOOKUP_TYPE = 'RCV TRANSACTION TYPE'
      AND R.GL_SL_LINK_TABLE = 'RSL'
      and RRS.CODE_COMBINATION_ID = GLCC.CODE_COMBINATION_ID
     
      and poh.po_header_id = pol.po_header_id      
      and pol.po_header_id = pod.po_header_id
      and pol.po_line_id = pod.po_line_id
      AND glcc.SEGMENT11 BETWEEN '17000' AND '19999'      
      AND poh.segment1 = REC1.PO_NO
      AND pol.line_num = REC1.PO_LINE_NO 
      AND TRUNC(JL.EFFECTIVE_DATE) <= TRUNC(v_CUTOFF_DATE)
      
      --------------------CASE RELEASE----------------------
     and PLL.po_release_id = REC1.PO_RELEASE_ID
     AND POD.line_location_id = PLL.line_location_id
     AND POD.PO_HEADER_ID = PLL.PO_HEADER_ID
     AND POD.PO_LINE_ID = PLL.PO_LINE_ID
      
      group by poh.segment1,pol.line_num,TRUNC(JL.EFFECTIVE_DATE)
      ; 
      COMMIT;
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END;   

     
     END IF;


     
     
     for rec_gl2 in c_gl2(REC1.PO_NO,REC1.PO_LINE_NO)
     loop
     BEGIN
        INSERT INTO TAC_ACCRUED_AGING(PO_NUMBER,PO_LINE,RCV_DATE,AMOUNT,AMT_TYPE,REQUEST_ID)
        select  --poh.segment1 ,pol.line_num
        REC1.PO_NO||REC1.RELEASE_NO,REC1.PO_LINE_NO
        ,TRUNC(JL.EFFECTIVE_DATE) RCV_DATE
        ,sum(NVL(jl.accounted_dr,0)-NVL(jl.accounted_cr,0)) grn_amt
        ,'GRN'
        ,P_REQUEST_ID
        from GL_CODE_COMBINATIONS_KFV GLCC
         ,GL_JE_HEADERS     jh
         ,GL_JE_LINES        jl
         ,GL_IMPORT_REFERENCES xla

    
        where  1=1
        and jl.status             = 'P'
        and jl.code_combination_id     = GLCC.code_combination_id
        and jh.status                = 'P' 
        and jh.actual_flag           = 'A'
        and jh.je_header_id          = jl.je_header_id
        and jh.je_source             = 'Payables' 
        and jh.je_category           = 'Purchase Invoices'
        and jl.effective_date        is not null   
        and xla.je_header_id         = jl.je_header_id  -- 721504
        and xla.je_line_num          = jl.je_line_num   -- 4
        and xla.REFERENCE_2      = TO_CHAR(REC_GL2.invoice_id)
        and xla.REFERENCE_3      = TO_CHAR(REC_GL2.distribution_line_number)       
        AND glcc.SEGMENT11 BETWEEN '17000' AND '19999'
    
        AND TRUNC(JL.EFFECTIVE_DATE) <= TRUNC(V_CUTOFF_DATE)         
        group by --poh.segment1,pol.line_num ,
        TRUNC(JL.EFFECTIVE_DATE)  
        ;
        COMMIT;
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END;
     
     END LOOP;

     
     
     BEGIN
        INSERT INTO TAC_ACCRUED_AGING(PO_NUMBER,PO_LINE,RCV_DATE,AMOUNT,AMT_TYPE,REQUEST_ID)
        SELECT fai.PO_NUMBER
              ,fab.ATTRIBUTE5
              ,nvl(TO_DATE(FAB.ATTRIBUTE9,'YYYY/MM/DD HH24:MI:SS'),FAB.CREATION_DATE) regis_date
              ,ROUND(SUM(fb.cost),2) REGIS_AMT
              --,ROUND(SUM(fb.original_cost),2) REGIS_AMT
              ,'REGISTER'
              ,P_REQUEST_ID
      
      from FA_ASSET_INVOICES fai,
      FA_BOOKS fb,
      FA_ADDITIONS_B FAB
      where 1=1
      and fai.asset_id = fb.asset_id
      and fb.asset_id = fab.asset_id
      and fb.DATE_INEFFECTIVE is null
      --AND FB.RETIREMENT_PENDING_FLAG = 'NO'
      and fai.PO_NUMBER = REC1.PO_NO||REC1.RELEASE_NO
      and fab.ATTRIBUTE5 = REC1.PO_LINE_NO
      --and fai.ATTRIBUTE1 = REC1.PO_LINE_NO
      and nvl(TO_DATE(FAB.ATTRIBUTE9,'YYYY/MM/DD HH24:MI:SS'),FAB.CREATION_DATE) <= last_day(to_date(P_PERIOD,'MON-YY'))
      AND UPPER(fb.BOOK_TYPE_CODE) NOT LIKE '%TAX%'
      group by fai.PO_NUMBER,fab.ATTRIBUTE5
      ,nvl(TO_DATE(FAB.ATTRIBUTE9,'YYYY/MM/DD HH24:MI:SS'),FAB.CREATION_DATE)
      
      ;
      COMMIT;
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END;
     
     
     
     BEGIN
     
        INSERT INTO TAC_ACCRUED_AGING(PO_NUMBER,PO_LINE,RCV_DATE,AMOUNT,AMT_TYPE,REQUEST_ID)
        SELECT FAI.PO_NUMBER
              ,fab.ATTRIBUTE5
              ,nvl(TO_DATE(FAR.DATE_RETIRED,'YYYY/MM/DD HH24:MI:SS'),FAR.DATE_RETIRED) regis_date
              ,SUM(COST_RETIRED) REGIST_AMT
              ,'REGISTER'
              ,P_REQUEST_ID
            FROM FA_RETIREMENTS           FAR,
            FA_ASSET_INVOICES       FAI,
            FA_ADDITIONS_B FAB
            WHERE  FAR.ASSET_ID =  FAI.ASSET_ID
            AND FAI.ASSET_ID = FAB.ASSET_ID
            and fai.PO_NUMBER = REC1.PO_NO||REC1.RELEASE_NO
            and fab.ATTRIBUTE5 = REC1.PO_LINE_NO
            --AND FAI.ATTRIBUTE1 = TO_CHAR(i_po_line_num)
            AND FAR.STATUS = 'PROCESSED'
            and nvl(TO_DATE(FAR.DATE_RETIRED,'YYYY/MM/DD HH24:MI:SS'),FAR.DATE_RETIRED) <= last_day(to_date(P_PERIOD,'MON-YY'))
            group by fai.PO_NUMBER,fab.ATTRIBUTE5
            ,nvl(TO_DATE(FAR.DATE_RETIRED,'YYYY/MM/DD HH24:MI:SS'),FAR.DATE_RETIRED)
            ;
            COMMIT;
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END; 
     
     
    V_MM_AMT := 0;
    V_CURRENT_AMT := 0;
    for rec_cl in c_cloud (REC1.PO_NO||REC1.RELEASE_NO,REC1.PO_LINE_NO,p_period)
    loop 
    
        V_MM_AMT := nvl(rec_cl.REGIST_AMT,0) - nvl(V_CURRENT_AMT,0); 
        V_CURRENT_AMT := nvl(rec_cl.REGIST_AMT,0);
    
    
     BEGIN
        INSERT INTO TAC_ACCRUED_AGING(PO_NUMBER,PO_LINE,RCV_DATE,AMOUNT,AMT_TYPE,REQUEST_ID)
        SELECT rec_cl.PO_NUMBER
              ,rec_cl.PO_LINE_NUMBER
              ,rec_cl.REGIST_DATE
              ,V_MM_AMT
              ,'REGISTER'
              ,P_REQUEST_ID
        FROM dual;
        COMMIT;
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END;
     end loop;
     
     
           
           
     
     
     BEGIN
        INSERT INTO TAC_ACCRUED_AGING(PO_NUMBER,PO_LINE,RCV_DATE,AMOUNT,AMT_TYPE,REQUEST_ID)
        SELECT PO_NUMBER
              ,PO_LINE_NUMBER
              ,LAST_DAY(TO_DATE(PERIOD_NAME,'MON-YY')) RCV_DATE
              ,SUM(PROGRESS_AMOUNT) PROGRESS_AMT 
              ,'PROGRESS'
              ,P_REQUEST_ID
        FROM TAC_PO_ADDITION_INFO
        WHERE PO_NUMBER = REC1.PO_NO||REC1.RELEASE_NO
        and PO_LINE_NUMBER = REC1.PO_LINE_NO
        AND LAST_DAY(TO_DATE(PERIOD_NAME,'MON-YY')) <= LAST_DAY(TO_DATE(P_PERIOD,'MON-YY')) 
        GROUP BY PO_NUMBER,PO_LINE_NUMBER,LAST_DAY(TO_DATE(PERIOD_NAME,'MON-YY'));
        COMMIT;
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END;
     
    --=============================================================================================--
    
    V_AGING := 0;
    V_AG1 := 0;
    V_AG2 := 0;
    V_AG3 := 0;
    V_AG4 := 0;
    V_AG5 := 0;
    V_AG6 := 0;
    V_AG7 := 0;
    V_AG_UNDEFY := 0; 
    V_GRN_AMT := 0;
    V_REGIS_AMT := 0;
    V_PROGRESS_AMT := 0;
    V_AMT_AG := 0;
    V_AMT_AGING := 0;
    V_AMOUNT := 0;
    V_RECAMT := 0;
    
    BEGIN
        SELECT SUM(AMOUNT) AMOUNT
        INTO V_GRN_AMT
        FROM TAC_ACCRUED_AGING
        WHERE PO_NUMBER = REC1.PO_NO||REC1.RELEASE_NO
        AND PO_LINE = REC1.PO_LINE_NO
        AND AMT_TYPE = 'GRN'
        AND REQUEST_ID = P_REQUEST_ID
        GROUP BY PO_NUMBER,PO_LINE;
     EXCEPTION WHEN OTHERS THEN
       NULL;
     END;
     
     BEGIN
        SELECT SUM(AMOUNT) AMOUNT
        INTO V_REGIS_AMT
        FROM TAC_ACCRUED_AGING
        WHERE PO_NUMBER = REC1.PO_NO||REC1.RELEASE_NO
        AND PO_LINE = REC1.PO_LINE_NO
        AND AMT_TYPE = 'REGISTER'
        AND REQUEST_ID = P_REQUEST_ID
        GROUP BY PO_NUMBER,PO_LINE;
     EXCEPTION WHEN OTHERS THEN
       NULL;
     END;
     
     BEGIN
        SELECT SUM(AMOUNT) AMOUNT
        INTO V_PROGRESS_AMT
        FROM TAC_ACCRUED_AGING
        WHERE PO_NUMBER = REC1.PO_NO||REC1.RELEASE_NO
        AND PO_LINE = REC1.PO_LINE_NO
        AND AMT_TYPE = 'PROGRESS'
        AND REQUEST_ID = P_REQUEST_ID
        GROUP BY PO_NUMBER,PO_LINE;
     EXCEPTION WHEN OTHERS THEN
       NULL;
     END;
     
     IF upper(REC1.TYPE_OF_WORK) IN ('E','U') THEN       
        V_BOOKING_AMT :=  GREATEST(V_GRN_AMT,V_REGIS_AMT);       
     ELSIF upper(REC1.TYPE_OF_WORK) = 'S' THEN       
        V_BOOKING_AMT := GREATEST(V_PROGRESS_AMT,V_GRN_AMT,V_REGIS_AMT);         
     END IF;
     
       
     
      IF upper(REC1.TYPE_OF_WORK) IN ('E','U') THEN
        V_AMOUNT :=  NVL(V_REGIS_AMT,0) - NVL(V_GRN_AMT,0);
      ELSIF upper(REC1.TYPE_OF_WORK) = 'S' THEN
        V_AMOUNT :=  NVL(V_BOOKING_AMT,0) - NVL(V_GRN_AMT,0);
      ELSIF upper(REC1.TYPE_OF_WORK) = 'T' THEN
        V_AMOUNT :=  NVL(V_REGIS_AMT,0) - NVL(V_GRN_AMT,0);
      ELSE
        V_AMOUNT := 0;
      END IF;

    
  if nvl(V_AMOUNT,0) > 0 then  
     
   FOR RECAMT IN C_AMT(REC1.PO_NO||REC1.RELEASE_NO,REC1.PO_LINE_NO,P_REQUEST_ID)
   LOOP  
        V_LAST_DATE := NULL;
        V_AGING := NULL;
        V_LAST_DATE := RECAMT.RCV_DATE;
        IF V_LAST_DATE IS NOT NULL THEN
            V_AGING := trunc(last_day(sysdate-30)) - trunc(V_LAST_DATE);
        END IF;
        
        --DBMS_OUTPUT.PUT_LINE('V_LAST_DATE >>'|| V_LAST_DATE);
        --DBMS_OUTPUT.PUT_LINE('RECAMT.RCV_DATE >>'|| RECAMT.RCV_DATE);
        
        IF   upper(REC1.TYPE_OF_WORK) IN ('E','U','T','S') 
             and V_REGIS_AMT > V_GRN_AMT 
             and RECAMT.AMT_TYPE IN ('REGISTER','GRN')
        
        THEN  
          

        if (upper(REC1.TYPE_OF_WORK) IN ('S') 
             AND V_REGIS_AMT < V_PROGRESS_AMT) then
               
                  null;
        else       
            V_RECAMT := 0;

            if V_REGIS_AMT < 0  then     
              IF RECAMT.AMT_TYPE IN ('REGISTER') THEN
               V_RECAMT := NVL(RECAMT.AMOUNT,0);
              ELSIF RECAMT.AMT_TYPE IN ('GRN') THEN
               V_RECAMT := NVL(RECAMT.AMOUNT,0) * -1;
              END IF;
            
           else
              if  RECAMT.AMT_TYPE in ('REGISTER') then 
                V_RECAMT := NVL(RECAMT.AMOUNT,0);
              end if;
           end if;
            
            V_AMT_AGING := NVL(V_AMT_AGING,0) + V_RECAMT;
          --  V_AMT_AG := V_RECAMT;
            
            IF V_AMT_AGING <= V_AMOUNT THEN
                V_AMT_AG := V_RECAMT;
            ELSE 
                
                V_AMT_AG := V_AMT_AGING - V_AMOUNT;
                V_AMT_AG := V_RECAMT - V_AMT_AG;
                V_AMT_AGING := V_AMOUNT;
                
            END IF;
            
            
                IF V_AGING <= 30 THEN
                    V_AG1 := NVL(V_AG1,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 31 AND V_AGING <= 180 THEN
                    V_AG2 := NVL(V_AG2,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 181 AND V_AGING <= 365 THEN
                    V_AG3 := NVL(V_AG3,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 366 AND V_AGING <= 730 THEN
                    V_AG4 := NVL(V_AG4,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 731 AND V_AGING <= 1095 THEN
                    V_AG5 := NVL(V_AG5,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 1096 AND V_AGING <= 1460 THEN
                    V_AG6 := NVL(V_AG6,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 1461 THEN
                    V_AG7 := NVL(V_AG7,0) + NVL(V_AMT_AG,0);
                else
                    V_AG_UNDEFY := NVL(V_AG_UNDEFY,0) + NVL(V_AMT_AG,0); 
                END IF;
                
                
        end if;        
      
        END IF;
        
        -----------------------------------------------------------------------------------------------
        IF upper(REC1.TYPE_OF_WORK) IN ('S') THEN  
          IF V_PROGRESS_AMT > V_REGIS_AMT 
             AND V_PROGRESS_AMT > V_GRN_AMT 
             --V_AMT_AGING < V_AMOUNT
            --AND RECAMT.AMT_TYPE = 'PROGRESS' THEN
              AND RECAMT.AMT_TYPE IN ('PROGRESS','GRN') THEN
            
            V_RECAMT := 0;
            
            
            if V_REGIS_AMT < 0  then     
             IF RECAMT.AMT_TYPE IN ('PROGRESS') THEN
               V_RECAMT := NVL(RECAMT.AMOUNT,0);
            ELSIF RECAMT.AMT_TYPE IN ('GRN') THEN
               V_RECAMT := NVL(RECAMT.AMOUNT,0) * -1;
            END IF;
            
           else
            if  RECAMT.AMT_TYPE in ('PROGRESS') then 
                V_RECAMT := NVL(RECAMT.AMOUNT,0);
            end if;
           end if;
            
            V_AMT_AGING := NVL(V_AMT_AGING,0) + V_RECAMT;
           -- V_AMT_AG := V_RECAMT;
              
            
            IF V_AMT_AGING <= V_AMOUNT THEN
                V_AMT_AG := V_RECAMT;
            ELSE 
                V_AMT_AG := V_AMT_AGING - V_AMOUNT;
                V_AMT_AG := V_RECAMT - V_AMT_AG;
                V_AMT_AGING := V_AMOUNT;
            END IF;
            
                IF V_AGING <= 30 THEN
                    V_AG1 := NVL(V_AG1,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 31 AND V_AGING <= 180 THEN
                    V_AG2 := NVL(V_AG2,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 181 AND V_AGING <= 365 THEN
                    V_AG3 := NVL(V_AG3,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 366 AND V_AGING <= 730 THEN
                    V_AG4 := NVL(V_AG4,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 731 AND V_AGING <= 1095 THEN
                    V_AG5 := NVL(V_AG5,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 1096 AND V_AGING <= 1460 THEN
                    V_AG6 := NVL(V_AG6,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 1461 THEN
                    V_AG7 := NVL(V_AG7,0) + NVL(V_AMT_AG,0);
                else
                    V_AG_UNDEFY := NVL(V_AG_UNDEFY,0) + NVL(V_AMT_AG,0); 
                END IF;
                
            
          END IF;
        END IF;
        
        ----------------------------------------------------------------------------------------------------
        IF (upper(REC1.TYPE_OF_WORK) IN ('T') 
            and  V_REGIS_AMT < V_GRN_AMT)
        THEN  
             
            V_RECAMT := 0;
            
            
                
           if V_REGIS_AMT < 0  then     
             IF RECAMT.AMT_TYPE IN ('REGISTER') THEN
               V_RECAMT := NVL(RECAMT.AMOUNT,0);
             ELSIF RECAMT.AMT_TYPE IN ('GRN') THEN
               V_RECAMT := NVL(RECAMT.AMOUNT,0) * -1;
             END IF;
            
           else
            if  RECAMT.AMT_TYPE in ('REGISTER') then 
                V_RECAMT := NVL(RECAMT.AMOUNT,0);
            end if;
           end if;
            
            V_AMT_AGING := NVL(V_AMT_AGING,0) + V_RECAMT;
            
           -- V_AMT_AG := V_RECAMT;

            IF V_AMT_AGING <= V_AMOUNT THEN
              V_AMT_AG := V_RECAMT;
            ELSE 
                V_AMT_AG := V_AMT_AGING - V_AMOUNT;
                V_AMT_AG := V_RECAMT - V_AMT_AG;
                V_AMT_AGING := V_AMOUNT;
                
            END IF;
            
            
                IF V_AGING <= 30 THEN
                    V_AG1 := NVL(V_AG1,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 31 AND V_AGING <= 180 THEN
                    V_AG2 := NVL(V_AG2,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 181 AND V_AGING <= 365 THEN
                    V_AG3 := NVL(V_AG3,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 366 AND V_AGING <= 730 THEN
                    V_AG4 := NVL(V_AG4,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 731 AND V_AGING <= 1095 THEN
                    V_AG5 := NVL(V_AG5,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 1096 AND V_AGING <= 1460 THEN
                    V_AG6 := NVL(V_AG6,0) + NVL(V_AMT_AG,0);
                ELSIF V_AGING >= 1461 THEN
                    V_AG7 := NVL(V_AG7,0) + NVL(V_AMT_AG,0);
                else
                    V_AG_UNDEFY := NVL(V_AG_UNDEFY,0) + NVL(V_AMT_AG,0); 
                END IF;

         END IF;
        
    
     
     END LOOP; ---LOOP AMOUNT
     
     
     
     INSERT INTO TAC_ACCRUED_AGING_TEMP
     (COMPANY
     ,PROJECT_ID
     ,PROJECT_CODE
     ,PROJECT_NAME
     ,PO_HEADER_ID
     ,PO_LINE_ID
     ,PO_NO
     ,PO_LINE_NO
     ,TYPE_OF_WORK
     ,WIP_PREPAID
     ,AG1
     ,AG2
     ,AG3
     ,AG4
     ,AG5
     ,AG6
     ,AG7
     ,AG_UNDEFY
     ,AMOUNT_TOTAL
     ,REQUEST_ID
     ,creation_date)
     VALUES(REC1.COMPANY
     ,REC1.PROJECT_ID
     ,REC1.PROJECT_CODE
     ,REC1.PROJECT_NAME
     ,REC1.PO_HEADER_ID
     ,REC1.PO_LINE_ID
     ,REC1.PO_NO||REC1.RELEASE_NO
     ,REC1.PO_LINE_NO
     ,REC1.TYPE_OF_WORK
     ,REC1.WIP_PREPAID
     ,V_AG1
     ,V_AG2
     ,V_AG3
     ,V_AG4
     ,V_AG5
     ,V_AG6
     ,V_AG7
     ,V_AG_UNDEFY
     ,V_AMOUNT
     ,P_REQUEST_ID
     ,sysdate);
     COMMIT;
  end if;   
    
 END LOOP;
 
   INSERT INTO TAC_ACCRUED_AGING_TEMP
     (COMPANY
     ,REQUEST_ID
     ,CREATION_DATE)
     VALUES('END'
     ,P_REQUEST_ID
     ,SYSDATE);
     COMMIT;
     
 
 EXCEPTION WHEN OTHERS THEN
    NULL;
 END;
 
 
end TAC_WIP_ACC_REPORT_PKG;
/