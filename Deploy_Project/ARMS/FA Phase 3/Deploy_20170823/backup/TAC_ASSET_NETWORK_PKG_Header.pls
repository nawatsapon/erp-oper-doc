create or replace PACKAGE      TAC_ASSET_NETWORK_PKG is                             
FUNCTION GET_PRORATE_DATE(PIN_DATE_PLACE_IN_SERVICE IN DATE) RETURN DATE;
procedure tac_fa006_get_dir_list( p_directory in varchar2 );
PROCEDURE READ_TEXTFILE (pFILENAME IN VARCHAR,
                          pFOLDER_INBOX       IN VARCHAR2,
                          pFOLDER_ARCHIVE     IN VARCHAR2,
                          pFOLDER_ERROR       IN VARCHAR2);
PROCEDURE MAIN_MASS_ADD_API(errbuf               OUT VARCHAR2,
                            retcode              OUT NUMBER,
                            P_FILENAME           IN VARCHAR2,
                            P_DATE_PLACE_IN_SERVICE    IN VARCHAR2
                            );
PROCEDURE MAIN_MASS_ADD_INT(errbuf               OUT VARCHAR2,
                            retcode              OUT NUMBER,
                            P_FOLDER_INBOX       IN VARCHAR2,
                            P_FOLDER_ARCHIVE     IN VARCHAR2,
                            P_FOLDER_ERROR       IN VARCHAR2
                            );
PROCEDURE UPDATE_CLEARING(errbuf               OUT VARCHAR2,
                            retcode            OUT NUMBER,
                            P_PERIOD           IN VARCHAR2
                            );
  
end TAC_ASSET_NETWORK_PKG;
