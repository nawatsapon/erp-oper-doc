create or replace PACKAGE BODY      TAC_FA_POADD_PKG is

PROCEDURE WRITE_LOG (pCHAR IN VARCHAR2, pTYPE IN VARCHAR2)
   IS
   BEGIN
      IF UPPER (pTYPE) = 'LOG'
      THEN
         fnd_file.put_LINE (FND_FILE.LOG, pCHAR);
      ELSE
         fnd_file.put_line (FND_FILE.OUTPUT, pCHAR);
      END IF;

      DBMS_OUTPUT.put_line (Pchar);
   exception when others then
       DBMS_OUTPUT.put_line (sqlerrm);
   END WRITE_LOG;
FUNCTION CHECK_POADD_RP(P_PO_NUMBER IN VARCHAR2,
                        P_PO_LINE IN VARCHAR2
                         ) RETURN varchar2
is

V_DISPLAY VARCHAR2(1);

BEGIN

SELECT DISPLAY_REPORT
INTO V_DISPLAY
FROM TAC_PO_ADDITION_INFO
WHERE PO_NUMBER = P_PO_NUMBER
AND PO_LINE_NUMBER = P_PO_LINE
AND LAST_UPDATE_DATE = (SELECT MAX(LAST_UPDATE_DATE)
                        FROM TAC_PO_ADDITION_INFO
                        WHERE PO_NUMBER = P_PO_NUMBER
                        AND PO_LINE_NUMBER = P_PO_LINE);

IF V_DISPLAY = 'Y' OR V_DISPLAY IS NULL THEN
   RETURN 'Y';
ELSE
   RETURN 'N';
END IF;

EXCEPTION WHEN OTHERS THEN
    RETURN 'N';
END;
   
FUNCTION get_host_name RETURN VARCHAR2 IS
        l_host_name     varchar2(20);
BEGIN
        select UTL_INADDR.GET_HOST_NAME into l_host_name from dual;
        return l_host_name;
EXCEPTION WHEN OTHERS THEN
        RETURN NULL;
END;

FUNCTION GET_PATH(PIN_DIR_NAME IN VARCHAR2) RETURN VARCHAR2 IS
    vDIRPATH VARCHAR2(4000);
BEGIN
        SELECT DIRECTORY_PATH 
        INTO vDIRPATH
        from all_directories 
        where directory_name = PIN_DIR_NAME;
        
        RETURN(vDIRPATH);
EXCEPTION WHEN OTHERS THEN
       RETURN(NULL);
END;


PROCEDURE VALIDATE_DATA (pFILENAME IN VARCHAR2,
                         oVALIDATE_FLAG OUT VARCHAR2
                         )
   
IS

CURSOR F1 IS
SELECT * 
FROM TAC_FA_POADD_TEMP
WHERE FILE_NAME = pFILENAME
and validate_flag is null;

vCNTError NUMBER;
vERROR_MSG VARCHAR2(32767);
vNULL VARCHAR2(500);
vPERIOD_NAME varchar2(50);
vPO_NUMBER varchar2(50);
vPO_LINE_NUMBER varchar2(50);
vTYPE_OF_WORK VARCHAR2(2);
vPO_HEADER_ID number;
vGRN_ADJUST number;
vPROGRESS_AMOUNT number;
vPROGRESS_QTY number;



BEGIN

   
   

   FOR RECF1 IN F1
   LOOP
   
   vERROR_MSG := NULL;
   vNULL := NULL;
   
   
    IF RECF1.PERIOD_NAME IS NULL THEN
        vNULL := 'PERIOD_NAME';
    ELSIF RECF1.PO_NUMBER IS NULL THEN
        vNULL := vNULL||',PO_NUMBER';
    ELSIF RECF1.PO_LINE_NUMBER IS NULL THEN
        vNULL := vNULL||',PO_LINE_NUMBER';
    --ELSIF RECF1.TYPE_OF_WORK IS NULL THEN
    --    vNULL := vNULL||',TYPE_OF_WORK';
    --ELSIF RECF1.DISPLAY_PROJECT IS NULL THEN
    --    vNULL := vNULL||',DISPLAY_PROJECT';
   -- ELSIF RECF1.BOQ_REQUIRE IS NULL THEN
   --     vNULL := vNULL||',BOQ_REQUIRE';
    END IF;
    
    
    IF vNULL IS NOT NULL THEN
      vERROR_MSG := substr(vERROR_MSG ||',Error FA012-005 :Field '|| vNULL ||' is null',1,2000);
      --write_log(vERROR_MSG,'log');
     -- WRITE_LOG(',Error FA012-005 :Field '|| vNULL ||' is null','log');
    END IF;
    
    --==========================================================================--
    
    IF RECF1.PERIOD_NAME IS NOT NULL THEN        
        vPERIOD_NAME := null;        
        BEGIN
            SELECT DISTINCT 'Y'
            INTO vPERIOD_NAME
            FROM FA_DEPRN_PERIODS
            WHERE PERIOD_NAME =  RECF1.PERIOD_NAME;
        EXCEPTION WHEN OTHERS THEN
            null;
        END;
        
        if vPERIOD_NAME is null then
            vERROR_MSG := substr(vERROR_MSG || ',Error FA012-001 :Period Name  does not  exists  in erp',1,2000) ;
            --write_log(vERROR_MSG,'log');
        end if;
     
    END IF;
    
    
    
    IF RECF1.PO_NUMBER IS NOT NULL THEN
        vPO_NUMBER := NULL;
        BEGIN
            SELECT 'Y',po_header_id
            INTO vPO_NUMBER,vPO_HEADER_ID
            FROM PO_HEADERS_ALL 
            WHERE SEGMENT1 = RECF1.PO_NUMBER;
         EXCEPTION  WHEN OTHERS THEN
            null ;
        END;
           
        if vPO_NUMBER is null then    
          begin 
            SELECT DISTINCT 'Y',poh.po_header_id
            INTO vPO_NUMBER,vPO_HEADER_ID
            FROM PO_HEADERS_ALL POH,
            PO_LINES_ALL POL,
            PO_LINE_LOCATIONS_ALL  PLL,
            PO_RELEASES_ALL        PRA
            WHERE POH.SEGMENT1 || decode(pra.RELEASE_NUM,null,null,'-') || pra.RELEASE_NUM = RECF1.PO_NUMBER
            AND POH.PO_HEADER_ID =  POL.PO_HEADER_ID
            AND POL.PO_LINE_ID = PLL.PO_LINE_ID
            AND PLL.PO_HEADER_ID = PRA.PO_HEADER_ID(+)
            AND PLL.PO_RELEASE_ID = PRA.PO_RELEASE_ID(+);
        EXCEPTION  WHEN OTHERS THEN
            null ;
        END;
       end if;
        
        if vPO_NUMBER is null then
           vERROR_MSG := substr(vERROR_MSG || ',Error FA012-002 :PO Number  does not  exists  in erp',1,2000) ;
           --write_log(vERROR_MSG,'log');
        end if;
    
    END IF;
    
    
    IF RECF1.PO_LINE_NUMBER IS NOT NULL THEN
        vPO_LINE_NUMBER := NULL;
        BEGIN
            SELECT 'Y'
            INTO vPO_LINE_NUMBER
            FROM PO_HEADERS_ALL H,PO_LINES_ALL L 
            WHERE H.PO_HEADER_ID = L.PO_HEADER_ID
            --AND H.SEGMENT1 = RECF1.PO_NUMBER
            AND H.PO_HEADER_ID = vPO_HEADER_ID
            AND L.LINE_NUM = RECF1.PO_LINE_NUMBER;

        EXCEPTION WHEN OTHERS THEN
              null;
        END;
        
        if vPO_LINE_NUMBER is null then
           vERROR_MSG := substr(vERROR_MSG || ',Error FA012-012: PO Line Number  does not  exists  in erp',1,2000) ;
           --write_log(',Error FA012-003: PO Line Number  does not  exists  in erp','log');
           --write_log(vERROR_MSG,'log');
        end if;
    
    END IF;
    
    
    
    IF RECF1.TYPE_OF_WORK IS NOT NULL THEN
    
    
        vTYPE_OF_WORK := NULL;
        
       BEGIN
            select 'Y'
            INTO vTYPE_OF_WORK
            from fnd_flex_value_sets fvs,
            fnd_flex_values fv
            where fvs.FLEX_VALUE_SET_ID=fv.FLEX_VALUE_SET_ID
            and fvs.FLEX_VALUE_SET_NAME = 'DTN Type of work'
            and upper(fv.flex_value) = upper(RECF1.TYPE_OF_WORK) ;
       EXCEPTION WHEN OTHERS THEN
            null;     
       END;
       
       if vTYPE_OF_WORK is null then
           vERROR_MSG := substr(vERROR_MSG || ',Error FA012-004 : Type Of Work not in List',1,2000) ;
            --write_log(',Error FA012-004 : Type Of Work not in List','log');
            --write_log(vERROR_MSG,'log');
       end if;
       
    END IF;
    
    
    IF RECF1.GRN_ADJUST IS NOT NULL THEN
      
        vGRN_ADJUST := NULL;
        
       BEGIN
            select to_number(RECF1.GRN_ADJUST)
            INTO vGRN_ADJUST
            from dual ;
       EXCEPTION WHEN OTHERS THEN
            vERROR_MSG := substr(vERROR_MSG || ',Error FA012-012 : GRN_ADJUST is invalid number',1,2000) ;    
       END;
       
       
    END IF;
    
    IF RECF1.PROGRESS_AMOUNT IS NOT NULL THEN
      
        vPROGRESS_AMOUNT := NULL;
        
       BEGIN
            select to_number(RECF1.PROGRESS_AMOUNT)
            INTO vPROGRESS_AMOUNT
            from dual ;
       EXCEPTION WHEN OTHERS THEN
            vERROR_MSG := substr(vERROR_MSG || ',Error FA012-012 : PROGRESS_AMOUNT is invalid number',1,2000) ;    
       END;
       
       
    END IF;
    
    
    IF RECF1.PROGRESS_QTY IS NOT NULL THEN
      
        vPROGRESS_QTY := NULL;
        
       BEGIN
            select to_number(RECF1.PROGRESS_QTY)
            INTO vPROGRESS_QTY
            from dual ;
       EXCEPTION WHEN OTHERS THEN
            vERROR_MSG := substr(vERROR_MSG || ',Error FA012-012 : PROGRESS_QTY is invalid number',1,2000) ;    
       END;
       
       
    END IF;
    
    
    
    
    
    IF RECF1.DISPLAY_PROJECT IS NOT NULL AND RECF1.DISPLAY_PROJECT NOT IN ('Y','N') THEN 
       vERROR_MSG := substr(vERROR_MSG || ',Error FA012-006 : Invalid Display Project ',1,2000) ;
      -- write_log(vERROR_MSG,'log');
       --write_log(',Error FA012-006 : Invalid Display Project ','log');
    END IF;
     
    IF RECF1.BOQ_REQUIRE IS NOT NULL AND RECF1.BOQ_REQUIRE NOT IN ('Y','N') THEN 
       vERROR_MSG := substr(vERROR_MSG || ',Error FA012-007 : Invalid BOQ Require',1,2000) ;
      -- write_log(vERROR_MSG,'log');
       --write_log(',Error FA012-007 : Invalid BOQ Require','log');
    END IF; 
    
    
    IF RECF1.DISPLAY_REPORT IS NOT NULL AND RECF1.DISPLAY_REPORT NOT IN ('Y','N') THEN 
       vERROR_MSG := substr(vERROR_MSG || ',Error FA012-011 : Invalid Display Report',1,2000) ;
       --write_log(vERROR_MSG,'log');
       --write_log(',Error FA012-006 : Invalid Display Project ','log');
    END IF;   

    
     --write_log(vERROR_MSG,'log');
    
    
   IF vERROR_MSG IS NOT NULL THEN
     UPDATE TAC_FA_POADD_TEMP
     SET VALIDATE_FLAG = 'E'
     ,ERROR_MESSAGE = substr(vERROR_MSG,1,2000)
     WHERE (PO_NUMBER = RECF1.PO_NUMBER OR RECF1.PO_NUMBER IS NULL)
     AND (PO_LINE_NUMBER = RECF1.PO_LINE_NUMBER OR RECF1.PO_LINE_NUMBER IS NULL)
     AND (PERIOD_NAME = RECF1.PERIOD_NAME OR RECF1.PERIOD_NAME IS NULL)
     AND FILE_NAME = pFILENAME
     AND RUNNING_NO = RECF1.RUNNING_NO;
     COMMIT;
   
   ELSE
     UPDATE TAC_FA_POADD_TEMP
     SET VALIDATE_FLAG = 'S'
     WHERE (PO_NUMBER = RECF1.PO_NUMBER OR RECF1.PO_NUMBER IS NULL)
     AND (PO_LINE_NUMBER = RECF1.PO_LINE_NUMBER OR RECF1.PO_LINE_NUMBER IS NULL)
     AND (PERIOD_NAME = RECF1.PERIOD_NAME OR RECF1.PERIOD_NAME IS NULL)
     AND FILE_NAME = pFILENAME
     AND RUNNING_NO = RECF1.RUNNING_NO
     ;
     COMMIT;
   END IF;
   


   END LOOP;
   
   
   vCNTError := 0;
  begin
    select count(*)
    into vCNTError
    from TAC_FA_POADD_TEMP
    where VALIDATE_FLAG = 'E'
    and file_name = pFILENAME;
  exception when others then
    null;
  end;
   
   
  IF vCNTError > 0 THEN
   oVALIDATE_FLAG := 'E';
  ELSE 
   oVALIDATE_FLAG := 'S';
  END IF;
   
END;

FUNCTION extrack_data(p_buffer IN varchar2
                        ,p_delimiter IN varchar2
                        ,p_pos_data IN number
                        ) RETURN varchar2 IS
 v_pos_start number;
 v_pos_end number;
 v_subsrt_len number;
 v_data varchar2(255);
 
BEGIN
    IF p_pos_data = 1 THEN
        v_pos_start := 1;
    ELSE
        v_pos_start := INSTR(p_buffer, p_delimiter, 1, p_pos_data - 1) + 1;
    END IF;

        v_pos_end := INSTR(p_buffer, p_delimiter, 1, p_pos_data);

    IF v_pos_end = 0 THEN
        v_subsrt_len := LENGTH(p_buffer);
    ELSE
        v_subsrt_len := v_pos_end - v_pos_start;
    END IF;
        
        v_data := SUBSTR(p_buffer, v_pos_start, v_subsrt_len);

    
    RETURN REPLACE(REPLACE(TRIM(v_data), CHR(10), ''),CHR(13),'');

EXCEPTION
 WHEN OTHERS THEN
   --write_log('[ERR] : @extrack_data : POS' || p_pos_data || ' : ' || SQLERRM,'log');
   RETURN NULL;
END extrack_data;

 procedure tac_fa012_get_dir_list( p_directory in varchar2 )
 as language java
 name 'TACFA012DirList.getList( java.lang.String )';
 
 
 PROCEDURE READ_TEXTFILE (pFILENAME IN VARCHAR,
                          pFOLDER_INBOX       IN VARCHAR2,
                          pFOLDER_ARCHIVE     IN VARCHAR2,
                          pFOLDER_ERROR       IN VARCHAR2)
 IS
   in_file        UTL_FILE.file_type;
   datarow        VARCHAR2 (32767);
   REC_TEMP TAC_FA_POADD_TEMP%ROWTYPE;
   l_host_name    varchar2(20) := get_host_name;
   i number := 0;
 
BEGIN
     in_file := UTL_FILE.fopen (pFOLDER_INBOX,pFILENAME, 'R');
     datarow := NULL;
     
     
     DELETE FROM TAC_FA_POADD_TEMP
     WHERE FILE_NAME = pFILENAME;
     commit;
     
     LOOP
               
                  UTL_FILE.get_line (in_file, datarow);
                  
                  datarow := REPLACE(REPLACE(datarow, CHR(10), ''), CHR(13), '');
                  
                  i := i+1;
                 -- write_log('i='||i,'out');
  
                  /* IF datarow IS NULL
                  THEN
                     WRITE_LOG ('END FILE', 'Log');
                     EXIT;
                  END IF;*/
                  
             IF datarow IS NULL
                  THEN
                    null;
             else
             
               --   MAY-17|8015000234|2|s|1000|3700|370|Pornpairin Chaiwiwat||||Y
                  
                REC_TEMP.PERIOD_NAME                    := extrack_data(datarow,'|', 1);
                REC_TEMP.PO_NUMBER                      := extrack_data(datarow,'|', 2);
                REC_TEMP.PO_LINE_NUMBER                 := extrack_data(datarow,'|', 3);
                REC_TEMP.TYPE_OF_WORK                   := extrack_data(datarow,'|', 4);
                REC_TEMP.GRN_ADJUST                     := extrack_data(datarow,'|', 5); --to_number(extrack_data(datarow,'|', 5));
                REC_TEMP.PROGRESS_AMOUNT                := extrack_data(datarow,'|', 6); --to_number(extrack_data(datarow,'|', 6));
                REC_TEMP.PROGRESS_QTY                   := extrack_data(datarow,'|', 7); --to_number(extrack_data(datarow,'|', 7));
               -- REC_TEMP.SYSTEM_CODE                    := extrack_data(datarow,'|', 8);
                REC_TEMP.FOLLOW_UP_WITH                 := extrack_data(datarow,'|', 8);
                REC_TEMP.DISPLAY_PROJECT                := extrack_data(datarow,'|', 9);   --  including in report
                REC_TEMP.BOQ_REQUIRE                    := extrack_data(datarow,'|', 10);
                REC_TEMP.STRIM_CODE                     := extrack_data(datarow,'|', 11);
                REC_TEMP.DISPLAY_REPORT                 := extrack_data(datarow,'|', 12);
                REC_TEMP.CREATION_DATE                  := sysdate;
                REC_TEMP.CREATED_BY                     := FND_PROFILE.VALUE('USER_ID');
                --REC_TEMP.LAST_UPDATE_DATE               := extrack_data(datarow,'|', 15);
                --REC_TEMP.LAST_UPDATE_BY                 := to_number(extrack_data(datarow,'|', 16));
                REC_TEMP.FILE_NAME                      := pFILENAME;
                
                
                -- write_log('REC_TEMP.PO_NUMBER = ' || REC_TEMP.PO_NUMBER ,'out');
                
                --write_log(REC_TEMP.STRIM_CODE,'log');                                
                INSERT INTO TAC_FA_POADD_TEMP(PERIOD_NAME,
                PO_NUMBER                ,
                PO_LINE_NUMBER           ,
                TYPE_OF_WORK             ,
                GRN_ADJUST               ,
                PROGRESS_AMOUNT          ,
                PROGRESS_QTY             ,
                --SYSTEM_CODE              ,
                FOLLOW_UP_WITH           ,
                DISPLAY_PROJECT          ,
                BOQ_REQUIRE              ,
                STRIM_CODE               ,
                DISPLAY_REPORT           ,
                CREATION_DATE            ,
                CREATED_BY               ,
                FILE_NAME                ,
                RUNNING_NO
                )
                VALUES(REC_TEMP.PERIOD_NAME,
                REC_TEMP.PO_NUMBER         ,
                REC_TEMP.PO_LINE_NUMBER    ,
                REC_TEMP.TYPE_OF_WORK      ,
                REC_TEMP.GRN_ADJUST        ,
                REC_TEMP.PROGRESS_AMOUNT   ,
                REC_TEMP.PROGRESS_QTY      ,
               -- REC_TEMP.SYSTEM_CODE       ,
                REC_TEMP.FOLLOW_UP_WITH    ,
                REC_TEMP.DISPLAY_PROJECT   ,
                REC_TEMP.BOQ_REQUIRE       ,
                REC_TEMP.STRIM_CODE        ,
                REC_TEMP.DISPLAY_REPORT    ,
                REC_TEMP.CREATION_DATE     ,
                REC_TEMP.CREATED_BY        ,
                REC_TEMP.FILE_NAME         ,
                i                   
                ); 

                COMMIT;    
                  
             end if;  --- end check record null    

END LOOP;
     
    EXCEPTION 
               WHEN no_data_found
               THEN

               NULL;
                 -- WRITE_LOG ('end file', 'LOG');
                  /*** Move file to directory loaded*/
           --UTL_FILE.FCOPY (pFOLDER_INBOX,pFILENAME,pFOLDER_ARCHIVE,pFILENAME);
           --UTL_FILE.FREMOVE (pFOLDER_INBOX,pFILENAME);
     
               WHEN OTHERS
               THEN
               
           WRITE_LOG(to_char(sysdate,'DD-MON-YY HH24:MI:SS')||'|'||
                to_char(sysdate,'DD-MON-YY HH24:MI:SS')||'|'||
                l_host_name||'|'||  
                'TAC : Interface PO Additional Information'||'|'||
                pFILENAME||'|'||
                ''||'|'||
                ''||'|'||
                ''||'|'||
                ''||'|'||
                'E'||'|'||
                'Error FA012-010 : Cannot Read File'
                ,'LOG');
                 -- INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Others Error : ',SYSDATE,'N');                  
                   /*** Move file to directory ERROR*/
           --UTL_FILE.FCOPY (pFOLDER_INBOX,pFILENAME,pFOLDER_ERROR,pFILENAME);
           --UTL_FILE.FREMOVE (pFOLDER_INBOX,pFILENAME);
     END; 

PROCEDURE MAIN_POADD_INT(errbuf               OUT VARCHAR2,
                            retcode              OUT NUMBER,
                            P_FOLDER_INBOX       IN VARCHAR2,
                            P_FOLDER_ARCHIVE     IN VARCHAR2,
                            P_FOLDER_ERROR       IN VARCHAR2
                            )
IS

   CURSOR CUR_FILE
   IS   
       SELECT FILENAME
       FROM TAC_FA012_INBOX
       ORDER BY FILENAME 
       ;


  CURSOR CUR_HEADERS(vFILENAME VARCHAR2)  IS
     SELECT *
     FROM TAC_FA_POADD_TEMP T
     WHERE 1=1
     AND FILE_NAME = vFILENAME
     AND NVL(T.VALIDATE_FLAG,'N') IN ('S')
     AND NVL(T.PROCESS_FLAG,'N') IN ('N')
     ORDER BY RUNNING_NO,PO_NUMBER;
     
     
    CURSOR CUR_LOG(vFILENAME VARCHAR2)  IS
     SELECT *
     FROM TAC_FA_POADD_TEMP T
     WHERE 1=1
     AND FILE_NAME = vFILENAME
     AND NVL(T.VALIDATE_FLAG,'N') IN ('S','E')
     --AND NVL(T.PROCESS_FLAG,'N') IN ('S','E')
     ORDER BY RUNNING_NO,PO_NUMBER;
     
     
    
vCNT_FILE NUMBER;
vVALIDATE_FLAG VARCHAR2(1);
vBeginDate  VARCHAR2(20);
vEndDate  VARCHAR2(20);
l_host_name             varchar2(20) := get_host_name;
vCNT_RECORD NUMBER;
vCNT_RECORD_ERR NUMBER;
vCNT_RECORD_COMP NUMBER;
l_chk_dup NUMBER;
l_chk_dup_po number;
vPROCESS_FLAG VARCHAR2(1);
vERROR_MSG VARCHAR2(2000);
vPO_HEADER_ID NUMBER;
v_error  varchar2(1);


 vPATH_INBOX       VARCHAR2 (5120);
 vPATH_ARCHIVE     VARCHAR2 (5120);
 vPATH_ERROR       VARCHAR2 (5120);

BEGIN


  vPATH_INBOX := GET_PATH(P_FOLDER_INBOX);
  vPATH_ARCHIVE := GET_PATH(P_FOLDER_ARCHIVE);
  vPATH_ERROR := GET_PATH(P_FOLDER_ERROR);


IF vPATH_INBOX IS NULL 
OR vPATH_ARCHIVE IS NULL 
OR vPATH_ERROR IS NULL 
THEN
     WRITE_LOG(to_char(sysdate,'DD-MON-YY HH24:MI:SS')||'|'||
                to_char(sysdate,'DD-MON-YY HH24:MI:SS')||'|'||
                l_host_name||'|'||  
                'TAC : Interface PO Additional Information'||'|'||
                ''||'|'||
                ''||'|'||
                ''||'|'||
                ''||'|'||
                ''||'|'||
                'E'||'|'||
                'Error FA012-008 : Not found Path '
                ,'LOG');
   
 ELSE 
 
    delete from tac_fa012_dir_list;
    commit;
    
    DELETE FROM TAC_FA_POADD_TEMP
    WHERE trunc(CREATION_DATE) < TRUNC(SYSDATE-60);
    COMMIT;
    
    v_error := null;
    
    begin
      tac_fa012_get_dir_list(vPATH_INBOX);   
    exception when others then
       v_error := 'E';
    end;

  if v_error = 'E' then   
  
     WRITE_LOG(to_char(sysdate,'DD-MON-YY HH24:MI:SS')||'|'||
                to_char(sysdate,'DD-MON-YY HH24:MI:SS')||'|'||
                l_host_name||'|'||  
                'TAC : Interface PO Additional Information'||'|'||
                ''||'|'||
                ''||'|'||
                ''||'|'||
                ''||'|'||
                ''||'|'||
                'E'||'|'||
                'Error FA012-008 : Not found Path '
                ,'LOG');
   else
    
    DELETE FROM TAC_FA012_INBOX;
    COMMIT;
    INSERT INTO TAC_FA012_INBOX (SELECT * FROM tac_fa012_dir_list);
    COMMIT;
    
    vCNT_FILE := 0;
    
   BEGIN 
    select count(*)
    into vCNT_FILE
    from TAC_FA012_INBOX;
   EXCEPTION WHEN OTHERS THEN
     vCNT_FILE := 0;
   END; 
   
  IF vCNT_FILE = 0 THEN
    WRITE_LOG(to_char(sysdate,'DD-MON-YY HH24:MI:SS')||'|'||
                to_char(sysdate,'DD-MON-YY HH24:MI:SS')||'|'||
                l_host_name||'|'||  
                'TAC : Interface PO Additional Information'||'|'||
                ''||'|'||
                ''||'|'||
                ''||'|'||
                ''||'|'||
                ''||'|'||
                'E'||'|'||
                'Error FA012-009 : Zero File'
                ,'LOG');
else   
  
 FOR REC_FILE IN CUR_FILE
 LOOP
    
            vBeginDate := to_char(sysdate,'DD-MON-YY HH24:MI:SS');
        
          --  write_log('step1 - start read textfile','out');
            READ_TEXTFILE (pFILENAME => REC_FILE.FILENAME,
                           pFOLDER_INBOX    => P_FOLDER_INBOX,
                           pFOLDER_ARCHIVE  => P_FOLDER_ARCHIVE,
                           pFOLDER_ERROR    => P_FOLDER_ERROR);
            commit;
     
     
     
         -- write_log('step2 - validate data','out');
          VALIDATE_DATA (pFILENAME => REC_FILE.FILENAME,
                    oVALIDATE_FLAG => vVALIDATE_FLAG
                    );
    
   --===========================================================--   
   
   if vVALIDATE_FLAG = 'E' then
       null;
   else
      FOR RECH1 IN CUR_HEADERS(REC_FILE.FILENAME)
      LOOP
      
                vPROCESS_FLAG := NULL;
                vERROR_MSG := NULL;
                l_chk_dup := 0;
                l_chk_dup_po := 0;
                
                
                 BEGIN
                    select distinct 1
                    into l_chk_dup
                    from TAC_PO_ADDITION_INFO
                    where PERIOD_NAME = RECH1.PERIOD_NAME
                    AND PO_NUMBER             = RECH1.PO_NUMBER
                    AND PO_LINE_NUMBER        = RECH1.PO_LINE_NUMBER
                    ;
                 EXCEPTION WHEN NO_DATA_FOUND THEN
                    l_chk_dup := 0;
                    WHEN OTHERS THEN
                    l_chk_dup := 0;
                 END;  
                 
                 
                 BEGIN
                    select distinct 1
                    into l_chk_dup_po
                    from TAC_PO_ADDITION_INFO
                    where PO_NUMBER             = RECH1.PO_NUMBER
                    AND PO_LINE_NUMBER        = RECH1.PO_LINE_NUMBER
                    ;
                 EXCEPTION WHEN NO_DATA_FOUND THEN
                    l_chk_dup_po := 0;
                    WHEN OTHERS THEN
                    l_chk_dup_po := 0;
                 END;  
                 
                 
                 IF l_chk_dup = 0 and l_chk_dup_po = 1 THEN
                 
                  BEGIN
                    INSERT INTO TAC_PO_ADDITION_INFO(PERIOD_NAME
                                                     ,PO_NUMBER
                                                     ,PO_LINE_NUMBER
                                                     ,TYPE_OF_WORK
                                                     ,GRN_ADJUST
                                                     ,PROGRESS_AMOUNT
                                                     ,PROGRESS_QTY
                                                     ,SYSTEM_CODE
                                                     ,FOLLOW_UP_WITH
                                                     ,DISPLAY_PROJECT
                                                     ,BOQ_REQUIRE
                                                     ,STRIM_CODE
                                                     ,DISPLAY_REPORT
                                                     ,CREATION_DATE
                                                     ,CREATED_BY
                                                     ,LAST_UPDATE_DATE
                                                     ,LAST_UPDATE_BY
                                                     )
                                                        SELECT RECH1.PERIOD_NAME
                                                            ,RECH1.PO_NUMBER
                                                            ,RECH1.PO_LINE_NUMBER
                                                            ,nvl(RECH1.TYPE_OF_WORK,TYPE_OF_WORK)
                                                            ,NVL(RECH1.GRN_ADJUST,nvl(GRN_ADJUST,0))
                                                            ,NVL(RECH1.PROGRESS_AMOUNT,nvl(PROGRESS_AMOUNT,0))
                                                            ,NVL(RECH1.PROGRESS_QTY,nvl(PROGRESS_QTY,0))
                                                            ,nvl(RECH1.SYSTEM_CODE,SYSTEM_CODE)
                                                            ,nvl(RECH1.FOLLOW_UP_WITH,FOLLOW_UP_WITH)
                                                            ,nvl(RECH1.DISPLAY_PROJECT,DISPLAY_PROJECT)
                                                            ,nvl(RECH1.BOQ_REQUIRE,BOQ_REQUIRE)
                                                            ,nvl(RECH1.STRIM_CODE,STRIM_CODE)
                                                            ,nvl(RECH1.DISPLAY_REPORT,DISPLAY_REPORT)
                                                            ,SYSDATE
                                                            ,FND_PROFILE.VALUE('USER_ID')
                                                            ,SYSDATE
                                                            ,FND_PROFILE.VALUE('USER_ID')
                                                        FROM TAC_PO_ADDITION_INFO
                                                        WHERE PO_NUMBER = RECH1.PO_NUMBER
                                                        AND PO_LINE_NUMBER = RECH1.PO_LINE_NUMBER
                                                        AND TO_DATE(PERIOD_NAME,'MON-YY') = (SELECT MAX(to_date(PERIOD_NAME,'MON-YY'))
                                                                                                FROM TAC_PO_ADDITION_INFO
                                                                                                WHERE PO_NUMBER = RECH1.PO_NUMBER
                                                                                                AND PO_LINE_NUMBER = RECH1.PO_LINE_NUMBER);
                                                    
                                                     COMMIT; 
                                              
                  EXCEPTION WHEN OTHERS THEN
                   
                    vERROR_MSG :=  sqlerrm;
                  END;
                  
                 elsif l_chk_dup = 0 and l_chk_dup_po = 0 then 
                    begin
                     INSERT INTO TAC_PO_ADDITION_INFO(PERIOD_NAME
                                                     ,PO_NUMBER
                                                     ,PO_LINE_NUMBER
                                                     ,TYPE_OF_WORK
                                                     ,GRN_ADJUST
                                                     ,PROGRESS_AMOUNT
                                                     ,PROGRESS_QTY
                                                     ,SYSTEM_CODE
                                                     ,FOLLOW_UP_WITH
                                                     ,DISPLAY_PROJECT
                                                     ,BOQ_REQUIRE
                                                     ,STRIM_CODE
                                                     ,DISPLAY_REPORT
                                                     ,CREATION_DATE
                                                     ,CREATED_BY
                                                     ,LAST_UPDATE_DATE
                                                     ,LAST_UPDATE_BY
                                                     )
                                                VALUES
                                                     (RECH1.PERIOD_NAME
                                                     ,RECH1.PO_NUMBER
                                                     ,RECH1.PO_LINE_NUMBER
                                                     ,RECH1.TYPE_OF_WORK
                                                     ,NVL(RECH1.GRN_ADJUST,0)
                                                     ,NVL(RECH1.PROGRESS_AMOUNT,0)
                                                     ,NVL(RECH1.PROGRESS_QTY,0)
                                                     ,RECH1.SYSTEM_CODE
                                                     ,RECH1.FOLLOW_UP_WITH
                                                     ,NVL(RECH1.DISPLAY_PROJECT,'Y')
                                                     ,RECH1.BOQ_REQUIRE
                                                     ,RECH1.STRIM_CODE
                                                     ,RECH1.DISPLAY_REPORT
                                                     ,SYSDATE
                                                     ,FND_PROFILE.VALUE('USER_ID')
                                                     ,SYSDATE
                                                     ,FND_PROFILE.VALUE('USER_ID')
                                                     );
                                                     COMMIT;
                                                     
                    EXCEPTION WHEN OTHERS THEN
                   
                       vERROR_MSG :=  sqlerrm;
                    END;
                 
                 
                 ELSE
                                 BEGIN
                                            UPDATE TAC_PO_ADDITION_INFO
                                                    SET TYPE_OF_WORK = NVL(RECH1.TYPE_OF_WORK,TYPE_OF_WORK)
                                                     ,GRN_ADJUST = NVL(RECH1.GRN_ADJUST,GRN_ADJUST)
                                                     ,PROGRESS_AMOUNT = NVL(RECH1.PROGRESS_AMOUNT,PROGRESS_AMOUNT)
                                                     ,PROGRESS_QTY = NVL(RECH1.PROGRESS_QTY,PROGRESS_QTY)
                                                    -- ,SYSTEM_CODE = NVL(RECH1.SYSTEM_CODE,SYSTEM_CODE)
                                                     ,FOLLOW_UP_WITH = NVL(RECH1.FOLLOW_UP_WITH,FOLLOW_UP_WITH)
                                                     ,DISPLAY_PROJECT = NVL(RECH1.DISPLAY_PROJECT,DISPLAY_PROJECT)
                                                     ,BOQ_REQUIRE = NVL(RECH1.BOQ_REQUIRE,BOQ_REQUIRE)
                                                     ,STRIM_CODE = NVL(RECH1.STRIM_CODE,STRIM_CODE)
                                                     ,DISPLAY_REPORT = NVL(RECH1.DISPLAY_REPORT,DISPLAY_REPORT)
                                                     ,LAST_UPDATE_DATE = SYSDATE
                                                     ,LAST_UPDATE_BY = FND_PROFILE.VALUE('USER_ID')
                    
                                            WHERE PERIOD_NAME = RECH1.PERIOD_NAME
                                            AND PO_NUMBER = RECH1.PO_NUMBER
                                            AND PO_LINE_NUMBER = RECH1.PO_LINE_NUMBER
                                            AND PERIOD_NAME = RECH1.PERIOD_NAME;
                                            COMMIT; 
                                            
                                                                 
                                EXCEPTION WHEN OTHERS THEN
                                    
                                    vERROR_MSG := vERROR_MSG || ',' || sqlerrm;
                                END;
                 
                 END IF; 
                 
           /* vPO_HEADER_ID := NULL;
                 
            BEGIN
                SELECT PO_HEADER_ID 
                INTO vPO_HEADER_ID
                FROM PO_HEADERS_ALL
                WHERE SEGMENT1 =  RECH1.PO_NUMBER;
            EXCEPTION WHEN OTHERS THEN
                NULL;
            END;
            
            BEGIN
            
             UPDATE PO_LINES_ALL
             SET ATTRIBUTE_CATEGORY = 'DTN Information'
             ,ATTRIBUTE1 =  NVL(RECH1.TYPE_OF_WORK,ATTRIBUTE1)
             ,ATTRIBUTE2 =  NVL(RECH1.GRN_ADJUST,ATTRIBUTE2)
             ,ATTRIBUTE3 =  NVL(RECH1.PROGRESS_AMOUNT,ATTRIBUTE3)
             ,ATTRIBUTE4 =  NVL(RECH1.PROGRESS_QTY,ATTRIBUTE4)
             ,ATTRIBUTE5 =  NVL(RECH1.SYSTEM_CODE,ATTRIBUTE5)
             ,ATTRIBUTE6 =  NVL(RECH1.FOLLOW_UP_WITH,ATTRIBUTE6)
             ,ATTRIBUTE7 =  NVL(RECH1.DISPLAY_PROJECT,ATTRIBUTE7)
             ,ATTRIBUTE8 =  NVL(RECH1.STRIM_CODE,ATTRIBUTE8)
             ,ATTRIBUTE9 =  NVL(RECH1.BOQ_REQUIRE,ATTRIBUTE9)
             WHERE PO_HEADER_ID = vPO_HEADER_ID
             AND LINE_NUM = RECH1.PO_LINE_NUMBER;
             
             COMMIT;
            
            
            
            EXCEPTION WHEN OTHERS THEN
                                    
                  vERROR_MSG := vERROR_MSG || ',' || sqlerrm;
             END;
              */  
                 
                  
           IF vERROR_MSG IS NOT NULL THEN
                 
                UPDATE  TAC_FA_POADD_TEMP   
                SET PROCESS_FLAG = 'E'
                ,PROCESS_ERROR_MESSAGE = vERROR_MSG
                WHERE PERIOD_NAME = RECH1.PERIOD_NAME
                AND PO_NUMBER = RECH1.PO_NUMBER
                AND PO_LINE_NUMBER = RECH1.PO_LINE_NUMBER
                AND PERIOD_NAME = RECH1.PERIOD_NAME;
                COMMIT;
           ELSE
                UPDATE  TAC_FA_POADD_TEMP   
                SET PROCESS_FLAG = 'S'
                WHERE PERIOD_NAME = RECH1.PERIOD_NAME
                AND PO_NUMBER = RECH1.PO_NUMBER
                AND PO_LINE_NUMBER = RECH1.PO_LINE_NUMBER
                AND PERIOD_NAME = RECH1.PERIOD_NAME;
                COMMIT;
           END IF;
     
      END LOOP;
    
 end if; -- end pass validate flag 
  
    --===========================================================--  
      
      vEndDate := to_char(sysdate,'DD-MON-YY HH24:MI:SS');
      vCNT_RECORD := 0;
      vCNT_RECORD_ERR := 0;
      vCNT_RECORD_COMP := 0;
      
   FOR REC_LOG IN CUR_LOG(REC_FILE.FILENAME)

    LOOP
      vCNT_RECORD := vCNT_RECORD + 1;
      
      IF REC_LOG.VALIDATE_FLAG = 'E' or REC_LOG.PROCESS_FLAG = 'E' THEN
         vCNT_RECORD_ERR := vCNT_RECORD_ERR +1 ;
      ELSE
         vCNT_RECORD_COMP := vCNT_RECORD_COMP +1 ;
      END IF;

      WRITE_LOG(vBeginDate||'|'||
                vEndDate||'|'||
                l_host_name||'|'||  
                'TAC : Interface PO Additional Information'||'|'||
                REC_LOG.FILE_NAME||'|'||
                vCNT_RECORD||'|'||
                REC_LOG.PERIOD_NAME||'|'||
                REC_LOG.PO_NUMBER||'|'||
                REC_LOG.PO_LINE_NUMBER||'|'||
                REC_LOG.VALIDATE_FLAG||'|'||
                REC_LOG.ERROR_MESSAGE
                ,'LOG');
    END LOOP;
    
    if vCNT_RECORD = vCNT_RECORD_COMP  and vCNT_RECORD > 0 then
         UTL_FILE.FCOPY (p_FOLDER_INBOX,REC_FILE.FILENAME,p_FOLDER_ARCHIVE,REC_FILE.FILENAME);
         UTL_FILE.FREMOVE (p_FOLDER_INBOX,REC_FILE.FILENAME);
    else
         UTL_FILE.FCOPY (p_FOLDER_INBOX,REC_FILE.FILENAME,p_FOLDER_ERROR,REC_FILE.FILENAME);
         UTL_FILE.FREMOVE (p_FOLDER_INBOX,REC_FILE.FILENAME);
    end if;
    
    WRITE_LOG('=======================================================','LOG');
    WRITE_LOG('TOTAL RECORD = '||vCNT_RECORD,'LOG');
    WRITE_LOG('COMPLETE = '||vCNT_RECORD_COMP,'LOG');
    WRITE_LOG('ERROR = '||vCNT_RECORD_ERR,'LOG');
 
END LOOP; --LOOP FILE

END IF; -- end zero file 
END IF; --end not found path 
end if; --end not found path 
END;
 
end TAC_FA_POADD_PKG;
