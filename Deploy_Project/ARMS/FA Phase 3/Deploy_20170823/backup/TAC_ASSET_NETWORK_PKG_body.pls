create or replace PACKAGE BODY      TAC_ASSET_NETWORK_PKG is

PROCEDURE WRITE_LOG (pCHAR IN VARCHAR2, pTYPE IN VARCHAR2)
   IS
   BEGIN
      IF UPPER (pTYPE) = 'LOG'
      THEN
         fnd_file.put_LINE (FND_FILE.LOG, pCHAR);
      ELSE
         fnd_file.put_line (FND_FILE.OUTPUT, pCHAR);
      END IF;

      DBMS_OUTPUT.put_line (Pchar);
   exception when others then
       DBMS_OUTPUT.put_line (sqlerrm);
   END WRITE_LOG;



FUNCTION get_host_name RETURN VARCHAR2 IS
        l_host_name     varchar2(100);
BEGIN
        select UTL_INADDR.GET_HOST_NAME into l_host_name from dual;
        return l_host_name;
EXCEPTION WHEN OTHERS THEN
        RETURN NULL;
END;

FUNCTION GET_PATH(PIN_DIR_NAME IN VARCHAR2) RETURN VARCHAR2 IS
    vDIRPATH VARCHAR2(4000);
BEGIN
        SELECT DIRECTORY_PATH 
        INTO vDIRPATH
        from all_directories 
        where directory_name = PIN_DIR_NAME;
        
        RETURN(vDIRPATH);
EXCEPTION WHEN OTHERS THEN
       RETURN(NULL);
END;

FUNCTION GET_PROJECT_ID (PIN_PROJECT_CODE IN VARCHAR2) RETURN NUMBER

IS
vPROJECT_ID NUMBER;
BEGIN
    
    SELECT PROJECT_ID
    INTO vPROJECT_ID
    FROM PA_PROJECTS_ALL
    WHERE SEGMENT1 = PIN_PROJECT_CODE;
    
    RETURN vPROJECT_ID;

EXCEPTION WHEN OTHERS THEN
    RETURN NULL;
END;

procedure GET_VENDOR(PIN_VENDOR_CODE IN VARCHAR2,
                    VVENDOR_ID out number,
                    vVENDOR_NAME out VARCHAR2) IS
    --VVENDOR_ID NUMBER;
    
BEGIN
    SELECT  VENDOR_ID,VENDOR_NAME
    INTO    VVENDOR_ID,vVENDOR_NAME
    FROM    PO_VENDORS
    WHERE   SEGMENT1 = PIN_VENDOR_CODE
    AND TRUNC(NVL(END_DATE_ACTIVE,SYSDATE)) >= TRUNC(SYSDATE);
    --RETURN(VVENDOR_ID);
EXCEPTION WHEN OTHERS THEN
    --RETURN(NULL);
    VVENDOR_ID := null;
    vVENDOR_NAME := null;
END GET_VENDOR;

/*FUNCTION GET_VENDOR_NAME(PIN_VENDOR_CODE IN VARCHAR2) RETURN VARCHAR2 IS
    vVENDOR_NAME VARCHAR2(250);
    
BEGIN
    SELECT  VENDOR_NAME
    INTO    vVENDOR_NAME
    FROM    PO_VENDORS
    WHERE   SEGMENT1 = PIN_VENDOR_CODE
    AND TRUNC(NVL(END_DATE_ACTIVE,SYSDATE)) >= TRUNC(SYSDATE);
    RETURN(vVENDOR_NAME);
EXCEPTION WHEN OTHERS THEN
    RETURN(NULL);
END GET_VENDOR_NAME;
*/

FUNCTION GET_LOCATION(PIN_LOCATION IN VARCHAR2) RETURN NUMBER IS
    VLOCATION_ID NUMBER;
BEGIN
    SELECT LOCATION_ID 
    INTO VLOCATION_ID
    FROM FA_LOCATIONS 
    WHERE SEGMENT1||'-'||SEGMENT2||'-N' = PIN_LOCATION || '-N';
    return(VLOCATION_ID);
EXCEPTION WHEN OTHERS THEN
    RETURN(NULL);
END GET_LOCATION;

FUNCTION GET_EXPENSE_ACCT(PIN_CATEGORY_ID IN NUMBER,PIN_BOOK_TYPE_CODE VARCHAR2) RETURN NUMBER IS
    vDEPRN_EXPENSE_ACCT NUMBER;
    vDEFAULTS_CCID NUMBER;
    vSEGMENT1 VARCHAR2(10);
    vcode_combination_id NUMBER;
BEGIN
    
    select FLEXBUILDER_DEFAULTS_CCID 
    INTO vDEFAULTS_CCID
    from FA_BOOK_CONTROLS
    where BOOK_TYPE_CODE = PIN_BOOK_TYPE_CODE;
    
    SELECT SEGMENT1
    INTO vSEGMENT1 
    FROM GL_CODE_COMBINATIONS
    WHERE CODE_COMBINATION_ID = vDEFAULTS_CCID;

    select DEPRN_EXPENSE_ACCT 
    INTO vDEPRN_EXPENSE_ACCT
    from FA_CATEGORY_BOOKS
    WHERE CATEGORY_ID = PIN_CATEGORY_ID
    AND BOOK_TYPE_CODE = PIN_BOOK_TYPE_CODE;
    
     SELECT code_combination_id
     INTO vcode_combination_id
     FROM gl_code_combinations_kfv
     WHERE concatenated_segments =vSEGMENT1||'.'||vDEPRN_EXPENSE_ACCT||'.0000.00000.000000.00.000.000000.000000';
    
    return(vcode_combination_id);
    
EXCEPTION WHEN OTHERS THEN
    RETURN(NULL);
END GET_EXPENSE_ACCT;


FUNCTION GET_CLEARING_ACCT(PIN_CLEARING IN VARCHAR2,PIN_BOOK_TYPE_CODE VARCHAR2) RETURN NUMBER IS

    vDEFAULTS_CCID NUMBER;
    vSEGMENT1 VARCHAR2(10);
    vcode_combination_id NUMBER;
BEGIN
    
    select FLEXBUILDER_DEFAULTS_CCID 
    INTO vDEFAULTS_CCID
    from FA_BOOK_CONTROLS
    where BOOK_TYPE_CODE = PIN_BOOK_TYPE_CODE;
    
    SELECT SEGMENT1
    INTO vSEGMENT1 
    FROM GL_CODE_COMBINATIONS
    WHERE CODE_COMBINATION_ID = vDEFAULTS_CCID;
    
     SELECT code_combination_id
     INTO vcode_combination_id
     FROM gl_code_combinations_kfv
     WHERE concatenated_segments =vSEGMENT1||'.'||PIN_CLEARING||'.0000.00000.000000.00.000.000000.000000';
    
    return(vcode_combination_id);
    
EXCEPTION WHEN OTHERS THEN
    RETURN(NULL);
END GET_CLEARING_ACCT;


FUNCTION GET_CATEGORY(PIN_CAT_CODE IN VARCHAR2) RETURN NUMBER IS
    VCAT_ID NUMBER;
BEGIN
    SELECT CATEGORY_ID
    INTO    VCAT_ID
    FROM    FA_CATEGORIES_B
    WHERE   SEGMENT1||'-'||SEGMENT2 = PIN_CAT_CODE;
    RETURN VCAT_ID;
EXCEPTION WHEN OTHERS THEN
    RETURN NULL;    
END GET_CATEGORY;

FUNCTION GET_PARENT_ASSET(PIN_PARENT_ASSET IN VARCHAR2) RETURN NUMBER IS
    VPARENT_ASSET_ID NUMBER;
BEGIN
    SELECT ASSET_ID
    INTO    VPARENT_ASSET_ID
    FROM    FA_ASSET_INVOICES
    WHERE   ATTRIBUTE3 = PIN_PARENT_ASSET;
    RETURN VPARENT_ASSET_ID;
EXCEPTION WHEN OTHERS THEN
    RETURN NULL;    
END GET_PARENT_ASSET;

FUNCTION GET_PRORATE_DATE(PIN_DATE_PLACE_IN_SERVICE IN DATE) RETURN DATE IS
    vPRORATE_DATE DATE;
BEGIN
    SELECT PRORATE_DATE 
    INTO vPRORATE_DATE
    FROM FA_CONVENTIONS
    WHERE TRUNC(START_DATE) >= TRUNC(PIN_DATE_PLACE_IN_SERVICE)
    AND  TRUNC(END_DATE) <= TRUNC(PIN_DATE_PLACE_IN_SERVICE)
    AND PRORATE_CONVENTION_CODE = 'DAY';
    RETURN vPRORATE_DATE;
EXCEPTION WHEN OTHERS THEN
    RETURN NULL;    
END GET_PRORATE_DATE;


FUNCTION GET_PERIOD_DATE(p_book varchar2)  RETURN DATE IS 

V_PERIOD_CLOSE_DATE date;

BEGIN
      SELECT P.CALENDAR_PERIOD_CLOSE_DATE
      into V_PERIOD_CLOSE_DATE
      FROM FA_BOOK_CONTROLS_SEC C,
      FA_DEPRN_PERIODS P
      WHERE C.LAST_PERIOD_COUNTER+1 = P.period_counter
      AND P.book_type_code = C.book_type_code
      AND P.book_type_code = p_book;
      
      return V_PERIOD_CLOSE_DATE;
 EXCEPTION WHEN OTHERS THEN
    RETURN NULL;    
END GET_PERIOD_DATE;     
      


FUNCTION GET_EMPLOYEE_ID(PIN_EMP_NO IN VARCHAR2) RETURN NUMBER IS
    vEMPLOYEE_ID NUMBER;
BEGIN
    select person_id
    INTO vEMPLOYEE_ID
    from per_people_f
    where employee_number = PIN_EMP_NO
      and EFFECTIVE_START_DATE <= SYSDATE
      and EFFECTIVE_END_DATE >= SYSDATE;
    RETURN vEMPLOYEE_ID;
EXCEPTION WHEN OTHERS THEN
    RETURN NULL;    
END GET_EMPLOYEE_ID;




PROCEDURE VALIDATE_DATA (pFILENAME IN VARCHAR2,
                         oVALIDATE_FLAG OUT VARCHAR2
                         )
   
IS  
-- QTY_REGISTER , UNIT_PRICE

CURSOR F1 IS
SELECT * 
FROM TAC_FA_INTERFACE_TEMP
WHERE FILE_NAME = pFILENAME
and validate_flag is null;

vERROR_MSG VARCHAR2(20000);
vNULL VARCHAR2(500);
vASSET_BOQ_REGISTER varchar2(50);
vDATE_PLACE_IN_SERVICE DATE;
vPROJECT_CODE VARCHAR2(30);
vPO_NUMBER VARCHAR2(30);
vPO_LINE VARCHAR2(10);
vVENDOR_ID NUMBER;
vVENDOR_NAME VARCHAR2(250);
vLOCATION_ID NUMBER;
vEMPLOYEE_ID NUMBER;
vCATEGORY_ID NUMBER;
vEXPENSE_ACCT_ID NUMBER;
vCLEARING_ACCT_ID NUMBER;
vSERIAL_NUMBER VARCHAR2 (10);
vTAG_NUMBER VARCHAR2(10);
vPARENT_ASSET VARCHAR2(10);
vGUARANTEE_DATE DATE;
vGRN_NUMBER VARCHAR2(10);
vINV_NUMBER VARCHAR2(10);
vFA_BOOK VARCHAR2(10);
vDate_Registration DATE;
vCNTError NUMBER;
vTYPE_OF_WORK VARCHAR2(2);
vGENERIC_NAME VARCHAR2(2);
vPO_HEADER_ID NUMBER;

BEGIN

   
   

   FOR RECF1 IN F1
   LOOP
   
       vERROR_MSG := NULL;
       vNULL := NULL;
   
   
    IF RECF1.ASSET_BOQ_REGISTER IS NULL THEN
        vNULL := 'ASSET_BOQ_REGISTER';
    ELSIF RECF1.PO_NUMBER IS NULL THEN
        vNULL := vNULL||',PO_NUMBER';
    ELSIF RECF1.PO_LINE IS NULL THEN
        vNULL := vNULL||',PO_LINE';
    ELSIF RECF1.TYPE_OF_WORK IS NULL THEN
        vNULL := vNULL||',TYPE_OF_WORK';
    --ELSIF RECF1.VENDOR_CODE IS NULL THEN
    --    vNULL := vNULL||',VENDOR_CODE';
    ELSIF RECF1.UNIT_PRICE IS NULL THEN
        vNULL := vNULL||',UNIT_PRICE';
    ELSIF RECF1.QTY_REGISTER IS NULL THEN
        vNULL := vNULL||',QTY_REGISTER';
    ELSIF RECF1.DATE_PLACE_IN_SERVICE IS NULL THEN
        vNULL := vNULL||',DATE_PLACE_IN_SERVICE';
    ELSIF RECF1.LOCATION_CODE IS NULL THEN
        vNULL := vNULL||',LOCATION_CODE';
    ELSIF RECF1.OWNERS IS NULL THEN
        vNULL := vNULL||',OWNERS';
    ELSIF RECF1.CATEGORY_CODE IS NULL THEN
        vNULL := vNULL||',CATEGORY_CODE';
    ELSIF RECF1.EQUIPMENT IS NULL THEN
        vNULL := vNULL||',EQUIPMENT';
    ELSIF RECF1.ASSET_PART_GENERIC_NAME IS NULL THEN
        vNULL := vNULL||',ASSET_PART_GENERIC_NAME';
    /*ELSIF RECF1.GRN_NUMBER IS NULL THEN
        vNULL := vNULL||',GRN_NUMBER';
    ELSIF RECF1.INVOICE_NUMBER IS NULL THEN
        vNULL := vNULL||',INVOICE_NUMBER';*/
    --ELSIF RECF1.FA_DESCRIPTION IS NULL THEN  -- not use
    --    vNULL := vNULL||',FA_DESCRIPTION';
    ELSIF RECF1.FA_BOOK IS NULL THEN
        vNULL := vNULL||',BOOK';
    ELSIF RECF1.Date_Registration IS NULL THEN
        vNULL := vNULL||',Date_Registration';
    END IF;
    
    
    IF vNULL IS NOT NULL THEN
      vERROR_MSG := SUBSTR(vERROR_MSG ||',Error FA006-008 :Field '|| vNULL ||' is null',1,2000);
      --WRITE_LOG(',Error FA006-008 :Field '|| vNULL ||' is null','log');
    END IF;
    
    --==========================================================================--
    
     vDATE_PLACE_IN_SERVICE := NULL;
     BEGIN
        SELECT TO_DATE(RECF1.DATE_PLACE_IN_SERVICE,'DD-MON-YYYY')
        INTO vDATE_PLACE_IN_SERVICE
        FROM DUAL;
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END;
     
     
     IF RECF1.DATE_PLACE_IN_SERVICE IS NOT NULL AND vDATE_PLACE_IN_SERVICE IS NULL THEN        
            vERROR_MSG := SUBSTR(vERROR_MSG || ',Error FA006-018 : Date Place Inservice is wrong format',1,2000) ;
            --write_log(',Error FA006-018 : Date Place Inservice is wrong format','log');
     --elsif RECF1.DATE_PLACE_IN_SERVICE IS NOT NULL AND RECF1.DATE_PLACE_IN_SERVICE < GET_PERIOD_DATE(RECF1.FA_BOOK) THEN
     --       vERROR_MSG := SUBSTR(vERROR_MSG || ',Error FA006-018 : Date Place Inservice is less than Period',1,2000) ;
     END IF;
     
     
     
     
     
     
     vGUARANTEE_DATE := NULL;
     BEGIN
        SELECT TO_DATE(RECF1.GUARANTEE_DATE,'DD-MON-YYYY')
        INTO vGUARANTEE_DATE
        FROM DUAL;
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END;
     
     IF RECF1.GUARANTEE_DATE IS NOT NULL AND vGUARANTEE_DATE IS NULL THEN        
            vERROR_MSG := SUBSTR(vERROR_MSG || ',Error FA006-017 : Guarantee Date is wrong format',1,2000) ;
            --write_log(',Error FA006-017 : Guarantee Date is wrong format','log');
     END IF;
     
     
     vDate_Registration := NULL;
     BEGIN
        SELECT TO_DATE(RECF1.Date_Registration,'DD-MON-YYYY')
        INTO vDate_Registration
        FROM DUAL;
     EXCEPTION WHEN OTHERS THEN
        NULL;
     END;
     
     IF RECF1.Date_Registration IS NOT NULL AND vDate_Registration IS NULL THEN        
            vERROR_MSG := SUBSTR(vERROR_MSG || ',Error FA006-026 : Date Registration is wrong format',1,2000) ;
            --write_log(',Error FA006-026 : Date Registration is wrong format' ,'log');
     END IF;
     
  

--=====================================================================--  
    IF RECF1.ASSET_BOQ_REGISTER IS NOT NULL THEN
        vASSET_BOQ_REGISTER := null;
        BEGIN
            SELECT 'Y'
            INTO vASSET_BOQ_REGISTER
            FROM FA_ASSET_INVOICES
            WHERE ATTRIBUTE3 = RECF1.ASSET_BOQ_REGISTER;
        EXCEPTION WHEN OTHERS THEN
            NULL;
        END;
    
        if vASSET_BOQ_REGISTER = 'Y' then
            vERROR_MSG := SUBSTR(vERROR_MSG || ',Error FA006-009 : Asset BOQ Register is dupplicate',1,2000) ;
            --write_log(',Error FA006-009 : Asset BOQ Register is dupplicate','log');
        end if;
    
    END IF;
    
    -- not check 
    /*if RECF1.PROJECT_CODE is not null then
        vPROJECT_CODE := NULL;
        BEGIN
            SELECT 'Y'
            INTO vPROJECT_CODE
            FROM PA_PROJECTS_ALL
            WHERE SEGMENT1 = RECF1.PROJECT_CODE
            AND trunc(START_DATE) <= vDATE_PLACE_IN_SERVICE
            AND trunc(COMPLETION_DATE) >= vDATE_PLACE_IN_SERVICE
            ;
        EXCEPTION WHEN OTHERS THEN
            NULL;
        END;
        
        if vPROJECT_CODE is null then
           vERROR_MSG := SUBSTR(vERROR_MSG || ',Error FA006-010 : Project Code does not exists',1,2000) ;
           --write_log(',Error FA006-010 : Project Code does not exists  in erp','log');
        end if;
    
    end if;
    */
    
    
    IF RECF1.PO_NUMBER IS NOT NULL THEN
        vPO_NUMBER := NULL;
        vPO_HEADER_ID := NULL;
        BEGIN
            /*SELECT 'Y'
            INTO vPO_NUMBER
            FROM PO_HEADERS_ALL 
            WHERE SEGMENT1 = RECF1.PO_NUMBER;*/
         IF  INSTR(RECF1.PO_NUMBER,'-') = 0 THEN
         
           SELECT DISTINCT 'Y',poh.po_header_id
            INTO vPO_NUMBER,vPO_HEADER_ID
            FROM PO_HEADERS_ALL POH
            WHERE SEGMENT1 = RECF1.PO_NUMBER;
         
         ELSE
           SELECT DISTINCT 'Y',poh.po_header_id
            INTO vPO_NUMBER,vPO_HEADER_ID
            FROM PO_HEADERS_ALL POH,
            PO_LINES_ALL POL,
            PO_LINE_LOCATIONS_ALL  PLL,
            PO_RELEASES_ALL        PRA
            WHERE POH.SEGMENT1 || decode(pra.RELEASE_NUM,null,null,'-') || pra.RELEASE_NUM = RECF1.PO_NUMBER
            AND POH.PO_HEADER_ID =  POL.PO_HEADER_ID
            AND POL.PO_LINE_ID = PLL.PO_LINE_ID
            AND PLL.PO_HEADER_ID = PRA.PO_HEADER_ID(+)
            AND PLL.PO_RELEASE_ID = PRA.PO_RELEASE_ID(+);
         END IF;   
          
        EXCEPTION WHEN OTHERS THEN
            NULL;
        END;
    
    
        if vPO_NUMBER is null then
           vERROR_MSG := SUBSTR(vERROR_MSG || ',Error FA006-011 :PO Number  does not  exists  in erp',1,2000) ;
           --write_log(',Error FA006-011 :PO Number  does not  exists  in erp','log');
        end if;
    
    END IF;
    
    
    IF RECF1.PO_LINE IS NOT NULL THEN
        vPO_LINE := NULL;
        BEGIN
            SELECT 'Y'
            INTO vPO_LINE
            FROM PO_HEADERS_ALL H,PO_LINES_ALL L 
            WHERE H.PO_HEADER_ID = L.PO_HEADER_ID
           -- AND H.SEGMENT1 = RECF1.PO_NUMBER
            AND H.PO_HEADER_ID = vPO_HEADER_ID
            AND L.LINE_NUM = RECF1.PO_LINE;
        EXCEPTION WHEN OTHERS THEN
            NULL;
        END;
    
    
        if vPO_LINE is null then
           vERROR_MSG := SUBSTR(vERROR_MSG || ',Error FA006-012 : PO Line Number  does not  exists  in erp',1,2000) ;
          -- write_log(',Error FA006-012 : PO Line Number  does not  exists  in erp','log');
        end if;
    
    END IF;
    
    
    IF RECF1.TYPE_OF_WORK IS NOT NULL THEN
        vTYPE_OF_WORK := NULL;
        
       BEGIN
            select 'Y'  
            INTO vTYPE_OF_WORK
            from fnd_flex_value_sets fvs,
            fnd_flex_values fv
            where fvs.FLEX_VALUE_SET_ID=fv.FLEX_VALUE_SET_ID
            and fvs.FLEX_VALUE_SET_NAME = 'DTN Type of work'
            and fv.flex_value = RECF1.TYPE_OF_WORK ;
       EXCEPTION WHEN OTHERS THEN
            NULL;
       END;
       
       IF vTYPE_OF_WORK is null THEN      
            vERROR_MSG := SUBSTR(vERROR_MSG || ',Error FA006-014 :  Type Of Work not in List',1,2000) ;
           -- write_log(',Error FA006-014 :  Type Of Work not in List','log');       
       END IF;
    END IF;
    
    
    IF RECF1.VENDOR_CODE IS NOT NULL THEN
        vVENDOR_ID := NULL;
        --vVENDOR_ID := GET_VENDOR_ID(RECF1.VENDOR_CODE);
        GET_VENDOR(RECF1.VENDOR_CODE,vVENDOR_ID,vVENDOR_NAME);
        
        if vVENDOR_ID IS NULL then
           vERROR_MSG := SUBSTR(vERROR_MSG || ',Error FA006-015 :Not found Vendor codoe',1,2000) ;
           --write_log(',Error FA006-015 :Not found Vendor codoe','log');
        end if;
    END IF;
    
    IF RECF1.UNIT_PRICE <= 0 THEN
          vERROR_MSG := SUBSTR(vERROR_MSG || ',Error FA006-025 :Unit Price less than 1 ',1,2000) ;
    END IF;
    
    IF RECF1.QTY_REGISTER <= 0 THEN
          vERROR_MSG := SUBSTR(vERROR_MSG || ',Error FA006-026 :QTY Register less than 1 ',1,2000) ;
    END IF;
    
    
    IF RECF1.LOCATION_CODE IS NOT NULL THEN
        vLOCATION_ID := NULL;
        vLOCATION_ID := GET_LOCATION(RECF1.LOCATION_CODE);
        
        if vLOCATION_ID IS NULL then
           vERROR_MSG := SUBSTR(vERROR_MSG || ',Error  FA006-006:Not found asset location',1,2000) ;
          -- write_log(',Error  FA006-006:Not found asset location','log');
        end if;
    END IF;
 
    IF RECF1.OWNERS IS NOT NULL THEN
        vEMPLOYEE_ID := NULL;
        vEMPLOYEE_ID := GET_EMPLOYEE_ID(RECF1.OWNERS);
        
        if vEMPLOYEE_ID IS NULL then
           vERROR_MSG := SUBSTR(vERROR_MSG || ',Error FA006-023 : OWNER does not exist in erp ',1,2000) ;
           --write_log(',Error FA006-023 : OWNER does not exist in erp ','log');
        end if;
    END IF;  
    
    
    IF RECF1.CATEGORY_CODE IS NOT NULL THEN
        vCATEGORY_ID := NULL;
        vCATEGORY_ID := GET_CATEGORY(RECF1.CATEGORY_CODE);
        
        if vCATEGORY_ID IS NULL then
           vERROR_MSG := SUBSTR(vERROR_MSG || ',Error FA006-003:Not found asset categories ',1,2000) ;
           --write_log(',Error FA006-003:Not found asset categories ','log');
        ELSE
           vEXPENSE_ACCT_ID := NULL;
           vEXPENSE_ACCT_ID := GET_EXPENSE_ACCT(vCATEGORY_ID,RECF1.FA_BOOK);
           
           if vEXPENSE_ACCT_ID IS NULL then
              vERROR_MSG := SUBSTR(vERROR_MSG || ',Error  FA006-004:Not found account combination for expense account ',1,2000) ;
              --write_log(',Error FA006-003:Not found asset categories ','log');
           end if;
           
        end if;
    END IF;
    
           vCLEARING_ACCT_ID := NULL;
           vCLEARING_ACCT_ID := GET_CLEARING_ACCT(RECF1.CLEARING_ACCOUNT,RECF1.FA_BOOK);
           
           if vCLEARING_ACCT_ID IS NULL then
              vERROR_MSG := SUBSTR(vERROR_MSG || ',Error  FA006-004:Not found account combination for clearing account ',1,2000) ;
              --write_log(',Error FA006-003:Not found asset categories ','log');
           end if;
    
    
    
    
    IF RECF1.SERIAL_NUMBER IS NOT NULL THEN
        vSERIAL_NUMBER := NULL;
        begin
            select 'Y'
            INTO vSERIAL_NUMBER
            from fa_additions_b
            where SERIAL_NUMBER = RECF1.SERIAL_NUMBER;
         exception when others then
            null;
         end;
  
        if vSERIAL_NUMBER = 'Y' then
           vERROR_MSG := SUBSTR(vERROR_MSG || ',Error FA006-019 : Duplicate Serial Number ',1,2000) ;
           --write_log(',Error FA006-019 : Dupplicate Serial Number ','log');
        end if;
    END IF; 
    
    
    /*
    vGENERIC_NAME := null;    
    IF RECF1.ASSET_PART_GENERIC_NAME IS NOT NULL THEN
      BEGIN
            select 'Y'  
            INTO vGENERIC_NAME
            from fnd_flex_value_sets fvs,
            fnd_flex_values fv
            where fvs.FLEX_VALUE_SET_ID=fv.FLEX_VALUE_SET_ID
            and fvs.FLEX_VALUE_SET_NAME = 'DTN_PART_GENERIC'
            and fv.flex_value = RECF1.ASSET_PART_GENERIC_NAME ;
       EXCEPTION WHEN OTHERS THEN
            NULL;
       END;
       
        if vGENERIC_NAME IS NULL then
           vERROR_MSG := SUBSTR(vERROR_MSG || ',Error FA006-025 : ASSET_PART_GENERIC_NAME not in List ',1,2000) ;
          -- write_log('vGENERIC_NAME' || vGENERIC_NAME || ','|| RECF1.ASSET_PART_GENERIC_NAME,'log');
           
        end if;
    END IF;
    */
    
    
    IF RECF1.TAG_NUMBER IS NOT NULL THEN
        vTAG_NUMBER := NULL;
        begin
            select 'Y'
            INTO vTAG_NUMBER
            from fa_additions_b
            where TAG_NUMBER = RECF1.TAG_NUMBER;
         exception when others then
            null;
         end;
  
        if vTAG_NUMBER = 'Y' then
           vERROR_MSG := SUBSTR(vERROR_MSG || ',Error FA006-016 :Tag Number is dupplicate ',1,2000) ;
           --write_log(',Error FA006-016 :Tag Number is dupplicate ','log');
        end if;
    END IF; 
    
    
    IF RECF1.PARENT_ASSET_BOQ_REGIST_NO IS NOT NULL THEN
        vPARENT_ASSET := NULL;
        begin
            select 'Y'
            INTO vPARENT_ASSET
            from fa_asset_invoices
            where ATTRIBUTE3 = RECF1.PARENT_ASSET_BOQ_REGIST_NO;
         exception when others then
            null;
         end;
  
        if vPARENT_ASSET is null then
           vERROR_MSG := SUBSTR(vERROR_MSG || ',Error FA006-007 : Not found parent  asset  ',1,2000) ;
           --write_log(',Error FA006-007 : Not found parent  asset  ','log');
        end if;
    END IF;

    
    IF RECF1.GRN_NUMBER IS NOT NULL THEN
        vGRN_NUMBER := NULL;
        begin
            select 'Y'
            INTO vGRN_NUMBER
            from RCV_SHIPMENT_HEADERS
            where RECEIPT_NUM = RECF1.GRN_NUMBER;
         exception when others then
            null;
         end;
  
        if vGRN_NUMBER is null then
           vERROR_MSG := SUBSTR(vERROR_MSG || ',Error FA006-024 : GRN Number does not exist in erp ',1,2000) ;
          -- write_log(',Error FA006-024 : GRN Number does not exist in erp ','log');
        end if;
    END IF;
    
    
    IF RECF1.INVOICE_NUMBER IS NOT NULL THEN
        vINV_NUMBER := NULL;
        begin
            select 'Y'
            INTO vINV_NUMBER
            from AP_INVOICES_ALL
            where INVOICE_NUM = RECF1.INVOICE_NUMBER;
         exception when others then
            null;
         end;
  
        if vINV_NUMBER is null then
           vERROR_MSG := SUBSTR(vERROR_MSG || ',Error FA006-024 : Invoice Number does not exist in erp ',1,2000) ;
          --write_log(',Error FA006-024 : Invoice Number does not exist in erp ','log');
        end if;
    END IF; 
    
    
    IF RECF1.FA_BOOK IS NOT NULL THEN
        vFA_BOOK := NULL;
        begin
            select DISTINCT 'Y'
            INTO vFA_BOOK
            from FA_CATEGORY_BOOKS
            where BOOK_TYPE_CODE = RECF1.FA_BOOK;
         exception when others then
            null;
         end;
  
        if vFA_BOOK is null then
           vERROR_MSG := SUBSTR(vERROR_MSG || ',Error FA006-002:Not found asset book control',1,2000) ;
          -- write_log(',Error FA006-002:Not found asset book control','log');
        end if;
    END IF;   
    

    
     --write_log(vERROR_MSG,'log');
    
    
   IF vERROR_MSG IS NOT NULL THEN
     UPDATE TAC_FA_INTERFACE_TEMP
     SET VALIDATE_FLAG = 'E'
     ,ERROR_MESSAGE = substr(vERROR_MSG,1,2000)
     WHERE (ASSET_BOQ_REGISTER = RECF1.ASSET_BOQ_REGISTER OR RECF1.ASSET_BOQ_REGISTER  IS NULL)
     and po_number = RECF1.po_number
     and PO_LINE  = RECF1.PO_LINE
     AND  FILE_NAME = pFILENAME;
     commit;
   
   ELSE
     UPDATE TAC_FA_INTERFACE_TEMP
     SET VALIDATE_FLAG = 'S'
     WHERE (ASSET_BOQ_REGISTER = RECF1.ASSET_BOQ_REGISTER OR RECF1.ASSET_BOQ_REGISTER  IS NULL)
     and po_number = RECF1.po_number
     and PO_LINE  = RECF1.PO_LINE
     AND  FILE_NAME = pFILENAME;
     commit;
   END IF;
   


   END LOOP;
   
   
   vCNTError := 0;
  begin
    select count(*)
    into vCNTError
    from TAC_FA_INTERFACE_TEMP
    where VALIDATE_FLAG = 'E'
    and file_name = pFILENAME;
  exception when others then
    null;
  end;
   
   
  IF vCNTError > 0 THEN
   oVALIDATE_FLAG := 'E';
  ELSE 
   oVALIDATE_FLAG := 'S';
  END IF;
   
END;

FUNCTION extrack_data(p_buffer IN varchar2
                        ,p_delimiter IN varchar2
                        ,p_pos_data IN number
                        ) RETURN varchar2 IS
 v_pos_start number;
 v_pos_end number;
 v_subsrt_len number;
 v_data varchar2(255);
 
BEGIN
    IF p_pos_data = 1 THEN
        v_pos_start := 1;
    ELSE
        v_pos_start := INSTR(p_buffer, p_delimiter, 1, p_pos_data - 1) + 1;
    END IF;

        v_pos_end := INSTR(p_buffer, p_delimiter, 1, p_pos_data);

    IF v_pos_end = 0 THEN
        v_subsrt_len := LENGTH(p_buffer);
    ELSE
        v_subsrt_len := v_pos_end - v_pos_start;
    END IF;
        
        v_data := SUBSTR(p_buffer, v_pos_start, v_subsrt_len);

    
    RETURN REPLACE(TRIM(v_data), CHR(10), '');

EXCEPTION
 WHEN OTHERS THEN
   --write_log('[ERR] : @extrack_data : POS' || p_pos_data || ' : ' || SQLERRM,'log');
   RETURN NULL;
END extrack_data;

 procedure tac_fa006_get_dir_list( p_directory in varchar2 )
 as language java
 name 'TACFA006DirList.getList( java.lang.String )';
 
 
 PROCEDURE READ_TEXTFILE (pFILENAME IN VARCHAR,
                          pFOLDER_INBOX       IN VARCHAR2,
                          pFOLDER_ARCHIVE     IN VARCHAR2,
                          pFOLDER_ERROR       IN VARCHAR2)
 IS
   in_file        UTL_FILE.file_type;
   datarow        VARCHAR2 (32767);
   REC_TEMP TAC_FA_INTERFACE_TEMP%ROWTYPE;
   l_host_name             varchar2(100) := get_host_name;
 
BEGIN
     in_file := UTL_FILE.fopen_NCHAR (pFOLDER_INBOX,pFILENAME, 'R');
     datarow := NULL;
     
     delete from TAC_FA_INTERFACE_TEMP
     where file_name = pFILENAME;
     commit;
     
     
     LOOP
               
               --   DELETE FROM TAC_FA_INTERFACE_TEMP;
               --   COMMIT;
   
   
                  UTL_FILE.get_line_NCHAR (in_file, datarow);
                  datarow := REPLACE(REPLACE(datarow, CHR(10), ''), CHR(13), '');

                  IF datarow IS NULL
                  THEN
                     --WRITE_LOG ('END FILE', 'Log');
                     --EXIT;
                     null;
                  else
                  
                REC_TEMP.ASSET_BOQ_REGISTER := extrack_data(datarow,'|', 1);
                REC_TEMP.PROJECT_CODE                := extrack_data(datarow,'|', 2);
                REC_TEMP.PO_NUMBER                   := extrack_data(datarow,'|', 3);
                REC_TEMP.PO_LINE                     := TO_NUMBER(extrack_data(datarow,'|', 4));
                --REC_TEMP.ASSET_ITEM_CODE             := extrack_data(datarow,'|', 5);
                REC_TEMP.BOQ_ITEM                    := extrack_data(datarow,'|', 5);
                REC_TEMP.BOQ_PART_CODE               := extrack_data(datarow,'|', 6);
                REC_TEMP.TYPE_OF_WORK                := extrack_data(datarow,'|', 7);
                REC_TEMP.VENDOR_CODE                 := extrack_data(datarow,'|', 8);
                REC_TEMP.UNIT_PRICE                  := TO_NUMBER(extrack_data(datarow,'|', 9));
                REC_TEMP.QTY_REGISTER                := TO_NUMBER(extrack_data(datarow,'|', 10));
                REC_TEMP.DATE_PLACE_IN_SERVICE       := extrack_data(datarow,'|', 11);
                REC_TEMP.LOCATION_CODE               := extrack_data(datarow,'|', 12);
                REC_TEMP.OWNERS                      := extrack_data(datarow,'|', 13);
                REC_TEMP.CATEGORY_CODE               := extrack_data(datarow,'|', 14);
                REC_TEMP.BRAND                       := extrack_data(datarow,'|', 15);
                REC_TEMP.MODELS                      := extrack_data(datarow,'|', 16);
                REC_TEMP.SERIAL_NUMBER               := extrack_data(datarow,'|', 17);
                REC_TEMP.EQUIPMENT                   := extrack_data(datarow,'|', 18);
                REC_TEMP.ASSET_PART_GENERIC_NAME     := extrack_data(datarow,'|', 19);
                REC_TEMP.COMMENTS                    := extrack_data(datarow,'|', 20);
                REC_TEMP.TAG_NUMBER                  := extrack_data(datarow,'|', 21);
                REC_TEMP.PARENT_ASSET_BOQ_REGIST_NO  := extrack_data(datarow,'|', 22);
                REC_TEMP.GUARANTEE_DATE              := extrack_data(datarow,'|', 23);
                REC_TEMP.GRN_NUMBER                  := extrack_data(datarow,'|', 24);
                REC_TEMP.INVOICE_NUMBER              := extrack_data(datarow,'|', 25);
                REC_TEMP.FA_DESCRIPTION              := extrack_data(datarow,'|', 26);
                REC_TEMP.FA_BOOK                     := extrack_data(datarow,'|', 27);
                REC_TEMP.Date_Registration           := extrack_data(datarow,'|', 28);
                REC_TEMP.CLEARING_ACCOUNT            := extrack_data(datarow,'|', 29);
                REC_TEMP.FILE_NAME                   := pFILENAME;

 


                 
INSERT INTO TAC_FA_INTERFACE_TEMP(ASSET_BOQ_REGISTER          ,
  PROJECT_CODE                ,
  PO_NUMBER                   ,
  PO_LINE                     ,
  --ASSET_ITEM_CODE             ,
  BOQ_ITEM                    ,
  BOQ_PART_CODE               ,
  TYPE_OF_WORK                ,
  VENDOR_CODE                 ,
  UNIT_PRICE                  ,
  QTY_REGISTER                ,
  DATE_PLACE_IN_SERVICE       ,
  LOCATION_CODE               ,
  OWNERS                      ,
  CATEGORY_CODE               ,
  BRAND                       ,
  MODELS                      ,
  SERIAL_NUMBER               ,
  EQUIPMENT                   ,
  ASSET_PART_GENERIC_NAME     ,
  COMMENTS                    ,
  TAG_NUMBER                  ,
  PARENT_ASSET_BOQ_REGIST_NO  ,
  GUARANTEE_DATE              ,
  GRN_NUMBER                  ,
  INVOICE_NUMBER              ,
  FA_DESCRIPTION              ,
  FA_BOOK                     ,
  Date_Registration           ,
  CLEARING_ACCOUNT            ,
  FILE_NAME                   ,
  CREATION_DATE                 
  )
  VALUES(REC_TEMP.ASSET_BOQ_REGISTER,
  REC_TEMP.PROJECT_CODE                ,
  REC_TEMP.PO_NUMBER                   ,
  REC_TEMP.PO_LINE                     ,
  --REC_TEMP.ASSET_ITEM_CODE             ,
  REC_TEMP.BOQ_ITEM                    ,
  REC_TEMP.BOQ_PART_CODE               ,
  REC_TEMP.TYPE_OF_WORK                ,
  REC_TEMP.VENDOR_CODE                 ,
  REC_TEMP.UNIT_PRICE                  ,
  REC_TEMP.QTY_REGISTER                ,
  REC_TEMP.DATE_PLACE_IN_SERVICE       ,
  REC_TEMP.LOCATION_CODE               ,
  REC_TEMP.OWNERS                      ,
  REC_TEMP.CATEGORY_CODE               ,
  REC_TEMP.BRAND                       ,
  REC_TEMP.MODELS                      ,
  REC_TEMP.SERIAL_NUMBER               ,
  REC_TEMP.EQUIPMENT                   ,
  REC_TEMP.ASSET_PART_GENERIC_NAME     ,
  REC_TEMP.COMMENTS                    ,
  REC_TEMP.TAG_NUMBER                  ,
  REC_TEMP.PARENT_ASSET_BOQ_REGIST_NO  ,
  REC_TEMP.GUARANTEE_DATE              ,
  REC_TEMP.GRN_NUMBER                  ,
  REC_TEMP.INVOICE_NUMBER              ,
  REC_TEMP.FA_DESCRIPTION              ,
  REC_TEMP.FA_BOOK                     ,
  REC_TEMP.Date_Registration           ,
  REC_TEMP.CLEARING_ACCOUNT            ,
  REC_TEMP.FILE_NAME                   ,
  SYSDATE              
  ); 

  COMMIT;    
                  
      END IF;

   END LOOP;
     
    EXCEPTION 
               WHEN no_data_found
               THEN
                  WRITE_LOG ('end read textfile', 'out');
                  /*** Move file to directory loaded*/
          -- UTL_FILE.FCOPY (pFOLDER_INBOX,pFILENAME,pFOLDER_ARCHIVE,pFILENAME);
          -- UTL_FILE.FREMOVE (pFOLDER_INBOX,pFILENAME);
     
               WHEN OTHERS
               THEN
               
           WRITE_LOG(to_char(sysdate,'DD-MON-YY HH24:MI:SS')||'|'||
                to_char(sysdate,'DD-MON-YY HH24:MI:SS')||'|'||
                l_host_name||'|'||  
                'TAC : Asset Register'||'|'||
                pFILENAME||'|'||
                ''||'|'||
                ''||'|'||
                ''||'|'||
                ''||'|'||
                ''||'|'||
                'E'||'|'||
                'Error FA006-022 : Cannot Read File '
                ,'LOG');
                 -- INSERT INTO TAC_SCF_LOG_TBL VALUES(V_CONC_REQ_ID,TAC_SCF_LOG_S.NEXTVAL,'Others Error : ',SYSDATE,'N');                  
                   /*** Move file to directory ERROR*/
           --UTL_FILE.FCOPY (pFOLDER_INBOX,pFILENAME,pFOLDER_ERROR,pFILENAME);
          -- UTL_FILE.FREMOVE (pFOLDER_INBOX,pFILENAME);
     END; 
     
PROCEDURE MAIN_MASS_ADD_API(errbuf               OUT VARCHAR2,
                            retcode              OUT NUMBER,
                            P_FILENAME           IN VARCHAR2,
                            P_DATE_PLACE_IN_SERVICE    IN VARCHAR2
                            )
                            
IS 



  CURSOR CUR_HEADERS(vFILENAME VARCHAR2,
                     vDATE_PLACE_IN_SERVICE VARCHAR2)  IS
     SELECT *
     FROM TAC_FA_INTERFACE_TEMP T
     WHERE 1=1
     AND FILE_NAME = vFILENAME
     AND DATE_PLACE_IN_SERVICE = vDATE_PLACE_IN_SERVICE
     AND NVL(T.VALIDATE_FLAG,'N') IN ('S')
     AND NVL(T.PROCESS_FLAG,'N') IN ('N')
     ORDER BY ASSET_BOQ_REGISTER;
     
     
    CURSOR CUR_LOG(vFILENAME VARCHAR2)  IS
     SELECT *
     FROM TAC_FA_INTERFACE_TEMP T
     WHERE 1=1
     AND FILE_NAME = vFILENAME
     AND NVL(T.VALIDATE_FLAG,'N') IN ('S','E')
     --AND NVL(T.PROCESS_FLAG,'N') IN ('S','E')
     ORDER BY ASSET_BOQ_REGISTER;
     
     
     
    h_status        varchar2(10);
    h_msg_count     number;
    h_mSg_data      varchar2(512);
    h_asset_id      number;
    h_asset_number  varchar2(30);
    h_tran_id       number;
    h_dist_tran_id  number;
    h_book_class    varchar2(15);
    h_inv_txn_id    number;
    h_created_by    number;
    h_creation_date date;
    h_count         number;
    

    

    l_trans_rec         FA_API_TYPES.trans_rec_type;
    l_dist_trans_rec    FA_API_TYPES.trans_rec_type;
    l_asset_hdr_rec     FA_API_TYPES.asset_hdr_rec_type;
    l_asset_desc_rec    FA_API_TYPES.asset_desc_rec_type;
    l_asset_cat_rec     FA_API_TYPES.asset_cat_rec_type;
    l_asset_type_rec    FA_API_TYPES.asset_type_rec_type;
    l_asset_hierarchy_rec FA_API_TYPES.asset_hierarchy_rec_type;
    l_asset_fin_rec     FA_API_TYPES.asset_fin_rec_type;
    l_asset_deprn_rec   FA_API_TYPES.asset_deprn_rec_type;
    l_asset_dist_rec    FA_API_TYPES.asset_dist_rec_type;
    l_asset_dist_tbl    FA_API_TYPES.asset_dist_tbl_type;
    l_inv_tbl           FA_API_TYPES.inv_tbl_type;
    l_inv_rate_tbl      FA_API_TYPES.inv_rate_tbl_type;
    l_inv_rec           FA_API_TYPES.inv_rec_type;
    

    
    l_return_status     VARCHAR2(1);
    l_mesg_count        number ;
    l_mesg_len          number;
    l_mesg              varchar2(1000);
    V_CONC_REQ_ID          NUMBER := fnd_global.conc_request_id;
    vEMPLOYEE_ID NUMBER;
    l_host_name             varchar2(100) := get_host_name;
    

    vLOOP           NUMBER := 1;
    vCNT_LOOP       NUMBER := 1;
    i               NUMBER := 0;
    j               NUMBER := 0;
    
    vCHR           VARCHAR2(1000);
    vCNT_RECORD     NUMBER := 0;
    vCNT_RECORD_ERR NUMBER := 0;
    vCNT_RECORD_COMP NUMBER := 0;
    vGOOD_RECORD    NUMBER := 0;
    vCHK_ERR        NUMBER := 0;
    
    vPROJECT_ID    PA_PROJECTS_ALL.PROJECT_ID%TYPE;
    vVENDOR_ID     PO_VENDORS.VENDOR_ID%TYPE;
    vVENDOR_NAME   PO_VENDORS.VENDOR_NAME%TYPE;
    vLOCATION_ID   FA_LOCATIONS.LOCATION_ID%TYPE;
    vCATEGORY_ID   FA_CATEGORIES_B.CATEGORY_ID%TYPE;
    vPARENT_ASSET_ID  FA_ADDITIONS_B.ASSET_ID%TYPE;
    vDEPRN_METHOD  FA_CATEGORY_BOOK_DEFAULTS.DEPRN_METHOD%TYPE;
    vLIFE_IN_MONTHS FA_CATEGORY_BOOK_DEFAULTS.LIFE_IN_MONTHS%TYPE;
    vCOST NUMBER;
    vPRORATE_DATE  DATE;
    vEXPENSE_ID  FA_CATEGORY_BOOKS.DEPRN_EXPENSE_ACCT%TYPE; 
    vASSET_ID  FA_ADDITIONS_B.ASSET_ID%TYPE;
    vCLEARING_ACCT_ID NUMBER;

x_ret_code  varchar2(2000);
x_error_buf varchar2(2000);
l_msg_index_out number;
vATTRIBUTE5 VARCHAR2(150);
vATTRIBUTE6 VARCHAR2(150);
V_ASSET_BOQ_REGISTER  VARCHAR2(50);
vCNT_FILE NUMBER;
vERRORFLAG varchar2(1);


vVALIDATE_FLAG VARCHAR2(1);
vBeginDate  VARCHAR2(20);
vEndDate  VARCHAR2(20);

 vPATH_INBOX       VARCHAR2 (5120);
 vPATH_ARCHIVE     VARCHAR2 (5120);
 vPATH_ERROR       VARCHAR2 (5120);
 v_property_type   varchar2(100);
 
 
BEGIN



  
    FOR REC_HEADER IN CUR_HEADERS(P_FILENAME,P_DATE_PLACE_IN_SERVICE)
    LOOP
    
      begin
        vCHK_ERR := 0;
    
        vCATEGORY_ID := GET_CATEGORY(REC_HEADER.CATEGORY_CODE);
        vPARENT_ASSET_ID := GET_PARENT_ASSET(REC_HEADER.PARENT_ASSET_BOQ_REGIST_NO);
        --vVENDOR_NAME := GET_VENDOR_NAME(REC_HEADER.VENDOR_CODE);
        
        GET_VENDOR(REC_HEADER.VENDOR_CODE,vVENDOR_ID,vVENDOR_NAME);
        
        v_property_type := '';
        begin
            select distinct PROPERTY_TYPE_CODE 
            into v_property_type
            from FA_CATEGORIES_B
            where category_id=vCATEGORY_ID;
        exception when others then
            v_property_type := '';
        end;
        
        l_asset_hdr_rec.asset_id := null;
        l_asset_desc_rec.asset_number := '';
        
        l_trans_rec.transaction_name    := 'Addition';
        l_asset_hdr_rec.book_type_code  := REC_HEADER.FA_BOOK;        
        l_asset_desc_rec.description        := REC_HEADER.EQUIPMENT; --REC_HEADER.FA_DESCRIPTION;
        l_asset_desc_rec.tag_number         := REC_HEADER.TAG_NUMBER;
        l_asset_desc_rec.serial_number      := REC_HEADER.SERIAL_NUMBER; 
        l_asset_desc_rec.asset_key_ccid     := 2029;
        l_asset_desc_rec.parent_asset_id    := vPARENT_ASSET_ID; 
        l_asset_desc_rec.manufacturer_name  := substr(vVENDOR_NAME,1,30); 
        l_asset_desc_rec.model_number       := REC_HEADER.models;
        l_asset_type_rec.asset_type         := 'CAPITALIZED';
        l_asset_cat_rec.category_id         := vCATEGORY_ID;
        
        l_asset_desc_rec.property_type_code := v_property_type;
        
  
        --l_asset_desc_rec.owned_leased       := REC_HEADER.OWNERS;
        
   BEGIN
        SELECT DEPRN_METHOD,LIFE_IN_MONTHS
        INTO vDEPRN_METHOD,vLIFE_IN_MONTHS
        FROM FA_CATEGORY_BOOK_DEFAULTS
        WHERE CATEGORY_ID = vCATEGORY_ID
        AND book_type_code = REC_HEADER.FA_BOOK;
   
   EXCEPTION WHEN OTHERS THEN
      vDEPRN_METHOD := NULL;
      vLIFE_IN_MONTHS := NULL;
   END;
   
   
   --vCOST := NVL(REC_HEADER.UNIT_PRICE,0) * NVL(REC_HEADER.QTY_REGISTER,0);
   vCOST := NVL(REC_HEADER.UNIT_PRICE,0) * 1;
   vPRORATE_DATE := GET_PRORATE_DATE(TO_DATE(REC_HEADER.DATE_PLACE_IN_SERVICE,'DD-MON-YYYY'));
   
   l_asset_fin_rec.date_placed_in_service  := TO_DATE(REC_HEADER.DATE_PLACE_IN_SERVICE,'DD-MON-YYYY');
   l_asset_fin_rec.deprn_method_code       := vDEPRN_METHOD;
   l_asset_fin_rec.life_in_months          := vLIFE_IN_MONTHS;
   l_asset_fin_rec.cost                    := vCOST;
   l_asset_fin_rec.ORIGINAL_COST           := vCOST;
   l_asset_fin_rec.salvage_value           := 0;
   l_asset_fin_rec.prorate_convention_code := 'DAY';
   l_asset_fin_rec.prorate_date            := vPRORATE_DATE;
   l_asset_fin_rec.depreciate_flag         := 'YES';
   l_asset_deprn_rec.ytd_deprn             := 0;
   l_asset_deprn_rec.deprn_reserve         := 0;
   l_asset_deprn_rec.bonus_ytd_deprn       := 0;
      
  /*  
    l_trans_rec.transaction_date_entered    := l_asset_fin_rec.date_placed_in_service;
    l_trans_rec.who_info.last_updated_by    := 1112;
  */
        
        
        
         
--===============  Source Line Infomation ========================-- 
        l_inv_tbl.DELETE;  
        j := 0;                                      
      --  FOR REC_INV IN CUR_INVOICE(REC_HEADER.ASSET_ID)
      --  LOOP
       j := j+1;
       
       vPROJECT_ID := GET_PROJECT_ID(REC_HEADER.PROJECT_CODE);
       --vVENDOR_ID := GET_VENDOR_ID(REC_HEADER.VENDOR_CODE);
       vCLEARING_ACCT_ID := GET_CLEARING_ACCT(REC_HEADER.CLEARING_ACCOUNT,REC_HEADER.FA_BOOK);
       
       
       l_inv_rec.INVOICE_NUMBER        := REC_HEADER.INVOICE_NUMBER;
       l_inv_rec.po_vendor_id          := vVENDOR_ID;
       l_inv_rec.fixed_assets_cost     := vCOST; 
       l_inv_rec.po_number             := REC_HEADER.PO_NUMBER;       
       l_inv_rec.project_id            := vPROJECT_ID;
       l_inv_rec.payables_units        := 1; --REC_HEADER.qty_register;
       l_inv_rec.payables_batch_name   := 'FA006-'||V_CONC_REQ_ID;
       l_inv_rec.payables_code_combination_id := vCLEARING_ACCT_ID;
       
      -- l_inv_rec.feeder_system_name    := 'OUTSTANDING';
      -- l_inv_rec.create_batch_id       := 1000;
       
       
       l_inv_tbl (j)                   := l_inv_rec;
       
      /*  
        l_inv_rec.deleted_flag          := 'NO';
        l_inv_rec.description           := REC_INV.description;
        l_inv_rec.unrevalued_cost       := 0;             
--    l_inv_rec.create_batch_id       := 1000;
--    l_inv_rec.payables_code_combination_id := 13528;
--   l_inv_rec.feeder_system_name    := 'ACK';
    --l_inv_rec.payables_cost         := ABS(REC_HEADER.UNIT) * REC_HEADER.COST;  
    l_inv_rec.inv_indicator         := 1;*/  
  --  END LOOP;
  
   --===============  End Source Line Infomation ========================--  
   
   

    --========================  Assignment Information  ========================--
    i := 0;
    l_asset_dist_tbl.delete;
    
   -- FOR REC_ASS IN CUR_ASS(REC_HEADER.ASSET_ID)
   -- LOOP
         i := i +1;
         
          vLOCATION_ID := GET_LOCATION(REC_HEADER.LOCATION_CODE);
          vEXPENSE_ID  := GET_EXPENSE_ACCT(vCATEGORY_ID,REC_HEADER.FA_BOOK);
          vEMPLOYEE_ID := GET_EMPLOYEE_ID(REC_HEADER.OWNERS);
                 
          l_asset_dist_rec.units_assigned     := 1;   --fix --REC_HEADER.qty_register;
          l_asset_dist_rec.assigned_to        := vEMPLOYEE_ID;
          l_asset_dist_rec.expense_ccid       := vEXPENSE_ID; 
          l_asset_dist_rec.location_ccid      := vLOCATION_ID;
          
      /*        
       l_asset_dist_rec.transaction_units  := REC_HEADER.qty_register;      
      */  
          l_asset_dist_tbl(i)                 := l_asset_dist_rec;
        
            
   -- END LOOP;
   
   --========================  End Assignment Information  ========================--
    
    l_mesg := null;
 -- IF vCHK_ERR != 1 THEN
    
    
    fa_addition_pub.do_addition
        (p_api_version          => 1.0,
        p_init_msg_list         => FND_API.G_TRUE,
        p_commit                => FND_API.G_FALSE,
        p_validation_level      => FND_API.G_VALID_LEVEL_FULL,
        x_return_status         => l_return_status, --var
        x_msg_count             => l_mesg_count, --num
        x_msg_data              => l_mesg, --var
        p_calling_fn            => null, --var
        px_trans_rec            => l_trans_rec,  -- fa_api_types.trans_rec_type
        px_dist_trans_rec       => l_dist_trans_rec, --fa_api_types.trans_rec_type
        px_asset_hdr_rec        => l_asset_hdr_rec,
        px_asset_desc_rec       => l_asset_desc_rec,
        px_asset_type_rec       => l_asset_type_rec,
        px_asset_cat_rec        => l_asset_cat_rec,
        px_asset_hierarchy_rec  => l_asset_hierarchy_rec,
        px_asset_fin_rec        => l_asset_fin_rec,
        px_asset_deprn_rec      => l_asset_deprn_rec,
        px_asset_dist_tbl       => l_asset_dist_tbl,
        px_inv_tbl              => l_inv_tbl, --fa_api_types.inv_tbl_type
        px_inv_rate_tbl         => l_inv_rate_tbl
        );
        
        COMMIT;
        
        

        write_log('l_return_status =' || l_return_status,'out');
            
        if (l_return_status <> FND_API.G_RET_STS_SUCCESS) then
        
                WRITE_LOG(pCHAR => SQLERRM,pTYPE => 'LINE');
                
                l_mesg_count := fnd_msg_pub.count_msg;
                
                if l_mesg_count > 0 then
                    l_mesg := chr(10) || substr(fnd_msg_pub.get
                    (fnd_msg_pub.G_FIRST, fnd_api.G_FALSE),1, 512);
                    for i in 1..2
                    loop -- (l_mesg_count - 1) loop
                        l_mesg := l_mesg || chr(10) ||
                        substr(fnd_msg_pub.get(fnd_msg_pub.G_NEXT,
                        fnd_api.G_FALSE), 1, 512);
                    end loop;
                    fnd_msg_pub.delete_msg();
                    l_mesg_len := length(l_mesg);
                END IF;
                    for i in 1..ceil(l_mesg_len/255)
                    loop
                        dbms_output.put_line('m1= '||substr(l_mesg, ((i*255)-254), 255));
                        --WRITE_LOG(pCHAR => 'm2 =' || substr(l_mesg, ((i*255)-254), 255),pTYPE => 'LINE'); --thunyathorn 26/04/2012
                        WRITE_LOG(l_mesg,'OUT');
                    end loop;
                    
                                x_ret_code  := sqlcode;
                                x_error_buf := 'm3 =' || sqlerrm||' - FA Details Update Failed ....!';
                                --FND_FILE.PUT_LINE( FND_FILE.LOG,'FA Details Update Failed for BX Number:'||rec_header.asset);
                                FND_FILE.PUT_LINE( FND_FILE.LOG,'FA Details Update Failed  x_error_buf:'||SUBSTR(x_error_buf, 1, 200));
 
 
                                      IF (fnd_msg_pub.count_msg > 1)
                                      THEN
                                            FOR j IN 1 .. fnd_msg_pub.count_msg
                                            LOOP
 
 
                                                  fnd_msg_pub.get(p_msg_index      =>  j,
                                                                  p_encoded        =>  'F',
                                                                  p_data           =>  l_mesg,
                                                                  p_msg_index_out  =>  l_msg_index_out);
 
 
                                                 l_mesg   :=    SUBSTR(l_mesg, 1, 400)||SUBSTR(l_mesg, 1, 400);
 
 
                                                 FND_FILE.PUT_LINE( FND_FILE.LOG, 'l_mesg:'||SUBSTR(l_mesg,1,400) );
 
 
                                            END LOOP;
 
 
                                      ELSE
 
 
                                            fnd_msg_pub.get(p_msg_index       =>  1,
                                                           p_encoded         =>  'F',
                                                            p_data            =>  l_mesg,
                                                            p_msg_index_out   =>  l_msg_index_out);
 
 
                                            l_mesg      :=     SUBSTR(l_mesg, 1, 400)||SUBSTR(l_mesg, 1, 400);
 
 
                                            FND_FILE.PUT_LINE( FND_FILE.LOG, 'm5 = l_error:'||SUBSTR(l_mesg,1,400) );
 
 
                                      END IF;
                    
                    
                    
                    
            vCHK_ERR := 1;        
        end if;
            
  /*else
           WRITE_LOG(pCHAR => '***ADDITION SUCCESS ASSET_NUMBER = '||l_asset_desc_rec.ASSET_NUMBER,pTYPE => 'LINE');--thunyathorn 27/04/2012
           NULL;
  end if;*/
  
    --END IF;
    
  -- WRITE_LOG(pCHAR => '***ADDITION SUCCESS ASSET_NUMBER = '||l_asset_desc_rec.ASSET_NUMBER,pTYPE => 'LINE');--thunyathorn 27/04/2012

    
    
    IF vCHK_ERR != 1 THEN
       BEGIN
           UPDATE TAC_FA_INTERFACE_TEMP
           SET PROCESS_FLAG = 'S'
           ,ASSET_NUMBER = l_asset_desc_rec.ASSET_NUMBER
           WHERE (ASSET_BOQ_REGISTER = REC_HEADER.ASSET_BOQ_REGISTER OR REC_HEADER.ASSET_BOQ_REGISTER IS NULL)
           and po_number = REC_HEADER.po_number
           and PO_LINE  = REC_HEADER.PO_LINE
           and file_name = P_FILENAME
           ;
           commit;
       END;
       
       
            BEGIN
                UPDATE FA_ADDITIONS_B B
                SET B.ATTRIBUTE1 = REC_HEADER.BRAND
                ,B.ATTRIBUTE2 = to_char(TO_DATE(REC_HEADER.GUARANTEE_DATE,'DD-MON-YYYY'),'yyyy/mm/dd hh24:mi:ss')
                ,B.ATTRIBUTE3 = REC_HEADER.COMMENTS
                
                ,B.ATTRIBUTE5 = REC_HEADER.PO_LINE
                ,B.ATTRIBUTE7 = REC_HEADER.GRN_NUMBER
                ,B.ATTRIBUTE8 = REC_HEADER.PROJECT_CODE
                ,B.ATTRIBUTE9 = to_char(TO_DATE(REC_HEADER.Date_Registration,'DD-MON-YYYY'),'yyyy/mm/dd hh24:mi:ss')
                ,B.ATTRIBUTE10 = REC_HEADER.QTY_REGISTER  --REC_HEADER.UNIT_PRICE
                --,B.ATTRIBUTE4 = REC_HEADER.QTY_REGISTER  --REC_HEADER.UNIT_PRICE
                --,B.ATTRIBUTE27 = REC_HEADER.BOQ_ITEM
                --,B.ATTRIBUTE28 = REC_HEADER.ASSET_PART_GENERIC_NAME
                WHERE B.ASSET_NUMBER = l_asset_desc_rec.ASSET_NUMBER
                ;
                commit;

            END;
            
            vASSET_ID := NULL;
            BEGIN
                SELECT ASSET_ID
                INTO vASSET_ID
                FROM FA_ADDITIONS_B
                WHERE ASSET_NUMBER = l_asset_desc_rec.ASSET_NUMBER;
                
            EXCEPTION WHEN OTHERS THEN
                NULL;
            END;
            
            BEGIN
               UPDATE FA_ASSET_INVOICES I
               SET
               -- I.ATTRIBUTE1 = REC_HEADER.PO_LINE
               --,I.ATTRIBUTE2 = REC_HEADER.PROJECT_CODE 
                I.ATTRIBUTE3 = REC_HEADER.ASSET_BOQ_REGISTER
               ,I.ATTRIBUTE4 = REC_HEADER.BOQ_ITEM
               ,I.ATTRIBUTE5 = REC_HEADER.BOQ_PART_CODE
               --,I.ATTRIBUTE6 = lower(REC_HEADER.TYPE_OF_WORK)
               ,I.ATTRIBUTE7 = REC_HEADER.ASSET_PART_GENERIC_NAME
               ,I.ATTRIBUTE_CATEGORY_CODE = 'DTAC_PO_Detail'
               WHERE 1=1
               AND ASSET_ID = vASSET_ID;
               commit;
            END;
            
    ELSE
       vCNT_RECORD_err := vCNT_RECORD_err + 1;
       BEGIN    
        UPDATE TAC_FA_INTERFACE_TEMP
        SET PROCESS_FLAG = 'E'
        ,PROCESS_ERROR_MESSAGE = l_mesg
        WHERE (ASSET_BOQ_REGISTER = REC_HEADER.ASSET_BOQ_REGISTER OR REC_HEADER.ASSET_BOQ_REGISTER IS NULL) 
        and po_number = REC_HEADER.po_number
        and PO_LINE  = REC_HEADER.PO_LINE
        and file_name = P_FILENAME
        ; 
        commit;
       END;
    END IF;
    
   COMMIT;
   
   EXCEPTION WHEN OTHERS THEN
   
        l_mesg := SQLERRM;
        
        UPDATE TAC_FA_INTERFACE_TEMP
        SET PROCESS_FLAG = 'E'
        ,VALIDATE_FLAG = 'E'
        ,PROCESS_ERROR_MESSAGE = l_mesg
        WHERE (ASSET_BOQ_REGISTER = REC_HEADER.ASSET_BOQ_REGISTER OR REC_HEADER.ASSET_BOQ_REGISTER IS NULL) 
        and po_number = REC_HEADER.po_number
        and PO_LINE  = REC_HEADER.PO_LINE
        and file_name = P_FILENAME
        ; 
      commit;
      EXIT;
   
  END;   
  END LOOP; --END LOOP HEADER
  
  
END;

PROCEDURE MAIN_MASS_ADD_INT(errbuf               OUT VARCHAR2,
                            retcode              OUT NUMBER,
                            P_FOLDER_INBOX       IN VARCHAR2,
                            P_FOLDER_ARCHIVE     IN VARCHAR2,
                            P_FOLDER_ERROR       IN VARCHAR2
                            )
IS

   CURSOR CUR_FILE
   IS   
       SELECT FILENAME
       FROM TAC_FA006_INBOX
       ORDER BY FILENAME
       ;


    CURSOR CUR_GROUP_DATE(vFILENAME VARCHAR2)  IS
     SELECT DATE_PLACE_IN_SERVICE
     FROM TAC_FA_INTERFACE_TEMP T
     WHERE 1=1
     AND FILE_NAME = vFILENAME
     AND NVL(T.VALIDATE_FLAG,'N') IN ('S')
     AND NVL(T.PROCESS_FLAG,'N') IN ('N')
     GROUP BY DATE_PLACE_IN_SERVICE
     order by DATE_PLACE_IN_SERVICE;

    CURSOR CUR_HEADERS(vFILENAME VARCHAR2,
                     vDATE_PLACE_IN_SERVICE VARCHAR2)  IS
     SELECT *
     FROM TAC_FA_INTERFACE_TEMP T
     WHERE 1=1
     AND FILE_NAME = vFILENAME
     AND DATE_PLACE_IN_SERVICE = vDATE_PLACE_IN_SERVICE
     AND NVL(T.VALIDATE_FLAG,'N') IN ('S')
     AND NVL(T.PROCESS_FLAG,'N') IN ('N')
     ORDER BY ASSET_BOQ_REGISTER;
     
     
    CURSOR CUR_LOG(vFILENAME VARCHAR2)  IS
     SELECT *
     FROM TAC_FA_INTERFACE_TEMP T
     WHERE 1=1
     AND FILE_NAME = vFILENAME
     AND NVL(T.VALIDATE_FLAG,'N') IN ('S','E')
     --AND NVL(T.PROCESS_FLAG,'N') IN ('S','E')
     ORDER BY ASSET_BOQ_REGISTER;
     
     
     
    h_status        varchar2(10);
    h_msg_count     number;
    h_mSg_data      varchar2(512);
    h_asset_id      number;
    h_asset_number  varchar2(30);
    h_tran_id       number;
    h_dist_tran_id  number;
    h_book_class    varchar2(15);
    h_inv_txn_id    number;
    h_created_by    number;
    h_creation_date date;
    h_count         number;
    

    

    l_trans_rec         FA_API_TYPES.trans_rec_type;
    l_dist_trans_rec    FA_API_TYPES.trans_rec_type;
    l_asset_hdr_rec     FA_API_TYPES.asset_hdr_rec_type;
    l_asset_desc_rec    FA_API_TYPES.asset_desc_rec_type;
    l_asset_cat_rec     FA_API_TYPES.asset_cat_rec_type;
    l_asset_type_rec    FA_API_TYPES.asset_type_rec_type;
    l_asset_hierarchy_rec FA_API_TYPES.asset_hierarchy_rec_type;
    l_asset_fin_rec     FA_API_TYPES.asset_fin_rec_type;
    l_asset_deprn_rec   FA_API_TYPES.asset_deprn_rec_type;
    l_asset_dist_rec    FA_API_TYPES.asset_dist_rec_type;
    l_asset_dist_tbl    FA_API_TYPES.asset_dist_tbl_type;
    l_inv_tbl           FA_API_TYPES.inv_tbl_type;
    l_inv_rate_tbl      FA_API_TYPES.inv_rate_tbl_type;
    l_inv_rec           FA_API_TYPES.inv_rec_type;
    

    
    l_return_status     VARCHAR2(1);
    l_mesg_count        number ;
    l_mesg_len          number;
    l_mesg              varchar2(1000);
    V_CONC_REQ_ID       NUMBER := fnd_global.conc_request_id;
    vEMPLOYEE_ID NUMBER;
    l_host_name             varchar2(100) := get_host_name;
    

    vLOOP           NUMBER := 1;
    vCNT_LOOP       NUMBER := 1;
    i               NUMBER := 0;
    j               NUMBER := 0;
    
    vCHR           VARCHAR2(1000);
    vCNT_RECORD     NUMBER := 0;
    vCNT_RECORD_ERR NUMBER := 0;
    vCNT_RECORD_COMP NUMBER := 0;
    vGOOD_RECORD    NUMBER := 0;
    vCHK_ERR        NUMBER := 0;
    
    vPROJECT_ID    PA_PROJECTS_ALL.PROJECT_ID%TYPE;
    vVENDOR_ID     PO_VENDORS.VENDOR_ID%TYPE;
    vVENDOR_NAME   PO_VENDORS.VENDOR_NAME%TYPE;
    vLOCATION_ID   FA_LOCATIONS.LOCATION_ID%TYPE;
    vCATEGORY_ID   FA_CATEGORIES_B.CATEGORY_ID%TYPE;
    vPARENT_ASSET_ID  FA_ADDITIONS_B.ASSET_ID%TYPE;
    vDEPRN_METHOD  FA_CATEGORY_BOOK_DEFAULTS.DEPRN_METHOD%TYPE;
    vLIFE_IN_MONTHS FA_CATEGORY_BOOK_DEFAULTS.LIFE_IN_MONTHS%TYPE;
    vCOST NUMBER;
    vPRORATE_DATE  DATE;
    vEXPENSE_ID  FA_CATEGORY_BOOKS.DEPRN_EXPENSE_ACCT%TYPE; 
    vASSET_ID  FA_ADDITIONS_B.ASSET_ID%TYPE;

x_ret_code  varchar2(2000);
x_error_buf varchar2(2000);
l_msg_index_out number;
vATTRIBUTE5 VARCHAR2(150);
vATTRIBUTE6 VARCHAR2(150);
V_ASSET_BOQ_REGISTER  VARCHAR2(50);
vCNT_FILE NUMBER;
vERRORFLAG varchar2(1);


vVALIDATE_FLAG VARCHAR2(1);
vBeginDate  VARCHAR2(20);
vEndDate  VARCHAR2(20);

 vPATH_INBOX       VARCHAR2 (5120);
 vPATH_ARCHIVE     VARCHAR2 (5120);
 vPATH_ERROR       VARCHAR2 (5120);
 
 
 l_responsibility_id NUMBER; 
l_application_id NUMBER; 
l_user_id NUMBER; 
l_request_id NUMBER; 
l_return      boolean;
l_phase       varchar2(30);
l_status      varchar2(30);
l_dev_phase   varchar2(30);
l_dev_status  varchar2(30);
l_message     varchar2(1000);
 

BEGIN

  
  vPATH_INBOX := GET_PATH(P_FOLDER_INBOX);
  vPATH_ARCHIVE := GET_PATH(P_FOLDER_ARCHIVE);
  vPATH_ERROR := GET_PATH(P_FOLDER_ERROR);


IF vPATH_INBOX IS NULL 
OR vPATH_ARCHIVE IS NULL 
OR vPATH_ERROR IS NULL 
     THEN
           
     
     WRITE_LOG(to_char(sysdate,'DD-MON-YY HH24:MI:SS')||'|'||
                to_char(sysdate,'DD-MON-YY HH24:MI:SS')||'|'||
                l_host_name||'|'||  
                'TAC : Asset Register'||'|'||
                ''||'|'||
                ''||'|'||
                ''||'|'||
                ''||'|'||
                ''||'|'||
                ''||'|'||
                'E'||'|'||
                'Error FA006-020 : Not found Path '
                ,'LOG');
   
 ELSE 
 
    delete from tac_fa006_dir_list;
    commit;
    tac_fa006_get_dir_list(vPATH_INBOX);   
    
    
    DELETE FROM TAC_FA006_INBOX;
    COMMIT;
    INSERT INTO TAC_FA006_INBOX (SELECT * FROM tac_fa006_dir_list);
    COMMIT;
    
    vCNT_FILE := 0;
    
   BEGIN 
    select count(*)
    into vCNT_FILE
    from TAC_FA006_INBOX;
   EXCEPTION WHEN OTHERS THEN
     vCNT_FILE := 0;
   END; 
   
   
   DELETE FROM TAC_FA_INTERFACE_TEMP
   WHERE TRUNC(CREATION_DATE) < TRUNC(SYSDATE-60);
   COMMIT;
   
    
    
if vCNT_FILE = 0 then 
    WRITE_LOG(to_char(sysdate,'DD-MON-YY HH24:MI:SS')||'|'||
                to_char(sysdate,'DD-MON-YY HH24:MI:SS')||'|'||
                l_host_name||'|'||  
                'TAC : Asset Register'||'|'||
                ''||'|'||
                ''||'|'||
                ''||'|'||
                ''||'|'||
                ''||'|'||
                ''||'|'||
                'E'||'|'||
                'Error FA006-021 : Zero File'
                ,'LOG');
   
 else  
 
    
  
 FOR REC_FILE IN CUR_FILE
 LOOP
    
            vBeginDate := to_char(sysdate,'DD-MON-YY HH24:MI:SS');
        
            write_log('step1 - start read textfile','out');
            READ_TEXTFILE (pFILENAME => REC_FILE.FILENAME,
                           pFOLDER_INBOX    => P_FOLDER_INBOX,
                           pFOLDER_ARCHIVE  => P_FOLDER_ARCHIVE,
                           pFOLDER_ERROR    => P_FOLDER_ERROR);
            commit;
     
     
     
          write_log('step2 - validate data','out');
          VALIDATE_DATA (pFILENAME => REC_FILE.FILENAME,
                    oVALIDATE_FLAG => vVALIDATE_FLAG
                    );
                    
     
  
     vCNT_RECORD_err := 0;
     
  if vVALIDATE_FLAG = 'E' then
        null;
       /*** Move file to directory ERROR*/
          -- UTL_FILE.FCOPY (P_FOLDER_INBOX,REC_FILE.FILENAME,P_FOLDER_ERROR,REC_FILE.FILENAME);
          -- UTL_FILE.FREMOVE (P_FOLDER_INBOX,REC_FILE.FILENAME);
     
  else
  
  --  call submit concurrent
  
  FOR REC_DATE IN CUR_GROUP_DATE(REC_FILE.FILENAME) -- CURSOR GROUP DATE
  LOOP
  BEGIN 
-- 
        SELECT DISTINCT fr.responsibility_id, 
        frx.application_id 
        INTO l_responsibility_id, 
        l_application_id 
        FROM apps.fnd_responsibility frx, 
        apps.fnd_responsibility_tl fr 
        WHERE fr.responsibility_id = frx.responsibility_id 
        AND LOWER (fr.responsibility_name) LIKE LOWER('Fixed Assets Manager'); 
        
        l_user_id := FND_PROFILE.VALUE('USER_ID'); 
        -- --To set environment context. 
        apps.fnd_global.apps_initialize (l_user_id,l_responsibility_id,l_application_id); 
        -- --Submitting Concurrent Request 
            
            l_request_id := fnd_request.submit_request ( 
            application => 'OFA', 
            program => 'TAC_FA006_API', 
            description => 'TAC Asset Register API', 
            start_time => sysdate, 
            sub_request => FALSE, 
            argument1 => REC_FILE.FILENAME,
            argument2 => REC_DATE.DATE_PLACE_IN_SERVICE 
            ); 
            COMMIT;
            
            
    if nvl(l_request_id,0) > 0 then
            -- wait for REQIMPORT to complete
            LOOP
                l_return := FND_CONCURRENT.wait_for_request(
                                            l_request_id,
                                            5,              -- time b/w checks. Number of seconds to sleep
                                            10800,          -- Max amount of time to wait (in seconds)
                                            l_phase,l_status,l_dev_phase,l_dev_status,l_message);

                l_return := FND_CONCURRENT.get_request_status(l_request_id,NULL,NULL,
                                            l_phase,l_status,l_dev_phase,l_dev_status,l_message);

                IF (NVL(l_dev_phase,'ERROR') = 'COMPLETE') THEN
                    EXIT;
                END IF;
            END LOOP;

            IF (NVL(l_dev_status, 'ERROR') NOT IN ('NORMAL', 'WARNING')) THEN
                -- completed with error
                write_log ('Concurrent request failed to submit','out'); 
                --raise stop_run;

            ELSE
                -- completed with normal or warning
                write_log('Successfully Submitted the Concurrent Request','out'); 

            END IF;

        else
            write_log ('Concurrent request failed to submit','out'); 
            --raise stop_run;
        end if;
            
   /* IF l_request_id = 0 
    THEN 
        write_log ('Concurrent request failed to submit','out'); 
    ELSE 
        write_log('Successfully Submitted the Concurrent Request','out'); 
    END IF; 
    */

    EXCEPTION WHEN OTHERS THEN 
        write_log('Error While Submitting Concurrent Request '||TO_CHAR(SQLCODE)||'-'||sqlerrm,'out'); 
    END;   
  
  END LOOP;
         
  
   
  end if;  -- end pass validate flag
  
    --===========================================================--  
      
      vEndDate := to_char(sysdate,'DD-MON-YY HH24:MI:SS');
      vCNT_RECORD := 0;
      vCNT_RECORD_ERR := 0;
      vCNT_RECORD_COMP := 0;
      
   FOR REC_LOG IN CUR_LOG(REC_FILE.FILENAME)

    LOOP
      vCNT_RECORD := vCNT_RECORD + 1;
      vERRORFLAG := null;
      
      IF REC_LOG.VALIDATE_FLAG = 'E' or REC_LOG.PROCESS_FLAG = 'E' THEN
         vCNT_RECORD_ERR := vCNT_RECORD_ERR +1 ;
         vERRORFLAG := 'E';
      ELSE
         vCNT_RECORD_COMP := vCNT_RECORD_COMP +1 ;
         vERRORFLAG := 'S';
      END IF;

      WRITE_LOG(vBeginDate||'|'||
                vEndDate||'|'||
                l_host_name||'|'||
                'TAC : Asset Register'||'|'||
                REC_LOG.FILE_NAME||'|'||
                vCNT_RECORD||'|'||
                REC_LOG.PO_NUMBER||'|'||
                REC_LOG.PO_LINE||'|'||
                REC_LOG.ASSET_BOQ_REGISTER||'|'||
                REC_LOG.ASSET_NUMBER||'|'||
                vERRORFLAG||'|'||
                REC_LOG.ERROR_MESSAGE || ',' || REC_LOG.PROCESS_ERROR_MESSAGE
                ,'LOG');
    END LOOP;
    
    if vCNT_RECORD = vCNT_RECORD_COMP and vCNT_RECORD > 0 then
         UTL_FILE.FCOPY (p_FOLDER_INBOX,REC_FILE.FILENAME,p_FOLDER_ARCHIVE,REC_FILE.FILENAME);
         UTL_FILE.FREMOVE (p_FOLDER_INBOX,REC_FILE.FILENAME);
    else
         UTL_FILE.FCOPY (p_FOLDER_INBOX,REC_FILE.FILENAME,p_FOLDER_ERROR,REC_FILE.FILENAME);
         UTL_FILE.FREMOVE (p_FOLDER_INBOX,REC_FILE.FILENAME);
    end if;
    
    WRITE_LOG('===================================================================================================','LOG');
    --WRITE_LOG('FILE NAME = '||REC_FILE.FILENAME,'LOG');
    WRITE_LOG('TOTAL RECORD = '||vCNT_RECORD,'LOG');
    WRITE_LOG('COMPLETE = '||vCNT_RECORD_COMP,'LOG');
    WRITE_LOG('ERROR = '||vCNT_RECORD_ERR,'LOG');
    WRITE_LOG('===================================================================================================','LOG');
    
    WRITE_LOG('===================================================================================================','OUT');
    WRITE_LOG('FILE NAME = '||REC_FILE.FILENAME,'OUT');
    WRITE_LOG('TOTAL RECORD = '||vCNT_RECORD,'OUT');
    WRITE_LOG('COMPLETE = '||vCNT_RECORD_COMP,'OUT');
    WRITE_LOG('ERROR = '||vCNT_RECORD_ERR,'OUT');
    WRITE_LOG('===================================================================================================','OUT');
 
END LOOP; --LOOP FILE

end if;  -- end zero file
  
END IF;  -- end not found path
END;

PROCEDURE UPDATE_CLEARING(errbuf               OUT VARCHAR2,
                            retcode            OUT NUMBER,
                            P_PERIOD           IN VARCHAR2
                            )
IS

CURSOR C1 IS
select adj.asset_id,
ADJ.ADJUSTMENT_LINE_ID,
fai.PAYABLES_CODE_COMBINATION_ID
from fa_adjustments adj,
FA_ASSET_INVOICES fai,
FA_DEPRN_PERIODS p
where adj.asset_id = fai.asset_id
and fai.PAYABLES_CODE_COMBINATION_ID != adj.CODE_COMBINATION_ID
and adj.PERIOD_COUNTER_CREATED = p.period_counter
and adj.BOOK_TYPE_CODE = p.BOOK_TYPE_CODE
and adj.ADJUSTMENT_TYPE='COST CLEARING'
AND fai.PAYABLES_BATCH_NAME LIKE 'FA006%'
and p.period_name = P_PERIOD
;


BEGIN

    FOR REC1 IN C1
    LOOP
       
      BEGIN 
        update fa_adjustments
        set CODE_COMBINATION_ID = REC1.PAYABLES_CODE_COMBINATION_ID
        where asset_id = REC1.asset_id
        AND ADJUSTMENT_LINE_ID = REC1.ADJUSTMENT_LINE_ID
        and ADJUSTMENT_TYPE='COST CLEARING'
        ;
        
        COMMIT;
        
      EXCEPTION WHEN OTHERS THEN
          NULL;
      END;
    
    END LOOP;

END UPDATE_CLEARING;
 
end TAC_ASSET_NETWORK_PKG; 