create or replace PACKAGE      TAC_FA_POADD_PKG is                             
procedure tac_fa012_get_dir_list( p_directory in varchar2 );
PROCEDURE READ_TEXTFILE (pFILENAME IN VARCHAR,
                          pFOLDER_INBOX       IN VARCHAR2,
                          pFOLDER_ARCHIVE     IN VARCHAR2,
                          pFOLDER_ERROR       IN VARCHAR2);
PROCEDURE MAIN_POADD_INT(errbuf               OUT VARCHAR2,
                            retcode              OUT NUMBER,
                            P_FOLDER_INBOX       IN VARCHAR2,
                            P_FOLDER_ARCHIVE     IN VARCHAR2,
                            P_FOLDER_ERROR       IN VARCHAR2
                            );
  
end TAC_FA_POADD_PKG;
