CREATE OR REPLACE PACKAGE TAC_FA007_TEXT_PKG AUTHID CURRENT_USER AS
-- *****************************************************************************
-- Type:    Package Spec
-- Name:    TAC_FA007_TEXT_PKG 
-- Desc:    Package for Export FA Category to TXT file
-- 
-- Revision History
-- Date            Author           Reason for Change
-- ----------------------------------------------------------------
-- 29 MAY 2017     Thanawat T.      Created.
-- *****************************************************************************

--------------------------------------------------------------------------------
--##    GLOBAL Constant Variable
--------------------------------------------------------------------------------
GC_PREFIX_FILENAME      CONSTANT    VARCHAR2(200)       :=  'TAC_Asset_Master_PO_';
GC_FORMAT_FILENAME      CONSTANT    VARCHAR2(100)       :=  'DDMMYYYY_HH24MISS';
GC_FILE_TYPE            CONSTANT    VARCHAR2(10)        :=  '.txt';
GC_SEPARATER            CONSTANT    VARCHAR2(10)        :=  '|';

GC_COMPLETE             CONSTANT    VARCHAR2(240)       :=  'Complete';
GC_ERROR                CONSTANT    VARCHAR2(240)       :=  'Error';

--## Mail
--GC_MAIL_SUBJECT         CONSTANT    VARCHAR2(2000)      :=  'Export Master Asset Category Text file (Outbound): '|| to_char(SYSDATE,'DD-Mon-YYYY');
GC_MAIL_SUBJECT         CONSTANT    VARCHAR2(2000)      := '[ARMS] Export Master PO (Outbound)';
GC_MAIL_FROM            VARCHAR2(2000)                  :=  NULL;
GC_MAIL_TO              VARCHAR2(2000)                  :=  NULL;
GC_SMTP_HOST            VARCHAR2(2000)                  :=  NULL;
GC_SMTP_PORT            NUMBER                          :=  NULL;



--## Error
GC_ERR_INVALID_DATA     CONSTANT    VARCHAR2(250)       :=  'Invalid Data';
GC_ERR_CONN_FAILED      CONSTANT    VARCHAR2(250)       :=  'Connection Failed';
GC_ERR_NO_PATH          CONSTANT    VARCHAR2(250)       :=  'Cannot Export File';
GC_ERR_FILE_FAILED      CONSTANT    VARCHAR2(250)       :=  'Cannot Export File';


GC_ENCODING_TO          CONSTANT    VARCHAR2(250)       :=  'UTF8';
GC_DB_CHARSET           CONSTANT    VARCHAR2(250)       :=  'TH8TISASCII';



G_CONC_REQUEST_ID       NUMBER          := FND_GLOBAL.CONC_REQUEST_ID;
G_DEBUG_MODE            VARCHAR2(10)    :=  'N';

G_FORMAT_DATE           VARCHAR2(100)   :=  'DD/MM/RRRR HH24:MI:SS';


TYPE PARAMETERS_RT IS RECORD
(
    REQUEST_ID                      NUMBER
    ,ORG_ID                         NUMBER
    ,FILENAME                       VARCHAR2(240)
    ,TOTAL_REC                      NUMBER
    ----------------------------------------------------------------------------
    ,PO_DATE_FROM                   VARCHAR2(240)
    ,PO_DATE_TO                     VARCHAR2(240)
    ,COMPANY_CODE                   VARCHAR2(240)
    ,DIRECTORY_NAME                 VARCHAR2(250)
    ,DIRECTORY_PATH                 VARCHAR2(240)
);


TYPE TRACE_LOG_RT IS RECORD
(
    BEGIN_DATE                      VARCHAR2(240)
    ,END_DATE                       VARCHAR2(240)
    ,HOST                           VARCHAR2(240)
    ,JOB_NAME                       VARCHAR2(240)
    ,DEST_FILE                      VARCHAR2(240)
    ,NUM_RECORDS                    VARCHAR2(240)
    ,STATUS                         VARCHAR2(100)
    ,ERROR_MESSAGE                  VARCHAR2(4000)
);


--## Program Log ARRAY
TYPE G_LOG_TAB IS TABLE OF TRACE_LOG_RT INDEX BY BINARY_INTEGER;
G_LOG_TABLE_TY        G_LOG_TAB;





PROCEDURE WRITE_OUTPUT(PARAM_MSG VARCHAR2);

PROCEDURE WRITE_LOG(PARAM_MSG VARCHAR2);

FUNCTION WRITE_FILE(P_I_PARAM_RT         IN  PARAMETERS_RT
                  ,P_O_TOTAL_REC  OUT NUMBER
                  )
RETURN VARCHAR2;

PROCEDURE INSERT_LOG_ARRAY(P_I_MSG  IN  VARCHAR2);


PROCEDURE  MAIL_NOTIFICATION(P_I_PARAM_RT   IN  PARAMETERS_RT
                          ,P_I_EXP_STATUS   IN  VARCHAR2
                          );

PROCEDURE PROGRAM_LOG(P_I_PARAM_RT   IN  PARAMETERS_RT
                   ,P_I_EXP_STATUS IN  VARCHAR2   DEFAULT NULL
                   );


function CF_GRN_QTY(P_I_MATCH_TYPE  IN VARCHAR2
                  ,P_I_GRN_QTY    IN  NUMBER
                  ,P_I_GRN_AMOUNT IN  NUMBER
                  ,P_I_UNIT_PRICE IN  NUMBER
                  ) 
RETURN NUMBER ;

FUNCTION ZZCF_AMOUNT(P_I_TYPE_OF_WORK         IN  VARCHAR2
                  ,P_I_GRN_AMOUNT         IN  NUMBER
                  ,P_I_REGISTER_AMOUNT    IN  NUMBER
                  ,P_I_PROGRESS_AMT       IN  NUMBER
                  ,P_O_CP_BOOKED_AMT      OUT NUMBER
                  ,P_O_CP_WIP_AMT         OUT NUMBER
                  ,P_O_CP_WIP_ACCRUED     OUT NUMBER
                  ,P_O_CP_PREPAID_15005   OUT NUMBER
                  ,P_O_CP_PREPAID_ACCRUAL OUT NUMBER
                  )
RETURN NUMBER ;

PROCEDURE CF_FA_DETAIL
(
  P_I_PO_HEADER_ID    IN  NUMBER
  ,P_I_LINE_NUM       IN  NUMBER
  -----------------------------------
  ,P_O_CIM            OUT  VARCHAR2
  ,P_O_FA_CAT         OUT  VARCHAR2
  ,P_O_BRAND          OUT  VARCHAR2
  ,P_O_MODEL_NUMBER   OUT  VARCHAR2
  ,P_O_COST_RETIRED   OUT  VARCHAR2
);


PROCEDURE CF_PO_ADD_INFO(P_I_PO_NUM             IN  VARCHAR2
                      ,P_I_LINE_NUM           IN  NUMBER
                      
                      ,P_O_FOLLOW_UP          OUT VARCHAR2
                      ,P_O_CP_BOQ_REQUIRE     OUT VARCHAR2
                      ,P_O_CP_INCLUDE_BOOK    OUT VARCHAR2
                      );

FUNCTION CF_GROUP_NAME(P_I_GROUP_NO IN  VARCHAR2)
RETURN VARCHAR2 ;

PROCEDURE MAIN
(
  ERR_MSG             OUT VARCHAR2
 ,ERR_CODE            OUT VARCHAR2
 ,P_I_ORG_ID          IN  NUMBER          DEFAULT NULL    --## Parameter 1
 ,P_I_PO_DATE_FROM    IN  VARCHAR2        DEFAULT NULL    --## Parameter 2
 ,P_I_PO_DATE_TO      IN  VARCHAR2        DEFAULT NULL    --## Parameter 3
 -------------------------------------------------------------------------
 ,P_I_DIR_NAME        IN  VARCHAR2        DEFAULT NULL    --## PARAMETER 4
 ,P_I_SMTP_HOST       IN  VARCHAR2        DEFAULT NULL    --## Parameter 5
 ,P_I_SMTP_PORT       IN  NUMBER          DEFAULT NULL    --## Parameter 6
 ,P_I_MAIL_FROM       IN  VARCHAR2        DEFAULT NULL    --## Parameter 7
 ,P_I_MAIL_TO         IN  VARCHAR2        DEFAULT NULL    --## Parameter 8
);


function CF_AMOUNT_V2(P_I_TYPE_OF_WORK         IN  VARCHAR2
                  ,P_I_GRN_AMOUNT         IN  NUMBER
                  ,P_I_REGISTER_AMOUNT    IN  NUMBER
                  ,P_I_PROGRESS_AMT       IN  NUMBER
                  ,P_I_PO_NUMBER          IN  VARCHAR2    --## Added  26-Jun-2017
                  ,P_I_PO_LINE_NUMBER     IN  NUMBER      --## Added  26-Jun-2017
                  ,P_O_CP_BOOKED_AMT      OUT NUMBER
                  ,P_O_CP_WIP_AMT         OUT NUMBER
                  ,P_O_CP_WIP_ACCRUED     OUT NUMBER
                  ,P_O_CP_PREPAID_15005   OUT NUMBER
                  ,P_O_CP_PREPAID_ACCRUAL OUT NUMBER
                  ,P_O_COST_RETIRED       OUT NUMBER      --## Added  26-Jun-2017
                  ,P_O_CF_REGISTER_AMOUNT OUT NUMBER      --## Added  26-Jun-2017
                  )
RETURN NUMBER  ;



END ; 
/
CREATE OR REPLACE PACKAGE BODY TAC_FA007_TEXT_PKG AS
-- *****************************************************************************
-- Type:    Package Body
-- Name:    TAC_FA007_TEXT_PKG 
-- Desc:    Package for Export FA Category to TXT file
-- 
-- Revision History
-- Date            Author           Reason for Change
-- ----------------------------------------------------------------
-- 29 MAY 2017     Thanawat T.      Created.
-- *****************************************************************************

-- *****************************************************************************
-- Type:    FUNCTION
-- Name:    GET_SERVER_PATH
-- Desc:    Get Path from Server 
--
-- Revision History
-- Date            Author           Reason for Change
-- ----------------------------------------------------------------
-- 29 MAY 2017     Thanawat T.      Created.
-- *****************************************************************************
FUNCTION GET_SERVER_PATH(P_I_DIRECTORY_NAME   IN  VARCHAR2)
RETURN VARCHAR2
IS

    v_path          ALL_DIRECTORIES.DIRECTORY_PATH%TYPE :=  NULL;

BEGIN

    SELECT  AD.DIRECTORY_PATH
    INTO    v_path
    FROM    ALL_DIRECTORIES     AD
    WHERE   1   =   1
    AND     DIRECTORY_NAME  =   P_I_DIRECTORY_NAME
    ;


    RETURN  v_path;

EXCEPTION
    WHEN NO_DATA_FOUND THEN
        --write_log('Not found directory name:  ' || P_I_DIRECTORY_NAME || ' on server.');
        RETURN  NULL;
        
    WHEN OTHERS THEN
        write_log('ERROR!! Function : GET_SERVER_PATH  Error Code: ' || SQLCODE || ' - ' || SQLERRM);
        RAISE;
        RETURN NULL;
        
END GET_SERVER_PATH;


-- *****************************************************************************
-- Type:    PROCEDURE
-- Name:    MAIN
-- Desc:    Main procedure called from concurrent "XX TAC Asset Master PO Report (Text)"
--          1. Generate data to text file delimited by |
-- 
-- Revision History
-- Date            Author           Reason for Change
-- ----------------------------------------------------------------
-- 29 MAY 2017     Thanawat T.      Created.
-- *****************************************************************************
PROCEDURE MAIN
(
    ERR_MSG             OUT VARCHAR2
   ,ERR_CODE            OUT VARCHAR2
   ,P_I_ORG_ID          IN  NUMBER          DEFAULT NULL    --## Parameter 1
   ,P_I_PO_DATE_FROM    IN  VARCHAR2        DEFAULT NULL    --## Parameter 2
   ,P_I_PO_DATE_TO      IN  VARCHAR2        DEFAULT NULL    --## Parameter 3
   -------------------------------------------------------------------------
   ,P_I_DIR_NAME        IN  VARCHAR2        DEFAULT NULL    --## PARAMETER 4
   ,P_I_SMTP_HOST       IN  VARCHAR2        DEFAULT NULL    --## Parameter 5
   ,P_I_SMTP_PORT       IN  NUMBER          DEFAULT NULL    --## Parameter 6
   ,P_I_MAIL_FROM       IN  VARCHAR2        DEFAULT NULL    --## Parameter 7
   ,P_I_MAIL_TO         IN  VARCHAR2        DEFAULT NULL    --## Parameter 8
)
IS

    V_DEBUG                 NUMBER          :=  0;
    V_COMPLETED_FLAG        VARCHAR2(10)    :=  'C';
    
    V_FILENAME              VARCHAR2(240)   :=  NULL;
    V_OUTBOUND_REC          NUMBER          :=  0;
    
    V_PARAM_RT              PARAMETERS_RT;
    V_EXPORT_STATUS         VARCHAR2(10)    :=  NULL;
    
BEGIN

    
    
    write_output('+***************************************************************************+');
    write_output('TAC Asset Master PO Report (Text)');
    write_output('Start Date: ' || to_char(sysdate, 'DD/MM/YYYY HH24:MI:SS'));
    write_output('+***************************************************************************+');
    write_output('Agruments');
    write_output('--------------------------------------------');
    write_output('Org ID : ' || P_I_ORG_ID);
    write_output('PO Date From : ' || TO_CHAR(TO_DATE(P_I_PO_DATE_FROM,'RRRR/MM/DD HH24:MI:SS'),G_FORMAT_DATE));    
    write_output('PO Date To : ' || TO_CHAR(TO_DATE(P_I_PO_DATE_TO,'RRRR/MM/DD HH24:MI:SS'),G_FORMAT_DATE));
    write_output('Directory Name : ' || P_I_DIR_NAME);
    
    
    V_DEBUG :=  10;
    --##Define Filename
    V_FILENAME  :=  GC_PREFIX_FILENAME
                    || TO_CHAR (SYSDATE, GC_FORMAT_FILENAME)
                    || GC_FILE_TYPE;
                    
    
    --## Mail Define
    GC_MAIL_FROM    :=  P_I_MAIL_FROM;
    GC_MAIL_TO      :=  P_I_MAIL_TO;
    GC_SMTP_HOST    :=  P_I_SMTP_HOST;
    GC_SMTP_PORT    :=  P_I_SMTP_PORT;
      
    
    --## Assign Parameter
    V_PARAM_RT                  :=  NULL;
    V_PARAM_RT.REQUEST_ID       :=  G_CONC_REQUEST_ID;
    V_PARAM_RT.ORG_ID           :=  P_I_ORG_ID;
    --V_PARAM_RT.PO_DATE_FROM     :=  TO_DATE(P_I_PO_DATE_FROM,'RRRR/MM/DD HH24:MI:SS');    --## Don't change type because some convert date in each function
    --V_PARAM_RT.PO_DATE_TO       :=  TO_DATE(P_I_PO_DATE_TO,'RRRR/MM/DD HH24:MI:SS');      --## Don't change type because some convert date in each function
    V_PARAM_RT.PO_DATE_FROM     :=  P_I_PO_DATE_FROM;
    V_PARAM_RT.PO_DATE_TO       :=  P_I_PO_DATE_TO;
    V_PARAM_RT.DIRECTORY_NAME   :=  P_I_DIR_NAME;
    V_PARAM_RT.DIRECTORY_PATH   :=  GET_SERVER_PATH(P_I_DIR_NAME);
    V_PARAM_RT.FILENAME         :=  V_FILENAME;
    
    
    IF NVL(G_DEBUG_MODE,'N') = 'Y' THEN
        write_output('----- Debug Program Parameter -----');
        write_output('V_PARAM_RT.PO_DATE_FROM : ' || V_PARAM_RT.PO_DATE_FROM);
        write_output('V_PARAM_RT.PO_DATE_TO : ' || V_PARAM_RT.PO_DATE_TO);
        write_output('V_PARAM_RT.DIRECTORY_PATH : ' || V_PARAM_RT.DIRECTORY_PATH);
        write_output('V_PARAM_RT.FILENAME : ' || V_PARAM_RT.FILENAME);
    END IF;
    
    
    --## Validate Directory Path on Server
    IF V_PARAM_RT.DIRECTORY_PATH IS NULL THEN
        INSERT_LOG_ARRAY('Not found directory name: ' || P_I_DIR_NAME || ' on server.');
        V_COMPLETED_FLAG    :=  'E';
    END IF;
    
    
    --## Validate Date From and Date To
    IF TO_DATE(V_PARAM_RT.PO_DATE_FROM,'RRRR/MM/DD HH24:MI:SS') > TO_DATE(V_PARAM_RT.PO_DATE_TO,'RRRR/MM/DD HH24:MI:SS') THEN
        INSERT_LOG_ARRAY('PO Date From can not be greater than PO Date To ');
        V_COMPLETED_FLAG    :=  'E';
    END IF;
    
    
    
    --## Write FILE when No Validation Error
    V_DEBUG :=  20;
    IF V_COMPLETED_FLAG = 'C' THEN
        V_COMPLETED_FLAG            :=  WRITE_FILE(V_PARAM_RT,V_OUTBOUND_REC);
    
    
        --## Keep Total Record
        V_DEBUG :=  30;
        V_PARAM_RT.TOTAL_REC        :=  V_OUTBOUND_REC;
    END IF;
    
    
    
    V_DEBUG :=  100;
    --## Log
    write_output('+***************************************************************************+');
    write_output('Status: ' || V_COMPLETED_FLAG);
    write_output('Request_id: ' || V_PARAM_RT.REQUEST_ID);
    write_output('Server Path: ' || V_PARAM_RT.DIRECTORY_PATH);
    write_output('Total Record: ' || V_PARAM_RT.TOTAL_REC);
    write_output('+***************************************************************************+');
    
    
    
    --## Mail Notification for show export Status
    V_DEBUG :=  40;
    MAIL_NOTIFICATION(V_PARAM_RT,V_COMPLETED_FLAG);
    
    
    --## Program Log
    V_DEBUG :=  50;
    PROGRAM_LOG(V_PARAM_RT,V_COMPLETED_FLAG);
    
    
    
    --## Return Status
    V_DEBUG :=  60;
    IF NVL(V_COMPLETED_FLAG,'E') = 'C' THEN
        ERR_CODE    :=  '0';
    
    ELSE
        ERR_CODE    :=  '2';
    
    END IF;
    

EXCEPTION
    WHEN OTHERS THEN
        write_log(V_DEBUG || ' ERROR!! TAC_FA007_TEXT_PKG.MAIN  Error Code: ' || SQLCODE || ' - ' || SQLERRM);
        ERR_CODE := '2';
        RAISE;
END;




-- *****************************************************************************
-- Type:    PROCEDURE
-- Name:    WRITE_FILE
-- Desc:    Write file to server
--          File format: TAC_Asset_Master_PO_ddmmyyyy_hh24miss.txt
-- 
-- Revision History
-- Date            Author           Reason for Change
-- ----------------------------------------------------------------
-- 29 MAY 2017      Thanawat T.     Created.
-- 26 JUN 2017      Thanawat T.     1. Not get value from CLOUD
-- *****************************************************************************
FUNCTION WRITE_FILE(P_I_PARAM_RT    IN  PARAMETERS_RT
                    ,P_O_TOTAL_REC  OUT NUMBER
                    )
RETURN VARCHAR2
IS

    V_DEBUG                 NUMBER  :=  0;

    FS                    UTL_FILE.FILE_TYPE;
    L_SRC_FILENAME        VARCHAR2 (100);
    VDR                   VARCHAR2 (4000);
    L_OU                  VARCHAR2 (10);

    V_COMPLETED_FLAG      VARCHAR2(10)    :=  'C';

    V_COUNT_REC             NUMBER          :=  0;
  

    V_CF_AMOUNT             NUMBER          :=  0;
    CP_BOOKED_AMT           NUMBER          :=  0;
    CP_WIP_AMT              NUMBER          :=  0;
    CP_WIP_ACCRUED          NUMBER          :=  0;  
    CP_PREPAID_15005        NUMBER          :=  0;
    CP_PREPAID_ACCRUAL      NUMBER          :=  0;
    CP_COST_RETIRED         NUMBER          :=  0;      --## Added @26-Jun-2017
    CF_REGISTER_AMOUNT      NUMBER          :=  0;      --## Added @26-Jun-2017
    
    
    
    
    V_CIM               VARCHAR2(240)   :=  NULL;
    V_FA_CAT            VARCHAR2(240)   :=  NULL;
    V_BRAND             VARCHAR2(240)   :=  NULL;
    V_MODEL_NUMBER      VARCHAR2(240)   :=  NULL;
    V_COST_RETIRED      VARCHAR2(240)   :=  NULL;
    
    V_FOLLOW_UP         VARCHAR2(240)   :=  NULL;
    CP_BOQ_REQUIRE      VARCHAR2(240)   :=  NULL;
    CP_INCLUDE_BOOK     VARCHAR2(240)   :=  NULL;


    CURSOR CUR_DATA
    IS
        SELECT  DISTINCT (SELECT ou.NAME
                    FROM hr_operating_units  ou
                    WHERE organization_id = POH.ORG_ID)    OU   --A
                 ,POH.po_header_id  
                 ,POL.po_line_id 
                 ,PLL.PO_RELEASE_ID
                 ,PPA.SEGMENT1          PROJECT_CODE            --B
                 ,POH.SEGMENT1          
                  || TAC_FA_REPORT_PKG.GET_RELEASE_NUM(/*POH.SEGMENT1,POL.LINE_NUM,P_I_PARAM_RT.PO_DATE_FROM,P_I_PARAM_RT.PO_DATE_TO,*/
                                                       PLL.PO_RELEASE_ID) PO_NUM --C
                 ,POL.LINE_NUM          LINE_NUM                --D
                 ,nvl(TAC_FA_REPORT_PKG.GET_TYPE_OF_WORK(pod.REQ_DISTRIBUTION_ID
                                                         ,POH.SEGMENT1|| TAC_FA_REPORT_PKG.GET_RELEASE_NUM(PLL.PO_RELEASE_ID)
                                                         ,POL.LINE_NUM),'u') TYPE_OF_WORK  --E
                 ,CASE 
                   WHEN PLL.INSPECTION_REQUIRED_FLAG  = 'N' AND PLL.RECEIPT_REQUIRED_FLAG = 'N'  THEN
                      '2-Way'
                   WHEN PLL.INSPECTION_REQUIRED_FLAG  = 'N' AND PLL.RECEIPT_REQUIRED_FLAG = 'Y'  THEN
                      '3-Way'
                   WHEN PLL.INSPECTION_REQUIRED_FLAG  = 'Y' AND PLL.RECEIPT_REQUIRED_FLAG = 'Y'  THEN
                      '4-Way'
                 END  MATCH_TYPE    --F  
                ,POH.CURRENCY_CODE    CURR    --G
                ,nvl(POH.RATE,1)      EX_RATE    --H
                ,POL.UNIT_PRICE
                ,(POL.UNIT_PRICE * TAC_FA_REPORT_PKG.GET_DIS_QTY(POH.PO_HEADER_ID,
                                                                 POL.PO_LINE_ID,pll.po_release_id)) AMT_CURR  --I
                 ,TAC_FA_REPORT_PKG.GET_DIS_QTY(POH.PO_HEADER_ID,
                                                POL.PO_LINE_ID,pll.po_release_id)  PO_QTY      --J
                ,POL.UNIT_MEAS_LOOKUP_CODE        UOM              --K
                ,(POL.UNIT_PRICE * TAC_FA_REPORT_PKG.GET_DIS_QTY(POH.PO_HEADER_ID,
                                                 POL.PO_LINE_ID,pll.po_release_id)) * NVL(POH.RATE ,1)   AMT_THB   --L
                ,TAC_FA_REPORT_PKG.GET_GRN_AMT_PO(POH.SEGMENT1|| TAC_FA_REPORT_PKG.GET_RELEASE_NUM(PLL.PO_RELEASE_ID)
                                                  ,POL.LINE_NUM) 
                  + TAC_FA_REPORT_PKG.GET_GRN_AMT_INV(POH.SEGMENT1||TAC_FA_REPORT_PKG.GET_RELEASE_NUM(PLL.PO_RELEASE_ID)
                                                      ,POL.LINE_NUM
                                                      )
                  + TAC_FA_REPORT_PKG.GET_GRN_AMT_GL(POH.SEGMENT1,POL.LINE_NUM
                                                    ,TAC_FA_REPORT_PKG.GET_RELEASE_NUM(PLL.PO_RELEASE_ID)
                                                    ,PLL.PO_RELEASE_ID) 
                 GRN_AMOUNT         --M
                 
                ,TAC_FA_REPORT_PKG.GET_GRN_QTY(POH.PO_HEADER_ID,POL.PO_LINE_ID,pll.po_release_id) GRN_QTY          --N
                
                /* --## Modified by SA @26-Jun-2017 => Not get value from CLOUD
                ,TAC_FA_REPORT_PKG.GET_REGIST_AMT_FA(POH.SEGMENT1|| TAC_FA_REPORT_PKG.GET_RELEASE_NUM(PLL.PO_RELEASE_ID)
                                                    ,POL.LINE_NUM)
                 + TAC_FA_REPORT_PKG.GET_REGIST_AMT_CLOUD(POH.SEGMENT1|| TAC_FA_REPORT_PKG.GET_RELEASE_NUM(PLL.PO_RELEASE_ID)
                                                         ,POL.LINE_NUM) REGISTER_AMOUNT         --O
                
                ,TAC_FA_REPORT_PKG.GET_REGIST_QTY_FA(POH.SEGMENT1|| TAC_FA_REPORT_PKG.GET_RELEASE_NUM(PLL.PO_RELEASE_ID)
                                                     ,POL.LINE_NUM)
                 + TAC_FA_REPORT_PKG.GET_REGIST_QTY_CLOUD(POH.SEGMENT1|| TAC_FA_REPORT_PKG.GET_RELEASE_NUM(PLL.PO_RELEASE_ID)
                                                         ,POL.LINE_NUM) REGISTER_QTY         --Q 
                */
                
                --## Only Text File not use CLOUD
                ,TAC_FA_REPORT_PKG.GET_REGIST_AMT_FA(POH.SEGMENT1|| TAC_FA_REPORT_PKG.GET_RELEASE_NUM(PLL.PO_RELEASE_ID)
                                                    ,POL.LINE_NUM)      REGISTER_AMOUNT
                
                ,TAC_FA_REPORT_PKG.GET_REGIST_QTY_FA(POH.SEGMENT1|| TAC_FA_REPORT_PKG.GET_RELEASE_NUM(PLL.PO_RELEASE_ID)
                                                     ,POL.LINE_NUM)     REGISTER_QTY
                
                ,TAC_FA_REPORT_PKG.GET_PROGRESS_AMT_PO(POH.SEGMENT1|| TAC_FA_REPORT_PKG.GET_RELEASE_NUM(PLL.PO_RELEASE_ID)
                                                      ,POL.LINE_NUM)  PROGRESS_AMT       --R
                ,TAC_FA_REPORT_PKG.GET_PROGRESS_QTY_PO(POH.SEGMENT1|| TAC_FA_REPORT_PKG.GET_RELEASE_NUM(PLL.PO_RELEASE_ID)
                                                      ,POL.LINE_NUM)  PROGRESS_QTY       --S                                         
                ,TAC_FA_REPORT_PKG.GET_ACCOUNT_WIP(POH.PO_HEADER_ID,POL.PO_LINE_ID,POD.PROJECT_ID)     ACCOUNT_WIP --Z
                ,TAC_FA_REPORT_PKG.GET_SYSTEM_CODE(pod.REQ_DISTRIBUTION_ID) SYSTEM_CODE
                ,REPLACE(NVL(POH.COMMENTS,TAC_FA_REPORT_PKG.GET_PR_DESC(pod.REQ_DISTRIBUTION_ID)),CHR(10),' ')   DESCRIPTION       --AC
                ,replace(POL.ITEM_DESCRIPTION,CHR(10),' ')    ITEM_DESC   --AD

                ,nvl(TAC_FA_REPORT_PKG.GET_APPROVED_DATE(/*POH.SEGMENT1,POL.LINE_NUM,P_I_PARAM_RT.PO_DATE_FROM,P_I_PARAM_RT.PO_DATE_TO,*/
                                                        PLL.PO_RELEASE_ID),trunc(poh.APPROVED_DATE) ) PO_APP_DATE
                --,POA.ACTION_CODE    PO_STATUS    --AM
                ,POH.AUTHORIZATION_STATUS  PO_STATUS    --AM
                ,( SELECT  DISTINCT DECODE (NVL (pol.closed_code, 'OPEN'),'OPEN', 'OPEN',polc.displayed_field )
                                     || DECODE (pol.cancel_flag,'Y', ',' || '' || polc.displayed_field,NULL )
                    FROM  po_lookup_codes polc
                    WHERE polc.lookup_code(+) = NVL (pol.closed_code, 'OPEN')
                    )   CLOSURE_STATUS           --AN
                 ,POH.ATTRIBUTE7       INCLUDE_BOOK    --AR
                 ,PV.SEGMENT1        SUP_CODE--AI
                 ,PV.VENDOR_NAME       SUP_NAME --AJ
                 ,(SELECT DISTINCT  PAPF.FULL_NAME EMPLOYEE
                   FROM PER_ALL_PEOPLE_F PAPF
                   WHERE  PAPF.PERSON_ID = POD.DELIVER_TO_PERSON_ID  
                   and CURRENT_EMPLOYEE_FLAG = 'Y'
                   and nvl(EFFECTIVE_END_DATE,sysdate) > SYSDATE
                   --## Added for fix BUG following iCE fixed @17-Jul-2017
                    AND ROWNUM = 1
                   )     REQUESTER--AK
                -- , POH.ATTRIBUTE6     FOLLOW_UP   --AL
                 , PPA.LONG_NAME         PROJECT_NAME     --AO
                 ,DECODE(PPA.PROJECT_STATUS_CODE , 'APPROVED','Y', 'CLOSED','N')     ACTIVE    --AQ 
                 ,(SELECT  PAPF.FULL_NAME
                    FROM PER_ALL_PEOPLE_F PAPF
                        ,PA_PROJECT_PLAYERS PAP
                    WHERE  PAPF.PERSON_ID =  PAP.PERSON_ID
                    AND  PAP.PROJECT_ID  =   PPA.PROJECT_ID
                    AND PAP.END_DATE_ACTIVE IS NULL  
                    and PROJECT_ROLE_TYPE = (select min(PROJECT_ROLE_TYPE) from PA_PROJECT_PLAYERS
                                                  where PROJECT_ID  =   PPA.PROJECT_ID
                                                  and END_DATE_ACTIVE IS NULL )    
                    --## Added for fix BUG following iCE fixed @17-Jul-2017
                    AND ROWNUM = 1
                    )  PM       --AV    
                 ,POL.CATEGORY_ID
                 ,POH.ORG_ID

                 ,PPA.ATTRIBUTE1  PROJECT_GROUP --AP
                 ,PPA.ATTRIBUTE2  GROUP_NO --AT 
                 ,PPA.ATTRIBUTE3  DIVISION --AS
                 
            FROM   PO_HEADERS_ALL          POH
                   ,PO_LINES_ALL           POL
                   ,PO_LINE_LOCATIONS_ALL  PLL        
                   ,PO_DISTRIBUTIONS_ALL   POD 
                   --,PO_ACTION_HISTORY      POA   
                   --,RCV_TRANSACTIONS       RCV 
                   ,PO_VENDORS             PV  
                   ,PA_PROJECTS_ALL        PPA 
                   ,GL_CODE_COMBINATIONS_KFV GLCC  --ktp add for new condition 18-Apr-2017   
                  
                        
            WHERE  POH.PO_HEADER_ID =  POL.PO_HEADER_ID
            AND POH.AUTHORIZATION_STATUS = 'APPROVED'
            AND POL.PO_HEADER_ID = PLL.PO_HEADER_ID
            AND POL.PO_LINE_ID = PLL.PO_LINE_ID
            --AND POL.PO_HEADER_ID = RCV.PO_HEADER_ID(+)
            --AND POL.PO_LINE_ID   = RCV.PO_LINE_ID(+)     
            --AND RCV.TRANSACTION_TYPE(+) = 'RECEIVE'   
            AND POH.PO_HEADER_ID = POD.PO_HEADER_ID  
            AND POL.PO_LINE_ID = POD.PO_LINE_ID  
            and pod.LINE_LOCATION_ID = pll.LINE_LOCATION_ID
            --AND POH.PO_HEADER_ID =  POA.OBJECT_ID 
            ----AND POA.OBJECT_SUB_TYPE_CODE = 'STANDARD'  -- ktp not check this case coz include blanket
            --AND POA.ACTION_CODE = 'APPROVE'
            --and poh.TYPE_LOOKUP_CODE = POA.OBJECT_SUB_TYPE_CODE
            AND NVL(POL.CANCEL_FLAG,'N') != 'Y'  --KTP
            AND POH.VENDOR_ID = PV.VENDOR_ID
            AND POD.PROJECT_ID =  PPA.PROJECT_ID(+) 
            AND POD.BUDGET_ACCOUNT_ID = GLCC.CODE_COMBINATION_ID(+)

            AND ( TAC_FA_REPORT_PKG.GET_ACCOUNT_WIP(POH.PO_HEADER_ID,POL.PO_LINE_ID,POD.PROJECT_ID) 
                  --is not null
                  IN (select lookup_code from fnd_lookup_values Where lookup_type = 'TAC_DTN_ASSET_REP_ACCOUNT')

                  or  TAC_FA_REPORT_PKG.CHECK_POADD_RP(POH.SEGMENT1,POL.LINE_NUM) = 'Y' 
                )   
            AND  NVL(TAC_FA_REPORT_PKG.CHECK_POADD_RP(POH.SEGMENT1,POL.LINE_NUM),'Y') != 'N' 
                  
                  
            AND (POL.UNIT_PRICE * TAC_FA_REPORT_PKG.GET_DIS_QTY(POH.PO_HEADER_ID,
                                                 POL.PO_LINE_ID,pll.po_release_id)) * NVL(POH.RATE ,1) != 0
                                                 

                                                 
            ----PARAMETER----
            --AND POH.SEGMENT1 = '991008019103' --'991015004384'
            -- '8015000234' line 3 po in condition but display flag ='N' not be show
            -- '8014004190' po not in condition but display flag = 'Y' must be show
            --'991008019103' blanket
            --and pol.line_num = 3
            --and pll.PO_RELEASE_ID = 36710
            --and ppa.segment1 = '04F03115001'
            AND POH.ORG_ID =  NVL(P_I_PARAM_RT.ORG_ID,POH.ORG_ID)
            and ( nvl(TAC_FA_REPORT_PKG.GET_APPROVED_DATE(PLL.PO_RELEASE_ID),trunc(poh.APPROVED_DATE) )
                  >=  TRUNC(TO_DATE(P_I_PARAM_RT.PO_DATE_FROM,'YYYY/MM/DD HH24:MI:SS')) 
                 or P_I_PARAM_RT.PO_DATE_FROM is null)
            and ( nvl(TAC_FA_REPORT_PKG.GET_APPROVED_DATE(PLL.PO_RELEASE_ID),trunc(poh.APPROVED_DATE) )
                 <=  TRUNC(TO_DATE(P_I_PARAM_RT.PO_DATE_TO,'YYYY/MM/DD HH24:MI:SS')) 
                 or P_I_PARAM_RT.PO_DATE_TO is null)


            ORDER BY   POH.po_header_id  ,POL.po_line_id ,PLL.PO_RELEASE_ID
            ;
    

BEGIN


    --## Filename
    V_DEBUG     :=  10;
    l_src_filename  :=  P_I_PARAM_RT.FILENAME;
    
                     
    --## Open File for writing
    V_DEBUG     :=  20;
    FS  :=  UTL_FILE.FOPEN (P_I_PARAM_RT.DIRECTORY_NAME, L_SRC_FILENAME, 'W');
    
    
    --## Heading COLUMN
    V_DEBUG     :=  30;
    vdr :=  (   
                'Operation Unit'                                                                    -- 1
                ||  GC_SEPARATER ||  'Project Code'                                                 -- 2
                ||  GC_SEPARATER ||  'PO No.'                                                       -- 3
                ||  GC_SEPARATER ||  'PO Line'                                                      -- 4
                ||  GC_SEPARATER ||  'Type of Work'                                                 -- 5
                ||  GC_SEPARATER ||  'Matching Type'                                                -- 6
                ||  GC_SEPARATER ||  'Currency'                                                     -- 7
                ||  GC_SEPARATER ||  'Exchange Rate'                                                -- 8
                ||  GC_SEPARATER ||  'PO Amount (Currency)'                                         -- 9
                ||  GC_SEPARATER ||  'PO QTY'                                                       -- 10
                ||  GC_SEPARATER ||  'UOM'                                                          -- 11
                ||  GC_SEPARATER ||  'PO Amount'                                                    -- 12
                ||  GC_SEPARATER ||  'GRN Amount'                                                   -- 13
                ||  GC_SEPARATER ||  'GRN QTY'                                                      -- 14
                ||  GC_SEPARATER ||  'Register Amount'                                              -- 15
                ||  GC_SEPARATER ||  'Register QTY'                                                 -- 16
                ||  GC_SEPARATER ||  'Progress Amount'                                              -- 17
                ||  GC_SEPARATER ||  'Progress QTY'                                                 -- 18
                ||  GC_SEPARATER ||  'Booked Amount'                                                -- 19
                ||  GC_SEPARATER ||  'WIP'                                                          -- 20
                ||  GC_SEPARATER ||  'WIP Accrued'                                                  -- 21
                ||  GC_SEPARATER ||  'Prepaid 15005'                                                -- 22
                ||  GC_SEPARATER ||  'Prepaid Accrual'                                              -- 23
                ||  GC_SEPARATER ||  'Write-Off or Disposal'                                        -- 24
                ||  GC_SEPARATER ||  'Account WIP'                                                  -- 25
                ||  GC_SEPARATER ||  'CIM'                                                          -- 26
                ||  GC_SEPARATER ||  'System Code'                                                  -- 27
                ||  GC_SEPARATER ||  'Descriotion'                                                  -- 28
                ||  GC_SEPARATER ||  'Item Description'                                             -- 29
                ||  GC_SEPARATER ||  'FA Category'                                                  -- 30
                ||  GC_SEPARATER ||  'Brand'                                                        -- 31
                ||  GC_SEPARATER ||  'Model'                                                        -- 32
                ||  GC_SEPARATER ||  'PO Approve Date'                                              -- 33
                ||  GC_SEPARATER ||  'Supplier Code'                                                -- 34
                ||  GC_SEPARATER ||  'Supplier Name'                                                -- 35
                ||  GC_SEPARATER ||  'Requester'                                                    -- 36
                ||  GC_SEPARATER ||  'Follow up with'                                               -- 37
                ||  GC_SEPARATER ||  'Status'                                                       -- 38
                ||  GC_SEPARATER ||  'Closure Status'                                               -- 39
                ||  GC_SEPARATER ||  'Project Name'                                                 -- 40
                ||  GC_SEPARATER ||  'Project Group'                                                -- 41
                ||  GC_SEPARATER ||  'Active(Y/N)'                                                  -- 42
                ||  GC_SEPARATER ||  'Include in Booking Report (Y/N) Don''t move from Y to N'      -- 43
                ||  GC_SEPARATER ||  'Division'                                                     -- 44
                ||  GC_SEPARATER ||  'Group No.'                                                    -- 45
                ||  GC_SEPARATER ||  'Group Name'                                                   -- 46
                ||  GC_SEPARATER ||  'PM'                                                           -- 47
                ||  GC_SEPARATER ||  'BOQ Require'                                                  -- 48
                
                
            );
            
    
    
    V_DEBUG     :=  40;        
    UTL_FILE.PUT_LINE (FS, VDR);
    

    --## Details Info. for Master Asset Category
    V_DEBUG     :=  50;
    FOR R IN CUR_DATA
    LOOP
        
        V_COUNT_REC :=  NVL(V_COUNT_REC,0)  +   1;   
        
        
        --## Initial Variable
        V_CF_AMOUNT             :=  0;
        CP_BOOKED_AMT           :=  0;
        CP_WIP_AMT              :=  0;
        CP_WIP_ACCRUED          :=  0;  
        CP_PREPAID_15005        :=  0;
        CP_PREPAID_ACCRUAL      :=  0;
        CP_COST_RETIRED         :=  0;      --## Added @26-Jun-2017
        CF_REGISTER_AMOUNT      :=  0;      --## Added @26-Jun-2017
        
        
        V_CIM               :=  NULL;
        V_FA_CAT            :=  NULL;
        V_BRAND             :=  NULL;
        V_MODEL_NUMBER      :=  NULL;
        V_COST_RETIRED      :=  NULL;
        
        
        V_FOLLOW_UP         :=  NULL;
        CP_BOQ_REQUIRE      :=  NULL;
        CP_INCLUDE_BOOK     :=  NULL;
        
        
         --## FA Information
        V_DEBUG     :=  70;
        CF_FA_DETAIL
        (
            P_I_PO_HEADER_ID    =>  R.PO_HEADER_ID
            , P_I_LINE_NUM      =>  R.LINE_NUM
            , P_O_CIM           =>  V_CIM
            , P_O_FA_CAT        =>  V_FA_CAT
            , P_O_BRAND         =>  V_BRAND
            , P_O_MODEL_NUMBER  =>  V_MODEL_NUMBER
            , P_O_COST_RETIRED  =>  V_COST_RETIRED
        );
        
        
        --## Get AMOUNT
        V_DEBUG     :=  60;
        V_CF_AMOUNT :=  CF_AMOUNT_V2
                        (
                            P_I_TYPE_OF_WORK            =>  R.TYPE_OF_WORK
                            , P_I_GRN_AMOUNT            =>  R.GRN_AMOUNT
                            , P_I_REGISTER_AMOUNT       =>  NVL(R.REGISTER_AMOUNT,0)
                            , P_I_PROGRESS_AMT          =>  R.PROGRESS_AMT
                            , P_I_PO_NUMBER             =>  R.PO_NUM
                            , P_I_PO_LINE_NUMBER        =>  R.LINE_NUM
                            , P_O_CP_BOOKED_AMT         =>  CP_BOOKED_AMT
                            , P_O_CP_WIP_AMT            =>  CP_WIP_AMT
                            , P_O_CP_WIP_ACCRUED        =>  CP_WIP_ACCRUED
                            , P_O_CP_PREPAID_15005      =>  CP_PREPAID_15005
                            , P_O_CP_PREPAID_ACCRUAL    =>  CP_PREPAID_15005
                            , P_O_COST_RETIRED          =>  CP_COST_RETIRED
                            , P_O_CF_REGISTER_AMOUNT    =>  CF_REGISTER_AMOUNT
                        );
                        
        /*
        V_CF_AMOUNT :=  CF_AMOUNT
                        (
                            P_I_TYPE_OF_WORK            =>  R.TYPE_OF_WORK
                            , P_I_GRN_AMOUNT            =>  R.GRN_AMOUNT
                            , P_I_REGISTER_AMOUNT       =>  NVL(R.REGISTER_AMOUNT,0) + NVL(V_COST_RETIRED,0)
                            , P_I_PROGRESS_AMT          =>  R.PROGRESS_AMT
                            , P_O_CP_BOOKED_AMT         =>  CP_BOOKED_AMT
                            , P_O_CP_WIP_AMT            =>  CP_WIP_AMT
                            , P_O_CP_WIP_ACCRUED        =>  CP_WIP_ACCRUED
                            , P_O_CP_PREPAID_15005      =>  CP_PREPAID_15005
                            , P_O_CP_PREPAID_ACCRUAL    =>  CP_PREPAID_ACCRUAL
                        );
         */
    
    
       
        
        
        --## PO Information
        V_DEBUG     :=  80;
        CF_PO_ADD_INFO
        (
            P_I_PO_NUM              =>  R.PO_NUM
            , P_I_LINE_NUM          =>  R.LINE_NUM
            , P_O_FOLLOW_UP         =>  V_FOLLOW_UP
            , P_O_CP_BOQ_REQUIRE    =>  CP_BOQ_REQUIRE
            , P_O_CP_INCLUDE_BOOK   =>  CP_INCLUDE_BOOK
        );
    
    
        V_DEBUG     :=  90;
        vdr :=  (   
                    R.OU                                                                                  -- 1
                    ||  GC_SEPARATER ||  R.PROJECT_CODE                                                   -- 2
                    ||  GC_SEPARATER ||  R.PO_NUM                                                         -- 3
                    ||  GC_SEPARATER ||  R.LINE_NUM                                                       -- 4
                    ||  GC_SEPARATER ||  R.TYPE_OF_WORK                                                   -- 5
                    ||  GC_SEPARATER ||  R.MATCH_TYPE                                                     -- 6
                    ||  GC_SEPARATER ||  R.CURR                                                           -- 7
                    ||  GC_SEPARATER ||  R.EX_RATE                                                        -- 8
                    ||  GC_SEPARATER ||  R.AMT_CURR                                                       -- 9
                    ||  GC_SEPARATER ||  R.PO_QTY                                                         -- 10
                    ||  GC_SEPARATER ||  R.UOM                                                            -- 11
                    ||  GC_SEPARATER ||  R.AMT_THB                                                        -- 12
                    ||  GC_SEPARATER ||  R.GRN_AMOUNT                                                     -- 13
                    ||  GC_SEPARATER ||  CF_GRN_QTY(R.MATCH_TYPE,R.GRN_QTY,R.GRN_AMOUNT,R.UNIT_PRICE)     -- 14
                    ||  GC_SEPARATER ||  CF_REGISTER_AMOUNT     --R.REGISTER_AMOUNT                       -- 15
                    ||  GC_SEPARATER ||  R.REGISTER_QTY                                                   -- 16
                    ||  GC_SEPARATER ||  R.PROGRESS_AMT                                                   -- 17
                    ||  GC_SEPARATER ||  R.PROGRESS_QTY                                                   -- 18
                    ||  GC_SEPARATER ||  CP_BOOKED_AMT                                                  -- 19
                    ||  GC_SEPARATER ||  CP_WIP_AMT                                                     -- 20
                    ||  GC_SEPARATER ||  CP_WIP_ACCRUED                                                 -- 21
                    ||  GC_SEPARATER ||  CP_PREPAID_15005                                               -- 22
                    ||  GC_SEPARATER ||  CP_PREPAID_ACCRUAL                                             -- 23
                    ||  GC_SEPARATER ||  V_COST_RETIRED                                                   -- 24
                    ||  GC_SEPARATER ||  R.ACCOUNT_WIP                                                    -- 25
                    ||  GC_SEPARATER ||  V_CIM                                                            -- 26
                    ||  GC_SEPARATER ||  R.SYSTEM_CODE                                                    -- 27
                    ||  GC_SEPARATER ||  R.DESCRIPTION                                                    -- 28
                    ||  GC_SEPARATER ||  R.ITEM_DESC                                                      -- 29
                    ||  GC_SEPARATER ||  V_FA_CAT                                                         -- 30
                    ||  GC_SEPARATER ||  V_BRAND                                                          -- 31
                    ||  GC_SEPARATER ||  V_MODEL_NUMBER                                                   -- 32
                    ||  GC_SEPARATER ||  R.PO_APP_DATE                                                    -- 33
                    ||  GC_SEPARATER ||  R.SUP_CODE                                                       -- 34
                    ||  GC_SEPARATER ||  R.SUP_NAME                                                       -- 35
                    ||  GC_SEPARATER ||  R.REQUESTER                                                      -- 36
                    ||  GC_SEPARATER ||  V_FOLLOW_UP                                                    -- 37
                    ||  GC_SEPARATER ||  R.PO_STATUS                                                      -- 38
                    ||  GC_SEPARATER ||  R.CLOSURE_STATUS                                                 -- 39
                    ||  GC_SEPARATER ||  R.PROJECT_NAME                                                   -- 40
                    ||  GC_SEPARATER ||  R.PROJECT_GROUP                                                  -- 41
                    ||  GC_SEPARATER ||  R.ACTIVE                                                         -- 42
                    ||  GC_SEPARATER ||  CP_INCLUDE_BOOK                                                -- 43
                    ||  GC_SEPARATER ||  R.DIVISION                                                       -- 44
                    ||  GC_SEPARATER ||  R.GROUP_NO                                                       -- 45
                    ||  GC_SEPARATER ||  CF_GROUP_NAME(R.GROUP_NO)                                        -- 46
                    ||  GC_SEPARATER ||  R.PM                                                             -- 47
                    ||  GC_SEPARATER ||  CP_BOQ_REQUIRE                                                 -- 48
                );
                
                
        
        
        --vdr := utl_file.fopen('<directory>', 'dummy.dat', 'w', 32767);
        --utl_file.put_line(file, convert(l_str, 'WE8MSWIN1252', 'AL16UTF16'));
        --utl_file.fclose(file);
        
        
        
        --## Put data in record
        V_DEBUG     :=  100;
        UTL_FILE.PUT_LINE (FS, VDR);
        
        /*
        BEGIN
            UTL_FILE.PUT_LINE(FS, CONVERT(VDR, 'UTF8', 'TH8TISASCII'));         --## From  'TH8TISASCII' to 'UTF8'
            
        EXCEPTION
            WHEN OTHERS THEN
                write_log('ERROR!! Convert file to UTF8 PO: ' || R.PO_NUM 
                            || ' LINE: ' || R.LINE_NUM || '  => ' || SQLERRM);
                
                --## Continuous this LINE
                UTL_FILE.PUT_LINE (FS, VDR);
        END;
        */
        
        
    END LOOP;

    
    --## Close FILE
    V_DEBUG     :=  110;
    UTL_FILE.fclose (fs);
  
  
    --## Out Parameter
    V_DEBUG     :=  120;
    P_O_TOTAL_REC   :=  V_COUNT_REC;

    
    RETURN    V_COMPLETED_FLAG;
  
  
EXCEPTION
    WHEN OTHERS THEN
        UTL_FILE.put(fs,'No Master Asset Category record found.');
        UTL_FILE.fclose (fs);
        
        write_log('ERROR!! ' || V_DEBUG || ': WRITE_FILE => ' || SQLERRM);
        INSERT_LOG_ARRAY('Error!! WRITE_FILE => ' || SQLERRM);
        
        RETURN 'E';
END WRITE_FILE;






-- *****************************************************************************
-- Type:    PROCEDURE
-- Name:    MAIL_NOTIFICATION
-- Desc:    Email Notification
-- 
-- Revision History
-- Date             Author          Reason for Change
-- ----------------------------------------------------------------
-- 29 MAY 2017      Thanawat T.     Created.
-- 24 MAR 2017      Thanawat T.     Issus: T-9999
--                                  Cause: Program get data too many value of row at line keyword ## ISSUE:T-9999
--                                  Fixed: Add EXCEPTION TOO_MANY_ROWS
-- *****************************************************************************
PROCEDURE  MAIL_NOTIFICATION(P_I_PARAM_RT   IN  PARAMETERS_RT
                            ,P_I_EXP_STATUS IN  VARCHAR2
                            )
IS

    V_DEBUG             NUMBER              :=  0;

    

    V_MAIL_CONN         UTL_SMTP.CONNECTION;
    CRLF                VARCHAR2(2)         := CHR(13)||CHR(10);
    V_HEAD              VARCHAR2(2000)      := NULL ;
    V_TEXT              VARCHAR2(20000)     := NULL ;
    V_MSG               VARCHAR2(2000)      := NULL ;
    S_TOTAL             NUMBER              := 0 ;
    S_COMPLETE_STATUS   NUMBER              := 0 ;
    S_ERROR             NUMBER              := 0 ;
    
    V_EXPORT_STATUS         VARCHAR2(100)   :=  NULL;
    V_CON_START_DATE        VARCHAR2(100)   :=  NULL;
    
    V_SERVER_NAME           FND_CONCURRENT_REQUESTS.OUTFILE_NODE_NAME%TYPE                  :=  NULL;
    V_CONCURRENT_NAME       FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME%TYPE    :=  NULL;
        
    

BEGIN

    IF NVL(P_I_EXP_STATUS,'X') = 'C' THEN
        V_EXPORT_STATUS :=  GC_COMPLETE;
    
    ELSE
        V_EXPORT_STATUS :=  GC_ERROR;
    
    END IF;
    
    
    --## Find Concurrent Name
    BEGIN
        SELECT  FCR.OUTFILE_NODE_NAME
                ,FCP.USER_CONCURRENT_PROGRAM_NAME
                ,TO_CHAR(FCR.ACTUAL_START_DATE,'DD-Mon-YYYY HH24:MI:SS')        START_DATE
        INTO    V_SERVER_NAME
                ,V_CONCURRENT_NAME
                ,V_CON_START_DATE
        FROM    FND_CONCURRENT_REQUESTS         FCR
                ,FND_CONCURRENT_PROGRAMS_TL     FCP
        WHERE   1   =   1
        AND     FCR.CONCURRENT_PROGRAM_ID   =   FCP.CONCURRENT_PROGRAM_ID
        AND     FCR.request_id              =   P_I_PARAM_RT.REQUEST_ID;
    
    EXCEPTION
        WHEN no_data_found THEN
            V_SERVER_NAME       :=  NULL;
            V_CONCURRENT_NAME   :=  NULL;
            V_CON_START_DATE    :=  NULL;
        
        WHEN OTHERS THEN
            V_SERVER_NAME       :=  NULL;
            V_CONCURRENT_NAME   :=  NULL;        
            V_CON_START_DATE    :=  NULL;
    END;
        

  
    V_DEBUG     :=  10;
    V_MAIL_CONN := UTL_SMTP.OPEN_CONNECTION(GC_SMTP_HOST, GC_SMTP_PORT);

    V_DEBUG :=  20;
    UTL_SMTP.HELO(V_MAIL_CONN, GC_SMTP_HOST);

    V_DEBUG :=  30;
    UTL_SMTP.MAIL(V_MAIL_CONN, GC_MAIL_FROM);

    V_DEBUG :=  40;
    UTL_SMTP.RCPT(V_MAIL_CONN, GC_MAIL_TO);
    
    V_DEBUG :=  50;
    UTL_SMTP.OPEN_DATA(V_MAIL_CONN);
   
    V_DEBUG :=  60;
    UTL_SMTP.WRITE_RAW_DATA(V_MAIL_CONN, UTL_RAW.CAST_TO_RAW(

        --'Date: '   || to_char(sysdate, 'Dy, DD Mon YYYY hh24:mi:ss') || crlf ||
        'Date: '   || TO_CHAR(FROM_TZ(CAST(SYSDATE AS TIMESTAMP), 'Asia/Bangkok'),'Dy, DD Mon YYYY hh24:mi:ss TZHTZM','NLS_DATE_LANGUAGE=AMERICAN') || crlf ||
        'From: '   || GC_MAIL_FROM || crlf ||
        --'Subject: '|| GC_MAIL_SUBJECT || crlf ||
        'Subject: '|| GC_MAIL_SUBJECT || ' [Request Id: '|| P_I_PARAM_RT.REQUEST_ID ||'] ['|| V_EXPORT_STATUS ||']' || crlf ||
        'To: '     || GC_MAIL_TO || crlf ||
        
        
        ----------------------------------------------------------------------------
        -- Message body
        ----------------------------------------------------------------------------
        crlf ||
        'Dear ERP Operation'||crlf ||
        crlf ||
        'Program: ' || V_CONCURRENT_NAME ||crlf ||
        crlf ||
        'Parameters: ' ||crlf ||
        '  Org ID = ' || P_I_PARAM_RT.ORG_ID ||crlf ||
        '  PO Date From = ' || TO_CHAR(TO_DATE(P_I_PARAM_RT.PO_DATE_FROM,'RRRR/MM/DD HH24:MI:SS'),G_FORMAT_DATE)  ||crlf ||
        '  PO Date To = ' || TO_CHAR(TO_DATE(P_I_PARAM_RT.PO_DATE_TO,'RRRR/MM/DD HH24:MI:SS'),G_FORMAT_DATE)  ||crlf ||
        '  Directory Name = ' || P_I_PARAM_RT.DIRECTORY_NAME ||crlf ||
        crlf ||
        'Concurrent Start: ' || V_CON_START_DATE ||crlf ||
        '------------------------------------------------------------' ||crlf ||        
        'Filename: ' ||P_I_PARAM_RT.FILENAME||crlf ||
        'Total Export: ' ||P_I_PARAM_RT.TOTAL_REC || ' records' || crlf ||
        'Server Path: ' || P_I_PARAM_RT.DIRECTORY_PATH ||crlf ||
        
        --'Status: ' || V_EXPORT_STATUS ||crlf ||
        --'Request Id: ' || P_I_PARAM_RT.REQUEST_ID ||crlf ||
        
        '------------------------------------------------------------' ||crlf ||
        'Concurrent End :' || TO_CHAR(SYSDATE,'DD-Mon-YYYY HH24:MI:SS') ||crlf ||
        
        
        /*
        ----------------------------------------------------------------------------
        -- Message body
        ----------------------------------------------------------------------------
        crlf ||
        'Dear ERP Operation'||crlf ||
        crlf ||
        'Status: ' || V_EXPORT_STATUS ||crlf ||
        'Request Id: ' || P_I_PARAM_RT.REQUEST_ID ||crlf ||
        'Date Start: ' || V_CON_START_DATE ||crlf ||
        'Date Completed: ' || TO_CHAR(SYSDATE,'DD-Mon-YYYY HH24:MI:SS') ||crlf ||
        'Concurrent Name: ' || V_CONCURRENT_NAME ||crlf ||
        '------------------------------------------------------------' ||crlf ||
        'Total Export: ' ||P_I_PARAM_RT.TOTAL_REC || ' records' || crlf ||
        'Filename: ' ||P_I_PARAM_RT.FILENAME||crlf ||
        'Server Path: ' || P_I_PARAM_RT.DIRECTORY_PATH ||crlf ||
        */
        
        ----------------------------------------------------------------------------
        -- Footer
        ----------------------------------------------------------------------------
        crlf ||
        crlf ||
        'Regards,'||crlf ||
        'DTN Migration Network Asset to ERP Project'||crlf ||
        crlf
    
    ));


    utl_smtp.close_data(v_Mail_Conn);

    utl_smtp.Quit(v_mail_conn);
  

EXCEPTION
    WHEN utl_smtp.Transient_Error OR utl_smtp.Permanent_Error THEN
        write_log('Unable to send mail');
        INSERT_LOG_ARRAY(GC_ERR_CONN_FAILED || ' : Unable to send mail');
        raise_application_error(-20000, 'Unable to send mail', TRUE);


    WHEN OTHERS THEN
        write_log(V_DEBUG || 'ERROR!! PROCEDURE: MAIL_NOTIFICATION => ' || SQLERRM);
        INSERT_LOG_ARRAY('ERROR!! MAIL_NOTIFICATION => ' || SQLERRM);
        RAISE;

END MAIL_NOTIFICATION;

-- *****************************************************************************
-- Type:    PROCEDURE
-- Name:    PROGRAM_LOG
-- Desc:    Generate Log for both Complete and Error
-- 
-- Revision History
-- Date            Author           Reason for Change
-- ----------------------------------------------------------------
-- 29 MAY 2017     Thanawat T.      Created.
-- *****************************************************************************
PROCEDURE PROGRAM_LOG(P_I_PARAM_RT   IN  PARAMETERS_RT
                     ,P_I_EXP_STATUS IN  VARCHAR2   DEFAULT NULL
                     )
IS

    V_DEBUG                 NUMBER          :=  0;
    NO_ERROR                EXCEPTION;
    
    V_LOG_ERR               VARCHAR2 (4000);
    V_EXPORT_STATUS         VARCHAR2(100)   :=  NULL;


    CURSOR CONCURRENT_CUR
    IS
        SELECT  FCR.REQUEST_ID
                ,FCR.ACTUAL_START_DATE                                          START_DATE
                ,TO_CHAR(FCR.ACTUAL_COMPLETION_DATE,'DD-Mon-YYYY HH24:MI:SS')   COMPLETION_DATE
                ,FCR.OUTFILE_NODE_NAME
                ,FCR.OUTFILE_NAME
                ,FCR.LOGFILE_NODE_NAME                                          HOST
                ,FCR.LOGFILE_NAME
                ,FCR.PHASE_CODE
                ,FCR.STATUS_CODE
                
                ,DECODE(FCR.PHASE_CODE,'C','Completed',FCR.PHASE_CODE)                  PHASE_STATUS
                ,DECODE(FCR.STATUS_CODE,'C','Normal','E','Error',FCR.STATUS_CODE)       STATUS
                
                ,FCP.USER_CONCURRENT_PROGRAM_NAME                               JOB_NAME
                ,FCP.DESCRIPTION                                                JOB_DESCRIPTION
                
        FROM    FND_CONCURRENT_REQUESTS     FCR
                ,FND_CONCURRENT_PROGRAMS_TL FCP
        WHERE   1   =   1
        AND     FCR.CONCURRENT_PROGRAM_ID   =   FCP.CONCURRENT_PROGRAM_ID
        AND     FCR.request_id              =   P_I_PARAM_RT.REQUEST_ID;

BEGIN

    
    V_DEBUG :=  10;
    IF NVL(P_I_EXP_STATUS,'X') = 'C' THEN
        V_EXPORT_STATUS :=  GC_COMPLETE;
    
    ELSE
        V_EXPORT_STATUS :=  GC_ERROR;
    
    END IF;
    
    
    --## Insert Log Complete to ARRAY =>> Value in Column ERROR_MESSAGE is blank.
    --##    If no record in array then ARRAY ERROR
    IF NVL(G_LOG_TABLE_TY.COUNT,0)  = 0 THEN
        INSERT_LOG_ARRAY('');
    END IF;
    
    
    --## Concurrent Cursor => Always show 1 record by REQUEST_ID
    FOR REC IN CONCURRENT_CUR
    LOOP
        
        --## Heading
        V_DEBUG :=  20;
        write_log(  'BEGINDATETIME'
                    ||  GC_SEPARATER ||  'ENDDATETIME'
                    ||  GC_SEPARATER ||  'HOST'
                    ||  GC_SEPARATER ||  'JOB_NAME'
                    ||  GC_SEPARATER ||  'DEST_FILE'
                    ||  GC_SEPARATER ||  'NUM_RECORDS'
                    ||  GC_SEPARATER ||  'STATUS'
                    ||  GC_SEPARATER ||  'ERROR_MESSAGE'
                );
        
        
        --## Check Array has value before LOOP.
        --##    If not have a value then ERROR
        IF NVL(G_LOG_TABLE_TY.COUNT,0)    >   0   THEN
        
            --## Array Array Log Message Table => G_ERROR_TABLE_TY
            FOR I IN G_LOG_TABLE_TY.FIRST .. G_LOG_TABLE_TY.LAST 
            LOOP
        
        
                --## Error Details
                V_DEBUG :=  30;
                write_log(  TO_CHAR(REC.START_DATE,'DD-Mon-YYYY HH24:MI:SS')
                            ||  GC_SEPARATER ||  TO_CHAR(SYSDATE,'DD-Mon-YYYY HH24:MI:SS')
                            ||  GC_SEPARATER ||  REC.HOST
                            ||  GC_SEPARATER ||  REC.JOB_NAME
                            ||  GC_SEPARATER ||  P_I_PARAM_RT.FILENAME
                            ||  GC_SEPARATER ||  P_I_PARAM_RT.TOTAL_REC
                            ||  GC_SEPARATER ||  V_EXPORT_STATUS                --REC.STATUS
                            ||  GC_SEPARATER ||  G_LOG_TABLE_TY(i).ERROR_MESSAGE
                        );
            END LOOP;
            --## End Array Log Message
        END IF;
    
    
    END LOOP;
    --## End Concurrent Cursor

EXCEPTION
    WHEN NO_ERROR THEN
        NULL;
    
    WHEN OTHERS THEN
        write_log('ERROR!! PROGRAM_LOG => ' || SQLERRM);
        RAISE;
END PROGRAM_LOG;


-- *****************************************************************************
-- Type:    PROCEDURE
-- Name:    INSERT_LOG_ARRAY
-- Desc:    Insert log to array object
--          1. Declare Global Variable for Table ARRAY
--          2. When program error or validation error, insert date to array
--
-- Revision History
-- Date            Author           Reason for Change
-- ----------------------------------------------------------------
-- 29 MAY 2017     Thanawat T.      Created.
-- *****************************************************************************
PROCEDURE INSERT_LOG_ARRAY(P_I_MSG  IN  VARCHAR2)
IS

    
    V_INDEX_NO              NUMBER  :=  0;

BEGIN

    --## Check current INDEX
    V_INDEX_NO  :=  NVL(G_LOG_TABLE_TY.COUNT,0)   +   1;
    
    
    
    --## Insert log message to ARRAY
    G_LOG_TABLE_TY(V_INDEX_NO).ERROR_MESSAGE      :=  SUBSTR(P_I_MSG,1,4000);
    
    
    
EXCEPTION
    WHEN OTHERS THEN
        write_log('ERROR!! PROCEDURE: INSERT_LOG_ARRAY => ' || SQLERRM);
        RAISE;

END INSERT_LOG_ARRAY;



-- *****************************************************************************
-- Type:    PROCEDURE
-- Name:    WRITE_LOG
-- Desc:    Write log by using FND_FILE.LOG
--
-- Revision History
-- Date            Author           Reason for Change
-- ----------------------------------------------------------------
-- 29 MAY 2017     Thanawat T.      Created.
-- *****************************************************************************
PROCEDURE WRITE_LOG(PARAM_MSG VARCHAR2) IS
BEGIN
    FND_FILE.PUT_LINE(FND_FILE.LOG, PARAM_MSG);
END WRITE_LOG;



-- *****************************************************************************
-- Type:    PROCEDURE
-- Name:    WRITE_OUTPUT
-- Desc:    Write log by using FND_FILE.OUTPUT
--
-- Revision History
-- Date            Author           Reason for Change
-- ----------------------------------------------------------------
-- 29 MAY 2017     Thanawat T.      Created.
-- *****************************************************************************
PROCEDURE WRITE_OUTPUT(PARAM_MSG VARCHAR2) IS
BEGIN
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT, PARAM_MSG);
END WRITE_OUTPUT;


-- *****************************************************************************
-- Type:    FUNCTION
-- Name:    CF_GRN_QTY
-- Desc:    Get GRN QTY
--
-- Revision History
-- Date            Author           Reason for Change
-- ----------------------------------------------------------------
-- 29 MAY 2017     Thanawat T.      Created.
-- *****************************************************************************
FUNCTION CF_GRN_QTY(P_I_MATCH_TYPE  IN VARCHAR2
                    ,P_I_GRN_QTY    IN  NUMBER
                    ,P_I_GRN_AMOUNT IN  NUMBER
                    ,P_I_UNIT_PRICE IN  NUMBER
                    ) 
RETURN NUMBER 
IS

    V_AMOUNT                NUMBER  :=  0;

BEGIN

  
    IF UPPER(P_I_MATCH_TYPE) = UPPER('2-Way') THEN
        IF P_I_GRN_QTY = 0 THEN
            V_AMOUNT    :=  (P_I_GRN_AMOUNT/P_I_UNIT_PRICE);
        
        ELSE
            V_AMOUNT    :=  nvl(P_I_GRN_QTY,0);
        END IF;
        
    ELSE
        V_AMOUNT    :=  P_I_GRN_QTY;
    END IF;

    RETURN  V_AMOUNT;

EXCEPTION
    WHEN OTHERS THEN
        write_log('ERROR!! PROCEDURE: CF_GRN_QTY => ' || SQLERRM);
        RETURN  NULL;
END;



-- *****************************************************************************
-- Type:    FUNCTION
-- Name:    CF_PO_ADD_INFO
-- Desc:    Get PO Additional Information
--
-- Revision History
-- Date            Author           Reason for Change
-- ----------------------------------------------------------------
-- 29 MAY 2017     Thanawat T.      Created.
-- *****************************************************************************
PROCEDURE CF_PO_ADD_INFO(P_I_PO_NUM             IN  VARCHAR2
                        ,P_I_LINE_NUM           IN  NUMBER
                        ,P_O_FOLLOW_UP          OUT VARCHAR2
                        ,P_O_CP_BOQ_REQUIRE     OUT VARCHAR2
                        ,P_O_CP_INCLUDE_BOOK    OUT VARCHAR2
                        )
IS

BEGIN
   
    BEGIN
        SELECT  NVL(BOQ_REQUIRE,'N')
                ,NVL(DISPLAY_PROJECT,'Y')
                ,FOLLOW_UP_WITH
        INTO    P_O_CP_BOQ_REQUIRE
                ,P_O_CP_INCLUDE_BOOK
                ,P_O_FOLLOW_UP
        FROM    TAC_PO_ADDITION_INFO 
        WHERE   PO_NUMBER       =   P_I_PO_NUM
        AND     PO_LINE_NUMBER  =   P_I_LINE_NUM
        AND     TO_DATE(PERIOD_NAME,'MON-YY') = (   SELECT  MAX(to_date(PERIOD_NAME,'MON-YY'))
                                                    FROM    TAC_PO_ADDITION_INFO
                                                    WHERE   PO_NUMBER       = P_I_PO_NUM
                                                    AND     PO_LINE_NUMBER  = P_I_LINE_NUM);

        /* --## Code change on 20-Jun-2017
        SELECT  BOQ_REQUIRE
                ,DISPLAY_PROJECT
                ,FOLLOW_UP_WITH
        INTO    P_O_CP_BOQ_REQUIRE
                ,P_O_CP_INCLUDE_BOOK
                ,P_O_FOLLOW_UP
        FROM    TAC_PO_ADDITION_INFO 
        WHERE   PO_NUMBER       =   P_I_PO_NUM
        AND     PO_LINE_NUMBER  =   P_I_LINE_NUM
        AND TO_DATE(PERIOD_NAME,'MON-YY') = (SELECT MAX(to_date(PERIOD_NAME,'MON-YY'))
                                                FROM TAC_PO_ADDITION_INFO
                                                WHERE PO_NUMBER = P_I_PO_NUM
                                                AND PO_LINE_NUMBER = P_I_LINE_NUM);
        */
    EXCEPTION 
        WHEN OTHERS THEN
            P_O_CP_BOQ_REQUIRE  :=  NULL;
            P_O_CP_INCLUDE_BOOK :=  NULL;
    END;
     
     
  
EXCEPTION 
    WHEN OTHERS THEN    
        write_log('ERROR!! PROCEDURE: CF_PO_ADD_INFO => ' || SQLERRM);
    
END;



-- *****************************************************************************
-- Type:    FUNCTION
-- Name:    CF_AMOUNT
-- Desc:    Get Amount
--
-- Revision History
-- Date            Author           Reason for Change
-- ----------------------------------------------------------------
-- 29 MAY 2017     Thanawat T.      Created.
-- *****************************************************************************
FUNCTION ZZCF_AMOUNT(P_I_TYPE_OF_WORK         IN  VARCHAR2
                    ,P_I_GRN_AMOUNT         IN  NUMBER
                    ,P_I_REGISTER_AMOUNT    IN  NUMBER
                    ,P_I_PROGRESS_AMT       IN  NUMBER
                    ,P_O_CP_BOOKED_AMT      OUT NUMBER
                    ,P_O_CP_WIP_AMT         OUT NUMBER
                    ,P_O_CP_WIP_ACCRUED     OUT NUMBER
                    ,P_O_CP_PREPAID_15005   OUT NUMBER
                    ,P_O_CP_PREPAID_ACCRUAL OUT NUMBER
                    )
RETURN NUMBER 
IS

    V_BOOKED_AMT        NUMBER;
    V_WIP_AMT           NUMBER;
    V_WIP_ACCRUED       NUMBER;
    V_PREPAID_15005     NUMBER;
    V_PREPAID_ACCRUAL   NUMBER;

begin
    
    v_booked_amt        := 0;
    v_wip_amt           := 0;
    v_wip_accrued       := 0;
    v_prepaid_15005     := 0;
    v_PREPAID_ACCRUAL   := 0;
    
    IF upper(P_I_TYPE_OF_WORK) in ('E','U') THEN       
        v_booked_amt := nvl(GREATEST(P_I_GRN_AMOUNT,P_I_REGISTER_AMOUNT),0);   
            
    ELSIF upper(P_I_TYPE_OF_WORK) = 'S' THEN       
        v_booked_amt := nvl(GREATEST(P_I_PROGRESS_AMT,P_I_GRN_AMOUNT,P_I_REGISTER_AMOUNT),0); 
        
    ELSIF upper(P_I_TYPE_OF_WORK) = 'T' THEN
        v_booked_amt := nvl(P_I_REGISTER_AMOUNT,0);
        
    ELSIF upper(P_I_TYPE_OF_WORK) = 'O' THEN
        v_booked_amt := 0; 
        
    ELSE
        v_booked_amt := 0;           
        
    END IF;
     
     
     IF upper(P_I_TYPE_OF_WORK) IN ('E','S','U') THEN
        v_wip_amt :=  NVL(v_booked_amt,0) - NVL(P_I_REGISTER_AMOUNT,0);
    /* ELSIF upper(P_I_TYPE_OF_WORK) IN ('T') THEN
        v_wip_amt :=  NVL(P_I_GRN_AMOUNT,0) - NVL(P_I_REGISTER_AMOUNT,0);*/
     ELSE
        v_wip_amt := 0;
     END IF;
     
    IF upper(P_I_TYPE_OF_WORK) IN ('E','U') THEN 
      v_wip_accrued :=  NVL(P_I_REGISTER_AMOUNT,0) - NVL(P_I_GRN_AMOUNT,0); 
    ELSIF  upper(P_I_TYPE_OF_WORK) IN ('S') THEN
      v_wip_accrued :=  NVL(v_booked_amt,0) - NVL(P_I_GRN_AMOUNT,0);
    END IF;
     
     
     IF upper(P_I_TYPE_OF_WORK) = 'T' THEN
        v_prepaid_15005 := NVL(P_I_GRN_AMOUNT,0) - NVL(P_I_REGISTER_AMOUNT,0);
     ELSE
        v_prepaid_15005 := 0;
     END IF;
     
     
     
     
     /*IF upper(P_I_TYPE_OF_WORK) IN ('S') THEN
        v_PREPAID_ACCRUAL := NVL(v_booked_amt,0) - NVL(P_I_REGISTER_AMOUNT,0);
     ELSIF upper(P_I_TYPE_OF_WORK) = 'E' THEN
        v_PREPAID_ACCRUAL := NVL(P_I_GRN_AMOUNT,0) - NVL(P_I_REGISTER_AMOUNT,0);
     ELSE
        v_PREPAID_ACCRUAL := 0;
     END IF;*/
     
     IF upper(P_I_TYPE_OF_WORK) IN ('T') THEN
        v_PREPAID_ACCRUAL := NVL(P_I_REGISTER_AMOUNT,0) - NVL(P_I_GRN_AMOUNT,0);
     ELSE
        v_PREPAID_ACCRUAL := 0;
     END IF;
     
 
    if v_booked_amt > 0 then
        P_O_CP_BOOKED_AMT := v_booked_amt;
    else 
        P_O_CP_BOOKED_AMT := 0;
    END if;
        
    if v_wip_amt > 0 then  
        P_O_CP_WIP_AMT := v_wip_amt;
    else 
        P_O_CP_WIP_AMT := 0;
    end if;
           
    if v_wip_accrued > 0 then
        P_O_CP_WIP_ACCRUED := v_wip_accrued;
    else 
        P_O_CP_WIP_ACCRUED := 0;
    END if;
         
    if v_prepaid_15005 > 0 then
        P_O_CP_PREPAID_15005 := v_prepaid_15005;
    else 
        P_O_CP_PREPAID_15005 := 0;
    end if;

    if v_PREPAID_ACCRUAL > 0 then
        P_O_CP_PREPAID_ACCRUAL := v_PREPAID_ACCRUAL;
    else 
        P_O_CP_PREPAID_ACCRUAL := 0;
    END if;
 
 
    RETURN 0; 
    
EXCEPTION 
    WHEN OTHERS THEN    
        WRITE_LOG('ERROR!! PROCEDURE: CF_AMOUNT => ' || SQLERRM);
        RETURN 0;
END;


function CF_AMOUNT_V2(P_I_TYPE_OF_WORK         IN  VARCHAR2
                    ,P_I_GRN_AMOUNT         IN  NUMBER
                    ,P_I_REGISTER_AMOUNT    IN  NUMBER
                    ,P_I_PROGRESS_AMT       IN  NUMBER
                    ,P_I_PO_NUMBER          IN  VARCHAR2    --## Added  26-Jun-2017
                    ,P_I_PO_LINE_NUMBER     IN  NUMBER      --## Added  26-Jun-2017
                    ,P_O_CP_BOOKED_AMT      OUT NUMBER
                    ,P_O_CP_WIP_AMT         OUT NUMBER
                    ,P_O_CP_WIP_ACCRUED     OUT NUMBER
                    ,P_O_CP_PREPAID_15005   OUT NUMBER
                    ,P_O_CP_PREPAID_ACCRUAL OUT NUMBER
                    ,P_O_COST_RETIRED       OUT NUMBER      --## Added  26-Jun-2017
                    ,P_O_CF_REGISTER_AMOUNT OUT NUMBER      --## Added  26-Jun-2017
                    )
RETURN NUMBER  
is

    v_booked_amt number;
    v_wip_amt number;
    v_wip_accrued number;
    v_prepaid_15005 number;
    v_PREPAID_ACCRUAL number;

begin
    
    v_booked_amt := 0;
    v_wip_amt := 0;
    v_wip_accrued := 0;
    v_prepaid_15005 := 0;
    v_PREPAID_ACCRUAL := 0;
    
    P_O_COST_RETIRED    :=  0;
    P_O_CF_REGISTER_AMOUNT  :=  0;
    --:COST_RETIRED := 0;
    --P_O_CF_REGISTER_AMOUNT := 0;


       BEGIN
            SELECT  SUM(COST_RETIRED) C
            --INTO :COST_RETIRED 
            INTO    P_O_COST_RETIRED
            FROM    FA_RETIREMENTS           FAR,
                    FA_ASSET_INVOICES       FAI,
                    FA_ADDITIONS_B          FAB
            WHERE  FAR.ASSET_ID     =  FAI.ASSET_ID
            AND     FAB.ASSET_ID    = FAI.ASSET_ID
            AND     FAI.PO_NUMBER   = P_I_PO_NUMBER     --:PO_NUM
            and     FAB.ATTRIBUTE5  = TO_CHAR(P_I_PO_LINE_NUMBER)
            --AND FAI.ATTRIBUTE1 = TO_CHAR(:LINE_NUM)
            AND     FAR.STATUS = 'PROCESSED'
             ;
         EXCEPTION WHEN OTHERS THEN
             --:COST_RETIRED := 0;
             P_O_COST_RETIRED   :=  0;
         END;
         
         
    --P_O_CF_REGISTER_AMOUNT := nvl(:REGISTER_AMOUNT,0) + NVL(:COST_RETIRED,0);
    P_O_CF_REGISTER_AMOUNT  :=  nvl(P_I_REGISTER_AMOUNT,0) + NVL(P_O_COST_RETIRED,0);
         
    
       IF upper(P_I_TYPE_OF_WORK) in ('E','U') THEN       
        v_booked_amt := nvl(GREATEST(P_I_GRN_AMOUNT,P_O_CF_REGISTER_AMOUNT),0);       
     ELSIF upper(P_I_TYPE_OF_WORK) = 'S' THEN       
        v_booked_amt := nvl(GREATEST(P_I_PROGRESS_AMT,P_I_GRN_AMOUNT,P_O_CF_REGISTER_AMOUNT),0); 
     ELSIF upper(P_I_TYPE_OF_WORK) = 'T' THEN
        v_booked_amt := NVL(P_O_CF_REGISTER_AMOUNT,0);  -- PROGRESS_AMT
     ELSIF upper(P_I_TYPE_OF_WORK) = 'O' THEN
         v_booked_amt := 0; 
     ELSE
         v_booked_amt := 0;           
     END IF;
     
     
     IF upper(P_I_TYPE_OF_WORK) IN ('E','S','U') THEN
        v_wip_amt :=  NVL(v_booked_amt,0) - NVL(P_O_CF_REGISTER_AMOUNT,0);

     ELSE
        v_wip_amt := 0;
     END IF;
     
    IF upper(P_I_TYPE_OF_WORK) IN ('E','U') THEN 
      v_wip_accrued :=  NVL(P_O_CF_REGISTER_AMOUNT,0) - NVL(P_I_GRN_AMOUNT,0); 
    ELSIF  upper(P_I_TYPE_OF_WORK) IN ('S') THEN
      v_wip_accrued :=  NVL(v_booked_amt,0) - NVL(P_I_GRN_AMOUNT,0);
    END IF;
     
     
     IF upper(P_I_TYPE_OF_WORK) = 'T' THEN
        v_prepaid_15005 := NVL(P_I_GRN_AMOUNT,0) - NVL(P_O_CF_REGISTER_AMOUNT,0);
     ELSE
        v_prepaid_15005 := 0;
     END IF;
     
     
     
     

     
     IF upper(P_I_TYPE_OF_WORK) IN ('T') THEN
        v_PREPAID_ACCRUAL := NVL(P_O_CF_REGISTER_AMOUNT,0) - NVL(P_I_GRN_AMOUNT,0);
     ELSE
        v_PREPAID_ACCRUAL := 0;
     END IF;
     
 
     if v_booked_amt > 0 then
            P_O_CP_BOOKED_AMT := v_booked_amt;
         else P_O_CP_BOOKED_AMT := 0;
         end if;
        
         if v_wip_amt > 0 then  
            P_O_CP_WIP_AMT := v_wip_amt;
         else P_O_CP_WIP_AMT := 0;
         end if;
            
            if v_wip_accrued > 0 then
             P_O_CP_WIP_ACCRUED := v_wip_accrued;
            else P_O_CP_WIP_ACCRUED := 0;
            end if;
         
         if v_prepaid_15005 > 0 then
            P_O_CP_PREPAID_15005 := v_prepaid_15005;
         else P_O_CP_PREPAID_15005 := 0;
         end if;
         
         if v_PREPAID_ACCRUAL > 0 then
            P_O_CP_PREPAID_ACCRUAL := v_PREPAID_ACCRUAL;
         else P_O_CP_PREPAID_ACCRUAL := 0;
         end if;
 
 
 return 0; 
 
EXCEPTION 
    WHEN OTHERS THEN    
        WRITE_LOG('ERROR!! PROCEDURE: CF_AMOUNT => ' || SQLERRM);
        RETURN 0;
END;


-- *****************************************************************************
-- Type:    PROCEDURE
-- Name:    CF_FA_DETAIL
-- Desc:    Get FA Information Details
--
-- Revision History
-- Date            Author           Reason for Change
-- ----------------------------------------------------------------
-- 29 MAY 2017     Thanawat T.      Created.
-- *****************************************************************************
PROCEDURE CF_FA_DETAIL
(
    P_I_PO_HEADER_ID    IN  NUMBER
    ,P_I_LINE_NUM       IN  NUMBER
    -----------------------------------
    ,P_O_CIM            OUT  VARCHAR2
    ,P_O_FA_CAT         OUT  VARCHAR2
    ,P_O_BRAND          OUT  VARCHAR2
    ,P_O_MODEL_NUMBER   OUT  VARCHAR2
    ,P_O_COST_RETIRED   OUT  VARCHAR2
)
IS

    V_ASSET_ID          NUMBER;
    V_BOOK_TYPE_CODE    VARCHAR2(100);
    
    
    
BEGIN
    
    
    --V_CIM                :=  NULL;
   -- V_FA_CAT             :=  NULL;
    --P_O_BRAND              :=  NULL;
    --P_O_MODEL_NUMBER       :=  NULL;
    V_ASSET_ID          :=  NULL;
    V_BOOK_TYPE_CODE    :=  NULL;
            
    BEGIN   
        SELECT  FAB.PROPERTY_TYPE_CODE   CIM--AA 
                ,FAB.ATTRIBUTE_CATEGORY_CODE   FA_CAT    --AE 
                ,FAB.MANUFACTURER_NAME     BRAND        --AF
                ,FAB.MODEL_NUMBER                  --AG  
                ,FB.ASSET_ID 
                ,FB.BOOK_TYPE_CODE              
     
       into     P_O_CIM
                ,P_O_FA_CAT
                ,P_O_BRAND
                ,P_O_MODEL_NUMBER
                ,V_ASSET_ID
                ,V_BOOK_TYPE_CODE 

        FROM    FA_ADDITIONS_B          FAB
                ,FA_ASSET_INVOICES       FAI
                ,FA_BOOKS                FB
                ,PO_HEADERS_ALL          PH
        WHERE   FAB.ASSET_ID = FAI.ASSET_ID
        AND     FAB.ASSET_ID = FB.ASSET_ID
        AND     FB.DATE_INEFFECTIVE  IS NULL
        AND     PH.SEGMENT1         = FAI.PO_NUMBER
        AND     PH.PO_HEADER_ID     = P_I_PO_HEADER_ID
        AND     FAB.ATTRIBUTE5      = TO_CHAR(P_I_LINE_NUM)
        AND     ROWNUM = 1;

    EXCEPTION 
        WHEN OTHERS THEN
            V_ASSET_ID          := NULL;
            V_BOOK_TYPE_CODE    := NULL;
    END;
         
         
         BEGIN
            SELECT  SUM(COST_RETIRED) C
            INTO    P_O_COST_RETIRED
            FROM    FA_RETIREMENTS           FAR   
            WHERE   V_ASSET_ID =  FAR.ASSET_ID
            AND     FAR.STATUS = 'PROCESSED'
            AND     FAR.BOOK_TYPE_CODE  = V_BOOK_TYPE_CODE ;
         EXCEPTION 
            WHEN OTHERS THEN
             P_O_COST_RETIRED := 0;
         END;
    
 
 
EXCEPTION 
    WHEN OTHERS THEN
        NULL;
end;


-- *****************************************************************************
-- Type:    PROCEDURE
-- Name:    CF_GROUP_NAME
-- Desc:    Get GROUP_NAME
--
-- Revision History
-- Date            Author           Reason for Change
-- ----------------------------------------------------------------
-- 29 MAY 2017     Thanawat T.      Created.
-- *****************************************************************************
FUNCTION CF_GROUP_NAME(P_I_GROUP_NO IN  VARCHAR2)
RETURN VARCHAR2 
IS

    V_GROUP_NAME VARCHAR2(250);

BEGIN

    SELECT  FV.DESCRIPTION   --AU
    INTO    V_GROUP_NAME
    FROM    FND_FLEX_VALUE_SETS FS,
            FND_FLEX_VALUES_VL FV
    WHERE   FS.FLEX_VALUE_SET_ID            =   FV.FLEX_VALUE_SET_ID 
    AND     UPPER(FS.FLEX_VALUE_SET_NAME)   =   UPPER('DTN GROUP')
    AND     FV.FLEX_VALUE                   =   P_I_GROUP_NO;

    RETURN V_GROUP_NAME;
        
EXCEPTION 
    WHEN OTHERS THEN
        RETURN NULL;
END;


END ; 
/
