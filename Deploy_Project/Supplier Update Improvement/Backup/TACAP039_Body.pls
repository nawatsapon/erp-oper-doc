create or replace package body TACAP039 is
  procedure upload_form is
	x_user_id number;
	x_set_of_book_id number;
	x_org_id number;
	x_appl_id number;
	x_resp_id number;
  begin
	x_user_id := fnd_global.user_id;
	x_set_of_book_id := fnd_profile.value('GL_SET_OF_BKS_ID');
	x_org_id := fnd_global.org_id;
	x_appl_id := fnd_profile.value('RESP_APPL_ID');
	x_resp_id := fnd_profile.value('RESP_ID');
	htp.p('<title>Oracle E-Business Sute Upload files</title>');
	htp.p('<img src="/OA_MEDIA/FNDSSCORP.gif">');
	htp.p('<font color = #336699 ><b>E-Business Sute</b></font>');
	htp.p('<img src="/OA_MEDIA/pbs.gif" >');
	htp.p('<HR>');
	htp.header(3, 'TAC Upload Supplier Interface Text File');
	htp.p('<B><U>���͹�</U></B>');
	htp.p('<BR>');
	htp.p('<B><font color=red>��Ңͧ���� column � Input CSV File �е�ͧ���������ͧ���� �,� ��������ͧ���� �,� ���������¡ column ��ҹ�� �� ��Ңͧ column ����繵���Ţ ���Ѵ����ͧ���� �,� �͡��������������Ţ�ʴ��Դ�ѹ ������ҧ 123456.78 �繵� ���ͤ�ҷ���繤�͸Ժ�¡�����������ͧ���� �,� �蹡ѹ ���᷹��ҹ�鹴��¤����ҧ </font></B>');
	htp.p('<BR>');
	htp.formopen(curl => 'tacap039.upload', cmethod => 'POST', cenctype => 'multipart/form-data');
	htp.p('<INPUT TYPE="FILE" NAME="name">');
	htp.p('<INPUT TYPE="HIDDEN" NAME="name" VALUE=' || x_user_id || '>');
	htp.p('<INPUT TYPE="HIDDEN" NAME="name" VALUE=' || x_set_of_book_id || '>');
	htp.p('<INPUT TYPE="HIDDEN" NAME="name" VALUE=' || x_org_id || '>');
	htp.p('<INPUT TYPE="HIDDEN" NAME="name" VALUE=' || x_appl_id || '>');
	htp.p('<INPUT TYPE="HIDDEN" NAME="name" VALUE=' || x_resp_id || '>');
	htp.br;
	htp.formsubmit;
	htp.formclose;
  exception
	when others then
	  htp.p(sqlerrm);
	  return;
  end;

  procedure upload(name in owa.vc_arr) is
	/* This procedure can upload both one single file as well as multiple files.
    The actual upload is done by the listener. You simply initialize the process
    by providing the file to be uploaded. */
	i binary_integer := 0;
	in_file utl_file.file_type;
	buf varchar2(6000);
	v_buf varchar2(6000);
	x_user_id number;
	x_resp_id number;
	x_appl_id number;
	x_conc_id number;
	x_conc_id_re number;
	supplier_number varchar2(1000);
	supplier_name varchar2(1000);
	alternate_supplier_name varchar2(1000);
	tax_registration_number varchar2(1000);
	supplier_type varchar2(1000);
	payment_term varchar2(1000);
	pay_group varchar2(1000);
	payment_priority varchar2(1000);
	terms_date_basis varchar2(1000);
	pay_date_basis varchar2(1000);
	payment_method varchar2(1000);
	invoice_currency varchar2(1000);
	allow_withholding_tax varchar2(1000);
	wht_phor_ngor_dor varchar2(1000);
	retailer_code varchar2(1000);
	payee varchar2(1000);
	bank_code varchar2(1000);
	bank_account_number varchar2(1000);
	citibank_client_code varchar2(1000);
	citibank_debit_account varchar2(1000);
	citibank_delivery_method varchar2(1000);
	citibank_tax_invoice_required varchar2(1000);
	citibank_receipt_required varchar2(1000);
	citibank_bill_required varchar2(1000);
	citibank_other_document varchar2(1000);
	supplier_site_code varchar2(1000);
	address_line_1 varchar2(1000);
	address_line_2 varchar2(1000);
	address_line_3 varchar2(1000);
	address_line_4 varchar2(1000);
	city varchar2(1000);
	province varchar2(1000);
	postal_code varchar2(1000);
	country varchar2(1000);
	pay_site varchar2(1000);
	purchasing_site varchar2(1000);
	site_payment_terms varchar2(1000);
	site_pay_group varchar2(1000);
	site_payment_priority varchar2(1000);
	site_invoice_currency varchar2(1000);
	site_term_date_basis varchar2(1000);
	site_pay_date_basis varchar2(1000);
	site_payment_method varchar2(1000);
	site_payee varchar2(1000);
	site_bank_code varchar2(1000);
	site_bank_account_number varchar2(1000);
	contact_first_name varchar2(1000);
	contact_last_name varchar2(1000);
	area_code varchar2(1000);
	telephone varchar2(1000);
	fax_number varchar2(1000);
	email varchar2(2000);
	status varchar2(1000);
	default_group varchar2(1000);
	shop_name varchar2(1000);
	mail_to_address1 varchar2(1000);
	mail_to_address2 varchar2(1000);
	mail_to_address3 varchar2(1000);
	owner_name varchar2(1000);
    branch_code varchar2(50);
	x_set_of_book_id number;
	x_org_id number;
	x_line_number number;
	x_num_buf number;
	x_check_error boolean;
	x_sup_name_number boolean;
	x_tax_number boolean;
	x_pnd_number boolean;
	x_retailer_number boolean;
	x_postcode_number boolean;
	x_check_col_sup number;
	x_check_data boolean;
	x_check_dup boolean;
	x_file_name varchar2(100);

  begin
	htp.p('<title>Oracle E-Business Sute Upload files</title>');
	htp.p('<img src="/OA_MEDIA/FNDSSCORP.gif">');
	htp.p('<font color = #336699 ><b>E-Business Sute</b></font>');
	htp.p('<img src="/OA_MEDIA/pbs.gif" >');
	htp.p('<HR>');
	htp.header(3, 'TAC Upload Supplier Interface Text File.');
	x_check_error := false;
	x_check_data := false;
	x_check_dup := true;

	begin
	  select file_name into x_file_name from tac_file_upload_name where file_name = ltrim(rtrim(substr(name(1), instr(name(1), '/') + 1)));

	  x_check_dup := true;
	  htp.p('<b><font color=red>ERROR: </font>' || ltrim(rtrim(substr(name(1), instr(name(1), '/') + 1))) ||
			'<font color=red> Duplicate File name.</font></b>');
	  htp.br;
	  htp.p('<HR>');
	  htp.p('<INPUT TYPE="button" NAME="Close" value = "Close" onclick = "window.close()">');
	exception
	  when no_data_found then
		x_check_dup := false;
	  when too_many_rows then
		begin
		  x_check_dup := true;
		  htp.p('<b><font color=red>ERROR: </font>' || ltrim(rtrim(substr(name(1), instr(name(1), '/') + 1))) ||
				'<font color=red> Duplicate File name.</font></b>');
		  htp.br;
		  htp.p('<HR>');
		  htp.p('<INPUT TYPE="button" NAME="Close" value = "Close" onclick = "window.close()">');
		end;
	end;

	if not x_check_dup then
	  loop
		i := i + 1;

		if name(i) is not null and i = 1 then
		  htp.p('<b>File Name: ' || substr(name(i), instr(name(i), '/') + 1) || ' uploaded.</b>');
		  ma_file(substr(name(i), instr(name(i), '/') + 1));
		  htp.br;
		  x_user_id := to_char(name(2));
		  x_set_of_book_id := to_char(name(3));
		  x_org_id := to_char(name(4));
		  x_appl_id := to_char(name(5));
		  x_resp_id := to_char(name(6));
		  validate_sup(x_check_error
					  ,x_check_data
					  ,substr(name(i), instr(name(i), '/') + 1)
					  ,x_user_id
					  ,x_set_of_book_id
					  ,x_org_id
					  ,x_appl_id
					  ,x_resp_id);

		  if not x_check_error then
			in_file := utl_file.fopen('/usr/tmp', substr(name(i), instr(name(i), '/') + 1), 'r');
			x_line_number := 0;

			loop
			  begin
				utl_file.get_line(in_file, buf);

				v_buf := buf;
				v_buf := replace(v_buf, chr(13), '');
				v_buf := replace(v_buf, chr(10), '');
				buf := v_buf;

				x_line_number := x_line_number + 1;

				if upper(substr(buf, 1, 3)) <> 'SUP' then
				  psplit(buf
						,supplier_number
						,supplier_name
						,alternate_supplier_name
						,tax_registration_number
						,supplier_type
						,payment_term
						,pay_group
						,payment_priority
						,terms_date_basis
						,pay_date_basis
						,payment_method
						,invoice_currency
						,allow_withholding_tax
						,wht_phor_ngor_dor
						,retailer_code
						,payee
						,bank_code
						,bank_account_number
						,citibank_client_code
						,citibank_debit_account
						,citibank_delivery_method
						,citibank_tax_invoice_required
						,citibank_receipt_required
						,citibank_bill_required
						,citibank_other_document
						,supplier_site_code
						,address_line_1
						,address_line_2
						,address_line_3
						,address_line_4
						,city
						,province
						,postal_code
						,country
						,pay_site
						,purchasing_site
						,site_payment_terms
						,site_pay_group
						,site_payment_priority
						,site_invoice_currency
						,site_term_date_basis
						,site_pay_date_basis
						,site_payment_method
						,site_payee
						,site_bank_code
						,site_bank_account_number
						,contact_first_name
						,contact_last_name
						,area_code
						,telephone
						,fax_number
						,email
						,status
						,default_group
						,shop_name
						,mail_to_address1
						,mail_to_address2
						,mail_to_address3
						,owner_name
                        ,branch_code);

				  if status = 'NEW' then
					insert into dtac_ap_suppliers_temp_int
					  (vendor_number
					  ,vendor_name
					  ,vendor_name_alt
					  ,vat_registration_num
					  ,vendor_type_lookup_code
					  ,terms_name
					  ,pay_group_lookup_code
					  ,payment_priority
					  ,terms_date_basis
					  ,pay_date_basis_lookup_code
					  ,payment_method_lookup_code
					  ,invoice_currency_code
					  ,allow_awt_flag
					  ,global_attribute20
					  ,attribute1
					  ,attribute2
					  ,attribute3
					  ,attribute4
					  ,attribute_category
					  ,attribute5
					  ,attribute6
					  ,attribute7
					  ,attribute8
					  ,attribute9
					  ,attribute10
					  ,attribute11
					  ,vendor_site_code
					  ,address_line1
					  ,address_line2
					  ,address_line3
					  ,address_line4
					  ,city
					  ,zip
					  ,province
					  ,country
					  ,pay_site_flag
					  ,purchasing_site_flag
					  ,site_terms_name
					  ,site_pay_group_lookup_code
					  ,site_payment_priority
					  ,site_invoice_currency_code
					  ,site_terms_date_basis
					  ,site_pay_date_basis_lookup_cod
					  ,site_payment_method_lookup_cod
					  ,site_attribute2
					  ,site_attribute3
					  ,site_attribute4
					  ,first_name
					  ,last_name
					  ,area_code
					  ,phone
					  ,email_address
					  ,fax
					  ,status
					  ,default_group
					  ,shop_name
					  ,mail_to_address1
					  ,mail_to_address2
					  ,mail_to_address3
					  ,owner_name
					  ,source_file_name
					  ,eai_crtd_dttm
            ,branch_code)
					values
					  (supplier_number
					  ,supplier_name
					  ,alternate_supplier_name
					  ,tax_registration_number
					  ,supplier_type
					  ,payment_term
					  ,pay_group
					  ,payment_priority
					  ,terms_date_basis
					  ,pay_date_basis
					  ,payment_method
					  ,invoice_currency
					  ,allow_withholding_tax
					  ,wht_phor_ngor_dor
					  ,retailer_code
					  ,payee
					  ,bank_code
					  ,bank_account_number
					  ,'CITIBANK'
					  ,citibank_client_code
					  ,citibank_debit_account
					  ,citibank_delivery_method
					  ,citibank_tax_invoice_required
					  ,citibank_receipt_required
					  ,citibank_bill_required
					  ,citibank_other_document
					  ,supplier_site_code
					  ,address_line_1
					  ,address_line_2
					  ,address_line_3
					  ,address_line_4
					  ,city
					  ,postal_code
					  ,province
					  ,country
					  ,pay_site
					  ,purchasing_site
					  ,site_payment_terms
					  ,site_pay_group
					  ,site_payment_priority
					  ,site_invoice_currency
					  ,site_term_date_basis
					  ,site_pay_date_basis
					  ,site_payment_method
					  ,site_payee
					  ,site_bank_code
					  ,site_bank_account_number
					  ,contact_first_name
					  ,contact_last_name
					  ,area_code
					  ,telephone
					  ,email
					  ,fax_number
					  ,status
					  ,default_group
					  ,shop_name
					  ,mail_to_address1
					  ,mail_to_address2
					  ,mail_to_address3
					  ,owner_name
					  ,substr(name(i), instr(name(i), '/') + 1)
					  ,sysdate
            ,branch_code);

					commit;
				  end if;
				end if;
			  exception
				when others then
				  exit;
			  end;
			end loop;

			utl_file.fclose(in_file);
		  else
			fnd_global.apps_initialize(x_user_id, x_resp_id, x_appl_id);
			x_conc_id := fnd_request.submit_request(application => 'SQLAP'
												   ,program => 'TACAP039'
												   ,description => null
												   ,start_time => sysdate
												   ,sub_request => false
												   ,argument1 => substr(name(i), instr(name(i), '/') + 1));
			commit;
		  end if;

		  htp.br;
		  htp.p('<b><u><font color=black>Validation Result:</font></u></b>');
		  htp.br;

		  if x_check_data then
			if not x_check_error then
			  fnd_global.apps_initialize(x_user_id, x_resp_id, x_appl_id);
			  x_conc_id_re := fnd_request.submit_request(application => 'SQLAP'
														,program => 'TACAP039'
														,description => null
														,start_time => sysdate
														,sub_request => false
														,argument1 => substr(name(i), instr(name(i), '/') + 1));
			  commit;
			  fnd_global.apps_initialize(x_user_id, x_resp_id, x_appl_id);
			  x_conc_id := fnd_request.submit_request(application => 'SQLAP'
													 ,program => 'TACAP002_V2'
													 ,description => null
													 ,start_time => sysdate
													 ,sub_request => false
													 ,argument1 => default_group
													 ,argument2 => x_set_of_book_id
													 ,argument3 => x_org_id);
			  commit;
			  htp.p('<b><font color=green>TEXT FILE VALIDATTION COMPLETED.</font></b>');
			  htp.br;
			  htp.p('<b><font color=blue>Please kindly check output of program at "TAC Upload Supplier Interface Text File" with Request ID : </font></b>' ||
					to_char(x_conc_id_re));
			  htp.br;
			  htp.p('<b><font color=blue>and check program run status at "TAC AP Supplier Interface Program Version2" with Request ID : </font></b>' ||
					to_char(x_conc_id));
			  htp.br;
			else

			  fnd_global.apps_initialize(x_user_id, x_resp_id, x_appl_id);
			  x_conc_id := fnd_request.submit_request(application => 'SQLAP'
													 ,program => 'TACAP039'
													 ,description => null
													 ,start_time => sysdate
													 ,sub_request => false
													 ,argument1 => substr(name(i), instr(name(i), '/') + 1));
			  commit;

			  htp.p('<b><font color=red>ERROR: TEXT FILE IS NOT VALID.</font></b>');
			  htp.br;
			  htp.p('<b><font color=red>Please kindly check Log Error at "TAC Upload Supplier Interface Text File" with Request ID : </font></b>' ||
					to_char(x_conc_id));
			  htp.br;
			end if;
		  else
			fnd_global.apps_initialize(x_user_id, x_resp_id, x_appl_id);
			x_conc_id_re := fnd_request.submit_request(application => 'SQLAP'
													  ,program => 'TACAP039'
													  ,description => null
													  ,start_time => sysdate
													  ,sub_request => false
													  ,argument1 => substr(name(i), instr(name(i), '/') + 1));
			commit;
			htp.p('<b><font color=black>No Data for validation.</font></b>');
			htp.br;
			htp.p('<b><font color=red>Please kindly check output at "TAC Upload Supplier Interface Text File" with Request ID : </font></b>' ||
				  to_char(x_conc_id_re));
			htp.br;
		  end if;
		  htp.br;
		  htp.p('<HR>');
		  htp.p('<INPUT TYPE="button" NAME="Close" value = "Close" onclick = "window.close()">');
		end if;

	  end loop;
	end if;

  exception
	when no_data_found then
	  null;
	when others then
	  htp.p(sqlerrm);
	  return;
  end;

  procedure download_form is
	-- This procedure shows you which files can be downloaded, and allows you to do so.
	cursor c1 is
	  select name from fnd_lobs_document;
  begin
	htp.htmlopen;
	htp.bodyopen;

	for l1 in c1
	loop
	  htp.anchor2(curl => 'tacap039.download?p_file=' || l1.name, ctext => l1.name, ctarget => 'frame3');
	  htp.br;
	end loop;

	htp.bodyclose;
	htp.htmlclose;
  exception
	when others then
	  htp.p(sqlerrm);
	  return;
  end;

  procedure download(p_file in varchar2) is
  begin
	/*
    The actual download is handled by the PL/SQL gateway based on the settings
    in your DAD. The code below simply initialize the process by specifying which file to get.
    */
	wpg_docload.download_file(p_file);
  exception
	when others then
	  htp.p(sqlerrm);
	  return;
  end;

  procedure remove_form is
	-- This procedure creates an HTML form that lets the end user delete unwanted files.
	cursor c1 is
	  select name
			,mime_type
			,doc_size
			,dad_charset
			,last_updated
			,content_type
	  from fnd_lobs_document;
  begin
	htp.header(3, 'Select the file(s) to remove');
	htp.formopen(curl => 'tacap039.remove', cmethod => 'POST');
	htp.tableopen(cborder => 'BORDER=1');

	for l1 in c1
	loop
	  htp.tablerowopen;
	  htp.tabledata(htf.formcheckbox(cname => 'p_file', cvalue => l1.name));
	  htp.tabledata(l1.name);
	  htp.tabledata(l1.mime_type);
	  htp.tabledata(l1.doc_size);
	  htp.tabledata(l1.dad_charset);
	  htp.tabledata(l1.last_updated);
	  htp.tabledata(l1.content_type);
	  htp.tablerowclose;
	end loop;

	htp.tableclose;
	htp.formsubmit;
	htp.formclose;
  exception
	when others then
	  htp.p(sqlerrm);
	  return;
  end;

  procedure remove(p_file in owa.vc_arr) is
	-- The procedure removes the file(s) that the end user has selected for deletion.
	i binary_integer := 0;
  begin
	loop
	  i := i + 1;
	  htp.p(p_file(i) || ' removed');
	  htp.br;

	  delete from fnd_lobs_document where name = p_file(i);
	end loop;
  exception
	when no_data_found then
	  null;
	when others then
	  htp.p(sqlerrm);
	  return;
  end;

  procedure writeblobtofile
  (
	p_directory in varchar2
   ,myfilename in varchar2
  ) is
	v_blob blob;
	blob_length integer;
	out_file utl_file.file_type;
	v_buffer raw(32767);
	chunk_size binary_integer := 32767;
	blob_position integer := 1;

	cursor w1_cur is
	  select blob_content from fnd_lobs_document where name = myfilename order by last_updated desc;
	w1_rec w1_cur%rowtype;

  begin
	-- Retrieve the BLOB for reading
	open w1_cur;
	fetch w1_cur
	  into w1_rec;
	v_blob := w1_rec.blob_content;
	close w1_cur;

	/*
    -- Retrieve the BLOB for reading
    SELECT blob_content
      INTO v_blob
      FROM fnd_lobs_document
     WHERE NAME = myfilename;
    */

	-- Retrieve the SIZE of the BLOB
	blob_length := dbms_lob.getlength(v_blob);
	-- Open a handle to the location where you are going to write the BLOB to file
	-- NOTE: The 'wb' parameter means "write in byte mode" and is only availabe
	--       in the UTL_FILE package with Oracle 10g or later
	-- out_file := UTL_FILE.FOPEN (P_DIRECTORY, myfilename, 'wb', chunk_size);
	out_file := utl_file.fopen(p_directory, myfilename, 'w', chunk_size);

	-- Write the BLOB to file in chunks
	while blob_position <= blob_length
	loop
	  if blob_position + chunk_size - 1 > blob_length then
		chunk_size := blob_length - blob_position + 1;
	  end if;

	  dbms_lob.read(v_blob, chunk_size, blob_position, v_buffer);
	  utl_file.put_raw(out_file, v_buffer, true);
	  blob_position := blob_position + chunk_size;
	end loop;

	-- Close the file handle
	utl_file.fclose(out_file);
  end;

  procedure ma_file(p_name in varchar2) is
	v_dir varchar2(255);
  begin
	update fnd_lobs_document set name = substr(name, instr(name, '/') + 1);

	begin
	  insert into tac_file_upload_name
		(file_name
		,upload_date
		,file_count)
	  values
		(p_name
		,sysdate
		,1);

	  commit;
	  v_dir := fnd_profile.value('BIS_DEBUG_LOG_DIRECTORY');
	  tacap039.writeblobtofile(v_dir, p_name);

	  delete from fnd_lobs_document where name = p_name;

	  commit;
	exception
	  when dup_val_on_index then
		htp.p('<font color = red ><b> Duplicate File name</b></font>');

		delete from fnd_lobs_document where name = p_name;

		commit;
	end;
  end;

  procedure psplit
  (
	v_instr in varchar2
   ,supplier_number out varchar2
   ,supplier_name out varchar2
   ,alternate_supplier_name out varchar2
   ,tax_registration_number out varchar2
   ,supplier_type out varchar2
   ,payment_term out varchar2
   ,pay_group out varchar2
   ,payment_priority out varchar2
   ,terms_date_basis out varchar2
   ,pay_date_basis out varchar2
   ,payment_method out varchar2
   ,invoice_currency out varchar2
   ,allow_withholding_tax out varchar2
   ,wht_phor_ngor_dor out varchar2
   ,retailer_code out varchar2
   ,payee out varchar2
   ,bank_code out varchar2
   ,bank_account_number out varchar2
   ,citibank_client_code out varchar2
   ,citibank_debit_account out varchar2
   ,citibank_delivery_method out varchar2
   ,citibank_tax_invoice_required out varchar2
   ,citibank_receipt_required out varchar2
   ,citibank_bill_required out varchar2
   ,citibank_other_document out varchar2
   ,supplier_site_code out varchar2
   ,address_line_1 out varchar2
   ,address_line_2 out varchar2
   ,address_line_3 out varchar2
   ,address_line_4 out varchar2
   ,city out varchar2
   ,province out varchar2
   ,postal_code out varchar2
   ,country out varchar2
   ,pay_site out varchar2
   ,purchasing_site out varchar2
   ,site_payment_terms out varchar2
   ,site_pay_group out varchar2
   ,site_payment_priority out varchar2
   ,site_invoice_currency out varchar2
   ,site_term_date_basis out varchar2
   ,site_pay_date_basis out varchar2
   ,site_payment_method out varchar2
   ,site_payee out varchar2
   ,site_bank_code out varchar2
   ,site_bank_account_number out varchar2
   ,contact_first_name out varchar2
   ,contact_last_name out varchar2
   ,area_code out varchar2
   ,telephone out varchar2
   ,fax_number out varchar2
   ,email out varchar2
   ,status out varchar2
   ,default_group out varchar2
   ,shop_name out varchar2
   ,mail_to_address1 out varchar2
   ,mail_to_address2 out varchar2
   ,mail_to_address3 out varchar2
   ,owner_name out varchar2
   ,branch_code out varchar2
  ) is
	v_str_end varchar2(4000);
	v_col varchar2(4000);
	v_str varchar2(4000);
  begin
	v_str := v_instr;

	if upper(substr(v_str, 1, 3)) <> 'SUP' then
	  --FOR i IN 1 .. 59
	  for i in 1 .. 60 -- Modified by AP@BAS on 12-Dec-2013 : add branch code column
	  loop
		v_str_end := instr(v_str, ',', 1);

		if v_str_end = 0 and length(v_str) > 0 then
		  v_str_end := length(v_str) + 1;
		end if;

		if v_str_end > 0 then
		  v_col := rtrim(ltrim(substr(v_str, 1, v_str_end - 1)));
		  v_str := substr(v_str, v_str_end + 1);

		  if i = 1 then
			supplier_number := v_col;
		  elsif i = 2 then
			supplier_name := v_col;
		  elsif i = 3 then
			alternate_supplier_name := v_col;
		  elsif i = 4 then
			tax_registration_number := v_col;
		  elsif i = 5 then
			supplier_type := v_col;
		  elsif i = 6 then
			payment_term := v_col;
		  elsif i = 7 then
			pay_group := v_col;
		  elsif i = 8 then
			payment_priority := v_col;
		  elsif i = 9 then
			terms_date_basis := v_col;
		  elsif i = 10 then
			pay_date_basis := v_col;
		  elsif i = 11 then
			payment_method := v_col;
		  elsif i = 12 then
			invoice_currency := v_col;
		  elsif i = 13 then
			allow_withholding_tax := v_col;
		  elsif i = 14 then
			wht_phor_ngor_dor := v_col;
		  elsif i = 15 then
			retailer_code := v_col;
		  elsif i = 16 then
			payee := v_col;
		  elsif i = 17 then
			bank_code := v_col;
		  elsif i = 18 then
			bank_account_number := v_col;
		  elsif i = 19 then
			citibank_client_code := v_col;
		  elsif i = 20 then
			citibank_debit_account := v_col;
		  elsif i = 21 then
			citibank_delivery_method := v_col;
		  elsif i = 22 then
			citibank_tax_invoice_required := v_col;
		  elsif i = 23 then
			citibank_receipt_required := v_col;
		  elsif i = 24 then
			citibank_bill_required := v_col;
		  elsif i = 25 then
			citibank_other_document := v_col;
		  elsif i = 26 then
			supplier_site_code := v_col;
		  elsif i = 27 then
			address_line_1 := v_col;
		  elsif i = 28 then
			address_line_2 := v_col;
		  elsif i = 29 then
			address_line_3 := v_col;
		  elsif i = 30 then
			address_line_4 := v_col;
		  elsif i = 31 then
			city := v_col;
		  elsif i = 32 then
			province := v_col;
		  elsif i = 33 then
			postal_code := v_col;
		  elsif i = 34 then
			country := v_col;
		  elsif i = 35 then
			pay_site := v_col;
		  elsif i = 36 then
			purchasing_site := v_col;
		  elsif i = 37 then
			site_payment_terms := v_col;
		  elsif i = 38 then
			site_pay_group := v_col;
		  elsif i = 39 then
			site_payment_priority := v_col;
		  elsif i = 40 then
			site_invoice_currency := v_col;
		  elsif i = 41 then
			site_term_date_basis := v_col;
		  elsif i = 42 then
			site_pay_date_basis := v_col;
		  elsif i = 43 then
			site_payment_method := v_col;
		  elsif i = 44 then
			site_payee := v_col;
		  elsif i = 45 then
			site_bank_code := v_col;
		  elsif i = 46 then
			site_bank_account_number := v_col;
		  elsif i = 47 then
			contact_first_name := v_col;
		  elsif i = 48 then
			contact_last_name := v_col;
		  elsif i = 49 then
			area_code := v_col;
		  elsif i = 50 then
			telephone := v_col;
		  elsif i = 51 then
			fax_number := v_col;
		  elsif i = 52 then
			email := v_col;
		  elsif i = 53 then
			status := v_col;
		  elsif i = 54 then
			default_group := v_col;
		  elsif i = 55 then
			shop_name := v_col;
		  elsif i = 56 then
			mail_to_address1 := v_col;
		  elsif i = 57 then
			mail_to_address2 := v_col;
		  elsif i = 58 then
			mail_to_address3 := v_col;
		  elsif i = 59 then
			owner_name := v_col;
		  elsif i = 60 then
			branch_code := v_col;
		  end if;
		else
		  exit;
		end if;
	  end loop;
	end if;
  end psplit;

  procedure write_log(param_msg varchar2) is
  begin
	fnd_file.put_line(fnd_file.log, param_msg);
  end write_log;

  procedure num_col_sup
  (
	v_instr in varchar2
   ,v_num out number
  ) is
	v_buf varchar2(1);
	v_index number;
	v_check number;
	v_len number;
  begin
	v_check := 0;
	v_index := 1;
	v_len := length(v_instr);

	while v_index <= v_len
	loop
	  v_buf := substr(v_instr, v_index, 1);

	  if v_buf = ',' then
		v_check := v_check + 1;
	  end if;

	  v_index := v_index + 1;
	end loop;

	v_num := v_check;
  end num_col_sup;

  procedure validate_sup
  (
	check_error out boolean
   ,check_data out boolean
   ,file_name in varchar2
   ,user_id in varchar2
   ,set_of_book_id in varchar2
   ,org_id in varchar2
   ,appl_id in varchar2
   ,resp_id in varchar2
  ) is
	i binary_integer := 0;
	in_file utl_file.file_type;
	buf varchar2(6000);
	v_buf varchar2(6000);
	x_user_id number;
	x_resp_id number;
	x_appl_id number;
	x_conc_id number;
	supplier_number varchar2(1000);
	supplier_name varchar2(1000);
	alternate_supplier_name varchar2(1000);
	tax_registration_number varchar2(1000);
	supplier_type varchar2(1000);
	payment_term varchar2(1000);
	pay_group varchar2(1000);
	payment_priority varchar2(1000);
	terms_date_basis varchar2(1000);
	pay_date_basis varchar2(1000);
	payment_method varchar2(1000);
	invoice_currency varchar2(1000);
	allow_withholding_tax varchar2(1000);
	wht_phor_ngor_dor varchar2(1000);
	retailer_code varchar2(1000);
	payee varchar2(1000);
	bank_code varchar2(1000);
	bank_account_number varchar2(1000);
	citibank_client_code varchar2(1000);
	citibank_debit_account varchar2(1000);
	citibank_delivery_method varchar2(1000);
	citibank_tax_invoice_required varchar2(1000);
	citibank_receipt_required varchar2(1000);
	citibank_bill_required varchar2(1000);
	citibank_other_document varchar2(1000);
	supplier_site_code varchar2(1000);
	address_line_1 varchar2(1000);
	address_line_2 varchar2(1000);
	address_line_3 varchar2(1000);
	address_line_4 varchar2(1000);
	city varchar2(1000);
	province varchar2(1000);
	postal_code varchar2(1000);
	country varchar2(1000);
	pay_site varchar2(1000);
	purchasing_site varchar2(1000);
	site_payment_terms varchar2(1000);
	site_pay_group varchar2(1000);
	site_payment_priority varchar2(1000);
	site_invoice_currency varchar2(1000);
	site_term_date_basis varchar2(1000);
	site_pay_date_basis varchar2(1000);
	site_payment_method varchar2(1000);
	site_payee varchar2(1000);
	site_bank_code varchar2(1000);
	site_bank_account_number varchar2(1000);
	contact_first_name varchar2(1000);
	contact_last_name varchar2(1000);
	area_code varchar2(1000);
	telephone varchar2(1000);
	fax_number varchar2(1000);
	email varchar2(2000);
	status varchar2(1000);
	default_group varchar2(1000);
	shop_name varchar2(1000);
	mail_to_address1 varchar2(1000);
	mail_to_address2 varchar2(1000);
	mail_to_address3 varchar2(1000);
	owner_name varchar2(1000);
	x_set_of_book_id number;
	x_org_id number;
	x_line_number number;
	x_num_buf number;
	x_check_error boolean;
	x_sup_name_number boolean;
	x_tax_number boolean;
	x_pnd_number boolean;
	x_retailer_number boolean;
	x_postcode_number boolean;
	x_check_col_sup number;
	x_check_data boolean;
	xx_default_group varchar2(1000);
	x_tax_str varchar2(100);
	x_check_error_find boolean;
	x_check_data_find boolean;
	branch_code varchar2(50); -- added by AP@BAS on 12-Dec-2013 : assign branch code to global_attribute14
  begin
	x_check_error := false;
	x_check_data := false;
	x_check_error_find := false;
	x_check_data_find := false;
	check_error := x_check_error;
	check_data := x_check_data;
	in_file := utl_file.fopen('/usr/tmp', file_name, 'r');
	x_line_number := 0;
	xx_default_group := null;

	loop
	  begin
		utl_file.get_line(in_file, buf);

		v_buf := buf;
		v_buf := replace(v_buf, chr(13), '');
		v_buf := replace(v_buf, chr(10), '');
		buf := v_buf;

		x_line_number := x_line_number + 1;

		if upper(substr(buf, 1, 3)) <> 'SUP' then
		  x_check_data := true;
		  psplit(buf
				,supplier_number
				,supplier_name
				,alternate_supplier_name
				,tax_registration_number
				,supplier_type
				,payment_term
				,pay_group
				,payment_priority
				,terms_date_basis
				,pay_date_basis
				,payment_method
				,invoice_currency
				,allow_withholding_tax
				,wht_phor_ngor_dor
				,retailer_code
				,payee
				,bank_code
				,bank_account_number
				,citibank_client_code
				,citibank_debit_account
				,citibank_delivery_method
				,citibank_tax_invoice_required
				,citibank_receipt_required
				,citibank_bill_required
				,citibank_other_document
				,supplier_site_code
				,address_line_1
				,address_line_2
				,address_line_3
				,address_line_4
				,city
				,province
				,postal_code
				,country
				,pay_site
				,purchasing_site
				,site_payment_terms
				,site_pay_group
				,site_payment_priority
				,site_invoice_currency
				,site_term_date_basis
				,site_pay_date_basis
				,site_payment_method
				,site_payee
				,site_bank_code
				,site_bank_account_number
				,contact_first_name
				,contact_last_name
				,area_code
				,telephone
				,fax_number
				,email
				,status
				,default_group
				,shop_name
				,mail_to_address1
				,mail_to_address2
				,mail_to_address3
				,owner_name
				,branch_code);
		  num_col_sup(buf, x_check_col_sup);

		  --IF x_check_col_sup < 58
		  if x_check_col_sup < 59 --Modified by AP@BAS on 12-Dec-2013 :
		   then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Number of column is less than definition.');
		  end if;

		  if length(supplier_number) > 30 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Supplier Number] is over.');
		  end if;

		  if supplier_number is not null then
			begin
			  x_num_buf := to_number(supplier_number);
			exception
			  when others then
				--write_log(:P_CONC_REQUEST_ID,'Line: '||to_char(x_line_number)||' Supplier Number is not a number format.');
				x_check_error := true;
				write_log('Line: ' || to_char(x_line_number) || ' ' || 'Supplier Number is not a number format.');
			end;
		  end if;

		  if length(supplier_name) > 240 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Supplier Name] is over.');
		  end if;

		  if (supplier_name is null) then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Supplier Name is null.');
		  end if;

		  if length(alternate_supplier_name) > 320 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Alternate Supplier Name] is over.');
		  end if;

		  if length(tax_registration_number) = 10 or length(tax_registration_number) = 13 then
			x_check_error := false;
		  else
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Tax Registration Number] must 10 or 13 ditgits. : ' ||
					  tax_registration_number);
		  end if;

		  -- Modify by AP@IS-ES on 23-Nov-2011
		  if length(tax_registration_number) = 13 then
			-- validate ID Card pattern
			if tac_validate_data.validate_supplier_tax(tax_registration_number) = 0 then
			  x_check_error := true;
			  write_log('Line: ' || to_char(x_line_number) || ' ' ||
						'Data of column [Tax Registration Number] does not comply Central Civil-Registration : ' || tax_registration_number);
			end if;
		  end if;
		  -- End of modify

		  x_tax_number := true;

		  begin
			select ltrim(rtrim(translate(trim(tax_registration_number), '1234567890', ' '))) into x_tax_str from dual;
			if x_tax_str is null then
			  x_tax_number := true;
			else
			  x_tax_number := false;
			end if;
		  exception
			when others then
			  x_tax_number := false;
		  end;

		  begin
			x_num_buf := to_number(tax_registration_number);
		  exception
			when others then
			  x_tax_number := false;
		  end;

		  --Modify by AP@IS-ES on 23-Nov-2011
		  -- tax_registration_number is null able by SRS.
		  --IF ((tax_registration_number IS NULL) OR (NOT x_tax_number))
		  if ((not x_tax_number)) then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Tax Registration Number is not a number format : ' || tax_registration_number
					  --|| 'Tax Registration Number is null or not a number format : ' || tax_registration_number
					  );
		  end if;

		  if length(supplier_type) > 30 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Supplier Type] is over.');
		  end if;

		  if (supplier_type is null) then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Supplier Type is null.');
		  end if;

		  if length(payment_term) > 50 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Payment Term] is over.');
		  end if;

		  if length(pay_group) > 25 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Pay Group] is over.');
		  end if;

		  if length(terms_date_basis) > 25 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Terms Date Basis] is over.');
		  end if;

		  if length(pay_date_basis) > 25 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Pay Date Basis] is over.');
		  end if;

		  if length(payment_method) > 25 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Payment Method] is over.');
		  end if;

		  if length(invoice_currency) > 15 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Invoice_Currency] is over.');
		  end if;

		  if length(allow_withholding_tax) > 1 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Allow Withholding Tax] is over.');
		  end if;

		  if length(wht_phor_ngor_dor) > 4 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [WHT Phor Ngor Dor] is over.');
		  end if;

		  x_pnd_number := true;

		  begin
			x_num_buf := to_number(wht_phor_ngor_dor);
		  exception
			when others then
			  x_pnd_number := false;
		  end;

		  if ((wht_phor_ngor_dor is null) or (not x_pnd_number)) then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'WHT Phor Ngor Dor is null or not a number format.');
		  end if;

		  if length(retailer_code) > 30 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Retailer Code] is over.');
		  end if;

		  x_retailer_number := true;

		  begin
			x_num_buf := to_number(retailer_code);
		  exception
			when others then
			  x_retailer_number := false;
		  end;

		  if (not x_retailer_number) then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Retailer Code is not a number format.');
		  end if;

		  if length(payee) > 150 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Payee] is over.');
		  end if;

		  if length(bank_code) > 7 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Bank Code] is over.');
		  end if;

		  if length(bank_account_number) > 150 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Bank Account Number Code] is over.');
		  end if;

		  if length(citibank_client_code) > 50 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Citibank Client Code] is over.');
		  end if;

		  if length(citibank_debit_account) > 35 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Citibank Debit Account] is over.');
		  end if;

		  if length(citibank_delivery_method) > 5 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Citibank Delivery Method] is over.');
		  end if;

		  if length(citibank_tax_invoice_required) > 1 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Citibank Tax Invoice Required] is over.');
		  end if;

		  if length(citibank_receipt_required) > 1 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Citibank Receipt Required] is over.');
		  end if;

		  if length(citibank_bill_required) > 1 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Citibank Bill Required] is over.');
		  end if;

		  if length(citibank_other_document) > 35 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Citibank Other Document] is over.');
		  end if;

		  if length(supplier_site_code) > 15 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Supplier Site Code] is over.');
		  end if;

		  if (supplier_site_code is null) then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Supplier Site Code is null.');
		  end if;

		  if length(address_line_1) > 240 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Address Line 1] is over.');
		  end if;

		  if (address_line_1 is null) then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Address Line 1 is null.');
		  end if;

		  if length(address_line_2) > 240 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Address Line 2] is over.');
		  end if;

		  if length(address_line_3) > 240 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Address Line 3] is over.');
		  end if;

		  if length(address_line_4) > 240 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Address Line 4] is over.');
		  end if;

		  if length(city) > 25 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [City] is over.');
		  end if;

		  if (city is null) then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'City is null.');
		  end if;

		  if length(province) > 150 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Province] is over.');
		  end if;

		  if (province is null) then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Province is null.');
		  end if;

		  if length(postal_code) > 20 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Postal Code] is over.');
		  end if;

		  x_postcode_number := true;

		  begin
			x_num_buf := to_number(postal_code);
		  exception
			when others then
			  x_postcode_number := false;
		  end;

		  /*
          IF ((postal_code IS NULL) OR (NOT x_postcode_number))
          THEN
             x_check_error := TRUE;
             write_log (   'Line: '
                        || TO_CHAR (x_line_number)
                        || ' '
                        || 'Postal Code is null or not a number format.'
                       );
          END IF;
          */

		  if length(country) > 25 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Country] is over.');
		  end if;

		  if (country is null) then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Country is null.');
		  end if;

		  if length(pay_site) > 1 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Pay Site] is over.');
		  end if;

		  if length(purchasing_site) > 1 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Purchasing Site] is over.');
		  end if;

		  if length(site_payment_terms) > 50 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Site Payment Terms] is over.');
		  end if;

		  if length(site_pay_group) > 25 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Site Pay Group] is over.');
		  end if;

		  if length(site_invoice_currency) > 15 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Site Invoice Currency] is over.');
		  end if;

		  if length(site_term_date_basis) > 25 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Site_Term_Date_Basis] is over.');
		  end if;

		  if length(site_pay_date_basis) > 25 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Site Pay Date Basis] is over.');
		  end if;

		  if length(site_payment_method) > 25 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Site Payment Method] is over.');
		  end if;

		  if length(site_payee) > 150 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Site Payee] is over.');
		  end if;

		  if length(site_bank_code) > 150 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Site Bank Code] is over.');
		  end if;

		  if length(site_bank_account_number) > 150 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Site Bank Account Number] is over.');
		  end if;

		  if length(contact_first_name) > 15 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Contact First Name] is over.');
		  end if;

		  if length(contact_last_name) > 20 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Contact Last Name] is over.');
		  end if;

		  if length(area_code) > 10 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Area Code] is over.');
		  end if;

		  if length(telephone) > 15 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Telephone] is over.');
		  end if;

		  if length(fax_number) > 15 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Fax Number] is over.');
		  end if;

		  if length(email) > 2000 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [E-Mail] is over.');
		  end if;

		  if length(status) > 30 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Status] is over.');
		  end if;

		  if ((status is null) or (status <> 'NEW')) then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Status is not NEW.');
		  end if;

		  if xx_default_group is null then

			xx_default_group := default_group;

		  end if;

		  if length(default_group) > 40 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Default Group] is over.');
		  end if;

		  if (default_group is null) then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Default Group is null.');
		  end if;

		  if (xx_default_group <> default_group) and (default_group is not null) then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Multiple Source(Default Group) Not Allow.');
		  end if;

		  if length(shop_name) > 150 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Shop Name] is over.');
		  end if;

		  if length(mail_to_address1) > 150 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Mail to Address1] is over.');
		  end if;

		  if length(mail_to_address2) > 150 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Mail to Address2] is over.');
		  end if;

		  if length(mail_to_address3) > 150 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Mail to Address3] is over.');
		  end if;

		  if length(owner_name) > 150 then
			x_check_error := true;
			write_log('Line: ' || to_char(x_line_number) || ' ' || 'Length of column [Owner Name] is over.');
		  end if;

		end if;

		if x_check_error then
		  x_check_error_find := true;
		end if;

		if x_check_data then
		  x_check_data_find := true;
		end if;

	  exception
		when others then
		  exit;
	  end;
	end loop;

	utl_file.fclose(in_file);
	check_error := x_check_error_find;
	check_data := x_check_data_find;
  end validate_sup;
end;