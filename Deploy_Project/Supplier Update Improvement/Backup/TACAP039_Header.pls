create or replace PACKAGE TACAP039
IS
   PROCEDURE upload_form;

   PROCEDURE upload (NAME IN OWA.vc_arr);

   PROCEDURE download_form;

   PROCEDURE download (p_file IN VARCHAR2);

   PROCEDURE remove_form;

   PROCEDURE remove (p_file IN OWA.vc_arr);

   PROCEDURE writeblobtofile (p_directory IN VARCHAR2, myfilename IN VARCHAR2);

   PROCEDURE ma_file (p_name IN VARCHAR2);
   PROCEDURE psplit (
      v_instr                         IN       VARCHAR2,
      supplier_number                 OUT      VARCHAR2,
      supplier_name                   OUT      VARCHAR2,
      alternate_supplier_name         OUT      VARCHAR2,
      tax_registration_number         OUT      VARCHAR2,
      supplier_type                   OUT      VARCHAR2,
      payment_term                    OUT      VARCHAR2,
      pay_group                       OUT      VARCHAR2,
      payment_priority                OUT      VARCHAR2,
      terms_date_basis                OUT      VARCHAR2,
      pay_date_basis                  OUT      VARCHAR2,
      payment_method                  OUT      VARCHAR2,
      invoice_currency                OUT      VARCHAR2,
      allow_withholding_tax           OUT      VARCHAR2,
      wht_phor_ngor_dor               OUT      VARCHAR2,
      retailer_code                   OUT      VARCHAR2,
      payee                           OUT      VARCHAR2,
      bank_code                       OUT      VARCHAR2,
      bank_account_number             OUT      VARCHAR2,
      citibank_client_code            OUT      VARCHAR2,
      citibank_debit_account          OUT      VARCHAR2,
      citibank_delivery_method        OUT      VARCHAR2,
      citibank_tax_invoice_required   OUT      VARCHAR2,
      citibank_receipt_required       OUT      VARCHAR2,
      citibank_bill_required          OUT      VARCHAR2,
      citibank_other_document         OUT      VARCHAR2,
      supplier_site_code              OUT      VARCHAR2,
      address_line_1                  OUT      VARCHAR2,
      address_line_2                  OUT      VARCHAR2,
      address_line_3                  OUT      VARCHAR2,
      address_line_4                  OUT      VARCHAR2,
      city                            OUT      VARCHAR2,
      province                        OUT      VARCHAR2,
      postal_code                     OUT      VARCHAR2,
      country                         OUT      VARCHAR2,
      pay_site                        OUT      VARCHAR2,
      purchasing_site                 OUT      VARCHAR2,
      site_payment_terms              OUT      VARCHAR2,
      site_pay_group                  OUT      VARCHAR2,
      site_payment_priority           OUT      VARCHAR2,
      site_invoice_currency           OUT      VARCHAR2,
      site_term_date_basis            OUT      VARCHAR2,
      site_pay_date_basis             OUT      VARCHAR2,
      site_payment_method             OUT      VARCHAR2,
      site_payee                      OUT      VARCHAR2,
      site_bank_code                  OUT      VARCHAR2,
      site_bank_account_number        OUT      VARCHAR2,
      contact_first_name              OUT      VARCHAR2,
      contact_last_name               OUT      VARCHAR2,
      area_code                       OUT      VARCHAR2,
      telephone                       OUT      VARCHAR2,
      fax_number                      OUT      VARCHAR2,
      email                           OUT      VARCHAR2,
      status                          OUT      VARCHAR2,
      default_group                   OUT      VARCHAR2,
      shop_name                       OUT      VARCHAR2,
      mail_to_address1                OUT      VARCHAR2,
      mail_to_address2                OUT      VARCHAR2,
      mail_to_address3                OUT      VARCHAR2,
      owner_name                      OUT      VARCHAR2,
      branch_code                     OUT      VARCHAR2
   );

   /*
   PROCEDURE psplit (
      v_instr                         IN       VARCHAR2,
      supplier_number                 OUT      VARCHAR2,
      supplier_name                   OUT      VARCHAR2,
      alternate_supplier_name         OUT      VARCHAR2,
      tax_registration_number         OUT      VARCHAR2,
      supplier_type                   OUT      VARCHAR2,
      payment_term                    OUT      VARCHAR2,
      pay_group                       OUT      VARCHAR2,
      payment_priority                OUT      VARCHAR2,
      terms_date_basis                OUT      VARCHAR2,
      pay_date_basis                  OUT      VARCHAR2,
      payment_method                  OUT      VARCHAR2,
      invoice_currency                OUT      VARCHAR2,
      allow_withholding_tax           OUT      VARCHAR2,
      wht_phor_ngor_dor               OUT      VARCHAR2,
      retailer_code                   OUT      VARCHAR2,
      payee                           OUT      VARCHAR2,
      bank_code                       OUT      VARCHAR2,
      bank_account_number             OUT      VARCHAR2,
      citibank_client_code            OUT      VARCHAR2,
      citibank_debit_account          OUT      VARCHAR2,
      citibank_delivery_method        OUT      VARCHAR2,
      citibank_tax_invoice_required   OUT      VARCHAR2,
      citibank_receipt_required       OUT      VARCHAR2,
      citibank_bill_required          OUT      VARCHAR2,
      citibank_other_document         OUT      VARCHAR2,
      supplier_site_code              OUT      VARCHAR2,
      address_line_1                  OUT      VARCHAR2,
      address_line_2                  OUT      VARCHAR2,
      address_line_3                  OUT      VARCHAR2,
      address_line_4                  OUT      VARCHAR2,
      city                            OUT      VARCHAR2,
      province                        OUT      VARCHAR2,
      postal_code                     OUT      VARCHAR2,
      country                         OUT      VARCHAR2,
      pay_site                        OUT      VARCHAR2,
      purchasing_site                 OUT      VARCHAR2,
      site_payment_terms              OUT      VARCHAR2,
      site_pay_group                  OUT      VARCHAR2,
      site_payment_priority           OUT      VARCHAR2,
      site_invoice_currency           OUT      VARCHAR2,
      site_term_date_basis            OUT      VARCHAR2,
      site_pay_date_basis             OUT      VARCHAR2,
      site_payment_method             OUT      VARCHAR2,
      site_payee                      OUT      VARCHAR2,
      site_bank_code                  OUT      VARCHAR2,
      site_bank_account_number        OUT      VARCHAR2,
      contact_first_name              OUT      VARCHAR2,
      contact_last_name               OUT      VARCHAR2,
      area_code                       OUT      VARCHAR2,
      telephone                       OUT      VARCHAR2,
      fax_number                      OUT      VARCHAR2,
      email                           OUT      VARCHAR2,
      status                          OUT      VARCHAR2,
      default_group                   OUT      VARCHAR2,
      shop_name                       OUT      VARCHAR2,
      mail_to_address1                OUT      VARCHAR2,
      mail_to_address2                OUT      VARCHAR2,
      mail_to_address3                OUT      VARCHAR2,
      owner_name                      OUT      VARCHAR2,
      branch_code                     OUT      VARCHAR2
   );
*/
   PROCEDURE write_log (param_msg VARCHAR2);

   PROCEDURE num_col_sup (v_instr IN VARCHAR2, v_num OUT NUMBER);

   PROCEDURE validate_sup (
      check_error      OUT      BOOLEAN,
      check_data       OUT      BOOLEAN,
      file_name        IN       VARCHAR2,
      user_id          IN       VARCHAR2,
      set_of_book_id   IN       VARCHAR2,
      org_id           IN       VARCHAR2,
      appl_id          IN       VARCHAR2,
      resp_id          IN       VARCHAR2
   );
END;