ALTER table wmuser.DTAC_AP_SUPPLIERS_TEMP_INT ADD
(
	match_approval_level varchar2(30),
	invoice_tax_code varchar2(20),
	wht_condition varchar2(50),
	wht_revenue_name varchar2(50),
	wht_revenue_type varchar2(50)
);
