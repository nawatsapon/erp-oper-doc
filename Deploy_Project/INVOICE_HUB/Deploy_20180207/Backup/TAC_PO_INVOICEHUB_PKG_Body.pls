create or replace PACKAGE BODY TAC_PO_INVOICEHUB_PKG -- dt_invoicehub_pkg_new2
 AS
  /******************************************************************************************************************************************************
  Package Name :DT_INVOICEHUB_PKG
  Description :This package is modified for moving pr data from source concession to destination concession
  
  Revisions:
  Ver Date Author Description
  --------- --------- --------------- ----------------------------------
  1.0 25/07/2016 Suttisak Uamornlert Package creation
  2.0[issue50] 18/05/2017 Suttisak Uamornlert Fixed case invalid check fund in ap invoice
  2.1[issue50] 17/09/2017 Suttisak Uamornlert Fixed case invalid check fund in ap invoice
                                              Fixed case create duplicate line po
  *****************************************************************************************************************************************************/
  /*-------------------------------*/
  /* parameter p_debug_type */
  /* log -- FND_FILE.LOG */
  /* output -- FND_FILE.OUTPUT */
  /*-------------------------------*/
  PROCEDURE DEBUG_MSG(P_DEBUG_TYPE IN VARCHAR2, P_MSG IN VARCHAR2) IS
  BEGIN
    IF P_DEBUG_TYPE = 'LOG' AND G_DEBUG_FLAG = 'Y' THEN
      FND_FILE.PUT_LINE(FND_FILE.LOG, P_MSG);
    ELSIF P_DEBUG_TYPE = 'OUTPUT' THEN
      FND_FILE.PUT_LINE(FND_FILE.OUTPUT, P_MSG);
    END IF;
  END DEBUG_MSG;

  PROCEDURE CALL_LOGMONITOR(P_PROGRAM         IN VARCHAR2,
                            P_CONC_REQUEST_ID IN NUMBER) IS
    L_POIMPORT_REQ_ID NUMBER;
  
    L_FINISHED   BOOLEAN;
    L_PHASE      VARCHAR2(100);
    L_STATUS     VARCHAR2(100);
    L_DEV_PHASE  VARCHAR2(100);
    L_DEV_STATUS VARCHAR2(100);
    L_MESSAGE    VARCHAR2(100);
    L_CHECK_DATA VARCHAR2(1);
  BEGIN
    L_POIMPORT_REQ_ID := FND_REQUEST.SUBMIT_REQUEST(APPLICATION => 'SQLAP',
                                                    PROGRAM     => 'DTINVHUB006',
                                                    SUB_REQUEST => FALSE,
                                                    ARGUMENT1   => P_PROGRAM, --'DTINVHUB002',
                                                    ARGUMENT2   => P_CONC_REQUEST_ID,
                                                    ARGUMENT3   => 'N');
    COMMIT;
  
    LOOP
      L_FINISHED := FND_CONCURRENT.WAIT_FOR_REQUEST(REQUEST_ID => L_POIMPORT_REQ_ID,
                                                    INTERVAL   => 5,
                                                    MAX_WAIT   => 0,
                                                    PHASE      => L_PHASE,
                                                    STATUS     => L_STATUS,
                                                    DEV_PHASE  => L_DEV_PHASE,
                                                    DEV_STATUS => L_DEV_STATUS,
                                                    MESSAGE    => L_MESSAGE);
    
      EXIT WHEN L_PHASE = 'Completed';
      DBMS_LOCK.SLEEP(10);
    END LOOP;
  END;

  FUNCTION F_INVHUB_GET_PR_AMOUNT(P_PR_NUMBER IN VARCHAR2,
                                  P_PR_LINE   IN NUMBER) RETURN NUMBER IS
    L_AMOUNT NUMBER;
  BEGIN
    SELECT (NVL(PRL.UNIT_PRICE, 0) * NVL(PRL.QUANTITY, 0))
      INTO L_AMOUNT
      FROM PO_REQUISITION_HEADERS_ALL PRH, PO_REQUISITION_LINES_ALL PRL
     WHERE PRH.REQUISITION_HEADER_ID = PRL.REQUISITION_HEADER_ID
       AND PRH.SEGMENT1 = P_PR_NUMBER
       AND PRL.LINE_NUM = P_PR_LINE;
  
    RETURN(L_AMOUNT);
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN(0);
  END;

  FUNCTION F_GET_VENDOR_CONTACT(P_PR_NUMBER IN VARCHAR2) RETURN NUMBER IS
    L_VENDOR_CONTACT NUMBER;
  BEGIN
    G_STEP := 'f_get_vendor_contact';
    DEBUG_MSG('LOG', G_STEP || ' p_pr_number =' || P_PR_NUMBER);
  
    SELECT DISTINCT PRL.VENDOR_CONTACT_ID
      INTO L_VENDOR_CONTACT
      FROM PO_REQUISITION_HEADERS_ALL PRH, PO_REQUISITION_LINES_ALL PRL
     WHERE PRH.SEGMENT1 = P_PR_NUMBER
       AND PRH.REQUISITION_HEADER_ID = PRL.REQUISITION_HEADER_ID;
  
    RETURN(L_VENDOR_CONTACT);
  EXCEPTION
    WHEN OTHERS THEN
      DEBUG_MSG('LOG', 'error :' || SQLERRM);
      RETURN(NULL);
  END;

  PROCEDURE GET_REQUESTER_INFO(P_REQ_HEADER_REFERENCE_NUM IN VARCHAR2,
                               P_REQ_LINE_REFERENCE_NUM   IN NUMBER,
                               O_REQUESTER_ID             IN OUT NUMBER,
                               O_DELIVER_TO_LOCATION      IN OUT NUMBER) IS
  BEGIN
    SELECT PRL.TO_PERSON_ID, PRL.DELIVER_TO_LOCATION_ID
      INTO O_REQUESTER_ID, O_DELIVER_TO_LOCATION
      FROM PO_REQUISITION_HEADERS_ALL PRH, PO_REQUISITION_LINES_ALL PRL
     WHERE PRH.SEGMENT1 = P_REQ_HEADER_REFERENCE_NUM
       AND PRL.LINE_NUM = P_REQ_LINE_REFERENCE_NUM
       AND PRH.REQUISITION_HEADER_ID = PRL.REQUISITION_HEADER_ID;
  EXCEPTION
    WHEN OTHERS THEN
      O_REQUESTER_ID        := NULL;
      O_DELIVER_TO_LOCATION := NULL;
  END;

  PROCEDURE GET_PROJECT_INFORMATION(P_REQ_HEADER_REFERENCE_NUM    IN VARCHAR2,
                                    P_REQ_LINE_REFERENCE_NUM      IN NUMBER,
                                    O_PROJECT_ID                  IN OUT PO_REQ_DISTRIBUTIONS_ALL.PROJECT_ID%TYPE,
                                    O_PROJECT_NAME                IN OUT PA_PROJECTS_ALL.SEGMENT1%TYPE,
                                    O_TASK_ID                     IN OUT PO_REQ_DISTRIBUTIONS_ALL.TASK_ID%TYPE,
                                    O_TASK_NAME                   IN OUT PA_TASKS.TASK_NUMBER%TYPE,
                                    O_EXPENDITURE_TYPE            IN OUT PO_REQ_DISTRIBUTIONS_ALL.EXPENDITURE_TYPE%TYPE,
                                    O_EXPENDITURE_ITEM_DATE       IN OUT PO_REQ_DISTRIBUTIONS_ALL.EXPENDITURE_ITEM_DATE%TYPE,
                                    O_EXPENDITURE_ORGANIZATION_ID IN OUT PO_REQ_DISTRIBUTIONS_ALL.EXPENDITURE_ORGANIZATION_ID%TYPE,
                                    O_PROJECT_ACCOUNTING_CONTEXT  IN OUT PO_REQ_DISTRIBUTIONS_ALL.PROJECT_ACCOUNTING_CONTEXT%TYPE) --[issue50]
   IS
  BEGIN
    SELECT PRD.PROJECT_ID,
           PPA.SEGMENT1,
           PRD.TASK_ID,
           PT.TASK_NUMBER,
           PRD.EXPENDITURE_TYPE,
           PRD.EXPENDITURE_ITEM_DATE,
           PRD.EXPENDITURE_ORGANIZATION_ID,
           nvl(PRD.PROJECT_ACCOUNTING_CONTEXT,'Yes') --[issue50]
      INTO O_PROJECT_ID,
           O_PROJECT_NAME,
           O_TASK_ID,
           O_TASK_NAME,
           O_EXPENDITURE_TYPE,
           O_EXPENDITURE_ITEM_DATE,
           O_EXPENDITURE_ORGANIZATION_ID,
           O_PROJECT_ACCOUNTING_CONTEXT --[issue50]
      FROM PO_REQUISITION_HEADERS_ALL PRH,
           PO_REQUISITION_LINES_ALL   PRL,
           PO_REQ_DISTRIBUTIONS_ALL   PRD,
           PA_PROJECTS_ALL            PPA,
           PA_TASKS                   PT
     WHERE PRH.REQUISITION_HEADER_ID = PRL.REQUISITION_HEADER_ID
       AND PRL.REQUISITION_LINE_ID = PRD.REQUISITION_LINE_ID
       AND PRH.SEGMENT1 = P_REQ_HEADER_REFERENCE_NUM
       AND PRL.LINE_NUM = P_REQ_LINE_REFERENCE_NUM
       AND PRD.PROJECT_ID = PPA.PROJECT_ID(+)
       AND PRD.PROJECT_ID = PT.PROJECT_ID(+)
       AND PRD.TASK_ID = PT.TASK_ID(+);
  EXCEPTION
    WHEN OTHERS THEN
      O_PROJECT_ID                  := NULL;
      O_PROJECT_NAME                := NULL;
      O_TASK_ID                     := NULL;
      O_TASK_NAME                   := NULL;
      O_EXPENDITURE_TYPE            := NULL;
      O_EXPENDITURE_ITEM_DATE       := NULL;
      O_EXPENDITURE_ORGANIZATION_ID := NULL;
      O_PROJECT_ACCOUNTING_CONTEXT  := NULL; --[issue50]
  END;

  FUNCTION GET_PR_ORG_ID(P_PR_NUMBER IN VARCHAR2) RETURN NUMBER IS
    L_ORG_ID NUMBER;
  BEGIN
    SELECT ORG_ID
      INTO L_ORG_ID
      FROM PO_REQUISITION_HEADERS_ALL
     WHERE SEGMENT1 = P_PR_NUMBER;
  
    RETURN(L_ORG_ID);
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN(-1);
  END;

  PROCEDURE WRITE_LOG_DETAIL(V_STARTPROCESS    IN TIMESTAMP,
                             P_CONC_REQUEST_ID IN NUMBER) IS
    V_ENDPROCESS TIMESTAMP;
  BEGIN
    DEBUG_MSG('LOG', 'in write_log_detail');
    DEBUG_MSG('LOG', 'g_log_detail.count=' || G_LOG_DETAIL.COUNT);
    DEBUG_MSG('LOG', 'g_log_detail.count=' || G_LOG_DETAIL.COUNT);
  
    IF G_LOG_DETAIL.COUNT > 0 THEN
      FORALL I IN G_LOG_DETAIL.FIRST .. G_LOG_DETAIL.LAST
        INSERT INTO APPS.DTINVHUB_LOG_DETAIL VALUES G_LOG_DETAIL (I);
    END IF;
  
    SELECT CURRENT_TIMESTAMP INTO V_ENDPROCESS FROM DUAL;
  
    DEBUG_MSG('LOG', 'p_conc_request_id=' || P_CONC_REQUEST_ID);
  
    INSERT INTO APPS.DTINVHUB_LOG_SUMMARY
      (LEVEL_TYPE,
       REQUEST_ID,
       PROGRAM_NAME,
       PROCESS_DATE,
       PROCESSING_TYPE,
       RECORD_COUNT_COMPLETION,
       RECORD_COUNT_ERROR,
       PROCESSING_DATE)
      SELECT 'H',
             REQUEST_ID,
             'DTINVHUB002',
             V_STARTPROCESS,
             'Download',
             SUM(DECODE(ERROR_CODE, NULL, 1, 0)) COMPLETE,
             SUM(DECODE(ERROR_CODE, NULL, 0, 1)) ERROR,
             SUBSTR(TO_CHAR(V_ENDPROCESS - V_STARTPROCESS), 12, 12)
        FROM APPS.DTINVHUB_LOG_DETAIL
       WHERE REQUEST_ID = P_CONC_REQUEST_ID
       GROUP BY 'H', REQUEST_ID, 'DTINVHUB002', SYSDATE, 'Download';
  END;

  PROCEDURE WRITE_PR_HEADER_LOG(P_STG_PO_HEADER  IN OUT R_PO_HEADER,
                                P_STG_LOG_DETAIL IN OUT T_LOG_DETAIL,
                                P_ERR_CODE       IN VARCHAR2,
                                P_ERR_MSG        IN VARCHAR2) IS
  BEGIN
    --if pError_log.count > 0 then
    --forall i in 1..pError_log.count insert into apps.DTINVHUB_LOG values pError_log(i);
    --end if;
    -- p_stg_log_detail.EXTEND;
    P_STG_LOG_DETAIL(P_STG_LOG_DETAIL.LAST).LEVEL_TYPE := 'D';
    P_STG_LOG_DETAIL(P_STG_LOG_DETAIL.LAST).REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
    P_STG_LOG_DETAIL(P_STG_LOG_DETAIL.LAST).ERROR_TYPE := 'PO Interface';
    P_STG_LOG_DETAIL(P_STG_LOG_DETAIL.LAST).PR_NUMBER := P_STG_PO_HEADER.PR_NUMBER;
    P_STG_LOG_DETAIL(P_STG_LOG_DETAIL.LAST).MATCH_OPTION := NULL; --pError_log(i).MATCH_OPTION;
    P_STG_LOG_DETAIL(P_STG_LOG_DETAIL.LAST).VENDOR_NUMBER := P_STG_PO_HEADER.VENDOR_NUM; --pError_log(i).VENDOR_NUMBER;
    P_STG_LOG_DETAIL(P_STG_LOG_DETAIL.LAST).VENDOR_SITE_NAME := P_STG_PO_HEADER.VENDOR_SITE_CODE; --pError_log(i).VENDOR_SITE_NAME;
    P_STG_LOG_DETAIL(P_STG_LOG_DETAIL.LAST).ERROR_CODE := P_ERR_CODE;
    P_STG_LOG_DETAIL(P_STG_LOG_DETAIL.LAST).ERROR_MESSAGE := P_ERR_MSG;
  END WRITE_PR_HEADER_LOG;

  PROCEDURE FETCH_PO_DISTRIBUTION(P_STG_PO_HEADER IN OUT R_PO_HEADER,
                                  P_STG_PO_LINE   IN R_PO_LINE,
                                  P_STG_PO_DIST   IN OUT T_PO_DIST) IS
    CURSOR C_FETCH_DIST(P_REQ_LINE_ID IN NUMBER) IS
      SELECT DISTRIBUTION_NUM,
             REQ_LINE_QUANTITY,
             CODE_COMBINATION_ID,
             GL_ENCUMBERED_DATE,
             DISTRIBUTION_ID
        FROM PO_REQ_DISTRIBUTIONS_ALL
       WHERE REQUISITION_LINE_ID = P_REQ_LINE_ID;
  
    L_FETCH_PO_DIST NUMBER;
  BEGIN
    G_STEP := 'FETCH_PO_DISTRIBUTION';
    DEBUG_MSG('LOG', G_STEP);
    L_FETCH_PO_DIST := 00;
  
    FOR I_FETCH_DIST IN C_FETCH_DIST(P_STG_PO_LINE.REQUISITION_LINE_ID) LOOP
      G_PO_DIST_INDX := G_PO_DIST_INDX + 1;
      P_STG_PO_DIST(G_PO_DIST_INDX).INTERFACE_HEADER_ID := P_STG_PO_LINE.INTERFACE_HEADER_ID;
      P_STG_PO_DIST(G_PO_DIST_INDX).INTERFACE_LINE_ID := P_STG_PO_LINE.INTERFACE_LINE_ID;
      P_STG_PO_DIST(G_PO_DIST_INDX).INTERFACE_DISTRIBUTION_ID := PO_DISTRIBUTIONS_INTERFACE_S.NEXTVAL;
      P_STG_PO_DIST(G_PO_DIST_INDX).DISTRIBUTION_NUM := I_FETCH_DIST.DISTRIBUTION_NUM;
      P_STG_PO_DIST(G_PO_DIST_INDX).QUANTITY_ORDERED := I_FETCH_DIST.REQ_LINE_QUANTITY;
      P_STG_PO_DIST(G_PO_DIST_INDX).CHARGE_ACCOUNT_ID := I_FETCH_DIST.CODE_COMBINATION_ID;
      P_STG_PO_DIST(G_PO_DIST_INDX).GL_ENCUMBERED_DATE := I_FETCH_DIST.GL_ENCUMBERED_DATE;
      P_STG_PO_DIST(G_PO_DIST_INDX).REQ_DISTRIBUTION_ID := I_FETCH_DIST.DISTRIBUTION_ID;
      P_STG_PO_DIST(G_PO_DIST_INDX).REQ_HEADER_REFERENCE_NUM := P_STG_PO_LINE.PR_NUMBER;
      P_STG_PO_DIST(G_PO_DIST_INDX).REQ_LINE_REFERENCE_NUM := P_STG_PO_LINE.LINE_NUM;
      P_STG_PO_DIST(G_PO_DIST_INDX).REQ_DISTRIBUTION_ID := I_FETCH_DIST.DISTRIBUTION_ID;
    END LOOP;
  
    DEBUG_MSG('LOG', 'p_stg_po_dist' || P_STG_PO_DIST.COUNT);
  END;

  PROCEDURE FETCH_PO_HEADER(P_GROUP_NAME IN VARCHAR2,
                            P_PO_HEADER  IN OUT T_PO_HEADER) IS
  BEGIN
    G_STEP := 'Fetch po data';
    DEBUG_MSG('LOG', G_STEP);
  
    SELECT DISTINCT GROUP_NAME,
                    SOURCE,
                    VENDOR_NUM,
                    VENDOR_SITE_CODE,
                    DOC_CATEGORY_CODE,
                    INVOICE_CURRENCY_CODE,
                    PR_NUMBER,
                    NULL,
                    NULL,
                    NULL,
                    'N',
                    NULL,
                    NULL,
                    NULL BULK COLLECT
      INTO P_PO_HEADER
      FROM TAC_INVHUB_INVOICE_STG
     WHERE UPPER(FILE_NAME) = UPPER(NVL(P_GROUP_NAME, FILE_NAME)) --upper(group_name) = upper(nvl(p_group_name,'FXTH_2W_20160801_2100')) --'FXTH_2W_20160725_1633'
       AND NVL(INTERFACE_PO_FLAG, 'N') = G_INTEFACE_PO
     ORDER BY GROUP_NAME, PR_NUMBER;
  
    DEBUG_MSG('LOG',
              'Total record inserted in collection=' || SQL%ROWCOUNT);
  END;

  PROCEDURE FETCH_PO_LINE(P_STG_PO_HEADER IN OUT R_PO_HEADER,
                          P_STG_PO_LINE   IN OUT T_PO_LINE,
                          P_STG_PO_DIST   IN OUT T_PO_DIST) IS
    CURSOR FETCH_PO_LINE(P_GROUP_NAME IN VARCHAR2, P_PR_NUMBER IN VARCHAR2) IS
      SELECT P_STG_PO_HEADER.INTERFACE_HEADER_ID INTERFACE_HEADER_ID,
             TIS.GROUP_NAME,
             TIS.PR_NUMBER PR_NUMBER,
             TIS.PR_LINE_NUMBER LINE_NUM,
             'Expenses' LINE_TYPE,
             TIS.TERMS_NAME PAYMENT_TERMS,
             TIS.MATCH_OPTION,
             'N' INTERFACE_FLAG,
             --TIS.INVOICE_NUM INVOICE_NUM,
             --TIS.LINE_NUMBER INVOICE_LINE,
             sum(TIS.LINE_AMOUNT) line_amount
      --bulk collect into p_stg_po_line
        FROM TAC_INVHUB_INVOICE_STG TIS
       WHERE TIS.GROUP_NAME = P_GROUP_NAME --p_stg_po_header.group_name
         AND NVL(TIS.INTERFACE_PO_FLAG, 'N') = G_INTEFACE_PO
         AND TIS.PR_NUMBER = P_PR_NUMBER --p_stg_po_header.pr_number
       GROUP BY  TIS.GROUP_NAME,
             TIS.PR_NUMBER ,
             TIS.PR_LINE_NUMBER ,
             'Expenses' ,
             TIS.TERMS_NAME ,
             TIS.MATCH_OPTION,
             'N' 
       ORDER BY TIS.GROUP_NAME, TIS.PR_NUMBER, TIS.PR_LINE_NUMBER;
  
    L_REQUISITION_LINE_ID NUMBER;
    L_CHECK               VARCHAR2(1);
    L_CHECK_DUP_PR        NUMBER;
    L_STARTTIME           TIMESTAMP;
    L_ENDTIME             TIMESTAMP;
    L_PO_LINENUM          NUMBER;
  BEGIN
    G_STEP := 'FETCH_PO_LINE';
    DEBUG_MSG('LOG', G_STEP);
  
    DEBUG_MSG('LOG',
              'p_stg_po_header.group_name=' || P_STG_PO_HEADER.GROUP_NAME);
    DEBUG_MSG('LOG',
              'p_stg_po_header.pr_number=' || P_STG_PO_HEADER.PR_NUMBER);
    L_PO_LINENUM := 0;
  
    FOR I_FETCH_PO_LINE IN FETCH_PO_LINE(P_STG_PO_HEADER.GROUP_NAME,
                                         P_STG_PO_HEADER.PR_NUMBER) LOOP
      -- p_stg_po_line.extend;
    
      BEGIN
        SELECT CURRENT_TIMESTAMP INTO L_STARTTIME FROM DUAL;
      
        SELECT COUNT(*)
          INTO L_CHECK_DUP_PR
          FROM TAC_INVHUB_INVOICE_STG TIS
         WHERE TIS.GROUP_NAME = P_STG_PO_HEADER.GROUP_NAME
           AND NVL(TIS.INTERFACE_PO_FLAG, 'N') = 'Y'
           AND TIS.PR_NUMBER = I_FETCH_PO_LINE.PR_NUMBER
           AND TIS.PR_LINE_NUMBER = I_FETCH_PO_LINE.LINE_NUM
         GROUP BY TIS.PR_NUMBER, TIS.PR_LINE_NUMBER;
      
        SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
      
        DEBUG_MSG('LOG', 'l_check_dup_pr=' || L_CHECK_DUP_PR);
      
        IF L_CHECK_DUP_PR > 0 THEN
          BEGIN
            -- EXCEPTION WHEN no_data_found THEN
            -- g_po_line_indx :=g_po_line_indx-1;
          
            G_LOG_DETAIL_SEQ := G_LOG_DETAIL_SEQ + 1;
            G_LOG_DETAIL(G_LOG_DETAIL_SEQ).LEVEL_TYPE := 'D';
            G_LOG_DETAIL(G_LOG_DETAIL_SEQ).REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
            G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_TYPE := 'PO Interface';
            G_LOG_DETAIL(G_LOG_DETAIL_SEQ).AP_INVOICE := NULL; --I_FETCH_PO_LINE.INVOICE_NUM; /*remove code on 17 Sep 2017 fixed case 1 pr multiple invoice*/
                                                                                              
            G_LOG_DETAIL(G_LOG_DETAIL_SEQ).AP_INVOIE_LINE := NULL; --I_FETCH_PO_LINE.INVOICE_LINE; /*remove code on 17 Sep 2017 fixed case 1 pr multiple invoice*/
            G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PR_NUMBER := I_FETCH_PO_LINE.PR_NUMBER;
            G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PR_LINE_NUMBER := I_FETCH_PO_LINE.LINE_NUM;
            G_LOG_DETAIL(G_LOG_DETAIL_SEQ).MATCH_OPTION := I_FETCH_PO_LINE.MATCH_OPTION;
            G_LOG_DETAIL(G_LOG_DETAIL_SEQ).VENDOR_NUMBER := P_STG_PO_HEADER.VENDOR_NUM;
            G_LOG_DETAIL(G_LOG_DETAIL_SEQ).VENDOR_SITE_NAME := P_STG_PO_HEADER.VENDOR_SITE_CODE;
            G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PAYMENT_TERM := I_FETCH_PO_LINE.PAYMENT_TERMS;
            G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_CODE := 'INVHUB002-007'; --Error INVHUB002-007: Duplicate PR number
            G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_MESSAGE := 'Duplicate PR number';
            G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PROCESSING_DATE := L_STARTTIME;
            G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                                             L_STARTTIME),
                                                                     12,
                                                                     12);
            P_STG_PO_HEADER.INTERFACE_FLAG := 'E';
          END;
        END IF;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          NULL;
      END;
    
      -- validate duplicate pr
      /* BEGIN
      SELECT DISTINCT 'Y'
      INTO l_check
      FROM po_requisition_headers_all
      WHERE segment1=p_stg_po_header.pr_number;
      
      END;
      */
    
      -- Validate Payment term
    
      --joe
      BEGIN
        SELECT DISTINCT 'Y'
          INTO L_CHECK
          FROM AP_TERMS
         WHERE NAME = I_FETCH_PO_LINE.PAYMENT_TERMS;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          -- g_po_line_indx :=g_po_line_indx-1;
          G_LOG_DETAIL_SEQ := G_LOG_DETAIL_SEQ + 1;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).LEVEL_TYPE := 'D';
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_TYPE := 'PO Interface';
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).AP_INVOICE := NULL; --I_FETCH_PO_LINE.INVOICE_NUM; /*remove code on 17 Sep 2017 fixed case 1 pr multiple invoice*/
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).AP_INVOIE_LINE := NULL; -- I_FETCH_PO_LINE.INVOICE_LINE; /*remove code on 17 Sep 2017 fixed case 1 pr multiple invoice*/
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PR_NUMBER := I_FETCH_PO_LINE.PR_NUMBER;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PR_LINE_NUMBER := I_FETCH_PO_LINE.LINE_NUM;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).MATCH_OPTION := I_FETCH_PO_LINE.MATCH_OPTION;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).VENDOR_NUMBER := P_STG_PO_HEADER.VENDOR_NUM;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).VENDOR_SITE_NAME := P_STG_PO_HEADER.VENDOR_SITE_CODE;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PAYMENT_TERM := I_FETCH_PO_LINE.PAYMENT_TERMS;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_CODE := 'INVHUB002-013 ';
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_MESSAGE := 'Invalid Term Name';
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PROCESSING_DATE := L_STARTTIME;
        
          SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
        
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                                           L_STARTTIME),
                                                                   12,
                                                                   12);
          P_STG_PO_HEADER.INTERFACE_FLAG := 'E';
      END;
    
      ------------------------
      -- Validate Supplier
      BEGIN
        SELECT DISTINCT 'Y'
          INTO L_CHECK
          FROM PO_VENDORS
         WHERE SEGMENT1 = P_STG_PO_HEADER.VENDOR_NUM;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          -- g_po_line_indx :=g_po_line_indx-1;
          G_LOG_DETAIL_SEQ := G_LOG_DETAIL_SEQ + 1;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).LEVEL_TYPE := 'D';
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_TYPE := 'PO Interface';
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).AP_INVOICE := NULL; --I_FETCH_PO_LINE.INVOICE_NUM; /*remove code on 17 Sep 2017 fixed case 1 pr multiple invoice*/
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).AP_INVOIE_LINE := NULL; --I_FETCH_PO_LINE.INVOICE_LINE; /*remove code on 17 Sep 2017 fixed case 1 pr multiple invoice*/
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PR_NUMBER := I_FETCH_PO_LINE.PR_NUMBER;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PR_LINE_NUMBER := I_FETCH_PO_LINE.LINE_NUM;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).MATCH_OPTION := I_FETCH_PO_LINE.MATCH_OPTION;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).VENDOR_NUMBER := P_STG_PO_HEADER.VENDOR_NUM;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).VENDOR_SITE_NAME := P_STG_PO_HEADER.VENDOR_SITE_CODE;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PAYMENT_TERM := I_FETCH_PO_LINE.PAYMENT_TERMS;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_CODE := 'INVHUB002-001';
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_MESSAGE := 'Invalid supplier';
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PROCESSING_DATE := L_STARTTIME;
        
          SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
        
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                                           L_STARTTIME),
                                                                   12,
                                                                   12);
          P_STG_PO_HEADER.INTERFACE_FLAG := 'E';
      END;
    
      ------------------------
      -- Validate Supplier site
      BEGIN
        SELECT DISTINCT 'Y'
          INTO L_CHECK
          FROM PO_VENDORS PV, PO_VENDOR_SITES_ALL PVS
         WHERE PV.VENDOR_ID = PVS.VENDOR_ID
           AND PVS.ORG_ID = P_STG_PO_HEADER.ORG_ID
           AND PVS.VENDOR_SITE_CODE = P_STG_PO_HEADER.VENDOR_SITE_CODE
           AND PV.SEGMENT1 = P_STG_PO_HEADER.VENDOR_NUM;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          -- g_po_line_indx :=g_po_line_indx-1;
          G_LOG_DETAIL_SEQ := G_LOG_DETAIL_SEQ + 1;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).LEVEL_TYPE := 'D';
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_TYPE := 'PO Interface';
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).AP_INVOICE := NULL;--I_FETCH_PO_LINE.INVOICE_NUM; /*remove code on 17 Sep 2017 fixed case 1 pr multiple invoice*/
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).AP_INVOIE_LINE := NULL; -- I_FETCH_PO_LINE.INVOICE_LINE; /*remove code on 17 Sep 2017 fixed case 1 pr multiple invoice*/
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PR_NUMBER := I_FETCH_PO_LINE.PR_NUMBER;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PR_LINE_NUMBER := I_FETCH_PO_LINE.LINE_NUM;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).MATCH_OPTION := I_FETCH_PO_LINE.MATCH_OPTION;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).VENDOR_NUMBER := P_STG_PO_HEADER.VENDOR_NUM;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).VENDOR_SITE_NAME := P_STG_PO_HEADER.VENDOR_SITE_CODE;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PAYMENT_TERM := I_FETCH_PO_LINE.PAYMENT_TERMS;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_CODE := 'INVHUB002-002';
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_MESSAGE := 'Invalid Supplier Site';
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PROCESSING_DATE := L_STARTTIME;
        
          SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
        
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                                           L_STARTTIME),
                                                                   12,
                                                                   12);
        
          P_STG_PO_HEADER.INTERFACE_FLAG := 'E';
      END;
    
      ------------------------
      -- Validate qty
      G_STEP := 'Validate po line qty';
      DEBUG_MSG('LOG', G_STEP);
    
      BEGIN
        IF I_FETCH_PO_LINE.LINE_AMOUNT <= 0 THEN
          --joe
          -- g_po_line_indx :=g_po_line_indx-1;
          G_LOG_DETAIL_SEQ := G_LOG_DETAIL_SEQ + 1;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).LEVEL_TYPE := 'D';
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_TYPE := 'PO Interface';
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).AP_INVOICE := NULL; --I_FETCH_PO_LINE.INVOICE_NUM; /*remove code on 17 Sep 2017 fixed case 1 pr multiple invoice*/
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).AP_INVOIE_LINE := NULL; --I_FETCH_PO_LINE.INVOICE_LINE; /*remove code on 17 Sep 2017 fixed case 1 pr multiple invoice*/
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PR_NUMBER := I_FETCH_PO_LINE.PR_NUMBER;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PR_LINE_NUMBER := I_FETCH_PO_LINE.LINE_NUM;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).MATCH_OPTION := I_FETCH_PO_LINE.MATCH_OPTION;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).VENDOR_NUMBER := P_STG_PO_HEADER.VENDOR_NUM;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).VENDOR_SITE_NAME := P_STG_PO_HEADER.VENDOR_SITE_CODE;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PAYMENT_TERM := I_FETCH_PO_LINE.PAYMENT_TERMS;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_CODE := 'INVHUB002-014';
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_MESSAGE := 'The Quantity must be greater than 0';
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PROCESSING_DATE := L_STARTTIME;
        
          SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
        
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                                           L_STARTTIME),
                                                                   12,
                                                                   12);
        
          P_STG_PO_HEADER.INTERFACE_FLAG := 'E';
        END IF;
      END;
    
      G_STEP := 'FETCH_PO_LINE - get pr information';
      DEBUG_MSG('LOG', G_STEP);
    
      -- get information from pr_line
      BEGIN
        G_STEP := 'FETCH_PO_LINE - get pr information';
        DEBUG_MSG('LOG', G_STEP);
        G_PO_LINE_INDX := G_PO_LINE_INDX + 1;
      
        SELECT PO_LINES_INTERFACE_S.NEXTVAL,
               PRL.ITEM_ID,
               PRL.CATEGORY_ID,
               PRL.ITEM_DESCRIPTION,
               MUO.UOM_CODE, --prl.unit_meas_lookup_code,
               PRL.QUANTITY, --(i_fetch_po_line.line_amount/nvl(prl.currency_unit_price,prl.unit_price)) quantity,--prl.quantity,
               NVL(PRL.CURRENCY_UNIT_PRICE, PRL.UNIT_PRICE),
               PRL.DESTINATION_ORGANIZATION_ID,
               PRL.DELIVER_TO_LOCATION_ID,
               PRL.REQUISITION_LINE_ID,
               PRL.TAX_CODE_ID,
               -- prl.justification,
               PRL.NOTE_TO_AGENT,
               PRL.NOTE_TO_RECEIVER,
               PRL.NEED_BY_DATE --,
        -- suggested_vendor_contact,sugg
          INTO P_STG_PO_LINE(G_PO_LINE_INDX).INTERFACE_LINE_ID,
               P_STG_PO_LINE(G_PO_LINE_INDX).ITEM_ID,
               P_STG_PO_LINE(G_PO_LINE_INDX).CATEGORY_ID,
               P_STG_PO_LINE(G_PO_LINE_INDX).ITEM_DESCRIPTION,
               P_STG_PO_LINE(G_PO_LINE_INDX).UOM_CODE,
               P_STG_PO_LINE(G_PO_LINE_INDX).QUANTITY,
               P_STG_PO_LINE(G_PO_LINE_INDX).UNIT_PRICE,
               P_STG_PO_LINE(G_PO_LINE_INDX).SHIP_TO_ORGANIZATION_CODE,
               P_STG_PO_LINE(G_PO_LINE_INDX).SHIP_TO_LOCATION,
               P_STG_PO_LINE(G_PO_LINE_INDX).REQUISITION_LINE_ID,
               P_STG_PO_LINE(G_PO_LINE_INDX).TAX_CODE_ID,
               P_STG_PO_LINE(G_PO_LINE_INDX).NOTE_TO_VENDOR,
               P_STG_PO_LINE(G_PO_LINE_INDX).NOTE_TO_RECEIVER,
               P_STG_PO_LINE(G_PO_LINE_INDX).NEED_BY_DATE
          FROM PO_REQUISITION_HEADERS_ALL PRH,
               PO_REQUISITION_LINES_ALL   PRL,
               MTL_UNITS_OF_MEASURE_VL    MUO
         WHERE PRH.REQUISITION_HEADER_ID = PRL.REQUISITION_HEADER_ID
           AND PRH.SEGMENT1 = I_FETCH_PO_LINE.PR_NUMBER
           AND PRL.LINE_NUM = I_FETCH_PO_LINE.LINE_NUM
           AND PRL.UNIT_MEAS_LOOKUP_CODE = MUO.UNIT_OF_MEASURE;
      
        L_PO_LINENUM := L_PO_LINENUM + 1;
        P_STG_PO_LINE(G_PO_LINE_INDX).INTERFACE_HEADER_ID := P_STG_PO_HEADER.INTERFACE_HEADER_ID;
        P_STG_PO_LINE(G_PO_LINE_INDX).PR_NUMBER := I_FETCH_PO_LINE.PR_NUMBER;
        P_STG_PO_LINE(G_PO_LINE_INDX).LINE_NUM := I_FETCH_PO_LINE.LINE_NUM;
        P_STG_PO_LINE(G_PO_LINE_INDX).LINE_TYPE := I_FETCH_PO_LINE.LINE_TYPE;
        P_STG_PO_LINE(G_PO_LINE_INDX).PAYMENT_TERMS := I_FETCH_PO_LINE.PAYMENT_TERMS;
        P_STG_PO_LINE(G_PO_LINE_INDX).MATCH_OPTION := SUBSTR(I_FETCH_PO_LINE.MATCH_OPTION,
                                                             1,
                                                             1);
        P_STG_PO_LINE(G_PO_LINE_INDX).INTERFACE_FLAG := I_FETCH_PO_LINE.INTERFACE_FLAG;
      
        FETCH_PO_DISTRIBUTION(P_STG_PO_HEADER,
                              P_STG_PO_LINE(G_PO_LINE_INDX),
                              P_STG_PO_DIST);
        P_STG_PO_LINE(G_PO_LINE_INDX).LINE_NUM := L_PO_LINENUM; --i_fetch_po_line.line_num;
      
        IF P_STG_PO_HEADER.INTERFACE_FLAG != 'E' THEN
          BEGIN
            --g_po_line_indx :=g_po_line_indx-1;
            /*g_log_detail_seq :=g_log_detail_seq+1;
            g_log_detail(g_log_detail_seq).level_type :='D';
            g_log_detail(g_log_detail_seq).request_id := fnd_global.CONC_REQUEST_ID;
            g_log_detail(g_log_detail_seq).error_type := 'PO Interface';
            g_log_detail(g_log_detail_seq).ap_invoice := i_fetch_po_line.invoice_num;
            g_log_detail(g_log_detail_seq).ap_invoie_line := i_fetch_po_line.invoice_line;
            g_log_detail(g_log_detail_seq).pr_number := i_fetch_po_line.pr_number;
            g_log_detail(g_log_detail_seq).pr_line_number :=i_fetch_po_line.line_num;
            g_log_detail(g_log_detail_seq).match_option := i_fetch_po_line.match_option;
            g_log_detail(g_log_detail_seq).vendor_number := p_stg_po_header.vendor_num;
            g_log_detail(g_log_detail_seq).vendor_site_name := p_stg_po_header.vendor_site_code;
            g_log_detail(g_log_detail_seq).payment_term := i_fetch_po_line.payment_terms;
            g_log_detail(g_log_detail_seq).ERROR_CODE := NULL;
            g_log_detail(g_log_detail_seq).error_message := NULL;
            g_log_detail(g_log_detail_seq).processing_date := l_starttime;
            
            SELECT current_timestamp
            INTO l_endtime
            FROM dual;
            g_log_detail(g_log_detail_seq).processing_time := substr(to_char(l_endtime-l_starttime),12,12);
            */
          
            BEGIN
              UPDATE TAC_INVHUB_INVOICE_STG TIS
                 SET INTERFACE_PO_FLAG = 'P'
               WHERE TIS.GROUP_NAME = P_STG_PO_HEADER.GROUP_NAME
                 AND NVL(TIS.INTERFACE_PO_FLAG, 'N') = 'N'
                 AND TIS.PR_NUMBER = I_FETCH_PO_LINE.PR_NUMBER
                 AND TIS.PR_LINE_NUMBER = I_FETCH_PO_LINE.LINE_NUM;
                -- AND TIS.INVOICE_NUM = I_FETCH_PO_LINE.INVOICE_NUM; /*remove code on 17 Sep 2017 fixed case 1 pr multiple invoice*/
            END;
          END;
        END IF;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          G_PO_LINE_INDX := G_PO_LINE_INDX - 1;
          G_LOG_DETAIL_SEQ := G_LOG_DETAIL_SEQ + 1;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).LEVEL_TYPE := 'D';
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_TYPE := 'PO Interface';
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).AP_INVOICE := NULL; --I_FETCH_PO_LINE.INVOICE_NUM; /*remove code on 17 Sep 2017 fixed case 1 pr multiple invoice*/
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).AP_INVOIE_LINE := NULL; --I_FETCH_PO_LINE.INVOICE_LINE; /*remove code on 17 Sep 2017 fixed case 1 pr multiple invoice*/
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PR_NUMBER := I_FETCH_PO_LINE.PR_NUMBER;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PR_LINE_NUMBER := I_FETCH_PO_LINE.LINE_NUM;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).MATCH_OPTION := I_FETCH_PO_LINE.MATCH_OPTION;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).VENDOR_NUMBER := P_STG_PO_HEADER.VENDOR_NUM;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).VENDOR_SITE_NAME := P_STG_PO_HEADER.VENDOR_SITE_CODE;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PAYMENT_TERM := I_FETCH_PO_LINE.PAYMENT_TERMS;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_CODE := 'INVHUB002-008';
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_MESSAGE := 'Invalid PR Number';
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PROCESSING_DATE := L_STARTTIME;
        
          SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
        
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                                           L_STARTTIME),
                                                                   12,
                                                                   12);
        
          P_STG_PO_HEADER.INTERFACE_FLAG := 'E';
      END;
    END LOOP;
  END;

  PROCEDURE INSERT_PO_HEADER_INT(STG_PO_HEADER     IN OUT T_PO_HEADER,
                                 P_CONC_REQUEST_ID IN NUMBER) IS
    L_VENDOR_CONTACT NUMBER;
  BEGIN
    G_STEP := 'INSERT_PO_HEADER_INT : ' || STG_PO_HEADER.COUNT;
    DEBUG_MSG('LOG', G_STEP);
  
    IF STG_PO_HEADER.COUNT > 0 THEN
      FOR I IN 1 .. STG_PO_HEADER.COUNT LOOP
        BEGIN
          DEBUG_MSG('LOG',
                    'stg_po_header(i).interface_flag=' || STG_PO_HEADER(I)
                    .INTERFACE_FLAG);
        
          IF STG_PO_HEADER(I).INTERFACE_FLAG = 'N' THEN
            L_VENDOR_CONTACT := F_GET_VENDOR_CONTACT(STG_PO_HEADER(I)
                                                     .PR_NUMBER);
          
            INSERT INTO PO_HEADERS_INTERFACE
              (INTERFACE_HEADER_ID,
               BATCH_ID,
               INTERFACE_SOURCE_CODE,
               ---process_code,
               ACTION,
               -- group_code,
               ORG_ID,
               DOCUMENT_TYPE_CODE,
               CURRENCY_CODE,
               AGENT_ID,
               VENDOR_NUM,
               VENDOR_SITE_CODE,
               APPROVAL_STATUS,
               VENDOR_CONTACT_ID,
               RATE_TYPE,
               RATE_DATE,
               RATE)
            VALUES
              (STG_PO_HEADER(I).INTERFACE_HEADER_ID,
               P_CONC_REQUEST_ID,
               --stg_po_header(i).batch_id,
               STG_PO_HEADER(I).SOURCE,
               --stg_po_header(i).process_code,
               --stg_po_header(i).action,
               'ORIGINAL',
               --stg_po_header(i).group_code,
               STG_PO_HEADER   (I).ORG_ID,
               'STANDARD',
               STG_PO_HEADER   (I).INVOICE_CURRENCY_CODE,
               STG_PO_HEADER   (I).AGENT_ID,
               STG_PO_HEADER   (I).VENDOR_NUM,
               STG_PO_HEADER   (I).VENDOR_SITE_CODE,
               'APPROVED',
               L_VENDOR_CONTACT,
               STG_PO_HEADER   (I).RATE_TYPE,
               STG_PO_HEADER   (I).RATE_DATE,
               STG_PO_HEADER   (I).RATE); --stg_po_header(i).approval_status);
          
            STG_PO_HEADER(I).INTERFACE_FLAG := 'Y';
          ELSE
            G_STEP := 'in update error to tac_invhub_invoice_stg';
            DEBUG_MSG('LOG', G_STEP);
            DEBUG_MSG('LOG',
                      'stg_po_header(i).group_name=' || STG_PO_HEADER(I)
                      .GROUP_NAME);
            DEBUG_MSG('LOG',
                      'stg_po_header(i).pr_number=' || STG_PO_HEADER(I)
                      .PR_NUMBER);
            DEBUG_MSG('LOG',
                      'stg_po_header(i).source=' || STG_PO_HEADER(I).SOURCE);
          
            UPDATE TAC_INVHUB_INVOICE_STG A
               SET A.INTERFACE_PO_FLAG = 'E'
             WHERE A.GROUP_NAME = STG_PO_HEADER(I).GROUP_NAME
               AND A.PR_NUMBER = STG_PO_HEADER(I).PR_NUMBER
               AND A.SOURCE = STG_PO_HEADER(I).SOURCE
               AND A.INTERFACE_PO_FLAG = 'N';
          END IF;
        END;
      END LOOP;
    END IF;
  END;

  PROCEDURE INSERT_PO_LINE_INT(STG_PO_LINE IN OUT T_PO_LINE) IS
  BEGIN
    G_STEP := 'INSERT_PO_LINE_INT : ' || STG_PO_LINE.COUNT;
    DEBUG_MSG('LOG', G_STEP);
  
    IF STG_PO_LINE.COUNT > 0 THEN
      FOR I IN 1 .. STG_PO_LINE.COUNT LOOP
        BEGIN
          IF STG_PO_LINE(I).INTERFACE_FLAG = 'N' THEN
            INSERT INTO PO_LINES_INTERFACE
              (INTERFACE_HEADER_ID,
               INTERFACE_LINE_ID,
               LINE_NUM,
               LINE_TYPE,
               ITEM_ID,
               CATEGORY_ID,
               ITEM_DESCRIPTION,
               UOM_CODE,
               QUANTITY,
               UNIT_PRICE,
               PAYMENT_TERMS,
               SHIP_TO_ORGANIZATION_ID,
               SHIP_TO_LOCATION_ID,
               PROMISED_DATE,
               REQUISITION_LINE_ID,
               INSPECTION_REQUIRED_FLAG,
               RECEIPT_REQUIRED_FLAG,
               ACCRUE_ON_RECEIPT_FLAG,
               -- receiving_routing
               -- match_option
               TAX_CODE_ID,
               TAXABLE_FLAG,
               NOTE_TO_VENDOR,
               NOTE_TO_RECEIVER,
               NEED_BY_DATE)
            VALUES
              (STG_PO_LINE(I).INTERFACE_HEADER_ID,
               STG_PO_LINE(I).INTERFACE_LINE_ID,
               STG_PO_LINE(I).LINE_NUM,
               STG_PO_LINE(I).LINE_TYPE,
               STG_PO_LINE(I).ITEM_ID,
               STG_PO_LINE(I).CATEGORY_ID,
               STG_PO_LINE(I).ITEM_DESCRIPTION,
               STG_PO_LINE(I).UOM_CODE,
               STG_PO_LINE(I).QUANTITY,
               STG_PO_LINE(I).UNIT_PRICE,
               STG_PO_LINE(I).PAYMENT_TERMS,
               STG_PO_LINE(I).SHIP_TO_ORGANIZATION_CODE,
               STG_PO_LINE(I).SHIP_TO_LOCATION,
               SYSDATE,
               STG_PO_LINE(I).REQUISITION_LINE_ID,
               'N',
               'N',
               'N', --,
               --'2'
               STG_PO_LINE(I).TAX_CODE_ID,
               DECODE(STG_PO_LINE(I).TAX_CODE_ID, NULL, NULL, 'Y'),
               NULL, --stg_po_line(i).note_to_vendor,
               STG_PO_LINE(I).NOTE_TO_RECEIVER,
               STG_PO_LINE(I).NEED_BY_DATE);
          
            STG_PO_LINE(I).INTERFACE_FLAG := 'Y';
          END IF;
        END;
      END LOOP;
    END IF;
  END;

  PROCEDURE INSERT_PO_DIST_INT(STG_PO_DIST IN OUT T_PO_DIST) IS
    L_PROJECT_ID                  PO_REQ_DISTRIBUTIONS_ALL.PROJECT_ID%TYPE;
    L_PROJECT_NAME                PA_PROJECTS_ALL.SEGMENT1%TYPE;
    L_TASK_ID                     PO_REQ_DISTRIBUTIONS_ALL.TASK_ID%TYPE;
    L_TASK_NAME                   PA_TASKS.TASK_NUMBER%TYPE;
    L_EXPENDITURE_TYPE            PO_REQ_DISTRIBUTIONS_ALL.EXPENDITURE_TYPE%TYPE;
    L_EXPENDITURE_ITEM_DATE       PO_REQ_DISTRIBUTIONS_ALL.EXPENDITURE_ITEM_DATE%TYPE;
    L_EXPENDITURE_ORGANIZATION_ID PO_REQ_DISTRIBUTIONS_ALL.EXPENDITURE_ORGANIZATION_ID%TYPE;
    L_PROJECT_ACCOUNTING_CONTEXT  PO_REQ_DISTRIBUTIONS_ALL.PROJECT_ACCOUNTING_CONTEXT%TYPE;
    L_DELIVER_TO                  NUMBER;
    L_DELIVER_LOCATION            NUMBER;
  BEGIN
    G_STEP := 'INSERT_PO_DIST_INT : ' || STG_PO_DIST.COUNT;
    DEBUG_MSG('LOG', G_STEP);
  
    FOR I IN 1 .. STG_PO_DIST.COUNT LOOP
      DEBUG_MSG('LOG',
                'stg_po_dist(i).interface_header_id =' || STG_PO_DIST(I)
                .INTERFACE_HEADER_ID);
    END LOOP;
  
    IF STG_PO_DIST.COUNT > 0 THEN
      FOR I IN 1 .. STG_PO_DIST.COUNT LOOP
        GET_PROJECT_INFORMATION(STG_PO_DIST                  (I)
                                .REQ_HEADER_REFERENCE_NUM,
                                STG_PO_DIST                  (I)
                                .REQ_LINE_REFERENCE_NUM,
                                L_PROJECT_ID,
                                L_PROJECT_NAME,
                                L_TASK_ID,
                                L_TASK_NAME,
                                L_EXPENDITURE_TYPE,
                                L_EXPENDITURE_ITEM_DATE,
                                L_EXPENDITURE_ORGANIZATION_ID,
                                L_PROJECT_ACCOUNTING_CONTEXT); --[issue50]
      
        GET_REQUESTER_INFO(STG_PO_DIST       (I).REQ_HEADER_REFERENCE_NUM,
                           STG_PO_DIST       (I).REQ_LINE_REFERENCE_NUM,
                           L_DELIVER_TO,
                           L_DELIVER_LOCATION);
      
        INSERT INTO PO_DISTRIBUTIONS_INTERFACE A
          (INTERFACE_HEADER_ID,
           INTERFACE_LINE_ID,
           INTERFACE_DISTRIBUTION_ID,
           DISTRIBUTION_NUM,
           QUANTITY_ORDERED,
           CHARGE_ACCOUNT_ID,
           GL_ENCUMBERED_DATE,
           REQ_DISTRIBUTION_ID,
           REQ_HEADER_REFERENCE_NUM,
           REQ_LINE_REFERENCE_NUM,
           PROJECT_ID,
           PROJECT,
           TASK_ID,
           TASK,
           EXPENDITURE_TYPE,
           EXPENDITURE_ITEM_DATE,
           EXPENDITURE_ORGANIZATION_ID,
           PROJECT_ACCOUNTING_CONTEXT, --[issue50]
           DELIVER_TO_PERSON_ID,
           DELIVER_TO_LOCATION_ID)
        VALUES
          (STG_PO_DIST                  (I).INTERFACE_HEADER_ID,
           STG_PO_DIST                  (I).INTERFACE_LINE_ID,
           STG_PO_DIST                  (I).INTERFACE_DISTRIBUTION_ID,
           STG_PO_DIST                  (I).DISTRIBUTION_NUM,
           STG_PO_DIST                  (I).QUANTITY_ORDERED,
           STG_PO_DIST                  (I).CHARGE_ACCOUNT_ID,
           SYSDATE, --stg_po_dist(i).gl_encumbered_date,
           STG_PO_DIST                  (I).REQ_DISTRIBUTION_ID,
           STG_PO_DIST                  (I).REQ_HEADER_REFERENCE_NUM,
           STG_PO_DIST                  (I).REQ_LINE_REFERENCE_NUM,
           L_PROJECT_ID,
           L_PROJECT_NAME,
           L_TASK_ID,
           L_TASK_NAME,
           L_EXPENDITURE_TYPE,
           L_EXPENDITURE_ITEM_DATE,
           L_EXPENDITURE_ORGANIZATION_ID,
           L_PROJECT_ACCOUNTING_CONTEXT, --[issue50]
           L_DELIVER_TO,
           L_DELIVER_LOCATION);
      END LOOP;
    END IF;
  END;

  FUNCTION GET_DEFAULT_BUYER RETURN NUMBER IS
    L_DEFAULT_BUYER NUMBER;
  BEGIN
    SELECT PAPF.PERSON_ID
      INTO L_DEFAULT_BUYER
      FROM PO_AGENTS            POAGENTEO,
           PER_ALL_PEOPLE_F     PAPF,
           PO_SHIP_TO_LOC_ORG_V PSL,
           MTL_CATEGORIES_KFV   MKFV,
           FND_USER             FU
     WHERE FU.EMPLOYEE_ID = PAPF.PERSON_ID
       AND POAGENTEO.AGENT_ID = PAPF.PERSON_ID
       AND POAGENTEO.CATEGORY_ID = MKFV.CATEGORY_ID(+)
       AND (PAPF.EMPLOYEE_NUMBER IS NOT NULL OR PAPF.NPW_NUMBER IS NOT NULL)
       AND TRUNC(SYSDATE) BETWEEN PAPF.EFFECTIVE_START_DATE AND
           PAPF.EFFECTIVE_END_DATE
       AND DECODE(HR_GENERAL.GET_XBG_PROFILE,
                  'Y',
                  PAPF.BUSINESS_GROUP_ID,
                  HR_GENERAL.GET_BUSINESS_GROUP_ID) =
           PAPF.BUSINESS_GROUP_ID
       AND POAGENTEO.LOCATION_ID = PSL.LOCATION_ID(+)
       AND FU.USER_NAME = C_DEFAULT_BUYER;
  
    RETURN(L_DEFAULT_BUYER);
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN(-1);
  END;

  PROCEDURE VALIDATE_PO_HEADER_INT(P_STG_PO_HEADER  IN OUT R_PO_HEADER,
                                   P_STG_LOG_DETAIL IN OUT T_LOG_DETAIL) IS -- (stg_po_header IN OUT t_po_header) IS
  BEGIN
    G_STEP                              := 'validate_po_header_int';
    P_STG_PO_HEADER.INTERFACE_HEADER_ID := PO_HEADERS_INTERFACE_S.NEXTVAL;
    P_STG_PO_HEADER.ORG_ID              := GET_PR_ORG_ID(P_STG_PO_HEADER.PR_NUMBER);
    P_STG_PO_HEADER.AGENT_ID            := GET_DEFAULT_BUYER;
  
    IF P_STG_PO_HEADER.INVOICE_CURRENCY_CODE != 'THB' THEN
      BEGIN
        SELECT --prl.rate_type,
         PRL.RATE_DATE, PRL.RATE
          INTO --p_stg_po_header.rate_type,
               P_STG_PO_HEADER.RATE_DATE,
               P_STG_PO_HEADER.RATE
          FROM PO_REQUISITION_HEADERS_ALL PRH, PO_REQUISITION_LINES_ALL PRL
         WHERE PRH.SEGMENT1 = P_STG_PO_HEADER.PR_NUMBER
           AND PRH.REQUISITION_HEADER_ID = PRL.REQUISITION_HEADER_ID
           AND PRL.CURRENCY_CODE = P_STG_PO_HEADER.INVOICE_CURRENCY_CODE
           AND PRH.ORG_ID = PRL.ORG_ID
           AND PRH.ORG_ID = P_STG_PO_HEADER.ORG_ID
           AND ROWNUM = 1;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          P_STG_PO_HEADER.RATE_TYPE := NULL;
          P_STG_PO_HEADER.RATE_DATE := NULL;
          P_STG_PO_HEADER.RATE      := NULL;
      END;
    END IF;
  END;

  PROCEDURE UNRESERVE_PR(PR_LINE_ID IN NUMBER) IS
    -- PRAGMA AUTONOMOUS_TRANSACTION;
  
    X_AUTHORIZATION_STATUS PO_REQUISITION_HEADERS_ALL.AUTHORIZATION_STATUS%TYPE;
    X_SUPPL_NOTIFIED_FLAG  PO_REQUISITION_HEADERS_ALL.SUPPLIER_NOTIFIED_FLAG%TYPE;
    X_CONTRACTOR_REQ_FLAG  PO_REQUISITION_HEADERS_ALL.CONTRACTOR_REQUISITION_FLAG%TYPE;
    X_WF_ITEM_KEY          PO_REQUISITION_HEADERS_ALL.WF_ITEM_KEY%TYPE;
    X_WF_ITEM_TYPE         PO_REQUISITION_HEADERS_ALL.WF_ITEM_TYPE%TYPE;
  
    L_RETURN_STATUS  VARCHAR2(100);
    L_PO_RETURN_CODE VARCHAR2(100);
    X_ENC_REPORT_ID  NUMBER;
    L_SUBTYPE        VARCHAR2(100);
    V_SQLCODE        VARCHAR2(1000);
    V_SQLERR         VARCHAR2(3000);
  BEGIN
    G_STEP := 'unreserve_pr line :' || PR_LINE_ID;
    /* fnd_global.apps_initialize (user_id => 1110,
    resp_id => 50265,
    resp_appl_id => 201
    );*/
  
    PO_DOCUMENT_FUNDS_PVT.DO_UNRESERVE(X_RETURN_STATUS     => L_RETURN_STATUS,
                                       P_DOC_TYPE          => 'REQUISITION' --p_document_type--l_doctype
                                      ,
                                       P_DOC_SUBTYPE       => NULL -- 'STANDARD' --null--'PURCHASE'--l_subtype--p_document_subtype--l_subtype
                                      ,
                                       P_DOC_LEVEL         => 'LINE' --l_doc_level
                                      ,
                                       P_DOC_LEVEL_ID      => PR_LINE_ID --561137--l_doc_level_id 561137
                                      ,
                                       P_USE_ENC_GT_FLAG   => 'N',
                                       P_VALIDATE_DOCUMENT => NULL --'Y'
                                      ,
                                       P_OVERRIDE_FUNDS    => 'N' --l_use_override_funds
                                      ,
                                       P_USE_GL_DATE       => 'N',
                                       P_OVERRIDE_DATE     => SYSDATE,
                                       P_EMPLOYEE_ID       => NULL --2656 --1408--264--l_employee_id
                                      ,
                                       X_PO_RETURN_CODE    => L_PO_RETURN_CODE,
                                       X_ONLINE_REPORT_ID  => X_ENC_REPORT_ID);
    -- commit;
    DEBUG_MSG('LOG', G_STEP);
    DEBUG_MSG('LOG', 'online_reports-id' || X_ENC_REPORT_ID);
    DEBUG_MSG('LOG',
              'po_return_code-' || L_PO_RETURN_CODE || '##' || SQLERRM);
    --dbms_output.put_line('status-'||l_return_status);
    --dbms_output.put_line('online_reports-id'|| x_enc_report_id);
    --dbms_output.put_line('po_return_code-'||l_po_return_code||'##'||sqlerrm);
  
  EXCEPTION
    WHEN OTHERS THEN
      V_SQLCODE := SQLCODE;
      V_SQLERR  := SQLERRM;
      DEBUG_MSG('LOG', 'Un_reserve :' || TO_CHAR(V_SQLCODE));
      DEBUG_MSG('LOG', 'Un_reserve error :' || V_SQLERR);
  END;

  FUNCTION GET_PR_DISTRIBUTION_ID(P_PR_NUMBER IN VARCHAR2,
                                  P_PR_LINE   IN NUMBER,
                                  P_ORG       IN NUMBER) RETURN NUMBER IS
    L_DIST_ID NUMBER;
  BEGIN
    SELECT PRD.DISTRIBUTION_ID
      INTO L_DIST_ID
      FROM PO_REQUISITION_HEADERS_ALL PRH,
           PO_REQUISITION_LINES_ALL   PRL,
           PO_REQ_DISTRIBUTIONS_ALL   PRD
     WHERE PRH.REQUISITION_HEADER_ID = PRL.REQUISITION_HEADER_ID
       AND PRH.ORG_ID = PRL.ORG_ID
          -- AND prl.requisition_header_id = prd.requisition_header_id
       AND PRL.REQUISITION_LINE_ID = PRD.REQUISITION_LINE_ID
       AND PRL.ORG_ID = PRD.ORG_ID
       AND PRH.SEGMENT1 = P_PR_NUMBER
       AND PRL.LINE_NUM = P_PR_LINE
       AND PRL.ORG_ID = P_ORG;
  
    RETURN(L_DIST_ID);
  EXCEPTION
    WHEN OTHERS THEN
      RETURN(NULL);
  END;

  PROCEDURE UPDATE_PO_TO_STG(P_PO_IMPORT_ID    IN NUMBER,
                             P_FILENAME        IN VARCHAR2,
                             L_STARTTIME       TIMESTAMP,
                             P_CONC_REQUEST_ID IN NUMBER) IS
    CURSOR C1(P_PO_IMPORT_ID IN NUMBER) IS
      SELECT POH.SEGMENT1                PO_NUMBER,
             POL.LINE_NUM                LINE_NUMBER,
             PD.REQ_HEADER_REFERENCE_NUM,
             PD.REQ_LINE_REFERENCE_NUM,
             POH.PO_HEADER_ID,
             POL.PO_LINE_ID,
             PLL.LINE_LOCATION_ID,
             POH.ORG_ID
        FROM PO_HEADERS_ALL        POH,
             PO_LINES_ALL          POL,
             PO_LINE_LOCATIONS_ALL PLL,
             PO_DISTRIBUTIONS_ALL  PD
       WHERE POH.PO_HEADER_ID = POL.PO_HEADER_ID
         AND POL.PO_HEADER_ID = PLL.PO_HEADER_ID
         AND POL.PO_LINE_ID = PLL.PO_LINE_ID
         AND PLL.PO_HEADER_ID = PD.PO_HEADER_ID
         AND PLL.PO_LINE_ID = PD.PO_LINE_ID
         AND PLL.LINE_LOCATION_ID = PD.LINE_LOCATION_ID
         AND POH.REQUEST_ID = P_PO_IMPORT_ID;
  
    CURSOR C_COMPLETE_PO(P_FILENAME    IN VARCHAR2,
                         P_REF_PR_NUM  IN VARCHAR2,
                         P_REF_PR_LINE IN NUMBER) IS
      SELECT INVOICE_NUM,
             LINE_NUMBER      INVOICE_LINE,
             PR_NUMBER,
             PR_LINE_NUMBER   LINE_NUM,
             MATCH_OPTION,
             VENDOR_NUM,
             VENDOR_SITE_CODE,
             TERMS_NAME
        FROM TAC_INVHUB_INVOICE_STG
       WHERE FILE_NAME = P_FILENAME
         AND INTERFACE_TYPE = '2WAY'
         AND INTERFACE_PO_FLAG = 'Y'
         AND PR_NUMBER = P_REF_PR_NUM
         AND PR_LINE_NUMBER = P_REF_PR_LINE;
  
    L_COUNT                       NUMBER;
    L_PROJECT_ID                  PO_REQ_DISTRIBUTIONS_ALL.PROJECT_ID%TYPE;
    L_PROJECT_NAME                PA_PROJECTS_ALL.SEGMENT1%TYPE;
    L_TASK_ID                     PO_REQ_DISTRIBUTIONS_ALL.TASK_ID%TYPE;
    L_TASK_NAME                   PA_TASKS.TASK_NUMBER%TYPE;
    L_EXPENDITURE_TYPE            PO_REQ_DISTRIBUTIONS_ALL.EXPENDITURE_TYPE%TYPE;
    L_EXPENDITURE_ITEM_DATE       PO_REQ_DISTRIBUTIONS_ALL.EXPENDITURE_ITEM_DATE%TYPE;
    L_EXPENDITURE_ORGANIZATION_ID PO_REQ_DISTRIBUTIONS_ALL.EXPENDITURE_ORGANIZATION_ID%TYPE;
    L_PROJECT_ACCOUNTING_CONTEXT  PO_REQ_DISTRIBUTIONS_ALL.PROJECT_ACCOUNTING_CONTEXT%TYPE;
    L_ENDTIME                     TIMESTAMP;
    L_REQ_HEADER                  NUMBER;
    L_REQ_LINE                    NUMBER;
    L_REQ_DIST_ID                 NUMBER;
  BEGIN
    G_STEP := 'UPDATE_PO_TO_STG';
    DEBUG_MSG('LOG', 'p_po_import_id :' || P_PO_IMPORT_ID);
    DEBUG_MSG('LOG', 'p_filename :' || P_FILENAME);
  
    BEGIN
      SELECT COUNT(*)
        INTO L_COUNT
        FROM PO_HEADERS_ALL        POH,
             PO_LINES_ALL          POL,
             PO_LINE_LOCATIONS_ALL PLL,
             PO_DISTRIBUTIONS_ALL  PD
       WHERE POH.PO_HEADER_ID = POL.PO_HEADER_ID
         AND POL.PO_HEADER_ID = PLL.PO_HEADER_ID
         AND POL.PO_LINE_ID = PLL.PO_LINE_ID
         AND PLL.PO_HEADER_ID = PD.PO_HEADER_ID
         AND PLL.PO_LINE_ID = PD.PO_LINE_ID
         AND PLL.LINE_LOCATION_ID = PD.LINE_LOCATION_ID
         AND POH.REQUEST_ID = P_PO_IMPORT_ID;
    END;
  
    DEBUG_MSG('LOG', 'l_count :' || L_COUNT);
  
    FOR I IN C1(P_PO_IMPORT_ID) LOOP
      DEBUG_MSG('LOG', 'i.po_number :' || I.PO_NUMBER);
      DEBUG_MSG('LOG', 'i.line_number :' || I.LINE_NUMBER);
      DEBUG_MSG('LOG',
                'i.req_header_reference_num :' ||
                I.REQ_HEADER_REFERENCE_NUM);
      DEBUG_MSG('LOG',
                'i.req_line_reference_num :' || I.REQ_LINE_REFERENCE_NUM);
    
      BEGIN
        SELECT PRH.REQUISITION_HEADER_ID, PRL.REQUISITION_LINE_ID
          INTO L_REQ_HEADER, L_REQ_LINE
          FROM PO_REQUISITION_HEADERS_ALL PRH, PO_REQUISITION_LINES_ALL PRL
         WHERE PRH.REQUISITION_HEADER_ID = PRL.REQUISITION_HEADER_ID
           AND PRH.ORG_ID = PRL.ORG_ID
           AND PRH.ORG_ID = I.ORG_ID
           AND PRH.SEGMENT1 = I.REQ_HEADER_REFERENCE_NUM
           AND PRL.LINE_NUM = I.REQ_LINE_REFERENCE_NUM;
      EXCEPTION
        WHEN OTHERS THEN
          L_REQ_HEADER := NULL;
          L_REQ_LINE   := NULL;
      END;
    
      IF L_REQ_HEADER IS NOT NULL AND L_REQ_LINE IS NOT NULL THEN
        BEGIN
          UNRESERVE_PR(L_REQ_LINE);
        
          UPDATE PO_REQUISITION_LINES_ALL
             SET LINE_LOCATION_ID = I.LINE_LOCATION_ID
           WHERE REQUISITION_HEADER_ID = L_REQ_HEADER
             AND REQUISITION_LINE_ID = L_REQ_LINE
             AND ORG_ID = I.ORG_ID;
        END;
      END IF;
    
      BEGIN
        UPDATE TAC_INVHUB_INVOICE_STG
           SET PO_NUMBER         = I.PO_NUMBER,
               PO_LINE_NUMBER    = I.LINE_NUMBER,
               INTERFACE_PO_FLAG = 'Y'
         WHERE FILE_NAME = P_FILENAME
           AND INTERFACE_TYPE = '2WAY'
           AND INTERFACE_PO_FLAG IN ('P', 'N')
           AND PR_NUMBER = I.REQ_HEADER_REFERENCE_NUM
           AND PR_LINE_NUMBER = I.REQ_LINE_REFERENCE_NUM;
      END;
    
      ---- Update log status for log monitor -----------------------
      FOR C_GET_PR_LOG IN C_COMPLETE_PO(P_FILENAME,
                                        I.REQ_HEADER_REFERENCE_NUM,
                                        I.REQ_LINE_REFERENCE_NUM) LOOP
        G_LOG_DETAIL_SEQ := G_LOG_DETAIL_SEQ + 1;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).LEVEL_TYPE := 'D';
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).REQUEST_ID := P_CONC_REQUEST_ID;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_TYPE := 'PO Interface';
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).AP_INVOICE := C_GET_PR_LOG.INVOICE_NUM;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).AP_INVOIE_LINE := C_GET_PR_LOG.INVOICE_LINE;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PR_NUMBER := C_GET_PR_LOG.PR_NUMBER;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PR_LINE_NUMBER := C_GET_PR_LOG.LINE_NUM;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).MATCH_OPTION := C_GET_PR_LOG.MATCH_OPTION;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).VENDOR_NUMBER := C_GET_PR_LOG.VENDOR_NUM;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).VENDOR_SITE_NAME := C_GET_PR_LOG.VENDOR_SITE_CODE;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PAYMENT_TERM := C_GET_PR_LOG.TERMS_NAME;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_CODE := NULL;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_MESSAGE := NULL;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PROCESSING_DATE := L_STARTTIME;
      
        SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
      
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                                         L_STARTTIME),
                                                                 12,
                                                                 12);
      END LOOP; -------------------------
    
      UPDATE PO_LINE_LOCATIONS_ALL
         SET MATCH_OPTION = 'P'
       WHERE PO_HEADER_ID = I.PO_HEADER_ID
         AND PO_LINE_ID = I.PO_LINE_ID
         AND LINE_LOCATION_ID = I.LINE_LOCATION_ID;
    
      GET_PROJECT_INFORMATION(I.REQ_HEADER_REFERENCE_NUM,
                              I.REQ_LINE_REFERENCE_NUM,
                              L_PROJECT_ID,
                              L_PROJECT_NAME,
                              L_TASK_ID,
                              L_TASK_NAME,
                              L_EXPENDITURE_TYPE,
                              L_EXPENDITURE_ITEM_DATE,
                              L_EXPENDITURE_ORGANIZATION_ID,
                              L_PROJECT_ACCOUNTING_CONTEXT);
    
      L_REQ_DIST_ID := GET_PR_DISTRIBUTION_ID(P_PR_NUMBER => I.REQ_HEADER_REFERENCE_NUM,
                                              P_PR_LINE   => I.REQ_LINE_REFERENCE_NUM,
                                              P_ORG       => I.ORG_ID);
    
      UPDATE PO_DISTRIBUTIONS_ALL
         SET PROJECT_ID                  = L_PROJECT_ID,
             TASK_ID                     = L_TASK_ID,
             EXPENDITURE_TYPE            = L_EXPENDITURE_TYPE,
             EXPENDITURE_ORGANIZATION_ID = L_EXPENDITURE_ORGANIZATION_ID,
             EXPENDITURE_ITEM_DATE       = L_EXPENDITURE_ITEM_DATE,
             REQ_DISTRIBUTION_ID         = L_REQ_DIST_ID,
             PROJECT_ACCOUNTING_CONTEXT  = L_PROJECT_ACCOUNTING_CONTEXT
       WHERE PO_HEADER_ID = I.PO_HEADER_ID
         AND PO_LINE_ID = I.PO_LINE_ID
         AND LINE_LOCATION_ID = I.LINE_LOCATION_ID;
    END LOOP;
  
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END;

  FUNCTION CALL_STANDARD_PO_INTERFACE(P_CONC_REQUEST_ID IN NUMBER,
                                      P_ORG_CODE        IN VARCHAR2)
    RETURN NUMBER IS
    L_POIMPORT_REQ_ID NUMBER;
    L_FINISHED        BOOLEAN;
    L_PHASE           VARCHAR2(100);
    L_STATUS          VARCHAR2(100);
    L_DEV_PHASE       VARCHAR2(100);
    L_DEV_STATUS      VARCHAR2(100);
    L_MESSAGE         VARCHAR2(100);
    L_CHECK_DATA      VARCHAR2(1);
    L_RESP_NAME       VARCHAR2(50);
    L_RESP_ID         NUMBER;
    L_RESP_APP_ID     NUMBER;
  BEGIN
    -- fnd_request.set_org_id(102);
    --submit po import
    G_STEP := 'CALL_STANDARD_PO_INTERFACE';
    DEBUG_MSG('LOG', G_STEP);
  
    IF P_ORG_CODE = 'DTAC' THEN
      L_RESP_NAME := 'PO-HO';
    ELSIF P_ORG_CODE = 'DTN' THEN
      L_RESP_NAME := 'DTN_PO';
    ELSIF P_ORG_CODE = 'PSB' THEN
      L_RESP_NAME := 'PB_PO';
    END IF;
  
    BEGIN
      SELECT FRESP.RESPONSIBILITY_ID, FRESP.APPLICATION_ID
        INTO L_RESP_ID, L_RESP_APP_ID
        FROM FND_USER FND, FND_RESPONSIBILITY_TL FRESP
       WHERE FND.USER_ID = FND_GLOBAL.USER_ID
         AND FRESP.RESPONSIBILITY_NAME = L_RESP_NAME;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        L_RESP_ID     := NULL;
        L_RESP_APP_ID := NULL;
    END;
  
    IF L_RESP_ID IS NOT NULL AND L_RESP_APP_ID IS NOT NULL THEN
      FND_GLOBAL.APPS_INITIALIZE(USER_ID      => FND_GLOBAL.USER_ID,
                                 RESP_ID      => L_RESP_ID,
                                 RESP_APPL_ID => L_RESP_APP_ID);
    END IF;
  
    L_POIMPORT_REQ_ID := FND_REQUEST.SUBMIT_REQUEST(APPLICATION => 'PO',
                                                    PROGRAM     => 'POXPOPDOI',
                                                    SUB_REQUEST => FALSE,
                                                    ARGUMENT1   => NULL,
                                                    ARGUMENT2   => 'STANDARD',
                                                    ARGUMENT3   => NULL,
                                                    ARGUMENT4   => 'Y',
                                                    ARGUMENT5   => NULL,
                                                    ARGUMENT6   => 'APPROVED',
                                                    ARGUMENT7   => NULL,
                                                    ARGUMENT8   => P_CONC_REQUEST_ID,
                                                    ARGUMENT9   => NULL,
                                                    ARGUMENT10  => NULL);
    COMMIT;
  
    DEBUG_MSG('LOG', 'Import request id=' || L_POIMPORT_REQ_ID);
  
    --Waiting until Requisition Import complete.
    LOOP
      L_FINISHED := FND_CONCURRENT.WAIT_FOR_REQUEST(REQUEST_ID => L_POIMPORT_REQ_ID,
                                                    INTERVAL   => 5,
                                                    MAX_WAIT   => 0,
                                                    PHASE      => L_PHASE,
                                                    STATUS     => L_STATUS,
                                                    DEV_PHASE  => L_DEV_PHASE,
                                                    DEV_STATUS => L_DEV_STATUS,
                                                    MESSAGE    => L_MESSAGE);
    
      EXIT WHEN L_PHASE = 'Completed';
      DBMS_LOCK.SLEEP(10);
    END LOOP;
  
    DEBUG_MSG('LOG', 'l_dev_phase =' || L_DEV_PHASE);
    DEBUG_MSG('LOG', 'l_status =' || L_STATUS);
    DEBUG_MSG('LOG', 'l_dev_status =' || L_DEV_STATUS);
    --COMMIT;
    RETURN(L_POIMPORT_REQ_ID);
  END;

  PROCEDURE VALIDATE_DUPLICATE_PR(P_FILE_NAME IN VARCHAR2) IS
    CURSOR C_PR(P_FILE_NAME IN VARCHAR2) IS
      SELECT DISTINCT A.PR_NUMBER,
                      PR_LINE_NUMBER,
                      DECODE(A.ORG_CODE,
                             'DTAC',
                             102,
                             'DTN',
                             142,
                             'PSB',
                             218) ORG_ID
        FROM TAC_INVHUB_INVOICE_STG A
       WHERE FILE_NAME = P_FILE_NAME
         AND INTERFACE_PO_FLAG = 'N'
         AND INTERFACE_AP_FLAG = 'N'
       ORDER BY 1, 2;
  
    CURSOR C1(P_FILE_NAME IN VARCHAR2,
              P_PR_NUMBER IN VARCHAR2,
              P_PR_LINE   IN NUMBER) IS
      SELECT A.INTERFACE_PO_FLAG,
             A.PR_NUMBER,
             PR_LINE_NUMBER,
             INVOICE_NUM,
             LINE_NUMBER,
             LINE_AMOUNT,
             FILE_NAME,
             APPS.TAC_PO_INVOICEHUB_PKG.F_INVHUB_GET_PR_AMOUNT(PR_NUMBER,
                                                               PR_LINE_NUMBER) PR_AMT,
             MATCH_OPTION,
             VENDOR_NUM,
             VENDOR_SITE_CODE,
             TERMS_NAME,
             DECODE(A.ORG_CODE, 'DTAC', 102, 'DTN', 142, 'PSB', 218) ORG_ID
        FROM TAC_INVHUB_INVOICE_STG A
       WHERE FILE_NAME = P_FILE_NAME
         AND A.PR_NUMBER = P_PR_NUMBER
         AND A.PR_LINE_NUMBER = P_PR_LINE
         AND INTERFACE_PO_FLAG = 'N'
         AND INTERFACE_AP_FLAG = 'N'
       ORDER BY 2, 3, 4, 5;
  
    L_INV_AMT         NUMBER := 0;
    L_EXISTING_PR_AMT NUMBER := 0;
    JOB_REC           C1%ROWTYPE;
    L_CHECK_PR        VARCHAR2(1);
    L_STARTTIME       TIMESTAMP;
    L_ENDTIME         TIMESTAMP;
  BEGIN
    G_STEP := 'validate_duplicate_pr';
    DEBUG_MSG('LOG', G_STEP);
  
    FOR I_PR IN C_PR(P_FILE_NAME) LOOP
      BEGIN
        DEBUG_MSG('LOG', ' pr_number=' || I_PR.PR_NUMBER);
        DEBUG_MSG('LOG', ' pr_line_number=' || I_PR.PR_LINE_NUMBER);
      
        L_INV_AMT := 0;
        DEBUG_MSG('LOG', ' l_inv_amt=' || L_INV_AMT);
      
        OPEN C1(P_FILE_NAME, I_PR.PR_NUMBER, I_PR.PR_LINE_NUMBER);
      
        LOOP
          FETCH C1
            INTO JOB_REC;
        
          IF C1%NOTFOUND THEN
            EXIT;
          ELSE
            -- check exist pr in system
            BEGIN
              SELECT CURRENT_TIMESTAMP INTO L_STARTTIME FROM DUAL;
            
              SELECT DISTINCT 'Y'
                INTO L_CHECK_PR
                FROM PO_REQUISITION_HEADERS_ALL
               WHERE SEGMENT1 = I_PR.PR_NUMBER
                 AND ORG_ID = I_PR.ORG_ID;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                L_CHECK_PR := 'N';
                G_LOG_DETAIL_SEQ := G_LOG_DETAIL_SEQ + 1;
                G_LOG_DETAIL(G_LOG_DETAIL_SEQ).LEVEL_TYPE := 'D';
                G_LOG_DETAIL(G_LOG_DETAIL_SEQ).REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
                G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_TYPE := 'PO Interface';
                G_LOG_DETAIL(G_LOG_DETAIL_SEQ).AP_INVOICE := JOB_REC.INVOICE_NUM;
              
                G_LOG_DETAIL(G_LOG_DETAIL_SEQ).AP_INVOIE_LINE := JOB_REC.LINE_NUMBER;
                G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PR_NUMBER := JOB_REC.PR_NUMBER;
                G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PR_LINE_NUMBER := JOB_REC.PR_LINE_NUMBER;
                G_LOG_DETAIL(G_LOG_DETAIL_SEQ).MATCH_OPTION := JOB_REC.MATCH_OPTION;
                G_LOG_DETAIL(G_LOG_DETAIL_SEQ).VENDOR_NUMBER := JOB_REC.VENDOR_NUM;
                G_LOG_DETAIL(G_LOG_DETAIL_SEQ).VENDOR_SITE_NAME := JOB_REC.VENDOR_SITE_CODE;
                G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PAYMENT_TERM := JOB_REC.TERMS_NAME;
                G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_CODE := 'INVHUB002-008'; --Error INVHUB002-007: Duplicate PR number
                G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_MESSAGE := 'Invalid PR Number';
              
                SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
              
                G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PROCESSING_DATE := L_STARTTIME;
                G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                                                 L_STARTTIME),
                                                                         12,
                                                                         12);
              
                -------------------------
                UPDATE TAC_INVHUB_INVOICE_STG
                   SET INTERFACE_PO_FLAG = 'E'
                -- ,interface_ap_flag='E'
                 WHERE PR_NUMBER = JOB_REC.PR_NUMBER
                   AND PR_LINE_NUMBER = JOB_REC.PR_LINE_NUMBER
                   AND INVOICE_NUM = JOB_REC.INVOICE_NUM
                   AND LINE_NUMBER = JOB_REC.LINE_NUMBER
                   AND FILE_NAME = JOB_REC.FILE_NAME
                   AND INTERFACE_TYPE = '2WAY';
            END;
          
            -- check duplicate pr in system
            IF L_CHECK_PR = 'Y' THEN
              BEGIN
                L_INV_AMT := L_INV_AMT + JOB_REC.LINE_AMOUNT;
                DEBUG_MSG('LOG',
                          ' job_rec.line_amount=' || JOB_REC.LINE_AMOUNT);
                DEBUG_MSG('LOG', ' l_inv_amt=' || L_INV_AMT);
              
                BEGIN
                  BEGIN
                    SELECT NVL(SUM(NVL(PD.ENCUMBERED_AMOUNT, 0)), 0)
                      INTO L_EXISTING_PR_AMT
                      FROM PO_HEADERS_ALL        POH,
                           PO_LINES_ALL          POL,
                           PO_LINE_LOCATIONS_ALL PLL,
                           PO_DISTRIBUTIONS_ALL  PD
                     WHERE POH.PO_HEADER_ID = POL.PO_HEADER_ID
                       AND POL.PO_HEADER_ID = PLL.PO_HEADER_ID
                       AND POL.PO_LINE_ID = PLL.PO_LINE_ID
                       AND PLL.PO_HEADER_ID = PD.PO_HEADER_ID
                       AND PLL.PO_LINE_ID = PD.PO_LINE_ID
                       AND PLL.LINE_LOCATION_ID = PD.LINE_LOCATION_ID
                       AND PD.REQ_HEADER_REFERENCE_NUM = I_PR.PR_NUMBER
                       AND PD.REQ_LINE_REFERENCE_NUM = I_PR.PR_LINE_NUMBER
                       AND POH.ORG_ID = I_PR.ORG_ID
                       AND NVL(POH.CANCEL_FLAG, 'N') != 'Y';
                  EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                      L_EXISTING_PR_AMT := 0;
                  END;
                
                  -- if for case check in existing system
                  DEBUG_MSG('LOG',
                            ' l_inv_amt=' ||
                            (NVL(L_INV_AMT, 0) + NVL(L_EXISTING_PR_AMT, 0)));
                  DEBUG_MSG('LOG',
                            ' l_existing_pr_amt=' ||
                            NVL(L_EXISTING_PR_AMT, 0));
                
                  IF (ROUND(NVL(L_INV_AMT, 0) + NVL(L_EXISTING_PR_AMT, 0),
                            2) > ROUND(NVL(L_EXISTING_PR_AMT, 0), 2)) AND
                     (L_EXISTING_PR_AMT != 0) THEN
                    DBMS_OUTPUT.PUT_LINE('l_inv_amt >job_rec.pr_amt ');
                  
                    G_LOG_DETAIL_SEQ := G_LOG_DETAIL_SEQ + 1;
                    G_LOG_DETAIL(G_LOG_DETAIL_SEQ).LEVEL_TYPE := 'D';
                    G_LOG_DETAIL(G_LOG_DETAIL_SEQ).REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
                    G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_TYPE := 'PO Interface';
                    G_LOG_DETAIL(G_LOG_DETAIL_SEQ).AP_INVOICE := JOB_REC.INVOICE_NUM;
                  
                    G_LOG_DETAIL(G_LOG_DETAIL_SEQ).AP_INVOIE_LINE := JOB_REC.LINE_NUMBER;
                    G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PR_NUMBER := JOB_REC.PR_NUMBER;
                    G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PR_LINE_NUMBER := JOB_REC.PR_LINE_NUMBER;
                    G_LOG_DETAIL(G_LOG_DETAIL_SEQ).MATCH_OPTION := JOB_REC.MATCH_OPTION;
                    G_LOG_DETAIL(G_LOG_DETAIL_SEQ).VENDOR_NUMBER := JOB_REC.VENDOR_NUM;
                    G_LOG_DETAIL(G_LOG_DETAIL_SEQ).VENDOR_SITE_NAME := JOB_REC.VENDOR_SITE_CODE;
                    G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PAYMENT_TERM := JOB_REC.TERMS_NAME;
                    G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_CODE := 'INVHUB002-007'; --Error INVHUB002-007: Duplicate PR number
                    G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_MESSAGE := 'Duplicate PR number';
                  
                    SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
                  
                    G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PROCESSING_DATE := L_STARTTIME;
                    G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                                                     L_STARTTIME),
                                                                             12,
                                                                             12);
                  
                    -------------------------
                    UPDATE TAC_INVHUB_INVOICE_STG
                       SET INTERFACE_PO_FLAG = 'E'
                    -- ,interface_ap_flag='E'
                     WHERE PR_NUMBER = JOB_REC.PR_NUMBER
                       AND PR_LINE_NUMBER = JOB_REC.PR_LINE_NUMBER
                       AND INVOICE_NUM = JOB_REC.INVOICE_NUM
                       AND LINE_NUMBER = JOB_REC.LINE_NUMBER
                       AND FILE_NAME = JOB_REC.FILE_NAME
                       AND INTERFACE_TYPE = '2WAY';
                  ELSE
                    DBMS_OUTPUT.PUT_LINE('job_rec.line_amount=' ||
                                         JOB_REC.LINE_AMOUNT);
                    DBMS_OUTPUT.PUT_LINE('l_inv_amt=' || L_INV_AMT);
                    DBMS_OUTPUT.PUT_LINE('job_rec.pr_amt=' ||
                                         JOB_REC.PR_AMT);
                  
                    -- check duplicate pr in the same file
                    IF ROUND(L_INV_AMT, 2) > ROUND(JOB_REC.PR_AMT, 2) THEN
                      DBMS_OUTPUT.PUT_LINE('l_inv_amt >job_rec.pr_amt ');
                    
                      G_LOG_DETAIL_SEQ := G_LOG_DETAIL_SEQ + 1;
                      G_LOG_DETAIL(G_LOG_DETAIL_SEQ).LEVEL_TYPE := 'D';
                      G_LOG_DETAIL(G_LOG_DETAIL_SEQ).REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
                      G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_TYPE := 'PO Interface';
                      G_LOG_DETAIL(G_LOG_DETAIL_SEQ).AP_INVOICE := JOB_REC.INVOICE_NUM;
                    
                      G_LOG_DETAIL(G_LOG_DETAIL_SEQ).AP_INVOIE_LINE := JOB_REC.LINE_NUMBER;
                      G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PR_NUMBER := JOB_REC.PR_NUMBER;
                      G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PR_LINE_NUMBER := JOB_REC.PR_LINE_NUMBER;
                      G_LOG_DETAIL(G_LOG_DETAIL_SEQ).MATCH_OPTION := JOB_REC.MATCH_OPTION;
                      G_LOG_DETAIL(G_LOG_DETAIL_SEQ).VENDOR_NUMBER := JOB_REC.VENDOR_NUM;
                      G_LOG_DETAIL(G_LOG_DETAIL_SEQ).VENDOR_SITE_NAME := JOB_REC.VENDOR_SITE_CODE;
                      G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PAYMENT_TERM := JOB_REC.TERMS_NAME;
                      G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_CODE := 'INVHUB002-014'; --Error INVHUB002-007: Duplicate PR number
                      G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_MESSAGE := 'Invoice amount exceed PR amount';
                    
                      SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
                    
                      G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PROCESSING_DATE := L_STARTTIME;
                      G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                                                       L_STARTTIME),
                                                                               12,
                                                                               12);
                    
                      -------------------------
                      UPDATE TAC_INVHUB_INVOICE_STG
                         SET INTERFACE_PO_FLAG = 'E'
                      -- ,interface_ap_flag='E'
                       WHERE PR_NUMBER = JOB_REC.PR_NUMBER
                         AND PR_LINE_NUMBER = JOB_REC.PR_LINE_NUMBER
                         AND INVOICE_NUM = JOB_REC.INVOICE_NUM
                         AND LINE_NUMBER = JOB_REC.LINE_NUMBER
                         AND FILE_NAME = JOB_REC.FILE_NAME
                         AND INTERFACE_TYPE = '2WAY';
                    END IF;
                  END IF;
                END;
              END;
            END IF;
            -- l_inv_amt := l_inv_amt + job_rec.line_amount;
            -- dbms_output.put_line('job_rec.rowid='||job_rec.rowid);
          
          END IF;
        END LOOP;
      
        CLOSE C1;
      END;
    END LOOP;
  
    COMMIT;
  END;

  PROCEDURE WRITE_OUTPUT(P_REQUEST_ID   IN NUMBER,
                         P_PROGRAM_NAME IN VARCHAR2) IS
    L_WRITETXT  VARCHAR2(1000);
    L_STARTTIME TIMESTAMP;
  
    CURSOR C_DETAIL1(P_REQUEST_ID IN NUMBER) IS
      SELECT DISTINCT LEVEL_TYPE || '|' || ERROR_TYPE || '|' || AP_INVOICE || '|' ||
                      AP_INVOIE_LINE || '|' || PO_NUMBER || '|' ||
                      PO_LINE_NUMBER || '|' || RECEIPT_NUMBER || '|' ||
                      RECEIPT_LINE_NUM || '|' || PR_NUMBER || '|' ||
                      PR_LINE_NUMBER || '|' || MATCH_OPTION || '|' ||
                      VENDOR_NUMBER || '|' || PROCESSING_DATE || '|' ||
                      PROCESSING_TIME || '|' ||
                      DECODE(ERROR_CODE, NULL, 'Completed', 'Error') || '|' ||
                      ERROR_CODE || '|' || ERROR_MESSAGE LOG_TXT
        FROM DTINVHUB_LOG_DETAIL
       WHERE REQUEST_ID = P_REQUEST_ID;
  
    CURSOR C_DETAIL2(P_REQUEST_ID IN NUMBER) IS
      SELECT LEVEL_TYPE || '|' || ERROR_TYPE || '|' || AP_INVOICE || '|' ||
             VENDOR_NUMBER || '|' || DUE_DATE || '|' || PAYMENT_STATUS || '|' ||
             PV_NUMBER || '|' || DOCUMENT_NUMBER || '|' ||
             AP_VOUCHER_NUMBER || '|' || PAYGROUP || '|' || PROCESSING_DATE || '|' ||
             PROCESSING_TIME || '|' || 'Completed' LOG_TXT
        FROM DTINVHUB_LOG_DETAIL
       WHERE REQUEST_ID = P_REQUEST_ID;
  
    CURSOR C_DETAIL3(P_REQUEST_ID IN NUMBER) IS
      SELECT LEVEL_TYPE || '|' || ERROR_TYPE || '|' || VENDOR_NUMBER || '|' ||
             VENDOR_NAME || '|' || VENDOR_SITE_NAME || '|' || PAYMENT_TERM || '|' ||
             SYSDATE || '|' || PROCESSING_DATE || '|' || PROCESSING_TIME || '|' ||
             'Completed' LOG_TXT
        FROM DTINVHUB_LOG_DETAIL
       WHERE REQUEST_ID = P_REQUEST_ID;
  BEGIN
    SELECT CURRENT_TIMESTAMP INTO L_STARTTIME FROM DUAL;
  
    BEGIN
      SELECT LEVEL_TYPE || '|' || REQUEST_ID || '|' || PROGRAM_NAME || '|' ||
             PROCESS_DATE || '|' || PROCESSING_TYPE || '|' ||
             PROCESSING_DATE || '|' || RECORD_COUNT_COMPLETION || '|' ||
             RECORD_COUNT_ERROR
        INTO L_WRITETXT
        FROM DTINVHUB_LOG_SUMMARY
       WHERE REQUEST_ID = P_REQUEST_ID;
    
      DEBUG_MSG('OUTPUT', L_WRITETXT);
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        IF P_PROGRAM_NAME IN ('DTINVHUB002', 'DTINVHUB003') THEN
          L_WRITETXT := 'H' || '|' || P_REQUEST_ID || '|' || P_PROGRAM_NAME || '|' ||
                        L_STARTTIME || '|' || 'Download' || '|' || 0 || '|' || 0 || '|' || 0;
        ELSE
          L_WRITETXT := 'H' || '|' || P_REQUEST_ID || '|' || P_PROGRAM_NAME || '|' ||
                        L_STARTTIME || '|' || 'Upload' || '|' || 0 || '|' || 0 || '|' || 0;
        END IF;
      
        DEBUG_MSG('OUTPUT', L_WRITETXT);
    END;
  
    IF P_PROGRAM_NAME IN ('DTINVHUB002', 'DTINVHUB003') THEN
      BEGIN
        FOR I IN C_DETAIL1(P_REQUEST_ID) LOOP
          DEBUG_MSG('OUTPUT', I.LOG_TXT);
        END LOOP;
      END;
    ELSIF P_PROGRAM_NAME = 'DTINVHUB004' THEN
      BEGIN
        FOR I IN C_DETAIL2(P_REQUEST_ID) LOOP
          DEBUG_MSG('OUTPUT', I.LOG_TXT);
        END LOOP;
      END;
    ELSIF P_PROGRAM_NAME = 'DTINVHUB005' THEN
      BEGIN
        FOR I IN C_DETAIL3(P_REQUEST_ID) LOOP
          DEBUG_MSG('OUTPUT', I.LOG_TXT);
        END LOOP;
      END;
    ELSE
      NULL;
    END IF;
  END;

  PROCEDURE GET_INTERFACE_ERROR(V_STARTPROCESS    IN TIMESTAMP,
                                P_CONC_REQUEST_ID IN NUMBER,
                                P_GROUP_NAME      IN VARCHAR2) IS
    CURSOR C1(P_CONC_REQUEST_ID IN NUMBER, P_GROUP_NAME IN VARCHAR2) IS
      SELECT PHI.BATCH_ID,
             PDI.REQ_HEADER_REFERENCE_NUM,
             PDI.REQ_LINE_REFERENCE_NUM,
             PIE.ERROR_MESSAGE,
             DECODE(PHI.ORG_ID, 102, 'DTAC', 142, 'DTN', 218, 'PSB') COMP_CODE,
             STG.INVOICE_NUM,
             STG.LINE_NUMBER,
             STG.MATCH_OPTION,
             STG.VENDOR_NUM,
             STG.VENDOR_SITE_CODE,
             STG.TERMS_NAME
        FROM PO_HEADERS_INTERFACE       PHI,
             PO_LINES_INTERFACE         PLI,
             PO_DISTRIBUTIONS_INTERFACE PDI,
             PO_INTERFACE_ERRORS        PIE,
             TAC_INVHUB_INVOICE_STG     STG
       WHERE PHI.INTERFACE_SOURCE_CODE = 'FXTH'
         AND PHI.PROCESS_CODE = 'REJECTED'
         AND PHI.INTERFACE_HEADER_ID = PLI.INTERFACE_HEADER_ID
         AND PLI.INTERFACE_HEADER_ID = PDI.INTERFACE_HEADER_ID
         AND PLI.INTERFACE_LINE_ID = PDI.INTERFACE_LINE_ID
            --AND PHI.INTERFACE_HEADER_ID=537289
         AND PHI.BATCH_ID = P_CONC_REQUEST_ID
         AND PHI.INTERFACE_HEADER_ID = PIE.INTERFACE_HEADER_ID
         AND STG.SOURCE = 'FXTH'
         AND STG.INTERFACE_TYPE = '2WAY'
         AND STG.PR_NUMBER = PDI.REQ_HEADER_REFERENCE_NUM
         AND STG.PR_LINE_NUMBER = PDI.REQ_LINE_REFERENCE_NUM
         AND DECODE(PHI.ORG_ID, 102, 'DTAC', 142, 'DTN', 218, 'PSB') =
             STG.ORG_CODE
         AND STG.GROUP_NAME = P_GROUP_NAME;
  
    L_ENDTIME TIMESTAMP;
  BEGIN
    FOR I IN C1(P_CONC_REQUEST_ID, P_GROUP_NAME) LOOP
      BEGIN
        SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
      
        G_LOG_DETAIL_SEQ := G_LOG_DETAIL_SEQ + 1;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).LEVEL_TYPE := 'D';
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).REQUEST_ID := P_CONC_REQUEST_ID;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_TYPE := 'PO Interface';
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).AP_INVOICE := I.INVOICE_NUM;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).AP_INVOIE_LINE := I.LINE_NUMBER;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PR_NUMBER := I.REQ_HEADER_REFERENCE_NUM;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PR_LINE_NUMBER := I.REQ_LINE_REFERENCE_NUM;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).MATCH_OPTION := I.MATCH_OPTION;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).VENDOR_NUMBER := I.VENDOR_NUM;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).VENDOR_SITE_NAME := I.VENDOR_SITE_CODE;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PAYMENT_TERM := I.TERMS_NAME;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_CODE := 'INVHUB002-099'; --Error INVHUB002-007: Duplicate PR number
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_MESSAGE := I.ERROR_MESSAGE;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PROCESSING_DATE := V_STARTPROCESS;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                                         V_STARTPROCESS),
                                                                 12,
                                                                 12);
      
        --p_stg_po_header.interface_flag :='E';
      
        UPDATE TAC_INVHUB_INVOICE_STG
           SET INTERFACE_PO_FLAG = 'E'
         WHERE GROUP_NAME = P_GROUP_NAME
           AND SOURCE = 'FXTH'
           AND INTERFACE_TYPE = '2WAY'
           AND PR_NUMBER = I.REQ_HEADER_REFERENCE_NUM
           AND PR_LINE_NUMBER = I.REQ_LINE_REFERENCE_NUM;
      END;
    END LOOP;
  END;

  FUNCTION CHECK_PO_REMAINING(P_PO_NUMBER   IN VARCHAR2,
                              P_PO_LINE     IN NUMBER,
                              P_LINE_AMOUNT IN NUMBER) RETURN VARCHAR2 IS
    L_REMAINING NUMBER;
  BEGIN
    --get po_remaining amount
    --if po_remaining_amount > p_line_amount then
    BEGIN
      SELECT SUM(NVL(PD.ENCUMBERED_AMOUNT, 0) -
                 NVL(PD.UNENCUMBERED_AMOUNT, 0) - NVL(PD.AMOUNT_BILLED, 0))
        INTO L_REMAINING
        FROM PO_HEADERS_ALL        POH,
             PO_LINES_ALL          POL,
             PO_LINE_LOCATIONS_ALL PLL,
             PO_DISTRIBUTIONS_ALL  PD
       WHERE POH.SEGMENT1 = P_PO_NUMBER
         AND POL.LINE_NUM = P_PO_LINE
         AND POH.PO_HEADER_ID = POL.PO_HEADER_ID
         AND POH.ORG_ID = POL.ORG_ID
         AND POL.PO_HEADER_ID = PLL.PO_HEADER_ID
         AND POL.PO_LINE_ID = PLL.PO_LINE_ID
         AND POL.ORG_ID = PLL.ORG_ID
         AND PLL.PO_HEADER_ID = PD.PO_HEADER_ID
         AND PLL.PO_LINE_ID = PD.PO_LINE_ID
         AND PLL.LINE_LOCATION_ID = PD.LINE_LOCATION_ID
         AND PLL.ORG_ID = PD.ORG_ID;
    EXCEPTION
      WHEN OTHERS THEN
        L_REMAINING := 0;
    END;
  
    IF L_REMAINING = 0 THEN
      RETURN('FALSE');
    ELSIF (L_REMAINING != 0) AND (L_REMAINING >= P_LINE_AMOUNT) THEN
      RETURN('TRUE');
    ELSE
      RETURN('FALSE');
    END IF;
    -- else return('FALSE');
    -- END IF;
  END;

  PROCEDURE UPDATE_EXISTING_PO(P_FILE_NAME IN VARCHAR2) IS
    CURSOR C_UDPATE_PO(P_FILE_NAME IN VARCHAR2) IS
      SELECT PR_NUMBER, PR_LINE_NUMBER, ORG_CODE, LINE_AMOUNT
        FROM TAC_INVHUB_INVOICE_STG
       WHERE UPPER(FILE_NAME) = UPPER(NVL(P_FILE_NAME, FILE_NAME))
         AND NVL(INTERFACE_PO_FLAG, 'N') = 'N'
       ORDER BY GROUP_NAME, PR_NUMBER, PR_LINE_NUMBER;
  
    CURSOR C_ERROR_PO_REMAINING(P_FILE_NAME IN VARCHAR2) IS
      SELECT INVOICE_NUM,
             LINE_NUMBER,
             PR_NUMBER,
             PR_LINE_NUMBER,
             MATCH_OPTION,
             VENDOR_NUM,
             VENDOR_SITE_CODE,
             TERMS_NAME,
             PO_NUMBER,
             PO_LINE_NUMBER,
             INTERFACE_PO_FLAG
        FROM TAC_INVHUB_INVOICE_STG
       WHERE UPPER(FILE_NAME) = UPPER(NVL(P_FILE_NAME, FILE_NAME))
         AND NVL(INTERFACE_PO_FLAG, 'N') IN ('Y', 'E')
       ORDER BY GROUP_NAME, PR_NUMBER, PR_LINE_NUMBER;
  
    L_PO_NUMBER PO_HEADERS_ALL.SEGMENT1%TYPE;
    L_PO_LINE   PO_LINES_ALL.LINE_NUM%TYPE;
    L_STARTTIME TIMESTAMP;
    L_ENDTIME   TIMESTAMP;
  BEGIN
    NULL;
  
    --joe
    SELECT CURRENT_TIMESTAMP INTO L_STARTTIME FROM DUAL;
  
    FOR I_UPDATE_PO IN C_UDPATE_PO(P_FILE_NAME) LOOP
      BEGIN
        BEGIN
          SELECT --prl.line_location_id,
           POH.SEGMENT1, POL.LINE_NUM
            INTO L_PO_NUMBER, L_PO_LINE
            FROM PO_REQUISITION_HEADERS_ALL PRH,
                 PO_REQUISITION_LINES_ALL   PRL,
                 PO_HEADERS_ALL             POH,
                 PO_LINES_ALL               POL,
                 PO_LINE_LOCATIONS_ALL      PLL
           WHERE PRH.REQUISITION_HEADER_ID = PRL.REQUISITION_HEADER_ID
             AND PRH.ORG_ID = PRL.ORG_ID
             AND PRH.ORG_ID = DECODE(I_UPDATE_PO.ORG_CODE,
                                     'DTAC',
                                     102,
                                     'DTN',
                                     142,
                                     'PSB',
                                     218)
             AND PRH.SEGMENT1 = I_UPDATE_PO.PR_NUMBER
             AND PRL.LINE_NUM = I_UPDATE_PO.PR_LINE_NUMBER
             AND POH.PO_HEADER_ID = POL.PO_HEADER_ID
             AND POH.ORG_ID = POL.ORG_ID
             AND POL.PO_HEADER_ID = PLL.PO_HEADER_ID
             AND POL.PO_LINE_ID = PLL.PO_LINE_ID
             AND POL.ORG_ID = PLL.ORG_ID
             AND PRL.LINE_LOCATION_ID = PLL.LINE_LOCATION_ID;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            L_PO_NUMBER := NULL;
            L_PO_LINE   := NULL;
        END;
      
        IF L_PO_NUMBER IS NOT NULL AND L_PO_LINE IS NOT NULL THEN
          BEGIN
            IF CHECK_PO_REMAINING(L_PO_NUMBER,
                                  L_PO_LINE,
                                  I_UPDATE_PO.LINE_AMOUNT) = 'TRUE' THEN
              BEGIN
                UPDATE TAC_INVHUB_INVOICE_STG
                   SET PO_NUMBER         = L_PO_NUMBER,
                       PO_LINE_NUMBER    = L_PO_LINE,
                       INTERFACE_PO_FLAG = 'Y'
                 WHERE PR_NUMBER = I_UPDATE_PO.PR_NUMBER
                   AND PR_LINE_NUMBER = I_UPDATE_PO.PR_LINE_NUMBER
                   AND ORG_CODE = I_UPDATE_PO.ORG_CODE
                   AND UPPER(FILE_NAME) =
                       UPPER(NVL(P_FILE_NAME, FILE_NAME))
                   AND NVL(INTERFACE_PO_FLAG, 'N') = 'N';
              END;
            ELSE
              BEGIN
                UPDATE TAC_INVHUB_INVOICE_STG
                   SET PO_NUMBER         = L_PO_NUMBER,
                       PO_LINE_NUMBER    = L_PO_LINE,
                       INTERFACE_PO_FLAG = 'E'
                 WHERE PR_NUMBER = I_UPDATE_PO.PR_NUMBER
                   AND PR_LINE_NUMBER = I_UPDATE_PO.PR_LINE_NUMBER
                   AND ORG_CODE = I_UPDATE_PO.ORG_CODE
                   AND UPPER(FILE_NAME) =
                       UPPER(NVL(P_FILE_NAME, FILE_NAME))
                   AND NVL(INTERFACE_PO_FLAG, 'N') = 'N';
              END;
            END IF;
          END;
        END IF;
      END;
    END LOOP;
  
    FOR I_ERROR IN C_ERROR_PO_REMAINING(P_FILE_NAME) LOOP
      BEGIN
        SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
      
        G_LOG_DETAIL_SEQ := G_LOG_DETAIL_SEQ + 1;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).LEVEL_TYPE := 'D';
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_TYPE := 'PO Interface';
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).AP_INVOICE := I_ERROR.INVOICE_NUM;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).AP_INVOIE_LINE := I_ERROR.LINE_NUMBER;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PR_NUMBER := I_ERROR.PR_NUMBER;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PR_LINE_NUMBER := I_ERROR.PR_LINE_NUMBER;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).MATCH_OPTION := I_ERROR.MATCH_OPTION;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).VENDOR_NUMBER := I_ERROR.VENDOR_NUM;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).VENDOR_SITE_NAME := I_ERROR.VENDOR_SITE_CODE;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PAYMENT_TERM := I_ERROR.TERMS_NAME;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PO_NUMBER := I_ERROR.PO_NUMBER;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PO_LINE_NUMBER := I_ERROR.PO_LINE_NUMBER;
      
        IF I_ERROR.INTERFACE_PO_FLAG = 'E' THEN
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_CODE := 'INVHUB002-015';
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_MESSAGE := 'PO Remaining is not enough for invoice';
        ELSE
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_CODE := NULL;
          G_LOG_DETAIL(G_LOG_DETAIL_SEQ).ERROR_MESSAGE := NULL;
        END IF;
      
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PROCESSING_DATE := L_STARTTIME;
        G_LOG_DETAIL(G_LOG_DETAIL_SEQ).PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                                         L_STARTTIME),
                                                                 12,
                                                                 12);
      END;
    END LOOP;
  
    COMMIT;
  END;

  PROCEDURE PO_INTERFACE(ERRBUF           OUT VARCHAR2,
                         ERRCODE          OUT VARCHAR2,
                         P_COMP_CODE      IN VARCHAR2,
                         P_SOURCE         IN VARCHAR2,
                         P_INTERFACE_TYPE IN VARCHAR2,
                         P_FILENAME       IN VARCHAR2,
                         P_RERUN          IN VARCHAR2,
                         P_FILEPATH       IN VARCHAR2,
                         P_DEBUG_FLAG     IN VARCHAR2) IS
    --TYPE r_log_detail apps.DTINVHUB_LOG_DETAIL%ROWTYPE;
  
    STG_LOG_DETAIL T_LOG_DETAIL; -- := t_log_detail();
    V_STEP         VARCHAR2(240);
    V_ERROR_FLAG   BOOLEAN;
    -- PO_STR_ERROR T_PR_STR_ERROR;
    OLD_LINE_NUM         NUMBER;
    INTERFACE_LINE_SEQ   NUMBER;
    STG_PO_HEADER        T_PO_HEADER;
    STG_PO_LINE          T_PO_LINE;
    STG_PO_DIST          T_PO_DIST;
    P_CONC_REQUEST_ID    NUMBER;
    P_GROUP_NAME         VARCHAR2(50);
    V_PO_IMPORT_REQ_ID   NUMBER;
    V_STARTPROCESS       TIMESTAMP;
    L_COUNT_DTAC         NUMBER;
    L_COUNT_DTN          NUMBER;
    L_COUNT_PSB          NUMBER;
    P_CURRENT_REQUEST_ID NUMBER;
  
    CURSOR C_READFILE(P_COMPANY_CODE IN VARCHAR2,
                      P_SOURCE       IN VARCHAR2,
                      P_FILENAME     IN VARCHAR2) IS
      SELECT DISTINCT FILE_NAME, GROUP_NAME
        FROM TAC_INVHUB_INVOICE_STG
       WHERE 1 = 1 --org_code = p_company_code
         AND SOURCE = P_SOURCE
         AND INTERFACE_TYPE = '2WAY' --i_interface_type
         AND INTERFACE_PO_FLAG = 'N'
         AND INTERFACE_AP_FLAG = 'N'
         AND FILE_NAME = NVL(P_FILENAME, FILE_NAME) --i_file_name,file_name)
         AND FILE_NAME IS NOT NULL
       ORDER BY FILE_NAME;
  BEGIN
    G_DEBUG_FLAG := P_DEBUG_FLAG;
    G_STEP       := 'Start process';
  
    SELECT CURRENT_TIMESTAMP INTO V_STARTPROCESS FROM DUAL;
  
    DEBUG_MSG('LOG', G_STEP);
    P_CONC_REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
    -- fetch data
    DEBUG_MSG('LOG', 'p_comp_code=' || P_COMP_CODE);
    DEBUG_MSG('LOG', 'p_interface_type=' || P_INTERFACE_TYPE);
    DEBUG_MSG('LOG', 'p_filename=' || NVL(P_FILENAME, '*'));
  
    FOR R_READFILE IN C_READFILE(P_COMP_CODE, P_SOURCE, P_FILENAME) LOOP
      BEGIN
        UPDATE_EXISTING_PO(R_READFILE.FILE_NAME);
        VALIDATE_DUPLICATE_PR(R_READFILE.FILE_NAME);
        STG_PO_HEADER.DELETE;
        STG_PO_LINE.DELETE;
        STG_PO_DIST.DELETE;
        G_PO_LINE_INDX := 0;
        G_PO_DIST_INDX := 0;
      
        FETCH_PO_HEADER(P_GROUP_NAME => R_READFILE.FILE_NAME, --p_group_name,
                        P_PO_HEADER  => STG_PO_HEADER);
      
        IF STG_PO_HEADER.COUNT > 0 THEN
          BEGIN
            FOR I IN 1 .. STG_PO_HEADER.COUNT LOOP
              VALIDATE_PO_HEADER_INT(P_STG_PO_HEADER  => STG_PO_HEADER(I),
                                     P_STG_LOG_DETAIL => STG_LOG_DETAIL);
            
              FETCH_PO_LINE(P_STG_PO_HEADER => STG_PO_HEADER(I),
                            P_STG_PO_LINE   => STG_PO_LINE,
                            P_STG_PO_DIST   => STG_PO_DIST);
              DBMS_OUTPUT.PUT_LINE('stg_po_line :' || STG_PO_LINE.COUNT);
            END LOOP;
          
            INSERT_PO_HEADER_INT(STG_PO_HEADER     => STG_PO_HEADER,
                                 P_CONC_REQUEST_ID => P_CONC_REQUEST_ID);
            INSERT_PO_LINE_INT(STG_PO_LINE => STG_PO_LINE);
            INSERT_PO_DIST_INT(STG_PO_DIST => STG_PO_DIST);
          END;
        END IF;
      
        COMMIT;
        DEBUG_MSG('LOG', 'p_conc_request_id' || P_CONC_REQUEST_ID);
        -- CALL INTERFACE FOR DTAC
        G_STEP := 'CALL STANDARD INTERFACE PROGRAM';
      
        BEGIN
          SELECT SUM(DECODE(ORG_ID, 102, 1, 0)),
                 SUM(DECODE(ORG_ID, 142, 1, 0)),
                 SUM(DECODE(ORG_ID, 218, 1, 0))
            INTO L_COUNT_DTAC, L_COUNT_DTN, L_COUNT_PSB
            FROM PO_HEADERS_INTERFACE
           WHERE BATCH_ID = P_CONC_REQUEST_ID;
        END;
      
        IF L_COUNT_DTAC != 0 THEN
          V_PO_IMPORT_REQ_ID := CALL_STANDARD_PO_INTERFACE(P_CONC_REQUEST_ID,
                                                           'DTAC');
          DEBUG_MSG('LOG', 'v_po_import_req_id' || V_PO_IMPORT_REQ_ID);
        
          -- v_po_import_req_id := NULL;
          IF V_PO_IMPORT_REQ_ID IS NOT NULL THEN
            DEBUG_MSG('LOG', 'before call update_po_to_stg');
            UPDATE_PO_TO_STG(P_PO_IMPORT_ID    => V_PO_IMPORT_REQ_ID,
                             P_FILENAME        => R_READFILE.FILE_NAME,
                             L_STARTTIME       => V_STARTPROCESS,
                             P_CONC_REQUEST_ID => P_CONC_REQUEST_ID);
          END IF;
        END IF;
      
        IF L_COUNT_DTN != 0 THEN
          V_PO_IMPORT_REQ_ID := CALL_STANDARD_PO_INTERFACE(P_CONC_REQUEST_ID,
                                                           'DTN');
          DEBUG_MSG('LOG', 'v_po_import_req_id' || V_PO_IMPORT_REQ_ID);
        
          --  v_po_import_req_id := NULL;
          IF V_PO_IMPORT_REQ_ID IS NOT NULL THEN
            DEBUG_MSG('LOG', 'before call update_po_to_stg');
            UPDATE_PO_TO_STG(P_PO_IMPORT_ID    => V_PO_IMPORT_REQ_ID,
                             P_FILENAME        => R_READFILE.FILE_NAME,
                             L_STARTTIME       => V_STARTPROCESS,
                             P_CONC_REQUEST_ID => P_CONC_REQUEST_ID);
          END IF;
        END IF;
      
        IF L_COUNT_PSB != 0 THEN
          V_PO_IMPORT_REQ_ID := CALL_STANDARD_PO_INTERFACE(P_CONC_REQUEST_ID,
                                                           'PSB');
          DEBUG_MSG('LOG', 'v_po_import_req_id' || V_PO_IMPORT_REQ_ID);
        
          --  v_po_import_req_id := NULL;
          IF V_PO_IMPORT_REQ_ID IS NOT NULL THEN
            DEBUG_MSG('LOG', 'before call update_po_to_stg');
            UPDATE_PO_TO_STG(P_PO_IMPORT_ID    => V_PO_IMPORT_REQ_ID,
                             P_FILENAME        => R_READFILE.FILE_NAME,
                             L_STARTTIME       => V_STARTPROCESS,
                             P_CONC_REQUEST_ID => P_CONC_REQUEST_ID);
          END IF;
        END IF;
      
        GET_INTERFACE_ERROR(V_STARTPROCESS,
                            P_CONC_REQUEST_ID,
                            R_READFILE.GROUP_NAME);
      END;
    END LOOP;
  
    STG_PO_HEADER.DELETE;
    STG_PO_LINE.DELETE;
    STG_PO_DIST.DELETE;
    G_STEP := 'Write log detail';
    --
    WRITE_LOG_DETAIL(V_STARTPROCESS, P_CONC_REQUEST_ID);
    COMMIT;
    CALL_LOGMONITOR('DTINVHUB002', P_CONC_REQUEST_ID);
    G_STEP := 'write_output';
    WRITE_OUTPUT(P_CONC_REQUEST_ID, 'DTINVHUB002');
  EXCEPTION
    WHEN OTHERS THEN
      STG_PO_HEADER.DELETE;
      STG_PO_LINE.DELETE;
      STG_PO_DIST.DELETE;
      DEBUG_MSG('LOG', 'Error in step ' || G_STEP || ' : ' || SQLERRM);
      ERRCODE := 2;
  END PO_INTERFACE;

  FUNCTION GET_DIR_PATH(P_ORACLE_PATH IN VARCHAR2)
    RETURN DBA_DIRECTORIES.DIRECTORY_PATH%TYPE IS
    L_PATH DBA_DIRECTORIES.DIRECTORY_PATH%TYPE;
  BEGIN
    G_STEP := 'in get_dir_path';
  
    SELECT DIRECTORY_PATH
      INTO L_PATH
      FROM DBA_DIRECTORIES
     WHERE DIRECTORY_NAME = P_ORACLE_PATH; --'INVHUB_INV_INBOX';
  
    RETURN(L_PATH);
  EXCEPTION
    WHEN OTHERS THEN
      DEBUG_MSG('LOG', 'Error in step ' || G_STEP || ' : ' || SQLERRM);
      RETURN(NULL);
  END;

  PROCEDURE RETURNPAIDINVOICE(ERRBUF        OUT VARCHAR2,
                              ERRCODE       OUT VARCHAR2,
                              P_REPLACE     IN VARCHAR2,
                              P_FILEPATH    IN VARCHAR2,
                              P_DATE        IN DATE,
                              P_DATA_SOURCE IN VARCHAR2,
                              P_DEBUG_FLAG  IN VARCHAR2) IS
    CURSOR C_PAID_INV(P_DATE IN DATE, P_SOURCE IN VARCHAR2) IS
      SELECT DISTINCT DECODE(AI.ORG_ID, 102, 'DTAC', 142, 'DTN', 218, 'PSB') COMPANY_CODE,
                      AI.INVOICE_NUM INVOICE_NUMBER,
                      PV.SEGMENT1 SUPPLIER_CODE,
                      PS.DUE_DATE DUE_DATE,
                      ACA.STATUS_LOOKUP_CODE PAYMENT_STATUS,
                      ACA.DOC_SEQUENCE_VALUE PV_NUMBER,
                      ACA.CHECK_NUMBER DOCUMENT_NUMBER,
                      AI.DOC_SEQUENCE_VALUE AP_VOUCHER_NUMBER,
                      AI.PAY_GROUP_LOOKUP_CODE PAYGROUP
        FROM AP_INVOICES_ALL AI,
             PO_VENDORS PV,
             (SELECT DUE_DATE, INVOICE_ID, PAYMENT_NUM
                FROM AP_PAYMENT_SCHEDULES_ALL A
               WHERE (INVOICE_ID, PAYMENT_NUM) =
                     (SELECT INVOICE_ID, MAX(PAYMENT_NUM)
                        FROM AP_PAYMENT_SCHEDULES_ALL B
                       WHERE B.INVOICE_ID = A.INVOICE_ID
                       GROUP BY INVOICE_ID)) PS,
             --ap_invoice_payments_all aip,
             (SELECT *
                FROM AP_INVOICE_PAYMENTS_ALL AIP
               WHERE (INVOICE_ID, ACCOUNTING_EVENT_ID) IN
                     (SELECT INVOICE_ID, MAX(ACCOUNTING_EVENT_ID)
                        FROM AP_INVOICE_PAYMENTS_ALL
                       WHERE INVOICE_ID = AIP.INVOICE_ID
                       GROUP BY INVOICE_ID)) AIP,
             AP_CHECKS_ALL ACA
       WHERE TRUNC(AI.LAST_UPDATE_DATE) = TRUNC(P_DATE - 1)
         AND AI.VENDOR_ID = PV.VENDOR_ID
         AND AI.INVOICE_ID = PS.INVOICE_ID
         AND PS.INVOICE_ID = AIP.INVOICE_ID
         AND PS.PAYMENT_NUM = AIP.PAYMENT_NUM
         AND AIP.CHECK_ID = ACA.CHECK_ID
         AND SOURCE = P_SOURCE
       ORDER BY 1, 2, 3, 4;
  
    DESTFILE    UTL_FILE.FILE_TYPE;
    L_PATH      DBA_DIRECTORIES.DIRECTORY_PATH%TYPE;
    L_FILE      VARCHAR2(50);
    L_WRITETXT  VARCHAR2(1000);
    L_STARTTIME TIMESTAMP;
    L_ENDTIME   TIMESTAMP;
  BEGIN
    G_DEBUG_FLAG := P_DEBUG_FLAG;
  
    G_STEP := 'in ReturnPaidInvoice';
    DEBUG_MSG('LOG', G_STEP);
  
    SELECT CURRENT_TIMESTAMP INTO L_STARTTIME FROM DUAL;
  
    L_PATH := GET_DIR_PATH(P_ORACLE_PATH => G_DEFAULT_BACKINV_PATH);
    DEBUG_MSG('LOG', 'l_path :' || L_PATH);
    G_STEP := 'create file';
    L_FILE := G_DEFAULT_BACKINV_FILE || TO_CHAR(SYSDATE, 'YYYYMMDD_HHMI') ||
              '.txt';
  
    DESTFILE := UTL_FILE.FOPEN_NCHAR(G_DEFAULT_BACKINV_PATH, L_FILE, 'A'); --utl_file.fopen(g_default_backinv_path,l_file,'W');
  
    IF UTL_FILE.IS_OPEN(DESTFILE) THEN
      FOR I IN C_PAID_INV(NVL(P_DATE, TRUNC(SYSDATE)), P_DATA_SOURCE) LOOP
        L_WRITETXT := I.COMPANY_CODE || '|' || I.INVOICE_NUMBER || '|' ||
                      I.SUPPLIER_CODE || '|' || I.DUE_DATE || '|' ||
                      I.PAYMENT_STATUS || '|' || I.PV_NUMBER || '|' ||
                      I.DOCUMENT_NUMBER || '|' || I.AP_VOUCHER_NUMBER || '|' ||
                      I.PAYGROUP;
        DEBUG_MSG('LOG', L_WRITETXT);
        --utl_file.put_line(destFile,l_writetxt);
        UTL_FILE.PUT_LINE_NCHAR(DESTFILE, L_WRITETXT);
        UTL_FILE.FFLUSH(DESTFILE);
      
        SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
      
        INSERT INTO DTINVHUB_LOG_DETAIL
          (LEVEL_TYPE,
           REQUEST_ID,
           ERROR_TYPE,
           AP_INVOICE,
           VENDOR_NUMBER,
           DUE_DATE,
           PAYMENT_STATUS,
           PV_NUMBER,
           DOCUMENT_NUMBER,
           AP_VOUCHER_NUMBER,
           PAYGROUP,
           ERROR_CODE,
           ERROR_MESSAGE,
           PROCESSING_DATE,
           PROCESSING_TIME)
        VALUES
          ('D',
           FND_GLOBAL.CONC_REQUEST_ID,
           'Back Invoice',
           I.INVOICE_NUMBER,
           I.SUPPLIER_CODE,
           I.DUE_DATE,
           I.PAYMENT_STATUS,
           I.PV_NUMBER,
           I.DOCUMENT_NUMBER,
           I.AP_VOUCHER_NUMBER,
           I.PAYGROUP,
           NULL,
           NULL,
           L_STARTTIME,
           SUBSTR(TO_CHAR(L_ENDTIME - L_STARTTIME), 12, 12));
      END LOOP;
    
      SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
    
      INSERT INTO APPS.DTINVHUB_LOG_SUMMARY
        (LEVEL_TYPE,
         REQUEST_ID,
         PROGRAM_NAME,
         PROCESS_DATE,
         PROCESSING_TYPE,
         RECORD_COUNT_COMPLETION,
         RECORD_COUNT_ERROR,
         PROCESSING_DATE)
        SELECT 'H',
               REQUEST_ID,
               'DTINVHUB004',
               L_STARTTIME,
               'Upload',
               SUM(DECODE(ERROR_CODE, NULL, 1, 0)) COMPLETE,
               SUM(DECODE(ERROR_CODE, NULL, 0, 1)) ERROR,
               SUBSTR(TO_CHAR(L_ENDTIME - L_STARTTIME), 12, 12)
          FROM APPS.DTINVHUB_LOG_DETAIL
         WHERE REQUEST_ID = FND_GLOBAL.CONC_REQUEST_ID
         GROUP BY 'H', REQUEST_ID, 'DTINVHUB004', L_STARTTIME, 'Upload';
    
      G_STEP := 'Close file';
      UTL_FILE.FCLOSE(DESTFILE);
      WRITE_OUTPUT(FND_GLOBAL.CONC_REQUEST_ID, 'DTINVHUB004');
      CALL_LOGMONITOR('DTINVHUB004', FND_GLOBAL.CONC_REQUEST_ID);
    ELSE
      DEBUG_MSG('LOG', 'Couldn''t OPEN FILE');
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      DEBUG_MSG('LOG', 'Error in step ' || G_STEP || ' : ' || SQLERRM);
  END;

  PROCEDURE SUPPLIERUPDATE(ERRBUF       OUT VARCHAR2,
                           ERRCODE      OUT VARCHAR2,
                           P_REPLACE    IN VARCHAR2,
                           P_FILEPATH   IN VARCHAR2,
                           P_DATE       IN DATE,
                           P_FIRST_LOAD IN VARCHAR2,
                           P_DEBUG_FLAG IN VARCHAR2) IS
    CURSOR C_SUPPLIER(P_DATE IN DATE) IS
      SELECT PV.SEGMENT1 SUPPLIER_CODE,
             PV.VENDOR_NAME SUPPLIER_NAME,
             PVS.VENDOR_SITE_CODE SUPPLIER_SITE_NAME,
             APT.NAME PAYMENT_TERM,
             GREATEST(PV.LAST_UPDATE_DATE, PVS.LAST_UPDATE_DATE) LAST_UDPATE_DATE
        FROM PO_VENDORS PV, PO_VENDOR_SITES_ALL PVS, AP_TERMS APT
       WHERE PV.VENDOR_ID = PVS.VENDOR_ID(+)
         AND PVS.TERMS_ID = APT.TERM_ID(+)
         AND (TRUNC(PV.LAST_UPDATE_DATE) = TRUNC(P_DATE - 1) OR
             TRUNC(PVS.LAST_UPDATE_DATE) = TRUNC(P_DATE - 1))
      --     AND trunc(pvs.last_update_date) BETWEEN  to_date('01/01/2014','DD/MM/YYYY')
      --                                        AND  TO_DATE('30/01/2014','DD/MM/YYYY')
       ORDER BY 1, 3, 4;
  
    CURSOR C_SUPPLIER_FIRSTLOAD IS
      SELECT PV.SEGMENT1 SUPPLIER_CODE,
             PV.VENDOR_NAME SUPPLIER_NAME,
             PVS.VENDOR_SITE_CODE SUPPLIER_SITE_NAME,
             APT.NAME PAYMENT_TERM,
             GREATEST(PV.LAST_UPDATE_DATE, PVS.LAST_UPDATE_DATE) LAST_UDPATE_DATE
        FROM PO_VENDORS PV, PO_VENDOR_SITES_ALL PVS, AP_TERMS APT
       WHERE PV.VENDOR_ID = PVS.VENDOR_ID(+)
         AND PVS.TERMS_ID = APT.TERM_ID(+)
      --AND pv.segment1 ='101010'
      --  AND trunc(pvs.last_update_date) BETWEEN  to_date('01/01/2014','DD/MM/YYYY')
      --                                       AND  TO_DATE('30/01/2014','DD/MM/YYYY')
       ORDER BY 1, 3, 4;
  
    DESTFILE   UTL_FILE.FILE_TYPE;
    L_PATH     DBA_DIRECTORIES.DIRECTORY_PATH%TYPE;
    L_FILE     VARCHAR2(50);
    L_WRITETXT VARCHAR2(1000);
  
    TYPE R_SUPPLIER IS RECORD(
      SUPPLIER_CODE      PO_VENDORS.SEGMENT1%TYPE,
      SUPPLIER_NAME      PO_VENDORS.VENDOR_NAME%TYPE,
      SUPPLIER_SITE_NAME PO_VENDOR_SITES_ALL.VENDOR_SITE_CODE%TYPE,
      PAYMENT_TERM       AP_TERMS.NAME%TYPE,
      LAST_UPDATE_DATE   DATE);
  
    TYPE T_SUPPLIER IS TABLE OF R_SUPPLIER INDEX BY BINARY_INTEGER;
  
    SUPPLIER_REC T_SUPPLIER;
    L_STARTTIME  TIMESTAMP;
    L_ENDTIME    TIMESTAMP;
  BEGIN
    G_DEBUG_FLAG := P_DEBUG_FLAG;
  
    G_STEP := 'in SupplierUpdate';
    DEBUG_MSG('LOG', G_STEP);
  
    SELECT CURRENT_TIMESTAMP INTO L_STARTTIME FROM DUAL;
  
    G_STEP   := 'create file';
    L_FILE   := G_DEFAULT_SUPPLIER_FILE ||
                TO_CHAR(SYSDATE, 'YYYYMMDD_HHMI') || '.txt';
    DESTFILE :=  --utl_file.fopen(g_default_supplier_path,l_file,'W');
     UTL_FILE.FOPEN_NCHAR(G_DEFAULT_SUPPLIER_PATH, L_FILE, 'A');
  
    IF UTL_FILE.IS_OPEN(DESTFILE) THEN
      DEBUG_MSG('LOG', 'OPEN FILE');
      DEBUG_MSG('LOG', 'p_date' || TO_CHAR(P_DATE, 'DD/MM/YYYY'));
      G_STEP := 'Open cursor';
      DEBUG_MSG('LOG', G_STEP);
      DEBUG_MSG('LOG', 'P FIRST LOAD :' || P_FIRST_LOAD);
    
      IF P_FIRST_LOAD = 'Y' THEN
        DEBUG_MSG('LOG', 'open c_supplier_firstload');
      
        OPEN C_SUPPLIER_FIRSTLOAD;
      
        FETCH C_SUPPLIER_FIRSTLOAD BULK COLLECT
          INTO SUPPLIER_REC;
      
        CLOSE C_SUPPLIER_FIRSTLOAD;
      ELSE
        DEBUG_MSG('LOG', 'open c_supplier');
      
        OPEN C_SUPPLIER(NVL(P_DATE, TRUNC(SYSDATE)));
      
        FETCH C_SUPPLIER BULK COLLECT
          INTO SUPPLIER_REC;
      
        CLOSE C_SUPPLIER;
      END IF;
    
      IF SUPPLIER_REC.COUNT > 0 THEN
        G_STEP := 'Write text';
        DEBUG_MSG('LOG', G_STEP);
        DEBUG_MSG('LOG', 'supplier_rec.count :' || SUPPLIER_REC.COUNT);
      
        FOR I IN 1 .. SUPPLIER_REC.COUNT LOOP
          L_WRITETXT := SUPPLIER_REC(I)
                        .SUPPLIER_CODE || '|' || SUPPLIER_REC(I)
                        .SUPPLIER_NAME || '|' || SUPPLIER_REC(I)
                        .SUPPLIER_SITE_NAME || '|' || SUPPLIER_REC(I)
                        .PAYMENT_TERM || '|' ||
                         TO_CHAR(SUPPLIER_REC(I).LAST_UPDATE_DATE,
                                 'DD/MM/YYYY');
          --utl_file.put_line(destFile,l_writetxt);
          UTL_FILE.PUT_LINE_NCHAR(DESTFILE, L_WRITETXT);
          UTL_FILE.FFLUSH(DESTFILE);
        
          SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
        
          INSERT INTO DTINVHUB_LOG_DETAIL
            (LEVEL_TYPE,
             REQUEST_ID,
             ERROR_TYPE,
             VENDOR_NUMBER,
             VENDOR_SITE_NAME,
             VENDOR_NAME,
             PAYMENT_TERM,
             ERROR_CODE,
             ERROR_MESSAGE,
             PROCESSING_DATE,
             PROCESSING_TIME)
          VALUES
            ('D',
             FND_GLOBAL.CONC_REQUEST_ID,
             'Supplier Interface',
             SUPPLIER_REC(I).SUPPLIER_CODE,
             SUPPLIER_REC(I).SUPPLIER_SITE_NAME,
             SUPPLIER_REC(I).SUPPLIER_NAME,
             SUPPLIER_REC(I).PAYMENT_TERM,
             NULL,
             NULL,
             L_STARTTIME,
             SUBSTR(TO_CHAR(L_ENDTIME - L_STARTTIME), 12, 12));
        END LOOP;
      END IF;
    
      SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
    
      INSERT INTO APPS.DTINVHUB_LOG_SUMMARY
        (LEVEL_TYPE,
         REQUEST_ID,
         PROGRAM_NAME,
         PROCESS_DATE,
         PROCESSING_TYPE,
         RECORD_COUNT_COMPLETION,
         RECORD_COUNT_ERROR,
         PROCESSING_DATE)
        SELECT 'H',
               REQUEST_ID,
               'DTINVHUB005',
               L_STARTTIME,
               'Upload',
               SUM(DECODE(ERROR_CODE, NULL, 1, 0)) COMPLETE,
               SUM(DECODE(ERROR_CODE, NULL, 0, 1)) ERROR,
               SUBSTR(TO_CHAR(L_ENDTIME - L_STARTTIME), 12, 12)
          FROM APPS.DTINVHUB_LOG_DETAIL
         WHERE REQUEST_ID = FND_GLOBAL.CONC_REQUEST_ID
         GROUP BY 'H', REQUEST_ID, 'DTINVHUB005', L_STARTTIME, 'Upload';
    
      G_STEP := 'Close cursor';
      DEBUG_MSG('LOG', G_STEP);
      UTL_FILE.FCLOSE(DESTFILE);
      SUPPLIER_REC.DELETE;
      WRITE_OUTPUT(FND_GLOBAL.CONC_REQUEST_ID, 'DTINVHUB005');
      CALL_LOGMONITOR('DTINVHUB005', FND_GLOBAL.CONC_REQUEST_ID);
    ELSE
      DEBUG_MSG('LOG', 'Couldn''t OPEN FILE');
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      DEBUG_MSG('LOG', 'Error in step ' || G_STEP || ' : ' || SQLERRM);
  END;

  FUNCTION CHECK_PO(P_PO_NUMBER IN VARCHAR2) RETURN BOOLEAN IS
    L_CHECK_PO_NUMBER VARCHAR2(1);
  BEGIN
    SELECT DISTINCT 'Y'
      INTO L_CHECK_PO_NUMBER
      FROM PO_HEADERS_ALL
     WHERE SEGMENT1 = P_PO_NUMBER;
  
    RETURN(TRUE);
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN(FALSE);
  END;

  FUNCTION CHECK_GRN(P_GRN_NUMBER IN VARCHAR2) RETURN BOOLEAN IS
    L_CHECK_GRN VARCHAR2(1);
  BEGIN
    SELECT DISTINCT 'Y'
      INTO L_CHECK_GRN
      FROM RCV_SHIPMENT_HEADERS
     WHERE RECEIPT_NUM = P_GRN_NUMBER;
  
    RETURN(TRUE);
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN(FALSE);
  END;

  /*PROCEDURE  getGRNInfomation(p_grn_number in varchar2,
                                                   p_company_code in  varchar2,
                                                   p_po_number in varchar2,
                                                   o_grn_infomation out APPS.t_grn_info,
                                                   o_err_code out varchar2,
                                                   o_err_mesg out varchar2) IS
                l_sum_grn  number;
           BEGIN
                o_err_code := null;
                o_err_mesg := null;
                -- validate parameter
                 if (p_grn_number is null) or (p_company_code is null) or (p_po_number is null) then
                    dbms_output.put_line('null parameter');
                    o_err_code  := 'INVHUB001-005';
                    o_err_mesg := 'Missing Parameter';
                 end if;
  
                -- validate  existing po in system
                if o_err_code is null and (not CHECK_PO (p_po_number)) then
                   o_err_code  := 'INVHUB001-002';
                   o_err_mesg := 'Not found PO';
                end if;
  
                -- validate  existing grn in system
                if o_err_code is null and (not check_grn(p_grn_number)) then
                   o_err_code  := 'INVHUB001-001';
                   o_err_mesg := 'Not found GRN';
                end if;
  
                if o_err_code is null then
                begin
  --                   insert into o_grn_infomation
  --                   select  rsh.receipt_num GRN_NUMBER,--rt.*
  --                              'DTAC'  Company_Code,
  --                              poh.segment1 PO_Number,
  --                              pol.line_num  PO_Line_Number,
  --                              rsl.shipment_line_status_code GRN_STATUS,
  --                              rsl.line_num GRN_LINE_NUMBER,
  --                              item.item_code ITEM_CODE,
  --                              rsl.item_description LINE_DESCRIPTION,
  --                              rsl.quantity_received GRN_QTY, -- if rsl.destination_type_code='INVENTORY'  then qty if EXPENSE THEN AMOUNT
  --                              rsl.unit_of_measure GRN_UOM,
  --                              rsl.quantity_received GRN_AMOUNT,
  --                              poh.currency_code GRN_CURRENCY,
  --                              0 TOTAL_GRN_AMOUNT,
  --                              org.organization_code ORG_CODE,
  --                              pv.segment1 VENDOR_NUMBER,
  --                              pvs.vendor_site_code VENDOR_SITE_CODE,
  --                              payterm.name PAYMENT_TERM,
  --                              sum(pll.quantity-pll.quantity_received-pll.quantity_rejected-pll.quantity_cancelled) PO_Remaining_Amount
  --                     --  bulk collect into o_grn_infomation
  --                      from rcv_shipment_headers rsh
  --                             ,rcv_shipment_lines rsl
  --                             ,rcv_transactions rt
  --                            ,(select msi.inventory_item_id,msi.segment1 ITEM_CODE
  --                                from mtl_system_items_b msi,mtl_parameters mp
  --                              where msi.organization_id=mp.organization_id
  --                                  and msi.organization_id=mp.master_organization_id) ITEM
  --                             ,po_headers_all poh
  --                             ,po_lines_all pol
  --                             ,po_line_locations_all pll
  --                             ,po_distributions_all pd
  --                             ,po_vendors pv
  --                             ,po_vendor_sites_all pvs
  --                             ,ap_terms payterm
  --                             ,org_organization_definitions org
  --                    where rsh.receipt_num=p_grn_number --'20160001447'
  --                        and poh.segment1=p_po_number
  --                        and rsh.shipment_header_id=rsl.shipment_header_id
  --                        and rsl.shipment_header_id=rt.shipment_header_id
  --                        and rsl.shipment_line_id=rt.shipment_line_id
  --                        and rt.source_document_code='PO'
  --                        and rt.transaction_type='RECEIVE'
  --                        and receipt_source_code='VENDOR'
  --                        and rsl.item_id=item.inventory_item_id(+)
  --                        and rt.po_header_id=pll.po_header_id
  --                        and rt.po_line_id=pll.po_line_id
  --                        and rt.po_line_location_id=pd.line_location_id
  --                        and rt.po_distribution_id=pd.po_distribution_id
  --                        and poh.po_header_id=pol.po_header_id
  --                        and pol.po_header_id=pll.po_header_id
  --                        and pol.po_line_id=pll.po_line_id
  --                        and pll.po_header_id=pd.po_header_id
  --                        and pll.po_line_id=pd.po_line_id
  --                        and pll.line_location_id=pd.line_location_id
  --                        and poh.vendor_id=pv.vendor_id
  --                        and poh.vendor_id=pvs.vendor_id
  --                        and poh.vendor_site_id=pvs.vendor_site_id
  --                        and pvs.terms_id=payterm.term_id
  --                        and pll.ship_to_organization_id=org.organization_id
  --                    group by rsh.receipt_num,--rt.*
  --                               'DTAC',-- Company Code
  --                               poh.segment1,
  --                               pol.line_num,
  --                               rsl.shipment_line_status_code,
  --                               rsl.line_num,
  --                                item.item_code,
  --                                rsl.item_description,
  --                                rsl.quantity_received, -- if rsl.destination_type_code='INVENTORY'  then qty if EXPENSE THEN AMOUNT
  --                                rsl.unit_of_measure,
  --                                rsl.quantity_received,
  --                                poh.currency_code,
  --                                pv.segment1,
  --                                pvs.vendor_site_code,
  --                                payterm.name,
  --                                 org.organization_code;
  
                            if o_grn_infomation.count =0 then
                                o_err_code  := 'INVHUB001-003';
                                o_err_mesg := 'GRN and PO Not Match';
                            else
                                for i in 1 ..  o_grn_infomation.count  loop
                                     l_sum_grn := nvl(l_sum_grn,0) + nvl(o_grn_infomation(i).GRN_QTY,0);
  
                                end loop;
                                dbms_output.put_line('sum grn='||l_sum_grn);
                                for i in 1 ..  o_grn_infomation.count  loop
                                     o_grn_infomation(i).TOTAL_GRN_AMOUNT := l_sum_grn;
                                end loop;
  
                            end if;
  
                end;
                end if;
  
  
  
           END;*/

  PROCEDURE READDATA(P_FILENAME IN VARCHAR2) IS
    DESTFILE   UTL_FILE.FILE_TYPE;
    L_PATH     DBA_DIRECTORIES.DIRECTORY_PATH%TYPE;
    L_FILE     VARCHAR2(50);
    L_WRITETXT VARCHAR2(1000);
  BEGIN
    /*
    destFile := utl_file.fopen(g_default_inv_path,l_file,'R');
    
    IF utl_file.is_open(destFile) THEN
    
        FOR i IN c_paid_inv(p_date,p_data_source) LOOP
            l_writetxt := i.company_code||'|'||
                          i.invoice_number||'|'||
                          i.supplier_code||'|'||
                          i.due_date||'|'||
                          i.payment_status||'|'||
                          i.pv_number||'|'||
                          i.document_number||'|'||
                          i.ap_voucher_number||'|'||
                          i.Paygroup;
            debug_msg('LOG',l_writetxt);
            utl_file.put_line(destFile,l_writetxt);
        END LOOP;
        g_step := 'Close file';
        utl_file.fclose(destFile);
    ELSE
       debug_msg('LOG','Couldn''t OPEN FILE');
    END IF;*/
    NULL;
  END;

  PROCEDURE WRITEDATA(P_FILENAME IN VARCHAR2) IS
    DESTFILE   UTL_FILE.FILE_TYPE;
    L_PATH     DBA_DIRECTORIES.DIRECTORY_PATH%TYPE;
    L_FILE     VARCHAR2(50);
    L_WRITETXT VARCHAR2(1000);
  BEGIN
    /*
    destFile := utl_file.fopen(g_default_inv_path,l_file,'R');
    
    IF utl_file.is_open(destFile) THEN
    
        FOR i IN c_paid_inv(p_date,p_data_source) LOOP
            l_writetxt := i.company_code||'|'||
                          i.invoice_number||'|'||
                          i.supplier_code||'|'||
                          i.due_date||'|'||
                          i.payment_status||'|'||
                          i.pv_number||'|'||
                          i.document_number||'|'||
                          i.ap_voucher_number||'|'||
                          i.Paygroup;
            debug_msg('LOG',l_writetxt);
            utl_file.put_line(destFile,l_writetxt);
        END LOOP;
        g_step := 'Close file';
        utl_file.fclose(destFile);
    ELSE
       debug_msg('LOG','Couldn''t OPEN FILE');
    END IF;*/
    NULL;
  END;

  PROCEDURE LOG_DTINVHUB002(P_CONCURRENT_ID IN NUMBER,
                            P_FILENAME      IN VARCHAR2) IS
    L_DESTFILE UTL_FILE.FILE_TYPE;
    L_PATH     DBA_DIRECTORIES.DIRECTORY_PATH%TYPE;
    L_FILE     VARCHAR2(100);
    L_WRITETXT VARCHAR2(1000);
  
    TYPE R_LOG_DETAIL IS RECORD(
      LOG_TXT VARCHAR2(1000));
  
    TYPE T_LOG_DETAIL IS TABLE OF R_LOG_DETAIL INDEX BY BINARY_INTEGER;
  
    L_LOG_DETAIL T_LOG_DETAIL;
  
    CURSOR C_LOG_DETAIL(P_REQ_ID IN NUMBER) IS
      SELECT DISTINCT LEVEL_TYPE || '|' || ERROR_TYPE || '|' || AP_INVOICE || '|' ||
                      AP_INVOIE_LINE || '|' || PO_NUMBER || '|' ||
                      PO_LINE_NUMBER || '|' || RECEIPT_NUMBER || '|' ||
                      RECEIPT_LINE_NUM || '|' || PR_NUMBER || '|' ||
                      PR_LINE_NUMBER || '|' || MATCH_OPTION || '|' ||
                      VENDOR_NUMBER || '|' || PROCESSING_DATE || '|' ||
                      PROCESSING_TIME || '|' ||
                      DECODE(ERROR_CODE, NULL, 'Completed', 'Error') || '|' ||
                      ERROR_CODE || '|' || ERROR_MESSAGE LOG_TXT
        FROM DTINVHUB_LOG_DETAIL
       WHERE REQUEST_ID = P_REQ_ID;
  BEGIN
    G_STEP := 'in log_DTINVHUB002';
    DEBUG_MSG('LOG', G_STEP);
    -- Write log for fxth
    L_PATH := GET_DIR_PATH(P_ORACLE_PATH => G_DEFAULT_INV_MONITOR_PATH);
    DEBUG_MSG('LOG', 'l_path :' || L_PATH);
    G_STEP := 'create file';
    DEBUG_MSG('LOG', G_STEP);
    L_FILE := P_FILENAME || TO_CHAR(SYSDATE, 'YYYYMMDD_HHMI') || '.txt';
  
    G_STEP := 'open file :' || L_FILE;
    DEBUG_MSG('LOG', G_STEP);
    DEBUG_MSG('LOG',
              'g_default_inv_monitor_path:' || G_DEFAULT_INV_MONITOR_PATH);
    L_DESTFILE := UTL_FILE.FOPEN_NCHAR(G_DEFAULT_INV_MONITOR_PATH,
                                       L_FILE,
                                       'A');
  
    IF UTL_FILE.IS_OPEN(L_DESTFILE) THEN
      --Write Header section
      SELECT LEVEL_TYPE || '|' || REQUEST_ID || '|' || PROGRAM_NAME || '|' ||
             PROCESS_DATE || '|' || PROCESSING_TYPE || '|' ||
             PROCESSING_DATE || '|' || RECORD_COUNT_COMPLETION || '|' ||
             RECORD_COUNT_ERROR
        INTO L_WRITETXT
        FROM DTINVHUB_LOG_SUMMARY
       WHERE REQUEST_ID = P_CONCURRENT_ID;
    
      DEBUG_MSG('LOG', L_WRITETXT);
      --  utl_file.put_nchar(l_destfile,l_writetxt||'\r');
      -- utl_file.new_line(l_destfile,1);
      UTL_FILE.PUTF_NCHAR(L_DESTFILE, L_WRITETXT || CHR(10));
      UTL_FILE.FFLUSH(L_DESTFILE);
    
      --Write detail section
      OPEN C_LOG_DETAIL(P_CONCURRENT_ID);
    
      FETCH C_LOG_DETAIL BULK COLLECT
        INTO L_LOG_DETAIL;
    
      IF L_LOG_DETAIL.COUNT > 0 THEN
        FOR I IN 1 .. L_LOG_DETAIL.COUNT LOOP
          --c_log_detail(p_concurrent_id) LOOP
          L_WRITETXT := L_LOG_DETAIL(I).LOG_TXT; --i.log_txt;
          DEBUG_MSG('LOG', L_WRITETXT);
          DEBUG_MSG('LOG', 'i=' || I);
          DEBUG_MSG('LOG', 'l_log_detail.count=' || L_LOG_DETAIL.COUNT);
        
          UTL_FILE.PUTF_NCHAR(L_DESTFILE, L_WRITETXT || CHR(10));
          UTL_FILE.FFLUSH(L_DESTFILE);
        END LOOP;
      END IF;
    
      G_STEP := 'Close file';
      UTL_FILE.FCLOSE(L_DESTFILE);
      L_LOG_DETAIL.DELETE;
      DEBUG_MSG('LOG', G_STEP);
    
      CLOSE C_LOG_DETAIL;
    ELSE
      DEBUG_MSG('LOG', 'Couldn''t OPEN FILE');
    END IF;
  
    -- Write log for splunk
    L_PATH := GET_DIR_PATH(P_ORACLE_PATH => G_DEFAULT_SPLUNK_LOG);
    DEBUG_MSG('LOG', 'l_path :' || L_PATH);
    G_STEP := 'create file';
    DEBUG_MSG('LOG', G_STEP);
    L_FILE := P_FILENAME || TO_CHAR(SYSDATE, 'YYYYMMDD_HHMI') || '.txt';
  
    G_STEP := 'open file :' || L_FILE;
    DEBUG_MSG('LOG', G_STEP);
    DEBUG_MSG('LOG', 'g_default_inv_monitor_path:' || G_DEFAULT_SPLUNK_LOG);
    L_DESTFILE := UTL_FILE.FOPEN_NCHAR(G_DEFAULT_SPLUNK_LOG, L_FILE, 'A');
  
    IF UTL_FILE.IS_OPEN(L_DESTFILE) THEN
      --Write Header section
      SELECT LEVEL_TYPE || '|' || REQUEST_ID || '|' || PROGRAM_NAME || '|' ||
             PROCESS_DATE || '|' || PROCESSING_TYPE || '|' ||
             PROCESSING_DATE || '|' || RECORD_COUNT_COMPLETION || '|' ||
             RECORD_COUNT_ERROR
        INTO L_WRITETXT
        FROM DTINVHUB_LOG_SUMMARY
       WHERE REQUEST_ID = P_CONCURRENT_ID;
    
      DEBUG_MSG('LOG', L_WRITETXT);
      --  utl_file.put_nchar(l_destfile,l_writetxt||'\r');
      -- utl_file.new_line(l_destfile,1);
      UTL_FILE.PUTF_NCHAR(L_DESTFILE, L_WRITETXT || CHR(10));
      UTL_FILE.FFLUSH(L_DESTFILE);
    
      --Write detail section
      OPEN C_LOG_DETAIL(P_CONCURRENT_ID);
    
      FETCH C_LOG_DETAIL BULK COLLECT
        INTO L_LOG_DETAIL;
    
      IF L_LOG_DETAIL.COUNT > 0 THEN
        FOR I IN 1 .. L_LOG_DETAIL.COUNT LOOP
          --c_log_detail(p_concurrent_id) LOOP
          L_WRITETXT := L_LOG_DETAIL(I).LOG_TXT; --i.log_txt;
          DEBUG_MSG('LOG', L_WRITETXT);
          DEBUG_MSG('LOG', 'i=' || I);
          DEBUG_MSG('LOG', 'l_log_detail.count=' || L_LOG_DETAIL.COUNT);
        
          UTL_FILE.PUTF_NCHAR(L_DESTFILE, L_WRITETXT || CHR(10));
          UTL_FILE.FFLUSH(L_DESTFILE);
        END LOOP;
      END IF;
    
      G_STEP := 'Close file';
      UTL_FILE.FCLOSE(L_DESTFILE);
      L_LOG_DETAIL.DELETE;
      DEBUG_MSG('LOG', G_STEP);
    
      CLOSE C_LOG_DETAIL;
    ELSE
      DEBUG_MSG('LOG', 'Couldn''t OPEN FILE');
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      DEBUG_MSG('LOG', 'Error in step ' || G_STEP || ' : ' || SQLERRM);
  END;

  PROCEDURE LOG_DTINVHUB003(P_CONCURRENT_ID IN NUMBER,
                            P_FILENAME      IN VARCHAR2) IS
    L_DESTFILE UTL_FILE.FILE_TYPE;
    L_PATH     DBA_DIRECTORIES.DIRECTORY_PATH%TYPE;
    L_FILE     VARCHAR2(100);
    L_WRITETXT VARCHAR2(1000);
  
    CURSOR C_LOG_DETAIL(P_REQ_ID IN NUMBER) IS
      SELECT DISTINCT LEVEL_TYPE || '|' || ERROR_TYPE || '|' || AP_INVOICE || '|' ||
                      AP_INVOIE_LINE || '|' || PO_NUMBER || '|' ||
                      PO_LINE_NUMBER || '|' || RECEIPT_NUMBER || '|' ||
                      RECEIPT_LINE_NUM || '|' || PR_NUMBER || '|' ||
                      PR_LINE_NUMBER || '|' || MATCH_OPTION || '|' ||
                      VENDOR_NUMBER || '|' || PROCESSING_DATE || '|' ||
                      PROCESSING_TIME || '|' ||
                      DECODE(ERROR_CODE, NULL, 'Completed', 'Error') || '|' ||
                      ERROR_CODE || '|' || ERROR_MESSAGE LOG_TXT
        FROM DTINVHUB_LOG_DETAIL
       WHERE REQUEST_ID = P_REQ_ID
       ORDER BY 1;
  BEGIN
    DBMS_LOCK.SLEEP(60);
    G_STEP := 'in log_DTINVHUB003';
    DEBUG_MSG('LOG', G_STEP);
    -- LOG FOR FXTH
    L_PATH := GET_DIR_PATH(P_ORACLE_PATH => G_DEFAULT_INV_MONITOR_PATH);
    DEBUG_MSG('LOG', 'l_path :' || L_PATH);
    G_STEP := 'create file';
    DEBUG_MSG('LOG', G_STEP);
    L_FILE := P_FILENAME || TO_CHAR(SYSDATE, 'YYYYMMDD_HHMI') || '.txt';
  
    G_STEP := 'open file :' || L_FILE;
    DEBUG_MSG('LOG', G_STEP);
    DEBUG_MSG('LOG',
              'g_default_inv_monitor_path:' || G_DEFAULT_INV_MONITOR_PATH);
    L_DESTFILE := UTL_FILE.FOPEN_NCHAR(G_DEFAULT_INV_MONITOR_PATH,
                                       L_FILE,
                                       'A');
  
    IF UTL_FILE.IS_OPEN(L_DESTFILE) THEN
      --Write Header section
      DEBUG_MSG('LOG', 'Start write header');
    
      SELECT LEVEL_TYPE || '|' || REQUEST_ID || '|' || PROGRAM_NAME || '|' ||
             PROCESS_DATE || '|' || PROCESSING_TYPE || '|' ||
             PROCESSING_DATE || '|' || RECORD_COUNT_COMPLETION || '|' ||
             RECORD_COUNT_ERROR
        INTO L_WRITETXT
        FROM DTINVHUB_LOG_SUMMARY
       WHERE REQUEST_ID = P_CONCURRENT_ID;
    
      DEBUG_MSG('LOG', L_WRITETXT);
      UTL_FILE.PUT_LINE_NCHAR(L_DESTFILE, L_WRITETXT);
      UTL_FILE.FFLUSH(L_DESTFILE);
    
      --Write detail section
    
      FOR I IN C_LOG_DETAIL(P_CONCURRENT_ID) LOOP
        L_WRITETXT := I.LOG_TXT;
        DEBUG_MSG('LOG', L_WRITETXT);
        UTL_FILE.PUT_LINE_NCHAR(L_DESTFILE, L_WRITETXT);
        UTL_FILE.FFLUSH(L_DESTFILE);
        --     UTL_FILE.FFLUSH(l_destFile);
      
      END LOOP;
    
      G_STEP := 'Close file';
      UTL_FILE.FCLOSE(L_DESTFILE);
    ELSE
      DEBUG_MSG('LOG', 'Couldn''t OPEN FILE');
    END IF;
  
    -- LOG FOR  SPLUNK
    L_PATH := GET_DIR_PATH(P_ORACLE_PATH => G_DEFAULT_SPLUNK_LOG);
    DEBUG_MSG('LOG', 'l_path :' || L_PATH);
    G_STEP := 'create file';
    DEBUG_MSG('LOG', G_STEP);
    L_FILE := P_FILENAME || TO_CHAR(SYSDATE, 'YYYYMMDD_HHMI') || '.txt';
  
    G_STEP := 'open file :' || L_FILE;
    DEBUG_MSG('LOG', G_STEP);
    DEBUG_MSG('LOG', 'g_default_splunk_log:' || G_DEFAULT_SPLUNK_LOG);
    L_DESTFILE := UTL_FILE.FOPEN_NCHAR(G_DEFAULT_SPLUNK_LOG, L_FILE, 'A');
  
    IF UTL_FILE.IS_OPEN(L_DESTFILE) THEN
      --Write Header section
      DEBUG_MSG('LOG', 'Start write header');
    
      SELECT LEVEL_TYPE || '|' || REQUEST_ID || '|' || PROGRAM_NAME || '|' ||
             PROCESS_DATE || '|' || PROCESSING_TYPE || '|' ||
             PROCESSING_DATE || '|' || RECORD_COUNT_COMPLETION || '|' ||
             RECORD_COUNT_ERROR
        INTO L_WRITETXT
        FROM DTINVHUB_LOG_SUMMARY
       WHERE REQUEST_ID = P_CONCURRENT_ID;
    
      DEBUG_MSG('LOG', L_WRITETXT);
      UTL_FILE.PUT_LINE_NCHAR(L_DESTFILE, L_WRITETXT);
      UTL_FILE.FFLUSH(L_DESTFILE);
    
      --Write detail section
    
      FOR I IN C_LOG_DETAIL(P_CONCURRENT_ID) LOOP
        L_WRITETXT := I.LOG_TXT;
        DEBUG_MSG('LOG', L_WRITETXT);
        UTL_FILE.PUT_LINE_NCHAR(L_DESTFILE, L_WRITETXT);
        UTL_FILE.FFLUSH(L_DESTFILE);
        --     UTL_FILE.FFLUSH(l_destFile);
      
      END LOOP;
    
      G_STEP := 'Close file';
      UTL_FILE.FCLOSE(L_DESTFILE);
    ELSE
      DEBUG_MSG('LOG', 'Couldn''t OPEN FILE');
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      DEBUG_MSG('LOG', 'Error in step ' || G_STEP || ' : ' || SQLERRM);
  END;

  PROCEDURE LOG_DTINVHUB004(P_CONCURRENT_ID IN NUMBER,
                            P_FILENAME      IN VARCHAR2) IS
    L_DESTFILE UTL_FILE.FILE_TYPE;
    L_PATH     DBA_DIRECTORIES.DIRECTORY_PATH%TYPE;
    L_FILE     VARCHAR2(100);
    L_WRITETXT VARCHAR2(1000);
  
    CURSOR C_LOG_DETAIL(P_REQ_ID IN NUMBER) IS
      SELECT LEVEL_TYPE || '|' || ERROR_TYPE || '|' || AP_INVOICE || '|' ||
             VENDOR_NUMBER || '|' || DUE_DATE || '|' || PAYMENT_STATUS || '|' ||
             PV_NUMBER || '|' || DOCUMENT_NUMBER || '|' ||
             AP_VOUCHER_NUMBER || '|' || PAYGROUP || '|' || PROCESSING_DATE || '|' ||
             PROCESSING_TIME || '|' || 'Completed' LOG_TXT
        FROM DTINVHUB_LOG_DETAIL
       WHERE REQUEST_ID = P_REQ_ID;
  BEGIN
    DBMS_LOCK.SLEEP(60);
    G_STEP := 'in log_DTINVHUB004';
    DEBUG_MSG('LOG', G_STEP);
  
    -- LOG FOR  SPLUNK
    L_PATH := GET_DIR_PATH(P_ORACLE_PATH => G_DEFAULT_SPLUNK_LOG);
    DEBUG_MSG('LOG', 'l_path :' || L_PATH);
    G_STEP := 'create file';
    DEBUG_MSG('LOG', G_STEP);
    L_FILE := P_FILENAME || TO_CHAR(SYSDATE, 'YYYYMMDD_HHMI') || '.txt';
  
    G_STEP := 'open file :' || L_FILE;
    DEBUG_MSG('LOG', G_STEP);
    DEBUG_MSG('LOG', 'g_default_splunk_log:' || G_DEFAULT_SPLUNK_LOG);
    L_DESTFILE := UTL_FILE.FOPEN_NCHAR(G_DEFAULT_SPLUNK_LOG, L_FILE, 'A');
  
    IF UTL_FILE.IS_OPEN(L_DESTFILE) THEN
      --Write Header section
      DEBUG_MSG('LOG', 'Start write header');
    
      SELECT LEVEL_TYPE || '|' || REQUEST_ID || '|' || PROGRAM_NAME || '|' ||
             PROCESS_DATE || '|' || PROCESSING_TYPE || '|' ||
             PROCESSING_DATE || '|' || RECORD_COUNT_COMPLETION || '|' ||
             RECORD_COUNT_ERROR
        INTO L_WRITETXT
        FROM DTINVHUB_LOG_SUMMARY
       WHERE REQUEST_ID = P_CONCURRENT_ID;
    
      DEBUG_MSG('LOG', L_WRITETXT);
      UTL_FILE.PUT_LINE_NCHAR(L_DESTFILE, L_WRITETXT);
      UTL_FILE.FFLUSH(L_DESTFILE);
    
      --Write detail section
    
      FOR I IN C_LOG_DETAIL(P_CONCURRENT_ID) LOOP
        L_WRITETXT := I.LOG_TXT;
        DEBUG_MSG('LOG', L_WRITETXT);
        UTL_FILE.PUT_LINE_NCHAR(L_DESTFILE, L_WRITETXT);
        UTL_FILE.FFLUSH(L_DESTFILE);
        --     UTL_FILE.FFLUSH(l_destFile);
      
      END LOOP;
    
      G_STEP := 'Close file';
      UTL_FILE.FCLOSE(L_DESTFILE);
    ELSE
      DEBUG_MSG('LOG', 'Couldn''t OPEN FILE');
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      DEBUG_MSG('LOG', 'Error in step ' || G_STEP || ' : ' || SQLERRM);
  END;

  PROCEDURE LOG_DTINVHUB005(P_CONCURRENT_ID IN NUMBER,
                            P_FILENAME      IN VARCHAR2) IS
    L_DESTFILE UTL_FILE.FILE_TYPE;
    L_PATH     DBA_DIRECTORIES.DIRECTORY_PATH%TYPE;
    L_FILE     VARCHAR2(100);
    L_WRITETXT VARCHAR2(1000);
  
    CURSOR C_LOG_DETAIL(P_REQ_ID IN NUMBER) IS
      SELECT LEVEL_TYPE || '|' || ERROR_TYPE || '|' || VENDOR_NUMBER || '|' ||
             VENDOR_NAME || '|' || VENDOR_SITE_NAME || '|' || PAYMENT_TERM || '|' ||
             SYSDATE || '|' || PROCESSING_DATE || '|' || PROCESSING_TIME || '|' ||
             'Completed' LOG_TXT
        FROM DTINVHUB_LOG_DETAIL
       WHERE REQUEST_ID = P_REQ_ID;
  BEGIN
    DBMS_LOCK.SLEEP(60);
    G_STEP := 'in log_DTINVHUB005';
    DEBUG_MSG('LOG', G_STEP);
  
    -- LOG FOR  SPLUNK
    L_PATH := GET_DIR_PATH(P_ORACLE_PATH => G_DEFAULT_SPLUNK_LOG);
    DEBUG_MSG('LOG', 'l_path :' || L_PATH);
    G_STEP := 'create file';
    DEBUG_MSG('LOG', G_STEP);
    L_FILE := P_FILENAME || TO_CHAR(SYSDATE, 'YYYYMMDD_HHMI') || '.txt';
  
    G_STEP := 'open file :' || L_FILE;
    DEBUG_MSG('LOG', G_STEP);
    DEBUG_MSG('LOG', 'g_default_splunk_log:' || G_DEFAULT_SPLUNK_LOG);
    L_DESTFILE := UTL_FILE.FOPEN_NCHAR(G_DEFAULT_SPLUNK_LOG, L_FILE, 'A');
  
    IF UTL_FILE.IS_OPEN(L_DESTFILE) THEN
      --Write Header section
      DEBUG_MSG('LOG', 'Start write header');
    
      SELECT LEVEL_TYPE || '|' || REQUEST_ID || '|' || PROGRAM_NAME || '|' ||
             PROCESS_DATE || '|' || PROCESSING_TYPE || '|' ||
             PROCESSING_DATE || '|' || RECORD_COUNT_COMPLETION || '|' ||
             RECORD_COUNT_ERROR
        INTO L_WRITETXT
        FROM DTINVHUB_LOG_SUMMARY
       WHERE REQUEST_ID = P_CONCURRENT_ID;
    
      DEBUG_MSG('LOG', L_WRITETXT);
      UTL_FILE.PUT_LINE_NCHAR(L_DESTFILE, L_WRITETXT);
      UTL_FILE.FFLUSH(L_DESTFILE);
    
      --Write detail section
    
      FOR I IN C_LOG_DETAIL(P_CONCURRENT_ID) LOOP
        L_WRITETXT := I.LOG_TXT;
        DEBUG_MSG('LOG', L_WRITETXT);
        UTL_FILE.PUT_LINE_NCHAR(L_DESTFILE, L_WRITETXT);
        UTL_FILE.FFLUSH(L_DESTFILE);
        --     UTL_FILE.FFLUSH(l_destFile);
      
      END LOOP;
    
      G_STEP := 'Close file';
      UTL_FILE.FCLOSE(L_DESTFILE);
    ELSE
      DEBUG_MSG('LOG', 'Couldn''t OPEN FILE');
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      DEBUG_MSG('LOG', 'Error in step ' || G_STEP || ' : ' || SQLERRM);
  END;

  PROCEDURE LOG_MONITOR(ERRBUF          OUT VARCHAR2,
                        ERRCODE         OUT VARCHAR2,
                        P_PROGRAM_NAME  IN VARCHAR2,
                        P_CONCURRENT_ID IN NUMBER,
                        -- p_folder_name   in varchar2,
                        P_DEBUG_FLAG IN VARCHAR2) IS
    L_PO_FILENAME       VARCHAR2(50) := 'INVHUB_POINBOUND_';
    L_AP_FILENAME       VARCHAR2(50) := 'INVHUB_APINVBOUND_';
    L_PAID_INV_FILENAME VARCHAR2(50) := 'INVHUB_PAIDINVOUTBOUND_';
    L_SUPPLIER          VARCHAR2(50) := 'INVHUB_SUPPLIEROUTBOUND_';
    L_WEBSERVICE        VARCHAR2(50) := 'INVHUB_GRN_';
  BEGIN
    G_DEBUG_FLAG := P_DEBUG_FLAG;
  
    IF P_PROGRAM_NAME = 'DTINVHUB002' THEN
      LOG_DTINVHUB002(P_CONCURRENT_ID, L_PO_FILENAME);
    ELSIF P_PROGRAM_NAME = 'DTINVHUB003' THEN
      LOG_DTINVHUB003(P_CONCURRENT_ID, L_AP_FILENAME);
    ELSIF P_PROGRAM_NAME = 'DTINVHUB004' THEN
      LOG_DTINVHUB004(P_CONCURRENT_ID, L_PAID_INV_FILENAME);
    ELSIF P_PROGRAM_NAME = 'DTINVHUB005' THEN
      LOG_DTINVHUB005(P_CONCURRENT_ID, L_SUPPLIER);
    ELSIF P_PROGRAM_NAME = 'DTINVHUB001' THEN
      NULL;
    END IF;
  END;
END TAC_PO_INVOICEHUB_PKG;
