create or replace package DT_INVOICEHUB_PKG2
AS 
  /* DECLARE GLOBAL VARIABLE  */
      
      TYPE r_po_header is record (group_name varchar2(21),
                                                source          varchar2(5),
                                                vendor_num  varchar2(30),
                                                vendor_site_code varchar2(15), 
                                                doc_category_code varchar2(50),
                                                pr_number   varchar2(20));
      
      TYPE r_grn_info IS  RECORD (GRN_NUMBER       Varchar2(30),
                                               Company_Code     Varchar2(10),
                                               PO_Number          Varchar2(20),
                                               PO_Line_Number   Number,
                                               GRN_STATUS        Varchar2(20),
                                               GRN_LINE_NUMBER Number,
                                               ITEM_CODE            varchar2(40),
                                               LINE_DESCRIPTION Varchar2(240),
                                               GRN_QTY               Number, 
                                               GRN_UOM              Varchar2(10),
                                               GRN_AMOUNT        Number,
                                               GRN_CURRENCY     Varchar2(15),
                                               TOTAL_GRN_AMOUNT Number,
                                               ORG_CODE            Varchar2(3),
                                               VENDOR_NUMBER  Varchar2(30),
                                               VENDOR_SITE_CODE Varchar2(15),
                                               PAYMENT_TERM    Varchar2(50),
                                               PO_Remaining_Amount Number);
                                               
  TYPE t_po_header IS TABLE OF r_po_header INDEX BY BINARY_INTEGER; 
  TYPE t_grn_info IS TABLE OF r_grn_info INDEX BY BINARY_INTEGER;    
  
  TYPE R_PR_STG_ERROR IS RECORD ( MODULE_TYPE  VARCHAR2(10),
                                  ROWNUMBER    NUMBER,
                                  P_KEY1       VARCHAR2(10),
                                  P_KEY2       VARCHAR2(10),
                                  ERROR_MSG    VARCHAR2(240)
                                );  
  TYPE T_PR_STR_ERROR IS TABLE OF R_PR_STG_ERROR INDEX BY PLS_INTEGER;
  
  g_po_error number;
  g_debug_flag varchar2(1):='N';
  c_interface_2way varchar2(10) := '2WAY';
  c_inteface_3way  varchar2(10) := '3WAY';
  g_step       varchar2(50);
  TYPE T_tac_invhub_invoice_stg IS TABLE OF tac_invhub_invoice_stg%ROWTYPE INDEX BY PLS_INTEGER;
  
                      
 
                          
      PROCEDURE  getGRNInfomation(grn_number in varchar2,
                                                 company_code in  varchar2,
                                                 po_number in varchar2,
                                                 result  out varchar2,
                                                 error_code out varchar2,
                                                 error_mesg out varchar2,
                                                 grn_infomation out TAC_GRN_INFO_TABLE--t_grn_info
                                                
                                                 );
  
  
END DT_INVOICEHUB_PKG2;