CREATE OR REPLACE TRIGGER "APPS".DTINVHUB_INVOICE_VALIDATE
BEFORE UPDATE
ON "AP"."AP_INVOICES_ALL"

FOR EACH ROW
DECLARE

v_invoices ap_invoices_all%ROWTYPE;

BEGIN
   IF AP_INVOICES_PKG.GET_APPROVAL_STATUS(:new.invoice_id,
                                          :new.INVOICE_AMOUNT,
                                          :new.PAYMENT_STATUS_FLAG,
                                          :new.INVOICE_TYPE_LOOKUP_CODE 
                                         )!='NEVER APPROVED' THEN
   APPS.TAC_AP_INVOICES_PKG.INSERT_VALIDATED_INVOICE(P_INVOICE_ID => :new.invoice_id,
                                                     P_USER_ID    => :new.last_updated_by,
                                                     P_VALIDATED_DATE  => SYSDATE);
    END IF;
EXCEPTION
WHEN OTHERS                                  THEN
   NULL;
END DTINVHUB_INVOICE_VALIDATE;
/

