create or replace PACKAGE BODY TAC_AP_INVOICEHUB_PKG is
  ----------------------------------------------------------------------------------------------------

  PROCEDURE get_project_information ( p_req_header_reference_num IN VARCHAR2,
                                        p_req_line_reference_num   IN NUMBER,
                                        o_project_id               IN OUT po_req_distributions_all.project_id%TYPE,
                                        o_project_name             IN OUT PA_PROJECTS_ALL.segment1%TYPE,
                                        o_task_id                  IN OUT po_req_distributions_all.task_id%TYPE,
                                        o_task_name                IN OUT PA_TASKS.task_number%TYPE,
                                        o_expenditure_type         IN OUT po_req_distributions_all.expenditure_type%TYPE,
                                        o_expenditure_item_date    IN OUT po_req_distributions_all.expenditure_item_date%TYPE,
                                        o_expenditure_organization_id  IN OUT po_req_distributions_all.expenditure_organization_id%TYPE) IS
    BEGIN
       SELECT prd.project_id,
              ppa.segment1,
              prd.task_id,
              pt.task_number,
              prd.expenditure_type,
              prd.expenditure_item_date,
              prd.expenditure_organization_id 
         INTO o_project_id,
              o_project_name,
              o_task_id,
              o_task_name,
              o_expenditure_type,
              o_expenditure_item_date,
              o_expenditure_organization_id
         FROM po_requisition_headers_all prh,
              po_requisition_lines_all prl,
              po_req_distributions_all prd,
              PA_PROJECTS_ALL ppa,
              PA_TASKS pt
        WHERE prh.requisition_header_id =prl.requisition_header_id
          AND prl.requisition_line_id = prd.requisition_line_id
          AND prh.segment1=p_req_header_reference_num
           AND prl.line_num =p_req_line_reference_num
           AND prd.project_id=ppa.project_id(+)
           AND prd.project_id=pt.project_id(+)
           AND prd.task_id =pt.task_id(+);
       EXCEPTION WHEN OTHERS THEN
         o_project_id := NULL;
         o_project_name :=NULL;
         o_task_id    := NULL;
         o_task_name := NULL;
         o_expenditure_type := NULL;
         o_expenditure_item_date := NULL;
         o_expenditure_organization_id := NULL;
    END;
    ---------------------------------------------------------------------------------------------
    PROCEDURE update_ap_line(p_filename IN VARCHAR2,gWritelog_detail IN OUT tDtinvhublog) IS
      
        CURSOR c_check_dup(p_filename IN VARCHAR2) IS SELECT file_name,ORG_CODE ,GROUP_NAME,
                                                             INVOICE_NUM,LINE_NUMBER,PO_NUMBER,
                                                             PO_LINE_NUMBER,RECEIPT_NUMBER,RECEIPT_LINE_NUMBER,
                                                             VENDOR_NUM,MATCH_OPTION,
                                                             COUNT(*) count_inv,
                                                             row_number() over (partition by INVOICE_NUM,VENDOR_NUM,MATCH_OPTION
                                                                                                         order by INVOICE_NUM) inv_line
                                                        from TAC_INVHUB_INVOICE_STG
                                                        WHERE file_name=p_filename
                                                        AND interface_po_flag='Y'
                                                        AND interface_ap_flag='N'
                                                        AND interface_type='3WAY'
                                                        GROUP BY  file_name,ORG_CODE ,GROUP_NAME,INVOICE_NUM,
                                                                  LINE_NUMBER,PO_NUMBER,PO_LINE_NUMBER,
                                                                  RECEIPT_NUMBER,RECEIPT_LINE_NUMBER,
                                                                  VENDOR_NUM,MATCH_OPTION
                                                        ORDER BY 1,2,3,4,5,6,7,8,9; 
         CURSOR c_check_dup_2w(p_filename IN VARCHAR2) IS SELECT file_name,ORG_CODE ,GROUP_NAME,
                                                             INVOICE_NUM,LINE_NUMBER,PO_NUMBER,
                                                             PO_LINE_NUMBER,RECEIPT_NUMBER,RECEIPT_LINE_NUMBER,
                                                             VENDOR_NUM,MATCH_OPTION,pr_number,pr_line_number,
                                                             COUNT(*) count_inv,
                                                             row_number() over (partition by INVOICE_NUM,VENDOR_NUM,MATCH_OPTION
                                                                                                         order by INVOICE_NUM) inv_line
                                                        from TAC_INVHUB_INVOICE_STG
                                                        WHERE file_name=p_filename
                                                        AND interface_po_flag='Y'
                                                        AND interface_ap_flag='N'
                                                        AND interface_type='2WAY'
                                                        GROUP BY  file_name,ORG_CODE ,GROUP_NAME,INVOICE_NUM,
                                                                  LINE_NUMBER,PO_NUMBER,PO_LINE_NUMBER,
                                                                  RECEIPT_NUMBER,RECEIPT_LINE_NUMBER,
                                                                  VENDOR_NUM,MATCH_OPTION,pr_number,pr_line_number
                                                        ORDER BY 1,2,3,4,5,6,7,8,9;
           l_prev_inv VARCHAR2(50):=NULL;
           l_prev_line NUMBER :=NULL;  
           next_line   NUMBER;      
           p_invlog    DTINVHUB_LOG_DETAIL%ROWTYPE;   
           p_err_msg   VARCHAR2(50);
           p_err_code    VARCHAR2(240);      
           l_starttime  TIMESTAMP;
           l_endtime    TIMESTAMP;          
      BEGIN
         write_log('In update_ap_line' );
         write_log('File type :'||SUBSTR(p_filename,6,2));
         IF SUBSTR(p_filename,6,2)='3W'  THEN
         BEGIN
             write_log('in loop for 3way');
             FOR i IN c_check_dup(p_filename) LOOP
             BEGIN
                -- IF l_prev_inv is NULL OR l_prev_inv!= i.invoice_num THEN
                --    l_prev_inv :=i.invoice_num;
                -- END IF;
                             
                 SELECT current_timestamp
                   INTO l_starttime
                   FROM dual;
                 write_log('i.invoice_num='||i.invoice_num);
                 write_log('i.LINE_NUMBER='||i.LINE_NUMBER);
                 write_log('count='||i.count_inv);
                 IF i.count_inv > 1 THEN
                 BEGIN
                   write_log('Update duplicate record');
                   UPDATE TAC_INVHUB_INVOICE_STG
                      SET interface_ap_flag='E'
                    WHERE file_name=i.file_name
                      AND interface_type='3WAY'
                      AND invoice_num= i.invoice_num
                      AND interface_po_flag='Y'
                      AND interface_ap_flag='N';
               
                    
                    p_invlog.LEVEL_TYPE            := 'D';
                    p_invlog.REQUEST_ID            := FND_GLOBAL.CONC_REQUEST_ID; 
                    p_invlog.ERROR_TYPE            := 'AP Invoice Interface';
                    p_invlog.AP_INVOICE            := i.invoice_num;
                    p_invlog.AP_INVOIE_LINE        := i.LINE_NUMBER;
                    p_invlog.PO_NUMBER             := i.PO_NUMBER;
                    p_invlog.PO_LINE_NUMBER        := i.PO_LINE_NUMBER;
                    p_invlog.RECEIPT_NUMBER        := i.RECEIPT_NUMBER;
                    p_invlog.RECEIPT_LINE_NUM      := i.RECEIPT_LINE_NUMBER;
                    p_invlog.PR_NUMBER             := NULL;
                    p_invlog.PR_LINE_NUMBER        := NULL;
                    p_invlog.MATCH_OPTION          := i.match_option;
                    p_invlog.VENDOR_NUMBER         := i.VENDOR_NUM;
                    p_invlog.VENDOR_SITE_NAME      := null;
                    p_invlog.VENDOR_NAME           := null;
                    p_invlog.DUE_DATE              := sysdate;
                    p_invlog.PAYMENT_STATUS        := null;
                    p_invlog.PV_NUMBER             := null;
                    p_invlog.DOCUMENT_NUMBER       := null;
                    p_invlog.AP_VOUCHER_NUMBER     := null;
                    p_invlog.PAYGROUP              := NULL;
                    p_invlog.PAYMENT_TERM          := NULL;
                    p_invlog.ERROR_CODE            := null;
                    p_invlog.ERROR_MESSAGE         := null;
                    p_invlog.PROCESSING_DATE       := l_starttime;-- v_timestamp_start;
                    
                    SELECT current_timestamp
                       INTO l_endtime
                       FROM dual; 
                       
                    p_invlog.PROCESSING_TIME       := substr(to_char(l_endtime-l_starttime),12,12);--systimestamp - v_timestamp_start;    
                    p_err_msg := 'Duplicate Invoice in ERP';--'*** INTERFACE DATA ERROR : Duplicate Invoice in ERP';
                    p_err_code := 'INVHUB003-003'; --'Error  INVHUB003-003 : Duplicate Invoice in ERP';
                    
                                    
                      
                    write_invdetail_log(p_invlog,gWritelog_detail,p_err_code,p_err_msg);
                      
                 END;
                 ELSE
                 BEGIN
                    
                       UPDATE TAC_INVHUB_INVOICE_STG
                          SET erp_ap_linenumber = i.inv_line
                        WHERE file_name=i.file_name
                          AND interface_type='3WAY'
                          AND invoice_num= i.invoice_num
                          AND LINE_NUMBER=i.line_number
                          AND PO_NUMBER=i.po_number
                          AND PO_LINE_NUMBER=i.PO_LINE_NUMBER
                          AND RECEIPT_NUMBER =i.RECEIPT_NUMBER
                          AND RECEIPT_LINE_NUMBER=i.RECEIPT_LINE_NUMBER
                          AND interface_po_flag='Y'
                          AND interface_ap_flag='N';
                          
                  END;
                  END IF;
                      
                   
             END;
             END LOOP;
         END;
         ELSIF SUBSTR(p_filename,6,2)='2W'  THEN
         BEGIN
            write_log('in loop for 2way');
            FOR i IN c_check_dup_2w(p_filename) LOOP
             BEGIN
                -- IF l_prev_inv is NULL OR l_prev_inv!= i.invoice_num THEN
                --    l_prev_inv :=i.invoice_num;
                -- END IF;
                             
                 SELECT current_timestamp
                   INTO l_starttime
                   FROM dual;
                 write_log('i.invoice_num='||i.invoice_num);
                 write_log('i.LINE_NUMBER='||i.LINE_NUMBER);
                 write_log('count='||i.count_inv);
                 IF i.count_inv > 1 THEN
                 BEGIN
                   write_log('Update duplicate record');
                   UPDATE TAC_INVHUB_INVOICE_STG
                      SET interface_ap_flag='E'
                    WHERE file_name=i.file_name
                      AND interface_type='2WAY'
                      AND invoice_num= i.invoice_num
                      AND interface_po_flag='Y'
                      AND interface_ap_flag='N';
               
                    
                    p_invlog.LEVEL_TYPE            := 'D';
                    p_invlog.REQUEST_ID            := FND_GLOBAL.CONC_REQUEST_ID; 
                    p_invlog.ERROR_TYPE            := 'AP Invoice Interface';
                    p_invlog.AP_INVOICE            := i.invoice_num;
                    p_invlog.AP_INVOIE_LINE        := i.LINE_NUMBER;
                    --p_invlog.PO_NUMBER             := i.PO_NUMBER;
                    --p_invlog.PO_LINE_NUMBER        := i.PO_LINE_NUMBER;
                    --p_invlog.RECEIPT_NUMBER        := i.RECEIPT_NUMBER;
                    --p_invlog.RECEIPT_LINE_NUM      := i.RECEIPT_LINE_NUMBER;
                    p_invlog.PR_NUMBER             := i.pr_number;
                    p_invlog.PR_LINE_NUMBER        := i.pr_line_number;
                    p_invlog.MATCH_OPTION          := i.match_option;
                    p_invlog.VENDOR_NUMBER         := i.VENDOR_NUM;
                    p_invlog.VENDOR_SITE_NAME      := null;
                    p_invlog.VENDOR_NAME           := null;
                    p_invlog.DUE_DATE              := sysdate;
                    p_invlog.PAYMENT_STATUS        := null;
                    p_invlog.PV_NUMBER             := null;
                    p_invlog.DOCUMENT_NUMBER       := null;
                    p_invlog.AP_VOUCHER_NUMBER     := null;
                    p_invlog.PAYGROUP              := NULL;
                    p_invlog.PAYMENT_TERM          := NULL;
                    p_invlog.ERROR_CODE            := null;
                    p_invlog.ERROR_MESSAGE         := null;
                    p_invlog.PROCESSING_DATE       := l_starttime;-- v_timestamp_start;
                    
                    SELECT current_timestamp
                       INTO l_endtime
                       FROM dual; 
                       
                    p_invlog.PROCESSING_TIME       := substr(to_char(l_endtime-l_starttime),12,12);--systimestamp - v_timestamp_start;    
                    p_err_msg := 'Duplicate Invoice in ERP';--'*** INTERFACE DATA ERROR : Duplicate Invoice in ERP';
                    p_err_code := 'INVHUB003-003'; --'Error  INVHUB003-003 : Duplicate Invoice in ERP';
                    
                                    
                      
                    write_invdetail_log(p_invlog,gWritelog_detail,p_err_code,p_err_msg);
                      
                 END;
                 ELSE
                 BEGIN
                    
                       UPDATE TAC_INVHUB_INVOICE_STG
                          SET erp_ap_linenumber = i.inv_line
                        WHERE file_name=i.file_name
                          AND interface_type='2WAY'
                          AND invoice_num= i.invoice_num
                          AND LINE_NUMBER=i.line_number
                          AND pr_number=i.pr_number
                          AND pr_line_number=i.pr_line_number
                           --AND PO_NUMBER=i.po_number
                          --AND PO_LINE_NUMBER=i.PO_LINE_NUMBER
                          --AND RECEIPT_NUMBER =i.RECEIPT_NUMBER
                          --AND RECEIPT_LINE_NUMBER=i.RECEIPT_LINE_NUMBER
                          AND interface_po_flag='Y'
                          AND interface_ap_flag='N';
                          
                  END;
                  END IF;
                      
                   
             END;
             END LOOP;
         END;
         END IF;
         COMMIT;
      END;  
  ----------------------------------------------------------------------------------------------------
  FUNCTION f_get_invoice_description(p_interface_type IN varchar2,
                                     P_PO_NUMBER      IN VARCHAR2,
                                     P_PO_LINE_NUMBER IN NUMBER,
                                     P_PR_NUMBER IN VARCHAR2,
                                     P_PR_LINE_NUMBER IN NUMBER) RETURN VARCHAR2 IS
  
    l_description VARCHAR2(240);
    l_po_header VARCHAR2(40);
    l_po_release NUMBER;
  BEGIN
     IF p_interface_type ='2WAY' THEN
     BEGIN
        SELECT prl.item_description
          INTO l_description
          FROM po_requisition_headers_all prh,po_requisition_lines_all prl
         WHERE prh.requisition_header_id= prl.requisition_header_id
           AND prh.segment1 =P_PR_NUMBER
           AND prl.line_num = NVL(P_PR_LINE_NUMBER,prl.line_num)
           AND ROWNUM=1;
           
         EXCEPTION WHEN no_data_found THEN
           l_description:=NULL;
     END;
     ELSIF p_interface_type ='3WAY' THEN
     BEGIN
         IF instr(P_PO_NUMBER,'-') !=0 THEN
            l_po_header :=  substr(P_PO_NUMBER,1,instr(P_PO_NUMBER,'-')-1);
            l_po_release := substr(p_po_number,instr(P_PO_NUMBER,'-')+1,length(p_po_number)-instr(P_PO_NUMBER,'-')); 
         ELSE 
            l_po_header := P_PO_NUMBER;
            l_po_release := 0;
         END IF;  
         
         
        SELECT pol.item_description
          INTO l_description
          FROM PO_HEADERS_ALL poh
              ,PO_RELEASES_ALL po_release
             , po_lines_all pol
        WHERE poh.po_header_id = pol.po_header_id
          AND poh.org_id=pol.org_id
          AND poh.po_header_id=po_release.po_header_id(+)
          AND poh.org_id=po_release.org_id(+)
          AND poh.segment1 = l_po_header
          AND nvl(po_release.release_num,0) = l_po_release
          AND pol.line_num = NVL(P_PO_LINE_NUMBER,pol.line_num)
          AND ROWNUM=1;
          EXCEPTION WHEN no_data_found THEN
           l_description:=NULL;
     END;
     ELSE
       RETURN(NULL);
     END IF;
     RETURN(l_description);
  END;
  procedure write_log(param_msg varchar2) is
  begin
     fnd_file.put_line(fnd_file.log, param_msg);
  end write_log;
    
  procedure write_invsummary_log(gWritelog_detail IN OUT tDtinvhublog
                                 ,p_current_request_id IN NUMBER
                                 ,p_startprocess IN TIMESTAMP
                                 ) IS
     l_endprocess TIMESTAMP;                            
  BEGIN
       write_log('Total record in gWritelog_detail :'||gWritelog_detail.count);
       if gWritelog_detail.count > 0 then
          forall i in 1..gWritelog_detail.count insert into apps.DTINVHUB_LOG_DETAIL values gWritelog_detail(i);
          write_log('p_current_request_id :'||p_current_request_id);
      end if;
         write_log('Total record in gWritelog_detail :'||gWritelog_detail.count);
      
       SELECT current_timestamp
           INTO l_endprocess
           FROM dual;
      
      INSERT INTO apps.dtinvhub_log_summary
                 (level_type,request_id,program_name,process_date,processing_type,record_count_completion
                 ,record_count_error,processing_date)
      SELECT 'H',request_id,'DTINVHUB003',p_startprocess,'Download', sum(DECODE(ERROR_CODE,NULL,1,0)) COMPLETE,
             sum(DECODE(ERROR_CODE,NULL,0,1)) ERROR
             ,substr(to_char(l_endprocess-p_startprocess),12,12)
        FROM apps.DTINVHUB_LOG_DETAIL WHERE request_id=p_current_request_id
       GROUP BY 'H',request_id,'DTINVHUB003',SYSDATE,'Download',substr(to_char(l_endprocess-p_startprocess),12,15); 
       write_log('Total record in apps.dtinvhub_log_summary :'||SQL%ROWCOUNT); 
         /* forall i in 1..pSummary_log.count 
          insert into apps.DTINVHUB_LOG_SUMMARY values pSummary_log(i);*/
  end;
    
  procedure write_invdetail_log(p_invlog      IN DTINVHUB_LOG_DETAIL%ROWTYPE,
                                gWritelog_detail IN OUT tDtinvhublog,
                                pErr_code     in VARCHAR2,
                                pMessage_err  in VARCHAR2) is
                                  
  
     l_endtime TIMESTAMP;
  BEGIN
      write_log('in write_invdetail_log');
          
     SELECT current_timestamp
       INTO l_endtime
       FROM dual;  
       
      gWritelog_detail.EXTEND;
      gWritelog_detail(gWritelog_detail.count).LEVEL_TYPE            := p_invlog.LEVEL_TYPE;
      gWritelog_detail(gWritelog_detail.count).REQUEST_ID            := p_invlog.REQUEST_ID; 
      gWritelog_detail(gWritelog_detail.count).ERROR_TYPE            := p_invlog.ERROR_TYPE;
      gWritelog_detail(gWritelog_detail.count).AP_INVOICE            := p_invlog.AP_INVOICE;
      gWritelog_detail(gWritelog_detail.count).AP_INVOIE_LINE        := p_invlog.AP_INVOIE_LINE;
      gWritelog_detail(gWritelog_detail.count).PO_NUMBER             := p_invlog.PO_NUMBER;
      gWritelog_detail(gWritelog_detail.count).PO_LINE_NUMBER        := p_invlog.PO_LINE_NUMBER;
      gWritelog_detail(gWritelog_detail.count).RECEIPT_NUMBER        := p_invlog.RECEIPT_NUMBER;
      gWritelog_detail(gWritelog_detail.count).RECEIPT_LINE_NUM      := p_invlog.RECEIPT_LINE_NUM;
      gWritelog_detail(gWritelog_detail.count).PR_NUMBER             := p_invlog.PR_NUMBER;
      gWritelog_detail(gWritelog_detail.count).PR_LINE_NUMBER        := p_invlog.PR_LINE_NUMBER;
      gWritelog_detail(gWritelog_detail.count).MATCH_OPTION          := p_invlog.MATCH_OPTION;
      gWritelog_detail(gWritelog_detail.count).VENDOR_NUMBER         := p_invlog.VENDOR_NUMBER;
      gWritelog_detail(gWritelog_detail.count).VENDOR_SITE_NAME      := p_invlog.VENDOR_SITE_NAME;
      gWritelog_detail(gWritelog_detail.count).VENDOR_NAME           := p_invlog.VENDOR_NAME;
      gWritelog_detail(gWritelog_detail.count).DUE_DATE              := p_invlog.DUE_DATE;
      gWritelog_detail(gWritelog_detail.count).PAYMENT_STATUS        := p_invlog.PAYMENT_STATUS;
      gWritelog_detail(gWritelog_detail.count).PV_NUMBER             := p_invlog.PV_NUMBER;
      gWritelog_detail(gWritelog_detail.count).DOCUMENT_NUMBER       := p_invlog.DOCUMENT_NUMBER;
      gWritelog_detail(gWritelog_detail.count).AP_VOUCHER_NUMBER     := p_invlog.AP_VOUCHER_NUMBER;
      gWritelog_detail(gWritelog_detail.count).PAYGROUP              := p_invlog.PAYGROUP;
      gWritelog_detail(gWritelog_detail.count).PAYMENT_TERM          := p_invlog.PAYMENT_TERM;
      gWritelog_detail(gWritelog_detail.count).ERROR_CODE            := pErr_code;
      gWritelog_detail(gWritelog_detail.count).ERROR_MESSAGE         := pMessage_err;
      gWritelog_detail(gWritelog_detail.count).processing_date       := p_invlog.processing_date;
      gWritelog_detail(gWritelog_detail.count).processing_time       := substr(to_char(l_endtime-p_invlog.processing_date),12,12);
   
  end write_invdetail_log;

  procedure write_out(param_msg varchar2) is
  begin
    fnd_file.put_line(fnd_file.output, param_msg);
  end write_out;
  
  PROCEDURE get_po_information (p_po_number IN po_headers_all.segment1%TYPE,
                                p_po_linenumber IN po_lines_all.line_num%TYPE,
                               -- p_po_header_id OUT po_distributions_all.po_header_id%TYPE,
                               -- p_po_line_id   OUT po_distributions_all.po_line_id%TYPE,
                                p_po_line_loc_id OUT po_distributions_all.line_location_id%TYPE,
                                p_po_dist_id   OUT po_distributions_all.po_distribution_id%TYPE,
                                p_match_option OUT VARCHAR2
                                ) IS
    l_po_header VARCHAR2(40);
    l_po_release NUMBER;                          
   BEGIN                             
      IF p_po_number is NOT NULL AND p_po_linenumber IS NOT NULL THEN
      BEGIN
            
         IF instr(P_PO_NUMBER,'-') !=0 THEN
            l_po_header :=  substr(P_PO_NUMBER,1,instr(P_PO_NUMBER,'-')-1);
            l_po_release := substr(p_po_number,instr(P_PO_NUMBER,'-')+1,length(p_po_number)-instr(P_PO_NUMBER,'-')); 
         ELSE 
            l_po_header := P_PO_NUMBER;
            l_po_release := 0;
         END IF;  
                       
          SELECT --pd.po_header_id,
                 --pd.po_line_id,
                 pd.line_location_id,
                 pd.po_distribution_id,
                 pll.match_option
            INTO --p_po_header_id,
                 --p_po_line_id,
                 p_po_line_loc_id,
                 p_po_dist_id,
                 p_match_option
            FROM po_headers_all poh,
                 po_releases_all po_release,
                 po_lines_all pol,
                 po_line_locations_all pll,
                 po_distributions_all pd
           WHERE poh.po_header_id=pol.po_header_id
             AND poh.org_id =pol.org_id
             AND poh.po_header_id=po_release.po_header_id(+)
             AND poh.org_id=po_release.org_id(+)
             AND pol.po_header_id=pll.po_header_id
             AND pol.po_line_id=pll.po_line_id
             AND pol.org_id = pll.org_id
             AND pll.po_header_id=pd.po_header_id
             AND pll.po_line_id=pd.po_line_id
             AND pll.org_id =pd.org_id
             AND pll.line_location_id=pd.line_location_id
             --AND poh.segment1||DECODE(po_release.release_num,NULL,NULL,'-'||po_release.release_num)=p_po_number
             --AND poh.segment1=p_po_number
             AND poh.segment1 = l_po_header
             AND nvl(po_release.release_num,0) = l_po_release
             AND pol.line_num=p_po_linenumber
             AND ROWNUM=1;
            EXCEPTION WHEN no_data_found THEN
              --p_po_header_id := NULL;
              --p_po_line_id := NULL;
              p_po_line_loc_id := NULL;
              p_po_dist_id := NULL;
              p_match_option :=NULL;
      END;
      END IF;
   END;

   PROCEDURE conc_wait(param_req     in  number
                       --,phase         out varchar2
                       --,dev_phase     out varchar2
                        ) is
    v_result varchar2(1);
    phase varchar2(30);
    status varchar2(30);
    dev_phase varchar2(30);
    dev_status varchar2(30);
    message varchar2(1000);
   begin
      commit;

      if fnd_concurrent.wait_for_request(param_req, 10, 0, phase, status, dev_phase, dev_status, message) then
        null;
      end if;
   END conc_wait;

  ---------------------------------------------------------------------------------------
  procedure insert_invoice(ir_invoice in ap_invoices_interface%rowtype) is
  begin
    insert into ap_invoices_interface
      (invoice_id
      ,invoice_num
      ,invoice_type_lookup_code
      ,invoice_date
      ,po_number
      ,vendor_id
      ,vendor_num
      ,vendor_name
      ,vendor_site_id
      ,vendor_site_code
      ,invoice_amount
      ,invoice_currency_code
      ,exchange_rate
      ,exchange_rate_type
      ,exchange_date
      ,terms_id
      ,terms_name
      ,description
      ,awt_group_id
      ,awt_group_name
      ,last_update_date
      ,last_updated_by
      ,last_update_login
      ,creation_date
      ,created_by
      ,attribute_category
      ,attribute1
      ,attribute2
      ,attribute3
      ,attribute4
      ,attribute5
      ,attribute6
      ,attribute7
      ,attribute8
      ,attribute9
      ,attribute10
      ,attribute11
      ,attribute12
      ,attribute13
      ,attribute14
      ,attribute15
      ,global_attribute_category
      ,global_attribute1
      ,global_attribute2
      ,global_attribute3
      ,global_attribute4
      ,global_attribute5
      ,global_attribute6
      ,global_attribute7
      ,global_attribute8
      ,global_attribute9
      ,global_attribute10
      ,global_attribute11
      ,global_attribute12
      ,global_attribute13
      ,global_attribute14
      ,global_attribute15
      ,global_attribute16
      ,global_attribute17
      ,global_attribute18
      ,global_attribute19
      ,global_attribute20
      ,status
      ,source
      ,group_id
      ,request_id
      ,payment_cross_rate_type
      ,payment_cross_rate_date
      ,payment_cross_rate
      ,payment_currency_code
      ,workflow_flag
      ,doc_category_code
      ,voucher_num
      ,payment_method_lookup_code
      ,pay_group_lookup_code
      ,goods_received_date
      ,invoice_received_date
      ,gl_date
      ,accts_pay_code_combination_id
      ,ussgl_transaction_code
      ,exclusive_payment_flag
      ,org_id
      ,amount_applicable_to_discount
      ,prepay_num
      ,prepay_dist_num
      ,prepay_apply_amount
      ,prepay_gl_date
      ,invoice_includes_prepay_flag
      ,no_xrate_base_amount
      ,vendor_email_address
      ,terms_date
      ,requester_id
      ,ship_to_location
      ,external_doc_ref)
    values
      (ir_invoice.invoice_id
      ,ir_invoice.invoice_num
      ,ir_invoice.invoice_type_lookup_code
      ,ir_invoice.invoice_date
      ,ir_invoice.po_number
      ,ir_invoice.vendor_id
      ,ir_invoice.vendor_num
      ,ir_invoice.vendor_name
      ,ir_invoice.vendor_site_id
      ,ir_invoice.vendor_site_code
      ,round(ir_invoice.invoice_amount,2)
      ,ir_invoice.invoice_currency_code
      ,ir_invoice.exchange_rate
      ,ir_invoice.exchange_rate_type
      ,ir_invoice.exchange_date
      ,ir_invoice.terms_id
      ,ir_invoice.terms_name
      ,ir_invoice.description
      ,ir_invoice.awt_group_id
      ,ir_invoice.awt_group_name
      ,ir_invoice.last_update_date
      ,ir_invoice.last_updated_by
      ,ir_invoice.last_update_login
      ,ir_invoice.creation_date
      ,ir_invoice.created_by
      ,ir_invoice.attribute_category
      ,ir_invoice.attribute1
      ,ir_invoice.attribute2
      ,ir_invoice.attribute3
      ,ir_invoice.attribute4
      ,ir_invoice.attribute5
      ,ir_invoice.attribute6
      ,ir_invoice.attribute7
      ,ir_invoice.attribute8
      ,ir_invoice.attribute9
      ,ir_invoice.attribute10
      ,ir_invoice.attribute11
      ,ir_invoice.attribute12
      ,ir_invoice.attribute13
      ,ir_invoice.attribute14
      ,ir_invoice.attribute15
      ,ir_invoice.global_attribute_category
      ,ir_invoice.global_attribute1
      ,ir_invoice.global_attribute2
      ,ir_invoice.global_attribute3
      ,ir_invoice.global_attribute4
      ,ir_invoice.global_attribute5
      ,ir_invoice.global_attribute6
      ,ir_invoice.global_attribute7
      ,ir_invoice.global_attribute8
      ,ir_invoice.global_attribute9
      ,ir_invoice.global_attribute10
      ,ir_invoice.global_attribute11
      ,ir_invoice.global_attribute12
      ,ir_invoice.global_attribute13
      ,ir_invoice.global_attribute14
      ,ir_invoice.global_attribute15
      ,ir_invoice.global_attribute16
      ,ir_invoice.global_attribute17
      ,ir_invoice.global_attribute18
      ,ir_invoice.global_attribute19
      ,ir_invoice.global_attribute20
      ,ir_invoice.status
      ,ir_invoice.source
      ,ir_invoice.group_id
      ,ir_invoice.request_id
      ,ir_invoice.payment_cross_rate_type
      ,ir_invoice.payment_cross_rate_date
      ,ir_invoice.payment_cross_rate
      ,ir_invoice.payment_currency_code
      ,ir_invoice.workflow_flag
      ,ir_invoice.doc_category_code
      ,ir_invoice.voucher_num
      ,ir_invoice.payment_method_lookup_code
      ,ir_invoice.pay_group_lookup_code
      ,ir_invoice.goods_received_date
      ,ir_invoice.invoice_received_date
      ,ir_invoice.gl_date
      ,ir_invoice.accts_pay_code_combination_id
      ,ir_invoice.ussgl_transaction_code
      ,ir_invoice.exclusive_payment_flag
      ,ir_invoice.org_id
      ,ir_invoice.amount_applicable_to_discount
      ,ir_invoice.prepay_num
      ,ir_invoice.prepay_dist_num
      ,ir_invoice.prepay_apply_amount
      ,ir_invoice.prepay_gl_date
      ,ir_invoice.invoice_includes_prepay_flag
      ,ir_invoice.no_xrate_base_amount
      ,ir_invoice.vendor_email_address
      ,ir_invoice.terms_date
      ,ir_invoice.requester_id
      ,ir_invoice.ship_to_location
      ,ir_invoice.external_doc_ref);
  end insert_invoice;

  ------------------------------------------------------------------------------
  procedure insert_invoice_line(ir_invoice_line in ap_invoice_lines_interface%rowtype) is
  BEGIN
    
    fnd_file.put_line(fnd_file.LOG,'ir_invoice_line.po_header_id:'||ir_invoice_line.po_header_id);
    insert into ap_invoice_lines_interface
      (invoice_id
      ,invoice_line_id
      ,line_number
      ,line_type_lookup_code
      ,line_group_number
      ,amount
      ,accounting_date
      ,description
      ,amount_includes_tax_flag
      ,prorate_across_flag
      ,tax_code
      ,tax_code_id
      ,tax_code_override_flag
      ,tax_recovery_rate
      ,tax_recovery_override_flag
      ,tax_recoverable_flag
      ,final_match_flag
      ,po_header_id
      ,po_line_id
      ,po_line_location_id
      ,PO_DISTRIBUTION_ID
      ,po_unit_of_measure
      ,inventory_item_id
      ,quantity_invoiced
      ,unit_price
      ,distribution_set_id
      ,dist_code_concatenated
      ,dist_code_combination_id
      ,awt_group_id
      ,attribute_category
      ,attribute1
      ,attribute2
      ,attribute3
      ,attribute4
      ,attribute5
      ,attribute6
      ,attribute7
      ,attribute8
      ,attribute9
      ,attribute10
      ,attribute11
      ,attribute12
      ,attribute13
      ,attribute14
      ,attribute15
      ,global_attribute_category
      ,global_attribute1
      ,global_attribute2
      ,global_attribute3
      ,global_attribute4
      ,global_attribute5
      ,global_attribute6
      ,global_attribute7
      ,global_attribute8
      ,global_attribute9
      ,global_attribute10
      ,global_attribute11
      ,global_attribute12
      ,global_attribute13
      ,global_attribute14
      ,global_attribute15
      ,global_attribute16
      ,global_attribute17
      ,global_attribute18
      ,global_attribute19
      ,global_attribute20
      ,po_release_id
      ,balancing_segment
      ,cost_center_segment
      ,account_segment
      ,project_id
      ,task_id
      ,expenditure_type
      ,expenditure_item_date
      ,expenditure_organization_id
      ,project_accounting_context
      ,pa_addition_flag
      ,pa_quantity
      ,stat_amount
      ,type_1099
      ,income_tax_region
      ,assets_tracking_flag
      ,price_correction_flag
      ,ussgl_transaction_code
      ,receipt_number
      ,receipt_line_number
      ,match_option
      ,rcv_transaction_id
      ,creation_date
      ,created_by
      ,last_update_date
      ,last_updated_by
      ,last_update_login
      ,org_id
      ,award_id
      ,price_correct_inv_num
      )
    values
      (ir_invoice_line.invoice_id
      ,ir_invoice_line.invoice_line_id
      ,ir_invoice_line.line_number
      ,ir_invoice_line.line_type_lookup_code
      ,ir_invoice_line.line_group_number
      ,round(ir_invoice_line.amount,2)
      ,ir_invoice_line.accounting_date
      ,ir_invoice_line.description
      ,ir_invoice_line.amount_includes_tax_flag
      ,ir_invoice_line.prorate_across_flag
      ,ir_invoice_line.tax_code
      ,ir_invoice_line.tax_code_id
      ,ir_invoice_line.tax_code_override_flag
      ,ir_invoice_line.tax_recovery_rate
      ,ir_invoice_line.tax_recovery_override_flag
      ,ir_invoice_line.tax_recoverable_flag
      ,ir_invoice_line.final_match_flag
      ,ir_invoice_line.po_header_id
      ,ir_invoice_line.po_line_id
      ,ir_invoice_line.po_line_location_id
      ,ir_invoice_line.po_distribution_id
      ,ir_invoice_line.po_unit_of_measure
      ,ir_invoice_line.inventory_item_id
      ,ir_invoice_line.quantity_invoiced
      ,ir_invoice_line.unit_price
      ,ir_invoice_line.distribution_set_id
      ,ir_invoice_line.dist_code_concatenated
      ,NULL --ir_invoice_line.dist_code_combination_id
      ,ir_invoice_line.awt_group_id
      ,ir_invoice_line.attribute_category
      ,ir_invoice_line.attribute1
      ,ir_invoice_line.attribute2
      ,ir_invoice_line.attribute3
      ,ir_invoice_line.attribute4
      ,ir_invoice_line.attribute5
      ,ir_invoice_line.attribute6
      ,ir_invoice_line.attribute7
      ,ir_invoice_line.attribute8
      ,ir_invoice_line.attribute9
      ,ir_invoice_line.attribute10
      ,ir_invoice_line.attribute11
      ,ir_invoice_line.attribute12
      ,ir_invoice_line.attribute13
      ,ir_invoice_line.attribute14
      ,ir_invoice_line.attribute15
      ,ir_invoice_line.global_attribute_category
      ,ir_invoice_line.global_attribute1
      ,ir_invoice_line.global_attribute2
      ,ir_invoice_line.global_attribute3
      ,ir_invoice_line.global_attribute4
      ,ir_invoice_line.global_attribute5
      ,ir_invoice_line.global_attribute6
      ,ir_invoice_line.global_attribute7
      ,ir_invoice_line.global_attribute8
      ,ir_invoice_line.global_attribute9
      ,ir_invoice_line.global_attribute10
      ,ir_invoice_line.global_attribute11
      ,ir_invoice_line.global_attribute12
      ,ir_invoice_line.global_attribute13
      ,ir_invoice_line.global_attribute14
      ,ir_invoice_line.global_attribute15
      ,ir_invoice_line.global_attribute16
      ,ir_invoice_line.global_attribute17
      ,ir_invoice_line.global_attribute18
      ,ir_invoice_line.global_attribute19
      ,ir_invoice_line.global_attribute20
      ,ir_invoice_line.po_release_id
      ,ir_invoice_line.balancing_segment
      ,ir_invoice_line.cost_center_segment
      ,ir_invoice_line.account_segment
      ,ir_invoice_line.project_id
      ,ir_invoice_line.task_id
      ,ir_invoice_line.expenditure_type
      ,ir_invoice_line.expenditure_item_date
      ,ir_invoice_line.expenditure_organization_id
      ,ir_invoice_line.project_accounting_context
      ,ir_invoice_line.pa_addition_flag
      ,ir_invoice_line.pa_quantity
      ,ir_invoice_line.stat_amount
      ,ir_invoice_line.type_1099
      ,ir_invoice_line.income_tax_region
      ,ir_invoice_line.assets_tracking_flag
      ,ir_invoice_line.price_correction_flag
      ,ir_invoice_line.ussgl_transaction_code
      ,ir_invoice_line.receipt_number
      ,ir_invoice_line.receipt_line_number
      ,ir_invoice_line.match_option
      ,ir_invoice_line.rcv_transaction_id
      ,ir_invoice_line.creation_date
      ,ir_invoice_line.created_by
      ,ir_invoice_line.last_update_date
      ,ir_invoice_line.last_updated_by
      ,ir_invoice_line.last_update_login
      ,ir_invoice_line.org_id
      ,ir_invoice_line.award_id
      ,ir_invoice_line.price_correct_inv_num
     );
  end insert_invoice_line;


  procedure update_staging_status(i_inv_num         varchar2,
                                  i_vendor_num      VARCHAR2,
                                    i_source          varchar2,   
                                    i_interface_type  varchar2,
                                    i_line_num        number := null,
                                    i_status          varchar2,
                                    i_filename        VARCHAR2,
                                    i_org             VARCHAR2) is
    pragma autonomous_transaction;
  begin
    update TAC_INVHUB_INVOICE_STG
    set interface_ap_flag = i_status
    where 1=1
        and source            = i_source
        and interface_type    = i_interface_type
      and invoice_num       = i_inv_num
      AND VENDOR_NUM        = i_vendor_num
        and line_number       = nvl(i_line_num, line_number)
        AND file_name         = i_filename
        AND org_code          = i_org
    and interface_po_flag = 'Y';
        commit;
  exception
    when others then
      write_log('Can not update interface status');
      rollback;
  end update_staging_status;

  function get_mapping_org_id_acccode(p_org_id number) return varchar2 is
  begin
     if (p_org_id = 142) then
         return '08';
        elsif (p_org_id = 218) then
         return '12';
        elsif (p_org_id = 238) then
         return '12';
        else
         return '01';
     end if;
  exception
     when others then
         return '01';
  END get_mapping_org_id_acccode;
  
  PROCEDURE validate_invoice_amount(p_filename IN VARCHAR2,gWritelog_detail IN OUT tDtinvhublog) IS
    
     CURSOR c1(p_filename IN VARCHAR2) IS 
      SELECT stg.file_name,stg.invoice_num,round(stg.invoice_amount,2) invoice_amount,
             round(sum(stg.line_amount),2) ori_line_amt,
             round(sum(stg.line_amount+((stg.line_amount*pltax.tax_rate)/100)),2) sum_line_amt,pltax.tax_rate
       FROM TAC_INVHUB_INVOICE_STG stg,
            po_headers_all POH,
            po_releases_all po_release,
            PO_lines_all POL,
            (SELECT DISTINCT pll.po_header_id,pll.po_line_id,atc.tax_rate
               FROM  po_line_locations_all PLL,
                     AP_TAX_CODES_ALL atc
              WHERE  pll.tax_code_id=atc.tax_id
            ) pltax
      WHERE stg.file_name =p_filename
      
      AND stg.po_number = poh.segment1||DECODE(po_release.release_num,NULL,NULL,'-'||po_release.release_num) --poh.segment1
      AND stg.po_line_number = pol.line_num
      AND poh.po_header_id=pol.po_header_id
      AND poh.org_id =pol.org_id
      AND poh.po_header_id=po_release.po_header_id(+)
      AND poh.org_id=po_release.org_id(+)
      AND pol.po_header_id=pltax.po_header_id
      AND pol.po_line_id=pltax.po_line_id
       AND stg.interface_po_flag='Y'
      AND stg.interface_ap_flag='N'
      GROUP BY stg.file_name,stg.invoice_num,stg.invoice_amount,pltax.tax_rate;
      
       p_invlog    DTINVHUB_LOG_DETAIL%ROWTYPE;
       p_err_msg   VARCHAR2(50);
       p_err_code    VARCHAR2(240);
       l_starttime  TIMESTAMP;
       l_endtime    TIMESTAMP;
     CURSOR c2(p_filename IN VARCHAR2) IS SELECT invoice_num,
                         LINE_NUMBER,
                         PO_NUMBER,
                         PO_LINE_NUMBER,
                         RECEIPT_NUMBER,
                         RECEIPT_LINE_NUMBER,
                         PR_NUMBER,
                         PR_LINE_NUMBER,
                         PAYMENT_METHOD_LOOKUP_CODE,
                         TERMS_NAME,
                         MATCH_OPTION,
                         vendor_num
                    FROM TAC_INVHUB_INVOICE_STG stg
                   WHERE stg.file_name =p_filename
                     AND stg.interface_po_flag='Y'
                     AND stg.interface_ap_flag='P';
                     
                    
  BEGIN
     write_log('validate_invoice_amount');
     write_log('p_filename='||p_filename);
     FOR i_check_inv IN c1(p_filename) LOOP
     BEGIN
                
         SELECT current_timestamp
           INTO l_starttime
           FROM dual;
        write_log('invoice_Num = '||i_check_inv.invoice_num); 
        write_log('invoice_amount = '||i_check_inv.invoice_amount);  
        write_log('line_amount = '||i_check_inv.ori_line_amt);
        
        IF i_check_inv.invoice_amount != i_check_inv.ori_line_amt THEN
        BEGIN  
            IF trunc(i_check_inv.invoice_amount) != trunc(i_check_inv.sum_line_amt) THEN
               UPDATE TAC_INVHUB_INVOICE_STG
                  SET interface_ap_flag='P'
                WHERE file_name=i_check_inv.file_name
                  AND invoice_Num = i_check_inv.invoice_num;
            END IF;
        END;    
        END IF;
     END;
     END LOOP;
     write_log('After update TAC_INVHUB_INVOICE_STG' );
      
     FOR j IN c2 (p_filename) LOOP
              
       
       SELECT current_timestamp
         INTO l_endtime
         FROM dual;  
        p_invlog.LEVEL_TYPE            := 'D';
        p_invlog.REQUEST_ID            := FND_GLOBAL.CONC_REQUEST_ID; 
        p_invlog.ERROR_TYPE            := 'AP Invoice Interface';
        p_invlog.AP_INVOICE            := j.invoice_num;
        p_invlog.AP_INVOIE_LINE        := j.LINE_NUMBER;
        p_invlog.PO_NUMBER             := j.PO_NUMBER;
        p_invlog.PO_LINE_NUMBER        := j.PO_LINE_NUMBER;
        p_invlog.RECEIPT_NUMBER        := j.RECEIPT_NUMBER;
        p_invlog.RECEIPT_LINE_NUM      := j.RECEIPT_LINE_NUMBER;
        p_invlog.PR_NUMBER             := j.PR_NUMBER;
        p_invlog.PR_LINE_NUMBER        := j.PR_LINE_NUMBER;
        p_invlog.MATCH_OPTION          := j.MATCH_OPTION;
        p_invlog.VENDOR_NUMBER         := j.vendor_num;
        p_invlog.VENDOR_SITE_NAME      := null;
        p_invlog.VENDOR_NAME           := null;
        p_invlog.DUE_DATE              := sysdate;
        p_invlog.PAYMENT_STATUS        := null;
        p_invlog.PV_NUMBER             := null;
        p_invlog.DOCUMENT_NUMBER       := null;
        p_invlog.AP_VOUCHER_NUMBER     := null;
        p_invlog.PAYGROUP              := j.PAYMENT_METHOD_LOOKUP_CODE;
        p_invlog.PAYMENT_TERM          := j.TERMS_NAME;
        p_invlog.ERROR_CODE            := null;
        p_invlog.ERROR_MESSAGE         := null;
        p_invlog.PROCESSING_DATE       := l_starttime;-- v_timestamp_start;
        p_invlog.PROCESSING_TIME       := substr(to_char(l_endtime-l_starttime),12,12);--systimestamp - v_timestamp_start;    
        p_err_msg := 'Line amount is not equal to Header amount';--'*** INTERFACE DATA ERROR : Duplicate Invoice in ERP';
        p_err_code := 'INVHUB003-005'; --'Error  INVHUB003-003 : Duplicate Invoice in ERP';
        write_invdetail_log(p_invlog,gWritelog_detail,p_err_code,p_err_msg);
    END LOOP;
    
    UPDATE TAC_INVHUB_INVOICE_STG
                  SET interface_ap_flag='E'
                WHERE file_name=p_filename
                  AND interface_ap_flag='P';
    COMMIT;
    write_log('end validate_invoice_amount');
  END;
  PROCEDURE get_error_from_interface(gWritelog_detail IN OUT tDtinvhublog
                                    ,v_group IN VARCHAR2
                                    ,v_starttime IN TIMESTAMP
                                    ,v_current_request_id IN NUMBER
                                    ,v_dtac_req_ID IN NUMBER
                                    ,v_dtn_req_ID IN  NUMBER
                                    ,v_psb_req_id IN NUMBER) IS
     l_log_interface DTINVHUB_LOG_DETAIL%ROWTYPE;
     p_err_msg   VARCHAR2(50);
     p_err_code    VARCHAR2(240); 
     l_endtime TIMESTAMP;
     CURSOR c_error(p_group_id IN VARCHAR2,
                    p_dtac_req_id IN NUMBER,
                    p_dtn_req_id in NUMBER,
                    p_psb_req_id IN NUMBER
                    ) IS SELECT DISTINCT ai.invoice_num,
                                                      ail.line_number,
                                                      stg.PO_NUMBER,
                                                      stg.PO_LINE_NUMBER,
                                                      stg.RECEIPT_NUMBER,
                                                      stg.RECEIPT_LINE_NUMBER,
                                                      stg.pr_number,
                                                      stg.pr_line_number,
                                                      stg.match_option,
                                                      stg.vendor_num,
                                                      air.reject_lookup_code,
                                                      STG.INTERFACE_TYPE
                         FROM tac_invhub_invoice_stg stg,
                              ap_invoices_interface Ai,
                              ap_invoice_lines_interface ail,
                              AP_INTERFACE_REJECTIONS air
                        WHERE stg.invoice_num= ai.invoice_num
                              AND nvl(stg.erp_ap_linenumber,stg.line_number)=ail.line_number
                              AND ai.invoice_id=ail.invoice_id(+)
                              AND  ai.invoice_id = air.parent_id
                              AND air.parent_table='AP_INVOICES_INTERFACE'
                              AND ai.status='REJECTED'
                              AND ai.source='FXTH'
                              AND ai.group_id=p_group_id
                              AND stg.group_name=p_group_id
                              AND request_id in (p_dtac_req_id,p_dtn_req_id,p_psb_req_id)
                        UNION
                       SELECT DISTINCT ai.invoice_num,
                              ail.line_number,
                              stg.PO_NUMBER,
                              stg.PO_LINE_NUMBER,
                              stg.RECEIPT_NUMBER,
                              stg.RECEIPT_LINE_NUMBER,
                              stg.pr_number,
                              stg.pr_line_number,
                              stg.match_option,
                              stg.vendor_num,
                              air.reject_lookup_code,
                              STG.INTERFACE_TYPE
                         FROM tac_invhub_invoice_stg stg,
                              ap_invoices_interface Ai,
                              ap_invoice_lines_interface ail,
                              AP_INTERFACE_REJECTIONS air
                        WHERE stg.invoice_num= ai.invoice_num
                          AND nvl(stg.erp_ap_linenumber,stg.line_number)=ail.line_number
                          AND ai.invoice_id=ail.invoice_id(+)
                          AND  ail.invoice_line_id = air.parent_id
                          AND air.parent_table='AP_INVOICE_LINES_INTERFACE'
                          AND ai.source='FXTH'
                          AND ai.status='REJECTED'
                          AND ai.group_id=p_group_id
                          AND stg.group_name=p_group_id
                          AND request_id in (p_dtac_req_id,p_dtn_req_id,p_psb_req_id); 
                          
      CURSOR c_complete(p_group_id IN VARCHAR2,
                    p_dtac_req_id IN NUMBER,
                    p_dtn_req_id in NUMBER,
                    p_psb_req_id IN NUMBER
                    ) IS                        
              SELECT DISTINCT ai.invoice_num,
                              ail.line_number,
                              stg.PO_NUMBER,
                              stg.PO_LINE_NUMBER,
                              stg.RECEIPT_NUMBER,
                              stg.RECEIPT_LINE_NUMBER,
                              stg.pr_number,
                              stg.pr_line_number,
                              stg.match_option,
                              stg.vendor_num,
                               STG.INTERFACE_TYPE
                         FROM tac_invhub_invoice_stg stg,
                              ap_invoices_interface Ai,
                              ap_invoice_lines_interface ail
                        WHERE stg.invoice_num= ai.invoice_num
                              AND nvl(stg.erp_ap_linenumber,stg.line_number)=ail.line_number
                              AND ai.invoice_id=ail.invoice_id(+)
                              AND ai.source='FXTH'
                              AND ai.status='PROCESSED'
                              AND ai.group_id=p_group_id
                              AND stg.group_name=p_group_id
                              AND request_id in (p_dtac_req_id,p_dtn_req_id,p_psb_req_id)
                              ORDER BY ai.invoice_num,ail.line_number;
  BEGIN
      write_log('in get_error_from_interface');
     FOR i IN c_error(v_group
                     ,nvl(v_dtac_req_ID,0)
                     ,nvl(v_dtn_req_ID,0)
                     ,nvl(v_psb_req_id,0)) LOOP
     BEGIN
          write_log('in get_error_from_interface with status error');
          l_log_interface.LEVEL_TYPE            := 'D';
          l_log_interface.REQUEST_ID            := v_current_request_id; 
          l_log_interface.ERROR_TYPE            := 'AP Invoice Interface';
          l_log_interface.AP_INVOICE            := i.invoice_num;
          l_log_interface.AP_INVOIE_LINE        := i.LINE_NUMBER;
          --IF substr(v_group,
          IF SUBSTR(v_group,6,2)='3W' THEN
              l_log_interface.PO_NUMBER             := i.PO_NUMBER;
              l_log_interface.PO_LINE_NUMBER        := i.PO_LINE_NUMBER;
              l_log_interface.RECEIPT_NUMBER        := i.RECEIPT_NUMBER;
              l_log_interface.RECEIPT_LINE_NUM      := i.RECEIPT_LINE_NUMBER;
          ELSIF SUBSTR(v_group,6,2)='2W' THEN
            
              l_log_interface.PR_NUMBER             := i.pr_number;
              l_log_interface.PR_LINE_NUMBER        := i.pr_line_number;
          END IF;
          l_log_interface.MATCH_OPTION          := i.match_option;
          l_log_interface.VENDOR_NUMBER         := i.VENDOR_NUM;
          l_log_interface.VENDOR_SITE_NAME      := null;
          l_log_interface.VENDOR_NAME           := null;
          l_log_interface.DUE_DATE              := sysdate;
          l_log_interface.PAYMENT_STATUS        := null;
          l_log_interface.PV_NUMBER             := null;
          l_log_interface.DOCUMENT_NUMBER       := null;
          l_log_interface.AP_VOUCHER_NUMBER     := null;
          l_log_interface.PAYGROUP              := NULL;
          l_log_interface.PAYMENT_TERM          := NULL;
          l_log_interface.ERROR_CODE            := null;
          l_log_interface.ERROR_MESSAGE         := null;
          l_log_interface.PROCESSING_DATE       := v_starttime;

          SELECT current_timestamp
             INTO l_endtime
             FROM dual;
          l_log_interface.PROCESSING_TIME       := substr(to_char(l_endtime-v_starttime),12,12);
          p_err_msg := i.reject_lookup_code;--'*** INTERFACE DATA ERROR : Duplicate Invoice in ERP';
          p_err_code := 'INVHUB003-040'; 
          write_log('in p_err_code:'||p_err_code||'-'||p_err_msg);
          write_invdetail_log(p_invlog => l_log_interface,
                              gWritelog_detail => gWritelog_detail,
                              pErr_code =>p_err_code,
                              pMessage_err  => p_err_msg);
         
         update TAC_INVHUB_INVOICE_STG
            set interface_ap_flag = 'E'
          where 1=1
            and source            = 'FXTH'
            and interface_type    = I.INTERFACE_TYPE
          and invoice_num       = i.invoice_num
          AND VENDOR_NUM        = i.VENDOR_NUM
            --and line_number       = nvl(i_line_num, line_number)
            AND group_name         = v_group
        and interface_po_flag = 'Y';                     
     END;
     END LOOP;
     
     FOR j IN c_complete(v_group
               ,nvl(v_dtac_req_ID,0)
               ,nvl(v_dtn_req_ID,0)
               ,nvl(v_psb_req_id,0)) LOOP
     BEGIN
          write_log('in get_error_from_interface with status completed');
          l_log_interface.LEVEL_TYPE            := 'D';
          l_log_interface.REQUEST_ID            := v_current_request_id; 
          l_log_interface.ERROR_TYPE            := 'AP Invoice Interface';
          l_log_interface.AP_INVOICE            := j.invoice_num;
          l_log_interface.AP_INVOIE_LINE        := j.LINE_NUMBER;
          IF SUBSTR(v_group,6,2)='3W' THEN
              l_log_interface.PO_NUMBER             := j.PO_NUMBER;
              l_log_interface.PO_LINE_NUMBER        := j.PO_LINE_NUMBER;
              l_log_interface.RECEIPT_NUMBER        := j.RECEIPT_NUMBER;
              l_log_interface.RECEIPT_LINE_NUM      := j.RECEIPT_LINE_NUMBER;
          ELSIF SUBSTR(v_group,6,2)='2W' THEN
            
              l_log_interface.PR_NUMBER             := j.pr_number;
              l_log_interface.PR_LINE_NUMBER        := j.pr_line_number;
          END IF;
          
          
          l_log_interface.MATCH_OPTION          := j.match_option;
          l_log_interface.VENDOR_NUMBER         := j.VENDOR_NUM;
          l_log_interface.VENDOR_SITE_NAME      := null;
          l_log_interface.VENDOR_NAME           := null;
          l_log_interface.DUE_DATE              := sysdate;
          l_log_interface.PAYMENT_STATUS        := null;
          l_log_interface.PV_NUMBER             := null;
          l_log_interface.DOCUMENT_NUMBER       := null;
          l_log_interface.AP_VOUCHER_NUMBER     := null;
          l_log_interface.PAYGROUP              := NULL;
          l_log_interface.PAYMENT_TERM          := NULL;
          l_log_interface.ERROR_CODE            := null;
          l_log_interface.ERROR_MESSAGE         := null;
          l_log_interface.PROCESSING_DATE       := v_starttime;

          SELECT current_timestamp
             INTO l_endtime
             FROM dual;
          l_log_interface.PROCESSING_TIME       := substr(to_char(l_endtime-v_starttime),12,12);
       
          write_invdetail_log(p_invlog => l_log_interface,
                              gWritelog_detail => gWritelog_detail,
                              pErr_code =>NULL,
                              pMessage_err  => NULL);
     END;
     END LOOP;
         
           
    
         
         
     
  END;

  PROCEDURE interface_ap_invoicehub
  ( err_msg             out varchar2
   ,err_code            out varchar2
   ,i_company_code      in varchar2
   ,i_source            in varchar2 --FXTH
   ,i_interface_type    in varchar2 --2way or 3way
   ,i_file_name         in varchar2
   ,i_rerun             in varchar2
   ,i_path              in varchar2
  ) is
    
    CURSOR c_readfile(p_company_code    IN VARCHAR2,
                      p_source          IN VARCHAR2,
                      p_interface_type  IN VARCHAR2,
                      p_filename        IN VARCHAR2
                      
                     ) IS
      SELECT DISTINCT file_name
        FROM TAC_INVHUB_INVOICE_STG
       WHERE 1=1--org_code = p_company_code
         AND SOURCE= p_source
         AND interface_type = p_interface_type
         AND interface_po_flag='Y'
         AND interface_ap_flag='N'
         AND file_name = NVL(p_filename,File_name) --i_file_name,file_name)
         AND file_name IS NOT NULL
      ORDER BY file_name;

     --v_mapped_org_id_acc_code varchar2(10);
    cursor c_inv(p_source          IN VARCHAR2,
                 p_interface_type  IN VARCHAR2,
                 p_filename IN VARCHAR2) is
      select inv.ORG_CODE,
                   inv.GROUP_NAME,
                   inv.SOURCE,
                   inv.INVOICE_TYPE_LOOKUP_CODE, 
                   inv.VENDOR_NUM,
                   inv.VENDOR_SITE_CODE,
                   inv.INVOICE_NUM,
                   inv.INVOICE_DATE,
                   inv.DOC_CATEGORY_CODE,
                   inv.INVOICE_CURRENCY_CODE,
                   inv.INVOICE_AMOUNT  invoice_amount,--sum(inv.INVOICE_AMOUNT)  invoice_amount,
                   inv.GL_DATE,
                   inv.INVOICE_DESCRIPTION,
                   inv.TERMS_NAME,
                   inv.PAYMENT_METHOD_LOOKUP_CODE,
                   inv.AWT_GROUP_NAME,
                   inv.MAIL_TO_NAME,
                  -- inv.PR_NUMBER,
                   --inv.PO_NUMBER,
                  -- inv.RECEIPT_NUMBER,
                   count('x') num_of_dist
      from TAC_INVHUB_INVOICE_STG inv
      where inv.source            = p_source
            and   inv.INTERFACE_TYPE    = p_interface_type
            and   inv.INTERFACE_PO_FLAG = 'Y'
            and   nvl(inv.INTERFACE_AP_FLAG,'N') = 'N'
            AND file_name=p_filename
      group by inv.ORG_CODE,
                   inv.GROUP_NAME,
                   inv.SOURCE,
                   inv.INVOICE_TYPE_LOOKUP_CODE, 
                   inv.VENDOR_NUM,
                   inv.VENDOR_SITE_CODE,
                   inv.INVOICE_NUM,
                   inv.INVOICE_DATE,
                   inv.DOC_CATEGORY_CODE,
                   inv.INVOICE_CURRENCY_CODE,
                   inv.INVOICE_AMOUNT,
                   inv.GL_DATE,
                   inv.INVOICE_DESCRIPTION,
                   inv.TERMS_NAME,
                   inv.PAYMENT_METHOD_LOOKUP_CODE,
                   inv.AWT_GROUP_NAME,
                   inv.MAIL_TO_NAME--,
                 --  inv.PR_NUMBER
                  -- inv.PO_NUMBER,
                  -- inv.RECEIPT_NUMBER
                  ;

    cursor c_line( p_source          IN VARCHAR2,
                   p_interface_type  IN VARCHAR2,
                   p_filename IN VARCHAR2,
                   p_inv_num in varchar2,
                   p_vendor_num IN VARCHAR2,
                   p_org IN VARCHAR2) is
      select l.ORG_CODE,
                   l.GROUP_NAME, 
                   l.SOURCE, 
                   l.INVOICE_TYPE_LOOKUP_CODE,
                   l.VENDOR_NUM,
                   l.VENDOR_SITE_CODE,
                   l.INVOICE_NUM,
                   l.INVOICE_DATE,
                   l.DOC_CATEGORY_CODE,
                   l.INVOICE_CURRENCY_CODE,
                   l.INVOICE_AMOUNT,
                   l.GL_DATE,
                   l.INVOICE_DESCRIPTION ,
                   l.TERMS_NAME,
                   l.PAYMENT_METHOD_LOOKUP_CODE,
                   l.LINE_NUMBER,
                   l.LINE_TYPE_LOOKUP_CODE,
                   l.LINE_AMOUNT,
                   l.LINE_TAX_CODE,
                   l.AWT_GROUP_NAME,
                   l.DIST_CODE_CONCATINATED,
                   l.LINE_DESCRIPTION,
                   l.ACT_VENDOR_NUM,
                   l.ACT_VENDOR_SITE_CODE,
                   decode(l.ORG_CODE, --l.REPORTING_ENTITY,
                         'DTAC',10000,
                         'DTN',10050,
                         'PSB',10094) REPORTING_ENTITY,
                   l.TAX_INVOICE_DATE,
                   l.TAX_INVOICE_NUM,
                   l.TAX_ACCT_PERIOD,
                   l.WHT_ACCT_PERIOD,
                   l.PHOR_NGOR_DOR,
                   l.WHT_CONDITION,
                   l.WHT_REVENUE_TYPE,
                   l.WHT_REVENUE_NAME,
                   l.PROJECT_NUMBER,
                   l.PROJECT_TASK_NUMBER,
                   l.EXPENDITURE_TYPE,
                   l.EXPENDITURE_ORGANIZATION,
                   l.EAI_CRTD_DTTM,
                   l.PAYEE,
                   l.MAIL_TO_NAME,
                   l.MAIL_TO_ADDRESS1,
                   l.MAIL_TO_ADDRESS2,
                   l.MAIL_TO_ADDRESS3,
                   l.MAIL_TO_ADDRESS4,                
                   l.PO_NUMBER,
                   l.PO_LINE_NUMBER,
                   l.RECEIPT_NUMBER,
                   l.RECEIPT_LINE_NUMBER,
                   l.PR_NUMBER,                  
                   l.PR_LINE_NUMBER,
                   l.MATCH_OPTION,
                   l.BILL_PLACEMENT_DATE,
                   l.INTERFACE_PO_FLAG,
                   l.INTERFACE_AP_FLAG,
                   l.FILE_NAME,
                   l.INTERFACE_TYPE,
                   hou.organization_id,
                   l.erp_ap_linenumber
      from TAC_INVHUB_INVOICE_STG l
          ,hr_organization_units hou
      where l.invoice_num       = p_inv_num
        and l.source            = p_source
        AND l.VENDOR_NUM        = p_vendor_num
        and l.INTERFACE_TYPE    = p_interface_type
        and l.INTERFACE_PO_FLAG = 'Y'
        and NVL(l.INTERFACE_AP_FLAG,'N') = 'N'
        and l.expenditure_organization = hou.name(+)
        AND l.file_name=p_filename
        AND l.org_code=p_org
      order by l.LINE_NUMBER;

    r_invoice               ap_invoices_interface%rowtype;
    r_invoice_line          ap_invoice_lines_interface%rowtype;
    v_group                 varchar2(80);
    v_source                varchar2(80);
    i_prog_name             varchar2(30) := 'DTINVHUB003';
        
    v_rate                  number;
    v_tax_ccid              number;
    v_line_num              number;
    v_site_auto_calc_flag   varchar2(10);
    v_supp_name             varchar2(100);
    v_period                varchar2(30);
    v_proj_id               number(20);
    v_task_id               number(20);
    v_expen_type            varchar(100);
    v_expen_org             number(20);
    v_flag                  boolean := false;
    v_chk_line              boolean := false;
    v_validate_error        boolean := false;
    ap_req_id               number;
    c_ap_req_id             number;
    v_i_group               varchar2(80);
    v_ap_req_id             number;
    v_line_count            number := 0;
    v_line_count_err        number := 0;
    v_site                  po_vendor_sites_all%rowtype;
    v_save_pnt              varchar2(100);
    v_tax_rate              number;
    v_requester_id          number;
    v_ecpenditure_org       number;
    v_receipt_num           rcv_shipment_headers.receipt_num%type;
    v_count                 number := 0;
    v_total_line_tax_amt    number := 0; 
    v_line_status_info      varchar2(32000);
    v_line_error            varchar2(32000);
    v_header_error          varchar2(32000);
    v_count_invalid_header  number := 0;
    v_inv_line_error_list   varchar2(10000);
    v_dist_acc_code         number;
    v_dist_acc_concate      gl_code_combinations_kfv.CONCATENATED_SEGMENTS%type;
    v_chek_inv              varchar2(1);
    v_check_period          varchar2(1);
    v_check_currency        varchar2(1);
    v_check_source          varchar2(1);
    v_check_po_number       varchar2(1);
    v_check_grn             VARCHAR2(1);
    v_conc_request          number := FND_GLOBAL.CONC_REQUEST_ID;
    v_timestamp_start       date := SYSTIMESTAMP;
    v_timestamp_end         date;
   -- tDtinvlog               tDtinvhublog  := tDtinvhublog();
    tSuminvlog              tSuminvhublog := tSuminvhublog();
    gWritelog_detail     tDtinvhublog := tDtinvhublog();
    v_firstline_flag        VARCHAR2(1):='N';
    r_invlog                DTINVHUB_LOG_DETAIL%ROWTYPE;
    l_project_id       po_req_distributions_all.project_id%TYPE;
    l_project_name     pa_projects_all.segment1%TYPE;
    l_task_id          po_req_distributions_all.task_id%TYPE;
    l_task_name        pa_tasks.task_number%TYPE;
    l_expenditure_type po_req_distributions_all.expenditure_type%TYPE;
    l_expenditure_item_date  po_req_distributions_all.expenditure_item_date%TYPE;
    l_expenditure_organization_id  po_req_distributions_all.expenditure_organization_id%TYPE;
    l_count_dtac    NUMBER;
    l_count_dtn     NUMBER;
    l_count_psb     NUMBER;
    l_resp_id       NUMBER;
    l_resp_app_id   NUMBER;
    p_current_request_id NUMBER;
    l_check_decimal NUMBER;
    --l_start_process TIMESTAMP;
    l_starttime     TIMESTAMP;
    l_endtime       TIMESTAMP;
    l_match_option VARCHAR2(1);
     l_po_header VARCHAR2(40);
    l_po_release NUMBER;
    l_dtac_req_ID NUMBER;
    l_dtn_req_ID NUMBER;
    l_psb_req_id NUMBER;
  begin
    --Initial
    g_step :='start program'; 
    l_starttime :=SYSDATE;   
    savepoint b4_process;
    write_log(' ');
    write_log('+----------------------------------------------------------------------------+');
    write_log('Start AP Invoice Interface from INVOICE HUB FXTH');
    write_log('Start Date: ' || to_char(sysdate, 'DD/MM/YYYY HH24:MI:SS'));
    write_log(' ');
    write_log('*****Parameter*****');
    write_log('--------------------');
    write_log('Company Code: ' || i_company_code);
    write_log('Source: ' || i_source);
    write_log('Interface Type: ' || i_interface_type);
    write_log('File Name: ' || i_file_name);
    write_log('Rerun: ' || i_rerun);
    write_log('Path: ' || i_path);
    write_log('+----------------------------------------------------------------------------+');
    p_current_request_id:= fnd_global.CONC_REQUEST_ID;
    FOR r_readfile IN c_readfile(i_company_code,
                                 i_source,
                                 i_interface_type,
                                 i_file_name
                                 ) LOOP
                              
                                 
    BEGIN
        g_step :='read file'; 
        write_log('i_company_code='||i_company_code);
        write_log('i_source='||i_source);
        write_log('i_interface_type='||i_interface_type);
        write_log('i_file_name='||i_file_name);
        -- validate invoice amount and invoice line amount
        validate_invoice_amount(r_readfile.file_name,gWritelog_detail); 
        update_ap_line(r_readfile.file_name,gWritelog_detail);
        
    for r_inv in c_inv(i_source,
                       i_interface_type,
                       r_readfile.file_name)
    LOOP 
       g_step :='fetch invoice';
       write_log(g_step);
       err_code := null;
       err_msg  := null;
        
       v_save_pnt := r_inv.invoice_num || r_inv.vendor_num;
       savepoint v_save_pnt;

       r_invoice := null;
       v_flag := false;
            
       write_log('Invoice Number : ' || r_inv.invoice_num);
       write_log('Vendor num :'||r_inv.vendor_num);
       v_group := r_inv.group_name;
       v_source := r_inv.source;
       v_line_num := 0;
        ---r_invoice.invoice_amount := 0;
        v_chk_line := false;
        g_step :='Fetch line';
        v_firstline_flag:='Y';
        write_log('r_readfile.file_name : ' ||r_readfile.file_name);
        write_log('r_invoice.invoice_num : ' || r_inv.invoice_num);
        write_log('i_source : ' || i_source);
        write_log('i_interface_type : ' ||i_interface_type);
        write_log('r_inv.vendor_num : ' || r_inv.vendor_num);    
        for r_line in c_line(i_source,
                             i_interface_type,
                             r_readfile.file_name,
                             r_inv.invoice_num,
                             r_inv.vendor_num,
                             r_inv.ORG_CODE)
        LOOP
          write_log('v_firstline_flag='||v_firstline_flag);
                         
                         
             SELECT current_timestamp
               INTO l_starttime
               FROM dual;
               
             r_invlog.LEVEL_TYPE            := 'D';
              r_invlog.REQUEST_ID            := v_conc_request; 
              r_invlog.ERROR_TYPE            := 'AP Invoice Interface';
              r_invlog.AP_INVOICE            := r_inv.invoice_num;
              r_invlog.AP_INVOIE_LINE        := r_line.LINE_NUMBER;
              r_invlog.PO_NUMBER             := r_line.PO_NUMBER;
              r_invlog.PO_LINE_NUMBER        := r_line.PO_LINE_NUMBER;
              r_invlog.RECEIPT_NUMBER        := r_line.RECEIPT_NUMBER;
              r_invlog.RECEIPT_LINE_NUM      := r_line.RECEIPT_LINE_NUMBER;
              r_invlog.PR_NUMBER             := r_line.PR_NUMBER;
              r_invlog.PR_LINE_NUMBER        := r_line.PR_LINE_NUMBER;
              r_invlog.MATCH_OPTION          := r_line.MATCH_OPTION;
              r_invlog.VENDOR_NUMBER         := r_inv.vendor_num;
              r_invlog.VENDOR_SITE_NAME      := null;
              r_invlog.VENDOR_NAME           := null;
              r_invlog.DUE_DATE              := sysdate;
              r_invlog.PAYMENT_STATUS        := null;
              r_invlog.PV_NUMBER             := null;
              r_invlog.DOCUMENT_NUMBER       := null;
              r_invlog.AP_VOUCHER_NUMBER     := null;
              r_invlog.PAYGROUP              := r_inv.PAYMENT_METHOD_LOOKUP_CODE;
              r_invlog.PAYMENT_TERM          := r_inv.TERMS_NAME;
              r_invlog.ERROR_CODE            := null;
              r_invlog.ERROR_MESSAGE         := null;
              r_invlog.PROCESSING_DATE       := l_starttime;
              r_invlog.PROCESSING_TIME       := NULL;--systimestamp - v_timestamp_start;
          IF v_firstline_flag='Y' THEN
          BEGIN
             select ap_invoices_interface_s.nextval 
               into r_invoice.invoice_id 
               from dual;
               write_log('r_invoice.invoice_id ='||r_invoice.invoice_id );
            
             BEGIN
                SELECT to_person_id 
                  INTO v_requester_id
                  FROM PO_REQUISITION_LINES_ALL prl,po_requisition_headers_all prh
                 WHERE prh.REQUISITION_HEADER_ID=prl.requisition_header_id
                   AND prh.segment1=r_line.pr_number
                  AND rownum=1;
                  r_invoice.requester_id := v_requester_id;  
                        
                EXCEPTION WHEN OTHERS THEN
                  v_requester_id:=NULL;
            END;
            --validate invoice date
            r_invoice.invoice_date:=r_inv.INVOICE_DATE;
            /*  begin
                  select TO_DATE(r_inv.INVOICE_DATE,'DD/MM/YYYY','NLS_CALENDAR=GREGORIAN') 
                  into r_invoice.invoice_date
                  from dual;
              exception
                  when others then
                      v_flag := true;
                      write_log('*Invalid GRN Number : ' || r_inv.RECEIPT_NUMBER);
                      err_msg := '*** INTERFACE DATA ERROR : Invoice Date Format not correct';
                      err_code := 'Error  INVHUB003-002: Date Format not correct';
                      write_invdetail_log(tDtinvlog,err_code,err_msg);
              end;
              */
              --validate invoice number
               g_step :='validate invoice number';
              begin
                  select 'Y'
                  into v_chek_inv
                  from ap_invoices_all  ap,po_vendors pv
                  where ap.INVOICE_NUM = r_inv.invoice_num
                    AND ap.vendor_id= pv.vendor_id
                    AND pv.segment1= r_inv.vendor_num
                    AND ap.org_id = decode(r_inv.ORG_CODE,'DTAC',102,'DTN',142,'PSB',218);
                   
                    
                     
                  IF SQL%FOUND THEN
                      v_flag := true;
                      write_log('*Duplicated Invoice Number : ' ||  r_inv.invoice_num);
                      write_log('*r_invlog : ' ||  r_invlog.ap_invoice);
                      err_msg := 'Duplicate Invoice in ERP';--'*** INTERFACE DATA ERROR : Duplicate Invoice in ERP';
                      err_code := 'INVHUB003-003'; --'Error  INVHUB003-003 : Duplicate Invoice in ERP';
                        
                       SELECT current_timestamp
                         INTO l_endtime
                         FROM dual;  
                         
                        r_invlog.PROCESSING_TIME := substr(to_char(l_endtime-l_starttime),12,12); 
                        
                      write_invdetail_log(r_invlog,gWritelog_detail,err_code,err_msg);
                  END IF;
              exception
                  when no_data_found then
                     null;
              end;
                        
              --validate org code
              g_step :=' validate org code';
              begin
                  select ORGANIZATION_ID
                  into r_invoice.org_id
                  from hr_organization_units hou
                  where name = decode(r_inv.ORG_CODE,'DTAC','TAC OU - HO','DTN','DTAC-N','PSB','PAYSBUY');
              exception
                  when no_data_found then
                      v_flag := true;
                      write_log('*Invalid ORG_CODE :*' || r_inv.ORG_CODE||'*');
                      write_log('length org_code :'||length(r_inv.ORG_CODE));
                      err_msg := 'Invalid invoice Org'; --'*** INTERFACE DATA ERROR : ORG_CODE name is invalid';
                      err_code := 'INVHUB003-015';--'Error  INVHUB003-015 : Invalid invoice Org';
                         SELECT current_timestamp
                         INTO l_endtime
                         FROM dual;  
                         
                        r_invlog.PROCESSING_TIME := substr(to_char(l_endtime-l_starttime),12,12); 
                      write_invdetail_log(r_invlog,gWritelog_detail,err_code,err_msg);
              end;
                        
                        
              --validate Supplier
              g_step :='validate Supplier';
              begin
                select v.vendor_id 
                        into r_invoice.vendor_id
                from po_vendors v
                where v.segment1 = r_inv.vendor_num;
              exception
                when no_data_found then
                  v_flag := true;
                  write_log('*Invalid Supplier : ' || r_inv.vendor_num);
                  err_msg := 'Invalid supplier';--'*** INTERFACE DATA ERROR : Supplier is invalid';
                  err_code := 'INVHUB003-001';--'Error INVHUB003-001: Invalid supplier';
                  
                  SELECT current_timestamp
                    INTO l_endtime
                    FROM dual;  
                         
                  r_invlog.PROCESSING_TIME := substr(to_char(l_endtime-l_starttime),12,12); 
                  write_invdetail_log(r_invlog,gWritelog_detail,err_code,err_msg);
              end;
                        
              --validate Supplier site
              g_step :='validate Supplier site';
              write_log('r_inv.vendor_site_code: ' || r_inv.vendor_site_code);
              write_log('r_invoice.vendor_id: ' ||r_invoice.vendor_id);
              begin
                select pvs.*
                into v_site
                from po_vendor_sites_all pvs
                where pvs.vendor_site_code = r_inv.vendor_site_code
                and   pvs.vendor_id = r_invoice.vendor_id
                AND   pvs.org_id  =r_invoice.org_id;
              exception
                when no_data_found then
                BEGIN
                    /*SELECT pvs.*
                      into v_site
                      FROM po_vendor_sites_all pvs
                     WHERE 1=1  --pvs.vendor_site_code = 'DTAC-N'  --r_inv.vendor_site_code
                       AND ROWNUM=1
                       AND   pvs.vendor_id = r_invoice.vendor_id
                       AND   pvs.org_id  =r_invoice.org_id;*/
                      IF instr(r_line.po_number,'-') !=0 THEN
                         l_po_header :=  substr(r_line.po_number,1,instr(r_line.po_number,'-')-1);
                         l_po_release := substr(r_line.po_number,instr(r_line.po_number,'-')+1,length(r_line.po_number)-instr(r_line.po_number,'-')); 
                      ELSE 
                         l_po_header := r_line.po_number;
                         l_po_release := 0;
                      END IF;   
                       
                       
                      SELECT pvs.*
                        into v_site
                        FROM po_vendor_sites_all pvs,
                             po_headers_all poh,
                             PO_RELEASES_ALL po_release
                       WHERE 1=1  --pvs.vendor_site_code = 'DTAC-N'  --r_inv.vendor_site_code
                         AND ROWNUM=1
                         AND pvs.vendor_id = poh.vendor_id
                         AND pvs.org_id  =poh.org_id
                         AND poh.po_header_id=po_release.po_header_id(+)
                         AND poh.org_id=po_release.org_id(+)
                         --AND poh.segment1=r_line.po_number
                         --AND poh.segment1||DECODE(po_release.release_num,NULL,NULL,'-'||po_release.release_num)=r_line.po_number
                         AND poh.segment1=l_po_header
                         AND nvl(po_release.release_num,0) = l_po_release
                         AND poh.org_id=r_invoice.org_id;
                       
                       EXCEPTION WHEN no_data_found THEN
                          v_flag := true;
                          write_log('*Invalid Supplier Site : ' || r_inv.vendor_site_code);
                          write_log('*Supplier Number : ' || r_inv.vendor_num);
                          err_msg := 'Invalid Supplier Site';-- '*** INTERFACE DATA ERROR : Supplier site is invalid';
                          err_code := 'INVHUB003-002';--'Error INVHUB003-002: Invalid Supplier Site';
                           SELECT current_timestamp
                               INTO l_endtime
                               FROM dual;  
                                 
                           r_invlog.PROCESSING_TIME := substr(to_char(l_endtime-l_starttime),12,12); 
                           write_invdetail_log(r_invlog,gWritelog_detail,err_code,err_msg);
                          v_site := null;
                 END;
              end;
                        
              r_invoice.vendor_site_id                := v_site.vendor_site_id;
              r_invoice.accts_pay_code_combination_id := v_site.accts_pay_code_combination_id;
              v_site_auto_calc_flag                   := v_site.auto_tax_calc_flag;

              --validate payment Term
              g_step :='validate payment Term';
              begin
                  select t.TERM_ID, t.creation_date
                  into r_invoice.TERMS_ID, r_invoice.terms_date
                  from ap_terms t
                  where t.name = r_inv.terms_name;
              exception
                  when no_data_found then
                      v_flag := true;
                      write_log('*Invalid Term Name : ' || r_inv.terms_name);
                      err_msg := 'Invalid Term Name';--'*** INTERFACE DATA ERROR : Term name is invalid';
                      err_code := 'INVHUB003-010';--'Error  INVHUB003-010 : Invalid Term Name';
                         
                         SELECT current_timestamp
                         INTO l_endtime
                         FROM dual;  
                         
                        r_invlog.PROCESSING_TIME := substr(to_char(l_endtime-l_starttime),12,12); 
                      write_invdetail_log(r_invlog,gWritelog_detail,err_code,err_msg);
              end;
                        
              --validate invoice source type
              g_step :='validate invoice source type';
              begin
                  select upper(displayed_field)
                  into v_source
                  from ap_lookup_codes
                  where lookup_type(+) = 'SOURCE' --'INVOICE TYPE'
                  and upper(displayed_field) = upper(trim(r_inv.source));
              exception
                  when no_data_found then
                      v_flag := true;
                      write_log('*Invalid  Source type : ' || r_inv.source);
                      err_msg := 'Invalid  Source type';--'*** INTERFACE DATA ERROR : Invalid  Source type';
                      err_code := 'INVHUB003-011';--'Error  INVHUB003-011 : Invalid  Source type';
                     SELECT current_timestamp
                     INTO l_endtime
                     FROM dual;  
                         
                    r_invlog.PROCESSING_TIME := substr(to_char(l_endtime-l_starttime),12,12); 
                      write_invdetail_log(r_invlog,gWritelog_detail,err_code,err_msg);
              end;
                        
              -- validate currency code
              g_step :=' validate currency code';
              begin            
                  select 'Y'
                  into v_check_currency
                  from fnd_currencies
                  where currency_code = r_inv.INVOICE_CURRENCY_CODE;
              exception
                  when no_data_found then
                      v_flag := true;
                      write_log('*Invalid  Currency code : ' || r_inv.INVOICE_CURRENCY_CODE);
                      err_msg := 'Invalid  Currency code';--'*** INTERFACE DATA ERROR : Invalid  Currency code';
                      err_code := 'INVHUB003-012'; --'Error  INVHUB003-012 : Invalid  Currency code';
                         SELECT current_timestamp
                         INTO l_endtime
                         FROM dual;  
                         
                        r_invlog.PROCESSING_TIME := substr(to_char(l_endtime-l_starttime),12,12); 
                      write_invdetail_log(r_invlog,gWritelog_detail,err_code,err_msg);
              end;
                        
              write_log('* 1  r_invoice.org_id : ' || r_invoice.org_id);
              write_log('* 1  inv period : ' || to_char(nvl(r_invoice.gl_date,SYSDATE),'MON-YY') );  
              
              --validate ap period open
              g_step :=' validate ap period open';
              begin        
                  SELECT  'Y'
                  into v_check_period
                  FROM  APPS.HR_OPERATING_UNITS HR_OPERATING_UNITS, 
                        HR.HR_ORGANIZATION_INFORMATION HR_ORGANIZATION_INFORMATION,
                        GL.GL_SETS_OF_BOOKS,
                        gl_period_statuses period
                  WHERE HR_OPERATING_UNITS.ORGANIZATION_ID = HR_ORGANIZATION_INFORMATION.ORGANIZATION_ID 
                  AND ((HR_ORGANIZATION_INFORMATION.ORG_INFORMATION_CONTEXT='Operating Unit Information'))
                  AND GL.GL_SETS_OF_BOOKS.SET_OF_BOOKS_ID = HR_OPERATING_UNITS.SET_OF_BOOKS_ID
                  and HR_OPERATING_UNITS.ORGANIZATION_ID  = r_invoice.org_id
                  and period.application_id   = 200
                  and period.closing_status   = 'O'
                  and period.PERIOD_NUM      <> 13
                  and period.PERIOD_NAME      = to_char(nvl(r_invoice.gl_date,SYSDATE),'MON-YY')   --to_char(r_invoice.invoice_date,'MON-YY')
                  --r_invoice.gl_date
                  and period.set_of_books_id  = GL.GL_SETS_OF_BOOKS.SET_OF_BOOKS_ID;
              exception
                  when no_data_found then
                      v_flag := true;
                      write_log('* 1  ORG_CODE : ' || r_inv.ORG_CODE);
                      err_msg := 'Invoice Date not in the Opened Period';--'*** INTERFACE DATA ERROR : Invoice Date not in the Opened Period';
                      err_code := 'INVHUB003-007';--'Error  INVHUB003-007 : Invoice Date not in the Opened Period';
                     SELECT current_timestamp
                     INTO l_endtime
                     FROM dual;  
                         
                    r_invlog.PROCESSING_TIME := substr(to_char(l_endtime-l_starttime),12,12); 
                      write_invdetail_log(r_invlog,gWritelog_detail,err_code,err_msg);
              end;
                        
                -----validate po number
               /* g_step :='validate po number';
                begin
                    select 'Y'
                    into v_check_po_number
                    from po_headers_all
                    where segment1 = r_inv.po_number;
                exception
                    when no_data_found then
                        v_flag := true;
                        write_log('*Invalid  PO Number : ' || r_inv.po_number);
                        err_msg := '*** INTERFACE DATA ERROR : Invalid  PO Number';
                        err_code := 'Error  INVHUB003-013 : Invalid  PO Number';
                        write_invdetail_log(tDtinvlog,err_code,err_msg);
                end;  */
                        
                if r_inv.gl_date is null then
                    r_invoice.gl_date := trunc(sysdate);
                else
                    r_invoice.gl_date := r_inv.gl_date;
                end if;          

                r_invoice.invoice_num                   := r_inv.invoice_num;
                r_invoice.invoice_type_lookup_code      := r_inv.invoice_type_lookup_code; --'STANDARD';
                --r_invoice.invoice_date                  := r_inv.invoice_date;
                r_invoice.vendor_num                    := r_inv.vendor_num;
                r_invoice.vendor_site_code              := r_inv.vendor_site_code;
                r_invoice.invoice_amount                := r_inv.invoice_amount;
                
                r_invoice.invoice_currency_code         := r_inv.invoice_currency_code;
                IF r_inv.invoice_currency_code != 'THB' THEN
                BEGIN
                   IF instr(r_line.PO_NUMBER,'-') !=0 THEN
                      l_po_header :=  substr(r_line.PO_NUMBER,1,instr(r_line.PO_NUMBER,'-')-1);
                      l_po_release := substr(r_line.PO_NUMBER,instr(r_line.PO_NUMBER,'-')+1,length(r_line.PO_NUMBER)-instr(r_line.PO_NUMBER,'-')); 
                   ELSE 
                      l_po_header := r_line.PO_NUMBER;
                      l_po_release := 0;
                   END IF;  
                                
                   SELECT rate_type
                         ,rate_date
                         ,rate
                     INTO r_invoice.exchange_rate_type 
                         ,r_invoice.exchange_date
                         ,r_invoice.exchange_rate
                     FROM po_headers_all poh,
                          po_releases_all po_release
                    WHERE --segment1=r_line.PO_NUMBER
                         segment1=l_po_header
                      AND nvl(po_release.release_num,0) = l_po_release
                         --  poh.segment1||DECODE(po_release.release_num,NULL,NULL,'-'||po_release.release_num)=r_line.PO_NUMBER
                      AND poh.po_header_id=po_release.po_header_id(+)
                      AND poh.org_id=po_release.org_id(+);
                   EXCEPTION WHEN OTHERS THEN
                     r_invoice.exchange_rate_type := '1001';
                END;
                END IF;
                r_invoice.source                        := r_inv.source;
                r_invoice.group_id                      := r_inv.group_name;
                r_invoice.doc_category_code             := r_inv.doc_category_code;
                r_invoice.description                   := f_get_invoice_description(i_interface_type,
                                                                                     r_line.PO_NUMBER,
                                                                                     NULL,--r_line.PO_LINE_NUMBER,
                                                                                     r_line.PR_NUMBER,
                                                                                     NULL--r_line.PR_LINE_NUMBER
                                                                                     );
            
                --r_invoice.terms_id                      := r_inv.terms_name;
                r_invoice.payment_method_lookup_code    := r_inv.payment_method_lookup_code;
                r_invoice.creation_date                 := sysdate;
                r_invoice.last_update_date              := sysdate;
                r_invoice.created_by                    := fnd_global.user_id;
                r_invoice.last_updated_by               := fnd_global.user_id;
                r_invoice.request_id                    := fnd_global.conc_request_id;
                r_invoice.attribute_category            := 'JA.TH.APXINWKB.DISTRIBUTIONS';
                r_invoice.global_attribute_category     := NULL; --'JA.TH.APXINWKB.DISTRIBUTIONS';
          END;
          END IF; -- end validate header
          
          v_line_num := v_line_num + 1;
          r_invoice_line := null;
          v_chk_line := false;
          
          write_log('* r_line.PO_LINE_NUMBER : ' ||  r_line.PO_LINE_NUMBER);
          write_log('* r_line.RECEIPT_LINE_NUMBER : ' ||r_line.RECEIPT_LINE_NUMBER);
          write_log('* r_line.LINE_NUMBER : ' || r_line.LINE_NUMBER);
          write_log('* r_line.PR_LINE_NUMBER : ' || r_line.PR_LINE_NUMBER);
          
          v_line_status_info := 'Row#' || v_line_num || ' |Invoice line#' ||r_line.line_number;

          select ap_invoice_lines_interface_s.nextval 
            into r_invoice_line.invoice_line_id 
            from dual;
          r_invoice.terms_date :=r_line.bill_placement_date;  --BILL_PLACEMENT_DATE 
          IF r_invoice.requester_id is NULL THEN
          BEGIN 
                 IF instr(r_line.po_number,'-') !=0 THEN
                    l_po_header :=  substr(r_line.po_number,1,instr(r_line.po_number,'-')-1);
                    l_po_release := substr(r_line.po_number
                                          ,instr(r_line.po_number,'-')+1
                                          ,length(r_line.po_number)-instr(r_line.po_number,'-')); 
                 ELSE 
                    l_po_header := r_line.po_number;
                    l_po_release := 0;
                 END IF;  
               
                SELECT pd.deliver_to_person_id 
                  INTO v_requester_id
                  FROM po_headers_all poh
                      ,po_releases_all po_release
                      ,po_lines_all pol
                      ,po_line_locations_all pll
                      ,po_distributions_all pd
                WHERE 1=1
                  --poh.segment1=r_line.po_number--'991015019556'
                  --    poh.segment1||DECODE(po_release.release_num,NULL,NULL,'-'||po_release.release_num)=r_line.po_number
                   AND poh.segment1 = l_po_header
                   AND nvl(po_release.release_num,0) = l_po_release
                  AND pol.line_num =r_line.po_line_number
                  AND poh.po_header_id=pol.po_header_id
                  AND poh.org_id =pol.org_id
                  AND poh.po_header_id=po_release.po_header_id(+)
                  AND poh.org_id=po_release.org_id(+)
                  AND pol.po_header_id=pll.po_header_id
                  AND pol.po_line_id=pll.po_line_id
                  AND pol.org_id=pll.org_id
                  AND pll.po_header_id =pd.po_header_id
                  AND pll.po_line_id=pd.po_line_id
                  AND pll.org_id=pd.org_id
                  AND pll.line_location_id=pd.line_location_id
                  AND ROWNUM=1;
                  r_invoice.requester_id := v_requester_id;
                 EXCEPTION WHEN OTHERS THEN
                v_requester_id:=NULL;
          END;
          END IF;
            
            
          r_invoice_line.amount := r_line.line_amount;
          g_step := 'validate GRN';
          IF i_interface_type = '3WAY' THEN
          BEGIN
              r_invoice_line.receipt_number      := r_line.RECEIPT_NUMBER;
              r_invoice_line.receipt_line_number := r_line.receipt_line_number;
                select DISTINCT 'Y' 
                  INTO v_check_grn
                  from rcv_shipment_headers rsh,
                     rcv_shipment_lines rsl,
                     po_distributions_all pod,
                     gl_code_combinations_kfv gl
               where 1=1 --rsh.organization_id     = r_invoice.org_id
                 and rsh.shipment_header_id  = rsl.shipment_header_id
                 and rsl.po_header_id        = pod.po_header_id
                 and rsl.po_distribution_id  = pod.po_distribution_id
                 and pod.code_combination_id = gl.code_combination_id
                 and rsh.receipt_num         = r_line.RECEIPT_NUMBER
                 and rsl.line_num            = r_line.RECEIPT_LINE_NUMBER;
           exception
                when no_data_found then
                     v_flag := true;
                     write_log('*Invalid GRN Number : ' || r_line.RECEIPT_NUMBER||': '||r_line.RECEIPT_LINE_NUMBER);
                     err_msg := 'Invalid GRN Number';--'*** INTERFACE DATA ERROR : Invalid Account Code';
                     err_code := 'INVHUB003-023';--'Error  INVHUB003-021 : Invalid Account Code (3WAY)';
                        SELECT current_timestamp
                         INTO l_endtime
                         FROM dual;  
                         
                        r_invlog.PROCESSING_TIME := substr(to_char(l_endtime-l_starttime),12,12); 
                     write_invdetail_log(r_invlog,gWritelog_detail,err_code,err_msg);
          END;
          END IF;
       /*   -- Validate precision amount --joe
          g_step := 'Validate precision amount';
        BEGIN
           select length(r_line.line_amount) - instr(r_line.line_amount,'.')
                  INTO l_check_decimal
             FROM dual;
           
           
           IF l_check_decimal > 2 THEN
              write_log('*Invalid precision : ' || r_line.RECEIPT_NUMBER||': '||r_line.RECEIPT_LINE_NUMBER);
              err_msg := 'Invalid precision ';--'*** INTERFACE DATA ERROR : Invalid Account Code';
              err_code := 'INVHUB003-024';--'Error  INVHUB003-021 : Invalid Account Code (3WAY)';
              write_invdetail_log(r_invlog,gWritelog_detail,err_code,err_msg);
           END IF;
        END;*/
           --validate acc code
                    
          g_step :='validate acc code';
          IF i_interface_type = '3WAY' THEN
          BEGIN
            
              r_invoice_line.receipt_number      := r_line.RECEIPT_NUMBER;
              r_invoice_line.receipt_line_number := r_line.receipt_line_number;
              write_log('r_invoice.org_id='||r_invoice.org_id);
              write_log('r_line.RECEIPT_NUMBER='||r_line.RECEIPT_NUMBER);
              write_log('r_line.RECEIPT_LINE_NUMBER='||r_line.RECEIPT_LINE_NUMBER);
              
              /*issue 28-oct-2016
                1) program get the wrong account type
                2) ignore get account code for 3way
                3) do the same way as citibank program
              */
              v_dist_acc_code:=NULL;
              v_dist_acc_concate:=NULL;
            /*  select DISTINCT gl.code_combination_id,gl.CONCATENATED_SEGMENTS
                into v_dist_acc_code,v_dist_acc_concate
                from rcv_shipment_headers rsh,
                     rcv_shipment_lines rsl,
                     po_distributions_all pod,
                     gl_code_combinations_kfv gl
               where 1=1 --rsh.organization_id     = r_invoice.org_id
                 and rsh.shipment_header_id  = rsl.shipment_header_id
                 and rsl.po_header_id        = pod.po_header_id
                 and rsl.po_distribution_id  = pod.po_distribution_id
                 and pod.code_combination_id = gl.code_combination_id
                 and rsh.receipt_num         = r_line.RECEIPT_NUMBER
                 and rsl.line_num            = r_line.RECEIPT_LINE_NUMBER;*/
                 
           exception
                when no_data_found then
                     v_flag := true;
                     write_log('*Invalid Account Code (3WAY) : ' || r_line.RECEIPT_NUMBER||' '||r_line.RECEIPT_LINE_NUMBER);
                     err_msg := 'Invalid Account Code (3WAY)';--'*** INTERFACE DATA ERROR : Invalid Account Code';
                     err_code := 'INVHUB003-021';--'Error  INVHUB003-021 : Invalid Account Code (3WAY)';
                        SELECT current_timestamp
                         INTO l_endtime
                         FROM dual;  
                         
                        r_invlog.PROCESSING_TIME := substr(to_char(l_endtime-l_starttime),12,12); 
                     write_invdetail_log(r_invlog,gWritelog_detail,err_code,err_msg);
           END;
           ELSE
           BEGIN
               write_log('* r_invoice.org_id : ' || r_invoice.org_id);
                write_log('* r_line.PO_NUMBER  : ' ||r_line.PO_NUMBER );
                 write_log('* r_line.PO_LINE_NUMBER : ' || r_line.PO_LINE_NUMBER);
                 
                  IF instr(r_line.PO_NUMBER,'-') !=0 THEN
                      l_po_header :=  substr(r_line.PO_NUMBER,1,instr(r_line.PO_NUMBER,'-')-1);
                      l_po_release := substr(r_line.PO_NUMBER
                                            ,instr(r_line.PO_NUMBER,'-')+1
                                            ,length(r_line.PO_NUMBER)-instr(r_line.PO_NUMBER,'-')); 
                   ELSE 
                      l_po_header := r_line.PO_NUMBER;
                      l_po_release := 0;
                   END IF;  
                 
            select DISTINCT gl.code_combination_id,gl.CONCATENATED_SEGMENTS
                into v_dist_acc_code,v_dist_acc_concate
                from po_headers_all po,
                     PO_RELEASES_ALL po_release,
                     po_lines_all pol,
                     po_distributions_all pod,
                     gl_code_combinations_kfv gl
               where po.org_id        = r_invoice.org_id
                 AND po.po_header_id=po_release.po_header_id(+)
                 AND po.org_id=po_release.org_id(+)
                 and po.po_header_id  = pol.po_header_id
                 and pol.po_header_id = pod.po_header_id
                 and pol.po_line_id   = pod.po_line_id
                 and pod.code_combination_id = gl.code_combination_id
                 AND po.segment1 = l_po_header
                 AND nvl(po_release.release_num,0) = l_po_release
                 --and po.segment1      = r_line.PO_NUMBER 
                -- AND po.segment1||DECODE(po_release.release_num,NULL,NULL,'-'||po_release.release_num) = r_line.PO_NUMBER
                 and pol.line_num     = r_line.PO_LINE_NUMBER;
              exception
                when no_data_found then
                     v_flag := true;
                     write_log('*Invalid Account Code (2WAY) : ' || r_line.PO_NUMBER||' '||r_line.PO_LINE_NUMBER);
                     err_msg := 'Invalid Account Code (2WAY)';--'*** INTERFACE DATA ERROR : Invalid Account Code (2WAY)';
                     err_code := 'INVHUB003-022';--'Error  INVHUB003-022 : Invalid Account Code (2WAY)';
                        SELECT current_timestamp
                         INTO l_endtime
                         FROM dual;  
                         
                        r_invlog.PROCESSING_TIME := substr(to_char(l_endtime-l_starttime),12,12); 
                     write_invdetail_log(r_invlog,gWritelog_detail,err_code,err_msg);
                           
          END;
          END IF;
                    
                    --validate tax code
                    g_step :='validate tax code';
          if r_line.line_tax_code is not null then
          begin
             select atc.tax_code_combination_id, atc.tax_id, atc.tax_rate
               into v_tax_ccid, r_invoice_line.tax_code_id, v_rate
               from ap_tax_codes_all atc 
              where atc.enabled_flag = 'Y'
                and atc.name = r_line.line_tax_code
                and nvl(atc.start_date, sysdate) <= sysdate
                and nvl(atc.inactive_date, sysdate) >= sysdate
                and atc.org_id = r_invoice.org_id;
            exception
              when others then
                   v_chk_line := true;
                   v_line_status_info := v_line_status_info || '|Invalid Tax Code (' || r_line.line_tax_code || ')';
                   err_msg := 'Tax code is invalid';--'*** INTERFACE DATA ERROR : Tax code is invalid';
                   err_code := 'INVHUB003-016';--'Error  INVHUB003-016 : Tax code is invalid';
                      SELECT current_timestamp
                         INTO l_endtime
                         FROM dual;  
                         
                        r_invlog.PROCESSING_TIME := substr(to_char(l_endtime-l_starttime),12,12); 
                   write_invdetail_log(r_invlog,gWritelog_detail,err_code,err_msg);
          end;
                        
            r_invoice_line.tax_code               := r_line.line_tax_code;
            r_invoice_line.tax_code_override_flag := 'N';
            r_invoice_line.tax_recoverable_flag   := 'Y';
          else
            r_invoice_line.tax_code          := null;
            r_invoice_line.tax_recovery_rate := null;
          end if;
                    
          v_supp_name := null;
                    
          --validate project and project task
          g_step :='validate project and project task';           
          if r_line.project_number is null and 
             r_line.project_task_number is null and 
             r_line.expenditure_type is null and
             r_line.organization_id is NULL then
             v_validate_error := false;
          else
              --validate Project number
              if r_line.project_number is null then
                v_validate_error := true;
                v_flag := true;
                v_line_status_info := v_line_status_info || '|Invalid Project (' || r_line.project_number || ')';
                write_log('*Invalid Project : ' || r_line.project_number);
                err_msg := 'Project is invalid';--'*** INTERFACE DATA ERROR : Project number is invalid';
                err_code := 'INVHUB003-017'; --'Error  INVHUB003-017 : Project is invalid';
                   SELECT current_timestamp
                         INTO l_endtime
                         FROM dual;  
                         
                        r_invlog.PROCESSING_TIME := substr(to_char(l_endtime-l_starttime),12,12); 
                write_invdetail_log(r_invlog,gWritelog_detail,err_code,err_msg);
              end if;

              --validate Task Number
              g_step :='validate Task Number';  
              if r_line.project_number is not null then
                begin
                  select pa.project_id,pt.task_id
                  into v_proj_id,v_task_id
                  from PA_PROJECTS_ALL pa,
                                       PA_TASKS pt
                                  where pa.project_id  = pt.project_id
                                  and   pa.segment1    = r_line.project_number
                                  and   pt.task_number = r_line.project_task_number;
                exception
                  when no_data_found then
                    v_validate_error := true;
                    v_flag := true;
                    v_line_status_info := v_line_status_info || '|Invalid Task (' || r_line.project_task_number || ')';
                    write_log('*Invalid Task : ' || r_line.project_task_number);
                    err_msg := 'Task number is invalid';--'*** INTERFACE DATA ERROR : Task number is invalid';
                    err_code := 'INVHUB003-018';--'Error  INVHUB003-018 : Task number is invalid';
                    SELECT current_timestamp
                      INTO l_endtime
                      FROM dual;  
                         
                    r_invlog.PROCESSING_TIME := substr(to_char(l_endtime-l_starttime),12,12); 
                    write_invdetail_log(r_invlog,gWritelog_detail,err_code,err_msg);
                end;
              end if;

              --validate Expenditure Type
              g_step :='validate Expenditure Type';  
              begin
                select pet.expenditure_type
                into v_expen_type
                from pa_expenditure_types pet
                where pet.expenditure_type = r_line.expenditure_type;
              exception
                when no_data_found then
                  v_validate_error := true;
                  v_flag := true;
                  v_line_status_info := v_line_status_info || '|Invalid Expenditure Type (' || r_line.expenditure_type || ')';
                  err_msg := 'Invalid Expenditure Type';--'*** INTERFACE DATA ERROR : Invalid Expenditure Type!';
                  err_code := 'INVHUB003-019';--'Error  INVHUB003-019 : Invalid Expenditure Type';
                 SELECT current_timestamp
                     INTO l_endtime
                     FROM dual;  
                         
                    r_invlog.PROCESSING_TIME := substr(to_char(l_endtime-l_starttime),12,12); 
                  write_invdetail_log(r_invlog,gWritelog_detail,err_code,err_msg);
              end;

              --validate Expenditure Org
              g_step :='validate Expenditure Org'; 
              if r_line.organization_id is null then
                begin
                  select hou.organization_id
                  into v_expen_org
                  from hr_organization_units hou, 
                                       TAC_INVHUB_INVOICE_STG l
                  where hou.organization_id = r_line.organization_id
                    and   hou.name(+)         = l.expenditure_organization;
                exception
                  when no_data_found then
                    v_validate_error := true;
                    v_flag := true;
                    v_line_status_info := v_line_status_info || '|Invalid Expenditure Organization (' || r_line.expenditure_organization || ')';
                    err_msg := 'Invalid Expenditure Organization';--'*** INTERFACE DATA ERROR : Invalid Expenditure Organization!';
                    err_code := 'INVHUB003-020';--'Error  INVHUB003-020 : Invalid Expenditure Organization';
                   SELECT current_timestamp
                     INTO l_endtime
                     FROM dual;  
                         
                    r_invlog.PROCESSING_TIME := substr(to_char(l_endtime-l_starttime),12,12); 
                    write_invdetail_log(r_invlog,gWritelog_detail,err_code,err_msg);
               end;
               end if;
          end if;
          
          get_project_information ( r_line.PR_NUMBER,
                                    r_line.PR_LINE_NUMBER,
                                    l_project_id,
                                    l_project_name,
                                    l_task_id,
                                    l_task_name,
                                    l_expenditure_type,
                                    l_expenditure_item_date,
                                    l_expenditure_organization_id);
          
          r_invoice_line.invoice_id                   := r_invoice.invoice_id;
          r_invoice_line.org_id                       := r_invoice.org_id;
          IF r_line.erp_ap_linenumber IS NOT NULL THEN    --i_interface_type = '3WAY' THEN
             r_invoice_line.line_number                  := r_line.erp_ap_linenumber; --r_line.LINE_NUMBER;
          ELSE 
             r_invoice_line.line_number                  := r_line.LINE_NUMBER;
          END IF;
          

          -------------------------------
          r_invoice_line.accounting_date              := r_line.gl_date;
          r_invoice_line.project_id                   := NULL;--l_project_id;--v_proj_id;--r_line.project_id;
          r_invoice_line.task_id                      := NULL;--l_task_id;--v_task_id; 
          r_invoice_line.expenditure_type             := NULL;--l_expenditure_type;--r_line.expenditure_type; 
          r_invoice_line.expenditure_organization_id  := NULL;--l_expenditure_organization_id;--r_line.organization_id; 
          r_invoice_line.expenditure_item_date        := NULL;--l_expenditure_item_date;--sysdate;
          r_invoice_line.line_type_lookup_code        := r_line.line_type_lookup_code; 
          r_invoice_line.description                  := f_get_invoice_description(i_interface_type,
                                                                                     r_line.PO_NUMBER,
                                                                                     r_line.PO_LINE_NUMBER,
                                                                                     r_line.PR_NUMBER,
                                                                                     r_line.PR_LINE_NUMBER
                                                                                     );
          r_invoice_line.dist_code_combination_id     := v_dist_acc_code;
          r_invoice_line.dist_code_concatenated       := v_dist_acc_concate;
          r_invoice_line.awt_group_name               := r_line.awt_group_name;
          r_invoice_line.attribute4                   := r_line.act_vendor_num;
          r_invoice_line.attribute5                   := r_line.act_vendor_site_code;
          r_invoice_line.global_attribute_category    := 'JA.TH.APXINWKB.DISTRIBUTIONS';
          get_po_information (r_line.po_number,
                              r_line.po_line_number,
                              --r_invoice_line.po_header_id,
                              --r_invoice_line.po_line_id,
                              r_invoice_line.po_line_location_id,
                              r_invoice_line.po_distribution_id,
                              l_match_option);
          -- r_invoice_line.po_number  := r_line.po_number;
        /*  IF i_interface_type = '3WAY' THEN
             r_invoice_line.line_number   := r_line.erp_ap_linenumber;
          ELSE
             r_invoice_line.line_number   := r_line.line_number; 
          END IF;         
        */  
          IF  i_interface_type = '3WAY' THEN
              IF nvl(l_match_option,'P') ='P' THEN
              BEGIN
                v_validate_error := true;
                v_flag := true;
                v_line_status_info := v_line_status_info ||'Invalid match option for po number : ' || r_line.po_number;
                    
                write_log('Invalid match option for po number : ' || r_line.po_number);
                err_msg := 'Invalid match option for po number : ' || r_line.po_number;--'*** INTERFACE DATA ERROR : Project number is invalid';
                err_code := 'INVHUB003-025'; --'Error  INVHUB003-017 : Project is invalid';
                   SELECT current_timestamp
                         INTO l_endtime
                         FROM dual;  
                         
                r_invlog.PROCESSING_TIME := substr(to_char(l_endtime-l_starttime),12,12); 
                write_invdetail_log(r_invlog,gWritelog_detail,err_code,err_msg);
              END;
              END IF;
          END IF;
           
         -- r_invoice_line.po_header_id                 := get_po_header(r_line.po_number

          if r_invoice_line.global_attribute16 is null then
            r_invoice_line.global_attribute16 := r_inv.source;
          end if;
                    
          if r_line.tax_acct_period is null then
            r_invoice_line.attribute13 := v_period;
          else
            r_invoice_line.attribute13 := r_line.tax_acct_period;
          end if;

          r_invoice_line.attribute15 := r_line.reporting_entity;
          r_invoice_line.global_attribute9 := v_supp_name;

          r_invoice_line.attribute_category := 'JA.TH.APXINWKB.DISTRIBUTIONS';

          r_invoice_line.global_attribute20 := r_line.reporting_entity;

          if r_line.awt_group_name is not null then
            r_invoice_line.global_attribute12 := r_line.wht_revenue_name;
            r_invoice_line.global_attribute13 := r_line.wht_revenue_type;
            r_invoice_line.global_attribute14 := r_line.wht_condition;
            r_invoice_line.global_attribute15 := r_line.phor_ngor_dor;
            r_invoice_line.global_attribute18 := r_line.wht_acct_period;
          end if;

          r_invoice_line.created_by := fnd_global.user_id;
          r_invoice_line.creation_date := sysdate;

          if v_chk_line or v_flag then
                        v_line_count_err := v_line_count_err + 1;
            v_line_status_info := v_line_status_info || '| Rejected';
            write_log('update_staging_status 1');
            update_staging_status(i_inv_num        => r_invoice.invoice_num
                                ,  i_vendor_num     => r_invoice.vendor_num
                                , i_source         => i_source
                                , i_interface_type => i_interface_type
                                , i_line_num       => r_line.line_number
                                , i_status         => 'E'
                                , i_filename       =>  r_readfile.file_name
                                , i_org            => r_inv.ORG_CODE);

            v_validate_error := true;
          else
            if r_line.line_type_lookup_code = 'ITEM' and r_line.line_tax_code is not null then
              begin
                select tax_rate
                into v_tax_rate
                from ap_tax_codes_all
                where name = r_line.line_tax_code
                                and   org_id = r_invoice.org_id
                  and ((enabled_flag = 'Y') or (enabled_flag is null))
                and ((trunc(start_date) <= trunc(sysdate)) and (trunc(nvl(inactive_date, sysdate)) >= trunc(sysdate)));
              exception
                when others then
                      v_tax_rate := 0;
              end;

              r_invoice_line.attribute9 := r_invoice_line.amount;
              r_invoice_line.attribute8 := round(r_invoice_line.amount * (v_tax_rate / 100), 2);
              r_invoice_line.attribute10 := v_supp_name;
            end if;
            
            if v_flag = false then
                            write_log('***** INSERT ap_invoice_lines_interface *****');
                g_step :='INSERT ap_invoice_lines_interface';             
                insert_invoice_line(ir_invoice_line => r_invoice_line);
            ELSE
                write_log('v_flag=true');
            end if;

            --r_invoice.invoice_amount := nvl(r_invoice.invoice_amount, 0) + r_invoice_line.amount; -- For Invoice Header

            v_line_status_info := v_line_status_info || '| Processed';
             g_step :='update_staging_status';
             write_log(g_step);
           --joe 
            update_staging_status(i_inv_num        => r_invoice.invoice_num
                                , i_vendor_num     => r_invoice.vendor_num
                                , i_source         => i_source
                                , i_interface_type => i_interface_type
                                , i_line_num       => r_line.line_number
                                , i_status         => 'Y'
                               , i_filename       =>  r_readfile.file_name
                               , i_org            =>  r_inv.ORG_CODE);
            write_log('after '||g_step);   
            write_log('r_invoice.invoice_num ='||r_invoice.invoice_num);             
            write_log('i_source ='||i_source);
            write_log('i_interface_type ='||i_interface_type);
            write_log('r_line.line_number ='||r_line.line_number);
            --joe
            write_log('r_invlog.ap_invoice='||r_invlog.ap_invoice);
            write_log('r_invlog.ap_invoie_line'||r_invlog.ap_invoie_line);
            
            --write_invdetail_log(r_invlog,gWritelog_detail,NULL,NULL);
            write_log('after '||g_step); 
            v_line_count := v_line_count + 1;
          end if;
                    
          if v_chk_line then
            v_flag := true;
          end if;

          write_log(v_line_status_info);
          v_firstline_flag:='N';
        end loop; --Line loop
      --end if;--------------------------------------------------

      if v_chk_line = false then
        write_log('***** INSERTERT ap_invoices_interface *****');
        insert_invoice(ir_invoice => r_invoice);
        write_log('Invoice ID: ' || r_invoice.invoice_id);
      end if;

      if v_flag = true then
           write_log('Rollback transaction of invoice number ' || r_inv.invoice_num);
        rollback to savepoint v_save_pnt;
      end if;
            
      --TAC_AP_INVOICEHUB_PKG.write_invdetail_log(r_invlog,gWritelog_detail,err_code,err_msg);
    end loop; -- Header loop

    if (v_line_count <= 0 ) then
        write_log('No invoice line to process');
    end if;

   -- if not v_validate_error and v_line_count > 0 then
    BEGIN
      commit;
            
      write_log('Start Payable Open Interface ---------------------------');
      BEGIN
         
              
           SELECT SUM(DECODE(org_id,102,1,0)),
                  SUM(DECODE(org_id,142,1,0)),
                  SUM(DECODE(org_id,218,1,0))
             INTO l_count_dtac 
                 ,l_count_dtn
                 ,l_count_psb
             from ap_invoices_interface
            WHERE group_id = v_group;
                   
      END;
      
      IF l_count_dtac != 0 THEN
      BEGIN   
         --l_resp_name :='AP-HO';
      
                 
          BEGIN       
          select fresp.responsibility_id, 
                 fresp.application_id 
            INTO l_resp_id,
                 l_resp_app_id
          from   fnd_user fnd 
          ,      fnd_responsibility_tl fresp 
          where  fnd.user_id = fnd_global.user_id
          and    fresp.responsibility_name = 'AP-HO';
           EXCEPTION WHEN no_data_found THEN
              l_resp_id :=NULL;
              l_resp_app_id :=NULL;
         END;
         
         IF l_resp_id is NOT NULL AND l_resp_app_id is NOT NULL THEN
           
             fnd_global.apps_initialize (user_id => fnd_global.user_id,
                                         resp_id => l_resp_id,
                                         resp_appl_id => l_resp_app_id);
          END IF;
          
           
            ap_req_id := fnd_request.submit_request(application  => 'SQLAP'
                                                          , program      => 'APXIIMPT'
                                                          , argument1    => v_source
                                                          , argument2    => v_group
                                                          , argument3    => v_group --i_batch
                                                          , argument4    => null
                                                          , argument5    => null
                                                          , argument6    => null
                                                          , argument7    => 'N');
          

          v_i_group   := v_group;
          v_ap_req_id := ap_req_id;
          l_dtac_req_ID := ap_req_id;


          commit;

          if ap_req_id > 0 then
                    write_log('+----------------------------------------------------------------------------+');
                    write_log('Payable Interface Start Request ID: ' || ap_req_id);
            conc_wait(ap_req_id);--,v_phase,v_request_phase);
                    --EXIT WHEN ( UPPER(v_request_phase) = 'COMPLETE' OR v_phase = 'C');
                    write_log('Successfull');
                    write_log('+----------------------------------------------------------------------------+');
            --c_ap_req_id := fnd_request.submit_request(application => 'SQLAP', program => 'TACAP016_V3_EXTD', argument1 => v_ap_req_id, argument2 => v_i_group);
          else
            write_log('Can not Submit Payable Open Interface ' || to_char(sysdate, 'DD/MM/YYYY HH24:MI:SS') || '----------------------------------');
          end if;
      END;
      END IF;
      
      -----  DTN
      IF l_count_dtn != 0 THEN
      BEGIN   
         --l_resp_name :='DTN_AP';
      
                 
          BEGIN       
          select fresp.responsibility_id, 
                 fresp.application_id 
            INTO l_resp_id,
                 l_resp_app_id
          from   fnd_user fnd 
          ,      fnd_responsibility_tl fresp 
          where  fnd.user_id = fnd_global.user_id
          and    fresp.responsibility_name = 'DTN_AP';
           EXCEPTION WHEN no_data_found THEN
              l_resp_id :=NULL;
              l_resp_app_id :=NULL;
         END;
         
         IF l_resp_id is NOT NULL AND l_resp_app_id is NOT NULL THEN
           
             fnd_global.apps_initialize (user_id => fnd_global.user_id,
                                         resp_id => l_resp_id,
                                         resp_appl_id => l_resp_app_id);
          END IF;
          
            
            ap_req_id := fnd_request.submit_request(application  => 'SQLAP'
                                                          , program      => 'APXIIMPT'
                                                          , argument1    => v_source
                                                          , argument2    => v_group
                                                          , argument3    => v_group --i_batch
                                                          , argument4    => null
                                                          , argument5    => null
                                                          , argument6    => null
                                                          , argument7    => 'N');
          

          v_i_group   := v_group;
          v_ap_req_id := ap_req_id;
          l_dtn_req_ID       := ap_req_id;
          commit;

          if ap_req_id > 0 then
                    write_log('+----------------------------------------------------------------------------+');
                    write_log('Payable Interface Start Request ID: ' || ap_req_id);
            conc_wait(ap_req_id);--,v_phase,v_request_phase);
                    --EXIT WHEN ( UPPER(v_request_phase) = 'COMPLETE' OR v_phase = 'C');
                    write_log('Successfull');
                    write_log('+----------------------------------------------------------------------------+');
            --c_ap_req_id := fnd_request.submit_request(application => 'SQLAP', program => 'TACAP016_V3_EXTD', argument1 => v_ap_req_id, argument2 => v_i_group);
          else
            write_log('Can not Submit Payable Open Interface ' || to_char(sysdate, 'DD/MM/YYYY HH24:MI:SS') || '----------------------------------');
          end if;
      END;
      END IF;
      
      -----  PSB
      IF l_count_psb != 0 THEN
      BEGIN   
       --  l_resp_name :='PB_AP';
      
                 
          BEGIN       
          select fresp.responsibility_id, 
                 fresp.application_id 
            INTO l_resp_id,
                 l_resp_app_id
          from   fnd_user fnd 
          ,      fnd_responsibility_tl fresp 
          where  fnd.user_id = fnd_global.user_id
          and    fresp.responsibility_name = 'PB_AP';
           EXCEPTION WHEN no_data_found THEN
              l_resp_id :=NULL;
              l_resp_app_id :=NULL;
         END;
         
         IF l_resp_id is NOT NULL AND l_resp_app_id is NOT NULL THEN
           
             fnd_global.apps_initialize (user_id => fnd_global.user_id,
                                         resp_id => l_resp_id,
                                         resp_appl_id => l_resp_app_id);
          END IF;
          
            
            ap_req_id := fnd_request.submit_request(application  => 'SQLAP'
                                                          , program      => 'APXIIMPT'
                                                          , argument1    => v_source
                                                          , argument2    => v_group
                                                          , argument3    => v_group --i_batch
                                                          , argument4    => null
                                                          , argument5    => null
                                                          , argument6    => null
                                                          , argument7    => 'N');
          
          v_i_group   := v_group;
          v_ap_req_id := ap_req_id;
          l_psb_req_id := ap_req_id;
          commit;

          if ap_req_id > 0 then
                    write_log('+----------------------------------------------------------------------------+');
                    write_log('Payable Interface Start Request ID: ' || ap_req_id);
            conc_wait(ap_req_id);--,v_phase,v_request_phase);
                    --EXIT WHEN ( UPPER(v_request_phase) = 'COMPLETE' OR v_phase = 'C');
                    write_log('Successfull');
                    write_log('+----------------------------------------------------------------------------+');
            --c_ap_req_id := fnd_request.submit_request(application => 'SQLAP', program => 'TACAP016_V3_EXTD', argument1 => v_ap_req_id, argument2 => v_i_group);
          else
            write_log('Can not Submit Payable Open Interface ' || to_char(sysdate, 'DD/MM/YYYY HH24:MI:SS') || '----------------------------------');
          end if;
      END;
      END IF;
            
    END;
  --  end if; -- Ctrl flag
        
      /*  tSuminvlog.EXTEND;
        tSuminvlog(tSuminvlog.LAST).LEVEL_TYPE                  := 'H';
        tSuminvlog(tSuminvlog.LAST).REQUEST_ID                  := v_conc_request;
        tSuminvlog(tSuminvlog.LAST).PROGRAM_NAME                := i_prog_name;   
        tSuminvlog(tSuminvlog.LAST).PROCESS_DATE                := v_timestamp_start;
        tSuminvlog(tSuminvlog.LAST).PROCESSING_TYPE             := 'Download';
        tSuminvlog(tSuminvlog.LAST).RECORD_COUNT_COMPLETION     := v_line_count;
        tSuminvlog(tSuminvlog.LAST).RECORD_COUNT_ERROR          := v_line_count_err;
        tSuminvlog(tSuminvlog.LAST).PROCESSING_DATE             := systimestamp - v_timestamp_start;
        
        TAC_AP_INVOICEHUB_PKG.write_invsummary_log(tSuminvlog);*/
      
      /*  write_invsummary_log(gWritelog_detail,p_current_request_id);
     
    commit;--fnd_global.CONC_REQUEST_ID
    APPS.tac_po_invoicehub_pkg.call_logmonitor('DTINVHUB003',p_current_request_id);     
    
    */
    -- write_invdetail_log --newjoe
    END;
    END LOOP;-- end loop read_file
    

    get_error_from_interface(gWritelog_detail,v_group,l_starttime,p_current_request_id,l_dtac_req_ID,l_dtn_req_ID,l_psb_req_id);
    write_invsummary_log(gWritelog_detail,p_current_request_id,l_starttime);
     
    commit;--fnd_global.CONC_REQUEST_ID
    APPS.tac_po_invoicehub_pkg.call_logmonitor('DTINVHUB003',p_current_request_id);
    tac_po_invoicehub_pkg.write_output(p_current_request_id,'DTINVHUB003');
    exception
        when others then
            write_log('DTINVHUB003 : Interface AP Invoices Exception .');
            write_log('Error step : '|| g_step);
            write_log('Error MSG: ' || sqlcode || ' - ' || sqlerrm);
            err_code:=2;
  end interface_ap_invoicehub;

end TAC_AP_INVOICEHUB_PKG;
