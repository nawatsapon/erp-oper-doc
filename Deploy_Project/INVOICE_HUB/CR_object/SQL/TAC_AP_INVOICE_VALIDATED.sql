create table TAC_AP_INVOICE_VALIDATED
(
  invoice_id     NUMBER,
  line_number    NUMBER,
  user_id        NUMBER,
  validated_date DATE
)
