CREATE OR REPLACE PACKAGE TAC_AP_INVOICES_PKG AS
    PROCEDURE  INSERT_VALIDATED_INVOICE(P_INVOICE_ID      NUMBER,
                                        P_USER_ID         NUMBER,
                                        P_VALIDATED_DATE  DATE);

END TAC_AP_INVOICES_PKG;



CREATE OR REPLACE PACKAGE BODY TAC_AP_INVOICES_PKG AS

    PROCEDURE  INSERT_VALIDATED_INVOICE(P_INVOICE_ID      IN NUMBER,
                                        P_USER_ID         IN NUMBER,
                                        P_VALIDATED_DATE  IN DATE) is

      l_line_num NUMBER := 0;

    BEGIN
        begin
            select nvl(max(a.LINE_NUMBER),0) + 1
            into l_line_num
            from TAC_AP_INVOICE_VALIDATED a
            where a.INVOICE_ID = P_INVOICE_ID;
        exception when others then
            l_line_num := 1;
        end;

        insert into TAC_AP_INVOICE_VALIDATED(INVOICE_ID,
                                             LINE_NUMBER,
                                             USER_ID,
                                             VALIDATED_DATE
                                             )
                                      values (P_INVOICE_ID,
                                              l_line_num,
                                              P_USER_ID,
                                              sysdate --P_VALIDATED_DATE
                                              );
        commit;

    END INSERT_VALIDATED_INVOICE;
    
END TAC_AP_INVOICES_PKG;
