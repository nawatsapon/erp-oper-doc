create or replace PACKAGE BODY TAC_AP_INVOICEHUB_PKG IS
/******************************************************************************************************************************************************
  Package Name        :DT_INVOICEHUB_PKG
  Description         :This package is modified for moving pr data from source concession to destination concession

  Revisions:
  Ver             Date                Author                      Description
  ---------       ---------           ---------------             ----------------------------------
  1.0             25/07/2016        Suttisak Uamornlert         Package creation    
  2.0             03/02/2017        Suttisak Uamornlert         Fixed case get wrong po release
  3.0             18/02/2017        Suttisak Uamornlert         Fixed case get po shipment number and send to ap invoice interface                         
  3.1             07/06/2017        Suttisak Uamornlert         User request change global attribute16 from FXTH to AP
  3.2             10/03/2018        Suttisak Uamornlert         Fixed case GRN Remaining is not enough ,Invoice shouldn't be interfaced
 *****************************************************************************************************************************************************/
  

  PROCEDURE GET_PROJECT_INFORMATION(P_REQ_HEADER_REFERENCE_NUM    IN VARCHAR2,
                                    P_REQ_LINE_REFERENCE_NUM      IN NUMBER,
                                    O_PROJECT_ID                  IN OUT PO_REQ_DISTRIBUTIONS_ALL.PROJECT_ID%TYPE,
                                    O_PROJECT_NAME                IN OUT PA_PROJECTS_ALL.SEGMENT1%TYPE,
                                    O_TASK_ID                     IN OUT PO_REQ_DISTRIBUTIONS_ALL.TASK_ID%TYPE,
                                    O_TASK_NAME                   IN OUT PA_TASKS.TASK_NUMBER%TYPE,
                                    O_EXPENDITURE_TYPE            IN OUT PO_REQ_DISTRIBUTIONS_ALL.EXPENDITURE_TYPE%TYPE,
                                    O_EXPENDITURE_ITEM_DATE       IN OUT PO_REQ_DISTRIBUTIONS_ALL.EXPENDITURE_ITEM_DATE%TYPE,
                                    O_EXPENDITURE_ORGANIZATION_ID IN OUT PO_REQ_DISTRIBUTIONS_ALL.EXPENDITURE_ORGANIZATION_ID%TYPE) IS
  BEGIN
    G_STEP := 'In  GET_PROJECT_INFORMATION';
    SELECT PRD.PROJECT_ID,
           PPA.SEGMENT1,
           PRD.TASK_ID,
           PT.TASK_NUMBER,
           PRD.EXPENDITURE_TYPE,
           PRD.EXPENDITURE_ITEM_DATE,
           PRD.EXPENDITURE_ORGANIZATION_ID
      INTO O_PROJECT_ID,
           O_PROJECT_NAME,
           O_TASK_ID,
           O_TASK_NAME,
           O_EXPENDITURE_TYPE,
           O_EXPENDITURE_ITEM_DATE,
           O_EXPENDITURE_ORGANIZATION_ID
      FROM PO_REQUISITION_HEADERS_ALL PRH,
           PO_REQUISITION_LINES_ALL   PRL,
           PO_REQ_DISTRIBUTIONS_ALL   PRD,
           PA_PROJECTS_ALL            PPA,
           PA_TASKS                   PT
     WHERE PRH.REQUISITION_HEADER_ID = PRL.REQUISITION_HEADER_ID
       AND PRL.REQUISITION_LINE_ID = PRD.REQUISITION_LINE_ID
       AND PRH.SEGMENT1 = P_REQ_HEADER_REFERENCE_NUM
       AND PRL.LINE_NUM = P_REQ_LINE_REFERENCE_NUM
       AND PRD.PROJECT_ID = PPA.PROJECT_ID(+)
       AND PRD.PROJECT_ID = PT.PROJECT_ID(+)
       AND PRD.TASK_ID = PT.TASK_ID(+);
  EXCEPTION
    WHEN OTHERS THEN
      O_PROJECT_ID                  := NULL;
      O_PROJECT_NAME                := NULL;
      O_TASK_ID                     := NULL;
      O_TASK_NAME                   := NULL;
      O_EXPENDITURE_TYPE            := NULL;
      O_EXPENDITURE_ITEM_DATE       := NULL;
      O_EXPENDITURE_ORGANIZATION_ID := NULL;
  END;

  ---------------------------------------------------------------------------------------------
  PROCEDURE UPDATE_AP_LINE(P_FILENAME       IN VARCHAR2,
                           GWRITELOG_DETAIL IN OUT TDTINVHUBLOG) IS
    CURSOR C_CHECK_DUP(P_FILENAME IN VARCHAR2) IS
      SELECT FILE_NAME,
             ORG_CODE,
             GROUP_NAME,
             INVOICE_NUM,
             LINE_NUMBER,
             PO_NUMBER,
             PO_LINE_NUMBER,
             RECEIPT_NUMBER,
             RECEIPT_LINE_NUMBER,
             VENDOR_NUM,
             MATCH_OPTION,
             COUNT(*) COUNT_INV,
             ROW_NUMBER() OVER(PARTITION BY INVOICE_NUM, VENDOR_NUM, MATCH_OPTION ORDER BY INVOICE_NUM) INV_LINE
        FROM TAC_INVHUB_INVOICE_STG
       WHERE FILE_NAME = P_FILENAME
         AND INTERFACE_PO_FLAG = 'Y'
         AND INTERFACE_AP_FLAG = 'N'
         AND INTERFACE_TYPE = '3WAY'
       GROUP BY FILE_NAME,
                ORG_CODE,
                GROUP_NAME,
                INVOICE_NUM,
                LINE_NUMBER,
                PO_NUMBER,
                PO_LINE_NUMBER,
                RECEIPT_NUMBER,
                RECEIPT_LINE_NUMBER,
                VENDOR_NUM,
                MATCH_OPTION
       ORDER BY 1, 2, 3, 4, 5, 6, 7, 8, 9;
  
    CURSOR C_CHECK_DUP_2W(P_FILENAME IN VARCHAR2) IS
      SELECT FILE_NAME,
             ORG_CODE,
             GROUP_NAME,
             INVOICE_NUM,
             LINE_NUMBER,
             PO_NUMBER,
             PO_LINE_NUMBER,
             RECEIPT_NUMBER,
             RECEIPT_LINE_NUMBER,
             VENDOR_NUM,
             MATCH_OPTION,
             PR_NUMBER,
             PR_LINE_NUMBER,
             COUNT(*) COUNT_INV,
             ROW_NUMBER() OVER(PARTITION BY INVOICE_NUM, VENDOR_NUM, MATCH_OPTION ORDER BY INVOICE_NUM) INV_LINE
        FROM TAC_INVHUB_INVOICE_STG
       WHERE FILE_NAME = P_FILENAME
         AND INTERFACE_PO_FLAG = 'Y'
         AND INTERFACE_AP_FLAG = 'N'
         AND INTERFACE_TYPE = '2WAY'
       GROUP BY FILE_NAME,
                ORG_CODE,
                GROUP_NAME,
                INVOICE_NUM,
                LINE_NUMBER,
                PO_NUMBER,
                PO_LINE_NUMBER,
                RECEIPT_NUMBER,
                RECEIPT_LINE_NUMBER,
                VENDOR_NUM,
                MATCH_OPTION,
                PR_NUMBER,
                PR_LINE_NUMBER
       ORDER BY 1, 2, 3, 4, 5, 6, 7, 8, 9;
  
    L_PREV_INV  VARCHAR2(50) := NULL;
    L_PREV_LINE NUMBER := NULL;
    NEXT_LINE   NUMBER;
    P_INVLOG    DTINVHUB_LOG_DETAIL%ROWTYPE;
    P_ERR_MSG   VARCHAR2(50);
    P_ERR_CODE  VARCHAR2(240);
    L_STARTTIME TIMESTAMP;
    L_ENDTIME   TIMESTAMP;
  BEGIN
    WRITE_LOG('In update_ap_line');
    WRITE_LOG('File type :' || SUBSTR(P_FILENAME, 6, 2));
  
    IF SUBSTR(P_FILENAME, 6, 2) = '3W' THEN
      BEGIN
        WRITE_LOG('in loop for 3way');
      
        FOR I IN C_CHECK_DUP(P_FILENAME) LOOP
          BEGIN
            -- IF l_prev_inv is NULL OR l_prev_inv!= i.invoice_num THEN
            --    l_prev_inv :=i.invoice_num;
            -- END IF;
          
            SELECT CURRENT_TIMESTAMP INTO L_STARTTIME FROM DUAL;
          
            WRITE_LOG('i.invoice_num=' || I.INVOICE_NUM);
            WRITE_LOG('i.LINE_NUMBER=' || I.LINE_NUMBER);
            WRITE_LOG('count=' || I.COUNT_INV);
          
            IF I.COUNT_INV > 1 THEN
              BEGIN
                WRITE_LOG('Update duplicate record');
              
                UPDATE TAC_INVHUB_INVOICE_STG
                   SET INTERFACE_AP_FLAG = 'E'
                 WHERE FILE_NAME = I.FILE_NAME
                   AND INTERFACE_TYPE = '3WAY'
                   AND INVOICE_NUM = I.INVOICE_NUM
                   AND INTERFACE_PO_FLAG = 'Y'
                   AND INTERFACE_AP_FLAG = 'N';
              
                P_INVLOG.LEVEL_TYPE        := 'D';
                P_INVLOG.REQUEST_ID        := FND_GLOBAL.CONC_REQUEST_ID;
                P_INVLOG.ERROR_TYPE        := 'AP Invoice Interface';
                P_INVLOG.AP_INVOICE        := I.INVOICE_NUM;
                P_INVLOG.AP_INVOIE_LINE    := I.LINE_NUMBER;
                P_INVLOG.PO_NUMBER         := I.PO_NUMBER;
                P_INVLOG.PO_LINE_NUMBER    := I.PO_LINE_NUMBER;
                P_INVLOG.RECEIPT_NUMBER    := I.RECEIPT_NUMBER;
                P_INVLOG.RECEIPT_LINE_NUM  := I.RECEIPT_LINE_NUMBER;
                P_INVLOG.PR_NUMBER         := NULL;
                P_INVLOG.PR_LINE_NUMBER    := NULL;
                P_INVLOG.MATCH_OPTION      := I.MATCH_OPTION;
                P_INVLOG.VENDOR_NUMBER     := I.VENDOR_NUM;
                P_INVLOG.VENDOR_SITE_NAME  := NULL;
                P_INVLOG.VENDOR_NAME       := NULL;
                P_INVLOG.DUE_DATE          := SYSDATE;
                P_INVLOG.PAYMENT_STATUS    := NULL;
                P_INVLOG.PV_NUMBER         := NULL;
                P_INVLOG.DOCUMENT_NUMBER   := NULL;
                P_INVLOG.AP_VOUCHER_NUMBER := NULL;
                P_INVLOG.PAYGROUP          := NULL;
                P_INVLOG.PAYMENT_TERM      := NULL;
                P_INVLOG.ERROR_CODE        := NULL;
                P_INVLOG.ERROR_MESSAGE     := NULL;
                P_INVLOG.PROCESSING_DATE   := L_STARTTIME; -- v_timestamp_start;
              
                SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
              
                P_INVLOG.PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                           L_STARTTIME),
                                                   12,
                                                   12); --systimestamp - v_timestamp_start;
                P_ERR_MSG                := 'Duplicate Invoice in ERP'; --'*** INTERFACE DATA ERROR : Duplicate Invoice in ERP';
                P_ERR_CODE               := 'INVHUB003-003'; --'Error  INVHUB003-003 : Duplicate Invoice in ERP';
              
                WRITE_INVDETAIL_LOG(P_INVLOG,
                                    GWRITELOG_DETAIL,
                                    P_ERR_CODE,
                                    P_ERR_MSG);
              END;
            ELSE
              BEGIN
                UPDATE TAC_INVHUB_INVOICE_STG
                   SET ERP_AP_LINENUMBER = I.INV_LINE
                 WHERE FILE_NAME = I.FILE_NAME
                   AND INTERFACE_TYPE = '3WAY'
                   AND INVOICE_NUM = I.INVOICE_NUM
                   AND LINE_NUMBER = I.LINE_NUMBER
                   AND PO_NUMBER = I.PO_NUMBER
                   AND PO_LINE_NUMBER = I.PO_LINE_NUMBER
                   AND RECEIPT_NUMBER = I.RECEIPT_NUMBER
                   AND RECEIPT_LINE_NUMBER = I.RECEIPT_LINE_NUMBER
                   AND INTERFACE_PO_FLAG = 'Y'
                   AND INTERFACE_AP_FLAG = 'N';
              END;
            END IF;
          END;
        END LOOP;
      END;
    ELSIF SUBSTR(P_FILENAME, 6, 2) = '2W' THEN
      BEGIN
        WRITE_LOG('in loop for 2way');
      
        FOR I IN C_CHECK_DUP_2W(P_FILENAME) LOOP
          BEGIN
            -- IF l_prev_inv is NULL OR l_prev_inv!= i.invoice_num THEN
            --    l_prev_inv :=i.invoice_num;
            -- END IF;
          
            SELECT CURRENT_TIMESTAMP INTO L_STARTTIME FROM DUAL;
          
            WRITE_LOG('i.invoice_num=' || I.INVOICE_NUM);
            WRITE_LOG('i.LINE_NUMBER=' || I.LINE_NUMBER);
            WRITE_LOG('count=' || I.COUNT_INV);
          
            IF I.COUNT_INV > 1 THEN
              BEGIN
                WRITE_LOG('Update duplicate record');
              
                UPDATE TAC_INVHUB_INVOICE_STG
                   SET INTERFACE_AP_FLAG = 'E'
                 WHERE FILE_NAME = I.FILE_NAME
                   AND INTERFACE_TYPE = '2WAY'
                   AND INVOICE_NUM = I.INVOICE_NUM
                   AND INTERFACE_PO_FLAG = 'Y'
                   AND INTERFACE_AP_FLAG = 'N';
              
                P_INVLOG.LEVEL_TYPE     := 'D';
                P_INVLOG.REQUEST_ID     := FND_GLOBAL.CONC_REQUEST_ID;
                P_INVLOG.ERROR_TYPE     := 'AP Invoice Interface';
                P_INVLOG.AP_INVOICE     := I.INVOICE_NUM;
                P_INVLOG.AP_INVOIE_LINE := I.LINE_NUMBER;
                --p_invlog.PO_NUMBER             := i.PO_NUMBER;
                --p_invlog.PO_LINE_NUMBER        := i.PO_LINE_NUMBER;
                --p_invlog.RECEIPT_NUMBER        := i.RECEIPT_NUMBER;
                --p_invlog.RECEIPT_LINE_NUM      := i.RECEIPT_LINE_NUMBER;
                P_INVLOG.PR_NUMBER         := I.PR_NUMBER;
                P_INVLOG.PR_LINE_NUMBER    := I.PR_LINE_NUMBER;
                P_INVLOG.MATCH_OPTION      := I.MATCH_OPTION;
                P_INVLOG.VENDOR_NUMBER     := I.VENDOR_NUM;
                P_INVLOG.VENDOR_SITE_NAME  := NULL;
                P_INVLOG.VENDOR_NAME       := NULL;
                P_INVLOG.DUE_DATE          := SYSDATE;
                P_INVLOG.PAYMENT_STATUS    := NULL;
                P_INVLOG.PV_NUMBER         := NULL;
                P_INVLOG.DOCUMENT_NUMBER   := NULL;
                P_INVLOG.AP_VOUCHER_NUMBER := NULL;
                P_INVLOG.PAYGROUP          := NULL;
                P_INVLOG.PAYMENT_TERM      := NULL;
                P_INVLOG.ERROR_CODE        := NULL;
                P_INVLOG.ERROR_MESSAGE     := NULL;
                P_INVLOG.PROCESSING_DATE   := L_STARTTIME; -- v_timestamp_start;
              
                SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
              
                P_INVLOG.PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                           L_STARTTIME),
                                                   12,
                                                   12); --systimestamp - v_timestamp_start;
                P_ERR_MSG                := 'Duplicate Invoice in ERP'; --'*** INTERFACE DATA ERROR : Duplicate Invoice in ERP';
                P_ERR_CODE               := 'INVHUB003-003'; --'Error  INVHUB003-003 : Duplicate Invoice in ERP';
              
                WRITE_INVDETAIL_LOG(P_INVLOG,
                                    GWRITELOG_DETAIL,
                                    P_ERR_CODE,
                                    P_ERR_MSG);
              END;
            ELSE
              BEGIN
                UPDATE TAC_INVHUB_INVOICE_STG
                   SET ERP_AP_LINENUMBER = I.INV_LINE
                 WHERE FILE_NAME = I.FILE_NAME
                   AND INTERFACE_TYPE = '2WAY'
                   AND INVOICE_NUM = I.INVOICE_NUM
                   AND LINE_NUMBER = I.LINE_NUMBER
                   AND PR_NUMBER = I.PR_NUMBER
                   AND PR_LINE_NUMBER = I.PR_LINE_NUMBER
                      --AND PO_NUMBER=i.po_number
                      --AND PO_LINE_NUMBER=i.PO_LINE_NUMBER
                      --AND RECEIPT_NUMBER =i.RECEIPT_NUMBER
                      --AND RECEIPT_LINE_NUMBER=i.RECEIPT_LINE_NUMBER
                   AND INTERFACE_PO_FLAG = 'Y'
                   AND INTERFACE_AP_FLAG = 'N';
              END;
            END IF;
          END;
        END LOOP;
      END;
    END IF;
  
    COMMIT;
  END;

  ----------------------------------------------------------------------------------------------------
  FUNCTION F_GET_INVOICE_DESCRIPTION(P_INTERFACE_TYPE IN VARCHAR2,
                                     P_PO_NUMBER      IN VARCHAR2,
                                     P_PO_LINE_NUMBER IN NUMBER,
                                     P_PR_NUMBER      IN VARCHAR2,
                                     P_PR_LINE_NUMBER IN NUMBER)
    RETURN VARCHAR2 IS
    L_DESCRIPTION VARCHAR2(240);
    L_PO_HEADER   VARCHAR2(40);
    L_PO_RELEASE  NUMBER;
  BEGIN
    IF P_INTERFACE_TYPE = '2WAY' THEN
      BEGIN
        SELECT PRL.ITEM_DESCRIPTION
          INTO L_DESCRIPTION
          FROM PO_REQUISITION_HEADERS_ALL PRH, PO_REQUISITION_LINES_ALL PRL
         WHERE PRH.REQUISITION_HEADER_ID = PRL.REQUISITION_HEADER_ID
           AND PRH.SEGMENT1 = P_PR_NUMBER
           AND PRL.LINE_NUM = NVL(P_PR_LINE_NUMBER, PRL.LINE_NUM)
           AND ROWNUM = 1;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          L_DESCRIPTION := NULL;
      END;
    ELSIF P_INTERFACE_TYPE = '3WAY' THEN
      BEGIN
        IF INSTR(P_PO_NUMBER, '-') != 0 THEN
          L_PO_HEADER  := SUBSTR(P_PO_NUMBER,
                                 1,
                                 INSTR(P_PO_NUMBER, '-') - 1);
          L_PO_RELEASE := SUBSTR(P_PO_NUMBER,
                                 INSTR(P_PO_NUMBER, '-') + 1,
                                 LENGTH(P_PO_NUMBER) -
                                 INSTR(P_PO_NUMBER, '-'));
        ELSE
          L_PO_HEADER  := P_PO_NUMBER;
          L_PO_RELEASE := 0;
        END IF;
      
        SELECT POL.ITEM_DESCRIPTION
          INTO L_DESCRIPTION
          FROM PO_HEADERS_ALL  POH,
               PO_RELEASES_ALL PO_RELEASE,
               PO_LINES_ALL    POL
         WHERE POH.PO_HEADER_ID = POL.PO_HEADER_ID
           AND POH.ORG_ID = POL.ORG_ID
           AND POH.PO_HEADER_ID = PO_RELEASE.PO_HEADER_ID(+)
           AND POH.ORG_ID = PO_RELEASE.ORG_ID(+)
           AND POH.SEGMENT1 = L_PO_HEADER
           AND NVL(PO_RELEASE.RELEASE_NUM, 0) = L_PO_RELEASE
           AND POL.LINE_NUM = NVL(P_PO_LINE_NUMBER, POL.LINE_NUM)
           AND ROWNUM = 1;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          L_DESCRIPTION := NULL;
      END;
    ELSE
      RETURN(NULL);
    END IF;
  
    RETURN(L_DESCRIPTION);
  END;

  PROCEDURE WRITE_LOG(PARAM_MSG VARCHAR2) IS
  BEGIN
    FND_FILE.PUT_LINE(FND_FILE.LOG, PARAM_MSG);
  END WRITE_LOG;

  PROCEDURE WRITE_INVSUMMARY_LOG(GWRITELOG_DETAIL     IN OUT TDTINVHUBLOG,
                                 P_CURRENT_REQUEST_ID IN NUMBER,
                                 P_STARTPROCESS       IN TIMESTAMP) IS
    L_ENDPROCESS TIMESTAMP;
  BEGIN
    WRITE_LOG('Total record in gWritelog_detail :' ||
              GWRITELOG_DETAIL.COUNT);
  
    IF GWRITELOG_DETAIL.COUNT > 0 THEN
      FORALL I IN 1 .. GWRITELOG_DETAIL.COUNT
        INSERT INTO APPS.DTINVHUB_LOG_DETAIL VALUES GWRITELOG_DETAIL (I);
    
      WRITE_LOG('p_current_request_id :' || P_CURRENT_REQUEST_ID);
    END IF;
  
    WRITE_LOG('Total record in gWritelog_detail :' ||
              GWRITELOG_DETAIL.COUNT);
  
    SELECT CURRENT_TIMESTAMP INTO L_ENDPROCESS FROM DUAL;
  
    INSERT INTO APPS.DTINVHUB_LOG_SUMMARY
      (LEVEL_TYPE,
       REQUEST_ID,
       PROGRAM_NAME,
       PROCESS_DATE,
       PROCESSING_TYPE,
       RECORD_COUNT_COMPLETION,
       RECORD_COUNT_ERROR,
       PROCESSING_DATE)
      SELECT 'H',
             REQUEST_ID,
             'DTINVHUB003',
             P_STARTPROCESS,
             'Download',
             SUM(DECODE(ERROR_CODE, NULL, 1, 0)) COMPLETE,
             SUM(DECODE(ERROR_CODE, NULL, 0, 1)) ERROR,
             SUBSTR(TO_CHAR(L_ENDPROCESS - P_STARTPROCESS), 12, 12)
        FROM APPS.DTINVHUB_LOG_DETAIL
       WHERE REQUEST_ID = P_CURRENT_REQUEST_ID
       GROUP BY 'H',
                REQUEST_ID,
                'DTINVHUB003',
                SYSDATE,
                'Download',
                SUBSTR(TO_CHAR(L_ENDPROCESS - P_STARTPROCESS), 12, 15);
  
    WRITE_LOG('Total record in apps.dtinvhub_log_summary :' ||
              SQL%ROWCOUNT);
    /* forall i in 1..pSummary_log.count
    insert into apps.DTINVHUB_LOG_SUMMARY values pSummary_log(i);*/
  END;

  PROCEDURE WRITE_INVDETAIL_LOG(P_INVLOG         IN DTINVHUB_LOG_DETAIL%ROWTYPE,
                                GWRITELOG_DETAIL IN OUT TDTINVHUBLOG,
                                PERR_CODE        IN VARCHAR2,
                                PMESSAGE_ERR     IN VARCHAR2) IS
    L_ENDTIME TIMESTAMP;
  BEGIN
    WRITE_LOG('in write_invdetail_log');
  
    SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
  
    GWRITELOG_DETAIL.EXTEND;
    GWRITELOG_DETAIL(GWRITELOG_DETAIL.COUNT).LEVEL_TYPE := P_INVLOG.LEVEL_TYPE;
    GWRITELOG_DETAIL(GWRITELOG_DETAIL.COUNT).REQUEST_ID := P_INVLOG.REQUEST_ID;
    GWRITELOG_DETAIL(GWRITELOG_DETAIL.COUNT).ERROR_TYPE := P_INVLOG.ERROR_TYPE;
    GWRITELOG_DETAIL(GWRITELOG_DETAIL.COUNT).AP_INVOICE := P_INVLOG.AP_INVOICE;
    GWRITELOG_DETAIL(GWRITELOG_DETAIL.COUNT).AP_INVOIE_LINE := P_INVLOG.AP_INVOIE_LINE;
    GWRITELOG_DETAIL(GWRITELOG_DETAIL.COUNT).PO_NUMBER := P_INVLOG.PO_NUMBER;
    GWRITELOG_DETAIL(GWRITELOG_DETAIL.COUNT).PO_LINE_NUMBER := P_INVLOG.PO_LINE_NUMBER;
    GWRITELOG_DETAIL(GWRITELOG_DETAIL.COUNT).RECEIPT_NUMBER := P_INVLOG.RECEIPT_NUMBER;
    GWRITELOG_DETAIL(GWRITELOG_DETAIL.COUNT).RECEIPT_LINE_NUM := P_INVLOG.RECEIPT_LINE_NUM;
    GWRITELOG_DETAIL(GWRITELOG_DETAIL.COUNT).PR_NUMBER := P_INVLOG.PR_NUMBER;
    GWRITELOG_DETAIL(GWRITELOG_DETAIL.COUNT).PR_LINE_NUMBER := P_INVLOG.PR_LINE_NUMBER;
    GWRITELOG_DETAIL(GWRITELOG_DETAIL.COUNT).MATCH_OPTION := P_INVLOG.MATCH_OPTION;
    GWRITELOG_DETAIL(GWRITELOG_DETAIL.COUNT).VENDOR_NUMBER := P_INVLOG.VENDOR_NUMBER;
    GWRITELOG_DETAIL(GWRITELOG_DETAIL.COUNT).VENDOR_SITE_NAME := P_INVLOG.VENDOR_SITE_NAME;
    GWRITELOG_DETAIL(GWRITELOG_DETAIL.COUNT).VENDOR_NAME := P_INVLOG.VENDOR_NAME;
    GWRITELOG_DETAIL(GWRITELOG_DETAIL.COUNT).DUE_DATE := P_INVLOG.DUE_DATE;
    GWRITELOG_DETAIL(GWRITELOG_DETAIL.COUNT).PAYMENT_STATUS := P_INVLOG.PAYMENT_STATUS;
    GWRITELOG_DETAIL(GWRITELOG_DETAIL.COUNT).PV_NUMBER := P_INVLOG.PV_NUMBER;
    GWRITELOG_DETAIL(GWRITELOG_DETAIL.COUNT).DOCUMENT_NUMBER := P_INVLOG.DOCUMENT_NUMBER;
    GWRITELOG_DETAIL(GWRITELOG_DETAIL.COUNT).AP_VOUCHER_NUMBER := P_INVLOG.AP_VOUCHER_NUMBER;
    GWRITELOG_DETAIL(GWRITELOG_DETAIL.COUNT).PAYGROUP := P_INVLOG.PAYGROUP;
    GWRITELOG_DETAIL(GWRITELOG_DETAIL.COUNT).PAYMENT_TERM := P_INVLOG.PAYMENT_TERM;
    GWRITELOG_DETAIL(GWRITELOG_DETAIL.COUNT).ERROR_CODE := PERR_CODE;
    GWRITELOG_DETAIL(GWRITELOG_DETAIL.COUNT).ERROR_MESSAGE := PMESSAGE_ERR;
    GWRITELOG_DETAIL(GWRITELOG_DETAIL.COUNT).PROCESSING_DATE := P_INVLOG.PROCESSING_DATE;
    GWRITELOG_DETAIL(GWRITELOG_DETAIL.COUNT).PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                                               P_INVLOG.PROCESSING_DATE),
                                                                       12,
                                                                       12);
  END WRITE_INVDETAIL_LOG;

  PROCEDURE WRITE_OUT(PARAM_MSG VARCHAR2) IS
  BEGIN
    FND_FILE.PUT_LINE(FND_FILE.OUTPUT, PARAM_MSG);
  END WRITE_OUT;

  PROCEDURE GET_PO_INFORMATION(P_PO_NUMBER     IN PO_HEADERS_ALL.SEGMENT1%TYPE,
                               P_PO_LINENUMBER IN PO_LINES_ALL.LINE_NUM%TYPE,
                               -- p_po_header_id OUT po_distributions_all.po_header_id%TYPE,
                               -- p_po_line_id   OUT po_distributions_all.po_line_id%TYPE,
                               P_PO_LINE_LOC_ID OUT PO_DISTRIBUTIONS_ALL.LINE_LOCATION_ID%TYPE,
                               P_PO_DIST_ID     OUT PO_DISTRIBUTIONS_ALL.PO_DISTRIBUTION_ID%TYPE,
                               P_MATCH_OPTION   OUT VARCHAR2) IS
    L_PO_HEADER  VARCHAR2(40);
    L_PO_RELEASE NUMBER;
  BEGIN
    IF P_PO_NUMBER IS NOT NULL AND P_PO_LINENUMBER IS NOT NULL THEN
      BEGIN
        IF INSTR(P_PO_NUMBER, '-') != 0 THEN
          L_PO_HEADER  := SUBSTR(P_PO_NUMBER,
                                 1,
                                 INSTR(P_PO_NUMBER, '-') - 1);
          L_PO_RELEASE := SUBSTR(P_PO_NUMBER,
                                 INSTR(P_PO_NUMBER, '-') + 1,
                                 LENGTH(P_PO_NUMBER) -
                                 INSTR(P_PO_NUMBER, '-'));
        ELSE
          L_PO_HEADER  := P_PO_NUMBER;
          L_PO_RELEASE := 0;
        END IF;
      
        SELECT --pd.po_header_id,
        --pd.po_line_id,
         PD.LINE_LOCATION_ID, PD.PO_DISTRIBUTION_ID, PLL.MATCH_OPTION
          INTO --p_po_header_id,
               --p_po_line_id,
                P_PO_LINE_LOC_ID,
               P_PO_DIST_ID,
               P_MATCH_OPTION
          FROM PO_HEADERS_ALL        POH,
               PO_RELEASES_ALL       PO_RELEASE,
               PO_LINES_ALL          POL,
               PO_LINE_LOCATIONS_ALL PLL,
               PO_DISTRIBUTIONS_ALL  PD
         WHERE POH.PO_HEADER_ID = POL.PO_HEADER_ID
           AND POH.ORG_ID = POL.ORG_ID
           AND POH.PO_HEADER_ID = PO_RELEASE.PO_HEADER_ID(+)
           AND POH.ORG_ID = PO_RELEASE.ORG_ID(+)
           AND POL.PO_HEADER_ID = PLL.PO_HEADER_ID
           AND POL.PO_LINE_ID = PLL.PO_LINE_ID
           AND POL.ORG_ID = PLL.ORG_ID
           AND PLL.PO_HEADER_ID = PD.PO_HEADER_ID
           AND PLL.PO_LINE_ID = PD.PO_LINE_ID
           AND PLL.ORG_ID = PD.ORG_ID
           AND PLL.LINE_LOCATION_ID = PD.LINE_LOCATION_ID
              --AND poh.segment1||DECODE(po_release.release_num,NULL,NULL,'-'||po_release.release_num)=p_po_number
              --AND poh.segment1=p_po_number
           AND NVL(PLL.PO_RELEASE_ID, 0) = NVL(PO_RELEASE.PO_RELEASE_ID, 0) /* fix 3 Feb 2017*/
           AND POH.SEGMENT1 = L_PO_HEADER
           AND NVL(PO_RELEASE.RELEASE_NUM, 0) = L_PO_RELEASE
           AND POL.LINE_NUM = P_PO_LINENUMBER
           AND ROWNUM = 1;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          --p_po_header_id := NULL;
          --p_po_line_id := NULL;
          P_PO_LINE_LOC_ID := NULL;
          P_PO_DIST_ID     := NULL;
          P_MATCH_OPTION   := NULL;
      END;
    END IF;
  END;

  PROCEDURE CONC_WAIT(PARAM_REQ IN NUMBER --,phase         out varchar2
                      --,dev_phase     out varchar2
                      ) IS
    V_RESULT   VARCHAR2(1);
    PHASE      VARCHAR2(30);
    STATUS     VARCHAR2(30);
    DEV_PHASE  VARCHAR2(30);
    DEV_STATUS VARCHAR2(30);
    MESSAGE    VARCHAR2(1000);
  BEGIN
    COMMIT;
  
    IF FND_CONCURRENT.WAIT_FOR_REQUEST(PARAM_REQ,
                                       10,
                                       0,
                                       PHASE,
                                       STATUS,
                                       DEV_PHASE,
                                       DEV_STATUS,
                                       MESSAGE) THEN
      NULL;
    END IF;
  END CONC_WAIT;

  ---------------------------------------------------------------------------------------
  PROCEDURE INSERT_INVOICE(IR_INVOICE IN AP_INVOICES_INTERFACE%ROWTYPE) IS
  BEGIN
    INSERT INTO AP_INVOICES_INTERFACE
      (INVOICE_ID,
       INVOICE_NUM,
       INVOICE_TYPE_LOOKUP_CODE,
       INVOICE_DATE,
       PO_NUMBER,
       VENDOR_ID,
       VENDOR_NUM,
       VENDOR_NAME,
       VENDOR_SITE_ID,
       VENDOR_SITE_CODE,
       INVOICE_AMOUNT,
       INVOICE_CURRENCY_CODE,
       EXCHANGE_RATE,
       EXCHANGE_RATE_TYPE,
       EXCHANGE_DATE,
       TERMS_ID,
       TERMS_NAME,
       DESCRIPTION,
       AWT_GROUP_ID,
       AWT_GROUP_NAME,
       LAST_UPDATE_DATE,
       LAST_UPDATED_BY,
       LAST_UPDATE_LOGIN,
       CREATION_DATE,
       CREATED_BY,
       ATTRIBUTE_CATEGORY,
       ATTRIBUTE1,
       ATTRIBUTE2,
       ATTRIBUTE3,
       ATTRIBUTE4,
       ATTRIBUTE5,
       ATTRIBUTE6,
       ATTRIBUTE7,
       ATTRIBUTE8,
       ATTRIBUTE9,
       ATTRIBUTE10,
       ATTRIBUTE11,
       ATTRIBUTE12,
       ATTRIBUTE13,
       ATTRIBUTE14,
       ATTRIBUTE15,
       GLOBAL_ATTRIBUTE_CATEGORY,
       GLOBAL_ATTRIBUTE1,
       GLOBAL_ATTRIBUTE2,
       GLOBAL_ATTRIBUTE3,
       GLOBAL_ATTRIBUTE4,
       GLOBAL_ATTRIBUTE5,
       GLOBAL_ATTRIBUTE6,
       GLOBAL_ATTRIBUTE7,
       GLOBAL_ATTRIBUTE8,
       GLOBAL_ATTRIBUTE9,
       GLOBAL_ATTRIBUTE10,
       GLOBAL_ATTRIBUTE11,
       GLOBAL_ATTRIBUTE12,
       GLOBAL_ATTRIBUTE13,
       GLOBAL_ATTRIBUTE14,
       GLOBAL_ATTRIBUTE15,
       GLOBAL_ATTRIBUTE16,
       GLOBAL_ATTRIBUTE17,
       GLOBAL_ATTRIBUTE18,
       GLOBAL_ATTRIBUTE19,
       GLOBAL_ATTRIBUTE20,
       STATUS,
       SOURCE,
       GROUP_ID,
       REQUEST_ID,
       PAYMENT_CROSS_RATE_TYPE,
       PAYMENT_CROSS_RATE_DATE,
       PAYMENT_CROSS_RATE,
       PAYMENT_CURRENCY_CODE,
       WORKFLOW_FLAG,
       DOC_CATEGORY_CODE,
       VOUCHER_NUM,
       PAYMENT_METHOD_LOOKUP_CODE,
       PAY_GROUP_LOOKUP_CODE,
       GOODS_RECEIVED_DATE,
       INVOICE_RECEIVED_DATE,
       GL_DATE,
       ACCTS_PAY_CODE_COMBINATION_ID,
       USSGL_TRANSACTION_CODE,
       EXCLUSIVE_PAYMENT_FLAG,
       ORG_ID,
       AMOUNT_APPLICABLE_TO_DISCOUNT,
       PREPAY_NUM,
       PREPAY_DIST_NUM,
       PREPAY_APPLY_AMOUNT,
       PREPAY_GL_DATE,
       INVOICE_INCLUDES_PREPAY_FLAG,
       NO_XRATE_BASE_AMOUNT,
       VENDOR_EMAIL_ADDRESS,
       TERMS_DATE,
       REQUESTER_ID,
       SHIP_TO_LOCATION,
       EXTERNAL_DOC_REF)
    VALUES
      (IR_INVOICE.INVOICE_ID,
       IR_INVOICE.INVOICE_NUM,
       IR_INVOICE.INVOICE_TYPE_LOOKUP_CODE,
       IR_INVOICE.INVOICE_DATE,
       IR_INVOICE.PO_NUMBER,
       IR_INVOICE.VENDOR_ID,
       IR_INVOICE.VENDOR_NUM,
       IR_INVOICE.VENDOR_NAME,
       IR_INVOICE.VENDOR_SITE_ID,
       IR_INVOICE.VENDOR_SITE_CODE,
       ROUND(IR_INVOICE.INVOICE_AMOUNT, 2),
       IR_INVOICE.INVOICE_CURRENCY_CODE,
       IR_INVOICE.EXCHANGE_RATE,
       IR_INVOICE.EXCHANGE_RATE_TYPE,
       IR_INVOICE.EXCHANGE_DATE,
       IR_INVOICE.TERMS_ID,
       IR_INVOICE.TERMS_NAME,
       IR_INVOICE.DESCRIPTION,
       IR_INVOICE.AWT_GROUP_ID,
       IR_INVOICE.AWT_GROUP_NAME,
       IR_INVOICE.LAST_UPDATE_DATE,
       IR_INVOICE.LAST_UPDATED_BY,
       IR_INVOICE.LAST_UPDATE_LOGIN,
       IR_INVOICE.CREATION_DATE,
       IR_INVOICE.CREATED_BY,
       IR_INVOICE.ATTRIBUTE_CATEGORY,
       IR_INVOICE.ATTRIBUTE1,
       IR_INVOICE.ATTRIBUTE2,
       IR_INVOICE.ATTRIBUTE3,
       IR_INVOICE.ATTRIBUTE4,
       IR_INVOICE.ATTRIBUTE5,
       IR_INVOICE.ATTRIBUTE6,
       IR_INVOICE.ATTRIBUTE7,
       IR_INVOICE.ATTRIBUTE8,
       IR_INVOICE.ATTRIBUTE9,
       IR_INVOICE.ATTRIBUTE10,
       IR_INVOICE.ATTRIBUTE11,
       IR_INVOICE.ATTRIBUTE12,
       IR_INVOICE.ATTRIBUTE13,
       IR_INVOICE.ATTRIBUTE14,
       IR_INVOICE.ATTRIBUTE15,
       IR_INVOICE.GLOBAL_ATTRIBUTE_CATEGORY,
       IR_INVOICE.GLOBAL_ATTRIBUTE1,
       IR_INVOICE.GLOBAL_ATTRIBUTE2,
       IR_INVOICE.GLOBAL_ATTRIBUTE3,
       IR_INVOICE.GLOBAL_ATTRIBUTE4,
       IR_INVOICE.GLOBAL_ATTRIBUTE5,
       IR_INVOICE.GLOBAL_ATTRIBUTE6,
       IR_INVOICE.GLOBAL_ATTRIBUTE7,
       IR_INVOICE.GLOBAL_ATTRIBUTE8,
       IR_INVOICE.GLOBAL_ATTRIBUTE9,
       IR_INVOICE.GLOBAL_ATTRIBUTE10,
       IR_INVOICE.GLOBAL_ATTRIBUTE11,
       IR_INVOICE.GLOBAL_ATTRIBUTE12,
       IR_INVOICE.GLOBAL_ATTRIBUTE13,
       IR_INVOICE.GLOBAL_ATTRIBUTE14,
       IR_INVOICE.GLOBAL_ATTRIBUTE15,
       IR_INVOICE.GLOBAL_ATTRIBUTE16,
       IR_INVOICE.GLOBAL_ATTRIBUTE17,
       IR_INVOICE.GLOBAL_ATTRIBUTE18,
       IR_INVOICE.GLOBAL_ATTRIBUTE19,
       IR_INVOICE.GLOBAL_ATTRIBUTE20,
       IR_INVOICE.STATUS,
       IR_INVOICE.SOURCE,
       IR_INVOICE.GROUP_ID,
       IR_INVOICE.REQUEST_ID,
       IR_INVOICE.PAYMENT_CROSS_RATE_TYPE,
       IR_INVOICE.PAYMENT_CROSS_RATE_DATE,
       IR_INVOICE.PAYMENT_CROSS_RATE,
       IR_INVOICE.PAYMENT_CURRENCY_CODE,
       IR_INVOICE.WORKFLOW_FLAG,
       IR_INVOICE.DOC_CATEGORY_CODE,
       IR_INVOICE.VOUCHER_NUM,
       IR_INVOICE.PAYMENT_METHOD_LOOKUP_CODE,
       IR_INVOICE.PAY_GROUP_LOOKUP_CODE,
       IR_INVOICE.GOODS_RECEIVED_DATE,
       IR_INVOICE.INVOICE_RECEIVED_DATE,
       IR_INVOICE.GL_DATE,
       IR_INVOICE.ACCTS_PAY_CODE_COMBINATION_ID,
       IR_INVOICE.USSGL_TRANSACTION_CODE,
       IR_INVOICE.EXCLUSIVE_PAYMENT_FLAG,
       IR_INVOICE.ORG_ID,
       IR_INVOICE.AMOUNT_APPLICABLE_TO_DISCOUNT,
       IR_INVOICE.PREPAY_NUM,
       IR_INVOICE.PREPAY_DIST_NUM,
       IR_INVOICE.PREPAY_APPLY_AMOUNT,
       IR_INVOICE.PREPAY_GL_DATE,
       IR_INVOICE.INVOICE_INCLUDES_PREPAY_FLAG,
       IR_INVOICE.NO_XRATE_BASE_AMOUNT,
       IR_INVOICE.VENDOR_EMAIL_ADDRESS,
       IR_INVOICE.TERMS_DATE,
       IR_INVOICE.REQUESTER_ID,
       IR_INVOICE.SHIP_TO_LOCATION,
       IR_INVOICE.EXTERNAL_DOC_REF);
  END INSERT_INVOICE;

  ------------------------------------------------------------------------------
  PROCEDURE INSERT_INVOICE_LINE(IR_INVOICE_LINE IN AP_INVOICE_LINES_INTERFACE%ROWTYPE) IS
  BEGIN
    FND_FILE.PUT_LINE(FND_FILE.LOG,
                      'ir_invoice_line.po_header_id:' ||
                      IR_INVOICE_LINE.PO_HEADER_ID);
  
    INSERT INTO AP_INVOICE_LINES_INTERFACE
      (INVOICE_ID,
       INVOICE_LINE_ID,
       LINE_NUMBER,
       LINE_TYPE_LOOKUP_CODE,
       LINE_GROUP_NUMBER,
       AMOUNT,
       ACCOUNTING_DATE,
       DESCRIPTION,
       AMOUNT_INCLUDES_TAX_FLAG,
       PRORATE_ACROSS_FLAG,
       TAX_CODE,
       TAX_CODE_ID,
       TAX_CODE_OVERRIDE_FLAG,
       TAX_RECOVERY_RATE,
       TAX_RECOVERY_OVERRIDE_FLAG,
       TAX_RECOVERABLE_FLAG,
       FINAL_MATCH_FLAG,
       PO_HEADER_ID,
       PO_LINE_ID,
       PO_LINE_LOCATION_ID,
       PO_DISTRIBUTION_ID,
       PO_UNIT_OF_MEASURE,
       INVENTORY_ITEM_ID,
       QUANTITY_INVOICED,
       UNIT_PRICE,
       DISTRIBUTION_SET_ID,
       DIST_CODE_CONCATENATED,
       DIST_CODE_COMBINATION_ID,
       AWT_GROUP_ID,
       ATTRIBUTE_CATEGORY,
       ATTRIBUTE1,
       ATTRIBUTE2,
       ATTRIBUTE3,
       ATTRIBUTE4,
       ATTRIBUTE5,
       ATTRIBUTE6,
       ATTRIBUTE7,
       ATTRIBUTE8,
       ATTRIBUTE9,
       ATTRIBUTE10,
       ATTRIBUTE11,
       ATTRIBUTE12,
       ATTRIBUTE13,
       ATTRIBUTE14,
       ATTRIBUTE15,
       GLOBAL_ATTRIBUTE_CATEGORY,
       GLOBAL_ATTRIBUTE1,
       GLOBAL_ATTRIBUTE2,
       GLOBAL_ATTRIBUTE3,
       GLOBAL_ATTRIBUTE4,
       GLOBAL_ATTRIBUTE5,
       GLOBAL_ATTRIBUTE6,
       GLOBAL_ATTRIBUTE7,
       GLOBAL_ATTRIBUTE8,
       GLOBAL_ATTRIBUTE9,
       GLOBAL_ATTRIBUTE10,
       GLOBAL_ATTRIBUTE11,
       GLOBAL_ATTRIBUTE12,
       GLOBAL_ATTRIBUTE13,
       GLOBAL_ATTRIBUTE14,
       GLOBAL_ATTRIBUTE15,
       GLOBAL_ATTRIBUTE16,
       GLOBAL_ATTRIBUTE17,
       GLOBAL_ATTRIBUTE18,
       GLOBAL_ATTRIBUTE19,
       GLOBAL_ATTRIBUTE20,
       PO_RELEASE_ID,
       BALANCING_SEGMENT,
       COST_CENTER_SEGMENT,
       ACCOUNT_SEGMENT,
       PROJECT_ID,
       TASK_ID,
       EXPENDITURE_TYPE,
       EXPENDITURE_ITEM_DATE,
       EXPENDITURE_ORGANIZATION_ID,
       PROJECT_ACCOUNTING_CONTEXT,
       PA_ADDITION_FLAG,
       PA_QUANTITY,
       STAT_AMOUNT,
       TYPE_1099,
       INCOME_TAX_REGION,
       ASSETS_TRACKING_FLAG,
       PRICE_CORRECTION_FLAG,
       USSGL_TRANSACTION_CODE,
       RECEIPT_NUMBER,
       RECEIPT_LINE_NUMBER,
       MATCH_OPTION,
       RCV_TRANSACTION_ID,
       CREATION_DATE,
       CREATED_BY,
       LAST_UPDATE_DATE,
       LAST_UPDATED_BY,
       LAST_UPDATE_LOGIN,
       ORG_ID,
       AWARD_ID,
       PRICE_CORRECT_INV_NUM)
    VALUES
      (IR_INVOICE_LINE.INVOICE_ID,
       IR_INVOICE_LINE.INVOICE_LINE_ID,
       IR_INVOICE_LINE.LINE_NUMBER,
       IR_INVOICE_LINE.LINE_TYPE_LOOKUP_CODE,
       IR_INVOICE_LINE.LINE_GROUP_NUMBER,
       ROUND(IR_INVOICE_LINE.AMOUNT, 2),
       IR_INVOICE_LINE.ACCOUNTING_DATE,
       IR_INVOICE_LINE.DESCRIPTION,
       IR_INVOICE_LINE.AMOUNT_INCLUDES_TAX_FLAG,
       IR_INVOICE_LINE.PRORATE_ACROSS_FLAG,
       IR_INVOICE_LINE.TAX_CODE,
       IR_INVOICE_LINE.TAX_CODE_ID,
       IR_INVOICE_LINE.TAX_CODE_OVERRIDE_FLAG,
       IR_INVOICE_LINE.TAX_RECOVERY_RATE,
       IR_INVOICE_LINE.TAX_RECOVERY_OVERRIDE_FLAG,
       IR_INVOICE_LINE.TAX_RECOVERABLE_FLAG,
       IR_INVOICE_LINE.FINAL_MATCH_FLAG,
       IR_INVOICE_LINE.PO_HEADER_ID,
       IR_INVOICE_LINE.PO_LINE_ID,
       IR_INVOICE_LINE.PO_LINE_LOCATION_ID,
       IR_INVOICE_LINE.PO_DISTRIBUTION_ID,
       IR_INVOICE_LINE.PO_UNIT_OF_MEASURE,
       IR_INVOICE_LINE.INVENTORY_ITEM_ID,
       IR_INVOICE_LINE.QUANTITY_INVOICED,
       IR_INVOICE_LINE.UNIT_PRICE,
       IR_INVOICE_LINE.DISTRIBUTION_SET_ID,
       IR_INVOICE_LINE.DIST_CODE_CONCATENATED,
       NULL --ir_invoice_line.dist_code_combination_id
      ,
       IR_INVOICE_LINE.AWT_GROUP_ID,
       IR_INVOICE_LINE.ATTRIBUTE_CATEGORY,
       IR_INVOICE_LINE.ATTRIBUTE1,
       IR_INVOICE_LINE.ATTRIBUTE2,
       IR_INVOICE_LINE.ATTRIBUTE3,
       IR_INVOICE_LINE.ATTRIBUTE4,
       IR_INVOICE_LINE.ATTRIBUTE5,
       IR_INVOICE_LINE.ATTRIBUTE6,
       IR_INVOICE_LINE.ATTRIBUTE7,
       IR_INVOICE_LINE.ATTRIBUTE8,
       IR_INVOICE_LINE.ATTRIBUTE9,
       IR_INVOICE_LINE.ATTRIBUTE10,
       IR_INVOICE_LINE.ATTRIBUTE11,
       IR_INVOICE_LINE.ATTRIBUTE12,
       IR_INVOICE_LINE.ATTRIBUTE13,
       IR_INVOICE_LINE.ATTRIBUTE14,
       IR_INVOICE_LINE.ATTRIBUTE15,
       IR_INVOICE_LINE.GLOBAL_ATTRIBUTE_CATEGORY,
       IR_INVOICE_LINE.GLOBAL_ATTRIBUTE1,
       IR_INVOICE_LINE.GLOBAL_ATTRIBUTE2,
       IR_INVOICE_LINE.GLOBAL_ATTRIBUTE3,
       IR_INVOICE_LINE.GLOBAL_ATTRIBUTE4,
       IR_INVOICE_LINE.GLOBAL_ATTRIBUTE5,
       IR_INVOICE_LINE.GLOBAL_ATTRIBUTE6,
       IR_INVOICE_LINE.GLOBAL_ATTRIBUTE7,
       IR_INVOICE_LINE.GLOBAL_ATTRIBUTE8,
       IR_INVOICE_LINE.GLOBAL_ATTRIBUTE9,
       IR_INVOICE_LINE.GLOBAL_ATTRIBUTE10,
       IR_INVOICE_LINE.GLOBAL_ATTRIBUTE11,
       IR_INVOICE_LINE.GLOBAL_ATTRIBUTE12,
       IR_INVOICE_LINE.GLOBAL_ATTRIBUTE13,
       IR_INVOICE_LINE.GLOBAL_ATTRIBUTE14,
       IR_INVOICE_LINE.GLOBAL_ATTRIBUTE15,
       IR_INVOICE_LINE.GLOBAL_ATTRIBUTE16,
       IR_INVOICE_LINE.GLOBAL_ATTRIBUTE17,
       IR_INVOICE_LINE.GLOBAL_ATTRIBUTE18,
       IR_INVOICE_LINE.GLOBAL_ATTRIBUTE19,
       IR_INVOICE_LINE.GLOBAL_ATTRIBUTE20,
       IR_INVOICE_LINE.PO_RELEASE_ID,
       IR_INVOICE_LINE.BALANCING_SEGMENT,
       IR_INVOICE_LINE.COST_CENTER_SEGMENT,
       IR_INVOICE_LINE.ACCOUNT_SEGMENT,
       IR_INVOICE_LINE.PROJECT_ID,
       IR_INVOICE_LINE.TASK_ID,
       IR_INVOICE_LINE.EXPENDITURE_TYPE,
       IR_INVOICE_LINE.EXPENDITURE_ITEM_DATE,
       IR_INVOICE_LINE.EXPENDITURE_ORGANIZATION_ID,
       IR_INVOICE_LINE.PROJECT_ACCOUNTING_CONTEXT,
       IR_INVOICE_LINE.PA_ADDITION_FLAG,
       IR_INVOICE_LINE.PA_QUANTITY,
       IR_INVOICE_LINE.STAT_AMOUNT,
       IR_INVOICE_LINE.TYPE_1099,
       IR_INVOICE_LINE.INCOME_TAX_REGION,
       IR_INVOICE_LINE.ASSETS_TRACKING_FLAG,
       IR_INVOICE_LINE.PRICE_CORRECTION_FLAG,
       IR_INVOICE_LINE.USSGL_TRANSACTION_CODE,
       IR_INVOICE_LINE.RECEIPT_NUMBER,
       IR_INVOICE_LINE.RECEIPT_LINE_NUMBER,
       IR_INVOICE_LINE.MATCH_OPTION,
       IR_INVOICE_LINE.RCV_TRANSACTION_ID,
       IR_INVOICE_LINE.CREATION_DATE,
       IR_INVOICE_LINE.CREATED_BY,
       IR_INVOICE_LINE.LAST_UPDATE_DATE,
       IR_INVOICE_LINE.LAST_UPDATED_BY,
       IR_INVOICE_LINE.LAST_UPDATE_LOGIN,
       IR_INVOICE_LINE.ORG_ID,
       IR_INVOICE_LINE.AWARD_ID,
       IR_INVOICE_LINE.PRICE_CORRECT_INV_NUM);
  END INSERT_INVOICE_LINE;

  PROCEDURE UPDATE_STAGING_STATUS(I_INV_NUM        VARCHAR2,
                                  I_VENDOR_NUM     VARCHAR2,
                                  I_SOURCE         VARCHAR2,
                                  I_INTERFACE_TYPE VARCHAR2,
                                  I_LINE_NUM       NUMBER := NULL,
                                  I_STATUS         VARCHAR2,
                                  I_FILENAME       VARCHAR2,
                                  I_ORG            VARCHAR2) IS
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    UPDATE TAC_INVHUB_INVOICE_STG
       SET INTERFACE_AP_FLAG = I_STATUS
     WHERE 1 = 1
       AND SOURCE = I_SOURCE
       AND INTERFACE_TYPE = I_INTERFACE_TYPE
       AND INVOICE_NUM = I_INV_NUM
       AND VENDOR_NUM = I_VENDOR_NUM
       AND NVL(ERP_AP_LINENUMBER, LINE_NUMBER) =
           NVL(I_LINE_NUM, NVL(ERP_AP_LINENUMBER, LINE_NUMBER))
       AND FILE_NAME = I_FILENAME
       AND ORG_CODE = I_ORG
       AND INTERFACE_PO_FLAG = 'Y';
  
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      WRITE_LOG('Can not update interface status');
      ROLLBACK;
  END UPDATE_STAGING_STATUS;

  FUNCTION GET_MAPPING_ORG_ID_ACCCODE(P_ORG_ID NUMBER) RETURN VARCHAR2 IS
  BEGIN
    IF (P_ORG_ID = 142) THEN
      RETURN '08';
    ELSIF (P_ORG_ID = 218) THEN
      RETURN '12';
    ELSIF (P_ORG_ID = 238) THEN
      RETURN '12';
    ELSE
      RETURN '01';
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN '01';
  END GET_MAPPING_ORG_ID_ACCCODE;

  PROCEDURE VALIDATE_INVOICE_AMOUNT(P_FILENAME       IN VARCHAR2,
                                    GWRITELOG_DETAIL IN OUT TDTINVHUBLOG) IS
    CURSOR C1(P_FILENAME IN VARCHAR2) IS
      SELECT STG.FILE_NAME,
             STG.INVOICE_NUM,
             ROUND(STG.INVOICE_AMOUNT, 2) INVOICE_AMOUNT,
             ROUND(SUM(STG.LINE_AMOUNT), 2) ORI_LINE_AMT,
             ROUND(SUM(STG.LINE_AMOUNT +
                       ((STG.LINE_AMOUNT * PLTAX.TAX_RATE) / 100)),
                   2) SUM_LINE_AMT,
             PLTAX.TAX_RATE
        FROM TAC_INVHUB_INVOICE_STG STG,
             PO_HEADERS_ALL POH,
             PO_RELEASES_ALL PO_RELEASE,
             PO_LINES_ALL POL,
             (SELECT DISTINCT PLL.PO_HEADER_ID, PLL.PO_LINE_ID, ATC.TAX_RATE
                FROM PO_LINE_LOCATIONS_ALL PLL, AP_TAX_CODES_ALL ATC
               WHERE PLL.TAX_CODE_ID = ATC.TAX_ID) PLTAX
       WHERE STG.FILE_NAME = P_FILENAME
         AND STG.PO_NUMBER =
             POH.SEGMENT1 ||
             DECODE(PO_RELEASE.RELEASE_NUM,
                    NULL,
                    NULL,
                    '-' || PO_RELEASE.RELEASE_NUM) --poh.segment1
         AND STG.PO_LINE_NUMBER = POL.LINE_NUM
         AND POH.PO_HEADER_ID = POL.PO_HEADER_ID
         AND POH.ORG_ID = POL.ORG_ID
         AND POH.PO_HEADER_ID = PO_RELEASE.PO_HEADER_ID(+)
         AND POH.ORG_ID = PO_RELEASE.ORG_ID(+)
         AND POL.PO_HEADER_ID = PLTAX.PO_HEADER_ID
         AND POL.PO_LINE_ID = PLTAX.PO_LINE_ID
         AND STG.INTERFACE_PO_FLAG = 'Y'
         AND STG.INTERFACE_AP_FLAG = 'N'
       GROUP BY STG.FILE_NAME,
                STG.INVOICE_NUM,
                STG.INVOICE_AMOUNT,
                PLTAX.TAX_RATE;
  
    P_INVLOG    DTINVHUB_LOG_DETAIL%ROWTYPE;
    P_ERR_MSG   VARCHAR2(50);
    P_ERR_CODE  VARCHAR2(240);
    L_STARTTIME TIMESTAMP;
    L_ENDTIME   TIMESTAMP;
    L_INVAMT    NUMBER;
  
    CURSOR C2(P_FILENAME IN VARCHAR2) IS
      SELECT INVOICE_NUM,
             LINE_NUMBER,
             PO_NUMBER,
             PO_LINE_NUMBER,
             RECEIPT_NUMBER,
             RECEIPT_LINE_NUMBER,
             PR_NUMBER,
             PR_LINE_NUMBER,
             PAYMENT_METHOD_LOOKUP_CODE,
             TERMS_NAME,
             MATCH_OPTION,
             VENDOR_NUM,
             INTERFACE_AP_FLAG
        FROM TAC_INVHUB_INVOICE_STG STG
       WHERE STG.FILE_NAME = P_FILENAME
         AND STG.INTERFACE_PO_FLAG = 'Y'
         AND STG.INTERFACE_AP_FLAG IN ('M', 'P');
  BEGIN
    WRITE_LOG('validate_invoice_amount');
    WRITE_LOG('p_filename=' || P_FILENAME);
  
    FOR I_CHECK_INV IN C1(P_FILENAME) LOOP
      BEGIN
        SELECT CURRENT_TIMESTAMP INTO L_STARTTIME FROM DUAL;
      
        WRITE_LOG('invoice_Num = ' || I_CHECK_INV.INVOICE_NUM);
        WRITE_LOG('invoice_amount = ' || I_CHECK_INV.INVOICE_AMOUNT);
        WRITE_LOG('line_amount = ' || I_CHECK_INV.ORI_LINE_AMT);
      
        IF I_CHECK_INV.INVOICE_AMOUNT != I_CHECK_INV.ORI_LINE_AMT THEN
          BEGIN
            IF TRUNC(I_CHECK_INV.INVOICE_AMOUNT) !=
               TRUNC(I_CHECK_INV.SUM_LINE_AMT) THEN
              UPDATE TAC_INVHUB_INVOICE_STG
                 SET INTERFACE_AP_FLAG = 'P'
               WHERE FILE_NAME = I_CHECK_INV.FILE_NAME
                 AND INVOICE_NUM = I_CHECK_INV.INVOICE_NUM;
            END IF;
          END;
        ELSE
          BEGIN
            SELECT DISTINCT INVOICE_AMOUNT
              INTO L_INVAMT
              FROM TAC_INVHUB_INVOICE_STG
             WHERE FILE_NAME = I_CHECK_INV.FILE_NAME
               AND INVOICE_NUM = I_CHECK_INV.INVOICE_NUM;
          EXCEPTION
            WHEN TOO_MANY_ROWS THEN
              UPDATE TAC_INVHUB_INVOICE_STG
                 SET INTERFACE_AP_FLAG = 'M'
               WHERE FILE_NAME = I_CHECK_INV.FILE_NAME
                 AND INVOICE_NUM = I_CHECK_INV.INVOICE_NUM;
          END;
        END IF;
      END;
    END LOOP;
  
    WRITE_LOG('After update TAC_INVHUB_INVOICE_STG');
  
    FOR J IN C2(P_FILENAME) LOOP
      SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
    
      IF J.INTERFACE_AP_FLAG = 'P' THEN
        BEGIN
          P_INVLOG.LEVEL_TYPE        := 'D';
          P_INVLOG.REQUEST_ID        := FND_GLOBAL.CONC_REQUEST_ID;
          P_INVLOG.ERROR_TYPE        := 'AP Invoice Interface';
          P_INVLOG.AP_INVOICE        := J.INVOICE_NUM;
          P_INVLOG.AP_INVOIE_LINE    := J.LINE_NUMBER;
          P_INVLOG.PO_NUMBER         := J.PO_NUMBER;
          P_INVLOG.PO_LINE_NUMBER    := J.PO_LINE_NUMBER;
          P_INVLOG.RECEIPT_NUMBER    := J.RECEIPT_NUMBER;
          P_INVLOG.RECEIPT_LINE_NUM  := J.RECEIPT_LINE_NUMBER;
          P_INVLOG.PR_NUMBER         := J.PR_NUMBER;
          P_INVLOG.PR_LINE_NUMBER    := J.PR_LINE_NUMBER;
          P_INVLOG.MATCH_OPTION      := J.MATCH_OPTION;
          P_INVLOG.VENDOR_NUMBER     := J.VENDOR_NUM;
          P_INVLOG.VENDOR_SITE_NAME  := NULL;
          P_INVLOG.VENDOR_NAME       := NULL;
          P_INVLOG.DUE_DATE          := SYSDATE;
          P_INVLOG.PAYMENT_STATUS    := NULL;
          P_INVLOG.PV_NUMBER         := NULL;
          P_INVLOG.DOCUMENT_NUMBER   := NULL;
          P_INVLOG.AP_VOUCHER_NUMBER := NULL;
          P_INVLOG.PAYGROUP          := J.PAYMENT_METHOD_LOOKUP_CODE;
          P_INVLOG.PAYMENT_TERM      := J.TERMS_NAME;
          P_INVLOG.ERROR_CODE        := NULL;
          P_INVLOG.ERROR_MESSAGE     := NULL;
          P_INVLOG.PROCESSING_DATE   := L_STARTTIME; -- v_timestamp_start;
          P_INVLOG.PROCESSING_TIME   := SUBSTR(TO_CHAR(L_ENDTIME -
                                                       L_STARTTIME),
                                               12,
                                               12); --systimestamp - v_timestamp_start;
          P_ERR_MSG                  := 'Line amount is not equal to Header amount'; --'*** INTERFACE DATA ERROR : Duplicate Invoice in ERP';
          P_ERR_CODE                 := 'INVHUB003-005'; --'Error  INVHUB003-003 : Duplicate Invoice in ERP';
          WRITE_INVDETAIL_LOG(P_INVLOG,
                              GWRITELOG_DETAIL,
                              P_ERR_CODE,
                              P_ERR_MSG);
        END;
      ELSIF J.INTERFACE_AP_FLAG = 'M' THEN
        BEGIN
          P_INVLOG.LEVEL_TYPE        := 'D';
          P_INVLOG.REQUEST_ID        := FND_GLOBAL.CONC_REQUEST_ID;
          P_INVLOG.ERROR_TYPE        := 'AP Invoice Interface';
          P_INVLOG.AP_INVOICE        := J.INVOICE_NUM;
          P_INVLOG.AP_INVOIE_LINE    := J.LINE_NUMBER;
          P_INVLOG.PO_NUMBER         := J.PO_NUMBER;
          P_INVLOG.PO_LINE_NUMBER    := J.PO_LINE_NUMBER;
          P_INVLOG.RECEIPT_NUMBER    := J.RECEIPT_NUMBER;
          P_INVLOG.RECEIPT_LINE_NUM  := J.RECEIPT_LINE_NUMBER;
          P_INVLOG.PR_NUMBER         := J.PR_NUMBER;
          P_INVLOG.PR_LINE_NUMBER    := J.PR_LINE_NUMBER;
          P_INVLOG.MATCH_OPTION      := J.MATCH_OPTION;
          P_INVLOG.VENDOR_NUMBER     := J.VENDOR_NUM;
          P_INVLOG.VENDOR_SITE_NAME  := NULL;
          P_INVLOG.VENDOR_NAME       := NULL;
          P_INVLOG.DUE_DATE          := SYSDATE;
          P_INVLOG.PAYMENT_STATUS    := NULL;
          P_INVLOG.PV_NUMBER         := NULL;
          P_INVLOG.DOCUMENT_NUMBER   := NULL;
          P_INVLOG.AP_VOUCHER_NUMBER := NULL;
          P_INVLOG.PAYGROUP          := J.PAYMENT_METHOD_LOOKUP_CODE;
          P_INVLOG.PAYMENT_TERM      := J.TERMS_NAME;
          P_INVLOG.ERROR_CODE        := NULL;
          P_INVLOG.ERROR_MESSAGE     := NULL;
          P_INVLOG.PROCESSING_DATE   := L_STARTTIME; -- v_timestamp_start;
          P_INVLOG.PROCESSING_TIME   := SUBSTR(TO_CHAR(L_ENDTIME -
                                                       L_STARTTIME),
                                               12,
                                               12); --systimestamp - v_timestamp_start;
          P_ERR_MSG                  := 'Multiple invoice amount in the same invoice'; --'*** INTERFACE DATA ERROR : Duplicate Invoice in ERP';
          P_ERR_CODE                 := 'INVHUB003-026'; --'Error  INVHUB003-003 : Duplicate Invoice in ERP';
          WRITE_INVDETAIL_LOG(P_INVLOG,
                              GWRITELOG_DETAIL,
                              P_ERR_CODE,
                              P_ERR_MSG);
        END;
      END IF;
    END LOOP;
  
    UPDATE TAC_INVHUB_INVOICE_STG
       SET INTERFACE_AP_FLAG = 'E'
     WHERE FILE_NAME = P_FILENAME
       AND INTERFACE_AP_FLAG IN ('M', 'P');
  
    COMMIT;
    WRITE_LOG('end validate_invoice_amount');
  END;
 
  PROCEDURE get_grn_amount (p_po_number IN VARCHAR2,
                           p_grn_number IN VARCHAR2,
                           p_grn_line  IN NUMBER,
                           p_company   IN VARCHAR2,
                           o_grn_amount IN OUT NUMBER,
                           o_rcv_transaction_id IN OUT NUMBER,
                           p_po_distribution_id IN OUT NUMBER
                          ) IS
   -- l_grn_amount NUMBER;
   -- l_rcv_transaction_id NUMBER;
   -- l_po_distribution_id NUMBER;
  BEGIN
    
   
                    
      select   rt.transaction_id
               ,pd.po_distribution_id
               ,SUM((nvl(rsl.quantity_received,0)*nvl(pol.unit_price,0))) GRN_AMOUNT
        INTO  o_rcv_transaction_id
             ,p_po_distribution_id
             ,o_grn_amount
        
        from rcv_shipment_headers rsh
               ,rcv_shipment_lines rsl
               ,rcv_transactions rt
              ,(select msi.inventory_item_id,msi.segment1 ITEM_CODE
                  from mtl_system_items_b msi,mtl_parameters mp
                where msi.organization_id=mp.organization_id
                    and msi.organization_id=mp.master_organization_id) ITEM
               ,po_headers_all poh
               ,PO_RELEASES_ALL po_release
               ,po_lines_all pol
               ,po_line_locations_all pll
               ,po_distributions_all pd
               ,po_vendors pv
               ,po_vendor_sites_all pvs 
               ,ap_terms payterm    
               ,org_organization_definitions org 
               ,AP_REPORTING_ENTITIES_all are 
      where rsh.receipt_num=p_grn_number  --'20160001447'
          AND rsl.line_num= p_grn_line
          --and poh.segment1=p_po_number
          AND poh.segment1||DECODE(po_release.release_num,NULL,NULL,'-'||po_release.release_num)=p_po_number
          AND poh.po_header_id=po_release.po_header_id(+)
          AND poh.org_id=po_release.org_id(+)
         -- AND nvl(po_release.release_num,'0')=p_release_num
          AND are.tax_entity_id = DECODE(p_company, --l.REPORTING_ENTITY,
                                        'DTAC',
                                        10000,
                                        'DTN',
                                        10050,
                                        'PSB',
                                        10094)
          and rsh.shipment_header_id=rsl.shipment_header_id
          and rsl.shipment_header_id=rt.shipment_header_id
          and rsl.shipment_line_id=rt.shipment_line_id
          and rt.source_document_code='PO'
          and rt.transaction_type='RECEIVE'
          and receipt_source_code='VENDOR'
          and rsl.item_id=item.inventory_item_id(+)
          and rt.po_header_id=pll.po_header_id
          and rt.po_line_id=pll.po_line_id
          and rt.po_line_location_id=pd.line_location_id
          and rt.po_distribution_id=pd.po_distribution_id
          and poh.po_header_id=pol.po_header_id
         -- and nvl(rt.po_release_id,0)= nvl(po_release.po_release_id,0) /* fix bug grn match with po release by suttisak on 22 Jan 2018*/
          and pol.po_header_id=pll.po_header_id
          and pol.po_line_id=pll.po_line_id
          and pll.po_header_id=pd.po_header_id
          and pll.po_line_id=pd.po_line_id
          and pll.line_location_id=pd.line_location_id
          and poh.vendor_id=pv.vendor_id
          and poh.vendor_id=pvs.vendor_id
          and poh.vendor_site_id=pvs.vendor_site_id
          and poh.terms_id=payterm.term_id
          --and pvs.terms_id=payterm.term_id  /* change by suttisak */
                                              /* get payment term from po instead supplier site setup */
          and pll.ship_to_organization_id=org.organization_id
          AND org.OPERATING_UNIT = are.ORG_ID
          GROUP BY rt.transaction_id
               ,pd.po_distribution_id;
        EXCEPTION WHEN OTHERS THEN
          o_grn_amount :=0;
          o_rcv_transaction_id :=-1;
          p_po_distribution_id :=-1;
  END;
  
  PROCEDURE VALIDATE_GRN_AMOUNT(P_SOURCE IN varchar2,
                                P_INTERFACE_TYPE IN VARCHAR2,
                                P_FILENAME       IN VARCHAR2,
                                GWRITELOG_DETAIL IN OUT TDTINVHUBLOG) IS
  
      CURSOR c_get_grn IS
      SELECT DISTINCT org_code,po_number,receipt_number,receipt_line_number
        FROM TAC_INVHUB_INVOICE_STG
       WHERE 1 = 1 --org_code = p_company_code
         AND SOURCE = P_SOURCE
         AND INTERFACE_TYPE = P_INTERFACE_TYPE
         AND INTERFACE_PO_FLAG = 'Y'
         AND INTERFACE_AP_FLAG = 'N'
         AND FILE_NAME = P_FILENAME --i_file_name,file_name)
         AND FILE_NAME IS NOT NULL
       ORDER BY po_number,receipt_number,receipt_line_number;
       
       CURSOR c_get_inv(P_SOURCE IN varchar2,
                        P_INTERFACE_TYPE IN VARCHAR2,
                        P_FILENAME       IN VARCHAR2,
                        P_ORG_CODE       IN VARCHAR2,
                        P_PO_NUMBER      IN VARCHAR2,
                        P_RECEIPT_NUMBER IN VARCHAR2,
                        P_RECEIPT_LINE   IN NUMBER)                        
                        
           IS
       SELECT invoice_num,
               vendor_num,
               nvl(sum(nvl(line_amount,0)),0) INV_AMT
         FROM TAC_INVHUB_INVOICE_STG
        WHERE interface_type=P_INTERFACE_TYPE
          AND org_code = P_ORG_CODE
          AND SOURCE=P_SOURCE
          AND po_number =P_PO_NUMBER
          AND receipt_number=P_RECEIPT_NUMBER
          AND receipt_line_number= P_RECEIPT_LINE
          AND interface_po_flag='Y'
          AND interface_ap_flag='N'
          AND file_name=P_FILENAME
        GROUP BY invoice_num,
                 vendor_num;
      
     
     CURSOR C_error(P_FILENAME IN VARCHAR2) IS
      SELECT INVOICE_NUM,
             LINE_NUMBER,
             PO_NUMBER,
             PO_LINE_NUMBER,
             RECEIPT_NUMBER,
             RECEIPT_LINE_NUMBER,
             PR_NUMBER,
             PR_LINE_NUMBER,
             PAYMENT_METHOD_LOOKUP_CODE,
             TERMS_NAME,
             MATCH_OPTION,
             VENDOR_NUM,
             INTERFACE_AP_FLAG
        FROM TAC_INVHUB_INVOICE_STG STG
       WHERE STG.FILE_NAME = P_FILENAME
         AND STG.INTERFACE_PO_FLAG = 'Y'
         AND STG.INTERFACE_AP_FLAG ='P'; 
         
     l_grn_amount NUMBER;
     l_rcv_transaction_id NUMBER;
     l_po_dist_id   NUMBER;
     l_sum_inv     NUMBER;  
     l_remaining_grn NUMBER; 
     L_STARTTIME TIMESTAMP;
     L_ENDTIME   TIMESTAMP;
     P_ERR_MSG   VARCHAR2(50);
     P_ERR_CODE  VARCHAR2(240);
     P_INVLOG    DTINVHUB_LOG_DETAIL%ROWTYPE;
  BEGIN
     WRITE_LOG('P_SOURCE=' || P_SOURCE);
     WRITE_LOG('P_INTERFACE_TYPE=' || P_INTERFACE_TYPE);
     WRITE_LOG('P_FILENAME=' || P_FILENAME);
     
     
     FOR I IN c_get_grn LOOP
     BEGIN
         l_remaining_grn:=0;
         get_grn_amount (i.po_number,
                         i.receipt_number,
                         i.receipt_line_number,
                         i.org_code,
                         l_grn_amount,
                         l_rcv_transaction_id,
                         l_po_dist_id
                        );
         
         WRITE_LOG('i.po_number=' || i.po_number);    
         WRITE_LOG('i.receipt_number=' || i.receipt_number);                   
         WRITE_LOG('l_grn_amount=' || l_grn_amount);                           
         IF l_grn_amount = 0 THEN
            UPDATE TAC_INVHUB_INVOICE_STG
               SET INTERFACE_AP_FLAG ='P'
             WHERE SOURCE = P_SOURCE
               AND INTERFACE_TYPE = P_INTERFACE_TYPE
               AND INTERFACE_PO_FLAG = 'Y'
               AND INTERFACE_AP_FLAG = 'N'
               AND FILE_NAME = P_FILENAME
               AND org_code  = i.org_code
               AND po_number = i.po_number
               AND receipt_number =  i.receipt_number
               AND receipt_line_number = i.receipt_line_number;
         ELSE
         BEGIN 
            BEGIN   
                WRITE_LOG('l_rcv_transaction_id=' || l_rcv_transaction_id);   
                WRITE_LOG('l_po_dist_id=' || l_po_dist_id);          
                SELECT nvl(sum(nvl(aid.amount,0)),0)
                  INTO l_sum_inv
                  FROM ap_invoices_all AI
                       ,AP_INVOICE_DISTRIBUTIONS_ALL aid
                WHERE ai.invoice_id = aid.invoice_id
                  AND aid.rcv_transaction_id=l_rcv_transaction_id
                  AND aid.po_distribution_id=l_po_dist_id;
                EXCEPTION WHEN OTHERS THEN
                    l_sum_inv:=0;
             END;
             BEGIN
                WRITE_LOG('l_sum_inv=' || l_sum_inv);    
                IF (l_grn_amount - l_sum_inv) <= 0 THEN
                BEGIN
                   UPDATE TAC_INVHUB_INVOICE_STG
                      SET INTERFACE_AP_FLAG ='P'
                    WHERE SOURCE = P_SOURCE
                      AND INTERFACE_TYPE = P_INTERFACE_TYPE
                      AND INTERFACE_PO_FLAG = 'Y'
                      AND INTERFACE_AP_FLAG = 'N'
                      AND FILE_NAME = P_FILENAME
                      AND org_code  = i.org_code
                      AND po_number = i.po_number
                      AND receipt_number =  i.receipt_number
                      AND receipt_line_number = i.receipt_line_number;
                END;
                ELSE
                BEGIN
                   l_remaining_grn := nvl(l_grn_amount,0) - nvl(l_sum_inv,0);
                   WRITE_LOG('step1 l_remaining_grn=' || l_remaining_grn);  
                   FOR j IN c_get_inv(P_SOURCE,
                                      P_INTERFACE_TYPE,
                                      P_FILENAME,
                                      i.org_code,
                                      i.po_number,
                                      i.receipt_number,
                                      i.receipt_line_number) LOOP
                   BEGIN
                       WRITE_LOG('j.INV_AMT=' || j.INV_AMT);  
                      l_remaining_grn := nvl(l_remaining_grn,0) - nvl(j.INV_AMT,0); 
                      WRITE_LOG('step2 l_remaining_grn=' || l_remaining_grn);  
                      IF l_remaining_grn < 0 THEN
                      BEGIN
                         WRITE_LOG('P_SOURCE='||P_SOURCE);
                         WRITE_LOG('P_INTERFACE_TYPE='||P_INTERFACE_TYPE);
                         WRITE_LOG('P_FILENAME='||P_FILENAME);
                         WRITE_LOG('i.org_code='||i.org_code);
                         WRITE_LOG('i.po_number='||i.po_number);
                         WRITE_LOG('i.receipt_number='||i.receipt_number);
                         WRITE_LOG('i.receipt_line_number='||i.receipt_line_number);
                         WRITE_LOG('j.invoice_num='||j.invoice_num);
                         WRITE_LOG('j.vendor_num='||j.vendor_num);
                         
                         UPDATE TAC_INVHUB_INVOICE_STG
                            SET INTERFACE_AP_FLAG ='P'
                          WHERE SOURCE = P_SOURCE
                            AND INTERFACE_TYPE = P_INTERFACE_TYPE
                            AND FILE_NAME = P_FILENAME
                            AND org_code  = i.org_code
                            AND po_number = i.po_number
                            AND receipt_number =  i.receipt_number
                            AND receipt_line_number = i.receipt_line_number
                            AND invoice_num  = j.invoice_num
                            AND vendor_num   = j.vendor_num
                            AND INTERFACE_PO_FLAG = 'Y'
                            AND INTERFACE_AP_FLAG = 'N';
                          WRITE_LOG('update TAC_INVHUB_INVOICE_STG to P:'||SQL%ROWCOUNT);  
                      END;
                      END IF;
                   END;     
                   END LOOP;
                END;
                END IF; 
             END;
         END;
         END IF;      
                   
                                  
      
     END;
     END LOOP;
     
     
     FOR k IN C_error(P_FILENAME) LOOP
      SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
    
      IF k.INTERFACE_AP_FLAG = 'P' THEN
        BEGIN
          P_INVLOG.LEVEL_TYPE        := 'D';
          P_INVLOG.REQUEST_ID        := FND_GLOBAL.CONC_REQUEST_ID;
          P_INVLOG.ERROR_TYPE        := 'AP Invoice Interface';
          P_INVLOG.AP_INVOICE        := k.INVOICE_NUM;
          P_INVLOG.AP_INVOIE_LINE    := k.LINE_NUMBER;
          P_INVLOG.PO_NUMBER         := k.PO_NUMBER;
          P_INVLOG.PO_LINE_NUMBER    := k.PO_LINE_NUMBER;
          P_INVLOG.RECEIPT_NUMBER    := k.RECEIPT_NUMBER;
          P_INVLOG.RECEIPT_LINE_NUM  := k.RECEIPT_LINE_NUMBER;
          P_INVLOG.PR_NUMBER         := k.PR_NUMBER;
          P_INVLOG.PR_LINE_NUMBER    := k.PR_LINE_NUMBER;
          P_INVLOG.MATCH_OPTION      := k.MATCH_OPTION;
          P_INVLOG.VENDOR_NUMBER     := k.VENDOR_NUM;
          P_INVLOG.VENDOR_SITE_NAME  := NULL;
          P_INVLOG.VENDOR_NAME       := NULL;
          P_INVLOG.DUE_DATE          := SYSDATE;
          P_INVLOG.PAYMENT_STATUS    := NULL;
          P_INVLOG.PV_NUMBER         := NULL;
          P_INVLOG.DOCUMENT_NUMBER   := NULL;
          P_INVLOG.AP_VOUCHER_NUMBER := NULL;
          P_INVLOG.PAYGROUP          := k.PAYMENT_METHOD_LOOKUP_CODE;
          P_INVLOG.PAYMENT_TERM      := k.TERMS_NAME;
          P_INVLOG.ERROR_CODE        := NULL;
          P_INVLOG.ERROR_MESSAGE     := NULL;
          P_INVLOG.PROCESSING_DATE   := L_STARTTIME; -- v_timestamp_start;
          P_INVLOG.PROCESSING_TIME   := SUBSTR(TO_CHAR(L_ENDTIME -
                                                       L_STARTTIME),
                                               12,
                                               12); --systimestamp - v_timestamp_start;
          P_ERR_MSG                  := 'GRN Remaining is not enough'; --'*** INTERFACE DATA ERROR : Duplicate Invoice in ERP';
          P_ERR_CODE                 := 'INVHUB003-040'; --'Error  INVHUB003-003 : Duplicate Invoice in ERP';
          WRITE_INVDETAIL_LOG(P_INVLOG,
                              GWRITELOG_DETAIL,
                              P_ERR_CODE,
                              P_ERR_MSG);
        END;
      
      END IF;
    END LOOP;
  
    UPDATE TAC_INVHUB_INVOICE_STG
       SET INTERFACE_AP_FLAG = 'E'
     WHERE FILE_NAME = P_FILENAME
       AND INTERFACE_AP_FLAG = 'P';
    WRITE_LOG('update TAC_INVHUB_INVOICE_STG to E:'||SQL%ROWCOUNT);     
  
    COMMIT;
    WRITE_LOG('end validate_grn_amount');
     
     
  END;
  
  PROCEDURE GET_ERROR_FROM_INTERFACE(GWRITELOG_DETAIL     IN OUT TDTINVHUBLOG,
                                     V_GROUP              IN VARCHAR2,
                                     V_STARTTIME          IN TIMESTAMP,
                                     V_CURRENT_REQUEST_ID IN NUMBER,
                                     V_DTAC_REQ_ID        IN NUMBER,
                                     V_DTN_REQ_ID         IN NUMBER,
                                     V_PSB_REQ_ID         IN NUMBER) IS
    L_LOG_INTERFACE DTINVHUB_LOG_DETAIL%ROWTYPE;
    P_ERR_MSG       VARCHAR2(50);
    P_ERR_CODE      VARCHAR2(240);
    L_ENDTIME       TIMESTAMP;
  
    CURSOR C_ERROR(P_GROUP_ID    IN VARCHAR2,
                   P_DTAC_REQ_ID IN NUMBER,
                   P_DTN_REQ_ID  IN NUMBER,
                   P_PSB_REQ_ID  IN NUMBER) IS
      SELECT DISTINCT AI.INVOICE_NUM,
                      STG.LINE_NUMBER, --ail.line_number, change line num to original line num
                      STG.PO_NUMBER,
                      STG.PO_LINE_NUMBER,
                      STG.RECEIPT_NUMBER,
                      STG.RECEIPT_LINE_NUMBER,
                      STG.PR_NUMBER,
                      STG.PR_LINE_NUMBER,
                      STG.MATCH_OPTION,
                      STG.VENDOR_NUM,
                      AIR.REJECT_LOOKUP_CODE,
                      STG.INTERFACE_TYPE
        FROM TAC_INVHUB_INVOICE_STG     STG,
             AP_INVOICES_INTERFACE      AI,
             AP_INVOICE_LINES_INTERFACE AIL,
             AP_INTERFACE_REJECTIONS    AIR
       WHERE STG.INVOICE_NUM = AI.INVOICE_NUM
         AND NVL(STG.ERP_AP_LINENUMBER, STG.LINE_NUMBER) = AIL.LINE_NUMBER
         AND AI.INVOICE_ID = AIL.INVOICE_ID(+)
         AND AI.INVOICE_ID = AIR.PARENT_ID
         AND AIR.PARENT_TABLE = 'AP_INVOICES_INTERFACE'
         AND AI.STATUS = 'REJECTED'
         AND AI.SOURCE = 'FXTH'
         AND AI.GROUP_ID = P_GROUP_ID
         AND STG.GROUP_NAME = P_GROUP_ID
         AND REQUEST_ID IN (P_DTAC_REQ_ID, P_DTN_REQ_ID, P_PSB_REQ_ID)
      UNION
      SELECT DISTINCT AI.INVOICE_NUM,
                      STG.LINE_NUMBER, --ail.line_number, change line num to original line num
                      STG.PO_NUMBER,
                      STG.PO_LINE_NUMBER,
                      STG.RECEIPT_NUMBER,
                      STG.RECEIPT_LINE_NUMBER,
                      STG.PR_NUMBER,
                      STG.PR_LINE_NUMBER,
                      STG.MATCH_OPTION,
                      STG.VENDOR_NUM,
                      AIR.REJECT_LOOKUP_CODE,
                      STG.INTERFACE_TYPE
        FROM TAC_INVHUB_INVOICE_STG     STG,
             AP_INVOICES_INTERFACE      AI,
             AP_INVOICE_LINES_INTERFACE AIL,
             AP_INTERFACE_REJECTIONS    AIR
       WHERE STG.INVOICE_NUM = AI.INVOICE_NUM
         AND NVL(STG.ERP_AP_LINENUMBER, STG.LINE_NUMBER) = AIL.LINE_NUMBER
         AND AI.INVOICE_ID = AIL.INVOICE_ID(+)
         AND AIL.INVOICE_LINE_ID = AIR.PARENT_ID
         AND AIR.PARENT_TABLE = 'AP_INVOICE_LINES_INTERFACE'
         AND AI.SOURCE = 'FXTH'
         AND AI.STATUS = 'REJECTED'
         AND AI.GROUP_ID = P_GROUP_ID
         AND STG.GROUP_NAME = P_GROUP_ID
         AND REQUEST_ID IN (P_DTAC_REQ_ID, P_DTN_REQ_ID, P_PSB_REQ_ID);
  
    CURSOR C_COMPLETE(P_GROUP_ID    IN VARCHAR2,
                      P_DTAC_REQ_ID IN NUMBER,
                      P_DTN_REQ_ID  IN NUMBER,
                      P_PSB_REQ_ID  IN NUMBER) IS
      SELECT DISTINCT AI.INVOICE_NUM,
                      STG.LINE_NUMBER, --ail.line_number, change line num to original line num
                      STG.PO_NUMBER,
                      STG.PO_LINE_NUMBER,
                      STG.RECEIPT_NUMBER,
                      STG.RECEIPT_LINE_NUMBER,
                      STG.PR_NUMBER,
                      STG.PR_LINE_NUMBER,
                      STG.MATCH_OPTION,
                      STG.VENDOR_NUM,
                      STG.INTERFACE_TYPE
        FROM TAC_INVHUB_INVOICE_STG     STG,
             AP_INVOICES_INTERFACE      AI,
             AP_INVOICE_LINES_INTERFACE AIL
       WHERE STG.INVOICE_NUM = AI.INVOICE_NUM
         AND NVL(STG.ERP_AP_LINENUMBER, STG.LINE_NUMBER) = AIL.LINE_NUMBER
         AND AI.INVOICE_ID = AIL.INVOICE_ID(+)
         AND AI.SOURCE = 'FXTH'
         AND AI.STATUS = 'PROCESSED'
         AND AI.GROUP_ID = P_GROUP_ID
         AND STG.GROUP_NAME = P_GROUP_ID
         AND REQUEST_ID IN (P_DTAC_REQ_ID, P_DTN_REQ_ID, P_PSB_REQ_ID)
       ORDER BY AI.INVOICE_NUM, STG.LINE_NUMBER; --ail.line_number;
  BEGIN
    WRITE_LOG('in get_error_from_interface');
  
    FOR I IN C_ERROR(V_GROUP,
                     NVL(V_DTAC_REQ_ID, 0),
                     NVL(V_DTN_REQ_ID, 0),
                     NVL(V_PSB_REQ_ID, 0)) LOOP
      BEGIN
        WRITE_LOG('in get_error_from_interface with status error');
        L_LOG_INTERFACE.LEVEL_TYPE     := 'D';
        L_LOG_INTERFACE.REQUEST_ID     := V_CURRENT_REQUEST_ID;
        L_LOG_INTERFACE.ERROR_TYPE     := 'AP Invoice Interface';
        L_LOG_INTERFACE.AP_INVOICE     := I.INVOICE_NUM;
        L_LOG_INTERFACE.AP_INVOIE_LINE := I.LINE_NUMBER;
      
        --IF substr(v_group,
        IF SUBSTR(V_GROUP, 6, 2) = '3W' THEN
          L_LOG_INTERFACE.PO_NUMBER        := I.PO_NUMBER;
          L_LOG_INTERFACE.PO_LINE_NUMBER   := I.PO_LINE_NUMBER;
          L_LOG_INTERFACE.RECEIPT_NUMBER   := I.RECEIPT_NUMBER;
          L_LOG_INTERFACE.RECEIPT_LINE_NUM := I.RECEIPT_LINE_NUMBER;
        ELSIF SUBSTR(V_GROUP, 6, 2) = '2W' THEN
          L_LOG_INTERFACE.PR_NUMBER      := I.PR_NUMBER;
          L_LOG_INTERFACE.PR_LINE_NUMBER := I.PR_LINE_NUMBER;
        END IF;
      
        L_LOG_INTERFACE.MATCH_OPTION      := I.MATCH_OPTION;
        L_LOG_INTERFACE.VENDOR_NUMBER     := I.VENDOR_NUM;
        L_LOG_INTERFACE.VENDOR_SITE_NAME  := NULL;
        L_LOG_INTERFACE.VENDOR_NAME       := NULL;
        L_LOG_INTERFACE.DUE_DATE          := SYSDATE;
        L_LOG_INTERFACE.PAYMENT_STATUS    := NULL;
        L_LOG_INTERFACE.PV_NUMBER         := NULL;
        L_LOG_INTERFACE.DOCUMENT_NUMBER   := NULL;
        L_LOG_INTERFACE.AP_VOUCHER_NUMBER := NULL;
        L_LOG_INTERFACE.PAYGROUP          := NULL;
        L_LOG_INTERFACE.PAYMENT_TERM      := NULL;
        L_LOG_INTERFACE.ERROR_CODE        := NULL;
        L_LOG_INTERFACE.ERROR_MESSAGE     := NULL;
        L_LOG_INTERFACE.PROCESSING_DATE   := V_STARTTIME;
      
        SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
      
        L_LOG_INTERFACE.PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                          V_STARTTIME),
                                                  12,
                                                  12);
        P_ERR_MSG                       := I.REJECT_LOOKUP_CODE; --'*** INTERFACE DATA ERROR : Duplicate Invoice in ERP';
        P_ERR_CODE                      := 'INVHUB003-040';
        WRITE_LOG('in p_err_code:' || P_ERR_CODE || '-' || P_ERR_MSG);
        WRITE_INVDETAIL_LOG(P_INVLOG         => L_LOG_INTERFACE,
                            GWRITELOG_DETAIL => GWRITELOG_DETAIL,
                            PERR_CODE        => P_ERR_CODE,
                            PMESSAGE_ERR     => P_ERR_MSG);
      
        UPDATE TAC_INVHUB_INVOICE_STG
           SET INTERFACE_AP_FLAG = 'E'
         WHERE 1 = 1
           AND SOURCE = 'FXTH'
           AND INTERFACE_TYPE = I.INTERFACE_TYPE
           AND INVOICE_NUM = I.INVOICE_NUM
           AND VENDOR_NUM = I.VENDOR_NUM
              --and line_number       = nvl(i_line_num, line_number)
           AND GROUP_NAME = V_GROUP
           AND INTERFACE_PO_FLAG = 'Y';
      END;
    END LOOP;
  
    FOR J IN C_COMPLETE(V_GROUP,
                        NVL(V_DTAC_REQ_ID, 0),
                        NVL(V_DTN_REQ_ID, 0),
                        NVL(V_PSB_REQ_ID, 0)) LOOP
      BEGIN
        WRITE_LOG('in get_error_from_interface with status completed');
        L_LOG_INTERFACE.LEVEL_TYPE     := 'D';
        L_LOG_INTERFACE.REQUEST_ID     := V_CURRENT_REQUEST_ID;
        L_LOG_INTERFACE.ERROR_TYPE     := 'AP Invoice Interface';
        L_LOG_INTERFACE.AP_INVOICE     := J.INVOICE_NUM;
        L_LOG_INTERFACE.AP_INVOIE_LINE := J.LINE_NUMBER;
      
        IF SUBSTR(V_GROUP, 6, 2) = '3W' THEN
          L_LOG_INTERFACE.PO_NUMBER        := J.PO_NUMBER;
          L_LOG_INTERFACE.PO_LINE_NUMBER   := J.PO_LINE_NUMBER;
          L_LOG_INTERFACE.RECEIPT_NUMBER   := J.RECEIPT_NUMBER;
          L_LOG_INTERFACE.RECEIPT_LINE_NUM := J.RECEIPT_LINE_NUMBER;
        ELSIF SUBSTR(V_GROUP, 6, 2) = '2W' THEN
          L_LOG_INTERFACE.PR_NUMBER      := J.PR_NUMBER;
          L_LOG_INTERFACE.PR_LINE_NUMBER := J.PR_LINE_NUMBER;
        END IF;
      
        L_LOG_INTERFACE.MATCH_OPTION      := J.MATCH_OPTION;
        L_LOG_INTERFACE.VENDOR_NUMBER     := J.VENDOR_NUM;
        L_LOG_INTERFACE.VENDOR_SITE_NAME  := NULL;
        L_LOG_INTERFACE.VENDOR_NAME       := NULL;
        L_LOG_INTERFACE.DUE_DATE          := SYSDATE;
        L_LOG_INTERFACE.PAYMENT_STATUS    := NULL;
        L_LOG_INTERFACE.PV_NUMBER         := NULL;
        L_LOG_INTERFACE.DOCUMENT_NUMBER   := NULL;
        L_LOG_INTERFACE.AP_VOUCHER_NUMBER := NULL;
        L_LOG_INTERFACE.PAYGROUP          := NULL;
        L_LOG_INTERFACE.PAYMENT_TERM      := NULL;
        L_LOG_INTERFACE.ERROR_CODE        := NULL;
        L_LOG_INTERFACE.ERROR_MESSAGE     := NULL;
        L_LOG_INTERFACE.PROCESSING_DATE   := V_STARTTIME;
      
        SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
      
        L_LOG_INTERFACE.PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                          V_STARTTIME),
                                                  12,
                                                  12);
      
        WRITE_INVDETAIL_LOG(P_INVLOG         => L_LOG_INTERFACE,
                            GWRITELOG_DETAIL => GWRITELOG_DETAIL,
                            PERR_CODE        => NULL,
                            PMESSAGE_ERR     => NULL);
      END;
    END LOOP;
  END;

  PROCEDURE INTERFACE_AP_INVOICEHUB(ERR_MSG          OUT VARCHAR2,
                                    ERR_CODE         OUT VARCHAR2,
                                    I_COMPANY_CODE   IN VARCHAR2,
                                    I_SOURCE         IN VARCHAR2 --FXTH
                                   ,
                                    I_INTERFACE_TYPE IN VARCHAR2 --2way or 3way
                                   ,
                                    I_FILE_NAME      IN VARCHAR2,
                                    I_RERUN          IN VARCHAR2,
                                    I_PATH           IN VARCHAR2) IS
    CURSOR C_READFILE(P_COMPANY_CODE   IN VARCHAR2,
                      P_SOURCE         IN VARCHAR2,
                      P_INTERFACE_TYPE IN VARCHAR2,
                      P_FILENAME       IN VARCHAR2) IS
      SELECT DISTINCT FILE_NAME
        FROM TAC_INVHUB_INVOICE_STG
       WHERE 1 = 1 --org_code = p_company_code
         AND SOURCE = P_SOURCE
         AND INTERFACE_TYPE = P_INTERFACE_TYPE
         AND INTERFACE_PO_FLAG = 'Y'
         AND INTERFACE_AP_FLAG = 'N'
         AND FILE_NAME = NVL(P_FILENAME, FILE_NAME) --i_file_name,file_name)
         AND FILE_NAME IS NOT NULL
       ORDER BY FILE_NAME;
  
    --v_mapped_org_id_acc_code varchar2(10);
    CURSOR C_INV(P_SOURCE         IN VARCHAR2,
                 P_INTERFACE_TYPE IN VARCHAR2,
                 P_FILENAME       IN VARCHAR2) IS
      SELECT INV.ORG_CODE,
             INV.GROUP_NAME,
             INV.SOURCE,
             INV.INVOICE_TYPE_LOOKUP_CODE,
             INV.VENDOR_NUM,
             INV.VENDOR_SITE_CODE,
             INV.INVOICE_NUM,
             INV.INVOICE_DATE,
             INV.DOC_CATEGORY_CODE,
             INV.INVOICE_CURRENCY_CODE,
             INV.INVOICE_AMOUNT INVOICE_AMOUNT, --sum(inv.INVOICE_AMOUNT)  invoice_amount,
             INV.GL_DATE,
             INV.INVOICE_DESCRIPTION,
             INV.TERMS_NAME,
             INV.PAYMENT_METHOD_LOOKUP_CODE,
             INV.AWT_GROUP_NAME,
             INV.MAIL_TO_NAME,
             -- inv.PR_NUMBER,
             --inv.PO_NUMBER,
             -- inv.RECEIPT_NUMBER,
             COUNT('x') NUM_OF_DIST
        FROM TAC_INVHUB_INVOICE_STG INV
       WHERE INV.SOURCE = P_SOURCE
         AND INV.INTERFACE_TYPE = P_INTERFACE_TYPE
         AND INV.INTERFACE_PO_FLAG = 'Y'
         AND NVL(INV.INTERFACE_AP_FLAG, 'N') = 'N'
         AND FILE_NAME = P_FILENAME
       GROUP BY INV.ORG_CODE,
                INV.GROUP_NAME,
                INV.SOURCE,
                INV.INVOICE_TYPE_LOOKUP_CODE,
                INV.VENDOR_NUM,
                INV.VENDOR_SITE_CODE,
                INV.INVOICE_NUM,
                INV.INVOICE_DATE,
                INV.DOC_CATEGORY_CODE,
                INV.INVOICE_CURRENCY_CODE,
                INV.INVOICE_AMOUNT,
                INV.GL_DATE,
                INV.INVOICE_DESCRIPTION,
                INV.TERMS_NAME,
                INV.PAYMENT_METHOD_LOOKUP_CODE,
                INV.AWT_GROUP_NAME,
                INV.MAIL_TO_NAME --,
      --  inv.PR_NUMBER
      -- inv.PO_NUMBER,
      -- inv.RECEIPT_NUMBER
      ;
  
    CURSOR C_LINE(P_SOURCE         IN VARCHAR2,
                  P_INTERFACE_TYPE IN VARCHAR2,
                  P_FILENAME       IN VARCHAR2,
                  P_INV_NUM        IN VARCHAR2,
                  P_VENDOR_NUM     IN VARCHAR2,
                  P_ORG            IN VARCHAR2) IS
      SELECT L.ORG_CODE,
             L.GROUP_NAME,
             L.SOURCE,
             L.INVOICE_TYPE_LOOKUP_CODE,
             L.VENDOR_NUM,
             L.VENDOR_SITE_CODE,
             L.INVOICE_NUM,
             L.INVOICE_DATE,
             L.DOC_CATEGORY_CODE,
             L.INVOICE_CURRENCY_CODE,
             L.INVOICE_AMOUNT,
             L.GL_DATE,
             L.INVOICE_DESCRIPTION,
             L.TERMS_NAME,
             L.PAYMENT_METHOD_LOOKUP_CODE,
             L.LINE_NUMBER,
             L.LINE_TYPE_LOOKUP_CODE,
             L.LINE_AMOUNT,
             L.LINE_TAX_CODE,
             L.AWT_GROUP_NAME,
             L.DIST_CODE_CONCATINATED,
             L.LINE_DESCRIPTION,
             L.ACT_VENDOR_NUM,
             L.ACT_VENDOR_SITE_CODE,
             DECODE(L.ORG_CODE, --l.REPORTING_ENTITY,
                    'DTAC',
                    10000,
                    'DTN',
                    10050,
                    'PSB',
                    10094) REPORTING_ENTITY,
             L.TAX_INVOICE_DATE,
             L.TAX_INVOICE_NUM,
             L.TAX_ACCT_PERIOD,
             L.WHT_ACCT_PERIOD,
             L.PHOR_NGOR_DOR,
             L.WHT_CONDITION,
             L.WHT_REVENUE_TYPE,
             L.WHT_REVENUE_NAME,
             L.PROJECT_NUMBER,
             L.PROJECT_TASK_NUMBER,
             L.EXPENDITURE_TYPE,
             L.EXPENDITURE_ORGANIZATION,
             L.EAI_CRTD_DTTM,
             L.PAYEE,
             L.MAIL_TO_NAME,
             L.MAIL_TO_ADDRESS1,
             L.MAIL_TO_ADDRESS2,
             L.MAIL_TO_ADDRESS3,
             L.MAIL_TO_ADDRESS4,
             L.PO_NUMBER,
             L.PO_LINE_NUMBER,
             L.RECEIPT_NUMBER,
             L.RECEIPT_LINE_NUMBER,
             L.PR_NUMBER,
             L.PR_LINE_NUMBER,
             L.MATCH_OPTION,
             L.BILL_PLACEMENT_DATE,
             L.INTERFACE_PO_FLAG,
             L.INTERFACE_AP_FLAG,
             L.FILE_NAME,
             L.INTERFACE_TYPE,
             HOU.ORGANIZATION_ID,
             L.ERP_AP_LINENUMBER
        FROM TAC_INVHUB_INVOICE_STG L, HR_ORGANIZATION_UNITS HOU
       WHERE L.INVOICE_NUM = P_INV_NUM
         AND L.SOURCE = P_SOURCE
         AND L.VENDOR_NUM = P_VENDOR_NUM
         AND L.INTERFACE_TYPE = P_INTERFACE_TYPE
         AND L.INTERFACE_PO_FLAG = 'Y'
         AND NVL(L.INTERFACE_AP_FLAG, 'N') = 'N'
         AND L.EXPENDITURE_ORGANIZATION = HOU.NAME(+)
         AND L.FILE_NAME = P_FILENAME
         AND L.ORG_CODE = P_ORG
       ORDER BY L.LINE_NUMBER;
  
    R_INVOICE      AP_INVOICES_INTERFACE%ROWTYPE;
    R_INVOICE_LINE AP_INVOICE_LINES_INTERFACE%ROWTYPE;
    V_GROUP        VARCHAR2(80);
    V_SOURCE       VARCHAR2(80);
    I_PROG_NAME    VARCHAR2(30) := 'DTINVHUB003';
  
    V_RATE                 NUMBER;
    V_TAX_CCID             NUMBER;
    V_LINE_NUM             NUMBER;
    V_SITE_AUTO_CALC_FLAG  VARCHAR2(10);
    V_SUPP_NAME            VARCHAR2(100);
    V_PERIOD               VARCHAR2(30);
    V_PROJ_ID              NUMBER(20);
    V_TASK_ID              NUMBER(20);
    V_EXPEN_TYPE           VARCHAR(100);
    V_EXPEN_ORG            NUMBER(20);
    V_FLAG                 BOOLEAN := FALSE;
    V_CHK_LINE             BOOLEAN := FALSE;
    V_VALIDATE_ERROR       BOOLEAN := FALSE;
    AP_REQ_ID              NUMBER;
    C_AP_REQ_ID            NUMBER;
    V_I_GROUP              VARCHAR2(80);
    V_AP_REQ_ID            NUMBER;
    V_LINE_COUNT           NUMBER := 0;
    V_LINE_COUNT_ERR       NUMBER := 0;
    V_SITE                 PO_VENDOR_SITES_ALL%ROWTYPE;
    V_SAVE_PNT             VARCHAR2(100);
    V_TAX_RATE             NUMBER;
    V_REQUESTER_ID         NUMBER;
    V_ECPENDITURE_ORG      NUMBER;
    V_RECEIPT_NUM          RCV_SHIPMENT_HEADERS.RECEIPT_NUM%TYPE;
    V_COUNT                NUMBER := 0;
    V_TOTAL_LINE_TAX_AMT   NUMBER := 0;
    V_LINE_STATUS_INFO     VARCHAR2(32000);
    V_LINE_ERROR           VARCHAR2(32000);
    V_HEADER_ERROR         VARCHAR2(32000);
    V_COUNT_INVALID_HEADER NUMBER := 0;
    V_INV_LINE_ERROR_LIST  VARCHAR2(10000);
    V_DIST_ACC_CODE        NUMBER;
    V_DIST_ACC_CONCATE     GL_CODE_COMBINATIONS_KFV.CONCATENATED_SEGMENTS%TYPE;
    V_CHEK_INV             VARCHAR2(1);
    V_CHECK_PERIOD         VARCHAR2(1);
    V_CHECK_CURRENCY       VARCHAR2(1);
    V_CHECK_SOURCE         VARCHAR2(1);
    V_CHECK_PO_NUMBER      VARCHAR2(1);
    V_CHECK_GRN            VARCHAR2(1);
    V_CONC_REQUEST         NUMBER := FND_GLOBAL.CONC_REQUEST_ID;
    V_TIMESTAMP_START      DATE := SYSTIMESTAMP;
    V_TIMESTAMP_END        DATE;
    -- tDtinvlog               tDtinvhublog  := tDtinvhublog();
    TSUMINVLOG                    TSUMINVHUBLOG := TSUMINVHUBLOG();
    GWRITELOG_DETAIL              TDTINVHUBLOG := TDTINVHUBLOG();
    V_FIRSTLINE_FLAG              VARCHAR2(1) := 'N';
    R_INVLOG                      DTINVHUB_LOG_DETAIL%ROWTYPE;
    L_PROJECT_ID                  PO_REQ_DISTRIBUTIONS_ALL.PROJECT_ID%TYPE;
    L_PROJECT_NAME                PA_PROJECTS_ALL.SEGMENT1%TYPE;
    L_TASK_ID                     PO_REQ_DISTRIBUTIONS_ALL.TASK_ID%TYPE;
    L_TASK_NAME                   PA_TASKS.TASK_NUMBER%TYPE;
    L_EXPENDITURE_TYPE            PO_REQ_DISTRIBUTIONS_ALL.EXPENDITURE_TYPE%TYPE;
    L_EXPENDITURE_ITEM_DATE       PO_REQ_DISTRIBUTIONS_ALL.EXPENDITURE_ITEM_DATE%TYPE;
    L_EXPENDITURE_ORGANIZATION_ID PO_REQ_DISTRIBUTIONS_ALL.EXPENDITURE_ORGANIZATION_ID%TYPE;
    L_COUNT_DTAC                  NUMBER;
    L_COUNT_DTN                   NUMBER;
    L_COUNT_PSB                   NUMBER;
    L_RESP_ID                     NUMBER;
    L_RESP_APP_ID                 NUMBER;
    P_CURRENT_REQUEST_ID          NUMBER;
    L_CHECK_DECIMAL               NUMBER;
    --l_start_process TIMESTAMP;
    L_STARTTIME    TIMESTAMP;
    L_ENDTIME      TIMESTAMP;
    L_MATCH_OPTION VARCHAR2(1);
    L_PO_HEADER    VARCHAR2(40);
    L_PO_RELEASE   NUMBER;
    L_DTAC_REQ_ID  NUMBER;
    L_DTN_REQ_ID   NUMBER;
    L_PSB_REQ_ID   NUMBER;
    L_FILE_NAME    VARCHAR2(100);
  BEGIN
    --Initial
    G_STEP      := 'start program';
    L_STARTTIME := SYSDATE;
    SAVEPOINT B4_PROCESS;
    WRITE_LOG(' ');
    WRITE_LOG('+----------------------------------------------------------------------------+');
    WRITE_LOG('Start AP Invoice Interface from INVOICE HUB FXTH');
    WRITE_LOG('Start Date: ' || TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
    WRITE_LOG(' ');
    WRITE_LOG('*****Parameter*****');
    WRITE_LOG('--------------------');
    WRITE_LOG('Company Code: ' || I_COMPANY_CODE);
    WRITE_LOG('Source: ' || I_SOURCE);
    WRITE_LOG('Interface Type: ' || I_INTERFACE_TYPE);
    WRITE_LOG('File Name: ' || I_FILE_NAME);
    WRITE_LOG('Rerun: ' || I_RERUN);
    WRITE_LOG('Path: ' || I_PATH);
    WRITE_LOG('+----------------------------------------------------------------------------+');
    P_CURRENT_REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
  
    FOR R_READFILE IN C_READFILE(I_COMPANY_CODE,
                                 I_SOURCE,
                                 I_INTERFACE_TYPE,
                                 I_FILE_NAME) LOOP
      BEGIN
        G_STEP := 'read file';
        WRITE_LOG('i_company_code=' || I_COMPANY_CODE);
        WRITE_LOG('i_source=' || I_SOURCE);
        WRITE_LOG('i_interface_type=' || I_INTERFACE_TYPE);
        WRITE_LOG('i_file_name=' || I_FILE_NAME);
      
        L_FILE_NAME := R_READFILE.FILE_NAME;
        -- validate invoice amount and invoice line amount
        G_STEP := 'VALIDATE_INVOICE_AMOUNT';
        VALIDATE_INVOICE_AMOUNT(R_READFILE.FILE_NAME, GWRITELOG_DETAIL);
        
        G_STEP := 'VALIDATE_GRN_AMOUNT';
        

        VALIDATE_GRN_AMOUNT(I_SOURCE,
                            I_INTERFACE_TYPE,
                            R_READFILE.FILE_NAME, 
                            GWRITELOG_DETAIL);
        
        G_STEP := 'UPDATE_AP_LINE';
        UPDATE_AP_LINE(R_READFILE.FILE_NAME, GWRITELOG_DETAIL);
      
        FOR R_INV IN C_INV(I_SOURCE, I_INTERFACE_TYPE, R_READFILE.FILE_NAME) LOOP
          G_STEP := 'fetch invoice';
          WRITE_LOG(G_STEP);
          ERR_CODE := NULL;
          ERR_MSG  := NULL;
        
          V_SAVE_PNT := R_INV.INVOICE_NUM || R_INV.VENDOR_NUM;
          SAVEPOINT V_SAVE_PNT;
        
          R_INVOICE := NULL;
          V_FLAG    := FALSE;
        
          WRITE_LOG('Invoice Number : ' || R_INV.INVOICE_NUM);
          WRITE_LOG('Vendor num :' || R_INV.VENDOR_NUM);
          V_GROUP    := R_INV.GROUP_NAME;
          V_SOURCE   := R_INV.SOURCE;
          V_LINE_NUM := 0;
          ---r_invoice.invoice_amount := 0;
          V_CHK_LINE       := FALSE;
          G_STEP           := 'Fetch line';
          V_FIRSTLINE_FLAG := 'Y';
          WRITE_LOG('r_readfile.file_name : ' || R_READFILE.FILE_NAME);
          WRITE_LOG('r_invoice.invoice_num : ' || R_INV.INVOICE_NUM);
          WRITE_LOG('i_source : ' || I_SOURCE);
          WRITE_LOG('i_interface_type : ' || I_INTERFACE_TYPE);
          WRITE_LOG('r_inv.vendor_num : ' || R_INV.VENDOR_NUM);
        
          FOR R_LINE IN C_LINE(I_SOURCE,
                               I_INTERFACE_TYPE,
                               R_READFILE.FILE_NAME,
                               R_INV.INVOICE_NUM,
                               R_INV.VENDOR_NUM,
                               R_INV.ORG_CODE) LOOP
            WRITE_LOG('v_firstline_flag=' || V_FIRSTLINE_FLAG);
          
            SELECT CURRENT_TIMESTAMP INTO L_STARTTIME FROM DUAL;
          
            R_INVLOG.LEVEL_TYPE        := 'D';
            R_INVLOG.REQUEST_ID        := V_CONC_REQUEST;
            R_INVLOG.ERROR_TYPE        := 'AP Invoice Interface';
            R_INVLOG.AP_INVOICE        := R_INV.INVOICE_NUM;
            R_INVLOG.AP_INVOIE_LINE    := R_LINE.LINE_NUMBER;
            R_INVLOG.PO_NUMBER         := R_LINE.PO_NUMBER;
            R_INVLOG.PO_LINE_NUMBER    := R_LINE.PO_LINE_NUMBER;
            R_INVLOG.RECEIPT_NUMBER    := R_LINE.RECEIPT_NUMBER;
            R_INVLOG.RECEIPT_LINE_NUM  := R_LINE.RECEIPT_LINE_NUMBER;
            R_INVLOG.PR_NUMBER         := R_LINE.PR_NUMBER;
            R_INVLOG.PR_LINE_NUMBER    := R_LINE.PR_LINE_NUMBER;
            R_INVLOG.MATCH_OPTION      := R_LINE.MATCH_OPTION;
            R_INVLOG.VENDOR_NUMBER     := R_INV.VENDOR_NUM;
            R_INVLOG.VENDOR_SITE_NAME  := NULL;
            R_INVLOG.VENDOR_NAME       := NULL;
            R_INVLOG.DUE_DATE          := SYSDATE;
            R_INVLOG.PAYMENT_STATUS    := NULL;
            R_INVLOG.PV_NUMBER         := NULL;
            R_INVLOG.DOCUMENT_NUMBER   := NULL;
            R_INVLOG.AP_VOUCHER_NUMBER := NULL;
            R_INVLOG.PAYGROUP          := R_INV.PAYMENT_METHOD_LOOKUP_CODE;
            R_INVLOG.PAYMENT_TERM      := R_INV.TERMS_NAME;
            R_INVLOG.ERROR_CODE        := NULL;
            R_INVLOG.ERROR_MESSAGE     := NULL;
            R_INVLOG.PROCESSING_DATE   := L_STARTTIME;
            R_INVLOG.PROCESSING_TIME   := NULL; --systimestamp - v_timestamp_start;
          
            IF V_FIRSTLINE_FLAG = 'Y' THEN
              BEGIN
                SELECT AP_INVOICES_INTERFACE_S.NEXTVAL
                  INTO R_INVOICE.INVOICE_ID
                  FROM DUAL;
              
                WRITE_LOG('r_invoice.invoice_id =' || R_INVOICE.INVOICE_ID);
              
                G_STEP := 'Get requester id';
                BEGIN
                  SELECT TO_PERSON_ID
                    INTO V_REQUESTER_ID
                    FROM PO_REQUISITION_LINES_ALL   PRL,
                         PO_REQUISITION_HEADERS_ALL PRH
                   WHERE PRH.REQUISITION_HEADER_ID =
                         PRL.REQUISITION_HEADER_ID
                     AND PRH.SEGMENT1 = R_LINE.PR_NUMBER
                     AND ROWNUM = 1;
                
                  R_INVOICE.REQUESTER_ID := V_REQUESTER_ID;
                EXCEPTION
                  WHEN OTHERS THEN
                    V_REQUESTER_ID := NULL;
                END;
              
                --validate invoice date
                R_INVOICE.INVOICE_DATE := R_INV.INVOICE_DATE;
                /*  begin
                    select TO_DATE(r_inv.INVOICE_DATE,'DD/MM/YYYY','NLS_CALENDAR=GREGORIAN')
                    into r_invoice.invoice_date
                    from dual;
                exception
                    when others then
                        v_flag := true;
                        write_log('*Invalid GRN Number : ' || r_inv.RECEIPT_NUMBER);
                        err_msg := '*** INTERFACE DATA ERROR : Invoice Date Format not correct';
                        err_code := 'Error  INVHUB003-002: Date Format not correct';
                        write_invdetail_log(tDtinvlog,err_code,err_msg);
                end;
                */
                --validate invoice number
                G_STEP := 'validate invoice number';
              
                BEGIN
                  SELECT 'Y'
                    INTO V_CHEK_INV
                    FROM AP_INVOICES_ALL AP, PO_VENDORS PV
                   WHERE AP.INVOICE_NUM = R_INV.INVOICE_NUM
                     AND AP.VENDOR_ID = PV.VENDOR_ID
                     AND PV.SEGMENT1 = R_INV.VENDOR_NUM
                     AND AP.ORG_ID = DECODE(R_INV.ORG_CODE,
                                            'DTAC',
                                            102,
                                            'DTN',
                                            142,
                                            'PSB',
                                            218);
                
                  IF SQL%FOUND THEN
                    V_FLAG := TRUE;
                    WRITE_LOG('*Duplicated Invoice Number : ' ||
                              R_INV.INVOICE_NUM);
                    WRITE_LOG('*r_invlog : ' || R_INVLOG.AP_INVOICE);
                    ERR_MSG  := 'Duplicate Invoice in ERP'; --'*** INTERFACE DATA ERROR : Duplicate Invoice in ERP';
                    ERR_CODE := 'INVHUB003-003'; --'Error  INVHUB003-003 : Duplicate Invoice in ERP';
                  
                    SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
                  
                    R_INVLOG.PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                               L_STARTTIME),
                                                       12,
                                                       12);
                  
                    WRITE_INVDETAIL_LOG(R_INVLOG,
                                        GWRITELOG_DETAIL,
                                        ERR_CODE,
                                        ERR_MSG);
                  END IF;
                EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                    NULL;
                END;
              
                --validate org code
                G_STEP := ' validate org code';
              
                BEGIN
                  SELECT ORGANIZATION_ID
                    INTO R_INVOICE.ORG_ID
                    FROM HR_ORGANIZATION_UNITS HOU
                   WHERE NAME = DECODE(R_INV.ORG_CODE,
                                       'DTAC',
                                       'TAC OU - HO',
                                       'DTN',
                                       'DTAC-N',
                                       'PSB',
                                       'PAYSBUY');
                EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                    V_FLAG := TRUE;
                    WRITE_LOG('*Invalid ORG_CODE :*' || R_INV.ORG_CODE || '*');
                    WRITE_LOG('length org_code :' ||
                              LENGTH(R_INV.ORG_CODE));
                    ERR_MSG  := 'Invalid invoice Org'; --'*** INTERFACE DATA ERROR : ORG_CODE name is invalid';
                    ERR_CODE := 'INVHUB003-015'; --'Error  INVHUB003-015 : Invalid invoice Org';
                  
                    SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
                  
                    R_INVLOG.PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                               L_STARTTIME),
                                                       12,
                                                       12);
                    WRITE_INVDETAIL_LOG(R_INVLOG,
                                        GWRITELOG_DETAIL,
                                        ERR_CODE,
                                        ERR_MSG);
                END;
              
                --validate Supplier
                G_STEP := 'validate Supplier';
              
                BEGIN
                  SELECT V.VENDOR_ID
                    INTO R_INVOICE.VENDOR_ID
                    FROM PO_VENDORS V
                   WHERE V.SEGMENT1 = R_INV.VENDOR_NUM;
                EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                    V_FLAG := TRUE;
                    WRITE_LOG('*Invalid Supplier : ' || R_INV.VENDOR_NUM);
                    ERR_MSG  := 'Invalid supplier'; --'*** INTERFACE DATA ERROR : Supplier is invalid';
                    ERR_CODE := 'INVHUB003-001'; --'Error INVHUB003-001: Invalid supplier';
                  
                    SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
                  
                    R_INVLOG.PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                               L_STARTTIME),
                                                       12,
                                                       12);
                    WRITE_INVDETAIL_LOG(R_INVLOG,
                                        GWRITELOG_DETAIL,
                                        ERR_CODE,
                                        ERR_MSG);
                END;
              
                --validate Supplier site
                G_STEP := 'validate Supplier site';
                WRITE_LOG('r_inv.vendor_site_code: ' ||
                          R_INV.VENDOR_SITE_CODE);
                WRITE_LOG('r_invoice.vendor_id: ' || R_INVOICE.VENDOR_ID);
              
                BEGIN
                  SELECT PVS.*
                    INTO V_SITE
                    FROM PO_VENDOR_SITES_ALL PVS
                   WHERE PVS.VENDOR_SITE_CODE = R_INV.VENDOR_SITE_CODE
                     AND PVS.VENDOR_ID = R_INVOICE.VENDOR_ID
                     AND PVS.ORG_ID = R_INVOICE.ORG_ID;
                EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                    BEGIN
                      /*SELECT pvs.*
                       into v_site
                       FROM po_vendor_sites_all pvs
                      WHERE 1=1  --pvs.vendor_site_code = 'DTAC-N'  --r_inv.vendor_site_code
                        AND ROWNUM=1
                        AND   pvs.vendor_id = r_invoice.vendor_id
                        AND   pvs.org_id  =r_invoice.org_id;*/
                      IF INSTR(R_LINE.PO_NUMBER, '-') != 0 THEN
                        L_PO_HEADER  := SUBSTR(R_LINE.PO_NUMBER,
                                               1,
                                               INSTR(R_LINE.PO_NUMBER, '-') - 1);
                        L_PO_RELEASE := SUBSTR(R_LINE.PO_NUMBER,
                                               INSTR(R_LINE.PO_NUMBER, '-') + 1,
                                               LENGTH(R_LINE.PO_NUMBER) -
                                               INSTR(R_LINE.PO_NUMBER, '-'));
                      ELSE
                        L_PO_HEADER  := R_LINE.PO_NUMBER;
                        L_PO_RELEASE := 0;
                      END IF;
                    
                      SELECT PVS.*
                        INTO V_SITE
                        FROM PO_VENDOR_SITES_ALL PVS,
                             PO_HEADERS_ALL      POH,
                             PO_RELEASES_ALL     PO_RELEASE
                       WHERE 1 = 1 --pvs.vendor_site_code = 'DTAC-N'  --r_inv.vendor_site_code
                         AND ROWNUM = 1
                         AND PVS.VENDOR_ID = POH.VENDOR_ID
                         AND PVS.ORG_ID = POH.ORG_ID
                         AND POH.PO_HEADER_ID = PO_RELEASE.PO_HEADER_ID(+)
                         AND POH.ORG_ID = PO_RELEASE.ORG_ID(+)
                            --AND poh.segment1=r_line.po_number
                            --AND poh.segment1||DECODE(po_release.release_num,NULL,NULL,'-'||po_release.release_num)=r_line.po_number
                         AND POH.SEGMENT1 = L_PO_HEADER
                         AND NVL(PO_RELEASE.RELEASE_NUM, 0) = L_PO_RELEASE
                         AND POH.ORG_ID = R_INVOICE.ORG_ID;
                    EXCEPTION
                      WHEN NO_DATA_FOUND THEN
                        V_FLAG := TRUE;
                        WRITE_LOG('*Invalid Supplier Site : ' ||
                                  R_INV.VENDOR_SITE_CODE);
                        WRITE_LOG('*Supplier Number : ' ||
                                  R_INV.VENDOR_NUM);
                        ERR_MSG  := 'Invalid Supplier Site'; -- '*** INTERFACE DATA ERROR : Supplier site is invalid';
                        ERR_CODE := 'INVHUB003-002'; --'Error INVHUB003-002: Invalid Supplier Site';
                      
                        SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
                      
                        R_INVLOG.PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                                   L_STARTTIME),
                                                           12,
                                                           12);
                        WRITE_INVDETAIL_LOG(R_INVLOG,
                                            GWRITELOG_DETAIL,
                                            ERR_CODE,
                                            ERR_MSG);
                        V_SITE := NULL;
                    END;
                END;
              
                R_INVOICE.VENDOR_SITE_ID                := V_SITE.VENDOR_SITE_ID;
                R_INVOICE.ACCTS_PAY_CODE_COMBINATION_ID := V_SITE.ACCTS_PAY_CODE_COMBINATION_ID;
                V_SITE_AUTO_CALC_FLAG                   := V_SITE.AUTO_TAX_CALC_FLAG;
              
                --validate payment Term
                G_STEP := 'validate payment Term';
              
                BEGIN
                  SELECT T.TERM_ID, T.CREATION_DATE
                    INTO R_INVOICE.TERMS_ID, R_INVOICE.TERMS_DATE
                    FROM AP_TERMS T
                   WHERE T.NAME = R_INV.TERMS_NAME;
                EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                    V_FLAG := TRUE;
                    WRITE_LOG('*Invalid Term Name : ' || R_INV.TERMS_NAME);
                    ERR_MSG  := 'Invalid Term Name'; --'*** INTERFACE DATA ERROR : Term name is invalid';
                    ERR_CODE := 'INVHUB003-010'; --'Error  INVHUB003-010 : Invalid Term Name';
                  
                    SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
                  
                    R_INVLOG.PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                               L_STARTTIME),
                                                       12,
                                                       12);
                    WRITE_INVDETAIL_LOG(R_INVLOG,
                                        GWRITELOG_DETAIL,
                                        ERR_CODE,
                                        ERR_MSG);
                END;
              
                --validate invoice source type
                G_STEP := 'validate invoice source type';
              
                BEGIN
                  SELECT UPPER(DISPLAYED_FIELD)
                    INTO V_SOURCE
                    FROM AP_LOOKUP_CODES
                   WHERE LOOKUP_TYPE(+) = 'SOURCE' --'INVOICE TYPE'
                     AND UPPER(DISPLAYED_FIELD) = UPPER(TRIM(R_INV.SOURCE));
                EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                    V_FLAG := TRUE;
                    WRITE_LOG('*Invalid  Source type : ' || R_INV.SOURCE);
                    ERR_MSG  := 'Invalid  Source type'; --'*** INTERFACE DATA ERROR : Invalid  Source type';
                    ERR_CODE := 'INVHUB003-011'; --'Error  INVHUB003-011 : Invalid  Source type';
                  
                    SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
                  
                    R_INVLOG.PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                               L_STARTTIME),
                                                       12,
                                                       12);
                    WRITE_INVDETAIL_LOG(R_INVLOG,
                                        GWRITELOG_DETAIL,
                                        ERR_CODE,
                                        ERR_MSG);
                END;
              
                -- validate currency code
                G_STEP := ' validate currency code';
              
                BEGIN
                  SELECT 'Y'
                    INTO V_CHECK_CURRENCY
                    FROM FND_CURRENCIES
                   WHERE CURRENCY_CODE = R_INV.INVOICE_CURRENCY_CODE;
                EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                    V_FLAG := TRUE;
                    WRITE_LOG('*Invalid  Currency code : ' ||
                              R_INV.INVOICE_CURRENCY_CODE);
                    ERR_MSG  := 'Invalid  Currency code'; --'*** INTERFACE DATA ERROR : Invalid  Currency code';
                    ERR_CODE := 'INVHUB003-012'; --'Error  INVHUB003-012 : Invalid  Currency code';
                  
                    SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
                  
                    R_INVLOG.PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                               L_STARTTIME),
                                                       12,
                                                       12);
                    WRITE_INVDETAIL_LOG(R_INVLOG,
                                        GWRITELOG_DETAIL,
                                        ERR_CODE,
                                        ERR_MSG);
                END;
              
                WRITE_LOG('* 1  r_invoice.org_id : ' || R_INVOICE.ORG_ID);
                WRITE_LOG('* 1  inv period : ' ||
                          TO_CHAR(NVL(R_INVOICE.GL_DATE, SYSDATE),
                                  'MON-YY'));
              
                --validate ap period open
                G_STEP := ' validate ap period open';
              
                BEGIN
                  SELECT 'Y'
                    INTO V_CHECK_PERIOD
                    FROM APPS.HR_OPERATING_UNITS        HR_OPERATING_UNITS,
                         HR.HR_ORGANIZATION_INFORMATION HR_ORGANIZATION_INFORMATION,
                         GL.GL_SETS_OF_BOOKS,
                         GL_PERIOD_STATUSES             PERIOD
                   WHERE HR_OPERATING_UNITS.ORGANIZATION_ID =
                         HR_ORGANIZATION_INFORMATION.ORGANIZATION_ID
                     AND ((HR_ORGANIZATION_INFORMATION.ORG_INFORMATION_CONTEXT =
                         'Operating Unit Information'))
                     AND GL.GL_SETS_OF_BOOKS.SET_OF_BOOKS_ID =
                         HR_OPERATING_UNITS.SET_OF_BOOKS_ID
                     AND HR_OPERATING_UNITS.ORGANIZATION_ID =
                         R_INVOICE.ORG_ID
                     AND PERIOD.APPLICATION_ID = 200
                     AND PERIOD.CLOSING_STATUS = 'O'
                     AND PERIOD.PERIOD_NUM <> 13
                     AND PERIOD.PERIOD_NAME =
                         TO_CHAR(NVL(R_INVOICE.GL_DATE, SYSDATE), 'MON-YY') --to_char(r_invoice.invoice_date,'MON-YY')
                        --r_invoice.gl_date
                     AND PERIOD.SET_OF_BOOKS_ID =
                         GL.GL_SETS_OF_BOOKS.SET_OF_BOOKS_ID;
                EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                    V_FLAG := TRUE;
                    WRITE_LOG('* 1  ORG_CODE : ' || R_INV.ORG_CODE);
                    ERR_MSG  := 'Invoice Date not in the Opened Period'; --'*** INTERFACE DATA ERROR : Invoice Date not in the Opened Period';
                    ERR_CODE := 'INVHUB003-007'; --'Error  INVHUB003-007 : Invoice Date not in the Opened Period';
                  
                    SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
                  
                    R_INVLOG.PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                               L_STARTTIME),
                                                       12,
                                                       12);
                    WRITE_INVDETAIL_LOG(R_INVLOG,
                                        GWRITELOG_DETAIL,
                                        ERR_CODE,
                                        ERR_MSG);
                END;
              
                -----validate po number
                /* g_step :='validate po number';
                begin
                    select 'Y'
                    into v_check_po_number
                    from po_headers_all
                    where segment1 = r_inv.po_number;
                exception
                    when no_data_found then
                        v_flag := true;
                        write_log('*Invalid  PO Number : ' || r_inv.po_number);
                        err_msg := '*** INTERFACE DATA ERROR : Invalid  PO Number';
                        err_code := 'Error  INVHUB003-013 : Invalid  PO Number';
                        write_invdetail_log(tDtinvlog,err_code,err_msg);
                end;  */
              
                IF R_INV.GL_DATE IS NULL THEN
                  R_INVOICE.GL_DATE := TRUNC(SYSDATE);
                ELSE
                  R_INVOICE.GL_DATE := R_INV.GL_DATE;
                END IF;
              
                R_INVOICE.INVOICE_NUM              := R_INV.INVOICE_NUM;
                R_INVOICE.INVOICE_TYPE_LOOKUP_CODE := R_INV.INVOICE_TYPE_LOOKUP_CODE; --'STANDARD';
                --r_invoice.invoice_date                  := r_inv.invoice_date;
                R_INVOICE.VENDOR_NUM       := R_INV.VENDOR_NUM;
                R_INVOICE.VENDOR_SITE_CODE := R_INV.VENDOR_SITE_CODE;
                R_INVOICE.INVOICE_AMOUNT   := R_INV.INVOICE_AMOUNT;
              
                R_INVOICE.INVOICE_CURRENCY_CODE := R_INV.INVOICE_CURRENCY_CODE;
              
                IF R_INV.INVOICE_CURRENCY_CODE != 'THB' THEN
                  BEGIN
                    IF INSTR(R_LINE.PO_NUMBER, '-') != 0 THEN
                      L_PO_HEADER  := SUBSTR(R_LINE.PO_NUMBER,
                                             1,
                                             INSTR(R_LINE.PO_NUMBER, '-') - 1);
                      L_PO_RELEASE := SUBSTR(R_LINE.PO_NUMBER,
                                             INSTR(R_LINE.PO_NUMBER, '-') + 1,
                                             LENGTH(R_LINE.PO_NUMBER) -
                                             INSTR(R_LINE.PO_NUMBER, '-'));
                    ELSE
                      L_PO_HEADER  := R_LINE.PO_NUMBER;
                      L_PO_RELEASE := 0;
                    END IF;
                    G_STEP := 'Get exchange rate 2way';
                    BEGIN
                      IF I_INTERFACE_TYPE = '2WAY' THEN
                        BEGIN
                          SELECT DISTINCT RATE_TYPE, RATE_DATE, RATE
                            INTO R_INVOICE.EXCHANGE_RATE_TYPE,
                                 R_INVOICE.EXCHANGE_DATE,
                                 R_INVOICE.EXCHANGE_RATE
                            FROM PO_HEADERS_ALL  POH,
                                 PO_RELEASES_ALL PO_RELEASE
                           WHERE --segment1=r_line.PO_NUMBER
                           SEGMENT1 = L_PO_HEADER
                           AND NVL(PO_RELEASE.RELEASE_NUM, 0) = L_PO_RELEASE
                          --  poh.segment1||DECODE(po_release.release_num,NULL,NULL,'-'||po_release.release_num)=r_line.PO_NUMBER
                           AND POH.PO_HEADER_ID = PO_RELEASE.PO_HEADER_ID(+)
                           AND POH.ORG_ID = PO_RELEASE.ORG_ID(+)
                           AND POH.ORG_ID = R_INVOICE.ORG_ID;
                        EXCEPTION
                          WHEN OTHERS THEN
                            R_INVOICE.EXCHANGE_RATE_TYPE := '1001';
                        END;
                      ELSIF I_INTERFACE_TYPE = '3WAY' THEN
                        BEGIN
                          G_STEP := 'Get exchange rate 3way';
                          SELECT -- rt.transaction_id
                          --,rt.po_release_id 
                          --,rt.currency_code
                          DISTINCT RT.CURRENCY_CONVERSION_TYPE,
                                   RT.CURRENCY_CONVERSION_DATE,
                                   RT.CURRENCY_CONVERSION_RATE
                          
                          --,rt.transaction_date
                          -- bulk collect into o_grn_infomation
                            INTO R_INVOICE.EXCHANGE_RATE_TYPE,
                                 R_INVOICE.EXCHANGE_DATE,
                                 R_INVOICE.EXCHANGE_RATE
                            FROM RCV_SHIPMENT_HEADERS RSH,
                                 RCV_SHIPMENT_LINES RSL,
                                 RCV_TRANSACTIONS RT,
                                 (SELECT MSI.INVENTORY_ITEM_ID,
                                         MSI.SEGMENT1 ITEM_CODE
                                    FROM MTL_SYSTEM_ITEMS_B MSI,
                                         MTL_PARAMETERS     MP
                                   WHERE MSI.ORGANIZATION_ID =
                                         MP.ORGANIZATION_ID
                                     AND MSI.ORGANIZATION_ID =
                                         MP.MASTER_ORGANIZATION_ID) ITEM,
                                 PO_HEADERS_ALL POH,
                                 PO_RELEASES_ALL PO_RELEASE,
                                 PO_LINES_ALL POL,
                                 PO_LINE_LOCATIONS_ALL PLL,
                                 PO_DISTRIBUTIONS_ALL PD,
                                 PO_VENDORS PV,
                                 PO_VENDOR_SITES_ALL PVS,
                                 AP_TERMS PAYTERM,
                                 ORG_ORGANIZATION_DEFINITIONS ORG,
                                 AP_REPORTING_ENTITIES_ALL ARE
                           WHERE RSH.RECEIPT_NUM = R_LINE.RECEIPT_NUMBER --'2015022225' --'20160001447'
                             AND POH.SEGMENT1 = L_PO_HEADER
                             AND NVL(PO_RELEASE.RELEASE_NUM, 0) =
                                 L_PO_RELEASE
                             AND POH.PO_HEADER_ID =
                                 PO_RELEASE.PO_HEADER_ID(+)
                             AND POH.ORG_ID = PO_RELEASE.ORG_ID(+)
                                -- AND nvl(po_release.release_num,'0')=p_release_num
                                --AND are.tax_entity_id = 10000
                             AND POH.ORG_ID = R_INVOICE.ORG_ID
                             AND RSH.SHIPMENT_HEADER_ID =
                                 RSL.SHIPMENT_HEADER_ID
                             AND RSL.SHIPMENT_HEADER_ID =
                                 RT.SHIPMENT_HEADER_ID
                             AND RSL.SHIPMENT_LINE_ID = RT.SHIPMENT_LINE_ID
                             AND RT.SOURCE_DOCUMENT_CODE = 'PO'
                             AND RT.TRANSACTION_TYPE = 'RECEIVE'
                             AND RECEIPT_SOURCE_CODE = 'VENDOR'
                             AND RSL.ITEM_ID = ITEM.INVENTORY_ITEM_ID(+)
                             AND RT.PO_HEADER_ID = PLL.PO_HEADER_ID
                             AND RT.PO_LINE_ID = PLL.PO_LINE_ID
                             AND RT.PO_LINE_LOCATION_ID =
                                 PD.LINE_LOCATION_ID
                             AND RT.PO_DISTRIBUTION_ID =
                                 PD.PO_DISTRIBUTION_ID
                             AND POH.PO_HEADER_ID = POL.PO_HEADER_ID
                             AND POL.PO_HEADER_ID = PLL.PO_HEADER_ID
                             AND POL.PO_LINE_ID = PLL.PO_LINE_ID
                             AND PLL.PO_HEADER_ID = PD.PO_HEADER_ID
                             AND PLL.PO_LINE_ID = PD.PO_LINE_ID
                             AND PLL.LINE_LOCATION_ID = PD.LINE_LOCATION_ID
                             AND POH.VENDOR_ID = PV.VENDOR_ID
                             AND POH.VENDOR_ID = PVS.VENDOR_ID
                             AND POH.VENDOR_SITE_ID = PVS.VENDOR_SITE_ID
                             AND NVL(PLL.PO_RELEASE_ID, 0) =
                                 NVL(PO_RELEASE.PO_RELEASE_ID, 0) /* fix 3 Feb 2017*/
                             AND POH.TERMS_ID = PAYTERM.TERM_ID
                             AND PLL.SHIP_TO_ORGANIZATION_ID =
                                 ORG.ORGANIZATION_ID
                             AND ORG.OPERATING_UNIT = ARE.ORG_ID;
                        END;
                      END IF;
                    END;
                  END;
                END IF;
              
                R_INVOICE.SOURCE            := R_INV.SOURCE;
                R_INVOICE.GROUP_ID          := R_INV.GROUP_NAME;
                R_INVOICE.DOC_CATEGORY_CODE := R_INV.DOC_CATEGORY_CODE;
              
                R_INVOICE.DESCRIPTION := F_GET_INVOICE_DESCRIPTION(I_INTERFACE_TYPE,
                                                                   R_LINE.PO_NUMBER,
                                                                   NULL, --r_line.PO_LINE_NUMBER,
                                                                   R_LINE.PR_NUMBER,
                                                                   NULL --r_line.PR_LINE_NUMBER
                                                                   );
              
                --r_invoice.terms_id                      := r_inv.terms_name;
                R_INVOICE.PAYMENT_METHOD_LOOKUP_CODE := R_INV.PAYMENT_METHOD_LOOKUP_CODE;
                R_INVOICE.CREATION_DATE              := SYSDATE;
                R_INVOICE.LAST_UPDATE_DATE           := SYSDATE;
                R_INVOICE.CREATED_BY                 := FND_GLOBAL.USER_ID;
                R_INVOICE.LAST_UPDATED_BY            := FND_GLOBAL.USER_ID;
                R_INVOICE.REQUEST_ID                 := FND_GLOBAL.CONC_REQUEST_ID;
                R_INVOICE.ATTRIBUTE_CATEGORY         := 'JA.TH.APXINWKB.DISTRIBUTIONS';
                R_INVOICE.GLOBAL_ATTRIBUTE_CATEGORY  := NULL; --'JA.TH.APXINWKB.DISTRIBUTIONS';
              END;
            END IF; -- end validate header
          
            V_LINE_NUM     := V_LINE_NUM + 1;
            R_INVOICE_LINE := NULL;
            V_CHK_LINE     := FALSE;
          
            WRITE_LOG('* r_line.PO_LINE_NUMBER : ' ||
                      R_LINE.PO_LINE_NUMBER);
            WRITE_LOG('* r_line.RECEIPT_LINE_NUMBER : ' ||
                      R_LINE.RECEIPT_LINE_NUMBER);
            WRITE_LOG('* r_line.LINE_NUMBER : ' || R_LINE.LINE_NUMBER);
            WRITE_LOG('* r_line.PR_LINE_NUMBER : ' ||
                      R_LINE.PR_LINE_NUMBER);
          
            V_LINE_STATUS_INFO := 'Row#' || V_LINE_NUM || ' |Invoice line#' ||
                                  R_LINE.LINE_NUMBER;
          
            SELECT AP_INVOICE_LINES_INTERFACE_S.NEXTVAL
              INTO R_INVOICE_LINE.INVOICE_LINE_ID
              FROM DUAL;
          
            R_INVOICE.TERMS_DATE := R_LINE.BILL_PLACEMENT_DATE; --BILL_PLACEMENT_DATE
          
            IF R_INVOICE.REQUESTER_ID IS NULL THEN
              BEGIN
                IF INSTR(R_LINE.PO_NUMBER, '-') != 0 THEN
                  L_PO_HEADER  := SUBSTR(R_LINE.PO_NUMBER,
                                         1,
                                         INSTR(R_LINE.PO_NUMBER, '-') - 1);
                  L_PO_RELEASE := SUBSTR(R_LINE.PO_NUMBER,
                                         INSTR(R_LINE.PO_NUMBER, '-') + 1,
                                         LENGTH(R_LINE.PO_NUMBER) -
                                         INSTR(R_LINE.PO_NUMBER, '-'));
                ELSE
                  L_PO_HEADER  := R_LINE.PO_NUMBER;
                  L_PO_RELEASE := 0;
                END IF;
                G_STEP := 'Get DELIVER_TO_PERSON_ID';
                SELECT PD.DELIVER_TO_PERSON_ID
                  INTO V_REQUESTER_ID
                  FROM PO_HEADERS_ALL        POH,
                       PO_RELEASES_ALL       PO_RELEASE,
                       PO_LINES_ALL          POL,
                       PO_LINE_LOCATIONS_ALL PLL,
                       PO_DISTRIBUTIONS_ALL  PD
                 WHERE 1 = 1 --poh.segment1=r_line.po_number--'991015019556'
                      --    poh.segment1||DECODE(po_release.release_num,NULL,NULL,'-'||po_release.release_num)=r_line.po_number
                   AND POH.SEGMENT1 = L_PO_HEADER
                   AND NVL(PO_RELEASE.RELEASE_NUM, 0) = L_PO_RELEASE
                   AND POL.LINE_NUM = R_LINE.PO_LINE_NUMBER
                   AND POH.PO_HEADER_ID = POL.PO_HEADER_ID
                   AND POH.ORG_ID = POL.ORG_ID
                   AND POH.PO_HEADER_ID = PO_RELEASE.PO_HEADER_ID(+)
                   AND POH.ORG_ID = PO_RELEASE.ORG_ID(+)
                   AND POL.PO_HEADER_ID = PLL.PO_HEADER_ID
                   AND POL.PO_LINE_ID = PLL.PO_LINE_ID
                   AND POL.ORG_ID = PLL.ORG_ID
                   AND PLL.PO_HEADER_ID = PD.PO_HEADER_ID
                   AND PLL.PO_LINE_ID = PD.PO_LINE_ID
                   AND PLL.ORG_ID = PD.ORG_ID
                   AND NVL(PLL.PO_RELEASE_ID, 0) =
                       NVL(PO_RELEASE.PO_RELEASE_ID, 0) /* fix 3 Feb 2017*/
                   AND PLL.LINE_LOCATION_ID = PD.LINE_LOCATION_ID
                   AND ROWNUM = 1;
              
                R_INVOICE.REQUESTER_ID := V_REQUESTER_ID;
              EXCEPTION
                WHEN OTHERS THEN
                  V_REQUESTER_ID := NULL;
              END;
            END IF;
          
            R_INVOICE_LINE.AMOUNT := R_LINE.LINE_AMOUNT;
            G_STEP                := 'validate GRN';
          
            IF I_INTERFACE_TYPE = '3WAY' THEN
              BEGIN
                R_INVOICE_LINE.RECEIPT_NUMBER      := R_LINE.RECEIPT_NUMBER;
                R_INVOICE_LINE.RECEIPT_LINE_NUMBER := R_LINE.RECEIPT_LINE_NUMBER;
                G_STEP                             := 'Check GRN';
                BEGIN
                  SELECT DISTINCT 'Y'
                    INTO V_CHECK_GRN
                    FROM RCV_SHIPMENT_HEADERS     RSH,
                         RCV_SHIPMENT_LINES       RSL,
                         PO_DISTRIBUTIONS_ALL     POD,
                         GL_CODE_COMBINATIONS_KFV GL
                   WHERE 1 = 1 --rsh.organization_id     = r_invoice.org_id
                     AND RSH.SHIPMENT_HEADER_ID = RSL.SHIPMENT_HEADER_ID
                     AND RSL.PO_HEADER_ID = POD.PO_HEADER_ID
                     AND RSL.PO_DISTRIBUTION_ID = POD.PO_DISTRIBUTION_ID
                     AND POD.CODE_COMBINATION_ID = GL.CODE_COMBINATION_ID
                     AND RSH.RECEIPT_NUM = R_LINE.RECEIPT_NUMBER
                     AND RSL.LINE_NUM = R_LINE.RECEIPT_LINE_NUMBER;
                EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                    V_FLAG := TRUE;
                    WRITE_LOG('*Invalid GRN Number : ' ||
                              R_LINE.RECEIPT_NUMBER || ': ' ||
                              R_LINE.RECEIPT_LINE_NUMBER);
                    ERR_MSG  := 'Invalid GRN Number'; --'*** INTERFACE DATA ERROR : Invalid Account Code';
                    ERR_CODE := 'INVHUB003-023'; --'Error  INVHUB003-021 : Invalid Account Code (3WAY)';
                  
                    SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
                  
                    R_INVLOG.PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                               L_STARTTIME),
                                                       12,
                                                       12);
                    WRITE_INVDETAIL_LOG(R_INVLOG,
                                        GWRITELOG_DETAIL,
                                        ERR_CODE,
                                        ERR_MSG);
                END;
              
                IF V_CHECK_GRN = 'Y' THEN
                  BEGIN
                    G_STEP := 'Get Receiving transaction id';
                    SELECT DISTINCT RT.TRANSACTION_ID
                         , RT.PO_RELEASE_ID
                         , PD.LINE_LOCATION_ID  /* fixed case get wrong po line location on 18 Feb 2017*/
                         , PD.PO_DISTRIBUTION_ID
                         , PLL.MATCH_OPTION
                         , PD.PO_HEADER_ID
                         , PD.PO_LINE_ID
                       
                      INTO R_INVOICE_LINE.RCV_TRANSACTION_ID,
                           R_INVOICE_LINE.PO_RELEASE_ID,
                           R_INVOICE_LINE.PO_LINE_LOCATION_ID,
                           R_INVOICE_LINE.PO_DISTRIBUTION_ID,
                           L_MATCH_OPTION,
                           R_INVOICE_LINE.PO_HEADER_ID,
                           R_INVOICE_LINE.PO_LINE_ID
                      FROM RCV_SHIPMENT_HEADERS RSH,
                           RCV_SHIPMENT_LINES RSL,
                           RCV_TRANSACTIONS RT,
                           (SELECT MSI.INVENTORY_ITEM_ID,
                                   MSI.SEGMENT1 ITEM_CODE
                              FROM MTL_SYSTEM_ITEMS_B MSI, MTL_PARAMETERS MP
                             WHERE MSI.ORGANIZATION_ID = MP.ORGANIZATION_ID
                               AND MSI.ORGANIZATION_ID =
                                   MP.MASTER_ORGANIZATION_ID) ITEM,
                           PO_HEADERS_ALL POH,
                           PO_RELEASES_ALL PO_RELEASE,
                           PO_LINES_ALL POL,
                           PO_LINE_LOCATIONS_ALL PLL,
                           PO_DISTRIBUTIONS_ALL PD,
                           PO_VENDORS PV,
                           PO_VENDOR_SITES_ALL PVS,
                           AP_TERMS PAYTERM,
                           ORG_ORGANIZATION_DEFINITIONS ORG,
                           AP_REPORTING_ENTITIES_ALL ARE
                     WHERE RSH.RECEIPT_NUM = R_LINE.RECEIPT_NUMBER --'2015022225' --'20160001447'
                       AND POH.SEGMENT1 = L_PO_HEADER
                       AND NVL(PO_RELEASE.RELEASE_NUM, 0) = L_PO_RELEASE
                       AND POL.LINE_NUM = R_LINE.PO_LINE_NUMBER
                       AND POH.PO_HEADER_ID = PO_RELEASE.PO_HEADER_ID(+)
                       AND POH.ORG_ID = PO_RELEASE.ORG_ID(+)
                          -- AND nvl(po_release.release_num,'0')=p_release_num
                          --AND are.tax_entity_id = 10000
                       AND POH.ORG_ID = R_INVOICE.ORG_ID
                       AND RSH.SHIPMENT_HEADER_ID = RSL.SHIPMENT_HEADER_ID
                       AND RSL.SHIPMENT_HEADER_ID = RT.SHIPMENT_HEADER_ID
                       AND RSL.SHIPMENT_LINE_ID = RT.SHIPMENT_LINE_ID
                       AND RT.SOURCE_DOCUMENT_CODE = 'PO'
                       AND RT.TRANSACTION_TYPE = 'RECEIVE'
                       AND RECEIPT_SOURCE_CODE = 'VENDOR'
                       AND RSL.ITEM_ID = ITEM.INVENTORY_ITEM_ID(+)
                       AND RT.PO_HEADER_ID = PLL.PO_HEADER_ID
                       AND RT.PO_LINE_ID = PLL.PO_LINE_ID
                       AND RT.PO_LINE_LOCATION_ID = PD.LINE_LOCATION_ID
                       AND RT.PO_DISTRIBUTION_ID = PD.PO_DISTRIBUTION_ID
                       AND POH.PO_HEADER_ID = POL.PO_HEADER_ID
                       AND POL.PO_HEADER_ID = PLL.PO_HEADER_ID
                       AND POL.PO_LINE_ID = PLL.PO_LINE_ID
                       AND PLL.PO_HEADER_ID = PD.PO_HEADER_ID
                       AND PLL.PO_LINE_ID = PD.PO_LINE_ID
                       AND PLL.LINE_LOCATION_ID = PD.LINE_LOCATION_ID
                       AND NVL(PLL.PO_RELEASE_ID, 0) =
                           NVL(PO_RELEASE.PO_RELEASE_ID, 0) /* fix 3 Feb 2017*/
                       AND POH.VENDOR_ID = PV.VENDOR_ID
                       AND POH.VENDOR_ID = PVS.VENDOR_ID
                       AND POH.VENDOR_SITE_ID = PVS.VENDOR_SITE_ID
                       AND POH.TERMS_ID = PAYTERM.TERM_ID
                       AND PLL.SHIP_TO_ORGANIZATION_ID =
                           ORG.ORGANIZATION_ID
                       AND ORG.OPERATING_UNIT = ARE.ORG_ID;
                  EXCEPTION
                    WHEN OTHERS THEN
                      WRITE_LOG('Error get receiving information : ' ||
                                SQLERRM);
                  END;
                END IF;
              
              END;
            END IF;
          
            /*   -- Validate precision amount --joe
              g_step := 'Validate precision amount';
            BEGIN
               select length(r_line.line_amount) - instr(r_line.line_amount,'.')
                      INTO l_check_decimal
                 FROM dual;
            
            
               IF l_check_decimal > 2 THEN
                  write_log('*Invalid precision : ' || r_line.RECEIPT_NUMBER||': '||r_line.RECEIPT_LINE_NUMBER);
                  err_msg := 'Invalid precision ';--'*** INTERFACE DATA ERROR : Invalid Account Code';
                  err_code := 'INVHUB003-024';--'Error  INVHUB003-021 : Invalid Account Code (3WAY)';
                  write_invdetail_log(r_invlog,gWritelog_detail,err_code,err_msg);
               END IF;
            END;*/
            --validate acc code
          
            G_STEP := 'validate acc code';
          
            IF I_INTERFACE_TYPE = '3WAY' THEN
              BEGIN
                R_INVOICE_LINE.RECEIPT_NUMBER      := R_LINE.RECEIPT_NUMBER;
                R_INVOICE_LINE.RECEIPT_LINE_NUMBER := R_LINE.RECEIPT_LINE_NUMBER;
              
                WRITE_LOG('r_invoice.org_id=' || R_INVOICE.ORG_ID);
                WRITE_LOG('r_line.RECEIPT_NUMBER=' ||
                          R_LINE.RECEIPT_NUMBER);
                WRITE_LOG('r_line.RECEIPT_LINE_NUMBER=' ||
                          R_LINE.RECEIPT_LINE_NUMBER);
              
                /*issue 28-oct-2016
                  1) program get the wrong account type
                  2) ignore get account code for 3way
                  3) do the same way as citibank program
                */
                V_DIST_ACC_CODE    := NULL;
                V_DIST_ACC_CONCATE := NULL;
                /*  select DISTINCT gl.code_combination_id,gl.CONCATENATED_SEGMENTS
                 into v_dist_acc_code,v_dist_acc_concate
                 from rcv_shipment_headers rsh,
                      rcv_shipment_lines rsl,
                      po_distributions_all pod,
                      gl_code_combinations_kfv gl
                where 1=1 --rsh.organization_id     = r_invoice.org_id
                  and rsh.shipment_header_id  = rsl.shipment_header_id
                  and rsl.po_header_id        = pod.po_header_id
                  and rsl.po_distribution_id  = pod.po_distribution_id
                  and pod.code_combination_id = gl.code_combination_id
                  and rsh.receipt_num         = r_line.RECEIPT_NUMBER
                  and rsl.line_num            = r_line.RECEIPT_LINE_NUMBER;*/
              
              EXCEPTION
                WHEN NO_DATA_FOUND THEN
                  V_FLAG := TRUE;
                  WRITE_LOG('*Invalid Account Code (3WAY) : ' ||
                            R_LINE.RECEIPT_NUMBER || ' ' ||
                            R_LINE.RECEIPT_LINE_NUMBER);
                  ERR_MSG  := 'Invalid Account Code (3WAY)'; --'*** INTERFACE DATA ERROR : Invalid Account Code';
                  ERR_CODE := 'INVHUB003-021'; --'Error  INVHUB003-021 : Invalid Account Code (3WAY)';
                
                  SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
                
                  R_INVLOG.PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                             L_STARTTIME),
                                                     12,
                                                     12);
                  WRITE_INVDETAIL_LOG(R_INVLOG,
                                      GWRITELOG_DETAIL,
                                      ERR_CODE,
                                      ERR_MSG);
              END;
            ELSE
              BEGIN
                WRITE_LOG('* r_invoice.org_id : ' || R_INVOICE.ORG_ID);
                WRITE_LOG('* r_line.PO_NUMBER  : ' || R_LINE.PO_NUMBER);
                WRITE_LOG('* r_line.PO_LINE_NUMBER : ' ||
                          R_LINE.PO_LINE_NUMBER);
              
                IF INSTR(R_LINE.PO_NUMBER, '-') != 0 THEN
                  L_PO_HEADER  := SUBSTR(R_LINE.PO_NUMBER,
                                         1,
                                         INSTR(R_LINE.PO_NUMBER, '-') - 1);
                  L_PO_RELEASE := SUBSTR(R_LINE.PO_NUMBER,
                                         INSTR(R_LINE.PO_NUMBER, '-') + 1,
                                         LENGTH(R_LINE.PO_NUMBER) -
                                         INSTR(R_LINE.PO_NUMBER, '-'));
                ELSE
                  L_PO_HEADER  := R_LINE.PO_NUMBER;
                  L_PO_RELEASE := 0;
                END IF;
                G_STEP := 'Get account id';
                SELECT DISTINCT GL.CODE_COMBINATION_ID,
                                GL.CONCATENATED_SEGMENTS
                  INTO V_DIST_ACC_CODE, V_DIST_ACC_CONCATE
                  FROM PO_HEADERS_ALL           PO,
                       PO_RELEASES_ALL          PO_RELEASE,
                       PO_LINES_ALL             POL,
                       PO_DISTRIBUTIONS_ALL     POD,
                       GL_CODE_COMBINATIONS_KFV GL
                 WHERE PO.ORG_ID = R_INVOICE.ORG_ID
                   AND PO.PO_HEADER_ID = PO_RELEASE.PO_HEADER_ID(+)
                   AND PO.ORG_ID = PO_RELEASE.ORG_ID(+)
                   AND PO.PO_HEADER_ID = POL.PO_HEADER_ID
                   AND POL.PO_HEADER_ID = POD.PO_HEADER_ID
                   AND POL.PO_LINE_ID = POD.PO_LINE_ID
                   AND POD.CODE_COMBINATION_ID = GL.CODE_COMBINATION_ID
                   AND PO.SEGMENT1 = L_PO_HEADER
                   AND NVL(PO_RELEASE.RELEASE_NUM, 0) = L_PO_RELEASE
                      --and po.segment1      = r_line.PO_NUMBER
                      -- AND po.segment1||DECODE(po_release.release_num,NULL,NULL,'-'||po_release.release_num) = r_line.PO_NUMBER
                   AND POL.LINE_NUM = R_LINE.PO_LINE_NUMBER;
              EXCEPTION
                WHEN NO_DATA_FOUND THEN
                  V_FLAG := TRUE;
                  WRITE_LOG('*Invalid Account Code (2WAY) : ' ||
                            R_LINE.PO_NUMBER || ' ' ||
                            R_LINE.PO_LINE_NUMBER);
                  ERR_MSG  := 'Invalid Account Code (2WAY)'; --'*** INTERFACE DATA ERROR : Invalid Account Code (2WAY)';
                  ERR_CODE := 'INVHUB003-022'; --'Error  INVHUB003-022 : Invalid Account Code (2WAY)';
                
                  SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
                
                  R_INVLOG.PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                             L_STARTTIME),
                                                     12,
                                                     12);
                  WRITE_INVDETAIL_LOG(R_INVLOG,
                                      GWRITELOG_DETAIL,
                                      ERR_CODE,
                                      ERR_MSG);
              END;
            END IF;
          
            --validate tax code
            G_STEP := 'validate tax code';
          
            IF R_LINE.LINE_TAX_CODE IS NOT NULL THEN
              G_STEP := 'Get tax';
              BEGIN
                SELECT ATC.TAX_CODE_COMBINATION_ID,
                       ATC.TAX_ID,
                       ATC.TAX_RATE
                  INTO V_TAX_CCID, R_INVOICE_LINE.TAX_CODE_ID, V_RATE
                  FROM AP_TAX_CODES_ALL ATC
                 WHERE ATC.ENABLED_FLAG = 'Y'
                   AND ATC.NAME = R_LINE.LINE_TAX_CODE
                   AND NVL(ATC.START_DATE, SYSDATE) <= SYSDATE
                   AND NVL(ATC.INACTIVE_DATE, SYSDATE) >= SYSDATE
                   AND ATC.ORG_ID = R_INVOICE.ORG_ID;
              EXCEPTION
                WHEN OTHERS THEN
                  V_CHK_LINE         := TRUE;
                  V_LINE_STATUS_INFO := V_LINE_STATUS_INFO ||
                                        '|Invalid Tax Code (' ||
                                        R_LINE.LINE_TAX_CODE || ')';
                  ERR_MSG            := 'Tax code is invalid'; --'*** INTERFACE DATA ERROR : Tax code is invalid';
                  ERR_CODE           := 'INVHUB003-016'; --'Error  INVHUB003-016 : Tax code is invalid';
                
                  SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
                
                  R_INVLOG.PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                             L_STARTTIME),
                                                     12,
                                                     12);
                  WRITE_INVDETAIL_LOG(R_INVLOG,
                                      GWRITELOG_DETAIL,
                                      ERR_CODE,
                                      ERR_MSG);
              END;
            
              R_INVOICE_LINE.TAX_CODE               := R_LINE.LINE_TAX_CODE;
              R_INVOICE_LINE.TAX_CODE_OVERRIDE_FLAG := 'N';
              R_INVOICE_LINE.TAX_RECOVERABLE_FLAG   := 'Y';
            ELSE
              R_INVOICE_LINE.TAX_CODE          := NULL;
              R_INVOICE_LINE.TAX_RECOVERY_RATE := NULL;
            END IF;
          
            V_SUPP_NAME := NULL;
          
            --validate project and project task
            G_STEP := 'validate project and project task';
          
            IF R_LINE.PROJECT_NUMBER IS NULL AND
               R_LINE.PROJECT_TASK_NUMBER IS NULL AND
               R_LINE.EXPENDITURE_TYPE IS NULL AND
               R_LINE.ORGANIZATION_ID IS NULL THEN
              V_VALIDATE_ERROR := FALSE;
            ELSE
              --validate Project number
              IF R_LINE.PROJECT_NUMBER IS NULL THEN
                V_VALIDATE_ERROR   := TRUE;
                V_FLAG             := TRUE;
                V_LINE_STATUS_INFO := V_LINE_STATUS_INFO ||
                                      '|Invalid Project (' ||
                                      R_LINE.PROJECT_NUMBER || ')';
                WRITE_LOG('*Invalid Project : ' || R_LINE.PROJECT_NUMBER);
                ERR_MSG  := 'Project is invalid'; --'*** INTERFACE DATA ERROR : Project number is invalid';
                ERR_CODE := 'INVHUB003-017'; --'Error  INVHUB003-017 : Project is invalid';
              
                SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
              
                R_INVLOG.PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                           L_STARTTIME),
                                                   12,
                                                   12);
                WRITE_INVDETAIL_LOG(R_INVLOG,
                                    GWRITELOG_DETAIL,
                                    ERR_CODE,
                                    ERR_MSG);
              END IF;
            
              --validate Task Number
              G_STEP := 'validate Task Number';
            
              IF R_LINE.PROJECT_NUMBER IS NOT NULL THEN
                BEGIN
                  SELECT PA.PROJECT_ID, PT.TASK_ID
                    INTO V_PROJ_ID, V_TASK_ID
                    FROM PA_PROJECTS_ALL PA, PA_TASKS PT
                   WHERE PA.PROJECT_ID = PT.PROJECT_ID
                     AND PA.SEGMENT1 = R_LINE.PROJECT_NUMBER
                     AND PT.TASK_NUMBER = R_LINE.PROJECT_TASK_NUMBER;
                EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                    V_VALIDATE_ERROR   := TRUE;
                    V_FLAG             := TRUE;
                    V_LINE_STATUS_INFO := V_LINE_STATUS_INFO ||
                                          '|Invalid Task (' ||
                                          R_LINE.PROJECT_TASK_NUMBER || ')';
                    WRITE_LOG('*Invalid Task : ' ||
                              R_LINE.PROJECT_TASK_NUMBER);
                    ERR_MSG  := 'Task number is invalid'; --'*** INTERFACE DATA ERROR : Task number is invalid';
                    ERR_CODE := 'INVHUB003-018'; --'Error  INVHUB003-018 : Task number is invalid';
                  
                    SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
                  
                    R_INVLOG.PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                               L_STARTTIME),
                                                       12,
                                                       12);
                    WRITE_INVDETAIL_LOG(R_INVLOG,
                                        GWRITELOG_DETAIL,
                                        ERR_CODE,
                                        ERR_MSG);
                END;
              END IF;
            
              --validate Expenditure Type
              G_STEP := 'validate Expenditure Type';
            
              BEGIN
                SELECT PET.EXPENDITURE_TYPE
                  INTO V_EXPEN_TYPE
                  FROM PA_EXPENDITURE_TYPES PET
                 WHERE PET.EXPENDITURE_TYPE = R_LINE.EXPENDITURE_TYPE;
              EXCEPTION
                WHEN NO_DATA_FOUND THEN
                  V_VALIDATE_ERROR   := TRUE;
                  V_FLAG             := TRUE;
                  V_LINE_STATUS_INFO := V_LINE_STATUS_INFO ||
                                        '|Invalid Expenditure Type (' ||
                                        R_LINE.EXPENDITURE_TYPE || ')';
                  ERR_MSG            := 'Invalid Expenditure Type'; --'*** INTERFACE DATA ERROR : Invalid Expenditure Type!';
                  ERR_CODE           := 'INVHUB003-019'; --'Error  INVHUB003-019 : Invalid Expenditure Type';
                
                  SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
                
                  R_INVLOG.PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                             L_STARTTIME),
                                                     12,
                                                     12);
                  WRITE_INVDETAIL_LOG(R_INVLOG,
                                      GWRITELOG_DETAIL,
                                      ERR_CODE,
                                      ERR_MSG);
              END;
            
              --validate Expenditure Org
              G_STEP := 'validate Expenditure Org';
            
              IF R_LINE.ORGANIZATION_ID IS NULL THEN
                BEGIN
                  SELECT HOU.ORGANIZATION_ID
                    INTO V_EXPEN_ORG
                    FROM HR_ORGANIZATION_UNITS  HOU,
                         TAC_INVHUB_INVOICE_STG L
                   WHERE HOU.ORGANIZATION_ID = R_LINE.ORGANIZATION_ID
                     AND HOU.NAME(+) = L.EXPENDITURE_ORGANIZATION;
                EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                    V_VALIDATE_ERROR   := TRUE;
                    V_FLAG             := TRUE;
                    V_LINE_STATUS_INFO := V_LINE_STATUS_INFO ||
                                          '|Invalid Expenditure Organization (' ||
                                          R_LINE.EXPENDITURE_ORGANIZATION || ')';
                    ERR_MSG            := 'Invalid Expenditure Organization'; --'*** INTERFACE DATA ERROR : Invalid Expenditure Organization!';
                    ERR_CODE           := 'INVHUB003-020'; --'Error  INVHUB003-020 : Invalid Expenditure Organization';
                  
                    SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
                  
                    R_INVLOG.PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                               L_STARTTIME),
                                                       12,
                                                       12);
                    WRITE_INVDETAIL_LOG(R_INVLOG,
                                        GWRITELOG_DETAIL,
                                        ERR_CODE,
                                        ERR_MSG);
                END;
              END IF;
            END IF;
            G_STEP := 'Get GET_PROJECT_INFORMATION';
            GET_PROJECT_INFORMATION(R_LINE.PR_NUMBER,
                                    R_LINE.PR_LINE_NUMBER,
                                    L_PROJECT_ID,
                                    L_PROJECT_NAME,
                                    L_TASK_ID,
                                    L_TASK_NAME,
                                    L_EXPENDITURE_TYPE,
                                    L_EXPENDITURE_ITEM_DATE,
                                    L_EXPENDITURE_ORGANIZATION_ID);
          
            R_INVOICE_LINE.INVOICE_ID := R_INVOICE.INVOICE_ID;
            R_INVOICE_LINE.ORG_ID     := R_INVOICE.ORG_ID;
          
            IF R_LINE.ERP_AP_LINENUMBER IS NOT NULL THEN
              --i_interface_type = '3WAY' THEN
              R_INVOICE_LINE.LINE_NUMBER := R_LINE.ERP_AP_LINENUMBER; --r_line.LINE_NUMBER;
            ELSE
              R_INVOICE_LINE.LINE_NUMBER := R_LINE.LINE_NUMBER;
            END IF;
          
            -------------------------------
            R_INVOICE_LINE.ACCOUNTING_DATE             := R_LINE.GL_DATE;
            R_INVOICE_LINE.PROJECT_ID                  := NULL; --l_project_id;--v_proj_id;--r_line.project_id;
            R_INVOICE_LINE.TASK_ID                     := NULL; --l_task_id;--v_task_id;
            R_INVOICE_LINE.EXPENDITURE_TYPE            := NULL; --l_expenditure_type;--r_line.expenditure_type;
            R_INVOICE_LINE.EXPENDITURE_ORGANIZATION_ID := NULL; --l_expenditure_organization_id;--r_line.organization_id;
            R_INVOICE_LINE.EXPENDITURE_ITEM_DATE       := NULL; --l_expenditure_item_date;--sysdate;
            R_INVOICE_LINE.LINE_TYPE_LOOKUP_CODE       := R_LINE.LINE_TYPE_LOOKUP_CODE;
            R_INVOICE_LINE.DESCRIPTION                 := F_GET_INVOICE_DESCRIPTION(I_INTERFACE_TYPE,
                                                                                    R_LINE.PO_NUMBER,
                                                                                    R_LINE.PO_LINE_NUMBER,
                                                                                    R_LINE.PR_NUMBER,
                                                                                    R_LINE.PR_LINE_NUMBER);
            R_INVOICE_LINE.DIST_CODE_COMBINATION_ID    := V_DIST_ACC_CODE;
            R_INVOICE_LINE.DIST_CODE_CONCATENATED      := V_DIST_ACC_CONCATE;
            R_INVOICE_LINE.AWT_GROUP_NAME              := R_LINE.AWT_GROUP_NAME;
            R_INVOICE_LINE.ATTRIBUTE4                  := R_LINE.ACT_VENDOR_NUM;
            R_INVOICE_LINE.ATTRIBUTE5                  := R_LINE.ACT_VENDOR_SITE_CODE;
            R_INVOICE_LINE.GLOBAL_ATTRIBUTE_CATEGORY   := 'JA.TH.APXINWKB.DISTRIBUTIONS';
            IF I_INTERFACE_TYPE = '2WAY' THEN
                GET_PO_INFORMATION(R_LINE.PO_NUMBER,
                                   R_LINE.PO_LINE_NUMBER,
                                   --r_invoice_line.po_header_id,
                                   --r_invoice_line.po_line_id,
                                   R_INVOICE_LINE.PO_LINE_LOCATION_ID,
                                   R_INVOICE_LINE.PO_DISTRIBUTION_ID,
                                   L_MATCH_OPTION);
            END IF;
          
            -- r_invoice_line.po_number  := r_line.po_number;
            /*  IF i_interface_type = '3WAY' THEN
                 r_invoice_line.line_number   := r_line.erp_ap_linenumber;
              ELSE
                 r_invoice_line.line_number   := r_line.line_number;
              END IF;
            */
            IF I_INTERFACE_TYPE = '3WAY' THEN
              IF NVL(L_MATCH_OPTION, 'P') = 'P' THEN
                BEGIN
                  V_VALIDATE_ERROR   := TRUE;
                  V_FLAG             := TRUE;
                  V_LINE_STATUS_INFO := V_LINE_STATUS_INFO ||
                                        'Invalid match option for po number : ' ||
                                        R_LINE.PO_NUMBER;
                
                  WRITE_LOG('Invalid match option for po number : ' ||
                            R_LINE.PO_NUMBER);
                  ERR_MSG  := 'Invalid match option for po number : ' ||
                              R_LINE.PO_NUMBER; --'*** INTERFACE DATA ERROR : Project number is invalid';
                  ERR_CODE := 'INVHUB003-025'; --'Error  INVHUB003-017 : Project is invalid';
                
                  SELECT CURRENT_TIMESTAMP INTO L_ENDTIME FROM DUAL;
                
                  R_INVLOG.PROCESSING_TIME := SUBSTR(TO_CHAR(L_ENDTIME -
                                                             L_STARTTIME),
                                                     12,
                                                     12);
                  WRITE_INVDETAIL_LOG(R_INVLOG,
                                      GWRITELOG_DETAIL,
                                      ERR_CODE,
                                      ERR_MSG);
                END;
              END IF;
            END IF;
          
            -- r_invoice_line.po_header_id                 := get_po_header(r_line.po_number
          
            /*IF R_INVOICE_LINE.GLOBAL_ATTRIBUTE16 IS NULL THEN 
               R_INVOICE_LINE.GLOBAL_ATTRIBUTE16 := R_INV.SOURCE; 
            END IF;*/ -- change default fxth to AP 
             
            IF R_INVOICE_LINE.GLOBAL_ATTRIBUTE16 IS NULL THEN 
               R_INVOICE_LINE.GLOBAL_ATTRIBUTE16 := 'AP'; 
            END IF;
          
            IF R_LINE.TAX_ACCT_PERIOD IS NULL THEN
              R_INVOICE_LINE.ATTRIBUTE13 := V_PERIOD;
            ELSE
              R_INVOICE_LINE.ATTRIBUTE13 := R_LINE.TAX_ACCT_PERIOD;
            END IF;
          
            R_INVOICE_LINE.ATTRIBUTE15       := R_LINE.REPORTING_ENTITY;
            R_INVOICE_LINE.GLOBAL_ATTRIBUTE9 := V_SUPP_NAME;
          
            R_INVOICE_LINE.ATTRIBUTE_CATEGORY := 'JA.TH.APXINWKB.DISTRIBUTIONS';
          
            R_INVOICE_LINE.GLOBAL_ATTRIBUTE20 := R_LINE.REPORTING_ENTITY;
          
            IF R_LINE.AWT_GROUP_NAME IS NOT NULL THEN
              R_INVOICE_LINE.GLOBAL_ATTRIBUTE12 := R_LINE.WHT_REVENUE_NAME;
              R_INVOICE_LINE.GLOBAL_ATTRIBUTE13 := R_LINE.WHT_REVENUE_TYPE;
              R_INVOICE_LINE.GLOBAL_ATTRIBUTE14 := R_LINE.WHT_CONDITION;
              R_INVOICE_LINE.GLOBAL_ATTRIBUTE15 := R_LINE.PHOR_NGOR_DOR;
              R_INVOICE_LINE.GLOBAL_ATTRIBUTE18 := R_LINE.WHT_ACCT_PERIOD;
            END IF;
          
            R_INVOICE_LINE.CREATED_BY    := FND_GLOBAL.USER_ID;
            R_INVOICE_LINE.CREATION_DATE := SYSDATE;
          
            --r_invoice_line.po_release_id
            --r_invoice_line.rcv_transaction_id
          
            IF V_CHK_LINE OR V_FLAG THEN
              V_LINE_COUNT_ERR   := V_LINE_COUNT_ERR + 1;
              V_LINE_STATUS_INFO := V_LINE_STATUS_INFO || '| Rejected';
              WRITE_LOG('update_staging_status 1');
              WRITE_LOG('R_LINE.LINE_NUMBER=' || R_LINE.LINE_NUMBER);
              WRITE_LOG('R_line.erp_ap_linenumber=' ||
                        R_LINE.ERP_AP_LINENUMBER);
              --joe
              G_STEP := 'UPDATE_STAGING_STATUS with status E';
              UPDATE_STAGING_STATUS(I_INV_NUM        => R_INVOICE.INVOICE_NUM,
                                    I_VENDOR_NUM     => R_INVOICE.VENDOR_NUM,
                                    I_SOURCE         => I_SOURCE,
                                    I_INTERFACE_TYPE => I_INTERFACE_TYPE,
                                    I_LINE_NUM       => R_LINE.ERP_AP_LINENUMBER, --R_LINE.LINE_NUMBER,
                                    I_STATUS         => 'E',
                                    I_FILENAME       => R_READFILE.FILE_NAME,
                                    I_ORG            => R_INV.ORG_CODE);
            
              V_VALIDATE_ERROR := TRUE;
            ELSE
              IF R_LINE.LINE_TYPE_LOOKUP_CODE = 'ITEM' AND
                 R_LINE.LINE_TAX_CODE IS NOT NULL THEN
                BEGIN
                  G_STEP := 'Get tax rate when line type lookup code=ITEM';
                  SELECT TAX_RATE
                    INTO V_TAX_RATE
                    FROM AP_TAX_CODES_ALL
                   WHERE NAME = R_LINE.LINE_TAX_CODE
                     AND ORG_ID = R_INVOICE.ORG_ID
                     AND ((ENABLED_FLAG = 'Y') OR (ENABLED_FLAG IS NULL))
                     AND ((TRUNC(START_DATE) <= TRUNC(SYSDATE)) AND
                         (TRUNC(NVL(INACTIVE_DATE, SYSDATE)) >=
                         TRUNC(SYSDATE)));
                EXCEPTION
                  WHEN OTHERS THEN
                    V_TAX_RATE := 0;
                END;
              
                R_INVOICE_LINE.ATTRIBUTE9  := R_INVOICE_LINE.AMOUNT;
                R_INVOICE_LINE.ATTRIBUTE8  := ROUND(R_INVOICE_LINE.AMOUNT *
                                                    (V_TAX_RATE / 100),
                                                    2);
                R_INVOICE_LINE.ATTRIBUTE10 := V_SUPP_NAME;
              END IF;
            
              IF V_FLAG = FALSE THEN
                WRITE_LOG('***** INSERT ap_invoice_lines_interface *****');
                G_STEP := 'INSERT ap_invoice_lines_interface';
                INSERT_INVOICE_LINE(IR_INVOICE_LINE => R_INVOICE_LINE);
              ELSE
                WRITE_LOG('v_flag=true');
              END IF;
            
              --r_invoice.invoice_amount := nvl(r_invoice.invoice_amount, 0) + r_invoice_line.amount; -- For Invoice Header
            
              V_LINE_STATUS_INFO := V_LINE_STATUS_INFO || '| Processed';
              G_STEP             := 'update_staging_status';
              WRITE_LOG(G_STEP);
              --joe
              WRITE_LOG('R_LINE.LINE_NUMBER=' || R_LINE.LINE_NUMBER);
              WRITE_LOG('R_line.erp_ap_linenumber=' ||
                        R_LINE.ERP_AP_LINENUMBER);
              G_STEP := 'UPDATE_STAGING_STATUS with status Y';
              UPDATE_STAGING_STATUS(I_INV_NUM        => R_INVOICE.INVOICE_NUM,
                                    I_VENDOR_NUM     => R_INVOICE.VENDOR_NUM,
                                    I_SOURCE         => I_SOURCE,
                                    I_INTERFACE_TYPE => I_INTERFACE_TYPE,
                                    I_LINE_NUM       => R_LINE.ERP_AP_LINENUMBER, --R_LINE.LINE_NUMBER,
                                    I_STATUS         => 'Y',
                                    I_FILENAME       => R_READFILE.FILE_NAME,
                                    I_ORG            => R_INV.ORG_CODE);
              WRITE_LOG('after ' || G_STEP);
              WRITE_LOG('r_invoice.invoice_num =' || R_INVOICE.INVOICE_NUM);
              WRITE_LOG('i_source =' || I_SOURCE);
              WRITE_LOG('i_interface_type =' || I_INTERFACE_TYPE);
              WRITE_LOG('r_line.line_number =' || R_LINE.LINE_NUMBER);
              --joe
              WRITE_LOG('r_invlog.ap_invoice=' || R_INVLOG.AP_INVOICE);
              WRITE_LOG('r_invlog.ap_invoie_line' ||
                        R_INVLOG.AP_INVOIE_LINE);
            
              --write_invdetail_log(r_invlog,gWritelog_detail,NULL,NULL);
              WRITE_LOG('after ' || G_STEP);
              V_LINE_COUNT := V_LINE_COUNT + 1;
            END IF;
          
            IF V_CHK_LINE THEN
              V_FLAG := TRUE;
            END IF;
          
            WRITE_LOG(V_LINE_STATUS_INFO);
            V_FIRSTLINE_FLAG := 'N';
          END LOOP; --Line loop
        
          --end if;--------------------------------------------------
        
          IF V_CHK_LINE = FALSE THEN
            WRITE_LOG('***** INSERTERT ap_invoices_interface *****');
            INSERT_INVOICE(IR_INVOICE => R_INVOICE);
            WRITE_LOG('Invoice ID: ' || R_INVOICE.INVOICE_ID);
          END IF;
        
          IF V_FLAG = TRUE THEN
            WRITE_LOG('Rollback transaction of invoice number ' ||
                      R_INV.INVOICE_NUM);
            ROLLBACK TO SAVEPOINT V_SAVE_PNT;
          END IF;
          --TAC_AP_INVOICEHUB_PKG.write_invdetail_log(r_invlog,gWritelog_detail,err_code,err_msg);
        END LOOP; -- Header loop
      
        IF (V_LINE_COUNT <= 0) THEN
          WRITE_LOG('No invoice line to process');
        END IF;
      
        -- if not v_validate_error and v_line_count > 0 then
        BEGIN
          COMMIT;
        
          WRITE_LOG('Start Payable Open Interface ---------------------------');
        
          BEGIN
            SELECT SUM(DECODE(ORG_ID, 102, 1, 0)),
                   SUM(DECODE(ORG_ID, 142, 1, 0)),
                   SUM(DECODE(ORG_ID, 218, 1, 0))
              INTO L_COUNT_DTAC, L_COUNT_DTN, L_COUNT_PSB
              FROM AP_INVOICES_INTERFACE
             WHERE GROUP_ID = V_GROUP;
          END;
        
          IF L_COUNT_DTAC != 0 THEN
            BEGIN
              --l_resp_name :='AP-HO';
            
              BEGIN
                SELECT FRESP.RESPONSIBILITY_ID, FRESP.APPLICATION_ID
                  INTO L_RESP_ID, L_RESP_APP_ID
                  FROM FND_USER FND, FND_RESPONSIBILITY_TL FRESP
                 WHERE FND.USER_ID = FND_GLOBAL.USER_ID
                   AND FRESP.RESPONSIBILITY_NAME = 'AP-HO';
              EXCEPTION
                WHEN NO_DATA_FOUND THEN
                  L_RESP_ID     := NULL;
                  L_RESP_APP_ID := NULL;
              END;
            
              IF L_RESP_ID IS NOT NULL AND L_RESP_APP_ID IS NOT NULL THEN
                FND_GLOBAL.APPS_INITIALIZE(USER_ID      => FND_GLOBAL.USER_ID,
                                           RESP_ID      => L_RESP_ID,
                                           RESP_APPL_ID => L_RESP_APP_ID);
              END IF;
            
              AP_REQ_ID := FND_REQUEST.SUBMIT_REQUEST(APPLICATION => 'SQLAP',
                                                      PROGRAM     => 'APXIIMPT',
                                                      ARGUMENT1   => V_SOURCE,
                                                      ARGUMENT2   => V_GROUP,
                                                      ARGUMENT3   => V_GROUP --i_batch
                                                     ,
                                                      ARGUMENT4   => NULL,
                                                      ARGUMENT5   => NULL,
                                                      ARGUMENT6   => NULL,
                                                      ARGUMENT7   => 'N');
            
              V_I_GROUP     := V_GROUP;
              V_AP_REQ_ID   := AP_REQ_ID;
              L_DTAC_REQ_ID := AP_REQ_ID;
            
              COMMIT;
            
              IF AP_REQ_ID > 0 THEN
                WRITE_LOG('+----------------------------------------------------------------------------+');
                WRITE_LOG('Payable Interface Start Request ID: ' ||
                          AP_REQ_ID);
                CONC_WAIT(AP_REQ_ID); --,v_phase,v_request_phase);
                --EXIT WHEN ( UPPER(v_request_phase) = 'COMPLETE' OR v_phase = 'C');
                WRITE_LOG('Successfull');
                WRITE_LOG('+----------------------------------------------------------------------------+');
                --c_ap_req_id := fnd_request.submit_request(application => 'SQLAP', program => 'TACAP016_V3_EXTD', argument1 => v_ap_req_id, argument2 => v_i_group);
              ELSE
                WRITE_LOG('Can not Submit Payable Open Interface ' ||
                          TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS') ||
                          '----------------------------------');
              END IF;
            END;
          END IF;
        
          -----  DTN
          IF L_COUNT_DTN != 0 THEN
            BEGIN
              --l_resp_name :='DTN_AP';
            
              BEGIN
                SELECT FRESP.RESPONSIBILITY_ID, FRESP.APPLICATION_ID
                  INTO L_RESP_ID, L_RESP_APP_ID
                  FROM FND_USER FND, FND_RESPONSIBILITY_TL FRESP
                 WHERE FND.USER_ID = FND_GLOBAL.USER_ID
                   AND FRESP.RESPONSIBILITY_NAME = 'DTN_AP';
              EXCEPTION
                WHEN NO_DATA_FOUND THEN
                  L_RESP_ID     := NULL;
                  L_RESP_APP_ID := NULL;
              END;
            
              IF L_RESP_ID IS NOT NULL AND L_RESP_APP_ID IS NOT NULL THEN
                FND_GLOBAL.APPS_INITIALIZE(USER_ID      => FND_GLOBAL.USER_ID,
                                           RESP_ID      => L_RESP_ID,
                                           RESP_APPL_ID => L_RESP_APP_ID);
              END IF;
            
              AP_REQ_ID := FND_REQUEST.SUBMIT_REQUEST(APPLICATION => 'SQLAP',
                                                      PROGRAM     => 'APXIIMPT',
                                                      ARGUMENT1   => V_SOURCE,
                                                      ARGUMENT2   => V_GROUP,
                                                      ARGUMENT3   => V_GROUP --i_batch
                                                     ,
                                                      ARGUMENT4   => NULL,
                                                      ARGUMENT5   => NULL,
                                                      ARGUMENT6   => NULL,
                                                      ARGUMENT7   => 'N');
            
              V_I_GROUP    := V_GROUP;
              V_AP_REQ_ID  := AP_REQ_ID;
              L_DTN_REQ_ID := AP_REQ_ID;
              COMMIT;
            
              IF AP_REQ_ID > 0 THEN
                WRITE_LOG('+----------------------------------------------------------------------------+');
                WRITE_LOG('Payable Interface Start Request ID: ' ||
                          AP_REQ_ID);
                CONC_WAIT(AP_REQ_ID); --,v_phase,v_request_phase);
                --EXIT WHEN ( UPPER(v_request_phase) = 'COMPLETE' OR v_phase = 'C');
                WRITE_LOG('Successfull');
                WRITE_LOG('+----------------------------------------------------------------------------+');
                --c_ap_req_id := fnd_request.submit_request(application => 'SQLAP', program => 'TACAP016_V3_EXTD', argument1 => v_ap_req_id, argument2 => v_i_group);
              ELSE
                WRITE_LOG('Can not Submit Payable Open Interface ' ||
                          TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS') ||
                          '----------------------------------');
              END IF;
            END;
          END IF;
        
          -----  PSB
          IF L_COUNT_PSB != 0 THEN
            BEGIN
              --  l_resp_name :='PB_AP';
            
              BEGIN
                SELECT FRESP.RESPONSIBILITY_ID, FRESP.APPLICATION_ID
                  INTO L_RESP_ID, L_RESP_APP_ID
                  FROM FND_USER FND, FND_RESPONSIBILITY_TL FRESP
                 WHERE FND.USER_ID = FND_GLOBAL.USER_ID
                   AND FRESP.RESPONSIBILITY_NAME = 'PB_AP';
              EXCEPTION
                WHEN NO_DATA_FOUND THEN
                  L_RESP_ID     := NULL;
                  L_RESP_APP_ID := NULL;
              END;
            
              IF L_RESP_ID IS NOT NULL AND L_RESP_APP_ID IS NOT NULL THEN
                FND_GLOBAL.APPS_INITIALIZE(USER_ID      => FND_GLOBAL.USER_ID,
                                           RESP_ID      => L_RESP_ID,
                                           RESP_APPL_ID => L_RESP_APP_ID);
              END IF;
            
              AP_REQ_ID := FND_REQUEST.SUBMIT_REQUEST(APPLICATION => 'SQLAP',
                                                      PROGRAM     => 'APXIIMPT',
                                                      ARGUMENT1   => V_SOURCE,
                                                      ARGUMENT2   => V_GROUP,
                                                      ARGUMENT3   => V_GROUP --i_batch
                                                     ,
                                                      ARGUMENT4   => NULL,
                                                      ARGUMENT5   => NULL,
                                                      ARGUMENT6   => NULL,
                                                      ARGUMENT7   => 'N');
            
              V_I_GROUP    := V_GROUP;
              V_AP_REQ_ID  := AP_REQ_ID;
              L_PSB_REQ_ID := AP_REQ_ID;
              COMMIT;
            
              IF AP_REQ_ID > 0 THEN
                WRITE_LOG('+----------------------------------------------------------------------------+');
                WRITE_LOG('Payable Interface Start Request ID: ' ||
                          AP_REQ_ID);
                CONC_WAIT(AP_REQ_ID); --,v_phase,v_request_phase);
                --EXIT WHEN ( UPPER(v_request_phase) = 'COMPLETE' OR v_phase = 'C');
                WRITE_LOG('Successfull');
                WRITE_LOG('+----------------------------------------------------------------------------+');
                --c_ap_req_id := fnd_request.submit_request(application => 'SQLAP', program => 'TACAP016_V3_EXTD', argument1 => v_ap_req_id, argument2 => v_i_group);
              ELSE
                WRITE_LOG('Can not Submit Payable Open Interface ' ||
                          TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS') ||
                          '----------------------------------');
              END IF;
            END;
          END IF;
        END;
        --  end if; -- Ctrl flag
      
        /*  tSuminvlog.EXTEND;
        tSuminvlog(tSuminvlog.LAST).LEVEL_TYPE                  := 'H';
        tSuminvlog(tSuminvlog.LAST).REQUEST_ID                  := v_conc_request;
        tSuminvlog(tSuminvlog.LAST).PROGRAM_NAME                := i_prog_name;
        tSuminvlog(tSuminvlog.LAST).PROCESS_DATE                := v_timestamp_start;
        tSuminvlog(tSuminvlog.LAST).PROCESSING_TYPE             := 'Download';
        tSuminvlog(tSuminvlog.LAST).RECORD_COUNT_COMPLETION     := v_line_count;
        tSuminvlog(tSuminvlog.LAST).RECORD_COUNT_ERROR          := v_line_count_err;
        tSuminvlog(tSuminvlog.LAST).PROCESSING_DATE             := systimestamp - v_timestamp_start;
        
        TAC_AP_INVOICEHUB_PKG.write_invsummary_log(tSuminvlog);*/
      
        /*  write_invsummary_log(gWritelog_detail,p_current_request_id);
        
        commit;--fnd_global.CONC_REQUEST_ID
        APPS.tac_po_invoicehub_pkg.call_logmonitor('DTINVHUB003',p_current_request_id);
        
        */
        -- write_invdetail_log --newjoe
      END;
    END LOOP; -- end loop read_file
  
    GET_ERROR_FROM_INTERFACE(GWRITELOG_DETAIL,
                             V_GROUP,
                             L_STARTTIME,
                             P_CURRENT_REQUEST_ID,
                             L_DTAC_REQ_ID,
                             L_DTN_REQ_ID,
                             L_PSB_REQ_ID);
    WRITE_INVSUMMARY_LOG(GWRITELOG_DETAIL,
                         P_CURRENT_REQUEST_ID,
                         L_STARTTIME);
  
    COMMIT; --fnd_global.CONC_REQUEST_ID
    APPS.TAC_PO_INVOICEHUB_PKG.CALL_LOGMONITOR('DTINVHUB003',
                                               P_CURRENT_REQUEST_ID);
    TAC_PO_INVOICEHUB_PKG.WRITE_OUTPUT(P_CURRENT_REQUEST_ID, 'DTINVHUB003');
  EXCEPTION
    WHEN OTHERS THEN
      WRITE_LOG('DTINVHUB003 : Interface AP Invoices Exception .');
      WRITE_LOG('Error step : ' || G_STEP);
      WRITE_LOG('Error MSG: ' || SQLCODE || ' - ' || SQLERRM);
    
      UPDATE TAC_INVHUB_INVOICE_STG
         SET INTERFACE_AP_FLAG = 'E'
       WHERE 1 = 1
         AND SOURCE = I_SOURCE
         AND INTERFACE_TYPE = I_INTERFACE_TYPE
         AND FILE_NAME = L_FILE_NAME;
    
      COMMIT;
    
      ERR_CODE := 2;
  END INTERFACE_AP_INVOICEHUB;
END TAC_AP_INVOICEHUB_PKG;
