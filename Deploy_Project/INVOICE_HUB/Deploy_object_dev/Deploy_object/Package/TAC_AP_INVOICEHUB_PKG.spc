CREATE OR REPLACE PACKAGE TAC_AP_INVOICEHUB_PKG is

    type tDtinvhublog            IS TABLE OF DTINVHUB_LOG_DETAIL%rowtype;
    type tSuminvhublog           IS TABLE OF DTINVHUB_LOG_SUMMARY%rowtype;
    g_step VARCHAR2(100);
    g_log_detail_seq  NUMBER:=0;

    procedure write_log(param_msg varchar2);

 /*   procedure write_invdetail_log(pError_log    in tDtinvhublog,
                                  pErr_code     in DTINVHUB_LOG_DETAIL.ERROR_CODE%type,
                                  pMessage_err  in DTINVHUB_LOG_DETAIL.ERROR_MESSAGE%type);*/

    procedure write_invdetail_log(p_invlog      IN DTINVHUB_LOG_DETAIL%ROWTYPE,
                                  gWritelog_detail IN OUT tDtinvhublog,
                                  pErr_code     in VARCHAR2,
                                  pMessage_err  in VARCHAR2);

    procedure write_invsummary_log(gWritelog_detail IN OUT tDtinvhublog,
                                   p_current_request_id IN NUMBER,
                                   p_startprocess IN TIMESTAMP);--(pSummary_log    in tSuminvhublog);

    procedure insert_invoice(ir_invoice in ap_invoices_interface%rowtype);

    procedure insert_invoice_line(ir_invoice_line in ap_invoice_lines_interface%rowtype);

    procedure interface_ap_invoicehub
        (
            err_msg             out varchar2
           ,err_code            out varchar2
           ,i_company_code      in varchar2
           ,i_source            in varchar2 --FXTH
           ,i_interface_type    in varchar2 --2way or 3way
           ,i_file_name         in varchar2
           ,i_rerun             in varchar2
           ,i_path              in varchar2
         );

    procedure update_staging_status(i_inv_num         varchar2,
                                    i_vendor_num      VARCHAR2,
                                    i_source          varchar2,
                                    i_interface_type  varchar2,
                                    i_line_num        number := null,
                                    i_status          varchar2);
end TAC_AP_INVOICEHUB_PKG;
/
