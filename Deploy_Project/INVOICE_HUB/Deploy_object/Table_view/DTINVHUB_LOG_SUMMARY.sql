create table DTINVHUB_LOG_SUMMARY
(
  level_type              VARCHAR2(1 BYTE),
  request_id              NUMBER,
  program_name            VARCHAR2(20 BYTE),
  process_date            DATE,
  processing_type         VARCHAR2(40 BYTE),
  record_count_completion NUMBER,
  record_count_error      NUMBER,
  processing_date         VARCHAR2(50)
);
