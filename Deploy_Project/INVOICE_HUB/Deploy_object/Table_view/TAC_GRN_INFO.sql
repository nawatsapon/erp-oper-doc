CREATE OR REPLACE TYPE "TAC_GRN_INFO"                                          AS OBJECT
(   GRN_NUMBER       Varchar2(30),
                                               Company_Code     Varchar2(10),
                                               PO_Number          Varchar2(20),
                                               PO_Line_Number   Number,
                                               GRN_STATUS        Varchar2(20),
                                               GRN_LINE_NUMBER Number,
                                               ITEM_CODE            varchar2(40),
                                               LINE_DESCRIPTION Varchar2(240),
                                               GRN_QTY               Number, 
                                               GRN_UOM              Varchar2(10),
                                               GRN_AMOUNT        Number,
                                               GRN_CURRENCY     Varchar2(15),
                                               TOTAL_GRN_AMOUNT Number,
                                               ORG_CODE            Varchar2(3),
                                               VENDOR_NUMBER  Varchar2(30),
                                               VENDOR_SITE_CODE Varchar2(15),
                                               PAYMENT_TERM    Varchar2(50),
                                               PO_Remaining_Amount Number
) 
/
