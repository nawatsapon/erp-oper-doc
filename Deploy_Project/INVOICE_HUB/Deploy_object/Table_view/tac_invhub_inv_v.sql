CREATE OR REPLACE VIEW tac_invhub_inv_v AS 
SELECT DISTINCT file_name ,interface_po_flag,interface_ap_flag,interface_type
  FROM tac_invhub_invoice_stg
  ORDER BY 1;
  
