create or replace package tac_po_invoicehub_pkg--dt_invoicehub_pkg_new2
AS
  /* DECLARE GLOBAL VARIABLE  */

      TYPE r_po_header is record (group_name varchar2(21),
                                                source          varchar2(5),
                                                vendor_num  varchar2(30),
                                                vendor_site_code varchar2(15),
                                                doc_category_code varchar2(50),
                                                INVOICE_CURRENCY_CODE varchar2(15),
                                                pr_number   varchar2(20),
                                                org_id          number,
                                                interface_header_id number,
                                                agent_id       number,
                                                interface_flag varchar2(1)

                                               -- process_code

                                                );
        TYPE r_po_line is record (interface_header_id number,
                                             interface_line_id      number,
                                             pr_number             varchar2(20),
                                             line_num                number,
                                             line_type                varchar2(25),
                                             item_id                  number,--                       varchar2(100),
                                             category_id             number,--    varchar2(100),
                                             item_description      varchar2(240),
                                             uom_code              varchar2(3),
                                             quantity                  number,
                                             unit_price               number,
                                             payment_terms       varchar2(50),
                                             ship_to_organization_code varchar2(3),
                                             ship_to_location      varchar2(60),
                                             match_option          varchar2(1),
                                             dist_code_concatinated varchar2(100),
                                             requisition_line_id   number,
                                             interface_flag varchar2(1),
                                             tax_code_id    NUMBER,
                                             note_to_vendor VARCHAR2(240),
                                             note_to_receiver VARCHAR2(240),
                                             need_by_date  DATE
                                             );
        TYPE r_po_dist is record (interface_header_id number,
                                             interface_line_id      number,
                                             interface_distribution_id number,
                                             distribution_num      number,
                                             quantity_ordered     number,
                                             charge_account_id   number,
                                             gl_encumbered_date  date,
                                             req_distribution_id     NUMBER,
                                             req_header_reference_num VARCHAR2(25),
                                             req_line_reference_num  VARCHAR2(25));


--
  TYPE t_po_header IS TABLE OF r_po_header INDEX BY BINARY_INTEGER;
  TYPE t_po_line      IS TABLE OF r_po_line  INDEX BY BINARY_INTEGER;
  TYPE t_po_dist      IS TABLE OF r_po_dist  INDEX BY BINARY_INTEGER;
--  TYPE t_grn_info IS TABLE OF r_grn_info INDEX BY BINARY_INTEGER ;
   TYPE t_log_detail IS TABLE OF apps.DTINVHUB_LOG_DETAIL%ROWTYPE INDEX BY BINARY_INTEGER;
  g_log_detail t_log_detail;
  g_log_detail_seq NUMBER :=0;
  g_po_error number;
  g_debug_flag varchar2(1);
  g_inteface_po varchar2(1) :='N';
  c_interface_2way varchar2(10) := '2WAY';
  c_inteface_3way  varchar2(10) := '3WAY';
  g_step       varchar2(100);
  g_po_line_indx  number := 0;
  g_po_dist_indx  number := 0;
  g_default_backinv_path VARCHAR2(50) := 'INVHUB_PV_INBOX';
  g_default_backinv_file VARCHAR2(50) := 'FXTH_PAY_';

  g_default_supplier_path VARCHAR2(50) := 'INVHUB_VENDOR_INBOX';
  g_default_supplier_file VARCHAR2(50) := 'FXTH_SUPPLIER_';

  g_default_inv_monitor_path VARCHAR2(50) := 'INVHUB_RESULT_INBOX';
  g_default_splunk_log    VARCHAR2(50) :='INVHUB_SPLUNK';

  c_default_buyer  varchar2(100) :='EKARAPC';

  /*
    CREATE DIRECTORY INVHUB_BACKINV_INBOX AS '/var/tmp/INVOICEHUB/OUT_BOUND/BACKINVOICE/INBOX';
    GRANT READ ON DIRECTORY INVHUB_BACKINV_INBOX TO PUBLIC;

    CREATE DIRECTORY INVHUB_BACKINV_INBOX AS '/var/tmp/INVOICEHUB/OUT_BOUND/SUPPLIER/INBOX';
    GRANT READ ON DIRECTORY INVHUB_BACKINV_INBOX TO PUBLIC;
  */
  TYPE T_tac_invhub_invoice_stg IS TABLE OF tac_invhub_invoice_stg%ROWTYPE INDEX BY PLS_INTEGER;

   PROCEDURE call_logmonitor(p_program IN varchar2,p_conc_request_id IN NUMBER);

  FUNCTION f_invhub_get_pr_amount(p_pr_number IN VARCHAR2,
                                    p_pr_line   IN NUMBER) RETURN NUMBER;
  PROCEDURE write_output (p_request_id IN NUMBER,p_program_name IN VARCHAR2);
  PROCEDURE PO_INTERFACE(errbuf       OUT VARCHAR2,
                         errcode      OUT VARCHAR2,
                         p_comp_code IN VARCHAR2,
                         p_source     IN VARCHAR2,
                         p_interface_type IN VARCHAR2,
                         p_filename   IN VARCHAR2,
                         p_rerun      IN VARCHAR2,
                         p_filepath   IN VARCHAR2,
                         p_debug_flag IN VARCHAR2
                         );

--      PROCEDURE  getGRNInfomation(p_grn_number in varchar2,
--                                                 p_company_code in  varchar2,
--                                                 p_po_number in varchar2,
--                                                 o_result  out varchar2,
--                                                  o_err_code out varchar2,
--                                                 o_err_mesg out varchar2
--                                                 o_grn_infomation out APPS.t_grn_info,
--                                                );
  PROCEDURE ReturnPaidInvoice (errbuf       OUT VARCHAR2,
                               errcode      OUT VARCHAR2,
                               p_replace    IN  VARCHAR2,
                               p_filepath   IN VARCHAR2,
                               p_date       IN DATE,
                               p_data_source IN VARCHAR2,
                               p_debug_flag IN VARCHAR2
                              );

  PROCEDURE SupplierUpdate    (errbuf       OUT VARCHAR2,
                               errcode      OUT VARCHAR2,
                               p_replace    IN  VARCHAR2,
                               p_filepath   IN VARCHAR2,
                               p_date       IN DATE,
                                p_first_load IN VARCHAR2,
                               p_debug_flag IN VARCHAR2
                              );


   PROCEDURE readdata (p_filename IN VARCHAR2);
   PROCEDURE LOG_MONITOR( errbuf       OUT VARCHAR2,
                          errcode      OUT VARCHAR2,
                          p_program_name  in varchar2,
                          p_concurrent_id in number,
                        -- p_folder_name   in varchar2,
                         p_debug_flag IN VARCHAR2);
END tac_po_invoicehub_pkg;