create or replace PACKAGE BODY tac_po_invoicehub_pkg-- dt_invoicehub_pkg_new2
AS

/******************************************************************************************************************************************************
  Package Name        :DT_INVOICEHUB_PKG
  Description         :This package is modified for moving pr data from source concession to destination concession

  Revisions:
  Ver             Date                Author                      Description
  ---------       ---------           ---------------             ----------------------------------
  1.0             25/07/2016        Suttisak Uamornlert         Package creation
 *****************************************************************************************************************************************************/
    /*-------------------------------*/
    /*  parameter p_debug_type       */
    /*  log -- FND_FILE.LOG          */
    /*  output -- FND_FILE.OUTPUT    */
    /*-------------------------------*/
    PROCEDURE DEBUG_MSG(p_debug_type IN VARCHAR2,
                        p_msg        IN VARCHAR2) IS
    BEGIN
       IF p_debug_type='LOG' AND g_debug_flag='Y' THEN
          fnd_file.put_line(fnd_file.LOG,p_msg);
       ELSIF p_debug_type='OUTPUT'  THEN
          fnd_file.put_line(fnd_file.OUTPUT,p_msg);
       END IF;

    END DEBUG_MSG;

    PROCEDURE call_logmonitor(p_program IN varchar2,p_conc_request_id IN NUMBER) IS
      l_poimport_req_id NUMBER;
      
      l_finished BOOLEAN;
      l_phase       varchar2 (100);
      l_status      varchar2 (100);
      l_dev_phase   varchar2 (100);
      l_dev_status  varchar2 (100);
      l_message     varchar2 (100);
      l_check_data  varchar2(1);
      
    BEGIN

        l_poimport_req_id := fnd_request.submit_request (application  => 'SQLAP',
                                                        program       =>'DTINVHUB006',
                                                        sub_request   => FALSE,
                                                        argument1    => p_program,--'DTINVHUB002',
                                                        argument2     => p_conc_request_id,
                                                        argument3    => 'N');
       COMMIT;
       
       LOOP
       l_finished := fnd_concurrent.wait_for_request ( request_id => l_poimport_req_id
                                                 ,interval   => 5
                                                 ,max_wait   => 0
                                                 ,phase      => l_phase
                                                 ,status     => l_status
                                                 ,dev_phase  => l_dev_phase
                                                 ,dev_status => l_dev_status
                                                 ,message    => l_message );

       EXIT WHEN  l_phase='Completed';
       DBMS_LOCK.SLEEP(10);
       END loop;
       
    END;

    FUNCTION f_invhub_get_pr_amount(p_pr_number IN VARCHAR2,
                                    p_pr_line   IN NUMBER) RETURN NUMBER IS
       l_amount NUMBER;
    BEGIN

        SELECT (nvl(prl.unit_price,0)*nvl(prl.quantity,0))
          INTO l_amount
          FROM po_requisition_headers_all prh
              ,po_requisition_lines_all prl
         WHERE prh.requisition_header_id = prl.requisition_header_id
           AND prh.segment1=p_pr_number
           AND prl.line_num =p_pr_line;
         RETURN(l_amount);
         EXCEPTION WHEN no_data_found THEN
           return(0);
    END;


    FUNCTION f_get_vendor_contact(p_pr_number IN VARCHAR2) RETURN NUMBER IS
        l_vendor_contact NUMBER;
    BEGIN
        g_step := 'f_get_vendor_contact';
        debug_msg('LOG',g_step ||' p_pr_number ='||p_pr_number);
        SELECT DISTINCT prl.vendor_contact_id
          INTO l_vendor_contact
          FROM po_requisition_headers_all prh,po_requisition_lines_all prl
        WHERE prh.segment1 = p_pr_number
        AND prh.requisition_header_id=prl.requisition_header_id;
        RETURN(l_vendor_contact);
        EXCEPTION WHEN OTHERS THEN
          debug_msg('LOG','error :'||SQLERRM);
          RETURN(NULL);
    END;
    PROCEDURE get_requester_info (p_req_header_reference_num IN VARCHAR2,
                                  p_req_line_reference_num   IN NUMBER,
                                  o_requester_id             IN OUT number,
                                  o_deliver_to_location      IN OUT NUMBER)
                                  IS

    BEGIN
       SELECT prl.to_person_id,prl.deliver_to_location_id
         INTO o_requester_id,o_deliver_to_location
         FROM po_requisition_headers_all prh,po_requisition_lines_all prl
        WHERE prh.segment1 = p_req_header_reference_num
          AND prl.line_num = p_req_line_reference_num
          AND prh.requisition_header_id=prl.requisition_header_id;
        EXCEPTION WHEN OTHERS THEN
          o_requester_id:=NULL;
          o_deliver_to_location:=NULL;
    END;
    PROCEDURE get_project_information ( p_req_header_reference_num IN VARCHAR2,
                                        p_req_line_reference_num   IN NUMBER,
                                        o_project_id               IN OUT po_req_distributions_all.project_id%TYPE,
                                        o_project_name             IN OUT PA_PROJECTS_ALL.segment1%TYPE,
                                        o_task_id                  IN OUT po_req_distributions_all.task_id%TYPE,
                                        o_task_name                IN OUT PA_TASKS.task_number%TYPE,
                                        o_expenditure_type         IN OUT po_req_distributions_all.expenditure_type%TYPE,
                                        o_expenditure_item_date    IN OUT po_req_distributions_all.expenditure_item_date%TYPE,
                                        o_expenditure_organization_id  IN OUT po_req_distributions_all.expenditure_organization_id%TYPE) IS
    BEGIN
       SELECT prd.project_id,
              ppa.segment1,
              prd.task_id,
              pt.task_number,
              prd.expenditure_type,
              prd.expenditure_item_date,
              prd.expenditure_organization_id
         INTO o_project_id,
              o_project_name,
              o_task_id,
              o_task_name,
              o_expenditure_type,
              o_expenditure_item_date,
              o_expenditure_organization_id
         FROM po_requisition_headers_all prh,
              po_requisition_lines_all prl,
              po_req_distributions_all prd,
              PA_PROJECTS_ALL ppa,
              PA_TASKS pt
        WHERE prh.requisition_header_id =prl.requisition_header_id
          AND prl.requisition_line_id = prd.requisition_line_id
          AND prh.segment1=p_req_header_reference_num
           AND prl.line_num =p_req_line_reference_num
           AND prd.project_id=ppa.project_id(+)
           AND prd.project_id=pt.project_id(+)
           AND prd.task_id =pt.task_id(+);
       EXCEPTION WHEN OTHERS THEN
         o_project_id := NULL;
         o_project_name :=NULL;
         o_task_id    := NULL;
         o_task_name := NULL;
         o_expenditure_type := NULL;
         o_expenditure_item_date := NULL;
         o_expenditure_organization_id := NULL;
    END;
    FUNCTION GET_PR_ORG_ID (p_pr_number in VARCHAR2) RETURN NUMBER IS
        l_org_id number;
    begin
         select org_id
            into l_org_id
          from po_requisition_headers_all
        where segment1=p_pr_number;
        return(l_org_id);
        exception when no_data_found then
          return(-1);
    end;
    PROCEDURE write_log_detail(v_startprocess IN TIMESTAMP,p_conc_request_id IN NUMBER) IS
       v_endprocess TIMESTAMP;
    BEGIN
       debug_msg('LOG','in write_log_detail');
       debug_msg('LOG','g_log_detail.count='||g_log_detail.count);
       debug_msg('LOG','g_log_detail.count='||g_log_detail.count);
       IF g_log_detail.count > 0 THEN
           forall i in g_log_detail.first ..g_log_detail.last insert into apps.DTINVHUB_LOG_DETAIL values g_log_detail(i);
       end if;

       SELECT current_timestamp
           INTO v_endprocess
           FROM dual;
       debug_msg('LOG','p_conc_request_id='||p_conc_request_id);

       INSERT INTO apps.dtinvhub_log_summary
      (level_type,request_id,program_name,process_date,processing_type,record_count_completion
       ,record_count_error,processing_date)
       SELECT 'H',request_id,'DTINVHUB002',v_startprocess,'Download', sum(DECODE(ERROR_CODE,NULL,1,0)) COMPLETE,
              sum(DECODE(ERROR_CODE,NULL,0,1)) ERROR
              ,substr(to_char(v_endprocess-v_startprocess),12,12)
        FROM apps.DTINVHUB_LOG_DETAIL WHERE request_id=p_conc_request_id
        GROUP BY 'H',request_id,'DTINVHUB002',SYSDATE,'Download';

    END;





    procedure write_pr_header_log(p_stg_po_header IN OUT r_po_header,
                                  p_stg_log_detail IN OUT t_log_detail,
                                  p_err_code      IN VARCHAR2,
                                  p_err_msg       IN VARCHAR2) is


    begin

        --if pError_log.count > 0 then
            --forall i in 1..pError_log.count insert into apps.DTINVHUB_LOG values pError_log(i);
        --end if;
       -- p_stg_log_detail.EXTEND;
        p_stg_log_detail(p_stg_log_detail.LAST).LEVEL_TYPE            := 'D';
        p_stg_log_detail(p_stg_log_detail.LAST).REQUEST_ID            := fnd_global.CONC_REQUEST_ID;
        p_stg_log_detail(p_stg_log_detail.LAST).ERROR_TYPE            := 'PO Interface';
        p_stg_log_detail(p_stg_log_detail.LAST).PR_NUMBER             := p_stg_po_header.pr_number;
        p_stg_log_detail(p_stg_log_detail.LAST).MATCH_OPTION          := NULL; --pError_log(i).MATCH_OPTION;
        p_stg_log_detail(p_stg_log_detail.LAST).VENDOR_NUMBER         := p_stg_po_header.vendor_num;--pError_log(i).VENDOR_NUMBER;
        p_stg_log_detail(p_stg_log_detail.LAST).VENDOR_SITE_NAME      := p_stg_po_header.vendor_site_code;--pError_log(i).VENDOR_SITE_NAME;
        p_stg_log_detail(p_stg_log_detail.LAST).ERROR_CODE            := p_err_code;
        p_stg_log_detail(p_stg_log_detail.LAST).ERROR_MESSAGE         := p_err_msg;

    end write_pr_header_log;




    PROCEDURE FETCH_PO_DISTRIBUTION ( p_stg_po_header IN OUT r_po_header,
                                      p_stg_po_line IN r_po_line,
                                      p_stg_po_dist IN OUT t_po_dist) IS
           cursor c_fetch_dist(p_req_line_id in number)
                                        is  select distribution_num,
                                                      req_line_quantity,
                                                      code_combination_id,
                                                      gl_encumbered_date,
                                                      distribution_id
                                              from po_req_distributions_all
                                            where requisition_line_id=p_req_line_id;
    BEGIN
        g_step :='FETCH_PO_DISTRIBUTION';
        debug_msg('LOG',g_step);
        for i_fetch_dist in c_fetch_dist(p_stg_po_line.requisition_line_id) loop
              g_po_dist_indx := g_po_dist_indx +1;
              p_stg_po_dist(g_po_dist_indx).interface_header_id := p_stg_po_line.interface_header_id;
              p_stg_po_dist(g_po_dist_indx).interface_line_id      := p_stg_po_line.interface_line_id;
              p_stg_po_dist(g_po_dist_indx).interface_distribution_id   := PO_DISTRIBUTIONS_INTERFACE_S.NEXTVAL;
              p_stg_po_dist(g_po_dist_indx).distribution_num     :=  i_fetch_dist.distribution_num;
              p_stg_po_dist(g_po_dist_indx).quantity_ordered     :=  p_stg_po_line.quantity;--.i_fetch_dist.req_line_quantity;
              p_stg_po_dist(g_po_dist_indx).charge_account_id  :=  i_fetch_dist.code_combination_id;
              p_stg_po_dist(g_po_dist_indx).gl_encumbered_date       := i_fetch_dist.gl_encumbered_date;
              p_stg_po_dist(g_po_dist_indx).req_distribution_id  :=  i_fetch_dist.distribution_id;
              p_stg_po_dist(g_po_dist_indx).req_header_reference_num :=p_stg_po_line.pr_number;
              p_stg_po_dist(g_po_dist_indx).req_line_reference_num  := p_stg_po_line.line_num;


        end loop;
        debug_msg('LOG','p_stg_po_dist'||p_stg_po_dist.count);

    END;
    PROCEDURE FETCH_PO_HEADER(p_group_name IN VARCHAR2,
                              p_po_header  IN OUT t_po_header) IS
    BEGIN
         g_step :='Fetch po data';
         debug_msg('LOG',g_step);

         select distinct group_name,
                         source,
                         vendor_num,
                         vendor_site_code,
                         doc_category_code,
                         invoice_currency_code,
                         pr_number,
                         null,
                         null,
                         null,
                         'N'
           bulk collect into p_po_header
           from tac_invhub_invoice_stg
          where upper(file_name) = upper(nvl(p_group_name,file_name))
            --upper(group_name) = upper(nvl(p_group_name,'FXTH_2W_20160801_2100'))  --'FXTH_2W_20160725_1633'
            and nvl(interface_po_flag,'N')=g_inteface_po
          order by group_name,pr_number;
          debug_msg('LOG','Total record inserted in collection='||sql%rowcount);
    END;


    PROCEDURE  FETCH_PO_LINE(p_stg_po_header IN OUT r_po_header,
                             p_stg_po_line   IN OUT t_po_line,
                             p_stg_po_dist   IN OUT t_po_dist) is
         cursor fetch_po_line(p_group_name in varchar2,p_pr_number in varchar2) is
           select  p_stg_po_header.interface_header_id interface_header_id,
                     tis.pr_number pr_number,
                     tis.pr_line_number line_num,
                     'Expenses' line_type,
                     tis.terms_name payment_terms,
                     tis.match_option,
                     'N' interface_flag,
                     tis.invoice_num invoice_num,
                     tis.line_number invoice_line,
                     tis.line_amount

            --bulk collect into p_stg_po_line
            from tac_invhub_invoice_stg tis
          where tis.group_name = p_group_name--p_stg_po_header.group_name
              and nvl(tis.interface_po_flag,'N') = g_inteface_po
              and tis.pr_number = p_pr_number--p_stg_po_header.pr_number
            order by tis.group_name,tis.pr_number,tis.pr_line_number;
         l_requisition_line_id number;
         l_check VARCHAR2(1);
         l_check_dup_pr NUMBER;
         l_starttime TIMESTAMP;
         l_endtime   TIMESTAMP;
    BEGIN
      
          g_step :='FETCH_PO_LINE';
          debug_msg('LOG',g_step);

           debug_msg('LOG','p_stg_po_header.group_name='||p_stg_po_header.group_name);
           debug_msg('LOG','p_stg_po_header.pr_number='||p_stg_po_header.pr_number);

         for i_fetch_po_line in fetch_po_line(p_stg_po_header.group_name,p_stg_po_header.pr_number) loop
            --  p_stg_po_line.extend;
              BEGIN
                SELECT current_timestamp
                  INTO l_starttime
                  FROM dual;
               
               SELECT COUNT(*)
                 INTO l_check_dup_pr
                 from tac_invhub_invoice_stg tis
                where tis.group_name = p_stg_po_header.group_name
                  and nvl(tis.interface_po_flag,'N') = 'Y'
                  and tis.pr_number = i_fetch_po_line.pr_number
                  AND tis.pr_line_number = i_fetch_po_line.line_num
                 GROUP BY tis.pr_number ,
                          tis.pr_line_number;
                
                SELECT current_timestamp
                  INTO l_endtime
                  FROM dual;           
                 debug_msg('LOG','l_check_dup_pr='||l_check_dup_pr);
                IF l_check_dup_pr > 0 THEN
                BEGIN
                --    EXCEPTION WHEN no_data_found THEN
                  -- g_po_line_indx :=g_po_line_indx-1;
                   g_log_detail_seq :=g_log_detail_seq+1;
                   g_log_detail(g_log_detail_seq).level_type :='D';
                   g_log_detail(g_log_detail_seq).request_id := fnd_global.CONC_REQUEST_ID;
                   g_log_detail(g_log_detail_seq).error_type := 'PO Interface';
                   g_log_detail(g_log_detail_seq).ap_invoice := i_fetch_po_line.invoice_num;

                   g_log_detail(g_log_detail_seq).ap_invoie_line := i_fetch_po_line.invoice_line;
                   g_log_detail(g_log_detail_seq).pr_number  := i_fetch_po_line.pr_number;
                   g_log_detail(g_log_detail_seq).pr_line_number :=i_fetch_po_line.line_num;
                   g_log_detail(g_log_detail_seq).match_option := i_fetch_po_line.match_option;
                   g_log_detail(g_log_detail_seq).vendor_number := p_stg_po_header.vendor_num;
                   g_log_detail(g_log_detail_seq).vendor_site_name := p_stg_po_header.vendor_site_code;
                   g_log_detail(g_log_detail_seq).payment_term := i_fetch_po_line.payment_terms;
                   g_log_detail(g_log_detail_seq).ERROR_CODE := 'INVHUB002-007';  --Error INVHUB002-007: Duplicate PR number
                   g_log_detail(g_log_detail_seq).error_message := 'Duplicate PR number';
                   g_log_detail(g_log_detail_seq).processing_date := l_starttime;
                   g_log_detail(g_log_detail_seq).processing_time := substr(to_char(l_endtime-l_starttime),12,12); 
                   p_stg_po_header.interface_flag :='E';
                   


                END;
                END IF;
                 EXCEPTION WHEN no_data_found THEN
                   NULL;
                END;
              -- validate duplicate pr
           /*   BEGIN
                 SELECT DISTINCT 'Y'
                   INTO l_check
                   FROM po_requisition_headers_all
                  WHERE segment1=p_stg_po_header.pr_number;

              END;
             */

              -- Validate Payment term
              
              --joe
              BEGIN
                SELECT DISTINCT 'Y'
                  INTO l_check
                  FROM ap_terms
                 WHERE NAME = i_fetch_po_line.payment_terms;
               
                        
                 EXCEPTION WHEN no_data_found THEN
                  
                  -- g_po_line_indx :=g_po_line_indx-1;
                   g_log_detail_seq :=g_log_detail_seq+1;
                   g_log_detail(g_log_detail_seq).level_type :='D';
                   g_log_detail(g_log_detail_seq).request_id := fnd_global.CONC_REQUEST_ID;
                   g_log_detail(g_log_detail_seq).error_type := 'PO Interface';
                   g_log_detail(g_log_detail_seq).ap_invoice := i_fetch_po_line.invoice_num;
                   g_log_detail(g_log_detail_seq).ap_invoie_line := i_fetch_po_line.invoice_line;
                   g_log_detail(g_log_detail_seq).pr_number  := i_fetch_po_line.pr_number;
                   g_log_detail(g_log_detail_seq).pr_line_number :=i_fetch_po_line.line_num;
                   g_log_detail(g_log_detail_seq).match_option := i_fetch_po_line.match_option;
                   g_log_detail(g_log_detail_seq).vendor_number := p_stg_po_header.vendor_num;
                   g_log_detail(g_log_detail_seq).vendor_site_name := p_stg_po_header.vendor_site_code;
                   g_log_detail(g_log_detail_seq).payment_term := i_fetch_po_line.payment_terms;
                   g_log_detail(g_log_detail_seq).ERROR_CODE := 'INVHUB002-013 ';
                   g_log_detail(g_log_detail_seq).error_message := 'Invalid Term Name';
                   g_log_detail(g_log_detail_seq).processing_date := l_starttime;
                   
                   SELECT current_timestamp
                     INTO l_endtime
                     FROM dual; 
                   g_log_detail(g_log_detail_seq).processing_time := substr(to_char(l_endtime-l_starttime),12,12); 
                   p_stg_po_header.interface_flag :='E';
              END;
              ------------------------
              -- Validate Supplier
              BEGIN

                  SELECT DISTINCT 'Y'
                    INTO l_check
                    FROM po_vendors
                   WHERE segment1=p_stg_po_header.vendor_num;
                EXCEPTION WHEN no_data_found THEN
                  -- g_po_line_indx :=g_po_line_indx-1;
                   g_log_detail_seq :=g_log_detail_seq+1;
                   g_log_detail(g_log_detail_seq).level_type :='D';
                   g_log_detail(g_log_detail_seq).request_id := fnd_global.CONC_REQUEST_ID;
                   g_log_detail(g_log_detail_seq).error_type := 'PO Interface';
                   g_log_detail(g_log_detail_seq).ap_invoice := i_fetch_po_line.invoice_num;
                   g_log_detail(g_log_detail_seq).ap_invoie_line := i_fetch_po_line.invoice_line;
                   g_log_detail(g_log_detail_seq).pr_number  := i_fetch_po_line.pr_number;
                   g_log_detail(g_log_detail_seq).pr_line_number :=i_fetch_po_line.line_num;
                   g_log_detail(g_log_detail_seq).match_option := i_fetch_po_line.match_option;
                   g_log_detail(g_log_detail_seq).vendor_number := p_stg_po_header.vendor_num;
                   g_log_detail(g_log_detail_seq).vendor_site_name := p_stg_po_header.vendor_site_code;
                   g_log_detail(g_log_detail_seq).payment_term := i_fetch_po_line.payment_terms;
                   g_log_detail(g_log_detail_seq).ERROR_CODE := 'INVHUB002-001';
                   g_log_detail(g_log_detail_seq).error_message := 'Invalid supplier';
                   g_log_detail(g_log_detail_seq).processing_date := l_starttime;
                   SELECT current_timestamp
                     INTO l_endtime
                     FROM dual; 
                   g_log_detail(g_log_detail_seq).processing_time := substr(to_char(l_endtime-l_starttime),12,12); 
                   p_stg_po_header.interface_flag :='E';
              END;
              ------------------------
              -- Validate Supplier site
               BEGIN


                  SELECT DISTINCT 'Y'
                    INTO l_check
                    FROM po_vendors pv,
                         po_vendor_sites_all pvs
                   WHERE pv.vendor_id= pvs.vendor_id
                     AND pvs.org_id=p_stg_po_header.org_id
                     AND pvs.vendor_site_code = p_stg_po_header.vendor_site_code
                     AND pv.segment1=p_stg_po_header.vendor_num;
                 EXCEPTION WHEN no_data_found THEN
                  -- g_po_line_indx :=g_po_line_indx-1;
                   g_log_detail_seq :=g_log_detail_seq+1;
                   g_log_detail(g_log_detail_seq).level_type :='D';
                   g_log_detail(g_log_detail_seq).request_id := fnd_global.CONC_REQUEST_ID;
                   g_log_detail(g_log_detail_seq).error_type := 'PO Interface';
                   g_log_detail(g_log_detail_seq).ap_invoice := i_fetch_po_line.invoice_num;
                   g_log_detail(g_log_detail_seq).ap_invoie_line := i_fetch_po_line.invoice_line;
                   g_log_detail(g_log_detail_seq).pr_number  := i_fetch_po_line.pr_number;
                   g_log_detail(g_log_detail_seq).pr_line_number :=i_fetch_po_line.line_num;
                   g_log_detail(g_log_detail_seq).match_option := i_fetch_po_line.match_option;
                   g_log_detail(g_log_detail_seq).vendor_number := p_stg_po_header.vendor_num;
                   g_log_detail(g_log_detail_seq).vendor_site_name := p_stg_po_header.vendor_site_code;
                   g_log_detail(g_log_detail_seq).payment_term := i_fetch_po_line.payment_terms;
                   g_log_detail(g_log_detail_seq).ERROR_CODE := 'INVHUB002-002';
                   g_log_detail(g_log_detail_seq).error_message := 'Invalid Supplier Site';
                    g_log_detail(g_log_detail_seq).processing_date := l_starttime;
                   SELECT current_timestamp
                     INTO l_endtime
                     FROM dual; 
                   g_log_detail(g_log_detail_seq).processing_time := substr(to_char(l_endtime-l_starttime),12,12); 
                   
                   p_stg_po_header.interface_flag :='E';
              END;
              ------------------------
               -- Validate qty
                g_step :='Validate po line qty';
               debug_msg('LOG',g_step);
              BEGIN
                 IF i_fetch_po_line.line_amount <= 0 THEN --joe
                  -- g_po_line_indx :=g_po_line_indx-1;
                   g_log_detail_seq :=g_log_detail_seq+1;
                   g_log_detail(g_log_detail_seq).level_type :='D';
                   g_log_detail(g_log_detail_seq).request_id := fnd_global.CONC_REQUEST_ID;
                   g_log_detail(g_log_detail_seq).error_type := 'PO Interface';
                   g_log_detail(g_log_detail_seq).ap_invoice := i_fetch_po_line.invoice_num;
                   g_log_detail(g_log_detail_seq).ap_invoie_line := i_fetch_po_line.invoice_line;
                   g_log_detail(g_log_detail_seq).pr_number  := i_fetch_po_line.pr_number;
                   g_log_detail(g_log_detail_seq).pr_line_number :=i_fetch_po_line.line_num;
                   g_log_detail(g_log_detail_seq).match_option := i_fetch_po_line.match_option;
                   g_log_detail(g_log_detail_seq).vendor_number := p_stg_po_header.vendor_num;
                   g_log_detail(g_log_detail_seq).vendor_site_name := p_stg_po_header.vendor_site_code;
                   g_log_detail(g_log_detail_seq).payment_term := i_fetch_po_line.payment_terms;
                   g_log_detail(g_log_detail_seq).ERROR_CODE := 'INVHUB002-014';
                   g_log_detail(g_log_detail_seq).error_message := 'The Quantity must be greater than 0';
                    g_log_detail(g_log_detail_seq).processing_date := l_starttime;
                   SELECT current_timestamp
                     INTO l_endtime
                     FROM dual; 
                   g_log_detail(g_log_detail_seq).processing_time := substr(to_char(l_endtime-l_starttime),12,12); 
                   
                   p_stg_po_header.interface_flag :='E';
                 END IF;
              END;
               g_step :='FETCH_PO_LINE - get pr information';
               debug_msg('LOG',g_step);
               -- get information  from pr_line
               BEGIN
                    g_step :='FETCH_PO_LINE - get pr information';
                    debug_msg('LOG',g_step);
                    g_po_line_indx := g_po_line_indx +1;
                    select  PO_LINES_INTERFACE_S.NEXTVAL,
                              prl.item_id,
                              prl.category_id,
                              prl.item_description,
                              prl.unit_meas_lookup_code,
                              (i_fetch_po_line.line_amount/nvl(prl.currency_unit_price,prl.unit_price)) quantity,--prl.quantity,
                              nvl(prl.currency_unit_price,prl.unit_price),
                              prl.destination_organization_id,
                              prl.deliver_to_location_id,
                              prl.requisition_line_id,
                              prl.tax_code_id,
                             -- prl.justification,
                              prl.note_to_agent,
                              prl.note_to_receiver,
                              prl.need_by_date--,
                             -- suggested_vendor_contact,sugg
                       into  p_stg_po_line(g_po_line_indx).interface_line_id,
                              p_stg_po_line(g_po_line_indx).item_id,
                              p_stg_po_line(g_po_line_indx).category_id,
                              p_stg_po_line(g_po_line_indx).item_description,
                              p_stg_po_line(g_po_line_indx).uom_code,
                              p_stg_po_line(g_po_line_indx).quantity,
                              p_stg_po_line(g_po_line_indx).unit_price,
                              p_stg_po_line(g_po_line_indx).ship_to_organization_code,
                              p_stg_po_line(g_po_line_indx).ship_to_location,
                              p_stg_po_line(g_po_line_indx).requisition_line_id,
                              p_stg_po_line(g_po_line_indx).tax_code_id,
                              p_stg_po_line(g_po_line_indx).note_to_vendor,
                              p_stg_po_line(g_po_line_indx).note_to_receiver,
                              p_stg_po_line(g_po_line_indx).need_by_date
                       from po_requisition_headers_all prh,
                              po_requisition_lines_all prl
                      where prh.requisition_header_id = prl.requisition_header_id
                          and prh.segment1 =  i_fetch_po_line.pr_number
                          and prl.line_num   =  i_fetch_po_line.line_num;


                       p_stg_po_line(g_po_line_indx).interface_header_id := p_stg_po_header.interface_header_id;
                       p_stg_po_line(g_po_line_indx).pr_number := i_fetch_po_line.pr_number;
                       p_stg_po_line(g_po_line_indx).line_num := i_fetch_po_line.line_num;
                       p_stg_po_line(g_po_line_indx).line_type := i_fetch_po_line.line_type;
                       p_stg_po_line(g_po_line_indx).payment_terms := i_fetch_po_line.payment_terms;
                       p_stg_po_line(g_po_line_indx).match_option := substr(i_fetch_po_line.match_option,1,1);
                       p_stg_po_line(g_po_line_indx).interface_flag := i_fetch_po_line.interface_flag;

                           FETCH_PO_DISTRIBUTION ( p_stg_po_header ,
                                        p_stg_po_line(g_po_line_indx),
                                                        p_stg_po_dist
                                                       );
                       IF p_stg_po_header.interface_flag !='E' THEN
                       BEGIN
                          --g_po_line_indx :=g_po_line_indx-1;
                          /*g_log_detail_seq :=g_log_detail_seq+1;
                          g_log_detail(g_log_detail_seq).level_type :='D';
                          g_log_detail(g_log_detail_seq).request_id := fnd_global.CONC_REQUEST_ID;
                          g_log_detail(g_log_detail_seq).error_type := 'PO Interface';
                          g_log_detail(g_log_detail_seq).ap_invoice := i_fetch_po_line.invoice_num;
                          g_log_detail(g_log_detail_seq).ap_invoie_line := i_fetch_po_line.invoice_line;
                          g_log_detail(g_log_detail_seq).pr_number  := i_fetch_po_line.pr_number;
                          g_log_detail(g_log_detail_seq).pr_line_number :=i_fetch_po_line.line_num;
                          g_log_detail(g_log_detail_seq).match_option := i_fetch_po_line.match_option;
                          g_log_detail(g_log_detail_seq).vendor_number := p_stg_po_header.vendor_num;
                          g_log_detail(g_log_detail_seq).vendor_site_name := p_stg_po_header.vendor_site_code;
                          g_log_detail(g_log_detail_seq).payment_term := i_fetch_po_line.payment_terms;
                          g_log_detail(g_log_detail_seq).ERROR_CODE := NULL;
                          g_log_detail(g_log_detail_seq).error_message := NULL;
                          g_log_detail(g_log_detail_seq).processing_date := l_starttime;
                         
                          SELECT current_timestamp
                            INTO l_endtime
                            FROM dual; 
                          g_log_detail(g_log_detail_seq).processing_time := substr(to_char(l_endtime-l_starttime),12,12); 
                          */

                          BEGIN
                             UPDATE tac_invhub_invoice_stg tis
                                SET interface_po_flag='P'
                              where tis.group_name = p_stg_po_header.group_name
                                and nvl(tis.interface_po_flag,'N') = 'N'
                                and tis.pr_number = i_fetch_po_line.pr_number
                                AND tis.pr_line_number = i_fetch_po_line.line_num
                                AND tis.invoice_num = i_fetch_po_line.invoice_num;
                          END;

                       END;
                       END IF;
                      EXCEPTION WHEN  no_data_found THEN
                         g_po_line_indx :=g_po_line_indx-1;
                         g_log_detail_seq :=g_log_detail_seq+1;
                         g_log_detail(g_log_detail_seq).level_type :='D';
                         g_log_detail(g_log_detail_seq).request_id := fnd_global.CONC_REQUEST_ID;
                         g_log_detail(g_log_detail_seq).error_type := 'PO Interface';
                         g_log_detail(g_log_detail_seq).ap_invoice := i_fetch_po_line.invoice_num;
                         g_log_detail(g_log_detail_seq).ap_invoie_line := i_fetch_po_line.invoice_line;
                         g_log_detail(g_log_detail_seq).pr_number  := i_fetch_po_line.pr_number;
                         g_log_detail(g_log_detail_seq).pr_line_number :=i_fetch_po_line.line_num;
                         g_log_detail(g_log_detail_seq).match_option := i_fetch_po_line.match_option;
                         g_log_detail(g_log_detail_seq).vendor_number := p_stg_po_header.vendor_num;
                         g_log_detail(g_log_detail_seq).vendor_site_name := p_stg_po_header.vendor_site_code;
                         g_log_detail(g_log_detail_seq).payment_term := i_fetch_po_line.payment_terms;
                         g_log_detail(g_log_detail_seq).ERROR_CODE := 'INVHUB002-008';
                         g_log_detail(g_log_detail_seq).error_message := 'Invalid PR Number';
                          g_log_detail(g_log_detail_seq).processing_date := l_starttime;
                        
                         SELECT current_timestamp
                           INTO l_endtime
                           FROM dual; 
                           g_log_detail(g_log_detail_seq).processing_time := substr(to_char(l_endtime-l_starttime),12,12); 
                   
                         p_stg_po_header.interface_flag :='E';


               end;







         end loop;


    END;


    PROCEDURE INSERT_PO_HEADER_INT(stg_po_header IN OUT t_po_header,
                                   p_conc_request_id IN NUMBER) IS
        l_vendor_contact NUMBER;
    BEGIN
          g_step :='INSERT_PO_HEADER_INT : '||stg_po_header.count;
          debug_msg('LOG',g_step);
          if stg_po_header.count >0 then
              for i in 1.. stg_po_header.count loop
              BEGIN
                   debug_msg('LOG','stg_po_header(i).interface_flag='||stg_po_header(i).interface_flag);
                   if stg_po_header(i).interface_flag ='N' THEN
                      l_vendor_contact := f_get_vendor_contact(stg_po_header(i).pr_number);
                      insert into  PO_HEADERS_INTERFACE
                                     (interface_header_id,
                                      batch_id,
                                      interface_source_code,
                                      ---process_code,
                                      action,
                                     -- group_code,
                                      org_id,
                                      document_type_code,
                                      currency_code,
                                      agent_id,
                                      vendor_num,
                                      vendor_site_code,
                                      approval_status,
                                      VENDOR_CONTACT_id

                                      )
                             values
                                    (stg_po_header(i).interface_header_id,
                                     p_conc_request_id,
                                     --stg_po_header(i).batch_id,
                                     stg_po_header(i).source,
                                     --stg_po_header(i).process_code,
                                     --stg_po_header(i).action,
                                     'ORIGINAL',
                                     --stg_po_header(i).group_code,
                                     stg_po_header(i).org_id,
                                     'STANDARD',
                                     stg_po_header(i).INVOICE_CURRENCY_CODE,
                                     stg_po_header(i).agent_id,
                                     stg_po_header(i).vendor_num,
                                     stg_po_header(i).vendor_site_code,
                                     'APPROVED',
                                     l_vendor_contact); --stg_po_header(i).approval_status);
                         stg_po_header(i).interface_flag := 'Y';
                   ELSE
                      g_step :='in update error to tac_invhub_invoice_stg';
                      debug_msg('LOG',g_step);
                      debug_msg('LOG','stg_po_header(i).group_name='||stg_po_header(i).group_name);
                      debug_msg('LOG','stg_po_header(i).pr_number='||stg_po_header(i).pr_number);
                      debug_msg('LOG','stg_po_header(i).source='||stg_po_header(i).source);

                      UPDATE tac_invhub_invoice_stg a
                         SET a.interface_po_flag ='E'
                       WHERE a.group_name = stg_po_header(i).group_name
                         AND a.pr_number  = stg_po_header(i).pr_number
                         AND a.source     = stg_po_header(i).source
                         AND a.interface_po_flag = 'N';



                   end if;
              end;
              end loop;
          end if;
    END;

    PROCEDURE INSERT_PO_LINE_INT(stg_po_line IN OUT t_po_line) IS
    BEGIN
          g_step := 'INSERT_PO_LINE_INT : '||stg_po_line.count;
          debug_msg('LOG',g_step);
          if stg_po_line.count >0 then
              for i in 1.. stg_po_line.count loop
              begin
                   if stg_po_line(i).interface_flag ='N' THEN

                      insert into po_lines_interface
                                     (interface_header_id,
                                        interface_line_id,
                                        line_num,
                                        line_type,
                                        item_id,
                                        category_id,
                                        item_description,
                                        uom_code,
                                        quantity,
                                        unit_price,
                                        payment_terms,
                                        ship_to_organization_id,
                                        ship_to_location_id,
                                        promised_date,
                                        requisition_line_id,
                                        INSPECTION_REQUIRED_FLAG,
                                        RECEIPT_REQUIRED_FLAG,
                                        accrue_on_receipt_flag,
                                      --  receiving_routing
                                       -- match_option
                                         tax_code_id,
                                         taxable_flag,
                                         note_to_vendor,
                                         note_to_receiver,
                                         need_by_date
                                        )
                            values (stg_po_line(i).interface_header_id,
                                        stg_po_line(i).interface_line_id,
                                        stg_po_line(i).line_num,
                                        stg_po_line(i).line_type,
                                        stg_po_line(i).item_id,
                                        stg_po_line(i).category_id,
                                        stg_po_line(i).item_description,
                                        stg_po_line(i).uom_code,
                                        stg_po_line(i).quantity,
                                        stg_po_line(i).unit_price,
                                        stg_po_line(i).payment_terms,
                                        stg_po_line(i).ship_to_organization_code,
                                        stg_po_line(i).ship_to_location,
                                        sysdate,
                                        stg_po_line(i).requisition_line_id,
                                        'N',
                                        'N',
                                        'N',--,
                                        --'2'
                                        stg_po_line(i).tax_code_id,
                                        decode(stg_po_line(i).tax_code_id,NULL,NULL,'Y'),
                                        NULL,--stg_po_line(i).note_to_vendor,
                                        stg_po_line(i).note_to_receiver,
                                        stg_po_line(i).need_by_date
                                        );
                         stg_po_line(i).interface_flag := 'Y';
                   end if;
              end;
              end loop;
          end if;
    END;


    PROCEDURE INSERT_PO_DIST_INT(stg_po_dist IN OUT t_po_dist) IS
        l_project_id       po_req_distributions_all.project_id%TYPE;
        l_project_name     pa_projects_all.segment1%TYPE;
        l_task_id          po_req_distributions_all.task_id%TYPE;
        l_task_name        pa_tasks.task_number%TYPE;
        l_expenditure_type po_req_distributions_all.expenditure_type%TYPE;
        l_expenditure_item_date  po_req_distributions_all.expenditure_item_date%TYPE;
        l_expenditure_organization_id  po_req_distributions_all.expenditure_organization_id%TYPE;
        l_deliver_to NUMBER;
        l_deliver_location NUMBER;
    BEGIN
          g_step := 'INSERT_PO_DIST_INT : '||stg_po_dist.count;
          debug_msg('LOG',g_step);
          FOR i IN 1 .. stg_po_dist.count LOOP
            debug_msg('LOG','stg_po_dist(i).interface_header_id ='||stg_po_dist(i).interface_header_id);
          END LOOP;
          if stg_po_dist.count >0 then
              FOR i in 1.. stg_po_dist.count LOOP

                     get_project_information ( stg_po_dist(i).req_header_reference_num,
                                               stg_po_dist(i).req_line_reference_num,
                                               l_project_id,
                                               l_project_name,
                                               l_task_id,
                                               l_task_name,
                                               l_expenditure_type,
                                               l_expenditure_item_date,
                                               l_expenditure_organization_id);

                      get_requester_info (stg_po_dist(i).req_header_reference_num,
                                          stg_po_dist(i).req_line_reference_num,
                                          l_deliver_to,
                                          l_deliver_location);

                      insert into po_distributions_interface a
                                     (interface_header_id,
                                       interface_line_id,
                                       interface_distribution_id,
                                       distribution_num,
                                       quantity_ordered,
                                       charge_account_id,
                                       gl_encumbered_date,
                                       req_distribution_id,
                                       req_header_reference_num,
                                       req_line_reference_num,
                                       project_id,
                                       project,
                                       task_id,
                                       task,
                                       expenditure_type,
                                       expenditure_item_date,
                                       expenditure_organization_id,
                                       deliver_to_person_id,
                                       deliver_to_location_id
                                       )
                           values (stg_po_dist(i).interface_header_id,
                                       stg_po_dist(i).interface_line_id,
                                       stg_po_dist(i).interface_distribution_id,
                                       stg_po_dist(i).distribution_num,
                                       stg_po_dist(i).quantity_ordered,
                                       stg_po_dist(i).charge_account_id,
                                       SYSDATE,  --stg_po_dist(i).gl_encumbered_date,
                                       stg_po_dist(i).req_distribution_id,
                                       stg_po_dist(i).req_header_reference_num,
                                       stg_po_dist(i).req_line_reference_num,
                                       l_project_id,
                                       l_project_name,
                                       l_task_id,
                                       l_task_name,
                                       l_expenditure_type,
                                       l_expenditure_item_date,
                                       l_expenditure_organization_id,
                                       l_deliver_to,
                                       l_deliver_location);
               END LOOP;


          end if;
    END;

    FUNCTION get_default_buyer return number is
         l_default_buyer number;
    begin
          SELECT papf.person_id
              INTO l_default_buyer
            FROM po_agents poagenteo,
                      per_all_people_f papf,
                      po_ship_to_loc_org_v psl,
                      mtl_categories_kfv mkfv,
                      fnd_user fu
         WHERE  fu.employee_id=papf.person_id
              AND poagenteo.agent_id = papf.person_id
              AND  poagenteo.category_id = mkfv.category_id(+)
              AND (  papf.employee_number IS NOT NULL
                         OR papf.npw_number IS NOT NULL
                     )
              AND  TRUNC (SYSDATE) BETWEEN papf.effective_start_date
              AND papf.effective_end_date
              AND DECODE (hr_general.get_xbg_profile,
                                   'Y', papf.business_group_id,
                                   hr_general.get_business_group_id
                                  ) = papf.business_group_id
              AND poagenteo.location_id = psl.location_id(+)
              AND fu.user_name = c_default_buyer;

             RETURN(l_default_buyer);
             exception when no_data_found then
             return(-1);
    end;

    PROCEDURE validate_po_header_int (p_stg_po_header in out r_po_header,
                                      p_stg_log_detail IN OUT t_log_detail
                                     ) is-- (stg_po_header IN OUT t_po_header) IS
    BEGIN
         g_step :='validate_po_header_int';
        -- FOR I IN 1 .. stg_po_header.COUNT LOOP
        --      stg_po_header(i).interface_header_id  := PO_HEADERS_INTERFACE_S.NEXTVAL;
        --      stg_po_header(i).org_id := GET_PR_ORG_ID(stg_po_header(i).pr_number);
         --END LOOP;
     /*    group_name,
                         source,
                         vendor_num,
                         vendor_site_code,
                         doc_category_code,
                         invoice_currency_code,
                         pr_number,*/
         -- validate


         --Error INVHUB002-001: Invalid supplier
        -- BEGIN
        --   SELECT 1 FROM dual;
        --    EXCEPTION WHEN OTHERS THEN
          --    write_pr_header_log(p_stg_po_header,
          --                        p_stg_log_detail,
          --                        NULL,-- p_err_code,
          --                        null);--p_err_msg);
       --  END;
         --Error INVHUB002-002: Invalid Supplier Site
         --Error INVHUB002-007: Duplicate PR number
         --Error INVHUB002-007: Duplicate PR  line number
         --Error INVHUB002-008: Invalid PR Number
         --Error INVHUB002-012: Invalid Org Code

         ------------
         p_stg_po_header.interface_header_id := PO_HEADERS_INTERFACE_S.NEXTVAL;
         p_stg_po_header.org_id := GET_PR_ORG_ID(p_stg_po_header.pr_number);
         p_stg_po_header.agent_id := get_default_buyer;
    END;
    PROCEDURE update_po_to_stg(p_po_import_id IN NUMBER
                              ,p_filename IN VARCHAR2
                              ,l_starttime TIMESTAMP
                              ,p_conc_request_id IN NUMBER
                              ) IS

        CURSOR c1(p_po_import_id IN NUMBER)
                  IS SELECT poh.segment1 po_number,
                            pol.line_num line_number,
                            pd.req_header_reference_num,
                            pd.req_line_reference_num,
                            poh.po_header_id,
                            pol.po_line_id,
                            pll.line_location_id

                       FROM po_headers_all poh,
                            po_lines_all pol,
                            po_line_locations_all pll,
                            po_distributions_all pd
                      WHERE poh.po_header_id=pol.po_header_id
                        AND pol.po_header_id=pll.po_header_id
                        AND pol.po_line_id=pll.po_line_id
                        AND pll.po_header_id=pd.po_header_id
                        AND pll.po_line_id=pd.po_line_id
                        AND pll.line_location_id=pd.line_location_id
                        AND poh.request_id=p_po_import_id;
                        
      CURSOR c_complete_po (p_filename IN VARCHAR2,
                            p_ref_pr_num IN VARCHAR2,
                            p_ref_pr_line IN NUMBER) IS
           SELECT invoice_num,
                  line_number invoice_line,
                  pr_number,
                  pr_line_number line_num,
                  match_option,
                  vendor_num,
                  vendor_site_code,
                  terms_name
             FROM tac_invhub_invoice_stg
            WHERE file_name = p_filename
              AND interface_type='2WAY'
              AND interface_po_flag ='Y'
              AND pr_number = p_ref_pr_num
              AND pr_line_number = p_ref_pr_line;                  
       l_count NUMBER;
       l_project_id       po_req_distributions_all.project_id%TYPE;
       l_project_name     pa_projects_all.segment1%TYPE;
       l_task_id          po_req_distributions_all.task_id%TYPE;
       l_task_name        pa_tasks.task_number%TYPE;
       l_expenditure_type po_req_distributions_all.expenditure_type%TYPE;
       l_expenditure_item_date  po_req_distributions_all.expenditure_item_date%TYPE;
       l_expenditure_organization_id  po_req_distributions_all.expenditure_organization_id%TYPE;
       l_endtime TIMESTAMP;
    BEGIN
       g_step := 'UPDATE_PO_TO_STG';
        debug_msg('LOG','p_po_import_id :'||p_po_import_id);
         debug_msg('LOG','p_filename :'||p_filename);
         BEGIN
            SELECT COUNT(*)
              INTO l_count
                       FROM po_headers_all poh,
                            po_lines_all pol,
                            po_line_locations_all pll,
                            po_distributions_all pd
                      WHERE poh.po_header_id=pol.po_header_id
                        AND pol.po_header_id=pll.po_header_id
                        AND pol.po_line_id=pll.po_line_id
                        AND pll.po_header_id=pd.po_header_id
                        AND pll.po_line_id=pd.po_line_id
                        AND pll.line_location_id=pd.line_location_id
                        AND poh.request_id=p_po_import_id;
         END;
          debug_msg('LOG','l_count :'||l_count);
       FOR i IN c1(p_po_import_id) LOOP
           debug_msg('LOG','i.po_number :'||i.po_number);
           debug_msg('LOG','i.line_number :'||i.line_number);
           debug_msg('LOG','i.req_header_reference_num :'||i.req_header_reference_num);
           debug_msg('LOG','i.req_line_reference_num :'||i.req_line_reference_num);


           UPDATE tac_invhub_invoice_stg
              SET po_number = i.po_number
                 ,po_line_number = i.line_number
                 ,interface_po_flag='Y'
            WHERE file_name = p_filename
              AND interface_type='2WAY'
              AND interface_po_flag in ('P','N')
              AND pr_number = i.req_header_reference_num
              AND pr_line_number = i.req_line_reference_num;

           
           ----  Update log status for log monitor  -----------------------
           FOR c_get_pr_log IN  c_complete_po (p_filename,
                                               i.req_header_reference_num,
                                               i.req_line_reference_num) LOOP
               g_log_detail_seq :=g_log_detail_seq+1;
               g_log_detail(g_log_detail_seq).level_type :='D';
               g_log_detail(g_log_detail_seq).request_id := p_conc_request_id;
               g_log_detail(g_log_detail_seq).error_type := 'PO Interface';
               g_log_detail(g_log_detail_seq).ap_invoice := c_get_pr_log.invoice_num;
               g_log_detail(g_log_detail_seq).ap_invoie_line := c_get_pr_log.invoice_line;
               g_log_detail(g_log_detail_seq).pr_number  := c_get_pr_log.pr_number;
               g_log_detail(g_log_detail_seq).pr_line_number :=c_get_pr_log.line_num;
               g_log_detail(g_log_detail_seq).match_option := c_get_pr_log.match_option;
               g_log_detail(g_log_detail_seq).vendor_number := c_get_pr_log.vendor_num;
               g_log_detail(g_log_detail_seq).vendor_site_name := c_get_pr_log.vendor_site_code;
               g_log_detail(g_log_detail_seq).payment_term := c_get_pr_log.terms_name;
               g_log_detail(g_log_detail_seq).ERROR_CODE := NULL;
               g_log_detail(g_log_detail_seq).error_message := NULL;
               g_log_detail(g_log_detail_seq).processing_date := l_starttime;
                               
                SELECT current_timestamp
                  INTO l_endtime
                  FROM dual; 
                g_log_detail(g_log_detail_seq).processing_time := substr(to_char(l_endtime-l_starttime),12,12);          
           
           END LOOP;-------------------------  

           UPDATE po_line_locations_all
              SET match_option='P'
            WHERE po_header_id= i.po_header_id
              AND po_line_id  = i.po_line_id
              AND line_location_id =i.line_location_id ;

             get_project_information ( i.req_header_reference_num,
                                       i.req_line_reference_num,
                                       l_project_id,
                                       l_project_name,
                                       l_task_id,
                                       l_task_name,
                                       l_expenditure_type,
                                       l_expenditure_item_date,
                                       l_expenditure_organization_id);
            UPDATE po_distributions_all
              SET project_id = l_project_id
                 ,task_id = l_task_id
                 ,expenditure_type = l_expenditure_type
                 ,expenditure_organization_id = l_expenditure_organization_id
                 ,expenditure_item_date = l_expenditure_item_date
            WHERE po_header_id= i.po_header_id
              AND po_line_id  = i.po_line_id
              AND line_location_id =i.line_location_id;

       END LOOP;
       COMMIT;

        EXCEPTION WHEN OTHERS THEN
          NULL;
    END;
    FUNCTION CALL_STANDARD_PO_INTERFACE(p_conc_request_id IN NUMBER,p_org_code IN VARCHAR2) RETURN NUMBER IS
      l_poimport_req_id NUMBER;
      l_finished BOOLEAN;
      l_phase       varchar2 (100);
      l_status      varchar2 (100);
      l_dev_phase   varchar2 (100);
      l_dev_status  varchar2 (100);
      l_message     varchar2 (100);
      l_check_data  varchar2(1);
      l_resp_name   VARCHAR2(50);
      l_resp_id      NUMBER;
      l_resp_app_id  NUMBER;
    BEGIN
     --  fnd_request.set_org_id(102);
       --submit po import
       g_step := 'CALL_STANDARD_PO_INTERFACE';
       debug_msg('LOG',g_step);

       IF p_org_code='DTAC' THEN
          l_resp_name :='PO-HO';
       ELSIF p_org_code='DTN' THEN
          l_resp_name := 'DTN_PO';
       ELSIF p_org_code='PSB' THEN
          l_resp_name :='PB_PO';
       END IF;

        BEGIN
        select fresp.responsibility_id,
               fresp.application_id
          INTO l_resp_id,
               l_resp_app_id
        from   fnd_user fnd
        ,      fnd_responsibility_tl fresp
        where  fnd.user_id = fnd_global.user_id
        and    fresp.responsibility_name = l_resp_name;
         EXCEPTION WHEN no_data_found THEN
            l_resp_id :=NULL;
            l_resp_app_id :=NULL;
       END;
       IF l_resp_id is NOT NULL AND l_resp_app_id is NOT NULL THEN

           fnd_global.apps_initialize (user_id => fnd_global.user_id,
                                       resp_id => l_resp_id,
                                       resp_appl_id => l_resp_app_id);
        END IF;
       l_poimport_req_id := fnd_request.submit_request (application  => 'PO',
                                                        program       => 'POXPOPDOI',
                                                        sub_request   => FALSE,
                                                        argument1    => null,
                                                        argument2     => 'STANDARD',
                                                        argument3    => null,
                                                        argument4     => 'Y',
                                                        argument5    => null,
                                                        argument6     => 'APPROVED',
                                                        argument7    => null,
                                                        argument8     => p_conc_request_id,
                                                        argument9    => null,
                                                        argument10    => null
                                                        );
       COMMIT;

       debug_msg('LOG','Import request id='||l_poimport_req_id);


       --Waiting until Requisition Import complete.
       LOOP
       l_finished := fnd_concurrent.wait_for_request ( request_id => l_poimport_req_id
                                                 ,interval   => 5
                                                 ,max_wait   => 0
                                                 ,phase      => l_phase
                                                 ,status     => l_status
                                                 ,dev_phase  => l_dev_phase
                                                 ,dev_status => l_dev_status
                                                 ,message    => l_message );

       EXIT WHEN  l_phase='Completed';
       DBMS_LOCK.SLEEP(10);
       END loop;
       debug_msg('LOG','l_dev_phase ='||l_dev_phase);
       debug_msg('LOG','l_status ='||l_status);
       debug_msg('LOG','l_dev_status ='||l_dev_status);
       --COMMIT;
       RETURN(l_poimport_req_id);


    END;

    PROCEDURE validate_duplicate_pr (p_file_name IN varchar2) IS


     CURSOR c_pr (p_file_name IN VARCHAR2)
                      IS SELECT DISTINCT a.pr_number,pr_line_number,decode(a.org_code,'DTAC',102,'DTN',142,'PSB',218) ORG_ID
                            FROM TAC_INVHUB_INVOICE_STG a
                           WHERE file_name =p_file_name
                             AND interface_po_flag='N'
                             AND interface_ap_flag='N'
                           ORDER BY 1,2;


     CURSOR c1(p_file_name IN VARCHAR2,p_pr_number IN VARCHAR2,p_pr_line IN NUMBER)
                       IS SELECT a.interface_po_flag,a.pr_number,pr_line_number,invoice_num,line_number,line_amount,file_name,tac_po_invoicehub_pkg.f_invhub_get_pr_amount(pr_number,pr_line_number) pr_amt
                                ,match_option
                                ,vendor_num
                                ,vendor_site_code
                                ,terms_name
                                ,decode(a.org_code,'DTAC',102,'DTN',142,'PSB',218) ORG_ID
                            FROM TAC_INVHUB_INVOICE_STG a
                           WHERE file_name =p_file_name
                             AND a.pr_number=p_pr_number
                             AND a.pr_line_number=p_pr_line
                             AND interface_po_flag='N'
                             AND interface_ap_flag='N'
                           ORDER BY 2,3,4,5;
     l_inv_amt NUMBER:=0;
     l_existing_pr_amt NUMBER:=0;
     job_rec c1%ROWTYPE;
     l_check_pr VARCHAR2(1);
     l_starttime TIMESTAMP;
     l_endtime   TIMESTAMP;
    BEGIN
       g_step := 'validate_duplicate_pr';
       debug_msg('LOG',g_step);
       FOR i_pr IN c_pr(p_file_name) LOOP
       BEGIN
           debug_msg('LOG',' pr_number='|| i_pr.pr_number);
           debug_msg('LOG',' pr_line_number='|| i_pr.pr_line_number);

           l_inv_amt :=0;
           debug_msg('LOG',' l_inv_amt='|| l_inv_amt);
           OPEN c1(p_file_name,i_pr.pr_number,i_pr.pr_line_number);
           LOOP
               FETCH c1 INTO job_rec;
               IF c1%NOTFOUND THEN
                  EXIT;
               ELSE
                   -- check exist pr in system
                   BEGIN
                                          
                     SELECT current_timestamp
                       INTO l_starttime
                       FROM dual;
                       
                      SELECT DISTINCT 'Y'
                        INTO l_check_pr
                        FROM po_requisition_headers_all
                       WHERE segment1=i_pr.pr_number
                         AND ORG_ID= i_pr.org_id;
                       EXCEPTION WHEN no_data_found THEN
                         
                         l_check_pr:='N';
                          g_log_detail_seq :=g_log_detail_seq+1;
                           g_log_detail(g_log_detail_seq).level_type :='D';
                           g_log_detail(g_log_detail_seq).request_id := fnd_global.CONC_REQUEST_ID;
                           g_log_detail(g_log_detail_seq).error_type := 'PO Interface';
                           g_log_detail(g_log_detail_seq).ap_invoice := job_rec.invoice_num;

                           g_log_detail(g_log_detail_seq).ap_invoie_line := job_rec.line_number;
                           g_log_detail(g_log_detail_seq).pr_number  := job_rec.pr_number;
                           g_log_detail(g_log_detail_seq).pr_line_number :=job_rec.pr_line_number;
                           g_log_detail(g_log_detail_seq).match_option := job_rec.match_option;
                           g_log_detail(g_log_detail_seq).vendor_number := job_rec.vendor_num;
                           g_log_detail(g_log_detail_seq).vendor_site_name := job_rec.vendor_site_code;
                           g_log_detail(g_log_detail_seq).payment_term := job_rec.terms_name;
                           g_log_detail(g_log_detail_seq).ERROR_CODE := 'INVHUB002-008';  --Error INVHUB002-007: Duplicate PR number
                           g_log_detail(g_log_detail_seq).error_message := 'Invalid PR Number';
                            SELECT current_timestamp
                             INTO l_endtime
                             FROM dual;
                           
                          g_log_detail(g_log_detail_seq).processing_date := l_starttime;
                          g_log_detail(g_log_detail_seq).processing_time := substr(to_char(l_endtime-l_starttime),12,12);   
                          -------------------------
                          UPDATE TAC_INVHUB_INVOICE_STG
                             SET interface_po_flag='E'
                               -- ,interface_ap_flag='E'
                           WHERE pr_number = job_rec.pr_number
                             AND pr_line_number  = job_rec.pr_line_number
                             AND invoice_num = job_rec.invoice_num
                             AND line_number = job_rec.line_number
                             AND file_name = job_rec.file_name
                             AND interface_type='2WAY';
                   END;
                   -- check duplicate pr in system
                   IF l_check_pr='Y' THEN
                   BEGIN
                     l_inv_amt := l_inv_amt + job_rec.line_amount;
                     debug_msg('LOG',' job_rec.line_amount='|| job_rec.line_amount);
                     debug_msg('LOG',' l_inv_amt='|| l_inv_amt);
                   BEGIN

                      BEGIN
                      SELECT nvl(sum(nvl(pd.encumbered_amount,0)),0)
                        INTO l_existing_pr_amt
                        FROM po_headers_all poh,
                             po_lines_all pol,
                             po_line_locations_all pll,
                             po_distributions_all pd
                       WHERE poh.po_header_id=pol.po_header_id
                         AND pol.po_header_id=pll.po_header_id
                         AND pol.po_line_id=pll.po_line_id
                         AND pll.po_header_id=pd.po_header_id
                         AND pll.po_line_id=pd.po_line_id
                         AND pll.line_location_id=pd.line_location_id
                         AND pd.req_header_reference_num=i_pr.pr_number
                         AND pd.req_line_reference_num=i_pr.pr_line_number
                         AND poh.org_id=i_pr.org_id
                         AND NVL(poh.cancel_flag,'N') !='Y';

                         EXCEPTION WHEN no_data_found THEN
                           l_existing_pr_amt:=0;
                       END;
                       -- if for case check in existing system
                       debug_msg('LOG',' l_inv_amt='|| (nvl(l_inv_amt,0)+nvl(l_existing_pr_amt,0)));
                       debug_msg('LOG',' l_existing_pr_amt='|| nvl(l_existing_pr_amt,0));
                       IF (round(nvl(l_inv_amt,0)+nvl(l_existing_pr_amt,0),2) > round(nvl(l_existing_pr_amt,0),2))
                           AND  (l_existing_pr_amt !=0) THEN
                           dbms_output.put_line('l_inv_amt >job_rec.pr_amt ');

                           g_log_detail_seq :=g_log_detail_seq+1;
                           g_log_detail(g_log_detail_seq).level_type :='D';
                           g_log_detail(g_log_detail_seq).request_id := fnd_global.CONC_REQUEST_ID;
                           g_log_detail(g_log_detail_seq).error_type := 'PO Interface';
                           g_log_detail(g_log_detail_seq).ap_invoice := job_rec.invoice_num;

                           g_log_detail(g_log_detail_seq).ap_invoie_line := job_rec.line_number;
                           g_log_detail(g_log_detail_seq).pr_number  := job_rec.pr_number;
                           g_log_detail(g_log_detail_seq).pr_line_number :=job_rec.pr_line_number;
                           g_log_detail(g_log_detail_seq).match_option := job_rec.match_option;
                           g_log_detail(g_log_detail_seq).vendor_number := job_rec.vendor_num;
                           g_log_detail(g_log_detail_seq).vendor_site_name := job_rec.vendor_site_code;
                           g_log_detail(g_log_detail_seq).payment_term := job_rec.terms_name;
                           g_log_detail(g_log_detail_seq).ERROR_CODE := 'INVHUB002-007';  --Error INVHUB002-007: Duplicate PR number
                           g_log_detail(g_log_detail_seq).error_message := 'Duplicate PR number';
                           
                            SELECT current_timestamp
                             INTO l_endtime
                             FROM dual;
                           
                          g_log_detail(g_log_detail_seq).processing_date := l_starttime;
                          g_log_detail(g_log_detail_seq).processing_time := substr(to_char(l_endtime-l_starttime),12,12);  
                          -------------------------
                          UPDATE TAC_INVHUB_INVOICE_STG
                             SET interface_po_flag='E'
                               -- ,interface_ap_flag='E'
                           WHERE pr_number = job_rec.pr_number
                             AND pr_line_number  = job_rec.pr_line_number
                             AND invoice_num = job_rec.invoice_num
                             AND line_number = job_rec.line_number
                             AND file_name = job_rec.file_name
                             AND interface_type='2WAY';
                       ELSE
                         dbms_output.put_line('job_rec.line_amount='||job_rec.line_amount);
                         dbms_output.put_line('l_inv_amt='||l_inv_amt);
                         dbms_output.put_line('job_rec.pr_amt='||job_rec.pr_amt);
                         -- check duplicate pr in the same file
                         IF round(l_inv_amt,2) > round(job_rec.pr_amt,2) THEN
                            dbms_output.put_line('l_inv_amt >job_rec.pr_amt ');

                             g_log_detail_seq :=g_log_detail_seq+1;
                             g_log_detail(g_log_detail_seq).level_type :='D';
                             g_log_detail(g_log_detail_seq).request_id := fnd_global.CONC_REQUEST_ID;
                             g_log_detail(g_log_detail_seq).error_type := 'PO Interface';
                             g_log_detail(g_log_detail_seq).ap_invoice := job_rec.invoice_num;

                             g_log_detail(g_log_detail_seq).ap_invoie_line := job_rec.line_number;
                             g_log_detail(g_log_detail_seq).pr_number  := job_rec.pr_number;
                             g_log_detail(g_log_detail_seq).pr_line_number :=job_rec.pr_line_number;
                             g_log_detail(g_log_detail_seq).match_option := job_rec.match_option;
                             g_log_detail(g_log_detail_seq).vendor_number := job_rec.vendor_num;
                             g_log_detail(g_log_detail_seq).vendor_site_name := job_rec.vendor_site_code;
                             g_log_detail(g_log_detail_seq).payment_term := job_rec.terms_name;
                             g_log_detail(g_log_detail_seq).ERROR_CODE := 'INVHUB002-014';  --Error INVHUB002-007: Duplicate PR number
                             g_log_detail(g_log_detail_seq).error_message := 'Invoice amount exceed PR amount';
                             
                             SELECT current_timestamp
                             INTO l_endtime
                             FROM dual;
                           
                             g_log_detail(g_log_detail_seq).processing_date := l_starttime;
                             g_log_detail(g_log_detail_seq).processing_time := substr(to_char(l_endtime-l_starttime),12,12);  
                            -------------------------
                            UPDATE TAC_INVHUB_INVOICE_STG
                               SET interface_po_flag='E'
                                 -- ,interface_ap_flag='E'
                             WHERE pr_number = job_rec.pr_number
                               AND pr_line_number  = job_rec.pr_line_number
                               AND invoice_num = job_rec.invoice_num
                               AND line_number = job_rec.line_number
                               AND file_name = job_rec.file_name
                               AND interface_type='2WAY';

                         END IF;
                       END IF;

                   END;
                   END;
                   END IF;
                -- l_inv_amt := l_inv_amt + job_rec.line_amount;
               --    dbms_output.put_line('job_rec.rowid='||job_rec.rowid);

               END IF;
           END LOOP;
           CLOSE c1;
       END;
       END LOOP;
       COMMIT;
    END;
    PROCEDURE write_output (p_request_id IN NUMBER,p_program_name IN VARCHAR2) IS
       l_writetxt VARCHAR2(1000);
       l_starttime TIMESTAMP;
       CURSOR c_detail1(p_request_id IN NUMBER) IS SELECT DISTINCT level_type||'|'||
                                           error_type||'|'||
                                           ap_invoice||'|'||
                                           ap_invoie_line||'|'||
                                           po_number||'|'||
                                           po_line_number||'|'||
                                           receipt_number||'|'||
                                           receipt_line_num||'|'||
                                           pr_number||'|'||
                                           pr_line_number||'|'||
                                           match_option||'|'||
                                           vendor_number||'|'||
                                           processing_date||'|'||
                                           processing_time||'|'||
                                           DECODE(ERROR_CODE,NULL,'Completed','Error')||'|'||
                                           ERROR_CODE||'|'||
                                           error_message log_txt
                                      from DTINVHUB_LOG_DETAIL
                                    WHERE request_id=p_request_id;
       CURSOR c_detail2(p_request_id IN NUMBER) IS  SELECT  LEVEL_TYPE||'|'||
                                                          ERROR_TYPE||'|'||
                                                          AP_INVOICE||'|'||
                                                          VENDOR_NUMBER||'|'||
                                                          DUE_DATE||'|'||
                                                          PAYMENT_STATUS||'|'||
                                                          PV_NUMBER||'|'||
                                                          DOCUMENT_NUMBER||'|'||
                                                          AP_VOUCHER_NUMBER||'|'||
                                                          PAYGROUP||'|'||
                                                          PROCESSING_DATE||'|'||
                                                          PROCESSING_TIME||'|'||
                                                          'Completed' log_txt
                                                   from DTINVHUB_LOG_DETAIL
                                                  WHERE request_id=p_request_id;                             
                                     
       CURSOR c_detail3(p_request_id IN NUMBER) IS  SELECT level_type||'|'||
                                                           error_type||'|'||
                                                           vendor_number||'|'||
                                                           vendor_name||'|'||
                                                           vendor_site_name||'|'||
                                                           payment_term||'|'||
                                                           SYSDATE||'|'||
                                                           processing_date||'|'||
                                                           processing_time||'|'||
                                                           'Completed' log_txt
                                                     from DTINVHUB_LOG_DETAIL
                                                    WHERE request_id=p_request_id;                             
                                    
                                    
    BEGIN
         SELECT current_timestamp
           INTO l_starttime
           FROM dual;
          
        BEGIN
        SELECT level_type||'|'||
                 request_id||'|'||
                 program_name||'|'||
                 process_date||'|'||
                 processing_type||'|'||
                 processing_date||'|'||
                 record_count_completion||'|'||
                 record_count_error
            INTO l_writetxt
            FROM dtinvhub_log_summary
          WHERE request_id=p_request_id;
          DEBUG_MSG('OUTPUT',
                    l_writetxt);
            EXCEPTION WHEN no_data_found THEN
              IF p_program_name IN ('DTINVHUB002','DTINVHUB003') THEN
                  l_writetxt := 'H'||'|'||
                                 p_request_id||'|'||
                                 p_program_name||'|'||
                                 l_starttime||'|'||
                                 'Download'||'|'||
                                 0||'|'||
                                 0||'|'||
                                 0;
              ELSE
                 l_writetxt := 'H'||'|'||
                                 p_request_id||'|'||
                                 p_program_name||'|'||
                                 l_starttime||'|'||
                                 'Upload'||'|'||
                                 0||'|'||
                                 0||'|'||
                                 0;
              END IF;
             DEBUG_MSG('OUTPUT',
                        l_writetxt);          
         END; 
          
         if p_program_name IN ('DTINVHUB002','DTINVHUB003') then
         BEGIN
            FOR i IN  c_detail1(p_request_id) LOOP
              DEBUG_MSG('OUTPUT',
                        i.log_txt);
            END LOOP;
         END;
         elsif p_program_name = 'DTINVHUB004' THEN
         BEGIN
             FOR i IN c_detail2(p_request_id) LOOP
                DEBUG_MSG('OUTPUT',
                          i.log_txt);
             END LOOP;
         END;
         elsif p_program_name = 'DTINVHUB005' then
         BEGIN
            FOR i IN c_detail3(p_request_id) LOOP
                DEBUG_MSG('OUTPUT',
                          i.log_txt);
             END LOOP;
         END;
         ELSE
            null;
         end if;          
    END;
    PROCEDURE GET_INTERFACE_ERROR( v_startprocess IN TIMESTAMP,
                                   p_conc_request_id IN NUMBER,
                                   p_group_name IN VARCHAR2) IS
       CURSOR c1(p_conc_request_id IN NUMBER,p_group_name IN VARCHAR2)
                  IS SELECT PHI.BATCH_ID,
                            PDI.REQ_HEADER_REFERENCE_NUM,
                            PDI.REQ_LINE_REFERENCE_NUM,
                            PIE.ERROR_MESSAGE,
                            decode(PHI.org_id,102,'DTAC',142,'DTN',218,'PSB') COMP_CODE,
                            stg.INVOICE_NUM,
                            stg.LINE_NUMBER,
                            stg.MATCH_OPTION,
                            stg.VENDOR_NUM,
                            stg.VENDOR_SITE_CODE,
                            stg.terms_name
                       FROM PO_HEADERS_INTERFACE PHI,po_lines_interface PLI,po_distributions_interface PDI,po_interface_errors PIE,
                            tac_invhub_invoice_stg stg
                      WHERE PHI.interface_source_code='FXTH'
                      AND PHI.procesS_code='REJECTED'
                      AND PHI.INTERFACE_HEADER_ID = PLI.INTERFACE_HEADER_ID
                      AND PLI.INTERFACE_HEADER_ID=PDI.INTERFACE_HEADER_ID
                      AND PLI.INTERFACE_LINE_ID=PDI.INTERFACE_LINE_ID
                      --AND  PHI.INTERFACE_HEADER_ID=537289
                      AND phi.batch_id=p_conc_request_id
                      AND PHI.INTERFACE_HEADER_ID = PIE.INTERFACE_HEADER_ID
                      AND stg.SOURCE= 'FXTH'
                      AND stg.INTERFACE_TYPE='2WAY'
                      AND stg.PR_NUMBER=PDI.REQ_HEADER_REFERENCE_NUM
                      AND stg.PR_LINE_NUMBER=PDI.REQ_LINE_REFERENCE_NUM
                      AND decode(PHI.org_id,102,'DTAC',142,'DTN',218,'PSB')=stg.ORG_CODE
                      AND stg.GROUP_NAME=p_group_name;
         l_endtime TIMESTAMP;
    BEGIN
       FOR i IN c1(p_conc_request_id,
                   p_group_name) LOOP
       BEGIN
           SELECT current_timestamp
           INTO l_endtime
           FROM dual;
           g_log_detail_seq :=g_log_detail_seq+1;
           g_log_detail(g_log_detail_seq).level_type :='D';
           g_log_detail(g_log_detail_seq).request_id :=p_conc_request_id;
           g_log_detail(g_log_detail_seq).error_type := 'PO Interface';
           g_log_detail(g_log_detail_seq).ap_invoice := i.invoice_num;
           g_log_detail(g_log_detail_seq).ap_invoie_line := i.LINE_NUMBER;
           g_log_detail(g_log_detail_seq).pr_number  := i.REQ_HEADER_REFERENCE_NUM;
           g_log_detail(g_log_detail_seq).pr_line_number :=i.REQ_LINE_REFERENCE_NUM;
           g_log_detail(g_log_detail_seq).match_option := i.MATCH_OPTION;
           g_log_detail(g_log_detail_seq).vendor_number := i.VENDOR_NUM;
           g_log_detail(g_log_detail_seq).vendor_site_name := i.VENDOR_SITE_CODE;
           g_log_detail(g_log_detail_seq).payment_term := i.terms_name;
           g_log_detail(g_log_detail_seq).ERROR_CODE := 'INVHUB002-099';  --Error INVHUB002-007: Duplicate PR number
           g_log_detail(g_log_detail_seq).error_message := i.ERROR_MESSAGE;
           g_log_detail(g_log_detail_seq).processing_date := v_startprocess;
           g_log_detail(g_log_detail_seq).processing_time := substr(to_char(l_endtime-v_startprocess),12,12); 
           --p_stg_po_header.interface_flag :='E';
           
           UPDATE tac_invhub_invoice_stg
              SET interface_po_flag='E'
            WHERE group_name =p_group_name
              AND SOURCE= 'FXTH'
              AND INTERFACE_TYPE='2WAY'
              AND PR_NUMBER= i.REQ_HEADER_REFERENCE_NUM
              AND PR_LINE_NUMBER=i.REQ_LINE_REFERENCE_NUM;
       END;
       END LOOP;
       
    END;
    PROCEDURE PO_INTERFACE (errbuf       OUT VARCHAR2,
                            errcode      OUT VARCHAR2,
                            p_comp_code IN VARCHAR2,
                            p_source     IN VARCHAR2,
                            p_interface_type IN VARCHAR2,
                            p_filename   IN VARCHAR2,
                            p_rerun      IN VARCHAR2,
                            p_filepath   IN VARCHAR2,
                            p_debug_flag IN VARCHAR2
                            )
                              IS
       --TYPE r_log_detail apps.DTINVHUB_LOG_DETAIL%ROWTYPE;

       stg_log_detail t_log_detail;-- := t_log_detail();
       v_step        VARCHAR2(240);
       v_error_flag  BOOLEAN;
     --  PO_STR_ERROR  T_PR_STR_ERROR;
       old_line_num  number;
       interface_line_seq number;
       stg_po_header t_po_header;
       stg_po_line      t_po_line;
       stg_po_dist      t_po_dist;
       p_conc_request_id number;
       p_group_name varchar2(50);
       v_po_import_req_id NUMBER;
       v_startprocess TIMESTAMP;
       l_count_dtac    NUMBER;
       l_count_dtn     NUMBER;
       l_count_psb     NUMBER;
       p_current_request_id NUMBER;
       CURSOR c_readfile(p_company_code IN varchar2,
                         p_source IN VARCHAR2,
                         p_filename     IN VARCHAR2
                        )IS
        SELECT DISTINCT file_name,GROUP_NAME
          FROM TAC_INVHUB_INVOICE_STG
         WHERE 1=1--org_code = p_company_code
           AND SOURCE= p_source
           AND interface_type = '2WAY'--i_interface_type
           AND interface_po_flag='N'
           AND interface_ap_flag='N'
           AND file_name = NVL(p_filename,File_name) --i_file_name,file_name)
           AND file_name IS NOT NULL
        ORDER BY file_name;
     BEGIN
         g_debug_flag := p_debug_flag;
         g_step :='Start process';
         SELECT current_timestamp
           INTO v_startprocess
           FROM dual;
         debug_msg('LOG',g_step);
         p_conc_request_id := fnd_global.CONC_REQUEST_ID;
        -- fetch data
         debug_msg('LOG','p_comp_code='||p_comp_code);
         debug_msg('LOG','p_interface_type='||p_interface_type);
         debug_msg('LOG','p_filename='||nvl(p_filename,'*'));

         FOR r_readfile IN c_readfile(p_comp_code,
                                      p_source,
                                      p_filename
                                      ) LOOP
         BEGIN
            
            validate_duplicate_pr (r_readfile.file_name);
            stg_po_header.delete;
            stg_po_line.delete;
            stg_po_dist.delete;
            g_po_line_indx:=0;
            g_po_dist_indx  := 0;
            
           
            
              FETCH_PO_HEADER(p_group_name => r_readfile.file_name,--p_group_name,
                             p_po_header  => stg_po_header);

             IF stg_po_header.count  > 0 then
             BEGIN
                FOR i in 1 .. stg_po_header.count LOOP
                    validate_po_header_int(p_stg_po_header => stg_po_header(I),
                                           p_stg_log_detail=> stg_log_detail);

                    FETCH_PO_LINE(p_stg_po_header => stg_po_header(I),
                                  p_stg_po_line   => stg_po_line,
                                  p_stg_po_dist   => stg_po_dist);
                    dbms_output.put_line('stg_po_line :'||stg_po_line.count);

                 END LOOP;

                 INSERT_PO_HEADER_INT(stg_po_header     => stg_po_header,
                                      p_conc_request_id => p_conc_request_id
                                                      );
                 INSERT_PO_LINE_INT(stg_po_line => stg_po_line);
                 INSERT_PO_DIST_INT(stg_po_dist => stg_po_dist);


             END;
             END IF;
             COMMIT;
             debug_msg('LOG','p_conc_request_id'||p_conc_request_id);
             -- CALL INTERFACE FOR DTAC
             g_step :='CALL STANDARD INTERFACE PROGRAM';
              BEGIN

   
              SELECT SUM(DECODE(org_id,102,1,0)),
                     SUM(DECODE(org_id,142,1,0)),
                     SUM(DECODE(org_id,218,1,0))
                INTO l_count_dtac
                     ,l_count_dtn
                     ,l_count_psb
                from PO_HEADERS_INTERFACE
               WHERE batch_id = p_conc_request_id;
              END;

             IF l_count_dtac != 0 THEN
                v_po_import_req_id := CALL_STANDARD_PO_INTERFACE(p_conc_request_id,'DTAC');
                debug_msg('LOG','v_po_import_req_id'||v_po_import_req_id);
               --  v_po_import_req_id := NULL;
                 IF v_po_import_req_id IS NOT NULL THEN
                    debug_msg('LOG','before call update_po_to_stg');
                    update_po_to_stg(p_po_import_id => v_po_import_req_id
                                    ,p_filename     => r_readfile.file_name
                                    ,l_starttime    => v_startprocess
                                    ,p_conc_request_id => p_conc_request_id
                                    );

                 END IF;
             END IF;

             IF l_count_dtn != 0 THEN
                v_po_import_req_id := CALL_STANDARD_PO_INTERFACE(p_conc_request_id,'DTN');
                debug_msg('LOG','v_po_import_req_id'||v_po_import_req_id);
               --  v_po_import_req_id := NULL;
                 IF v_po_import_req_id IS NOT NULL THEN
                    debug_msg('LOG','before call update_po_to_stg');
                    update_po_to_stg(p_po_import_id => v_po_import_req_id
                                    ,p_filename     => r_readfile.file_name
                                    ,l_starttime    => v_startprocess
                                    ,p_conc_request_id => p_conc_request_id);

                 END IF;
             END IF;

             IF l_count_psb != 0 THEN
                v_po_import_req_id := CALL_STANDARD_PO_INTERFACE(p_conc_request_id,'PSB');
                debug_msg('LOG','v_po_import_req_id'||v_po_import_req_id);
                 --  v_po_import_req_id := NULL;
                   IF v_po_import_req_id IS NOT NULL THEN
                      debug_msg('LOG','before call update_po_to_stg');
                      update_po_to_stg(p_po_import_id => v_po_import_req_id
                                      ,p_filename     => r_readfile.file_name
                                      ,l_starttime    => v_startprocess
                                      ,p_conc_request_id => p_conc_request_id);

                   END IF;
             END IF;
             
             GET_INTERFACE_ERROR( v_startprocess,p_conc_request_id,r_readfile.GROUP_NAME);
         END;
         END LOOP;
         
         stg_po_header.delete;
         stg_po_line.delete;
         stg_po_dist.delete;
         g_step :='Write log detail';
         --
         write_log_detail(v_startprocess,p_conc_request_id);
         COMMIT;
         call_logmonitor('DTINVHUB002',p_conc_request_id);
         g_step :='write_output';
         write_output(p_conc_request_id,'DTINVHUB002');
         exception when others THEN
           stg_po_header.delete;
           stg_po_line.delete;
           stg_po_dist.delete;
           debug_msg('LOG','Error in step '||g_step ||' : '||sqlerrm);



  END PO_INTERFACE;

  FUNCTION get_dir_path (p_oracle_path IN VARCHAR2) RETURN dba_directories.directory_path%TYPE IS
      l_path dba_directories.directory_path%TYPE;
  BEGIN

     g_step := 'in get_dir_path';
     SELECT DIRECTORY_PATH
       INTO l_path
       FROM dba_directories
      WHERE DIRECTORY_NAME= p_oracle_path;--'INVHUB_INV_INBOX';
      RETURN(l_path);
     EXCEPTION WHEN OTHERS THEN
       debug_msg('LOG','Error in step '||g_step ||' : '||sqlerrm);
       RETURN(NULL);
  END;



  PROCEDURE ReturnPaidInvoice (errbuf       OUT VARCHAR2,
                               errcode      OUT VARCHAR2,
                               p_replace    IN  VARCHAR2,
                               p_filepath   IN VARCHAR2,
                               p_date       IN DATE,
                               p_data_source IN VARCHAR2,
                               p_debug_flag IN VARCHAR2
                              ) IS
     CURSOR c_paid_inv(p_date IN DATE,p_source IN VARCHAR2) IS
                                 SELECT DISTINCT decode(ai.org_id,102,'DTAC',
                                                         142,'DTN',
                                                         218,'PSB'
                                              ) company_code,
                                 ai.invoice_num invoice_number,
                                 pv.segment1 supplier_code,
                                 ps.due_date due_date,
                                 aca.status_lookup_code payment_status,
                                 aca.doc_sequence_value pv_number,
                                 aca.check_number document_number,
                                 ai.doc_sequence_value ap_voucher_number,
                                 ai.pay_group_lookup_code Paygroup
                            FROM ap_invoices_all ai,
                                 po_vendors pv,
                                 (SELECT due_date,invoice_id,payment_num
                                    FROM AP_PAYMENT_SCHEDULES_ALL a
                                   WHERE (invoice_id,payment_num) =(SELECT invoice_id,MAX(payment_num)
                                                                      FROM AP_PAYMENT_SCHEDULES_ALL b
                                                                     WHERE b.invoice_Id=a.invoice_id
                                                                     GROUP BY invoice_id)) ps,
                                 --ap_invoice_payments_all aip,
                                 (SELECT * FROM ap_invoice_payments_all aip
                                   WHERE (INVOICE_ID,ACCOUNTING_EVENT_ID) IN (SELECT INVOICE_ID,MAX(ACCOUNTING_EVENT_ID)
                                                                                FROM ap_invoice_payments_all
                                                                               WHERE INVOICE_ID=AIP.INVOICE_ID
                                                                               GROUP BY INVOICE_ID
                                                                              )
                                 ) AIP,
                                 ap_checks_all aca
                           WHERE TRUNC(ai.last_update_date) = TRUNC(p_date-1)
                             AND ai.vendor_id = pv.vendor_id
                             AND ai.invoice_id = ps.invoice_id
                             AND ps.invoice_id = aip.invoice_id
                             AND ps.payment_num = aip.payment_num
                             AND aip.check_id = aca.check_id
                             AND SOURCE=p_source
                           ORDER BY 1,2,3,4;
     destFile utl_file.file_type;
     l_path   dba_directories.directory_path%TYPE;
     l_file VARCHAR2(50);
     l_writetxt VARCHAR2(1000);
     l_starttime TIMESTAMP;
     l_endtime   TIMESTAMP;
  BEGIN
     g_debug_flag := p_debug_flag;
    
     g_step := 'in ReturnPaidInvoice';
     debug_msg('LOG',g_step);
     
      SELECT current_timestamp
                  INTO l_starttime
                  FROM dual;
                  
     l_path := get_dir_path(p_oracle_path => g_default_backinv_path);
     debug_msg('LOG','l_path :'||l_path);
     g_step := 'create file';
     l_file := g_default_backinv_file||TO_CHAR(SYSDATE,'YYYYMMDD_HHMI')||'.txt';



     destFile := UTL_FILE.FOPEN_NCHAR(g_default_backinv_path,l_file,'A');--utl_file.fopen(g_default_backinv_path,l_file,'W');
     IF utl_file.is_open(destFile) THEN

         FOR i IN c_paid_inv(p_date,p_data_source) LOOP
             l_writetxt := i.company_code||'|'||
                           i.invoice_number||'|'||
                           i.supplier_code||'|'||
                           i.due_date||'|'||
                           i.payment_status||'|'||
                           i.pv_number||'|'||
                           i.document_number||'|'||
                           i.ap_voucher_number||'|'||
                           i.Paygroup;
             debug_msg('LOG',l_writetxt);
             --utl_file.put_line(destFile,l_writetxt);
              utl_file.PUT_line_NCHAR (destFile,l_writetxt);
              UTL_FILE.FFLUSH(destFile);
              
              
              SELECT current_timestamp
                  INTO l_endtime
                  FROM dual;
             INSERT INTO DTINVHUB_LOG_DETAIL
                         (
                          LEVEL_TYPE,
                          REQUEST_ID,
                          ERROR_TYPE,
                          AP_INVOICE,
                          VENDOR_NUMBER,
                          DUE_DATE,
                          PAYMENT_STATUS,
                          PV_NUMBER,
                          DOCUMENT_NUMBER,
                          AP_VOUCHER_NUMBER,
                          PAYGROUP,
                          ERROR_CODE,
                          ERROR_MESSAGE,
                          PROCESSING_DATE,
                          PROCESSING_TIME)
                     values  
                         (
                          'D',
                          fnd_global.CONC_REQUEST_ID,
                          'Back Invoice',
                          i.invoice_number,
                          i.supplier_code,
                          i.due_date,
                          i.payment_status,
                          i.pv_number,
                          i.document_number,
                          i.ap_voucher_number,
                          i.Paygroup,
                          null,
                          null,
                          l_starttime,
                          substr(to_char(l_endtime-l_starttime),12,12));
              
         END LOOP;
         
         
         SELECT current_timestamp
                  INTO l_endtime
                  FROM dual;
         INSERT INTO apps.dtinvhub_log_summary
         (level_type,request_id,program_name,process_date,processing_type,record_count_completion
         ,record_count_error,processing_date)
         SELECT 'H',request_id,'DTINVHUB004',l_starttime,'Upload', sum(DECODE(ERROR_CODE,NULL,1,0)) COMPLETE,
                sum(DECODE(ERROR_CODE,NULL,0,1)) ERROR
               ,substr(to_char(l_endtime-l_starttime),12,12)
          FROM apps.DTINVHUB_LOG_DETAIL WHERE request_id=fnd_global.CONC_REQUEST_ID
         GROUP BY 'H',request_id,'DTINVHUB004',l_starttime,'Upload';
         
         g_step := 'Close file';
         utl_file.fclose(destFile);
         write_output(fnd_global.CONC_REQUEST_ID,'DTINVHUB004');
         call_logmonitor('DTINVHUB004',fnd_global.CONC_REQUEST_ID);
         
     ELSE
        debug_msg('LOG','Couldn''t OPEN FILE');
     END IF;



     EXCEPTION WHEN OTHERS THEN
       debug_msg('LOG','Error in step '||g_step ||' : '||sqlerrm);

  END;


  PROCEDURE SupplierUpdate    (errbuf       OUT VARCHAR2,
                               errcode      OUT VARCHAR2,
                               p_replace    IN  VARCHAR2,
                               p_filepath   IN VARCHAR2,
                               p_date       IN DATE,
                               p_first_load IN VARCHAR2,
                               p_debug_flag IN VARCHAR2
                              )IS
       CURSOR c_supplier(p_date IN DATE) IS  SELECT pv.segment1 supplier_code,
               pv.vendor_name supplier_name,
               pvs.vendor_site_code supplier_site_name,
               apt.name payment_term,
               greatest(pv.last_update_date,pvs.last_update_date) last_udpate_date
          FROM po_vendors pv,
               po_vendor_sites_all pvs,
               ap_terms apt
         WHERE pv.vendor_id = pvs.vendor_id
           AND pvs.terms_id = apt.term_id
           AND (TRUNC(pv.last_update_date)=TRUNC(p_date-1)
                OR
                TRUNC(pvs.last_update_date)=TRUNC(p_date-1)
                )
          --     AND trunc(pvs.last_update_date) BETWEEN  to_date('01/01/2014','DD/MM/YYYY')
           --                                        AND  TO_DATE('30/01/2014','DD/MM/YYYY')
         ORDER BY 1,3,4;

         CURSOR c_supplier_firstload IS  SELECT pv.segment1 supplier_code,
               pv.vendor_name supplier_name,
               pvs.vendor_site_code supplier_site_name,
               apt.name payment_term,
               greatest(pv.last_update_date,pvs.last_update_date) last_udpate_date
          FROM po_vendors pv,
               po_vendor_sites_all pvs,
               ap_terms apt
         WHERE pv.vendor_id = pvs.vendor_id
           AND pvs.terms_id = apt.term_id
           --AND pv.segment1 ='101010'
            --  AND trunc(pvs.last_update_date) BETWEEN  to_date('01/01/2014','DD/MM/YYYY')
            --                                       AND  TO_DATE('30/01/2014','DD/MM/YYYY')
         ORDER BY 1,3,4;

     destFile utl_file.file_type;
     l_path   dba_directories.directory_path%TYPE;
     l_file VARCHAR2(50);
     l_writetxt VARCHAR2(1000);
     TYPE r_supplier IS RECORD (supplier_code   po_vendors.segment1%TYPE,
                                supplier_name   po_vendors.vendor_name%TYPE,
                                supplier_site_name po_vendor_sites_all.vendor_site_code%TYPE,
                                payment_term    ap_terms.name%TYPE,
                                last_update_date DATE);
     TYPE t_supplier IS TABLE OF r_supplier INDEX BY BINARY_INTEGER;

     supplier_rec t_supplier;
     l_starttime TIMESTAMP;
     l_endtime   TIMESTAMP;
  BEGIN
     g_debug_flag := p_debug_flag;

     g_step := 'in SupplierUpdate';
     debug_msg('LOG',g_step);
     
     SELECT current_timestamp
       INTO l_starttime
       FROM dual;
                  
     g_step := 'create file';
     l_file := g_default_supplier_file||TO_CHAR(SYSDATE,'YYYYMMDD_HHMI')||'.txt';
     destFile := --utl_file.fopen(g_default_supplier_path,l_file,'W');
                 UTL_FILE.FOPEN_NCHAR(g_default_supplier_path,l_file,'A');

     IF utl_file.is_open(destFile) THEN
         debug_msg('LOG','OPEN FILE');
         debug_msg('LOG','p_date'||to_char(p_date,'DD/MM/YYYY'));
         g_step := 'Open cursor';
         debug_msg('LOG',g_step);
         debug_msg('LOG','P FIRST LOAD :'|| p_first_load);
         IF p_first_load ='Y' THEN
            debug_msg('LOG','open c_supplier_firstload');
            OPEN c_supplier_firstload;
            FETCH c_supplier_firstload
             BULK COLLECT INTO supplier_rec;
            CLOSE c_supplier_firstload;
         ELSE
            debug_msg('LOG','open c_supplier');
            OPEN c_supplier(p_date);
            FETCH c_supplier
             BULK COLLECT INTO supplier_rec;
            CLOSE c_supplier;
         END IF;

         IF supplier_rec.count > 0 THEN
            g_step := 'Write text';
            debug_msg('LOG',g_step);
            debug_msg('LOG','supplier_rec.count :'||supplier_rec.count );
            FOR i IN 1.. supplier_rec.count LOOP
                l_writetxt := supplier_rec(i).supplier_code||'|'||
                              supplier_rec(i).supplier_name||'|'||
                              supplier_rec(i).supplier_site_name||'|'||
                              supplier_rec(i).payment_term||'|'||
                              to_char(supplier_rec(i).last_update_date,'DD/MM/YYYY');
                 --utl_file.put_line(destFile,l_writetxt);
                 utl_file.PUT_line_NCHAR (destFile,l_writetxt);
                 UTL_FILE.FFLUSH(destFile);
                 
                 SELECT current_timestamp
                   INTO l_endtime
                   FROM dual;
                 
                 INSERT INTO DTINVHUB_LOG_DETAIL
                         (
                          LEVEL_TYPE,
                          REQUEST_ID,
                          ERROR_TYPE,
                          VENDOR_NUMBER,
                          VENDOR_SITE_NAME,
                          VENDOR_NAME,
                          PAYMENT_TERM,
                          ERROR_CODE,
                          ERROR_MESSAGE,
                          PROCESSING_DATE,
                          PROCESSING_TIME)
                    values  
                         (
                          'D',
                          fnd_global.CONC_REQUEST_ID,
                          'Supplier Interface',
                          supplier_rec(i).supplier_code,
                          supplier_rec(i).supplier_site_name,
                          supplier_rec(i).supplier_name,
                          supplier_rec(i).payment_term,
                          null,
                          null,
                          l_starttime,
                          substr(to_char(l_endtime-l_starttime),12,12));
                 
                 
            END LOOP;
         END IF;
         
        SELECT current_timestamp
          INTO l_endtime
          FROM dual;
          
         INSERT INTO apps.dtinvhub_log_summary
         (level_type,request_id,program_name,process_date,processing_type,record_count_completion
         ,record_count_error,processing_date)
         SELECT 'H',request_id,'DTINVHUB005',l_starttime,'Upload', sum(DECODE(ERROR_CODE,NULL,1,0)) COMPLETE,
                sum(DECODE(ERROR_CODE,NULL,0,1)) ERROR
               ,substr(to_char(l_endtime-l_starttime),12,12)
          FROM apps.DTINVHUB_LOG_DETAIL WHERE request_id=fnd_global.CONC_REQUEST_ID
         GROUP BY 'H',request_id,'DTINVHUB005',l_starttime,'Upload';
         
         g_step := 'Close cursor';
         debug_msg('LOG',g_step);
         utl_file.fclose(destFile);
         supplier_rec.DELETE;
         write_output(fnd_global.CONC_REQUEST_ID,'DTINVHUB005');
         call_logmonitor('DTINVHUB005',fnd_global.CONC_REQUEST_ID);
     ELSE
        debug_msg('LOG','Couldn''t OPEN FILE');
     END IF;



     EXCEPTION WHEN OTHERS THEN
       debug_msg('LOG','Error in step '||g_step ||' : '||sqlerrm);

  END;

    FUNCTION     CHECK_PO (P_PO_NUMBER IN VARCHAR2) RETURN BOOLEAN IS
             l_check_po_number varchar2(1);
       BEGIN
             SELECT DISTINCT 'Y'
                 INTO l_check_po_number
                FROM po_headers_all
               WHERE segment1=p_po_number;
              return(true);
              exception when no_data_found then
              return(false);
       END;

       FUNCTION CHECK_GRN(P_GRN_NUMBER IN VARCHAR2) RETURN BOOLEAN IS
             l_check_grn varchar2(1);
       BEGIN
             SELECT DISTINCT 'Y'
                 INTO l_check_grn
                 FROM rcv_shipment_headers
                WHERE receipt_num=P_GRN_NUMBER;
            RETURN(TRUE);
             exception when no_data_found then
             return(false);
       END;
       /*PROCEDURE  getGRNInfomation(p_grn_number in varchar2,
                                                 p_company_code in  varchar2,
                                                 p_po_number in varchar2,
                                                 o_grn_infomation out APPS.t_grn_info,
                                                 o_err_code out varchar2,
                                                 o_err_mesg out varchar2) IS
              l_sum_grn  number;
         BEGIN
              o_err_code := null;
              o_err_mesg := null;
              -- validate parameter
               if (p_grn_number is null) or (p_company_code is null) or (p_po_number is null) then
                  dbms_output.put_line('null parameter');
                  o_err_code  := 'INVHUB001-005';
                  o_err_mesg := 'Missing Parameter';
               end if;

              -- validate  existing po in system
              if o_err_code is null and (not CHECK_PO (p_po_number)) then
                 o_err_code  := 'INVHUB001-002';
                 o_err_mesg := 'Not found PO';
              end if;

              -- validate  existing grn in system
              if o_err_code is null and (not check_grn(p_grn_number)) then
                 o_err_code  := 'INVHUB001-001';
                 o_err_mesg := 'Not found GRN';
              end if;

              if o_err_code is null then
              begin
--                   insert into o_grn_infomation
--                   select  rsh.receipt_num GRN_NUMBER,--rt.*
--                              'DTAC'  Company_Code,
--                              poh.segment1 PO_Number,
--                              pol.line_num  PO_Line_Number,
--                              rsl.shipment_line_status_code GRN_STATUS,
--                              rsl.line_num GRN_LINE_NUMBER,
--                              item.item_code ITEM_CODE,
--                              rsl.item_description LINE_DESCRIPTION,
--                              rsl.quantity_received GRN_QTY, -- if rsl.destination_type_code='INVENTORY'  then qty if EXPENSE THEN AMOUNT
--                              rsl.unit_of_measure GRN_UOM,
--                              rsl.quantity_received GRN_AMOUNT,
--                              poh.currency_code GRN_CURRENCY,
--                              0 TOTAL_GRN_AMOUNT,
--                              org.organization_code ORG_CODE,
--                              pv.segment1 VENDOR_NUMBER,
--                              pvs.vendor_site_code VENDOR_SITE_CODE,
--                              payterm.name PAYMENT_TERM,
--                              sum(pll.quantity-pll.quantity_received-pll.quantity_rejected-pll.quantity_cancelled) PO_Remaining_Amount
--                     --  bulk collect into o_grn_infomation
--                      from rcv_shipment_headers rsh
--                             ,rcv_shipment_lines rsl
--                             ,rcv_transactions rt
--                            ,(select msi.inventory_item_id,msi.segment1 ITEM_CODE
--                                from mtl_system_items_b msi,mtl_parameters mp
--                              where msi.organization_id=mp.organization_id
--                                  and msi.organization_id=mp.master_organization_id) ITEM
--                             ,po_headers_all poh
--                             ,po_lines_all pol
--                             ,po_line_locations_all pll
--                             ,po_distributions_all pd
--                             ,po_vendors pv
--                             ,po_vendor_sites_all pvs
--                             ,ap_terms payterm
--                             ,org_organization_definitions org
--                    where rsh.receipt_num=p_grn_number --'20160001447'
--                        and poh.segment1=p_po_number
--                        and rsh.shipment_header_id=rsl.shipment_header_id
--                        and rsl.shipment_header_id=rt.shipment_header_id
--                        and rsl.shipment_line_id=rt.shipment_line_id
--                        and rt.source_document_code='PO'
--                        and rt.transaction_type='RECEIVE'
--                        and receipt_source_code='VENDOR'
--                        and rsl.item_id=item.inventory_item_id(+)
--                        and rt.po_header_id=pll.po_header_id
--                        and rt.po_line_id=pll.po_line_id
--                        and rt.po_line_location_id=pd.line_location_id
--                        and rt.po_distribution_id=pd.po_distribution_id
--                        and poh.po_header_id=pol.po_header_id
--                        and pol.po_header_id=pll.po_header_id
--                        and pol.po_line_id=pll.po_line_id
--                        and pll.po_header_id=pd.po_header_id
--                        and pll.po_line_id=pd.po_line_id
--                        and pll.line_location_id=pd.line_location_id
--                        and poh.vendor_id=pv.vendor_id
--                        and poh.vendor_id=pvs.vendor_id
--                        and poh.vendor_site_id=pvs.vendor_site_id
--                        and pvs.terms_id=payterm.term_id
--                        and pll.ship_to_organization_id=org.organization_id
--                    group by rsh.receipt_num,--rt.*
--                               'DTAC',-- Company Code
--                               poh.segment1,
--                               pol.line_num,
--                               rsl.shipment_line_status_code,
--                               rsl.line_num,
--                                item.item_code,
--                                rsl.item_description,
--                                rsl.quantity_received, -- if rsl.destination_type_code='INVENTORY'  then qty if EXPENSE THEN AMOUNT
--                                rsl.unit_of_measure,
--                                rsl.quantity_received,
--                                poh.currency_code,
--                                pv.segment1,
--                                pvs.vendor_site_code,
--                                payterm.name,
--                                 org.organization_code;

                          if o_grn_infomation.count =0 then
                              o_err_code  := 'INVHUB001-003';
                              o_err_mesg := 'GRN and PO Not Match';
                          else
                              for i in 1 ..  o_grn_infomation.count  loop
                                   l_sum_grn := nvl(l_sum_grn,0) + nvl(o_grn_infomation(i).GRN_QTY,0);

                              end loop;
                              dbms_output.put_line('sum grn='||l_sum_grn);
                              for i in 1 ..  o_grn_infomation.count  loop
                                   o_grn_infomation(i).TOTAL_GRN_AMOUNT := l_sum_grn;
                              end loop;

                          end if;

              end;
              end if;



         END;*/

    PROCEDURE readdata (p_filename IN VARCHAR2) IS
       destFile utl_file.file_type;
       l_path   dba_directories.directory_path%TYPE;
       l_file VARCHAR2(50);
       l_writetxt VARCHAR2(1000);
    BEGIN
       /*
       destFile := utl_file.fopen(g_default_inv_path,l_file,'R');

       IF utl_file.is_open(destFile) THEN

           FOR i IN c_paid_inv(p_date,p_data_source) LOOP
               l_writetxt := i.company_code||'|'||
                             i.invoice_number||'|'||
                             i.supplier_code||'|'||
                             i.due_date||'|'||
                             i.payment_status||'|'||
                             i.pv_number||'|'||
                             i.document_number||'|'||
                             i.ap_voucher_number||'|'||
                             i.Paygroup;
               debug_msg('LOG',l_writetxt);
               utl_file.put_line(destFile,l_writetxt);
           END LOOP;
           g_step := 'Close file';
           utl_file.fclose(destFile);
       ELSE
          debug_msg('LOG','Couldn''t OPEN FILE');
       END IF;*/
       NULL;

    END;

    PROCEDURE writedata (p_filename IN VARCHAR2) IS
       destFile utl_file.file_type;
       l_path   dba_directories.directory_path%TYPE;
       l_file VARCHAR2(50);
       l_writetxt VARCHAR2(1000);
    BEGIN
       /*
       destFile := utl_file.fopen(g_default_inv_path,l_file,'R');

       IF utl_file.is_open(destFile) THEN

           FOR i IN c_paid_inv(p_date,p_data_source) LOOP
               l_writetxt := i.company_code||'|'||
                             i.invoice_number||'|'||
                             i.supplier_code||'|'||
                             i.due_date||'|'||
                             i.payment_status||'|'||
                             i.pv_number||'|'||
                             i.document_number||'|'||
                             i.ap_voucher_number||'|'||
                             i.Paygroup;
               debug_msg('LOG',l_writetxt);
               utl_file.put_line(destFile,l_writetxt);
           END LOOP;
           g_step := 'Close file';
           utl_file.fclose(destFile);
       ELSE
          debug_msg('LOG','Couldn''t OPEN FILE');
       END IF;*/
       NULL;

    END;
   procedure log_DTINVHUB002(p_concurrent_id in number,p_filename IN VARCHAR2) is
     l_destFile utl_file.file_type;
     l_path   dba_directories.directory_path%TYPE;
     l_file VARCHAR2(100);
     l_writetxt VARCHAR2(1000);
     TYPE r_log_detail  IS RECORD(log_txt VARCHAR2(1000));
     TYPE t_log_detail  IS TABLE OF r_log_detail INDEX BY BINARY_INTEGER;
     l_log_detail t_log_detail;
     CURSOR c_log_detail(p_req_id IN NUMBER) IS
                  SELECT DISTINCT level_type||'|'||
                         error_type||'|'||
                         ap_invoice||'|'||
                         ap_invoie_line||'|'||
                         po_number||'|'||
                         po_line_number||'|'||
                         receipt_number||'|'||
                         receipt_line_num||'|'||
                         pr_number||'|'||
                         pr_line_number||'|'||
                         match_option||'|'||
                         vendor_number||'|'||
                         processing_date||'|'||
                         processing_time||'|'||
                         DECODE(ERROR_CODE,NULL,'Completed','Error')||'|'||
                         ERROR_CODE||'|'||
                         error_message log_txt
                    from DTINVHUB_LOG_DETAIL
                  WHERE request_id=p_req_id;
   BEGIN


      g_step := 'in log_DTINVHUB002';
      debug_msg('LOG',g_step);
      -- Write log for fxth
      l_path := get_dir_path(p_oracle_path => g_default_inv_monitor_path);
      debug_msg('LOG','l_path :'||l_path);
      g_step := 'create file';
      debug_msg('LOG',g_step);
      l_file := p_filename||TO_CHAR(SYSDATE,'YYYYMMDD_HHMI')||'.txt';


       g_step := 'open file :'||l_file;
       debug_msg('LOG',g_step);
       debug_msg('LOG','g_default_inv_monitor_path:'||g_default_inv_monitor_path);
       l_destFile := UTL_FILE.FOPEN_NCHAR(g_default_inv_monitor_path,l_file,'A');

      IF utl_file.is_open(l_destFile) THEN
         --Write Header section
         SELECT level_type||'|'||
                 request_id||'|'||
                 program_name||'|'||
                 process_date||'|'||
                 processing_type||'|'||
                 processing_date||'|'||
                 record_count_completion||'|'||
                 record_count_error
            INTO l_writetxt
            FROM dtinvhub_log_summary
          WHERE request_id=p_concurrent_id;
          debug_msg('LOG',l_writetxt);
         --  utl_file.put_nchar(l_destfile,l_writetxt||'\r');
          -- utl_file.new_line(l_destfile,1);
          UTL_FILE.PUTF_NCHAR(l_destFile,l_writetxt||chr(10));
          UTL_FILE.FFLUSH(l_destFile);
          --Write detail section
         OPEN c_log_detail(p_concurrent_id);
         FETCH c_log_detail BULK COLLECT INTO l_log_detail;

         IF  l_log_detail.count > 0 THEN
             FOR i IN 1 ..l_log_detail.count LOOP --c_log_detail(p_concurrent_id) LOOP
                 l_writetxt := l_log_detail(i).log_txt;--i.log_txt;
                 debug_msg('LOG',l_writetxt);
                 debug_msg('LOG','i='||i);
                 debug_msg('LOG','l_log_detail.count='||l_log_detail.count);

                    UTL_FILE.PUTF_NCHAR(l_destFile,l_writetxt||chr(10));
                    UTL_FILE.FFLUSH(l_destFile);

             END LOOP;
         END IF;
         g_step := 'Close file';
         utl_file.fclose(l_destFile);
         l_log_detail.delete;
         debug_msg('LOG',g_step);
         CLOSE c_log_detail;
     ELSE
        debug_msg('LOG','Couldn''t OPEN FILE');
     END IF;
     

     -- Write log for splunk
      l_path := get_dir_path(p_oracle_path => g_default_splunk_log);
      debug_msg('LOG','l_path :'||l_path);
      g_step := 'create file';
      debug_msg('LOG',g_step);
      l_file := p_filename||TO_CHAR(SYSDATE,'YYYYMMDD_HHMI')||'.txt';


       g_step := 'open file :'||l_file;
       debug_msg('LOG',g_step);
       debug_msg('LOG','g_default_inv_monitor_path:'||g_default_splunk_log);
       l_destFile := UTL_FILE.FOPEN_NCHAR(g_default_splunk_log,l_file,'A');

      IF utl_file.is_open(l_destFile) THEN
         --Write Header section
         SELECT level_type||'|'||
                 request_id||'|'||
                 program_name||'|'||
                 process_date||'|'||
                 processing_type||'|'||
                 processing_date||'|'||
                 record_count_completion||'|'||
                 record_count_error
            INTO l_writetxt
            FROM dtinvhub_log_summary
          WHERE request_id=p_concurrent_id;
          debug_msg('LOG',l_writetxt);
         --  utl_file.put_nchar(l_destfile,l_writetxt||'\r');
          -- utl_file.new_line(l_destfile,1);
          UTL_FILE.PUTF_NCHAR(l_destFile,l_writetxt||chr(10));
          UTL_FILE.FFLUSH(l_destFile);
          --Write detail section
         OPEN c_log_detail(p_concurrent_id);
         FETCH c_log_detail BULK COLLECT INTO l_log_detail;

         IF  l_log_detail.count > 0 THEN
             FOR i IN 1 ..l_log_detail.count LOOP --c_log_detail(p_concurrent_id) LOOP
                 l_writetxt := l_log_detail(i).log_txt;--i.log_txt;
                 debug_msg('LOG',l_writetxt);
                 debug_msg('LOG','i='||i);
                 debug_msg('LOG','l_log_detail.count='||l_log_detail.count);

                    UTL_FILE.PUTF_NCHAR(l_destFile,l_writetxt||chr(10));
                    UTL_FILE.FFLUSH(l_destFile);

             END LOOP;
         END IF;
         g_step := 'Close file';
         utl_file.fclose(l_destFile);
         l_log_detail.delete;
         debug_msg('LOG',g_step);
         CLOSE c_log_detail;
     ELSE
        debug_msg('LOG','Couldn''t OPEN FILE');
     END IF;

     EXCEPTION WHEN OTHERS THEN
       debug_msg('LOG','Error in step '||g_step ||' : '||sqlerrm);

   end;

   procedure log_DTINVHUB003(p_concurrent_id in number,p_filename IN VARCHAR2) IS
     l_destFile utl_file.file_type;
     l_path   dba_directories.directory_path%TYPE;
     l_file VARCHAR2(100);
     l_writetxt VARCHAR2(1000);



     CURSOR c_log_detail(p_req_id IN NUMBER) IS
                  SELECT DISTINCT
                         level_type||'|'||
                         error_type||'|'||
                         ap_invoice||'|'||
                         ap_invoie_line||'|'||
                         po_number||'|'||
                         po_line_number||'|'||
                         receipt_number||'|'||
                         receipt_line_num||'|'||
                         pr_number||'|'||
                         pr_line_number||'|'||
                         match_option||'|'||
                         vendor_number||'|'||
                         processing_date||'|'||
                         processing_time||'|'||
                         DECODE(ERROR_CODE,NULL,'Completed','Error')||'|'||
                         ERROR_CODE||'|'||
                         error_message log_txt
                    from DTINVHUB_LOG_DETAIL
                  WHERE request_id=p_req_id
                  ORDER BY 1;
   BEGIN

       DBMS_LOCK.SLEEP(60);
      g_step := 'in log_DTINVHUB003';
      debug_msg('LOG',g_step);
      -- LOG FOR FXTH
      l_path := get_dir_path(p_oracle_path => g_default_inv_monitor_path);
      debug_msg('LOG','l_path :'||l_path);
      g_step := 'create file';
      debug_msg('LOG',g_step);
      l_file := p_filename||TO_CHAR(SYSDATE,'YYYYMMDD_HHMI')||'.txt';


       g_step := 'open file :'||l_file;
       debug_msg('LOG',g_step);
         debug_msg('LOG','g_default_inv_monitor_path:'||g_default_inv_monitor_path);
      l_destFile := UTL_FILE.FOPEN_NCHAR(g_default_inv_monitor_path,
                                   l_file,'A');



      IF utl_file.is_open(l_destFile) THEN
         --Write Header section
         debug_msg('LOG','Start write header');
             SELECT level_type||'|'||
                 request_id||'|'||
                 program_name||'|'||
                 process_date||'|'||
                 processing_type||'|'||
                 processing_date||'|'||
                 record_count_completion||'|'||
                 record_count_error
            INTO l_writetxt
            FROM dtinvhub_log_summary
          WHERE request_id=p_concurrent_id;
          debug_msg('LOG',l_writetxt);
          UTL_FILE.PUT_LINE_NCHAR(l_destFile,l_writetxt);
          UTL_FILE.FFLUSH(l_destFile);

          --Write detail section

         FOR i IN c_log_detail(p_concurrent_id) LOOP
             l_writetxt := i.log_txt;
             debug_msg('LOG',l_writetxt);
             UTL_FILE.PUT_LINE_NCHAR(l_destFile,l_writetxt);
             UTL_FILE.FFLUSH(l_destFile);
        --     UTL_FILE.FFLUSH(l_destFile);



         END LOOP;
         g_step := 'Close file';
         utl_file.fclose(l_destFile);
     ELSE
        debug_msg('LOG','Couldn''t OPEN FILE');
     END IF;

      -- LOG FOR  SPLUNK
      l_path := get_dir_path(p_oracle_path => g_default_splunk_log);
      debug_msg('LOG','l_path :'||l_path);
      g_step := 'create file';
      debug_msg('LOG',g_step);
      l_file := p_filename||TO_CHAR(SYSDATE,'YYYYMMDD_HHMI')||'.txt';


       g_step := 'open file :'||l_file;
       debug_msg('LOG',g_step);
         debug_msg('LOG','g_default_splunk_log:'||g_default_splunk_log);
      l_destFile := UTL_FILE.FOPEN_NCHAR(g_default_splunk_log,
                                   l_file,'A');



      IF utl_file.is_open(l_destFile) THEN
         --Write Header section
         debug_msg('LOG','Start write header');
             SELECT level_type||'|'||
                 request_id||'|'||
                 program_name||'|'||
                 process_date||'|'||
                 processing_type||'|'||
                 processing_date||'|'||
                 record_count_completion||'|'||
                 record_count_error
            INTO l_writetxt
            FROM dtinvhub_log_summary
          WHERE request_id=p_concurrent_id;
          debug_msg('LOG',l_writetxt);
          UTL_FILE.PUT_LINE_NCHAR(l_destFile,l_writetxt);
          UTL_FILE.FFLUSH(l_destFile);

          --Write detail section

         FOR i IN c_log_detail(p_concurrent_id) LOOP
             l_writetxt := i.log_txt;
             debug_msg('LOG',l_writetxt);
             UTL_FILE.PUT_LINE_NCHAR(l_destFile,l_writetxt);
             UTL_FILE.FFLUSH(l_destFile);
        --     UTL_FILE.FFLUSH(l_destFile);



         END LOOP;
         g_step := 'Close file';
         utl_file.fclose(l_destFile);
     ELSE
        debug_msg('LOG','Couldn''t OPEN FILE');
     END IF;


     EXCEPTION WHEN OTHERS THEN
       debug_msg('LOG','Error in step '||g_step ||' : '||sqlerrm);


   end;

   procedure log_DTINVHUB004(p_concurrent_id in number,p_filename IN VARCHAR2) is
     l_destFile utl_file.file_type;
     l_path   dba_directories.directory_path%TYPE;
     l_file VARCHAR2(100);
     l_writetxt VARCHAR2(1000);


     CURSOR c_log_detail(p_req_id IN NUMBER) IS
                  SELECT  LEVEL_TYPE||'|'||
                          ERROR_TYPE||'|'||
                          AP_INVOICE||'|'||
                          VENDOR_NUMBER||'|'||
                          DUE_DATE||'|'||
                          PAYMENT_STATUS||'|'||
                          PV_NUMBER||'|'||
                          DOCUMENT_NUMBER||'|'||
                          AP_VOUCHER_NUMBER||'|'||
                          PAYGROUP||'|'||
                          PROCESSING_DATE||'|'||
                          PROCESSING_TIME||'|'||
                          'Completed' log_txt
                   from DTINVHUB_LOG_DETAIL
                  WHERE request_id=p_req_id;
                                
   BEGIN

      DBMS_LOCK.SLEEP(60);
      g_step := 'in log_DTINVHUB004';
      debug_msg('LOG',g_step);
 
      -- LOG FOR  SPLUNK
      l_path := get_dir_path(p_oracle_path => g_default_splunk_log);
      debug_msg('LOG','l_path :'||l_path);
      g_step := 'create file';
      debug_msg('LOG',g_step);
      l_file := p_filename||TO_CHAR(SYSDATE,'YYYYMMDD_HHMI')||'.txt';


       g_step := 'open file :'||l_file;
       debug_msg('LOG',g_step);
       debug_msg('LOG','g_default_splunk_log:'||g_default_splunk_log);
       l_destFile := UTL_FILE.FOPEN_NCHAR(g_default_splunk_log,l_file,'A');



      IF utl_file.is_open(l_destFile) THEN
         --Write Header section
         debug_msg('LOG','Start write header');
         
                 
        SELECT level_type||'|'||
               request_id||'|'||
               program_name||'|'||
               process_date||'|'||
               processing_type||'|'||
               processing_date||'|'||
               record_count_completion||'|'||
               record_count_error
          INTO l_writetxt
         from DTINVHUB_LOG_summary
        WHERE request_id=p_concurrent_id;
         
         
         
          debug_msg('LOG',l_writetxt);
          UTL_FILE.PUT_LINE_NCHAR(l_destFile,l_writetxt);
          UTL_FILE.FFLUSH(l_destFile);

          --Write detail section

         FOR i IN c_log_detail(p_concurrent_id) LOOP
             l_writetxt := i.log_txt;
             debug_msg('LOG',l_writetxt);
             UTL_FILE.PUT_LINE_NCHAR(l_destFile,l_writetxt);
             UTL_FILE.FFLUSH(l_destFile);
        --     UTL_FILE.FFLUSH(l_destFile);



         END LOOP;
         g_step := 'Close file';
         utl_file.fclose(l_destFile);
     ELSE
        debug_msg('LOG','Couldn''t OPEN FILE');
     END IF;


     EXCEPTION WHEN OTHERS THEN
       debug_msg('LOG','Error in step '||g_step ||' : '||sqlerrm);
   end;

   procedure log_DTINVHUB005(p_concurrent_id in number,p_filename IN VARCHAR2) is
     l_destFile utl_file.file_type;
     l_path   dba_directories.directory_path%TYPE;
     l_file VARCHAR2(100);
     l_writetxt VARCHAR2(1000);


     CURSOR c_log_detail(p_req_id IN NUMBER) IS
                  SELECT level_type||'|'||
                         error_type||'|'||
                         vendor_number||'|'||
                         vendor_name||'|'||
                         vendor_site_name||'|'||
                         payment_term||'|'||
                         SYSDATE||'|'||
                         processing_date||'|'||
                         processing_time||'|'||
                         'Completed' log_txt
                   from DTINVHUB_LOG_DETAIL
                  WHERE request_id=p_req_id;
                                
   BEGIN

      DBMS_LOCK.SLEEP(60);
      g_step := 'in log_DTINVHUB005';
      debug_msg('LOG',g_step);
 
      -- LOG FOR  SPLUNK
      l_path := get_dir_path(p_oracle_path => g_default_splunk_log);
      debug_msg('LOG','l_path :'||l_path);
      g_step := 'create file';
      debug_msg('LOG',g_step);
      l_file := p_filename||TO_CHAR(SYSDATE,'YYYYMMDD_HHMI')||'.txt';


       g_step := 'open file :'||l_file;
       debug_msg('LOG',g_step);
         debug_msg('LOG','g_default_splunk_log:'||g_default_splunk_log);
      l_destFile := UTL_FILE.FOPEN_NCHAR(g_default_splunk_log,
                                   l_file,'A');



      IF utl_file.is_open(l_destFile) THEN
         --Write Header section
         debug_msg('LOG','Start write header');
         
                 
        SELECT level_type||'|'||
               request_id||'|'||
               program_name||'|'||
               process_date||'|'||
               processing_type||'|'||
               processing_date||'|'||record_count_completion||'|'||
               record_count_error
          INTO l_writetxt
         from DTINVHUB_LOG_summary
        WHERE request_id=p_concurrent_id;
         
         
         
          debug_msg('LOG',l_writetxt);
          UTL_FILE.PUT_LINE_NCHAR(l_destFile,l_writetxt);
          UTL_FILE.FFLUSH(l_destFile);

          --Write detail section

         FOR i IN c_log_detail(p_concurrent_id) LOOP
             l_writetxt := i.log_txt;
             debug_msg('LOG',l_writetxt);
             UTL_FILE.PUT_LINE_NCHAR(l_destFile,l_writetxt);
             UTL_FILE.FFLUSH(l_destFile);
        --     UTL_FILE.FFLUSH(l_destFile);



         END LOOP;
         g_step := 'Close file';
         utl_file.fclose(l_destFile);
     ELSE
        debug_msg('LOG','Couldn''t OPEN FILE');
     END IF;


     EXCEPTION WHEN OTHERS THEN
       debug_msg('LOG','Error in step '||g_step ||' : '||sqlerrm);

   end;

   PROCEDURE LOG_MONITOR( errbuf       OUT VARCHAR2,
                          errcode      OUT VARCHAR2,
                          p_program_name  in varchar2,
                          p_concurrent_id in number,
                        -- p_folder_name   in varchar2,
                          p_debug_flag IN VARCHAR2
                         )
                         is
      l_po_filename varchar2(50):= 'INVHUB_POINBOUND_';
      l_ap_filename varchar2(50):= 'INVHUB_APINVBOUND_';
      l_paid_inv_filename varchar2(50) := 'INVHUB_PAIDINVOUTBOUND_';
      l_supplier    varchar2(50) := 'INVHUB_SUPPLIEROUTBOUND_';
      l_webservice  varchar2(50) := 'INVHUB_GRN_';
   BEGIN
      g_debug_flag := p_debug_flag;
      if p_program_name = 'DTINVHUB002' then
         log_DTINVHUB002(p_concurrent_id,l_po_filename);
      elsif p_program_name = 'DTINVHUB003' then
         log_DTINVHUB003(p_concurrent_id,l_ap_filename);
      elsif p_program_name = 'DTINVHUB004' then
         log_DTINVHUB004(p_concurrent_id,l_paid_inv_filename);
      elsif p_program_name = 'DTINVHUB005' then
         log_DTINVHUB005(p_concurrent_id,l_supplier);
      elsif p_program_name = 'DTINVHUB001' then
         null;
      end if;
   end;
END tac_po_invoicehub_pkg;