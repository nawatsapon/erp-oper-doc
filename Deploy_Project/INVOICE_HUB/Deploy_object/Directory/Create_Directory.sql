create directory INVHUB_VENDOR_INBOX as '/MP/GERP_PROD/INVOICEHUB/OUTPUT/SUPPLIER/INBOX';
grant read on directory  INVHUB_VENDOR_INBOX to public;
grant write on directory  INVHUB_VENDOR_INBOX to public;

create directory INVHUB_PV_INBOX as ' /MP/GERP_PROD/INVOICEHUB/OUTPUT/BACKINVOICE/INBOX';
grant read on directory  INVHUB_PV_INBOX to public;
grant write on directory  INVHUB_PV_INBOX to public;

create directory INVHUB_SPLUNK as ' /var/tmp/INVOICEHUB/MONITOR';
grant read on directory  INVHUB_SPLUNK to public;
grant write on directory  INVHUB_SPLUNK to public;

create directory INVHUB_RESULT_INBOX as ' /MP/GERP_PROD/INVOICEHUB/INPUT/APINVOICE/RESULT';
grant read on directory  INVHUB_RESULT_INBOX to public;
grant write on directory  INVHUB_RESULT_INBOX to public;

select * from SYS.DBA_DIRECTORIES where DIRECTORY_NAME like '%INVHUB%';