create or replace PACKAGE BODY DT_INVOICEHUB_PKG2
AS

/******************************************************************************************************************************************************
  Package Name        :DT_INVOICEHUB_PKG
  Description         :This package is modified for moving pr data from source concession to destination concession

  Revisions:
  Ver             Date                Author                      Description
  ---------       ---------           ---------------             ----------------------------------
  1.0             25/07/2016        Suttisak Uamornlert         Package creation
 *****************************************************************************************************************************************************/
    /*-------------------------------*/
    /*  parameter p_debug_type       */
    /*  log -- FND_FILE.LOG          */
    /*  output -- FND_FILE.OUTPUT    */
    /*-------------------------------*/
    PROCEDURE DEBUG_MSG(p_debug_type IN VARCHAR2,
                        p_msg        IN VARCHAR2) IS
    BEGIN
       IF p_debug_type='LOG' AND g_debug_flag='Y' THEN
          fnd_file.put_line(fnd_file.LOG,p_msg);
       ELSIF p_debug_type='OUTPUT'  THEN
          fnd_file.put_line(fnd_file.OUTPUT,p_msg);
       END IF;

    END DEBUG_MSG;

    PROCEDURE INSERT_ERROR(PO_ERROR IN OUT T_PR_STR_ERROR,--l_po_error IN OUT NUMBER,

                           P_KEY1 IN VARCHAR2,P_KEY2 IN VARCHAR2,P_ERR_MSG IN VARCHAR2) IS
    BEGIN
       g_po_error:=g_po_error+1;
       PO_ERROR(g_po_error).module_type:= 'PO';
       PO_ERROR(g_po_error).rownumber  := g_po_error;
       PO_ERROR(g_po_error).p_key1     := P_KEY1;
       PO_ERROR(g_po_error).p_key2     := P_KEY2;
       PO_ERROR(g_po_error).error_msg  := P_ERR_MSG;
    END;


    PROCEDURE FETCH_PO_DATA(p_group_name in varchar2,
                                                 p_po_header   in out t_po_header) IS
    BEGIN
         g_step :='Fetch po data';
         debug_msg('LOG',g_step);
         select distinct group_name,
                             source,
                             vendor_num,
                             vendor_site_code,
                             doc_category_code,
                             pr_number
             bulk collect into p_po_header
            from tac_invhub_invoice_stg
          where upper(group_name) = upper(p_group_name)  --'FXTH_2W_20160725_1633'
              and nvl(interface_po_flag,'N')='Y'
            order by group_name,pr_number;





        debug_msg('LOG','Total record inserted in collection='||sql%rowcount);
    END;

    PROCEDURE CALL_STANDARD_PO_INTERFACE(p_status IN VARCHAR2,
                                         p_po_type IN VARCHAR2,
                                         p_source_org_id IN NUMBER,
                                         p_dest_org_id IN NUMBER,
                                         p_conc_request_id IN NUMBER,
                                         p_orig_trans IN VARCHAR2) IS
      l_poimport_req_id NUMBER;
      l_finished BOOLEAN;
      l_phase       varchar2 (100);
      l_status      varchar2 (100);
      l_dev_phase   varchar2 (100);
      l_dev_status  varchar2 (100);
      l_message     varchar2 (100);
      l_check_data  varchar2(1);
    BEGIN
      -- fnd_request.set_org_id(p_dest_org_id);
       --submit po import
       l_poimport_req_id := fnd_request.submit_request (application  => 'PO',
                              program       => 'POXPOPDOI',
                              sub_request   => FALSE,
                              argument1     => NULL,
                              argument2     => p_po_type,--'STANDARD',
                              argument3     => NULL,
                              argument4     => 'N',
                              argument5     => NULL,
                              argument6     => p_status,
                              argument7     => null,
                              argument8     => p_conc_request_id,
                              argument9     => null,
                              argument10    => null,
                              argument11    => null,
                              argument12    => null,
                              argument13    => null,
                              argument14    => null
                              );
       COMMIT;

       debug_msg('LOG','Import request id='||l_poimport_req_id);


       --Waiting until Requisition Import complete.
       LOOP
       l_finished := fnd_concurrent.wait_for_request ( request_id => l_poimport_req_id
                                                 ,interval   => 5
                                                 ,max_wait   => 0
                                                 ,phase      => l_phase
                                                 ,status     => l_status
                                                 ,dev_phase  => l_dev_phase
                                                 ,dev_status => l_dev_status
                                                 ,message    => l_message );

       EXIT WHEN l_finished;
       DBMS_LOCK.SLEEP(10);
       END loop;
    END;




  FUNCTION     CHECK_PO (P_PO_NUMBER IN VARCHAR2) RETURN BOOLEAN IS
             l_check_po_number varchar2(1);
       BEGIN
             SELECT DISTINCT 'Y'
                 INTO l_check_po_number
                FROM po_headers_all poh,
                     po_releases_all po_release
               WHERE poh.segment1||DECODE(po_release.release_num,NULL,NULL,'-'||po_release.release_num)=p_po_number
                 AND poh.po_header_id=po_release.po_header_id(+)
                 AND poh.org_id =po_release.org_id(+);

              return(true);
              exception when no_data_found then
              return(false);
       END;

       FUNCTION     CHECK_PO_INCOMPANY (P_PO_NUMBER IN VARCHAR2,P_COMP_ID IN NUMBER) RETURN BOOLEAN IS
             l_check_po_number varchar2(1);
       BEGIN
             SELECT DISTINCT 'Y'
               INTO l_check_po_number
               FROM po_headers_all poh,
                     po_releases_all po_release,
                    AP_REPORTING_ENTITIES_all are
              WHERE  poh.segment1||DECODE(po_release.release_num,NULL,NULL,'-'||po_release.release_num)=p_po_number
                AND poh.po_header_id=po_release.po_header_id(+)
                AND poh.org_id =po_release.org_id(+)
                AND poh.org_id=are.org_id
                AND are.tax_entity_id=P_COMP_ID;

              return(true);
              exception when no_data_found then
              return(false);
       END;

       FUNCTION CHECK_GRN(P_GRN_NUMBER IN VARCHAR2) RETURN BOOLEAN IS
             l_check_grn varchar2(1);
       BEGIN
             SELECT DISTINCT 'Y'
                 INTO l_check_grn
                 FROM rcv_shipment_headers
                WHERE receipt_num=P_GRN_NUMBER;
            RETURN(TRUE);
             exception when no_data_found then
             return(false);
       END;

       FUNCTION CHECK_COMPANY_CODE(P_COMP_CODE IN VARCHAR2) RETURN NUMBER IS
             l_comp_id NUMBER;
       BEGIN
          SELECT TAX_ENTITY_ID
            INTO l_comp_id
            FROM AP_REPORTING_ENTITIES_all
           WHERE entity_name = DECODE(P_COMP_CODE,'DTAC','TAC - HO','DTN','DTAC-N','PSB','PAYSBUY')
             AND tax_entity_id in (10000,10050,10094);

            RETURN(l_comp_id);
             exception when no_data_found then
             return(-1);
       END;

       PROCEDURE  getGRNInfomation(grn_number in varchar2,
                                                 company_code in  varchar2,
                                                 po_number in varchar2,
                                                 result  out varchar2,
                                                 error_code out varchar2,
                                                 error_mesg out varchar2,
                                                 grn_infomation out TAC_GRN_INFO_TABLE--t_grn_info
                                                 ) IS

       CURSOR C1(p_comp_code_id IN NUMBER,
                 p_po_number IN VARCHAR2,
                 p_grn_number IN varchar2
                )
                 IS
       select  rsh.receipt_num GRN_NUMBER,--rt.*
                              decode(are.tax_entity_id,10000,'DTAC',10050,'DTN',10094,'PSB')  Company_Code,
                              poh.segment1||DECODE(po_release.release_num,NULL,NULL,'-'||po_release.release_num) PO_Number,
                              pol.line_num  PO_Line_Number,
                              rsl.shipment_line_status_code GRN_STATUS,
                              rsl.line_num GRN_LINE_NUMBER,
                              item.item_code ITEM_CODE,
                              rsl.item_description LINE_DESCRIPTION,
                              rsl.quantity_received GRN_QTY, -- if rsl.destination_type_code='INVENTORY'  then qty if EXPENSE THEN AMOUNT
                              rsl.unit_of_measure GRN_UOM,
                              (nvl(rsl.quantity_received,0)*nvl(pol.unit_price,0)) GRN_AMOUNT,--rsl.quantity_received GRN_AMOUNT,
                              poh.currency_code GRN_CURRENCY,
                              0 TOTAL_GRN_AMOUNT,
                              org.organization_code ORG_CODE,
                              pv.segment1 VENDOR_NUMBER,
                              pvs.vendor_site_code VENDOR_SITE_CODE,
                              payterm.name PAYMENT_TERM,
                              sum(pll.quantity-pll.quantity_received-pll.quantity_rejected-pll.quantity_cancelled)*nvl(pol.unit_price,0) PO_Remaining_Amount
                      -- bulk collect into o_grn_infomation
                      from rcv_shipment_headers rsh
                             ,rcv_shipment_lines rsl
                             ,rcv_transactions rt
                            ,(select msi.inventory_item_id,msi.segment1 ITEM_CODE
                                from mtl_system_items_b msi,mtl_parameters mp
                              where msi.organization_id=mp.organization_id
                                  and msi.organization_id=mp.master_organization_id) ITEM
                             ,po_headers_all poh
                             ,PO_RELEASES_ALL po_release
                             ,po_lines_all pol
                             ,po_line_locations_all pll
                             ,po_distributions_all pd
                             ,po_vendors pv
                             ,po_vendor_sites_all pvs
                             ,ap_terms payterm
                             ,org_organization_definitions org
                             ,AP_REPORTING_ENTITIES_all are
                    where rsh.receipt_num=p_grn_number --'20160001447'
                        --and poh.segment1=p_po_number
                        AND poh.segment1||DECODE(po_release.release_num,NULL,NULL,'-'||po_release.release_num)=p_po_number
                        AND poh.po_header_id=po_release.po_header_id(+)
                        AND poh.org_id=po_release.org_id(+)
                       -- AND nvl(po_release.release_num,'0')=p_release_num
                        AND are.tax_entity_id = p_comp_code_id
                        and rsh.shipment_header_id=rsl.shipment_header_id
                        and rsl.shipment_header_id=rt.shipment_header_id
                        and rsl.shipment_line_id=rt.shipment_line_id
                        and rt.source_document_code='PO'
                        and rt.transaction_type='RECEIVE'
                        and receipt_source_code='VENDOR'
                        and rsl.item_id=item.inventory_item_id(+)
                        and rt.po_header_id=pll.po_header_id
                        and rt.po_line_id=pll.po_line_id
                        and rt.po_line_location_id=pd.line_location_id
                        and rt.po_distribution_id=pd.po_distribution_id
                        and poh.po_header_id=pol.po_header_id
                        and pol.po_header_id=pll.po_header_id
                        and pol.po_line_id=pll.po_line_id
                        and pll.po_header_id=pd.po_header_id
                        and pll.po_line_id=pd.po_line_id
                        and pll.line_location_id=pd.line_location_id
                        and poh.vendor_id=pv.vendor_id
                        and poh.vendor_id=pvs.vendor_id
                        and poh.vendor_site_id=pvs.vendor_site_id
                        and pvs.terms_id=payterm.term_id
                        and pll.ship_to_organization_id=org.organization_id
                        AND org.OPERATING_UNIT = are.ORG_ID
                    group by rsh.receipt_num,--rt.*
                               decode(are.tax_entity_id,10000,'DTAC',10050,'DTN',10094,'PSB'),-- Company Code
                               poh.segment1||DECODE(po_release.release_num,NULL,NULL,'-'||po_release.release_num),--poh.segment1,
                               pol.line_num,
                               rsl.shipment_line_status_code,
                               rsl.line_num,
                                item.item_code,
                                rsl.item_description,
                                (nvl(rsl.quantity_received,0)*nvl(pol.unit_price,0)),--rsl.quantity_received, -- if rsl.destination_type_code='INVENTORY'  then qty if EXPENSE THEN AMOUNT
                                rsl.unit_of_measure,
                                rsl.quantity_received,
                                poh.currency_code,
                                pv.segment1,
                                pvs.vendor_site_code,
                                payterm.name,
                                 org.organization_code,
                                 nvl(pol.unit_price,0);
              l_sum_grn  number;
             -- o_err_code  varchar2(2000);
             -- o_err_mesg  varchar2(2000);

             P_GRN_INFO        TAC_GRN_INFO;
             p_company_id      NUMBER;
             l_destFile utl_file.file_type;
             l_po_number po_headers_all.segment1%TYPE;
             l_release_num VARCHAR2(5);
             l_file VARCHAR2(100);
             l_writetxt VARCHAR2(1000);
             l_default_splunk_log    VARCHAR2(50) :='INVHUB_SPLUNK';
            -- o_grn_infomation  TAC_GRN_INFO_TABLE := TAC_GRN_INFO_TABLE ();
             i number := 0;
             l_starttime TIMESTAMP;
             l_endtime   TIMESTAMP;
         BEGIN
              SELECT current_timestamp
                INTO l_starttime
                FROM dual;
              error_code := null;
              error_mesg := null;
              grn_infomation := TAC_GRN_INFO_TABLE ();

              -- validate parameter
               if (grn_number is null) or (company_code is null) or (po_number is null) then
                  dbms_output.put_line('null parameter');
                 result := 'FAIL';
                 error_code  := 'INVHUB001-005';
                 error_mesg := 'Missing Parameter';
               end if;

              -- validate   company code
              if error_code is null  THEN
                 p_company_id:=CHECK_COMPANY_CODE(UPPER(company_code));
                 IF p_company_id=-1 THEN
                    result := 'FAIL';
                    error_code  := 'INVHUB001-006';
                    error_mesg := 'Not found company code '||company_code;
                 END if;
              end if;
              -- validate  existing po in system
              if error_code is null and (not CHECK_PO (po_number)) then
                result := 'FAIL';
                error_code  := 'INVHUB001-002';
                error_mesg := 'Not found PO';
              end if;
              -- validate  existing po base on company in system
              if error_code is null and (not CHECK_PO_INCOMPANY (po_number,p_company_id)) then
                result := 'FAIL';
                error_code  := 'INVHUB001-007';
                error_mesg := 'Not found PO in company code '||company_code;
              end if;

              -- validate  existing grn in system
              if error_code is null and (not check_grn(grn_number)) then
                 result := 'FAIL';
                 error_code  := 'INVHUB001-001';
                 error_mesg := 'Not found GRN';
              end if;



              if error_code is null then
              begin



                FOR REC1 IN C1( p_company_id,po_number,grn_number )
                LOOP

                  i := i+1;


                  P_GRN_INFO :=TAC_GRN_INFO (NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
                  P_GRN_INFO.GRN_NUMBER := REC1.GRN_NUMBER;
                  P_GRN_INFO.Company_Code := REC1.Company_Code;
                  P_GRN_INFO.PO_Number := REC1.PO_Number;
                  P_GRN_INFO.PO_Line_Number := REC1.PO_Line_Number;
                  P_GRN_INFO.GRN_STATUS := REC1.GRN_STATUS;
                  P_GRN_INFO.GRN_LINE_NUMBER := REC1.GRN_LINE_NUMBER;

                  P_GRN_INFO.ITEM_CODE := REC1.ITEM_CODE;
                  P_GRN_INFO.LINE_DESCRIPTION := REC1.LINE_DESCRIPTION;
                  P_GRN_INFO.GRN_QTY := REC1.GRN_QTY;
                  P_GRN_INFO.GRN_UOM := REC1.GRN_UOM;
                  P_GRN_INFO.GRN_AMOUNT := round(REC1.GRN_AMOUNT,2);
                  P_GRN_INFO.GRN_CURRENCY := REC1.GRN_CURRENCY;

                  P_GRN_INFO.TOTAL_GRN_AMOUNT := round(REC1.TOTAL_GRN_AMOUNT,2);
                  P_GRN_INFO.ORG_CODE := REC1.ORG_CODE;
                  P_GRN_INFO.VENDOR_NUMBER := REC1.VENDOR_NUMBER;
                  P_GRN_INFO.VENDOR_SITE_CODE := REC1.VENDOR_SITE_CODE;
                  P_GRN_INFO.PAYMENT_TERM := REC1.PAYMENT_TERM;
                  P_GRN_INFO.PO_Remaining_Amount := round(REC1.PO_Remaining_Amount,2);

                  grn_infomation.EXTEND;
                  grn_infomation (i) := P_GRN_INFO;

                END LOOP;

                          if grn_infomation.count =0 then

                              error_code  := 'INVHUB001-003';
                              error_mesg := 'GRN and PO Not Match';
                          else
                              for i in 1 ..  grn_infomation.count  loop
                                   l_sum_grn := nvl(l_sum_grn,0) + nvl(grn_infomation(i).GRN_AMOUNT,0);--nvl(grn_infomation(i).GRN_QTY,0);

                              end loop;
                              dbms_output.put_line('sum grn='||l_sum_grn);
                              for i in 1 ..  grn_infomation.count  loop
                                   grn_infomation(i).TOTAL_GRN_AMOUNT := l_sum_grn;
                              end loop;

                          end if;

              end;
              end if;

              --GRN_Number|Company Code|PO Number|Process date|Processing time|Return result
              --|STATUS|ERRORCODE|ERRORMESSAGE

              ---------------------
              --INVHUB_GRN_ddmmyyyy.txt
              --grn_number in varchar2,
              --company_code in  varchar2,
              --po_number in varchar2,
              --l_starttime,
              --substr(to_char(l_endtime-l_starttime),12,12)
              SELECT current_timestamp
                INTO l_endtime
                FROM dual;

              l_file := 'INVHUB_GRN_'||TO_CHAR(SYSDATE,'YYYYMMDD')||'.txt';
              l_destFile := UTL_FILE.FOPEN_NCHAR(l_default_splunk_log,l_file,'A');

              IF utl_file.is_open(l_destFile) THEN
                 IF ERROR_CODE is NULL THEN
                     l_writetxt :=grn_number||'|'||
                                  company_code||'|'||
                                  po_number||'|'||
                                  to_char(l_starttime,'DD/MM/YYYY HH24:MI')||'|'||
                                  substr(to_char(l_endtime-l_starttime),12,12)||'|'||
                                  grn_infomation.count||'|'||
                                  'SUCCESS'||'|'||
                                  NULL||'|';
                 ELSE
                    l_writetxt :=grn_number||'|'||
                                  company_code||'|'||
                                  po_number||'|'||
                                  to_char(l_starttime,'DD/MM/YYYY HH24:MI')||'|'||
                                  substr(to_char(l_endtime-l_starttime),12,12)||'|'||
                                  grn_infomation.count||'|'||
                                  'FAILED'||'|'||
                                  error_code||'|'||
                                  error_mesg;
                 END IF;
                 UTL_FILE.PUTF_NCHAR(l_destFile,l_writetxt||chr(10));
                 utl_file.fclose(l_destFile);
              END IF;

              ---------------------


         END;


END DT_INVOICEHUB_PKG2;
