CREATE OR REPLACE PACKAGE BODY TAC_MONEY_COLLECT_GL_ITF IS

-- Author  : TM
-- Created : 28/08/2018
-- Purpose : Interface data from Money Collection into ERP

--error_open_file exception;
error_output_path	EXCEPTION;
error_file_name		EXCEPTION;

G_NO_ERROR		VARCHAR2(100)	:=	'No Error';

TYPE varchar_by_varchar_tabtyp IS TABLE OF VARCHAR2(1000) INDEX BY VARCHAR2(100);
TYPE varchar2_tabtyp IS TABLE OF VARCHAR2(4000) INDEX BY BINARY_INTEGER;
TYPE nested_varchar_rectyp IS RECORD
(company		VARCHAR2(100)
,filename		VARCHAR2(100)
,line_num		NUMBER
,err_code		VARCHAR2(100)
,err_desc		VARCHAR2(1000)
,err_detail		varchar2_tabtyp);
TYPE nested_varchar_tabtyp IS TABLE OF nested_varchar_rectyp INDEX BY BINARY_INTEGER;
CT_ERROR			varchar_by_varchar_tabtyp;

---------------------------------------------------------------------------
PROCEDURE conc_wait(p_conc_req_id NUMBER) IS
	phase		VARCHAR2(30);
	status		VARCHAR2(30);
	dev_phase	VARCHAR2(30);
	dev_status	VARCHAR2(30);
	message		VARCHAR2(1000);
BEGIN
	COMMIT;

	IF fnd_concurrent.wait_for_request(p_conc_req_id, 10, 0, phase, status, dev_phase, dev_status, message) THEN
		NULL;
	END IF;

END conc_wait;
---------------------------------------------------------------------------
PROCEDURE conc_wait (p_conc_req_id		NUMBER,
		p_phase			OUT NOCOPY VARCHAR2,
		p_status		OUT NOCOPY VARCHAR2,
		p_message		OUT NOCOPY VARCHAR2,
		p_max_wait			NUMBER DEFAULT 60	) IS

	v_phase			VARCHAR2(30);
	v_status		VARCHAR2(30);
	v_dev_phase		VARCHAR2(30);
	v_dev_status	VARCHAR2(30);
	v_message		VARCHAR2(1000);
	v_wait_complete	BOOLEAN;

BEGIN
	COMMIT;

	IF p_conc_req_id > 0 THEN

		v_wait_complete := fnd_concurrent.wait_for_request(request_id	=> p_conc_req_id
													,INTERVAL			=> 10
													,max_wait			=> p_max_wait
													,phase				=> v_phase		-- Out
													,status				=> v_status		-- Out
													,dev_phase			=> v_dev_phase	-- Out
													,dev_status			=> v_dev_status	-- Out
													,message			=> v_message);	-- Out
	END IF;

	p_phase := v_phase;
	p_status := v_status;
	p_message := v_message;

END conc_wait;
---------------------------------------------------------------------------
PROCEDURE write_log(p_msg VARCHAR2) IS
BEGIN

	fnd_file.put_line(fnd_file.log, p_msg);

EXCEPTION
	WHEN OTHERS THEN
		NULL;
END write_log;
---------------------------------------------------------------------------
PROCEDURE send_email
(i_smtp_server		IN	VARCHAR2	:=  'mail-gw.tac.co.th'
,i_smtp_port		IN	VARCHAR2	:=  25
,i_from_email		IN	VARCHAR2	:=  'ERPOperationSupport@dtac.co.th'
,i_to_email			IN	VARCHAR2	-- Separate by ,
,i_cc_email			IN	VARCHAR2	:=  NULL
,i_bcc_email		IN	VARCHAR2	:=  NULL
,i_subject			IN	VARCHAR2
,i_program_name		IN	VARCHAR2
,i_sob_id			IN	NUMBER
,i_file_path		IN	VARCHAR2
,i_filename_pattern	IN	VARCHAR2
,i_filename			IN	VARCHAR2
,i_delimiter		IN	VARCHAR2
,i_file_date		IN	VARCHAR2
,i_email_address	IN	VARCHAR2
,it_error			IN	nested_varchar_tabtyp
,i_start_time		IN	DATE
,i_end_time			IN	DATE
--,i_reply_email		IN	VARCHAR2	:=  'gl2anaplan_outb_itf@dtac.co.th'
,i_priority			IN	NUMBER	  :=  3   -- Normal priority
,io_status			IN OUT  VARCHAR2
)
IS
	C_ERROR				CONSTANT VARCHAR2(40)   :=  '$$ERROR$$';
	NEW_LINE_IN_MSG		CONSTANT VARCHAR2(10)   :=  '<BR>';
	v_msg_template		VARCHAR2(32000) :=
		'Program : ' || i_program_name || NEW_LINE_IN_MSG ||
		'  Parameter : ' || NEW_LINE_IN_MSG ||
		'    Set of Books ID : ' || i_sob_id || NEW_LINE_IN_MSG ||
		'    File Path : ' || i_file_path || NEW_LINE_IN_MSG ||
		'    File Name Pattern : ' || i_filename_pattern || NEW_LINE_IN_MSG ||
		'    File Name : ' || i_filename || NEW_LINE_IN_MSG ||
		'    Delimiter : ' || i_delimiter || NEW_LINE_IN_MSG ||
		'    File Date : ' || i_file_date || NEW_LINE_IN_MSG ||
		'    Email Address : ' || i_email_address || NEW_LINE_IN_MSG ||
		NEW_LINE_IN_MSG ||
		'  Concurrent Start : ' || TO_CHAR(i_start_time,'DD Mon YYYY HH24.MI.SS') || NEW_LINE_IN_MSG ||
		'  --------------------------------------------------------------------------' || NEW_LINE_IN_MSG ||
		'  Error' || NEW_LINE_IN_MSG ||
		'  --------------------------------------------------------------------------' || NEW_LINE_IN_MSG ||
		NEW_LINE_IN_MSG ||
		'  Error' || NEW_LINE_IN_MSG ||
		NEW_LINE_IN_MSG || '<TABLE BORDER=1>$$ERROR$$</TABLE>' || NEW_LINE_IN_MSG ||
		NEW_LINE_IN_MSG ||
		'  --------------------------------------------------------------------------' || NEW_LINE_IN_MSG ||
		'  Concurrent End : ' || TO_CHAR(i_end_time,'DD Mon YYYY HH24.MI.SS') || NEW_LINE_IN_MSG
		;
	v_msg				CLOB;

BEGIN
	io_status   :=  G_NO_ERROR;

	v_msg	:=  v_msg_template;

-- Column Heading
	IF it_error.COUNT > 0 THEN
		write_log(RPAD('Error Code',12) || RPAD('Description',40) || 'Error Detail');
		v_msg	:=	REPLACE(v_msg,'$$ERROR$$','<TR><TH>Error Code</TH><TH>Description</TH><TH>Error Detail</TH></TR>' || CHR(10) || '$$ERROR$$');
	END IF;

-- Data
	FOR i IN 1..it_error.COUNT
	LOOP
		FOR j IN 1..it_error(i).err_detail.COUNT
		LOOP
			write_log(RPAD(it_error(i).err_code,12) || RPAD(it_error(i).err_desc,40) || it_error(i).err_detail(j));
			v_msg	:=	REPLACE(v_msg,'$$ERROR$$','<TR><TD>' || it_error(i).err_code || '</TD><TD>' || it_error(i).err_desc || '</TD><TD>' || it_error(i).err_detail(j) || '</TD></TR>' || CHR(10) || '$$ERROR$$');
		END LOOP;
	END LOOP;

	v_msg	:=	REPLACE(v_msg,'$$ERROR$$',NULL);

-- Start Temporary for checking email
	write_log('Sending email info:');
	write_log('SMTP Server:   ' || i_smtp_server);
	write_log('SMTP Port:     ' || i_smtp_port);
	write_log('From email:    ' || i_from_email);
	write_log('To email:      ' || i_to_email);
	write_log('CC email:      ' || i_cc_email);
	write_log('BCC email:     ' || i_bcc_email);
	write_log('Email Subject: ' || i_subject);
	write_log('Email Message: ');
	write_log(v_msg);
-- End Temporary for checking email

	mail_tools.sendmail
		(smtp_server		=>  i_smtp_server
		,smtp_server_port	=>  i_smtp_port
		,from_name			=>  i_from_email
		,to_name			=>  i_to_email  -- list of TO email addresses separated by commas (,)
		,cc_name			=>  i_cc_email  -- list of CC email addresses separated by commas (,)
		,bcc_name			=>  i_bcc_email -- list of BCC email addresses separated by commas (,)
		,subject			=>  i_subject
		,message			=>  v_msg
		,priority			=>  i_priority  -- 1-5 1 being the highest priority and 3 normal priority
		);
EXCEPTION
	WHEN OTHERS THEN
		io_status	:=	'Error while sending email : ' || SQLERRM;
END send_email;
---------------------------------------------------------------------------
PROCEDURE write_output(p_msg VARCHAR2) IS
BEGIN
	fnd_file.put_line(fnd_file.output, p_msg);
EXCEPTION
	WHEN OTHERS THEN
		NULL;
END write_output;
---------------------------------------------------------------------------
PROCEDURE write_line( p_file_handle IN OUT utl_file.file_type,
		p_line_buff IN VARCHAR2) IS
	user_error VARCHAR2(255); -- to store translated file_error
BEGIN

	utl_file.put_line(p_file_handle, p_line_buff);
	utl_file.fflush(p_file_handle);

EXCEPTION
	WHEN utl_file.invalid_filehandle THEN
		user_error := 'Invalid filehandle';
		write_log(user_error);
	WHEN utl_file.invalid_operation THEN
		user_error := 'Invalid operation';
		write_log(user_error);
	WHEN utl_file.write_error THEN
		user_error := 'Write error';
		write_log(user_error);
	WHEN OTHERS THEN
		user_error := 'Write error others';
		write_log(user_error);
END write_line;
---------------------------------------------------------------------------
PROCEDURE write_line_nchar(	p_file_handle IN OUT utl_file.file_type,
		p_line_buff IN NVARCHAR2  ) IS
	user_error VARCHAR2(255); -- to store translated file_error
BEGIN

	utl_file.put_line_nchar(p_file_handle, p_line_buff);
	utl_file.fflush(p_file_handle);

EXCEPTION
	WHEN utl_file.invalid_filehandle THEN
		user_error := 'Invalid filehandle';
		write_log(user_error);
	WHEN utl_file.invalid_operation THEN
		user_error := 'Invalid operation';
		write_log(user_error);
	WHEN utl_file.write_error THEN
		user_error := 'Write error';
		write_log(user_error);
	WHEN OTHERS THEN
		user_error := 'Write error others';
		write_log(user_error);
END write_line_nchar;
---------------------------------------------------------------------------
FUNCTION get_directory_path(p_directory_name VARCHAR2) RETURN VARCHAR2 IS
	v_path VARCHAR2(250);
BEGIN
	SELECT directory_path
	INTO v_path
	FROM all_directories
	WHERE directory_name = p_directory_name;

	RETURN v_path;
EXCEPTION
	WHEN OTHERS THEN
		RETURN NULL;
END get_directory_path;
---------------------------------------------------------------------------
FUNCTION char_to_date (p_date_char VARCHAR2, p_format_mask VARCHAR2 DEFAULT NULL) RETURN DATE IS
	v_date DATE;
BEGIN
	IF (p_date_char IS NULL) THEN
		RETURN NULL;
	END IF;


	IF (p_format_mask IS NOT NULL) THEN

		BEGIN
			v_date := to_date(p_date_char, p_format_mask);
			RETURN v_date;
		EXCEPTION
			WHEN OTHERS THEN
				NULL;
		END;

	END IF;

-- Format Mask = dd/mm/yyyy hh24:mi:ss
	BEGIN
		v_date := to_date(p_date_char, 'dd/mm/yyyy hh24:mi:ss');
		RETURN v_date;
	EXCEPTION
		WHEN OTHERS THEN
			NULL;
	END;

-- Format Mask = dd/mm/yyyy hh24:mi
	BEGIN
	  v_date := to_date(p_date_char, 'dd/mm/yyyy hh24:mi');
	  RETURN v_date;
	EXCEPTION
	  WHEN OTHERS THEN
		NULL;
	END;

-- Format Mask = dd/mm/yyyy
	BEGIN
	  v_date := to_date(p_date_char, 'dd/mm/yyyy');
	  RETURN v_date;
	EXCEPTION
	  WHEN OTHERS THEN
		NULL;
	END;

-- Format Mask = dd/m/yyyy
	BEGIN
	  v_date := to_date(p_date_char, 'dd/m/yyyy');
	  RETURN v_date;
	EXCEPTION
	  WHEN OTHERS THEN
		NULL;
	END;

-- Format Mask = d/mm/yyyy
	BEGIN
	  v_date := to_date(p_date_char, 'd/mm/yyyy');
	  RETURN v_date;
	EXCEPTION
	  WHEN OTHERS THEN
		NULL;
	END;

-- Format Mask = d/m/yyyy
	BEGIN
	  v_date := to_date(p_date_char, 'd/m/yyyy');
	  RETURN v_date;
	EXCEPTION
	  WHEN OTHERS THEN
		RETURN p_date_char;
	END;

EXCEPTION
  WHEN OTHERS THEN
		RETURN p_date_char;
END char_to_date;
---------------------------------------------------------------------------
FUNCTION extract_data
(p_buffer		IN	VARCHAR2
,p_separator	IN	VARCHAR2
,p_pos_data		IN	NUMBER
,o_no_field		OUT	BOOLEAN)
RETURN VARCHAR2
IS
	v_pos_start  NUMBER;
	v_pos_end	NUMBER;
	v_subsrt_len NUMBER;
	v_data	VARCHAR2(255);
BEGIN
	o_no_field	:=	FALSE;
	IF p_pos_data = 1 THEN
		v_pos_start	:=	1;
	ELSE
		v_pos_start	:=	instr(p_buffer, p_separator, 1, p_pos_data - 1) + 1;
	END IF;
	
	v_pos_end	:=	instr(p_buffer, p_separator, 1, p_pos_data);
	
	IF v_pos_end = 0 THEN
		v_subsrt_len	:=	length(p_buffer);
		o_no_field	:=	TRUE;
	ELSE
		v_subsrt_len	:=	v_pos_end - v_pos_start;
	END IF;
	
	v_data	:=	substr(p_buffer, v_pos_start, v_subsrt_len);
	
	RETURN v_data;
EXCEPTION
	WHEN OTHERS THEN
		RETURN NULL;
END extract_data;
---------------------------------------------------------------------------
FUNCTION split_gl_data
(p_line_buff	IN	VARCHAR2
,p_delimiter	IN	VARCHAR2	:=	'|'
,p_debug_flag	IN	VARCHAR2	:=	'N'
,o_missing_field	OUT	BOOLEAN)
RETURN tac_mc_gl_interface_temp%ROWTYPE
IS
	v_rec_data			tac_mc_gl_interface_temp%ROWTYPE;
	v_missing_field		BOOLEAN;
BEGIN
	IF p_line_buff IS NULL THEN
		RETURN v_rec_data;
	END IF;
	v_rec_data.status					:=	extract_data(p_line_buff
														,p_delimiter
														,1, o_missing_field);
	v_rec_data.set_of_books_id			:=	extract_data(p_line_buff
														,p_delimiter
														,2, o_missing_field);
	v_rec_data.accounting_date			:=	char_to_date(
											extract_data(p_line_buff
														,p_delimiter
														,3, o_missing_field)
														,'dd/mm/yyyy');
	v_rec_data.currency_code			:=	extract_data(p_line_buff
														,p_delimiter
														,4, o_missing_field);
	v_rec_data.date_created				:=	char_to_date(
											extract_data(p_line_buff
														,p_delimiter
														,5, o_missing_field)
														,'dd/mm/yy');
-- Edit by TM 13/12/2017
	v_rec_data.created_by				:=	extract_data(p_line_buff
														,p_delimiter
														,6, o_missing_field);
/*	g_created_by				:=	extract_data(p_line_buff
												,p_delimiter
												,6);
	IF g_created_by = g_prev_created_by THEN
		NULL;
	ELSE
		g_user_id	:=	NULL;
		FOR rec IN (SELECT * FROM fnd_user u WHERE u.user_name = g_created_by)
		LOOP
			g_user_id	:=	rec.user_id;
			EXIT;
		END LOOP;
		IF g_user_id IS NULL THEN
			v_rec_data.error_msg	:=	'Error created by username ' || g_created_by || ' not found, please check before proceed';
			RETURN v_rec_data;
		END IF;
		g_prev_created_by		:=	g_created_by;
	END IF;
	v_rec_data.created_by	:=	g_user_id;*/
-- End Edit by TM 13/12/2017
	v_rec_data.actual_flag				:=	extract_data(p_line_buff
														,p_delimiter
														,7, o_missing_field);
	v_rec_data.user_je_category_name	:=	extract_data(p_line_buff
														,p_delimiter
														,8, o_missing_field);
	v_rec_data.user_je_source_name		:=	extract_data(p_line_buff
														,p_delimiter
														,9, o_missing_field);
	v_rec_data.accounted_dr				:=	extract_data(p_line_buff
														,p_delimiter
														,10, o_missing_field);
	v_rec_data.accounted_cr				:=	extract_data(p_line_buff
														,p_delimiter
														,11, o_missing_field);
	v_rec_data.currency_conversion_date	:=	char_to_date(
											extract_data(p_line_buff
														,p_delimiter
														,12, o_missing_field)
														,g_datechar_format);
	v_rec_data.currency_conversion_rate	:=	extract_data(p_line_buff
														,p_delimiter
														,13, o_missing_field);
	v_rec_data.entered_dr				:=	extract_data(p_line_buff
														,p_delimiter
														,14, o_missing_field);
	v_rec_data.entered_cr				:=	extract_data(p_line_buff
														,p_delimiter
														,15, o_missing_field);
	v_rec_data.je_line_num				:=	extract_data(p_line_buff
														,p_delimiter
														,16, o_missing_field);
	v_rec_data.period_name				:=	upper(
											extract_data(p_line_buff
														,p_delimiter
														,17, o_missing_field));
	v_rec_data.reference1				:=	extract_data(p_line_buff
														,p_delimiter
														,18, o_missing_field);
	v_rec_data.reference4				:=	extract_data(p_line_buff
														,p_delimiter
														,19, o_missing_field);
	v_rec_data.reference5				:=	extract_data(p_line_buff
														,p_delimiter
														,20, o_missing_field);
	v_rec_data.segment1					:=	extract_data(p_line_buff
														,p_delimiter
														,21, o_missing_field);
	v_rec_data.segment11				:=	extract_data(p_line_buff
														,p_delimiter
														,22, o_missing_field);
	v_rec_data.segment10				:=	extract_data(p_line_buff
														,p_delimiter
														,23, o_missing_field);
	v_rec_data.segment12				:=	extract_data(p_line_buff
														,p_delimiter
														,24, o_missing_field);
	v_rec_data.segment13				:=	extract_data(p_line_buff
														,p_delimiter
														,25, o_missing_field);
	v_rec_data.segment14				:=	extract_data(p_line_buff
														,p_delimiter
														,26, o_missing_field);
	v_rec_data.segment18				:=	extract_data(p_line_buff
														,p_delimiter
														,27, o_missing_field);
	v_rec_data.segment19				:=	extract_data(p_line_buff
														,p_delimiter
														,29, o_missing_field);	-- Resequence column
	v_rec_data.segment21				:=	extract_data(p_line_buff
														,p_delimiter
														,28, o_missing_field);	-- Resequence column
	v_rec_data.reference21				:=	extract_data(p_line_buff
														,p_delimiter
														,30, o_missing_field);
	v_rec_data.reference10				:=	extract_data(p_line_buff
														,p_delimiter
														,31, v_missing_field);	-- Ignore error for last field
	
-- Accounted / Entered Amount, can send any field
	IF NVL(v_rec_data.accounted_dr,0) = 0 THEN
		v_rec_data.accounted_dr	:=	v_rec_data.entered_dr;
	ELSIF NVL(v_rec_data.entered_dr,0) = 0 THEN
		v_rec_data.entered_dr	:=	v_rec_data.accounted_dr;
	END IF;

	IF NVL(v_rec_data.accounted_cr,0) = 0 THEN
		v_rec_data.accounted_cr	:=	v_rec_data.entered_cr;
	ELSIF NVL(v_rec_data.entered_cr,0) = 0 THEN
		v_rec_data.entered_cr	:=	v_rec_data.accounted_cr;
	END IF;
	
	IF p_debug_flag = 'Y' THEN
		write_log(g_log_line);
		write_log('Line data = ' || p_line_buff);
		/*
		write_log('		company_code  = ' || v_rec_data.company_code);
		write_log('		transaction_type  = ' || v_rec_data.transaction_type);
		write_log('		grn_number  = ' || v_rec_data.grn_number);
		write_log('		po_number  = ' || v_rec_data.po_number);
		write_log('		release_number  = ' || v_rec_data.release_number);
		write_log('		po_line_number  = ' || v_rec_data.po_line_number);
		write_log('		product_sku_code  = ' || v_rec_data.product_sku_code);
		write_log('		invoice_number  = ' || v_rec_data.invoice_number);
		write_log('		rate_date  = ' || v_rec_data.rate_date);
		write_log('		qty  = ' || v_rec_data.qty);
		write_log('		inventory_org_code  = ' || v_rec_data.inventory_org_code);
		write_log('		sub_inventory  = ' || v_rec_data.sub_inventory);
		write_log('		locator  = ' || v_rec_data.locator);
		write_log('		create_by  = ' || v_rec_data.create_by);
		write_log('		creation_date  = ' || v_rec_data.creation_date);
		*/
	END IF;
	
	RETURN v_rec_data;

EXCEPTION
	WHEN OTHERS THEN
		v_rec_data.error_msg := SQLCODE || ' - ' || SQLERRM;
		RETURN v_rec_data;

END split_gl_data;
---------------------------------------------------------------------------
PROCEDURE move_data_file (p_source_file VARCHAR2, p_dest_path VARCHAR2 ) IS
	v_movefile_req_id	NUMBER;
	v_phase				VARCHAR2(100);
	v_status			VARCHAR2(100);
	v_message			VARCHAR2(1000);
BEGIN
	write_log(' ');
	write_log('Moving File ' || p_source_file || ' to ' || p_dest_path);

	v_movefile_req_id := fnd_request.submit_request(application => 'FND'
												   ,program => 'TACFND_MVFILE'
												   ,argument1 => p_source_file
												   ,argument2 => p_dest_path);
	COMMIT;
	write_log('Concurrent Move File is ' || v_movefile_req_id);

	IF (v_movefile_req_id > 0) THEN
		conc_wait(v_movefile_req_id, v_phase, v_status, v_message, 60);
		IF (v_status = 'E') THEN
			write_log('Move file from ' || p_source_file || ' to ' || p_dest_path || ' failed.');
		ELSE
			write_log('Move file from ' || p_source_file || ' to ' || p_dest_path || ' successful.');
		END IF;
	ELSE
		write_log('Move file from ' || p_source_file || ' to ' || p_dest_path || ' faile, No concurrent program [TACPO_MVFILE_INF].');
	END IF;

	write_log(' ');

	EXCEPTION
		WHEN OTHERS THEN
			write_log('Move file from ' || p_source_file || ' to ' || p_dest_path || ' failed with error ' || SQLERRM);

END move_data_file;
---------------------------------------------------------------------------
PROCEDURE validate_gl
(p_company		IN	VARCHAR2
,p_sob_id		IN	VARCHAR2
,p_location		IN	VARCHAR2
,p_filename		IN	VARCHAR2
,p_delimiter	IN	VARCHAR2
,p_debug_flag	IN	VARCHAR2
,p_request_id	IN	NUMBER
,o_status		OUT	VARCHAR2
,iot_error		IN OUT NOCOPY nested_varchar_tabtyp
,io_total_lines	IN OUT NUMBER)
IS
	t_error			varchar2_tabtyp;
	v_file_handle	utl_file.file_type;

	v_line_data		NVARCHAR2(30000);
	v_line_count	NUMBER	:=	0;

	v_rec_data		tac_mc_gl_interface_temp%ROWTYPE;
	v_missing_field	BOOLEAN;
BEGIN
	o_status	:=	G_NO_ERROR;

	write_log('     Validate Data : '||TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SSSSS'));

-- Read and Close for checking
	t_error.DELETE;
	BEGIN
		v_file_handle	:=	utl_file.fopen_nchar
								(location		=>	p_location
								,filename		=>	p_filename
								,open_mode		=>	'r'
								,max_linesize	=>	4000);
	EXCEPTION
		WHEN OTHERS THEN
			t_error(t_error.COUNT+1)	:=	'Error reading file : ' || SQLERRM;
			utl_file.fclose(v_file_handle);
	END;

	IF t_error.COUNT > 0 THEN
		o_status	:=	'Error validate_gl';
--write_log('iot_error.COUNT = ' || iot_error.COUNT);
		iot_error(iot_error.COUNT+1)	:=	NULL;
		iot_error(iot_error.COUNT).company		:=	p_company;
		iot_error(iot_error.COUNT).filename		:=	p_filename;
		iot_error(iot_error.COUNT).line_num		:=	NULL;
		iot_error(iot_error.COUNT).err_code		:=	'FR-001';
		iot_error(iot_error.COUNT).err_desc		:=	CT_ERROR(iot_error(iot_error.COUNT).err_code);
		iot_error(iot_error.COUNT).err_detail	:=	t_error;
		RETURN;	-- Read file error ==> return
	END IF;

	-- Read first line for header (ignore BOM byte)
	utl_file.get_line_nchar	(file	=>	v_file_handle
							,buffer	=>	v_line_data);

	v_line_data		:=	'';
	v_line_count	:=	0;	-- Clear number of lines

	LOOP
		BEGIN
			utl_file.get_line_nchar	(file	=>	v_file_handle
									,buffer	=>	v_line_data);
			v_line_data	:=	REPLACE( REPLACE(v_line_data, chr(10), '') ,chr(13) ,'');
			IF (v_line_data = 'END' OR v_line_data IS NULL) THEN
				EXIT;
			END IF;
			v_line_count	:=	v_line_count + 1;

			v_rec_data		:=	split_gl_data(p_line_buff		=>	v_line_data
											 ,p_delimiter		=>	p_delimiter
											 ,p_debug_flag		=>	p_debug_flag
											 ,o_missing_field	=>	v_missing_field);
			IF v_missing_field THEN
				v_rec_data.error_msg := v_rec_data.error_msg || ';FF-001';
				t_error.DELETE;
				t_error(t_error.COUNT+1)	:=	'File format error, please check fields in the input file';
				iot_error(iot_error.COUNT+1)	:=	NULL;
				iot_error(iot_error.COUNT).company		:=	p_company;
				iot_error(iot_error.COUNT).filename		:=	p_filename;
				iot_error(iot_error.COUNT).line_num		:=	v_line_count;
				iot_error(iot_error.COUNT).err_code		:=	'FF-001';
				iot_error(iot_error.COUNT).err_desc		:=	CT_ERROR(iot_error(iot_error.COUNT).err_code);
				iot_error(iot_error.COUNT).err_detail	:=	t_error;
--				write_log('Line#' || to_char(v_line_count, 90000) || ' [ERR]	=>	File format error ' || v_line_data);
--				write_output('Line#' || to_char(v_line_count, 90000) || ' [ERR]	=>	File format error ' || v_line_data);
			END IF;

			IF v_rec_data.set_of_books_id = p_sob_id THEN
				NULL;
			ELSE	-- SOB ID specified in text file is not matched to the setup
				v_rec_data.error_msg := v_rec_data.error_msg || ';EV-001';
				t_error.DELETE;
				t_error(t_error.COUNT+1)	:=	'SOB ID in File : ' || v_rec_data.set_of_books_id || ' , SOB ID in Parameter : ' || p_sob_id;
				iot_error(iot_error.COUNT+1)	:=	NULL;
				iot_error(iot_error.COUNT).company		:=	p_company;
				iot_error(iot_error.COUNT).filename		:=	p_filename;
				iot_error(iot_error.COUNT).line_num		:=	v_line_count;
				iot_error(iot_error.COUNT).err_code		:=	'EV-001';
				iot_error(iot_error.COUNT).err_desc		:=	CT_ERROR(iot_error(iot_error.COUNT).err_code);
				iot_error(iot_error.COUNT).err_detail	:=	t_error;
--				write_log('Line#' || to_char(v_line_count, 90000) || ' [ERR]	=>	Incorrect SOB_ID ' || v_line_data);
--				write_output('Line#' || to_char(v_line_count, 90000) || ' [ERR]	=>	Incorrect SOB_ID ' || v_rec_data.set_of_books_id);
			END IF;
			BEGIN
				v_rec_data.request_id		:=	p_request_id;
				v_rec_data.interface_date	:=	SYSDATE;

				IF NOT tac_gl_check_pkg.check_source(v_rec_data.user_je_source_name) THEN
					v_rec_data.error_msg := v_rec_data.error_msg || ';EV-002';
					t_error.DELETE;
					t_error(t_error.COUNT+1)	:=	'JE Source : ' || v_rec_data.user_je_source_name;
					iot_error(iot_error.COUNT+1)	:=	NULL;
					iot_error(iot_error.COUNT).company		:=	p_company;
					iot_error(iot_error.COUNT).filename		:=	p_filename;
					iot_error(iot_error.COUNT).line_num		:=	v_line_count;
					iot_error(iot_error.COUNT).err_code		:=	'EV-002';
					iot_error(iot_error.COUNT).err_desc		:=	CT_ERROR(iot_error(iot_error.COUNT).err_code);
					iot_error(iot_error.COUNT).err_detail	:=	t_error;
				END IF;

				IF NOT tac_gl_check_pkg.check_category(v_rec_data.user_je_category_name) THEN
					v_rec_data.error_msg := v_rec_data.error_msg || ';EV-003';
					t_error.DELETE;
					t_error(t_error.COUNT+1)	:=	'JE Category : ' || v_rec_data.user_je_category_name;
					iot_error(iot_error.COUNT+1)	:=	NULL;
					iot_error(iot_error.COUNT).company		:=	p_company;
					iot_error(iot_error.COUNT).filename		:=	p_filename;
					iot_error(iot_error.COUNT).line_num		:=	v_line_count;
					iot_error(iot_error.COUNT).err_code		:=	'EV-003';
					iot_error(iot_error.COUNT).err_desc		:=	CT_ERROR(iot_error(iot_error.COUNT).err_code);
					iot_error(iot_error.COUNT).err_detail	:=	t_error;
				END IF;

				IF NOT tac_gl_check_pkg.check_account(v_rec_data.segment1
							,v_rec_data.segment10
							,v_rec_data.segment11
							,v_rec_data.segment12
							,v_rec_data.segment13
							,v_rec_data.segment14
							,v_rec_data.segment18
							,v_rec_data.segment19
							,v_rec_data.segment21) THEN
					v_rec_data.error_msg := v_rec_data.error_msg || ';EV-004';
					t_error.DELETE;
					t_error(t_error.COUNT+1)	:=	'segment1 : ' || v_rec_data.segment1 || ' segment10 : ' || v_rec_data.segment10 ||
													' segment11 : ' || v_rec_data.segment11 || ' segment12 : ' || v_rec_data.segment12 ||
													' segment13 : ' || v_rec_data.segment13 || ' segment14 : ' || v_rec_data.segment14 ||
													' segment18 : ' || v_rec_data.segment18 || ' segment19 : ' || v_rec_data.segment19 ||
													' segment21 : ' || v_rec_data.segment21;
					iot_error(iot_error.COUNT+1)	:=	NULL;
					iot_error(iot_error.COUNT).company		:=	p_company;
					iot_error(iot_error.COUNT).filename		:=	p_filename;
					iot_error(iot_error.COUNT).line_num		:=	v_line_count;
					iot_error(iot_error.COUNT).err_code		:=	'EV-004';
					iot_error(iot_error.COUNT).err_desc		:=	CT_ERROR(iot_error(iot_error.COUNT).err_code);
					iot_error(iot_error.COUNT).err_detail	:=	t_error;
				END IF;

				IF NVL(v_rec_data.accounted_dr,0) <> NVL(v_rec_data.entered_dr,0) OR
				   NVL(v_rec_data.accounted_cr,0) <> NVL(v_rec_data.entered_cr,0) THEN
					v_rec_data.error_msg := v_rec_data.error_msg || ';EV-005';
					t_error.DELETE;
					t_error(t_error.COUNT+1)	:=	'Accounted DR : ' || v_rec_data.accounted_dr || ' Entered DR : ' || v_rec_data.entered_dr ||
													' Accounted CR : ' || v_rec_data.accounted_cr || ' Entered CR : ' || v_rec_data.entered_cr;
					iot_error(iot_error.COUNT+1)	:=	NULL;
					iot_error(iot_error.COUNT).company		:=	p_company;
					iot_error(iot_error.COUNT).filename		:=	p_filename;
					iot_error(iot_error.COUNT).line_num		:=	v_line_count;
					iot_error(iot_error.COUNT).err_code		:=	'EV-005';
					iot_error(iot_error.COUNT).err_desc		:=	CT_ERROR(iot_error(iot_error.COUNT).err_code);
					iot_error(iot_error.COUNT).err_detail	:=	t_error;
				END IF;

/*				IF v_rec_data.error_msg IS NULL THEN
					write_log('Line #' || to_char(v_line_count, 90000) || ' [OK] =>  ' || v_line_data);
				ELSIF v_rec_data.error_msg IS NOT NULL THEN
					write_log('Line #' || to_char(v_line_count, 90000) || ' [ERR] => ' || v_line_data || ';' || v_rec_data.error_msg);
					write_output('Line #' || to_char(v_line_count, 90000) || ' [ERR] => ' || v_line_data || ';' || v_rec_data.error_msg);
				END IF;
*/
				INSERT INTO tac_mc_gl_interface_temp
					(status
					,set_of_books_id
					,accounting_date
					,currency_code
					,date_created
					,created_by
					,actual_flag
					,user_je_category_name
					,user_je_source_name
					,accounted_dr
					,accounted_cr
					,currency_conversion_date
					,currency_conversion_rate
					,entered_dr
					,entered_cr
					,je_line_num
					,period_name
					,reference1
					,reference4
					,reference5
					,segment1
					,segment11
					,segment10
					,segment12
					,segment13
					,segment14
					,segment18
					,segment19
					,segment21
					,reference21
					,reference10	-- JE Line Description
					,request_id
					,interface_date
					,validate_status
					,error_msg)
				VALUES
					(v_rec_data.status
					,v_rec_data.set_of_books_id
					,v_rec_data.accounting_date
					,v_rec_data.currency_code
					,v_rec_data.date_created
					,v_rec_data.created_by
					,v_rec_data.actual_flag
					,v_rec_data.user_je_category_name
					,v_rec_data.user_je_source_name
-- Edit by TM 18/07/2019
/*					,v_rec_data.accounted_dr
					,v_rec_data.accounted_cr*/
					,DECODE(NVL(v_rec_data.accounted_cr,0),0,v_rec_data.accounted_dr)
					,DECODE(NVL(v_rec_data.accounted_cr,0),0,null,v_rec_data.accounted_cr)
-- End Edit by TM 18/07/2019
					,v_rec_data.currency_conversion_date
					,v_rec_data.currency_conversion_rate
-- Edit by TM 18/07/2019
/*					,v_rec_data.entered_dr
					,v_rec_data.entered_cr*/
					,DECODE(NVL(v_rec_data.entered_cr,0),0,v_rec_data.entered_dr)
					,DECODE(NVL(v_rec_data.entered_cr,0),0,null,v_rec_data.entered_cr)
-- End Edit by TM 18/07/2019
					,v_rec_data.je_line_num
					,v_rec_data.period_name
					,v_rec_data.reference1
					,v_rec_data.reference4
					,v_rec_data.reference5
					,v_rec_data.segment1
					,v_rec_data.segment11
					,v_rec_data.segment10
					,v_rec_data.segment12
					,v_rec_data.segment13
					,v_rec_data.segment14
					,v_rec_data.segment18
					,v_rec_data.segment19
					,v_rec_data.segment21
					,v_rec_data.reference21
					,v_rec_data.reference10	-- JE Line Description
					,v_rec_data.request_id
					,v_rec_data.interface_date
					,v_rec_data.validate_status
					,v_rec_data.error_msg);

--				write_log('Line#' || to_char(v_line_count, 90000) || ' [ OK]	=>	' || v_line_data);
			EXCEPTION
				WHEN OTHERS THEN
					v_rec_data.error_msg := v_rec_data.error_msg || ';ER-001';
					t_error.DELETE;
					t_error(t_error.COUNT+1)	:=	'Error Message : ' || SQLERRM;
					iot_error(iot_error.COUNT+1)	:=	NULL;
					iot_error(iot_error.COUNT).company		:=	p_company;
					iot_error(iot_error.COUNT).filename		:=	p_filename;
					iot_error(iot_error.COUNT).line_num		:=	v_line_count;
					iot_error(iot_error.COUNT).err_code		:=	'ER-001';
					iot_error(iot_error.COUNT).err_desc		:=	CT_ERROR(iot_error(iot_error.COUNT).err_code);
					iot_error(iot_error.COUNT).err_detail	:=	t_error;
/*					write_log('Line#' || to_char(v_line_count, 90000) || ' [ERR] =>	' || v_line_data);
					write_output('Line#' || to_char(v_line_count, 90000) || ' [ERR]	=>	' || SQLERRM);*/
			END;
		EXCEPTION
			WHEN no_data_found THEN
				EXIT;
			WHEN OTHERS THEN
				v_rec_data.error_msg := v_rec_data.error_msg || ';ER-001';
				t_error.DELETE;
				t_error(t_error.COUNT+1)	:=	'Error Message : ' || SQLERRM;
				iot_error(iot_error.COUNT+1)	:=	NULL;
				iot_error(iot_error.COUNT).company		:=	p_company;
				iot_error(iot_error.COUNT).filename		:=	p_filename;
				iot_error(iot_error.COUNT).line_num		:=	v_line_count;
				iot_error(iot_error.COUNT).err_code		:=	'ER-001';
				iot_error(iot_error.COUNT).err_desc		:=	CT_ERROR(iot_error(iot_error.COUNT).err_code);
				iot_error(iot_error.COUNT).err_detail	:=	t_error;
--				write_log('Read data from text file error :  ' || SQLERRM);
				EXIT;
		END;
	END LOOP;

	IF iot_error.COUNT > 0 THEN
		o_status	:=	'Error validate_gl';
	END IF;

	io_total_lines	:=	io_total_lines + v_line_count;	-- Total line count from all files

	-- check open and close
	IF (utl_file.is_open(file	=>	v_file_handle)) THEN
		utl_file.fclose(file	=>	v_file_handle);
	END IF;

EXCEPTION
	WHEN OTHERS THEN
		o_status		:=	'Error validate_gl : ' || SQLERRM;
		write_log('     Validate Data : '||o_status);

		t_error.DELETE;
		t_error(t_error.COUNT+1)	:=	'Error Message : ' || SQLERRM;
		iot_error(iot_error.COUNT+1)	:=	NULL;
		iot_error(iot_error.COUNT).company		:=	p_company;
		iot_error(iot_error.COUNT).filename		:=	p_filename;
		iot_error(iot_error.COUNT).line_num		:=	v_line_count;
		iot_error(iot_error.COUNT).err_code		:=	'ER-001';
		iot_error(iot_error.COUNT).err_desc		:=	CT_ERROR(iot_error(iot_error.COUNT).err_code);
		iot_error(iot_error.COUNT).err_detail	:=	t_error;

		-- check open and close
		IF (utl_file.is_open(file	=>	v_file_handle)) THEN
			utl_file.fclose(file	=>	v_file_handle);
		END IF;
END validate_gl;
---------------------------------------------------------------------------
-- MAIN PROGRAM --
PROCEDURE interface_gl
(err_msg					OUT VARCHAR2
,err_code					OUT VARCHAR2
,p_set_of_books_id			IN	NUMBER
,p_file_path				IN	VARCHAR2	-- Directory Object Name
,p_file_name_format			IN	VARCHAR2
,p_file_name				IN	VARCHAR2	:=	NULL
,p_delimiter				IN	VARCHAR2	:=	'|'
,p_debug_flag				IN	VARCHAR2	:=	'N'
,p_purge_days				IN	NUMBER		:=	45
,p_insert_interface_table	IN	VARCHAR2	:=	'Y'
,p_email_address			IN	VARCHAR2	:=	'ERPOperationSupport@dtac.co.th'
,p_file_date				IN	VARCHAR2
,p_folder_history			IN	VARCHAR2	:=	'MC_GL_HISTORY'
,p_folder_error				IN	VARCHAR2	:=	'MC_GL_ERROR')
IS
-- Information
-- g_gl_inbox,p_file_path				varchar2(50) := 'MC_GL_INBOX';
-- g_gl_error,p_folder_error			varchar2(50) := 'MC_GL_ERROR';
-- g_gl_history,p_folder_history		varchar2(50) := 'MC_GL_HISTORY';

	C_PROGRAM_NAME		CONSTANT VARCHAR2(1000)	:=	'TAC Money Collection Interface GL';

	CURSOR c_books IS
		SELECT	lv.lookup_code, lv.attribute1 sob_code_in_file, lv.attribute2 sob_id
		FROM	fnd_lookup_values lv
		WHERE	lv.lookup_type	=	'TAC_IFRS_INTERFACE'
		AND		lv.attribute2	=	NVL(p_set_of_books_id,lv.attribute2)
		ORDER BY lv.lookup_code;

	v_file_name				VARCHAR2(100);
	v_date					DATE := TO_DATE(p_file_date,'YYYY/MM/DD HH24:MI:SS');

	v_insert_error_flag		VARCHAR2(1) := 'N';

	v_request_id			NUMBER := fnd_profile.value('CONC_REQUEST_ID');
	v_debug_flag			VARCHAR2(10);
	v_delimiter				VARCHAR2(10);

	v_login_id				NUMBER := fnd_profile.value('LOGIN_ID');
	v_user_id				NUMBER := fnd_profile.value('USER_ID');

	v_insert_itf_table		VARCHAR2(10) := 'Y';

	v_count_gl				NUMBER := 0;
	v_status				VARCHAR2(2000);
	v_status_all			VARCHAR2(2000)	:=	G_NO_ERROR;
	t_error_all				nested_varchar_tabtyp;
	t_error					varchar2_tabtyp;
	v_start_time			DATE	:=	SYSDATE;
	v_total_line_count		NUMBER	:=	0;

	CURSOR c_chk_total IS
		SELECT	t.set_of_books_id, t.user_je_source_name, t.user_je_category_name
				,t.reference1 /* Batch Name */, t.reference4 /* Header Name */
				,COUNT(*) total_rows, COUNT(DISTINCT t.reference5 /* Header Description */) cnt_hdr_desc
				,SUM(t.accounted_dr) total_acct_dr, SUM(t.accounted_cr) total_acct_cr
		FROM	tac_mc_gl_interface_temp t
		WHERE	request_id	=	v_request_id
		GROUP BY t.set_of_books_id, t.user_je_source_name, t.user_je_category_name
				,t.reference1 /* Batch Name */, t.reference4 /* Header Name */
		HAVING	COUNT(DISTINCT t.reference5 /* Header Description */) > 1
		OR		NVL(SUM(t.accounted_dr),0) <> NVL(SUM(t.accounted_cr),0);

	v_no_mapping	BOOLEAN	:=	TRUE;
BEGIN
	err_code		:= '2';
	v_delimiter		:= p_delimiter;
	v_debug_flag	:= p_debug_flag;

	write_log(' ');
	write_log('+---------------------------------------------------------------------------+');
	write_log('Start Concurrent GL Interface from Money Collection');
	write_log('Start Date: ' || to_char(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'));
	write_log(' ');
	write_log('Agruments');
	write_log('-----------');
	write_log('p_set_of_books_id        =  ' || p_set_of_books_id);
	write_log('p_file_path              =  ' || p_file_path);
	write_log('p_file_name_format       =  ' || p_file_name_format);
	write_log('p_file_name              =  ' || p_file_name);
	write_log('p_delimiter              =  ' || p_delimiter);
	write_log('p_debug_flag             =  ' || p_debug_flag);
	write_log('p_purge_days             =  ' || p_purge_days);
	write_log('p_insert_interface_table =  ' || p_insert_interface_table);
	write_log('p_file_date              =  ' || TO_CHAR(v_date,'DD/MM/YYYY'));
	write_log('p_folder_history         =  ' || p_folder_history);
	write_log('p_folder_error           =  ' || p_folder_error);

	write_log('+----------------------------------------------------------------------------+');

	-- Start import data from text file
	v_insert_itf_table := nvl(p_insert_interface_table, 'Y');

	IF p_file_path IS NULL THEN
		RAISE utl_file.invalid_path;
	END IF;
	
-- Purge old data
	BEGIN
		DELETE	tac_mc_gl_interface_temp tmp
		WHERE	tmp.interface_date <= TRUNC(SYSDATE - nvl(p_purge_days, 45));
		
		UPDATE	tac_mc_gl_interface_temp tmp
		SET		status = 'OLD'
		WHERE	request_id <> v_request_id
		AND		status = 'NEW';
		COMMIT;
	EXCEPTION
		WHEN OTHERS THEN
			NULL;
	END;

	SAVEPOINT before_insert_temp_all;
	FOR r_books IN c_books
	LOOP
		v_no_mapping	:=	FALSE;	-- Mapping is found
		IF (p_file_name IS NOT NULL) THEN
			v_file_name	:=	p_file_name;
		ELSE
			IF p_file_name_format IS NULL THEN
				RAISE utl_file.invalid_filename;
			END IF;
			v_file_name	:=	REPLACE(p_file_name_format,
								'$DATE$',
								TRIM(to_char(v_date, 'yyyymmdd')));
			v_file_name	:=	REPLACE(v_file_name,
								'$SOB_FILENAME$',
								r_books.sob_code_in_file);
		END IF;
		
		write_log(g_log_line);
		write_log('Import GL data from file ' ||
					get_directory_path(p_file_path) || '/' || v_file_name);
		write_log(' ');
		
		SAVEPOINT before_insert_temp;

-- Validate file
		validate_gl	(p_company		=>	r_books.sob_code_in_file
					,p_sob_id		=>	r_books.sob_id
					,p_location		=>	p_file_path
					,p_filename		=>	v_file_name
					,p_delimiter	=>	p_delimiter
					,p_debug_flag	=>	p_debug_flag
					,p_request_id	=>	v_request_id
					,o_status		=>	v_status
					,iot_error		=>	t_error_all
					,io_total_lines	=>	v_total_line_count);
		
		IF v_status <> G_NO_ERROR THEN
			v_status_all	:=	v_status;
			ROLLBACK TO before_insert_temp;
			write_output(' ');
			write_output(' ');
			write_output(g_log_line);
			
			write_log(' ');
			write_log('TAC_MONEY_COLLECT_GL_ITF.interface_gl @import_data exception when insert data. See output for error detail.');
			
			err_msg	:=	'Error TAC_MONEY_COLLECT_GL_ITF.interface_gl @import_data : Some record error during insert to table. See output for detail.';
			err_code	:=	'2';
/*			move_data_file(get_directory_path(p_file_path) || '/' || v_file_name,
							get_directory_path(p_folder_error));
			
			RETURN;
		ELSE
			COMMIT;
			move_data_file(get_directory_path(p_file_path) || '/' || v_file_name,
							get_directory_path(p_folder_history));*/
		END IF;
		-- End of import data from text file
		IF (p_file_name IS NOT NULL) THEN
			EXIT;	-- specify filename
		END IF;
	END LOOP;
	
	IF v_no_mapping THEN
		t_error.DELETE;
		t_error(t_error.COUNT+1)	:=	'Mapping not found for specified SOB ID';
		t_error_all(t_error_all.COUNT+1)	:=	NULL;
		t_error_all(t_error_all.COUNT).company		:=	NULL;
		t_error_all(t_error_all.COUNT).filename		:=	NULL;
		t_error_all(t_error_all.COUNT).line_num		:=	NULL;
		t_error_all(t_error_all.COUNT).err_code		:=	'ER-002';
		t_error_all(t_error_all.COUNT).err_desc		:=	CT_ERROR(t_error_all(t_error_all.COUNT).err_code);
		t_error_all(t_error_all.COUNT).err_detail	:=	t_error;
		v_status_all	:=	'Error validate overall';
	END IF;

-- Validate Overall
	FOR rec IN c_chk_total
	LOOP
		IF rec.cnt_hdr_desc > 1 THEN
			t_error.DELETE;
			t_error(t_error.COUNT+1)	:=	'Count Diff Header Desc : ' || rec.cnt_hdr_desc;
			t_error_all(t_error_all.COUNT+1)	:=	NULL;
			t_error_all(t_error_all.COUNT).company		:=	NULL;
			t_error_all(t_error_all.COUNT).filename		:=	NULL;
			t_error_all(t_error_all.COUNT).line_num		:=	NULL;
			t_error_all(t_error_all.COUNT).err_code		:=	'EV-006';
			t_error_all(t_error_all.COUNT).err_desc		:=	CT_ERROR(t_error_all(t_error_all.COUNT).err_code);
			t_error_all(t_error_all.COUNT).err_detail	:=	t_error;
		ELSIF NVL(rec.total_acct_dr,0) <> NVL(rec.total_acct_cr,0) THEN
			t_error.DELETE;
			t_error(t_error.COUNT+1)	:=	'Total DR : ' || rec.total_acct_dr || ' Total CR : ' || rec.total_acct_cr;
			t_error_all(t_error_all.COUNT+1)	:=	NULL;
			t_error_all(t_error_all.COUNT).company		:=	NULL;
			t_error_all(t_error_all.COUNT).filename		:=	NULL;
			t_error_all(t_error_all.COUNT).line_num		:=	NULL;
			t_error_all(t_error_all.COUNT).err_code		:=	'EV-007';
			t_error_all(t_error_all.COUNT).err_desc		:=	CT_ERROR(t_error_all(t_error_all.COUNT).err_code);
			t_error_all(t_error_all.COUNT).err_detail	:=	t_error;
		END IF;
		v_status_all	:=	'Error validate overall';
	END LOOP;

write_log('v_status_all = ' || v_status_all);
	IF v_status_all <> G_NO_ERROR THEN
		ROLLBACK TO before_insert_temp_all;

		write_log('Validation Error :');
		write_log('---------------------------------------');
		write_log(v_status_all);
		FOR i IN 1..t_error_all.COUNT
		LOOP
			write_log('Company : ' || t_error_all(i).company || ' Filename : ' || t_error_all(i).filename || ' Error : ' || t_error_all(i).err_code || ' : ' || t_error_all(i).err_desc);
/*			FOR j IN 1..t_error_all(i).err_detail.COUNT
			LOOP
				write_log('Company : ' || t_error_all(i).company || ' Filename : ' || t_error_all(i).filename || ' Error : ' || t_error_all(i).err_code || ' : ' || t_error_all(i).err_desc || ' : ' || t_error_all(i).err_detail(j));
			END LOOP;*/
		END LOOP;
		write_log(' ');
		write_log('---------------------------------------');
		ERR_MSG		:=	v_status;
		ERR_CODE	:=	'2';	-- Error

		-- Send email
		send_email  (i_to_email		=>  p_email_address
					,i_cc_email		=>  NULL
					,i_bcc_email	=>  NULL
					,i_subject		=>	'ERP : ' || C_PROGRAM_NAME || ' Request ID : ' || v_request_id || ' - VALIDATION FAILED'
					,i_program_name	=>	C_PROGRAM_NAME
					,i_sob_id			=>	p_set_of_books_id
					,i_file_path		=>	p_file_path
					,i_filename_pattern	=>	p_file_name_format
					,i_filename			=>	p_file_name
					,i_delimiter		=>	p_delimiter
					,i_file_date		=>	p_file_date
					,i_email_address	=>	p_email_address
					,it_error		=>	t_error_all
					,i_start_time	=>	v_start_time
					,i_end_time		=>	SYSDATE
					,io_status		=>  v_status
					);
		IF v_status <> G_NO_ERROR THEN
			write_log('Error while sending email = ' || v_status);
		END IF;
		RETURN;
/*	ELSE
		COMMIT;*/
	END IF;

-- Move file (if error / if not error)
	FOR r_books IN c_books
	LOOP
		IF (p_file_name IS NOT NULL) THEN
			v_file_name	:=	p_file_name;
		ELSE
			IF p_file_name_format IS NULL THEN
				RAISE utl_file.invalid_filename;
			END IF;
			v_file_name	:=	REPLACE(p_file_name_format,
								'$DATE$',
								TRIM(to_char(v_date, 'yyyymmdd')));
			v_file_name	:=	REPLACE(v_file_name,
								'$SOB_FILENAME$',
								r_books.sob_code_in_file);
		END IF;

		IF v_status_all <> G_NO_ERROR THEN
			move_data_file(get_directory_path(p_file_path) || '/' || v_file_name,
							get_directory_path(p_folder_error));
		ELSE
			move_data_file(get_directory_path(p_file_path) || '/' || v_file_name,
							get_directory_path(p_folder_history));
		END IF;
		-- End of import data from text file
		IF (p_file_name IS NOT NULL) THEN
			EXIT;	-- specify filename
		END IF;
	END LOOP;

	SAVEPOINT before_insert_interface;
	v_insert_error_flag	:=	'N';
	-- validate GL
	IF v_status_all <> G_NO_ERROR THEN
		write_log('Validate : Failed.');
	ELSE
		write_log('Validate : Pass.');
		IF (v_insert_itf_table = 'Y') THEN
			BEGIN
				INSERT INTO gl_interface
					(status
					,set_of_books_id
					,accounting_date
					,currency_code
					,date_created
					,created_by
					,actual_flag
					,user_je_category_name
					,user_je_source_name
					,accounted_dr
					,accounted_cr
					,currency_conversion_date
					,currency_conversion_rate
					,entered_dr
					,entered_cr
					,je_line_num
					,period_name
					,reference1
					,reference4
					,reference5
					,segment1
					,segment11
					,segment10
					,segment12
					,segment13
					,segment14
					,segment18
					,segment19
					,segment21
					,reference21
					,reference10	-- JE Line Description
					,group_id)
				SELECT	status
						,set_of_books_id
						,accounting_date
						,currency_code
						,date_created
						,created_by
						,actual_flag
						,user_je_category_name
						,user_je_source_name
						,accounted_dr
						,accounted_cr
						,currency_conversion_date
						,currency_conversion_rate
						,entered_dr
						,entered_cr
						,je_line_num
						,period_name
						,reference1
						,reference4
						,reference5
						,segment1
						,segment11
						,segment10
						,segment12
						,segment13
						,segment14
						,segment18
						,segment19
						,segment21
						,reference21
						,reference10	-- JE Line Description
						,request_id
				FROM	tac_mc_gl_interface_temp tmp
				WHERE	tmp.request_id = v_request_id
				AND		tmp.status = 'NEW';
				v_count_gl	:=	SQL%ROWCOUNT;
--				COMMIT;
			EXCEPTION
				WHEN OTHERS THEN
					v_insert_error_flag	:=	'Y';
					ROLLBACK TO before_insert_interface;
					write_output(' ');
					write_output(' ');
					write_output(SQLERRM);
						
					write_log(' ');
					write_log('TAC_MONEY_COLLECT_GL_ITF.interface_gl @insert interface exception when insert data to gl_interface table. See output for error detail.');
						
					err_msg		:=	'Error TAC_MONEY_COLLECT_GL_ITF.interface_gl @insert interface : Some record during insert data to gl_interface table. See output for detail.';
					err_code	:=	'2';
					RETURN;
			END;
		END IF;
	END IF;

	IF (v_count_gl = v_total_line_count OR v_insert_itf_table = 'N') THEN
		err_code	:=	'0';
	ELSE
		err_code	:=	'2';
		write_output(' ');
		write_output('ERROR : Total records that insert to gl_interface table are not equal (' ||
						v_count_gl || '/' || v_total_line_count ||
						' with text file.');
			
		write_log(' ');
		write_log('ERROR : Total records that insert to gl_interface table are not equal (' ||
					v_count_gl || '/' || v_total_line_count ||
					' with text file.');
	END IF;
	COMMIT;
	write_log('+----------------------------------------------------------------------------+');

EXCEPTION
	WHEN utl_file.invalid_path THEN
		err_code := '2';
		err_msg := SQLERRM;
		write_output('Invalid File Location');
		write_log('Invalid File Location');
		raise_application_error(-20052, 'Invalid File Location');
	WHEN utl_file.invalid_operation THEN
		err_code := '2';
		err_msg := SQLERRM;
		write_output('Invalid File Operation');
		write_log('Invalid Operation');
		raise_application_error(-20054, 'Invalid Operation');
	WHEN utl_file.read_error THEN
		err_code := '2';
		err_msg := SQLERRM;
		write_output('Read File Error');
		write_log('Read Error');
		raise_application_error(-20055, 'Read Error');
	WHEN utl_file.invalid_maxlinesize THEN
		err_code := '2';
		err_msg := SQLERRM;
		write_output('Line Size Exceeds 32K');
		write_log('Line Size Exceeds 32K');
		raise_application_error(-20060, 'Line Size Exceeds 32K');
	WHEN utl_file.invalid_filename THEN
		err_code := '2';
		err_msg := SQLERRM;
		write_output('Invalid File Name');
		write_log('Invalid File Name');
		raise_application_error(-20061, 'Invalid File Name');
	WHEN utl_file.access_denied THEN
		err_code := '2';
		err_msg := SQLERRM;
		write_output('File Access Denied');
		write_log('File Access Denied');
		raise_application_error(-20062, 'File Access Denied');
	WHEN OTHERS THEN
		err_code := '2';
		err_msg := SQLERRM;
		write_output(SQLERRM);
		raise_application_error(-20011, SQLCODE || ': ' || SQLERRM);

END interface_gl;
----------------------------------------------------------------------------
BEGIN
	CT_ERROR('FR-001')	:=	'File reading error';
	CT_ERROR('FF-001')	:=	'File Format error (total 31 fields are required, some fields are missing)';

	CT_ERROR('EV-001')	:=	'SOB ID in text file mismatch to parameter specified';
	CT_ERROR('EV-002')	:=	'Invalid Journal Source specified';
	CT_ERROR('EV-003')	:=	'Invalid Journal Category specified';
	CT_ERROR('EV-004')	:=	'Invalid GL Code Combination specified';
	CT_ERROR('EV-005')	:=	'Accounted DR/CR must be equal to Entered DR/CR';
	CT_ERROR('EV-006')	:=	'Header Description (reference5) must be identical for the same Batch Name (reference1) and Header Name (reference4)';
	CT_ERROR('EV-007')	:=	'Total DR Amount must be equal to Total CR Amount in each Journal Header';

	CT_ERROR('ER-001')	:=	'Unexpected Error';
	CT_ERROR('ER-002')	:=	'SOB ID specified not found in lookup type TAC_IFRS_INTERFACE configuration';
END TAC_MONEY_COLLECT_GL_ITF;
