-- Create table
create table TAC_MC_GL_INTERFACE_TEMP
(
  status                   VARCHAR2(50) not null,
  set_of_books_id          NUMBER(15) not null,
  accounting_date          DATE not null,
  currency_code            VARCHAR2(15) not null,
  date_created             DATE not null,
  created_by               NUMBER(15) not null,
  actual_flag              VARCHAR2(1) not null,
  user_je_category_name    VARCHAR2(25) not null,
  user_je_source_name      VARCHAR2(25) not null,
  accounted_dr             NUMBER,
  accounted_cr             NUMBER,
  currency_conversion_date DATE,
  currency_conversion_rate NUMBER,
  entered_dr               NUMBER,
  entered_cr               NUMBER,
  je_line_num              NUMBER(15),
  period_name              VARCHAR2(15),
  reference1               VARCHAR2(100),
  reference4               VARCHAR2(100),
  reference5               VARCHAR2(240),
  segment1                 VARCHAR2(25),
  segment11                VARCHAR2(25),
  segment10                VARCHAR2(25),
  segment12                VARCHAR2(25),
  segment13                VARCHAR2(25),
  segment14                VARCHAR2(25),
  segment18                VARCHAR2(25),
  segment19                VARCHAR2(25),
  segment21                VARCHAR2(25),
  reference21              VARCHAR2(240),
  reference10              VARCHAR2(240),
  request_id               NUMBER,
  interface_date           DATE,
  validate_status          VARCHAR2(25),
  error_msg                VARCHAR2(1000)
);
-- Create/Recreate indexes 
create index TAC_MC_GL_ITF_TEMP_IDX1 on TAC_MC_GL_INTERFACE_TEMP (REQUEST_ID, STATUS);
create index TAC_MC_GL_ITF_TEMP_IDX2 on TAC_MC_GL_INTERFACE_TEMP (INTERFACE_DATE);

