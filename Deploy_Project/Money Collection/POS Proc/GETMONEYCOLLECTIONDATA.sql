--------------------------------------------------------
--  File created - Wednesday-October-31-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure GETMONEYCOLLECTIONDATA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "POS"."GETMONEYCOLLECTIONDATA" (
  "DATEFROM" IN VARCHAR2, 
  "DATETO" IN VARCHAR2, 
  "CUR_HEADER" OUT SYS_REFCURSOR, 
  "CUR_DATA" OUT SYS_REFCURSOR) IS

  -- Declare constants and variables in this section.
  -- Example: <Variable Identifier> <DATATYPE>
  --          <Variable Identifier> CONSTANT <DATATYPE>
  --          varEname  VARCHAR2(40);
  --          varComm   REAL;
  --          varSalary CONSTANT NUMBER:=1000;
  --          comm_missing EXCEPTION;

BEGIN -- executable part starts here


	open cur_header for 'select ''Data'' as message from dual';

	open cur_data for 
	SELECT 
		stp.ROWID ID,
		TO_CHAR(stn.DOC_DATE, 'YYYY/MM/DD HH24:MI:SS') DOCUMENT_DATE, 
		stn.DOC_NO DOCUMENT_NO, 
		stn.ou_code COMPANY, 
		stn.SUBINV_CODE LOCATION_CODE, 
		(SELECT subinv_desc FROM pb_subinv WHERE subinv_code = stn.subinv_code AND ou_code = stn.OU_CODE) LOCATION_NAME, 
		'SERVICE' PRODUCT_TYPE,
		-- add by nawatsapon, 27-AUG-2018 --
		stn.CUST_NAME CUSTOMER_NAME, 
		nvl(stn.TAX_ID,' ') TAX_ID,
		------------------------------------
		DECODE(NVL(stn.DOC_TYPE,3),3, (stn.NET_AMT * -1), stn.NET_AMT) RECEIPT_AMOUNT,
		DECODE(NVL(stn.DOC_TYPE,3),3, (NVL(stn.wht_amt, 0) * -1), NVL(stn.wht_amt, 0)) WHT,
		NVL(stp.payment_code, ' ') PAYMENT_TYPE,
		DECODE(NVL(stn.DOC_TYPE,3),3, (NVL(stp.pay_amt, 0) * -1), NVL(stp.pay_amt, 0)) PAYMENT_AMOUNT,
		-- add by nawatsapon, 27-AUG-2018 --
		--(SELECT bank_name FROM SA_BANK WHERE bank_code = nvl(stp.BANK_CODE, ' ')) BANK_NAME,
		nvl(stp.BANK_CODE,' ') BANK_CODE,
		------------------------------------
		NVL(stp.REF_NO, ' ') REF_NO
	FROM SC_TRANSACTION stn
	INNER JOIN SC_trans_pay stp ON stn.doc_no = stp.doc_no
	WHERE 1=1
	AND DOC_DATE >= TO_DATE(DATEFROM, 'YYYY-MM-DD HH24:MI:SS') 
	AND DOC_DATE <= TO_DATE("DATETO", 'YYYY-MM-DD HH24:MI:SS')
	UNION
	SELECT 
		stp.ROWID ID,
		TO_CHAR(stn.DOC_DATE, 'YYYY/MM/DD HH24:MI:SS') DOCUMENT_DATE, 
		stn.DOC_NO DOCUMENT_NO, 
		stn.ou_code COMPANY, 
		stn.SUBINV_CODE LOCATION_CODE, 
		(SELECT subinv_desc FROM pb_subinv WHERE subinv_code = stn.subinv_code AND ou_code = stn.OU_CODE) LOCATION_NAME, 
		'GOODS' PRODUCT_TYPE,
		-- add by nawatsapon, 27-AUG-2018 --
		stn.CUST_NAME CUSTOMER_NAME, 
		nvl(stn.TAX_ID,' ') TAX_ID,
		------------------------------------		
		DECODE(NVL(stn.DOC_TYPE,3),3, (stn.NET_AMT * -1), stn.NET_AMT) RECEIPT_AMOUNT,
		0 WHT,
		NVL(stp.payment_code, ' ') PAYMENT_TYPE,
		DECODE(NVL(stn.DOC_TYPE,3),3, (NVL(stp.pay_amt, 0) * -1), NVL(stp.pay_amt, 0)) PAYMENT_AMOUNT,
		-- add by nawatsapon, 27-AUG-2018 --
		--(SELECT bank_name FROM SA_BANK WHERE bank_code = nvl(stp.BANK_CODE, ' ')) BANK_NAME,
		nvl(stp.BANK_CODE,' ') BANK_CODE,
		------------------------------------
		NVL(stp.REF_NO, ' ') REF_NO
	FROM SA_TRANSACTION stn
	INNER JOIN SA_trans_pay stp ON stn.doc_no = stp.doc_no
	WHERE 1=1
	AND DOC_DATE >= TO_DATE(DATEFROM, 'YYYY-MM-DD HH24:MI:SS') 
	AND DOC_DATE <= TO_DATE("DATETO", 'YYYY-MM-DD HH24:MI:SS')
	ORDER BY DOCUMENT_DATE ASC;

END;

/
