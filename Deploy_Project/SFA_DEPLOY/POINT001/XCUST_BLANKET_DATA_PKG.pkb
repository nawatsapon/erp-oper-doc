CREATE OR REPLACE PACKAGE BODY APPS.XCUST_BLANKET_DATA_PKG
IS
   FUNCTION get_blanket_data (p_org_id IN NUMBER default null,
                              p_item_code IN VARCHAR2 default null)
      RETURN TAC_blanket_data_table
   IS
   
   CURSOR BLANKET IS
       select DISTINCT A.BLANKET_ID,
       A.SEGMENT1,
       A.VENDOR_CODE,
       A.VENDOR_NAME,
       A.VENDOR_SITE_CODE,
       A.Line_Number,
       A.UOM,
       A.PRICE,
       A.Currency
from 
(
(SELECT  H.po_header_id BLANKET_ID,
                  H.SEGMENT1,
                  V.SEGMENT1 VENDOR_CODE,
                  V.VENDOR_NAME,
                  VS.VENDOR_SITE_CODE,
                  L.LINE_NUM Line_Number,
                  L.UNIT_MEAS_LOOKUP_CODE UOM,
                  L.UNIT_PRICE PRICE,
                  H.CURRENCY_CODE Currency
  FROM   po_headers_all H,
         po_lines_all l,
         PO_VENDORS V,
         PO_VENDOR_SITES_ALL VS,
         MTL_SYSTEM_ITEMS I
 WHERE       H.PO_HEADER_ID = L.PO_HEADER_ID
         AND H.ORG_ID = L.ORG_ID
         AND H.VENDOR_ID = V.VENDOR_ID
         AND V.VENDOR_ID = VS.VENDOR_ID
         AND H.VENDOR_SITE_ID = VS.VENDOR_SITE_ID
         AND H.ORG_ID = VS.ORG_ID
         AND L.ITEM_ID = I.INVENTORY_ITEM_ID
       --  AND H.ORG_ID = I.ORGANIZATION_ID
         AND H.TYPE_LOOKUP_CODE = 'BLANKET'
         AND H.AUTHORIZATION_STATUS = 'APPROVED'
         
         AND (l.COMMITTED_AMOUNT IS NULL
              OR l.COMMITTED_AMOUNT >   
                      (  SELECT   SUM (plla.price_override * plla.quantity)
                                     released_amount
                           FROM   po_headers_all pha1,
                                  po_line_locations_all plla,
                                  po_lines_all L1
                          WHERE       plla.po_header_id = pha1.po_header_id
                                  AND plla.ORG_ID = pha1.ORG_ID
                                  AND plla.po_header_id = L1.po_header_id
                                  AND plla.po_line_id = L1.po_line_id
                                  AND plla.ORG_ID = L1.ORG_ID
                                  AND L1.from_header_id = L.PO_HEADER_ID
                                  AND L1.from_line_id = L.PO_LINE_ID
                                  AND L1.ORG_ID = L.ORG_ID
                       GROUP BY   L1.from_header_id, L1.from_line_id))
         AND (L.EXPIRATION_DATE IS NULL OR L.EXPIRATION_DATE > SYSDATE)
         
         AND (H.BLANKET_TOTAL_AMOUNT IS NULL
              OR H.BLANKET_TOTAL_AMOUNT >
                      (  SELECT   SUM (plla.price_override * plla.quantity)
                                     released_amount
                           FROM   po_headers_all pha2,
                                  po_line_locations_all plla,
                                  po_lines_all l2
                          WHERE       plla.po_header_id = pha2.po_header_id
                                  AND plla.ORG_ID = pha2.ORG_ID
                                  AND plla.po_header_id = l2.po_header_id
                                  AND plla.po_line_id = l2.po_line_id
                                  AND plla.ORG_ID = l2.ORG_ID
                                  AND l2.from_header_id = H.PO_HEADER_ID
                                  AND l2.ORG_ID = H.ORG_ID
                       GROUP BY   L2.from_header_id
                       ))
         AND (H.END_DATE IS NULL OR H.END_DATE > SYSDATE)
         
         AND H.ORG_ID=P_ORG_ID  --102
         AND I.SEGMENT1 = P_ITEM_CODE -- 'S12NO15201I'
         )
         union all
         (
         
SELECT    H.po_header_id BLANKET_ID,
                  H.SEGMENT1,
                  V.SEGMENT1 VENDOR_CODE,
                  V.VENDOR_NAME,
                  VS.VENDOR_SITE_CODE,
                  L.LINE_NUM Line_Number,
                  L.UNIT_MEAS_LOOKUP_CODE UOM,
                  L.UNIT_PRICE PRICE,
                  H.CURRENCY_CODE Currency
  FROM   po_headers_all H,
         po_lines_all l,
         PO_VENDORS V,
         PO_VENDOR_SITES_ALL VS,
         MTL_SYSTEM_ITEMS I
 WHERE       H.PO_HEADER_ID = L.PO_HEADER_ID
         AND H.ORG_ID = L.ORG_ID
         AND H.VENDOR_ID = V.VENDOR_ID
         AND V.VENDOR_ID = VS.VENDOR_ID
         AND H.ORG_ID = VS.ORG_ID
         AND H.VENDOR_SITE_ID = VS.VENDOR_SITE_ID
         AND L.ITEM_ID = I.INVENTORY_ITEM_ID
         --AND H.ORG_ID = I.ORGANIZATION_ID
         AND H.TYPE_LOOKUP_CODE = 'BLANKET'
         AND H.AUTHORIZATION_STATUS = 'APPROVED'
          AND (l.COMMITTED_AMOUNT IS NULL
              OR l.COMMITTED_AMOUNT >
                  
                      (  SELECT   SUM (plla.price_override * plla.quantity)
                                     released_amount
                           FROM   po_headers_all pha1,
                                  po_line_locations_all plla,
                                  po_lines_all l1
                          WHERE       plla.po_header_id = pha1.po_header_id
                                  AND plla.ORG_ID = pha1.ORG_ID
                                  AND plla.po_header_id = l1.po_header_id
                                  AND plla.po_line_id = l1.po_line_id
                                  AND plla.ORG_ID = l1.ORG_ID
                                  AND l1.po_header_id = L.PO_HEADER_ID
                                  AND l1.po_line_id = L.PO_LINE_ID
                                  AND l1.ORG_ID = L.ORG_ID
                       GROUP BY   l1.from_header_id, l1.from_line_id))
         AND (L.EXPIRATION_DATE IS NULL OR L.EXPIRATION_DATE > SYSDATE)
         AND (H.BLANKET_TOTAL_AMOUNT IS NULL
              OR H.BLANKET_TOTAL_AMOUNT >
                      (  SELECT   SUM (plla.price_override * plla.quantity)
                                     released_amount
                           FROM   po_headers_all pha2,
                                  po_line_locations_all plla,
                                  po_lines_all l2
                          WHERE       plla.po_header_id = pha2.po_header_id
                                  AND plla.ORG_ID = PHA2.ORG_ID
                                  AND plla.po_header_id = l2.po_header_id
                                  AND plla.po_line_id = l2.po_line_id
                                  AND plla.ORG_ID = L2.ORG_ID
                                  AND L2.po_header_id = H.PO_HEADER_ID 
                                  AND L2.ORG_ID = H.ORG_ID
                       GROUP BY   l2.from_header_id
                       ))
         AND (H.END_DATE IS NULL OR H.END_DATE > SYSDATE)
         AND H.ORG_ID=P_ORG_ID  --102
         AND I.SEGMENT1 = P_ITEM_CODE --'S12NO15201I'
         )) A
       ;
       
       
       
      p_blanket_data   tac_blanket_data;  --po_headers_all%ROWTYPE;
      t_blanket_data   tac_blanket_data_table := tac_blanket_data_table();
      I NUMBER := 0;
 
   BEGIN

       /* SELECT   TAC_blanket_data (1,
                                  '',
                                  '',
                                  '',
                                  '',
                                  '',
                                  '',
                                  '')
        INTO   p_blanket_data
        FROM   DUAL;*/
     
    FOR REC_B IN  BLANKET
    LOOP
      i := i + 1;

       p_blanket_data          := tac_blanket_data(null, null, null,null, null, null, null, null,null);
       p_blanket_data.Blanket_ID       := REC_B.BLANKET_ID;
       p_blanket_data.Vendor_Code      := REC_B.Vendor_Code;
       p_blanket_data.Vendor_Name      := REC_B.Vendor_Name;
       p_blanket_data.Vendor_Site_Code := REC_B.Vendor_Site_Code;
       p_blanket_data.Line_Number     := REC_B.Line_Number;
       p_blanket_data.UOM             := REC_B.UOM;
       p_blanket_data.Price           := REC_B.PRICE;
       p_blanket_data.Currency        := REC_B.Currency;
       p_blanket_data.Error_Msg       := '';


       t_blanket_data.extend;
       t_blanket_data(i) := p_blanket_data;  

    END LOOP;
    
    
   
    
    IF I = 0 THEN
     i := i + 1;
       p_blanket_data          := tac_blanket_data(null, null, null,null, null, null, null, null,null);
       
    IF p_org_id IS NULL  THEN 
      p_blanket_data.Error_Msg           := 'No ORG_ID found,';
    ELSIF p_item_code IS NULL THEN
      p_blanket_data.Error_Msg           := 'No ITEM CODE found';
    ELSE
      p_blanket_data.Error_Msg           := 'No Data Found';
    END IF;
       
       
       t_blanket_data.extend;
       t_blanket_data(i) := p_blanket_data;  
       
    
    END IF;
    
    
      RETURN (t_blanket_data);
 
   
EXCEPTION WHEN OTHERS THEN


       p_blanket_data          := tac_blanket_data(null, null, null,null, null, null, null, null,null);
       p_blanket_data.Error_Msg           := 'Others Error :'|| sqlerrm;
       t_blanket_data.extend;
       t_blanket_data(1) := p_blanket_data;  

    
      RETURN (t_blanket_data);   
  END;
END;
/
