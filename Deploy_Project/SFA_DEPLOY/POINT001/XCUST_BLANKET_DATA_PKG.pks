CREATE OR REPLACE PACKAGE APPS.XCUST_BLANKET_DATA_PKG
AS
   FUNCTION get_blanket_data (p_org_id IN NUMBER default null,
                              p_item_code IN VARCHAR2 default null)
      RETURN TAC_blanket_data_table;                       --po_headers_all%rowtype;
      
                             
 
END XCUST_BLANKET_DATA_PKG;
/
