CREATE OR REPLACE PACKAGE APPS.XCUST_SUPPLIER_LIST_PKG
AS

   FUNCTION get_supplier_list (p_org_id IN NUMBER DEFAULT 0, 
                               p_item_code IN VARCHAR2)
      RETURN tac_supplier_list_table;                 --po_vendor_sites_all%rowtype;


END XCUST_SUPPLIER_LIST_PKG;
/
