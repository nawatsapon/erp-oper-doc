CREATE OR REPLACE PACKAGE BODY APPS.XCUST_SUPPLIER_LIST_PKG
IS
  

   FUNCTION get_supplier_list (p_org_id IN NUMBER DEFAULT 0 , 
                               p_item_code IN VARCHAR2)
      RETURN tac_supplier_list_table                   --po_vendor_sites_all%rowtype
   IS
     
     cursor SUP(PIN_ORG_ID NUMBER) IS
       
       SELECT 
        V.SEGMENT1 VENDOR_CODE,
        V.VENDOR_NAME,
        VS.VENDOR_SITE_CODE
        FROM
        PO_VENDORS V,
        PO_VENDOR_SITES_ALL VS
       WHERE 
       V.VENDOR_ID = VS.VENDOR_ID
       AND V.ATTRIBUTE12 = 'Y'
       AND (V.END_DATE_ACTIVE IS NULL OR V.END_DATE_ACTIVE> SYSDATE)
       AND (VS.INACTIVE_DATE IS NULL OR VS.INACTIVE_DATE> SYSDATE)
      --AND V.SEGMENT1='206293'
       AND VS.ORG_ID=NVL(PIN_ORG_ID,0)
       /*UNION ALL
       SELECT 
        'TEST' VENDOR_CODE,
        'TEST' VENDOR_NAME,
        'TEST' VENDOR_SITE_CODE
        FROM
        DUAL*/
        --WHERE PIN_ORG_ID = 0
       ORDER BY 1
       ;
      
      
       --p_supplier_list po_vendor_sites_all%rowtype;
      p_supplier_list   tac_supplier_list;
      t_supplier_list   tac_supplier_list_table := tac_supplier_list_table();
      
      VORG_ID NUMBER;
      I NUMBER := 0;
   BEGIN
   
   VORG_ID := NVL(P_ORG_ID,0);
   
     FOR REC_SUP IN SUP(PIN_ORG_ID => VORG_ID)
     LOOP

 
       i := i + 1;

       p_supplier_list          := tac_supplier_list(null, null, null,null);
       -- p_supplier_list          := tac_supplier_list(REC_SUP.Vendor_Code, REC_SUP.Vendor_Name, REC_SUP.Vendor_Site_Code,null);
       p_supplier_list.Vendor_Code           := REC_SUP.Vendor_Code;
       p_supplier_list.Vendor_Name           := REC_SUP.Vendor_Name;
       p_supplier_list.Vendor_Site_Code      := REC_SUP.Vendor_Site_Code;
       p_supplier_list.Error_Msg      := '';
       
       t_supplier_list.extend;
       t_supplier_list(i) := p_supplier_list;  

     END LOOP;
     
     
    
    IF I = 0 THEN
       i := i + 1;
       p_supplier_list          := tac_supplier_list(null, null, null,null);
       --p_supplier_list          := tac_supplier_list('A', 'A', 'A',null);
        IF p_org_id IS NULL  THEN 
     
           p_supplier_list.Error_Msg           := 'No ORG ID found';
        ELSE
           p_supplier_list.Error_Msg           := 'No Data Found';
    
        END IF;
        
       t_supplier_list.extend;
       t_supplier_list(i) := p_supplier_list;  
       
    
    END IF;
    


      RETURN (t_supplier_list);
      
   EXCEPTION WHEN OTHERS THEN
   
       p_supplier_list          := tac_supplier_list(null, null, null,null);
       p_supplier_list.Error_Msg           := 'Others Error :'|| sqlerrm;
       t_supplier_list.extend;
       t_supplier_list(1) := p_supplier_list;  
       
       RETURN (t_supplier_list);
   END get_supplier_list;
   
   
   
  /*  FUNCTION get_supplier_list (p_org_id IN NUMBER, p_item_code IN VARCHAR2)
      RETURN tac_supplier_list_table                   --po_vendor_sites_all%rowtype
   IS
     
    
      
       --p_supplier_list po_vendor_sites_all%rowtype;
      p_supplier_list   tac_supplier_list;
      t_supplier_list   tac_supplier_list_table := tac_supplier_list_table();
      I NUMBER;
   BEGIN
   

       p_supplier_list          := tac_supplier_list('a', 'v', 'd','g');
      
       t_supplier_list.extend;
       --t_supplier_list(1).Vendor_Code := 'aaaaa';  
       t_supplier_list(1) := p_supplier_list; 


      RETURN t_supplier_list;
   END get_supplier_list;*/

END;
/
