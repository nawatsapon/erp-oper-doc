DROP TYPE APPS.TAC_PO_STATUS;

CREATE OR REPLACE TYPE APPS.tac_po_status AS OBJECT
(
     Quote_Number VARCHAR2(40)
    ,Transaction_ID varchar2(50)
    ,PO_Number VARCHAR2(40)
    ,PO_Release_No VARCHAR2(40)
    ,PO_STATUS VARCHAR2(40)
    ,PO_Price NUMBER
    ,Currency VARCHAR2(40)
    ,Vendor_Num VARCHAR2(240)
    ,Vendor_Name VARCHAR2(240)
    ,Vendor_Site_Code VARCHAR2(40)
    ,Error_Msg VARCHAR2(240)
);
/

