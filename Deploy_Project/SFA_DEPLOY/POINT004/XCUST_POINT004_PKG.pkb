CREATE OR REPLACE PACKAGE BODY APPS.XCUST_POINT004_PKG
IS
   FUNCTION get_po_status (
      p_org_id                      IN NUMBER,
      p_Quote_Number                IN VARCHAR2,
      p_tac_po_status_input_table   IN tac_po_status_input_table
   )
      --(p_org_id IN NUMBER, p_Quote_Number IN VARCHAR2,p_Transaction_ID IN NUMBER)
      RETURN TAC_po_status_table
   IS
      CURSOR PO (
         p_org_id           IN            NUMBER,
         p_Quote_Number     IN            VARCHAR2,
         p_Transaction_ID   IN            VARCHAR2
      )
      IS
         /*SELECT   A.ORG_ID,
                  A.Quote_Number,
                  A.Transaction_ID,
                  A.PR_IR_Number,
                  A.PO_NUMBER,
                  A.PO_RELEASE_NO,
                  A.PO_PRICE,
                  A.CURRENCY,
                  A.VENDOR_CODE,
                  A.VENDOR_NAME,
                  A.VENDOR_SITE_CODE
           FROM   ( (                        SELECT   DISTINCT H.ORG_ID,
                                       l.attribute1 Quote_Number,
                                       l.attribute2 Transaction_ID,
                                       h.segment1 PR_IR_Number,
                                       PO_STD.SEGMENT1 PO_NUMBER,
                                       '' PO_RELEASE_NO,
                                       PO_STD.PO_PRICE,
                                       PO_STD.CURRENCY,
                                       PO_STD.VENDOR_CODE,
                                       PO_STD.VENDOR_NAME,
                                       PO_STD.VENDOR_SITE_CODE
                       FROM   po_requisition_headers_all h,
                              PO_REQUISITION_LINES_ALL l,
                              po_req_distributions_all d,
                              XCUST_POINT004_STD_V PO_STD
                      WHERE   h.requisition_header_id =
                                 l.requisition_header_id
                              AND H.ORG_ID = L.ORG_ID
                              AND d.requisition_line_id = l.requisition_line_id
                              AND d.distribution_id = PO_STD.req_distribution_id
                              AND L.LINE_LOCATION_ID = PO_STD.LINE_LOCATION_ID
                              AND H.ORG_ID = PO_STD.ORG_ID)
                   UNION ALL
                   (SELECT   DISTINCT H.ORG_ID,
                                      l.attribute1 Quote_Number,
                                      l.attribute2 Transaction_ID,
                                      h.segment1 PR_IR_Number,
                                      '' PO_NUMBER,
                                      PO_BLK.PO_RELEASE_NO,
                                      PO_BLK.PO_PRICE,
                                      PO_BLK.CURRENCY,
                                      PO_BLK.VENDOR_CODE,
                                      PO_BLK.VENDOR_NAME,
                                      PO_BLK.VENDOR_SITE_CODE
                      FROM   po_requisition_headers_all h,
                             PO_REQUISITION_LINES_ALL l,
                             XCUST_POINT004_BLK_V PO_BLK
                     WHERE   h.requisition_header_id =
                                l.requisition_header_id
                             AND H.ORG_ID = L.ORG_ID
                             AND L.BLANKET_PO_HEADER_ID = PO_BLK.PO_header_id
                             AND L.LINE_LOCATION_ID = PO_BLK.LINE_LOCATION_ID))
                  A
          WHERE       A.ORG_ID = p_org_id                                --102
                  AND A.Quote_Number = p_Quote_Number              --SFAQUOTE1
                  AND A.Transaction_ID = p_Transaction_ID           -- SFA1001
        UNION ALL

        SELECT    NULL ORG_ID,
                  p_Quote_Number Quote_Number,
                  p_Transaction_ID Transaction_ID,
                  NULL PR_IR_Number,
                  NULL PO_NUMBER,
                  NULL PO_RELEASE_NO,
                  NULL PO_PRICE,
                  NULL CURRENCY,
                  NULL VENDOR_CODE,
                  NULL VENDOR_NAME,
                 NULL VENDOR_SITE_CODE
         FROM DUAL
         WHERE  p_Transaction_ID  NOT IN
              ( SELECT A.Transaction_ID
           FROM   ( (                        SELECT   DISTINCT H.ORG_ID,
                                       l.attribute1 Quote_Number,
                                       l.attribute2 Transaction_ID,
                                       h.segment1 PR_IR_Number,
                                       PO_STD.SEGMENT1 PO_NUMBER,
                                       '' PO_RELEASE_NO,
                                       PO_STD.PO_PRICE,
                                       PO_STD.CURRENCY,
                                       PO_STD.VENDOR_CODE,
                                       PO_STD.VENDOR_NAME,
                                       PO_STD.VENDOR_SITE_CODE
                       FROM   po_requisition_headers_all h,
                              PO_REQUISITION_LINES_ALL l,
                              po_req_distributions_all d,
                              XCUST_POINT004_STD_V PO_STD
                      WHERE   h.requisition_header_id =
                                 l.requisition_header_id
                              AND H.ORG_ID = L.ORG_ID
                              AND d.requisition_line_id = l.requisition_line_id
                              AND d.distribution_id = PO_STD.req_distribution_id
                              AND L.LINE_LOCATION_ID = PO_STD.LINE_LOCATION_ID
                              AND H.ORG_ID = PO_STD.ORG_ID )
                   UNION ALL
                   (SELECT   DISTINCT H.ORG_ID,
                                      l.attribute1 Quote_Number,
                                      l.attribute2 Transaction_ID,
                                      h.segment1 PR_IR_Number,
                                      '' PO_NUMBER,
                                      PO_BLK.PO_RELEASE_NO,
                                      PO_BLK.PO_PRICE,
                                      PO_BLK.CURRENCY,
                                      PO_BLK.VENDOR_CODE,
                                      PO_BLK.VENDOR_NAME,
                                      PO_BLK.VENDOR_SITE_CODE
                      FROM   po_requisition_headers_all h,
                             PO_REQUISITION_LINES_ALL l,
                             XCUST_POINT004_BLK_V PO_BLK
                     WHERE   h.requisition_header_id =
                                l.requisition_header_id
                             AND H.ORG_ID = L.ORG_ID
                             AND L.BLANKET_PO_HEADER_ID = PO_BLK.PO_header_id
                             AND L.LINE_LOCATION_ID = PO_BLK.LINE_LOCATION_ID))
                  A
          WHERE       A.ORG_ID = p_org_id                                --102
                  AND A.Quote_Number = p_Quote_Number              --SFAQUOTE1
                  AND A.Transaction_ID = p_Transaction_ID )          -- SFA1001
                                                         ;*/
         /* SELECT   DISTINCT H.ORG_ID,
                            l.attribute1 Quote_Number,
                            l.attribute2 Transaction_ID,
                            h.segment1 PR_IR_Number,
                            d.distribution_id,
                            L.BLANKET_PO_HEADER_ID,
                            L.LINE_LOCATION_ID
            FROM   po_requisition_headers_all h,
                   PO_REQUISITION_LINES_ALL l,
                   po_req_distributions_all d
           WHERE       h.requisition_header_id = l.requisition_header_id
                   AND H.ORG_ID = L.ORG_ID
                   AND d.requisition_line_id = l.requisition_line_id
                   AND H.ORG_ID = D.ORG_ID
                   AND H.ORG_ID = p_org_id                                --102
                   AND l.attribute1 = p_Quote_Number                --SFAQUOTE1
                   AND l.attribute2 = p_Transaction_ID --SFA1001
          UNION ALL
          SELECT   NULL ORG_ID,
                   p_Quote_Number Quote_Number,
                   p_Transaction_ID Transaction_ID,
                   NULL PR_IR_Number,
                   NULL distribution_id,
                   NULL BLANKET_PO_HEADER_ID,
                   NULL LINE_LOCATION_ID
            FROM   DUAL
           WHERE   p_Transaction_ID NOT IN
                         (SELECT   DISTINCT l.attribute2
                            FROM   po_requisition_headers_all h,
                                   PO_REQUISITION_LINES_ALL l,
                                   po_req_distributions_all d
                           WHERE   h.requisition_header_id =
                                      l.requisition_header_id
                                   AND H.ORG_ID = L.ORG_ID
                                   AND d.requisition_line_id =
                                         l.requisition_line_id
                                   AND H.ORG_ID = D.ORG_ID
                                   AND H.ORG_ID = p_org_id                --102
                                   AND l.attribute1 = p_Quote_Number --SFAQUOTE1
                                   AND l.attribute2 = p_Transaction_ID -- SFA1001
                                                                      );*/

         SELECT   DISTINCT H.ORG_ID,
                           H.BATCH_NAME Quote_Number,
                           H.TRANSACTION_ID Transaction_ID,
                           h.PR_NUMBER PR_IR_Number,
                           d.distribution_id,
                           L.BLANKET_PO_HEADER_ID,
                           L.LINE_LOCATION_ID,
                           H.requisition_header_id,
                           l.requisition_line_id,
                           h.REQUISITION_TYPE
           FROM   XCUST_PO_INT002_STG h,
                  PO_REQUISITION_LINES_ALL l,
                  po_req_distributions_all d
          WHERE       h.requisition_header_id = l.requisition_header_id
                  AND H.REQUISITION_LINE_ID = L.REQUISITION_LINE_ID
                  AND d.requisition_line_id = l.requisition_line_id
                  AND H.ORG_ID = L.ORG_ID
                  AND L.ORG_ID = D.ORG_ID
                  AND H.ORG_ID = p_org_id                                --102
                  AND H.BATCH_NAME = p_Quote_Number                --SFAQUOTE1
                  AND H.TRANSACTION_ID = p_Transaction_ID            --SFA1001
                  AND h.intf_date =
                        (SELECT   MAX (intf_date)
                           FROM   XCUST_PO_INT002_STG
                          WHERE       ORG_ID = p_org_id
                                  AND BATCH_NAME = p_Quote_Number
                                  AND TRANSACTION_ID = p_Transaction_ID)
         UNION ALL
         SELECT   NULL ORG_ID,
                  p_Quote_Number Quote_Number,
                  p_Transaction_ID Transaction_ID,
                  NULL PR_IR_Number,
                  NULL distribution_id,
                  NULL BLANKET_PO_HEADER_ID,
                  NULL LINE_LOCATION_ID,
                  NULL requisition_header_id,
                  NULL requisition_line_id,
                  NULL REQUISITION_TYPE
           FROM   DUAL
          WHERE   p_Transaction_ID NOT IN
                        (SELECT   DISTINCT H.TRANSACTION_ID
                           FROM   XCUST_PO_INT002_STG h,
                                  PO_REQUISITION_LINES_ALL l,
                                  po_req_distributions_all d
                          WHERE   h.requisition_header_id =
                                     l.requisition_header_id
                                  AND H.REQUISITION_LINE_ID =
                                        L.REQUISITION_LINE_ID
                                  AND d.requisition_line_id =
                                        l.requisition_line_id
                                  AND H.ORG_ID = L.ORG_ID
                                  AND L.ORG_ID = D.ORG_ID
                                  AND H.ORG_ID = p_org_id                --102
                                  AND H.BATCH_NAME = p_Quote_Number --SFAQUOTE1
                                  AND H.TRANSACTION_ID = p_Transaction_ID --SFA1001
                                  AND h.intf_date =
                                        (SELECT   MAX (intf_date)
                                           FROM   XCUST_PO_INT002_STG
                                          WHERE   ORG_ID = p_org_id
                                                  AND BATCH_NAME =
                                                        p_Quote_Number
                                                  AND TRANSACTION_ID =
                                                        p_Transaction_ID));


      p_Transaction_ID     VARCHAR2 (50);
      p_po_status          tac_po_status;
      t_po_status          tac_po_status_table := tac_po_status_table ();
      I                    NUMBER := 0;
      P_ERR_STATUS         VARCHAR2 (240);
      P_ERR_STATUS_LINE    VARCHAR2 (240);

      V_PO_NUMBER          VARCHAR2 (50);
      V_PO_RELEASE_NO      VARCHAR2 (50);
      V_PO_STATUS          VARCHAR2 (50);
      V_PO_PRICE           NUMBER;
      V_CURRENCY           VARCHAR2 (50);
      V_VENDOR_CODE        VARCHAR2 (50);
      V_VENDOR_NAME        VARCHAR2 (250);
      V_VENDOR_SITE_CODE   VARCHAR2 (50);
   BEGIN
      IF p_org_id IS NULL OR p_Quote_Number IS NULL
      --OR p_tac_po_status_input_table (1).Transaction_ID is null
      THEN
         p_po_status :=
            tac_po_status (NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL);

         IF p_org_id IS NULL
         THEN
            P_ERR_STATUS := 'No ORG_ID found,';
         END IF;

         IF p_Quote_Number IS NULL
         THEN
            P_ERR_STATUS := P_ERR_STATUS || 'No Quote Number found';
         END IF;
      END IF;

      IF p_tac_po_status_input_table.COUNT > 50
      THEN
         p_po_status :=
            tac_po_status (NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL);
         p_po_status.Error_Msg := 'Transactions more than 50 rows';

         t_po_status.EXTEND;
         t_po_status (1) := p_po_status;
      ELSE
         FOR j IN 1 .. p_tac_po_status_input_table.COUNT
         LOOP
            p_Transaction_ID := p_tac_po_status_input_table (j).Transaction_ID;


            FOR REC_PO IN PO (p_org_id, p_Quote_Number, p_Transaction_ID)
            LOOP
               i := i + 1;
               V_PO_NUMBER := NULL;
               V_PO_RELEASE_NO := NULL;
               V_PO_STATUS := NULL;
               V_PO_PRICE := NULL;
               V_CURRENCY := NULL;
               V_VENDOR_CODE := NULL;
               V_VENDOR_NAME := NULL;
               V_VENDOR_SITE_CODE := NULL;
               
                IF p_Transaction_ID IS NULL AND P_ERR_STATUS IS NOT NULL
               THEN
                  P_ERR_STATUS_LINE := P_ERR_STATUS || ',';
               ELSE
                  P_ERR_STATUS_LINE := P_ERR_STATUS;
               END IF;

               IF p_Transaction_ID IS NULL
               THEN
                  P_ERR_STATUS_LINE :=
                     P_ERR_STATUS_LINE || 'No Transaction ID found';
               ELSIF p_Transaction_ID IS NOT NULL AND p_Quote_Number IS NOT NULL AND REC_PO.LINE_LOCATION_ID IS NULL
               THEN
                 -- P_ERR_STATUS_LINE := P_ERR_STATUS_LINE || 'No Data Found';
                 NULL;
               END IF;


               IF REC_PO.REQUISITION_TYPE = 'PURCHASE'
               THEN
                  BEGIN
                     SELECT   A.PO_NUMBER,
                              A.PO_RELEASE_NO,
                              A.PO_STATUS,
                              A.PO_PRICE,
                              A.CURRENCY,
                              A.VENDOR_CODE,
                              A.VENDOR_NAME,
                              A.VENDOR_SITE_CODE
                       INTO   V_PO_NUMBER,
                              V_PO_RELEASE_NO,
                              V_PO_STATUS,
                              V_PO_PRICE,
                              V_CURRENCY,
                              V_VENDOR_CODE,
                              V_VENDOR_NAME,
                              V_VENDOR_SITE_CODE
                       FROM   (SELECT   SEGMENT1 PO_NUMBER,
                                        '' PO_RELEASE_NO,
                                        PO_STATUS,
                                        PO_PRICE,
                                        CURRENCY,
                                        VENDOR_CODE,
                                        VENDOR_NAME,
                                        VENDOR_SITE_CODE
                                 FROM   XCUST_POINT004_STD_V
                                WHERE   ORG_ID = REC_PO.ORG_ID           --102
                                        AND REQ_DISTRIBUTION_ID =
                                              REC_PO.distribution_id  --498915
                                        AND LINE_LOCATION_ID =
                                              REC_PO.LINE_LOCATION_ID --280533
                               UNION ALL
                               SELECT   '' PO_NUMBER,
                                        PO_RELEASE_NO,
                                        PO_STATUS,
                                        PO_PRICE,
                                        CURRENCY,
                                        VENDOR_CODE,
                                        VENDOR_NAME,
                                        VENDOR_SITE_CODE
                                 FROM   XCUST_POINT004_BLK_V
                                WHERE   ORG_ID = REC_PO.ORG_ID          -- 102
                                        AND PO_header_id =
                                              REC_PO.BLANKET_PO_HEADER_ID --161889
                                        AND LINE_LOCATION_ID =
                                              REC_PO.LINE_LOCATION_ID --280536
                                                                     ) A;
                  EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        P_ERR_STATUS_LINE := P_ERR_STATUS_LINE || 'No Data Found';
                  END;
               ELSE
                  --  INTERNAL
                  BEGIN
                     SELECT   OH.ORDER_NUMBER PO_NUMBER,
                              '' PO_RELEASE_NO,
                              OH.FLOW_STATUS_CODE PO_STATUS,
                              OL.UNIT_SELLING_PRICE PO_PRICE,
                              OH.TRANSACTIONAL_CURR_CODE CURRENCY,
                              cust_acct.account_number VENDOR_CODE,
                              party.party_name VENDOR_NAME,
                              (SELECT   attribute7
                                 FROM   HZ_CUST_ACCT_SITES_ALL
                                WHERE   CUST_ACCT_SITE_ID =
                                           ship_su.CUST_ACCT_SITE_ID)
                                 VENDOR_SITE_CODE
                       INTO   V_PO_NUMBER,
                              V_PO_RELEASE_NO,
                              V_PO_STATUS,
                              V_PO_PRICE,
                              V_CURRENCY,
                              V_VENDOR_CODE,
                              V_VENDOR_NAME,
                              V_VENDOR_SITE_CODE
                       FROM   OE_ORDER_HEADERS_ALL OH,
                              OE_ORDER_LINES_ALL OL,
                              hz_parties party,
                              hz_cust_accounts cust_acct,
                              hz_cust_site_uses_all ship_su
                      WHERE       1 = 1
                              AND OH.HEADER_ID = OL.HEADER_ID
                              AND OH.ORG_ID = OL.ORG_ID
                              AND OH.sold_to_org_id = cust_acct.cust_account_id(+)
                              AND CUST_ACCT.PARTY_ID = PARTY.PARTY_ID(+)
                              AND OH.ship_to_org_id = ship_su.site_use_id(+)
                              AND OH.SOURCE_DOCUMENT_ID = REC_PO.requisition_header_id
                              AND OL.SOURCE_DOCUMENT_LINE_ID = REC_PO.requisition_line_id;
                  EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        P_ERR_STATUS_LINE := P_ERR_STATUS_LINE || 'No Data Found';
                  END;
               END IF;


              


               p_po_status :=
                  tac_po_status (NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL,
                                 NULL);
               p_po_status.Quote_Number := REC_PO.Quote_Number;
               p_po_status.Transaction_ID := REC_PO.Transaction_ID;
               p_po_status.PO_Number := V_PO_Number;
               p_po_status.PO_Release_No := V_PO_Release_No;
               p_po_status.PO_STATUS := V_PO_STATUS;
               p_po_status.PO_Price := V_PO_Price;
               p_po_status.Currency := V_Currency;
               p_po_status.Vendor_Num := V_Vendor_code;
               p_po_status.Vendor_Name := V_Vendor_Name;
               p_po_status.Vendor_Site_Code := V_Vendor_Site_Code;
               p_po_status.Error_Msg := P_ERR_STATUS_LINE;

               t_po_status.EXTEND;
               t_po_status (i) := p_po_status;
            END LOOP;
         END LOOP;


         IF I = 0
         THEN
            p_po_status :=
               tac_po_status (NULL,
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              NULL);
            p_po_status.Error_Msg := 'No Data Found';

            t_po_status.EXTEND;
            t_po_status (1) := p_po_status;
         END IF;
      END IF;


      RETURN (t_po_status);
   EXCEPTION
      WHEN OTHERS
      THEN
         p_po_status :=
            tac_po_status (NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL);
         p_po_status.Error_Msg := 'Others Error :' || SQLERRM;
         t_po_status.EXTEND;
         t_po_status (i) := p_po_status;

         RETURN (t_po_status);
   END;
END;
/
