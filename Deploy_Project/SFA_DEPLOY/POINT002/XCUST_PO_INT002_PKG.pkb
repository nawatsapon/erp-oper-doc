CREATE OR REPLACE package body APPS.XCUST_PO_INT002_PKG as


PROCEDURE PR_CREATE
(int_data   in xcust_po_int002_tab
,request   out varchar2
) is
v_request_id number;
l_request_id number;

vri          xcust_po_int002_tab  := int_data;
vro          xcust_po_int002_stg%rowtype;

v_msg        varchar2(2000);
v_msg_tmp    varchar2(2000);
v_status     varchar2(15);
v_user_id    number;
v_resp_id    number;
v_app_id     number;
v_user_name  varchar2(30);
v_err_out    varchar2(30);
v_resp_org_id number;
v_profile_org_id number;


cursor c2 (p_req_id number) is
select a.user_name,a.org_id
from   xcust_po_int002_stg a
where a.request_id = p_req_id
  and a.validate_flag = 'S'
;
--------------------------------------------------------------------------------
cursor c3 (p_user_name varchar2) is
select 
(case when a.responsibility_name like 'iPROC-USER%HO%' then 1
  else 99
  end ) order_by
,a.responsibility_name,a.responsibility_id,a.application_id,u.user_id
from   fnd_user_resp_groups_direct b
      ,fnd_user u
      ,fnd_responsibility_vl a
where b.user_id = u.user_id
  and u.user_name = p_user_name
  and a.responsibility_name like 'iPROC%USER%'
  and trunc(sysdate) between b.start_date and nvl(b.end_date,sysdate+1)
  and b.responsibility_id = a.responsibility_id
  and b.responsibility_application_id = a.application_id
order by order_by,responsibility_name
;


--------------------------------------------------------------------------------
-- function -- function -- function -- function -- function -- function -- function 
--------------------------------------------------------------------------------
function validate_des_org(p_inv_org_code varchar2) return varchar2 is
v_master_org_id_tmp number;
v_org_id_tmp number;
begin
    if p_inv_org_code is null then
        v_msg_tmp := 'DESTINATION_ORGANIZATION_CODE cannot be null.';
        v_status  := 'E';
        if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;

    else
        begin
            select p.organization_id,p.master_organization_id
            into   v_org_id_tmp , v_master_org_id_tmp
            from   mtl_parameters p
            where p.organization_code = p_inv_org_code
              ;
        exception when no_data_found then
            v_msg_tmp := 'DESTINATION_ORGANIZATION_CODE('||p_inv_org_code||') is not found in oracle.';
            v_status  := 'E';
            if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
        when others then
            null;
        end;
        
        if v_org_id_tmp = v_master_org_id_tmp then
            v_msg_tmp := 'DESTINATION_ORGANIZATION_CODE('||p_inv_org_code||') cannot be a master org.';
            v_status  := 'E';
            if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
        end if;
        
    end if;

return p_inv_org_code;
exception when others then
    return p_inv_org_code;
end validate_des_org;
--------------------------------------------------------------------------------
function validate_auth_status(p_text_in in varchar2,p_col_name in varchar2) return varchar2 is
v_ret varchar2(240);
begin
    if p_text_in is null then
        v_msg_tmp := p_col_name||' cannot be null.';
        v_status  := 'E';
        if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
    elsif p_text_in <> 'INCOMPLETE' then
        v_msg_tmp := p_col_name||' must be INCOMPLETE only.';
        v_status  := 'E';
        if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
    end if;
return substr(p_text_in,1,25);
exception when others then
    return substr(p_text_in,1,25);
end validate_auth_status;
--------------------------------------------------------------------------------
procedure validate_autosource_doc
(p_doc_head_id in number
,p_doc_line    in number
,p_head_out   out number
,p_line_out   out number
,p_doc_type   out varchar2
) is
begin
    
    if nvl(p_doc_head_id,0) = 0 and nvl(p_doc_line,0) = 0 then
        p_head_out := null;
        p_line_out := null;
        p_doc_type := null;
    
    elsif nvl(p_doc_head_id,0) > 0 and nvl(p_doc_line,0) > 0 then
        begin
            select a.type_lookup_code,a.po_header_id
            into   p_doc_type        ,p_head_out
            from   po_headers_all a
            where a.po_header_id = p_doc_head_id
            ;
        exception when no_data_found then 
            v_msg_tmp := 'AUTOSOURCE_DOC_HEADER_ID('||p_doc_head_id||') is not found in oracle.';
            v_status  := 'E';
            if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
        when others then
            null;
        end;
        
        if p_doc_type is not null then
            begin
                select a.line_num
                into   p_line_out
                from   po_lines_all a
                where a.po_header_id = p_doc_head_id
                  and a.line_num = p_doc_line
                  and rownum=1
                ;
            exception when no_data_found then 
                v_msg_tmp := 'AUTOSOURCE_DOC_LINE_NUM('||p_doc_line||') is not found in oracle.';
                v_status  := 'E';
                if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
            when others then
                null;
            end;
        end if;
        
    elsif nvl(p_doc_head_id,0) > 0 and nvl(p_doc_line,0) = 0 then
        v_msg_tmp := 'AUTOSOURCE_DOC_LINE_NUM cannot be null if AUTOSOURCE_DOC_HEADER_ID is not null.';
        v_status  := 'E';
        if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;

    elsif nvl(p_doc_head_id,0) = 0 and nvl(p_doc_line,0) > 0 then
        v_msg_tmp := 'AUTOSOURCE_DOC_HEADER_ID cannot be null if AUTOSOURCE_DOC_LINE_NUM is not null.';
        v_status  := 'E';
        if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
        
    end if;

exception when others then
    null;
end validate_autosource_doc;
--------------------------------------------------------------------------------
function validate_user_name(p_user in varchar2) return varchar2 is
v_emp_id number;
v_person_id number;
begin
    if p_user is null then
        v_msg_tmp := 'USER_NAME cannot be null.';
        v_status  := 'E';
        if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
    
    else
        begin
            select a.employee_id
            into   v_emp_id
            from   fnd_user a
            where a.user_name = p_user
            ;
        exception when no_data_found then
            v_msg_tmp := 'Invalid USER_NAME('||p_user||') on oracle.';
            v_status  := 'E';
            if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
        when others then
            null;
        end;
        
        if v_emp_id is not null then
            begin
                select a.person_id
                into   v_person_id
                from   per_people_f  a
                where a.person_id = v_emp_id
                  and a.current_employee_flag = 'Y'
                  and rownum=1
                  ;
            exception when no_data_found then
                v_msg_tmp := 'Invalid Employee USER_NAME('||p_user||') on oracle.';
                v_status  := 'E';
                if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
            when others then
                null;
            end;
        end if;
    end if;
    
return p_user;
exception when others then
    return p_user;
end validate_user_name;
--------------------------------------------------------------------------------
function validate_not_null(p_text_in in varchar2,p_col_name in varchar2,p_length in number) return varchar2 is
v_ret varchar2(240);
begin
    if p_text_in is null then
        v_msg_tmp := p_col_name||' cannot be null.';
        v_status  := 'E';
        if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
    elsif length(p_text_in) > p_length then
        v_msg_tmp := p_col_name||' cannot larger than '||p_length||' chars.';
        v_status  := 'E';
        if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
    end if;
return substr(p_text_in,1,p_length);
exception when others then
    return substr(p_text_in,1,p_length);
end validate_not_null;
--------------------------------------------------------------------------------
function validate_line_type(p_text_in in varchar2,p_col_name in varchar2) return varchar2 is
v_ret varchar2(240);
begin
    if p_text_in is null then
        v_msg_tmp := p_col_name||' cannot be null.';
        v_status  := 'E';
        if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
    elsif p_text_in not in ('Inventory','Expense') then
        v_msg_tmp := p_col_name||'('||p_text_in||') must be Inventory or Expense.';
        v_status  := 'E';
        if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
    end if;
return substr(p_text_in,1,25);
exception when others then
    return substr(p_text_in,1,25);
end validate_line_type;
--------------------------------------------------------------------------------
function validate_req_type(p_text_in in varchar2,p_col_name in varchar2) return varchar2 is
v_ret varchar2(240);
begin
    if p_text_in is null then
        v_msg_tmp := p_col_name||' cannot be null.';
        v_status  := 'E';
        if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
    elsif p_text_in not in ('PURCHASE','INTERNAL') then
        v_msg_tmp := p_col_name||'('||p_text_in||') must be PURCHASE or INTERNAL.';
        v_status  := 'E';
        if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
    end if;
return substr(p_text_in,1,25);
exception when others then
    return substr(p_text_in,1,25);
end validate_req_type;
--------------------------------------------------------------------------------
function validate_int_source_code(p_text_in in varchar2,p_col_name in varchar2) return varchar2 is
v_ret varchar2(240);
begin
    if p_text_in is null then
        v_msg_tmp := p_col_name||' cannot be null.';
        v_status  := 'E';
        if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
    elsif p_text_in <> 'SFDC' then
        v_msg_tmp := p_col_name||'('||p_text_in||') must be SFDC only.';
        v_status  := 'E';
        if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
    end if;
return substr(p_text_in,1,25);
exception when others then
    return substr(p_text_in,1,25);
end validate_int_source_code;
--------------------------------------------------------------------------------
function validate_transaction_id(p_text_in in varchar2,p_col_name in varchar2) return varchar2 is
v_ret varchar2(240);
begin
    if p_text_in is null then
        v_msg_tmp := p_col_name||' cannot be null.';
        v_status  := 'E';
        if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
    elsif length(p_text_in) > 25 then
        v_msg_tmp := p_col_name||' cannot larger than 25 chars.';
        v_status  := 'E';
        if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
    end if;
return substr(p_text_in,1,25);
exception when others then
    return substr(p_text_in,1,25);
end validate_transaction_id;
--------------------------------------------------------------------------------
function get_src_doc_type_code(p_h_id in number) return varchar2 is
v_ret varchar2(240);
begin
    if p_h_id is not null then
        select a.type_lookup_code
        into   v_ret
        from   po_headers_all a
        where a.po_header_id = p_h_id
          ;
    end if;
return v_ret;
exception when others then
    return null;
end get_src_doc_type_code;
--------------------------------------------------------------------------------
function get_valid_date(p_date_text in varchar2,p_col_name in varchar2) return date is
v_ret date;
begin
    if p_date_text is null then
            v_msg_tmp := p_col_name||' cannot be null.';
            v_status  := 'E';
            if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
    else
        begin
            select to_date(p_date_text,'DDMMYYYY')
            into   v_ret
            from   dual
            ;
        exception when others then
            v_msg_tmp := p_col_name||' is invalid (DDMMYYYY), value='||p_date_text;
            v_status  := 'E';
            if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
        end;
    end if;
return v_ret;
exception when others then
    return null;
end get_valid_date;
--------------------------------------------------------------------------------
function get_rate_date(p_date_text in varchar2) return date is
v_ret date;
begin
    if p_date_text is null then
            v_ret := null;
    else
        begin
            select to_date(p_date_text,'DDMMYYYY')
            into   v_ret
            from   dual
            ;
        exception when others then
            v_msg_tmp := 'RATE_DATE is invalid (DDMMYYYY), value='||p_date_text;
            v_status  := 'E';
            if v_msg is null then v_msg:=v_msg_tmp; else v_msg:=substr((v_msg||chr(10)||v_msg_tmp),1,2000); end if;
        end;
    end if;
return v_ret;
exception when others then
    return null;
end get_rate_date;
--------------------------------------------------------------------------------
function get_ou_id(p_inv_org_code varchar2) return number is
v_ret number;
begin
    if p_inv_org_code is not null then
        select i.org_information3
        into   v_ret
        from   mtl_parameters p
              ,hr_organization_information_v i
        where p.organization_code = p_inv_org_code
          and p.organization_id   = i.organization_id
          and i.org_information_context = 'Accounting Information'
          and rownum=1
          ;
    else
        v_ret := null;
    end if;
return v_ret;
exception when others then
    return null;
end get_ou_id;
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- main -- main -- main -- main -- main -- main -- main -- main -- main -- main 
--------------------------------------------------------------------------------
begin  --main

    for i in vri.FIRST..vri.LAST loop
        
        if v_request_id is null then
            begin
                select xcust_po_int002_request_s.nextval
                into   v_request_id
                from   dual
                ;
            exception when others then
                v_err_out := 'N01';
                raise ;
            end;
        end if;
        v_status       := 'S';
        v_msg          := '';
        vro.request_id                    := v_request_id;
        vro.line_no                       := i;
        vro.intf_date                     := sysdate;
        select xcust_po_int002_int_line_s.nextval into vro.interface_source_line_id from dual;
        vro.transaction_id                := validate_not_null       (vri(i).transactionid      ,'TRANSACTION_ID',25);
        vro.interface_source_code         := validate_int_source_code(vri(i).interfacesourcecode,'INTERFACE_SOURCE_CODE');
        vro.batch_name                    := validate_not_null       (vri(i).batchname          ,'BATCH_NAME',240);
        vro.last_update_date              := vri(i).lastupdatedate;
        vro.last_update_date_ora          := get_valid_date(vro.last_update_date,'LAST_UPDATE_DATE');
        vro.creation_date                 := vri(i).creationdate;
        vro.creation_date_ora             := get_valid_date(vro.creation_date,'CREATION_DATE');
        vro.source_type_code              := validate_not_null       (vri(i).sourcetypecode     ,'SOURCE_TYPE_CODE',25);
        vro.requisition_type              := validate_req_type       (vri(i).requisitiontype    ,'REQUISITION_TYPE');
        vro.header_description            := vri(i).headerdescription;          
        vro.destination_type_code         := validate_not_null       (vri(i).destinationtypecode,'DESTINATION_TYPE_CODE',25);
        vro.line_type                     := validate_line_type      (initcap(vri(i).linetype)  ,'LINE_TYPE');
        vro.item_segment1                 := vri(i).itemsegment1;               
        vro.quantity                      := vri(i).quantity;                   
        vro.unit_price                    := vri(i).unitprice;                  
        vro.currency_unit_price           := vri(i).currencyunitprice;          
        vro.unit_of_measure               := vri(i).unitofmeasure;              
        vro.authorization_status          := validate_auth_status    (vri(i).authorizationstatus,'AUTHORIZATION_STATUS');
        vro.group_code                    := vri(i).groupcode;                  
        vro.user_name                     := validate_user_name (vri(i).username);
        vro.justification                 := vri(i).justification;              
        vro.note_to_buyer                 := vri(i).notetobuyer;                
        vro.destination_organization_code := validate_des_org   (vri(i).destinationorganizationcode);
        vro.deliver_to_location_code      := vri(i).delivertolocationcode;      
        vro.source_organization_code      := vri(i).sourceorganizationcode;     
        vro.suggested_vendor_code         := vri(i).suggestedvendorcode;        
        vro.suggested_vendor_site         := vri(i).suggestedvendorsite;        
        vro.need_by_date                  := vri(i).needbydate;
        vro.need_by_date_ora              := get_valid_date(vro.need_by_date,'NEED_BY_DATE');
        vro.gl_date                       := vri(i).gldate;
        vro.gl_date_ora                   := get_valid_date(vro.gl_date,'GL_DATE');
        vro.amount                        := vri(i).amount;                     
        vro.currency_amount               := vri(i).currencyamount;             
        vro.currency_code                 := vri(i).currencycode;               
        vro.rate_date                     := vri(i).ratedate;
        vro.rate_date_ora                 := get_rate_date(vro.rate_date);
        vro.special_condition             := vri(i).specialcondition;           
        vro.special1                      := vri(i).special1;                   
        vro.special2                      := vri(i).special2;                   
        vro.special3                      := vri(i).special3;                   
        vro.special4                      := vri(i).special4;                   
        vro.special5                      := vri(i).special5;                   
        vro.special6                      := vri(i).special6;                   
        vro.special7                      := vri(i).special7;                   
        vro.special8                      := vri(i).special8;
        --vro.autosource_doc_header_id      := vri(i).autosourcedocheaderid;
        --vro.autosource_doc_line_num       := vri(i).autosourcedoclinenum;
        validate_autosource_doc(vri(i).autosourcedocheaderid   --in
                               ,vri(i).autosourcedoclinenum    --in
                               ,vro.autosource_doc_header_id   --out
                               ,vro.autosource_doc_line_num    --out
                               ,vro.document_type_code         --out
                               );
        vro.autosource_flag               := vri(i).autosourceflag;
        vro.document_type_code            := get_src_doc_type_code(vro.autosource_doc_header_id);
        vro.org_id                        := get_ou_id(vro.destination_organization_code);
        
        vro.validate_flag := v_status;
        vro.error_msg     := v_msg;
        if vro.validate_flag = 'E' then
            vro.process_flag := 'E';
        end if;
        
        begin
            insert into xcust_po_int002_stg values vro;
        exception when others then
            v_err_out := 'N02';
            raise;
        end;
    end loop; --for i

    -- Get user name from input data
    v_user_name := null;
    if v_request_id is not null then
        for r2 in c2(v_request_id) loop exit when c2%notfound;
            v_user_name   := r2.user_name;
            v_resp_org_id := r2.org_id;
            exit;
        end loop; --r2 c2
    end if;
    if v_user_name is not null then
        for r3 in c3(v_user_name) loop exit when c3%notfound;
            -- Get ORG_ID system from profile option
            v_profile_org_id := null;
            begin
                select b.profile_option_value  -- ORG_ID
                into   v_profile_org_id
                from   fnd_profile_option_values b
                where b.profile_option_id = 1991 -- 'ORG_ID' -- MO: Operating Unit
                  and b.application_id = 0
                  and b.level_id = 10003
                  and b.level_value = r3.responsibility_id --in (50407,50723) resp_id
                  and rownum=1
                ;
            exception when others then
                null;
            end;
            
            if nvl(v_profile_org_id,0) = v_resp_org_id then
                v_user_id := r3.user_id;
                v_resp_id := r3.responsibility_id;
                v_app_id  := r3.application_id;
                exit;
            end if;
        end loop;
    end if;
    
    if v_user_id is not null and v_resp_id is not null and v_request_id is not null then
        fnd_global.apps_initialize(v_user_id,v_resp_id,v_app_id,0,-1);
        begin
            l_request_id := fnd_request.submit_request(application  => 'PO',
                                        program          => 'XCUSTPOINT002',
                                        description      => null,
                                        start_time       => null,
                                        sub_request      => FALSE,
                                        argument1        => v_request_id, --p_saleforce_request_id
                                        argument2        => 'Y'           --p_submit_wf to initial approval process
                                        );
            commit;
        exception when others then
            null;
        end;
    end if;
    
request := 'Y';

exception when others then
    if v_err_out is not null then
        request := v_err_out;
    else
        request := 'N';
    end if;
end PR_CREATE;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
PROCEDURE PR_IMPORT
(p_request_id in number
,p_submit_pr_import in varchar2
,p_submit_init_wf   in varchar2
,p_status    out varchar2
,p_msg       out varchar2
) is
v_saleforce_request_id number := p_request_id;
v_rate_date date   := trunc(sysdate);
v_user_id   number := fnd_profile.value('USER_ID');
v_login_id  number := fnd_profile.value('LOGIN_ID');

stop_run exception;
vr1 po_requisitions_interface_all%rowtype;
v_batch_id number;

v_msg varchar2(2000);
l_request_id  number;
l_return      boolean;
l_phase       varchar2(30);
l_status      varchar2(30);
l_dev_phase   varchar2(30);
l_dev_status  varchar2(30);
l_message     varchar2(1000);

v_import_status varchar2(30);
v_msg_tmp varchar2(2000);
v_tamplate_code_spectext varchar2(30);
v_tamplate_code_speccon  varchar2(30);
v_att_value    varchar2(240);
v_seq          number;
v_special_text number;
v_special_con  number;
v_source_organization_id NUMBER;
V_ITEM_ID NUMBER;
V_ITEM_COST NUMBER;
V_LIST_PRICE NUMBER;

--------------------------------------------------------------------------------
cursor c1 is
select a.*
from   xcust_po_int002_stg a
where a.request_id = v_saleforce_request_id
  and a.validate_flag = 'S'
  and (a.process_flag is null or a.process_flag='E')
;
cursor c11 is
select a.rowid,a.*
from   xcust_po_int002_stg a
where a.request_id = v_saleforce_request_id
  and a.validate_flag = 'S'
  and (a.process_flag is null or a.process_flag='E')
;
cursor c2(p_int_trx_id number) is
select e.column_name||' : '||e.error_message msg
from   po_interface_errors e
where e.interface_type = 'REQIMPORT'
  and e.interface_transaction_id = p_int_trx_id
;

cursor c3 is -- for insert Special Condition
select a.template_code
,a.template_name
,b.display_sequence
,b.attribute_code
,b.attribute_name
,b.description
,b.required_flag
from   por_templates_v a
      ,por_template_attributes_v b
where a.template_name ='Special Condition'  --'Special text for Device' -- 'Special Condition'
  and a.template_code = b.template_code
order by b.display_sequence
;


cursor c33 is -- for insert Special Text for Blanket
select a.template_code
,a.template_name
,b.display_sequence
,b.attribute_code
,b.attribute_name
,b.description
,b.required_flag
from   por_templates_v a
      ,por_template_attributes_v b
where a.template_name = 'Special text Blanket' --'Special Text for Blanket'
  and a.template_code = b.template_code
order by b.display_sequence
;

--------------------------------------------------------------------------------
-- function -- function -- function -- function -- function -- function -- function 
--------------------------------------------------------------------------------
function get_vendor_name(p_vendor_code in varchar2,p_ven_id out number) return varchar2 is
v_ret varchar2(240);
begin
    select a.vendor_name,a.vendor_id
    into   v_ret,p_ven_id
    from  po_vendors a
    where a.segment1 = p_vendor_code
      ;
return v_ret;
exception when others then
    return null;
end get_vendor_name;
--------------------------------------------------------------------------------
function get_persion_id(p_user varchar2) return number is
v_ret number;
begin
    select a.person_id
    into  v_ret
    from  per_people_f  a
         ,fnd_user      b
    where a.person_id = b.employee_id
      and a.current_employee_flag = 'Y'
      and b.user_name = p_user
      and rownum=1
      ;
return v_ret;
exception when others then
    return -1;
end get_persion_id;
--------------------------------------------------------------------------------
function get_ou_id(p_inv_org_code varchar2) return number is
v_ret number;
begin
    select i.org_information3
    into   v_ret
    from   mtl_parameters p
          ,hr_organization_information_v i
    where p.organization_code = p_inv_org_code
      and p.organization_id   = i.organization_id
      and i.org_information_context = 'Accounting Information'
      and rownum=1
      ;
return v_ret;
exception when others then
    return -1;
end get_ou_id;
--------------------------------------------------------------------------------
function get_charge_ccid(p_inv_org_code varchar2) return number is
v_ret number;
begin
    select a.material_account
    into   v_ret
    from   mtl_parameters a
    where a.organization_code = p_inv_org_code
      ;
return v_ret;
exception when others then
    return -1;
end get_charge_ccid;
--------------------------------------------------------------------------------
function get_src_doc_type_code(p_h_id in number) return varchar2 is
v_ret varchar2(240);
begin
    if nvl(p_h_id,0) > 0 then
        begin
            select a.type_lookup_code
            into   v_ret
            from   po_headers_all a
            where a.po_header_id = p_h_id
              ;
        exception when others then
            null;
        end;
    end if;
return v_ret;
exception when others then
    return null;
end get_src_doc_type_code;
--------------------------------------------------------------------------------
function get_thb_rate(p_rate_type in varchar2,p_rate_date in date,p_currency_code in varchar2) return number is
v_ret number;
begin
    select a.conversion_rate
    into   v_ret
    from   gl_daily_rates a
    where a.conversion_type = p_rate_type
      and a.from_currency = p_currency_code
      and a.to_currency = 'THB' 
      and a.conversion_date = p_rate_date
      ;
return v_ret;
exception when others then
    return 1;
end get_thb_rate;
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- main -- main -- main -- main -- main -- main -- main -- main -- main -- main 
--------------------------------------------------------------------------------

begin
    -- get template code
    begin
        select a.template_code
        into   v_tamplate_code_spectext
        from   por_templates_v a
        where a.template_name = 'Special text Blanket' --'Special Text for Blanket'
        ;
    exception when others then
        null;
    end;
    begin
        select a.template_code
        into   v_tamplate_code_speccon
        from   por_templates_v a
        where a.template_name = 'Special Condition'
        ;
    exception when others then
        null;
    end;
    
    
    for r1 in c1 loop exit when c1%notfound;
        if v_batch_id is null then
            begin
                select xcust_po_int002_batch_s.nextval
                into   v_batch_id
                from   dual
                ;
            exception when others then
                v_msg := 'Error during get batch_id from sequence xcust_po_int002_batch_s '||sqlerrm;
                raise stop_run;
            end;
        end if;
vr1.interface_source_code         := r1.interface_source_code;
vr1.interface_source_line_id      := r1.interface_source_line_id;
--select xcust_po_int002_int_line_s.nextval into vr1.interface_source_line_id from dual;
--vr1.batch_name                    := r1.batch_name;
vr1.batch_id                      := v_batch_id;
vr1.last_update_date              := r1.last_update_date_ora;
vr1.creation_date                 := r1.creation_date_ora;                
vr1.source_type_code              := r1.source_type_code;             
vr1.requisition_type              := r1.requisition_type;             
vr1.header_description            := substr(r1.header_description,1,240);
vr1.destination_type_code         := r1.destination_type_code;        
vr1.line_type                     := r1.line_type;                    
vr1.item_segment1                 := r1.item_segment1;                
vr1.quantity                      := r1.quantity;  


--KTP 20150420

begin
  SELECT organization_id 
  into v_source_organization_id
  FROM mtl_parameters
  WHERE organization_code = r1.source_organization_code
  and rownum=1;
exception when no_data_found then
 null;
end;

BEGIN
SELECT  INVENTORY_ITEM_ID 
INTO V_ITEM_ID
FROM MTL_SYSTEM_ITEMS_B
WHERE SEGMENT1 = r1.item_segment1
and rownum=1;
EXCEPTION WHEN NO_DATA_FOUND THEN
NULL;
END;

  IF r1.requisition_type = 'INTERNAL' THEN    

    BEGIN
      SELECT CIC.ITEM_COST
      INTO V_ITEM_COST
      FROM  CST_ITEM_COSTS_FOR_GL_VIEW CIC 
      WHERE CIC.INVENTORY_ITEM_ID = V_ITEM_ID
      AND  CIC.ORGANIZATION_ID = v_source_organization_id
      and rownum=1;
    EXCEPTION WHEN NO_DATA_FOUND THEN 
      V_ITEM_COST                    := null;
    END;
    
    
    BEGIN
       SELECT LIST_PRICE_PER_UNIT
       INTO V_LIST_PRICE
       FROM MTL_SYSTEM_ITEMS_B
       WHERE INVENTORY_ITEM_ID = V_ITEM_ID
       AND ORGANIZATION_ID = v_source_organization_id
       and rownum=1;
    EXCEPTION WHEN NO_DATA_FOUND THEN 
      V_LIST_PRICE                    := null;
    END;
    
    IF V_ITEM_COST IS NULL AND V_LIST_PRICE IS NULL THEN
       vr1.unit_price  := 0;
    ELSE
       vr1.unit_price  := NULL;
    END IF;
  
  ELSE               
    vr1.unit_price                    := r1.unit_price;
  END IF;   
                
vr1.currency_unit_price           := r1.currency_unit_price;          
vr1.unit_of_measure               := r1.unit_of_measure;              
vr1.authorization_status          := r1.authorization_status;         
vr1.group_code                    := r1.group_code;                   
--vr1.user_name                     := r1.user_name;
vr1.preparer_id                   := get_persion_id(r1.user_name);
vr1.deliver_to_requestor_id       := vr1.preparer_id;
vr1.justification                 := substr(r1.justification,1,480);                
vr1.note_to_buyer                 := substr(r1.note_to_buyer,1,480);
vr1.destination_organization_code := r1.destination_organization_code;

vr1.deliver_to_location_code      := r1.deliver_to_location_code;     
vr1.source_organization_code      := r1.source_organization_code;     
--vr1.suggested_vendor_code         := r1.suggested_vendor_code;
vr1.suggested_vendor_name         := get_vendor_name(r1.suggested_vendor_code,vr1.suggested_vendor_id);
vr1.suggested_vendor_site         := r1.suggested_vendor_site;        
vr1.need_by_date                  := r1.need_by_date_ora;
vr1.gl_date                       := r1.gl_date_ora;


if vr1.quantity is not null and vr1.unit_price is not null then
    vr1.amount                    := null;
else
   IF r1.requisition_type = 'INTERNAL' THEN  
       vr1.amount                    := null;
   ELSE
       vr1.amount                    := r1.amount;
   END IF;
end if;


if vr1.quantity is not null and vr1.currency_unit_price is not null then
    vr1.currency_amount                    := null;
else
   IF r1.requisition_type = 'INTERNAL' THEN  
    vr1.currency_amount                    :=  NULL;
   ELSE
    vr1.currency_amount                    := r1.currency_amount;
   END IF;
end if;
              
vr1.currency_code                 := r1.currency_code;

if nvl(r1.currency_code,'THB') <> 'THB' then
    vr1.rate_date                 := v_rate_date;
    vr1.rate_type                 := '1001'; --'SELLING';
    vr1.unit_price                := round((vr1.currency_unit_price * get_thb_rate(vr1.rate_type,vr1.rate_date,r1.currency_code)),2);
else
    vr1.rate_date                 := null;
    vr1.rate_type                 := null;
end if;

vr1.autosource_doc_header_id      := r1.autosource_doc_header_id;     
vr1.autosource_doc_line_num       := r1.autosource_doc_line_num;
if    vr1.autosource_doc_header_id is not null and vr1.autosource_doc_line_num is not null and r1.document_type_code is null then
    vr1.document_type_code        := nvl(r1.document_type_code,get_src_doc_type_code(r1.autosource_doc_header_id));
elsif vr1.autosource_doc_header_id is not null and vr1.autosource_doc_line_num is not null and r1.document_type_code is not null then
    vr1.document_type_code        := r1.document_type_code;
end if;
vr1.autosource_flag               := r1.autosource_flag;
if r1.org_id is null then
    vr1.org_id                    := get_ou_id(r1.destination_organization_code);
else
    vr1.org_id                    := r1.org_id;
end if;
vr1.charge_account_id             := get_charge_ccid(r1.destination_organization_code);
vr1.line_attribute1               := r1.batch_name;
vr1.line_attribute2               := r1.transaction_id;

--vr1.DESTINATION_SUBINVENTORY      := r1.deliver_to_location_code;  --KTP



        begin
            insert into po_requisitions_interface_all values vr1; 
        exception when others then
            raise;
        end;
    end loop;

    -- submit REQIMPORT
--if001
    if p_submit_pr_import = 'Y' and v_batch_id is not null then
       l_request_id := fnd_request.submit_request(application      => 'PO',
                                    program          => 'REQIMPORT',
                                    description      => null,
                                    start_time       => null,
                                    sub_request      => FALSE,
                                    argument1        => 'SFDC', --Interface Source code,
                                    argument2        => v_batch_id,  --Batch ID,
                                    argument3        => 'VENDOR',    --Group By,  PO_LOOKUP_CODES LOOKUP_TYPE='REQIMPORT GROUP BY'
                                    argument4        => null,        --Last Req Number,
                                    argument5        => 'N',         --MULTI_DISTRIBUTIONS Multi Distributions,
                                    argument6        => nvl(p_submit_init_wf,'Y')          --INITIATE_REQAPPR_AFTER_REQIMP
                                    );
        commit;
        
        if nvl(l_request_id,0) > 0 then
            -- wait for REQIMPORT to complete
            LOOP
                l_return := FND_CONCURRENT.wait_for_request(
                                            l_request_id,
                                            5,              -- time b/w checks. Number of seconds to sleep
                                            10800,          -- Max amount of time to wait (in seconds)
                                            l_phase,l_status,l_dev_phase,l_dev_status,l_message);

                l_return := FND_CONCURRENT.get_request_status(l_request_id,NULL,NULL,
                                            l_phase,l_status,l_dev_phase,l_dev_status,l_message);

                IF (NVL(l_dev_phase,'ERROR') = 'COMPLETE') THEN
                    EXIT;
                END IF;
            END LOOP;
            
            IF (NVL(l_dev_status, 'ERROR') NOT IN ('NORMAL', 'WARNING')) THEN
                -- completed with error
                v_msg := 'Error!!! "Requisition Import" completed with error request_id='||l_request_id;
                raise stop_run;
                
            ELSE
                -- completed with normal or warning
                v_import_status := 'DONE';
                
            END IF;
            
        else
            v_msg := 'Error!!! submit "Requisition Import" REQIMPORT.';
            raise stop_run;
        end if;
--if001
    end if;
    
    ----------------------------------------------------------------------------
    -- update import result to custom table
    ----------------------------------------------------------------------------
    if nvl(v_import_status,'X') = 'DONE' then
        for r1 in c11 loop exit when c11%notfound;

            if r1.requisition_header_id is not null and r1.requisition_line_id is not null then
                --------------------------------------------------------------------
                -- success record ,update AUTOSOURCE_DOC_HEADER_ID info
                --------------------------------------------------------------------
                begin
                    update po_requisition_lines_all b
                    set b.blanket_po_header_id = r1.autosource_doc_header_id
                       ,b.blanket_po_line_num  = r1.autosource_doc_line_num
                       ,b.document_type_code   = r1.document_type_code
                    where b.requisition_line_id = r1.requisition_line_id
                    ;
                exception when others then
                    null;
                end;
                
                  --------------------------------------------------------------------
                -- success record ,update APPS_SOURCE_CODE info for special field in PO FORM by KTP 20150410
                --------------------------------------------------------------------
                begin
                    update po_requisition_headers_all h
                    set h.apps_source_code = 'POR'
                    ,h.note_to_authorizer = substr(r1.justification,1,480)  --update justification
                    where h.requisition_header_id = r1.requisition_header_id
                    ;
                exception when others then
                    null;
                end;
                
                -- generate special information
                -- Special Condition
                begin
                    select count(*)
                    into   v_special_con
                    from   por_template_assoc_v a
                    where  a.region_code           = v_tamplate_code_speccon -- 'IFT_86' --'IFT_208'
                      and  a.item_or_category_flag = 'C'
                      and  a.item_or_category_id   = r1.category_id
                      ;
                exception when others then
                    null;
                end;
                
                if nvl(v_special_con,0) > 0 and r1.requisition_line_id is not null then
                    v_seq := 0;
                    -- insert data
                    for r3 in c3 loop exit when c3%notfound;
                        v_seq := v_seq+1;
                        if    v_seq = 1 then v_att_value := nvl(r1.Special_Condition,'-');
                        else  v_att_value := '-';
                        end if;
                        
                        begin
                            insert into por_template_info a (a.requisition_line_id, a.attribute_code, a.attribute_value,
                            a.created_by, a.creation_date, a.last_updated_by, a.last_update_date, a.last_update_login)
                            values (r1.requisition_line_id,r3.attribute_code,v_att_value,
                            v_user_id,sysdate,v_user_id,sysdate,v_login_id
                            );
                        exception when others then
                            raise;
                        end;
                    end loop;
                end if;
                
                -- generate special information
                -- Special Text for Blanket
                begin
                    select count(*)
                    into   v_special_text
                    from   por_template_assoc_v a
                    where  a.region_code           = v_tamplate_code_spectext -- 'IFT_86' --'IFT_208'
                      and  a.item_or_category_flag = 'C'
                      and  a.item_or_category_id   = r1.category_id
                      ;
                exception when others then
                    null;
                end;
                
                if nvl(v_special_text,0) > 0 and r1.requisition_line_id is not null then
                    v_seq := 0;
                    -- insert data
                    for r3 in c33 loop exit when c33%notfound;
                        v_seq := v_seq+1;
                        if    v_seq = 1 then v_att_value := nvl(r1.special1,'-');
                        elsif v_seq = 2 then v_att_value := nvl(r1.special2,'-');
                        elsif v_seq = 3 then v_att_value := nvl(r1.special3,'-');
                        elsif v_seq = 4 then v_att_value := nvl(r1.special4,'-');
                        elsif v_seq = 5 then v_att_value := nvl(r1.special5,'-');
                        elsif v_seq = 6 then v_att_value := nvl(r1.special6,'-');
                        elsif v_seq = 7 then v_att_value := nvl(r1.special7,'-');
                        elsif v_seq = 8 then v_att_value := nvl(r1.special8,'-');
                        else  v_att_value := '-';
                        end if;
                        begin
                            insert into por_template_info a (a.requisition_line_id, a.attribute_code, a.attribute_value,
                            a.created_by, a.creation_date, a.last_updated_by, a.last_update_date, a.last_update_login)
                            values (r1.requisition_line_id,r3.attribute_code,v_att_value,
                            v_user_id,sysdate,v_user_id,sysdate,v_login_id
                            );
                        exception when others then
                            raise;
                        end;
                    end loop;
                end if;
                
                
                -- update status COMPLETE to xcust_po_int002_stg
                begin
                    update xcust_po_int002_stg a
                    set a.process_flag = 'C'
                       ,a.error_msg    = ''
                     where a.rowid = r1.rowid
                    ;
                exception when others then
                    null;
                end;
                
            else
                --------------------------------------------------------------------
                -- Error record ,get error info
                --------------------------------------------------------------------
                v_msg_tmp := '';
                for r2 in c2(r1.interface_transaction_id) loop exit when c2%notfound;
                    if v_msg_tmp is null then
                        v_msg_tmp := substr(r2.msg,1,2000);
                    else
                        v_msg_tmp := substr((v_msg_tmp||chr(10)||substr(r2.msg,1,2000)),1,2000);
                    end if;
                end loop;
                
                if v_msg_tmp is null and r1.imp_request_id is not null then
                    v_msg_tmp := 'Plase see information and/or error in "Requisition Import" REQUEST_ID='||r1.imp_request_id;
                end if;
                
                -- update status ERROR to xcust_po_int002_stg
                begin
                    update xcust_po_int002_stg a
                    set a.process_flag = 'E'
                       ,a.error_msg    = v_msg_tmp
                     where a.rowid = r1.rowid
                    ;
                exception when others then
                    null;
                end;
                
            end if;
        end loop;
    end if;
    
    
p_status := 'S';

exception when stop_run then
    p_status := 'E';
    p_msg    := v_msg;
when others then
    p_status := 'E';
    p_msg    := 'Others error!!! '||sqlerrm;
end PR_IMPORT;
--------------------------------------------------------------------------------

end XCUST_PO_INT002_PKG;
/
