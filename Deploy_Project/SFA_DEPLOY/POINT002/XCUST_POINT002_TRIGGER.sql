CREATE OR REPLACE TRIGGER APPS.XCUST_PO_REQ_INTERFACE_ARU
 AFTER UPDATE ON PO.PO_REQUISITIONS_INTERFACE_ALL
REFERENCING NEW AS NEW OLD AS OLD FOR EACH ROW
WHEN (NEW.REQUISITION_HEADER_ID IS NOT NULL
   OR NEW.REQUISITION_LINE_ID   IS NOT NULL
   OR NEW.TRANSACTION_ID IS NOT NULL)
BEGIN
    if :new.transaction_id is not null then
        begin
            update xcust_po_int002_stg a
            set a.interface_transaction_id   = :new.transaction_id
               ,a.imp_request_id             = fnd_global.conc_request_id
            where a.interface_source_line_id = :old.interface_source_line_id
              and a.interface_source_code    = 'SFDC'
            ;
        exception when others then
            null;
        end;
    end if;
    
    if :new.requisition_header_id is not null or :new.requisition_line_id is not null then
        begin
            update xcust_po_int002_stg a
            set a.requisition_header_id = :new.requisition_header_id
               ,a.requisition_line_id   = :new.requisition_line_id
               ,a.pr_number             = (select x.segment1 
                                           from   po_requisition_headers_all x 
                                           where  x.requisition_header_id = :new.requisition_header_id)
               ,a.category_id           = (select x.category_id 
                                           from   po_requisition_lines_all x 
                                           where  x.requisition_line_id = :new.requisition_line_id)
            where a.interface_source_line_id = :old.interface_source_line_id
              and a.interface_source_code    = 'SFDC'
            ;
        exception when others then
            null;
        end;
    end if;
EXCEPTION WHEN OTHERS THEN
    NULL;
END;
/

