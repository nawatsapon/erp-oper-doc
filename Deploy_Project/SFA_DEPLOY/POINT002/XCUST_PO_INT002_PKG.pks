CREATE OR REPLACE package APPS.XCUST_PO_INT002_PKG as

PROCEDURE PR_CREATE
(int_data   in xcust_po_int002_tab
,request   out varchar2
);

PROCEDURE PR_IMPORT
(p_request_id in number
,p_submit_pr_import in varchar2
,p_submit_init_wf   in varchar2
,p_status    out varchar2
,p_msg       out varchar2
);

end XCUST_PO_INT002_PKG;
/
