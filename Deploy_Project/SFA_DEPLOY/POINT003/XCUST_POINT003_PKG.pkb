CREATE OR REPLACE PACKAGE BODY APPS.XCUST_POINT003_PKG
IS
   FUNCTION get_pr_status (
      p_org_id                      IN NUMBER,
      p_Quote_Number                IN VARCHAR2,
      p_tac_pr_status_input_table   IN tac_pr_status_input_table := tac_pr_status_input_table()
   )
      --(p_org_id IN NUMBER, p_Quote_Number IN VARCHAR2,p_Transaction_ID IN NUMBER)
      RETURN TAC_pr_status_table
   IS
      CURSOR PR (
         p_org_id           IN            NUMBER,
         p_Quote_Number     IN            VARCHAR2,
         p_Transaction_ID   IN            VARCHAR2
      )
      IS
 SELECT   DISTINCT STG.BATCH_NAME Quote_Number,
                  STG.TRANSACTION_ID Transaction_ID,
                  STG.PR_NUMBER PR_IR_Number,
                  DECODE(l.CURRENCY_CODE,'THB',l.UNIT_PRICE,l.CURRENCY_UNIT_PRICE) PRICE,
                  STG.Process_Flag,
                  substr(STG.Error_Msg,1,240) Error_Msg
  FROM  XCUST_PO_INT002_STG STG,PO_REQUISITION_LINES_ALL l
 WHERE      STG.REQUISITION_LINE_ID = L.REQUISITION_LINE_ID(+)
         AND STG.REQUISITION_HEADER_ID = L.REQUISITION_HEADER_ID(+)
         and STG.ORG_ID = p_org_id   --102
         AND STG.BATCH_NAME = p_Quote_Number     --SFAQUOTE1
         AND STG.TRANSACTION_ID = p_Transaction_ID    --SFA1001
         and stg.intf_date = (select max(intf_date) from XCUST_PO_INT002_STG where ORG_ID = p_org_id
         AND BATCH_NAME = p_Quote_Number 
         and TRANSACTION_ID = p_Transaction_ID)
UNION ALL
SELECT   DISTINCT p_Quote_Number Quote_Number,
                  p_Transaction_ID Transaction_ID,
                  '' PR_IR_Number,
                  NULL PRICE,
                  '' Process_Flag,
                  '' Error_Msg
  FROM   DUAL
 WHERE   (p_Transaction_ID) NOT IN
               (SELECT DISTINCT STG.TRANSACTION_ID 
  FROM  XCUST_PO_INT002_STG STG,PO_REQUISITION_LINES_ALL l
 WHERE   STG.REQUISITION_LINE_ID = L.REQUISITION_LINE_ID(+)
         AND STG.REQUISITION_HEADER_ID = L.REQUISITION_HEADER_ID(+)
         and STG.ORG_ID = p_org_id                                --102
         AND STG.BATCH_NAME = p_Quote_Number                --SFAQUOTE1
         AND STG.TRANSACTION_ID = p_Transaction_ID                --SFA1001
          and stg.intf_date = (select max(intf_date) from XCUST_PO_INT002_STG where ORG_ID = p_org_id
         AND BATCH_NAME = p_Quote_Number 
         and TRANSACTION_ID = p_Transaction_ID)
         ) ;
      
      /*   SELECT   DISTINCT l.attribute1 Quote_Number,
                  l.attribute2 Transaction_ID,
                  h.segment1 PR_IR_Number,
                  l.UNIT_PRICE PRICE,
                  'C' Process_Flag,
                  '' Error_Msg
  FROM   po_requisition_headers_all h, PO_REQUISITION_LINES_ALL l
 WHERE       h.requisition_header_id = l.requisition_header_id
         AND H.ORG_ID = L.ORG_ID
         AND H.ORG_ID = p_org_id                                --102
         AND l.attribute1 = p_Quote_Number                --SFAQUOTE1
         AND l.attribute2 = p_Transaction_ID                --SFA1001
UNION ALL
SELECT   DISTINCT p_Quote_Number Quote_Number,
                  p_Transaction_ID Transaction_ID,
                  '' PR_IR_Number,
                  NULL PRICE,
                  '' Process_Flag,
                  '' Error_Msg
  FROM   DUAL
 WHERE   (p_Transaction_ID) NOT IN
               (SELECT   DISTINCT l.attribute2
                  FROM   po_requisition_headers_all h,
                         PO_REQUISITION_LINES_ALL l
                 WHERE   h.requisition_header_id = l.requisition_header_id
                         AND H.ORG_ID = L.ORG_ID
                         AND H.ORG_ID = p_org_id                                --102
                         AND l.attribute1 = p_Quote_Number                --SFAQUOTE1
                         AND l.attribute2 = p_Transaction_ID             --'SFA1002'
                         )
                                                     ;
                                                     */


      p_Transaction_ID   VARCHAR2 (50);

      p_pr_status        tac_pr_status;
      t_pr_status        tac_pr_status_table := tac_pr_status_table ();
      I                  NUMBER := 0;
      P_ERR_STATUS       VARCHAR2 (240);
      P_ERR_STATUS_LINE  VARCHAR2 (240);

   BEGIN
      IF    p_org_id IS NULL OR p_Quote_Number IS NULL 
      THEN
         p_pr_status :=tac_pr_status (NULL,NULL,NULL,NULL,NULL,NULL);
         
         IF p_org_id IS NULL THEN
         P_ERR_STATUS  := 'No ORG_ID found,';
         END IF;

         IF p_Quote_Number IS NULL THEN
         P_ERR_STATUS := P_ERR_STATUS ||'No Quote Number found';
         END IF;

      END IF;
      
         IF p_tac_pr_status_input_table.COUNT > 50
         THEN
            p_pr_status :=tac_pr_status (NULL,NULL,NULL,NULL,NULL,NULL);
            p_pr_status.Error_Msg := 'Transactions more than 50 rows';

            t_pr_status.EXTEND;
            t_pr_status (1) := p_pr_status;
         ELSE
            FOR j IN 1 .. p_tac_pr_status_input_table.COUNT
            LOOP
               p_Transaction_ID :=
                  p_tac_pr_status_input_table (j).Transaction_ID;


               FOR REC_PR IN PR (p_org_id, p_Quote_Number, p_Transaction_ID)
               LOOP
                  i := i + 1;
                  
                  IF p_Transaction_ID IS NULL AND P_ERR_STATUS IS NOT NULL THEN
                  P_ERR_STATUS_LINE := P_ERR_STATUS || ',';
                  else 
                   P_ERR_STATUS_LINE := P_ERR_STATUS;
                  END IF;
                  
                  IF p_Transaction_ID IS NULL THEN
                  P_ERR_STATUS_LINE :=  P_ERR_STATUS_LINE||'No Transaction ID found';
                  elsif p_Transaction_ID is not null and p_Quote_Number is not null and REC_PR.Price is null then
                  P_ERR_STATUS_LINE :=  P_ERR_STATUS_LINE|| 'No Data Found';
                  END IF;
                 

                  p_pr_status :=tac_pr_status (NULL,NULL,NULL,NULL,NULL,NULL);
                  p_pr_status.Quote_Number := REC_PR.Quote_Number;
                  p_pr_status.Transaction_ID := REC_PR.Transaction_ID;
                  p_pr_status.PR_IR_Number := REC_PR.PR_IR_Number;
                  p_pr_status.Price := REC_PR.Price;
                  p_pr_status.Process_Flag := REC_PR.Process_Flag;
                  p_pr_status.Error_Msg := REC_PR.Error_Msg||P_ERR_STATUS_LINE;

                  t_pr_status.EXTEND;
                  t_pr_status (i) := p_pr_status;
               END LOOP;
            END LOOP;



            IF I = 0
            THEN
               p_pr_status :=tac_pr_status (NULL,NULL,NULL,NULL,NULL,NULL);
               p_pr_status.Error_Msg := 'No Data Found';

               t_pr_status.EXTEND;
               t_pr_status (1) := p_pr_status;
            END IF;
         
           
         END IF;
      

      RETURN (t_pr_status);
   EXCEPTION
      WHEN OTHERS
      THEN
         p_pr_status :=tac_pr_status (NULL,NULL,NULL,NULL,NULL,NULL);
         p_pr_status.Error_Msg := 'Others Error :' || SQLERRM;
         t_pr_status.EXTEND;
         t_pr_status (i) := p_pr_status;
         
         RETURN (t_pr_status);
   END;
END;
/
