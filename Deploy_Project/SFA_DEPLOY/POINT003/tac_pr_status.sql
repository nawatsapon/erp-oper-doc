DROP TYPE APPS.TAC_PR_STATUS;

CREATE OR REPLACE TYPE APPS.tac_pr_status AS OBJECT
(
     Quote_Number VARCHAR2(40)
    ,Transaction_ID varchar2(50)
    ,PR_IR_Number VARCHAR2(40)
    ,Price NUMBER
    ,Process_Flag VARCHAR2(10)
    ,Error_Msg VARCHAR2(240)
);
/

