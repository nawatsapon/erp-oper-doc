declare

CURSOR SUB_PRI IS
  SELECT 
         DH.PRICELIST_DESC PRICELIST_CODE,
         DH.START_DATE     START_DATE,
         DH.END_DATE       END_DATE
  FROM   DTAC_SA_PRICELIST_V  DH
  WHERE 1 = 1
  AND DH.END_DATE IS NOT NULL
  AND EXISTS (SELECT distinct H.pricelist_code,
         H.PRICELIST_DESC,
         H.START_DATE     START_DATE,
         H.END_DATE       END_DATE
  FROM   SA_PRICELIST  H
  WHERE 1 = 1 
    AND DH.PRICELIST_DESC = H.PRICELIST_CODE
    AND DH.START_DATE = H.START_DATE
    AND H.END_DATE IS NULL
    );
    
begin

FOR S IN SUB_PRI
    LOOP

dbms_output.put_line('S.pricelist_code: '||S.pricelist_code||' / ' ||'start_date: '||S.start_date||' / '||'end_date: '||S.end_date);

UPDATE SA_PRICELIST
set end_date = S.end_date
where 1 = 1
and pricelist_code = S.pricelist_code
AND start_date = S.start_date
AND END_DATE IS NULL;

END LOOP;

end;
