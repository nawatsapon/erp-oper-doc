  SELECT 
         DH.PRICELIST_DESC PRICELIST_CODE,
         DD.ITEM_CODE      ITEM_CODE,
         DD.STD_PRICE      STD_PRICE,
         DD.START_DATE     START_DATE,
         DD.END_DATE       END_DATE,
         DD.PRECEDENCE     PRECEDENCE
         ,DD.org_id
  FROM   DTAC_SA_PRICELIST_V  DH,
         DTAC_SA_PRICE_ITEM_V DD
  WHERE DD.PRICELIST_CODE = DH.PRICELIST_CODE
  AND DD.END_DATE IS NOT NULL
  AND EXISTS (SELECT distinct H.pricelist_code,
         H.PRICELIST_DESC,
         D.ITEM_CODE      ITEM_CODE,
         D.STD_PRICE      STD_PRICE,
         D.START_DATE     START_DATE,
         D.END_DATE       END_DATE
  FROM   SA_PRICELIST  H,
         SA_PRICE_ITEM D,
         PB_COMPANY PC
  WHERE D.PRICELIST_CODE = H.PRICELIST_CODE
    AND PC.ou_code = D.ou_code
    AND PC.oa_org = DD.org_id
    AND DH.PRICELIST_DESC = H.PRICELIST_CODE
    AND DD.ITEM_CODE = D.ITEM_CODE
    --AND DD.std_price = D.std_price
    AND DD.START_DATE = D.START_DATE
    AND D.END_DATE IS NULL
    )
