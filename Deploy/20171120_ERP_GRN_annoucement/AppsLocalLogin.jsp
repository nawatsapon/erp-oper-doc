<%-- $Header: AppsLocalLogin.jsp 115.99 2009/04/27 17:04:03 dggriffi noship $ --%>

<%@ page import="oracle.cabo.share.config.ConfigurationImpl" %>
<%@ page import='oracle.cabo.ui.ServletRenderingContext'%>
<%@ page import='oracle.cabo.ui.UIConstants'%>
<%@ page import='oracle.cabo.ui.beans.BodyBean'%>
<%@ page import='oracle.cabo.ui.beans.TipBean'%>
<%@ page import='oracle.cabo.ui.beans.StyleSheetBean'%>
<%@ page import='oracle.cabo.ui.beans.form.SubmitButtonBean'%>
<%@ page import='oracle.cabo.ui.beans.form.FormBean'%>
<%@ page import='oracle.cabo.ui.beans.RawTextBean'%>
<%@ page import='oracle.cabo.ui.beans.FormattedTextBean'%>
<%@ page import='oracle.cabo.ui.beans.message.MessageTextInputBean'%>
<%@ page import='oracle.cabo.ui.beans.message.MessageBoxBean'%>
<%@ page import='oracle.cabo.ui.beans.layout.PageLayoutBean'%>
<%@ page import='oracle.cabo.ui.beans.layout.LabeledFieldLayoutBean'%>
<%@ page import='oracle.cabo.ui.beans.layout.FlowLayoutBean'%>
<%@ page import='oracle.cabo.ui.beans.layout.TableLayoutBean'%>
<%@ page import='oracle.cabo.ui.beans.layout.RowLayoutBean'%>
<%@ page import='oracle.cabo.ui.beans.layout.CellFormatBean'%>
<%@ page import='oracle.cabo.ui.beans.ImageBean'%>
<%@ page import='oracle.cabo.ui.beans.layout.SpacerBean'%>
<%@ page import='oracle.cabo.ui.beans.StyledTextBean'%>
<%@ page import='oracle.cabo.ui.beans.message.MessageStyledTextBean'%>
<%@ page import='oracle.cabo.ui.beans.nav.LinkBean'%>
<%@ page import='oracle.cabo.ui.beans.nav.ButtonBean'%>
<%@ page import='oracle.cabo.ui.beans.layout.StackLayoutBean'%>
<%@ page import='oracle.cabo.ui.beans.form.FormValueBean'%>
<%@ page import='oracle.cabo.share.agent.Agent'%>
<%@ page import='oracle.cabo.share.nls.LocaleContext'%>
<%@ page import='oracle.apps.fnd.common.Message'%>
<%@ page import='oracle.apps.fnd.common.Log'%>
<%@ page import='oracle.apps.fnd.common.AppsLog'%>
<%@ page import='oracle.apps.fnd.common.WebAppsContext'%>
<%@ page import='oracle.apps.fnd.common.ResourceStore'%>
<%@ page import='oracle.apps.fnd.common.AppsProfileStore'%>
<%@ page import='oracle.apps.fnd.sso.SessionMgr'%>
<%@ page import='oracle.apps.fnd.sso.SSOManager'%>
<%@ page import='oracle.apps.fnd.sso.SSOUtil'%>
<%@ page import='oracle.apps.fnd.sso.SSOAccessEnabler'%>
<%@ page import='oracle.apps.fnd.sso.Authenticator'%>
<%@ page import='oracle.apps.fnd.i18n.util.SSOMapper'%>
<%@ page import='oracle.apps.fnd.i18n.util.NLSMapper'%>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.StringTokenizer" %>
<%@ page import="java.util.Locale" %>
<%@ page import="java.sql.Connection" %>
<%@page import="oracle.apps.fnd.umx.password.PasswordUtil" %>
<%@page import="oracle.apps.fnd.oam.sdk.launchMode.restricted.WarningInfo" %>
<%@page import="oracle.apps.fnd.oam.sdk.launchMode.restricted.OAMRestrictedModeUtil" %>
<%@page import='oracle.apps.fnd.common.Log'%>
<%@page import='oracle.apps.fnd.functionSecurity.FunctionSecurity'%>
<%@page import='oracle.apps.fnd.functionSecurity.Function'%>
<%@ page session="false" %>

<%
  response.setHeader("Cache-Control", "no-cache");
  response.setHeader("Pragma", "no-cache");
  response.setDateHeader("Expires", 0);
%>

<%!
boolean isAgentPDA(PageContext jspPageContext) 
{
    ServletRenderingContext context =
    new ServletRenderingContext(jspPageContext);
    Agent agent = context.getAgent();

    if ((agent != null) && (agent.getAgentType() == Agent.TYPE_PDA)) 
    {
        return true;
    } 
    else 
    {
        return false;
    }
}
%>
<%
  SSOAccessEnabler sso = new SSOAccessEnabler(this);
  boolean alreadySet = sso.isAppsContextAvailable();
  WebAppsContext wctx = sso.getAppsContext();
  String methodName = "oracle.apps.fns.sso.html.AppsLocalLogin.jsp";
  AppsLog log = sso.getLog(wctx);
  boolean log_PROCEDURE = log.isEnabled(methodName,log.PROCEDURE);
  boolean log_STATEMENT = log.isEnabled(methodName,log.STATEMENT);
 boolean log_UNEXPECTED = log.isEnabled(methodName,log.UNEXPECTED);

  if (log_PROCEDURE) log.write(methodName,"BEGIN",log.PROCEDURE);

  Connection conn = null;

  try 
    {
        sso.setRequestCharacterEncoding(request);

	request = sso.verifySecurity(request, response, wctx);

        boolean isPDA = isAgentPDA(pageContext);

        String registerUrl = null;
        //Getting the URL parameters

        String requestUrl = request.getParameter("requestUrl");
        String cancelUrl = request.getParameter("cancelUrl");
        String errCode = request.getParameter("errCode");

        //Stop looking at errText, Bug 4724985
        String errText = null;
        String langCode = request.getParameter("langCode");
        String home_url = request.getParameter("home_url");
        String usernameParam = request.getParameter("username");

        StringBuffer debugBuff =  new StringBuffer();

        if (log_STATEMENT) log.write(methodName, "Url parameters: "+
		" 1.requestUrl: "+requestUrl+
		" 2.cancelUrl: "+cancelUrl+
		" 3.registerUrl: "+registerUrl+
		" 4.errCode: "+errCode+        
		" 5.errText: "+errText+
		" 6.langCode: "+langCode+
		" 7.home_url: "+home_url+
		" 7.username: "+usernameParam, log.STATEMENT);    

        //      out.println("<br>Point 2");
        String FND_SSO_LOGIN = null;
        String FND_SSO_CANCEL = null;
        String FND_SSO_REGISTER_HERE = null;
        String FND_SSO_FORGOT_PASSWORD = null;

        String FND_SSO_USER_NAME = null;
        String FND_SSO_PASSWORD = null;
        String FND_SSO_HINT_USERNAME = null;
        String FND_SSO_HINT_PASSWORD = null;

        String FND_SSO_SYSTEM_NOT_AVAIL = null;
        String FND_NOJAVASCRIPT = null;
        String FND_ORACLE_LOGO = null;
        String FND_SSO_EBIZ_SUITE = null;

        String FND_SSO_SARBANES_OXLEY_TEXT = null;
        String FND_SSO_COPYRIGHT_TEXT = null;
        String FND_SSO_WELCOME = null;

        String errMsg = null;
        String stackTrace = "";
        Message msg = null;

        String pNlsLanguage = null;
        String hLang = "";
        String dir = "";
        String end = "right";

        Vector myImages = null;
        int myImageSize = 0;

        String sOriginatingPage = "AppsLocalLogin.jsp";
        String sTargetPage = "AppsLocalLogin.jsp";

        ResourceStore rStore = null;
        SSOMapper ssoMapper = Authenticator.lmap;

        /**
        * Bitmap values for optional Login Page attributes
        * To show these attributes, just add the numeric values of all desired
        * attributes. So, for example to show PASSWORD_HINT and FORGOT_PASSWORD_URL
        * set the profile option, FND_SSO_LOGIN_MASK to 18 (2 + 16)
        * 
        * Converting decimal 512 to hexadecimal
        *    512 / 16 = 32, 0 (remainder in hex)
        *     32 / 16 = 2, 0
        *      2 / 16 = 0, 2
        * So decimal 512 is 0X200 in hexadecimal
        *    128 = 0X80, 256 = 0X100
        */
                final int USERNAME_HINT       = 0x01; // 1
        final int PASSWORD_HINT       = 0x02; // 2
        final int CANCEL_BUTTON       = 0x04; // 4
        final int FORGOT_PASSWORD_URL = 0x08; // 08
        final int REGISTER_URL        = 0x10; // 16
        final int LANGUAGE_IMAGES     = 0x20; // 32
        final int SARBANES_OXLEY_TEXT = 0x40; // 64

        conn = sso.getConnection();
        String mask = wctx.getProfileStore().getProfile("FND_SSO_LOCAL_LOGIN_MASK");
        if (log_STATEMENT) log.write(methodName, "Login Mask profile: "+mask, log.STATEMENT);
        if (mask == null || mask.equals("")) 
	{
		if (log_STATEMENT) log.write(methodName, "Defaulting mask to 0", log.STATEMENT);
		mask = "0";
	}

        int displayMask = 0;
	try {
		displayMask = Integer.parseInt(mask);

	} catch (NumberFormatException nfe) {
		if (log_STATEMENT) log.write(methodName, "NumberformatException Defaulting mask to 0, displayMask: "+displayMask, log.STATEMENT);
		displayMask = 0;
	}

        rStore = wctx.getResourceStore();

        boolean isICXDefault = false;
        if(langCode == null)
	{
		if (log_STATEMENT) log.write(methodName, "langCode is null", log.STATEMENT);
		String langs = request.getHeader("Accept-Language");
		if (log_STATEMENT) log.write(methodName, "Browser Langs: "+langs, log.STATEMENT);
		boolean selectedFromBrowser = false;
		if(langs != null)
		{
			if (log_STATEMENT) log.write(methodName, "Trying to get language from browser: "+langs, log.STATEMENT);
			if(langs.indexOf(",") == -1)
			{
				if (log_STATEMENT) log.write(methodName, "Only one browser lang: "+langs, log.STATEMENT);
				langCode = ssoMapper.getOracleLangFromHttp(langs.trim());
				if(SessionMgr.isInstalledLanguage(langCode)) 
				{
					selectedFromBrowser = true;
				}
				if (log_STATEMENT) log.write(methodName, "Setting langCode to langCode="+langCode, log.STATEMENT);
			} 
			else 
			{
				if (log_STATEMENT) log.write(methodName, "Multiple browser languagess trying find match with "
				        + "installed languages "+langs, log.STATEMENT);
				StringTokenizer st = new StringTokenizer(langs, ",");
				boolean foundMatch = false;
				while(st.hasMoreTokens())
				{
					String tmpLang = st.nextToken();
					int ind = tmpLang.indexOf(";");
					if(ind != -1)
					tmpLang = tmpLang.substring(0,ind);
					langCode = ssoMapper.getOracleLangFromHttp(tmpLang.trim());
					langCode = langCode.toUpperCase();
					if(SessionMgr.isInstalledLanguage(langCode))
					{
						if (log_STATEMENT) log.write(methodName, "found a match: "+langCode, log.STATEMENT);
						selectedFromBrowser = true;
						break;
					}
				}
			}
			if(!selectedFromBrowser) 
			{
				if (log_STATEMENT) log.write(methodName, "None of the browser languages is installed "
				        + "in apps selectedFromBrowser: "+selectedFromBrowser, log.STATEMENT);
				pNlsLanguage = wctx.getProfileStore().getProfile("ICX_LANGUAGE");
				if (log_STATEMENT) log.write(methodName, "profile ICX_LANGUAGE: "+pNlsLanguage, log.STATEMENT);
				isICXDefault = true;
				if(pNlsLanguage != null && !pNlsLanguage.equals("")) 
				{
					langCode = wctx.getLangCode(pNlsLanguage);
					if (!SessionMgr.isInstalledLanguage(langCode)) 
					{
						if (log_STATEMENT) log.write(methodName, "language set in profile ICX_LANGUAGE is not installed", log.STATEMENT);
						if (log_STATEMENT) log.write(methodName, "Defaulting to langCode = US", log.STATEMENT);
						langCode = "US";
					} 
				} 
				else 
				{
					if (log_STATEMENT) log.write(methodName, "profile ICX_LANGUAGE is not set", log.STATEMENT);
					if (log_STATEMENT) log.write(methodName, "Defaulting to langCode = US", log.STATEMENT);
					langCode = "US";
				}
			}
		} 
		else 
		{
			if (log_STATEMENT) log.write(methodName, "No browser languages available", log.STATEMENT);
			pNlsLanguage = wctx.getProfileStore().getProfile("ICX_LANGUAGE");
			isICXDefault = true;
			if (log_STATEMENT) log.write(methodName, "profile ICX_LANGUAGE: "+pNlsLanguage, log.STATEMENT);
			if(pNlsLanguage != null && !pNlsLanguage.equals("")) 
			{
				langCode = wctx.getLangCode(pNlsLanguage);
				if (!SessionMgr.isInstalledLanguage(langCode)) 
				{
					if (log_STATEMENT) log.write(methodName, "language set in profile ICX_LANGUAGE is not installed", log.STATEMENT);
					if (log_STATEMENT) log.write(methodName, "Defaulting to langCode = US", log.STATEMENT);
					langCode = "US";
				} 
			} 
			else 
			{
				if (log_STATEMENT) log.write(methodName, "profile ICX_LANGUAGE is not set", log.STATEMENT);
				if (log_STATEMENT) log.write(methodName, "Defaulting to langCode = US", log.STATEMENT);
				langCode = "US";
			}
		}
	} 
        else 
	{
		if (!SessionMgr.isInstalledLanguage(langCode)) 
		{
			if (log_STATEMENT) log.write(methodName, "langCode from url is not installed in apps", log.STATEMENT);
			pNlsLanguage = wctx.getProfileStore().getProfile("ICX_LANGUAGE");
			isICXDefault = true;
			if (log_STATEMENT) log.write(methodName, "profile ICX_LANGUAGE: "+pNlsLanguage, log.STATEMENT);
			if(pNlsLanguage != null && !pNlsLanguage.equals("")) 
			{
				langCode = wctx.getLangCode(pNlsLanguage);
				if (!SessionMgr.isInstalledLanguage(langCode)) 
				{
					if (log_STATEMENT) log.write(methodName, "language set in profile ICX_LANGUAGE is not installed", log.STATEMENT);
					if (log_STATEMENT) log.write(methodName, "Defaulting to langCode = US", log.STATEMENT);
					langCode = "US";
				} 
			} 
			else 
			{
				if (log_STATEMENT) log.write(methodName, "profile ICX_LANGUAGE is not set", log.STATEMENT);
				if (log_STATEMENT) log.write(methodName, "Defaulting to langCode = US", log.STATEMENT);
				langCode = "US";
			}
		}   
	}
        //langCode should never be null by this stage
        if (log_STATEMENT) log.write(methodName, "Trying to set session language langCode: "+langCode, log.STATEMENT);
        if(langCode != null && !langCode.equals("")) 
	{
		langCode = langCode.toUpperCase();
		oracle.apps.fnd.common.LangInfo info = wctx.getLangInfo(langCode , null, conn);
          if (info==null)
          {
              if (log_UNEXPECTED) 
              { 
                 log.write(methodName,"AOL/J failed to retreive langInfo",log.UNEXPECTED);
                 for (int i = 0; i < wctx.getErrorStack().getMessageCount(); i++) {
                      log.write(methodName, wctx.getErrorStack().nextMessage(), Log.UNEXPECTED);
                  }
              }
              throw new RuntimeException("Unexpected internal error");
          }

          
		pNlsLanguage = info.getNLSLanguage();
		wctx.setNLSContext(pNlsLanguage, null, null, null, null, null);
		wctx.setCurrLang(langCode);
		if (log_STATEMENT) log.write(methodName, "Setting the current session langCode: "+langCode, log.STATEMENT);

		String cval = SessionMgr.getAppsCookie(request);
		if(cval!= null && !cval.equals("-1") && !cval.equals("") )
		{
			if (log_STATEMENT) log.write(methodName, "Session exists:: "+cval+" setting lang :: "+langCode, log.STATEMENT);
			wctx.validateSession(cval);
			boolean check = wctx.setLanguageContext(pNlsLanguage, null, null, 
			    null, null, null, null);
		}
	}


        hLang ="lang=\""+ssoMapper.getHttpLangFromOracle(langCode)+"\"";      

        // BiDi
        if (sso.isRtl(langCode)) 
	{
		dir = " dir=rtl";
		end = "left";
		if (log_STATEMENT) log.write(methodName, "Setting lang direction dir: "+dir+" end: "+end, log.STATEMENT);
	}
        if (log_STATEMENT) log.write(methodName, "registerUrl parameter is null trying to get from UMX", log.STATEMENT);
        registerUrl = 
        oracle.apps.fnd.umx.util.regURL.RegURLGenerator.generateDefaultURL(
        wctx, SSOManager.getLoginUrl());
        if (log_STATEMENT) log.write(methodName, "registerUrl from UMX: "+registerUrl, log.STATEMENT);

        try 
	{
		if (requestUrl == null || requestUrl.equals("") || requestUrl.equals("APPSHOMEPAGE") ) 
		{
			if (log_STATEMENT) log.write(methodName, "requesrUrl: "+requestUrl, log.STATEMENT);
			if (isPDA) 
			{
				String dbc = oracle.apps.fnd.common.WebRequestUtil.getDBC(
				request, response);
				requestUrl = sso.getFwkServerWithoutTS(wctx) +
				"/OA_HTML/OA.jsp?page=/oracle/apps/fnd/framework/navigate/webui/" +
				"AppsNavigateMobilePG&dbc="+dbc;
				if (log_STATEMENT) log.write(methodName, "Defaulting for PDA requesrUrl: "+requestUrl, log.STATEMENT);
			} 
			else 
			{
				requestUrl = "APPSHOMEPAGE";
				if (log_STATEMENT) log.write(methodName, "Defaulting for non PDA requesrUrl: "+requestUrl, log.STATEMENT);
			} 
		}

		response.setContentType("text/html; charset=" + oracle.apps.fnd.sso.SessionMgr.getCharSet());

		if (errCode != null && !errCode.trim().equals("")) 
		{
			if (log_STATEMENT) log.write(methodName, "Processing Error Code errCode: "+errCode, log.STATEMENT);
			msg = new Message("FND", errCode);
			errMsg = msg.getMessageText(wctx.getResourceStore());
		}

		if (errCode == null && errText != null && !errText.trim().equals("")) 
		{
			if (log_STATEMENT) log.write(methodName, "Processing Error Code is null and errText available", log.STATEMENT);
			errMsg = errText;
		}
		if (log_STATEMENT) log.write(methodName, "Error Message errMsg: "+errMsg, log.STATEMENT);

		msg = new Message("FND", "FND_SSO_LOGIN");
		FND_SSO_LOGIN = msg.getMessageText(rStore);

		msg = new Message("FND", "FND_SSO_CANCEL");
		FND_SSO_CANCEL = msg.getMessageText(rStore);

		msg = new Message("FND", "FND_SSO_REGISTER_HERE");
		FND_SSO_REGISTER_HERE = msg.getMessageText(rStore);

		msg = new Message("FND", "FND_SSO_USER_NAME");
		FND_SSO_USER_NAME = msg.getMessageText(rStore);

		msg = new Message("FND", "FND_SSO_PASSWORD");
		FND_SSO_PASSWORD = msg.getMessageText(rStore);

		msg = new Message("FND", "FND_SSO_SYSTEM_NOT_AVAIL");
		FND_SSO_SYSTEM_NOT_AVAIL = msg.getMessageText(rStore);

		msg = new Message("FND", "FND_NOJAVASCRIPT");
		FND_NOJAVASCRIPT = msg.getMessageText(rStore);

		msg = new Message("FND", "FND_ORACLE_LOGO");
		FND_ORACLE_LOGO = msg.getMessageText(rStore);

		msg = new Message("FND", "FND_SSO_WELCOME");
		FND_SSO_WELCOME = msg.getMessageText(rStore);

		msg = new Message("FND", "FND_SSO_HINT_USERNAME");
		FND_SSO_HINT_USERNAME = msg.getMessageText(rStore);

		msg = new Message("FND", "FND_SSO_HINT_PASSWORD");
		FND_SSO_HINT_PASSWORD = msg.getMessageText(rStore);

		msg = new Message("FND", "FND_SSO_SARBANES_OXLEY_TEXT");
		FND_SSO_SARBANES_OXLEY_TEXT = msg.getMessageText(rStore);

		msg = new Message("FND", "FND_SSO_COPYRIGHT_TEXT");
		FND_SSO_COPYRIGHT_TEXT = msg.getMessageText(rStore);

		msg = new Message("FND", "FND_SSO_FORGOT_PASSWORD");
		FND_SSO_FORGOT_PASSWORD = msg.getMessageText(rStore);

		/** Bug 4320955.
		The branding text on LOGIN page and HOME page should be consistent.
		OA Fwk gets the branding text on HOME page using the UserFunctionName.
		Hence modified the code here to get the text from UserFunctionName instead
		from message text.  */
		FunctionSecurity fs = new FunctionSecurity(wctx);
		Function function = fs.getFunction("FWK_HOMEPAGE_BRAND");
		String userFunctionName = function.getUserFunctionName();
		FND_SSO_EBIZ_SUITE = userFunctionName;

		if (log_STATEMENT)
		{
		    log.write(methodName, "Messages used in Login Page:"+
			" FND_SSO_LOGIN: "+FND_SSO_LOGIN+
			" FND_SSO_CANCEL: "+FND_SSO_CANCEL, log.STATEMENT);
		    log.write(methodName, "Messages used in Login Page:"+          
			" FND_SSO_REGISTER_HERE: "+FND_SSO_REGISTER_HERE+
			" FND_SSO_USER_NAME: "+FND_SSO_USER_NAME, log.STATEMENT);
		    log.write(methodName, "Messages used in Login Page:"+  
			" FND_SSO_PASSWORD: "+FND_SSO_PASSWORD+
			" FND_SSO_SYSTEM_NOT_AVAIL: "+FND_SSO_SYSTEM_NOT_AVAIL, log.STATEMENT);
		    log.write(methodName, "Messages used in Login Page:"+
			" FND_NOJAVASCRIPT: "+FND_NOJAVASCRIPT+
			" FND_ORACLE_LOGO: "+FND_ORACLE_LOGO, log.STATEMENT);
		    log.write(methodName, "Messages used in Login Page:"+
			" FND_SSO_HINT_USERNAME: "+FND_SSO_HINT_USERNAME+
			" FND_SSO_HINT_PASSWORD: "+FND_SSO_HINT_PASSWORD, log.STATEMENT);
		    log.write(methodName, "Messages used in Login Page:"+
			" FND_SSO_SARBANES_OXLEY_TEXT: "+FND_SSO_SARBANES_OXLEY_TEXT+
			" FND_SSO_COPYRIGHT_TEXT: "+FND_SSO_COPYRIGHT_TEXT, log.STATEMENT);
		    log.write(methodName, "Messages used in Login Page:"+
			" FND_SSO_FORGOT_PASSWORD: "+FND_SSO_FORGOT_PASSWORD+
			" FND_SSO_EBIZ_SUITE: "+FND_SSO_EBIZ_SUITE, log.STATEMENT);
		}


		if(!isPDA)
		{
			myImages = SessionMgr.getInstalledLanguageImgInfo(request, wctx);
			myImageSize = myImages.size();
			if (log_STATEMENT) log.write(methodName, "Number of language Images: "+myImageSize, log.STATEMENT);
		}
	} 
	catch (Exception e) 
	{
		stackTrace = sso.getExceptionStackTrace(e);
		if (log_STATEMENT) log.write(methodName, "In the exception block: "+stackTrace, log.STATEMENT);
		errCode = e.getMessage();
		if (log_STATEMENT) log.write(methodName, "In the exception block errCode from Exception: "+errCode, log.STATEMENT);
		msg = new Message("FND", errCode);
		errMsg = msg.getMessageText(rStore);
		if (log_STATEMENT) log.write(methodName, "In the exception block from Exception errMsg: "+errMsg, log.STATEMENT);

		if (errCode == null) 
		{
			if (log_STATEMENT) log.write(methodName, "In the exception block errCode from Exception is null", log.STATEMENT);
			msg = new Message("FND", "FND-9914");
			errMsg = msg.getMessageText(rStore);
			if (log_STATEMENT) log.write(methodName, "In the exception block from default errMsg: "+errMsg, log.STATEMENT);
		}

	}

        String finalNoscript = "";
        String userAgent = request.getHeader("User-Agent");
        if(userAgent != null && !userAgent.equals("") && userAgent.indexOf("Blazer/4.0") != -1) 
	{
		finalNoscript = "";
	} 
        else 
	{
		finalNoscript = "<noscript>"+FND_NOJAVASCRIPT+"</noscript>";
	}

        %>
        <html <%=dir%> <%=hLang%>>
        <head>
        <title><%=FND_SSO_WELCOME%></title>
        <%
        // Rendering part
        ServletRenderingContext rContext = new ServletRenderingContext(pageContext);
        ConfigurationImpl cimpl = new ConfigurationImpl("myConfig");
        cimpl.putFullURIAndPath(cimpl.BASE_DIRECTORY, "/OA_HTML/cabo/",
        pageContext.getServletContext().getRealPath("/")+"/cabo/");
        cimpl.putProperty("scope-of-uix-cookie", "session");
        cimpl.register();
        rContext.setConfiguration("myConfig");


        java.util.Locale myLocale =
        new Locale(ssoMapper.getHttpLangFromOracle(langCode),
        ssoMapper.getTerritoryCode(langCode));

        rContext.setLocaleContext(new LocaleContext(myLocale));
        StyleSheetBean.sharedInstance().render(rContext);
        %>
        </head>
        <%=finalNoscript%>
        <%
        /*
        The following is the bean hierarchy:
        pageLayout
        |_ stackLayout
        |_TableLayout
        |_rowLayout1
        |_cellFormat
        |_LabelledFeildLayout
        | |_username
        | |_password
        | |_spacerBean
        |_flowLayout1
        | |_submitButton(login)
        | |_cancelButton(Cancel)
        |_Spacer
        |_FlowLayout2
        | |_RowLayout2
        |   |_cellFormat
        |   | |_tip      (Text:Did you)
        |   |_cellFormat
        |   | |_spacer
        |   |_cellFormat
        |     |_link      (forgot your password ?)
        |__RawText(Empty)
        |_FlowLayout3
        | |_RowLayout3
        |   |_cellFormat
        |   | |_tip
        |   |_cellFormat
        |   | |_spacer
        |   |_cellFormat
        |     |_link     (register here)

        */
        BodyBean body = new BodyBean();
        String ssoScript = " document.myForm.username.focus();"+
        " document.myForm.password.onkeypress = keyhandler;"+
        " function keyhandler(e){ "+
        "var kc; "+
        "if(window.event) kc=window.event.keyCode; "+
        "else if(e)  kc=e.which; "+
        "if (kc == 13) {  submitForm('myForm'); }}";

        body.setOnLoad(ssoScript);

        PageLayoutBean pageLayout = new PageLayoutBean();
        TableLayoutBean brandingFlbean1 = new TableLayoutBean();
        RowLayoutBean rowLay1 = new RowLayoutBean();
        CellFormatBean cbean1 = new CellFormatBean();

        if(isPDA)//bug 3624006 
	{ 
		FormattedTextBean formattedText = new FormattedTextBean("ORACLE "); 
		formattedText.setStyleClass("OraInlineErrorText"); // OraHeaderSub
		cbean1.addIndexedChild(formattedText);
		cbean1.setWrappingDisabled(true);
		FormattedTextBean formattedText1 = new FormattedTextBean(FND_SSO_EBIZ_SUITE); 
		formattedText1.setStyleClass("OraInlineErrorText"); // OraHeaderSub 
		cbean1.addIndexedChild(formattedText1);
		rowLay1.addIndexedChild(cbean1);
		brandingFlbean1.addIndexedChild(rowLay1);
	} 
        else 
	{ 
		cbean1.setVAlign(UIConstants.V_ALIGN_TOP);
		ImageBean imgBean1 = new ImageBean("/OA_MEDIA/FNDSSCORP.gif", FND_ORACLE_LOGO);
		cbean1.addIndexedChild(imgBean1);
		rowLay1.addIndexedChild(cbean1);

		CellFormatBean cbean2 = new CellFormatBean();
		cbean2.setVAlign(UIConstants.V_ALIGN_BOTTOM);
		cbean2.setWrappingDisabled(true);
		FormattedTextBean formattedText = new FormattedTextBean(FND_SSO_EBIZ_SUITE);
		formattedText.setStyleClass("OraHeaderSub"); // OraHeaderSub
		cbean2.addIndexedChild(formattedText); 
		rowLay1.addIndexedChild(cbean2);


		StackLayoutBean stb = new StackLayoutBean();
		stb.addIndexedChild(new SpacerBean(4,4));
		ImageBean imgBean2 = new ImageBean("/OA_MEDIA/fndpbs.gif");
		stb.addIndexedChild(imgBean2);
		rowLay1.addIndexedChild(stb);
		brandingFlbean1.addIndexedChild(rowLay1);

		RowLayoutBean rowLay2 = new RowLayoutBean();
		rowLay2.addIndexedChild(new SpacerBean(1,30));
		rowLay2.addIndexedChild(new SpacerBean(1,30));
		rowLay2.addIndexedChild(new SpacerBean(1,30));
		brandingFlbean1.addIndexedChild(rowLay2);      
	}
        pageLayout.setCorporateBranding(brandingFlbean1);
        pageLayout.setTitle(FND_SSO_WELCOME);


        if (errMsg!=null && !errMsg.trim().equals("")) 
	{
		MessageBoxBean msgBoxBean =
		new MessageBoxBean(UIConstants.MESSAGE_TYPE_ERROR, errMsg);
		pageLayout.addIndexedChild(msgBoxBean);
	}

        String restrictedModeMsg = "";
        WarningInfo wi = OAMRestrictedModeUtil.getWarningMessage(wctx);
        if (wi != null) 
	{
		restrictedModeMsg = wi.getCommentsText();
	}

        if (restrictedModeMsg!=null && !restrictedModeMsg.trim().equals("") 
            && (errMsg == null || "".equals(errMsg)) ) 
	{

		MessageBoxBean msgBoxBean =
		new MessageBoxBean(UIConstants.MESSAGE_TYPE_WARNING, restrictedModeMsg);
		pageLayout.addIndexedChild(msgBoxBean);
	}

        StackLayoutBean stackLayout = new StackLayoutBean();
        FormBean form = new FormBean("myForm");
        form.setMethod("POST"); 
        form.setDestination("fndvald.jsp");
        form.setAttributeValue(UIConstants.NO_AUTO_COMPLETE_ATTR, new Boolean(true));

        if (!isPDA) 
	{
		StackLayoutBean stackLayoutForCopyright = new StackLayoutBean();
		StyledTextBean copyRight = new StyledTextBean();
		copyRight.setText(FND_SSO_COPYRIGHT_TEXT);

		if ((displayMask & SARBANES_OXLEY_TEXT) != 0) 
		{
			StyledTextBean legalMessage = new StyledTextBean();
			legalMessage.setText(FND_SSO_SARBANES_OXLEY_TEXT);
			stackLayoutForCopyright.addIndexedChild(legalMessage);
		}
		stackLayoutForCopyright.addIndexedChild(copyRight);
		pageLayout.setCopyright(stackLayoutForCopyright);
	}

        if (FND_SSO_USER_NAME == null || FND_SSO_PASSWORD == null) 
	{ //bug 3210032
		MessageBoxBean msgBoxBean =
		new MessageBoxBean(UIConstants.MESSAGE_TYPE_ERROR, FND_SSO_SYSTEM_NOT_AVAIL);
		pageLayout.addIndexedChild(msgBoxBean);
		body.render(rContext);
	}
        else
	{
		TableLayoutBean tlayout = new TableLayoutBean();
		tlayout.setWidth("100%");
		RowLayoutBean rl1 = new RowLayoutBean();
		cbean1 = new CellFormatBean();
		cbean1.setHAlign(UIConstants.H_ALIGN_CENTER);
		LabeledFieldLayoutBean layout = new LabeledFieldLayoutBean();
		MessageTextInputBean username = null;
		username = new MessageTextInputBean("username");
		if(usernameParam != null && !usernameParam.equals(""))
		{
			username.setText(usernameParam.trim());
		}
		username.setPrompt(FND_SSO_USER_NAME);

		if (((displayMask & USERNAME_HINT) != 0) && !isPDA) 
		{
			username.setTip(FND_SSO_HINT_USERNAME);
		}
		layout.addIndexedChild(username);
		MessageTextInputBean password = new MessageTextInputBean("password");
		if (((displayMask & PASSWORD_HINT) != 0) && !isPDA) 
		{
			password.setTip(FND_SSO_HINT_PASSWORD);
		}
		password.setPrompt(FND_SSO_PASSWORD);
		password.setSecret(true);
		layout.addIndexedChild(password);
		layout.addIndexedChild(new  SpacerBean(1,30));
		SubmitButtonBean button = new SubmitButtonBean(FND_SSO_LOGIN);
		
                //Bug:6079380. Modifying so it will check for displayMask first
                //if cancelUrl is not set will default to:
                //"oa_servlets/oracle.apps.fnd.sso.AppsLogin"
                
                if ((displayMask & CANCEL_BUTTON) != 0) {
                        if (cancelUrl == null || cancelUrl.equals("")) {
                             cancelUrl= SSOManager.getLocalLoginUrl();
                             if (langCode!=null && !"".equals(langCode))
                                cancelUrl +="?langCode="+langCode;
                        }                
			FlowLayoutBean flbean = new FlowLayoutBean();
			flbean.addIndexedChild(button);
			ButtonBean cbutton = new ButtonBean(FND_SSO_CANCEL);
			cbutton.setDestination(cancelUrl);
			flbean.addIndexedChild(cbutton);
			layout.addIndexedChild(flbean);
		} 
		else 
		{
			layout.addIndexedChild(button);
		}

		String forgotPwdUrl = null;
		try
		{
			if(!isPDA) 
				forgotPwdUrl = PasswordUtil.generateForgotPwdUrl(wctx , SSOManager.getLoginUrl(), SSOManager.getLoginUrl(), FND_SSO_LOGIN);
		}
		catch (Exception e) 
		{
			stackTrace = sso.getExceptionStackTrace(e);
			errCode = e.getMessage();
			msg = new Message("FND", errCode);
			errMsg = msg.getMessageText(rStore);

			if (errCode == null) 
			{
				msg = new Message("FND", "FND-9914");
				errMsg = msg.getMessageText(rStore);
			}
		} 

		if (((displayMask & FORGOT_PASSWORD_URL) != 0) && !isPDA &&
			(forgotPwdUrl != null && !("".equals(forgotPwdUrl)))) 
		{

			FlowLayoutBean flbean_tip1 = new FlowLayoutBean();
			RowLayoutBean tip_row1 = new RowLayoutBean();
			if (dir!=null && !dir.equals(""))
			tip_row1.setHAlign(UIConstants.H_ALIGN_RIGHT);
			else
			tip_row1.setHAlign(UIConstants.H_ALIGN_LEFT);

			CellFormatBean cf11 = new CellFormatBean();
			TipBean tip1 = new TipBean("");
			cf11.addIndexedChild(tip1);
			tip_row1.addIndexedChild(cf11);


			CellFormatBean cf12 = new CellFormatBean();
			SpacerBean spacer1 = new  SpacerBean(5,1);
			cf12.addIndexedChild(spacer1);
			tip_row1.addIndexedChild(cf12);

			CellFormatBean cf13 = new CellFormatBean();
			cf13.setHAlign(UIConstants.H_ALIGN_LEFT);
			LinkBean forgotPasswordLink = new LinkBean(FND_SSO_FORGOT_PASSWORD
			, forgotPwdUrl);
			cf13.addIndexedChild(forgotPasswordLink);
			tip_row1.addIndexedChild(cf13);
			flbean_tip1.addIndexedChild(tip_row1);
			layout.addIndexedChild(new SpacerBean(1,20));
			layout.addIndexedChild(flbean_tip1);
		}
		layout.addIndexedChild(new RawTextBean());
		if (((displayMask & REGISTER_URL) != 0) && !isPDA
			&& (registerUrl != null && !("".equals(registerUrl)))) 
		{

			FlowLayoutBean flbean_tip2 = new FlowLayoutBean();
			RowLayoutBean tip_row2 = new RowLayoutBean();

			CellFormatBean cf21 = new CellFormatBean();
			TipBean tip2 = new TipBean();
			cf21.addIndexedChild(tip2);
			tip_row2.addIndexedChild(cf21);

			CellFormatBean cf22 = new CellFormatBean();
			SpacerBean spacer2 = new SpacerBean(5,1);
			cf22.addIndexedChild(spacer2);
			tip_row2.addIndexedChild(cf22);

			CellFormatBean cf23 = new CellFormatBean();
			LinkBean registerLink = new LinkBean(FND_SSO_REGISTER_HERE, registerUrl);
			cf23.addIndexedChild(registerLink);
			tip_row2.addIndexedChild(cf23);

			flbean_tip2.addIndexedChild(tip_row2);
			layout.addIndexedChild(flbean_tip2);
		}

		layout.addIndexedChild(new RawTextBean());
		if (displayMask == 127 ) 
		{
			layout.addIndexedChild(new SpacerBean(1, 20));
		} 
		else 
		{
			layout.addIndexedChild(new SpacerBean(1, 5));
		}
		layout.addIndexedChild(new RawTextBean());
		cbean1.addIndexedChild(layout);
		rl1.addIndexedChild(cbean1);
		tlayout.addIndexedChild(rl1);

		if (myImageSize > 1 && ((displayMask & LANGUAGE_IMAGES) != 0) && !isPDA) 
		{
			RowLayoutBean rl2 = new RowLayoutBean();
			rl2.setWidth("100%");
			CellFormatBean cbean2 = new CellFormatBean();
			cbean2.setWidth("100%");
			cbean2.setHAlign("center");
			FlowLayoutBean langSwitcher = new FlowLayoutBean();
			Vector tmpVec = (Vector)myImages.elementAt(0);
			langSwitcher.addIndexedChild(new ImageBean((String)tmpVec.elementAt(1),
			(String)tmpVec.elementAt(2), SSOManager.recalcSignature((String)tmpVec.elementAt(3),wctx)));

			for ( int i=1; i<myImageSize;i++) 
			{
				tmpVec = (Vector)myImages.elementAt(i);

				langSwitcher.addIndexedChild(new ImageBean("/OA_MEDIA/lang_bullet.gif"));
				langSwitcher.addIndexedChild(new ImageBean((String)tmpVec.elementAt(1),
				(String)tmpVec.elementAt(2), SSOManager.recalcSignature((String)tmpVec.elementAt(3),wctx)));
			} 
			cbean2.addIndexedChild(langSwitcher);
			rl2.addIndexedChild(cbean2);
			tlayout.addIndexedChild(rl2);
		}
		form.addIndexedChild(tlayout);
		if (langCode != null) 
		{
			if(!isICXDefault) 
				form.addIndexedChild(new FormValueBean("langCode", langCode));
		} 

		if (home_url != null) 
		{
			form.addIndexedChild(new FormValueBean("home_url",home_url ));
		}

		if (cancelUrl != null)
		{
			form.addIndexedChild(new FormValueBean("cancelUrl",cancelUrl ));
		}

		form.addIndexedChild(new FormValueBean("requestUrl", requestUrl ));
		stackLayout.addIndexedChild(form);
		pageLayout.addIndexedChild(stackLayout);
		body.addIndexedChild(pageLayout);
		body.render(rContext);
	}
    } 
  catch(Exception e){
      out.println("<pre><code>"+sso.getExceptionStackTrace(e)+"</pre></code>");
  }
  finally
  {
         if(conn!=null) sso.releaseConnection();
         if(alreadySet == false) sso.releaseAppsContext();
  }
%>
<p>
<center>
<img src="/OA_MEDIA/dtac_ann1.png" alt="dtac">
</center>
</p>
<p>
<center><font size="3" color="black">If you have any queries, please feel free to drop your message at <a href="mailto:FA_AP@dtac.co.th" target="_top">FA_AP_Unit
</a></font></center>
</p>
</html>
