DECLARE
	---------------------------------------------------------
	--Setup scope of GL Period
	P_DTAC_GL_PERIOD_START				NUMBER	:= 	20000001;
	P_DTAC_GL_PERIOD_END				NUMBER	:= 	20190008;
	P_DTN_GL_PERIOD_START				NUMBER	:= 	20000001;
	P_DTN_GL_PERIOD_END					NUMBER	:= 	20190008;
	--Setup scope of GL Period
	---------------------------------------------------------

    ----------------------------------------
    	PROCEDURE DTAC_PREP_DATA_A IS
		  p_set_of_books_id       financials_system_params_all.set_of_books_id%type;
		  p_gl_date               DATE;
		  p_SEGMENT1              VARCHAR2(10);
		  p_SEGMENT11             VARCHAR2(10);
	BEGIN
	  p_set_of_books_id := 1001;                                                    -- DTAC 1001, DTN 1008
	  p_SEGMENT1 := '01';                                                           -- DTAC 01, DTN 08
	  P_SEGMENT11 := '70003';
	  
      insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('TAC_PO_ACCR_GRN_DTAC','Begin',sysdate); commit;
	  --LOOP period
	  FOR l_code_combi_id IN (
				SELECT CODE_COMBINATION_ID
				FROM GL_SUMMARY_COMBINATIONS_V
				WHERE chart_of_accounts_id    = 101                                 --**--
				AND NVL(set_of_books_id, p_set_of_books_id) = p_set_of_books_id --1001
				AND (CODE_COMBINATION_ID     IN
				  (SELECT CODE_COMBINATION_ID
				  FROM GL_CODE_COMBINATIONS
				  WHERE 1                 =1
				  AND SEGMENT1            = p_SEGMENT1                              --**--
				  AND SEGMENT11           = P_SEGMENT11                             --**--
				  AND CHART_OF_ACCOUNTS_ID=101                                      --**--
				  ))
				ORDER BY CODE_COMBINATION_ID
	  ) LOOP
		  FOR l_gl_period IN (
			  SELECT effective_period_num 
			  FROM GL_JE_JOURNAL_LINES_V
							WHERE set_of_books_id = p_set_of_books_id --1008
							AND batch_status||''              = 'P'
							AND (actual_flag                  = 'A')
							AND (line_set_of_books_id         = p_set_of_books_id --1008
								AND line_code_combination_id      = l_code_combi_id.CODE_COMBINATION_ID --p_code_combination_id --34656
								) 
							AND (currency_code               != 'STAT')
							and effective_period_num BETWEEN P_DTAC_GL_PERIOD_START AND P_DTAC_GL_PERIOD_END 
			  GROUP BY effective_period_num
		  ) LOOP
			  INSERT INTO TAC_PO_ACCR_GRN_DTAC
				SELECT effective_period_num, 
					   gl.je_header_id,
					   gl.line_je_line_num,
					   gl.PERIOD_NAME,
					   p_SEGMENT1 SEGMENT1,
					   P_SEGMENT11 SEGMENT11,
					   gl.BATCH_NAME,
					   gl.HEADER_NAME,
					   gl.JE_SOURCE,
					   gl.CURRENCY_CODE,
					   gl.LINE_ACCOUNTED_CR,
					   gl.LINE_ACCOUNTED_DR,
					   po.PO_ORDER_NUMBER, 
					   po.RCV_RECEIPT_NUM,
					   po.GRN_DATE GRN_DATE_HDR,
					   GL.LINE_EFFECTIVE_DATE GRN_GL_DATE,
					   gl.set_of_books_id,
					   gl.LINE_CODE_COMBINATION_ID,
					   gl.BATCH_DATE_CREATED,
					   po.TRX_HDR_ID,
					   po.TRX_DATE GRN_DATE_TRX_LINE
				FROM GL_JE_JOURNAL_LINES_V gl,
					 (
						select * 
						from TAC_XLA_PO_AEL_GL_V
						where (je_header_id, je_line_num) in 
						(
						  SELECT JE_HEADER_ID, LINE_JE_LINE_NUM
							  FROM GL_JE_JOURNAL_LINES_V gl
							  WHERE gl.set_of_books_id = p_set_of_books_id --1008
								AND gl.batch_status||''              = 'P'
								AND (gl.actual_flag                  = 'A')
								AND (gl.line_set_of_books_id         = p_set_of_books_id --1008
								AND gl.line_code_combination_id      = l_code_combi_id.CODE_COMBINATION_ID --34656
								AND effective_period_num = l_gl_period.effective_period_num
								  )
								AND (gl.currency_code               != 'STAT') 
								and gl.JE_SOURCE = 'Purchasing'
						)
					 ) po
				WHERE gl.set_of_books_id = p_set_of_books_id 
					AND gl.batch_status||''              = 'P'
					AND (gl.actual_flag                  = 'A')
					AND (gl.line_set_of_books_id         = p_set_of_books_id
					AND gl.line_code_combination_id      = l_code_combi_id.CODE_COMBINATION_ID --34656
					AND effective_period_num = l_gl_period.effective_period_num
						)
					AND (gl.currency_code               != 'STAT') 
					and gl.JE_HEADER_ID = po.je_header_id(+)
					AND gl.LINE_JE_LINE_NUM = po.je_line_num(+)
					and gl.JE_SOURCE = 'Purchasing'
	--				and po.application_id = 201  
	--				AND TRUNC(GL.BATCH_DATE_CREATED) <= p_gl_date
				  ORDER BY gl.je_header_id, gl.line_je_line_num;
			 -- insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('TAC_PO_ACCR_GRN_DTAC - '||l_code_combi_id.CODE_COMBINATION_ID,l_gl_period.effective_period_num,sysdate); commit;
		  END LOOP;
		  commit;
                    
	  END LOOP;
	  commit;
    insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('TAC_PO_ACCR_GRN_DTAC','End',sysdate); commit;
	DBMS_OUTPUT.PUT_LINE('Process successfully.');

	EXCEPTION
	  WHEN others THEN
		DBMS_OUTPUT.PUT_LINE('An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
		rollback;
	END;
	--DTAC_PREP_DATA A END
    -----------------
    
     	PROCEDURE DTAC_PREP_DATA_B IS
		  p_set_of_books_id       financials_system_params_all.set_of_books_id%type;
		  p_gl_date               DATE;
		  p_SEGMENT1              VARCHAR2(10);
		  p_SEGMENT11             VARCHAR2(10);
	BEGIN
	  p_set_of_books_id := 1001;                                                    -- DTAC 1001, DTN 1008
	  p_SEGMENT1 := '01';                                                           -- DTAC 01, DTN 08
	  P_SEGMENT11 := '70003';
	  
      insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('TAC_PO_ACCR_INVOICE_DTAC','Begin',sysdate); commit;
	  --LOOP period
	  FOR l_code_combi_id IN (
				SELECT CODE_COMBINATION_ID
				FROM GL_SUMMARY_COMBINATIONS_V
				WHERE chart_of_accounts_id    = 101                                 --**--
				AND NVL(set_of_books_id, p_set_of_books_id) = p_set_of_books_id --1001
				AND (CODE_COMBINATION_ID     IN
				  (SELECT CODE_COMBINATION_ID
				  FROM GL_CODE_COMBINATIONS
				  WHERE 1                 =1
				  AND SEGMENT1            = p_SEGMENT1                              --**--
				  AND SEGMENT11           = P_SEGMENT11                             --**--
				  AND CHART_OF_ACCOUNTS_ID=101                                      --**--
				  ))
				ORDER BY CODE_COMBINATION_ID
	  ) LOOP
		  FOR l_gl_period IN (
			  SELECT effective_period_num 
			  FROM GL_JE_JOURNAL_LINES_V
							WHERE set_of_books_id = p_set_of_books_id --1008
							AND batch_status||''              = 'P'
							AND (actual_flag                  = 'A')
							AND (line_set_of_books_id         = p_set_of_books_id --1008
								AND line_code_combination_id      = l_code_combi_id.CODE_COMBINATION_ID --p_code_combination_id --34656
								) 
							AND (currency_code               != 'STAT')
							and effective_period_num BETWEEN P_DTAC_GL_PERIOD_START AND P_DTAC_GL_PERIOD_END 
			  GROUP BY effective_period_num
		  ) LOOP
			
			  
				--++--
				INSERT INTO TAC_PO_ACCR_INVOICE_DTAC
				SELECT effective_period_num, 
					   gl.je_header_id,
					   gl.line_je_line_num,
					   gl.PERIOD_NAME,
					   p_SEGMENT1 SEGMENT1,
					   p_SEGMENT11 SEGMENT11,
					   gl.BATCH_NAME,
					   gl.HEADER_NAME,
					   gl.JE_SOURCE,
					   gl.CURRENCY_CODE,
					   gl.LINE_ACCOUNTED_CR,
					   gl.LINE_ACCOUNTED_DR,
					   inv.PO_ORDER_NUMBER, --inv.TRX_NUMBER_DISPLAYED INVOICE_NO, 
					   inv.RCV_RECEIPT_NUM, 
					   inv.TRX_DATE INVOICE_DATE, 
					   --inv.INV_GL_DATE,
					   GL.LINE_EFFECTIVE_DATE INV_GL_DATE,
					   gl.set_of_books_id,
					   gl.LINE_CODE_COMBINATION_ID,
					   gl.BATCH_DATE_CREATED,
					   inv.RCT_TRX_ID,
					   inv.TRX_NUMBER_C INVOICE_NUMBER
				FROM GL_JE_JOURNAL_LINES_V gl,
					 --TAC_XLA_AP_INV_AEL_GL_V inv
					 --
					 (
						  select *
						  from TAC_XLA_AP_INV_AEL_GL_V2
						  where (je_header_id,je_line_num) in 
								(
									select JE_HEADER_ID,LINE_JE_LINE_NUM
									from GL_JE_JOURNAL_LINES_V
									where set_of_books_id = p_set_of_books_id
									  AND batch_status||''              = 'P'
									  AND (actual_flag                  = 'A')
									  AND (line_set_of_books_id         = p_set_of_books_id
									  AND line_code_combination_id      = l_code_combi_id.CODE_COMBINATION_ID --34656
									  AND effective_period_num = l_gl_period.effective_period_num
										  )
									  AND (currency_code               != 'STAT') 
									  and JE_SOURCE = 'Payables'
								)
					 ) inv
					 --
				WHERE gl.set_of_books_id = p_set_of_books_id
					AND gl.batch_status||''              = 'P'
					AND (gl.actual_flag                  = 'A')
					AND (gl.line_set_of_books_id         = p_set_of_books_id --1008
					AND gl.line_code_combination_id      = l_code_combi_id.CODE_COMBINATION_ID --34656
					AND effective_period_num = l_gl_period.effective_period_num
						)
					AND (gl.currency_code               != 'STAT') 
					and gl.JE_HEADER_ID = inv.je_header_id(+)
					AND gl.LINE_JE_LINE_NUM = inv.je_line_num(+)
				  and gl.JE_SOURCE = 'Payables'
	--			  and inv.application_id = 200
	--			  AND TRUNC(GL.BATCH_DATE_CREATED) <= p_gl_date
				  ORDER BY gl.je_header_id, gl.line_je_line_num;
				--++--
			  insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('TAC_PO_ACCR_INVOICE_DTAC - '||l_code_combi_id.CODE_COMBINATION_ID,l_gl_period.effective_period_num,sysdate); commit;
		  END LOOP;
		  commit;
	  END LOOP;
	  commit;
        insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('TAC_PO_ACCR_INVOICE_DTAC','End',sysdate); commit;
	DBMS_OUTPUT.PUT_LINE('Process successfully.');

	EXCEPTION
	  WHEN others THEN
		DBMS_OUTPUT.PUT_LINE('An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
		rollback;
	END;
	--DTAC_PREP_DATA B END
    -----------------
    
    PROCEDURE DTAC_PREP_DATA_C IS
		  p_set_of_books_id       financials_system_params_all.set_of_books_id%type;
		  p_gl_date               DATE;
		  p_SEGMENT1              VARCHAR2(10);
		  p_SEGMENT11             VARCHAR2(10);
	BEGIN
	  p_set_of_books_id := 1001;                                                    -- DTAC 1001, DTN 1008
	  p_SEGMENT1 := '01';                                                           -- DTAC 01, DTN 08
	  P_SEGMENT11 := '70003';
	  
      insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('TAC_PO_ACCR_OTHERS_DTAC','Begin',sysdate); commit;
	  --LOOP period
	  FOR l_code_combi_id IN (
				SELECT CODE_COMBINATION_ID
				FROM GL_SUMMARY_COMBINATIONS_V
				WHERE chart_of_accounts_id    = 101                                 --**--
				AND NVL(set_of_books_id, p_set_of_books_id) = p_set_of_books_id --1001
				AND (CODE_COMBINATION_ID     IN
				  (SELECT CODE_COMBINATION_ID
				  FROM GL_CODE_COMBINATIONS
				  WHERE 1                 =1
				  AND SEGMENT1            = p_SEGMENT1                              --**--
				  AND SEGMENT11           = P_SEGMENT11                             --**--
				  AND CHART_OF_ACCOUNTS_ID=101                                      --**--
				  ))
				ORDER BY CODE_COMBINATION_ID
	  ) LOOP
		  FOR l_gl_period IN (
			  SELECT effective_period_num 
			  FROM GL_JE_JOURNAL_LINES_V
							WHERE set_of_books_id = p_set_of_books_id --1008
							AND batch_status||''              = 'P'
							AND (actual_flag                  = 'A')
							AND (line_set_of_books_id         = p_set_of_books_id --1008
								AND line_code_combination_id      = l_code_combi_id.CODE_COMBINATION_ID --p_code_combination_id --34656
								) 
							AND (currency_code               != 'STAT')
							and effective_period_num BETWEEN P_DTAC_GL_PERIOD_START AND P_DTAC_GL_PERIOD_END 
			  GROUP BY effective_period_num
		  ) LOOP
			
			
				--++--
				INSERT INTO TAC_PO_ACCR_OTHERS_DTAC
				SELECT l_gl_period.effective_period_num,
					   BATCH_NAME, HEADER_NAME,JE_SOURCE, CURRENCY_CODE,LINE_JE_LINE_NUM,  LINE_ACCOUNTED_DR, LINE_ACCOUNTED_CR,
					   TRUNC(BATCH_DATE_CREATED) GL_DATE
				FROM GL_JE_JOURNAL_LINES_V
				WHERE set_of_books_id = p_set_of_books_id --1008
				AND batch_status||''              = 'P'
				AND (actual_flag                  = 'A')
				AND (line_set_of_books_id         = p_set_of_books_id --1008
					  AND line_code_combination_id = l_code_combi_id.CODE_COMBINATION_ID --34656
					  AND effective_period_num = l_gl_period.effective_period_num
					)
				AND (currency_code               != 'STAT')
				AND JE_SOURCE  not in ('Payables','Purchasing')
				ORDER BY je_header_id, line_je_line_num;
                
                insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('TAC_PO_ACCR_OTHERS_DTAC - '||l_gl_period.effective_period_num,l_gl_period.effective_period_num,sysdate); commit;
                
		  END LOOP;
		  commit;
	  END LOOP;
	  commit;
        insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('TAC_PO_ACCR_OTHERS_DTAC','End',sysdate); commit;
	DBMS_OUTPUT.PUT_LINE('Process successfully.');

	EXCEPTION
	  WHEN others THEN
		DBMS_OUTPUT.PUT_LINE('An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
		rollback;
	END;
	--DTAC_PREP_DATA C END
    
 
	
	
	
	--GEN_REPORT_DTAC BEGIN
	PROCEDURE GEN_REPORT_DTAC IS
	BEGIN
	
	insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('T2_TAC_PO_ACCR_REPORT_DTAC','Begin insert',sysdate); commit;
	
		INSERT INTO T2_TAC_PO_ACCR_REPORT_DTAC
			SELECT 99 MATCHED_CASE , GRN.*, INV.*, NVL(GRN_AMOUNT,0) + NVL(INV_AMOUNT,0) TT_AMOUNT,null po_status
			FROM
			(
				SELECT MAX(EFFECTIVE_PERIOD_NUM) GRN_PERIOD_NUM, po_order_number GRN_PO_NUMBER, rcv_receipt_num GRN_RECEIPT_NUMBER,GRN_DATE_HDR, GRN_DATE_TRX_LINE, GRN_GL_DATE, TRX_HDR_ID,
					   SUM(NVL(LINE_ACCOUNTED_CR,0)) - SUM(NVL(LINE_ACCOUNTED_DR,0)) GRN_AMOUNT
				FROM TAC_PO_ACCR_GRN_DTAC
				GROUP BY po_order_number, rcv_receipt_num, GRN_DATE_HDR, GRN_DATE_TRX_LINE, GRN_GL_DATE, TRX_HDR_ID
				
			) GRN full outer join
			(
				SELECT MAX(EFFECTIVE_PERIOD_NUM) INV_PERIOD_NUM, po_order_number INV_PO_NUMBER, rcv_receipt_num INV_RECEIPT_NUMBER, RCT_TRX_ID,
					   SUM(NVL(LINE_ACCOUNTED_CR,0)) - SUM(NVL(LINE_ACCOUNTED_DR,0)) INV_AMOUNT
				FROM TAC_PO_ACCR_INVOICE_DTAC
				GROUP BY po_order_number, rcv_receipt_num, RCT_TRX_ID
				
			) INV
			ON GRN.GRN_PO_NUMBER = INV.INV_PO_NUMBER
			AND GRN.GRN_RECEIPT_NUMBER = INV.INV_RECEIPT_NUMBER
			and grn.TRX_HDR_ID = inv.RCT_TRX_ID
			ORDER BY 1,2,3,6
		;

        insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('T2_TAC_PO_ACCR_REPORT_DTAC','End insert',sysdate); commit;
        
        insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('T2_TAC_PO_ACCR_REPORT_DTAC','Begin update case1',sysdate); commit;		
		---
		-- CASE 1
		UPDATE T2_TAC_PO_ACCR_REPORT_DTAC 
		SET MATCHED_CASE = 1
		WHERE MATCHED_CASE = 99
		  and GRN_PO_NUMBER IN (
				select GRN_PO_NUMBER --grn.*,inv.*, grn.grn_amount + inv.inv_amount as chk
				from 
				(SELECT MAX(EFFECTIVE_PERIOD_NUM) GRN_PERIOD_NUM, po_order_number GRN_PO_NUMBER
					 --, rcv_receipt_num GRN_RECEIPT_NUMBER
					 , SUM(NVL(LINE_ACCOUNTED_CR,0)) - SUM(NVL(LINE_ACCOUNTED_DR,0)) GRN_AMOUNT       
				FROM TAC_PO_ACCR_GRN_DTAC
				GROUP BY po_order_number
					   --, rcv_receipt_num
				) grn,
				(
				SELECT MAX(EFFECTIVE_PERIOD_NUM) INV_PERIOD_NUM, po_order_number INV_PO_NUMBER
					 --, rcv_receipt_num INV_RECEIPT_NUMBER
					 , SUM(NVL(LINE_ACCOUNTED_CR,0)) - SUM(NVL(LINE_ACCOUNTED_DR,0)) INV_AMOUNT
				FROM TAC_PO_ACCR_INVOICE_DTAC
				GROUP BY po_order_number
					   --, rcv_receipt_num;
				) inv
				where grn.GRN_PO_NUMBER = inv.INV_PO_NUMBER
				and (grn.grn_amount + inv.inv_amount) = 0
		);
		--
        insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('T2_TAC_PO_ACCR_REPORT_DTAC','End update case1',sysdate); commit;
        
        insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('T2_TAC_PO_ACCR_REPORT_DTAC','Begin update case11',sysdate); commit;
		
		UPDATE T2_TAC_PO_ACCR_REPORT_DTAC 
		SET MATCHED_CASE = 1
		WHERE MATCHED_CASE = 99
		  and INV_PO_NUMBER IN (
				select GRN_PO_NUMBER --grn.*,inv.*, grn.grn_amount + inv.inv_amount as chk
				from 
				(SELECT MAX(EFFECTIVE_PERIOD_NUM) GRN_PERIOD_NUM, po_order_number GRN_PO_NUMBER
					 --, rcv_receipt_num GRN_RECEIPT_NUMBER
					 , SUM(NVL(LINE_ACCOUNTED_CR,0)) - SUM(NVL(LINE_ACCOUNTED_DR,0)) GRN_AMOUNT       
				FROM TAC_PO_ACCR_GRN_DTAC
				GROUP BY po_order_number
					   --, rcv_receipt_num
				) grn,
				(
				SELECT MAX(EFFECTIVE_PERIOD_NUM) INV_PERIOD_NUM, po_order_number INV_PO_NUMBER
					 --, rcv_receipt_num INV_RECEIPT_NUMBER
					 , SUM(NVL(LINE_ACCOUNTED_CR,0)) - SUM(NVL(LINE_ACCOUNTED_DR,0)) INV_AMOUNT
				FROM TAC_PO_ACCR_INVOICE_DTAC
				GROUP BY po_order_number
					   --, rcv_receipt_num;
				) inv
				where grn.GRN_PO_NUMBER = inv.INV_PO_NUMBER
				and (grn.grn_amount + inv.inv_amount) = 0
		);

        insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('T2_TAC_PO_ACCR_REPORT_DTAC','End update case11',sysdate); commit;
        
        insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('T2_TAC_PO_ACCR_REPORT_DTAC','Begin update case2',sysdate); commit;
		--------------------------------------------------------------------------------
		--------------------------------------------------------------------------------
		-- CASE 2

		UPDATE T2_TAC_PO_ACCR_REPORT_DTAC 
		SET MATCHED_CASE = 2
		WHERE MATCHED_CASE = 99 AND
			GRN_PO_NUMBER IN (
			select GRN_PO_NUMBER 
				   --grn.*,inv.*, grn.grn_amount + inv.inv_amount as chk
			from 
			(SELECT MAX(EFFECTIVE_PERIOD_NUM) GRN_PERIOD_NUM, po_order_number GRN_PO_NUMBER
				 --, rcv_receipt_num GRN_RECEIPT_NUMBER
				 , SUM(NVL(LINE_ACCOUNTED_CR,0)) - SUM(NVL(LINE_ACCOUNTED_DR,0)) GRN_AMOUNT       
			FROM TAC_PO_ACCR_GRN_DTAC
			GROUP BY po_order_number
				   --, rcv_receipt_num
			) grn,
			(
			SELECT MAX(EFFECTIVE_PERIOD_NUM) INV_PERIOD_NUM, po_order_number INV_PO_NUMBER
				 --, rcv_receipt_num INV_RECEIPT_NUMBER
				 , SUM(NVL(LINE_ACCOUNTED_CR,0)) - SUM(NVL(LINE_ACCOUNTED_DR,0)) INV_AMOUNT
			FROM TAC_PO_ACCR_INVOICE_DTAC
			GROUP BY po_order_number
				   --, rcv_receipt_num;
			) inv
			where grn.GRN_PO_NUMBER = inv.INV_PO_NUMBER
			and (grn.grn_amount + inv.inv_amount) <> 0
		);

        insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('T2_TAC_PO_ACCR_REPORT_DTAC','End update case2',sysdate); commit;
        
        insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('T2_TAC_PO_ACCR_REPORT_DTAC','Begin update case3',sysdate); commit;
		--------------------------------------------------------------------------------
		--------------------------------------------------------------------------------
		-- CASE 3

		UPDATE T2_TAC_PO_ACCR_REPORT_DTAC 
		SET MATCHED_CASE = 3
		WHERE MATCHED_CASE = 99 AND
			  GRN_PO_NUMBER IN (
					select grn.po_order_number
					from TAC_PO_ACCR_GRN_DTAC grn
					where not exists (
					  select * from TAC_PO_ACCR_INVOICE_DTAC inv 
					  where grn.po_order_number = inv.po_order_number
			  )
		);
		
        insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('T2_TAC_PO_ACCR_REPORT_DTAC','End update case3',sysdate); commit;
        
        insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('T2_TAC_PO_ACCR_REPORT_DTAC','Begin update case4',sysdate); commit;		
		--------------------------------------------------------------------------------
		--------------------------------------------------------------------------------
		-- CASE 4

		UPDATE T2_TAC_PO_ACCR_REPORT_DTAC 
		SET MATCHED_CASE = 4
		WHERE MATCHED_CASE = 99 AND
			  inv_po_number IN (
								  select distinct inv.po_order_number
								  from TAC_PO_ACCR_INVOICE_DTAC inv 
								  where not exists (
									select * from TAC_PO_ACCR_GRN_DTAC grn
									where grn.po_order_number = inv.po_order_number
								)
		);
        insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('T2_TAC_PO_ACCR_REPORT_DTAC','End update case4',sysdate); commit;
        
        insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('T2_TAC_PO_ACCR_REPORT_DTAC','Begin update case22',sysdate); commit;		
		--------------------------------------------------------------------------------
		--------------------------------------------------------------------------------
		--	CASE 2 PART 2

		UPDATE T2_TAC_PO_ACCR_REPORT_DTAC 
		SET MATCHED_CASE = 2
		WHERE MATCHED_CASE = 99
		  AND inv_po_number IN (
								  select rpt2.grn_po_number
								  from T2_TAC_PO_ACCR_REPORT_DTAC rpt2              
		);
		
        insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('T2_TAC_PO_ACCR_REPORT_DTAC','End update case22',sysdate); commit;
        
        insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('T2_TAC_PO_ACCR_REPORT_DTAC','Begin update case5',sysdate); commit;		
		--------------------------------------------------------------------------------
		--------------------------------------------------------------------------------
		-- CASE 5
		UPDATE T2_TAC_PO_ACCR_REPORT_DTAC 
		SET MATCHED_CASE = 5
		WHERE MATCHED_CASE = 4
		  and INV_RECEIPT_NUMBER is not null
		;
		
		COMMIT;
		insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('T2_TAC_PO_ACCR_REPORT_DTAC','End update case5',sysdate); commit;
	EXCEPTION
	  WHEN others THEN
		DBMS_OUTPUT.PUT_LINE('An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
		rollback;
	END;
	--GEN_REPORT_DTAC END



	
	---  purge_table dtac
    Procedure purge_table_dtac is
    
    begin	
	
    insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('purge_table dtac','Begin',sysdate); commit;
        delete from TAC_PO_ACCR_GRN_DTAC;commit;
        delete from TAC_PO_ACCR_INVOICE_DTAC;commit;
        delete from TAC_PO_ACCR_OTHERS_DTAC;commit;
        delete from T2_TAC_PO_ACCR_REPORT_DTAC; commit;
    insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('purge_table dtac','End',sysdate); commit;
    end;
    ---  purge_table 
	
   ---------------------------------
    ---  update PO status
    Procedure update_po_status is
    
    begin
    insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('T2_TAC_PO_ACCR_REPORT_DTAC','Begin update po_status',sysdate); commit;
       UPDATE T2_TAC_PO_ACCR_REPORT_DTAC a
		SET a.PO_STATUS =  (select poh.authorization_status po_status from PO_HEADERS_ALL POH 
                           where poh.org_id = 102  -- Dtac OU
                             and poh.segment1 = a.grn_po_number)
		WHERE  a.PO_STATUS IS NULL;
       COMMIT;
       	insert into T2_TAC_PO_ACCR_REPORT_DTAC_LOG (att1,att2,cur_date) values('T2_TAC_PO_ACCR_REPORT_DTAC','End update po_status',sysdate); commit;
    end;
    ---  update PO status
    
----------------------------------------------------------------	
--MAIN PACKAGE--
BEGIN
	-- 1. Purge Log (One time) ****
	delete from T2_TAC_PO_ACCR_REPORT_DTAC_LOG; commit;	
	
    ---- adding dtac by phinphimon 21 Aug 2019
	---- For dtac only
	 -- 2. Purge data
       purge_table_dtac;
	 -- 3. Generated data  
     DBMS_OUTPUT.PUT_LINE('Invoke Dtac_PREP_DATA_A() ');
	   DTAC_PREP_DATA_A;
     DBMS_OUTPUT.PUT_LINE('Invoke Dtac_PREP_DATA_B() ');
       DTAC_PREP_DATA_B; 
      DBMS_OUTPUT.PUT_LINE('Invoke Dtac_PREP_DATA_C() ');
      DTAC_PREP_DATA_C;  
    -- 4. Generate report
   --  DBMS_OUTPUT.PUT_LINE('Invoke GEN_REPORT_DTAC() ');
	   GEN_REPORT_DTAC(); 
    -- 5. Update PO status
      update_po_status;
	
END;