select att1,att2, to_char(cur_date,'dd-mon-yyyy hh24:mi:ss') dt from T2_TAC_PO_ACCR_REPORT_DTAC_LOG 
where att1 like 'T%' and att2 in ('begin','End','Begin','end')
--group by att1,att2,to_char(cur_date,'dd-mon-yyyy hh24:mi:ss')
order by 1
--- Dtac
select count(*) from TAC_PO_ACCR_GRN_DTAC;

select count(*) from TAC_PO_ACCR_INVOICE_DTAC;

select count(*) from TAC_PO_ACCR_OTHERS_DTAC;
---

select count(*) from T2_TAC_PO_ACCR_REPORT_DTAC
-- 580931
select * from T2_TAC_PO_ACCR_REPORT_DTAC

------ DTN
-- 30313
select *  from T2_TAC_PO_ACCR_REPORT_DTN where po_status is not null 
and po_status = 'REQUIRES REAPPROVAL'
IN PROCESS

select count(*) from TAC_PO_ACCR_GRN_DTN;

select count(*) from TAC_PO_ACCR_INVOICE_DTN;

select count(*) from TAC_PO_ACCR_OTHERS_DTN;
-----

select effective_period_num , count(*) cnt from TAC_PO_ACCR_GRN_DTAC_B group by effective_period_num order by effective_period_num desc;

select effective_period_num , count(*) cnt from TAC_PO_ACCR_GRN_DTAC group by effective_period_num order by effective_period_num desc;

--
select effective_period_num , count(*) cnt from TAC_PO_ACCR_INVOICE_DTAC_B group by effective_period_num order by effective_period_num desc;

select effective_period_num , count(*) cnt from TAC_PO_ACCR_INVOICE_DTAC group by effective_period_num order by effective_period_num desc;
--

select gl_period , count(*) cnt from TAC_PO_ACCR_OTHERS_DTAC_B group by gl_period order by gl_period desc;

select gl_period , count(*) cnt from TAC_PO_ACCR_OTHERS_DTAC group by gl_period order by gl_period desc;

select * from TAC_PO_ACCR_OTHERS_DTAC_B 

--------------------------
--3--
-- SHEET NAME : "DTAC Summary by Period"

SELECT grn.EFFECTIVE_PERIOD_NUM, grn.GRN_AMOUNT, inv.INV_AMOUNT, grn.GRN_AMOUNT + inv.INV_AMOUNT  AS "All source without Manual", OTH.MANUAL_AMOUNT,
       (grn.GRN_AMOUNT + inv.INV_AMOUNT + OTH.MANUAL_AMOUNT) AS "All source"
from (
    SELECT EFFECTIVE_PERIOD_NUM, SUM(NVL(LINE_ACCOUNTED_CR,0)) - SUM(NVL(LINE_ACCOUNTED_DR,0)) GRN_AMOUNT
    FROM TAC_PO_ACCR_GRN_DTAC_B
    GROUP BY EFFECTIVE_PERIOD_NUM
) GRN full outer join
(
    SELECT EFFECTIVE_PERIOD_NUM, SUM(NVL(LINE_ACCOUNTED_CR,0)) - SUM(NVL(LINE_ACCOUNTED_DR,0)) INV_AMOUNT
    FROM TAC_PO_ACCR_INVOICE_DTAC_B
    GROUP BY EFFECTIVE_PERIOD_NUM
) INV 
ON GRN.EFFECTIVE_PERIOD_NUM = INV.EFFECTIVE_PERIOD_NUM
full outer join
(
    SELECT GL_PERIOD, SUM(NVL(LINE_ACCOUNTED_CR,0)) - SUM(NVL(LINE_ACCOUNTED_DR,0)) MANUAL_AMOUNT
    FROM TAC_PO_ACCR_OTHERS_DTAC_B
    GROUP BY GL_PERIOD
) oth
ON GRN.EFFECTIVE_PERIOD_NUM = oth.GL_PERIOD
ORDER BY 1;
-----------------------------------------------------------------------------------------------------------------------------------------------------------

--4--
--	SHEET NAME : "DTAC Accrual"

SELECT * 
FROM T2_TAC_PO_ACCR_REPORT_DTAC_B
ORDER BY 2,3,4;
-----------------------------------------------------------------------------------------------------------------------------------------------------------
--END--



