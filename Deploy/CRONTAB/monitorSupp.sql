@/home/oraprod/SCRIPT/MONITOR_ERP/connsql.sql

DECLARE
	-- Date : 23-Nov-2011
	-- Author : Aphichart Phophong
	/*--
		-- Add column error_message to each sub-query and outter query
	--*/
	
	-- Date : 23-Nov-2011
	-- Author : Aphichart Phophong
	/*--
		-- Remark old cursor query.
		-- Modify cursor cur_st by union sub-query for inquiry invalid vat registeration number
		from interface table
		-- Change date condition of each sub-query
			from "and to_date(a.eai_crtd_dttm, 'DD/MM/YYYY') between
                to_date(sysdate, 'DD/MM/YYYY') and
                to_date(sysdate + 1, 'DD/MM/YYYY')"
			to "and a.eai_crtd_dttm between trunc(sysdate,'dd') and trunc(sysdate,'dd')+1"
		-- Modify text output column header by added "ERROR MESSAGE'
	--*/
  CURSOR cur_st IS
      SELECT (x.attribute1 || '|' || x.vendor_number || '|' ||
           x.vendor_site_code || '|' || x.vendor_name || '|' ||
           x.VAT_REGISTRATION_NUM || '|' || x.status || '|' ||
           to_char(x.eai_crtd_dttm, 'dd/mm/yyyy') || '|' ||
           x.VendorNameERP || '|' || --Modify by Supranee.t 10-03-2008
           x.TAXIDERP || '|' || -- Modify by AP@IS-ES on 04-Nov-2011
		   x.error_message -- Modify by AP@IS-ES on 04-Nov-2011
		   ) v_output --Modify by Supranee.t 27-May-08
    from (
    select distinct a.attribute1
                         ,a.vendor_number
                         ,a.vendor_site_code
                         ,a.vendor_name
                         ,a.VAT_REGISTRATION_NUM
                         ,a.status
                         ,eai_crtd_dttm
                         ,b.vendor_name VendorNameERP
                         , --Modify by Supranee.t 10-03-2008
                          b.vat_registration_num TAXIDERP --Modify by Supranee.t 27-May-08
                         ,a.error_message -- Modify by AP@IS-ES on 04-Nov-2011
          from dtac_ap_suppliers_temp_int a, po_vendors b
          where a.status <> 'PROCESSED'
          and a.vendor_number = b.segment1(+)
          -- Modify by AP@IS-ES on 23-Nov-2011
          and a.eai_crtd_dttm between trunc(sysdate,'dd') and trunc(sysdate,'dd')+1
          /* -- remark by AP@IS-ES on 23-Nov-2011
          and to_date(a.eai_crtd_dttm, 'DD/MM/YYYY') between
                to_date(sysdate, 'DD/MM/YYYY') and
                to_date(sysdate + 1, 'DD/MM/YYYY')*/
          and a.vendor_number is NOT NULL
          
          union
          -- Vat duplication with another vendor
          select distinct a.attribute1
                         ,a.vendor_number
                         ,a.vendor_site_code
                         ,a.vendor_name
                         ,a.VAT_REGISTRATION_NUM
                         ,a.status
                         ,eai_crtd_dttm
                         ,b.vendor_name VenderNameERP
                         , --Modify by Supranee.t 10-03-2008
                          b.vat_registration_num TAXIDERP --Modify by Supranee.t 27-May-08
                         ,a.error_message -- Modify by AP@IS-ES on 04-Nov-2011
          from dtac_ap_suppliers_temp_int a, po_vendors b
          where a.status <> 'PROCESSED'
          and a.VAT_REGISTRATION_NUM = b.vat_registration_num
          -- Modify by AP@IS-ES on 23-Nov-2011
          and a.eai_crtd_dttm between trunc(sysdate,'dd') and trunc(sysdate,'dd')+1
          /* -- remark by AP@IS-ES on 23-Nov-2011
          and to_date(a.eai_crtd_dttm, 'DD/MM/YYYY') between
                to_date(sysdate, 'DD/MM/YYYY') and
                to_date(sysdate + 1, 'DD/MM/YYYY')*/
          and a.vendor_number is NULL
          
          
          union --Modify by Supranee.t 27-May-08
          
          select distinct a.attribute1
                         ,a.vendor_number
                         ,a.vendor_site_code
                         ,a.vendor_name
                         ,a.VAT_REGISTRATION_NUM
                         ,a.status
                         ,eai_crtd_dttm
                         ,b.vendor_name VendorNameERP
                         ,b.vat_registration_num TAXIDERP
                         ,a.error_message -- Modify by AP@IS-ES on 04-Nov-2011
          from dtac_ap_suppliers_temp_int a, po_vendors b
          where a.status <> 'PROCESSED'
          and a.vendor_name = b.vendor_name
          -- Modify by AP@IS-ES on 23-Nov-2011
          and a.eai_crtd_dttm between trunc(sysdate,'dd') and trunc(sysdate,'dd')+1
          /* -- remark by AP@IS-ES on 23-Nov-2011
          and to_date(a.eai_crtd_dttm, 'DD/MM/YYYY') between
                to_date(sysdate, 'DD/MM/YYYY') and
                to_date(sysdate + 1, 'DD/MM/YYYY')*/
          and a.vendor_number is Null
          
          -- Modify by AP@IS-ES on 23-Nov-2011
          union
          -- invalid vat_registration_num
          select distinct a.attribute1
                         ,a.vendor_number
                         ,a.vendor_site_code
                         ,a.vendor_name
                         ,a.vat_registration_num
                         ,a.status
                         ,eai_crtd_dttm
                         ,null vendornameerp
                         ,null taxiderp
                         ,a.error_message
          from dtac_ap_suppliers_temp_int a
          where a.status <> 'PROCESSED'
          and a.eai_crtd_dttm between trunc(sysdate,'dd') and trunc(sysdate,'dd')+1
          and tac_validate_data.validate_supplier_tax(a.vat_registration_num) = 0
	) x;
	
  
  
  /*      SELECT  distinct (a.attribute1 || '|' || a.vendor_number || '|' || a.vendor_site_code || '|' || a.vendor_name || '|' || a.VAT_REGISTRATION_NUM || '|' || a.status || '|' || to_char(a.eai_crtd_dttm,'dd/mm/yyyy') || '|' || b.vendor_name || '|' || b.vat_registration_num) v_output
          FROM dtac_ap_suppliers_temp_int a, po_vendors b
          WHERE status <> 'PROCESSED'
          AND a.vendor_number = b.segment1(+)
          AND to_date(a.eai_crtd_dttm,'YYYY-MM-DD') = to_date(sysdate,'YYYY-MM-DD');
  
  --      WHERE rownum <= 50;*/
  /* -- Remark by AP@IS-ES on 23-Nov-2011
    SELECT (x.attribute1 || '|' || x.vendor_number || '|' ||
           x.vendor_site_code || '|' || x.vendor_name || '|' ||
           x.VAT_REGISTRATION_NUM || '|' || x.status || '|' ||
           to_char(x.eai_crtd_dttm, 'dd/mm/yyyy') || '|' ||
           x.VendorNameERP || '|' || --Modify by Supranee.t 10-03-2008
           x.TAXIDERP || '|' || -- Modify by AP@IS-ES on 04-Nov-2011
		   x.error_message -- <==Modify by AP@IS-ES on 04-Nov-2011
		   ) v_output --Modify by Supranee.t 27-May-08
    from (select distinct a.attribute1
                         ,a.vendor_number
                         ,a.vendor_site_code
                         ,a.vendor_name
                         ,a.VAT_REGISTRATION_NUM
                         ,a.status
                         ,eai_crtd_dttm
                         ,b.vendor_name VendorNameERP
                         , --Modify by Supranee.t 10-03-2008
                          b.vat_registration_num TAXIDERP --Modify by Supranee.t 27-May-08
                         ,a.error_message -- <==Modify by AP@IS-ES on 04-Nov-2011
          from dtac_ap_suppliers_temp_int a, po_vendors b
          where a.status <> 'PROCESSED'
          and a.vendor_number = b.segment1(+)
          and to_date(a.eai_crtd_dttm, 'DD/MM/YYYY') between
                to_date(sysdate, 'DD/MM/YYYY') and
                to_date(sysdate + 1, 'DD/MM/YYYY')
               --and a.eai_crtd_dttm between to_date('06/02/2008','DD/MM/YYYY') and to_date('07/02/2008','DD/MM/YYYY')
          and a.vendor_number is NOT NULL
          union
          select distinct a.attribute1
                         ,a.vendor_number
                         ,a.vendor_site_code
                         ,a.vendor_name
                         ,a.VAT_REGISTRATION_NUM
                         ,a.status
                         ,eai_crtd_dttm
                         ,b.vendor_name VenderNameERP
                         , --Modify by Supranee.t 10-03-2008
                          b.vat_registration_num TAXIDERP --Modify by Supranee.t 27-May-08
                         ,a.error_message -- <==Modify by AP@IS-ES on 04-Nov-2011
          from dtac_ap_suppliers_temp_int a, po_vendors b
          where a.status <> 'PROCESSED'
          and a.VAT_REGISTRATION_NUM = b.vat_registration_num
          and to_date(a.eai_crtd_dttm, 'DD/MM/YYYY') between
                to_date(sysdate, 'DD/MM/YYYY') and
                to_date(sysdate + 1, 'DD/MM/YYYY')
               --and a.eai_crtd_dttm between to_date('06/02/2008','DD/MM/YYYY') and to_date('07/02/2008','DD/MM/YYYY')
          and a.vendor_number is NULL
          --and b.vat_registration_num <> '1111111111111' Modify by Supranee 25-FEB-2008
          union --Modify by Supranee.t 27-May-08
          select distinct a.attribute1
                         ,a.vendor_number
                         ,a.vendor_site_code
                         ,a.vendor_name
                         ,a.VAT_REGISTRATION_NUM
                         ,a.status
                         ,eai_crtd_dttm
                         ,b.vendor_name VendorNameERP
                         ,b.vat_registration_num TAXIDERP
                         ,a.error_message -- <==Modify by AP@IS-ES on 04-Nov-2011
          from dtac_ap_suppliers_temp_int a, po_vendors b
          where a.status <> 'PROCESSED'
          and a.vendor_name = b.vendor_name
          and to_date(a.eai_crtd_dttm, 'DD/MM/YYYY') between
                to_date(sysdate, 'DD/MM/YYYY') and
                to_date(sysdate + 1, 'DD/MM/YYYY')
               --and a.eai_crtd_dttm between to_date('27/03/2008','DD/MM/YYYY') and to_date('28/03/2008','DD/MM/YYYY')
          and a.vendor_number is Null
	) x;
	*/
	
  cur_fn cur_st%rowtype;

  v_log_filename utl_file.file_type;
  v_log_path     VARCHAR2(200) := '/usr/tmp';
  v_filename     VARCHAR2(200) := '&1';

BEGIN

  BEGIN
    --- Write File Complete ---
    v_log_filename := utl_file.fopen(v_log_path, v_filename, 'A');
  
  END;

  --utl_file.put_line(v_log_filename,'vendor_number|' || 'vendor_name|' || 'attribute1|' || 'vendor_site_code|' || 'eai_crtd_dttm|' || 'status');
  --utl_file.put_line(v_log_filename,'Retailer Code|' || 'vender_number|' || 'vendor_site_code|' || 'a.vendor_name|' || 'a.vat_registration_num|' || 'status|' || 'eai_crtd_dttm|' || 'b.vendor_name|' || 'b.vat_registration_num');

  utl_file.put_line(v_log_filename,
                    'Retailer Code' || '|' || 'VENDOR_NUMBER' || '|' ||
                    'VENDOR_SITE_CODE' || '|' || 'VENDOR_NAME' || '|' ||
                    'VAT_REGISTRATION_NUM' || '|' || 'STATUS' || '|' ||
                    'EAI_CRTD_DTTM' || '|' || 'Vendor Name ERP' || '|' ||
                    'TAX ID ERP' || '|' ||
					'ERROR MESSAGE'); -- Modigy by AP@IS-ES on 23-Nov-2011

  OPEN cur_st;
  LOOP
    FETCH cur_st
      into cur_fn;
    EXIT WHEN cur_st%notfound;
  
    utl_file.put_line(v_log_filename, cur_fn.v_output);
  
  END LOOP;
  CLOSE cur_st;

  utl_file.fclose(v_log_filename);

END;
/
EXIT;
