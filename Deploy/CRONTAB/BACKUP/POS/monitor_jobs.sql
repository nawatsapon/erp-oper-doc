@/usr/home/applprod/SCRIPT/MONITOR_POS/connsql.sql
	set heading off;
	set linesize 2000;

	------------1:  JOB BROKEN -----------------
	spool &&1
		SELECT BROKEN  
    || '|' || JOB
    || '|' || LOG_USER
    || '|' || 'DBMS_JOB.BROKEN(' || JOB || ',false);' 
    || '|' || WHAT 
    FROM DBA_JOBS
    WHERE BROKEN='Y'
    AND JOB <> 337;
	spool off;

	------------2:  JOB FINE ------------
	spool &&2
		SELECT BROKEN  
    || '|' || JOB
    || '|' || LOG_USER
    || '|' || WHAT 
    FROM DBA_JOBS
    WHERE BROKEN='N';
	spool off;
exit;

