#!/bin/ksh
#export ORACLE_HOME=/posdb/prod/oracle/product/9.2.0
#export ORACLE_SID=posprod	
#export TNS_ADMIN=/posdb/prod/oracle/product/9.2.0/network/admin
#export NLS_LANG=American_America.TH8TISASCII
#export PATH=$PATH:/posdb/prod/oracle/product/9.2.0/bin:/usr/sbin:/usr/bin:/etc:/usr/local/bin

. /usr/home/applprod/.profile

##############################################################################
CHK_DT=`echo $(date '+20%y%m%d')`
DTM=`echo $(date '+%d/%m/20%y %H:%M')`

SC_PATH="/usr/home/applprod/SCRIPT/MONITOR_POS"
LOG_PATH="${SC_PATH}/LOG"

SC_NAME="monitorGL"
SQL_NAME="${SC_NAME}.sql"

LOGSC="${SC_NAME}.log"
LOGNAME="${SC_NAME}_`echo $(date '+20%y%m%d_%H%M')`.log"
MAILDETAIL="${SC_NAME}_`echo $(date '+20%y%m%d_%H%M')`.tmp"


########## Function _mail()  Start process send E-mail    ###############
function send_mail
{
	ERPTEAM="ostanonp@dtac.co.th"
	SODTEAM="ERPOperationSupport@tac.co.th"
	LISTMAIL="${ERPTEAM}, ${SODTEAM},tantida.yodhasmutr@dtac.co.th"
	#LISTMAIL="tantida.yodhasmutr@dtac.co.th"
	
	mailx -s "${SUBJECT}" ${LISTMAIL} < ${LOG_PATH}/${MAILDETAIL}
	rm ${LOG_PATH}/${MAILDETAIL}
}

##############################################
sqlplus /nolog @${SC_PATH}/${SQL_NAME}  ${LOG_PATH}/${LOGNAME}

chmod 666 ${LOG_PATH}/${LOGNAME}
#chown -h posprod:posprod ${LOG_PATH}/${LOGNAME}

##### Check Log file #####
cd ${LOG_PATH}

#CHK_RECORD=`grep 'rows selected' ${LOG_PATH}/${LOGNAME} |awk '{print $1}' `

CHK_ALL=`wc -l ${LOGNAME} |awk '{print $1}'`
CHK_RECORD=`expr ${CHK_ALL} - 2`

#if [ "${CHK_RECORD}" = "no" ]; then
	if [ ${CHK_RECORD}  -eq 0 ] ; then
	SUBJECT="Inform Monitor POS : Check GL Interface ${DTM}"

	echo -e "Dear POS Helpdesk Support\n" >> ${LOG_PATH}/${MAILDETAIL}
	echo -e "\tInform monitor not have status not 'NEW' of ${DTM}" >> ${LOG_PATH}/${MAILDETAIL}
	echo -e "\n\nBest Regard,\nERP Operation Support\nEmail: SOD-EA-ERPBOSDept@tac.co.th" >> ${LOG_PATH}/${MAILDETAIL}

	rm ${LOGNAME}
	send_mail		## Call function send_mail
	echo -e "[$(date '+%d/%m/20%y %H:%M')] Not have status not 'NEW'" >> ${LOGSC}
	
else	
	SUBJECT="Inform Monitor POS : Check GL Interface ${DTM}"

	echo -e "Dear POS Helpdesk Support\n" >> ${LOG_PATH}/${MAILDETAIL}
	echo -e "\tThis output of monitor GL Interface have status not 'NEW' of ${DTM} = ${CHK_RECORD} records." >> ${LOG_PATH}/${MAILDETAIL}
	echo -e "Please see detail your below.\n" >> ${LOG_PATH}/${MAILDETAIL}
	
	echo "STATUS|BATCH_NAME|JOURNAL_ENTRY_NAME|STATUS_DESCRIPTION" >> ${LOG_PATH}/${MAILDETAIL}
	grep -v "rows selected" ${LOG_PATH}/${LOGNAME} >> ${LOG_PATH}/${MAILDETAIL}
	echo -e "\n\nBest Regard,\nERP Operation Support\nEmail: SOD-EA-ERPBOSDept@tac.co.th" >> ${LOG_PATH}/${MAILDETAIL}

	send_mail		## Call function send_mail
	echo -e "[$(date '+%d/%m/20%y %H:%M')] GL Interface have status not 'NEW' = ${CHK_RECORD} records." >> ${LOGSC}
fi
