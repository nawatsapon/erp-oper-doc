#!/bin/ksh
#export ORACLE_HOME=/app/oracle/erpprod/11.2.0
#export ORACLE_SID=PROD
#export TNS_ADMIN=/app/oracle/erpprod/11.2.0/network/admin/PROD_hydrus2
#export NLS_LANG=American_America.TH8TISASCII
#export PATH=$PATH:/app/oracle/erpprod/11.2.0/bin:/usr/sbin:/usr/bin:/etc:/usr/local/bin
#/usr/home/applprod/.profile
#ORACLE_SID=PROD;
#ORACLE_UNQNAME=PROD;
#PS1="`tput smso`[$ORACLE_SID-`hostname`]`tput rmso`"'($PWD)$ ';
#ORACLE_HOSTNAME=hydrus22;
#ORACLE_BASE=/app/oracle/db;
#DB_HOME=/app/oracle/erpdb/product/11.2.0/db_1;
#ORACLE_HOME=$DB_HOME;
#PATH=$ORACLE_HOME/bin:/bin:/usr/bin:/usr/ccs/bin:/usr/ucb:/etc:/app/oracle/grid/11.2.0/grid_1/bin:.;
#LD_LIBRARY_PATH=$ORACLE_HOME/lib:$ORACLE_HOME/sqlplus/lib:/usr/lib:/usr/ccs/lib:/usr/ucblib;
#CLASSPATH=$ORACLE_HOME/JRE:$ORACLE_HOME/jlib:$ORACLE_HOME/rdbms/jlib;
#export ORA_NLS10=/app/oracle/erpdb/product/11.2.0/db_1/nls/data/9idata;
#export ORACLE_UNQNAME ORACLE_BASE ORACLE_HOSTNAME ORACLE_SID DB_HOME ORACLE_HOME PATH LD_LIBRARY_PATH CLASSPATH TMP TMPDIR;
#umask 0002;
#

. /usr/home/applprod/.profile

CHK_DT=`echo $(date '+20%y%m%d')`
DTM=`echo $(date '+%d/%m/20%y')`

SC_PATH="/home/applprod/SCRIPT/MONITOR_ERP"
SC_NAME="monitorSupp"
LOG_PATH="${SC_PATH}/LOG"
LOGSC="${SC_NAME}.log"
LOGNAME="${SC_NAME}_`echo $(date '+20%y%m%d_%H%M')`.csv"
MAILDETAIL="mail_`echo $(date '+20%y%m%d_%H%M')`.tmp"
# AILDETAIL="mail_`echo $(date '+20%y%m%d_%H%M')`.tmp"  -- Siraprapha date 15012014
##############################################################################
# July 4, 2007  Remove mail K.Paninee and Add mail K.Cholchanis (Case : 3254)
# Aug 31, 2007  Revise send attach file
# Jan 1, 2009   Move script to /home/oraprod/SCRIPT/MONITOR_ERP


########## Function _mail()  Start process send E-mail    ###############
function send_mail
{
        SODTEAM="ERPOperationSupport@dtac.co.th"   #C134156
        MONTON="CNM_Profile@dtac.co.th"
        NUTTHAPAT="DMSOperationSupport@dtac.co.th"
        NAMPEUNG="STSOperationSupport@dtac.co.th"
		SIRAPRAPHA="Siraprapha@dtac.co.th"
        JIRAPORN="JirapornSa@dtac.co.th"

		#C134156
        LISTMAIL="${SIRAPRAPHA}, ${NUTTHAPAT}, ${MONTON}, ${SODTEAM}, ${NAMPEUNG},${JIRAPORN},tantida.yodhasmutr@dtac.co.th"	
		
		#LISTMAIL="tantida.yodhasmutr@dtac.co.th"	

        cd ${LOG_PATH}
        if [ ${CHK_RECORD}  -eq 0 ] ; then
                mailx  -s "${SUBJECT}" ${LISTMAIL} < ${LOG_PATH}/${MAILDETAIL}
        else
                mailx  -s "${SUBJECT}" -a ${LOG_PATH}/${LOGNAME} ${LISTMAIL} < ${LOG_PATH}/${MAILDETAIL}
								
        fi
        rm ${LOG_PATH}/${MAILDETAIL}
}

##############################################
echo "This Path" ${PATH}
touch /usr/tmp/${LOGNAME}
sqlplus /nolog @${SC_PATH}/monitorSupp.sql  ${LOGNAME}  ${CHK_DT}
chmod 644 /usr/tmp/${LOGNAME}
mv  /usr/tmp/${LOGNAME}  ${LOG_PATH}

chmod 644 ${LOG_PATH}/${LOGNAME}

##### Check Log file #####
cd ${LOG_PATH}

CHK_ALL=`wc -l ${LOGNAME} |awk '{print $1}'`
CHK_RECORD=`expr ${CHK_ALL} - 1`

if [ CHK_RECORD -gt 0 ]; then
        echo "[$(date '+%d/%m/20%y %H:%M')] Cannot interface supplier of ${DTM} = ${CHK_RECORD} records, send mail inform success." >> ${LOGSC}
		echo "before mail detail gt 0"
		echo "This Path" ${PATH}

        SUBJECT="Cannot interface supplier of ${DTM}"
        echo -e "Dear All\n\n\t${SUBJECT} = ${CHK_RECORD} records.\n" >> ${LOG_PATH}/${MAILDETAIL}
        
        echo -e "\n\nBest Regards,\nERP Operation Support\nEmail: ERPOperationSupport@dtac.co.th" >> ${LOG_PATH}/${MAILDETAIL}

        send_mail               ## Call function send_mail
else
        echo "[$(date '+%d/%m/20%y %H:%M')] No have interface supplier of ${DTM}, send mail inform success." >> ${LOGSC}
		echo "before mail detail else"
        SUBJECT="[Inform] No Rejected interface supplier of `echo $(date '+%d/%m/20%y')`"
        echo -e "Dear All\n\n\t${SUBJECT} \n" >> ${LOG_PATH}/${MAILDETAIL}
        echo -e "\n\nBest Regards,\nERP Operation Support\nEmail: ERPOperationSupport@dtac.co.th" >> ${LOG_PATH}/${MAILDETAIL}
        rm ${LOGNAME}

        send_mail               ## Call function send_mail
fi
