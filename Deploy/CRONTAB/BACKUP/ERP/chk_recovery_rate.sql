@/usr/home/applprod/SCRIPT/MONITOR_ERP/connsql.sql

DECLARE
	CURSOR cur_st IS
	
		SELECT (A.segment1) v_output
		FROM PO_Headers A, PO_LINE_LOCATIONS B, PO_DISTRIBUTIONS_V C  
		WHERE A.PO_Header_ID = B.PO_Header_ID  
		AND A.PO_Header_ID = C.PO_Header_ID  
		AND B.tax_Code_ID is not null  
		AND (C.Recovery_Rate is null or C.Recovery_Rate < 100);

	cur_fn cur_st%rowtype;

	v_log_filename utl_file.file_type;
	v_log_path VARCHAR2(200) := '/usr/tmp';
	v_filename VARCHAR2(200) := '&1';

BEGIN

	BEGIN --- Write File Complete ---
		v_log_filename := utl_file.fopen(v_log_path, v_filename, 'W');
	END;

	utl_file.put_line(v_log_filename,'PO_Number');

	OPEN cur_st;
	LOOP
	FETCH cur_st into cur_fn;
	EXIT WHEN cur_st%notfound;

		utl_file.put_line(v_log_filename, cur_fn.v_output);

	END LOOP;
	CLOSE cur_st;

	utl_file.fclose(v_log_filename);

END;
/
EXIT;
