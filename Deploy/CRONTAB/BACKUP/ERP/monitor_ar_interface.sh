#!/bin/ksh
#export ORACLE_HOME=/app/oracle/erpprod/11.2.0
#export ORACLE_SID=PROD
#export TNS_ADMIN=/app/oracle/erpprod/11.2.0/network/admin/PROD_hydrus2
#export NLS_LANG=American_America.TH8TISASCII
#export PATH=$PATH:/app/oracle/erpprod/11.2.0/bin:/usr/sbin:/usr/bin:/etc:/usr/local/bin
#ORACLE_SID=PROD;
#ORACLE_UNQNAME=PROD;
#PS1="`tput smso`[$ORACLE_SID-`hostname`]`tput rmso`"'($PWD)$ ';
#ORACLE_HOSTNAME=hydrus22;
#ORACLE_BASE=/app/oracle/db;
#DB_HOME=/app/oracle/erpdb/product/11.2.0/db_1;
#ORACLE_HOME=$DB_HOME;
#PATH=$ORACLE_HOME/bin:/bin:/usr/bin:/usr/ccs/bin:/usr/ucb:/etc:/app/oracle/grid/11.2.0/grid_1/bin:.;
#LD_LIBRARY_PATH=$ORACLE_HOME/lib:$ORACLE_HOME/sqlplus/lib:/usr/lib:/usr/ccs/lib:/usr/ucblib;
#CLASSPATH=$ORACLE_HOME/JRE:$ORACLE_HOME/jlib:$ORACLE_HOME/rdbms/jlib;
#export ORA_NLS10=/app/oracle/erpdb/product/11.2.0/db_1/nls/data/9idata;
#export ORACLE_UNQNAME ORACLE_BASE ORACLE_HOSTNAME ORACLE_SID DB_HOME ORACLE_HOME PATH LD_LIBRARY_PATH CLASSPATH TMP TMPDIR;
#umask 0002;

. /usr/home/applprod/.profile

##############################################################################
CHK_DT=`echo $(date '+20%y%m%d')`
DTM=`echo $(date '+%d/%m/20%y')`

SC_PATH="/home/applprod/SCRIPT/MONITOR_ERP"
SC_NAME="monitor_ar_interface"
LOG_PATH="${SC_PATH}/LOG"
LOGSC="${SC_NAME}.log"
LOGNAME="${SC_NAME}_`echo $(date '+20%y%m%d_%H%M')`.csv"

##############################################################################
# Aug 22, 2007		Create new script for monitoring


########## Function _mail()  Start process send E-mail    ###############
function send_mail
{
	YODCHAI="Yodchai@dtac.co.th"
	ORAWAN="OrawanE@dtac.co.th"
	NUANCHAN="Nuanchan@dtac.co.th"
    PRANEE="Pranee.K@dtac.co.th"
    NOPPADOL="NoppadoA@dtac.co.th"
	TAPANUN="TapanunN@dtac.co.th" #HD0010011, WO76799,  R51-10-1290-1
	NIPADA="Nipada@dtac.co.th"
    SODTEAM="ERPOperationSupport@tac.co.th"
    PATUMWADEE="siraprapha@dtac.co.th"
	

	LISTMAIL="${YODCHAI}, ${ORAWAN}, ${NUANCHAN}, ${PRANEE}, ${NOPPADOL}, ${TAPANUN}, ${SODTEAM}, ${NIPADA}, ${PATUMWADEE},tantida.yodhasmutr@dtac.co.th"
	
	#LISTMAIL="tantida.yodhasmutr@dtac.co.th"
	
	cd ${LOG_PATH}
	if [ ${CHK_RECORD}  -eq 0 ] ; then			
		echo "Before mail - Data detail equal to 0"
		mailx  -s "${SUBJECT}" ${LISTMAIL} < ${SC_PATH}/mail.msg
	else
		echo "Before mail - Data detail more than 0"
		mailx  -s "${SUBJECT}" -a ${LOG_PATH}/${LOGNAME} ${LISTMAIL} < ${SC_PATH}/mail.msg
		#(cat ${SC_PATH}/mail.msg; uuencode ${LOGNAME} ${LOGNAME})|mailx  -s "${SUBJECT}" ${LISTMAIL}
	fi
}

##############################################
#sqlplus /nolog @${SC_PATH}/${SC_NAME}.sql  ${LOGNAME}
#mv  /usr/tmp/${LOGNAME}  ${LOG_PATH}

#chmod 644 ${LOG_PATH}/${LOGNAME}

echo "This Path" ${PATH}
touch /usr/tmp/${LOGNAME}
sqlplus /nolog @${SC_PATH}/${SC_NAME}.sql  ${LOGNAME}  ${CHK_DT}
chmod 644 /usr/tmp/${LOGNAME}
mv  /usr/tmp/${LOGNAME}  ${LOG_PATH}

chmod 644 ${LOG_PATH}/${LOGNAME}

##### Check Log file #####
cd ${LOG_PATH}
CHK_ALL=`wc -l ${LOGNAME} |awk '{print $1}'`
CHK_RECORD=`expr ${CHK_ALL} - 1`

SUBJECT="Inform Monitor AR Interface of ${DTM} = ${CHK_RECORD} records."
echo "[$(date '+%d/%m/20%y %H:%M')] AR Interface of ${DTM} = ${CHK_RECORD} records." >> ${LOGSC}
send_mail		## Call function send_mail
