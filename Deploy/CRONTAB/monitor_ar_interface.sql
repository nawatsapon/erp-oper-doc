@/home/oraprod/SCRIPT/MONITOR_ERP/connsql.sql

DECLARE
	-- Aug 29,2007	Revise delimeter from '|' to ','
  -- Feb 16,2016  Revise delimeter from ',' to '|'
	CURSOR cur_st IS
		SELECT	(INTERFACE_LINE_ID ||'|'|| INTERFACE_LINE_CONTEXT ||'|'|| INTERFACE_LINE_ATTRIBUTE1 ||'|'|| INTERFACE_LINE_ATTRIBUTE2 ||'|'||
				INTERFACE_LINE_ATTRIBUTE3 ||'|'|| BATCH_SOURCE_NAME ||'|'|| Description ||'|'|| Currency_Code ||'|'|| Amount ||'|'|| Quantity ||'|'|| Unit_Selling_Price ||'|'|| 
				TAX_CODE ||'|'|| to_char(Ship_Date_Actual,'dd/mm/yyyy hh24:mi:ss') ||'|'|| Ship_Via ||'|'|| Purchase_Order ||'|'||	Header_Attribute5 ||'|'|| to_char(Creation_date,'dd/mm/yyyy hh24:mi:ss') ||'|'|| to_char(Last_Update_Date,'dd/mm/yyyy hh24:mi:ss')) v_output
		FROM	RA_INTERFACE_LINES_ALL;

	cur_fn cur_st%rowtype;

	utl_buff			VARCHAR2(30000);
	v_log_filename		utl_file.file_type;
	v_log_path			VARCHAR2(200) := '/usr/tmp';
	v_filename			VARCHAR2(200) := '&1';

BEGIN

	BEGIN --- Write File Complete ---
		v_log_filename := utl_file.fopen(v_log_path, v_filename, 'W');
	END;

	utl_buff :=		'INTERFACE_LINE_ID' ||'|'||	
				'INTERFACE_LINE_CONTEXT' ||'|'||	
				'ORDERNO' ||'|'||	
				'ORDERTYPE' ||'|'||	
				'DELIVERNO' ||'|'||	
				'BATCH_SOURCE_NAME' ||'|'||	
				'DESCRIPTION' ||'|'||	
				'CURRENCY_CODE' ||'|'||	
				'AMOUNT' ||'|'||	
				'QUANTITY' ||'|'||	
				'UNIT_SELLING_PRICE' ||'|'||	
				'TAX_CODE' ||'|'||	
				'SHIP_DATE_ACTUAL' ||'|'||	
				'SHIP_VIA' ||'|'||	
				'PURCHASE_ORDER' ||'|'||	
				'HEADER_ATTRIBUTE5' ||'|'||	
				'CREATION_DATE' ||'|'||	
				'LAST_UPDATE_DATE';
	utl_file.put_line(v_log_filename,utl_buff);

	OPEN cur_st;
	LOOP
	FETCH cur_st into cur_fn;
	EXIT WHEN cur_st%notfound;

		utl_file.put_line(v_log_filename, cur_fn.v_output);

	END LOOP;
	CLOSE cur_st;

	utl_file.fclose(v_log_filename);

END;
/
EXIT;
