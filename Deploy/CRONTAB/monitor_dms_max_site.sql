@/home/oraprod/SCRIPT/MONITOR_ERP/connsql.sql

DECLARE
	-- Date : 23-Nov-2011
	-- Author : Aphichart Phophong
	/*--
		-- Add column error_message to each sub-query and outter query
	--*/
	
	-- Date : 23-Nov-2011
	-- Author : Aphichart Phophong
	/*--
		-- Remark old cursor query.
		-- Modify cursor cur_st by union sub-query for inquiry invalid vat registeration number
		from interface table
		-- Change date condition of each sub-query
			from "and to_date(a.eai_crtd_dttm, 'DD/MM/YYYY') between
                to_date(sysdate, 'DD/MM/YYYY') and
                to_date(sysdate + 1, 'DD/MM/YYYY')"
			to "and a.eai_crtd_dttm between trunc(sysdate,'dd') and trunc(sysdate,'dd')+1"
		-- Modify text output column header by added "ERROR MESSAGE'
	--*/
  CURSOR cur_st IS
    SELECT DISTINCT (X.ATTRIBUTE1
  || '|'
  || X.VENDOR_SITE_CODE
  || '|'
  ||
  CASE WHEN SUPP_STATUS = 'INACTIVE' THEN 'INACTIVE'
  ELSE  
      CASE
        WHEN Y.ATTRIBUTE1 IS NULL
        THEN NVL(X.MAX_SITE,'-')
        ELSE Y.SEGMENT1
      END 
  END
  ) V_OUTPUT
FROM
  ( SELECT DISTINCT A.ATTRIBUTE1 ,
    A.VENDOR_NUMBER ,
    A.VENDOR_SITE_CODE ,
    A.VENDOR_NAME ,
    A.VAT_REGISTRATION_NUM ,
    A.STATUS ,
    EAI_CRTD_DTTM ,
    B.VENDOR_NAME VENDORNAMEERP ,   --Modify by Supranee.t 10-03-2008
    B.VAT_REGISTRATION_NUM TAXIDERP --Modify by Supranee.t 27-May-08
    ,
    A.ERROR_MESSAGE -- Modify by AP@IS-ES on 04-Nov-2011
    ,
    M_SITE.MAX_SITE
  FROM
    (SELECT *
    FROM DTAC_AP_SUPPLIERS_TEMP_INT
    WHERE VAT_REGISTRATION_NUM NOT IN ('1111111111111', '0000000000000')
    ) A,
    PO_VENDORS B,
    (SELECT VENDOR_NO,
      VAT_REGISTRATION_NUM,
      VENDOR_NAME,
      MAX_SITE
    FROM
      (SELECT ROW_NUMBER() OVER(PARTITION BY VENDOR_ID ORDER BY SITE_NO DESC) ROW_NO,
        PVS.*
      FROM
        (SELECT PO_VENDORS.VENDOR_ID,
          TO_NUMBER(REGEXP_SUBSTR(VENDOR_SITE_CODE, '[0-9]+')) AS SITE_NO,
          PO_VENDORS.SEGMENT1 VENDOR_NO,
          PO_VENDORS.VAT_REGISTRATION_NUM,
          PO_VENDORS.VENDOR_NAME,
          VENDOR_SITE_CODE MAX_SITE
        FROM PO_VENDOR_SITES_ALL,
          PO_VENDORS
        WHERE 0                           =0
        AND PO_VENDOR_SITES_ALL.VENDOR_ID = PO_VENDORS.VENDOR_ID
        AND ORG_ID                        = 102
        AND REGEXP_LIKE (VENDOR_SITE_CODE, '(^HO[123456789])')
        AND (PO_VENDOR_SITES_ALL.INACTIVE_DATE     IS NULL
        OR TRUNC(PO_VENDOR_SITES_ALL.INACTIVE_DATE) > SYSDATE)
        ) PVS
      ORDER BY VENDOR_ID
      )
    WHERE ROW_NO =1
    ) M_SITE
  WHERE A.STATUS     <> 'PROCESSED'
  AND A.VENDOR_NUMBER = B.SEGMENT1(+)
  AND A.VENDOR_NUMBER = M_SITE.VENDOR_NO(+)
    -- Modify by AP@IS-ES on 23-Nov-2011
  AND A.EAI_CRTD_DTTM BETWEEN TRUNC(SYSDATE,'dd') AND TRUNC(SYSDATE,'dd')+1
    /* -- remark by AP@IS-ES on 23-Nov-2011
    and to_date(a.eai_crtd_dttm, 'DD/MM/YYYY') between
    to_date(sysdate, 'DD/MM/YYYY') and
    to_date(sysdate + 1, 'DD/MM/YYYY')*/
  AND A.VENDOR_NUMBER IS NOT NULL
  UNION
  -- Vat duplication with another vendor
  SELECT DISTINCT A.ATTRIBUTE1 ,
    A.VENDOR_NUMBER ,
    A.VENDOR_SITE_CODE ,
    A.VENDOR_NAME ,
    A.VAT_REGISTRATION_NUM ,
    A.STATUS ,
    EAI_CRTD_DTTM ,
    B.VENDOR_NAME VENDERNAMEERP ,   --Modify by Supranee.t 10-03-2008
    B.VAT_REGISTRATION_NUM TAXIDERP --Modify by Supranee.t 27-May-08
    ,
    A.ERROR_MESSAGE -- Modify by AP@IS-ES on 04-Nov-2011
    ,
    M_SITE.MAX_SITE
  FROM
    (SELECT *
    FROM DTAC_AP_SUPPLIERS_TEMP_INT
    WHERE VAT_REGISTRATION_NUM NOT IN ('1111111111111', '0000000000000')
    ) A,
    PO_VENDORS B,
    (SELECT VENDOR_NO,
      VAT_REGISTRATION_NUM,
      VENDOR_NAME,
      MAX_SITE
    FROM
      (SELECT ROW_NUMBER() OVER(PARTITION BY VENDOR_ID ORDER BY SITE_NO DESC) ROW_NO,
        PVS.*
      FROM
        (SELECT PO_VENDORS.VENDOR_ID,
          TO_NUMBER(REGEXP_SUBSTR(VENDOR_SITE_CODE, '[0-9]+')) AS SITE_NO,
          PO_VENDORS.SEGMENT1 VENDOR_NO,
          PO_VENDORS.VAT_REGISTRATION_NUM,
          PO_VENDORS.VENDOR_NAME,
          VENDOR_SITE_CODE MAX_SITE
        FROM PO_VENDOR_SITES_ALL,
          PO_VENDORS
        WHERE 0                           =0
        AND PO_VENDOR_SITES_ALL.VENDOR_ID = PO_VENDORS.VENDOR_ID
        AND ORG_ID                        = 102
        AND REGEXP_LIKE (VENDOR_SITE_CODE, '(^HO[123456789])')
        AND (PO_VENDOR_SITES_ALL.INACTIVE_DATE     IS NULL
        OR TRUNC(PO_VENDOR_SITES_ALL.INACTIVE_DATE) > SYSDATE)
        ) PVS
      ORDER BY VENDOR_ID
      )
    WHERE ROW_NO =1
    ) M_SITE
  WHERE A.STATUS            <> 'PROCESSED'
  AND A.VAT_REGISTRATION_NUM = B.VAT_REGISTRATION_NUM
  AND A.VAT_REGISTRATION_NUM = M_SITE.VAT_REGISTRATION_NUM(+)
    -- Modify by AP@IS-ES on 23-Nov-2011
  AND A.EAI_CRTD_DTTM BETWEEN TRUNC(SYSDATE,'dd') AND TRUNC(SYSDATE,'dd')+1
    /* -- remark by AP@IS-ES on 23-Nov-2011
    and to_date(a.eai_crtd_dttm, 'DD/MM/YYYY') between
    to_date(sysdate, 'DD/MM/YYYY') and
    to_date(sysdate + 1, 'DD/MM/YYYY')*/
  AND A.VENDOR_NUMBER IS NULL
  UNION --Modify by Supranee.t 27-May-08
  SELECT DISTINCT A.ATTRIBUTE1 ,
    A.VENDOR_NUMBER ,
    A.VENDOR_SITE_CODE ,
    A.VENDOR_NAME ,
    A.VAT_REGISTRATION_NUM ,
    A.STATUS ,
    EAI_CRTD_DTTM ,
    B.VENDOR_NAME VENDORNAMEERP ,
    B.VAT_REGISTRATION_NUM TAXIDERP ,
    A.ERROR_MESSAGE -- Modify by AP@IS-ES on 04-Nov-2011
    ,
    M_SITE.MAX_SITE
  FROM
    (SELECT *
    FROM DTAC_AP_SUPPLIERS_TEMP_INT
    WHERE VAT_REGISTRATION_NUM NOT IN ('1111111111111', '0000000000000')
    ) A,
    PO_VENDORS B,
    (SELECT VENDOR_NO,
      VAT_REGISTRATION_NUM,
      VENDOR_NAME,
      MAX_SITE
    FROM
      (SELECT ROW_NUMBER() OVER(PARTITION BY VENDOR_ID ORDER BY SITE_NO DESC) ROW_NO,
        PVS.*
      FROM
        (SELECT PO_VENDORS.VENDOR_ID,
          TO_NUMBER(REGEXP_SUBSTR(VENDOR_SITE_CODE, '[0-9]+')) AS SITE_NO,
          PO_VENDORS.SEGMENT1 VENDOR_NO,
          PO_VENDORS.VAT_REGISTRATION_NUM,
          PO_VENDORS.VENDOR_NAME,
          VENDOR_SITE_CODE MAX_SITE
        FROM PO_VENDOR_SITES_ALL,
          PO_VENDORS
        WHERE 0                           =0
        AND PO_VENDOR_SITES_ALL.VENDOR_ID = PO_VENDORS.VENDOR_ID
        AND ORG_ID                        = 102
        AND REGEXP_LIKE (VENDOR_SITE_CODE, '(^HO[123456789])')
        AND (PO_VENDOR_SITES_ALL.INACTIVE_DATE     IS NULL
        OR TRUNC(PO_VENDOR_SITES_ALL.INACTIVE_DATE) > SYSDATE)
        ) PVS
      ORDER BY VENDOR_ID
      )
    WHERE ROW_NO =1
    ) M_SITE
  WHERE A.STATUS   <> 'PROCESSED'
  AND A.VENDOR_NAME = B.VENDOR_NAME
  AND A.VENDOR_NAME = M_SITE.VENDOR_NAME(+)
    -- Modify by AP@IS-ES on 23-Nov-2011
  AND A.EAI_CRTD_DTTM BETWEEN TRUNC(SYSDATE,'dd') AND TRUNC(SYSDATE,'dd')+1
    /* -- remark by AP@IS-ES on 23-Nov-2011
    and to_date(a.eai_crtd_dttm, 'DD/MM/YYYY') between
    to_date(sysdate, 'DD/MM/YYYY') and
    to_date(sysdate + 1, 'DD/MM/YYYY')*/
  AND A.VENDOR_NUMBER IS NULL
  -- Modify by AP@IS-ES on 23-Nov-2011
  UNION
  -- invalid vat_registration_num
  SELECT DISTINCT A.ATTRIBUTE1 ,
    A.VENDOR_NUMBER ,
    A.VENDOR_SITE_CODE ,
    A.VENDOR_NAME ,
    A.VAT_REGISTRATION_NUM ,
    A.STATUS ,
    EAI_CRTD_DTTM ,
    NULL VENDORNAMEERP ,
    NULL TAXIDERP ,
    A.ERROR_MESSAGE,
    NULL MAX_SITE
  FROM
    (SELECT *
    FROM DTAC_AP_SUPPLIERS_TEMP_INT
    WHERE VAT_REGISTRATION_NUM NOT IN ('1111111111111', '0000000000000')
    ) A
  WHERE A.STATUS <> 'PROCESSED'
  AND A.EAI_CRTD_DTTM BETWEEN TRUNC(SYSDATE,'dd') AND TRUNC(SYSDATE,'dd')+1
  AND TAC_VALIDATE_DATA.VALIDATE_SUPPLIER_TAX(A.VAT_REGISTRATION_NUM) = 0
  ) X,
  (SELECT DISTINCT PO_VENDOR_SITES_ALL.ATTRIBUTE1,
    PO_VENDORS.SEGMENT1
    , case when PO_VENDORS.END_DATE_ACTIVE > sysdate or PO_VENDORS.END_DATE_ACTIVE is null then  'ACTIVE' ELSE 'INACTIVE' END SUPP_STATUS
  FROM PO_VENDORS,
    PO_VENDOR_SITES_ALL
  WHERE 0                             =0
  AND PO_VENDORS.VENDOR_ID            = PO_VENDOR_SITES_ALL.VENDOR_ID
  AND PO_VENDOR_SITES_ALL.ATTRIBUTE1 IS NOT NULL
  ) Y
WHERE X.ATTRIBUTE1 = Y.ATTRIBUTE1(+)
ORDER BY 1 ;
	
  
	
  cur_fn cur_st%rowtype;

  v_log_filename utl_file.file_type;
  v_log_path     VARCHAR2(200) := '/usr/tmp';
  v_filename     VARCHAR2(200) := '&1';

BEGIN

  BEGIN
    --- Write File Complete ---
    v_log_filename := utl_file.fopen(v_log_path, v_filename, 'A');
  
  END;

  --utl_file.put_line(v_log_filename,'vendor_number|' || 'vendor_name|' || 'attribute1|' || 'vendor_site_code|' || 'eai_crtd_dttm|' || 'status');
  --utl_file.put_line(v_log_filename,'Retailer Code|' || 'vender_number|' || 'vendor_site_code|' || 'a.vendor_name|' || 'a.vat_registration_num|' || 'status|' || 'eai_crtd_dttm|' || 'b.vendor_name|' || 'b.vat_registration_num');


  OPEN cur_st;
  LOOP
    FETCH cur_st
      into cur_fn;
    EXIT WHEN cur_st%notfound;
  
    utl_file.put_line(v_log_filename, cur_fn.v_output);
  
  END LOOP;
  CLOSE cur_st;

  utl_file.fclose(v_log_filename);

END;
/
EXIT;
