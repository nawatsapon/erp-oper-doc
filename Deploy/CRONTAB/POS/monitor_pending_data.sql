@/home/posprod/SCRIPT/MONITOR_POS/connsql.sql
	set heading off;
	set linesize 2000;

	------------1:  Inventory Transaction -----------------
	spool &&1
		SELECT  transaction_interface_id ||'|'|| item_segment1 ||'|'|| transaction_quantity ||'|'|| transaction_uom ||'|'|| to_char(transaction_date,'dd/mm/yyyy hh24:mi:ss') ||'|'|| subinventory_code ||'|'|| loc_segment1 ||'|'|| transaction_source_name v_output
		FROM mtl_transactions_interface
		WHERE source_code = 'POS'
		AND (process_flag = 1 or (process_flag = 3 and transaction_header_id is null));
	spool off;

	------------2:  Internal, Purchase Requisition ------------
	spool &&2
		SELECT  transaction_id ||'|'|| header_description ||'|'|| req_number_segment1 ||'|'|| decode(source_type_code, 'INVENTORY', 'I0' || req_number_segment1, 'E0' || substr(req_number_segment1, 2)) ||'|'|| item_segment1 ||'|'|| quantity ||'|'|| unit_of_measure ||'|'|| authorization_status ||'|'|| preparer_id
		FROM  po_requisitions_interface_all
		WHERE  interface_source_code = 'FUTURE'
		AND  process_flag is null;
	spool off;

	------------3:  Receive From Logistic, Supplier -----------
	spool &&3
		SELECT  interface_transaction_id ||'|'|| item_num ||'|'|| quantity ||'|'|| unit_of_measure ||'|'|| receipt_source_code ||'|'|| document_num ||'|'|| decode(receipt_source_code, null, 'PURCHASE', receipt_source_code) ||'|'|| 'I0' || req_num ||'|'|| subinventory ||'|'|| locator v_output
		FROM  rcv_transactions_interface
		WHERE  processing_status_code = 'PENDING'
		AND  group_id = 1;
	spool off;
exit;
