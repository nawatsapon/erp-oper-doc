. /usr/home/applprod/.profile
##############################################################################

CHK_DT=`echo $(date '+20%y%m%d')`
DTM=`echo $(date '+%d-%m-20%y')`

SC_PATH="/usr/home/applprod/SCRIPT/MONITOR_POS"
LOG_PATH="${SC_PATH}/LOG"

SC_NAME="export_terminate_fee"
SQL_NAME="${SC_NAME}.sql"

LOGSC="${SC_NAME}.log"
LOGNAME="${LOG_PATH}/${SC_NAME}_`echo $(date '+20%y%m%d_%H%M')`.csv"

########## Function _mail()  Start process send E-mail    ###############
function send_mail
{
	#ERPTEAM="ERPOperationSupport@tac.co.th"
	LISTMAIL="ERPOperationSupport@tac.co.th KhajohnH@dtac.co.th purntipa.wannasuth@dtac.co.th Arwut@dtac.co.th"
	#LISTMAIL="tantida.yodhasmutr@dtac.co.th "

	cd ${LOG_PATH}
	if [ ${CHK_RECORD}  -eq 0 ] ; then
		echo "Before mail - Data detail equal to 0"
		SUBJECT="POS-ERP :: Terminate Fee Data as of ${DTM} : No data found"
		mailx  -s "${SUBJECT}" ${LISTMAIL} < ${SC_PATH}/mail.msg
	else
		echo "Before mail - Data detail more than 0"
		SUBJECT="POS-ERP :: Terminate Fee Data as of ${DTM} = ${CHK_RECORD} records."
		mailx  -s "${SUBJECT}" -a ${LOGNAME} ${LISTMAIL} < ${SC_PATH}/mail.msg
		#(cat ${SC_PATH}/mail.msg; uuencode ${LOGNAME} ${LOGNAME})|mailx -m -s "${SUBJECT}" ${LISTMAIL}
	fi
	rm ${LOGNAME}
}

##############################################
#sqlplus /nolog @${SC_PATH}/${SQL_NAME}  ${LOGNAME}

#chmod 666 ${LOGNAME}
#chown -h posprod:posprod ${LOGNAME}


#echo "This Path" ${PATH}
#touch /usr/tmp/${LOGNAME}
#sqlplus /nolog @${SC_PATH}/${SQL_NAME}  ${LOG_PATH}/${LOGNAME}
#chmod 644 /usr/tmp/${LOGNAME}
#mv  ${SC_PATH}/${LOGNAME}  ${LOG_PATH}

echo "This Path" ${PATH}
#touch /usr/tmp/${LOGNAME}
#echo -e  "DOC_DATE|CUST_CODE|CUST_NAME|CCB_CUST_NO|BRANCH_CODE|AMOUNT|DOC_NO|SUBINV_CODE|SUBINV_DESC" >> ${SC_PATH}/${LOGNAME}
export NLS_LANG='AMERICAN_AMERICA.TH8TISASCII'
sqlplus /nolog @${SC_PATH}/${SQL_NAME}  ${LOGNAME}
chmod 666 ${LOGNAME}
sed -i '/^$/d' ${LOGNAME}
sed -i '1 i\TRANS_CODE|DOC_DATE|CUST_NAME|CCB_CUST_NO|CUST_TEL|AMOUNT|DOC_NO|SUBINV_CODE|SUBINV_DESC' ${LOGNAME}
#mv  ${SC_PATH}/${LOGNAME}  ${LOG_PATH}


#chmod 644 ${LOG_PATH}/${LOGNAME}

##### Check Log file #####
cd ${LOG_PATH}
CHK_ALL=`cat ${LOGNAME} | grep 'no rows selected' | wc -l`
echo "Rec_ALL" ${CHK_ALL}

if [ ${CHK_ALL} -eq 1 ] ; then

	CHK_RECORD=0
else
	echo "Rec_ALL_Path" ${LOGNAME}
	CHK_ALL=`wc -l ${LOGNAME} |awk '{print $1}'`
	CHK_RECORD=`expr ${CHK_ALL}`
fi
echo "Rec_ALL_New" ${CHK_ALL}
echo "Rec" ${CHK_RECORD}

echo "[$(date '+%d/%m/20%y %H:%M')] Terminate Fee Data as of ${DTM} = ${CHK_RECORD} records." >> ${LOGSC}
send_mail		## Call function send_mail
