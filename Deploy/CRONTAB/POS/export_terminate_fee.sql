@/usr/home/applprod/SCRIPT/MONITOR_POS/connsql.sql
	set heading off;
	set linesize 2000;
	
	------------ Check Invoice & Credit Note Data -----------------
	spool &&1
		SELECT SC_TRANS_DTL.TRANS_CODE
			  || '|'
			  || SC_TRANSACTION.DOC_DATE
			  || '|'
			  || SC_TRANSACTION.CUST_NAME
			  || '|'
			  || SC_TRANSACTION.CCB_CUST_NO
			  || '|'
			  || SC_TRANSACTION.CUST_TEL
			  || '|'
			  || SC_TRANS_DTL.AMOUNT
			  || '|'
			  || SC_TRANS_DTL.DOC_NO
			  || '|'
			  || SC_TRANSACTION.SUBINV_CODE
			  || '|'
			  || PB_SUBINV.SUBINV_DESC
			  || '|'
			  || NULL
			  V_OUT
			FROM SC_TRANSACTION,
			  SC_TRANS_DTL,
			  PB_SUBINV
			WHERE SC_TRANSACTION.OU_CODE       = SC_TRANS_DTL.OU_CODE
			AND SC_TRANSACTION.SUBINV_CODE     = SC_TRANS_DTL.SUBINV_CODE
			AND SC_TRANSACTION.DOC_NO          = SC_TRANS_DTL.DOC_NO
			AND SC_TRANSACTION.SUBINV_CODE     = PB_SUBINV.SUBINV_CODE
			AND SC_TRANSACTION.OU_CODE         = PB_SUBINV.OU_CODE
			AND TRUNC(SC_TRANSACTION.DOC_DATE) = TRUNC(sysdate-1)
			AND SC_TRANS_DTL.TRANS_CODE                      IN ('0203')
			ORDER BY SC_TRANSACTION.DOC_DATE;
	spool off;

exit;
