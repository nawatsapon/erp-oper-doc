@/usr/home/applprod/SCRIPT/MONITOR_POS/connsql.sql
SET serveroutput ON size 1000000
SET linesize 10000
col USER_CONCURRENT_QUEUE_NAME FORMAT a80;
col max_processes FORMAT a10;
col running_processes FORMAT a10;
col running_count FORMAT a10;
col pending_count FORMAT a80;
col crm_pend_count FORMAT a10;
col CONTROL_FLAG FORMAT a30;
col DESCRIPTION FORMAT a80;



DECLARE
  -- Aug 29,2007 Revise delimeter from '|' to ','
  -- Sep 10,2007  Add column msi.description modified by Jeerana
  -- Dec 12,2007  Add SIM-REP to   AND  msi.item_type IN ('SIM-POST','SIM-PRE','SIM-REP') by jeerana
  -- Jul 16,2010  Yodchai L. change msi.item_type IN ('SIM-POST','SIM-PRE','SIM-REP') to be like SIM% for avoid change script if add new item type of sim.
  utl_buff VARCHAR2(30000);
  v_log_filename utl_file.file_type;
  v_log_path VARCHAR2(200) := '/usr/tmp';
  v_filename VARCHAR2(200) := '&1';
  v_rundtm   VARCHAR2(200);
  v_number   NUMBER := 0;
  
  type tab_ou is table of VARCHAR2(3 BYTE);
  t_ou tab_ou;
type tab_subinv is table of VARCHAR2(10 BYTE);
  t_subinv tab_subinv;
type tab_doc_type is table of VARCHAR2(3 BYTE);
  t_doc_type tab_doc_type;
type tab_doc_no is table of VARCHAR2(15 BYTE);
  t_doc_no tab_doc_no;
type tab_upd_by is table of VARCHAR2(15 BYTE);
  t_upd_by tab_upd_by;
type tab_upd_date is table of DATE;
  t_upd_date tab_upd_date;
type tab_upd_pgm is table of VARCHAR2(8 BYTE);
  t_upd_pgm tab_upd_pgm;
  
BEGIN
  v_log_filename := utl_file.fopen(v_log_path, v_filename , 'W');
  --utl_buff       := 'ORDER_NUMBER' ||'|'|| 'NAME' ||'|'|| 'PARTY_NAME' ||'|'|| 'ITEM_NUMBER' ||'|'|| 'DESCRIPTION' ||'|'|| 'ORIG_SYS_DOCUMENT_REF' ||'|'|| 'ORG' ||'|'|| 'SUBINVENTORY_CODE' ||'|'|| 'QTY' ||'|'|| 'TRANSACTION_DATE' ||'|'|| 'DEALER_NUMBER' ||'|'|| 'TRANSACTION_ID' ||'|'|| 'SEPARATOR' ||'|'|| 'TRANSACTION_ID_B' ||'|'|| 'ALLOCATE_TYPE' ||'|'|| 'DOCUMENT_NUMBER' ||'|'|| 'DEALER_NUMBER_B' ||'|'|| 'DOCUMENT_DATE' ||'|'|| 'TOTAL';
  utl_file.put_line(v_log_filename,'===========================================================================');
  utl_file.put_line(v_log_filename,'ACT '||' '||'TAR '||' '|| 'RUN '||' '||'Pending   '||' '||'Status                            ' || 'Concurrent Queue Name' );
  utl_file.put_line(v_log_filename,'===========================================================================');
  
  SELECT SA_TRANSACTION.OU_CODE,
  SA_TRANSACTION.SUBINV_CODE,
  SA_TRANSACTION.DOC_TYPE,
  SA_TRANSACTION.DOC_NO,
  SA_TRANSACTION.UPD_BY,
  SA_TRANSACTION.UPD_DATE,
  SA_TRANSACTION.UPD_PGM
   BULK COLLECT INTO t_ou,
    t_subinv,
    t_doc_type,
    t_doc_no,
    t_upd_by,
    t_upd_date,
    t_upd_pgm
  FROM SA_TRANSACTION ,
  SA_TRANS_PAY 
WHERE 0                =0
AND SA_TRANSACTION.DOC_NO           = SA_TRANS_PAY.DOC_NO(+)
AND SA_TRANSACTION.OU_CODE          = SA_TRANS_PAY.OU_CODE(+)
AND TRUNC(SA_TRANSACTION.DOC_DATE) >= to_date('01/12/2017','dd/mm/yyyy')
AND SA_TRANS_PAY.DOC_NO          IS NULL 
--AND a.DOC_NO  = 'IVN029591700980'
;
  dbms_output.put_line('count insert : '|| t_doc_no.count);
  FOR cSub IN 1 .. t_doc_no.count
  LOOP
    utl_file.put_line(v_log_filename,'OU '|| T_ou(cSub) ||' Subinv '|| T_subinv(cSub) || ' DOC_TYPE ' || t_doc_type(cSub) || ' doc_no ' || t_doc_no(cSub)|| ' upd_by ' || t_upd_by(cSub)|| ' upd_date ' || to_char(t_upd_date(cSub),'dd/mm/yyyy')|| ' upd_pgm ' || t_upd_pgm(cSub));
    
  /*  INSERT INTO SA_TRANS_PAY
  (
    OU_CODE,
    SUBINV_CODE,
    DOC_TYPE,
    DOC_NO,
    PAYMENT_CODE,
    REF_NO,
    BANK_CODE,
    CREDIT_CARD,
    CR_BANK_RATE,
    PAY_AMT,
    TF_S_C,
    TF_C_O,
    UPD_BY,
    UPD_DATE,
    UPD_PGM,
    APPROVE_NO,
    BATCH_NAME,
    LOCKBOX_FILE,
    EDC_NO,
    BANK_ACCOUNT,
    PAY_NET
  )
  VALUES
  (
    T_ou(cSub),
    T_subinv(cSub),
    t_doc_type(cSub),
    t_doc_no(cSub),
    'CASH',
    NULL,
    NULL,
    NULL,
    NULL,
    0,
    NULL,
    NULL,
    t_upd_by(cSub),
    t_upd_date(cSub),
    'DTSADT02',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    0
  );
	rollback;*/
    
   /* UPDATE PB_SUBINV
    SET DEFAULT_LOC   = T_locator(cSub)
    WHERE SUBINV_CODE = T_subinv(cSub)
    AND OU_CODE       = T_ou(cSub);*/
  END LOOP;
  
  utl_file.put_line(v_log_filename,'==========================================================================='); 
  utl_file.fclose_all;
  
END;
/
EXIT;
