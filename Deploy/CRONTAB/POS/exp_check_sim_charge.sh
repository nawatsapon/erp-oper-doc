. /usr/home/applprod/.profile
##############################################################################

CHK_DT=`echo $(date '+20%y%m%d')`
DTM=`echo $(date '+%m-20%y')`

SC_PATH="/usr/home/applprod/SCRIPT/MONITOR_POS"
LOG_PATH="${SC_PATH}/LOG"

SC_NAME="exp_check_sim_charge"
SQL_NAME="${SC_NAME}.sql"

LOGSC="${SC_NAME}.log"
LOGNAME="${LOG_PATH}/${SC_NAME}_`echo $(date '+20%y%m%d_%H%M')`.csv"


#SC_PATH="/home/applprod/SCRIPT/MONITOR_POS"
#SC_NAME="exp_check_sim_charge"
#SQL_NAME="${SC_NAME}.sql"
#LOG_PATH="${SC_PATH}/LOG"
#LOGSC="${SC_NAME}.log"
#LOGNAME="${SC_NAME}_`echo $(date '+20%y%m%d_%H%M')`.csv"

########## Function _mail()  Start process send E-mail    ###############
function send_mail
{
	#ERPTEAM="ERPOperationSupport@tac.co.th"
	LISTMAIL="ERPOperationSupport@tac.co.th varinP@dtac.co.th Chinnapat.Ngernsongserm@dtac.co.th sasitorL@dtac.co.th jermporntip@dtac.co.th"
	#LISTMAIL="tantida.yodhasmutr@dtac.co.th"

	cd ${LOG_PATH}
	if [ ${CHK_RECORD}  -eq 0 ] ; then
		echo "Before mail - Data detail equal to 0"
		SUBJECT="POS-ERP :: Check sim charge Data as of ${DTM} : No data found"
		mailx  -s "${SUBJECT}" ${LISTMAIL} < ${SC_PATH}/mail.msg
	else
		echo "Before mail - Data detail more than 0"
		SUBJECT="POS-ERP :: Check sim charge Data as of ${DTM} = ${CHK_RECORD} records."
		mailx  -s "${SUBJECT}" -a ${LOGNAME} ${LISTMAIL} < ${SC_PATH}/mail.msg
		#(cat ${SC_PATH}/mail.msg; uuencode ${LOGNAME} ${LOGNAME})|mailx -m -s "${SUBJECT}" ${LISTMAIL}
	fi
	rm ${LOGNAME}
}

##############################################
#sqlplus /nolog @${SC_PATH}/${SQL_NAME}  ${LOGNAME}

#chmod 666 ${LOGNAME}
#chown -h posprod:posprod ${LOGNAME}


#echo "This Path" ${PATH}
#touch /usr/tmp/${LOGNAME}
#sqlplus /nolog @${SC_PATH}/${SQL_NAME}  ${LOG_PATH}/${LOGNAME}
#chmod 644 /usr/tmp/${LOGNAME}
#mv  ${SC_PATH}/${LOGNAME}  ${LOG_PATH}

echo "This Path" ${PATH}
#touch /usr/tmp/${LOGNAME}
#echo -e  "DOC_DATE|CUST_CODE|CUST_NAME|CCB_CUST_NO|BRANCH_CODE|AMOUNT|DOC_NO|SUBINV_CODE|SUBINV_DESC" >> ${LOGNAME}

export NLS_LANG='AMERICAN_AMERICA.TH8TISASCII'
sqlplus /nolog @${SC_PATH}/${SQL_NAME}  ${LOGNAME}
chmod 666 ${LOGNAME}
sed -i '/^$/d' ${LOGNAME}
sed -i '1 i\DOC_NO|DOC_DATE|ITEM_CODE|SUBINV_CODE|SUBINV_DESC|PRICE_LIST_CODE|QTY|UNITPRICE|CAL_AMT|AMOUNT|SUM_QTY|CHARGE_TYPE|CH_AMOUNT|FREE_TYPE|FR_AMOUNT|SALESPERSON|DIFF' ${LOGNAME}
#mv  ${SC_PATH}/${LOGNAME}  ${LOG_PATH}


#chmod 644 ${LOG_PATH}/${LOGNAME}

##### Check Log file #####
cd ${LOG_PATH}
CHK_ALL=`cat ${LOGNAME} | grep 'no rows selected' | wc -l`
echo "Rec_ALL" ${CHK_ALL}

if [ ${CHK_ALL} -eq 1 ] ; then

	CHK_RECORD=0
else
	echo "Rec_ALL_Path" ${LOGNAME}
	CHK_ALL=`wc -l ${LOGNAME} |awk '{print $1}'`
	CHK_RECORD=`expr ${CHK_ALL}`
fi
echo "Rec_ALL_New" ${CHK_ALL}
echo "Rec" ${CHK_RECORD}

echo "[$(date '+%d/%m/20%y %H:%M')] Check SIM Data as of ${DTM} = ${CHK_RECORD} records." >> ${LOGSC}
send_mail		## Call function send_mail
