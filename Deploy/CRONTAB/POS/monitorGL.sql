@/usr/home/applprod/SCRIPT/MONITOR_POS/connsql.sql
	set heading off;
	spool &&1
		SELECT DISTINCT (G.STATUS || '|' || G.USER_JE_CATEGORY_NAME || '|' || segment1 || '.' || segment11 || '.' || segment10 || '.' || segment12 || '.' || segment13 || '.' || segment14 || '|' || G.STATUS_DESCRIPTION) v_output
		FROM GL_INTERFACE G
		WHERE G.USER_JE_CATEGORY_NAME = 'POS-REVENUE'
		AND G.STATUS != 'NEW';
	spool off;
exit;
