#!/bin/ksh
export ORACLE_HOME=/posdb/prod/oracle/product/9.2.0
export ORACLE_SID=posprod	
export TNS_ADMIN=/posdb/prod/oracle/product/9.2.0/network/admin
export NLS_LANG=American_America.TH8TISASCII
export PATH=$PATH:/posdb/prod/oracle/product/9.2.0/bin:/usr/sbin:/usr/bin:/etc:/usr/local/bin

CHK_DT=`echo $(date '+20%y%m%d')`
DTM=`echo $(date '+%d/%m/20%y %H:%M')`

SC_PATH="/home/posprod/SCRIPT/MONITOR_APP"
LOG_PATH="${SC_PATH}/LOG"

SC_NAME="monitor_invoice_cn"
SQL_NAME="${SC_NAME}.sql"

LOGSC="${SC_NAME}.log"
LOGNAME="${LOG_PATH}/${SC_NAME}_`echo $(date '+20%y%m%d_%H%M')`.log"
MAILDETAIL="${SC_NAME}_`echo $(date '+20%y%m%d_%H%M')`.tmp"


########## Function _mail()  Start process send E-mail    ###############
function send_mail
{
	ERPTEAM="ostanonp@dtac.co.th" 
	SODTEAM="ERPOperationSupport@tac.co.th"
	LISTMAIL="${ERPTEAM}, ${SODTEAM}" 
	
	mailx -s "${SUBJECT}" ${LISTMAIL} < ${LOG_PATH}/${MAILDETAIL}
	rm ${LOG_PATH}/${MAILDETAIL}
}

##############################################
sqlplus /nolog @${SC_PATH}/${SQL_NAME}  ${LOGNAME}

chmod 666 ${LOGNAME}
chown -h posprod:posprod ${LOGNAME}

##### Check Log file #####
cd ${LOG_PATH}

CHK_RECORD=`grep '|' ${LOGNAME} |wc -l`

if [ ${CHK_RECORD} -eq  0 ]; then

	SUBJECT="Inform Monitor POS : Check Invoice & Credit Note Data : ${DTM}"

	echo "Dear POS Helpdesk Support\n" >> ${LOG_PATH}/${MAILDETAIL}
	echo "\tInform monitor Invoice & Credit Note Data not have record error of ${DTM}" >> ${LOG_PATH}/${MAILDETAIL}
	echo "\n\nBest Regard,\nERP Operation Support\nEmail: SOD-EA-ERPBOSDept@tac.co.th" >> ${LOG_PATH}/${MAILDETAIL}

	send_mail		## Call function send_mail
	rm ${LOGNAME}
	echo "[$(date '+%d/%m/20%y %H:%M')] Invoice & Credit Note Data not have error." >> ${LOGSC}
		
else	

	SUBJECT="Inform Monitor POS : Check Invoice & Credit Note Data : ${DTM}"

	echo "Dear POS Helpdesk Support\n" >> ${LOG_PATH}/${MAILDETAIL}
	echo "\tThis output of monitor Invoice & Credit Note Data have record error of ${DTM} = ${CHK_RECORD} records." >> ${LOG_PATH}/${MAILDETAIL}
	echo "Please see detail your below.\n" >> ${LOG_PATH}/${MAILDETAIL}
	echo " DOC_NO|DOC_DATE|GROSS_AMT|SUM(PY.PAY_AMT)|SUM(PY.PAY_NET)" >> ${LOG_PATH}/${MAILDETAIL}
	grep -v "rows selected" ${LOGNAME} >> ${LOG_PATH}/${MAILDETAIL}
	echo "\n\nBest Regard,\nERP Operation Support\nEmail: SOD-EA-ERPBOSDept@tac.co.th" >> ${LOG_PATH}/${MAILDETAIL}
	
	send_mail		## Call function send_mail
	echo "[$(date '+%d/%m/20%y %H:%M')] [1: Inventory Transaction]  have record error = ${CHK_RECORD1} records." >> ${LOGSC}
fi
