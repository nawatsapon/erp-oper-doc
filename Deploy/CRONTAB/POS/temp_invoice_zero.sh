#!/bin/ksh
export ORACLE_HOME=/posdb/prod/oracle/product/9.2.0
export ORACLE_SID=posprod	
export TNS_ADMIN=/posdb/prod/oracle/product/9.2.0/network/admin
export NLS_LANG=American_America.TH8TISASCII
export PATH=$PATH:/posdb/prod/oracle/product/9.2.0/bin:/usr/sbin:/usr/bin:/etc:/usr/local/bin


CHK_DT=`echo $(date '+20%y%m%d')`
DTM=`echo $(date '+%d/%m/20%y')`

SC_PATH="/usr/home/applprod/SCRIPT/MONITOR_POS"
SC_NAME="temp_invoice_zero"
LOG_PATH="${SC_PATH}/LOG"
LOGSC="${SC_NAME}.log"
LOGNAME="${SC_NAME}_`echo $(date '+20%y%m%d_%H%M')`.log"
MAILDETAIL="${SC_NAME}_`echo $(date '+20%y%m%d_%H%M')`.tmp"

##############################################################################
# Aug 22, 2007		Create new script for monitoring
# Sep 6, 2007		Add mail K.Kanchanat Kijjapool (Kanchanat@dtac.co.th) Ref
# Dec 26, 2007		Add mail WarapoSo@dtac.co.th, Sirichai@dtac.co.th  and delete mail Sompong.K@dtac.co.th Ref : HD0005742 


########## Function _mail()  Start process send E-mail    ###############
function send_mail
{
	
	#LISTMAIL="${YODCHAI}, ${KANCHANAT}, ${THUNWARATEM}, ${SODTEAM}, ${CHAWIWAN}, ${SUTHANYA}, ${ORNPAPHA}, ${PANEE}, ${NATTAPORN},tantida.yodhasmutr@dtac.co.th"
	LISTMAIL="tantida.yodhasmutr@dtac.co.th"
	#LISTMAIL="ERPOperationSupport@dtac.co.th"
	
	cd ${LOG_PATH}
	#if [ ${CHK_RECORD}  -eq 0 ] ; then
	#	echo "Before mail - Data detail equal to 0"
	#	mailx  -s "${SUBJECT}" ${LISTMAIL} < ${SC_PATH}/mail.msg
	#else
		#(cat ${SC_PATH}/mail.msg; uuencode ${LOGNAME} ${LOGNAME})|mailx  -s "${SUBJECT}" ${LISTMAIL}
	#	echo "Before mail - Data detail more than 0"
	echo "Before mail"
	#	mailx  -s "${SUBJECT}" -a ${LOG_PATH}/${LOGNAME} ${LISTMAIL} < ${SC_PATH}/mail.msg
	#fi
	
	
	mailx -s "${SUBJECT}" ${LISTMAIL} < ${LOG_PATH}/${MAILDETAIL}
	rm ${LOG_PATH}/${MAILDETAIL}
}

##############################################
touch /usr/tmp/${LOGNAME}
sqlplus /nolog @${SC_PATH}/${SC_NAME}.sql  ${LOGNAME}
mv  /usr/tmp/${LOGNAME}  ${LOG_PATH}

chmod 644 ${LOG_PATH}/${LOGNAME}

##### Check Log file #####
cd ${LOG_PATH}
CHK_ALL=`wc -l ${LOGNAME} |awk '{print $1}'`
CHK_RECORD=`expr ${CHK_ALL} - 1`

SUBJECT="Invoice Zero"

grep -v "rows selected" ${LOGNAME} >> ${LOG_PATH}/${MAILDETAIL}
#echo "[$(date '+%d/%m/20%y %H:%M')] Data allocate of ${DTM} = ${CHK_RECORD} records." >> ${LOGSC}
#rm ${LOGNAME}
echo -e "\n\nBest Regard,\nERP Operation Support\nEmail: ERPOperationSupport@dtac.co.th" >> ${LOG_PATH}/${MAILDETAIL}

send_mail		## Call function send_mail
