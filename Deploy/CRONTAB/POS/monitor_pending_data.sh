#!/bin/ksh
export ORACLE_HOME=/posdb/prod/oracle/product/9.2.0
export ORACLE_SID=posprod	
export TNS_ADMIN=/posdb/prod/oracle/product/9.2.0/network/admin
export NLS_LANG=American_America.TH8TISASCII
export PATH=$PATH:/posdb/prod/oracle/product/9.2.0/bin:/usr/sbin:/usr/bin:/etc:/usr/local/bin

CHK_DT=`echo $(date '+20%y%m%d')`
DTM=`echo $(date '+%d/%m/20%y %H:%M')`

SC_PATH="/home/posprod/SCRIPT/MONITOR_APP"
LOG_PATH="${SC_PATH}/LOG"

SC_NAME="monitor_pending_data"
SQL_NAME="${SC_NAME}.sql"

LOGSC="${SC_NAME}.log"
LOGNAME1="${LOG_PATH}/${SC_NAME}_`echo $(date '+20%y%m%d_%H%M')`_1.log"
LOGNAME2="${LOG_PATH}/${SC_NAME}_`echo $(date '+20%y%m%d_%H%M')`_2.log"
LOGNAME3="${LOG_PATH}/${SC_NAME}_`echo $(date '+20%y%m%d_%H%M')`_3.log"
MAILDETAIL="${SC_NAME}_`echo $(date '+20%y%m%d_%H%M')`.tmp"


########## Function _mail()  Start process send E-mail    ###############
function send_mail
{
	ERPTEAM="ostanonp@dtac.co.th" 
	SODTEAM="ERPOperationSupport@tac.co.th"
	LISTMAIL="${ERPTEAM}, ${SODTEAM}" 
	
	mailx -s "${SUBJECT}" ${LISTMAIL} < ${LOG_PATH}/${MAILDETAIL}
	rm ${LOG_PATH}/${MAILDETAIL}
}

##############################################
sqlplus /nolog @${SC_PATH}/${SQL_NAME}  ${LOGNAME1} ${LOGNAME2} ${LOGNAME3}

chmod 666 ${LOGNAME1} ${LOGNAME2} ${LOGNAME3}
chown -h posprod:posprod ${LOGNAME1} ${LOGNAME2} ${LOGNAME3}

##### Check Log file #####
cd ${LOG_PATH}

CHK_RECORD1=`grep '|' ${LOGNAME1} |wc -l`
CHK_RECORD2=`grep '|' ${LOGNAME2} |wc -l`
CHK_RECORD3=`grep '|' ${LOGNAME3} |wc -l`

if [ ${CHK_RECORD1} -eq  0  -a  ${CHK_RECORD2} -eq  0  -a  ${CHK_RECORD3} -eq  0 ]; then

	SUBJECT="Inform Monitor POS : Check Transactions Interface : Wait for erp process : ${DTM}"

	echo "Dear POS Helpdesk Support\n" >> ${LOG_PATH}/${MAILDETAIL}
	echo "\tInform monitor pending data wait for ERP process not have record error of ${DTM}" >> ${LOG_PATH}/${MAILDETAIL}
	echo "\n\nBest Regard,\nERP Operation Support\nEmail: SOD-EA-ERPBOSDept@tac.co.th" >> ${LOG_PATH}/${MAILDETAIL}

	send_mail		## Call function send_mail
	rm ${LOGNAME1} ${LOGNAME2} ${LOGNAME3}
	echo "[$(date '+%d/%m/20%y %H:%M')] [1: Inventory Transaction]  not have error." >> ${LOGSC}
	echo "[$(date '+%d/%m/20%y %H:%M')] [2: Internal, Purchase Requisition]  not have error." >> ${LOGSC}
	echo "[$(date '+%d/%m/20%y %H:%M')] [3: Receive From Logistic, Supplier]  not have error." >> ${LOGSC}
	
else	

	SUBJECT="Inform Monitor POS : Check Transactions Interface : Wait for erp process : ${DTM}"

	echo "Dear POS Helpdesk Support\n" >> ${LOG_PATH}/${MAILDETAIL}
	echo "\tThis output of monitor pending data wait for ERP process have record error of ${DTM}" >> ${LOG_PATH}/${MAILDETAIL}
	echo "Please see detail your below.\n" >> ${LOG_PATH}/${MAILDETAIL}
	
	
	if [ ${CHK_RECORD1} -gt  0 ]; then
		echo "\n[1:  Inventory Transaction]  have record error = ${CHK_RECORD1} records." >> ${LOG_PATH}/${MAILDETAIL}
		echo "TRANSACTION_INTERFACE_ID|ITEM_SEGMENT1|TRANSACTION_QUANTITY|TRANSACTION_UOM|TRANSACTION_DATE|SUBINVENTORY_CODE|LOC_SEGMENT1|TRANSACTION_SOURCE_NAME" >> ${LOG_PATH}/${MAILDETAIL}
		grep -v "rows selected" ${LOGNAME1} >> ${LOG_PATH}/${MAILDETAIL}
		echo "[$(date '+%d/%m/20%y %H:%M')] [1: Inventory Transaction]  have record error = ${CHK_RECORD1} records." >> ${LOGSC}
	else
		echo "\n[1:  Inventory Transaction]  not have error." >> ${LOG_PATH}/${MAILDETAIL}
		echo "[$(date '+%d/%m/20%y %H:%M')] [1: Inventory Transaction]  not have error." >> ${LOGSC}
		rm  ${LOGNAME1}
	fi


	if [ ${CHK_RECORD2} -gt  0 ]; then
		echo "\n[2:  Internal, Purchase Requisition]  have record error = ${CHK_RECORD2} records." >> ${LOG_PATH}/${MAILDETAIL}
		echo "TRANSACTION_ID|HEADER_DESCRIPTION|ERP_REQ_NO|POS_REQ_NO	ITEM_SEGMENT1|QUANTITY|UNIT_OF_MEASURE|AUTHORIZATION_STATUS|PREPARER_ID" >> ${LOG_PATH}/${MAILDETAIL}
		grep -v "rows selected" ${LOGNAME2} >> ${LOG_PATH}/${MAILDETAIL}
		echo "[$(date '+%d/%m/20%y %H:%M')] [2: Internal, Purchase Requisition]  have record error = ${CHK_RECORD2} records." >> ${LOGSC}
	else
		echo "\n[2:  Internal, Purchase Requisition]  not have error." >> ${LOG_PATH}/${MAILDETAIL}
		echo "[$(date '+%d/%m/20%y %H:%M')] [2: Internal, Purchase Requisition]  not have error." >> ${LOGSC}
		rm  ${LOGNAME2}
	fi


	if [ ${CHK_RECORD3} -gt  0 ]; then
		echo "\n[3:  Receive From Logistic, Supplier]  have error record = ${CHK_RECORD3} records." >> ${LOG_PATH}/${MAILDETAIL}
		echo "INTERFACE_TRANSACTION_ID|ITEM_NUM|QUANTITY|UNIT_OF_MEASURE|RECEIPT_SOURCE_CODE|PO_NO_OR_RCV_NO|RCV_TYPE|POS_REQ_NO|SUBINVENTORY|LOCATOR" >> ${LOG_PATH}/${MAILDETAIL}
		grep -v "rows selected" ${LOGNAME3} >> ${LOG_PATH}/${MAILDETAIL}
		echo "[$(date '+%d/%m/20%y %H:%M')] [3: Receive From Logistic, Supplier]  have record error = ${CHK_RECORD3} records." >> ${LOGSC}
	else
		echo "\n[3:  Receive From Logistic, Supplier]  not have error." >> ${LOG_PATH}/${MAILDETAIL}
		echo "[$(date '+%d/%m/20%y %H:%M')] [3: Receive From Logistic, Supplier]  not have error." >> ${LOGSC}
		rm  ${LOGNAME3}
	fi
	
	echo "\n\nBest Regard,\nERP Operation Support\nEmail: ERP Operation Support" >> ${LOG_PATH}/${MAILDETAIL}

	
#send_mail		## Call function send_mail
fi
