#!/bin/ksh
export ORACLE_HOME=/posdb/prod/oracle/product/9.2.0
export ORACLE_SID=posprod	
export TNS_ADMIN=/posdb/prod/oracle/product/9.2.0/network/admin
export NLS_LANG=American_America.TH8TISASCII
export PATH=$PATH:/posdb/prod/oracle/product/9.2.0/bin:/usr/sbin:/usr/bin:/etc:/usr/local/bin

. /usr/home/applprod/.profile


CHK_DT=`echo $(date '+20%y%m%d')`
DTM=`echo $(date '+%d/%m/20%y %H:%M')`

SC_PATH="/usr/home/applprod/SCRIPT/MONITOR_POS"
LOG_PATH="${SC_PATH}/LOG"

SC_NAME="POS_fix_job_broken"
SQL_NAME="${SC_NAME}.sql"

LOGSC="${SC_NAME}.log"
LOGNAME1="${LOG_PATH}/${SC_NAME}_`echo $(date '+20%y%m%d_%H%M')`_1.log"
LOGNAME2="${LOG_PATH}/${SC_NAME}_`echo $(date '+20%y%m%d_%H%M')`_2.log"

MAILDETAIL="${SC_NAME}_`echo $(date '+20%y%m%d_%H%M')`.tmp"

##############################################################################
# Aug 22, 2007		Create new script for monitoring
# Sep 6, 2007		Add mail K.Kanchanat Kijjapool (Kanchanat@dtac.co.th) Ref
# Dec 26, 2007		Add mail WarapoSo@dtac.co.th, Sirichai@dtac.co.th  and delete mail Sompong.K@dtac.co.th Ref : HD0005742 


########## Function _mail()  Start process send E-mail    ###############
function send_mail
{
		
	LISTMAIL="ERPOperationSupport@tac.co.th"
	#LISTMAIL="tantida.yodhasmutr@dtac.co.th"
	
	mailx -s "${SUBJECT}" ${LISTMAIL} < ${LOG_PATH}/${MAILDETAIL}
	rm ${LOG_PATH}/${MAILDETAIL}
}

##############################################
sqlplus /nolog @${SC_PATH}/${SQL_NAME}  ${LOGNAME1} ${LOGNAME2} 


chmod 666 ${LOGNAME1} ${LOGNAME2} 
##### Check Log file #####
cd ${LOG_PATH}

CHK_RECORD1=`grep '|' ${LOGNAME1} |wc -l`
CHK_RECORD2=`grep '|' ${LOGNAME2} |wc -l`

	if [ ${CHK_RECORD1} -gt  0 ]; then
		SUBJECT="!!!MUST SEE - Monitor POS JOB : ONE or MORE JOBS BROKEN!!! :  ${DTM}"
		echo -e "\n[POS JOB BROKEN BEFORE FIX] = ${CHK_RECORD1} records." >> ${LOG_PATH}/${MAILDETAIL}
		echo -e  "BROKEN|JOB|LOG_USER|FIX_CODE|WHAT" >> ${LOG_PATH}/${MAILDETAIL}
		grep -v "rows selected" ${LOGNAME1} >> ${LOG_PATH}/${MAILDETAIL}
		echo -e "\n[POS JOB BROKEN AFTER FIX] = ${CHK_RECORD2} records." >> ${LOG_PATH}/${MAILDETAIL}
		echo -e  "BROKEN|JOB|LOG_USER|WHAT" >> ${LOG_PATH}/${MAILDETAIL}
		grep -v "rows selected" ${LOGNAME2} >> ${LOG_PATH}/${MAILDETAIL}
		echo -e "[$(date '+%d/%m/20%y %H:%M')] [POS JOBS BROKEN BEFORE FIX]  = ${CHK_RECORD1} records." >> ${LOGSC}
		echo -e "[$(date '+%d/%m/20%y %H:%M')] [POS JOBS BROKEN AFTER FIX] = ${CHK_RECORD1} records." >> ${LOGSC}
		echo -e "\n\nBest Regard,\nERP Operation Support\nEmail: ERPOperationSupport@dtac.co.th" >> ${LOG_PATH}/${MAILDETAIL}
		send_mail		## Call function send_mail
	fi
	
		rm ${LOGNAME1} 
		rm ${LOGNAME2}
		


