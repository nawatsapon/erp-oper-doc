@/usr/home/applprod/SCRIPT/MONITOR_POS/connsql.sql
set heading off;
SET serveroutput ON size 1000000
SET linesize 10000;


	spool &&1
		SELECT BROKEN  
    || '|' || JOB
    || '|' || LOG_USER
	|| '|' || 'DBMS_JOB.BROKEN(' || JOB || ',false);' 
    || '|' || WHAT 
    FROM DBA_JOBS
    WHERE BROKEN='Y'
	AND JOB <> 337;
	spool off;

DECLARE
  -- May 30,2018 Create Program by TTD
  
  type tab_BROKEN is table of VARCHAR2(1);
  t_BROKEN tab_BROKEN;
type tab_JOB is table of NUMBER;
  t_JOB tab_JOB;
type tab_LOG_USER is table of VARCHAR2(30);
  t_LOG_USER tab_LOG_USER;
type tab_FIX is table of VARCHAR2(45);
  t_FIX tab_FIX;
type tab_WHAT is table of VARCHAR2(4000);
  t_WHAT tab_WHAT;
  
BEGIN

		SELECT SYS.DBA_JOBS.BROKEN,
		  SYS.DBA_JOBS.JOB,
		  SYS.DBA_JOBS.LOG_USER,
		  'DBMS_JOB.BROKEN(' || JOB || ',false);' FIX,
		  SYS.DBA_JOBS.WHAT  
		  BULK COLLECT INTO
		  t_BROKEN,
		  t_JOB,
		  t_LOG_USER,
		  t_FIX,
		  t_WHAT
		FROM SYS.DBA_JOBS
		WHERE BROKEN='Y'
		AND JOB <> 337;
		
	FOR cJob IN 1 .. t_JOB.count
	LOOP
  
	DBMS_JOB.BROKEN(t_JOB(cJob),false);
	commit;
	
	END LOOP;
  

END;
/
	spool &&2
		SELECT BROKEN  
    || '|' || JOB
    || '|' || LOG_USER
	|| '|' || 'DBMS_JOB.BROKEN(' || JOB || ',false);' 
    || '|' || WHAT 
    FROM DBA_JOBS
    WHERE BROKEN='Y'
	AND JOB <> 337;
	spool off;
EXIT;
