@/home/posprod/SCRIPT/MONITOR_POS/connsql.sql
	set heading off;
	set linesize 2000;

	------------ Check Invoice & Credit Note Data -----------------
	spool &&1
		SELECT  HD.DOC_NO ||'|'|| to_char(HD.DOC_DATE,'dd/mm/yyyy hh24:mi:ss') ||'|'|| HD.GROSS_AMT ||'|'|| SUM(PY.PAY_AMT) ||'|'|| SUM(PY.PAY_NET)  v_output
		FROM    SA_TRANSACTION HD, SA_TRANS_PAY PY 
		WHERE  HD.OU_CODE       = PY.OU_CODE(+) 
		AND       HD.DOC_TYPE    = PY.DOC_TYPE(+) 
		AND    HD.DOC_NO      = PY.DOC_NO(+)
		HAVING NVL(HD.GROSS_AMT,0) <> NVL(SUM(PY.PAY_NET),0) 
		GROUP BY HD.DOC_NO, HD.DOC_DATE, HD.GROSS_AMT;
	spool off;

exit;
