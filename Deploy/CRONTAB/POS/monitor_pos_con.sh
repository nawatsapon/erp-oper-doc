#!/bin/ksh 
## Declar Variable ##
export ORACLE_HOME=/posdb/prod/oracle/product/9.2.0 
export ORACLE_SID=posprod
export TNS_ADMIN=/posdb/prod/oracle/product/9.2.0/network/admin
export NLS_LANG=American_America.TH8TISASCII
export PATH=$PATH:/posdb/prod/oracle/product/9.2.0/bin:/usr/sbin:/usr/bin:/etc:/usr/local/bin



CHK_DT=`echo $(date '+20%y%m%d')`
DTM=`echo $(date '+%d/%m/20%y %H:%M')`

SC_PATH="/home/posprod/SCRIPT/MONITOR_APP"
LOG_PATH="${SC_PATH}/LOG"

SC_NAME="monitor_pos_con"
SQL_NAME="${SC_NAME}.sql"

LOGSC="${SC_NAME}.log"
LOGNAME="${LOG_PATH}/${SC_NAME}_`echo $(date '+20%y%m%d_%H%M')`.log"
MAILDETAIL="${SC_NAME}_`echo $(date '+20%y%m%d_%H%M')`.tmp"
########## Function _mail()  Start process send E-mail    ###############
function send_mail
{
	CHOL="siraprapha@dtac.co.th"
	SODTEAM="ERPOperationSupport@tac.co.th"
	LISTMAIL="${SODTEAM}"
	#LISTMAIL="${CHOL}"

	mailx -s "${SUBJECT}" ${LISTMAIL} < ${LOG_PATH}/${MAILDETAIL}
	rm ${LOG_PATH}/${MAILDETAIL}
}
######################Run SQL Statement###################################
sqlplus /nolog @${SC_PATH}/${SQL_NAME}  ${LOGNAME}
chmod 666 ${LOGNAME}
chown -h posprod:dba ${LOGNAME}
###########################################################################


##### Check Log file #####
cd ${LOG_PATH}

CHK_RECORD=`grep 'POS' ${LOGNAME} |wc -l`

if [ ${CHK_RECORD} -eq  0 ]; then
	SUBJECT="Inform Monitor POS : Check Status Job Control : ${DTM} broken ${CHK_RECORD} records"

	echo "Dear POS Team Support\n" >> ${LOG_PATH}/${MAILDETAIL}
	echo "\tInform monitor Status Job Control Data have not Broken of ${DTM}" >> ${LOG_PATH}/${MAILDETAIL}
	echo "\n\nBest Regard,\nERP Operation Support\nEmail: SOD-EA-ERPBOSDept@tac.co.th" >> ${LOG_PATH}/${MAILDETAIL}

	send_mail               ## Call function send_mail
	rm ${LOGNAME}
	echo "[$(date '+%d/%m/20%y %H:%M')] Status Job Conctol have not broken." >> ${LOGSC}

else

	SUBJECT="Inform Monitor POS : Check Status Job Control : ${DTM} broken ${CHK_RECORD} records"

	echo "Dear POS Team Support\n" >> ${LOG_PATH}/${MAILDETAIL}
	echo "\tThis output of monitor Check Status Job Control have Broken of ${DTM} = ${CHK_RECORD} records." >> ${LOG_PATH}/${MAILDETAIL}
	echo "Please see detail your below.\n" >> ${LOG_PATH}/${MAILDETAIL}
	echo "LOG_USER|JOB_NAME|BROKEN" >> ${LOG_PATH}/${MAILDETAIL}
	grep -v "BROKEN" ${LOGNAME} >> ${LOG_PATH}/${MAILDETAIL}
	echo "\n\nBest Regard,\nERP Operation Support\nEmail: SOD-EA-ERPBOSDept@tac.co.th" >> ${LOG_PATH}/${MAILDETAIL}

	send_mail  		## Call function send_mail
	rm ${LOGNAME}
	echo "[$(date '+%d/%m/20%y %H:%M')] Status Job Control have Broken = ${CHK_RECORD} records." >> ${LOGSC}

fi
