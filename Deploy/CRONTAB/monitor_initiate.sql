@/home/oraprod/SCRIPT/MONITOR_ERP/connsql.sql
DECLARE
  -- Aug 29,2007 Revise delimeter from '|' to ','
  -- Feb 20,2009  Change SQL Script  WO95,666
  -- May 12,2009  Add condition check source in SQL Script WO106465
  CURSOR cur_st
  IS
    SELECT (aia.DOC_SEQUENCE_VALUE
      || '|'
      || aia.invoice_num
      || '|'
      || aia.invoice_id
      || '|'
      || TO_CHAR(fnu.due_date,'dd/mm/yyyy')) v_output
    FROM ap_invoices_all aia ,
      ap_payment_schedules_all fnu
    WHERE aia.wfapproval_status = 'INITIATED'
    AND aia.invoice_id          = fnu.invoice_id
    AND (fnu.due_date          >= sysdate
    AND fnu.due_date           <= sysdate+30)
    AND DOC_CATEGORY_CODE      <> 'INIR'
    AND source                 <> 'STS';
  cur_fn cur_st%rowtype;
  v_log_filename utl_file.file_type;
  v_log_path VARCHAR2(200) := '/usr/tmp';
  v_filename VARCHAR2(200) := '&1';
BEGIN
  BEGIN --- Write File Complete ---
    v_log_filename := utl_file.fopen(v_log_path, v_filename, 'W');
  END;
  utl_file.put_line(v_log_filename,'VOUCHER_NUM' ||'|'|| 'INVOICE_NUM' ||'|'|| 'INVOICE_ID' ||'|'|| 'DUE_DATE');
  OPEN cur_st;
  LOOP
    FETCH cur_st INTO cur_fn;
    EXIT
  WHEN cur_st%notfound;
    utl_file.put_line(v_log_filename, cur_fn.v_output);
  END LOOP;
  CLOSE cur_st;
  utl_file.fclose(v_log_filename);
END;
/
EXIT;
