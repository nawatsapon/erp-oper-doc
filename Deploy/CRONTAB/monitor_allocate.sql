@/home/oraprod/SCRIPT/MONITOR_ERP/connsql.sql
SET serveroutput ON size 1000000
SET linesize 10000
DECLARE
  -- Aug 29,2007 Revise delimeter from '|' to ','
  -- Sep 10,2007  Add column msi.description modified by Jeerana
  -- Dec 12,2007  Add SIM-REP to   AND  msi.item_type IN ('SIM-POST','SIM-PRE','SIM-REP') by jeerana
  -- Jul 16,2010  Yodchai L. change msi.item_type IN ('SIM-POST','SIM-PRE','SIM-REP') to be like SIM% for avoid change script if add new item type of sim.
  utl_buff VARCHAR2(30000);
  v_log_filename utl_file.file_type;
  v_log_path VARCHAR2(200) := '/usr/tmp';
  v_filename VARCHAR2(200) := '&1';
  v_rundtm   VARCHAR2(200);
  v_number   NUMBER := 0;
  CURSOR main_cur
  IS
    SELECT (a.order_number
      || '|'
      || a.name
      || '|'
      || a.party_name
      || '|'
      || a.item_number
      || '|'
      || a.description
      || '|'
      || a.orig_sys_document_ref
      || '|'
      || a.org
      || '|'
      || a.subinventory_code
      || '|'
      || a.qty
      || '|'
      || TO_CHAR(a.transaction_date,'dd/mm/yyyy')
      || '|'
      || a.dealer_number
      || '|'
      || a.transaction_id
      || '|'
      || a.Separator
      || '|'
      || b.TRANSACTION_ID_B
      || '|'
      || b.ALLOCATE_TYPE
      || '|'
      || b.DOCUMENT_NUMBER
      || '|'
      || b.DEALER_NUMBER_B
      || '|'
      || TO_CHAR(b.DOCUMENT_DATE,'dd/mm/yyyy')
      || '|'
      || b.Total) v_output
    FROM
      (SELECT
        /*+ rule */
        ooh.order_number order_number,
        oe_tran.name name,
        party.PARTY_NAME party_name,
        msi.segment1 item_number,
        msi.description description,
        ooh.orig_sys_document_ref orig_sys_document_ref,
        ORG_MAST.Organization_Code ORG,
        mmt.subinventory_code subinventory_code ,
        mmt.transaction_quantity Qty,
        mmt.transaction_date transaction_date,
        NVL(bill_cas.attribute1,cust_acct.attribute1) dealer_number,
        mmt.transaction_id transaction_id,
        '<--SaleOrderAllocateData to CCB-->' Separator
      FROM mtl_material_transactions mmt,
        mtl_transaction_types mtt,
        mtl_system_items msi,
        oe_order_headers_all ooh,
        oe_order_sources oos,
        oe_order_lines_all ool,
        hz_cust_accounts cust_acct,
        hz_cust_site_uses_all bill_su,
        hz_cust_acct_sites_all bill_cas,
        OE_TRANSACTION_TYPES_TL oe_tran ,
        hz_parties party,
        MTL_PARAMETERS ORG_MAST
      WHERE mmt.transaction_type_id         = mtt.transaction_type_id
      AND mmt.inventory_item_id             = msi.inventory_item_id
      AND mmt.organization_id               = msi.organization_id
      AND NVL(mmt.source_code,'INVENTORY') IN ( 'ORDER ENTRY', 'ORDER ENTRY INTERNAL' )
      AND mmt.trx_source_line_id            = ool.line_id
      AND ool.header_id                     = ooh.header_id
      AND ooh.Order_Source_Id               = oos.order_source_id
      AND ooh.sold_to_org_id                = cust_acct.cust_account_id(+)
      AND ooh.invoice_to_org_id             = bill_su.site_use_id(+)
      AND bill_su.cust_acct_site_id         = bill_cas.cust_acct_site_id(+)
      AND ooh.order_type_id                 = oe_tran.transaction_type_id(+)
      AND ooh.sold_to_org_id                = cust_acct.cust_account_id(+)
      AND CUST_ACCT.PARTY_ID                = PARTY.PARTY_ID(+)
      AND mmt.organization_id               = ORG_MAST.Organization_Id
      AND msi.item_type LIKE 'SIM%' --IN ('SIM-POST','SIM-PRE','SIM-REP','SIM-POST-RD')
      AND mtt.transaction_type_name                IN ('Sales order issue','Int Order Intr Ship')
      AND (TRANSACTION_DATE         >= TRUNC(sysdate-1)
      AND TRANSACTION_DATE           < TRUNC(sysdate) )
        --AND TRANSACTION_DATE          >= to_date(sysdate-1,'dd/mm/yyyy')
        --AND TRANSACTION_DATE           < (to_date(sysdate,'dd/mm/yyyy') + 1)
      ) A,
    (SELECT TRANSACTION_ID transaction_id_b,
      ALLOCATE_TYPE,
      DOCUMENT_NUMBER,
      DEALER_NUMBER dealer_number_b,
      DOCUMENT_DATE,
      COUNT(*) Total
    FROM dtac_allc_itfc
    WHERE 0=0
      --AND DOCUMENT_DATE >= to_date(sysdate-1,'dd/mm/yyyy')
      --AND DOCUMENT_DATE    < (to_date(sysdate,'dd/mm/yyyy') + 1)
    AND (DOCUMENT_DATE >= TRUNC(sysdate-1)
    AND DOCUMENT_DATE   < TRUNC(sysdate))
    GROUP BY TRANSACTION_ID,
      ALLOCATE_TYPE,
      DOCUMENT_NUMBER,
      DEALER_NUMBER,
      DOCUMENT_DATE
    ) B
  WHERE A.transaction_id = b.transaction_id_b(+)
  ORDER BY ORG,
    Party_name;
  c1 main_cur%rowtype;
BEGIN
  v_log_filename := utl_file.fopen(v_log_path, v_filename , 'W');
  utl_buff       := 'ORDER_NUMBER' ||'|'|| 'NAME' ||'|'|| 'PARTY_NAME' ||'|'|| 'ITEM_NUMBER' ||'|'|| 'DESCRIPTION' ||'|'|| 'ORIG_SYS_DOCUMENT_REF' ||'|'|| 'ORG' ||'|'|| 'SUBINVENTORY_CODE' ||'|'|| 'QTY' ||'|'|| 'TRANSACTION_DATE' ||'|'|| 'DEALER_NUMBER' ||'|'|| 'TRANSACTION_ID' ||'|'|| 'SEPARATOR' ||'|'|| 'TRANSACTION_ID_B' ||'|'|| 'ALLOCATE_TYPE' ||'|'|| 'DOCUMENT_NUMBER' ||'|'|| 'DEALER_NUMBER_B' ||'|'|| 'DOCUMENT_DATE' ||'|'|| 'TOTAL';
  utl_file.put_line(v_log_filename,utl_buff);
  SELECT TO_CHAR(sysdate - 1,'ddmmyyyy') INTO v_rundtm FROM dual;
  OPEN main_cur;
  LOOP
    FETCH main_cur INTO c1;
    EXIT
  WHEN main_cur%notfound;
    v_number := v_number +1;
    utl_file.put_line(v_log_filename,c1.v_output);
  END LOOP;
  CLOSE main_cur;
  utl_file.fclose_all;
END;
/
EXIT;
