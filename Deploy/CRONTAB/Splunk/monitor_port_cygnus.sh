lineCnt1=`nc -zv cygnus22 8000 | grep succ* | wc -l`
if [ $lineCnt1 -eq 0 ]; 
then
   date '+%d/%m/%Y %H:%M:%S|CYGNUS22|8000|DOWN' >> "/MP/ERP/DASHBOARD_LOG/LOG_PORT/log_port_$(date +%Y-%m-%d).log"
else
   date '+%d/%m/%Y %H:%M:%S|CYGNUS22|8000|UP' >> "/MP/ERP/DASHBOARD_LOG/LOG_PORT/log_port_$(date +%Y-%m-%d).log"
fi

lineCnt2=`nc -zv cygnus22 9000 | grep succ* | wc -l`
#nc -zv cygnus22 9000 | grep succ* | wc -l > /dev/null
if [ $lineCnt2 -eq 0 ]; 
then
   date '+%d/%m/%Y %H:%M:%S|CYGNUS22|9000|DOWN' >> "/MP/ERP/DASHBOARD_LOG/LOG_PORT/log_port_$(date +%Y-%m-%d).log"
else
   date '+%d/%m/%Y %H:%M:%S|CYGNUS22|9000|UP' >> "/MP/ERP/DASHBOARD_LOG/LOG_PORT/log_port_$(date +%Y-%m-%d).log"
fi

lineCnt3=`nc -zv cygnus23 8000 | grep succ* | wc -l`
#nc -zv cygnus23 8000 | grep succ* | wc -l > /dev/null
if [ $lineCnt3 -eq 0 ]; 
then
   date '+%d/%m/%Y %H:%M:%S|CYGNUS23|8000|DOWN' >> "/MP/ERP/DASHBOARD_LOG/LOG_PORT/log_port_$(date +%Y-%m-%d).log"
else
   date '+%d/%m/%Y %H:%M:%S|CYGNUS23|8000|UP' >> "/MP/ERP/DASHBOARD_LOG/LOG_PORT/log_port_$(date +%Y-%m-%d).log"
fi

lineCnt4=`nc -zv cygnus23 9000 | grep succ* | wc -l`
#nc -zv cygnus23 9000 | grep succ* | wc -l > /dev/null
if [ lineCnt4 -eq 0 ] 
then
   date '+%d/%m/%Y %H:%M:%S|CYGNUS23|9000|DOWN' >> "/MP/ERP/DASHBOARD_LOG/LOG_PORT/log_port_$(date +%Y-%m-%d).log"
else
   date '+%d/%m/%Y %H:%M:%S|CYGNUS23|9000|UP' >> "/MP/ERP/DASHBOARD_LOG/LOG_PORT/log_port_$(date +%Y-%m-%d).log"
fi

find /MP/ERP/DASHBOARD_LOG/LOG_PORT/ -type f -name '*.*' -mtime +30 -exec mv {} /MP/ERP/DASHBOARD_LOG/BACKUP \;
