@/home/oraprod/SCRIPT/MONITOR_ERP/connsql.sql
SET serveroutput ON size 1000000
SET linesize 10000
col USER_CONCURRENT_QUEUE_NAME FORMAT a80;
col max_processes FORMAT a10;
col running_processes FORMAT a10;
col running_count FORMAT a10;
col pending_count FORMAT a80;
col crm_pend_count FORMAT a10;
col CONTROL_FLAG FORMAT a30;
col DESCRIPTION FORMAT a80;



DECLARE
  -- Aug 22,2017 Create monitoring concurrent manager to feed as a source for splunk dashboard by Tantida Yod.
  utl_buff VARCHAR2(30000);
  v_log_filename utl_file.file_type;
  v_log_path VARCHAR2(200) := '/usr/tmp';
  v_filename VARCHAR2(200) := '&1';
  v_rundtm   VARCHAR2(200);
  v_number   NUMBER := 0;
  
  running_count  NUMBER := 0;
  pending_count  NUMBER := 0;
  crm_pend_count NUMBER := 0;
  
  CURSOR conc_que
  IS
    SELECT CONCURRENT_QUEUE_ID,
  CONCURRENT_QUEUE_NAME,
  USER_CONCURRENT_QUEUE_NAME,
  CASE
    WHEN CONTROL_CODE = 'E'
    THEN 'Deactivated'
    ELSE CONTROL_CODE
  END CONTROL_FLAG,
  MAX_PROCESSES,
  RUNNING_PROCESSES,
  NODE_NAME,
  DESCRIPTION
FROM APPS.FND_CONCURRENT_QUEUES_VL
WHERE 0=0
  
AND CONCURRENT_QUEUE_NAME NOT LIKE 'XDP%'
AND CONCURRENT_QUEUE_NAME NOT LIKE 'IEU%'
AND CONCURRENT_QUEUE_NAME NOT IN ('ARTAXMGR','PASMGR') ;
	
BEGIN
  v_log_filename := utl_file.fopen(v_log_path, v_filename , 'W');
  utl_file.put_line(v_log_filename,'RUN_DATE'||'|'||'ACTUAL '||'|'||'TARGET'||'|'|| 'RUNNING'||'|'||'PENDING'||'|'||'STATUS' ||'|'||'CONCURRENT_NAME' );
  
  SELECT TO_CHAR(sysdate - 1,'ddmmyyyy') INTO v_rundtm FROM dual;
  
  FOR i IN conc_que
  LOOP
    SELECT
      /*+ RULE */
      NVL(SUM(DECODE(phase_code, 'R', 1, 0)), 0),
      NVL(SUM(DECODE(phase_code, 'P', 1, 0)), 0)
    INTO running_count,
      pending_count
    FROM fnd_concurrent_worker_requests
    WHERE requested_start_date <= sysdate
    AND concurrent_queue_id     = i.concurrent_queue_id
    AND hold_flag              != 'Y';
    --for each manager get the list of requests pending due to conflicts in each manager
    SELECT
      /*+ RULE */
      COUNT(1)
    INTO crm_pend_count
    FROM apps.fnd_concurrent_worker_requests a
    WHERE concurrent_queue_id = 4
    AND hold_flag            != 'Y'
    AND requested_start_date <= sysdate
    AND EXISTS
      (SELECT 'x'
      FROM apps.fnd_concurrent_worker_requests b
      WHERE a.request_id        =b.request_id
      AND concurrent_queue_id   = i.concurrent_queue_id
      AND hold_flag            != 'Y'
      AND requested_start_date <= sysdate
      );
	utl_file.put_line(v_log_filename, TO_CHAR(sysdate,'dd-mon-yyyy hh24:mi:ss') || '|' || i.running_processes|| '|'  || i.max_processes || '|' || running_count || '|' || pending_count|| '|' || i.CONTROL_FLAG || '|' || i.user_concurrent_queue_name) ;
    
  END LOOP;
  utl_file.fclose_all;
END;
/
EXIT;
