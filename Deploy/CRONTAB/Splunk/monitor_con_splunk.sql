@/home/oraprod/SCRIPT/MONITOR_ERP/connsql.sql
SET serveroutput ON size 1000000
SET linesize 10000
col USER_CONCURRENT_QUEUE_NAME FORMAT a80;
col max_processes FORMAT a10;
col running_processes FORMAT a10;
col running_count FORMAT a10;
col pending_count FORMAT a80;
col crm_pend_count FORMAT a10;
col CONTROL_FLAG FORMAT a30;
col DESCRIPTION FORMAT a80;



DECLARE
  -- Aug 22,2017 Create monitoring concurrent daily to feed as a source for splunk dashboard by Tantida Yod.
  utl_buff VARCHAR2(30000);
  v_log_filename utl_file.file_type;
  v_log_path     VARCHAR2(200) := '/usr/tmp';
  v_filename     VARCHAR2(200) := '&1';
  v_rundtm       VARCHAR2(200);
  v_number       NUMBER := 0;
  running_count  NUMBER := 0;
  pending_count  NUMBER := 0;
  crm_pend_count NUMBER := 0;
  CURSOR conc_count
  IS
    SELECT sysdate RUN_DATE,
      'H' L_TYPE,
      SUM(C_All) TOTAL,
      SUM(C_NORMAL) "NORMAL",
      SUM(C_WARNING) "WARNING",
      SUM(C_ERROR) "ERROR"
    FROM
      (SELECT
        CASE
          WHEN STATUS_DESC = 'Normal'
          THEN 1
          ELSE 0
        END C_NORMAL,
        CASE
          WHEN STATUS_DESC = 'Warning'
          THEN 1
          ELSE 0
        END C_WARNING,
        CASE
          WHEN STATUS_DESC = 'Error'
          THEN 1
          ELSE 0
        END C_ERROR,
        1 C_All
      FROM TAC_CONCURRENT_MONITOR_V
      ) ;
  CURSOR conc_err_detail
  IS
    SELECT sysdate RUN_DATE,
      'D' L_TYPE,
      tcm.ACTUAL_COMPLETION_DATE,
      tcm.REQUEST_ID ,
      tcm.CONCURRENT_PROGRAM_NAME "CON_NAME",
      tcm.STATUS_DESC "STATUS",
      tcm.COMPLETION_TEXT "ERR_DESC"
    FROM TAC_CONCURRENT_MONITOR_V tcm
    WHERE tcm.STATUS_DESC IN ('Warning','Error')
    ;
  
BEGIN
  v_log_filename := utl_file.fopen(v_log_path, v_filename , 'W');
  --utl_file.put_line(v_log_filename,'ACTUAL '||'|'||'TARGET'||'|'|| 'RUNNING'||'|'||'PENDING'||'|'||'STATUS' ||'|'||'CONCURRENT_NAME' );
  SELECT TO_CHAR(sysdate - 1,'ddmmyyyy')
  INTO v_rundtm
  FROM dual;
  FOR i IN conc_count
  LOOP
    --utl_file.put_line(v_log_filename, i.running_processes|| '|' || i.max_processes || '|' || running_count || '|' || pending_count|| '|' || i.CONTROL_FLAG || '|' || i.user_concurrent_queue_name) ;
    --DBMS_OUTPUT.PUT_LINE(i.L_TYPE|| '|' ||TO_CHAR(i.RUN_DATE,'dd/mm/yyyy')|| '|' ||i.TOTAL|| '|' ||i.NORMAL|| '|' ||i.WARNING|| '|' ||i.ERROR);
	utl_file.put_line(v_log_filename, i.L_TYPE|| '|' ||TO_CHAR(i.RUN_DATE,'dd-mon-yyyy hh24:mi:ss')|| '|' ||i.TOTAL|| '|' ||i.NORMAL|| '|' ||i.WARNING|| '|' ||i.ERROR) ;
  END LOOP;
  
  FOR j in conc_err_detail
  LOOP
    --DBMS_OUTPUT.PUT_LINE(j.L_TYPE|| '|' ||TO_CHAR(j.RUN_DATE,'dd/mm/yyyy')|| '|' ||TO_CHAR(j.ACTUAL_COMPLETION_DATE,'dd/mm/yyyy')|| '|' ||j.REQUEST_ID|| '|' ||j.CON_NAME|| '|' ||j.STATUS|| '|' ||j.ERR_DESC);
	utl_file.put_line(v_log_filename, j.L_TYPE|| '|' ||TO_CHAR(j.RUN_DATE,'dd-mon-yyyy hh24:mi:ss')|| '|' ||TO_CHAR(j.ACTUAL_COMPLETION_DATE,'dd-mon-yyyy hh24:mi:ss')|| '|' ||j.REQUEST_ID|| '|' ||j.CON_NAME|| '|' ||j.STATUS|| '|' ||j.ERR_DESC) ;
  END LOOP;
  utl_file.fclose_all;
END;
/
EXIT;
