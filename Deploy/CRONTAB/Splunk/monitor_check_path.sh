IsMissing="false"

while read line
do
  lineCnt=`df | grep $line | wc -l`
#  echo $lineCnt
  if [ $lineCnt -eq 0 ]
then
   IsMissing="true"
   ListMissing=`echo "$line\n$ListMissing"`
fi
done < check_MAP_Path.txt
#echo $IsMissing
  if [ $IsMissing = "true" ]
then
   echo "-----------------------------"
   echo "FAIL! Path Missing:" 
   echo "-----------------------------"
   echo "$ListMissing"
 else
   echo "-----------------------------"
   echo "SUCCESS! All Path is here:"
   echo "-----------------------------"
   df | tr -s ' ' | cut -d ' ' -f 2
fi

