lineCnt=`echo "exit" | sqlplus apps/cl9ofkiNf_crUdgd0@hydrus22:1521/PROD | grep Connected | wc -l`
if [ $lineCnt -eq 0 ];
then
   date '+%d/%m/%Y %H:%M:%S|Hydrus22|DOWN' >> "/MP/ERP/DASHBOARD_LOG/LOG_DB_SERVICE/log_db_$(date +%Y-%m-%d).log"
else
   date '+%d/%m/%Y %H:%M:%S|Hydrus22|UP' >> "/MP/ERP/DASHBOARD_LOG/LOG_DB_SERVICE/log_db_$(date +%Y-%m-%d).log"
fi

find /MP/ERP/DASHBOARD_LOG/LOG_DB_SERVICE/ -type f -name '*.*' -mtime +30 -exec mv {} /MP/ERP/DASHBOARD_LOG/BACKUP \;
