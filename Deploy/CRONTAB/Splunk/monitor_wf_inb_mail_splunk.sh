. /usr/home/applprod/.profile

SC_PATH="/usr/home/applprod/SCRIPT/MONITOR_ERP/Splunk_Log_script"
SC_NAME="monitor_wf_inb_mail_splunk"
LOG_PATH="/MP/ERP/DASHBOARD_LOG/LOG_WF_INB_MAIL"
LOGSC="${SC_NAME}.log"
LOGNAME="${SC_NAME}_`echo $(date '+20%y%m%d_%H%M')`.log"

##############################################################################
# Aug 22, 2017		Create new script for monitoring concurrent manager to feed as a source for splunk dashboard by Tantida Yod.
##############################################
touch /usr/tmp/${LOGNAME}
sqlplus /nolog @${SC_PATH}/${SC_NAME}.sql  ${LOGNAME}
cp /usr/tmp/${LOGNAME}  ${LOG_PATH}
rm /usr/tmp/${LOGNAME}

chmod 644 ${LOG_PATH}/${LOGNAME}

find ${LOG_PATH} -type f -name '*.*' -mtime +30 -exec mv {} /MP/ERP/DASHBOARD_LOG/BACKUP \;

