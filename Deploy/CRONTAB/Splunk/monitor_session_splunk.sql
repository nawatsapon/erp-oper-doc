@/home/oraprod/SCRIPT/MONITOR_ERP/connsql.sql
SET serveroutput ON size 1000000
SET linesize 10000
col USER_CONCURRENT_QUEUE_NAME FORMAT a80;
col max_processes FORMAT a10;
col running_processes FORMAT a10;
col running_count FORMAT a10;
col pending_count FORMAT a80;
col crm_pend_count FORMAT a10;
col CONTROL_FLAG FORMAT a30;
col DESCRIPTION FORMAT a80;



DECLARE
  -- Aug 22,2017 Create monitoring concurrent daily to feed as a source for splunk dashboard by Tantida Yod.
  utl_buff VARCHAR2(30000);
  v_log_filename utl_file.file_type;
  v_log_path     VARCHAR2(200) := '/usr/tmp';
  v_filename     VARCHAR2(200) := '&1';
  v_number       NUMBER := 0;
  running_count  NUMBER := 0;
  pending_count  NUMBER := 0;
  crm_pend_count NUMBER := 0;
  CURSOR count_session
  IS
  WITH vs AS
  (SELECT rownum rnum,
    inst_id,
    sid,
    serial#,
    status,
    username,
    last_call_et,
    command,
    machine,
    osuser,
    module,
    action,
    resource_consumer_group,
    client_info,
    client_identifier,
    type,
    terminal,
    sql_id
  FROM gv$session
  )
SELECT sysdate run_date,
  'H' Line_Type,
  SUM(DECODE(status,'active',1,0)) count_active,
  SUM(DECODE(status,'inactive',1,0)) count_inactive,
  sum(case when status <> 'active' AND status <> 'inactive' THEN '1' end) count_other,
  COUNT(*) Total
FROM
  (SELECT vs.inst_id,
    vs.sid ,
    serial# serial,
    vs.sql_id,
    vs.username uname,
    CASE
      WHEN vs.status = 'ACTIVE'
      THEN last_call_et
      ELSE NULL
    END SECONDS_IN_WAIT,
    DECODE(vs.command, 0,NULL, 1,'CRE TAB', 2,'INSERT', 3,'SELECT', 4,'CRE CLUSTER', 5,'ALT CLUSTER', 6,'UPDATE', 7,'DELETE', 8,'DRP CLUSTER', 9,'CRE INDEX', 10,'DROP INDEX', 11,'ALT INDEX', 12,'DROP TABLE', 13,'CRE SEQ', 14,'ALT SEQ', 15,'ALT TABLE', 16,'DROP SEQ', 17,'GRANT', 18,'REVOKE', 19,'CRE SYN', 20,'DROP SYN', 21,'CRE VIEW', 22,'DROP VIEW', 23,'VAL INDEX', 24,'CRE PROC', 25,'ALT PROC', 26,'LOCK TABLE', 28,'RENAME', 29,'COMMENT', 30,'AUDIT', 31,'NOAUDIT', 32,'CRE DBLINK', 33,'DROP DBLINK', 34,'CRE DB', 35,'ALTER DB', 36,'CRE RBS', 37,'ALT RBS', 38,'DROP RBS', 39,'CRE TBLSPC', 40,'ALT TBLSPC', 41,'DROP TBLSPC', 42,'ALT SESSION', 43,'ALT USER', 44,'COMMIT', 45,'ROLLBACK', 46,'SAVEPOINT', 47,'PL/SQL EXEC', 48,'SET XACTN', 49,'SWITCH LOG', 50,'EXPLAIN', 51,'CRE USER', 52,'CRE ROLE', 53,'DROP USER', 54,'DROP ROLE', 55,'SET ROLE', 56,'CRE SCHEMA', 57,'CRE CTLFILE', 58,'ALTER TRACING', 59,'CRE TRIGGER', 60,'ALT TRIGGER', 61,'DRP TRIGGER', 62,'ANALYZE TAB', 63,'ANALYZE IX', 64,
    'ANALYZE CLUS', 65,'CRE PROFILE', 66,'DRP PROFILE', 67,'ALT PROFILE', 68,'DRP PROC', 69,'DRP PROC', 70,'ALT RESOURCE', 71,'CRE SNPLOG', 72,'ALT SNPLOG', 73,'DROP SNPLOG', 74,'CREATE SNAP', 75,'ALT SNAP', 76,'DROP SNAP', 79,'ALTER ROLE', 79,'ALTER ROLE', 85,'TRUNC TAB', 86,'TRUNC CLUST', 88,'ALT VIEW', 91,'CRE FUNC', 92,'ALT FUNC', 93,'DROP FUNC', 94,'CRE PKG', 95,'ALT PKG', 96,'DROP PKG', 97,'CRE PKG BODY', 98,'ALT PKG BODY', 99,'DRP PKG BODY', TO_CHAR(vs.command)) COMMAND,
    vs.machine MACHINE,
    vs.osuser osuser,
    lower(vs.status) status,
    vs.module module,
    vs.action ACTION,
    vs.resource_consumer_group,
    vs.client_info,
    vs.client_identifier
  FROM vs
  WHERE vs.USERNAME      IS NOT NULL
  AND NVL(vs.osuser,'x') <> 'SYSTEM'
  AND vs.type            <> 'BACKGROUND'
  ORDER BY 6 DESC
  ); 
  CURSOR top_10_session
  IS
    WITH vs AS
  (SELECT rownum rnum,
    inst_id,
    sid,
    serial#,
    status,
    username,
    last_call_et,
    command,
    machine,
    osuser,
    module,
    action,
    resource_consumer_group,
    client_info,
    client_identifier,
    type,
    terminal,
    sql_id
  FROM gv$session
  )
SELECT sysdate run_date,
'D' Line_Type,
sid,
serial,
SQL_ID,
uname,
SECONDS_IN_WAIT,
COMMAND,
MACHINE,
osuser,
status,
module,
ACTION,
resource_consumer_group,
client_info,
client_identifier
FROM
  (SELECT vs.inst_id,
    vs.sid ,
    serial# serial,
    vs.sql_id,
    vs.username uname,
    CASE
      WHEN vs.status = 'ACTIVE'
      THEN last_call_et
      ELSE NULL
    END SECONDS_IN_WAIT,
    DECODE(vs.command, 0,NULL, 1,'CRE TAB', 2,'INSERT', 3,'SELECT', 4,'CRE CLUSTER', 5,'ALT CLUSTER', 6,'UPDATE', 7,'DELETE', 8,'DRP CLUSTER', 9,'CRE INDEX', 10,'DROP INDEX', 11,'ALT INDEX', 12,'DROP TABLE', 13,'CRE SEQ', 14,'ALT SEQ', 15,'ALT TABLE', 16,'DROP SEQ', 17,'GRANT', 18,'REVOKE', 19,'CRE SYN', 20,'DROP SYN', 21,'CRE VIEW', 22,'DROP VIEW', 23,'VAL INDEX', 24,'CRE PROC', 25,'ALT PROC', 26,'LOCK TABLE', 28,'RENAME', 29,'COMMENT', 30,'AUDIT', 31,'NOAUDIT', 32,'CRE DBLINK', 33,'DROP DBLINK', 34,'CRE DB', 35,'ALTER DB', 36,'CRE RBS', 37,'ALT RBS', 38,'DROP RBS', 39,'CRE TBLSPC', 40,'ALT TBLSPC', 41,'DROP TBLSPC', 42,'ALT SESSION', 43,'ALT USER', 44,'COMMIT', 45,'ROLLBACK', 46,'SAVEPOINT', 47,'PL/SQL EXEC', 48,'SET XACTN', 49,'SWITCH LOG', 50,'EXPLAIN', 51,'CRE USER', 52,'CRE ROLE', 53,'DROP USER', 54,'DROP ROLE', 55,'SET ROLE', 56,'CRE SCHEMA', 57,'CRE CTLFILE', 58,'ALTER TRACING', 59,'CRE TRIGGER', 60,'ALT TRIGGER', 61,'DRP TRIGGER', 62,'ANALYZE TAB', 63,'ANALYZE IX', 64,
    'ANALYZE CLUS', 65,'CRE PROFILE', 66,'DRP PROFILE', 67,'ALT PROFILE', 68,'DRP PROC', 69,'DRP PROC', 70,'ALT RESOURCE', 71,'CRE SNPLOG', 72,'ALT SNPLOG', 73,'DROP SNPLOG', 74,'CREATE SNAP', 75,'ALT SNAP', 76,'DROP SNAP', 79,'ALTER ROLE', 79,'ALTER ROLE', 85,'TRUNC TAB', 86,'TRUNC CLUST', 88,'ALT VIEW', 91,'CRE FUNC', 92,'ALT FUNC', 93,'DROP FUNC', 94,'CRE PKG', 95,'ALT PKG', 96,'DROP PKG', 97,'CRE PKG BODY', 98,'ALT PKG BODY', 99,'DRP PKG BODY', TO_CHAR(vs.command)) COMMAND,
    vs.machine MACHINE,
    vs.osuser osuser,
    lower(vs.status) status,
    vs.module module,
    vs.action ACTION,
    vs.resource_consumer_group,
    vs.client_info,
    vs.client_identifier
  FROM vs
  WHERE vs.USERNAME      IS NOT NULL
  AND NVL(vs.osuser,'x') <> 'SYSTEM'
  AND vs.type            <> 'BACKGROUND'
  AND vs.status           = 'ACTIVE'
  ORDER BY 6 DESC
  )
WHERE rownum <=10 ; 
BEGIN
  v_log_filename := utl_file.fopen(v_log_path, v_filename , 'W');
  --utl_file.put_line(v_log_filename,'ACTUAL '||'|'||'TARGET'||'|'|| 'RUNNING'||'|'||'PENDING'||'|'||'STATUS' ||'|'||'CONCURRENT_NAME' );
  FOR i IN count_session
  LOOP
    utl_file.put_line(v_log_filename, i.Line_Type|| '|' ||TO_CHAR(i.RUN_DATE,'dd-mon-yyyy hh24:mi:ss')|| '|' ||i.TOTAL|| '|' ||i.count_active|| '|' ||i.count_inactive || '|' || i.count_other) ;
    --DBMS_OUTPUT.PUT_LINE(i.Line_Type|| '|' ||TO_CHAR(i.RUN_DATE,'dd-mon-yyyy hh24:mi:ss')|| '|' ||i.TOTAL|| '|' ||i.count_active|| '|' ||i.count_inactive);
  END LOOP;
  FOR j IN top_10_session
  LOOP
	utl_file.put_line(v_log_filename, j.Line_Type|| '|' ||TO_CHAR(j.RUN_DATE,'dd-mon-yyyy hh24:mi:ss')|| '|' ||j.sid|| '|' ||j.serial|| '|' ||j.SQL_ID|| '|' ||j.uname|| '|' ||j.SECONDS_IN_WAIT || '|' ||j.COMMAND || '|' ||j.MACHINE || '|' ||j.osuser || '|' ||j.status || '|' ||j.module || '|' ||j.ACTION ) ;
  --DBMS_OUTPUT.PUT_LINE(j.Line_Type|| '|' ||TO_CHAR(j.RUN_DATE,'dd-mon-yyyy hh24:mi:ss')|| '|' ||j.sid|| '|' ||j.serial|| '|' ||j.SQL_ID|| '|' ||j.uname|| '|' ||j.SECONDS_IN_WAIT || '|' ||j.COMMAND || '|' ||j.MACHINE || '|' ||j.osuser || '|' ||j.status || '|' ||j.module || '|' ||j.ACTION || '|' ||j.service_name || '|' ||j.program );
  END LOOP;
  utl_file.fclose_all;
END;
/
EXIT;

