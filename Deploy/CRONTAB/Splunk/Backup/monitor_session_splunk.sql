@/home/oraprod/SCRIPT/MONITOR_ERP/connsql.sql
SET serveroutput ON size 1000000
SET linesize 10000
col USER_CONCURRENT_QUEUE_NAME FORMAT a80;
col max_processes FORMAT a10;
col running_processes FORMAT a10;
col running_count FORMAT a10;
col pending_count FORMAT a80;
col crm_pend_count FORMAT a10;
col CONTROL_FLAG FORMAT a30;
col DESCRIPTION FORMAT a80;



DECLARE
  -- Aug 22,2017 Create monitoring concurrent daily to feed as a source for splunk dashboard by Tantida Yod.
  utl_buff VARCHAR2(30000);
  v_log_filename utl_file.file_type;
  v_log_path     VARCHAR2(200) := '/usr/tmp';
  v_filename     VARCHAR2(200) := '&1';
  v_number       NUMBER := 0;
  running_count  NUMBER := 0;
  pending_count  NUMBER := 0;
  crm_pend_count NUMBER := 0;
  CURSOR count_session
  IS
    SELECT sysdate run_date,
      'H' Line_Type,
      SUM(DECODE(status,'ACTIVE',1,0)) count_active,
      SUM(DECODE(status,'INACTIVE',1,0)) count_inactive,
      COUNT(*) Total
    FROM
      (SELECT NVL(s.username, '(oracle)') AS username,
        s.osuser,
        s.sid,
        s.serial#,
        p.spid,
        s.lockwait,
        s.status,
        s.service_name,
        TRUNC((sysdate - sql_exec_start) * 24*60*60,'00') SECONDS_IN_WAIT_1,
        s.module,
        s.machine,
        s.program,
        TO_CHAR(s.logon_Time,'DD-MON-YYYY HH24:MI:SS') AS logon_time,
        S.SECONDS_IN_WAIT
      FROM v$session s,
        v$process p
      WHERE s.paddr = p.addr
        --AND s.status  = 'ACTIVE'
        --AND S.OSUSER <> 'oracle'
      AND S.USERNAME <> '(oracle)'
      ORDER BY SECONDS_IN_WAIT_1 DESC
      ) ;
  CURSOR top_10_session
  IS
    SELECT *
    FROM
      (SELECT sysdate run_date,
        'D' Line_Type,
        s.sid,
        s.serial# AS serial,
        S.SQL_ID,
        NVL(s.username, '(oracle)') AS username,
        NVL(TRUNC((sysdate - sql_exec_start) * 24*60*60,'00'),0) SECONDS_IN_WAIT,
        S.COMMAND,
        S.MACHINE,
        s.osuser,
        s.status,
        s.module,
        S.ACTION,
        s.service_name,
        s.program
      FROM v$session s,
        v$process p
      WHERE s.paddr = p.addr
      AND s.status  = 'ACTIVE'
        --AND S.OSUSER <> 'oracle'
      AND S.USERNAME <> '(oracle)'
      ORDER BY SECONDS_IN_WAIT DESC
      )
  WHERE rownum <=10 ;
BEGIN
  v_log_filename := utl_file.fopen(v_log_path, v_filename , 'W');
  --utl_file.put_line(v_log_filename,'ACTUAL '||'|'||'TARGET'||'|'|| 'RUNNING'||'|'||'PENDING'||'|'||'STATUS' ||'|'||'CONCURRENT_NAME' );
  FOR i IN count_session
  LOOP
    utl_file.put_line(v_log_filename, i.Line_Type|| '|' ||TO_CHAR(i.RUN_DATE,'dd-mon-yyyy hh24:mi:ss')|| '|' ||i.TOTAL|| '|' ||i.count_active|| '|' ||i.count_inactive) ;
    --DBMS_OUTPUT.PUT_LINE(i.Line_Type|| '|' ||TO_CHAR(i.RUN_DATE,'dd-mon-yyyy hh24:mi:ss')|| '|' ||i.TOTAL|| '|' ||i.count_active|| '|' ||i.count_inactive);
  END LOOP;
  FOR j IN top_10_session
  LOOP
	utl_file.put_line(v_log_filename, j.Line_Type|| '|' ||TO_CHAR(j.RUN_DATE,'dd-mon-yyyy hh24:mi:ss')|| '|' ||j.sid|| '|' ||j.serial|| '|' ||j.SQL_ID|| '|' ||j.username|| '|' ||j.SECONDS_IN_WAIT || '|' ||j.COMMAND || '|' ||j.MACHINE || '|' ||j.osuser || '|' ||j.status || '|' ||j.module || '|' ||j.ACTION || '|' ||j.service_name || '|' ||j.program) ;
  --DBMS_OUTPUT.PUT_LINE(j.Line_Type|| '|' ||TO_CHAR(j.RUN_DATE,'dd-mon-yyyy hh24:mi:ss')|| '|' ||j.sid|| '|' ||j.serial|| '|' ||j.SQL_ID|| '|' ||j.username|| '|' ||j.SECONDS_IN_WAIT || '|' ||j.COMMAND || '|' ||j.MACHINE || '|' ||j.osuser || '|' ||j.status || '|' ||j.module || '|' ||j.ACTION || '|' ||j.service_name || '|' ||j.program );
  END LOOP;
  utl_file.fclose_all;
END;
/
EXIT;

