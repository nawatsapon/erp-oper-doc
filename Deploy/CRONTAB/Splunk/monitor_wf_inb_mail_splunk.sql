@/home/oraprod/SCRIPT/MONITOR_ERP/connsql.sql
SET serveroutput ON size 1000000
SET linesize 10000
col USER_CONCURRENT_QUEUE_NAME FORMAT a80;
col max_processes FORMAT a10;
col running_processes FORMAT a10;
col running_count FORMAT a10;
col pending_count FORMAT a80;
col crm_pend_count FORMAT a10;
col CONTROL_FLAG FORMAT a30;
col DESCRIPTION FORMAT a80;

DECLARE
  -- Aug 22,2017 Create monitor concurrent running to feed as a source for splunk dashboard by Tantida Yod.
  utl_buff VARCHAR2(30000);
  v_log_filename utl_file.file_type;
  v_log_path     VARCHAR2(200) := '/usr/tmp';
  v_filename     VARCHAR2(200) := '&1';
  v_number       NUMBER        := 0;
  running_count  NUMBER        := 0;
  pending_count  NUMBER        := 0;
  crm_pend_count NUMBER        := 0;
  CURSOR wf_mail_in
  IS
    SELECT Sysdate run_date,
      win.enq_time processed_time,
      wfn.status notification_status,
      CASE
        WHEN wfn.subject LIKE 'Expense%'
        THEN SUBSTR(wfn.subject,INSTR(wfn.subject,'EMP', 1, 1),INSTR(wfn.subject,' ', 1, 2)-INSTR(wfn.subject,'EMP', 1, 1))
        ELSE regexp_substr(wfn.subject,'[0-9]+')
      END PO_NO
    FROM
      (SELECT wi.msgid,
        wi.corrid,
        wi.enq_time,
        wi.state,
        wi.sender_name,
        (SELECT str_value
        FROM TABLE (wi.user_data.header.properties)
        WHERE NAME = 'NOTIFICATION_ID'
        ) notification_id
    FROM apps.wf_notification_in wi
      ) win,
      apps.wf_error werr,
      apps.wf_notifications wfn
    WHERE win.notification_id = wfn.notification_id
    AND win.msgid             = werr.msgid(+)
    AND wfn.status            = 'OPEN'
    ;
  BEGIN
    v_log_filename := utl_file.fopen(v_log_path, v_filename , 'W');
    --utl_file.put_line(v_log_filename,'ACTUAL '||'|'||'TARGET'||'|'|| 'RUNNING'||'|'||'PENDING'||'|'||'STATUS' ||'|'||'CONCURRENT_NAME' );
    FOR i IN wf_mail_in
    LOOP
      utl_file.put_line(v_log_filename, TO_CHAR(i.run_date,'dd-mon-yyyy hh24:mi:ss')|| '|' ||TO_CHAR(i.processed_time,'dd-mon-yyyy hh24:mi:ss')|| '|' ||i.notification_status|| '|' ||i.PO_NO) ;
      --DBMS_OUTPUT.PUT_LINE(TO_CHAR(i.run_date,'dd-mon-yyyy hh24:mi:ss')|| '|' ||TO_CHAR(i.processed_time,'dd-mon-yyyy hh24:mi:ss')|| '|' ||i.notification_status|| '|' ||i.PO_NO);
    END LOOP;
    utl_file.fclose_all;
  END;
  /
  EXIT;

