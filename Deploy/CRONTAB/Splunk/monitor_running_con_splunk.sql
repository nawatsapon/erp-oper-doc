@/home/oraprod/SCRIPT/MONITOR_ERP/connsql.sql
SET serveroutput ON size 1000000
SET linesize 10000
col USER_CONCURRENT_QUEUE_NAME FORMAT a80;
col max_processes FORMAT a10;
col running_processes FORMAT a10;
col running_count FORMAT a10;
col pending_count FORMAT a80;
col crm_pend_count FORMAT a10;
col CONTROL_FLAG FORMAT a30;
col DESCRIPTION FORMAT a80;

DECLARE
  -- Aug 22,2017 Create monitor concurrent running to feed as a source for splunk dashboard by Tantida Yod.
  utl_buff VARCHAR2(30000);
  v_log_filename utl_file.file_type;
  v_log_path     VARCHAR2(200) := '/usr/tmp';
  v_filename     VARCHAR2(200) := '&1';
  v_number       NUMBER        := 0;
  running_count  NUMBER        := 0;
  pending_count  NUMBER        := 0;
  crm_pend_count NUMBER        := 0;
  CURSOR con_running
  IS
    SELECT sysdate run_date,
      FCWR.REQUEST_ID,
      FCWR.REQUESTED_START_DATE,
      FCWR.CONCURRENT_PROGRAM_NAME,
      FCWR.REQUEST_DESCRIPTION,
      FCWR.ARGUMENT_TEXT,
      FNU.USER_NAME
      ||
      CASE
        WHEN PPF.FULL_NAME IS NOT NULL
        THEN ' : '
          ||PPF.FULL_NAME
      END REQUESTED_BY
    FROM FND_CONCURRENT_WORKER_REQUESTS FCWR,
      FND_USER FNU,
      PER_ALL_PEOPLE_F PPF
    WHERE 0                        =0
    AND FCWR.REQUESTED_BY          = FNU.USER_ID
    AND FNU.EMPLOYEE_ID            = PPF.PERSON_ID(+)
    AND FCWR.Phase_Code            = 'R'
    AND FCWR.hold_flag            != 'Y'
    AND FCWR.Requested_Start_Date <= SYSDATE
    AND (''                       IS NULL
    OR (''                         = 'B'
    AND FCWR.PHASE_CODE            = 'R'
    AND FCWR.STATUS_CODE          IN ('I', 'Q')))
    AND '1'                       IN (0,1,4)
    AND (FCWR.CONCURRENT_QUEUE_ID  =0)
    AND (FCWR.QUEUE_APPLICATION_ID =0)
    ORDER BY Priority,
      FCWR.Priority_Request_ID,
      FCWR.Request_ID;
BEGIN
  v_log_filename := utl_file.fopen(v_log_path, v_filename , 'W');
  --utl_file.put_line(v_log_filename,'ACTUAL '||'|'||'TARGET'||'|'|| 'RUNNING'||'|'||'PENDING'||'|'||'STATUS' ||'|'||'CONCURRENT_NAME' );
  FOR i IN con_running
  LOOP
    utl_file.put_line(v_log_filename, TO_CHAR(i.run_date,'dd-mon-yyyy hh24:mi:ss')|| '|' ||i.REQUEST_ID|| '|' ||TO_CHAR(i.REQUESTED_START_DATE,'dd-mon-yyyy hh24:mi:ss')|| '|' ||i.CONCURRENT_PROGRAM_NAME|| '|' ||i.REQUEST_DESCRIPTION|| '|' ||i.ARGUMENT_TEXT|| '|' ||i.REQUESTED_BY) ;
    --DBMS_OUTPUT.PUT_LINE(TO_CHAR(i.run_date,'dd-mon-yyyy hh24:mi:ss')|| '|' ||i.REQUEST_ID|| '|' ||TO_CHAR(i.REQUESTED_START_DATE,'dd-mon-yyyy hh24:mi:ss')|| '|' ||i.CONCURRENT_PROGRAM_NAME|| '|' ||i.REQUEST_DESCRIPTION|| '|' ||i.ARGUMENT_TEXT|| '|' ||i.REQUESTED_BY);
  END LOOP;
  utl_file.fclose_all;
END;
/
EXIT;

