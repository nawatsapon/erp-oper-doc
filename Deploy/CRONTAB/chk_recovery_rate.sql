@/usr/home/applprod/SCRIPT/MONITOR_ERP/connsql.sql
DECLARE
  CURSOR cur_st
  IS
    SELECT
      CASE
        WHEN PO_NO LIKE '991%'
        THEN 'PO-HO'
        WHEN PO_NO LIKE '992%'
        THEN 'PO-PSL'
        WHEN PO_NO LIKE '993%'
        THEN 'PO-KKC'
        WHEN PO_NO LIKE '994%'
        THEN 'PO-SRC'
        WHEN PO_NO LIKE '995%'
        THEN 'PO-SRT'
        WHEN PO_NO LIKE '801%'
        THEN 'DTN-PO'
        WHEN PO_NO LIKE '121%'
        THEN 'PB_PO'
        WHEN PO_NO LIKE '151%'
        THEN 'PO-DTAC-A'
        WHEN PO_NO LIKE '601%'
        THEN 'PO-DTAC-I'
        WHEN PO_NO LIKE '701%'
        THEN 'PO-DTAC-W'
      END
      || '|'
      || TO_CHAR(PO_LIST.PO_NO)
      || '|'
      || TO_CHAR(PO_LIST.LINE_NUM)
      || '|'
      || TO_CHAR(PO_LIST.APPROVE_DATE)
      || '|'
      || PO_LIST.AUTHORIZATION_STATUS
      || '|'
      || TO_CHAR(PO_LIST.NONRECOVERABLE_TAX)
      || '|'
      || TO_CHAR(PO_LIST.RECOVERABLE_TAX)
      || '|'
      || TO_CHAR(PO_LIST.TAX_CODE_ID) V_OUTPUT
      --PO_LIST.*
    FROM
      ( SELECT DISTINCT PO_HEADERS_ALL.SEGMENT1 PO_NO,
        PO_LINES_ALL.LINE_NUM LINE_NUM,
        TO_CHAR(PO_HEADERS_ALL.APPROVED_DATE,'dd/mm/yyyy HH24:MI:SS') APPROVE_DATE,
        PO_HEADERS_ALL.AUTHORIZATION_STATUS,
        PO_DISTRIBUTIONS_ALL.NONRECOVERABLE_TAX,
        PO_DISTRIBUTIONS_ALL.RECOVERABLE_TAX,
        PO_LINE_LOCATIONS_ALL.TAX_CODE_ID
      FROM PO.PO_HEADERS_ALL,
        PO.PO_LINES_ALL,
        PO.PO_LINE_LOCATIONS_ALL,
        PO.PO_DISTRIBUTIONS_ALL
      WHERE PO_HEADERS_ALL.PO_HEADER_ID        = PO_LINES_ALL.PO_HEADER_ID
      AND PO_HEADERS_ALL.PO_HEADER_ID          = PO_LINE_LOCATIONS_ALL.PO_HEADER_ID
      AND PO_LINES_ALL.PO_LINE_ID              = PO_LINE_LOCATIONS_ALL.PO_LINE_ID
      AND PO_LINES_ALL.PO_LINE_ID              = PO_DISTRIBUTIONS_ALL.PO_LINE_ID
      AND TRUNC(PO_HEADERS_ALL.APPROVED_DATE) >= sysdate-1
      AND PO_HEADERS_ALL.TYPE_LOOKUP_CODE      = 'STANDARD'
      AND PO_LINES_ALL.FROM_HEADER_ID         IS NOT NULL
      AND (PO_DISTRIBUTIONS_ALL.RECOVERY_RATE IS NULL
      OR PO_DISTRIBUTIONS_ALL.RECOVERY_RATE    < 100)
      AND PO_HEADERS_ALL.CANCEL_FLAG          IS NULL -- SWE 280509
      AND PO_HEADERS_ALL.TYPE_LOOKUP_CODE     <> 'PLANNED'
      ORDER BY 1,2,3,4
      ) PO_LIST ;
  cur_fn cur_st%rowtype;
  v_log_filename utl_file.file_type;
  v_log_path VARCHAR2(200) := '/usr/tmp';
  v_filename VARCHAR2(200) := '&1';
BEGIN
  BEGIN --- Write File Complete ---
    v_log_filename := utl_file.fopen(v_log_path, v_filename, 'W');
  END;
  utl_file.put_line(v_log_filename,'PO_MENU|PO_No|RELEASE_NUM|RELEASE_DATE|APPROVE_DATE|AUTHORIZATION_STATUS|NONRECOVERABLE_TAX');
  OPEN cur_st;
  LOOP
    FETCH cur_st INTO cur_fn;
    EXIT
  WHEN cur_st%notfound;
    utl_file.put_line(v_log_filename, cur_fn.v_output);
  END LOOP;
  CLOSE cur_st;
  utl_file.fclose(v_log_filename);
END;
/
EXIT;
