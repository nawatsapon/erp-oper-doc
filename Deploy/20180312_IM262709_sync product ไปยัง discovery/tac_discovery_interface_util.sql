create or replace package body tac_discovery_interface_util is

  --error_open_file exception;
  error_output_path exception;
  error_file_name exception;

  procedure conc_wait(p_conc_req_id number) is
	phase varchar2(30);
	status varchar2(30);
	dev_phase varchar2(30);
	dev_status varchar2(30);
	message varchar2(1000);
  begin
	commit;
	if fnd_concurrent.wait_for_request(p_conc_req_id, 10, 0, phase, status, dev_phase, dev_status, message) then
	  null;
	end if;
  end conc_wait;

  procedure conc_wait
  (
	p_conc_req_id number
   ,p_phase out nocopy varchar2
   ,p_status out nocopy varchar2
   ,p_message out nocopy varchar2
   ,p_max_wait number default 60
  ) is
	v_phase varchar2(30);
	v_status varchar2(30);
	v_dev_phase varchar2(30);
	v_dev_status varchar2(30);
	v_message varchar2(1000);
	v_wait_complete boolean;
  begin
	commit;
	if p_conc_req_id > 0 then
	  v_wait_complete := fnd_concurrent.wait_for_request(request_id => p_conc_req_id
														,interval => 10
														,max_wait => p_max_wait
														 -- out arguments
														,phase => v_phase
														,status => v_status
														,dev_phase => v_dev_phase
														,dev_status => v_dev_status
														,message => v_message);
	end if;
	p_phase := v_phase;
	p_status := v_status;
	p_message := v_message;
  end conc_wait;

  procedure write_log(p_msg varchar2) is
  begin
	fnd_file.put_line(fnd_file.log, p_msg);
  exception
	when others then
	  null;
  end write_log;

  procedure write_output(p_msg varchar2) is
  begin
	fnd_file.put_line(fnd_file.output, p_msg);
  exception
	when others then
	  null;
  end write_output;

  procedure write_line
  (
	p_file_handle in out utl_file.file_type
   ,p_line_buff in varchar2
  ) is
	user_error varchar2(255); -- to store translated file_error
  begin

	utl_file.put_line(p_file_handle, p_line_buff);
	utl_file.fflush(p_file_handle);
  exception
	when utl_file.invalid_filehandle then
	  user_error := 'Invalid filehandle';
	  write_log(user_error);
	when utl_file.invalid_operation then
	  user_error := 'Invalid operation';
	  write_log(user_error);
	when utl_file.write_error then
	  user_error := 'Write error';
	  write_log(user_error);
	when others then
	  user_error := 'Error others.';
	  write_log(user_error);
  end write_line;

  procedure write_line_nchar
  (
	p_file_handle in out utl_file.file_type
   ,p_line_buff in nvarchar2
  ) is
	user_error varchar2(255); -- to store translated file_error
  begin

	utl_file.put_line_nchar(p_file_handle, p_line_buff);
	utl_file.fflush(p_file_handle);
  exception
	when utl_file.invalid_filehandle then
	  user_error := 'Invalid filehandle';
	  write_log(user_error);
	when utl_file.invalid_operation then
	  user_error := 'Invalid operation';
	  write_log(user_error);
	when utl_file.write_error then
	  user_error := 'Write error';
	  write_log(user_error);
	when others then
	  user_error := 'Error others.';
	  write_log(user_error);
  end write_line_nchar;

  function get_directory_path(p_directory_name varchar2) return varchar2 is
	v_path varchar2(250);
  begin
	select directory_path into v_path from all_directories where directory_name = p_directory_name;

	return v_path;
  exception
	when others then
	  return null;
  end get_directory_path;

  function char_to_date
  (
	p_date_char varchar2
   ,p_format_mask varchar2 default null
  ) return date is
	v_date date;
  begin
	if (p_date_char is null) then
	  return null;
	end if;

	if (p_format_mask is not null) then
	  v_date := to_date(p_date_char, p_format_mask);
	  return v_date;
	end if;

	-- Format Mask = dd/mm/yyyy hh24:mi:ss
	begin
	  v_date := to_date(p_date_char, 'dd/mm/yyyy hh24:mi:ss');
	  return v_date;
	exception
	  when others then
		null;
	end;

	-- Format Mask = dd/mm/yyyy
	begin
	  v_date := to_date(p_date_char, 'dd/mm/yyyy');
	  return v_date;
	exception
	  when others then
		null;
	end;

	-- Format Mask = dd/m/yyyy
	begin
	  v_date := to_date(p_date_char, 'dd/m/yyyy');
	  return v_date;
	exception
	  when others then
		null;
	end;

	-- Format Mask = d/mm/yyyy
	begin
	  v_date := to_date(p_date_char, 'd/mm/yyyy');
	  return v_date;
	exception
	  when others then
		null;
	end;

	-- Format Mask = d/m/yyyy
	begin
	  v_date := to_date(p_date_char, 'd/m/yyyy');
	  return v_date;
	exception
	  when others then
		return p_date_char;
	end;
  end char_to_date;

  function extrack_data
  (
	p_buffer in varchar2
   ,p_separator in varchar2
   ,p_pos_data in number
  ) return varchar2 is
	v_pos_start number;
	v_pos_end number;
	v_subsrt_len number;
	v_data varchar2(255);
  begin
	if p_pos_data = 1 then
	  v_pos_start := 1;
	else
	  v_pos_start := instr(p_buffer, p_separator, 1, p_pos_data - 1) + 1;
	end if;

	v_pos_end := instr(p_buffer, p_separator, 1, p_pos_data);

	if v_pos_end = 0 then
	  v_subsrt_len := length(p_buffer);
	else
	  v_subsrt_len := v_pos_end - v_pos_start;
	end if;

	v_data := substr(p_buffer, v_pos_start, v_subsrt_len);

	return v_data;
  exception
	when others then
	  return null;
  end extrack_data;

  function split_grn_data
  (
	p_line_buff in varchar2
   ,p_delimiter in varchar2 default '|'
   ,p_debug_flag varchar2 default 'N'
  ) return dtac_grn_interface_temp %rowtype is
	v_rec_data dtac_grn_interface_temp%rowtype;
  begin
	if p_line_buff is null then
	  return v_rec_data;
	end if;
	v_rec_data.company_code := extrack_data(p_line_buff, p_delimiter, 1);
	v_rec_data.transaction_type := extrack_data(p_line_buff, p_delimiter, 2);
	v_rec_data.grn_number := extrack_data(p_line_buff, p_delimiter, 3);
	v_rec_data.po_number := extrack_data(p_line_buff, p_delimiter, 4);
	v_rec_data.release_number := extrack_data(p_line_buff, p_delimiter, 5);
	v_rec_data.po_line_number := extrack_data(p_line_buff, p_delimiter, 6);
	v_rec_data.product_sku_code := extrack_data(p_line_buff, p_delimiter, 7);

	v_rec_data.invoice_number := extrack_data(p_line_buff, p_delimiter, 8);
	v_rec_data.rate_date := char_to_date(extrack_data(p_line_buff, p_delimiter, 9), g_datechar_format);
	v_rec_data.qty := extrack_data(p_line_buff, p_delimiter, 10);
	v_rec_data.inventory_org_code := extrack_data(p_line_buff, p_delimiter, 11);
	v_rec_data.sub_inventory := extrack_data(p_line_buff, p_delimiter, 12);
	v_rec_data.locator := extrack_data(p_line_buff, p_delimiter, 13);
	v_rec_data.create_by := extrack_data(p_line_buff, p_delimiter, 14);
	v_rec_data.creation_date := char_to_date(extrack_data(p_line_buff, p_delimiter, 15), g_datechar_format);

	if p_debug_flag = 'Y' then
	  write_log(g_log_line);
	  write_log('Line data = ' || p_line_buff);
	  write_log('        company_code  = ' || v_rec_data.company_code);
	  write_log('        transaction_type  = ' || v_rec_data.transaction_type);
	  write_log('        grn_number  = ' || v_rec_data.grn_number);
	  write_log('        po_number  = ' || v_rec_data.po_number);
	  write_log('        release_number  = ' || v_rec_data.release_number);
	  write_log('        po_line_number  = ' || v_rec_data.po_line_number);
	  write_log('        product_sku_code  = ' || v_rec_data.product_sku_code);
	  write_log('        invoice_number  = ' || v_rec_data.invoice_number);
	  write_log('        rate_date  = ' || v_rec_data.rate_date);
	  write_log('        qty  = ' || v_rec_data.qty);
	  write_log('        inventory_org_code  = ' || v_rec_data.inventory_org_code);
	  write_log('        sub_inventory  = ' || v_rec_data.sub_inventory);
	  write_log('        locator  = ' || v_rec_data.locator);
	  write_log('        create_by  = ' || v_rec_data.create_by);
	  write_log('        creation_date  = ' || v_rec_data.creation_date);
	end if;

	return v_rec_data;
  exception
	when others then
	  --write_log('Error @tac_ap_interface_util_smt.split_grn_data : ' || sqlcode || ' - ' || sqlerrm);
	  v_rec_data.error_msg := sqlcode || ' - ' || sqlerrm;
	  return v_rec_data;
  end split_grn_data;

  function split_gl_data
  (
	p_line_buff in varchar2
   ,p_delimiter in varchar2 default '|'
   ,p_debug_flag varchar2 default 'N'
  ) return dtac_gl_interface_temp %rowtype is
	v_rec_data dtac_gl_interface_temp%rowtype;
  begin
	if p_line_buff is null then
	  return v_rec_data;
	end if;
	v_rec_data.status := extrack_data(p_line_buff, p_delimiter, 1);
	v_rec_data.set_of_books_id := extrack_data(p_line_buff, p_delimiter, 2);
	v_rec_data.accounting_date := char_to_date(extrack_data(p_line_buff, p_delimiter, 3), 'dd/mm/yyyy');
	v_rec_data.currency_code := extrack_data(p_line_buff, p_delimiter, 4);
	v_rec_data.date_created := char_to_date(extrack_data(p_line_buff, p_delimiter, 5), 'dd/mm/yy');
	v_rec_data.created_by := extrack_data(p_line_buff, p_delimiter, 6);
	v_rec_data.actual_flag := extrack_data(p_line_buff, p_delimiter, 7);
	v_rec_data.user_je_category_name := extrack_data(p_line_buff, p_delimiter, 8);
	v_rec_data.user_je_source_name := extrack_data(p_line_buff, p_delimiter, 9);
	v_rec_data.accounted_dr := extrack_data(p_line_buff, p_delimiter, 10);
	v_rec_data.accounted_cr := extrack_data(p_line_buff, p_delimiter, 11);
	v_rec_data.currency_conversion_date := char_to_date(extrack_data(p_line_buff, p_delimiter, 12), g_datechar_format);
	v_rec_data.currency_conversion_rate := extrack_data(p_line_buff, p_delimiter, 13);
	v_rec_data.entered_dr := extrack_data(p_line_buff, p_delimiter, 14);
	v_rec_data.entered_cr := extrack_data(p_line_buff, p_delimiter, 15);
	v_rec_data.je_line_num := extrack_data(p_line_buff, p_delimiter, 16);
	v_rec_data.period_name := upper(extrack_data(p_line_buff, p_delimiter, 17));
	v_rec_data.reference1 := extrack_data(p_line_buff, p_delimiter, 18);
	v_rec_data.reference4 := extrack_data(p_line_buff, p_delimiter, 19);
	v_rec_data.reference5 := extrack_data(p_line_buff, p_delimiter, 20);
	v_rec_data.segment1 := extrack_data(p_line_buff, p_delimiter, 21);
	v_rec_data.segment11 := extrack_data(p_line_buff, p_delimiter, 22);
	v_rec_data.segment10 := extrack_data(p_line_buff, p_delimiter, 23);
	v_rec_data.segment12 := extrack_data(p_line_buff, p_delimiter, 24);
	v_rec_data.segment13 := extrack_data(p_line_buff, p_delimiter, 25);
	v_rec_data.segment14 := extrack_data(p_line_buff, p_delimiter, 26);
	v_rec_data.segment18 := extrack_data(p_line_buff, p_delimiter, 27);
	v_rec_data.segment19 := extrack_data(p_line_buff, p_delimiter, 28);
	v_rec_data.segment21 := extrack_data(p_line_buff, p_delimiter, 29);
	v_rec_data.reference21 := extrack_data(p_line_buff, p_delimiter, 30);

	if p_debug_flag = 'Y' then
	  write_log(g_log_line);
	  write_log('Line data = ' || p_line_buff);
	  /*
      write_log('        company_code  = ' || v_rec_data.company_code);
      write_log('        transaction_type  = ' || v_rec_data.transaction_type);
      write_log('        grn_number  = ' || v_rec_data.grn_number);
      write_log('        po_number  = ' || v_rec_data.po_number);
      write_log('        release_number  = ' || v_rec_data.release_number);
      write_log('        po_line_number  = ' || v_rec_data.po_line_number);
      write_log('        product_sku_code  = ' || v_rec_data.product_sku_code);
      write_log('        invoice_number  = ' || v_rec_data.invoice_number);
      write_log('        rate_date  = ' || v_rec_data.rate_date);
      write_log('        qty  = ' || v_rec_data.qty);
      write_log('        inventory_org_code  = ' || v_rec_data.inventory_org_code);
      write_log('        sub_inventory  = ' || v_rec_data.sub_inventory);
      write_log('        locator  = ' || v_rec_data.locator);
      write_log('        create_by  = ' || v_rec_data.create_by);
      write_log('        creation_date  = ' || v_rec_data.creation_date);
      */
	end if;

	return v_rec_data;
  exception
	when others then
	  --write_log('Error @tac_ap_interface_util_smt.split_grn_data : ' || sqlcode || ' - ' || sqlerrm);
	  v_rec_data.error_msg := sqlcode || ' - ' || sqlerrm;
	  return v_rec_data;
  end split_gl_data;

  procedure move_data_file
  (
	p_source_file varchar2
   ,p_dest_path varchar2
  ) is
	v_movefile_req_id number;
	v_phase varchar2(100);
	v_status varchar2(100);
	v_message varchar2(1000);
  begin
	write_log(' ');
	write_log('Moving File ' || p_source_file || ' to ' || p_dest_path);

	v_movefile_req_id := fnd_request.submit_request(application => 'FND'
												   ,program => 'TACFND_MVFILE'
												   ,argument1 => p_source_file
												   ,argument2 => p_dest_path);
	commit;
	write_log('Concurrent Move File is ' || v_movefile_req_id);

	if (v_movefile_req_id > 0) then
	  conc_wait(v_movefile_req_id, v_phase, v_status, v_message, 60);
	  if (v_status = 'E') then
		write_log('Move file from ' || p_source_file || ' to ' || p_dest_path || ' failed.');
	  else
		write_log('Move file from ' || p_source_file || ' to ' || p_dest_path || ' successful.');
	  end if;
	else
	  write_log('Move file from ' || p_source_file || ' to ' || p_dest_path || ' faile, No concurrent program [TACPO_MVFILE_INF].');
	end if;
	write_log(' ');
  exception
	when others then
	  write_log('Move file from ' || p_source_file || ' to ' || p_dest_path || ' failed with error ' || sqlerrm);
  end move_data_file;

  procedure export_product_master
  (
	err_msg out varchar2
   ,err_code out varchar2
   ,p_interface_type varchar2
   ,p_increment_date varchar2
   ,p_output_path varchar2
   ,p_file_name_format varchar2
   ,p_separator varchar2 default '|'
  ) is

	v_file_handle utl_file.file_type;
	v_line_data nvarchar2(32767);
	v_file_name varchar2(100);
	v_date date;
	v_line_count number;
	v_file_date date := sysdate;
	v_bom_raw raw(3);
	v_put_bom_flag varchar2(10) := 'N';
  begin
	write_log(' ');
	write_log('+---------------------------------------------------------------------------+');
	write_log('Start Concurrent export Product Master to Discovery ');
	write_log('Start Date: ' || to_char(sysdate, 'DD/MM/YYYY HH24:MI:SS'));
	write_log(' ');
	write_log('Agruments');
	write_log('-----------');
	write_log('p_interface_type =  ' || p_interface_type);
	write_log('p_increment_date =  ' || p_increment_date);
	write_log('p_output_path =  ' || p_output_path);
	write_log('p_file_name_format =  ' || p_file_name_format);
	write_log('p_separator =  ' || p_separator);
	write_log('+----------------------------------------------------------------------------+');

	-- validate parameter
	if p_output_path is null then
	  raise utl_file.invalid_path;
	end if;
	if p_file_name_format is null then
	  raise utl_file.invalid_filename;
	end if;
	if p_increment_date is null then
	  v_date := trunc(sysdate) - 1;
	  v_file_date := sysdate;
	else
	  v_date := trunc(to_date(p_increment_date, 'yyyy/mm/dd hh24:mi:ss'));
	  v_file_date := v_date;
	end if;

	v_file_name := p_file_name_format;
	v_file_name := replace(v_file_name, '$DATETIME$', to_char(v_file_date, 'YYYYMMDD_HH24MISS'));
	v_file_name := replace(v_file_name, '$DATE$', to_char(v_file_date, 'YYYYMMDD'));
	-- open file for write item master
	write_log('+++++++++++++++++++++++++++++++++++++++++++++++');
	write_log('Create file ' || get_directory_path(p_output_path) || '/' || v_file_name || ' for write data');

	v_file_handle := utl_file.fopen_nchar(p_output_path, v_file_name, 'w');
	begin
	  utl_file.fclose(v_file_handle);
	exception
	  when others then
		null;
	end;
	v_file_handle := utl_file.fopen_nchar(p_output_path, v_file_name, 'w', max_linesize);
	v_bom_raw := hextoraw('EFBBBF');
	--utl_file.put_raw(v_file_handle, v_bom_raw);

	write_log('+---------------------------------------------------------------------------+');
	write_log('Qeury data and write to file.');
	write_log(' ');
	v_line_count := 0;
	err_code := '2';
	-- query increment item master (creation date)
	-- modify by nwt 05-Mar-2018: add company code DTN to export DTN item master
	--for r_data in (select 'DTAC' company_code  
	for r_data in (select decode(mpt.organization_code, 'Z00','DTAC','DTN') company_code
						 ,msi.segment1 product_sku
						 ,msi.description product_name
						 ,case msi.serial_number_control_code
							when 1 then
							 'N'
							else
							 'Y'
						  end flag_serial
						 ,msi.item_type product_group
						 ,'EA' uom
						 ,msi.inventory_item_status_code item_status
				   from mtl_system_items_b msi
					   ,mtl_parameters mpt
					   ,fnd_lookup_values_vl fv
					   ,fnd_lookup_types_vl fl
				   where 1 = 1
						 --and mpt.organization_code = 'Z00' -- modify by nwt 05-Mar-2018: add company code DTN to export DTN item master
						 and mpt.organization_code in ('Z00','N00')
						 and msi.item_type in ('DEVICE', 'MOBILE')
						 and msi.inventory_item_status_code in ('Active', 'Dead Stock')
						 and msi.item_type = fv.lookup_code
						 and fv.lookup_type = fl.lookup_type
						 and fl.lookup_type like 'ITEM_TYPE'
						 and fl.application_id = 401
						--and nvl(fv.attribute15, 'N') = 'Y'
						 and msi.organization_id = mpt.organization_id
						 and mpt.organization_id = mpt.master_organization_id
						 and (trunc(msi.creation_date) = decode(p_interface_type, 'ALL', trunc(msi.creation_date), v_date) or
						 trunc(msi.last_update_date) = decode(p_interface_type, 'ALL', trunc(msi.last_update_date), v_date))
				   order by msi.segment1)
	loop
	  v_line_data := replace(r_data.company_code, p_separator, '') || p_separator || replace(r_data.product_sku, p_separator, '') || p_separator ||
					 replace(r_data.product_name, p_separator, '') || p_separator || replace(r_data.flag_serial, p_separator, '') || p_separator ||
					 replace(r_data.product_group, p_separator, '') || p_separator || replace(r_data.uom, p_separator, '') || p_separator ||
					 replace(r_data.item_status, p_separator, '');

	  if (v_put_bom_flag = 'N') then
		v_put_bom_flag := 'Y';
		utl_file.put_raw(v_file_handle, v_bom_raw);
	  end if;

	  write_line_nchar(v_file_handle, v_line_data);
	  write_log(v_line_data);
	  v_line_count := v_line_count + 1;
	end loop;
	write_log('+---------------------------------------------------------------------------+');

	if utl_file.is_open(v_file_handle) then
	  utl_file.fclose(v_file_handle);
	end if;
	err_code := 0;
  exception
	when utl_file.access_denied then
	  err_msg := 'ACCESS DENIED: Access to the file has been denied by the operating system';
	  write_log(err_msg);
	  raise_application_error(-20001, err_msg);
	when utl_file.file_open then
	  err_msg := 'FILE OPEN: File is already open';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.internal_error then
	  err_msg := 'INTERNAL ERROR: Unhandled internal error in the UTL_FILE package';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.invalid_filehandle then
	  err_msg := 'INVALID FILE HANDLE: File handle does not exist';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.invalid_filename then
	  err_msg := 'INVALID FILENAME: A file with the specified name does not exist in the path';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.invalid_maxlinesize then
	  err_msg := 'INVALID MAXLINESIZE: The MAX_LINESIZE value for FOPEN() is invalid; it should be within the range 1 to 32767';
	  write_log(err_msg);
	  raise_application_error(-20001, err_msg);
	when value_error then
	  err_msg := 'VAULE ERROR: GET_LINE value is larger than the buffer';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.invalid_mode then
	  err_msg := 'INVALID MODE: The open_mode parameter in FOPEN is invalid';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.invalid_operation then
	  err_msg := 'INVALID OPERATION: File could not be opened or operated on as requested';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.invalid_path then
	  err_msg := 'INVALID PATH: Specified path does not exist or is not visible to Oracle';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.read_error then
	  err_msg := 'READ ERROR: Unable to read file';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when no_data_found then
	  if utl_file.is_open(v_file_handle) then
		utl_file.fclose(v_file_handle);
	  end if;
	when others then
	  if utl_file.is_open(v_file_handle) then
		utl_file.fclose(v_file_handle);
	  end if;
	  raise_application_error(-20011, sqlcode || ': ' || sqlerrm);
  end export_product_master;

  procedure export_customer_master
  (
	err_msg out varchar2
   ,err_code out varchar2
   ,p_interface_type varchar2
   ,p_increment_date varchar2
   ,p_output_path varchar2
   ,p_file_name_format varchar2
   ,p_separator varchar2 default '|'
  ) is

	v_file_handle utl_file.file_type;
	v_line_data nvarchar2(32767);
	v_file_name varchar2(100);
	v_date date;
	v_line_count number;
	v_file_date date := sysdate;
	v_bom_raw raw(3);
	v_put_bom_flag varchar2(10) := 'N';
  begin
	write_log(' ');
	write_log('+---------------------------------------------------------------------------+');
	write_log('Start Concurrent export Customer Master to Discovery ');
	write_log('Start Date: ' || to_char(sysdate, 'DD/MM/YYYY HH24:MI:SS'));
	write_log(' ');
	write_log('Agruments');
	write_log('-----------');
	write_log('p_interface_type =  ' || p_interface_type);
	write_log('p_increment_date =  ' || p_increment_date);
	write_log('p_output_path =  ' || p_output_path);
	write_log('p_file_name_format =  ' || p_file_name_format);
	write_log('p_separator =  ' || p_separator);
	write_log('+----------------------------------------------------------------------------+');

	-- validate parameter
	if p_output_path is null then
	  raise utl_file.invalid_path;
	end if;
	if p_file_name_format is null then
	  raise utl_file.invalid_filename;
	end if;
	if p_increment_date is null then
	  v_date := trunc(sysdate) - 1;
	  v_file_date := sysdate;
	else
	  v_date := trunc(to_date(p_increment_date, 'yyyy/mm/dd hh24:mi:ss'));
	  v_file_date := v_date;
	end if;

	v_file_name := p_file_name_format;
	v_file_name := replace(v_file_name, '$DATETIME$', to_char(v_file_date, 'YYYYMMDD_HH24MISS'));
	v_file_name := replace(v_file_name, '$DATE$', to_char(v_file_date, 'YYYYMMDD'));
	-- open file for write item master
	write_log('+++++++++++++++++++++++++++++++++++++++++++++++');
	write_log('Create file ' || get_directory_path(p_output_path) || '/' || v_file_name || ' for write data');

	v_file_handle := utl_file.fopen_nchar(p_output_path, v_file_name, 'w');
	begin
	  utl_file.fclose(v_file_handle);
	exception
	  when others then
		null;
	end;
	v_file_handle := utl_file.fopen_nchar(p_output_path, v_file_name, 'w', max_linesize);
	v_bom_raw := hextoraw('EFBBBF');
	-- Move put_raw into loop
	--utl_file.put_raw(v_file_handle, v_bom_raw);

	write_log('+---------------------------------------------------------------------------+');
	write_log('Qeury data and write to file.');
	write_log(' ');
	v_line_count := 0;
	err_code := '2';
	-- query increment item master (creation date)
	for r_data in (select case addr.org_id
							when 102 then
							 'DTAC'
							when 142 then
							 'DTN'
							else
							 'DTAC'
						  end company_code
						 ,cust.customer_number customer_no
						 ,cust.customer_name customer_name
						 ,cust.tax_reference tax_id
						 ,bill.location billto_code
						 ,decode(bill.location, null, null, loc.address1) billto_address1
						 ,decode(bill.location, null, null, loc.address2) billto_address2
						 ,decode(bill.location, null, null, loc.address3) billto_address3
						 ,decode(bill.location, null, null, loc.province) billto_province
						 ,decode(bill.location, null, null, loc.postal_code) billto_postal
						 ,bill_cr.overall_credit_limit credit_limit
						 ,substr(nvl(bill.location, ship.location), 1, instr(nvl(bill.location, ship.location), '_') - 1) division
						 ,trm.standard_terms_name term
						 ,ship.location shipto_code
						 ,decode(ship.location, null, null, loc.address1) shipto_address1
						 ,decode(ship.location, null, null, loc.address2) shipto_address2
						 ,decode(ship.location, null, null, loc.address3) shipto_address3
						 ,decode(ship.location, null, null, loc.province) shipto_province
						 ,decode(ship.location, null, null, loc.postal_code) shipto_postal
						 ,addr.attribute6 partner_code
						 ,addr.attribute8 parent_partner_code

						  -------------------------------
						 ,cust.creation_date creation_date_cust
						 ,cust.last_update_date last_update_date_cust
						 ,addr.creation_date creation_date_addr
						 ,addr.last_update_date last_update_date_addr
						 ,loc.creation_date creation_date_loc
						 ,loc.last_update_date last_update_date_loc
						 ,bill_cr.creation_date creation_date_bill_cr
						 ,bill_cr.last_update_date last_update_date_bill_cr
				   from ar_customers_all_v cust
					   ,hz_cust_acct_sites_all addr
					   ,hz_party_sites party_site
					   ,hz_locations loc
						--  ,hz_loc_assignments loc_assign
					   ,hz_cust_site_uses_all bill
					   ,hz_cust_site_uses_all ship
					   ,hz_cust_profile_amts bill_cr
					   ,ar_customer_profiles_v trm
				   where 1 = 1
						 and cust.customer_id = addr.cust_account_id
						 and addr.party_site_id = party_site.party_site_id
						 and party_site.location_id = loc.location_id
						--    and loc.location_id = loc_assign.location_id
						 and addr.cust_acct_site_id = bill.cust_acct_site_id(+)
						 and addr.cust_acct_site_id = ship.cust_acct_site_id(+)

						 and bill.site_use_id = bill_cr.site_use_id(+)
						 and bill.site_use_id = trm.site_use_id(+)
						 and ((addr.org_id = 102 and substr(nvl(bill.location, ship.location), 1, instr(nvl(bill.location, ship.location), '_') - 1) in
						 ('H0', 'H3', 'H4', 'H9', 'H10', 'H11', 'H15')) or
						 (addr.org_id = 142 and substr(nvl(bill.location, ship.location), 1, instr(nvl(bill.location, ship.location), '_') - 1) in
						 ('N0', 'N3', 'N4', 'N9', 'N10', 'N11', 'N15')))
						 and 'BILL_TO' = bill.site_use_code(+)
						 and 'SHIP_TO' = ship.site_use_code(+)
						--and cust.customer_number = '1357'
						--and party_site.party_site_number = 1228
						 and (trunc(cust.creation_date) = decode(p_interface_type, 'ALL', trunc(cust.creation_date), v_date) or
						 trunc(cust.last_update_date) = decode(p_interface_type, 'ALL', trunc(cust.last_update_date), v_date) or
						 trunc(addr.creation_date) = decode(p_interface_type, 'ALL', trunc(addr.creation_date), v_date) or
						 trunc(addr.last_update_date) = decode(p_interface_type, 'ALL', trunc(addr.last_update_date), v_date) or
						 trunc(loc.creation_date) = decode(p_interface_type, 'ALL', trunc(loc.creation_date), v_date) or
						 trunc(loc.last_update_date) = decode(p_interface_type, 'ALL', trunc(loc.last_update_date), v_date) or
						 trunc(bill_cr.creation_date) = decode(p_interface_type, 'ALL', trunc(bill_cr.creation_date), v_date) or
						 trunc(bill_cr.last_update_date) = decode(p_interface_type, 'ALL', trunc(bill_cr.last_update_date), v_date))
				   order by addr.org_id
						   ,cust.customer_number)
	loop
	  /*
      v_line_data := replace(r_data.company_code, p_separator, '') || p_separator ||
                     replace(r_data.customer_no, p_separator, '') || p_separator ||
                     replace(r_data.customer_name, p_separator, '') || p_separator ||
                      replace(r_data.tax_id, p_separator, '') || p_separator ||
                     replace(r_data.billto_code, p_separator, '') || p_separator ||
                     replace(r_data.billto_address1, p_separator, '') || p_separator ||
                     replace(r_data.billto_address2, p_separator, '') || p_separator ||
                     replace(r_data.billto_address3, p_separator, '') || p_separator ||
                     replace(r_data.billto_province, p_separator, '') || p_separator ||
                     replace(r_data.billto_postal, p_separator, '') || p_separator ||
                     replace(r_data.credit_limit, p_separator, '') || p_separator ||
                     replace(r_data.division, p_separator, '') || p_separator ||
                     replace(r_data.term, p_separator, '') || p_separator ||
                     replace(r_data.shipto_code, p_separator, '') ||p_separator ||
                     replace(r_data.shipto_address1, p_separator, '') || p_separator ||
                     replace(r_data.shipto_address2, p_separator, '') || p_separator ||
                     replace(r_data.shipto_address3, p_separator, '') || p_separator ||
                     replace(r_data.shipto_province, p_separator, '') || p_separator ||
                     replace(r_data.shipto_postal, p_separator, '') || p_separator ||
                     replace(r_data.partner_code, p_separator, '') || p_separator ||
                     replace(r_data.parent_partner_code, p_separator, '');
                     */

	  v_line_data := replace(r_data.company_code, p_separator, '') || p_separator || replace(r_data.customer_no, p_separator, '') || p_separator ||
					 replace(r_data.customer_name, p_separator, '') || p_separator || replace(r_data.tax_id, p_separator, '') || p_separator ||
					 replace(r_data.billto_code, p_separator, '') || p_separator || replace(r_data.billto_address1, p_separator, '') || p_separator ||
					 replace(r_data.billto_address2, p_separator, '') || p_separator || replace(r_data.billto_address3, p_separator, '') ||
					 p_separator || replace(r_data.billto_province, p_separator, '') || p_separator || replace(r_data.billto_postal, p_separator, '') ||
					 p_separator || replace(r_data.credit_limit, p_separator, '') || p_separator || replace(r_data.division, p_separator, '') ||
					 p_separator || replace(r_data.term, p_separator, '') || p_separator || replace(r_data.shipto_code, p_separator, '') ||
					 p_separator || replace(r_data.shipto_address1, p_separator, '') || p_separator ||
					 replace(r_data.shipto_address2, p_separator, '') || p_separator || replace(r_data.shipto_address3, p_separator, '') ||
					 p_separator || replace(r_data.shipto_province, p_separator, '') || p_separator || replace(r_data.shipto_postal, p_separator, '') ||
					 p_separator || replace(r_data.partner_code, p_separator, '') || p_separator ||
					 replace(r_data.parent_partner_code, p_separator, '');

	  if (v_put_bom_flag = 'N') then
		v_put_bom_flag := 'Y';
		utl_file.put_raw(v_file_handle, v_bom_raw);
	  end if;

	  write_line_nchar(v_file_handle, v_line_data);
	  write_log(v_line_data);
	  v_line_count := v_line_count + 1;
	end loop;
	write_log('+---------------------------------------------------------------------------+');

	if utl_file.is_open(v_file_handle) then
	  utl_file.fclose(v_file_handle);
	end if;
	err_code := 0;
  exception
	when utl_file.access_denied then
	  err_msg := 'ACCESS DENIED: Access to the file has been denied by the operating system';
	  write_log(err_msg);
	  raise_application_error(-20001, err_msg);
	when utl_file.file_open then
	  err_msg := 'FILE OPEN: File is already open';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.internal_error then
	  err_msg := 'INTERNAL ERROR: Unhandled internal error in the UTL_FILE package';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.invalid_filehandle then
	  err_msg := 'INVALID FILE HANDLE: File handle does not exist';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.invalid_filename then
	  err_msg := 'INVALID FILENAME: A file with the specified name does not exist in the path';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.invalid_maxlinesize then
	  err_msg := 'INVALID MAXLINESIZE: The MAX_LINESIZE value for FOPEN() is invalid; it should be within the range 1 to 32767';
	  write_log(err_msg);
	  raise_application_error(-20001, err_msg);
	when value_error then
	  err_msg := 'VAULE ERROR: GET_LINE value is larger than the buffer';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.invalid_mode then
	  err_msg := 'INVALID MODE: The open_mode parameter in FOPEN is invalid';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.invalid_operation then
	  err_msg := 'INVALID OPERATION: File could not be opened or operated on as requested';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.invalid_path then
	  err_msg := 'INVALID PATH: Specified path does not exist or is not visible to Oracle';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.read_error then
	  err_msg := 'READ ERROR: Unable to read file';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when no_data_found then
	  if utl_file.is_open(v_file_handle) then
		utl_file.fclose(v_file_handle);
	  end if;
	when others then
	  if utl_file.is_open(v_file_handle) then
		utl_file.fclose(v_file_handle);
	  end if;
	  raise_application_error(-20011, sqlcode || ': ' || sqlerrm);
  end export_customer_master;

  procedure export_purchase_order
  (
	err_msg out varchar2
   ,err_code out varchar2
   ,p_interface_type varchar2
   ,p_increment_date varchar2
   ,p_output_path varchar2
   ,p_file_name_format varchar2
   ,p_separator varchar2 default '|'
   ,p_org_id_list varchar2 default '102' -- FOrmat = '102,142,144,...''
  ) is

	v_file_handle utl_file.file_type;
	v_line_data nvarchar2(32767);
	v_file_name varchar2(100);
	v_date date;
	v_line_count number;
	v_file_date date := sysdate;
	v_bom_raw raw(3);
	v_net_qty number;
	v_quantity number;
	v_canceled number;
	v_put_bom_flag varchar2(10) := 'N';
  begin
	write_log(' ');
	write_log('+---------------------------------------------------------------------------+');
	write_log('Start Concurrent export Customer Master to Discovery ');
	write_log('Start Date: ' || to_char(sysdate, 'DD/MM/YYYY HH24:MI:SS'));
	write_log(' ');
	write_log('Agruments');
	write_log('-----------');
	write_log('p_interface_type =  ' || p_interface_type);
	write_log('p_increment_date =  ' || p_increment_date);
	write_log('p_output_path =  ' || p_output_path);
	write_log('p_file_name_format =  ' || p_file_name_format);
	write_log('p_separator =  ' || p_separator);
	write_log('p_org_id_list =  ' || p_org_id_list);
	write_log('+----------------------------------------------------------------------------+');

	-- validate parameter
	if p_output_path is null then
	  raise utl_file.invalid_path;
	end if;
	if p_file_name_format is null then
	  raise utl_file.invalid_filename;
	end if;
	if p_increment_date is null then
	  v_date := trunc(sysdate) - 1;
	  v_file_date := sysdate;
	else
	  v_date := trunc(to_date(p_increment_date, 'yyyy/mm/dd hh24:mi:ss'));
	  v_file_date := v_date;
	end if;

	v_file_name := p_file_name_format;
	v_file_name := replace(v_file_name, '$DATETIME$', to_char(v_file_date, 'YYYYMMDD_HH24MISS'));
	v_file_name := replace(v_file_name, '$DATE$', to_char(v_file_date, 'YYYYMMDD'));
	-- open file for write item master
	write_log('++++++++');
	write_log('Create file ' || get_directory_path(p_output_path) || '/' || v_file_name || ' for write data');

	v_file_handle := utl_file.fopen_nchar(p_output_path, v_file_name, 'w');
	begin
	  utl_file.fclose(v_file_handle);
	exception
	  when others then
		null;
	end;
	v_file_handle := utl_file.fopen_nchar(p_output_path, v_file_name, 'w', max_linesize);
	v_bom_raw := hextoraw('EFBBBF');
	--utl_file.put_raw(v_file_handle, v_bom_raw);

	write_log('+---------------------------------------------------------------------------+');
	write_log('Parameters');
	write_log('++++++++');
	write_log('p_interface_type = ' || p_interface_type);
	write_log('v_date = ' || to_char(v_date, 'dd/mm/yyyy hh24:mi:ss'));
	write_log(' ');
	write_log('+---------------------------------------------------------------------------+');
	write_log('Qeury data and write to file.');
	write_log(' ');
	v_line_count := 0;
	err_code := '2';
	-- query increment item master (creation date)
	for r_data in (select distinct case poh.org_id
									 when 102 then
									  'DTAC'
									 when 142 then
									  'DTN'
									 else
									  'DTAC'
								   end company_code
								  ,poh.segment1 po_number
								  ,poh.revision_num release_no
								  ,to_char(poh.creation_date, 'dd/mm/yyyy hh24:mi:ss') po_date
								  ,pv.segment1 supplier_code
								  ,pvs.vendor_site_code supplier_site_code
								  ,replace(poh.comments, '|', '/') description
								  ,poh.currency_code currency_code
								  ,pol.line_num po_line_number
								  ,msi.segment1 product_sku_code
								  ,org.organization_code org_code
								  ,'' sub_inven_code
								  ,pol.quantity quantity
								  ,sum(poll.quantity_cancelled) over(partition by poll.po_line_id) cancel_qty
								  ,sum(poll.quantity_received) over(partition by poll.po_line_id) received_qty
								  ,poll.unit_meas_lookup_code uom
								  ,pol.unit_price unit_price
								  ,to_char(poh.creation_date, 'dd/mm/yyyy hh24:mi:ss') creation_date
								  ,(select min(prl.attribute2)
									from po_lines_all pol1
										,po_line_locations_all poll
										,po_distributions_all pod
										,po_req_distributions_all prd
										,po_requisition_lines_all prl
									where pol1.po_line_id = pol.po_line_id
										  and pol.po_line_id = poll.po_line_id
										  and poll.line_location_id = pod.line_location_id
										  and pod.req_distribution_id = prd.distribution_id
										  and prd.requisition_line_id = prl.requisition_line_id
										  and prl.attribute2 is not null) sfa_transaction_code
				   from po_headers_all poh
					   ,po_lines_all pol
					   ,po_vendors pv
					   ,po_vendor_sites_all pvs
					   ,mtl_system_items_b msi
					   ,mtl_parameters mpt
					   ,org_organization_definitions iorg
					   ,fnd_lookup_values_vl fv
					   ,fnd_lookup_types_vl fl
					   ,po_line_locations_all poll
					   ,org_organization_definitions org
					   ,po_line_types plt
				   where 1 = 1
						 and msi.item_type in ('DEVICE', 'MOBILE')
						 and poh.vendor_id = pv.vendor_id
						 and poh.org_id = pvs.org_id
						 and poh.vendor_site_id = pvs.vendor_site_id
						 and poh.po_header_id = pol.po_header_id
						 and pol.item_id = msi.inventory_item_id
						 and msi.organization_id = mpt.organization_id
						 and mpt.organization_id = mpt.master_organization_id
						 and msi.organization_id = iorg.organization_id
						 and poh.org_id = iorg.operating_unit
						 and msi.item_type = fv.lookup_code
						 and fv.lookup_type = fl.lookup_type
						 and fl.lookup_type = 'ITEM_TYPE'
						 and fl.application_id = 401
						 and poh.authorization_status = 'APPROVED' -- FDD_150909 : DTAC{'991015007900', ''91015007890'} DTN {8015003047}
						 and poh.closed_code = 'OPEN'
						 and pol.po_line_id = poll.po_line_id
						 and poll.ship_to_organization_id = org.organization_id
						-- Add condition po Line Type = 'Expense' ; FDD_151002
						 and pol.line_type_id = plt.line_type_id
						 and plt.line_type = 'Expenses'

						--    and poh.segment1 =  '991015007900', ''91015007890', '991007014310' -- '991007014310', 991007014411, 991007015054, 991007015234, 991007016508
						/* -- FDD_150909 : Change to po apprival date
                        and (trunc(poh.creation_date) = decode(p_interface_type, 'ALL', trunc(poh.creation_date), v_date) or
                        trunc(poh.last_update_date) = decode(p_interface_type, 'ALL', trunc(poh.last_update_date), v_date) or
                        trunc(pol.creation_date) = decode(p_interface_type, 'ALL', trunc(pol.creation_date), v_date) or
                        trunc(pol.last_update_date) = decode(p_interface_type, 'ALL', trunc(pol.last_update_date), v_date) or
                        trunc(poll.creation_date) = decode(p_interface_type, 'ALL', trunc(poll.creation_date), v_date) or
                        trunc(poll.last_update_date) = decode(p_interface_type, 'ALL', trunc(poll.last_update_date), v_date))
                        */
						 and
						 ('ALL' = p_interface_type or (p_interface_type != 'ALL' and poh.approved_date >= v_date and poh.approved_date < (v_date + 1)))
						 and poh.org_id in
						 (select substr(org_ids, instr(org_ids, ',', 1, lvl) + 1, instr(org_ids, ',', 1, lvl + 1) - instr(org_ids, ',', 1, lvl) - 1) orderid
							  from (select ',' || org_ids || ',' as org_ids from (select p_org_id_list as org_ids from dual))
								  ,(select level as lvl from dual connect by level <= 20)
							  where lvl <= length(org_ids) - length(replace(org_ids, ',')) - 1)
				   order by poh.segment1
						   ,pol.line_num)
	loop
	  v_net_qty := 0;
	  if (r_data.cancel_qty > 0) then
		v_net_qty := nvl(r_data.received_qty, 0);
		v_quantity := 0;
	  else
		v_net_qty := nvl(r_data.quantity, 0);
		v_quantity := nvl(r_data.quantity, 0);
	  end if;

	  v_line_data := replace(r_data.company_code, p_separator, '') || p_separator || replace(r_data.po_number, p_separator, '') || p_separator ||
					 replace(r_data.release_no, p_separator, '') || p_separator || replace(r_data.po_date, p_separator, '') || p_separator ||
					 replace(r_data.supplier_code, p_separator, '') || p_separator || replace(r_data.supplier_site_code, p_separator, '') ||
					 p_separator || replace(r_data.description, p_separator, '') || p_separator || replace(r_data.currency_code, p_separator, '') ||
					 p_separator || replace(r_data.po_line_number, p_separator, '') || p_separator ||
					 replace(r_data.product_sku_code, p_separator, '') || p_separator || replace(r_data.org_code, p_separator, '') || p_separator ||
					 replace(r_data.sub_inven_code, p_separator, '') || p_separator || replace(v_quantity, p_separator, '') || p_separator ||
					 replace(r_data.cancel_qty, p_separator, '') || p_separator || replace(v_net_qty, p_separator, '') || p_separator ||
					 replace(r_data.uom, p_separator, '') || p_separator || replace(r_data.unit_price, p_separator, '') || p_separator ||
					 replace(r_data.creation_date, p_separator, '') || p_separator || replace(r_data.sfa_transaction_code, p_separator, '');

	  if (v_put_bom_flag = 'N') then
		v_put_bom_flag := 'Y';
		utl_file.put_raw(v_file_handle, v_bom_raw);
	  end if;

	  write_line_nchar(v_file_handle, v_line_data);
	  write_log(v_line_data);
	  v_line_count := v_line_count + 1;
	end loop;
	write_log('+---------------------------------------------------------------------------+');

	if utl_file.is_open(v_file_handle) then
	  utl_file.fclose(v_file_handle);
	end if;
	err_code := 0;
  exception
	when utl_file.access_denied then
	  err_msg := 'ACCESS DENIED: Access to the file has been denied by the operating system';
	  write_log(err_msg);
	  raise_application_error(-20001, err_msg);
	when utl_file.file_open then
	  err_msg := 'FILE OPEN: File is already open';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.internal_error then
	  err_msg := 'INTERNAL ERROR: Unhandled internal error in the UTL_FILE package';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.invalid_filehandle then
	  err_msg := 'INVALID FILE HANDLE: File handle does not exist';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.invalid_filename then
	  err_msg := 'INVALID FILENAME: A file with the specified name does not exist in the path';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.invalid_maxlinesize then
	  err_msg := 'INVALID MAXLINESIZE: The MAX_LINESIZE value for FOPEN() is invalid; it should be within the range 1 to 32767';
	  write_log(err_msg);
	  raise_application_error(-20001, err_msg);
	when value_error then
	  err_msg := 'VAULE ERROR: GET_LINE value is larger than the buffer';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.invalid_mode then
	  err_msg := 'INVALID MODE: The open_mode parameter in FOPEN is invalid';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.invalid_operation then
	  err_msg := 'INVALID OPERATION: File could not be opened or operated on as requested';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.invalid_path then
	  err_msg := 'INVALID PATH: Specified path does not exist or is not visible to Oracle';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.read_error then
	  err_msg := 'READ ERROR: Unable to read file';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when no_data_found then
	  if utl_file.is_open(v_file_handle) then
		utl_file.fclose(v_file_handle);
	  end if;
	when others then
	  if utl_file.is_open(v_file_handle) then
		utl_file.fclose(v_file_handle);
	  end if;
	  raise_application_error(-20011, sqlcode || ': ' || sqlerrm);
  end export_purchase_order;

  function validate_grn(p_request_id number) return varchar2 is
	v_result varchar2(1000) := null;
  begin
	if (p_request_id is not null) then
	  return '';
	else
	  return 'ERROR : No request identify.';
	end if;
  exception
	when others then
	  v_result := 'ERROR : ' || sqlerrm;
	  return v_result;
  end;

  function validate_gl(p_request_id number) return varchar2 is
	v_result varchar2(1000) := null;
  begin
	if (p_request_id is not null) then
	  return '';
	else
	  return 'ERROR : No request identify.';
	end if;
  exception
	when others then
	  v_result := 'ERROR : ' || sqlerrm;
	  return v_result;
  end;
  procedure interface_grn
  (
	err_msg out varchar2
   ,err_code out varchar2
   ,p_company_map_org_id varchar2
   ,p_file_path varchar2
   ,p_file_name_format varchar2
   ,p_file_name varchar2 default null
   ,p_delimiter varchar2 default '|'
   ,p_debug_flag varchar2 default 'N'
   ,p_purge_days number default 45
   ,p_run_interface_process varchar2 default 'Y'
  ) is
	v_file_name varchar2(100);
	v_date date := sysdate;
	v_file_handle utl_file.file_type;
	v_line_data nvarchar2(32767);
	v_line_count number := 0;

	v_rec_data dtac_grn_interface_temp %rowtype;
	v_insert_error_flag varchar2(1) := 'N';

	v_request_id number := fnd_profile.value('CONC_REQUEST_ID');
	v_creation_date date := sysdate;
	v_debug_flag varchar2(10);
	v_delimiter varchar2(10);
	v_company_map_org_id varchar2(500);

	v_login_id number := fnd_profile.value('LOGIN_ID');
	v_user_id number := fnd_profile.value('USER_ID');
	v_header_interface_id number;
	v_group_interface_id number;
	v_interface_transaction_id number;
	--v_receipt_group_interface_id number;
	v_return2receive_group_id number;
	v_return2vendor_group_id number;
	v_created_by number;
	v_employee_id number;
	v_locator_id number;
	v_item_unit_of_measure varchar2(25);

	v_erp_receipt_num varchar2(50);

	v_destination_type_code varchar2(50) := 'INVENTORY';
	v_submit_request_id number;

	v_run_interface_process varchar2(10) := 'Y';

	v_count_receive number := 0;
	v_count_return number := 0;

  begin
	err_code := '2';
	v_delimiter := p_delimiter;
	v_debug_flag := p_debug_flag;
	v_company_map_org_id := nvl(p_company_map_org_id, 'DTAC=102,DTN=142');

	write_log(' ');
	write_log('+---------------------------------------------------------------------------+');
	write_log('Start Concurrent Goods Receipt Interface from Discovery ');
	write_log('Start Date: ' || to_char(sysdate, 'DD/MM/YYYY HH24:MI:SS'));
	write_log(' ');
	write_log('Agruments');
	write_log('-----------');
	write_log('p_company_map_org_id =  ' || p_company_map_org_id);
	write_log('p_file_path =  ' || p_file_path);
	write_log('p_file_name_format =  ' || p_file_name_format);
	write_log('p_file_name =  ' || p_file_name);
	write_log('p_delimiter =  ' || p_delimiter);
	write_log('p_debug_flag =  ' || p_debug_flag);
	write_log('p_run_interface_process =  ' || p_run_interface_process);

	write_log('+----------------------------------------------------------------------------+');

	-- Start import data from text file
	v_run_interface_process := nvl(p_run_interface_process, 'Y');
	if p_file_path is null then
	  raise utl_file.invalid_path;
	end if;

	if (p_file_name is not null) then
	  v_file_name := p_file_name;
	else
	  if p_file_name_format is null then
		raise utl_file.invalid_filename;
	  end if;
	  v_file_name := replace(p_file_name_format, '$DATE$', trim(to_char(v_date, 'yyyymmdd')));
	end if;

	write_log(g_log_line);
	write_log('Import GRN data from file ' || get_directory_path(g_grn_inbox) || '/' || v_file_name);
	write_log(' ');

	begin
	  delete from dtac_grn_interface_temp tmp where tmp.interface_date <= (sysdate - nvl(p_purge_days, 45));
	  commit;
	exception
	  when others then
		null;
	end;

	begin
	  --v_file_handle := utl_file.fopen(location => p_file_path, filename => v_file_name, open_mode => 'r');
	  v_file_handle := utl_file.fopen_nchar(location => p_file_path, filename => v_file_name, open_mode => 'r');
	  utl_file.fclose(v_file_handle);
	exception
	  when others then
		write_log(' Error first read : ' || sqlerrm);
		write_log(' ===============');
		write_log('p_file_path = ' || p_file_path);
		write_log('v_file_name = ' || v_file_name);
		write_log(' ===============');
	end;
	--v_file_handle := utl_file.fopen(location => p_file_path, filename => v_file_name, open_mode => 'r');
	v_file_handle := utl_file.fopen_nchar(location => p_file_path, filename => v_file_name, open_mode => 'r');

    -- Read first line for header (ignore BOM byte)
    utl_file.get_line_nchar(file => v_file_handle, buffer => v_line_data);
    v_line_data := '';

	savepoint before_insert_temp;
	loop
	  begin
		--utl_file.get_line(file => v_file_handle, buffer => v_line_data);
		utl_file.get_line_nchar(file => v_file_handle, buffer => v_line_data);
		v_line_data := replace(replace(v_line_data, chr(10), ''), chr(13), '');
		if (v_line_data = 'END') then
		  exit;
		end if;
		v_line_count := v_line_count + 1;

		v_rec_data := split_grn_data(p_line_buff => v_line_data, p_delimiter => v_delimiter, p_debug_flag => v_debug_flag);
		begin
		  v_rec_data.request_id := v_request_id;
		  v_rec_data.interface_date := sysdate;

		  insert into dtac_grn_interface_temp
			(company_code
			,transaction_type
			,grn_number
			,po_number
			,release_number
			,po_line_number
			,product_sku_code
			,invoice_number
			,rate_date
			,qty
			,inventory_org_code
			,sub_inventory
			,locator
			,create_by
			,creation_date
			,request_id
			,interface_date
			,validate_status
			,error_msg)
		  values
			(v_rec_data.company_code
			,v_rec_data.transaction_type
			,v_rec_data.grn_number
			,v_rec_data.po_number
			,v_rec_data.release_number
			,v_rec_data.po_line_number
			,v_rec_data.product_sku_code
			,v_rec_data.invoice_number
			,v_rec_data.rate_date
			,v_rec_data.qty
			,v_rec_data.inventory_org_code
			,v_rec_data.sub_inventory
			,v_rec_data.locator
			,v_rec_data.create_by
			,v_rec_data.creation_date
			,v_rec_data.request_id
			,v_rec_data.interface_date
			,v_rec_data.validate_status
			,v_rec_data.error_msg);

		  write_log('Line#' || to_char(v_line_count, 90000) || ' [ OK] =>  ' || v_line_data);
		exception
		  when others then
			v_insert_error_flag := 'Y';
			write_log('Line#' || to_char(v_line_count, 90000) || ' [ERR] =>  ' || v_line_data);
			write_output('Line#' || to_char(v_line_count, 90000) || ' [ERR] =>  ' || sqlerrm);
		end;
	  exception
		when no_data_found then
		  exit;
		when others then
		  v_insert_error_flag := 'Y';
		  write_log('Read data from text file error :  ' || sqlerrm);
		  exit;
	  end;
	end loop;
	-- check open and close
	if (utl_file.is_open(file => v_file_handle)) then
	  utl_file.fclose(file => v_file_handle);
	end if;

	if (v_insert_error_flag = 'Y') then
	  rollback to savepoint before_insert_temp;
	  write_output(' ');
	  write_output(' ');
	  write_output(g_log_line);

	  write_log(' ');
	  write_log('tac_discovery_interface_util.interface_grn @import_data exception when insert data. See output for error detail.');

	  err_msg := 'Error tac_discovery_interface_util.interface_grn @import_data : Some record error during insert to table. See output for detail.';
	  err_code := '2';
	  move_data_file(get_directory_path(g_grn_inbox) || '/' || v_file_name, get_directory_path(g_grn_error));

	  return;
	else
	  commit;
	  move_data_file(get_directory_path(g_grn_inbox) || '/' || v_file_name, get_directory_path(g_grn_history));
	end if;
	-- End of import data from text file

	savepoint before_insert_interface;
	v_insert_error_flag := 'N';
	-- validate GRN
	if (validate_grn(v_request_id) is not null) then
	  write_log('Validate : Failed.');
	else
	  write_log('Validate : Pass.');
	  -- Start insert data from temp table to interface table
	  select rcv_interface_groups_s.nextval into v_group_interface_id from dual;
	  select rcv_interface_groups_s.nextval into v_return2receive_group_id from dual;
	  select rcv_interface_groups_s.nextval into v_return2vendor_group_id from dual;

	  for r_po_grn in (select tmp.po_number
							 ,tmp.transaction_type
							 ,tmp.request_id
							 ,count(distinct tmp.invoice_number) count_invoice_number
					   from dtac_grn_interface_temp tmp
					   where tmp.request_id = v_request_id
					   group by tmp.po_number
							   ,tmp.transaction_type
							   ,tmp.request_id)
	  loop
		if (r_po_grn.transaction_type = 'Receipt') then
		  v_count_receive := v_count_receive + 1;
		  select rcv_headers_interface_s.nextval into v_header_interface_id from dual;
		  --select rcv_interface_groups_s.nextval into v_group_interface_id from dual;

		  -- RCV Header
		  insert into rcv_headers_interface
			(header_interface_id
			,group_id
			,processing_status_code
			,receipt_source_code
			,transaction_type
			,last_update_date
			,last_updated_by
			,last_update_login
			,creation_date
			,created_by
			,shipped_date
			,vendor_id
			,expected_receipt_date
			,validation_flag
			,packing_slip)
			select v_header_interface_id header_interface_id
				  ,v_group_interface_id group_id
				  ,'PENDING' processing_status_code
				  ,'VENDOR' receipt_source_code
				  ,'NEW' transaction_type
				  ,sysdate last_update_date
				  ,v_user_id last_updated_by
				  ,v_login_id last_update_login
				  ,sysdate creation_date
				   /* -- change from employe_code to user_lan value
                                                                                                                                                                                                                                    ,(select fu.user_id
                                                                                                                                                                                                                                    from per_people_f emp
                                                                                                                                                                                                                                    ,fnd_user fu
                                                                                                                                                                                                                                    where emp.employee_number = tmp.create_by
                                                                                                                                                                                                                                    and emp.effective_end_date >= sysdate
                                                                                                                                                                                                                                    and emp.person_id = fu.employee_id) created_by*/
				  ,fu.user_id created_by
				  ,sysdate shipped_date
				  ,poh.vendor_id vendor_id
				  ,sysdate expected_receipt_date
				  ,'Y' validation_flag
				  ,tmp.invoice_number packing_slip
			from dtac_grn_interface_temp tmp
				,po_headers_all poh
				,fnd_user fu
			where 1 = 1
				  and tmp.po_number = poh.segment1
				  and upper(tmp.create_by) = fu.user_name(+)
				  and tmp.transaction_type = r_po_grn.transaction_type
				  and tmp.request_id = r_po_grn.request_id
				  and poh.segment1 = r_po_grn.po_number
				  and rownum = 1;
		  -- RCV Transaction
		  for r_rcv_trx in (select poh.po_header_id
								  ,poh.vendor_id
								  ,poh.vendor_site_id
								  ,pol.po_line_id
								  ,pll.line_location_id
								  ,nvl(tmp.release_number, por.release_num) release_num
								  ,tmp.po_line_number
								  ,pll.shipment_num
								  ,pll.ship_to_location_id
								  ,(select substr(data_pair, instr(data_pair, '=') + 1) org_id
									from (select substr(orderids
													   ,instr(orderids, ',', 1, lvl) + 1
													   ,instr(orderids, ',', 1, lvl + 1) - instr(orderids, ',', 1, lvl) - 1) data_pair
										  from (select ',' || orderids || ',' as orderids from (select v_company_map_org_id as orderids from dual))
											  ,(select level as lvl from dual connect by level <= 50)
										  where lvl <= length(orderids) - length(replace(orderids, ',')) - 1)
									where substr(data_pair, 1, instr(data_pair, '=') - 1) = tmp.company_code) ou_id
								  ,pll.ship_to_organization_id ship_to_organization_id
								  ,pol.unit_meas_lookup_code
								  ,tmp.qty quantity
								  ,pol.item_id
								  ,msi.inventory_item_id
								  ,tmp.product_sku_code item_code
								  ,por.po_release_id
								  ,nvl(tmp.rate_date, poh.rate_date) rate_date
								  ,tmp.grn_number attribute7
								  ,mp.organization_code
								  ,mp.master_organization_id
								  ,msi.organization_id
								  ,tmp.create_by
								  ,tmp.sub_inventory
								  ,tmp.locator
								  ,tmp.po_number
							from dtac_grn_interface_temp tmp
								,po_headers_all poh
								,po_releases_all por
								,po_lines_all pol
								,po_line_locations_all pll
								,mtl_parameters mp
								,mtl_system_items_b msi
							where tmp.po_number = poh.segment1
								  and poh.po_header_id = pol.po_header_id
								  and pol.line_num = tmp.po_line_number
								  and pol.po_line_id = pll.po_line_id
								  and poh.po_header_id = por.po_header_id(+)
								  and nvl(pll.po_release_id, -99) = nvl(por.po_release_id, -99)
								  and nvl(por.release_num, -99) = nvl(tmp.release_number, nvl(por.release_num, -99))
								  and tmp.inventory_org_code = mp.organization_code
								  and tmp.product_sku_code = msi.segment1
								  and msi.organization_id = mp.master_organization_id

								  and tmp.po_number = r_po_grn.po_number
								  and tmp.transaction_type = r_po_grn.transaction_type
								  and tmp.request_id = r_po_grn.request_id)
		  loop
			select rcv_transactions_interface_s.nextval into v_interface_transaction_id from dual;
			begin
			  select fu.user_id
					,fu.employee_id
			  into v_created_by
				  ,v_employee_id
			  from fnd_user fu
			  where fu.user_name = upper(r_rcv_trx.create_by);
			exception
			  when others then
				v_created_by := null;
				v_employee_id := null;
			end;

			begin
			  select distinct inventory_location_id into v_locator_id from mtl_item_locations mil where segment1 = r_rcv_trx.locator;
			exception
			  when others then
				v_locator_id := null;
			end;
			begin
			  insert into rcv_transactions_interface
				(interface_transaction_id
				,group_id
				,header_interface_id
				,last_update_date
				,last_updated_by
				,creation_date
				,created_by
				,transaction_type
				,transaction_date
				,processing_status_code
				,processing_mode_code
				,transaction_status_code
				,quantity
				,unit_of_measure
				,interface_source_code
				,item_id
				,employee_id
				,auto_transact_code
				,receipt_source_code
				,to_organization_id
				,source_document_code
				,destination_type_code
				,deliver_to_location_id
				,locator_id
				,subinventory
				,expected_receipt_date
				,vendor_id
				,vendor_site_id
				,po_header_id
				,po_release_id
				,po_line_id
				,po_line_location_id
				,shipment_num
				,validation_flag
				,currency_conversion_date
				,substitute_item_id
				,attribute7)
				select v_interface_transaction_id
					  ,v_group_interface_id
					  ,v_header_interface_id
					  ,sysdate last_update_date
					  ,v_user_id last_updated_by
					  ,sysdate creation_date
					  ,v_created_by created_by
					  ,'RECEIVE' transaction_type
					  ,sysdate transaction_date
					  ,'PENDING' processing_status_code
					  ,'BATCH' processing_mode_code
					  ,'PENDING' transaction_status_code
					  ,r_rcv_trx.quantity quantity
					  ,r_rcv_trx.unit_meas_lookup_code unit_of_measure
					  ,'RCV' interface_source_code
					  ,r_rcv_trx.inventory_item_id item_id
					  ,v_employee_id employee_id
					  ,'DELIVER' auto_transact_code
					  ,'VENDOR' receipt_source_code
					  ,r_rcv_trx.ship_to_organization_id to_organization_id
					  ,'PO' source_document_code
					  ,v_destination_type_code destination_type_code -- INVENTORY, EXPENSE
					  ,r_rcv_trx.ship_to_location_id deliver_to_location_id
					  ,v_locator_id locator_id
					  ,r_rcv_trx.sub_inventory subinventory
					  ,sysdate expected_receipt_date
					  ,r_rcv_trx.vendor_id vendor_id
					  ,r_rcv_trx.vendor_site_id vendor_site_id
					  ,r_rcv_trx.po_header_id po_header_id
					  ,r_rcv_trx.po_release_id po_release_id
					  ,r_rcv_trx.po_line_id po_line_id
					  ,r_rcv_trx.line_location_id po_line_location_id
					  ,r_rcv_trx.shipment_num shipment_num
					  ,'Y' validation_flag
					  ,trunc(r_rcv_trx.rate_date, 'dd') currency_conversion_date
					  ,null substitute_item_id
					  ,r_rcv_trx.attribute7
				from dual;
			exception
			  when others then
				v_insert_error_flag := 'Y';
				write_log('[ERR] => ' || upper(r_po_grn.transaction_type) || ' : ' || 'PO# ' || r_rcv_trx.po_number || ' PO Line# ' ||
						  r_rcv_trx.po_line_number);
				write_output('[ERR] => ' || upper(r_po_grn.transaction_type) || ' : ' || 'PO# ' || r_rcv_trx.po_number || ' PO Line# ' ||
							 r_rcv_trx.po_line_number || ' : ' || sqlerrm);
			end;
		  end loop;
		else
		  v_count_return := v_count_return + 1;
		  -- RETURN TO RECEIVING
		  /*
          A) perform a "RETURN to RECEIVING" for a standard Purchase Order
          through ROI
          TRANSACTION_TYPE = RETURN TO RECEIVING
          PARENT_TRANSACTION_ID = (TRANSACTION_ID for DELIVER transaction)
          */
		  /*
              for r_rtn2rcv_trx in (select distinct rsh.receipt_num
                                                   ,rt.transaction_id
                                                   ,rt.transaction_date
                                                   ,tmp.qty quantity
                                                   ,rt.unit_of_measure
                                                   ,rt.shipment_header_id
                                                   ,rt.shipment_line_id
                                                   ,rt.source_document_code
                                                   ,rt.destination_type_code
                                                   ,rt.employee_id
                                                   ,rt.parent_transaction_id
                                                   ,rt.po_header_id
                                                   ,rt.po_line_id
                                                   ,rsl.item_id
                                                   ,rt.po_line_location_id
                                                   ,rt.po_distribution_id
                                                   ,rt.deliver_to_person_id
                                                   ,rt.deliver_to_location_id
                                                   ,rt.vendor_id
                                                   ,rt.vendor_site_id
                                                   ,rt.organization_id
                                                   ,rt.subinventory
                                                   ,rt.locator_id
                                                   ,rt.location_id
                                                   ,rsh.ship_to_org_id
                                                   ,(select fu.user_id
                                                     from per_people_f emp
                                                         ,fnd_user fu
                                                     where emp.employee_number = tmp.create_by
                                                           and emp.effective_end_date >= sysdate
                                                           and emp.person_id = fu.employee_id) created_by
                                                    -------------------------------
                                                   ,tmp.request_id
                                                   ,tmp.grn_number ax_grn_number
                                                   ,tmp.po_number
                                                   ,tmp.po_line_number
                                                   ,rt.transaction_type
                                                   ,rsh.receipt_num erp_receipt_num
                                    from dtac_grn_interface_temp tmp
                                        ,po_headers_all poh
                                        ,po_lines_all pol
                                        ,rcv_transactions rt
                                        ,rcv_shipment_headers rsh
                                        ,rcv_shipment_lines rsl
                                    where tmp.po_number = poh.segment1
                                          and tmp.po_line_number = pol.line_num
                                          and poh.po_header_id = pol.po_header_id
                                          and tmp.grn_number = rt.attribute7
                                          and poh.po_header_id = rt.po_header_id
                                          and rt.transaction_type = 'DELIVER'
                                          and rt.shipment_header_id = rsh.shipment_header_id
                                          and rt.shipment_line_id = rsl.shipment_line_id

                                          and tmp.request_id = r_po_grn.request_id
                                          and tmp.transaction_type = r_po_grn.transaction_type
                                          and tmp.po_number = r_po_grn.po_number)
              loop
                begin
                  select min(msi.primary_unit_of_measure) item_unit_of_measure
                  into v_item_unit_of_measure
                  from mtl_system_items_b msi
                  where msi.inventory_item_id = r_rtn2rcv_trx.item_id;

                  insert into rcv_transactions_interface
                    (interface_transaction_id
                    ,group_id
                    ,last_update_date
                    ,last_updated_by
                    ,creation_date
                    ,created_by
                    ,last_update_login
                    ,transaction_type
                    ,transaction_date
                    ,processing_status_code
                    ,processing_mode_code
                    ,transaction_status_code
                    ,quantity
                    ,unit_of_measure
                    ,item_id
                    ,employee_id
                    ,shipment_header_id
                    ,shipment_line_id
                    ,receipt_source_code
                    ,vendor_id
                    ,from_organization_id
                    ,from_subinventory
                    ,from_locator_id
                    ,source_document_code
                    ,parent_transaction_id
                    ,po_header_id
                    ,po_line_id
                    ,po_line_location_id
                    ,po_distribution_id
                    ,destination_type_code
                    ,deliver_to_person_id
                    ,location_id
                    ,deliver_to_location_id
                    ,validation_flag)
                  values
                    (rcv_transactions_interface_s.nextval --INTERFACE_TRANSACTION_ID
                    ,v_return2receive_group_id --GROUP_ID
                    ,sysdate --LAST_UPDATE_DATE
                    ,v_user_id --LAST_UPDATE_BY
                    ,sysdate --CREATION_DATE
                    ,r_rtn2rcv_trx.created_by --CREATED_BY
                    ,v_login_id --LAST_UPDATE_LOGIN
                    ,'RETURN TO RECEIVING' --TRANSACTION_TYPE
                    ,sysdate --TRANSACTION_DATE
                    ,'PENDING' --PROCESSING_STATUS_CODE
                    ,'BATCH' --PROCESSING_MODE_CODE
                    ,'PENDING' --TRANSACTION_STATUS_CODE
                    ,r_rtn2rcv_trx.quantity --QUANTITY
                    ,nvl(v_item_unit_of_measure, 'EA') --UNIT_OF_MEASURE
                    ,r_rtn2rcv_trx.item_id --ITEM_ID
                    ,r_rtn2rcv_trx.employee_id --EMPLOYEE_ID
                    ,r_rtn2rcv_trx.shipment_header_id --SHIPMENT_HEADER_ID
                    ,r_rtn2rcv_trx.shipment_line_id --SHIPMENT_LINE_ID
                    ,'VENDOR' --RECEIPT_SOURCE_CODE
                    ,r_rtn2rcv_trx.vendor_id --VENDOR_ID
                    ,r_rtn2rcv_trx.organization_id --FROM_ORGANIZATION_ID
                    ,r_rtn2rcv_trx.subinventory --FROM_SUBINVENTORY
                    ,r_rtn2rcv_trx.locator_id --FROM_LOCATOR_ID
                    ,'PO' --SOURCE_DOCUMENT_CODE
                    ,r_rtn2rcv_trx.transaction_id --PARENT_TRANSACTION_ID
                    ,r_rtn2rcv_trx.po_header_id --PO_HEADER_ID
                    ,r_rtn2rcv_trx.po_line_id --PO_LINE_ID
                    ,r_rtn2rcv_trx.po_line_location_id --PO_LINE_LOCATION_ID
                    ,r_rtn2rcv_trx.po_distribution_id --PO_DISTRIBUTION_ID
                    ,r_rtn2rcv_trx.destination_type_code --DESTINATION_TYPE_CODE
                    ,null --DELIVER_TO_PERSON_ID
                    ,null --LOCATION_ID
                    ,null --DELIVER_TO_LOCATION_ID
                    ,'Y' --VALIDATION_FLAG
                     );
                exception
                  when others then
                    v_insert_error_flag := 'Y';
                    write_log('[ERR] => Insert rcv_transactions_interface for return  to receive ' || upper(r_po_grn.transaction_type) || ' : ' ||
                              'PO# ' || r_rtn2rcv_trx.po_number || ' PO Line# ' || r_rtn2rcv_trx.po_line_number);
                    write_output('[ERR] =>  Insert rcv_transactions_interface for return to receive ' || upper(r_po_grn.transaction_type) || ' : ' ||
                                 'PO# ' || r_rtn2rcv_trx.po_number || ' PO Line# ' || r_rtn2rcv_trx.po_line_number || ' : ' || sqlerrm);
                end;
              end loop;
          */

		  -- RETURN TO VENDOR
		  /*
          B) perform a "RETURN to VENDOR" for a standard Purchase Order
          through ROI
          TRANSACTION_TYPE = RETURN TO VENDOR
          PARENT_TRANSACTION_ID = (TRANSACTION_ID for RECEIVE transaction)
          */
		  for r_rtn2vendor_trx in (select distinct rsh.receipt_num
												  ,rt.transaction_id
												  ,rt.transaction_date
												  ,tmp.qty quantity
												  ,rt.unit_of_measure
												  ,rt.shipment_header_id
												  ,rt.shipment_line_id
												  ,rt.source_document_code
												  ,rt.destination_type_code
												  ,rt.employee_id
												  ,rt.parent_transaction_id
												  ,rt.po_header_id
												  ,rt.po_line_id
												  ,rsl.item_id
												  ,rt.po_line_location_id
												  ,rt.po_distribution_id
												  ,rt.deliver_to_person_id
												  ,rt.deliver_to_location_id
												  ,rt.vendor_id
												  ,rt.vendor_site_id
												  ,rt.organization_id
												  ,rt.subinventory
												  ,rt.locator_id
												  ,rt.location_id
												  ,rsh.ship_to_org_id
												   /* Change from employee_code to User_Lan
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ,(select fu.user_id
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             from per_people_f emp
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ,fnd_user fu
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             where emp.employee_number = tmp.create_by
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   and emp.effective_end_date >= sysdate
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   and emp.person_id = fu.employee_id) created_by*/
												  ,fu.user_id created_by
												   -------------------------------
												  ,tmp.request_id
												  ,tmp.grn_number ax_grn_number
												  ,tmp.po_number
												  ,tmp.po_line_number
												  ,rt.transaction_type
												  ,rsh.receipt_num erp_receipt_num
								   from dtac_grn_interface_temp tmp
									   ,po_headers_all poh
									   ,po_lines_all pol
									   ,rcv_transactions rt
									   ,rcv_shipment_headers rsh
									   ,rcv_shipment_lines rsl
									   ,fnd_user fu
								   where tmp.po_number = poh.segment1
										 and tmp.po_line_number = pol.line_num
										 and poh.po_header_id = pol.po_header_id
										 and tmp.grn_number = rt.attribute7
										 and poh.po_header_id = rt.po_header_id
										 and rt.transaction_type = 'RECEIVE'
										 and rt.shipment_header_id = rsh.shipment_header_id
										 and rt.shipment_line_id = rsl.shipment_line_id
										 and upper(tmp.create_by) = fu.user_name(+)

										 and tmp.request_id = r_po_grn.request_id
										 and tmp.transaction_type = r_po_grn.transaction_type
										 and tmp.po_number = r_po_grn.po_number)
		  loop
			begin
			  select min(msi.primary_unit_of_measure) item_unit_of_measure
			  into v_item_unit_of_measure
			  from mtl_system_items_b msi
			  where msi.inventory_item_id = r_rtn2vendor_trx.item_id;

			  insert into rcv_transactions_interface
				(interface_transaction_id
				,group_id
				,last_update_date
				,last_updated_by
				,creation_date
				,created_by
				,last_update_login
				,transaction_type
				,transaction_date
				,processing_status_code
				,processing_mode_code
				,transaction_status_code
				,quantity
				,unit_of_measure
				,item_id
				,employee_id
				,shipment_header_id
				,shipment_line_id
				,receipt_source_code
				,vendor_id
				,from_organization_id
				,from_subinventory
				,from_locator_id
				,source_document_code
				,parent_transaction_id
				,po_header_id
				,po_line_id
				,po_line_location_id
				,po_distribution_id
				,destination_type_code
				,deliver_to_person_id
				,location_id
				,deliver_to_location_id
				,validation_flag)
			  values
				(rcv_transactions_interface_s.nextval --INTERFACE_TRANSACTION_ID
				,v_return2vendor_group_id --GROUP_ID
				,sysdate --LAST_UPDATE_DATE
				,v_user_id --LAST_UPDATE_BY
				,sysdate --CREATION_DATE
				,r_rtn2vendor_trx.created_by --CREATED_BY
				,v_login_id --LAST_UPDATE_LOGIN
				,'RETURN TO VENDOR' --TRANSACTION_TYPE
				,sysdate --TRANSACTION_DATE
				,'PENDING' --PROCESSING_STATUS_CODE
				,'BATCH' --PROCESSING_MODE_CODE
				,'PENDING' --TRANSACTION_STATUS_CODE
				,r_rtn2vendor_trx.quantity --QUANTITY
				,nvl(v_item_unit_of_measure, 'EA') --UNIT_OF_MEASURE
				,r_rtn2vendor_trx.item_id --ITEM_ID
				,r_rtn2vendor_trx.employee_id --EMPLOYEE_ID
				,r_rtn2vendor_trx.shipment_header_id --SHIPMENT_HEADER_ID
				,r_rtn2vendor_trx.shipment_line_id --SHIPMENT_LINE_ID
				,'VENDOR' --RECEIPT_SOURCE_CODE
				,r_rtn2vendor_trx.vendor_id --VENDOR_ID
				,r_rtn2vendor_trx.organization_id --FROM_ORGANIZATION_ID
				,r_rtn2vendor_trx.subinventory --FROM_SUBINVENTORY
				,r_rtn2vendor_trx.locator_id --FROM_LOCATOR_ID
				,'PO' --SOURCE_DOCUMENT_CODE
				,r_rtn2vendor_trx.transaction_id --PARENT_TRANSACTION_ID
				,r_rtn2vendor_trx.po_header_id --PO_HEADER_ID
				,r_rtn2vendor_trx.po_line_id --PO_LINE_ID
				,r_rtn2vendor_trx.po_line_location_id --PO_LINE_LOCATION_ID
				,r_rtn2vendor_trx.po_distribution_id --PO_DISTRIBUTION_ID
				,r_rtn2vendor_trx.destination_type_code --DESTINATION_TYPE_CODE
				,null --DELIVER_TO_PERSON_ID
				,null --LOCATION_ID
				,null --DELIVER_TO_LOCATION_ID
				,'Y' --VALIDATION_FLAG
				 );
			exception
			  when others then
				v_insert_error_flag := 'Y';
				write_log('[ERR] => Insert rcv_transactions_interface for return  to vendor ' || upper(r_po_grn.transaction_type) || ' : ' || 'PO# ' ||
						  r_rtn2vendor_trx.po_number || ' PO Line# ' || r_rtn2vendor_trx.po_line_number);
				write_output('[ERR] =>  Insert rcv_transactions_interface for return to vendor ' || upper(r_po_grn.transaction_type) || ' : ' ||
							 'PO# ' || r_rtn2vendor_trx.po_number || ' PO Line# ' || r_rtn2vendor_trx.po_line_number || ' : ' || sqlerrm);
			end;
		  end loop;

		end if;
	  end loop;

	  if (v_insert_error_flag = 'Y') then
		rollback to savepoint before_insert_interface;
		write_log(' ');
		write_log('tac_discovery_interface_util.interface_grn @ insert interface. See output for error detail.');

		err_msg := 'Error tac_discovery_interface_util.interface_grn @insert interface : Some record error during insert to table. See output for detail.';
		err_code := '2';
	  else
		commit;
	  end if;
	end if;

	if (v_insert_error_flag <> 'Y' and v_run_interface_process = 'Y') then
	  -- Receive
	  if (v_count_receive > 0) then
		v_submit_request_id := fnd_request.submit_request(application => 'PO'
														 ,program => 'RVCTP'
														 ,description => null
														 ,start_time => sysdate
														 ,sub_request => false
														 ,argument1 => 'BATCH'
														 ,argument2 => v_group_interface_id);
		commit;
		if v_submit_request_id > 0 then
		  conc_wait(v_submit_request_id);
		end if;
	  end if;

	  if (v_count_return > 0) then
		/*
        -- Return to Receiving
        v_submit_request_id := fnd_request.submit_request(application => 'PO'
                                                         ,program => 'RVCTP'
                                                         ,description => null
                                                         ,start_time => sysdate
                                                         ,sub_request => false
                                                         ,argument1 => 'BATCH'
                                                         ,argument2 => v_return2receive_group_id);
        commit;
        if v_submit_request_id > 0 then
          conc_wait(v_submit_request_id);
        end if;
        */
		-- Return to Vendor
		v_submit_request_id := fnd_request.submit_request(application => 'PO'
														 ,program => 'RVCTP'
														 ,description => null
														 ,start_time => sysdate
														 ,sub_request => false
														 ,argument1 => 'BATCH'
														 ,argument2 => v_return2vendor_group_id);

		commit;
		if v_submit_request_id > 0 then
		  conc_wait(v_submit_request_id);
		end if;
	  end if;
	end if;

	err_code := '0';
  exception
	when utl_file.invalid_path then
	  write_output('Invalid File Location');
	  write_log('Invalid File Location');
	  raise_application_error(-20052, 'Invalid File Location');
	when utl_file.invalid_operation then
	  write_output('Invalid File Operation');
	  write_log('Invalid Operation');
	  raise_application_error(-20054, 'Invalid Operation');
	when utl_file.read_error then
	  write_output('Read File Error');
	  write_log('Read Error');
	  raise_application_error(-20055, 'Read Error');
	when utl_file.invalid_maxlinesize then
	  write_output('Line Size Exceeds 32K');
	  write_log('Line Size Exceeds 32K');
	  raise_application_error(-20060, 'Line Size Exceeds 32K');
	when utl_file.invalid_filename then
	  write_output('Invalid File Name');
	  write_log('Invalid File Name');
	  raise_application_error(-20061, 'Invalid File Name');
	when utl_file.access_denied then
	  write_output('File Access Denied By');
	  write_log('File Access Denied By');
	  raise_application_error(-20062, 'File Access Denied By');
	when others then
	  if (utl_file.is_open(file => v_file_handle)) then
		utl_file.fclose(file => v_file_handle);
	  end if;
	  err_code := '2';
	  err_msg := sqlerrm;
	  write_output(sqlerrm);
	  raise_application_error(-20011, sqlcode || ': ' || sqlerrm);
  end interface_grn;

  procedure interface_gl
  (
	err_msg out varchar2
   ,err_code out varchar2
   ,p_file_path varchar2
   ,p_file_name_format varchar2
   ,p_file_name varchar2 default null
   ,p_delimiter varchar2 default '|'
   ,p_debug_flag varchar2 default 'N'
   ,p_purge_days number default 45
   ,p_insert_interface_table varchar2 default 'Y'
  ) is
	v_file_name varchar2(100);
	v_date date := sysdate;
	v_file_handle utl_file.file_type;
	v_line_data nvarchar2(32767);
	v_line_count number := 0;

	v_rec_data dtac_gl_interface_temp %rowtype;
	v_insert_error_flag varchar2(1) := 'N';

	v_request_id number := fnd_profile.value('CONC_REQUEST_ID');
	v_creation_date date := sysdate;
	v_debug_flag varchar2(10);
	v_delimiter varchar2(10);
	v_company_map_org_id varchar2(500);

	v_login_id number := fnd_profile.value('LOGIN_ID');
	v_user_id number := fnd_profile.value('USER_ID');
	v_header_interface_id number;
	v_group_interface_id number;
	v_interface_transaction_id number;
	--v_receipt_group_interface_id number;
	v_return2receive_group_id number;
	v_return2vendor_group_id number;
	v_created_by number;
	v_employee_id number;
	v_submit_request_id number;

	v_insert_interface_table varchar2(10) := 'Y';

	v_count_gl number := 0;

  begin
	err_code := '2';
	v_delimiter := p_delimiter;
	v_debug_flag := p_debug_flag;

	write_log(' ');
	write_log('+---------------------------------------------------------------------------+');
	write_log('Start Concurrent GL Interface from Discovery ');
	write_log('Start Date: ' || to_char(sysdate, 'DD/MM/YYYY HH24:MI:SS'));
	write_log(' ');
	write_log('Agruments');
	write_log('-----------');
	write_log('p_file_path =  ' || p_file_path);
	write_log('p_file_name_format =  ' || p_file_name_format);
	write_log('p_file_name =  ' || p_file_name);
	write_log('p_delimiter =  ' || p_delimiter);
	write_log('p_debug_flag =  ' || p_debug_flag);
	write_log('p_insert_interface_table =  ' || p_insert_interface_table);

	write_log('+----------------------------------------------------------------------------+');

	-- Start import data from text file
	v_insert_interface_table := nvl(p_insert_interface_table, 'Y');
	if p_file_path is null then
	  raise utl_file.invalid_path;
	end if;

	if (p_file_name is not null) then
	  v_file_name := p_file_name;
	else
	  if p_file_name_format is null then
		raise utl_file.invalid_filename;
	  end if;
	  v_file_name := replace(p_file_name_format, '$DATE$', trim(to_char(v_date, 'yyyymmdd')));
	end if;

	write_log(g_log_line);
	write_log('Import GL data from file ' || get_directory_path(g_gl_inbox) || '/' || v_file_name);
	write_log(' ');

	begin
	  delete from dtac_gl_interface_temp tmp where tmp.interface_date <= (sysdate - nvl(p_purge_days, 45));
	  commit;
	exception
	  when others then
		null;
	end;

	begin
	  --v_file_handle := utl_file.fopen(location => p_file_path, filename => v_file_name, open_mode => 'r');

	  v_file_handle := utl_file.fopen_nchar(location => p_file_path, filename => v_file_name, open_mode => 'r');
	  utl_file.fclose(v_file_handle);
	exception
	  when others then
		write_log(' Error first read : ' || sqlerrm);
		write_log(' ===============');
		write_log('p_file_path = ' || p_file_path);
		write_log('v_file_name = ' || v_file_name);
		write_log(' ===============');
	end;
	--v_file_handle := utl_file.fopen(location => p_file_path, filename => v_file_name, open_mode => 'r');
	v_file_handle := utl_file.fopen_nchar(location => p_file_path, filename => v_file_name, open_mode => 'r');

    -- Read first line for header (ignore BOM byte)
    utl_file.get_line_nchar(file => v_file_handle, buffer => v_line_data);
    v_line_data := '';

	savepoint before_insert_temp;
	loop
	  begin
		--utl_file.get_line(file => v_file_handle, buffer => v_line_data);
		utl_file.get_line_nchar(file => v_file_handle, buffer => v_line_data);
		v_line_data := replace(replace(v_line_data, chr(10), ''), chr(13), '');
		if (v_line_data = 'END') then
		  exit;
		end if;
		v_line_count := v_line_count + 1;

		v_rec_data := split_gl_data(p_line_buff => v_line_data, p_delimiter => v_delimiter, p_debug_flag => v_debug_flag);
		begin
		  v_rec_data.request_id := v_request_id;
		  v_rec_data.interface_date := sysdate;

		  insert into dtac_gl_interface_temp
			(status
			,set_of_books_id
			,accounting_date
			,currency_code
			,date_created
			,created_by
			,actual_flag
			,user_je_category_name
			,user_je_source_name
			,accounted_dr
			,accounted_cr
			,currency_conversion_date
			,currency_conversion_rate
			,entered_dr
			,entered_cr
			,je_line_num
			,period_name
			,reference1
			,reference4
			,reference5
			,segment1
			,segment11
			,segment10
			,segment12
			,segment13
			,segment14
			,segment18
			,segment19
			,segment21
			,reference21
			,request_id
			,interface_date
			,validate_status
			,error_msg)
		  values
			(v_rec_data.status
			,v_rec_data.set_of_books_id
			,v_rec_data.accounting_date
			,v_rec_data.currency_code
			,v_rec_data.date_created
			,v_rec_data.created_by
			,v_rec_data.actual_flag
			,v_rec_data.user_je_category_name
			,v_rec_data.user_je_source_name
			,v_rec_data.accounted_dr
			,v_rec_data.accounted_cr
			,v_rec_data.currency_conversion_date
			,v_rec_data.currency_conversion_rate
			,v_rec_data.entered_dr
			,v_rec_data.entered_cr
			,v_rec_data.je_line_num
			,v_rec_data.period_name
			,v_rec_data.reference1
			,v_rec_data.reference4
			,v_rec_data.reference5
			,v_rec_data.segment1
			,v_rec_data.segment11
			,v_rec_data.segment10
			,v_rec_data.segment12
			,v_rec_data.segment13
			,v_rec_data.segment14
			,v_rec_data.segment18
			,v_rec_data.segment19
			,v_rec_data.segment21
			,v_rec_data.reference21
			,v_rec_data.request_id
			,v_rec_data.interface_date
			,v_rec_data.validate_status
			,v_rec_data.error_msg);

		  write_log('Line#' || to_char(v_line_count, 90000) || ' [ OK] =>  ' || v_line_data);
		exception
		  when others then
			v_insert_error_flag := 'Y';
			write_log('Line#' || to_char(v_line_count, 90000) || ' [ERR] =>  ' || v_line_data);
			write_output('Line#' || to_char(v_line_count, 90000) || ' [ERR] =>  ' || sqlerrm);
		end;
	  exception
		when no_data_found then
		  exit;
		when others then
		  v_insert_error_flag := 'Y';
		  write_log('Read data from text file error :  ' || sqlerrm);
		  exit;
	  end;
	end loop;
	-- check open and close
	if (utl_file.is_open(file => v_file_handle)) then
	  utl_file.fclose(file => v_file_handle);
	end if;

	if (v_insert_error_flag = 'Y') then
	  rollback to savepoint before_insert_temp;
	  write_output(' ');
	  write_output(' ');
	  write_output(g_log_line);

	  write_log(' ');
	  write_log('tac_discovery_interface_util.interface_gl @import_data exception when insert data. See output for error detail.');

	  err_msg := 'Error tac_discovery_interface_util.interface_gl @import_data : Some record error during insert to table. See output for detail.';
	  err_code := '2';
	  move_data_file(get_directory_path(g_gl_inbox) || '/' || v_file_name, get_directory_path(g_gl_error));

	  return;
	else
	  commit;
	  move_data_file(get_directory_path(g_gl_inbox) || '/' || v_file_name, get_directory_path(g_gl_history));
	end if;
	-- End of import data from text file

	savepoint before_insert_interface;
	v_insert_error_flag := 'N';
	-- validate GL
	if (validate_grn(v_request_id) is not null) then
	  write_log('Validate : Failed.');
	else
	  write_log('Validate : Pass.');
	  if (v_insert_interface_table = 'Y') then
		begin
		  insert into gl_interface
			(status
			,set_of_books_id
			,accounting_date
			,currency_code
			,date_created
			,created_by
			,actual_flag
			,user_je_category_name
			,user_je_source_name
			,accounted_dr
			,accounted_cr
			,currency_conversion_date
			,currency_conversion_rate
			,entered_dr
			,entered_cr
			,je_line_num
			,period_name
			,reference1
			,reference4
			,reference5
			,segment1
			,segment11
			,segment10
			,segment12
			,segment13
			,segment14
			,segment18
			,segment19
			,segment21
			,reference21)
			select status
				  ,set_of_books_id
				  ,accounting_date
				  ,currency_code
				  ,date_created
				  ,created_by
				  ,actual_flag
				  ,user_je_category_name
				  ,user_je_source_name
				  ,accounted_dr
				  ,accounted_cr
				  ,currency_conversion_date
				  ,currency_conversion_rate
				  ,entered_dr
				  ,entered_cr
				  ,je_line_num
				  ,period_name
				  ,reference1
				  ,reference4
				  ,reference5
				  ,segment1
				  ,segment11
				  ,segment10
				  ,segment12
				  ,segment13
				  ,segment14
				  ,segment18
				  ,segment19
				  ,segment21
				  ,reference21
			from dtac_gl_interface_temp tmp
			where tmp.request_id = v_request_id
				  and tmp.status = 'NEW';
		  v_count_gl := sql%rowcount;
		  commit;
		exception
		  when others then
			v_insert_error_flag := 'Y';
			rollback to savepoint before_insert_interface;
			write_output(' ');
			write_output(' ');
			write_output(sqlerrm);

			write_log(' ');
			write_log('tac_discovery_interface_util.interface_gl @insert interface exception when insert data to gl_interface table. See output for error detail.');

			err_msg := 'Error tac_discovery_interface_util.interface_gl @insert interface : Some record during insert data to gl_interface table. See output for detail.';
			err_code := '2';
			return;
		end;
	  end if;
	end if;

	if (v_count_gl = v_line_count or v_insert_interface_table = 'N') then
	  err_code := '0';
	else
	  err_code := '2';
	  write_output(' ');
	  write_output('ERROR : Total records that insert to gl_interface table are not equal (' || v_count_gl || '/' || v_line_count ||
				   ' with text file.');

	  write_log(' ');
	  write_log('ERROR : Total records that insert to gl_interface table are not equal (' || v_count_gl || '/' || v_line_count || ' with text file.');
	end if;

  exception
	when utl_file.invalid_path then
	  write_output('Invalid File Location');
	  write_log('Invalid File Location');
	  raise_application_error(-20052, 'Invalid File Location');
	when utl_file.invalid_operation then
	  write_output('Invalid File Operation');
	  write_log('Invalid Operation');
	  raise_application_error(-20054, 'Invalid Operation');
	when utl_file.read_error then
	  write_output('Read File Error');
	  write_log('Read Error');
	  raise_application_error(-20055, 'Read Error');
	when utl_file.invalid_maxlinesize then
	  write_output('Line Size Exceeds 32K');
	  write_log('Line Size Exceeds 32K');
	  raise_application_error(-20060, 'Line Size Exceeds 32K');
	when utl_file.invalid_filename then
	  write_output('Invalid File Name');
	  write_log('Invalid File Name');
	  raise_application_error(-20061, 'Invalid File Name');
	when utl_file.access_denied then
	  write_output('File Access Denied By');
	  write_log('File Access Denied By');
	  raise_application_error(-20062, 'File Access Denied By');
	when others then
	  if (utl_file.is_open(file => v_file_handle)) then
		utl_file.fclose(file => v_file_handle);
	  end if;
	  err_code := '2';
	  err_msg := sqlerrm;
	  write_output(sqlerrm);
	  raise_application_error(-20011, sqlcode || ': ' || sqlerrm);
  end interface_gl;

end tac_discovery_interface_util;
