create or replace package body tac_ar_interface_util_vgs is
  --error_open_file exception;
  error_output_path exception;
  error_file_name exception;

  /*
  -- Procedure write_log
  -- Write message to log file for each concurrent request.
  */
  procedure write_log(p_msg varchar2) is
  begin
	fnd_file.put_line(fnd_file.log, p_msg);
  exception
	when others then
	  null;
  end write_log;

  procedure write_output(p_msg varchar2) is
  begin
	fnd_file.put_line(fnd_file.output, p_msg);
  exception
	when others then
	  null;
  end write_output;

  procedure write_line
  (
	p_file_handle in out utl_file.file_type
   ,p_line_buff in nvarchar2
  ) is
	user_error varchar2(255);
  begin

	utl_file.put_line_nchar(p_file_handle, p_line_buff);
	utl_file.fflush(p_file_handle);
  exception
	when utl_file.invalid_filehandle then
	  user_error := 'Invalid filehandle';
	  write_log(user_error);
	when utl_file.invalid_operation then
	  user_error := 'Invalid operation';
	  write_log(user_error);
	when utl_file.write_error then
	  user_error := 'Write error';
	  write_log(user_error);
	when others then
	  user_error := 'Error others : ' || substr(sqlerrm, 1, 240);
	  write_log(user_error);
  end write_line;

  function get_directory_path(p_directory_name varchar2) return varchar2 is
	v_directory_path varchar2(500);
  begin
	select dir.directory_path into v_directory_path from all_directories dir where dir.directory_name = p_directory_name;

	return v_directory_path;
  exception
	when others then
	  return null;
  end get_directory_path;

  function char_to_date
  (
	p_date_char varchar2
   ,p_format_mask varchar2 default null
  ) return date is
	v_date date;
  begin
	if (p_date_char is null) then
	  return null;
	end if;

	if (p_format_mask is not null) then
	  v_date := to_date(p_date_char, p_format_mask);
	  return v_date;
	end if;

	-- Format Mask = dd/mm/yyyy
	begin
	  v_date := to_date(p_date_char, 'dd/mm/yyyy');
	  return v_date;
	exception
	  when others then
		null;
	end;

	-- Format Mask = dd/m/yyyy
	begin
	  v_date := to_date(p_date_char, 'dd/m/yyyy');
	  return v_date;
	exception
	  when others then
		null;
	end;

	-- Format Mask = d/mm/yyyy
	begin
	  v_date := to_date(p_date_char, 'd/mm/yyyy');
	  return v_date;
	exception
	  when others then
		null;
	end;

	-- Format Mask = d/m/yyyy
	begin
	  v_date := to_date(p_date_char, 'd/m/yyyy');
	  return v_date;
	exception
	  when others then
		return p_date_char;
	end;
  end char_to_date;

  function extrack_data
  (
	p_buffer in varchar2
   ,p_separator in varchar2
   ,p_pos_data in number
  ) return varchar2 is
	v_pos_start number;
	v_pos_end number;
	v_subsrt_len number;
	v_data varchar2(255);
  begin
	if p_pos_data = 1 then
	  v_pos_start := 1;
	else
	  v_pos_start := instr(p_buffer, p_separator, 1, p_pos_data - 1) + 1;
	end if;

	v_pos_end := instr(p_buffer, p_separator, 1, p_pos_data);

	if v_pos_end = 0 then
	  v_subsrt_len := length(p_buffer);
	else
	  v_subsrt_len := v_pos_end - v_pos_start;
	end if;
	v_data := substr(p_buffer, v_pos_start, v_subsrt_len);

	return replace(trim(v_data), chr(10), '');
  exception
	when others then
	  return null;
  end extrack_data;

  function split_data
  (
	p_line_buff in varchar2
   ,p_delimiter in varchar2 default '|'
   ,p_debug_flag varchar2 default 'N'
  ) return tac_ra_interface_lines_all %rowtype is
	v_rec_data tac_ra_interface_lines_all %rowtype;
	v_creation_date date := sysdate;
  begin
	if p_line_buff is null then
	  return v_rec_data;
	end if;
	v_rec_data.batch_source_name := extrack_data(p_line_buff, p_delimiter, 1);
	v_rec_data.set_of_books_id := extrack_data(p_line_buff, p_delimiter, 2);
	v_rec_data.line_type := extrack_data(p_line_buff, p_delimiter, 3);
	v_rec_data.interface_line_context := extrack_data(p_line_buff, p_delimiter, 4);
	v_rec_data.interface_line_attribute1 := extrack_data(p_line_buff, p_delimiter, 5);
	v_rec_data.interface_line_attribute2 := extrack_data(p_line_buff, p_delimiter, 6);
	v_rec_data.comments := extrack_data(p_line_buff, p_delimiter, 7);
	v_rec_data.currency_code := extrack_data(p_line_buff, p_delimiter, 8);
	v_rec_data.conversion_rate := extrack_data(p_line_buff, p_delimiter, 9);
	v_rec_data.conversion_type := extrack_data(p_line_buff, p_delimiter, 10);
	v_rec_data.amount := extrack_data(p_line_buff, p_delimiter, 11);
	v_rec_data.cust_trx_type_name := extrack_data(p_line_buff, p_delimiter, 12);
	v_rec_data.term_name := extrack_data(p_line_buff, p_delimiter, 13);
	v_rec_data.orig_system_bill_customer_ref := extrack_data(p_line_buff, p_delimiter, 14);
	v_rec_data.orig_system_bill_address_ref := extrack_data(p_line_buff, p_delimiter, 15);
	v_rec_data.orig_system_ship_customer_ref := extrack_data(p_line_buff, p_delimiter, 16);
	v_rec_data.orig_system_ship_address_ref := extrack_data(p_line_buff, p_delimiter, 17);
	v_rec_data.trx_date := to_date(extrack_data(p_line_buff, p_delimiter, 18), g_date_format);
	v_rec_data.gl_date := to_date(extrack_data(p_line_buff, p_delimiter, 19), g_date_format);
	v_rec_data.line_number := extrack_data(p_line_buff, p_delimiter, 20);
	v_rec_data.mtl_system_items_seg1 := extrack_data(p_line_buff, p_delimiter, 21);
	v_rec_data.description := extrack_data(p_line_buff, p_delimiter, 22);
	v_rec_data.quantity := extrack_data(p_line_buff, p_delimiter, 23);
	v_rec_data.unit_selling_price := extrack_data(p_line_buff, p_delimiter, 24);
	v_rec_data.tax_code := extrack_data(p_line_buff, p_delimiter, 25);
	v_rec_data.uom_code := extrack_data(p_line_buff, p_delimiter, 26);
	v_rec_data.primary_salesrep_number := extrack_data(p_line_buff, p_delimiter, 27);
	v_rec_data.status := extrack_data(p_line_buff, p_delimiter, 28);

	v_rec_data.created_by := -1;
	v_rec_data.creation_date := v_creation_date;

	if p_debug_flag = 'Y' then
	  write_log(g_log_line);
	  write_log('Line buffer = ' || p_line_buff);
	  write_log('        BATCH_SOURCE_NAME  = ' || v_rec_data.batch_source_name);
	  write_log('        SET_OF_BOOKS_ID  = ' || v_rec_data.set_of_books_id);
	  write_log('        LINE_TYPE  = ' || v_rec_data.line_type);
	  write_log('        INTERFACE_LINE_CONTEXT  = ' || v_rec_data.interface_line_context);
	  write_log('        INTERFACE_LINE_ATTRIBUTE1  = ' || v_rec_data.interface_line_attribute1);
	  write_log('        INTERFACE_LINE_ATTRIBUTE2  = ' || v_rec_data.interface_line_attribute2);
	  write_log('        COMMENTS  = ' || v_rec_data.comments);
	  write_log('        CURRENCY_CODE  = ' || v_rec_data.currency_code);
	  write_log('        CONVERSION_RATE  = ' || v_rec_data.conversion_rate);
	  write_log('        CONVERSION_TYPE  = ' || v_rec_data.conversion_type);
	  write_log('        AMOUNT  = ' || v_rec_data.amount);
	  write_log('        CUST_TRX_TYPE_NAME  = ' || v_rec_data.cust_trx_type_name);
	  write_log('        TERM_NAME  = ' || v_rec_data.term_name);
	  write_log('        ORIG_SYSTEM_BILL_CUSTOMER_REF  = ' || v_rec_data.orig_system_bill_customer_ref);
	  write_log('        ORIG_SYSTEM_BILL_ADDRESS_REF  = ' || v_rec_data.orig_system_bill_address_ref);
	  write_log('        ORIG_SYSTEM_SHIP_CUSTOMER_REF  = ' || v_rec_data.orig_system_ship_customer_ref);
	  write_log('        ORIG_SYSTEM_SHIP_ADDRESS_REF  = ' || v_rec_data.orig_system_ship_address_ref);
	  write_log('        TRX_DATE  = ' || to_char(v_rec_data.trx_date, g_date_format));
	  write_log('        GL_DATE  = ' || to_char(v_rec_data.gl_date, g_date_format));
	  write_log('        LINE_NUMBER  = ' || v_rec_data.line_number);
	  write_log('        MEMO_LINE_NAME  = ' || v_rec_data.mtl_system_items_seg1);
	  write_log('        DESCRIPTION  = ' || v_rec_data.description);
	  write_log('        QUANTITY  = ' || v_rec_data.quantity);
	  write_log('        UNIT_SELLING_PRICE  = ' || v_rec_data.unit_selling_price);
	  write_log('        TAX_CODE  = ' || v_rec_data.tax_code);
	  write_log('        UOM_CODE  = ' || v_rec_data.uom_code);
	  write_log('        PRIMARY_SALESREP_NUMBER  = ' || v_rec_data.primary_salesrep_number);
	  write_log('        STATUS  = ' || v_rec_data.status);
	end if;

	return v_rec_data;
  exception
	when others then
	  --write_log('Error @tac_ap_interface_util_smt.split_data : ' || sqlcode || ' - ' || sqlerrm);
	  v_rec_data.error_log := sqlcode || ' - ' || sqlerrm;
	  return v_rec_data;
  end split_data;

  procedure delete_temp_table
  (
	p_source_name varchar2 default 'VGS'
   ,p_old_days number default 45
  ) is
	pragma autonomous_transaction;
  begin
	delete from tac_ra_interface_lines_all
	where creation_date <= (sysdate - p_old_days)
		  and interface_line_context = nvl(p_source_name, 'VGS');
	commit;

  exception
	when others then
	  null;
  end delete_temp_table;

  procedure move_data_file
  (
	p_source_file varchar2
   ,p_dest_path varchar2
  ) is
	v_req_id number;
  begin
	v_req_id := fnd_request.submit_request(application => 'AR'
										  ,program => 'TACAR_MVFILE_VGS'
										  ,argument1 => p_source_file
										  ,argument2 => p_dest_path);
	write_log('Move file from ' || p_source_file || ' to ' || p_dest_path || ' success.');
  exception
	when others then
	  write_log('Move file from ' || p_source_file || ' to ' || p_dest_path || ' failed with error ' || sqlerrm);
  end move_data_file;

  procedure import_data
  (
	err_msg out varchar2
   ,err_code out varchar2
   ,p_source_name varchar2
   ,p_file_path varchar2
   ,p_file_name_format varchar2
   ,p_file_name varchar2 default null
   ,p_delimiter varchar2 default '|'
   ,p_debug_flag varchar2 default 'N'
   ,p_user_id number
   ,p_request_id number
  ) is
	v_file_name varchar2(100);
	v_date date := sysdate;
	v_file_handle utl_file.file_type;
	v_line_data nvarchar2(32767);
	v_line_count number := 0;

	v_rec_data tac_ra_interface_lines_all%rowtype;
	v_insert_error_flag varchar2(1) := 'N';
  begin

	delete_temp_table(p_source_name, 45);

	if p_file_path is null then
	  raise utl_file.invalid_path;
	end if;

	if (p_file_name is not null) then
	  v_file_name := p_file_name;
	else
	  if p_file_name_format is null then
		raise utl_file.invalid_filename;
	  end if;
	  v_file_name := replace(p_file_name_format, '$DATE$', trim(to_char(v_date, 'yyyymmdd')));
	end if;

	write_log(g_log_line);
	write_log('Import data from file ' || get_directory_path(p_file_path) || '/' || v_file_name);
	begin
	  v_file_handle := utl_file.fopen(location => p_file_path, filename => v_file_name, open_mode => 'r');
	  --v_file_handle := utl_file.fopen_nchar(location => p_file_path, filename => v_file_name, open_mode => 'r');
	  utl_file.fclose(v_file_handle);
	  --write_log('Read file first time.');
	exception
	  when others then
		null;
	end;
	v_file_handle := utl_file.fopen(location => p_file_path, filename => v_file_name, open_mode => 'r');
	--v_file_handle := utl_file.fopen_nchar(location => p_file_path, filename => v_file_name, open_mode => 'r');

	savepoint before_insert_data;
	loop
	  begin
		utl_file.get_line(file => v_file_handle, buffer => v_line_data);
		--utl_file.get_line_nchar(file => v_file_handle, buffer => v_line_data);
		v_line_data := replace(replace(v_line_data, chr(10), ''), chr(13), '');
		if (v_line_data = 'END') then
		  exit;
		end if;
		v_line_count := v_line_count + 1;
		v_rec_data := split_data(p_line_buff => v_line_data, p_delimiter => p_delimiter, p_debug_flag => p_debug_flag);
		begin
		  insert into tac_ra_interface_lines_all
			(batch_source_name
			,set_of_books_id
			,line_type
			,interface_line_context
			,interface_line_attribute1
			,interface_line_attribute2
			,comments
			,currency_code
			,conversion_date
			,conversion_rate
			,conversion_type
			,amount
			,cust_trx_type_name
			,cust_trx_type_id
			,term_name
			,term_id
			,orig_system_bill_customer_ref
			,orig_system_bill_customer_id
			,orig_system_bill_address_ref
			,orig_system_bill_address_id
			,orig_system_ship_customer_ref
			,orig_system_ship_customer_id
			,orig_system_ship_address_ref
			,orig_system_ship_address_id
			,trx_number
			,trx_date
			,gl_date
			,line_number
			,mtl_system_items_seg1
			,inventory_item_id
			,description
			,quantity
			,unit_selling_price
			,tax_code
			,uom_code
			,primary_salesrep_number
			,primary_salesrep_id
			,status
			,error_log
			,request_id
			,created_by
			,creation_date)
		  values
			(v_rec_data.batch_source_name
			,v_rec_data.set_of_books_id
			,v_rec_data.line_type
			,v_rec_data.interface_line_context
			,v_rec_data.interface_line_attribute1
			,v_rec_data.interface_line_attribute2
			,v_rec_data.comments
			,v_rec_data.currency_code
			,v_rec_data.conversion_date
			,v_rec_data.conversion_rate
			,v_rec_data.conversion_type
			,v_rec_data.amount
			,v_rec_data.cust_trx_type_name
			,v_rec_data.cust_trx_type_id
			,v_rec_data.term_name
			,v_rec_data.term_id
			,v_rec_data.orig_system_bill_customer_ref
			,v_rec_data.orig_system_bill_customer_id
			,v_rec_data.orig_system_bill_address_ref
			,v_rec_data.orig_system_bill_address_id
			,v_rec_data.orig_system_ship_customer_ref
			,v_rec_data.orig_system_ship_customer_id
			,v_rec_data.orig_system_ship_address_ref
			,v_rec_data.orig_system_ship_address_id
			,v_rec_data.trx_number
			,v_rec_data.trx_date
			,v_rec_data.gl_date
			,v_rec_data.line_number
			,v_rec_data.mtl_system_items_seg1
			,v_rec_data.inventory_item_id
			,v_rec_data.description
			,v_rec_data.quantity
			,v_rec_data.unit_selling_price
			,v_rec_data.tax_code
			,v_rec_data.uom_code
			,v_rec_data.primary_salesrep_number
			,v_rec_data.primary_salesrep_id
			,v_rec_data.status
			,v_rec_data.error_log
			,p_request_id
			,p_user_id
			,v_rec_data.creation_date);

		  write_log('Line#' || to_char(v_line_count, 0000) || ' [ OK] =>  ' || v_line_data);
		exception
		  when others then
			v_insert_error_flag := 'Y';
			write_log('Line#' || to_char(v_line_count, 0000) || ' [ERR] =>  ' || v_line_data);
			write_output('Line#' || to_char(v_line_count, 0000) || ' [ERR] =>  ' || v_rec_data.error_log);
			write_output('');
		end;
	  exception
		when no_data_found then
		  exit;
	  end;
	end loop;
	write_log(g_log_line);
	-- check open and close
	if (utl_file.is_open(file => v_file_handle)) then
	  utl_file.fclose(file => v_file_handle);
	end if;

	if (v_insert_error_flag = 'Y') then
	  rollback to savepoint before_insert_data;
	  write_log('tac_ar_interface_util_vgs.import_data exception when insert data. See output for error detail.');
	  --write_log('Error @import_data : Some record error during insert to table.');

	  err_msg := 'Error @import_data : Some record error during insert to table. See output for detail.';
	  err_code := '2';
	  move_data_file(get_directory_path(g_inbox) || '/' || v_file_name, get_directory_path(g_error));
	else
	  commit;
	  move_data_file(get_directory_path(g_inbox) || '/' || v_file_name, get_directory_path(g_history));
	end if;

  exception
	when others then
	  -- check open and close
	  if (utl_file.is_open(file => v_file_handle)) then
		utl_file.fclose(file => v_file_handle);
	  else
		begin
		  rollback to savepoint before_insert_data;
		exception
		  when others then
			null;
		end;
	  end if;
	  write_log('tac_ar_interface_util_vgs.import_data exception when others .');
	  write_log('Error @import_data : ' || sqlcode || ' - ' || sqlerrm);

	  err_msg := 'Error @import_data : ' || sqlcode || ' - ' || sqlerrm;
	  err_code := '2';
	  move_data_file(get_directory_path(g_inbox) || '/' || v_file_name, get_directory_path(g_error));
  end import_data;

  procedure ar_interface_vgs
  (
	err_msg out varchar2
   ,err_code out varchar2
   ,p_org_id number
   ,p_set_of_books_id number
   ,p_source_name varchar2
   ,p_file_path varchar2
   ,p_file_name_format varchar2
   ,p_file_name varchar2 default null
   ,p_separator varchar2 default '|'
   ,p_debug_flag varchar2
  ) is
	v_user_id number;
	v_conc_request_id number;
	v_error_log varchar2(4000);
	v_status varchar2(50);
	v_line_count number := 0;
	v_validate_pass number := 0;
	v_validate_error number := 0;
	v_creation_date date := sysdate;

	v_cust_trx_type_id number;
	v_orig_system_bill_customer_id number;
	v_orig_system_bill_address_id number;
	v_bill_to_payment_term_id number;
	v_orig_system_ship_customer_id number;
	v_orig_system_ship_address_id number;
	v_cust_payment_term_id number;

	v_term_name ra_interface_lines_all.term_name%type;
	v_term_id number;
	v_inventory_item_id number;
	v_item_description ra_interface_lines_all.description%type;

	v_memo_line_name ra_interface_lines_all.memo_line_name%type;
	v_memo_line_id number;
	v_primary_salesrep_id ra_interface_lines_all.primary_salesrep_id%type;

  begin
	-- Process import from text file to temp table (tac_ra_interface_lines_all)
	-- End import data from text file

	v_user_id := fnd_profile.value('USER_ID');
	v_conc_request_id := fnd_profile.value('CONC_REQUEST_ID');
	--Stamp request_id
	begin

	  import_data(err_msg => err_msg
				 ,err_code => err_code
				 ,p_source_name => p_source_name
				 ,p_file_path => p_file_path
				 ,p_file_name_format => p_file_name_format
				 ,p_file_name => p_file_name
				 ,p_delimiter => p_separator
				 ,p_debug_flag => p_debug_flag
				 ,p_user_id => v_user_id
				 ,p_request_id => v_conc_request_id);

	  -- Check import status
	  if (nvl(err_code, '99') = '2') then
		return;
	  end if;

	  /*
      update tac_ra_interface_lines_all
      set request_id = v_conc_request_id
      where batch_source_name = nvl(p_source_name, 'VGS')
            and nvl(status, 'NEW') = 'NEW';
      commit;
      */
	end;

	if (p_debug_flag = 'Y') then
	  null;
	end if;

	write_log(g_log_line);
	write_log('Validate data...');
	for v_rec_data in (select inf.batch_source_name
							 ,inf.set_of_books_id
							 ,inf.line_type
							 ,inf.interface_line_context
							 ,inf.interface_line_attribute1
							 ,inf.interface_line_attribute2
							 ,inf.comments
							 ,inf.currency_code
							 ,inf.conversion_rate
							 ,inf.conversion_type
							 ,inf.amount
							 ,inf.cust_trx_type_name
							 ,inf.term_name
							 ,'derive from term_name' term_id
							 ,inf.orig_system_bill_customer_ref
							 ,inf.orig_system_bill_customer_id
							 ,inf.orig_system_bill_address_ref
							 ,inf.orig_system_bill_address_id
							 ,inf.orig_system_ship_customer_ref
							 ,inf.orig_system_ship_customer_id
							 ,inf.orig_system_ship_address_ref
							 ,inf.orig_system_ship_address_id
							 ,inf.trx_date
							 ,inf.gl_date
							 ,inf.line_number
							 ,inf.mtl_system_items_seg1 memo_line_name
							 ,mml.memo_line_id memo_line_id
							 ,nvl(mml.description, inf.description) description
							 ,inf.quantity
							 ,inf.unit_selling_price
							 ,inf.tax_code
							 ,inf.uom_code
							 ,inf.primary_salesrep_number
							 ,inf.primary_salesrep_id
							 ,inf.status
							 ,inf.request_id
							 ,inf.rowid row_id
					   from tac_ra_interface_lines_all inf
					   left join ar_memo_lines_all_tl mml on (inf.mtl_system_items_seg1 = mml.name)
					   where inf.batch_source_name = p_source_name
							 and nvl(inf.status, 'NEW') = 'NEW'
							 and inf.set_of_books_id = p_set_of_books_id
							 and mml.org_id = p_org_id -- fixed by Nawatsapon, 18-Jun-2019, fix issue VG insert dupplicate SO line to ar interface table
							 and inf.request_id = v_conc_request_id
					   order by inf.interface_line_attribute1
							   ,inf.line_number)
	loop
	  v_error_log := '';
	  v_line_count := v_line_count + 1;

	  if v_rec_data.line_type is null then
		v_error_log := v_error_log || chr(10) || 'LINE_TYPE is null';
	  end if;
	  if v_rec_data.currency_code is null then
		v_error_log := v_error_log || chr(10) || 'CURRENCY_CODE is null';
	  end if;

	  if v_rec_data.conversion_type is null then
		v_error_log := v_error_log || chr(10) || 'CONVERSION_TYPE is null';
	  end if;

	  -- Get Transaction Type ID
	  begin
		select cust_trx_type_id into v_cust_trx_type_id from ra_cust_trx_types_all where name = v_rec_data.cust_trx_type_name;
	  exception
		when no_data_found then
		  v_error_log := v_error_log || chr(10) || 'Invalid CUST_TRX_TYPE_NAME (' || v_rec_data.cust_trx_type_name || ')';
		when others then
		  v_error_log := v_error_log || chr(10) || 'Validate CUST_TRX_TYPE_NAME, ' || sqlcode || ' : ' || substr(sqlerrm, 240);
		  v_cust_trx_type_id := null;
	  end;

	  -- Get bill customer id
	  --v_rec_data.orig_system_bill_customer_ref
	  --v_rec_data.orig_system_bill_customer_id
	  --
	  begin
		select c.customer_id
		into v_orig_system_bill_customer_id
		from ar_customers_v c
		where c.customer_number = v_rec_data.orig_system_bill_customer_ref;
	  exception
		when no_data_found then
		  v_error_log := v_error_log || chr(10) || 'Invalid ORIG_SYSTEM_BILL_CUSTOMER_REF (' || v_rec_data.orig_system_bill_customer_ref || ')';
		when others then
		  v_error_log := v_error_log || chr(10) || 'Validate ORIG_SYSTEM_BILL_CUSTOMER_REF, ' || sqlcode || ' : ' || substr(sqlerrm, 240);
		  v_orig_system_bill_customer_id := null;
	  end;

	  -- Get address_id from bill
	  --v_rec_data.orig_system_bill_address_ref (location in site use)
	  --v_rec_data.orig_system_bill_address_id (address_id)
	  begin
		select addr.cust_acct_site_id
			  ,cp.standard_terms
		into v_orig_system_bill_address_id
			,v_bill_to_payment_term_id
		from ar_customers_v cust
			,hz_party_sites ps
			,hz_cust_acct_sites_all addr
			,hz_locations loc
			,hz_cust_site_uses_all site_use
			,ar_customer_profiles_v cp
		where cust.party_id = ps.party_id
			  and ps.location_id = loc.location_id
			  and ps.party_site_id = addr.party_site_id
			  and addr.cust_acct_site_id = site_use.cust_acct_site_id
			  and site_use.site_use_code = 'BILL_TO'
			  and cust.customer_number = v_rec_data.orig_system_bill_customer_ref
			  and site_use.location = v_rec_data.orig_system_bill_address_ref
			  and site_use.site_use_id = cp.site_use_id(+);
	  exception
		when no_data_found then
		  v_error_log := v_error_log || chr(10) || 'Invalid ORIG_SYSTEM_BILL_ADDRESS_REF (' || v_rec_data.orig_system_bill_address_ref || ')';
		when others then
		  v_error_log := v_error_log || chr(10) || 'Validate ORIG_SYSTEM_BILL_ADDRESS_REF, ' || sqlcode || ' : ' || substr(sqlerrm, 240);
		  v_orig_system_bill_address_id := null;
	  end;

	  -- Get bill customer id
	  --v_rec_data.orig_system_ship_customer_ref
	  --v_rec_data.orig_system_ship_customer_id
	  begin
		select c.customer_id
		into v_orig_system_ship_customer_id
		from ar_customers_v c
		where c.customer_number = v_rec_data.orig_system_ship_customer_ref;
	  exception
		when no_data_found then
		  v_error_log := v_error_log || chr(10) || 'Invalid ORIG_SYSTEM_SHIP_CUSTOMER_REF (' || v_rec_data.orig_system_ship_customer_ref || ')';
		when others then
		  v_error_log := v_error_log || chr(10) || 'Validate ORIG_SYSTEM_SHIP_CUSTOMER_REF, ' || sqlcode || ' : ' || substr(sqlerrm, 240);
		  v_orig_system_ship_customer_id := null;
	  end;

	  -- Get address_id from ship
	  --v_rec_data.orig_system_ship_address_ref (location in site use)
	  --v_rec_data.orig_system_ship_address_id (address_id)
	  begin
		select addr.cust_acct_site_id
		into v_orig_system_ship_address_id
		from ar_customers_v cust
			,hz_party_sites ps
			,hz_cust_acct_sites_all addr
			,hz_locations loc
			,hz_cust_site_uses_all site_use
		where cust.party_id = ps.party_id
			  and ps.location_id = loc.location_id
			  and ps.party_site_id = addr.party_site_id
			  and addr.cust_acct_site_id = site_use.cust_acct_site_id
			  and site_use.site_use_code = 'SHIP_TO'
			  and cust.customer_number = v_rec_data.orig_system_ship_customer_ref
			  and site_use.location = v_rec_data.orig_system_ship_address_ref;
	  exception
		when no_data_found then
		  v_error_log := v_error_log || chr(10) || 'Invalid ORIG_SYSTEM_SHIP_ADDRESS_REF (' || v_rec_data.orig_system_ship_address_ref || ')';
		when others then
		  v_error_log := v_error_log || chr(10) || 'Validate ORIG_SYSTEM_SHIP_ADDRESS_REF, ' || sqlcode || ' : ' || substr(sqlerrm, 240);
		  v_orig_system_ship_address_id := null;
	  end;
	  -- Get Term ID
	  if (v_rec_data.term_name is null) then
		begin
		  select cp.standard_terms
		  into v_cust_payment_term_id
		  from ar_customer_profiles_v cp
		  where customer_id = v_orig_system_ship_customer_id
				and site_use_id is null;
		exception
		  when others then
			v_cust_payment_term_id := null;
		end;

		begin
		  select nvl(nvl(term_id, v_bill_to_payment_term_id), v_cust_payment_term_id)
		  into v_term_id
		  from ra_terms_vl
		  where name = v_rec_data.term_name;
		exception
		  when no_data_found then
			v_term_id := null;
		  when others then
			v_term_id := null;
		end;
		v_term_id := nvl(nvl(v_term_id, v_bill_to_payment_term_id), v_cust_payment_term_id);
	  else
		begin
		  select term_id into v_term_id from ra_terms_vl where name = v_rec_data.term_name;
		exception
		  when others then
			v_term_id := null;
		end;
	  end if;
	  if v_rec_data.description is null then
		v_error_log := v_error_log || chr(10) || 'Invalid MEMO Line (' || v_rec_data.memo_line_name || ').';
	  end if;

	  if v_error_log is not null then
		v_error_log := 'Error :  Invoice ' || v_rec_data.interface_line_attribute1 || '  line ' || v_rec_data.line_number || v_error_log;
		v_error_log := v_error_log;
		v_status := 'ERROR';
		v_validate_error := v_validate_error + 1;
		write_log('  ' || replace(v_error_log, chr(10), chr(10) || '    - '));
		write_output('  ' || replace(v_error_log, chr(10), chr(10) || '    - '));
	  else
		v_status := 'PROCESSED';
		v_validate_pass := v_validate_pass + 1;
		write_log('Passed :  Invoice ' || v_rec_data.interface_line_attribute1 || '  line ' || v_rec_data.line_number);
	  end if;
	  -- Update temp table
	  begin
		update tac_ra_interface_lines_all
		set status = v_status
		   ,error_log = v_error_log
		   ,request_id = v_conc_request_id
		where rowid = v_rec_data.row_id;
	  exception
		when others then
		  write_log('Update table error : ' || sqlerrm);
	  end;
	  if v_status = 'PROCESSED' then
		insert into ra_interface_lines_all
		  (batch_source_name
		  ,set_of_books_id
		  ,line_type
		  ,interface_line_context
		  ,interface_line_attribute1
		  ,interface_line_attribute2
		  ,comments
		  ,currency_code
		  ,conversion_rate
		  ,conversion_type
		  ,amount
		  ,cust_trx_type_name
		  ,cust_trx_type_id
		  ,term_name
		  ,term_id
		  ,orig_system_bill_customer_id
		  ,orig_system_bill_address_id
		  ,orig_system_ship_customer_id
		  ,orig_system_ship_address_id
		  ,trx_date
		  ,gl_date
		  ,line_number
		  ,memo_line_name
		  ,memo_line_id
		  ,description
		  ,quantity
		  ,unit_selling_price
		  ,tax_code
		  ,uom_code
		  ,primary_salesrep_number
		  ,primary_salesrep_id
		  ,created_by
		  ,creation_date)
		values
		  (v_rec_data.batch_source_name
		  ,v_rec_data.set_of_books_id
		  ,v_rec_data.line_type
		  ,v_rec_data.interface_line_context
		  ,v_rec_data.interface_line_attribute1
		  ,v_rec_data.interface_line_attribute2
		  ,v_rec_data.comments
		  ,v_rec_data.currency_code
		  ,v_rec_data.conversion_rate
		  ,v_rec_data.conversion_type
		  ,v_rec_data.amount
		  ,v_rec_data.cust_trx_type_name
		  ,v_cust_trx_type_id
		  ,v_rec_data.term_name
		  ,v_term_id
		  ,v_orig_system_bill_customer_id
		  ,v_orig_system_bill_address_id
		  ,v_orig_system_ship_customer_id
		  ,v_orig_system_ship_address_id
		  ,v_rec_data.trx_date
		  ,v_rec_data.gl_date
		  ,v_rec_data.line_number
		  ,v_rec_data.memo_line_name
		  ,v_rec_data.memo_line_id
		  ,v_rec_data.description
		  ,v_rec_data.quantity
		  ,v_rec_data.unit_selling_price
		  ,v_rec_data.tax_code
		  ,v_rec_data.uom_code
		  ,v_rec_data.primary_salesrep_number
		  ,v_primary_salesrep_id
		  ,v_user_id
		  ,v_creation_date);
	  end if;
	end loop;
	commit;

	write_log(' ');
	write_log(g_log_line);
	if (v_validate_error > 0) and (v_validate_error = v_line_count) then
	  err_code := '2';
	elsif (v_validate_error > 0) and (v_validate_pass > 0) then
	  err_code := '1';
	else
	  err_code := '0';
	end if;
	write_log('');
	write_log('Summary :');
	write_log('Total record = ' || v_line_count);
	write_log('Successes record = ' || v_validate_pass);
	write_log('Error record = ' || v_validate_error);
	write_log(g_log_line);

	write_output(g_log_line);
	write_output('Summary :');
	write_output('  Total record = ' || v_line_count);
	write_output('  Successes record = ' || v_validate_pass);
	write_output('  Error record = ' || v_validate_error);
	write_output(' ');
	write_output(g_log_line);
	write_output('Please see log of request_id (' || v_conc_request_id || ')  for the detail.');
  exception
	when others then
	  err_code := '2';
	  err_msg := 'Error : ' || sqlcode || ': ' || sqlerrm;
	  write_log('Error : ' || sqlcode || ': ' || sqlerrm);
	  raise_application_error(-20011, sqlcode || ': ' || sqlerrm);
  end ar_interface_vgs;

  procedure ar_invoice_number_out
  (
	err_msg out varchar2
   ,err_code out varchar2
   ,p_org_id number
   ,p_source_name varchar2
   ,p_transaction_date_from varchar2
   ,p_transaction_date_to varchar2
   ,p_output_path varchar2
   ,p_file_name_format varchar2
   ,p_separator varchar2 default '|'
  ) is
	v_file_handle utl_file.file_type;
	v_line_data nvarchar2(32767);
	v_file_name varchar2(100);
	v_date date := sysdate;
	v_line_count number;
  begin
	err_code := 2;
	if p_output_path is null then
	  raise utl_file.invalid_path;
	end if;
	if p_file_name_format is null then
	  raise utl_file.invalid_filename;
	end if;

	v_file_name := p_file_name_format;
	v_file_name := replace(v_file_name, '$DATETIME$', to_char(v_date, 'YYYYMMDD_HH24MISS'));
	v_file_name := replace(v_file_name, '$TRXMONTH$', to_char(to_date(p_transaction_date_from, 'yyyy/mm/dd hh24:mi:ss'), 'YYYYMM'));
	-- open file for write item master
	write_log('+++++++++++++++++++++++++++++++++++++++++++++++');
	write_log('Create file ' || v_file_name || ' to write data');

	v_file_handle := utl_file.fopen_nchar(p_output_path, v_file_name, 'w');
	begin
	  utl_file.fclose(v_file_handle);
	exception
	  when others then
		null;
	end;
	v_file_handle := utl_file.fopen_nchar(p_output_path, v_file_name, 'w', max_linesize);

	write_log('+---------------------------------------------------------------------------+');
	write_log('Qeury data and write to file.');
	write_log(' ');
	v_line_count := 0;
	for v_rec_data in (select rct.interface_header_attribute1 ref_invoice_number_dcp
							 ,rct.trx_number transaction_number
							 ,rct.rac_bill_to_customer_num bill_to_number
							 ,to_char(rct.trx_date, 'DD-MM-YYYY') transaction_date
							 ,nvl(mml.name, ctl.description) memo_line_name
							 ,to_char(ctd.amount, 'fm9999999999990.00') distribution_amount
							 ,to_char(tax.extended_amount, 'fm99999990.00') tax_amount
							 ,to_char(nvl(ctd.amount, 0) + nvl(tax.extended_amount, 0), 'fm99999999990.00') unit_price
					   --,to_char(ctl.unit_selling_price, 'fm99999999990.00') unit_price -- Exclude VAT not use unitprice directly
					   from ra_customer_trx_partial_v rct
					   inner join ra_batch_sources bs on (rct.batch_source_id = bs.batch_source_id)
					   inner join ra_customer_trx_lines_v ctl on (rct.customer_trx_id = ctl.customer_trx_id)
					   left join ar_memo_lines_vl mml on (ctl.inventory_item_id = mml.memo_line_id)
					   inner join ra_cust_trx_line_gl_dist_v ctd on (ctl.customer_trx_line_id = ctd.customer_trx_line_id)
					   left join ar_tax_lines_v tax on (ctl.customer_trx_line_id = tax.link_to_cust_trx_line_id)
					   where ctl.line_type = 'LINE'
							 and rct.complete_flag = 'Y'
							 and rct.trx_date >= to_date(p_transaction_date_from, 'yyyy/mm/dd hh24:mi:ss')
							 and rct.trx_date < to_date(p_transaction_date_to, 'yyyy/mm/dd hh24:mi:ss') + 1
							 and bs.name = p_source_name)
	loop
	  v_line_data := replace(v_rec_data.ref_invoice_number_dcp, p_separator, '') || p_separator ||
					 replace(v_rec_data.transaction_number, p_separator, '') || p_separator || replace(v_rec_data.bill_to_number, p_separator, '') ||
					 p_separator || replace(v_rec_data.transaction_date, p_separator, '') || p_separator ||
					 replace(v_rec_data.memo_line_name, p_separator, '') || p_separator || replace(v_rec_data.distribution_amount, p_separator, '') ||
					 p_separator || replace(v_rec_data.tax_amount, p_separator, '') || p_separator || replace(v_rec_data.unit_price, p_separator, '');

	  write_line(v_file_handle, v_line_data);
	  write_log(v_line_data);
	  v_line_count := v_line_count + 1;
	end loop;
	write_log('+---------------------------------------------------------------------------+');

	if utl_file.is_open(v_file_handle) then
	  utl_file.fclose(v_file_handle);
	end if;
	err_code := 0;
  exception
	when utl_file.access_denied then
	  err_msg := 'ACCESS DENIED: Access to the file has been denied by the operating system';
	  write_log(err_msg);
	  raise_application_error(-20001, err_msg);
	when utl_file.file_open then
	  err_msg := 'FILE OPEN: File is already open';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.internal_error then
	  err_msg := 'INTERNAL ERROR: Unhandled internal error in the UTL_FILE package';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.invalid_filehandle then
	  err_msg := 'INVALID FILE HANDLE: File handle does not exist';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.invalid_filename then
	  err_msg := 'INVALID FILENAME: A file with the specified name does not exist in the path';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.invalid_maxlinesize then
	  err_msg := 'INVALID MAXLINESIZE: The MAX_LINESIZE value for FOPEN() is invalid; it should be within the range 1 to 32767';
	  write_log(err_msg);
	  raise_application_error(-20001, err_msg);
	when value_error then
	  err_msg := 'VAULE ERROR: GET_LINE value is larger than the buffer';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.invalid_mode then
	  err_msg := 'INVALID MODE: The open_mode parameter in FOPEN is invalid';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.invalid_operation then
	  err_msg := 'INVALID OPERATION: File could not be opened or operated on as requested';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.invalid_path then
	  err_msg := 'INVALID PATH: Specified path does not exist or is not visible to Oracle';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when utl_file.read_error then
	  err_msg := 'READ ERROR: Unable to read file';
	  write_log(err_msg);
	  raise_application_error(-20002, err_msg);
	when no_data_found then
	  null;
	when others then
	  if utl_file.is_open(v_file_handle) then
		utl_file.fclose(v_file_handle);
	  end if;
	  raise_application_error(-20011, sqlcode || ': ' || sqlerrm);
  end ar_invoice_number_out;

  function get_interface_error_log(p_request_id number) return varchar2 is
	v_error_msg varchar2(32000) := null;
  begin
	for r_err in (select inf.error_log
				  from tac_ra_interface_lines_all inf
				  where inf.request_id = p_request_id
						and inf.status = 'ERROR'
				  order by inf.interface_line_attribute1
						  ,inf.interface_line_attribute2)
	loop
	  v_error_msg := v_error_msg || r_err.error_log || chr(10);
	end loop;
	if (v_error_msg is null) then
	  v_error_msg := 'No Error';
	end if;
	return v_error_msg;
  exception
	when others then
	  return 'No Error';
  end get_interface_error_log;

  function get_interface_error_log return varchar2 is
	v_error_msg varchar2(32000) := null;
  begin
	for r_err in (select inf.error_log
				  from tac_ra_interface_lines_all inf
				  where inf.request_id = (select max(fcr.request_id) from fnd_conc_req_summary_v fcr where fcr.program_short_name = 'TACAR_INFVGS')
						and inf.status = 'ERROR'
				  order by inf.interface_line_attribute1
						  ,inf.interface_line_attribute2)
	loop
	  v_error_msg := v_error_msg || r_err.error_log || chr(10);
	end loop;
	if (v_error_msg is null) then
	  v_error_msg := 'No Error';
	end if;
	return v_error_msg;
  exception
	when others then
	  return 'No Error';
  end get_interface_error_log;

end tac_ar_interface_util_vgs;
