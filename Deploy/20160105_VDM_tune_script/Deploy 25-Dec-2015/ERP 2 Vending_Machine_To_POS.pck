create or replace package Vending_Machine_To_POS is
  procedure export_sn_with_prices (
    err_msg   out varchar2,
    err_code  out varchar2,
    p_vending_code varchar2,
    p_document_date varchar2
  );
  procedure export_sim_sold_by_others (
    err_msg   out varchar2,
    err_code  out varchar2,
    p_vending_code varchar2,
    p_document_date varchar2
  );
end Vending_Machine_To_POS;
/
create or replace package body Vending_Machine_To_POS is

  /* Write stats for being used in Splunk */
  procedure put_splunk_logs(
    folder varchar2, 
    data_file_path varchar2, 
    date_start date, 
    date_end date, 
    job_name varchar2, 
    rec_num number,
    status varchar2,
    message varchar2) 
  is
    f_h          utl_file.file_type;
    l_exists     boolean;
    l_size       number;
    l_block_size number;
    file_name    varchar2(15);
    host         varchar(50);
  begin
    file_name := to_char(sysdate,'MON-YYYY') || '.log';
    
    SELECT  host_name into host FROM v$instance;
    
    utl_file.fgetattr(folder, file_name, l_exists, l_size, l_block_size );
    if l_exists then
      f_h := utl_file.fopen(folder, file_name, 'a');
    else
      f_h := utl_file.fopen(folder, file_name, 'w');
      utl_file.put_line(f_h, 'BEGINDATETIME|ENDDATETIME|HOST|JOB_NAME|DEST_FILE|NUM_RECORDS|STATUS|ERROR_MESSAGE');
      utl_file.fflush(f_h);
    end if;
    
    utl_file.put_line(f_h, 
        to_char(date_start, 'YYYY-MM-DD hh24:mm:ss') || '|' || 
        to_char(date_end, 'YYYY-MM-DD hh24:mm:ss')   || '|' || 
        host              || '|' || 
        job_name          || '|' || 
        data_file_path    || '|' || 
        rec_num           || '|' || 
        status            || '|' || 
        message);
    utl_file.fclose(f_h);
  exception
    when others then
      null;
  end put_splunk_logs;

  /* Write logs to file */
  procedure write_log(p_msg varchar2) is
  begin
    fnd_file.put_line(fnd_file.log, p_msg);
    dbms_output.put(p_msg);
  exception
    when others then
      null;
  end write_log;
  
  /* Write text to file */
  procedure write_output(p_msg varchar2) is
  begin
    fnd_file.put_line(fnd_file.output, p_msg);
    dbms_output.put(p_msg);
  exception
	when others then
	  null;
  end write_output;
  
  /* Write out line of data */
  procedure write_line(p_file_handle in out utl_file.file_type , p_line_buff in varchar2) is
    user_error varchar2(255); -- to store translated file_error
  begin
    utl_file.put_line(p_file_handle, p_line_buff);
    utl_file.fflush(p_file_handle);
  exception
	when utl_file.invalid_filehandle then
	  user_error := 'Invalid filehandle';
	  write_log(user_error);
	when utl_file.invalid_operation then
	  user_error := 'Invalid operation';
	  write_log(user_error);
	when utl_file.write_error then
	  user_error := 'Write error';
	  write_log(user_error);
	when others then
	  user_error := 'Error others.';
	  write_log(user_error);
  end write_line;
  
  /* Write out line of data in Unicode */
  procedure write_line_nchar(p_file_handle in out utl_file.file_type, p_line_buff in nvarchar2) is
    user_error varchar2(255); -- to store translated file_error
  begin
    utl_file.put_line_nchar(p_file_handle, p_line_buff);
    utl_file.fflush(p_file_handle);
  exception
	when utl_file.invalid_filehandle then
	  user_error := 'Invalid filehandle';
	  write_log(user_error);
	when utl_file.invalid_operation then
	  user_error := 'Invalid operation';
	  write_log(user_error);
	when utl_file.write_error then
	  user_error := 'Write error';
	  write_log(user_error);
	when others then
	  user_error := 'Error others.';
	  write_log(user_error);
  end write_line_nchar;
  
  /* Read physical directory path from DB  */
  function get_directory_path(p_directory_name varchar2) return varchar2 is
    v_path varchar2(250);
  begin
    select directory_path into v_path from all_directories where directory_name = p_directory_name;
    return v_path;
  exception
	when others then
	  return null;
  end get_directory_path;
  
  /* Procedure to export SIM Prices data */
  procedure export_sn_with_prices(
    err_msg   out varchar2,
    err_code  out varchar2,
    p_vending_code varchar2,
	  p_document_date varchar2
  ) is
    v_file_handle   utl_file.file_type;
    v_line_data     nvarchar2(3000);
    v_file_name     varchar2(150);
    v_line_count    number := 0;
    v_bom_raw       raw(3);
    /** 
     * Define date range 
     * In case of stamdard datetime has passed from ERP, format 'YYYY/MM/DD HH24:MI:SS' will be in use.
     */
    v_date_format  varchar2(22) := 'YYYY/MM/DD HH24:MI:SS';
    v_document_date date ;
    v_file_date    date := sysdate-1; --nvl(v_document_date, sysdate);
	
    /* Vending Machine's configurations */
    v_ou_code 				      vending_machine_configs.ou_code%type;
    v_subinv_code 			    vending_machine_configs.subinv_code%type;
    v_part_codes 			      vending_machine_configs.part_codes%type;
    v_price_list_code 		  vending_machine_configs.price_list_code%type;
    v_data_path_id 			    vending_machine_configs.data_path_id%type;
    v_data_file_format 		  vending_machine_configs.data_file_format%type;
    v_data_field_separator 	vending_machine_configs.data_field_separator%type;
    v_transaction_type      vending_machine_configs.tran_type_code%type;
  begin
    begin
      /* Load Vending Machine's configurations */
      select ou_code, subinv_code, part_codes, price_list_code, data_path_id, data_file_format, data_field_separator, tran_type_code
      into v_ou_code, v_subinv_code, v_part_codes, v_price_list_code, v_data_path_id, v_data_file_format, v_data_field_separator, v_transaction_type
      from vending_machine_configs
      where vending_code = p_vending_code;
  	
      /* Date from of Document to get data */
      v_document_date := nvl(trunc(to_date(p_document_date, v_date_format)), trunc(sysdate)-1);

      
      /* Validate parameter */
      if v_data_path_id is null then
        raise utl_file.invalid_path;
      end if;
      if v_data_file_format is null then
        raise utl_file.invalid_filename;
      end if;
      
      /* De-templated filename if is using date pattern  */
      v_file_name := v_data_file_format;
      v_file_name := replace(v_file_name, '$DATETIME$', to_char(v_file_date, 'YYYYMMDD_HH24MISS'));
      v_file_name := replace(v_file_name, '$DATE$', to_char(v_file_date, 'YYYYMMDD'));
      
      /* Create new file, assume that the file isn't existed */
      v_file_handle := utl_file.fopen_nchar(v_data_path_id, v_file_name, 'w');
      begin
        utl_file.fclose(v_file_handle);
      exception
        when others then
        null;
      end;
      
      /* Log out at starting point */
      write_log('Exec-Prog: Vending Machine Price Data Generation Logs, started at ' || to_char(sysdate, 'DD/MM/YYYY HH24:MI:SS'));
      write_log('Input parameters: p_vending_code=' || p_vending_code || ', p_document_date=' || p_document_date);
    
      /* Open file as Unicode writable */
      v_file_handle := utl_file.fopen_nchar(v_data_path_id, v_file_name, 'w');
      v_bom_raw := hextoraw('EFBBBF');
      utl_file.put_raw(v_file_handle, v_bom_raw);
    
      /* Build header */
      v_line_data := 'LOCATION' || v_data_field_separator || 'SN' || v_data_field_separator || 'PHONE NUMBER' || v_data_field_separator || 'PRICE';
      write_line_nchar(v_file_handle, v_line_data);
      write_output(v_line_data);

      for r in (
        select 
          l.subinv_code,
          l.serial,
          sn.attribute1  phone_num,
          p.std_price
        from in_trans_sn_lot l
          ,in_trans_head h
          ,sa_price_item p 
          ,in_item i 
          ,mtl_serial_numbers sn 
        where l.ou_code = v_ou_code
          and l.subinv_code = v_subinv_code
          and l.tran_type_code = v_transaction_type
          and l.item_code in  (select * from the (select cast(to_str_comma_delimited(v_part_codes) as type_str_comma_delimited) from dual))
          and h.ou_code = l.ou_code
             and h.subinv_code = l.subinv_code
             and h.tran_type_code = l.tran_type_code
             and h.doc_no = l.doc_no
          and p.item_code = l.item_code
             and p.ou_code = l.ou_code
          and l.ou_code = i.ou_code
             and l.item_code = i.item_code
          and i.OA_ITEM = sn.inventory_item_id
             and l.serial =  sn.serial_number
             and sn.current_status = 3
          /*and not exists ( select 'X'
                                    from mtl_serial_numbers_interface sint
                                    where l.serial = sint.fm_serial_number ) */
          and h.doc_date >= v_document_date
          and h.doc_date < v_document_date +1
          and p.pricelist_code = v_price_list_code
          and p.start_date <= sysdate
          and (p.end_date is null or p.end_date >= sysdate)
      ) loop
        v_line_count := v_line_count + 1;
        v_line_data := r.subinv_code || v_data_field_separator 
          || r.serial || v_data_field_separator 
          || r.phone_num || v_data_field_separator 
          || r.std_price;
        write_line_nchar(v_file_handle, v_line_data);
        write_output(v_line_data);
      end loop;
  	
      /* Write out total results */
      write_line_nchar(v_file_handle, 'TOTAL,' || v_line_count || ',END');
      write_output('TOTAL,' || v_line_count || ',END');
    
      /* Close opened file handle */
      if utl_file.is_open(v_file_handle) then
        utl_file.fclose(v_file_handle);
      end if;
    
    exception
      when others then
        /* Wite out splunk logs - in behalf of failure  */
        put_splunk_logs(
          folder => v_data_path_id, 
          data_file_path => v_file_name,
          date_start => v_file_date, 
          date_end => sysdate, 
          job_name => 'Vending_Machine_To_POS.export_sn_with_prices', 
          rec_num => 0, 
          status => 'FAIL', 
          message => sqlcode || ': ' || sqlerrm);
        /* Throw error out to other scope */
        raise;
    end;
    
    /* Log out at finished point */
    write_log('Exec-Prog: Vending Machine Price Data Generation Logs ended successfully, ' || to_char(sysdate, 'DD/MM/YYYY HH24:MI:SS'));
    
    err_code := '0';
    err_msg  := 'Report was generated successfully';
    
    /* Wite out splunk logs - in behalf of succeed  */
    put_splunk_logs(
      folder => v_data_path_id, 
      data_file_path => v_file_name,
      date_start => v_file_date, 
      date_end => sysdate, 
      job_name => 'Vending_Machine_To_POS.export_sn_with_prices', 
      rec_num => v_line_count, 
      status => 'SUCCESS', 
      message => '');
  exception
    /* throwing errors */
    when utl_file.access_denied then
      err_msg := 'ACCESS DENIED: Access to the file has been denied by the operating system';
      write_log(err_msg);
      raise_application_error(-20001, err_msg);
    when utl_file.file_open then
      err_msg := 'FILE OPEN: File is already open';
      write_log(err_msg);
      raise_application_error(-20002, err_msg);
    when utl_file.internal_error then
      err_msg := 'INTERNAL ERROR: Unhandled internal error in the UTL_FILE package';
      write_log(err_msg);
      raise_application_error(-20002, err_msg);
    when utl_file.invalid_filehandle then
      err_msg := 'INVALID FILE HANDLE: File handle does not exist';
      write_log(err_msg);
      raise_application_error(-20002, err_msg);
    when utl_file.invalid_filename then
      err_msg := 'INVALID FILENAME: A file with the specified name does not exist in the path';
      write_log(err_msg);
      raise_application_error(-20002, err_msg);
    when utl_file.invalid_maxlinesize then
      err_msg := 'INVALID MAXLINESIZE: The MAX_LINESIZE value for FOPEN() is invalid; it should be within the range 1 to 32767';
      write_log(err_msg);
      raise_application_error(-20001, err_msg);
    when value_error then
      err_msg := 'VAULE ERROR: GET_LINE value is larger than the buffer';
      write_log(err_msg);
      raise_application_error(-20002, err_msg);
    when utl_file.invalid_mode then
      err_msg := 'INVALID MODE: The open_mode parameter in FOPEN is invalid';
      write_log(err_msg);
      raise_application_error(-20002, err_msg);
    when utl_file.invalid_operation then
      err_msg := 'INVALID OPERATION: File could not be opened or operated on as requested';
      write_log(err_msg);
      raise_application_error(-20002, err_msg);
    when utl_file.invalid_path then
      err_msg := 'INVALID PATH: Specified path does not exist or is not visible to Oracle';
      write_log(err_msg);
      raise_application_error(-20002, err_msg);
    when utl_file.read_error then
      err_msg := 'READ ERROR: Unable to read file';
      write_log(err_msg);
      raise_application_error(-20002, err_msg);
    when no_data_found then
      if utl_file.is_open(v_file_handle) then
      utl_file.fclose(v_file_handle);
      end if;
    when others then
      if utl_file.is_open(v_file_handle) then
      utl_file.fclose(v_file_handle);
      end if;
      raise_application_error(-20011, sqlcode || ': ' || sqlerrm);
  end export_sn_with_prices;

  procedure export_sim_sold_by_others(
    err_msg   out varchar2,
    err_code  out varchar2,
    p_vending_code varchar2,
	  p_document_date varchar2
  ) is
    v_file_handle   utl_file.file_type;
    v_line_data     nvarchar2(3000);
    v_file_name     varchar2(150);
    v_line_count    number := 0;
    v_bom_raw       raw(3);
    /** 
     * Define date range 
     * In case of stamdard datetime has passed from ERP, format 'YYYY/MM/DD HH24:MI:SS' will be in use.
     */
    v_date_format  varchar2(22) := 'YYYY/MM/DD HH24:MI:SS';
    v_document_date date;
    v_file_date    date := sysdate-1; --nvl(v_document_date, sysdate);
	
    /* Vending Machine's configurations */
    v_ou_code 				      vending_machine_configs.ou_code%type;
    v_subinv_code 			    vending_machine_configs.subinv_code%type;
	  v_sales_person 			      vending_machine_configs.sales_person%type;
    v_part_codes 			      vending_machine_configs.part_codes%type;
    v_data_path_id 			    vending_machine_configs.data_path_id%type;
    v_data_file_format 		  vending_machine_configs.data_purge_file_format%type;
    v_data_field_separator 	vending_machine_configs.data_field_separator%type;
  begin
    begin
      /* Load Vending Machine's configurations */
      select ou_code, subinv_code, part_codes, data_path_id, data_purge_file_format, data_field_separator, sales_person
      into v_ou_code, v_subinv_code, v_part_codes, v_data_path_id, v_data_file_format, v_data_field_separator, v_sales_person
      from vending_machine_configs
      where vending_code = p_vending_code;
      
      /* Date from of Document to get data */
      v_document_date := nvl(trunc(to_date(p_document_date, v_date_format)), trunc(sysdate)-1);
  	
      /* Validate parameter */
      if v_data_path_id is null then
        raise utl_file.invalid_path;
      end if;
      if v_data_file_format is null then
        raise utl_file.invalid_filename;
      end if;
      
      /* De-templated filename if is using date pattern  */
      v_file_name := v_data_file_format;
      v_file_name := replace(v_file_name, '$DATETIME$', to_char(v_file_date, 'YYYYMMDD_HH24MISS'));
      v_file_name := replace(v_file_name, '$DATE$', to_char(v_file_date, 'YYYYMMDD'));
      
      /* Create new file, assume that the file isn't existed */
      v_file_handle := utl_file.fopen_nchar(v_data_path_id, v_file_name, 'w');
      begin
        utl_file.fclose(v_file_handle);
      exception
        when others then
        null;
      end;
      
      /* Log out at starting point */
      write_log('Exec-Prog: Vending Machine Purge Data Generation Logs, started at ' || to_char(sysdate, 'DD/MM/YYYY HH24:MI:SS'));
      write_log('Input parameters: p_vending_code=' || p_vending_code || ', p_document_date=' || p_document_date);
    
      /* Open file as Unicode writable */
      v_file_handle := utl_file.fopen_nchar(v_data_path_id, v_file_name, 'w');
      v_bom_raw := hextoraw('EFBBBF');
      utl_file.put_raw(v_file_handle, v_bom_raw);
    
      /* Build header */
      v_line_data := 'SN' || v_data_field_separator || 'CUTOFF DATE';
      write_line_nchar(v_file_handle, v_line_data);
      write_output(v_line_data);

      for r in (
        select
          l.serial,
          t.doc_date
        from sa_transaction t
          join sa_trans_sn_lot l
            on l.ou_code = t.ou_code
            and l.subinv_code = t.subinv_code
            and l.doc_type = t.doc_type
            and l.doc_no = t.doc_no
        where t.ou_code = v_ou_code
          and t.subinv_code = v_subinv_code
          and t.salesperson <> v_sales_person
          and t.doc_date >= v_document_date
          and t.doc_date < v_document_date + 1
          and l.item_code in (select * from (select cast(to_str_comma_delimited(v_part_codes) as type_str_comma_delimited) from dual))
      ) loop
        v_line_count := v_line_count + 1;
        v_line_data := r.serial || v_data_field_separator || r.doc_date;
        write_line_nchar(v_file_handle, v_line_data);
        write_output(v_line_data);
      end loop;
  	
      /* Write out total results */
      write_line_nchar(v_file_handle, 'TOTAL,' || v_line_count || ',END');
      write_output('TOTAL,' || v_line_count || ',END');
    
      /* Close opened file handle */
      if utl_file.is_open(v_file_handle) then
        utl_file.fclose(v_file_handle);
      end if;
    
    exception
      when others then
        /* Wite out splunk logs - in behalf of failure  */
        put_splunk_logs(
          folder => v_data_path_id, 
          data_file_path => v_file_name,
          date_start => v_file_date, 
          date_end => sysdate, 
          job_name => 'Vending_Machine_To_POS.export_sim_sold_by_others', 
          rec_num => 0, 
          status => 'FAIL', 
          message => sqlcode || ': ' || sqlerrm);
        /* Throw error out to other scope */
        raise;
    end;
    
    /* Log out at finished point */
    write_log('Exec-Prog: Vending Machine Purge Data Generation Logs ended successfully, ' || to_char(sysdate, 'DD/MM/YYYY HH24:MI:SS'));
    
    err_code := '0';
    err_msg  := 'Report was generated successfully';
    
    /* Wite out splunk logs - in behalf of succeed  */
    put_splunk_logs(
      folder => v_data_path_id, 
      data_file_path => v_file_name,
      date_start => v_file_date, 
      date_end => sysdate, 
      job_name => 'Vending_Machine_To_POS.export_sim_sold_by_others', 
      rec_num => v_line_count, 
      status => 'SUCCESS', 
      message => '');
  exception
    /* throwing errors */
    when utl_file.access_denied then
      err_msg := 'ACCESS DENIED: Access to the file has been denied by the operating system';
      write_log(err_msg);
      raise_application_error(-20001, err_msg);
    when utl_file.file_open then
      err_msg := 'FILE OPEN: File is already open';
      write_log(err_msg);
      raise_application_error(-20002, err_msg);
    when utl_file.internal_error then
      err_msg := 'INTERNAL ERROR: Unhandled internal error in the UTL_FILE package';
      write_log(err_msg);
      raise_application_error(-20002, err_msg);
    when utl_file.invalid_filehandle then
      err_msg := 'INVALID FILE HANDLE: File handle does not exist';
      write_log(err_msg);
      raise_application_error(-20002, err_msg);
    when utl_file.invalid_filename then
      err_msg := 'INVALID FILENAME: A file with the specified name does not exist in the path';
      write_log(err_msg);
      raise_application_error(-20002, err_msg);
    when utl_file.invalid_maxlinesize then
      err_msg := 'INVALID MAXLINESIZE: The MAX_LINESIZE value for FOPEN() is invalid; it should be within the range 1 to 32767';
      write_log(err_msg);
      raise_application_error(-20001, err_msg);
    when value_error then
      err_msg := 'VAULE ERROR: GET_LINE value is larger than the buffer';
      write_log(err_msg);
      raise_application_error(-20002, err_msg);
    when utl_file.invalid_mode then
      err_msg := 'INVALID MODE: The open_mode parameter in FOPEN is invalid';
      write_log(err_msg);
      raise_application_error(-20002, err_msg);
    when utl_file.invalid_operation then
      err_msg := 'INVALID OPERATION: File could not be opened or operated on as requested';
      write_log(err_msg);
      raise_application_error(-20002, err_msg);
    when utl_file.invalid_path then
      err_msg := 'INVALID PATH: Specified path does not exist or is not visible to Oracle';
      write_log(err_msg);
      raise_application_error(-20002, err_msg);
    when utl_file.read_error then
      err_msg := 'READ ERROR: Unable to read file';
      write_log(err_msg);
      raise_application_error(-20002, err_msg);
    when no_data_found then
      if utl_file.is_open(v_file_handle) then
        utl_file.fclose(v_file_handle);
      end if;
    when others then
      if utl_file.is_open(v_file_handle) then
        utl_file.fclose(v_file_handle);
      end if;
      raise_application_error(-20011, sqlcode || ': ' || sqlerrm);
  end export_sim_sold_by_others;
end Vending_Machine_To_POS;
/
