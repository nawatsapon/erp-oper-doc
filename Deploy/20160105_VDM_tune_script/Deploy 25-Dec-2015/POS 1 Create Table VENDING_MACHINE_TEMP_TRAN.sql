create table VENDING_MACHINE_TEMP_TRAN
(
  GROUP_GUID   VARCHAR2(50) not null,
  SERIAL       VARCHAR2(40) not null,
  ITEM_CODE    VARCHAR2(20),
  LOCATOR_CODE VARCHAR2(15),
  UNIT_PRICE   NUMBER,
  CREATED_DATE DATE not null,
  CREATED_BY   VARCHAR2(15) not null,
  STATUS       VARCHAR2(20),
  ERROR_MSG    VARCHAR2(255),
  JOB_ID       NUMBER,
  JOB_SEQ      NUMBER,
  SUBINV_CODE  VARCHAR2(10),
  VENDING_CODE VARCHAR2(10)
);

-- Create/Recreate primary, unique and foreign key constraints 
alter table VENDING_MACHINE_TEMP_TRAN
  add constraint VM_GROUP_GUID_SERIAL_PK primary key (GROUP_GUID, SERIAL)
  using index 
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index VM_GROUP_GUID_SERIAL_IND1 on VENDING_MACHINE_TEMP_TRAN (SERIAL)
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
