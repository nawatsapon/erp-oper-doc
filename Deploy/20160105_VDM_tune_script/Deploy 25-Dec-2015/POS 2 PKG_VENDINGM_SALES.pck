create or replace package PKG_VENDINGM_SALES is

  -- Author  : T888062
  -- Created : 28/10/2015 13:25:48
  -- Purpose : For be using in reciept generation only for order that comes from Vending Machine

  -- Public function and procedure declarations
  procedure process_reciept(
    p_error_code              out number,
    p_error_mesg              out varchar2,
    p_receipt_no              out varchar2,
    p_document_date           out varchar2,
    p_job_no                  out varchar2,
    p_job_seq                 out varchar2,
    p_vending_code            varchar2,
    p_submission_date         varchar2,
    p_payment_code            varchar2,
    p_comma_separated_serials varchar2,
    p_cus_passport            varchar2,
    p_cus_fname               varchar2,
    p_cus_lname               varchar2,
    p_cus_address             varchar2);

end PKG_VENDINGM_SALES;
/
create or replace package body PKG_VENDINGM_SALES is

  /* Write out logs into job_monitor table */
  procedure put_log_monitor(p_job_id number, p_status varchar2, p_job_seq number, p_logs varchar2, p_rec_count number) is
  begin
    INSERT INTO jobs_monitor
      (job_id, job_seq, job_name, job_mesg, job_time, suc_cnt, err_cnt, upd_by, upd_date, upd_pgm, status)
    VALUES
      (p_job_id, p_job_seq, 'PKG_VENDINGM_SALES.process_reciept', p_logs, SYSDATE, p_rec_count, 0, 'Jobs', SYSDATE, 'Jobs', null);
  end put_log_monitor;

  /* Saving as temporary, for being calculated once validation passed */
  procedure save_sn_to_temp_table(p_guid varchar2,p_vending_code varchar2, p_item_code varchar2
    , p_serial varchar2, p_locator_code varchar2, p_unit_price number
    , p_status varchar2, p_errmsg varchar2, p_job_id varchar2, p_job_seq varchar2) is
  begin
    INSERT INTO VENDING_MACHINE_TEMP_TRAN
      (GROUP_GUID,VENDING_CODE, ITEM_CODE, SERIAL, LOCATOR_CODE, UNIT_PRICE, CREATED_DATE, CREATED_BY, STATUS, ERROR_MSG, JOB_ID, JOB_SEQ)
    VALUES
      (p_guid, p_vending_code, p_item_code, p_serial, p_locator_code, p_unit_price, sysdate, 'Vending Machine', p_status, p_errmsg, p_job_id, p_job_seq);
    COMMIT;      
  end save_sn_to_temp_table;
  
  procedure mark_all_as(p_guid varchar2, p_status varchar2) is
  begin
    
      UPDATE VENDING_MACHINE_TEMP_TRAN SET status = p_status
      WHERE group_guid = p_guid;
      
  end mark_all_as;

  -- Function and procedure implementations
  procedure process_reciept(
    p_error_code              out number,
    p_error_mesg              out varchar2,
    p_receipt_no              out varchar2,
    p_document_date           out varchar2,
    p_job_no                  out varchar2,
    p_job_seq                 out varchar2,
    p_vending_code            varchar2,
    p_submission_date         varchar2,
    p_payment_code            varchar2,
    p_comma_separated_serials varchar2,
    p_cus_passport            varchar2,
    p_cus_fname               varchar2,
    p_cus_lname               varchar2,
    p_cus_address             varchar2)
  IS   
    /* Calc A Transaction */
    CURSOR calc_a_transaction(guid varchar2, vat_rate number) IS
      select
        Sum(total_before_vat) total_before_vat,
        Sum(total_before_vat) - Sum(total_after_vat) total_vat,
        Sum(total_after_vat) total_after_vat
      from (
        select
            Min(item.unit_price)*Count(*) total_before_vat,
            Min(item.unit_price)*Count(*) - (Min(item.unit_price)*Count(*)) * 100/(100+vat_rate) vat,
            Min(item.unit_price)*Count(*) * (100/(100+vat_rate)) total_after_vat
        from VENDING_MACHINE_TEMP_TRAN item
        where item.group_guid = guid
        group by item.item_code
      );
    
    v_calc_transaction calc_a_transaction%rowtype;
    
    /* Calc Trasantion's Details */
    CURSOR calc_transaction_details(guid varchar2, vat_rate number)
    IS    
      select 
          item.item_code,
          item.locator_code,
          item.unit_price std_price, 
          Count(*) qty, 
          Min(item.unit_price)*Count(*) total_before_vat,
          Min(item.unit_price)*Count(*) - (Min(item.unit_price)*Count(*)) * 100/(100+vat_rate) vat,
          Min(item.unit_price)*Count(*) * 100/(100+vat_rate) total_after_vat
      from VENDING_MACHINE_TEMP_TRAN item
      where item.group_guid = guid
      group by item.item_code, item.locator_code, item.unit_price;
    
    /* CURSOR: All Single Serials */
    CURSOR c_serials_by_item_code(guid varchar2, p_item_code varchar2) IS
      select serial, locator_code, item_code, unit_price 
      from VENDING_MACHINE_TEMP_TRAN
      where group_guid=guid and item_code=p_item_code;
    /* END - CURSOR: All Single Serials */

    /* Check each SN status */
    CURSOR current_serial_status(p_card_number VARCHAR2) IS
      SELECT S.current_status,
             S.current_organization_id,
             S.current_subinventory_code,
             S.current_locator_id,
             L.segment1 LOCATOR_CODE,
             S.inventory_item_id,
             S.serial_number,
             S.LAST_TXN_SOURCE_NAME
      FROM mtl_serial_numbers S, mtl_item_locations L
      WHERE S.serial_number = p_card_number
        AND S.current_locator_id = L.inventory_location_id(+)
      ORDER BY S.current_status;
      
    v_current_serial_status current_serial_status%rowtype;
    /* END - Check each SN status */

    /* Source of Input Serials */
    CURSOR rec_sn_sources(sn_list varchar2) IS
    (
      select column_value as serial 
      from the (select cast(to_str_comma_delimited(sn_list) as type_str_comma_delimited) from dual)
    );
    /* END - Source of Input Serials */

    v_vat_code      sa_transaction.vat_code%TYPE;
    v_vat_rate      sa_transaction.vat_rate%TYPE;
    v_doc_no        sa_transaction.doc_no%TYPE;
    v_doc_date      sa_transaction.doc_date%TYPE;
    v_stype         sa_transaction.stype_code%TYPE;
    v_channel       sa_transaction.channel_code%TYPE;
    v_sales_person  sa_transaction.salesperson%TYPE;
    v_branch_code   sa_transaction.branch_code%TYPE;

    v_user          sa_transaction.upd_by%TYPE;
    v_vate_rate     NUMBER;

    v_unit_price    sa_trans_dtl.unitprice%TYPE;
    v_payment_code  sa_trans_pay.payment_code%TYPE;

    v_program_id    VARCHAR2(20);
    v_iv_trans_type pb_parameter_setup.parameter_value%TYPE;
    v_line_seq      NUMBER(2);
    v_sn_seq        NUMBER(2);
    v_count         NUMBER(2);
    
    /* Some from Vending Machine's configurations */
    v_ou_code 				      vending_machine_configs.ou_code%type;
    v_subinv_code 			    vending_machine_configs.subinv_code%type;
    v_part_codes 			      vending_machine_configs.part_codes%type;
    v_pricelist_code 		    vending_machine_configs.price_list_code%type;
    v_transaction_type      vending_machine_configs.tran_type_code%type;
    
    v_reciept_doc_type      vending_machine_configs.tran_type_code%type;

    /* Temp varivales */
    v_found_info                   varchar2(1);
    v_subinv_type_code             VARCHAR2(10);
    v_latest_sn_before_error       VARCHAR2(25);
    v_latest_refdoc_before_error   VARCHAR2(50);
    v_job_seq                      NUMBER;
    v_job_id                       NUMBER(10) := 602;
    v_item_code                    VARCHAR2(25);
    v_guid                         VARCHAR2(50);
    v_errmsg                       VARCHAR2(2048);

  begin
    select sys_guid() into v_guid from dual;
  
    /* Doc/Type for be visible to POS app */
    v_reciept_doc_type := 1;
    
    /* Get next job monitor Id */
    select nvl(Max(job_seq), 0)+1 into v_job_seq from jobs_monitor where job_id=v_job_id;
    /* Assign OUT parameters */
    p_job_no  := to_char(v_job_id);
    p_job_seq := to_char(v_job_seq);

    /* Monitoring endpoint */
    --put_log_monitor(v_job_id, 'Main Process', v_job_seq, 'VM date=' || p_submission_date, 0);

    /* Load Vending Machine's configurations */
    select ou_code, subinv_code, part_codes, price_list_code, vat_code, tran_type_code, sales_person
    into v_ou_code,
         v_subinv_code,
         v_part_codes,
         v_pricelist_code,
         v_vat_code,
         v_transaction_type,
         v_sales_person
    from vending_machine_configs
    where vending_code = p_vending_code;

    /* Default parameters */
    v_program_id    := 'VMAC_SRV';
    v_iv_trans_type := pb_parameter_setup_pkg.Get_parameter_value('DTSART15', 'SALES');
    v_payment_code  := p_payment_code;
    v_channel       := 'SHP';
    v_stype         := '001';
    v_doc_date      := SYSDATE;
    v_user          := 'POS';

    /* Get Vat rate */
    SELECT sa_vat_pkg.Get_vat_rate(v_vat_code) INTO v_vat_rate FROM dual;

    /* Verify LANG environment */
    IF Userenv('LANG') <> 'US' THEN
      Raise_application_error(-20997, 'Configuration Error: Default language used should be US. See variable Userenv(''LANG'')');
    END IF;

    /* Verify sales person */
    SELECT Count(*) INTO v_count
    FROM su_user u, pb_user_subinven usub, sa_salesperson us
    WHERE u.user_id = v_sales_person AND u.user_id = usub.user_id AND usub.user_id = us.person_code;
    IF v_count <= 0 THEN
      Raise_application_error(-20998, 'Configuration Error in table VENDING_MACHINE_CONFIGS: Sales person is invalid or is not existing');
    END IF;

    /* Verify vat code */
    SELECT Count(*), Min(vat_rate) INTO v_count, v_vate_rate 
    FROM sa_vat WHERE vat_code = v_vat_code;
    IF v_count <= 0 THEN
      Raise_application_error(-20999, 'Configuration Error in table VENDING_MACHINE_CONFIGS: Vat code is invalid or is not existing');
    END IF;

    /* Verify payment code */
    SELECT Count(*) INTO v_count FROM sa_payment WHERE payment_code = v_payment_code;
    IF v_count <= 0 THEN
      Raise_application_error(-20881, 'Configuration Error in table VENDING_MACHINE_CONFIGS: Payment code is invalid or is not existing');
    END IF;

    /* A. Validate SIMs, and also insert each serial into temporary table */
    FOR rec_s IN rec_sn_sources(p_comma_separated_serials) LOOP
      /* Check if this SN is existing */
      v_found_info := 'N';
      v_latest_sn_before_error := rec_s.serial;

      /* Validate SN status
       * And, it automatically exits at first loop */
      OPEN current_serial_status(rec_s.serial);
      FETCH current_serial_status INTO v_current_serial_status;
      CLOSE current_serial_status;
      
      /* Find subinv's status */
      SELECT Nvl(Min(subinv_typ_code), 'I') INTO v_subinv_type_code
      FROM pb_subinv
      WHERE ou_code = v_ou_code
        AND subinv_code = v_current_serial_status.current_subinventory_code;
      
      /* Find Item-Code */
      select min(item_code) into v_item_code
      from in_item 
      where oa_item = v_current_serial_status.inventory_item_id
          and ou_code = v_ou_code;

      /* Check each SN at any status */
	    v_latest_refdoc_before_error := v_current_serial_status.LAST_TXN_SOURCE_NAME;
      IF v_current_serial_status.current_status = 3 THEN
        IF v_subinv_code = v_current_serial_status.current_subinventory_code THEN
          v_found_info := 'Y';

        ELSE
          v_errmsg := 'SIM ' || v_latest_sn_before_error || ' is not in shop, sub inventory=' || v_subinv_code;
          save_sn_to_temp_table(v_guid, p_vending_code, v_item_code, rec_s.serial, v_current_serial_status.locator_code, v_unit_price, 'VALIDATED FAIL', v_errmsg, v_job_id, v_job_seq);
          
          Raise_application_error(-20995, v_errmsg);
        END IF;
      
      ELSIF v_current_serial_status.current_status = 4 THEN
        v_errmsg := 'SIM ' || v_latest_sn_before_error || ' is cutting-off from stock with doc_no ' || v_latest_refdoc_before_error;
        save_sn_to_temp_table(v_guid, p_vending_code, v_item_code, rec_s.serial, v_current_serial_status.locator_code, v_unit_price, 'VALIDATED FAIL', v_errmsg, v_job_id, v_job_seq);
        
        Raise_application_error(-20993, v_errmsg);
      ELSIF v_current_serial_status.current_status = 5 THEN
        v_errmsg := 'SIM ' || v_latest_sn_before_error || ' is not be received on POS. please receive the SIM into system first';
        save_sn_to_temp_table(v_guid, p_vending_code, v_item_code, rec_s.serial, v_current_serial_status.locator_code, v_unit_price, 'VALIDATED FAIL', v_errmsg, v_job_id, v_job_seq);
        
        Raise_application_error(-20994, v_errmsg);
      END IF;
      
      IF v_found_info = 'N' THEN
        v_errmsg := 'SIM ' || v_latest_sn_before_error || ' has not been found in POS. Please check and update SIM on POS';
        save_sn_to_temp_table(v_guid, p_vending_code, v_item_code, rec_s.serial, v_current_serial_status.locator_code, v_unit_price, 'VALIDATED FAIL', v_errmsg, v_job_id, v_job_seq);
			  Raise_application_error(-20992, v_errmsg);
		  END IF;
      /* END - Validate SN status */

      /* Find & Validate Std-Price */
      SELECT Count(*), Min(std_price) INTO v_count, v_unit_price
		  FROM sa_price_item
		  WHERE item_code = v_item_code 
        AND ou_code = v_ou_code 
        AND pricelist_code = v_pricelist_code;

      IF v_count <= 0 THEN
        v_errmsg := 'Invalid price-list code for Item-Code=' ||v_item_code|| ', S/N=' || v_latest_sn_before_error;
        save_sn_to_temp_table(v_guid, p_vending_code, v_item_code, rec_s.serial, v_current_serial_status.locator_code, v_unit_price, 'VALIDATED FAIL', v_errmsg, v_job_id, v_job_seq);
        Raise_application_error(-20990, v_errmsg);
      END IF;
      /* END - Find & Validate Std-Price */

      /* Check stock Qty is available */
      v_count := trn_chk_stock.Get_available_qty(v_ou_code, v_subinv_code, v_current_serial_status.locator_code, v_item_code, rec_s.serial, '%', 'AVL');
      IF v_count <= 0 THEN
        v_errmsg := 'Stock of ' || v_latest_sn_before_error || ' is running out';
        save_sn_to_temp_table(v_guid, p_vending_code, v_item_code, rec_s.serial, v_current_serial_status.locator_code, v_unit_price, 'VALIDATED FAIL', v_errmsg, v_job_id, v_job_seq);
        Raise_application_error(-20996, v_errmsg);
      END IF;
      
      /* Save a validated one into temp table in order to be calculating after */
      save_sn_to_temp_table(v_guid, p_vending_code, v_item_code, rec_s.serial, v_current_serial_status.locator_code, v_unit_price, 'INIT', '', v_job_id, v_job_seq);

    END LOOP;
    /* END - Validate SIMs */
    
    /* Mark all temporary S/N as starting process */
    mark_all_as(v_guid, 'PROCESSING');
    
    --put_log_monitor(v_job_id, 'Start gen, subinv= ' || v_subinv_code, v_job_seq, 'S/N & data validated', 0);

    /* Generate Doc No */
		v_doc_no  := pb_running_pkg.Get_running(v_ou_code, pb_parameter_setup_pkg.Get_parameter_value('DTSART14', 'IV_PREFIX'),
                                            v_subinv_code, sysdate, v_user, v_program_id);

    --put_log_monitor(v_job_id, 'Doc/No=' || v_doc_no, v_job_seq, 'Start insert "SA_TRANSACTION"', 0);
    
    /* A. Insert calculated results into SA_TRANSACTION. */
    OPEN calc_a_transaction(v_guid, v_vat_rate);
    FETCH calc_a_transaction INTO v_calc_transaction;
    CLOSE calc_a_transaction;
    
    INSERT INTO sa_transaction
			(ou_code,
			subinv_code,
			doc_no,
			doc_type,
			doc_date,
			pricelist_code,
			cust_code,
			stype_code,
			channel_code,
			status,
			total_amt,
			disc_baht,
			salesperson,
			vat_amt,
			vat_rate,
			net_amt,
			upd_by,
			vat_code,
			upd_date,
			upd_pgm,
			pos_no,
			cashier_code,
			hire_purchase,
			reprint,
			hp_periods,
			cn_flag,
			cashier_point,
			team_header,
			area_mgr,
			shop_mgr,
			serial_flg,
			cust_add,
			cust_amphur,
			cust_province,
			cust_postal,
			cust_tel,
			cust_name,
			disc_perc,
			branch_code,
			tax_id,
			gross_amt,
			remark)
		VALUES
			(v_ou_code,
			v_subinv_code,
			v_doc_no,
			v_reciept_doc_type,
			SYSDATE,
			v_pricelist_code,
			NULL,
			v_stype,
			v_channel,
			'N',
			v_calc_transaction.total_after_vat,
			0,
			v_sales_person,
			v_calc_transaction.total_vat,
			v_vat_rate,
			v_calc_transaction.total_before_vat,
			v_sales_person,
			v_vat_code,
			SYSDATE,
			v_program_id,
			NULL,
			NULL,
			'N',
			0,
			1,
			'N',
			NULL,
			NULL,
			NULL,
			NULL,
			'Y',
			p_cus_address,
			NULL,
			NULL,
			NULL,
			NULL,
			p_cus_fname || ' ' || p_cus_lname,
			0,
			v_branch_code,
			p_cus_passport,
			v_calc_transaction.total_before_vat,
			'');-- Leave remark blank, Auto Gen by Auto Invoice(VENDING MACHINE)
    
    --put_log_monitor(v_job_id, 'Doc/No=' || v_doc_no, v_job_seq, 'Insert "SA_TRANSACTION" done', 1);
    
    /* Update IN_TRAN */
    --put_log_monitor(v_job_id, 'Doc/No=' || v_doc_no, v_job_seq, 'Start "pti_transfer.Pos_to_inv"', 0);
      
		pti_transfer.Pos_to_inv('A',
                            v_ou_code,
                            v_subinv_code,
                            v_reciept_doc_type,
                            v_doc_no,
                            v_iv_trans_type,
                            v_user,
                            v_program_id);
    --put_log_monitor(v_job_id, 'Doc/No=' || v_doc_no, v_job_seq, 'Update "pti_transfer.Pos_to_inv" done', 1);
    /* END - Update IN_TRAN */ 
        
    /* B. Insert calculated results into SA_TRANS_DTL. */
    v_line_seq := 0;
    FOR rec_g in calc_transaction_details(v_guid, v_vat_rate) LOOP
      
      v_line_seq := v_line_seq + 1;
      --put_log_monitor(v_job_id, v_line_seq || ', item_code=' || rec_g.item_code, v_job_seq, 'Start insert "SA_TRANS_DTL"', v_line_seq); COMMIT;
      
      /* Start insert sa_trans_dtl */
      INSERT INTO sa_trans_dtl
				(ou_code, subinv_code, doc_no, doc_type, seq_no, stype_code, barcode, item_code, locator_code, price_list_code, qty, unitprice, amount, std_price, upd_by, upd_date, upd_pgm, ucost, gross_amt, disc_amt, disc_tot)
			VALUES
				(v_ou_code,
				v_subinv_code,
				v_doc_no,
				v_reciept_doc_type,
				v_line_seq,
				'001',
				rec_g.item_code,
				rec_g.item_code,
				rec_g.locator_code,
				v_pricelist_code,
				rec_G.Qty,
				rec_g.std_price,
				rec_g.total_before_vat,
				rec_g.std_price,
				v_sales_person,
				SYSDATE,
				v_program_id,
				sa_item_pkg.Get_unit_cost(v_ou_code,
										   v_subinv_code,
										   rec_g.locator_code,
										   rec_g.item_code),
				rec_g.total_before_vat,
				0,
				0);
      
      --put_log_monitor(v_job_id, ', item_code=' || rec_g.item_code, v_job_seq, 'Insert "SA_TRANS_DTL" done', v_line_seq); COMMIT;
      
      /* Insert detail into IN_TRAN */
      --put_log_monitor(v_job_id, ', item_code=' || rec_g.item_code, v_job_seq, 'Start "pti_transfer.Pos_to_inv"', 0); COMMIT;
      
      pti_transfer.Pos_to_inv('A', v_ou_code, v_subinv_code, v_reciept_doc_type, v_doc_no, v_iv_trans_type, v_sales_person, v_program_id, rec_g.item_code, rec_g.locator_code, rec_g.item_code, null);
      
      --put_log_monitor(v_job_id, ', item_code=' || rec_g.item_code, v_job_seq, 'Update "pti_transfer.Pos_to_inv" done', 0); COMMIT;
                          
      /* C. Insert all SN into SA_TRANS_SN_LOT. */
      v_sn_seq := 0;
      FOR rec_s in c_serials_by_item_code(v_guid, rec_g.item_code) LOOP
         
        v_sn_seq := v_sn_seq + 1;
        --put_log_monitor(v_job_id, ', S/N=' || rec_s.serial, v_job_seq, 'Start insert "SA_TRANS_SN_LOT"', v_sn_seq); COMMIT;
        
        /* Insert sa_trans_sn_lot with calculated data */
        INSERT INTO sa_trans_sn_lot
          (ou_code, subinv_code, doc_no, doc_type, seq_no, item_code, seq_sn_lot, locator_code, serial, qty, upd_by, upd_date, upd_pgm)
        VALUES
          (v_ou_code, v_subinv_code, v_doc_no, v_reciept_doc_type, v_line_seq, rec_s.item_code, v_sn_seq, rec_s.locator_code, rec_s.serial, 1, v_sales_person, SYSDATE, v_program_id);

        --put_log_monitor(v_job_id, ', S/N=' || rec_s.serial, v_job_seq, 'Insert "SA_TRANS_SN_LOT" done', v_sn_seq); COMMIT;
        
        /* INSERT SERIAL TO IN_TRAN */
        --put_log_monitor(v_job_id, ', S/N=' || rec_s.serial, v_job_seq, 'Start "pti_transfer.Pti_edit_sn_lot"', v_sn_seq); COMMIT;
        pti_transfer.Pti_edit_sn_lot(v_ou_code,
                                   v_subinv_code,
                                   v_reciept_doc_type,
                                   v_doc_no,
                                   v_iv_trans_type,
                                   rec_s.item_code,
                                   rec_s.locator_code,
                                   v_sales_person,
                                   v_program_id,
                                   v_sn_seq);
        --put_log_monitor(v_job_id, ', S/N=' || rec_s.serial, v_job_seq, 'Update "pti_transfer.Pti_edit_sn_lot" done', 0); COMMIT;
        
      END LOOP;
      /* END - C. */
    END LOOP;
    
    /* D. Insert a row of payment, SA_TRANS_PAY. */
    --put_log_monitor(v_job_id, 'Doc/No=' || v_doc_no, v_job_seq, 'Start insert "SA_TRANS_PAY"', 1);
		
    INSERT INTO sa_trans_pay
			(ou_code, subinv_code, doc_no, doc_type, payment_code, pay_amt, bank_code, upd_by, upd_date, upd_pgm, pay_net, ref_no, credit_card)
		VALUES
			(v_ou_code, v_subinv_code, v_doc_no, v_reciept_doc_type, v_payment_code, v_calc_transaction.total_before_vat, NULL, v_user, SYSDATE, v_program_id, v_calc_transaction.total_before_vat, NULL, NULL);
    
    --put_log_monitor(v_job_id, 'Doc/No=' || v_doc_no, v_job_seq, 'Insert "SA_TRANS_PAY" done', 1);COMMIT;
    /* END - D. */
    
    /* Mark all temporary S/N as completed */
    mark_all_as(v_guid, 'COMPLETED');


    /* Assign OUT parameters */
    p_receipt_no    := v_doc_no;
    p_document_date := to_char(v_doc_date, 'DD/MM/YYYY HH24:MI:SS');
    p_job_no        := to_char(v_job_id);
    p_job_seq       := to_char(v_job_seq);

    --put_log_monitor(v_job_id, 'Doc/No=' || p_receipt_no, v_job_seq, 'INV COMPLETED', 1); 
    COMMIT;
    
    p_error_code := 0;
    p_error_mesg := '';

    EXCEPTION
      WHEN OTHERS THEN
        p_error_code := sqlcode;
        p_error_mesg := sqlerrm;
	      /* Re-assign monitoring id */
		    p_job_no        := to_char(v_job_id);
		    p_job_seq       := to_char(v_job_seq);
        rollback;
        

  END process_reciept;

END PKG_VENDINGM_SALES;
/
