create or replace PROCEDURE RCV_SUB_LOC(V_OU_CODE   IN VARCHAR2,
			                            V_UPD_BY    IN VARCHAR2,
			                            V_UPD_DATE  IN DATE,
			                            V_UPD_PGM   IN VARCHAR2,
			                            V_FULL_LOAD IN VARCHAR2,
										N_NO        IN NUMBER,
										N_ID        IN NUMBER) IS

  CURSOR sub  IS SELECT   S.*
                 FROM     PB_SUBINV_INFO_V S
                 WHERE    s.master_organization_id = (SELECT OA_ORG FROM PB_COMPANY WHERE OU_CODE = V_OU_CODE) 
                 ORDER BY S.SUBINV_CODE;
                 
                 

                 
  CURSOR loc  IS SELECT   L.*
                 FROM     PB_LOCATOR_INFO_V L
				 WHERE    L.SUBINVENTORY_CODE IN 
                  (SELECT V.SUBINV_CODE FROM PB_SUBINV_INFO_V V
                   WHERE    V.master_organization_id = (SELECT OA_ORG FROM PB_COMPANY WHERE OU_CODE = V_OU_CODE)
                   )
				 AND      L.DESCRIPTION IS NOT NULL
				 ORDER BY L.OA_LOCATOR_CODE;

  CURSOR sub_upd(V_DATE DATE)  IS SELECT   S.*
                                  FROM     PB_SUBINV_INFO_V S
					              WHERE    (S.CRE_DATE >= V_DATE OR S.UPD_DATE >= V_DATE)
                        AND      S.master_organization_id = (SELECT OA_ORG FROM PB_COMPANY WHERE OU_CODE = V_OU_CODE)
                                  ORDER BY S.SUBINV_CODE;
                                  
  CURSOR loc_upd(V_DATE DATE)  IS SELECT   L.*
                                  FROM     PB_LOCATOR_INFO_V L
								  WHERE    L.SUBINVENTORY_CODE IN 
                          (SELECT V.SUBINV_CODE FROM PB_SUBINV_INFO_V V
                           WHERE    V.master_organization_id = (SELECT OA_ORG FROM PB_COMPANY WHERE OU_CODE = V_OU_CODE)
                           )
				          AND      L.DESCRIPTION IS NOT NULL
								  AND     (L.CRE_DATE >= V_DATE OR L.UPD_DATE >= V_DATE)
								  ORDER BY L.OA_LOCATOR_CODE;
                  
                  


  v_ck        VARCHAR2(1);
  v_reg       PB_SUBINV.REGION_CODE%TYPE;
  v_prv       PB_SUBINV.PROVINCE_CODE%TYPE;
  v_amp       PB_SUBINV.AMPHUR_CODE%TYPE;
  v_post      PB_SUBINV.POSTAL_CODE%TYPE;
  v_def       PB_LOCATOR.USE_DEFAULT%TYPE;
  TEMP_STATUS PB_INTERFACE.STATUS%TYPE    := 'Completed';
  ERR_CODE    NUMBER;
  ERR_MESG    VARCHAR2(200);
  SUC_CNT     NUMBER;
  ERR_CNT     NUMBER;
  M_SEQ       NUMBER;
  E_SEQ       NUMBER;
  D_LAST      DATE;
  CK_UPD      VARCHAR2(1);
  V_TEMP      VARCHAR2(1);
  V_TAX_ID    PB_COMPANY.ORG_TAX_ID%TYPE;
  V_DEFAULT_LOCATOR PB_SUBINV.DEFAULT_LOC%TYPE;
  
BEGIN

   -- CHECK WORK CONDITION
   --V_WORK := 'N';
   --V_WORK := CHECK_JOB_WORK(N_NO);

   --IF NVL(V_WORK,'N') = 'Y' THEN

	   SUC_CNT := 0;
	   ERR_CNT := 0;

       -- WRITE START PROCESS
	   M_SEQ := GET_MONITOR_SEQ(N_ID);
	   M_SEQ := M_SEQ+0.01;
	   BEGIN
	     INSERT INTO JOBS_MONITOR(JOB_ID, JOB_SEQ, JOB_NAME, JOB_MESG, JOB_TIME, SUC_CNT, ERR_CNT, UPD_BY, UPD_DATE, UPD_PGM, STATUS)
	     VALUES (N_ID, M_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', 'Start process receive Subinventory and Locator from ERP.', SYSDATE, NULL, NULL, V_UPD_BY, SYSDATE, V_UPD_PGM,'Start Process');
	   EXCEPTION WHEN OTHERS THEN NULL;
	   END;

	  -- MAIN PROCESS
    select Min(ORG_TAX_ID) into V_TAX_ID from PB_COMPANY where ou_code=V_OU_CODE;

	  E_SEQ    := GET_LOG_SEQ(N_ID);

	  IF NVL(V_FULL_LOAD,'N') = 'Y' THEN -- INTERFACE ALL DATA

		  -- WRITE START SUBINVENTORY PROCESS
    	  M_SEQ := M_SEQ+0.01;
		  BEGIN
			INSERT INTO JOBS_MONITOR(JOB_ID, JOB_SEQ, JOB_NAME, JOB_MESG, JOB_TIME, SUC_CNT, ERR_CNT, UPD_BY, UPD_DATE, UPD_PGM, STATUS)
			VALUES (N_ID, M_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', 'Processing table PB_SUBINV', SYSDATE, SUC_CNT, ERR_CNT, V_UPD_BY, SYSDATE, V_UPD_PGM,'Start Process');
		  EXCEPTION WHEN OTHERS THEN NULL;
		  END;

		  FOR i IN sub LOOP

		  TEMP_STATUS := 'OK';
              
          --dbms_output.put_line('loop sub :' || i.SUBINV_CODE);
          
		      -- ***** Get Mapping Value ***** --
			     -- Get Province
				 BEGIN
				   SELECT REGION_CODE, PROVINCE_CODE
				   INTO   v_reg, v_prv
				   FROM   PB_PROVINCE P
				   WHERE  P.PROVINCE_NAME     = DECODE(i.PROVINCE,'กรุงเทพมหานคร','กรุงเทพ',i.PROVINCE)
				   AND    ROWNUM = 1;
					 -- Get Amphur
					 BEGIN
		               SELECT AMPHUR_CODE
					   INTO   v_amp
					   FROM   PB_AMPHUR AM
					   WHERE  i.AMPHUR LIKE '%'||AM.AMPHUR_NAME(+)||'%'
					   AND    AM.PROVINCE_CODE = v_prv
					   AND    ROWNUM = 1;
					     -- Get Postal
						 BEGIN
						   SELECT PT.POSTAL_CODE
						   INTO   v_post
						   FROM   PB_POSTAL PT
						   WHERE  PT.POSTAL_CODE = i.POSTAL_CODE
						   AND    PT.AMPHUR_CODE = v_amp
						   AND    ROWNUM = 1;
						 EXCEPTION WHEN NO_DATA_FOUND THEN
						   v_reg  := NULL;
						   v_prv  := NULL;
						   v_amp  := NULL;
						   v_post := NULL;
						 END;
					 EXCEPTION WHEN NO_DATA_FOUND THEN
					   v_reg  := NULL;
					   v_prv  := NULL;
					   v_amp  := NULL;
					   v_post := NULL;
					 END;
				 EXCEPTION WHEN NO_DATA_FOUND THEN
				   v_reg  := NULL;
				   v_prv  := NULL;
				   v_amp  := NULL;
				   v_post := NULL;
				 END;

			  -- ***** Main Process ***** --

			  -- *** Start Subinventory Process *** --
			  -- Check Exist Subinventory
			  BEGIN
		        SELECT DISTINCT 'X'
            INTO   v_ck
            FROM   PB_SUBINV
		        WHERE  OU_CODE = V_OU_CODE
            AND    SUBINV_CODE = i.SUBINV_CODE;
				-- Update Subinventory
				BEGIN
          --dbms_output.put_line('loop sub Update sub inven:' || i.SUBINV_CODE);
					UPDATE PB_SUBINV
					SET    SUBINV_DESC         = i.SUBINV_NAME,
			                /*  Cherry :- Modify on 31/05/2550 --> HD0002207 No Synchronize Address on Update */
							  -- SUBINV_ADD          = i.ADDRESS,
			 			      -- AMPHUR_CODE         = v_amp,
			                  -- PROVINCE_CODE       = v_prv,
						      -- POSTAL_CODE	       = v_post,
			                  -- REGION_CODE         = v_reg,
						   SUBINV_TAXID        =      V_TAX_ID,
			               DEAL_CODE           = '01',
			               PRICELIST_CODE      = 'CASH', --'7007'
			               OA_SUBINV_CODE      = i.SUBINV_CODE,
						   SUBINV_TYP_CODE     = DECODE(i.SUBINV_TYPE_CODE,'INTRANSIT','I','SHOP','S','WAREHOUSE','W','SUPPLIER','W','S'),
			               OA_SUBINV_INTRANSIT = i.ORG_SUBINV_INTRANSIT,
						   OA_ORG_CODE         = i.ORG_CODE,
				           OA_ORG_ID		   = i.ORG_ID,
			               OA_OU_ID            = i.OU_ID,
			               CHARGE_ACCOUNT_ID   = i.ORG_AP_ACCRUAL_ACCOUNT,
						   BUDGET_ACCOUNT_ID   = i.ORG_AP_ACCRUAL_ACCOUNT,
			               PREFIX_SUBINV	   = i.SUBINV_CODE,
			               SUBINV_TYPE         = i.SUBINV_TYPE_CODE,
			               OA_LOCATION_CODE    = i.SUBINV_LOCATION_CODE,
						   UPD_DATE            = SYSDATE,
						   UPD_BY              = 'SS-STAFF'
					WHERE  OU_CODE     = V_OU_CODE
					AND    SUBINV_CODE = i.SUBINV_CODE;
					SUC_CNT := SUC_CNT +1 ;
						  -- ***** Clear Monitor and Error Log ****
						    -- Clear Error Log
							BEGIN
							  DELETE FROM JOBS_LOG
							  WHERE  JOB_ID   = n_id
							  AND    ERR_MESG LIKE '%Subinventory%'
							  AND    DATA     =  i.subinv_code;
							EXCEPTION WHEN OTHERS THEN
							  NULL;
							END;
							-- Clear Monitor Log
							BEGIN
							  SELECT DISTINCT 'X'
							  INTO   V_TEMP
							  FROM   JOBS_LOG
							  WHERE  JOB_ID   = n_id
							  AND    ERR_MESG LIKE '%Subinventory%'
							  AND    DATA     <>  i.subinv_code;
							EXCEPTION WHEN NO_DATA_FOUND THEN
							  BEGIN
							    UPDATE JOBS_MONITOR
								SET    SUC_CNT  = SUC_CNT + ERR_CNT,
								       ERR_CNT  = 0
								WHERE  JOB_ID   = n_id
								AND    JOB_NAME = 'RCV ERP SUBINVENTORY AND LOCATOR'
								AND    JOB_MESG = 'Complete Table PB_SUBINV';
							  EXCEPTION WHEN OTHERS THEN NULL;
							  END;
							END;
				EXCEPTION WHEN DUP_VAL_ON_INDEX THEN TEMP_STATUS := 'Error';
				              ERR_CNT := ERR_CNT +1 ;
						      ERR_CODE := SQLCODE;
						      ERR_MESG := SQLERRM;
							  E_SEQ    := E_SEQ+0.01;
						      -- INSERT TO LOG TABLE
						      BEGIN
						        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
						        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', i.subinv_code, 'Duplicate Subinventory!', SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
						      EXCEPTION WHEN OTHERS THEN NULL;
						      END;
           				  WHEN INVALID_NUMBER   THEN TEMP_STATUS := 'Error';
				              ERR_CNT := ERR_CNT +1 ;
						      ERR_CODE := SQLCODE;
						      ERR_MESG := SQLERRM;
							  E_SEQ    := E_SEQ+0.01;
						      -- INSERT TO LOG TABLE
						      BEGIN
						        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
						        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', i.subinv_code, 'Invalid Number of Subinventory!', SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
						      EXCEPTION WHEN OTHERS THEN NULL;
						      END;
						  WHEN VALUE_ERROR      THEN TEMP_STATUS := 'Error';
				              ERR_CNT := ERR_CNT +1 ;
						      ERR_CODE := SQLCODE;
						      ERR_MESG := SQLERRM;
							  E_SEQ    := E_SEQ+0.01;
						      -- INSERT TO LOG TABLE
						      BEGIN
						        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
						        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', i.subinv_code, 'Value Error of Subinventory!', SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
						      EXCEPTION WHEN OTHERS THEN NULL;
						      END;
				          WHEN OTHERS           THEN TEMP_STATUS := 'Error';
				              ERR_CNT := ERR_CNT +1 ;
						      ERR_CODE := SQLCODE;
						      ERR_MESG := SQLERRM;
							  E_SEQ    := E_SEQ+0.01;
						      -- INSERT TO LOG TABLE
						      BEGIN
						        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
						        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', i.subinv_code, SUBSTR(ERR_CODE||'-'||ERR_MESG,1,200), SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
						      EXCEPTION WHEN OTHERS THEN NULL;
						      END;
				END;
			  EXCEPTION WHEN NO_DATA_FOUND THEN
		 		-- Insert Subinventory
          --dbms_output.put_line('loop sub Insert sub inven:' || i.SUBINV_CODE);
			    BEGIN
		          INSERT INTO PB_SUBINV(OU_CODE, SUBINV_CODE, SUBINV_DESC, SUBINV_ADD, AMPHUR_CODE, PROVINCE_CODE, POSTAL_CODE,
									    REGION_CODE, SUBINV_TAXID, DEAL_CODE, PRICELIST_CODE, OA_SUBINV_CODE, SUBINV_TYP_CODE,
									    OA_SUBINV_INTRANSIT, OA_ORG_CODE, OA_ORG_ID, OA_OU_ID, CHARGE_ACCOUNT_ID,
									    BUDGET_ACCOUNT_ID, PREFIX_SUBINV, 
                      OA_ACC_SEGMENT1, 
                      OA_ACC_SEGMENT2, 
                      SUBINV_TYPE,
										OA_LOCATION_CODE, UPD_DATE, UPD_BY )
				  VALUES (V_OU_CODE, i.SUBINV_CODE, i.SUBINV_NAME,i.ADDRESS, v_amp, v_prv, v_post,
				          v_reg, V_TAX_ID, '01', 'CASH',i.SUBINV_CODE, DECODE(i.SUBINV_TYPE_CODE,'INTRANSIT','I','SHOP','S','WAREHOUSE','W','SUPPLIER','W','S'),
		                  i.ORG_SUBINV_INTRANSIT, i.ORG_CODE, i.ORG_ID, i.OU_ID,i.ORG_AP_ACCRUAL_ACCOUNT,
						          i.ORG_AP_ACCRUAL_ACCOUNT, i.SUBINV_CODE, 
                      '000A', 
                      CASE WHEN V_OU_CODE in ('TAC','DTN','TPY') THEN '0511' 
                           WHEN V_OU_CODE in ('PAY') THEN '0846' 
                           ELSE NULL
                      END, 
                      i.SUBINV_TYPE_CODE,
						          i.SUBINV_LOCATION_CODE, SYSDATE, 'SS-STAFF');
                      
              INSERT_FIX_USER_PERMISSION(V_OU_CODE,i.SUBINV_CODE);
						  SUC_CNT := SUC_CNT +1 ;
						  -- ***** Clear Monitor and Error Log ****
						    -- Clear Error Log
							BEGIN
							  DELETE FROM JOBS_LOG
							  WHERE  JOB_ID   = n_id
							  AND    ERR_MESG LIKE '%Subinventory%'
							  AND    DATA     =  i.subinv_code;
							EXCEPTION WHEN OTHERS THEN
							  NULL;
							END;
							-- Clear Monitor Log
							BEGIN
							  SELECT DISTINCT 'X'
							  INTO   V_TEMP
							  FROM   JOBS_LOG
							  WHERE  JOB_ID   = n_id
							  AND    ERR_MESG LIKE '%Subinventory%'
							  AND    DATA     <>  i.subinv_code;
							EXCEPTION WHEN NO_DATA_FOUND THEN
							  BEGIN
							    UPDATE JOBS_MONITOR
								SET    SUC_CNT  = SUC_CNT + ERR_CNT,
								       ERR_CNT  = 0
								WHERE  JOB_ID   = n_id
								AND    JOB_NAME = 'RCV ERP SUBINVENTORY AND LOCATOR'
								AND    JOB_MESG = 'Complete Table PB_SUBINV';
							  EXCEPTION WHEN OTHERS THEN NULL;
							  END;
							END;
				EXCEPTION WHEN DUP_VAL_ON_INDEX THEN TEMP_STATUS := 'Error';
				              ERR_CNT := ERR_CNT +1 ;
						      ERR_CODE := SQLCODE;
						      ERR_MESG := SQLERRM;
							  E_SEQ    := E_SEQ+0.01;
						      -- INSERT TO LOG TABLE
						      BEGIN
						        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
						        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', i.subinv_code, 'Duplicate Subinventory!', SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
						      EXCEPTION WHEN OTHERS THEN NULL;
						      END;
				          WHEN INVALID_NUMBER   THEN TEMP_STATUS := 'Error';
				              ERR_CNT := ERR_CNT +1 ;
						      ERR_CODE := SQLCODE;
						      ERR_MESG := SQLERRM;
							  E_SEQ    := E_SEQ+0.01;
						      -- INSERT TO LOG TABLE
						      BEGIN
						        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
						        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', i.subinv_code, 'Invalid Number of Subinventory!', SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
						      EXCEPTION WHEN OTHERS THEN NULL;
						      END;
						  WHEN VALUE_ERROR      THEN TEMP_STATUS := 'Error';
				              ERR_CNT := ERR_CNT +1 ;
						      ERR_CODE := SQLCODE;
						      ERR_MESG := SQLERRM;
							  E_SEQ    := E_SEQ+0.01;
						      -- INSERT TO LOG TABLE
						      BEGIN
						        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
						        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', i.subinv_code, 'Value Error of Subinventory!', SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
						      EXCEPTION WHEN OTHERS THEN NULL;
						      END;
						  WHEN OTHERS           THEN TEMP_STATUS := 'Error';
				              ERR_CNT := ERR_CNT +1 ;
						      ERR_CODE := SQLCODE;
						      ERR_MESG := SQLERRM;
							  E_SEQ    := E_SEQ+0.01;
						      -- INSERT TO LOG TABLE
						      BEGIN
						        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
						        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', i.subinv_code, SUBSTR(ERR_CODE||'-'||ERR_MESG,1,200), SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
						      EXCEPTION WHEN OTHERS THEN NULL;
						      END;
				END;
			  END;

		  END LOOP;

		  -- UPDATE INTERFACE DATE
		  BEGIN
		    UPDATE PB_INTERFACE
		    SET    LAST_TF  = SYSDATE,
		           UPD_BY   = V_UPD_BY,
		           UPD_DATE = SYSDATE,
		           UPD_PGM  = V_UPD_PGM
		    WHERE  TNAME    = 'PB_SUBINV'
		    AND    OU_CODE  = V_OU_CODE;
		  EXCEPTION WHEN OTHERS THEN NULL;
		  END;


		  -- WRITE END PROCESS
		  M_SEQ := M_SEQ+0.01;

		  BEGIN
		    INSERT INTO JOBS_MONITOR(JOB_ID, JOB_SEQ, JOB_NAME, JOB_MESG, JOB_TIME, SUC_CNT, ERR_CNT, UPD_BY, UPD_DATE, UPD_PGM, STATUS)
		    VALUES (N_ID, M_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', 'Complete Table PB_SUBINV', SYSDATE, SUC_CNT, ERR_CNT, V_UPD_BY, SYSDATE, V_UPD_PGM,'Completed');
		  EXCEPTION WHEN OTHERS THEN NULL;
		  END;

			  -- *** End Subinventory Process *** --


			  -- *** Start Locator Process *** --

			  SUC_CNT     := 0;
			  ERR_CNT     := 0;

			  -- WRITE START SUBINVENTORY PROCESS
	    	  M_SEQ := M_SEQ+0.01;
			  BEGIN
				INSERT INTO JOBS_MONITOR(JOB_ID, JOB_SEQ, JOB_NAME, JOB_MESG, JOB_TIME, SUC_CNT, ERR_CNT, UPD_BY, UPD_DATE, UPD_PGM, STATUS)
				VALUES (N_ID, M_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', 'Processing table PB_LOCATOR', SYSDATE, SUC_CNT, ERR_CNT, V_UPD_BY, SYSDATE, V_UPD_PGM,'Start Process');
			  EXCEPTION WHEN OTHERS THEN NULL;
			  END;

			  -- Check Locator of Subinventory
			  FOR j IN loc LOOP

                  TEMP_STATUS := 'OK';

		            -- Check Default Locator
					v_def  := 'N';
					IF j.DESCRIPTION = 'Good' THEN
					   BEGIN
					     SELECT DISTINCT 'X'
		                 INTO   v_ck
						 FROM   PB_LOCATOR
						 WHERE  OU_CODE              = V_OU_CODE
						 AND    SUBINV_CODE          = j.SUBINVENTORY_CODE
						 AND    NVL(USE_DEFAULT,'N') = 'Y';
						 v_def  := 'N';
					   EXCEPTION WHEN NO_DATA_FOUND THEN
					     v_def  := 'Y';
					   END;
					END IF;

		            -- Check Default Locator
		            BEGIN
					  SELECT DISTINCT 'X'
					  INTO   v_ck
					  FROM   PB_LOCATOR
					  WHERE  OU_CODE      = V_OU_CODE
					  AND    SUBINV_CODE  = j.SUBINVENTORY_CODE
					  AND    LOCATOR_CODE = j.OA_LOCATOR_CODE;
					  -- Update Locator
					  BEGIN
					    UPDATE PB_LOCATOR
						SET    LOC_DESC        = j.DESCRIPTION,
						       -- USE_DEFAULT     = v_def,
						       --OA_LOCATOR      = j.OA_LOCATOR_ID,  -- Yodchai L. 24-Sep-14
					 	       -- LOCATOR_TYPE    = DECODE(j.DESCRIPTION,'Good','A','Defect','D','Claim','C','A'),
						       OA_LOCATOR_CODE = j.OA_LOCATOR_CODE,
							   UPD_BY          = 'SS-STAFF',
							   UPD_DATE        = SYSDATE
						WHERE  OU_CODE      = V_OU_CODE
					    AND    SUBINV_CODE  = j.SUBINVENTORY_CODE
					    AND    LOCATOR_CODE = j.OA_LOCATOR_CODE;
						SUC_CNT := SUC_CNT +1 ;
						  -- ***** Clear Monitor and Error Log ****
						    -- Clear Error Log
							BEGIN
							  DELETE FROM JOBS_LOG
							  WHERE  JOB_ID   = n_id
							  AND    ERR_MESG LIKE '%Locator%'
							  AND    DATA     =  j.SUBINVENTORY_CODE||'|'||j.OA_LOCATOR_CODE;
							EXCEPTION WHEN OTHERS THEN
							  NULL;
							END;
							-- Clear Monitor Log
							BEGIN
							  SELECT DISTINCT 'X'
							  INTO   V_TEMP
							  FROM   JOBS_LOG
							  WHERE  JOB_ID   = n_id
							  AND    ERR_MESG LIKE '%Locator%'
							  AND    DATA     <>  j.SUBINVENTORY_CODE||'|'||j.OA_LOCATOR_CODE;
							EXCEPTION WHEN NO_DATA_FOUND THEN
							  BEGIN
							    UPDATE JOBS_MONITOR
								SET    SUC_CNT  = SUC_CNT + ERR_CNT,
								       ERR_CNT  = 0
								WHERE  JOB_ID   = n_id
								AND    JOB_NAME = 'RCV ERP SUBINVENTORY AND LOCATOR'
								AND    JOB_MESG = 'Complete Table PB_LOCATOR';
							  EXCEPTION WHEN OTHERS THEN NULL;
							  END;
							END;
					  EXCEPTION WHEN DUP_VAL_ON_INDEX THEN TEMP_STATUS := 'Error';
							      ERR_CNT  := ERR_CNT +1;
								  ERR_CODE := SQLCODE;
							      ERR_MESG := SQLERRM;
								  E_SEQ    := E_SEQ+0.01;
							      -- INSERT TO LOG TABLE
							      BEGIN
							        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
							        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', j.SUBINVENTORY_CODE||'|'||j.OA_LOCATOR_CODE, 'Duplicate Locator!', SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
							      EXCEPTION WHEN OTHERS THEN NULL;
							      END;
					            WHEN INVALID_NUMBER   THEN TEMP_STATUS := 'Error';
							      ERR_CNT  := ERR_CNT +1;
							      ERR_CODE := SQLCODE;
							      ERR_MESG := SQLERRM;
								  E_SEQ    := E_SEQ+0.01;
							      -- INSERT TO LOG TABLE
							      BEGIN
							        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
							        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', j.SUBINVENTORY_CODE||'|'||j.OA_LOCATOR_CODE, 'Invalid Number of Locator!', SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
							      EXCEPTION WHEN OTHERS THEN NULL;
							      END;
								WHEN VALUE_ERROR      THEN TEMP_STATUS := 'Error';
							      ERR_CNT  := ERR_CNT +1;
							      ERR_CODE := SQLCODE;
							      ERR_MESG := SQLERRM;
								  E_SEQ    := E_SEQ+0.01;
							      -- INSERT TO LOG TABLE
							      BEGIN
							        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
							        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', j.SUBINVENTORY_CODE||'|'||j.OA_LOCATOR_CODE, 'Value Error of  Locator!', SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
							      EXCEPTION WHEN OTHERS THEN NULL;
							      END;
								WHEN OTHERS           THEN TEMP_STATUS := 'Error';
							      ERR_CNT  := ERR_CNT +1;
							      ERR_CODE := SQLCODE;
							      ERR_MESG := SQLERRM;
								  E_SEQ    := E_SEQ+0.01;
							      -- INSERT TO LOG TABLE
							      BEGIN
							        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
							        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', j.SUBINVENTORY_CODE||'|'||j.OA_LOCATOR_CODE, SUBSTR(ERR_CODE||'-'||ERR_MESG,1,200), SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
							      EXCEPTION WHEN OTHERS THEN NULL;
							      END;
					  END;
					EXCEPTION WHEN NO_DATA_FOUND THEN
					  -- Insert Locator
					  BEGIN
					    INSERT INTO PB_LOCATOR(OU_CODE, SUBINV_CODE, LOCATOR_CODE, LOC_DESC, USE_DEFAULT, OA_LOCATOR, UPD_BY, UPD_DATE, LOCATOR_TYPE, OA_LOCATOR_CODE)
						VALUES (V_OU_CODE, j.SUBINVENTORY_CODE, j.OA_LOCATOR_CODE, j.DESCRIPTION, v_def, j.OA_LOCATOR_ID, 'SS-STAFF', SYSDATE, DECODE(j.DESCRIPTION,'Good','A','Defect','D','Claim','C','A'), j.OA_LOCATOR_CODE);
						SUC_CNT := SUC_CNT +1 ;
						  -- ***** Clear Monitor and Error Log ****
						    -- Clear Error Log
							BEGIN
							  DELETE FROM JOBS_LOG
							  WHERE  JOB_ID   = n_id
							  AND    ERR_MESG LIKE '%Locator%'
							  AND    DATA     =  j.SUBINVENTORY_CODE||'|'||j.OA_LOCATOR_CODE;
							EXCEPTION WHEN OTHERS THEN
							  NULL;
							END;
							-- Clear Monitor Log
							BEGIN
							  SELECT DISTINCT 'X'
							  INTO   V_TEMP
							  FROM   JOBS_LOG
							  WHERE  JOB_ID   = n_id
							  AND    ERR_MESG LIKE '%Locator%'
							  AND    DATA     <>  j.SUBINVENTORY_CODE||'|'||j.OA_LOCATOR_CODE;
							EXCEPTION WHEN NO_DATA_FOUND THEN
							  BEGIN
							    UPDATE JOBS_MONITOR
								SET    SUC_CNT  = SUC_CNT + ERR_CNT,
								       ERR_CNT  = 0
								WHERE  JOB_ID   = n_id
								AND    JOB_NAME = 'RCV ERP SUBINVENTORY AND LOCATOR'
								AND    JOB_MESG = 'Complete Table PB_LOCATOR';
							  EXCEPTION WHEN OTHERS THEN NULL;
							  END;
							END;
					  EXCEPTION WHEN DUP_VAL_ON_INDEX THEN TEMP_STATUS := 'Error';
							      ERR_CNT  := ERR_CNT +1;
							      ERR_CODE := SQLCODE;
							      ERR_MESG := SQLERRM;
								  E_SEQ    := E_SEQ+0.01;
							      -- INSERT TO LOG TABLE
							      BEGIN
							        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
							        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', j.SUBINVENTORY_CODE||'|'||j.OA_LOCATOR_CODE, 'Duplicate Locator!', SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
							      EXCEPTION WHEN OTHERS THEN NULL;
							      END;
					            WHEN INVALID_NUMBER   THEN TEMP_STATUS := 'Error';
							      ERR_CNT  := ERR_CNT +1;
							      ERR_CODE := SQLCODE;
							      ERR_MESG := SQLERRM;
								  E_SEQ    := E_SEQ+0.01;
							      -- INSERT TO LOG TABLE
							      BEGIN
							        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
							        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', j.SUBINVENTORY_CODE||'|'||j.OA_LOCATOR_CODE, 'Invalid Number of Locator!', SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
							      EXCEPTION WHEN OTHERS THEN NULL;
							      END;
								WHEN VALUE_ERROR      THEN TEMP_STATUS := 'Error';
							      ERR_CNT  := ERR_CNT +1;
							      ERR_CODE := SQLCODE;
							      ERR_MESG := SQLERRM;
								  E_SEQ    := E_SEQ+0.01;
							      -- INSERT TO LOG TABLE
							      BEGIN
							        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
							        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', j.SUBINVENTORY_CODE||'|'||j.OA_LOCATOR_CODE, 'Value Error of  Locator!', SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
							      EXCEPTION WHEN OTHERS THEN NULL;
							      END;
								WHEN OTHERS           THEN TEMP_STATUS := 'Error';
							      ERR_CNT  := ERR_CNT +1;
							      ERR_CODE := SQLCODE;
							      ERR_MESG := SQLERRM;
								  E_SEQ    := E_SEQ+0.01;
							      -- INSERT TO LOG TABLE
							      BEGIN
							        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
							        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', j.SUBINVENTORY_CODE||'|'||j.OA_LOCATOR_CODE, SUBSTR(ERR_CODE||'-'||ERR_MESG,1,200), SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
							      EXCEPTION WHEN OTHERS THEN NULL;
							      END;
					  END;
					END;

			  END LOOP;
        
        --To Update Default Locator 27-Apr-2016
        FOR i in sub Loop
          --dbms_output.put_line('YOD-->OU:' || V_OU_CODE || ' SUBINV:' || i.SUBINV_CODE);
          SELECT DEFAULT_LOC INTO V_DEFAULT_LOCATOR 
          FROM PB_SUBINV WHERE OU_CODE = V_OU_CODE
					     AND    SUBINV_CODE = i.SUBINV_CODE;
          
          IF V_DEFAULT_LOCATOR is NULL THEN
             --dbms_output.put_line('YOD-->OU:' || V_OU_CODE || ' SUBINV:' || i.SUBINV_CODE || ' V_DEFAULT_LOCATOR:' || V_DEFAULT_LOCATOR  );
            
             UPDATE PB_SUBINV SET DEFAULT_LOC = (SELECT MIN(LOCATOR_CODE)
                                                FROM PB_LOCATOR
                                                WHERE  OU_CODE = V_OU_CODE
                                                AND    SUBINV_CODE = i.SUBINV_CODE)
             WHERE OU_CODE = V_OU_CODE
             AND    SUBINV_CODE = i.SUBINV_CODE;
          END IF;
          
        END LOOP;

			  -- UPDATE INTERFACE DATE
			  BEGIN
			    UPDATE PB_INTERFACE
			    SET    LAST_TF  = SYSDATE,
			           UPD_BY   = V_UPD_BY,
			           UPD_DATE = SYSDATE,
			           UPD_PGM  = V_UPD_PGM
			    WHERE  TNAME    = 'PB_LOCATOR'
			    AND    OU_CODE  = V_OU_CODE;
			  EXCEPTION WHEN OTHERS THEN NULL;
			  END;


			  -- WRITE END PROCESS
			  M_SEQ := M_SEQ+0.01;

			  BEGIN
			    INSERT INTO JOBS_MONITOR(JOB_ID, JOB_SEQ, JOB_NAME, JOB_MESG, JOB_TIME, SUC_CNT, ERR_CNT, UPD_BY, UPD_DATE, UPD_PGM, STATUS)
			    VALUES (N_ID, M_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', 'Complete Table PB_LOCATOR', SYSDATE, SUC_CNT, ERR_CNT, V_UPD_BY, SYSDATE, V_UPD_PGM,'Completed');
			  EXCEPTION WHEN OTHERS THEN NULL;
			  END;

			  -- *** End Locator Process *** --

	   ELSIF NVL(V_FULL_LOAD,'N') = 'N' THEN -- INTERFACE ONLY UPDATE DATA

	     BEGIN
		     SELECT LAST_TF
	         INTO   D_LAST
			 FROM   PB_INTERFACE
	         WHERE  TNAME = 'PB_SUBINV'
	         AND    OU_CODE = V_OU_CODE;
		 EXCEPTION WHEN NO_DATA_FOUND THEN
	         D_LAST := SYSDATE;
		 END;

		  -- CHECK LAST DATE TF
		  BEGIN
		   SELECT DISTINCT 'Y'
		   INTO   CK_UPD
		   FROM   PB_SUBINV_INFO_V
		   WHERE  master_organization_id = (SELECT OA_ORG FROM PB_COMPANY WHERE OU_CODE = V_OU_CODE)
       AND   (UPD_DATE >= D_LAST OR CRE_DATE >= D_LAST);
		  EXCEPTION WHEN NO_DATA_FOUND THEN CK_UPD := 'N';
		  END;

	     IF CK_UPD = 'Y' THEN -- FOUND SUBINVENTORY DATA TO UPDATE

			  -- WRITE START SUBINVENTORY PROCESS
	    	  M_SEQ := M_SEQ+0.01;
			  BEGIN
				INSERT INTO JOBS_MONITOR(JOB_ID, JOB_SEQ, JOB_NAME, JOB_MESG, JOB_TIME, SUC_CNT, ERR_CNT, UPD_BY, UPD_DATE, UPD_PGM, STATUS)
				VALUES (N_ID, M_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', 'Processing table PB_SUBINV', SYSDATE, SUC_CNT, ERR_CNT, V_UPD_BY, SYSDATE, V_UPD_PGM,'Start Process');
			  EXCEPTION WHEN OTHERS THEN NULL;
			  END;

			  FOR i IN sub_upd(D_LAST) LOOP

			  TEMP_STATUS := 'OK';

			      -- ***** Get Mapping Value ***** --
				     -- Get Province
					 BEGIN
					   SELECT REGION_CODE, PROVINCE_CODE
					   INTO   v_reg, v_prv
					   FROM   PB_PROVINCE P
					   WHERE  P.PROVINCE_NAME     = DECODE(i.PROVINCE,'กรุงเทพมหานคร','กรุงเทพ',i.PROVINCE)
					   AND    ROWNUM = 1;
						 -- Get Amphur
						 BEGIN
			               SELECT AMPHUR_CODE
						   INTO   v_amp
						   FROM   PB_AMPHUR AM
						   WHERE  i.AMPHUR LIKE '%'||AM.AMPHUR_NAME(+)||'%'
						   AND    AM.PROVINCE_CODE = v_prv
						   AND    ROWNUM = 1;
						     -- Get Postal
							 BEGIN
							   SELECT PT.POSTAL_CODE
							   INTO   v_post
							   FROM   PB_POSTAL PT
							   WHERE  PT.POSTAL_CODE = i.POSTAL_CODE
							   AND    PT.AMPHUR_CODE = v_amp
							   AND    ROWNUM = 1;
							 EXCEPTION WHEN NO_DATA_FOUND THEN
							   v_reg  := NULL;
							   v_prv  := NULL;
							   v_amp  := NULL;
							   v_post := NULL;
							 END;
						 EXCEPTION WHEN NO_DATA_FOUND THEN
						   v_reg  := NULL;
						   v_prv  := NULL;
						   v_amp  := NULL;
						   v_post := NULL;
						 END;
					 EXCEPTION WHEN NO_DATA_FOUND THEN
					   v_reg  := NULL;
					   v_prv  := NULL;
					   v_amp  := NULL;
					   v_post := NULL;
					 END;

				  -- ***** Main Process ***** --

				  -- *** Start Subinventory Process *** --
				  -- Check Exist Subinventory
				  BEGIN
			        SELECT DISTINCT 'X'
					INTO   v_ck
					FROM   PB_SUBINV
			        WHERE  SUBINV_CODE = i.SUBINV_CODE;
					-- Update Subinventory
					BEGIN
						UPDATE PB_SUBINV
						SET    SUBINV_DESC         = i.SUBINV_NAME,
			                /*  Cherry :- Modify on 31/05/2550 --> HD0002207 No Synchronize Address on Update */
							  -- SUBINV_ADD          = i.ADDRESS,
			 			      -- AMPHUR_CODE         = v_amp,
			                  -- PROVINCE_CODE       = v_prv,
						      -- POSTAL_CODE	       = v_post,
			                  -- REGION_CODE         = v_reg,
							   SUBINV_TAXID        = V_TAX_ID,
				               DEAL_CODE           = '01',
				               PRICELIST_CODE      = 'CASH', --'7007'
				               OA_SUBINV_CODE      = i.SUBINV_CODE,
							   SUBINV_TYP_CODE     = DECODE(i.SUBINV_TYPE_CODE,'INTRANSIT','I','SHOP','S','WAREHOUSE','W','SUPPLIER','W','S'),
				               OA_SUBINV_INTRANSIT = i.ORG_SUBINV_INTRANSIT,
							   OA_ORG_CODE         = i.ORG_CODE,
					           OA_ORG_ID		   = i.ORG_ID,
				               OA_OU_ID            = i.OU_ID,
				               CHARGE_ACCOUNT_ID   = i.ORG_AP_ACCRUAL_ACCOUNT,
							   BUDGET_ACCOUNT_ID   = i.ORG_AP_ACCRUAL_ACCOUNT,
				               PREFIX_SUBINV	   = i.SUBINV_CODE,
				               SUBINV_TYPE         = i.SUBINV_TYPE_CODE,
				               OA_LOCATION_CODE    = i.SUBINV_LOCATION_CODE,
							   UPD_DATE            = SYSDATE,
							   UPD_BY              = 'SS-STAFF'
						WHERE  OU_CODE = V_OU_CODE
						AND    SUBINV_CODE = i.SUBINV_CODE;
						SUC_CNT := SUC_CNT +1 ;
						  -- ***** Clear Monitor and Error Log ****
						    -- Clear Error Log
							BEGIN
							  DELETE FROM JOBS_LOG
							  WHERE  JOB_ID   = n_id
							  AND    ERR_MESG LIKE '%Subinventory%'
							  AND    DATA     =  i.subinv_code;
							EXCEPTION WHEN OTHERS THEN
							  NULL;
							END;
							-- Clear Monitor Log
							BEGIN
							  SELECT DISTINCT 'X'
							  INTO   V_TEMP
							  FROM   JOBS_LOG
							  WHERE  JOB_ID   = n_id
							  AND    ERR_MESG LIKE '%Subinventory%'
							  AND    DATA     <>  i.subinv_code;
							EXCEPTION WHEN NO_DATA_FOUND THEN
							  BEGIN
							    UPDATE JOBS_MONITOR
								SET    SUC_CNT  = SUC_CNT + ERR_CNT,
								       ERR_CNT  = 0
								WHERE  JOB_ID   = n_id
								AND    JOB_NAME = 'RCV ERP SUBINVENTORY AND LOCATOR'
								AND    JOB_MESG = 'Complete Table PB_SUBINV';
							  EXCEPTION WHEN OTHERS THEN NULL;
							  END;
							END;
					EXCEPTION WHEN DUP_VAL_ON_INDEX THEN TEMP_STATUS := 'Error';
							      ERR_CNT := ERR_CNT +1 ;
								  ERR_CODE := SQLCODE;
							      ERR_MESG := SQLERRM;
								  E_SEQ    := E_SEQ+0.01;
							      -- INSERT TO LOG TABLE
							      BEGIN
							        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
							        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', i.subinv_code, 'Duplicate Subinventory!', SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
							      EXCEPTION WHEN OTHERS THEN NULL;
							      END;
	           				  WHEN INVALID_NUMBER   THEN TEMP_STATUS := 'Error';
							      ERR_CNT := ERR_CNT +1 ;
							      ERR_CODE := SQLCODE;
							      ERR_MESG := SQLERRM;
								  E_SEQ    := E_SEQ+0.01;
							      -- INSERT TO LOG TABLE
							      BEGIN
							        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
							        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', i.subinv_code, 'Invalid Number of Subinventory!', SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
							      EXCEPTION WHEN OTHERS THEN NULL;
							      END;
							  WHEN VALUE_ERROR      THEN TEMP_STATUS := 'Error';
							      ERR_CNT := ERR_CNT +1 ;
							      ERR_CODE := SQLCODE;
							      ERR_MESG := SQLERRM;
								  E_SEQ    := E_SEQ+0.01;
							      -- INSERT TO LOG TABLE
							      BEGIN
							        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
							        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', i.subinv_code, 'Value Error of Subinventory!', SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
							      EXCEPTION WHEN OTHERS THEN NULL;
							      END;
					          WHEN OTHERS           THEN TEMP_STATUS := 'Error';
							      ERR_CNT := ERR_CNT +1 ;
							      ERR_CODE := SQLCODE;
							      ERR_MESG := SQLERRM;
								  E_SEQ    := E_SEQ+0.01;
							      -- INSERT TO LOG TABLE
							      BEGIN
							        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
							        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', i.subinv_code, SUBSTR(ERR_CODE||'-'||ERR_MESG,1,200), SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
							      EXCEPTION WHEN OTHERS THEN NULL;
							      END;
					END;
				  EXCEPTION WHEN NO_DATA_FOUND THEN
			 		-- Insert Subinventory
				    BEGIN
			          INSERT INTO PB_SUBINV(OU_CODE, SUBINV_CODE, SUBINV_DESC, SUBINV_ADD, AMPHUR_CODE, PROVINCE_CODE, POSTAL_CODE,
										    REGION_CODE, SUBINV_TAXID, DEAL_CODE, PRICELIST_CODE, OA_SUBINV_CODE, SUBINV_TYP_CODE,
										    OA_SUBINV_INTRANSIT, OA_ORG_CODE, OA_ORG_ID, OA_OU_ID, CHARGE_ACCOUNT_ID,
										    BUDGET_ACCOUNT_ID, PREFIX_SUBINV, 
                        OA_ACC_SEGMENT1, 
                        OA_ACC_SEGMENT2, 
                        SUBINV_TYPE,
											OA_LOCATION_CODE, UPD_DATE, UPD_BY)
					      VALUES (V_OU_CODE, i.SUBINV_CODE, i.SUBINV_NAME,i.ADDRESS, v_amp, v_prv, v_post,
					              v_reg, V_TAX_ID, '01', 'CASH',i.SUBINV_CODE, DECODE(i.SUBINV_TYPE_CODE,'INTRANSIT','I','SHOP','S','WAREHOUSE','W','SUPPLIER','W','S'),
			                  i.ORG_SUBINV_INTRANSIT, i.ORG_CODE, i.ORG_ID, i.OU_ID,i.ORG_AP_ACCRUAL_ACCOUNT,
							          i.ORG_AP_ACCRUAL_ACCOUNT, i.SUBINV_CODE, 
                        '000A', 
                        CASE WHEN V_OU_CODE in ('TAC','DTN','TPY') THEN '0511' 
                             WHEN V_OU_CODE in ('PAY') THEN '0846' 
                             ELSE NULL
                        END, 
                        i.SUBINV_TYPE_CODE,
                        i.SUBINV_LOCATION_CODE, SYSDATE, 'SS-STAFF');
                        
                INSERT_FIX_USER_PERMISSION(V_OU_CODE,i.SUBINV_CODE);
                 
							  SUC_CNT := SUC_CNT +1 ;
						  -- ***** Clear Monitor and Error Log ****
						    -- Clear Error Log
							BEGIN
							  DELETE FROM JOBS_LOG
							  WHERE  JOB_ID   = n_id
							  AND    ERR_MESG LIKE '%Subinventory%'
							  AND    DATA     =  i.subinv_code;
							EXCEPTION WHEN OTHERS THEN
							  NULL;
							END;
							-- Clear Monitor Log
							BEGIN
							  SELECT DISTINCT 'X'
							  INTO   V_TEMP
							  FROM   JOBS_LOG
							  WHERE  JOB_ID   = n_id
							  AND    ERR_MESG LIKE '%Subinventory%'
							  AND    DATA     <>  i.subinv_code;
							EXCEPTION WHEN NO_DATA_FOUND THEN
							  BEGIN
							    UPDATE JOBS_MONITOR
								SET    SUC_CNT  = SUC_CNT + ERR_CNT,
								       ERR_CNT  = 0
								WHERE  JOB_ID   = n_id
								AND    JOB_NAME = 'RCV ERP SUBINVENTORY AND LOCATOR'
								AND    JOB_MESG = 'Complete Table PB_SUBINV';
							  EXCEPTION WHEN OTHERS THEN NULL;
							  END;
							END;
					EXCEPTION WHEN DUP_VAL_ON_INDEX THEN TEMP_STATUS := 'Error';
							      ERR_CNT := ERR_CNT +1 ;
							      ERR_CODE := SQLCODE;
							      ERR_MESG := SQLERRM;
								  E_SEQ    := E_SEQ+0.01;
							      -- INSERT TO LOG TABLE
							      BEGIN
							        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
							        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', i.subinv_code, 'Duplicate Subinventory!', SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
							      EXCEPTION WHEN OTHERS THEN NULL;
							      END;
					          WHEN INVALID_NUMBER   THEN TEMP_STATUS := 'Error';
							      ERR_CNT := ERR_CNT +1 ;
							      ERR_CODE := SQLCODE;
							      ERR_MESG := SQLERRM;
								  E_SEQ    := E_SEQ+0.01;
							      -- INSERT TO LOG TABLE
							      BEGIN
							        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
							        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', i.subinv_code, 'Invalid Number of Subinventory!', SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
							      EXCEPTION WHEN OTHERS THEN NULL;
							      END;
							  WHEN VALUE_ERROR      THEN TEMP_STATUS := 'Error';
							      ERR_CNT := ERR_CNT +1 ;
							      ERR_CODE := SQLCODE;
							      ERR_MESG := SQLERRM;
								  E_SEQ    := E_SEQ+0.01;
							      -- INSERT TO LOG TABLE
							      BEGIN
							        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
							        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', i.subinv_code, 'Value Error of Subinventory!', SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
							      EXCEPTION WHEN OTHERS THEN NULL;
							      END;
							  WHEN OTHERS           THEN TEMP_STATUS := 'Error';
							      ERR_CNT := ERR_CNT +1 ;
							      ERR_CODE := SQLCODE;
							      ERR_MESG := SQLERRM;
								  E_SEQ    := E_SEQ+0.01;
							      -- INSERT TO LOG TABLE
							      BEGIN
							        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
							        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', i.subinv_code, SUBSTR(ERR_CODE||'-'||ERR_MESG,1,200), SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
							      EXCEPTION WHEN OTHERS THEN NULL;
							      END;
					END;
				  END;

			  END LOOP;

			  -- UPDATE INTERFACE DATE
			  BEGIN
			    UPDATE PB_INTERFACE
			    SET    LAST_TF  = SYSDATE,
			           UPD_BY   = V_UPD_BY,
			           UPD_DATE = SYSDATE,
			           UPD_PGM  = V_UPD_PGM
			    WHERE  TNAME    = 'PB_SUBINV'
			    AND    OU_CODE  = V_OU_CODE;
			  EXCEPTION WHEN OTHERS THEN NULL;
			  END;


			  -- WRITE END PROCESS
			  M_SEQ := M_SEQ+0.01;

			  BEGIN
			    INSERT INTO JOBS_MONITOR(JOB_ID, JOB_SEQ, JOB_NAME, JOB_MESG, JOB_TIME, SUC_CNT, ERR_CNT, UPD_BY, UPD_DATE, UPD_PGM, STATUS)
			    VALUES (N_ID, M_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', 'Complete Table PB_SUBINV', SYSDATE, SUC_CNT, ERR_CNT, V_UPD_BY, SYSDATE, V_UPD_PGM,'Completed');
			  EXCEPTION WHEN OTHERS THEN NULL;
			  END;

				  -- *** End Subinventory Process *** --

	     END IF; -- FOUND SUBINVENTORY DATA TO UPDATE

	     BEGIN
		     SELECT LAST_TF
	         INTO   D_LAST
			 FROM   PB_INTERFACE
	         WHERE  TNAME = 'PB_LOCATOR'
	         AND    OU_CODE = V_OU_CODE;
		 EXCEPTION WHEN NO_DATA_FOUND THEN
	         D_LAST := SYSDATE;
		 END;

		  -- CHECK LAST DATE TF
		  BEGIN
		   SELECT DISTINCT 'Y'
		   INTO   CK_UPD
		   FROM   PB_LOCATOR_INFO_V
		   WHERE  (UPD_DATE >= D_LAST OR CRE_DATE >= D_LAST);
		  EXCEPTION WHEN NO_DATA_FOUND THEN CK_UPD := 'N';
		  END;

	     IF CK_UPD = 'Y' THEN -- FOUND LOCATOR DATA TO UPDATE

			  -- *** Start Locator Process *** --

			  SUC_CNT     := 0;
			  ERR_CNT     := 0;

			  -- WRITE START SUBINVENTORY PROCESS
	    	  M_SEQ := M_SEQ+0.01;
			  BEGIN
				INSERT INTO JOBS_MONITOR(JOB_ID, JOB_SEQ, JOB_NAME, JOB_MESG, JOB_TIME, SUC_CNT, ERR_CNT, UPD_BY, UPD_DATE, UPD_PGM, STATUS)
				VALUES (N_ID, M_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', 'Processing table PB_LOCATOR', SYSDATE, SUC_CNT, ERR_CNT, V_UPD_BY, SYSDATE, V_UPD_PGM,'Start Process');
			  EXCEPTION WHEN OTHERS THEN NULL;
			  END;

			  -- Check Locator of Subinventory
			  FOR j IN loc_upd(D_LAST) LOOP

                  TEMP_STATUS := 'OK';

		            -- Check Default Locator
					v_def  := 'N';
					IF j.DESCRIPTION = 'Good' THEN
					   BEGIN
					     SELECT DISTINCT 'X'
		                 INTO   v_ck
						 FROM   PB_LOCATOR
						 WHERE  OU_CODE              = V_OU_CODE
						 AND    SUBINV_CODE          = j.SUBINVENTORY_CODE
						 AND    NVL(USE_DEFAULT,'N') = 'Y';
						 v_def  := 'N';
					   EXCEPTION WHEN NO_DATA_FOUND THEN
					     v_def  := 'Y';
					   END;
					END IF;

		            -- Check Default Locator
		            BEGIN
					  SELECT DISTINCT 'X'
					  INTO   v_ck
					  FROM   PB_LOCATOR
					  WHERE  OU_CODE      = V_OU_CODE
					  AND    SUBINV_CODE  = j.SUBINVENTORY_CODE
					  AND    LOCATOR_CODE = j.OA_LOCATOR_CODE;
					  -- Update Locator
					  BEGIN
					    UPDATE PB_LOCATOR
						SET    LOC_DESC        = j.DESCRIPTION,
						       --USE_DEFAULT     = v_def,
						       --OA_LOCATOR      = j.OA_LOCATOR_ID,   --comment 27-Aug-14 Fix bug program update wrong locator_id
					 	       --LOCATOR_TYPE    = DECODE(j.DESCRIPTION,'Good','A','Defect','D','Claim','C','A'),
						       OA_LOCATOR_CODE = j.OA_LOCATOR_CODE,
							   UPD_BY          = 'SS-STAFF',
							   UPD_DATE        = SYSDATE
						WHERE  OU_CODE      = V_OU_CODE
					    AND    SUBINV_CODE  = j.SUBINVENTORY_CODE
					    AND    LOCATOR_CODE = j.OA_LOCATOR_CODE
              AND    OA_LOCATOR = j.OA_LOCATOR_ID;  --add 27-Aug-14 Fix bug program update wrong locator_id
              
						SUC_CNT := SUC_CNT +1 ;
						  -- ***** Clear Monitor and Error Log ****
						    -- Clear Error Log
							BEGIN
							  DELETE FROM JOBS_LOG
							  WHERE  JOB_ID   = n_id
							  AND    ERR_MESG LIKE '%Locator%'
							  AND    DATA     =  j.SUBINVENTORY_CODE||'|'||j.OA_LOCATOR_CODE;
							EXCEPTION WHEN OTHERS THEN
							  NULL;
							END;
							-- Clear Monitor Log
							BEGIN
							  SELECT DISTINCT 'X'
							  INTO   V_TEMP
							  FROM   JOBS_LOG
							  WHERE  JOB_ID   = n_id
							  AND    ERR_MESG LIKE '%Locator%'
							  AND    DATA     <>  j.SUBINVENTORY_CODE||'|'||j.OA_LOCATOR_CODE;
							EXCEPTION WHEN NO_DATA_FOUND THEN
							  BEGIN
							    UPDATE JOBS_MONITOR
								SET    SUC_CNT  = SUC_CNT + ERR_CNT,
								       ERR_CNT  = 0
								WHERE  JOB_ID   = n_id
								AND    JOB_NAME = 'RCV ERP SUBINVENTORY AND LOCATOR'
								AND    JOB_MESG = 'Complete Table PB_LOCATOR';
							  EXCEPTION WHEN OTHERS THEN NULL;
							  END;
							END;
					  EXCEPTION WHEN DUP_VAL_ON_INDEX THEN TEMP_STATUS := 'Error';
							      ERR_CNT  := ERR_CNT +1;
								  ERR_CODE := SQLCODE;
							      ERR_MESG := SQLERRM;
								  E_SEQ    := E_SEQ+0.01;
							      -- INSERT TO LOG TABLE
							      BEGIN
							        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
							        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', j.SUBINVENTORY_CODE||'|'||j.OA_LOCATOR_CODE, 'Duplicate Locator!', SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
							      EXCEPTION WHEN OTHERS THEN NULL;
							      END;
					            WHEN INVALID_NUMBER   THEN TEMP_STATUS := 'Error';
							      ERR_CNT  := ERR_CNT +1;
							      ERR_CODE := SQLCODE;
							      ERR_MESG := SQLERRM;
								  E_SEQ    := E_SEQ+0.01;
							      -- INSERT TO LOG TABLE
							      BEGIN
							        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
							        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', j.SUBINVENTORY_CODE||'|'||j.OA_LOCATOR_CODE, 'Invalid Number of Locator!', SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
							      EXCEPTION WHEN OTHERS THEN NULL;
							      END;
								WHEN VALUE_ERROR      THEN TEMP_STATUS := 'Error';
							      ERR_CNT  := ERR_CNT +1;
							      ERR_CODE := SQLCODE;
							      ERR_MESG := SQLERRM;
								  E_SEQ    := E_SEQ+0.01;
							      -- INSERT TO LOG TABLE
							      BEGIN
							        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
							        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', j.SUBINVENTORY_CODE||'|'||j.OA_LOCATOR_CODE, 'Value Error of  Locator!', SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
							      EXCEPTION WHEN OTHERS THEN NULL;
							      END;
								WHEN OTHERS           THEN TEMP_STATUS := 'Error';
							      ERR_CNT  := ERR_CNT +1;
							      ERR_CODE := SQLCODE;
							      ERR_MESG := SQLERRM;
								  E_SEQ    := E_SEQ+0.01;
							      -- INSERT TO LOG TABLE
							      BEGIN
							        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
							        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', j.SUBINVENTORY_CODE||'|'||j.OA_LOCATOR_CODE, SUBSTR(ERR_CODE||'-'||ERR_MESG,1,200), SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
							      EXCEPTION WHEN OTHERS THEN NULL;
							      END;
					  END;
					EXCEPTION WHEN NO_DATA_FOUND THEN
					  -- Insert Locator
					  BEGIN
					    INSERT INTO PB_LOCATOR(OU_CODE, SUBINV_CODE, LOCATOR_CODE, LOC_DESC, USE_DEFAULT, OA_LOCATOR, UPD_BY, UPD_DATE, LOCATOR_TYPE, OA_LOCATOR_CODE)
						VALUES (V_OU_CODE, j.SUBINVENTORY_CODE, j.OA_LOCATOR_CODE, j.DESCRIPTION, v_def, j.OA_LOCATOR_ID, 'SS-STAFF', SYSDATE, DECODE(j.DESCRIPTION,'Good','A','Defect','D','Claim','C','A'), j.OA_LOCATOR_CODE);
						SUC_CNT := SUC_CNT +1 ;
						  -- ***** Clear Monitor and Error Log ****
						    -- Clear Error Log
							BEGIN
							  DELETE FROM JOBS_LOG
							  WHERE  JOB_ID   = n_id
							  AND    ERR_MESG LIKE '%Locator%'
							  AND    DATA     =  j.SUBINVENTORY_CODE||'|'||j.OA_LOCATOR_CODE;
							EXCEPTION WHEN OTHERS THEN
							  NULL;
							END;
							-- Clear Monitor Log
							BEGIN
							  SELECT DISTINCT 'X'
							  INTO   V_TEMP
							  FROM   JOBS_LOG
							  WHERE  JOB_ID   = n_id
							  AND    ERR_MESG LIKE '%Locator%'
							  AND    DATA     <>  j.SUBINVENTORY_CODE||'|'||j.OA_LOCATOR_CODE;
							EXCEPTION WHEN NO_DATA_FOUND THEN
							  BEGIN
							    UPDATE JOBS_MONITOR
								SET    SUC_CNT  = SUC_CNT + ERR_CNT,
								       ERR_CNT  = 0
								WHERE  JOB_ID   = n_id
								AND    JOB_NAME = 'RCV ERP SUBINVENTORY AND LOCATOR'
								AND    JOB_MESG = 'Complete Table PB_LOCATOR';
							  EXCEPTION WHEN OTHERS THEN NULL;
							  END;
							END;
					  EXCEPTION WHEN DUP_VAL_ON_INDEX THEN TEMP_STATUS := 'Error';
							      ERR_CNT  := ERR_CNT +1;
							      ERR_CODE := SQLCODE;
							      ERR_MESG := SQLERRM;
								  E_SEQ    := E_SEQ+0.01;
							      -- INSERT TO LOG TABLE
							      BEGIN
							        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
							        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', j.SUBINVENTORY_CODE||'|'||j.OA_LOCATOR_CODE, 'Duplicate Locator!', SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
							      EXCEPTION WHEN OTHERS THEN NULL;
							      END;
					            WHEN INVALID_NUMBER   THEN TEMP_STATUS := 'Error';
							      ERR_CNT  := ERR_CNT +1;
							      ERR_CODE := SQLCODE;
							      ERR_MESG := SQLERRM;
								  E_SEQ    := E_SEQ+0.01;
							      -- INSERT TO LOG TABLE
							      BEGIN
							        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
							        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', j.SUBINVENTORY_CODE||'|'||j.OA_LOCATOR_CODE, 'Invalid Number of Locator!', SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
							      EXCEPTION WHEN OTHERS THEN NULL;
							      END;
								WHEN VALUE_ERROR      THEN TEMP_STATUS := 'Error';
							      ERR_CNT  := ERR_CNT +1;
							      ERR_CODE := SQLCODE;
							      ERR_MESG := SQLERRM;
								  E_SEQ    := E_SEQ+0.01;
							      -- INSERT TO LOG TABLE
							      BEGIN
							        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
							        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', j.SUBINVENTORY_CODE||'|'||j.OA_LOCATOR_CODE, 'Value Error of  Locator!', SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
							      EXCEPTION WHEN OTHERS THEN NULL;
							      END;
								WHEN OTHERS           THEN TEMP_STATUS := 'Error';
							      ERR_CNT  := ERR_CNT +1;
							      ERR_CODE := SQLCODE;
							      ERR_MESG := SQLERRM;
								  E_SEQ    := E_SEQ+0.01;
							      -- INSERT TO LOG TABLE
							      BEGIN
							        INSERT INTO JOBS_LOG(JOB_ID, JOB_SEQ, JOB_NAME, DATA, ERR_MESG, ERR_TIME, UPD_BY, UPD_DATE, UPD_PGM)
							        VALUES (N_ID, E_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', j.SUBINVENTORY_CODE||'|'||j.OA_LOCATOR_CODE, SUBSTR(ERR_CODE||'-'||ERR_MESG,1,200), SYSDATE, V_UPD_BY , SYSDATE, V_UPD_BY );
							      EXCEPTION WHEN OTHERS THEN NULL;
							      END;
					  END;
					END;

			  END LOOP;
        
        --To Update Default Locator 27-Apr-2016
        FOR i in sub_upd(D_LAST) Loop
          SELECT DEFAULT_LOC INTO V_DEFAULT_LOCATOR 
          FROM PB_SUBINV WHERE OU_CODE = V_OU_CODE
					     AND    SUBINV_CODE = i.SUBINV_CODE;
          
          IF V_DEFAULT_LOCATOR is NULL THEN
            
             UPDATE PB_SUBINV SET DEFAULT_LOC = (SELECT MIN(LOCATOR_CODE)
                                                FROM PB_LOCATOR
                                                WHERE  OU_CODE = V_OU_CODE
                                                AND    SUBINV_CODE = i.SUBINV_CODE)
             WHERE OU_CODE = V_OU_CODE
             AND    SUBINV_CODE = i.SUBINV_CODE;
          END IF;
          
        END LOOP;

			  -- UPDATE INTERFACE DATE
			  BEGIN
			    UPDATE PB_INTERFACE
			    SET    LAST_TF  = SYSDATE,
			           UPD_BY   = V_UPD_BY,
			           UPD_DATE = SYSDATE,
			           UPD_PGM  = V_UPD_PGM
			    WHERE  TNAME    = 'PB_LOCATOR'
			    AND    OU_CODE  = V_OU_CODE;
			  EXCEPTION WHEN OTHERS THEN NULL;
			  END;


			  -- WRITE END PROCESS
			  M_SEQ := M_SEQ+0.01;

			  BEGIN
			    INSERT INTO JOBS_MONITOR(JOB_ID, JOB_SEQ, JOB_NAME, JOB_MESG, JOB_TIME, SUC_CNT, ERR_CNT, UPD_BY, UPD_DATE, UPD_PGM, STATUS)
			    VALUES (N_ID, M_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', 'Complete Table PB_LOCATOR', SYSDATE, SUC_CNT, ERR_CNT, V_UPD_BY, SYSDATE, V_UPD_PGM,'Completed');
			  EXCEPTION WHEN OTHERS THEN NULL;
			  END;

			  -- *** End Locator Process *** --


	     END IF; -- FOUND LOCATOR DATA TO UPDATE

	   END IF; -- FULL LOAD OR UPDATE DATA

	   M_SEQ := M_SEQ+0.01;
	   BEGIN
	     INSERT INTO JOBS_MONITOR(JOB_ID, JOB_SEQ, JOB_NAME, JOB_MESG, JOB_TIME, SUC_CNT, ERR_CNT, UPD_BY, UPD_DATE, UPD_PGM, STATUS)
	     VALUES (N_ID, M_SEQ, 'RCV ERP SUBINVENTORY AND LOCATOR', 'End process receive Subinventory and Locator from ERP.', SYSDATE, NULL, NULL, V_UPD_BY, SYSDATE, V_UPD_PGM,'Completed');
	   EXCEPTION WHEN OTHERS THEN NULL;
	   END;

	   --V_WORK := CLEAR_JOB_WORK(N_NO);

	   COMMIT;

   --END IF;
   
   -- Add by Yodchai L. 17-Oct-2013 to auto gen data pb_area_dtl
    insert into pb_area_dtl
    select ou_code,'A' area_code, subinv_code, null, null, 'SIRAPRAPHA', sysdate, 'DTPBRT07' 
    from PB_SUBINV a 
    where not exists(select 'x' from pb_area_dtl b where b.ou_code =a.ou_code and b.subinv_code = a.subinv_code)
    and subinv_type = 'SHOP';

    COMMIT;

END RCV_SUB_LOC;
