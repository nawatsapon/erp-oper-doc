/* Formatted on 24/8/2016 23:17:20 (QP5 v5.136.908.31019) */
--ALTER TABLE APPS.DTTCNX002_LOG_DETAIL
-- DROP PRIMARY KEY CASCADE;

DROP TABLE APPS.DTTCNX003_LOG_DETAIL CASCADE CONSTRAINTS;

CREATE TABLE APPS.DTTCNX003_LOG_DETAIL
(
   LEVEL_TYPE        VARCHAR2 (1 BYTE) NOT NULL,
   REQUEST_ID        NUMBER NOT NULL,
   ERROR_TYPE        VARCHAR2 (50 BYTE) NOT NULL,
   FILE_NAME         VARCHAR2 (50 BYTE),
   LINE_TYPE         VARCHAR2 (20 BYTE),
   INVOICE_NO        VARCHAR2 (50 BYTE),
   LINE_NUMBER       NUMBER,
   CUSTOMER_NO       VARCHAR2 (50 BYTE),
   ITEM              VARCHAR2 (50 BYTE),
   AMOUNT            NUMBER,
   PROCESS_DATE      TIMESTAMP,
   PROCESSING_TIME   TIMESTAMP,
   STATUS            VARCHAR2 (50 BYTE),
   ERROR_CODE        VARCHAR2 (50 BYTE),
   ERROR_MESSAGE     VARCHAR2 (240 BYTE)
)
TABLESPACE APPS_TS_TX_DATA
PCTUSED 0
PCTFREE 10
INITRANS 1
MAXTRANS 255
STORAGE (INITIAL 128 K
         NEXT 1 M
         MINEXTENTS 1
         MAXEXTENTS UNLIMITED
         PCTINCREASE 0
         BUFFER_POOL DEFAULT)
LOGGING
NOCOMPRESS
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX APPS.DTTCNX003_LOG_DET_N01
   ON APPS.DTTCNX003_LOG_DETAIL (REQUEST_ID)
   LOGGING
   TABLESPACE APPS_TS_TX_IDX
   PCTFREE 10
   INITRANS 2
   MAXTRANS 255
   STORAGE (INITIAL 1 M
            NEXT 1 M
            MINEXTENTS 1
            MAXEXTENTS UNLIMITED
            PCTINCREASE 0
            BUFFER_POOL DEFAULT)
   NOPARALLEL;


CREATE INDEX APPS.DTTCNX003_LOG_DET_N02
   ON APPS.DTTCNX003_LOG_DETAIL (ERROR_TYPE)
   LOGGING
   TABLESPACE APPS_TS_TX_IDX
   PCTFREE 10
   INITRANS 2
   MAXTRANS 255
   STORAGE (INITIAL 1 M
            NEXT 1 M
            MINEXTENTS 1
            MAXEXTENTS UNLIMITED
            PCTINCREASE 0
            BUFFER_POOL DEFAULT)
   NOPARALLEL;


CREATE INDEX APPS.DTTCNX003_LOG_DET_N03
   ON APPS.DTTCNX003_LOG_DETAIL (REQUEST_ID, ERROR_TYPE)
   LOGGING
   TABLESPACE APPS_TS_TX_IDX
   PCTFREE 10
   INITRANS 2
   MAXTRANS 255
   STORAGE (INITIAL 1 M
            NEXT 1 M
            MINEXTENTS 1
            MAXEXTENTS UNLIMITED
            PCTINCREASE 0
            BUFFER_POOL DEFAULT)
   NOPARALLEL;


CREATE INDEX APPS.DTTCNX003_LOG_DET_N04
   ON APPS.DTTCNX003_LOG_DETAIL (REQUEST_ID, FILE_NAME)
   LOGGING
   TABLESPACE APPS_TS_TX_IDX
   PCTFREE 10
   INITRANS 2
   MAXTRANS 255
   STORAGE (INITIAL 1 M
            NEXT 1 M
            MINEXTENTS 1
            MAXEXTENTS UNLIMITED
            PCTINCREASE 0
            BUFFER_POOL DEFAULT)
   NOPARALLEL;


CREATE INDEX APPS.DTTCNX003_LOG_DET_N05
   ON APPS.DTTCNX003_LOG_DETAIL (REQUEST_ID, ERROR_TYPE, FILE_NAME)
   LOGGING
   TABLESPACE APPS_TS_TX_IDX
   PCTFREE 10
   INITRANS 2
   MAXTRANS 255
   STORAGE (INITIAL 1 M
            NEXT 1 M
            MINEXTENTS 1
            MAXEXTENTS UNLIMITED
            PCTINCREASE 0
            BUFFER_POOL DEFAULT)
   NOPARALLEL;