#!/bin/sh
# +==========================================================================+
# |
# |   Program Name:     TAC - Read and Insert to Temp table - TCNX
# |
# |   Program Type:     Korn Shell Script
# |
# |   Created by:       Wisut Munapisith 
# |
# |
# |   Purpose:          Generic shell script to
# |                   1. Call package for insert data to Temp table
# |
# |
# |   Called By:        Oracle Applications Concurrent Manager Request
# |			run ln -s $FND_TOP/bin/fndcpesr DTTCNX001_2
# |
# |   Program Parameters:
# |      0: Program file name
# |      1: User ID/Password for connect database
# |      2: Last login ID
# |      3: User Name for login application
# |      4: Request ID
# |
# |   Program Parameters:
# |      5: Batch Source Name
# |      6: Interface Line Context
# |      7: Customer Transaction Type Name
# |      8: Term Name
# |      9: File Name
# |    10: Folder Name
# |
# +===========================================================================+
# | Change History
# | Date      Author      Version Description
# | --------- ----------- ------- ------------------------------------------------
# | 18-Aug-16  Wisut M.    1.00    Created Program
# |
# +===========================================================================+

# ----------------------------------------------------------------------------
# Declare variable
# ----------------------------------------------------------------------------

v_progfile=$0
v_userid=$1
v_loginid=$2
v_username=$3
v_requestid=$4
v_batchname=$5
v_itfline=$6
v_trxtype=$7
v_termname=$8
v_filelist=$9
v_xmldir=${10}
v_logdir=${11}
v_expireday=${12}
v_logparafile="TAC_TCNX_READ_AND_INSERT_PARAM.log"`date +%y%m%d%H%M%S`
v_logname="VALIDATE_DATA"
#v_logdir="ERP_TCNX_AR_LOG"
v_month=""
v_year=""
v_filename=""
v_readpath=""
v_logpath=""
v_numloop=0
v_filecount=0

# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# Get path for read XML file
# ----------------------------------------------------------------------------

load_directories()
{
(sqlplus -s /nolog << end_of_sql
conn $v_userid
set serveroutput on size 99999
exec TAC_PKG_TCNX_CUST.P_GET_DIRECTORIES('IBX','$v_xmldir');
exec TAC_PKG_TCNX_CUST.P_GET_DIRECTORIES('LOG','$v_logdir');
exit
end_of_sql
) > $v_logparafile

#v_readpath=`cat $v_logparafile | sed -n '1p;1q' | sed -e "s/PARA_IBX_PATH=//"`
#v_logpath=`cat $v_logparafile | sed -n '3p;3q' | sed -e "s/PARA_LOG_PATH=//"`
v_readpath=`grep -i "PARA_IBX_PATH" $v_logparafile | sed -e "s/PARA_IBX_PATH=//"`
v_logpath=`grep -i "PARA_LOG_PATH" $v_logparafile | sed -e "s/PARA_LOG_PATH=//"`

echo "XML Path: $v_readpath"

rm $v_logparafile
}

# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# Insert temp table
# ----------------------------------------------------------------------------

insert_temp()
{
(sqlplus -s /nolog << end_of_sql
conn $v_userid
set serveroutput on size 99999
exec TAC_PKG_TCNX_CUST.P_READ_AND_INSERT($v_requestid,'$v_month',$v_year,'$v_batchname','$v_itfline','$v_trxtype','$v_termname','$v_filename','$v_xmldir');
exit
end_of_sql
)
}

# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# Log Monitor
# ----------------------------------------------------------------------------

log_monitor()
{
(sqlplus -s /nolog << end_of_sql
conn $v_userid
set serveroutput on size 99999
exec TAC_PKG_TCNX_CUST.P_LOG_MONITOR('$v_logname',$v_requestid,'$v_logdir');
exit
end_of_sql
)
}

# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# Main Program
# ----------------------------------------------------------------------------
echo 'Starting...'

#echo "Total Number of Parameters : $#"
#echo '+----------------------------------------------------------------------------+'
#echo "Print System Parameters"
#echo '+----------------------------------------------------------------------------+'
#echo "Program File: $v_progfile"
#echo "User ID: $v_userid"
#echo "Logiin: $v_loginid"
#echo "User Name: $v_username"
#echo "Request ID: $v_requestid"
#echo " "
echo '+----------------------------------------------------------------------------+'
echo "Print Program Parameters"
echo '+----------------------------------------------------------------------------+'
echo "Batch source name: $v_batchname"
echo "Interface: $v_itfline"
echo "Interface line context: $v_trxtype"
echo "Term name: $v_termname"
echo "File list name: $v_filelist"
echo "Inbox directory name: $v_xmldir"
echo "Log directory name: $v_logdir"
echo "Log parameter file: $v_logparafile"
echo "Log expiration day: $v_expireday"
echo " "
echo '+----------------------------------------------------------------------------+'
echo "XML Directory Path:"
echo '+----------------------------------------------------------------------------+'
load_directories
echo '+3----------------------------------------------------------------------------+'

while read f1
do

v_numloop=`expr $v_numloop + 1`
#echo $v_numloop

if [ $v_numloop = "1" ]; then
#   echo "L$v_numloop: $f1"
   
   v_month=$f1
   echo "Month :$v_month"
else
   
   if [ $v_numloop = "2" ]; then
#      echo "L$v_numloop: $f1"
      
      v_year=$f1
      echo "Year :$v_year"
   else
#      echo "L$v_numloop: $f1"
      
      v_filecount=`expr $v_filecount + 1`
      v_filename=$f1
      echo "XML File Name :$v_filename"
      echo '+----------------------------------------------------------------------------+'
      insert_temp
      
      rm $v_readpath/$v_filename
   fi
fi

done < $v_readpath/$v_filelist

rm $v_readpath/$v_filelist

echo "File Count: $v_filecount"

echo '+----------------------------------------------------------------------------+'
echo "Log Monitor:"
echo '+----------------------------------------------------------------------------+'
find $v_logpath/DTTCNX003_VALIDATE_DATA*.TXT -mtime +$v_expireday -exec rm {} \
log_monitor

# ----------------------------------------------------------------------------
# End of Unix Program
