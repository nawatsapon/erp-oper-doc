/* Formatted on 24/8/2016 23:25:36 (QP5 v5.136.908.31019) */
ALTER TABLE APPS.DTTCNX004_LOG_SUMMARY
 DROP PRIMARY KEY CASCADE;

DROP TABLE APPS.DTTCNX004_LOG_SUMMARY CASCADE CONSTRAINTS;

CREATE TABLE APPS.DTTCNX004_LOG_SUMMARY
(
   LEVEL_TYPE          VARCHAR2 (1 BYTE) NOT NULL,
   REQUEST_ID          NUMBER NOT NULL,
   PROGRAM_NAME        VARCHAR2 (50 BYTE),
   PROCESS_DATE        TIMESTAMP,
   PROCESSING_TIME     TIMESTAMP,
   OUTPUT_FILE_COUNT   NUMBER 
)
TABLESPACE APPS_TS_TX_DATA
PCTUSED 0
PCTFREE 10
INITRANS 1
MAXTRANS 255
STORAGE (INITIAL 128 K
         NEXT 1 M
         MINEXTENTS 1
         MAXEXTENTS UNLIMITED
         PCTINCREASE 0
         BUFFER_POOL DEFAULT)
LOGGING
NOCOMPRESS
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX APPS.DTTCNX004_LOG_SUM_PK
   ON APPS.DTTCNX004_LOG_SUMMARY (LEVEL_TYPE, REQUEST_ID)
   LOGGING
   TABLESPACE APPS_TS_TX_IDX
   PCTFREE 10
   INITRANS 2
   MAXTRANS 255
   STORAGE (INITIAL 128 K
            NEXT 1 M
            MINEXTENTS 1
            MAXEXTENTS UNLIMITED
            PCTINCREASE 0
            BUFFER_POOL DEFAULT)
   NOPARALLEL;


CREATE UNIQUE INDEX APPS.DTTCNX004_LOG_SUM_U01
   ON APPS.DTTCNX004_LOG_SUMMARY (REQUEST_ID)
   LOGGING
   TABLESPACE APPS_TS_TX_IDX
   PCTFREE 10
   INITRANS 2
   MAXTRANS 255
   STORAGE (INITIAL 1 M
            NEXT 1 M
            MINEXTENTS 1
            MAXEXTENTS UNLIMITED
            PCTINCREASE 0
            BUFFER_POOL DEFAULT)
   NOPARALLEL;


ALTER TABLE APPS.DTTCNX004_LOG_SUMMARY ADD (
  CONSTRAINT DTTCNX004_LOG_SUM_PK
 PRIMARY KEY
 (LEVEL_TYPE, REQUEST_ID)
    USING INDEX
    TABLESPACE APPS_TS_TX_IDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128 K
                NEXT             1 M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT DTTCNX004_LOG_SUM_U01
 UNIQUE (REQUEST_ID)
    USING INDEX
    TABLESPACE APPS_TS_TX_IDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          1 M
                NEXT             1 M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));