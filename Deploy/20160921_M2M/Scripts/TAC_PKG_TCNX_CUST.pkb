CREATE OR REPLACE PACKAGE BODY APPS.TAC_PKG_TCNX_CUST AS
/******************************************************************************
    NAME: APPS.tac_pkg_tcnx_cust
    PURPOSE: tac customize function, procedure

    REVISIONS:
    Ver                 Date                    Author                              Description
    ----------          ---------------         --------------------                ----------------------------------------
    1.0                 16/08/2016              wsmice                              1. Created this package body.
******************************************************************************/
    
    ERROR_OUTPUT_PATH   EXCEPTION;
    ERROR_FILE_NAME   EXCEPTION;
    
  /*
  -- Procedure write_log
  -- Write message to log file for each concurrent request.
  */
    PROCEDURE P_WRITE_LOG ( 
                                                PI_MSG IN VARCHAR2 
                                             ) IS 
    BEGIN
        FND_FILE.PUT_LINE(FND_FILE.LOG, PI_MSG);
    EXCEPTION
        WHEN OTHERS THEN
            NULL;
    END P_WRITE_LOG;

    PROCEDURE P_WRITE_OUTPUT ( 
                                                        PI_MSG IN VARCHAR2 
                                                   ) IS 
    BEGIN
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT, PI_MSG);
    EXCEPTION
        WHEN OTHERS THEN
            NULL;
    END P_WRITE_OUTPUT;

    PROCEDURE P_WRITE_LINE ( 
                                                    PIO_FILE_HANDLE IN OUT UTL_FILE.FILE_TYPE, 
                                                    PI_LINE_BUFF IN NVARCHAR2 
                                             ) IS 
        
        -- For write error log
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
    
        UTL_FILE.PUT_LINE_NCHAR(PIO_FILE_HANDLE, PI_LINE_BUFF);
        UTL_FILE.FFLUSH(PIO_FILE_HANDLE);
    EXCEPTION
        WHEN UTL_FILE.INVALID_FILEHANDLE THEN
            V_USER_ERROR := 'Invalid filehandle.';
            P_WRITE_LOG(V_USER_ERROR);
        WHEN UTL_FILE.INVALID_OPERATION THEN
            V_USER_ERROR := 'Invalid operation.';
            P_WRITE_LOG(V_USER_ERROR);
        WHEN UTL_FILE.WRITE_ERROR THEN
            V_USER_ERROR := 'Write error.';
            P_WRITE_LOG(V_USER_ERROR);
        WHEN OTHERS THEN
            V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
            P_WRITE_LOG(V_USER_ERROR);
    END P_WRITE_LINE;
    
    PROCEDURE P_SET_COMPLETE_STATUS /*( 
                                                                        PIO_ERRCODE IN OUT VARCHAR2, 
                                                                        PIO_ERRMSG IN OUT VARCHAR2 
                                                                )*/ IS
        
        v_status_flag   BOOLEAN;
        
    BEGIN
        v_status_flag := fnd_concurrent.set_completion_status ('WARNING', NULL);
    END P_SET_COMPLETE_STATUS;
    
    FUNCTION FN_GET_PROCESS_DATE /*( 
                                                                PIO_ERRCODE IN OUT VARCHAR2, 
                                                                PIO_ERRMSG IN OUT VARCHAR2 
                                                          )*/  RETURN TIMESTAMP IS
        
        V_PROCESS_DATE   TIMESTAMP;
        
        -- For write error log
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
        SELECT CURRENT_TIMESTAMP 
        INTO V_PROCESS_DATE
        FROM DUAL;
        
        RETURN (V_PROCESS_DATE);
    EXCEPTION
        WHEN OTHERS THEN
            V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
            P_WRITE_LOG(V_USER_ERROR);
            
            RETURN (NULL);
    END FN_GET_PROCESS_DATE;
    
    FUNCTION FN_GET_PROCESS_TIME ( 
                                                                /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                                PIO_ERRMSG IN OUT VARCHAR2, */
                                                                PI_PROCESS_DATE IN TIMESTAMP 
                                                         ) RETURN TIMESTAMP IS
        
        V_PROCESS_DATE   TIMESTAMP;
        V_PROCESS_TIME   TIMESTAMP;
        
        -- For write error log
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
        V_PROCESS_DATE := FN_GET_PROCESS_DATE;
        
        IF (PI_PROCESS_DATE IS NOT NULL) AND (V_PROCESS_DATE IS NOT NULL) THEN
            V_PROCESS_TIME := TO_TIMESTAMP(TO_CHAR(PI_PROCESS_DATE, 'DDMMYYYY')||' '||TO_CHAR(V_PROCESS_DATE - PI_PROCESS_DATE, 'HH24:MI:SS.FF'), 'DDMMYYYY HH24:MI:SS.FF');
        ELSE
            V_PROCESS_TIME := NULL;
        END IF;
        
        RETURN V_PROCESS_TIME;
    EXCEPTION
        WHEN OTHERS THEN
            V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
            P_WRITE_LOG(V_USER_ERROR);
            
            RETURN (NULL);
    END FN_GET_PROCESS_TIME;
    
    FUNCTION FN_GET_DIRECTORIES ( 
                                                            /*PIO_ERR_CODE IN OUT VARCHAR2, 
                                                            PIO_ERR_MSG IN OUT VARCHAR2, */
                                                            PI_DIR_NAME IN VARCHAR2 
                                                      ) RETURN VARCHAR2 IS 
        
        V_DIR_PATH   DBA_DIRECTORIES.DIRECTORY_PATH%TYPE;
        
        -- For write error log
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
        SELECT dbad.DIRECTORY_PATH 
        INTO V_DIR_PATH 
        FROM DBA_DIRECTORIES dbad 
        WHERE dbad.DIRECTORY_NAME = PI_DIR_NAME;
        
        RETURN (V_DIR_PATH);
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            V_USER_ERROR := 'Path was not defined in table:DBA_DIRECTORIES.';
            P_WRITE_LOG(V_USER_ERROR);
            
            RETURN (NULL);
        WHEN TOO_MANY_ROWS THEN
            V_USER_ERROR := 'Found data too many rows.';
            P_WRITE_LOG(V_USER_ERROR);
            
            RETURN (NULL);
        WHEN OTHERS THEN
            V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
            P_WRITE_LOG(V_USER_ERROR);
            
            RETURN (NULL);
    END FN_GET_DIRECTORIES;
    
    FUNCTION FN_GET_PROGRAM_NAME ( 
                                                                /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                                PIO_ERRMSG IN OUT VARCHAR2, */
                                                                PI_CONC_REQUEST_ID IN NUMBER 
                                                           ) RETURN VARCHAR2 IS
        
        V_USER_CONC_PROG_NAME   FND_CONCURRENT_PROGRAMS_VL.USER_CONCURRENT_PROGRAM_NAME%TYPE;
        
        -- For write error log
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
--        SELECT FNDC_PVL.USER_CONCURRENT_PROGRAM_NAME 
--        INTO V_USER_CONC_PROG_NAME 
--        FROM FND_CONCURRENT_PROGRAMS_VL FNDC_PVL 
--        WHERE FNDC_PVL.CONCURRENT_PROGRAM_ID = PI_CONC_PROG_ID;
        SELECT FNDC_PVL.USER_CONCURRENT_PROGRAM_NAME 
        INTO V_USER_CONC_PROG_NAME 
        FROM FND_CONCURRENT_PROGRAMS_VL FNDC_PVL, 
                  FND_CONCURRENT_REQUESTS FNDC_REQ 
        WHERE FNDC_PVL.CONCURRENT_PROGRAM_ID = FNDC_REQ.CONCURRENT_PROGRAM_ID 
        AND FNDC_REQ.REQUEST_ID = PI_CONC_REQUEST_ID;
        
        RETURN V_USER_CONC_PROG_NAME;
    EXCEPTION
        WHEN TOO_MANY_ROWS THEN
            V_USER_ERROR := 'Found data too many rows.';
            P_WRITE_LOG(V_USER_ERROR);
            
            RETURN (NULL);
        WHEN OTHERS THEN
            V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
            P_WRITE_LOG(V_USER_ERROR);
            
            RETURN (NULL);
    END FN_GET_PROGRAM_NAME;
    
    PROCEDURE P_GET_DIRECTORIES ( 
                                                            PI_DIR_TYPE IN VARCHAR2, 
                                                            PI_DIR_NAME IN VARCHAR2 
                                                       )  IS
        
        V_DIR_PATH   DBA_DIRECTORIES.DIRECTORY_PATH%TYPE;
        
    BEGIN
        V_DIR_PATH := FN_GET_DIRECTORIES(PI_DIR_NAME);
        
        dbms_output.put_line('PARA_'||PI_DIR_TYPE||'_PATH='||V_DIR_PATH);
    END P_GET_DIRECTORIES;
    
--    PROCEDURE P_GET_PROCESS_DATE IS
--        
--        V_PROCESS_DATE   TIMESTAMP;
--        
--    BEGIN
--        V_PROCESS_DATE := FN_GET_PROCESS_DATE;
--            
--        dbms_output.put_line('PARA_PROCESS_DATE='||V_PROCESS_DATE);
--    END P_GET_PROCESS_DATE;
--    
--    PROCEDURE P_GET_PROCESS_TIME (
--                                                                PI_PROCESS_DATE IN TIMESTAMP
--                                                         ) IS
--        
--        V_PROCESS_TIME   TIMESTAMP;
--        
--    BEGIN
--        V_PROCESS_TIME := FN_GET_PROCESS_TIME(PI_PROCESS_DATE);
--            
--        dbms_output.put_line('PARA_PROCESS_DATE='||V_PROCESS_TIME);
--    END P_GET_PROCESS_TIME;
    
    FUNCTION FN_FIND_LOG002_SUMMARY ( 
                                                                    /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                                    PIO_ERRMSG IN OUT VARCHAR2, */
                                                                    PI_CONC_REQUEST_ID IN NUMBER/*, 
                                                                    PI_FILE_COUNT IN NUMBER, 
                                                                    PI_PROCESS_DATE IN TIMESTAMP */
                                                               ) RETURN NUMBER IS
        
        V_CONC_REQUEST_ID   NUMBER;
        V_FOUND   NUMBER;
        
        -- For write error log
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
        V_CONC_REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
        
        BEGIN
            SELECT 1 
            INTO V_FOUND 
            FROM DTTCNX002_LOG_SUMMARY 
            WHERE REQUEST_ID = PI_CONC_REQUEST_ID;
            
            RETURN (V_FOUND);
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                RETURN (NULL);
            WHEN OTHERS THEN
                V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
                P_WRITE_LOG(V_USER_ERROR);
                
                RETURN (NULL);
        END;
    END FN_FIND_LOG002_SUMMARY;
    
    PROCEDURE P_DELETE_LOG002_SUMMARY ( 
                                                                            /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                                            PIO_ERRMSG IN OUT VARCHAR2, */
                                                                            PI_CONC_REQUEST_ID IN NUMBER 
                                                                   ) IS
        
        V_CONC_REQUEST_ID   NUMBER;
--        V_FOUND   NUMBER;
        
        -- For write error log
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
        V_CONC_REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
        
        IF NVL(FN_FIND_LOG002_SUMMARY(PI_CONC_REQUEST_ID), 0) = 1 THEN
            BEGIN
                DELETE DTTCNX002_LOG_SUMMARY 
                WHERE REQUEST_ID = PI_CONC_REQUEST_ID;
                
                COMMIT;
            EXCEPTION
                WHEN OTHERS THEN
                    ROLLBACK;
                    
                    V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
                    P_WRITE_LOG(V_USER_ERROR);
            END;
        END IF;
    END P_DELETE_LOG002_SUMMARY;
    
    PROCEDURE P_INSERT_LOG002_SUMMARY ( 
                                                                        /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                                        PIO_ERRMSG IN OUT VARCHAR2, */
                                                                        PI_CONC_REQUEST_ID IN NUMBER/*, 
                                                                        PI_PROCESS_DATE IN TIMESTAMP */
                                                                   ) IS
        
        V_LEVEL_TYPE   VARCHAR2(1) := 'H';
        V_CONC_REQUEST_ID   NUMBER;
        V_CONC_PROG_ID   NUMBER;
        V_CONC_PROG_NAME   VARCHAR2(240);
        V_PROCESS_DATE   TIMESTAMP;
        V_PROCESS_TIME   TIMESTAMP;
        V_XML_FILE_COUNT   NUMBER := 0;
        
        -- For write error log
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
        V_CONC_REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
        V_CONC_PROG_ID := FND_GLOBAL.CONC_PROGRAM_ID;
        V_CONC_PROG_NAME := FN_GET_PROGRAM_NAME(PI_CONC_REQUEST_ID);
        V_PROCESS_DATE := FN_GET_PROCESS_DATE;
--        V_PROCESS_TIME := FN_GET_PROCESS_TIME(V_PROCESS_DATE);
        V_PROCESS_TIME := V_PROCESS_DATE;
        
        BEGIN
            /*INSERT INTO DTTCNX002_TEMP
                (
                        LEVEL_TYPE 
                )
            VALUES
                (
                        V_LEVEL_TYPE 
                );*/
            INSERT INTO DTTCNX002_LOG_SUMMARY
                (
                        LEVEL_TYPE, 
                        REQUEST_ID, 
                        PROGRAM_NAME, 
                        PROCESS_DATE, 
                        PROCESSING_TIME, 
                        XML_FILE_COUNT 
                )
            VALUES
                (
                        V_LEVEL_TYPE, 
                        PI_CONC_REQUEST_ID, 
                        V_CONC_PROG_NAME, 
                        V_PROCESS_DATE, 
                        V_PROCESS_TIME, 
                        V_XML_FILE_COUNT 
                );
            
            COMMIT;
        EXCEPTION
            WHEN OTHERS THEN
                ROLLBACK;
                
                V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
                P_WRITE_LOG(V_USER_ERROR);
        END;
    END P_INSERT_LOG002_SUMMARY;
    
    PROCEDURE P_UPDATE_LOG002_SUMMARY ( 
                                                                        /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                                        PIO_ERRMSG IN OUT VARCHAR2, */
                                                                        PI_CONC_REQUEST_ID IN NUMBER, 
                                                                        PI_FILE_COUNT IN NUMBER, 
                                                                        PI_PROCESS_TIME IN TIMESTAMP 
                                                                    ) IS
        
        V_CONC_REQUEST_ID   NUMBER;
--        V_PROCESS_DATE   TIMESTAMP;
--        V_PROCESS_TIME   TIMESTAMP;
        
        -- For write error log
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
        V_CONC_REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
--        V_PROCESS_TIME := FN_GET_PROCESS_TIME(PI_PROCESS_DATE);
        
        BEGIN
            UPDATE DTTCNX002_LOG_SUMMARY 
            SET PROCESSING_TIME = PI_PROCESS_TIME, 
                   XML_FILE_COUNT =  PI_FILE_COUNT 
            WHERE REQUEST_ID = PI_CONC_REQUEST_ID;
            
            COMMIT;
        EXCEPTION
            WHEN OTHERS THEN
                ROLLBACK;
                
                V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
                P_WRITE_LOG(V_USER_ERROR);
        END;
    END P_UPDATE_LOG002_SUMMARY;
    
    PROCEDURE P_INSERT_LOG002_DETAIL ( 
                                                                    /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                                    PIO_ERRMSG IN OUT VARCHAR2, */ 
                                                                    PI_CONC_REQUEST_ID IN NUMBER, 
                                                                    PI_FILE_COUNT IN NUMBER, 
                                                                    PI_ERROR_TYPE IN VARCHAR2, 
                                                                    PI_PATH_NAME IN VARCHAR2, 
                                                                    PI_FILE_NAME IN VARCHAR2,  
--                                                                    PI_PROCESS_DATE IN TIMESTAMP, 
                                                                    PI_STATUS IN VARCHAR2, 
                                                                    PI_ERRCODE IN VARCHAR2, 
                                                                    PI_ERRMSG IN VARCHAR2 
                                                               ) IS 
        
        V_LEVEL_TYPE   VARCHAR2(1) := 'D';
        V_CONC_REQUEST_ID   NUMBER;
        V_PROCESS_DATE   TIMESTAMP;
        V_PROCESS_TIME   TIMESTAMP;
--        V_FILE_COUNT   NUMBER := 0;
        
        -- For write error log
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
        V_CONC_REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
        
--        V_PROCESS_DATE := FN_GET_PROCESS_DATE;
        BEGIN
            SELECT PROCESS_DATE 
            INTO V_PROCESS_DATE 
            FROM DTTCNX002_LOG_SUMMARY dtt2
            WHERE dtt2.REQUEST_ID = PI_CONC_REQUEST_ID;
            
--            V_PROCESS_TIME := FN_GET_PROCESS_TIME(V_PROCESS_DATE);
            V_PROCESS_TIME := FN_GET_PROCESS_DATE;
            
            BEGIN
                INSERT INTO DTTCNX002_LOG_DETAIL
                    (
                            LEVEL_TYPE, 
                            REQUEST_ID, 
                            ERROR_TYPE, 
                            PATH_NAME, 
                            FILE_NAME, 
                            PROCESS_DATE, 
                            PROCESSING_TIME, 
                            STATUS, 
                            ERROR_CODE, 
                            ERROR_MESSAGE 
                    )
                VALUES
                    (
                            V_LEVEL_TYPE, 
                            PI_CONC_REQUEST_ID, 
                            PI_ERROR_TYPE, 
                            PI_PATH_NAME, 
                            PI_FILE_NAME, 
                            V_PROCESS_DATE, 
                            V_PROCESS_TIME, 
                            PI_STATUS, 
                            PI_ERRCODE, 
                            PI_ERRMSG  
                    );
                
                P_UPDATE_LOG002_SUMMARY ( 
                                                                    PI_CONC_REQUEST_ID, 
                                                                    PI_FILE_COUNT, 
                                                                    V_PROCESS_TIME 
                                                             );
                    
                COMMIT;
            EXCEPTION
                WHEN OTHERS THEN
                    ROLLBACK;
                    P_DELETE_LOG002_SUMMARY(PI_CONC_REQUEST_ID);
                    
                    V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
                    P_WRITE_LOG(V_USER_ERROR);
            END;
        EXCEPTION
            WHEN OTHERS THEN
                V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
                P_WRITE_LOG(V_USER_ERROR);
        END;
    END P_INSERT_LOG002_DETAIL;
    
--    PROCEDURE P_UPDATE_LOG002_DETAIL ( 
--                                                                    PI_PROCESS_DATE IN TIMESTAMP, 
--                                                                    PI_STATUS IN VARCHAR2, 
--                                                                    PI_ERRCODE IN VARCHAR2, 
--                                                                    PI_ERRMSG IN VARCHAR2 
--                                                               ) IS
--        
--        V_CONC_REQUEST_ID   NUMBER;
--        V_PROCESS_TIME   TIMESTAMP;
--        
--        -- For write error log
--        V_USER_ERROR   VARCHAR2(240);
--        
--    BEGIN
--        V_CONC_REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
--        V_PROCESS_TIME := FN_GET_PROCESS_TIME(PI_PROCESS_DATE);
--        
--        UPDATE DTTCNX002_LOG_DETAIL 
--        SET PROCESSING_TIME = V_PROCESS_TIME, 
--               STATUS =  PI_STATUS, 
--               ERROR_CODE = PI_ERRCODE, 
--               ERROR_MESSAGE = PI_ERRMSG 
--        WHERE REQUEST_ID = V_CONC_REQUEST_ID; 
--    EXCEPTION
--        WHEN OTHERS THEN
--            V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
--            P_WRITE_LOG(V_USER_ERROR);
--    END P_UPDATE_LOG002_DETAIL;
    
    FUNCTION FN_FIND_LOG003_SUMMARY ( 
                                                                    /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                                    PIO_ERRMSG IN OUT VARCHAR2, */
                                                                    PI_CONC_REQUEST_ID IN NUMBER/*, 
                                                                    PI_FILE_COUNT IN NUMBER, 
                                                                    PI_PROCESS_DATE IN TIMESTAMP */
                                                               ) RETURN NUMBER IS
        
        V_CONC_REQUEST_ID   NUMBER;
        V_FOUND   NUMBER;
        
        -- For write error log
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
        V_CONC_REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
        
        BEGIN
            SELECT 1 
            INTO V_FOUND 
            FROM DTTCNX003_LOG_SUMMARY 
            WHERE REQUEST_ID = PI_CONC_REQUEST_ID;
            
            RETURN (V_FOUND);
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                RETURN (NULL);
            WHEN OTHERS THEN
                V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
                P_WRITE_LOG(V_USER_ERROR);
                
                RETURN (NULL);
        END;
    END FN_FIND_LOG003_SUMMARY;
    
    PROCEDURE P_DELETE_LOG003_SUMMARY ( 
                                                                            /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                                            PIO_ERRMSG IN OUT VARCHAR2, */
                                                                            PI_CONC_REQUEST_ID IN NUMBER 
                                                                   ) IS
        
        V_CONC_REQUEST_ID   NUMBER;
--        V_FOUND   NUMBER;
        
        -- For write error log
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
        V_CONC_REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
        
        IF NVL(FN_FIND_LOG002_SUMMARY(PI_CONC_REQUEST_ID), 0) = 1 THEN
            BEGIN
                DELETE DTTCNX003_LOG_SUMMARY 
                WHERE REQUEST_ID = PI_CONC_REQUEST_ID;
                
                COMMIT;
            EXCEPTION
                WHEN OTHERS THEN
                    ROLLBACK;
                    
                    V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
                    P_WRITE_LOG(V_USER_ERROR);
            END;
        END IF;
    END P_DELETE_LOG003_SUMMARY;
    
    PROCEDURE P_INSERT_LOG003_SUMMARY ( 
                                                                        /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                                        PIO_ERRMSG IN OUT VARCHAR2, */
                                                                        PI_CONC_REQUEST_ID IN NUMBER, 
                                                                        PI_FILE_NAME IN VARCHAR2, 
                                                                        PI_PROCESS_DATE IN TIMESTAMP 
                                                                   ) IS
        
        V_LEVEL_TYPE   VARCHAR2(1) := 'H';
        V_CONC_REQUEST_ID   NUMBER;
        V_CONC_PROG_ID   NUMBER;
        V_CONC_PROG_NAME   VARCHAR2(240);
        V_PROCESS_DATE   TIMESTAMP;
        V_PROCESS_TIME   TIMESTAMP;
        
        -- For write error log
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
        V_CONC_REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
        V_CONC_PROG_ID := FND_GLOBAL.CONC_PROGRAM_ID;
        V_CONC_PROG_NAME := FN_GET_PROGRAM_NAME(PI_CONC_REQUEST_ID);
--        V_PROCESS_DATE := FN_GET_PROCESS_DATE;
--        V_PROCESS_TIME := FN_GET_PROCESS_TIME(V_PROCESS_DATE);
        V_PROCESS_TIME := PI_PROCESS_DATE;
        
       INSERT INTO DTTCNX003_LOG_SUMMARY
            (
                   LEVEL_TYPE, 
                   REQUEST_ID, 
                   FILE_NAME, 
                   PROGRAM_NAME, 
                   PROCESS_DATE, 
                   PROCESSING_TIME, 
                   RECORD_COUNT_COMPLETE, 
                   RECORD_COUNT_ERROR 
            )
        VALUES
            (
                   V_LEVEL_TYPE, 
                   PI_CONC_REQUEST_ID, 
                   PI_FILE_NAME, 
                   V_CONC_PROG_NAME, 
                   PI_PROCESS_DATE, 
                   V_PROCESS_TIME, 
                   0, 
                   0 
            );
        
        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            
            V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
            P_WRITE_LOG(V_USER_ERROR);
    END P_INSERT_LOG003_SUMMARY;
    
    PROCEDURE P_UPDATE_LOG003_SUMMARY ( 
                                                                        /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                                        PIO_ERRMSG IN OUT VARCHAR2, */ 
                                                                        PI_CONC_REQUEST_ID IN NUMBER, 
                                                                        PI_FILE_NAME IN VARCHAR2, 
--                                                                        PI_PROCESS_DATE IN TIMESTAMP, 
                                                                        PI_RECORD_COUNT_COMPLETE IN NUMBER, 
                                                                        PI_RECORD_COUNT_ERROR IN NUMBER 
                                                                    ) IS
        
        V_CONC_REQUEST_ID   NUMBER;
        V_PROCESS_DATE   TIMESTAMP;
        V_PROCESS_TIME   TIMESTAMP;
        
        -- For write error log
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
        V_CONC_REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
--        BEGIN
--            SELECT PROCESS_DATE 
--            INTO V_PROCESS_DATE 
--            FROM DTTCNX003_LOG_SUMMARY dtt3
--            WHERE dtt3.REQUEST_ID = PI_CONC_REQUEST_ID;
            
--            V_PROCESS_TIME := FN_GET_PROCESS_TIME(V_PROCESS_DATE);
            V_PROCESS_TIME := FN_GET_PROCESS_DATE;
            
            BEGIN
                UPDATE DTTCNX003_LOG_SUMMARY 
                SET PROCESSING_TIME = V_PROCESS_TIME, 
                       RECORD_COUNT_COMPLETE =  PI_RECORD_COUNT_COMPLETE, 
                       RECORD_COUNT_ERROR =  PI_RECORD_COUNT_ERROR 
                WHERE REQUEST_ID = PI_CONC_REQUEST_ID
                AND FILE_NAME = PI_FILE_NAME;
                    
                COMMIT; 
            EXCEPTION
                WHEN OTHERS THEN
                    ROLLBACK;
                        
                    V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
                    P_WRITE_LOG(V_USER_ERROR);
            END;
--        EXCEPTION
--            WHEN OTHERS THEN
--                V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
--                P_WRITE_LOG(V_USER_ERROR);
--        END;
    END P_UPDATE_LOG003_SUMMARY;
    
    PROCEDURE P_INSERT_LOG003_DETAIL ( 
                                                                    /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                                    PIO_ERRMSG IN OUT VARCHAR2, */ 
                                                                    PI_CONC_REQUEST_ID IN NUMBER, 
                                                                    PI_ERROR_TYPE IN VARCHAR2, 
                                                                    PI_FILE_NAME IN VARCHAR2, 
                                                                    PI_LINE_TYPE IN VARCHAR2, 
                                                                    PI_INVOICE_NO IN VARCHAR2, 
                                                                    PI_LINE_NUMBER IN NUMBER, 
                                                                    PI_CUSTOMER_NO IN VARCHAR2, 
                                                                    PI_ITEM IN VARCHAR2, 
                                                                    PI_AMOUNT IN NUMBER, 
                                                                    PI_PROCESS_DATE IN TIMESTAMP, 
                                                                    PI_STATUS IN VARCHAR2, 
                                                                    PI_ERRCODE IN VARCHAR2, 
                                                                    PI_ERRMSG IN VARCHAR2 
                                                               ) IS 
            
        V_LEVEL_TYPE   VARCHAR2(1) := 'D';
        V_CONC_REQUEST_ID   NUMBER;
        V_PROCESS_DATE   TIMESTAMP;
        V_PROCESS_TIME   TIMESTAMP;
            
        -- For write error log
        V_USER_ERROR   VARCHAR2(240);
            
    BEGIN
        V_CONC_REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
--        V_PROCESS_DATE := FN_GET_PROCESS_DATE;
--        V_PROCESS_TIME := FN_GET_PROCESS_TIME(V_PROCESS_DATE);
        V_PROCESS_TIME := PI_PROCESS_DATE;
        
        BEGIN
            INSERT INTO DTTCNX003_LOG_DETAIL
                (
                    LEVEL_TYPE, 
                    REQUEST_ID, 
                    ERROR_TYPE, 
                    FILE_NAME, 
                    LINE_TYPE, 
                    INVOICE_NO, 
                    LINE_NUMBER, 
                    CUSTOMER_NO, 
                    ITEM, 
                    AMOUNT, 
                    PROCESS_DATE, 
                    PROCESSING_TIME, 
                    STATUS, 
                    ERROR_CODE, 
                    ERROR_MESSAGE 
                )
            VALUES
                (
                    V_LEVEL_TYPE, 
                    PI_CONC_REQUEST_ID, 
                    PI_ERROR_TYPE, 
                    PI_FILE_NAME, 
                    PI_LINE_TYPE, 
                    PI_INVOICE_NO, 
                    PI_LINE_NUMBER, 
                    PI_CUSTOMER_NO, 
                    PI_ITEM, 
                    PI_AMOUNT, 
                    PI_PROCESS_DATE, 
                    V_PROCESS_TIME, 
                    PI_STATUS, 
                    PI_ERRCODE, 
                    PI_ERRMSG 
                );
           
           COMMIT; 
        EXCEPTION
            WHEN OTHERS THEN
                ROLLBACK;
                    
                V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
                P_WRITE_LOG(V_USER_ERROR);
        END;
    END P_INSERT_LOG003_DETAIL;
    
    PROCEDURE P_UPDATE_LOG003_DETAIL ( 
                                                                    PI_CONC_REQUEST_ID IN NUMBER, 
                                                                    PI_INVOICE_NO IN VARCHAR2, 
                                                                    PI_LINE_NUMBER IN NUMBER, 
--                                                                    PI_PROCESS_DATE IN TIMESTAMP, 
                                                                    PI_STATUS IN VARCHAR2, 
                                                                    PI_ERRCODE IN VARCHAR2, 
                                                                    PI_ERRMSG IN VARCHAR2 
                                                               ) IS
        
        V_CONC_REQUEST_ID   NUMBER;
        V_PROCESS_DATE   TIMESTAMP;
        V_PROCESS_TIME   TIMESTAMP;
        
        -- For write error log
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
        V_CONC_REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
        
        BEGIN
--            SELECT PROCESS_DATE 
--            INTO V_PROCESS_DATE 
--            FROM DTTCNX003_LOG_DETAIL dtt3 
--            WHERE dtt3.REQUEST_ID = PI_CONC_REQUEST_ID 
--            AND dtt3.LINE_NUMBER = PI_LINE_NUMBER;
            
--            V_PROCESS_TIME := FN_GET_PROCESS_TIME(V_PROCESS_DATE);
            V_PROCESS_TIME := FN_GET_PROCESS_DATE;
            
            BEGIN
                UPDATE DTTCNX003_LOG_DETAIL 
                SET PROCESSING_TIME = V_PROCESS_TIME, 
                       STATUS =  PI_STATUS, 
                       ERROR_CODE = PI_ERRCODE, 
                       ERROR_MESSAGE = PI_ERRMSG 
                WHERE REQUEST_ID = PI_CONC_REQUEST_ID 
                AND INVOICE_NO = PI_INVOICE_NO 
                AND LINE_NUMBER = PI_LINE_NUMBER;
                
               COMMIT; 
            EXCEPTION
                WHEN OTHERS THEN
                    ROLLBACK;
                    
                    V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
                    P_WRITE_LOG(V_USER_ERROR);
            END;
        EXCEPTION
            WHEN OTHERS THEN
                V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
                P_WRITE_LOG(V_USER_ERROR);
        END;
    END P_UPDATE_LOG003_DETAIL;
    
    FUNCTION FN_FIND_LOG004_SUMMARY ( 
                                                                    /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                                    PIO_ERRMSG IN OUT VARCHAR2, */
                                                                    PI_CONC_REQUEST_ID IN NUMBER/*, 
                                                                    PI_FILE_COUNT IN NUMBER, 
                                                                    PI_PROCESS_DATE IN TIMESTAMP */
                                                               ) RETURN NUMBER IS
        
        V_CONC_REQUEST_ID   NUMBER;
        V_FOUND   NUMBER;
        
        -- For write error log
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
        V_CONC_REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
        
        BEGIN
            SELECT 1 
            INTO V_FOUND 
            FROM DTTCNX004_LOG_SUMMARY 
            WHERE REQUEST_ID = PI_CONC_REQUEST_ID;
            
            RETURN (V_FOUND);
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                RETURN (NULL);
            WHEN OTHERS THEN
                V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
                P_WRITE_LOG(V_USER_ERROR);
                
                RETURN (NULL);
        END;
    END FN_FIND_LOG004_SUMMARY;
    
    PROCEDURE P_DELETE_LOG004_SUMMARY ( 
                                                                            /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                                            PIO_ERRMSG IN OUT VARCHAR2, */
                                                                            PI_CONC_REQUEST_ID IN NUMBER 
                                                                   ) IS
        
        V_CONC_REQUEST_ID   NUMBER;
--        V_FOUND   NUMBER;
        
        -- For write error log
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
        V_CONC_REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
        
        IF NVL(FN_FIND_LOG004_SUMMARY(PI_CONC_REQUEST_ID), 0) = 1 THEN
            BEGIN
                DELETE DTTCNX004_LOG_SUMMARY 
                WHERE REQUEST_ID = PI_CONC_REQUEST_ID;
                
                COMMIT;
            EXCEPTION
                WHEN OTHERS THEN
                    ROLLBACK;
                    
                    V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
                    P_WRITE_LOG(V_USER_ERROR);
            END;
        END IF;
    END P_DELETE_LOG004_SUMMARY;
    
    PROCEDURE P_INSERT_LOG004_SUMMARY ( 
                                                                        /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                                        PIO_ERRMSG IN OUT VARCHAR2, */
                                                                        PI_CONC_REQUEST_ID IN NUMBER/*, 
                                                                        PI_PROCESS_DATE IN TIMESTAMP */
                                                                   ) IS
        
        V_LEVEL_TYPE   VARCHAR2(1) := 'H';
        V_CONC_REQUEST_ID   NUMBER;
        V_CONC_PROG_ID   NUMBER;
        V_CONC_PROG_NAME   VARCHAR2(240);
        V_PROCESS_DATE   TIMESTAMP;
        V_PROCESS_TIME   TIMESTAMP;
        V_OUTPUT_FILE_COUNT   NUMBER := 0;
        
        -- For write error log
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
        V_CONC_REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
        V_CONC_PROG_ID := FND_GLOBAL.CONC_PROGRAM_ID;
        V_CONC_PROG_NAME := FN_GET_PROGRAM_NAME(PI_CONC_REQUEST_ID);
        V_PROCESS_DATE := FN_GET_PROCESS_DATE;
--        V_PROCESS_TIME := FN_GET_PROCESS_TIME(V_PROCESS_DATE);
        V_PROCESS_TIME := V_PROCESS_DATE;
        
        INSERT INTO DTTCNX004_LOG_SUMMARY
            (
                    LEVEL_TYPE, 
                    REQUEST_ID, 
                    PROGRAM_NAME, 
                    PROCESS_DATE, 
                    PROCESSING_TIME, 
                    OUTPUT_FILE_COUNT 
            )
        VALUES
            (
                    V_LEVEL_TYPE, 
                    PI_CONC_REQUEST_ID, 
                    V_CONC_PROG_NAME, 
                    V_PROCESS_DATE, 
                    V_PROCESS_TIME, 
                    V_OUTPUT_FILE_COUNT 
            );
                    
        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            
            V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
            P_WRITE_LOG(V_USER_ERROR);
    END P_INSERT_LOG004_SUMMARY;
    
    PROCEDURE P_UPDATE_LOG004_SUMMARY ( 
                                                                        /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                                        PIO_ERRMSG IN OUT VARCHAR2, */ 
                                                                        PI_CONC_REQUEST_ID IN NUMBER, 
                                                                        PI_FILE_COUNT IN NUMBER, 
                                                                        PI_PROCESS_TIME IN TIMESTAMP 
                                                                    ) IS
        
        V_CONC_REQUEST_ID   NUMBER;
--        V_PROCESS_DATE   TIMESTAMP;
--        V_PROCESS_TIME   TIMESTAMP;
        
        -- For write error log
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
        V_CONC_REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
--        V_PROCESS_TIME := FN_GET_PROCESS_TIME(PI_PROCESS_DATE);
        
        UPDATE DTTCNX004_LOG_SUMMARY 
        SET PROCESSING_TIME = PI_PROCESS_TIME, 
               OUTPUT_FILE_COUNT = PI_FILE_COUNT 
        WHERE REQUEST_ID = PI_CONC_REQUEST_ID;
        
        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            
            V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
            P_WRITE_LOG(V_USER_ERROR);
    END P_UPDATE_LOG004_SUMMARY;
    
    PROCEDURE P_INSERT_LOG004_DETAIL ( 
                                                                    /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                                    PIO_ERRMSG IN OUT VARCHAR2, */ 
                                                                    PI_CONC_REQUEST_ID IN NUMBER, 
                                                                    PI_FILE_COUNT IN NUMBER, 
                                                                    PI_ERROR_TYPE IN VARCHAR2, 
                                                                    PI_PATH_NAME IN VARCHAR2, 
                                                                    PI_FILE_NAME IN VARCHAR2,  
--                                                                    PI_PROCESS_DATE IN TIMESTAMP, 
                                                                    PI_STATUS IN VARCHAR2, 
                                                                    PI_ERRCODE IN VARCHAR2, 
                                                                    PI_ERRMSG IN VARCHAR2 
                                                               ) IS 
        
        V_LEVEL_TYPE   VARCHAR2(1) := 'D';
        V_CONC_REQUEST_ID   NUMBER;
        V_PROCESS_DATE   TIMESTAMP;
        V_PROCESS_TIME   TIMESTAMP;
        
        -- For write error log
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
        V_CONC_REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
        
--        V_PROCESS_DATE := FN_GET_PROCESS_DATE;
        BEGIN
            SELECT PROCESS_DATE 
            INTO V_PROCESS_DATE 
            FROM DTTCNX004_LOG_SUMMARY dtt4
            WHERE dtt4.REQUEST_ID = PI_CONC_REQUEST_ID;
            
--            V_PROCESS_TIME := FN_GET_PROCESS_TIME(V_PROCESS_DATE);
            V_PROCESS_TIME := FN_GET_PROCESS_DATE;
            
            BEGIN
                INSERT INTO DTTCNX004_LOG_DETAIL
                    (
                            LEVEL_TYPE, 
                            REQUEST_ID, 
                            ERROR_TYPE, 
                            PATH_NAME, 
                            FILE_NAME, 
                            PROCESS_DATE, 
                            PROCESSING_TIME, 
                            STATUS, 
                            ERROR_CODE, 
                            ERROR_MESSAGE 
                    )
                VALUES
                    (
                            V_LEVEL_TYPE, 
                            PI_CONC_REQUEST_ID, 
                            PI_ERROR_TYPE, 
                            PI_PATH_NAME, 
                            PI_FILE_NAME, 
                            V_PROCESS_DATE, 
                            V_PROCESS_TIME, 
                            PI_STATUS, 
                            PI_ERRCODE, 
                            PI_ERRMSG 
                    );
                                        
                COMMIT;
                P_UPDATE_LOG004_SUMMARY ( 
                                                                    PI_CONC_REQUEST_ID, 
                                                                    PI_FILE_COUNT, 
                                                                    V_PROCESS_TIME 
                                                             );
            EXCEPTION
                WHEN OTHERS THEN
                    ROLLBACK;
                    P_DELETE_LOG004_SUMMARY(PI_CONC_REQUEST_ID);
                    
                    V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
                    P_WRITE_LOG(V_USER_ERROR);
            END;
        EXCEPTION
            WHEN OTHERS THEN
                V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
                P_WRITE_LOG(V_USER_ERROR);
        END;
    END P_INSERT_LOG004_DETAIL;
    
--    PROCEDURE P_UPDATE_LOG004_DETAIL ( 
--                                                                    PI_PROCESS_DATE IN TIMESTAMP, 
--                                                                    PI_STATUS IN VARCHAR2, 
--                                                                    PI_ERRCODE IN VARCHAR2, 
--                                                                    PI_ERRMSG IN VARCHAR2 
--                                                               ) IS
--        
--        V_CONC_REQUEST_ID   NUMBER;
--        V_PROCESS_TIME   TIMESTAMP;
--        
--        -- For write error log
--        V_USER_ERROR   VARCHAR2(240);
--        
--    BEGIN
--        V_CONC_REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
--        V_PROCESS_TIME := FN_GET_PROCESS_TIME(PI_PROCESS_DATE);
--        
--        UPDATE DTTCNX004_LOG_DETAIL 
--        SET PROCESSING_TIME = V_PROCESS_TIME, 
--               STATUS =  PI_STATUS, 
--               ERROR_CODE = PI_ERRCODE, 
--               ERROR_MESSAGE = PI_ERRMSG 
--        WHERE REQUEST_ID = V_CONC_REQUEST_ID; 
--    EXCEPTION
--        WHEN OTHERS THEN
--            V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
--            P_WRITE_LOG(V_USER_ERROR);
--    END P_UPDATE_LOG004_DETAIL;
--    
--    FUNCTION FN_FOUND_TEMP_DATA ( 
--                                                            /*PIO_ERRCODE IN OUT VARCHAR2, 
--                                                            PIO_ERRMSG IN OUT VARCHAR2, */ 
--                                                            PI_CONC_REQUEST_ID IN NUMBER, 
--                                                            PI_CUSTOMER_NO IN VARCHAR2, 
--                                                            PI_MEMO_LINE IN VARCHAR2, 
--                                                            PI_TRX_DATE IN DATE
--                                                        ) RETURN BOOLEAN IS 
--        
--        V_FOUND_DATA   NUMBER;
--        
--        -- For write error log
--        V_USER_ERROR   VARCHAR2(240);
--        
--    BEGIN
--        SELECT DISTINCT 1 
--        INTO V_FOUND_DATA 
--        FROM TAC_RA_INTERFACE_LINES_ALL TAC_RAIL
--        WHERE TAC_RAIL.INTERFACE_LINE_ATTRIBUTE1 = PI_CUSTOMER_NO 
--        AND TAC_RAIL.MTL_SYSTEM_ITEMS_SEG1 = PI_MEMO_LINE 
--        AND TAC_RAIL.TRX_DATE = PI_TRX_DATE
--        AND TAC_RAIL.REQUEST_ID = PI_CONC_REQUEST_ID;
--        
--        RETURN(TRUE);
--    EXCEPTION
--        WHEN NO_DATA_FOUND THEN
--            RETURN(FALSE);
--        WHEN OTHERS THEN
--            V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
--            P_WRITE_LOG(V_USER_ERROR);
--            
--            RETURN(TRUE);
--    END FN_FOUND_TEMP_DATA;
    
    PROCEDURE P_INSERT_TEMP ( 
                                                    PIO_ERRCODE IN OUT VARCHAR2, 
                                                    PIO_ERRMSG IN OUT VARCHAR2, 
                                                    PIO_FLAG IN OUT NUMBER, 
                                                    PI_CONC_REQUEST_ID IN NUMBER, 
                                                    PI_BATCH_SOURCE_NAME IN VARCHAR2, 
                                                    PI_SET_OF_BOOKS_ID IN NUMBER, 
                                                    PI_LINE_TYPE IN VARCHAR2, 
                                                    PI_INF_LINE_CONTEXT IN VARCHAR2, 
                                                    PI_CUST_NO IN VARCHAR2, 
                                                    PI_INVOICE_NO IN VARCHAR2, 
                                                    PI_COMMENTS IN VARCHAR2, 
                                                    PI_CURRENCY_CODE IN VARCHAR2, 
                                                    PI_CONVERSION_DATE IN DATE, 
                                                    PI_CONVERSION_RATE IN NUMBER, 
                                                    PI_CONVERSION_TYPE IN VARCHAR2, 
                                                    PI_AMOUNT IN NUMBER, 
                                                    PI_CUST_TRX_TYPE IN VARCHAR2, 
                                                    PI_TERM_NAME IN VARCHAR2, 
                                                    PI_CUST_BILL_ADDR IN VARCHAR2, 
                                                    PI_CUST_SHIP_ADDR IN VARCHAR2, 
                                                    PI_TRX_DATE IN DATE, 
                                                    PI_GL_DATE IN DATE, 
                                                    PI_LINE_NUMBER IN NUMBER, 
                                                    PI_MEMO_LINE IN VARCHAR2, 
                                                    PI_DESCRIPTION IN VARCHAR2, 
                                                    PI_QUANTITY IN NUMBER, 
                                                    PI_UNIT_SELLING_PRICE IN NUMBER, 
                                                    PI_TAX_CODE IN VARCHAR2, 
                                                    PI_UOM_CODE IN VARCHAR2, 
                                                    PI_SALESREP_NUMBER IN VARCHAR2, 
                                                    PI_STATUS IN VARCHAR2 
                                                ) IS 
        
        V_CONC_REQUEST_ID   NUMBER;
        
        -- For write error log
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
        V_CONC_REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
        
        INSERT INTO TAC_RA_INTERFACE_LINES_ALL
            (
                BATCH_SOURCE_NAME, 
                SET_OF_BOOKS_ID, 
                LINE_TYPE, 
                INTERFACE_LINE_CONTEXT, 
                INTERFACE_LINE_ATTRIBUTE1, 
                INTERFACE_LINE_ATTRIBUTE2, 
                COMMENTS, 
                CURRENCY_CODE, 
                CONVERSION_DATE, 
                CONVERSION_RATE, 
                CONVERSION_TYPE, 
                AMOUNT, 
                CUST_TRX_TYPE_NAME, 
                TERM_NAME, 
                ORIG_SYSTEM_BILL_CUSTOMER_REF, 
                ORIG_SYSTEM_BILL_ADDRESS_REF, 
                ORIG_SYSTEM_SHIP_CUSTOMER_REF, 
                ORIG_SYSTEM_SHIP_ADDRESS_REF, 
                TRX_DATE, 
                GL_DATE, 
                LINE_NUMBER, 
                MTL_SYSTEM_ITEMS_SEG1, 
                DESCRIPTION, 
                QUANTITY, 
                UNIT_SELLING_PRICE, 
                TAX_CODE, 
                UOM_CODE, 
                PRIMARY_SALESREP_NUMBER, 
                STATUS, 
                REQUEST_ID,
                CREATED_BY, 
                CREATION_DATE 
            )
        VALUES
            (
                PI_BATCH_SOURCE_NAME, 
                PI_SET_OF_BOOKS_ID, 
                PI_LINE_TYPE, 
                PI_INF_LINE_CONTEXT, 
                PI_INVOICE_NO, 
                PI_LINE_NUMBER, 
                PI_COMMENTS, 
                PI_CURRENCY_CODE, 
                PI_CONVERSION_DATE, 
                PI_CONVERSION_RATE, 
                PI_CONVERSION_TYPE, 
                PI_AMOUNT, 
                PI_CUST_TRX_TYPE, 
                PI_TERM_NAME, 
                PI_CUST_NO, 
                PI_CUST_BILL_ADDR, 
                PI_CUST_NO, 
                PI_CUST_SHIP_ADDR, 
                PI_TRX_DATE, 
                PI_GL_DATE, 
                PI_LINE_NUMBER, 
                PI_MEMO_LINE, 
                PI_DESCRIPTION, 
                PI_QUANTITY, 
                PI_UNIT_SELLING_PRICE, 
                PI_TAX_CODE, 
                PI_UOM_CODE, 
                PI_SALESREP_NUMBER, 
                PI_STATUS, 
                PI_CONC_REQUEST_ID,
                0, 
                SYSDATE 
            );
        
        PIO_FLAG := 0;
        PIO_ERRCODE := NULL;
        PIO_ERRMSG := NULL;
        
        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            
            PIO_FLAG := 1;
            PIO_ERRCODE := SUBSTR(SQLCODE, 50);
            PIO_ERRMSG := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
            V_USER_ERROR := PIO_ERRMSG;
            P_WRITE_LOG(V_USER_ERROR);
    END P_INSERT_TEMP;
    
--    PROCEDURE P_UPDATE_TEMP ( 
--                                                    /*PIO_ERRCODE IN OUT VARCHAR2, 
--                                                    PIO_ERRMSG IN OUT VARCHAR2, */ 
--                                                    PI_CONC_REQUEST_ID IN NUMBER, 
--                                                    PI_CUSTOMER_NO IN VARCHAR2, 
--                                                    PI_MEMO_LINE IN VARCHAR2, 
--                                                    PI_AMOUNT IN NUMBER, 
--                                                    PI_TRX_DATE IN DATE 
--                                                ) IS 
--        
--        -- For write error log
--        V_USER_ERROR   VARCHAR2(240);
--            
--    BEGIN
--        UPDATE TAC_RA_INTERFACE_LINES_ALL 
--        SET AMOUNT = NVL(AMOUNT, 0) + PI_AMOUNT 
--        WHERE INTERFACE_LINE_ATTRIBUTE1 = PI_CUSTOMER_NO 
--        AND MTL_SYSTEM_ITEMS_SEG1 = PI_MEMO_LINE 
--        AND TRX_DATE = PI_TRX_DATE
--        AND REQUEST_ID = PI_CONC_REQUEST_ID;
--        
--        COMMIT;
--    EXCEPTION
--        WHEN OTHERS THEN
--            ROLLBACK;
--            
--            V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
--            P_WRITE_LOG(V_USER_ERROR);
--    END P_UPDATE_TEMP;
--    
    PROCEDURE P_READ_AND_INSERT ( 
                                                            /*PIO_ERRCODE IN OUT VARCHAR2, 
                                                            PIO_ERRMSG IN OUT VARCHAR2, */
                                                            PI_CONC_REQUEST_ID IN NUMBER, 
                                                            PI_MONTH IN VARCHAR2, 
                                                            PI_YEAR IN NUMBER, 
                                                            PI_BATCH_SOURCE_NAME IN VARCHAR2, 
                                                            PI_INF_LINE_CONTEXT IN VARCHAR2, 
                                                            PI_CUST_TRX_TYPE IN VARCHAR2, 
                                                            PI_TERM_NAME IN VARCHAR2, 
                                                            PI_FILE_NAME IN VARCHAR2, 
                                                            PI_DIR_NAME IN VARCHAR2 
                                                       ) IS
        
        -- For read data from XML file
        V_DIR_PATH   DBA_DIRECTORIES.DIRECTORY_PATH%TYPE;
        V_XML_DATA   XMLType;
        
        -- For Insert data to 'TAC_RA_INTERFACE_LINES_ALL'
        V_SET_OF_BOOK   NUMBER(15) := 1008;
        V_LINE_TYPE   VARCHAR2(20) := 'LINE';
        V_INVOICE_NO   VARCHAR2(50);
        V_FIN_CUST_NO   VARCHAR2(50);
        V_COMMENTS   VARCHAR2(50) := NULL;
        V_CURR_CODE   VARCHAR2(15) := 'THB';
        V_CONV_DATE   DATE := NULL;
        V_CONV_RATE   NUMBER := 1;
        V_CONV_TYPE   VARCHAR2(30) := 'User';
        V_ITEM   VARCHAR(40) := NULL;
        V_SMS_AMOUNT   NUMBER := 0;
        V_DATA_AMOUNT   NUMBER := 0;
        V_OTH_AMOUNT   NUMBER := 0;
        V_UNIT_SELLING_PRICE   NUMBER := 0;
        V_CUST_BILL_ADDR   VARCHAR2(20) := 'N3_B1';
        V_CUST_SHIP_ADDR   VARCHAR2(50) := 'N3_S1';
        V_TRX_DATE   DATE := NULL;
        V_GL_DATE   DATE := NULL;
        V_LINE_NUM   NUMBER := NULL;
        V_MEMO_LINE   VARCHAR2(40) := NULL;
        V_DESCRIPTION   VARCHAR2(40) := NULL;
        V_QUANTITY   NUMBER := 1;
        V_TAX_CODE   VARCHAR2(50) := 'I-SVAT';
        V_UOM_CODE   VARCHAR2(3) := 'EA';
        V_SALESREP_NUMBER   VARCHAR2(20) := '-3';
        V_TEMP_STATUS   VARCHAR2(20) := 'NEW';
        V_INSERT_FLAG   NUMBER;
        V_SMS_FLAG   BOOLEAN := FALSE;
        V_DATA_FLAG   BOOLEAN := FALSE;
        V_OTH_FLAG   BOOLEAN := FALSE;
        V_LESS_ZERO_FLAG   BOOLEAN := FALSE;
        V_NO_ITEM_FLAG   BOOLEAN := TRUE;
        
        -- For write program error log
        V_CONC_REQUEST_ID   NUMBER;
        V_ERROR_TYPE   VARCHAR2(50) := 'Insert data';
        V_PROCESS_DATE_SUM   DATE := NULL;
        V_PROCESS_DATE_DET   DATE := NULL;
        V_RECORD_COUNT_COMPLETE   NUMBER := 0;
        V_RECORD_COUNT_ERROR   NUMBER := 0;
        V_LOG_STATUS   VARCHAR2(20) := 'Complete';
        V_ERR_CODE   DTTCNX003_LOG_DETAIL.ERROR_CODE%TYPE;
        V_ERR_MSG   DTTCNX003_LOG_DETAIL.ERROR_MESSAGE%TYPE;
        
        -- For write error handling log
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
        V_CONC_REQUEST_ID := FND_GLOBAL.CONC_REQUEST_ID;
        V_DIR_PATH := FN_GET_DIRECTORIES(PI_DIR_NAME);
        V_TRX_DATE := TO_DATE('05/'||PI_MONTH||'/'||TO_CHAR(PI_YEAR, '9999'), 'DD/MM/YYYY');
        V_GL_DATE := V_TRX_DATE;
        V_PROCESS_DATE_SUM := FN_GET_PROCESS_DATE;
        
        P_INSERT_LOG003_SUMMARY ( 
                                                            PI_CONC_REQUEST_ID, 
                                                            PI_FILE_NAME, 
                                                            V_PROCESS_DATE_SUM 
                                                    );
        
        dbms_output.put_line('FILE NAME='||PI_FILE_NAME);
        dbms_output.put_line('DIRECTORY NAME='||PI_DIR_NAME);
        dbms_output.put_line('-----------------------------------------------------');
        
        BEGIN
            SELECT XMLTYPE ( 
                                            BFILENAME (PI_DIR_NAME, PI_FILE_NAME), 
                                            NLS_CHARSET_ID ('UTF8')
                                       ) XML_DATA 
            INTO V_XML_DATA 
            FROM DUAL;
            
            BEGIN
                SELECT EXTRACTVALUE(VALUE(INV_INFO),'/InvoiceInfo/InvoiceNo/text()') AS INVOICE_NO 
                INTO V_INVOICE_NO 
                FROM TABLE(XMLSEQUENCE(EXTRACT(V_XML_DATA,'/Invoice/InvoiceInfo'))) INV_INFO;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    V_INVOICE_NO := NULL;
                WHEN OTHERS THEN
                    V_INVOICE_NO := NULL;
                    
                    V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
                    P_WRITE_LOG(V_USER_ERROR);
            END;
            
            dbms_output.put_line('INVOICE NO.='||V_INVOICE_NO);
            dbms_output.put_line('-----------------------------------------------------');
            
            BEGIN
                SELECT EXTRACTVALUE(VALUE(CUST_INFO),'/CustomerInfo/FinancialCustomerNo/text()') AS FIN_CUST_NO 
                INTO V_FIN_CUST_NO 
                FROM TABLE(XMLSEQUENCE(EXTRACT(V_XML_DATA,'/Invoice/CustomerInfo'))) CUST_INFO;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    V_FIN_CUST_NO := NULL;
                WHEN OTHERS THEN
                    V_FIN_CUST_NO := NULL;
                    
                    V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
                    P_WRITE_LOG(V_USER_ERROR);
            END;
            
            dbms_output.put_line('CUSTOMER NO.='||V_FIN_CUST_NO);
            dbms_output.put_line('-----------------------------------------------------');
            
            IF (V_FIN_CUST_NO IS NULL) OR (V_FIN_CUST_NO = '') THEN
                V_LINE_NUM := 1;
                V_ITEM := NULL;
                V_SMS_AMOUNT := NULL;
                V_PROCESS_DATE_DET := V_PROCESS_DATE_SUM;
                V_LOG_STATUS := 'Warning';
                V_ERR_CODE := 'DTTCNX001-009';
                V_ERR_MSG := 'Financial Customer No is blank value (Not insert data to "Temp table").';
                V_NO_ITEM_FLAG := FALSE;
                
                dbms_output.put_line('ERROR MESSAGE='||V_ERR_MSG);
                dbms_output.put_line('-----------------------------------------------------');
                
                P_INSERT_LOG003_DETAIL ( 
                                                            PI_CONC_REQUEST_ID, 
                                                            V_ERROR_TYPE, 
                                                            PI_FILE_NAME, 
                                                            V_LINE_TYPE, 
                                                            V_INVOICE_NO, 
                                                            V_LINE_NUM, 
                                                            V_FIN_CUST_NO, 
                                                            V_ITEM, 
                                                            V_SMS_AMOUNT, 
                                                            V_PROCESS_DATE_DET, 
                                                            V_LOG_STATUS, 
                                                            V_ERR_CODE, 
                                                            V_ERR_MSG 
                                                        );
                
                V_RECORD_COUNT_ERROR := 1;
                
                P_UPDATE_LOG003_DETAIL ( 
                                                            PI_CONC_REQUEST_ID, 
                                                            V_INVOICE_NO, 
                                                            V_LINE_NUM, 
--                                                            V_PROCESS_DATE_DET, 
                                                            V_LOG_STATUS, 
                                                            V_ERR_CODE, 
                                                            V_ERR_MSG 
                                                        );
                
--                P_UPDATE_LOG003_SUMMARY (  
--                                                                    PI_CONC_REQUEST_ID, 
--                                                                    PI_FILE_NAME, 
----                                                                    V_PROCESS_DATE_SUM, 
--                                                                    V_RECORD_COUNT_COMPLETE, 
--                                                                    V_RECORD_COUNT_ERROR 
--                                                              );
            ELSE
                FOR l_item IN ( 
                                        SELECT EXTRACTVALUE(VALUE(ITEM_INFO),'/Item/Name/text()') AS ITEM_NAME, 
                                                    TO_NUMBER(EXTRACTVALUE(VALUE(ITEM_INFO),'/Item/NetAmount/text()')) AS NET_AMOUNT 
                                        FROM TABLE(XMLSEQUENCE(EXTRACT(V_XML_DATA,'/Invoice/ItemsInfo/Item'))) ITEM_INFO 
                                     ) 
                LOOP
                    dbms_output.put_line('ITEM NAME='||l_item.item_name);
                    dbms_output.put_line('NET AMOUNT='||l_item.net_amount);
                    dbms_output.put_line('-----------------------------------------------------');
                    
                    V_NO_ITEM_FLAG := FALSE;
                    
                    IF NVL(l_item.NET_AMOUNT, 0) < 0 THEN
                        V_LESS_ZERO_FLAG := TRUE;
                    END IF;
                    
                    IF l_item.item_name LIKE 'GPRS%' THEN
                        V_DATA_FLAG := TRUE;
                        V_DATA_AMOUNT := V_DATA_AMOUNT + NVL(l_item.NET_AMOUNT, 0);
                    ELSIF l_item.item_name LIKE 'SMS%' THEN
                        V_SMS_FLAG := TRUE;
                        V_SMS_AMOUNT := V_SMS_AMOUNT + NVL(l_item.NET_AMOUNT, 0);
                    ELSE
                        V_OTH_FLAG := TRUE;
                        V_OTH_AMOUNT := V_OTH_AMOUNT + NVL(l_item.NET_AMOUNT, 0);
                    END IF;
                END LOOP;
                
                dbms_output.put_line('SMS AMOUNT='||V_SMS_AMOUNT);
                dbms_output.put_line('DATA AMOUNT='||V_DATA_AMOUNT);
                dbms_output.put_line('OTHER AMOUNT='||V_OTH_AMOUNT);
                dbms_output.put_line('-----------------------------------------------------');
                
                IF (V_LESS_ZERO_FLAG = FALSE) THEN
                
                    IF (V_SMS_FLAG = TRUE) Then
                        V_LINE_NUM := 1;
                        V_PROCESS_DATE_DET := V_PROCESS_DATE_SUM;
                        V_MEMO_LINE := 'SMS Service';
                        V_DESCRIPTION := V_MEMO_LINE;
                        V_ITEM := V_MEMO_LINE;
                        
                        dbms_output.put_line('SMS FLAG=TRUE');
                        dbms_output.put_line('-----------------------------------------------------');
                        
                        -- Insert SMS Service
--                        IF V_SMS_AMOUNT >= 0 THEN
                            V_SMS_AMOUNT := ROUND(V_SMS_AMOUNT, 2);
                            
                            dbms_output.put_line('SMS AMOUNT='||V_SMS_AMOUNT);
                            dbms_output.put_line('-----------------------------------------------------');
                            
                            V_UNIT_SELLING_PRICE := V_SMS_AMOUNT;
                            V_RECORD_COUNT_COMPLETE := V_RECORD_COUNT_COMPLETE + 1;
                            
                            P_INSERT_LOG003_DETAIL ( 
                                                                        PI_CONC_REQUEST_ID, 
                                                                        V_ERROR_TYPE, 
                                                                        PI_FILE_NAME, 
                                                                        V_LINE_TYPE, 
                                                                        V_INVOICE_NO, 
                                                                        V_LINE_NUM, 
                                                                        V_FIN_CUST_NO, 
                                                                        V_ITEM, 
                                                                        V_SMS_AMOUNT, 
                                                                        V_PROCESS_DATE_DET, 
                                                                        NULL, 
                                                                        NULL, 
                                                                        NULL 
                                                                    );
                            
                            P_INSERT_TEMP ( 
                                                        V_ERR_CODE, 
                                                        V_ERR_MSG, 
                                                        V_INSERT_FLAG, 
                                                        V_CONC_REQUEST_ID, 
                                                        PI_BATCH_SOURCE_NAME, 
                                                        V_SET_OF_BOOK, 
                                                        V_LINE_TYPE, 
                                                        PI_INF_LINE_CONTEXT, 
                                                        V_FIN_CUST_NO, 
                                                        V_INVOICE_NO, 
                                                        V_COMMENTS, 
                                                        V_CURR_CODE, 
                                                        V_CONV_DATE, 
                                                        V_CONV_RATE, 
                                                        V_CONV_TYPE, 
                                                        V_SMS_AMOUNT, 
                                                        PI_CUST_TRX_TYPE, 
                                                        PI_TERM_NAME, 
                                                        V_CUST_BILL_ADDR, 
                                                        V_CUST_SHIP_ADDR, 
                                                        V_TRX_DATE, 
                                                        V_GL_DATE, 
                                                        V_LINE_NUM, 
                                                        V_MEMO_LINE, 
                                                        V_DESCRIPTION, 
                                                        V_QUANTITY, 
                                                        V_UNIT_SELLING_PRICE, 
                                                        V_TAX_CODE, 
                                                        V_UOM_CODE, 
                                                        V_SALESREP_NUMBER, 
                                                        V_TEMP_STATUS 
                                                    );
                            
                            IF V_INSERT_FLAG = 0 THEN
                                V_LOG_STATUS := 'Completed';
                            ELSE
                                V_LOG_STATUS := 'Error';
                            END IF;
                            
                            P_UPDATE_LOG003_DETAIL ( 
                                                                        PI_CONC_REQUEST_ID, 
                                                                        V_INVOICE_NO, 
                                                                        V_LINE_NUM, 
--                                                                        V_PROCESS_DATE_DET, 
                                                                        V_LOG_STATUS, 
                                                                        V_ERR_CODE, 
                                                                        V_ERR_MSG 
                                                                    );
                        /*ELSE
--                            V_INVOICE_NO := NULL;
--                            V_SMS_AMOUNT := NULL;
                            V_LOG_STATUS := 'Warning';
                            V_ERR_CODE := 'DTTCNX001-010';
                            V_ERR_MSG := 'Net Amount less than zero (Not insert data to "Temp table").';
                            V_RECORD_COUNT_ERROR := V_RECORD_COUNT_ERROR + 1;
                            
                            P_INSERT_LOG003_DETAIL ( 
                                                                        PI_CONC_REQUEST_ID, 
                                                                        V_ERROR_TYPE, 
                                                                        PI_FILE_NAME, 
                                                                        V_LINE_TYPE, 
                                                                        V_INVOICE_NO, 
                                                                        V_LINE_NUM, 
                                                                        V_FIN_CUST_NO, 
                                                                        V_ITEM, 
                                                                        V_SMS_AMOUNT, 
                                                                        V_PROCESS_DATE_DET, 
                                                                        V_LOG_STATUS, 
                                                                        V_ERR_CODE, 
                                                                        V_ERR_MSG 
                                                                    );
                            
                            P_UPDATE_LOG003_DETAIL ( 
                                                                        PI_CONC_REQUEST_ID, 
                                                                        V_INVOICE_NO, 
                                                                        V_LINE_NUM, 
--                                                                        V_PROCESS_DATE_DET, 
                                                                        V_LOG_STATUS, 
                                                                        V_ERR_CODE, 
                                                                        V_ERR_MSG 
                                                                    );
                        END IF;*/
                    END IF;
                    
                    IF (V_DATA_FLAG = TRUE) Then
                        
                        dbms_output.put_line('DATA FLAG=TRUE');
                        dbms_output.put_line('-----------------------------------------------------');
                        
                        IF V_LINE_NUM IS NULL THEN
                            V_LINE_NUM := 1;
                        ELSE
                            V_LINE_NUM := V_LINE_NUM + 1;
                        END IF;
                                
                        IF V_PROCESS_DATE_DET IS NULL THEN
                            V_PROCESS_DATE_DET := V_PROCESS_DATE_SUM;
                        ELSE
                            V_PROCESS_DATE_DET := FN_GET_PROCESS_DATE;
                        END IF;
                        
                        V_MEMO_LINE := 'Data Service';
                        V_DESCRIPTION := V_MEMO_LINE;
                        V_ITEM := V_MEMO_LINE;
                        
                        -- Insert Data Service
--                        IF V_DATA_AMOUNT >= 0 THEN
                            V_DATA_AMOUNT := ROUND(V_DATA_AMOUNT, 2);
                            
                            dbms_output.put_line('DATA AMOUNT='||V_DATA_AMOUNT);
                            dbms_output.put_line('-----------------------------------------------------');
                            
                            V_UNIT_SELLING_PRICE := V_DATA_AMOUNT;
                            V_RECORD_COUNT_COMPLETE := V_RECORD_COUNT_COMPLETE + 1; 
                            
                            P_INSERT_LOG003_DETAIL ( 
                                                                        PI_CONC_REQUEST_ID, 
                                                                        V_ERROR_TYPE, 
                                                                        PI_FILE_NAME, 
                                                                        V_LINE_TYPE, 
                                                                        V_INVOICE_NO, 
                                                                        V_LINE_NUM, 
                                                                        V_FIN_CUST_NO, 
                                                                        V_ITEM, 
                                                                        V_DATA_AMOUNT, 
                                                                        V_PROCESS_DATE_DET, 
                                                                        NULL, 
                                                                        NULL, 
                                                                        NULL 
                                                                    );
                            
                            dbms_output.put_line('CUSTOMER BILL ARRDESS='||V_CUST_BILL_ADDR);
                            dbms_output.put_line('CUSTOMER SHIP ARRDESS='||V_CUST_SHIP_ADDR);
                            
                            P_INSERT_TEMP ( 
                                                        V_ERR_CODE, 
                                                        V_ERR_MSG, 
                                                        V_INSERT_FLAG, 
                                                        V_CONC_REQUEST_ID, 
                                                        PI_BATCH_SOURCE_NAME, 
                                                        V_SET_OF_BOOK, 
                                                        V_LINE_TYPE, 
                                                        PI_INF_LINE_CONTEXT, 
                                                        V_FIN_CUST_NO, 
                                                        V_INVOICE_NO, 
                                                        V_COMMENTS, 
                                                        V_CURR_CODE, 
                                                        V_CONV_DATE, 
                                                        V_CONV_RATE, 
                                                        V_CONV_TYPE, 
                                                        V_DATA_AMOUNT, 
                                                        PI_CUST_TRX_TYPE, 
                                                        PI_TERM_NAME, 
                                                        V_CUST_BILL_ADDR, 
                                                        V_CUST_SHIP_ADDR, 
                                                        V_TRX_DATE, 
                                                        V_GL_DATE, 
                                                        V_LINE_NUM, 
                                                        V_MEMO_LINE, 
                                                        V_DESCRIPTION, 
                                                        V_QUANTITY, 
                                                        V_UNIT_SELLING_PRICE, 
                                                        V_TAX_CODE, 
                                                        V_UOM_CODE, 
                                                        V_SALESREP_NUMBER, 
                                                        V_TEMP_STATUS 
                                                    );
                            
                            IF V_INSERT_FLAG = 0 THEN
                                V_LOG_STATUS := 'Completed';
                            ELSE
                                V_LOG_STATUS := 'Error';
                            END IF;
                            
                            P_UPDATE_LOG003_DETAIL ( 
                                                                        PI_CONC_REQUEST_ID, 
                                                                        V_INVOICE_NO, 
                                                                        V_LINE_NUM, 
--                                                                        V_PROCESS_DATE_DET, 
                                                                        V_LOG_STATUS, 
                                                                        V_ERR_CODE, 
                                                                        V_ERR_MSG 
                                                                    );
                        /*ELSE
                            V_INVOICE_NO := NULL;
--                            V_DATA_AMOUNT := NULL;
                            V_LOG_STATUS := 'Warning';
                            V_ERR_CODE := 'DTTCNX001-010';
                            V_ERR_MSG := 'Net Amount less than zero (Not insert data to "Temp table").';
                            V_RECORD_COUNT_ERROR := V_RECORD_COUNT_ERROR + 1;
                            
                            P_INSERT_LOG003_DETAIL ( 
                                                                        PI_CONC_REQUEST_ID, 
                                                                        V_ERROR_TYPE, 
                                                                        PI_FILE_NAME, 
                                                                        V_LINE_TYPE, 
                                                                        V_INVOICE_NO, 
                                                                        V_LINE_NUM, 
                                                                        V_FIN_CUST_NO, 
                                                                        V_ITEM, 
                                                                        V_DATA_AMOUNT, 
                                                                        V_PROCESS_DATE_DET, 
                                                                        V_LOG_STATUS, 
                                                                        V_ERR_CODE, 
                                                                        V_ERR_MSG 
                                                                    );
                            
                            P_UPDATE_LOG003_DETAIL ( 
                                                                        PI_CONC_REQUEST_ID, 
                                                                        V_INVOICE_NO, 
                                                                        V_LINE_NUM, 
--                                                                        V_PROCESS_DATE_DET, 
                                                                        V_LOG_STATUS, 
                                                                        V_ERR_CODE, 
                                                                        V_ERR_MSG 
                                                                    );
                        END IF;*/
                    END IF;
                    
                    IF (V_OTH_FLAG = TRUE) Then
                        
                        dbms_output.put_line('OTHER FLAG=TRUE');
                        dbms_output.put_line('-----------------------------------------------------');
                        
                        IF V_LINE_NUM IS NULL THEN
                            V_LINE_NUM := 1;
                        ELSE
                            V_LINE_NUM := V_LINE_NUM + 1;
                        END IF;
                        
                        IF V_PROCESS_DATE_DET IS NULL THEN
                            V_PROCESS_DATE_DET := V_PROCESS_DATE_SUM;
                        ELSE
                            V_PROCESS_DATE_DET := FN_GET_PROCESS_DATE;
                        END IF;
                        
                        V_MEMO_LINE := 'Other Service';
                        V_DESCRIPTION := V_MEMO_LINE;
                        V_ITEM := V_MEMO_LINE;
                        
                        -- Insert Other Service
--                        IF V_OTH_AMOUNT >= 0 THEN
                            V_OTH_AMOUNT := ROUND(V_OTH_AMOUNT, 2);
                            
                            dbms_output.put_line('OTHER AMOUNT='||V_OTH_AMOUNT);
                            dbms_output.put_line('-----------------------------------------------------');
                            
                            V_UNIT_SELLING_PRICE := V_OTH_AMOUNT;
                            V_RECORD_COUNT_COMPLETE := V_RECORD_COUNT_COMPLETE + 1; 
                            
                            P_INSERT_LOG003_DETAIL ( 
                                                                        PI_CONC_REQUEST_ID, 
                                                                        V_ERROR_TYPE, 
                                                                        PI_FILE_NAME, 
                                                                        V_LINE_TYPE, 
                                                                        V_INVOICE_NO, 
                                                                        V_LINE_NUM, 
                                                                        V_FIN_CUST_NO, 
                                                                        V_ITEM, 
                                                                        V_OTH_AMOUNT, 
                                                                        V_PROCESS_DATE_DET, 
                                                                        NULL, 
                                                                        NULL, 
                                                                        NULL 
                                                                    );
                            
                            dbms_output.put_line('CUSTOMER BILL ARRDESS='||V_CUST_BILL_ADDR);
                            dbms_output.put_line('CUSTOMER SHIP ARRDESS='||V_CUST_SHIP_ADDR);
                            
                            P_INSERT_TEMP ( 
                                                        V_ERR_CODE, 
                                                        V_ERR_MSG, 
                                                        V_INSERT_FLAG, 
                                                        V_CONC_REQUEST_ID, 
                                                        PI_BATCH_SOURCE_NAME, 
                                                        V_SET_OF_BOOK, 
                                                        V_LINE_TYPE, 
                                                        PI_INF_LINE_CONTEXT, 
                                                        V_FIN_CUST_NO, 
                                                        V_INVOICE_NO, 
                                                        V_COMMENTS, 
                                                        V_CURR_CODE, 
                                                        V_CONV_DATE, 
                                                        V_CONV_RATE, 
                                                        V_CONV_TYPE, 
                                                        V_OTH_AMOUNT, 
                                                        PI_CUST_TRX_TYPE, 
                                                        PI_TERM_NAME, 
                                                        V_CUST_BILL_ADDR, 
                                                        V_CUST_SHIP_ADDR, 
                                                        V_TRX_DATE, 
                                                        V_GL_DATE, 
                                                        V_LINE_NUM, 
                                                        V_MEMO_LINE, 
                                                        V_DESCRIPTION, 
                                                        V_QUANTITY, 
                                                        V_UNIT_SELLING_PRICE, 
                                                        V_TAX_CODE, 
                                                        V_UOM_CODE, 
                                                        V_SALESREP_NUMBER, 
                                                        V_TEMP_STATUS 
                                                    );
                            
                            IF V_INSERT_FLAG = 0 THEN
                                V_LOG_STATUS := 'Completed';
                            ELSE
                                V_LOG_STATUS := 'Error';
                            END IF;
                            
                            P_UPDATE_LOG003_DETAIL ( 
                                                                        PI_CONC_REQUEST_ID, 
                                                                        V_INVOICE_NO, 
                                                                        V_LINE_NUM, 
--                                                                        V_PROCESS_DATE_DET, 
                                                                        V_LOG_STATUS, 
                                                                        V_ERR_CODE, 
                                                                        V_ERR_MSG 
                                                                    );
                        /*ELSE
                            dbms_output.put_line('OTH NET AMOUNT LESS THAN ZERO');
                            dbms_output.put_line('OTH AMT:'||V_OTH_AMOUNT);
                            
--                            V_INVOICE_NO := NULL;
--                            V_OTH_AMOUNT := NULL;
                            V_LOG_STATUS := 'Warning';
                            V_ERR_CODE := 'DTTCNX001-010';
                            V_ERR_MSG := 'Net Amount less than zero (Not insert data to "Temp table").';
                            V_RECORD_COUNT_ERROR := V_RECORD_COUNT_ERROR + 1;
                            
                            P_INSERT_LOG003_DETAIL ( 
                                                                        PI_CONC_REQUEST_ID, 
                                                                        V_ERROR_TYPE, 
                                                                        PI_FILE_NAME, 
                                                                        V_LINE_TYPE, 
                                                                        V_INVOICE_NO, 
                                                                        V_LINE_NUM, 
                                                                        V_FIN_CUST_NO, 
                                                                        V_ITEM, 
                                                                        V_OTH_AMOUNT, 
                                                                        V_PROCESS_DATE_DET, 
                                                                        V_LOG_STATUS, 
                                                                        V_ERR_CODE, 
                                                                        V_ERR_MSG 
                                                                    );
                            
                            P_UPDATE_LOG003_DETAIL ( 
                                                                        PI_CONC_REQUEST_ID, 
                                                                        V_INVOICE_NO, 
                                                                        V_LINE_NUM, 
--                                                                        V_PROCESS_DATE_DET, 
                                                                        V_LOG_STATUS, 
                                                                        V_ERR_CODE, 
                                                                        V_ERR_MSG 
                                                                    );
                        END IF;*/
                    END IF;
                ELSE
                    V_LINE_NUM := 1;
                    V_ITEM := NULL;
                    V_SMS_AMOUNT := NULL;
                    V_PROCESS_DATE_DET := V_PROCESS_DATE_SUM;
                    V_LOG_STATUS := 'Error';
                    V_ERR_CODE := 'DTTCNX001-010';
                    V_ERR_MSG := 'Net Amount less than zero (Not insert data to "Temp table").';
                    V_NO_ITEM_FLAG := FALSE;
                    
                    dbms_output.put_line('ERROR MESSAGE='||V_ERR_MSG);
                    dbms_output.put_line('-----------------------------------------------------');
                    
                    P_INSERT_LOG003_DETAIL ( 
                                                                PI_CONC_REQUEST_ID, 
                                                                V_ERROR_TYPE, 
                                                                PI_FILE_NAME, 
                                                                V_LINE_TYPE, 
                                                                V_INVOICE_NO, 
                                                                V_LINE_NUM, 
                                                                V_FIN_CUST_NO, 
                                                                V_ITEM, 
                                                                V_SMS_AMOUNT, 
                                                                V_PROCESS_DATE_DET, 
                                                                V_LOG_STATUS, 
                                                                V_ERR_CODE, 
                                                                V_ERR_MSG 
                                                            );
                    
                    V_RECORD_COUNT_ERROR := 1;
                    
                    P_UPDATE_LOG003_DETAIL ( 
                                                                PI_CONC_REQUEST_ID, 
                                                                V_INVOICE_NO, 
                                                                V_LINE_NUM, 
--                                                                V_PROCESS_DATE_DET, 
                                                                V_LOG_STATUS, 
                                                                V_ERR_CODE, 
                                                                V_ERR_MSG 
                                                            );
                END IF;
            END IF;
        EXCEPTION
            WHEN OTHERS THEN
                dbms_output.put_line('Others Error');
                dbms_output.put_line('SQLCODE='||SQLCODE);
                dbms_output.put_line('SQLERRM='||SQLERRM);
                
                V_INVOICE_NO := NULL;
                V_LINE_NUM := 1;
                V_ITEM := NULL;
                V_SMS_AMOUNT := NULL;
                
                IF V_PROCESS_DATE_DET IS NULL THEN
                    V_PROCESS_DATE_DET := V_PROCESS_DATE_SUM;
                ELSE
                    V_PROCESS_DATE_DET := FN_GET_PROCESS_DATE;
                END IF;
                
                V_LOG_STATUS := 'Error';
                V_ERR_CODE := 'DTTCNX001-008';
                V_ERR_MSG := 'Cannot read XML file.';
                V_RECORD_COUNT_ERROR := V_RECORD_COUNT_ERROR + 1;
                
                P_INSERT_LOG003_DETAIL ( 
                                                            PI_CONC_REQUEST_ID, 
                                                            V_ERROR_TYPE, 
                                                            PI_FILE_NAME, 
                                                            V_LINE_TYPE, 
                                                            V_INVOICE_NO, 
                                                            V_LINE_NUM, 
                                                            V_FIN_CUST_NO, 
                                                            V_ITEM, 
                                                            V_SMS_AMOUNT, 
                                                            V_PROCESS_DATE_DET, 
                                                            V_LOG_STATUS, 
                                                            V_ERR_CODE, 
                                                            V_ERR_MSG 
                                                        );
                
                V_RECORD_COUNT_ERROR := 1;
                
                V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
                P_WRITE_LOG(V_USER_ERROR);
        END;
        
        IF V_NO_ITEM_FLAG THEN
            V_INVOICE_NO := NULL;
            V_LINE_NUM := 1;
            V_ITEM := NULL;
            V_SMS_AMOUNT := NULL;
            V_PROCESS_DATE_DET := V_PROCESS_DATE_SUM;
            
            V_LOG_STATUS := 'Warning';
            V_ERR_CODE := 'DTTCNX001-011';
            V_ERR_MSG := 'Not have service line.';
            V_RECORD_COUNT_ERROR := V_RECORD_COUNT_ERROR + 1;
                
            P_INSERT_LOG003_DETAIL ( 
                                                        PI_CONC_REQUEST_ID, 
                                                        V_ERROR_TYPE, 
                                                        PI_FILE_NAME, 
                                                        V_LINE_TYPE, 
                                                        V_INVOICE_NO, 
                                                        V_LINE_NUM, 
                                                        V_FIN_CUST_NO, 
                                                        V_ITEM, 
                                                        V_SMS_AMOUNT, 
                                                        V_PROCESS_DATE_DET, 
                                                        V_LOG_STATUS, 
                                                        V_ERR_CODE, 
                                                        V_ERR_MSG 
                                                    );
                        
                        P_UPDATE_LOG003_DETAIL ( 
                                                                    PI_CONC_REQUEST_ID, 
                                                                    V_INVOICE_NO, 
                                                                    V_LINE_NUM, 
--                                                                    V_PROCESS_DATE_DET, 
                                                                    V_LOG_STATUS, 
                                                                    V_ERR_CODE, 
                                                                    V_ERR_MSG 
                                                                );
        END IF;
        
        P_UPDATE_LOG003_SUMMARY (  
                                                            PI_CONC_REQUEST_ID, 
                                                            PI_FILE_NAME, 
--                                                            V_PROCESS_DATE_SUM, 
                                                            V_RECORD_COUNT_COMPLETE, 
                                                            V_RECORD_COUNT_ERROR 
                                                      );
    END P_READ_AND_INSERT;
    
    PROCEDURE P_LOG002_MONITOR ( 
                                                            PIO_ERRCODE IN OUT VARCHAR2, 
                                                            PIO_ERRMSG IN OUT VARCHAR2, 
                                                            PI_CONC_REQUEST_ID IN NUMBER, 
                                                            PI_DIR_NAME IN VARCHAR2, 
                                                            P_FILENAME IN VARCHAR2 
                                                      ) IS
        
        CURSOR C_LOG_DETAIL ( P_CONC_ID IN NUMBER ) IS 
            SELECT LEVEL_TYPE||'|'||
                        ERROR_TYPE||'|'||
                        PATH_NAME||'|'||
                        FILE_NAME||'|'||
                        TO_CHAR(PROCESS_DATE, 'DD/MM/YYYYHH24:MI')||'|'||
                        SUBSTR(TO_CHAR(PROCESSING_TIME - PROCESS_DATE), 12, 15)||'|'||
                        STATUS||'|'||
                        ERROR_CODE||'|'||
                        ERROR_MESSAGE AS LOG_TXT 
            FROM DTTCNX002_LOG_DETAIL 
            WHERE REQUEST_ID = P_CONC_ID;
        
        V_DESTFILE   UTL_FILE.FILE_TYPE;
        V_FILENAME   VARCHAR2(100) := P_FILENAME||'_'||TO_CHAR(SYSDATE, 'YYYYMMDD_HHMI')||'.TXT';
        V_WRITETXT   NVARCHAR2(32767);
        
        -- For write error handling log
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
        dbms_output.put_line('LOG002');
        dbms_output.put_line(PI_CONC_REQUEST_ID);
        dbms_output.put_line(PI_DIR_NAME);
        dbms_output.put_line(P_FILENAME);
        
        V_DESTFILE := UTL_FILE.FOPEN_NCHAR ( 
                                                                        PI_DIR_NAME, 
                                                                        V_FILENAME, 
                                                                        'W', 
                                                                        MAX_LINESIZE 
                                                                    );
        
        IF UTL_FILE.IS_OPEN(V_DESTFILE) THEN
            --Write Header section
            BEGIN
                SELECT LEVEL_TYPE||'|'||
                            REQUEST_ID||'|'||
                            PROGRAM_NAME||'|'||
                            TO_CHAR(PROCESS_DATE, 'DD/MM/YYYYHH24:MI')||'|'||
                            SUBSTR(TO_CHAR(PROCESSING_TIME - PROCESS_DATE), 12, 15)||'|'||
                            XML_FILE_COUNT AS LOG_TXT 
                INTO V_WRITETXT 
                FROM DTTCNX002_LOG_SUMMARY 
                WHERE REQUEST_ID = PI_CONC_REQUEST_ID;
                
                dbms_output.put_line(V_WRITETXT);
                
                P_WRITE_LOG(V_WRITETXT);
--                UTL_FILE.PUT_LINE(V_DESTFILE, V_WRITETXT);
                P_WRITE_LINE(V_DESTFILE, V_WRITETXT);
            EXCEPTION
                WHEN OTHERS THEN
                    V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
                    P_WRITE_LOG(V_USER_ERROR);
            END;
            
            --Write detail section
            FOR I IN C_LOG_DETAIL ( PI_CONC_REQUEST_ID ) 
            LOOP
                V_WRITETXT := I.LOG_TXT;
                
                dbms_output.put_line(V_WRITETXT);
                
                P_WRITE_LOG(V_WRITETXT);
--                UTL_FILE.PUT_LINE(V_DESTFILE, V_WRITETXT);
                P_WRITE_LINE(V_DESTFILE, V_WRITETXT);
            END LOOP;
            
            UTL_FILE.FCLOSE(V_DESTFILE);
        ELSE
            V_USER_ERROR := 'Couldn''t OPEN FILE';
            P_WRITE_LOG(V_USER_ERROR);
        END IF;

    EXCEPTION WHEN OTHERS THEN
        V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
        P_WRITE_LOG(V_USER_ERROR);
    END P_LOG002_MONITOR;
    
    PROCEDURE P_LOG003_MONITOR ( 
                                                            PIO_ERRCODE IN OUT VARCHAR2, 
                                                            PIO_ERRMSG IN OUT VARCHAR2, 
                                                            PI_CONC_REQUEST_ID IN NUMBER, 
                                                            PI_DIR_NAME IN VARCHAR2, 
                                                            P_FILENAME IN VARCHAR2 
                                                      ) IS
        
        CURSOR C_LOG_SUMMARY ( P_CONC_ID IN NUMBER ) IS 
                SELECT FILE_NAME, 
                            LEVEL_TYPE||'|'||
                            REQUEST_ID||'|'||
                            PROGRAM_NAME||'|'||
                            FILE_NAME||'|'||
                            TO_CHAR(PROCESS_DATE, 'DD/MM/YYYYHH24:MI')||'|'||
                            SUBSTR(TO_CHAR(PROCESSING_TIME - PROCESS_DATE), 12, 15)||'|'||
                            RECORD_COUNT_COMPLETE||'|'||
                            RECORD_COUNT_ERROR AS LOG_TXT 
                FROM DTTCNX003_LOG_SUMMARY 
                WHERE REQUEST_ID = PI_CONC_REQUEST_ID;
        
        CURSOR C_LOG_DETAIL ( P_CONC_ID IN NUMBER, P_FILE_NAME IN VARCHAR2 ) IS 
            SELECT LEVEL_TYPE||'|'||
                        ERROR_TYPE||'|'||
                        FILE_NAME||'|'||
                        LINE_TYPE||'|'||
                        INVOICE_NO||'|'||
                        LINE_NUMBER||'|'||
                        CUSTOMER_NO||'|'||
                        ITEM||'|'||
                        AMOUNT||'|'||
                        TO_CHAR(PROCESS_DATE, 'DD/MM/YYYYHH24:MI')||'|'||
                        SUBSTR(TO_CHAR(PROCESSING_TIME - PROCESS_DATE), 12, 15)||'|'||
                        STATUS||'|'||
                        ERROR_CODE||'|'||
                        ERROR_MESSAGE AS LOG_TXT 
            FROM DTTCNX003_LOG_DETAIL 
            WHERE REQUEST_ID = P_CONC_ID 
            AND FILE_NAME = P_FILE_NAME;
        
        V_DESTFILE   UTL_FILE.FILE_TYPE;
        V_FILENAME   VARCHAR2(100) := P_FILENAME||'_'||TO_CHAR(SYSDATE, 'YYYYMMDD_HHMI')||'.TXT';
        V_WRITETXT   NVARCHAR2(32767);
        
        -- For write error handling log
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
        dbms_output.put_line(PI_CONC_REQUEST_ID);
        dbms_output.put_line(PI_DIR_NAME);
        dbms_output.put_line(P_FILENAME);
        
        V_DESTFILE := UTL_FILE.FOPEN_NCHAR ( 
                                                                        PI_DIR_NAME, 
                                                                        V_FILENAME, 
                                                                        'W', 
                                                                        MAX_LINESIZE 
                                                                    );
        
        IF UTL_FILE.IS_OPEN(V_DESTFILE) THEN
            --Write Header section
            FOR I IN C_LOG_SUMMARY ( PI_CONC_REQUEST_ID ) 
            LOOP
                V_WRITETXT := I.LOG_TXT;
                
                P_WRITE_LOG(V_WRITETXT);
                P_WRITE_LINE(V_DESTFILE, V_WRITETXT);
            
                --Write detail section
                FOR J IN C_LOG_DETAIL ( PI_CONC_REQUEST_ID, I.FILE_NAME ) 
                LOOP
                    V_WRITETXT := J.LOG_TXT;
                    
                    P_WRITE_LOG(V_WRITETXT);
                    P_WRITE_LINE(V_DESTFILE, V_WRITETXT);
                END LOOP;
            END LOOP;
                
            UTL_FILE.FCLOSE(V_DESTFILE);
        ELSE
            V_USER_ERROR := 'Couldn''t OPEN FILE';
            P_WRITE_LOG(V_USER_ERROR);
        END IF;

    EXCEPTION WHEN OTHERS THEN
        V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
        P_WRITE_LOG(V_USER_ERROR);           
    END P_LOG003_MONITOR;
    
    PROCEDURE P_LOG004_MONITOR ( 
                                                            PIO_ERRCODE IN OUT VARCHAR2, 
                                                            PIO_ERRMSG IN OUT VARCHAR2, 
                                                            PI_CONC_REQUEST_ID IN NUMBER, 
                                                            PI_DIR_NAME IN VARCHAR2, 
                                                            P_FILENAME IN VARCHAR2 
                                                      ) IS
        
        CURSOR C_LOG_DETAIL ( P_CONC_ID IN NUMBER ) IS 
            SELECT LEVEL_TYPE||'|'||
                        ERROR_TYPE||'|'||
                        PATH_NAME||'|'||
                        FILE_NAME||'|'||
                        TO_CHAR(PROCESS_DATE, 'DD/MM/YYYYHH24:MI')||'|'||
                        SUBSTR(TO_CHAR(PROCESSING_TIME - PROCESS_DATE), 12, 15)||'|'||
                        STATUS||'|'||
                        ERROR_CODE||'|'||
                        ERROR_MESSAGE AS LOG_TXT 
            FROM DTTCNX004_LOG_DETAIL 
            WHERE REQUEST_ID = P_CONC_ID;
        
        V_DESTFILE   UTL_FILE.FILE_TYPE;
        V_FILENAME   VARCHAR2(100) := P_FILENAME||'_'||TO_CHAR(SYSDATE, 'YYYYMMDD_HHMI')||'.TXT';
        V_WRITETXT   NVARCHAR2(32767);
        
        -- For write error handling log
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
        dbms_output.put_line(PI_CONC_REQUEST_ID);
        dbms_output.put_line(PI_DIR_NAME);
        dbms_output.put_line(P_FILENAME);
        
        V_DESTFILE := UTL_FILE.FOPEN_NCHAR ( 
                                                                        PI_DIR_NAME, 
                                                                        V_FILENAME, 
                                                                        'W', 
                                                                        MAX_LINESIZE 
                                                                    );
        
        IF UTL_FILE.IS_OPEN(V_DESTFILE) THEN
            --Write Header section
            BEGIN
                SELECT LEVEL_TYPE||'|'||
                            REQUEST_ID||'|'||
                            PROGRAM_NAME||'|'||
                            TO_CHAR(PROCESS_DATE, 'DD/MM/YYYYHH24:MI')||'|'||
                            SUBSTR(TO_CHAR(PROCESSING_TIME - PROCESS_DATE), 12, 15)||'|'||
                            OUTPUT_FILE_COUNT AS LOG_TXT 
                INTO V_WRITETXT 
                FROM DTTCNX004_LOG_SUMMARY 
                WHERE REQUEST_ID = PI_CONC_REQUEST_ID;
                
                P_WRITE_LOG(V_WRITETXT);
                P_WRITE_LINE(V_DESTFILE, V_WRITETXT);
            EXCEPTION
                WHEN OTHERS THEN
                    V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
                    P_WRITE_LOG(V_USER_ERROR);
            END;
            
            --Write detail section
            FOR I IN C_LOG_DETAIL ( PI_CONC_REQUEST_ID ) 
            LOOP
                V_WRITETXT := I.LOG_TXT;
                
                P_WRITE_LOG(V_USER_ERROR);
                P_WRITE_LINE(V_DESTFILE, V_WRITETXT);
            END LOOP;
            
            UTL_FILE.FCLOSE(V_DESTFILE);
        ELSE
            V_USER_ERROR := 'Couldn''t OPEN FILE';
            P_WRITE_LOG(V_USER_ERROR);
        END IF;
        
    EXCEPTION WHEN OTHERS THEN
        V_USER_ERROR := 'Error others : ' || SUBSTR(SQLERRM, 1, 220);
        P_WRITE_LOG(V_USER_ERROR);           
    END P_LOG004_MONITOR;
    
    PROCEDURE P_LOG_MONITOR ( 
                                                        PIO_ERRCODE IN OUT VARCHAR2, 
                                                        PIO_ERRMSG IN OUT VARCHAR2, 
                                                        PI_LOG_NAME IN VARCHAR2, 
                                                        PI_CONC_REQUEST_ID IN NUMBER, 
                                                        PI_DIR_NAME IN VARCHAR2 
                                                 ) IS
        
        -- For write log monitor
        V_LOG002_DEFAULT_FILE_NAME   VARCHAR2(50) := 'DTTCNX002_CHECK_ZIP_FILE';
        V_LOG003_DEFAULT_FILE_NAME   VARCHAR2(50) := 'DTTCNX003_VALIDATE_DATA';
        V_LOG004_DEFAULT_FILE_NAME   VARCHAR2(50) := 'DTTCNX004_CHECK_OUTPUT_FILE';
        
        -- For write error handling log
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
        dbms_output.put_line(PI_LOG_NAME);
        
        IF PI_LOG_NAME = 'FIND_ZIP' THEN
            P_LOG002_MONITOR ( 
                                                PIO_ERRCODE, 
                                                PIO_ERRMSG, 
                                                PI_CONC_REQUEST_ID, 
                                                PI_DIR_NAME, 
                                                V_LOG002_DEFAULT_FILE_NAME
                                           );
        ELSIF PI_LOG_NAME = 'VALIDATE_DATA' THEN
            P_LOG003_MONITOR ( 
                                                PIO_ERRCODE, 
                                                PIO_ERRMSG, 
                                                PI_CONC_REQUEST_ID, 
                                                PI_DIR_NAME, 
                                                V_LOG003_DEFAULT_FILE_NAME
                                           );
        ELSIF PI_LOG_NAME = 'CHECK_OUTPUT' THEN
            P_LOG004_MONITOR ( 
                                                PIO_ERRCODE, 
                                                PIO_ERRMSG, 
                                                PI_CONC_REQUEST_ID, 
                                                PI_DIR_NAME, 
                                                V_LOG004_DEFAULT_FILE_NAME
                                           );
        END IF;
    END P_LOG_MONITOR;
    
    PROCEDURE P_LOG_MONITOR ( 
                                                        PI_LOG_NAME IN VARCHAR2, 
                                                        PI_CONC_REQUEST_ID IN NUMBER, 
                                                        PI_DIR_NAME IN VARCHAR2 
                                                 ) IS
        
        -- For write log monitor
        V_LOG002_DEFAULT_FILE_NAME   VARCHAR2(50) := 'DTTCNX002_CHECK_ZIP_FILE';
        V_LOG003_DEFAULT_FILE_NAME   VARCHAR2(50) := 'DTTCNX003_VALIDATE_DATA';
        V_LOG004_DEFAULT_FILE_NAME   VARCHAR2(50) := 'DTTCNX004_CHECK_OUTPUT_FILE';
        
        -- For write error handling log
        V_ERRCODE   VARCHAR2(30); 
        V_ERRMSG   VARCHAR2(240);
        V_USER_ERROR   VARCHAR2(240);
        
    BEGIN
        dbms_output.put_line(PI_LOG_NAME);
        
        IF PI_LOG_NAME = 'FIND_ZIP' THEN
            P_LOG002_MONITOR ( 
                                                V_ERRCODE, 
                                                V_ERRMSG, 
                                                PI_CONC_REQUEST_ID, 
                                                PI_DIR_NAME, 
                                                V_LOG002_DEFAULT_FILE_NAME
                                           );
        ELSIF PI_LOG_NAME = 'VALIDATE_DATA' THEN
            P_LOG003_MONITOR ( 
                                                V_ERRCODE, 
                                                V_ERRMSG, 
                                                PI_CONC_REQUEST_ID, 
                                                PI_DIR_NAME, 
                                                V_LOG003_DEFAULT_FILE_NAME
                                           );
        ELSIF PI_LOG_NAME = 'CHECK_OUTPUT' THEN
            P_LOG004_MONITOR ( 
                                                V_ERRCODE, 
                                                V_ERRMSG, 
                                                PI_CONC_REQUEST_ID, 
                                                PI_DIR_NAME, 
                                                V_LOG004_DEFAULT_FILE_NAME
                                           );
        END IF;
    END P_LOG_MONITOR;
    
END TAC_PKG_TCNX_CUST; 
/

