package dtac.erp;

import java.util.Properties;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class LdapLogin {

   /* To test ldap connection. with java 1.4
    * This will print 'Hello World' as the output
    */
	private String m_LdapServer=null;
	private String m_LdapPort=null;
	private String m_LdapDomain=null;
	private String m_Fixpwd=null;
	
	public LdapLogin(){
		Properties prop = new Properties();
		try{
		prop.load(LdapLogin.class.getClassLoader().getResourceAsStream("DtacLdapconfig.properties"));
		m_LdapServer=prop.getProperty("LdapServer");
		m_LdapPort=prop.getProperty("LdapPort");
		m_LdapDomain=prop.getProperty("LdapDomain");
		m_Fixpwd=prop.getProperty("Fixpwd");
		
		System.out.println(m_LdapServer);
    	System.out.println(m_LdapPort);
    	System.out.println(m_LdapDomain);
		System.out.println(m_Fixpwd);
		
		} catch (IOException ex) 
		{
    		ex.printStackTrace();
			System.out.println("  loginldap : Error LdapLogin Constructor : Loading value from DtacLdapconfig.properties");
        } 

	}
	
    public static void main(String []args) {
		boolean bLogin;
		
       System.out.println("Begin main"); 
	   System.out.println("args[0]=" + args[0]);
	   System.out.println("args[1]=" + args[1]);
	   
	   LdapLogin 	myLdapLogin = new LdapLogin();
	   
	   bLogin = myLdapLogin.loginldap(args[0],args[1]);
	   if(bLogin){
			System.out.println("Success Login"); 
	   }else{
			System.out.println("Fail Login"); 
	    }
    }
	
	public String getFixpwd(){
		return m_Fixpwd;
	}
	
	public boolean loginldap(String userName,String password){
	
	   String[] strArrayLdapServer=null;
	   java.util.Date date = new java.util.Date();
	   //System.out.println(System.getProperty("java.class.path"));
	
	  Properties env = new Properties();
	  env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
	  env.put(Context.PROVIDER_URL, "ldap://10.20.3.20:389/");
      env.put(Context.SECURITY_PRINCIPAL, m_LdapDomain + "\\" + userName);
      env.put(Context.SECURITY_CREDENTIALS, password);
	  
	  System.out.println(date.toString() + " user: " + userName );
	  
	  strArrayLdapServer = m_LdapServer.split(",");
		
		for(int i = 0; i < strArrayLdapServer.length; i++) {
		
			System.out.println(date.toString() + "  loginldap : For Ldap Server:" + strArrayLdapServer[i]);
			
			env.remove(Context.PROVIDER_URL);
			env.put(Context.PROVIDER_URL, "LDAP://" + strArrayLdapServer[i] + ":" + m_LdapPort);
			
	  		try {
				DirContext ctx =new InitialDirContext(env);
				System.out.println(date.toString() + "  loginldap : Return true" );
				return true;
			} catch (NamingException e) {
				e.printStackTrace();
				System.out.println(date.toString() + " loginldap : catch NamingException e " + e.toString() ); 
			} catch (Throwable e){
				e.printStackTrace();
				System.out.println(date.toString() + " loginldap : catch Throwable e " + e.toString() ); 
			}

		}
		
		//If all server try to login fail return false
		System.out.println(date.toString() + " loginldap : Return false" );
		return false;
	  
	}
} 