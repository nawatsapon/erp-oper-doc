<%-- $Header: fndvald.jsp 115.37 2008/11/19 01:36:00 dggriffi noship $ --%>
<%@ page language="java" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="oracle.apps.fnd.common.WebAppsContext" %>
<%@ page import="oracle.apps.fnd.common.Message" %>
<%@ page import="oracle.apps.fnd.common.AppsLog" %>
<%@ page import="oracle.apps.fnd.common.Log" %>
<%@ page import="oracle.apps.fnd.sso.AuthenticationException" %>
<%@ page import="oracle.apps.fnd.sso.SessionMgr" %>
<%@ page import="oracle.apps.fnd.sso.SSOUtil" %>
<%@ page import="oracle.apps.fnd.sso.Utils" %>
<%@ page import="oracle.apps.fnd.sso.AppsAgent" %>
<%@ page import="oracle.apps.fnd.security.CSS" %>
<%@ page import="oracle.apps.fnd.security.UserPwd" %>
<%@ page import="oracle.apps.fnd.security.HTMLProcessor" %>
<%@ page import='oracle.apps.fnd.common.Message'%>
<%@ page import='oracle.apps.fnd.common.ResourceStore'%>
<%@ page import='oracle.apps.fnd.functionSecurity.RunFunctionUtil'%>
<%@ page import="oracle.apps.fnd.common.ThreadContext" %>
<%@ page import="oracle.apps.fnd.common.Const" %>
<%@ page import='dtac.erp.LdapLogin'%>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.*,java.io.*,java.util.regex.*,javax.naming.*,javax.naming.directory.*,java.util.*,java.lang.*,java.util.Date,java.text.SimpleDateFormat,javax.sql.*" %>
<%
  response.setHeader("Cache-Control", "no-cache");
  response.setHeader("Pragma", "no-cache");
  response.setDateHeader("Expires", 0);
%>
<%
	/* Logs begins at time */
	Date transcStartAt = new Date();
	String ErrorMsg = "";
%>
<%
  boolean alreadySet = false;
  ResourceStore rStore = null;
  String notMsg = null;
  String saNotifyProf  = "N";
  String failedAttempts = "0";

  //Bug7169414: Initialized to ensure NullPointerException does not occur
  
  ThreadContext c = new ThreadContext();
  c.put(Const.INVALID_LOGINS, failedAttempts);
  c.put(Const.SIGNON_AUDIT_NOTIFY_PROFILE, saNotifyProf);

  WebAppsContext wctx = null;
  if(Utils.isAppsContextAvailable()){
       wctx = Utils.getAppsContext();      
       alreadySet = true;
  }
  else{
       wctx = Utils.getAppsContext();
  }

  rStore = wctx.getResourceStore();

  Utils.setRequestCharacterEncoding(request);
  String forwardUrl = "";
  
  String username = request.getParameter("username").trim();
  String password = request.getParameter("password");
  String langCode = request.getParameter("langCode");
  
  if(password.equals("") || password.equals(null) ) {
	  password = "<<BLANK>>";
  }


  Connection conn = null;
  if(langCode != null) wctx.setCurrLang(langCode);
  
  // ---Begin1 Yodchai L. 19-July-2015
  PreparedStatement preparedStatement = null;
  String strLoginType="SSO";
  String sql,dtacLdapFixpwd;
  java.util.Date date = new java.util.Date();
  // ---End1 Yodchai L. 19-July-2015
  
  try {
    conn = Utils.getConnection();
	
	// ---Begin2 Yodchai L. 19-July-2015
	sql = "select profile_option_value "
			 +	" from fnd_profile_option_values a "
			 +	" where application_id =0 "
			 +	"	 and profile_option_id = 7501 "
			 +	"    and ( level_value = (select min(user_id) from fnd_user where user_name = ? ) "
			 +  "        or level_id = 10001  ) "
			 +  "    and profile_option_value is not null "
			 +	" order by level_id";
	preparedStatement = conn.prepareStatement(sql);
	preparedStatement.setString(1, username.toUpperCase());
	ResultSet rs = preparedStatement.executeQuery();
	
			
	System.out.println("-------" + date.toString() + "---- user ---- " + username );
	//System.out.println(date.toString() + System.getProperty("java.class.path"));
	System.out.println(date.toString() + " DTAC TESTing 1");
	
	while (rs.next()) {

		strLoginType = rs.getString("profile_option_value");
		//System.out.println(date.toString() + " DTAC :  profile_option_value : " + strLoginType);

	}
	
	System.out.println(date.toString() + " DTAC TESTing 2 : Login Type : " + strLoginType);
	
	if ( (strLoginType.equals("SSO") || strLoginType.equals("BOTH")) && !username.toUpperCase().equals("SYSADMIN") ){
	  //-----Start Modify to Single Password with Dtac AD 14-Oct-13
	  
	  System.out.println(date.toString() + " DTAC :  In IF1  : " + strLoginType);
	  
	  LdapLogin dtacLdap = new LdapLogin();
	  System.out.println(date.toString() + " DTAC :  In IF1.1  : " + strLoginType);
	  
	  boolean bLoginResult = false;
	  dtacLdapFixpwd = dtacLdap.getFixpwd();
	  
	  System.out.println(date.toString() + " DTAC :  In IF2  : " + dtacLdapFixpwd);
	  
	  if (strLoginType.equals("BOTH") && password.equals(dtacLdapFixpwd)) {
		  bLoginResult = true;
	  }else{
		  bLoginResult = dtacLdap.loginldap(username,password);
		  ErrorMsg = dtacLdap.getErrorMsg();
	  }
	  
	  System.out.println(date.toString() + " DTAC :  In IF3  : " + String.valueOf(bLoginResult));
	  
	  if( bLoginResult ){
		password = dtacLdapFixpwd;
	  }else{
		password = dtacLdapFixpwd + 'X';
	  }
    }
	// ---End2 Yodchai L. 19-July-2015
	
	//System.out.println(date.toString() + " DTAC :  Out IF4  : " + String.valueOf(bLoginResult));
	
    UserPwd pUser = new UserPwd(username, password);
    forwardUrl = SessionMgr.createAppsSession(pUser, request, response);
    //out.println("<br>forwardUrl: "+forwardUrl);
    conn.commit();
    Utils.writeToLog("html/fndvald.jsp",  "forwardUrl: "+forwardUrl, wctx, Log.STATEMENT);
    forwardUrl = RunFunctionUtil.processRequestURL (forwardUrl, wctx.getUserId(), wctx);
    Utils.writeToLog("html/fndvald.jsp", "After func security scan:: "+forwardUrl, wctx); 
    
  //Bug7169414: Get values from Thread to determine if Signon Notification
  //should be raised.
  
  failedAttempts = (String) String.valueOf(c.get(Const.INVALID_LOGINS));
  saNotifyProf   = (String) String.valueOf(c.get(Const.SIGNON_AUDIT_NOTIFY_PROFILE));

  //out.println("<br>failedAttempts is " + failedAttempts+" and saNotifyProf is "+saNotifyProf);

  if (Integer.parseInt(failedAttempts)>0 && "Y".equals(saNotifyProf))
  {
    Message msg = new Message("FND","SECURITY-UNSUCCESSFUL LOGINS");
    msg.setToken("NUMBER",failedAttempts,false);
    notMsg = msg.getMessageText(rStore,langCode);
    notMsg = notMsg.replaceAll("\n","\\\\n").replaceAll("'","\\\\'").replaceAll("\"","\\\\\"");

    out.println("<html><head>\n"
        +" <meta http-equiv=refresh content=0;"+forwardUrl+">\n"
        +"</head>");
    out.println("<body>&nbsp");
    out.println("<script>\n"
        +"alert('"+notMsg+"');\n"
       +"location='"+forwardUrl+"';\n"
        +"</script>");
    out.println("</body></html>");
    
  }else{
       response.sendRedirect(forwardUrl);
  }

 // Bug7169414: Moved to ensure the context is not destroyed before
 // retrieving the relevant information like the message
 
 } catch(Exception e) {
      conn.rollback();
         Utils.writeToLog("html/fndvald.jsp",  "Generic Exception catch : "+
         Utils.getExceptionStackTrace(e), wctx, Log.EXCEPTION);

      // show some nice error message.
      throw e;
   
 } finally {
      if (conn != null) { Utils.releaseConnection(); }
      if(alreadySet == false) Utils.releaseAppsContext();

      //Bug716414 Clear thread variables
      c.clear();
 }

%>

<%
	/* Performing logs-opt only on ERP */
	if ("GET".equalsIgnoreCase(request.getMethod())) {
		return;
	}
	
	/* Logs begins */
	Writer wLog = null;
	String logFile = "/erpapp/prod/LOG/TAC_ERP_User_Login_" +  new SimpleDateFormat("yyyyMMdd").format(transcStartAt) + ".txt";
	
	/* Logs ends */
	SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	boolean isNewlyCreated = false;
		
	try {
		isNewlyCreated = !new File(logFile).exists();
		wLog = new FileWriter(logFile, true);

		if (isNewlyCreated) {
			// write out headers
			wLog.write("BEGINDATETIME|ENDDATETIME|IP_CALLER|POS_SERVER|ENDPOINT|USER|LOGIN_TYPE|STATUS|MESSAGE\r\n");
		}
		wLog.write(fmt.format(transcStartAt) + "|"); //BEGINDATETIME
		wLog.write(fmt.format(new Date()) + "|"); //ENDDATETIME
		wLog.write(request.getRemoteAddr() + "|"); //IP_CALLER
		wLog.write(request.getServerName() + "|"); //POS_SERVER
		wLog.write(request.getProtocol().substring(0, 4).toLowerCase() + "://" + request.getServerName() + request.getRequestURI() + "|"); //ENDPOINT
		wLog.write(request.getParameter("username").trim() + "|");
		
		boolean bLoginResult = forwardUrl.indexOf("FND_APPL_LOGIN_FAILED") == -1;
		
		wLog.write(strLoginType + "|");
		wLog.write(bLoginResult? "SUCCESS|User has logged-in successfully" : "FAIL|Login Failed, " + ErrorMsg);
		// wLog.write(forwardUrl);
		wLog.write("\r\n");
		wLog.flush();
	} catch(Exception ex) {
		System.out.println(new Date().toString() + " Error while trying to write logs into " + logFile + ". See message, " + ex.getMessage());
		ex.printStackTrace();
	} finally {
		if (wLog != null) wLog.close();
	}
	
    /* Logs ends */
%>