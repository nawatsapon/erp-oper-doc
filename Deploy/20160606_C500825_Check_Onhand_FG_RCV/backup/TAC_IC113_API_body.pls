create or replace PACKAGE BODY TAC_IC113_API IS
-----------------------------------------------------------------------------------------
PROCEDURE write_log (param_msg VARCHAR2)
IS
BEGIN
	fnd_file.put_line(fnd_file.log, param_msg);
	fnd_file.put_line(fnd_file.output, param_msg);
END write_log;
---------------------------------------------------------------------------------
PROCEDURE conc_wait (param_req NUMBER)
IS
	phase        VARCHAR2 (30);
	status       VARCHAR2 (30);
	dev_phase    VARCHAR2 (30);
	dev_status   VARCHAR2 (30);
	message      VARCHAR2 (1000);
BEGIN
	COMMIT;
	IF fnd_concurrent.wait_for_request (param_req, 10, 0, phase, status, dev_phase, dev_status, message) THEN
		NULL;
	END IF;
END conc_wait;
-----------------------------------------------------------------------------------------
PROCEDURE delete_sim_product (o_status OUT varchar2)
IS
BEGIN
	DELETE	dtac_sim_receipt_interface	sri
	WHERE   request_id    = G_REQUEST_ID ;
	--	WHERE	sri.SOURCE			=	g_source_company
	--	AND	sri.transaction_source_name	=	g_source_name
	--	AND	sri.selected_flag		=	'Y';
	COMMIT;  -- 27-apr-07
EXCEPTION WHEN OTHERS THEN
	Write_Log('Delete Selected Sim Product : Result >> '||SQLERRM);
	o_status	:=	SQLERRM;
END delete_sim_product;
-----------------------------------------------------------------------------------------
PROCEDURE insert_trx_inf (ir_mtl_trx_inf IN mtl_transactions_interface%ROWTYPE, o_error OUT VARCHAR2)
IS
BEGIN
	o_error	:=	NULL;
	INSERT INTO mtl_transactions_interface
		(transaction_interface_id
		,transaction_header_id
		,source_code
		,source_line_id
		,source_header_id
		,process_flag
		,transaction_mode
		,lock_flag
		,last_update_date
		,last_updated_by
		,creation_date
		,created_by
		,last_update_login
		,inventory_item_id
		,item_segment1
		,organization_id
		,transaction_quantity
		,primary_quantity
		,transaction_uom
		,transaction_date
		,subinventory_code
		,loc_segment1
		,transaction_source_id
		,transaction_source_name
		,transaction_source_type_id
		,transaction_action_id
		,transaction_type_id
		,reason_id
		,transaction_reference
		,transaction_cost
		,distribution_account_id)
	VALUES (ir_mtl_trx_inf.transaction_interface_id
		,ir_mtl_trx_inf.transaction_header_id
		,ir_mtl_trx_inf.source_code
		,ir_mtl_trx_inf.source_line_id
		,ir_mtl_trx_inf.source_header_id
		,ir_mtl_trx_inf.process_flag
		,ir_mtl_trx_inf.transaction_mode
		,ir_mtl_trx_inf.lock_flag
		,ir_mtl_trx_inf.last_update_date
		,ir_mtl_trx_inf.last_updated_by
		,ir_mtl_trx_inf.creation_date
		,ir_mtl_trx_inf.created_by
		,ir_mtl_trx_inf.last_update_login
		,ir_mtl_trx_inf.inventory_item_id
		,ir_mtl_trx_inf.item_segment1
		,ir_mtl_trx_inf.organization_id
		,ir_mtl_trx_inf.transaction_quantity
		,ir_mtl_trx_inf.primary_quantity
		,ir_mtl_trx_inf.transaction_uom
		,ir_mtl_trx_inf.transaction_date
		,ir_mtl_trx_inf.subinventory_code
		,ir_mtl_trx_inf.loc_segment1
		,ir_mtl_trx_inf.transaction_source_id
		,ir_mtl_trx_inf.transaction_source_name
		,ir_mtl_trx_inf.transaction_source_type_id
		,ir_mtl_trx_inf.transaction_action_id
		,ir_mtl_trx_inf.transaction_type_id
		,ir_mtl_trx_inf.reason_id
		,ir_mtl_trx_inf.transaction_reference
		,ir_mtl_trx_inf.transaction_cost
		,ir_mtl_trx_inf.distribution_account_id);
EXCEPTION WHEN OTHERS THEN
	o_error := 'Error: '||SQLERRM;
END insert_trx_inf;
-----------------------------------------------------------------------------------------
PROCEDURE assign_trx_inf(i_trx_type	IN	VARCHAR2
			,i_trx_hdr_id	IN	NUMBER
			,ir_po_trx_line	IN	po_trx_type
			,or_mtl_trx_inf	OUT	mtl_transactions_interface%ROWTYPE
			,o_error	OUT	VARCHAR2
			,i_Trans_Cost	in	Number) -- Add by Korn.iCE on 15-Sept.-2009: Desctination parameter from Main_Conc.
IS
BEGIN
	or_mtl_trx_inf	:=	NULL;
	BEGIN
		SELECT	Mtl_Material_Transactions_s.NEXTVAL
		INTO	or_mtl_trx_inf.transaction_interface_id
		FROM	DUAL;
	EXCEPTION WHEN OTHERS THEN
		o_error := 'Error: '||SQLERRM;
		RETURN;
	END;

	or_mtl_trx_inf.transaction_header_id		:=	i_trx_hdr_id;
	or_mtl_trx_inf.source_line_id			:=	FND_GLOBAL.CONC_REQUEST_ID;
	or_mtl_trx_inf.source_header_id			:=	-1;
	or_mtl_trx_inf.process_flag			:=	'1';
	or_mtl_trx_inf.transaction_mode			:=	3;
	or_mtl_trx_inf.lock_flag			:=	2;
	or_mtl_trx_inf.last_update_date			:=	SYSDATE;
	or_mtl_trx_inf.last_updated_by			:=	FND_GLOBAL.user_id;
	or_mtl_trx_inf.last_update_login		:=	NULL;
	or_mtl_trx_inf.creation_date			:=	SYSDATE;
	or_mtl_trx_inf.created_by			:=	FND_GLOBAL.user_id;
	or_mtl_trx_inf.Inventory_Item_Id		:=	NULL;
	or_mtl_trx_inf.transaction_source_id		:=	NULL;
	or_mtl_trx_inf.transaction_source_name		:=	ir_po_trx_line.packing_sim;
	or_mtl_trx_inf.transaction_date			:=	g_trx_date;
	or_mtl_trx_inf.transaction_source_type_id	:=	NULL;
	or_mtl_trx_inf.transaction_action_id		:=	NULL;
	or_mtl_trx_inf.reason_id			:=	NULL;
	or_mtl_trx_inf.transaction_reference		:=	NULL;
	or_mtl_trx_inf.attribute14              	:=	ir_Po_trx_line.ref_doc1 ;
	or_mtl_trx_inf.attribute15              	:=	ir_Po_trx_line.ref_doc2 ;
	--or_mtl_trx_inf.source_code			:=	ir_po_trx_line.packing_sim;

	IF i_trx_type = g_fg_source THEN  ----- FG
		or_mtl_trx_inf.source_code		:=	g_fg_source;
		or_mtl_trx_inf.transaction_type_id	:=	g_trx_type_fg_id;
		or_mtl_trx_inf.distribution_account_id	:=	g_wip_combine_id;

		or_mtl_trx_inf.item_segment1		:=	ir_po_trx_line.item_number;
		or_mtl_trx_inf.organization_id		:=	ir_po_trx_line.organization_id;
		or_mtl_trx_inf.transaction_quantity	:=	ir_po_trx_line.transaction_quantity;
		or_mtl_trx_inf.primary_quantity		:=	ir_po_trx_line.transaction_quantity;
		or_mtl_trx_inf.transaction_uom		:=	ir_po_trx_line.transaction_uom;
		or_mtl_trx_inf.subinventory_code	:=	ir_po_trx_line.subinventory;
		or_mtl_trx_inf.loc_segment1		:=	ir_po_trx_line.locator_code;


		or_mtl_trx_inf.transaction_cost		:=	i_Trans_Cost; --Added by Korn.iCE on 15-Sept-2009

		-- Comment by Korn.iCE on 15-Sept.-2009
		-- Reason: Move this Query to Main_Conc because Transaction_Cost should get only one time for FG and RM.
		/*
		BEGIN
			select	cst.item_cost
			INTO	or_mtl_trx_inf.transaction_cost
			from	mtl_system_items msi,
				cst_item_costs cst,
				mtl_parameters mp
			where	cst.organization_id	= mp.organization_id
			and	cst.cost_type_id	= mp.primary_cost_method
			and	cst.inventory_item_id	= msi.inventory_item_id
			and	cst.organization_id	= msi.organization_id
			and	msi.organization_id	= ir_po_trx_line.mtl_organization_id
			and	msi.segment1		= ir_po_trx_line.material_part;
		EXCEPTION WHEN OTHERS THEN
			or_mtl_trx_inf.transaction_cost := NULL ;
		End ;
		*/

	ELSIF i_trx_type = g_rm_source THEN ----- RM
		or_mtl_trx_inf.source_code		:=	g_rm_source;
		or_mtl_trx_inf.transaction_type_id	:=	g_trx_type_rm_id;
		or_mtl_trx_inf.distribution_account_id	:=	g_wip_combine_id;

		or_mtl_trx_inf.transaction_cost		:=	i_Trans_Cost; --Added by Korn.iCE on 15-Sept-2009
		--or_mtl_trx_inf.transaction_cost	:=	NULL; Commented by Korn.iCE on 15-Sept-2009

		or_mtl_trx_inf.item_segment1		:=	ir_po_trx_line.material_part;
		or_mtl_trx_inf.organization_id		:=	ir_po_trx_line.mtl_organization_id;
		or_mtl_trx_inf.transaction_quantity	:=	-1*(ir_po_trx_line.material_qty);
		or_mtl_trx_inf.primary_quantity		:=	-1*(ir_po_trx_line.material_qty);
		or_mtl_trx_inf.transaction_uom		:=	ir_po_trx_line.material_uom;
		or_mtl_trx_inf.subinventory_code	:=	ir_po_trx_line.material_subinventory;
		or_mtl_trx_inf.loc_segment1		:=	ir_po_trx_line.material_locator_code;

	ELSIF i_trx_type = g_cost_source THEN ----- ADJ
		or_mtl_trx_inf.source_code		:=	g_cost_source;
		or_mtl_trx_inf.transaction_type_id	:=	g_trx_type_packing_id;
		or_mtl_trx_inf.distribution_account_id  :=	g_accrued_combine_id;
		or_mtl_trx_inf.transaction_cost         :=	ir_po_trx_line.transaction_cost ;

		or_mtl_trx_inf.item_segment1		:=	ir_po_trx_line.item_number;
		or_mtl_trx_inf.organization_id		:=	ir_po_trx_line.organization_id;
		or_mtl_trx_inf.transaction_quantity	:=	ir_po_trx_line.transaction_quantity;
		or_mtl_trx_inf.primary_quantity		:=	ir_po_trx_line.transaction_quantity;
		or_mtl_trx_inf.transaction_uom		:=	ir_po_trx_line.transaction_uom;
		or_mtl_trx_inf.subinventory_code	:=	NULL ;
		or_mtl_trx_inf.loc_segment1		:=	NULL ;
	END IF;
	insert_trx_inf (ir_mtl_trx_inf	=>	or_mtl_trx_inf
			,o_error	=>	o_error);
EXCEPTION WHEN OTHERS THEN
	o_error		:=	'Error - '||SQLERRM;
END assign_trx_inf;
-----------------------------------------------------------------------------------------
PROCEDURE insert_serial_inf (ir_mtl_serial_inf IN mtl_serial_numbers_interface%ROWTYPE, o_error	OUT VARCHAR2)
IS
BEGIN
	INSERT INTO mtl_serial_numbers_interface
		(transaction_interface_id
		,source_code
		,source_line_id
		,last_update_date
		,last_updated_by
		,creation_date
		,created_by
		,last_update_login
		,fm_serial_number
		,to_serial_number
		,process_flag
		,attribute1
		,attribute2)
	VALUES (ir_mtl_serial_inf.transaction_interface_id
		,ir_mtl_serial_inf.source_code
		,ir_mtl_serial_inf.source_line_id
		,ir_mtl_serial_inf.last_update_date
		,ir_mtl_serial_inf.last_updated_by
		,ir_mtl_serial_inf.creation_date
		,ir_mtl_serial_inf.created_by
		,ir_mtl_serial_inf.last_update_login
		,ir_mtl_serial_inf.fm_serial_number
		,ir_mtl_serial_inf.to_serial_number
		,ir_mtl_serial_inf.process_flag
		,ir_mtl_serial_inf.attribute1
		,ir_mtl_serial_inf.attribute2);
EXCEPTION WHEN OTHERS THEN
	o_error := 'Error: '||SQLERRM;
END insert_serial_inf;
-----------------------------------------------------------------------------------------
PROCEDURE assign_serial_inf (i_trx_type			IN	VARCHAR2
				,ir_serial_trx_line	IN	serial_trx_type
				,ir_mtl_trx_inf		IN	mtl_transactions_interface%ROWTYPE
				,or_mtl_serial_inf	OUT	mtl_serial_numbers_interface%ROWTYPE
				,o_error		OUT	VARCHAR2)
IS
BEGIN
	or_mtl_serial_inf				:=	NULL;
	or_mtl_serial_inf.transaction_interface_id	:=	ir_mtl_trx_inf.transaction_interface_id;
	or_mtl_serial_inf.source_line_id		:=	ir_mtl_trx_inf.source_line_id;
	or_mtl_serial_inf.last_update_date		:=	ir_mtl_trx_inf.last_update_date;
	or_mtl_serial_inf.last_updated_by		:=	ir_mtl_trx_inf.last_updated_by;
	or_mtl_serial_inf.creation_date			:=	ir_mtl_trx_inf.creation_date;
	or_mtl_serial_inf.created_by			:=	ir_mtl_trx_inf.created_by;
	or_mtl_serial_inf.last_update_login		:=	ir_mtl_trx_inf.last_update_login;
	or_mtl_serial_inf.process_flag			:=	ir_mtl_trx_inf.process_flag;
	or_mtl_serial_inf.attribute1			:=	ir_serial_trx_line.tel_number;
	or_mtl_serial_inf.attribute2			:=	TO_CHAR(ir_serial_trx_line.expire_date,G_FORMAT_DATE);

	IF i_trx_type = g_fg_source THEN
		or_mtl_serial_inf.source_code		:=	g_fg_source;
	ELSIF i_trx_type = g_rm_source THEN
		or_mtl_serial_inf.source_code		:=	g_rm_source;
	END IF;
	or_mtl_serial_inf.fm_serial_number		:=	ir_serial_trx_line.fm_serial_number;
	or_mtl_serial_inf.to_serial_number		:=	ir_serial_trx_line.to_serial_number;

	insert_serial_inf(ir_mtl_serial_inf	   	=>	or_mtl_serial_inf
				,o_error		=>	o_error);
EXCEPTION
	WHEN OTHERS THEN o_error := 'Error: '||SQLERRM;
END assign_serial_inf;
-----------------------------------------------------------------------------------------
PROCEDURE validate_serial
	(i_item_id		IN	NUMBER
	,i_serial_no		IN	dtac_sim_receipt_interface.serial_no%TYPE
	,i_org_id		IN	NUMBER
	,i_subinv_code		IN	dtac_sim_receipt_interface.subinventory%TYPE
	,i_locator_id		IN	NUMBER
	,i_mtl_part_id		IN	NUMBER
	,i_mtl_serial_no	IN	dtac_sim_receipt_interface.material_serial_no%TYPE
	,i_mtl_org_id		IN	NUMBER
	,i_mtl_subinv_code	IN	dtac_sim_receipt_interface.material_subinventory%TYPE
	,i_mtl_locator_id	IN	NUMBER
	,i_rm_inf_id            IN	NUMBER
	,i_fg_inf_id            IN	NUMBER
	,o_err_item_type	OUT	VARCHAR2
	,o_status		OUT	VARCHAR2)
IS
	v_serial	NUMBER;
	v_sn_status	NUMBER;
	v_inf		VARCHAR2(1) ;
BEGIN
	----- FG Serial -----
	IF i_serial_no IS NOT NULL THEN
		BEGIN
			SELECT	COUNT(*),msn.current_status
			INTO	v_serial,v_sn_status
			FROM	mtl_serial_numbers	msn
			WHERE	msn.serial_number		=	i_serial_no
			AND	msn.inventory_item_id		=	i_item_id
			-- Start Comment by Korn.iCE on 15/05/2008 --
			--AND	msn.current_subinventory_code	=	i_subinv_code
			--AND	NVL(msn.current_locator_id,-999)=	NVL(i_locator_id,NVL(msn.current_locator_id,-999))
			--AND	msn.current_organization_id	=	i_org_id
			-- End Comment by Korn.iCE on 15/05/2008 --
			GROUP BY msn.current_status;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			v_serial	:=	0;
			v_sn_status	:=	4;
		END;

		IF v_serial > 0 AND v_sn_status <> 4 THEN
			o_status	:=	'Error validate FG Serial No = '||i_serial_no||', Current Status = '||v_sn_status;
			o_err_item_type	:=	'FG';
			RETURN;
		END IF;

		v_inf := 'N' ;
		BEGIN
			SELECT DISTINCT 'Y'
			INTO   v_inf
			FROM   mtl_serial_numbers_interface
			WHERE 	fm_serial_number            = i_serial_no ;
			--AND    to_serial_number            <= i_serial_no
		EXCEPTION WHEN OTHERS THEN
			v_inf := 'N' ;
		END ;

		IF  v_inf = 'Y'  THEN
			o_status	:=	'Error validate FG Serial No = '||i_serial_no||', Current Status = Waiting on interface ';
			o_err_item_type	:=	'FG';
			RETURN ;
		END IF ;
	END IF;

	----- RM Serial -----
	v_serial	:=	NULL;
	v_sn_status	:=	NULL;

	IF i_mtl_serial_no IS NOT NULL THEN
		BEGIN
			SELECT	COUNT(*),msn.current_status
			INTO	v_serial,v_sn_status
			FROM	mtl_serial_numbers	msn
			WHERE	msn.serial_number			=	i_mtl_serial_no
			AND	msn.inventory_item_id			=	i_mtl_part_id
			AND	msn.current_subinventory_code		=	i_mtl_subinv_code
			AND	NVL(msn.current_locator_id,-999)	=	NVL(i_mtl_locator_id,NVL(msn.current_locator_id,-999))
			AND	msn.current_organization_id		=	i_mtl_org_id
			GROUP BY msn.current_status;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			v_serial	:=	0;
		END;

		IF v_serial > 0 AND v_sn_status = 4  THEN
			o_status	:=	'Error validate RM Serial No = '||i_mtl_serial_no||', Current Status = '||v_sn_status;
			o_err_item_type	:=	'RM';
			RETURN;
		ELSIF v_serial = 0 THEN
			o_status	:=	'Error validate RM Serial No = '||i_mtl_serial_no||', Current Status = '||v_sn_status;
			o_err_item_type	:=	'RM';
			RETURN;
		END IF;

		v_inf := 'N' ;
		BEGIN
			SELECT DISTINCT 'Y'
			INTO   v_inf
			FROM   mtl_serial_numbers_interface
			WHERE 	fm_serial_number = i_mtl_serial_no ;
			--AND    to_serial_number <= i_mtl_serial_no
		EXCEPTION WHEN OTHERS THEN
			v_inf := 'N' ;
		END ;
		IF  v_inf = 'Y'  THEN
			o_status	:=	'Error validate FG Serial No = '||i_serial_no||', Current Status = Waiting on interface ';
			o_err_item_type	:=	'FG';
			RETURN ;
		END IF ;
	END IF;
EXCEPTION WHEN OTHERS THEN
	Write_log('Error validate FG Serial No : '||i_serial_no||' - '||SQLERRM);
	Write_log('Error validate RM Serial No = '||i_mtl_serial_no||' - '||SQLERRM);
	o_err_item_type	:=	NULL;
	o_status	:=	SQLERRM;
END validate_serial;
-----------------------------------------------------------------------------------------
/*
FUNCTION delete_sim_receipt_inf (i_row_id	IN	ROWID)
RETURN VARCHAR2
IS
	v_row_id	ROWID;
BEGIN  --!!
	DELETE	FROM	dtac_sim_receipt_interface
	WHERE 	ROWID = i_row_id;
	RETURN	NULL;
EXCEPTION WHEN OTHERS THEN
	RETURN ('Error: '||SQLERRM);
END delete_sim_receipt_inf;
*/
-----------------------------------------------------------------------------------------
FUNCTION write_db_error (i_row_id IN ROWID, i_err_message IN VARCHAR2) RETURN VARCHAR2
IS
BEGIN
	UPDATE	dtac_sim_receipt_interface
	SET	process_flag = 'E',
		message_code = SUBSTR(i_err_message,1,100)
	WHERE 	ROWID = i_row_id;

	RETURN	NULL;

EXCEPTION WHEN OTHERS THEN
	RETURN ('Error: '||SQLERRM);
END write_db_error;
-----------------------------------------------------------------------------------------
PROCEDURE insert_rcv_header_inf (ir_rcv_header_inf	IN		rcv_headers_interface%ROWTYPE
						 		,o_error			OUT		VARCHAR2)
IS
BEGIN
	o_error	:=	NULL;
	INSERT INTO rcv_headers_interface
				(header_interface_id
				,group_id
				,processing_status_code
				,auto_transact_code
				,receipt_source_code
				,shipment_num
				,shipped_date
				,transaction_type
				,last_update_date
				,last_updated_by
				,creation_date
				,created_by
				,ship_to_organization_code
				,vendor_num
				,expected_receipt_date
				,validation_flag
				,vendor_id
				,vendor_site_id
            	,ship_to_organization_id
            	,location_id
            	,employee_id)

	VALUES		(ir_rcv_header_inf.header_interface_id
				,ir_rcv_header_inf.group_id
				,ir_rcv_header_inf.processing_status_code
				,ir_rcv_header_inf.auto_transact_code
				,ir_rcv_header_inf.receipt_source_code
				,ir_rcv_header_inf.shipment_num
				,ir_rcv_header_inf.shipped_date
				,ir_rcv_header_inf.transaction_type
				,ir_rcv_header_inf.last_update_date
				,ir_rcv_header_inf.last_updated_by
				,ir_rcv_header_inf.creation_date
				,ir_rcv_header_inf.created_by
				,ir_rcv_header_inf.ship_to_organization_code
				,ir_rcv_header_inf.vendor_num
				,ir_rcv_header_inf.expected_receipt_date
				,ir_rcv_header_inf.validation_flag
				,ir_rcv_header_inf.vendor_id
				,ir_rcv_header_inf.vendor_site_id
            	,ir_rcv_header_inf.ship_to_organization_id
            	,ir_rcv_header_inf.location_id
            	,ir_rcv_header_inf.employee_id);
EXCEPTION
	WHEN OTHERS THEN o_error :=  'Error: '||SQLERRM;
END insert_rcv_header_inf;
-----------------------------------------------------------------------------------------
PROCEDURE assign_rcv_header_inf (ir_po_trx_inf		  	IN		rcv_trx_type
								                ,i_rcv_grp_id			    IN		NUMBER
								                ,or_rcv_header_inf		OUT		rcv_headers_interface%ROWTYPE
								                ,o_error				      OUT		VARCHAR2)
IS
	v_header_interface_id		NUMBER;
BEGIN
	or_rcv_header_inf	:=	NULL;
	o_error				    :=	NULL;
	BEGIN
		SELECT	rcv_headers_interface_s.nextval
		INTO	v_header_interface_id
		FROM	dual;
	EXCEPTION
		WHEN OTHERS THEN o_error := 'Error: '||SQLERRM;
	END;

	or_rcv_header_inf.group_id 					             :=	i_rcv_grp_id;
	or_rcv_header_inf.header_interface_id 		       :=	v_header_interface_id;
	or_rcv_header_inf.processing_status_code	       :=	'PENDING';
	or_rcv_header_inf.auto_transact_code		         :=	'RECEIVE';--'DELIVER';
	or_rcv_header_inf.receipt_source_code		         :=	'VENDOR';
	or_rcv_header_inf.shipment_num				           :=	ir_po_trx_inf.shipment_num;
	or_rcv_header_inf.shipped_date				           :=	SYSDATE;
	or_rcv_header_inf.transaction_type			         :=	'NEW';
	or_rcv_header_inf.last_update_date			         :=	SYSDATE;
	or_rcv_header_inf.last_updated_by			           :=	FND_GLOBAL.user_id;
	or_rcv_header_inf.creation_date				           :=	SYSDATE;
	or_rcv_header_inf.created_by				             :=	FND_GLOBAL.user_id;
	or_rcv_header_inf.ship_to_organization_code	     :=	ir_po_trx_inf.ship_to_organization_code;
	or_rcv_header_inf.vendor_num				             :=	ir_po_trx_inf.vendor_num;
	or_rcv_header_inf.expected_receipt_date		       :=	SYSDATE;
	or_rcv_header_inf.validation_flag			           :=	'Y';
	or_rcv_header_inf.vendor_id					             :=	ir_po_trx_inf.vendor_id;
	or_rcv_header_inf.vendor_site_id			           :=	ir_po_trx_inf.vendor_site_id;
	or_rcv_header_inf.ship_to_organization_id	       :=	ir_po_trx_inf.ship_to_organization_id;
	or_rcv_header_inf.location_id				             :=	ir_po_trx_inf.ship_to_location_id;
	or_rcv_header_inf.employee_id				             :=	ir_po_trx_inf.employee_id;
	--Don't insert to rcv_headers_interface
	insert_rcv_header_inf (ir_rcv_header_inf	=>	or_rcv_header_inf
    					,o_error				=>	o_error);
EXCEPTION
	WHEN OTHERS THEN o_error := 'Error: '||SQLERRM;
END assign_rcv_header_inf;
-----------------------------------------------------------------------------------------
PROCEDURE insert_rcv_trx_inf (ir_rcv_trx_inf	IN		rcv_transactions_interface%ROWTYPE
						 	 ,o_error			OUT		VARCHAR2)
IS
BEGIN
	o_error	:=	NULL;
	INSERT INTO rcv_transactions_interface
				(interface_transaction_id
				,header_interface_id
				,group_id
				,last_update_date
				,last_updated_by
				,creation_date
				,created_by
				,transaction_type
				,transaction_date
				,processing_status_code
				,processing_mode_code
				,transaction_status_code
				,quantity
				,unit_of_measure
				,auto_transact_code
				,receipt_source_code
				,destination_type_code
				,vendor_num
				,source_document_code
				,document_num
				,document_line_num
				,document_shipment_line_num
				,ship_to_location_id
				,validation_flag
				,shipment_num
				,category_id
        ,interface_source_code
        ,item_description
        ,uom_code
        ,employee_id
        ,primary_quantity
        ,primary_unit_of_measure
        ,vendor_id
        ,vendor_site_id
        ,to_organization_id
        ,po_header_id
        ,po_line_id
        ,po_line_location_id
        ,po_unit_price
        ,currency_code
    	  ,location_id
				,inspection_status_code
				,destination_context
				,source_doc_quantity
				,source_doc_unit_of_measure)

	VALUES		(ir_rcv_trx_inf.interface_transaction_id
				,ir_rcv_trx_inf.header_interface_id
				,ir_rcv_trx_inf.group_id
				,ir_rcv_trx_inf.last_update_date
				,ir_rcv_trx_inf.last_updated_by
				,ir_rcv_trx_inf.creation_date
				,ir_rcv_trx_inf.created_by
				,ir_rcv_trx_inf.transaction_type
				,ir_rcv_trx_inf.transaction_date
				,ir_rcv_trx_inf.processing_status_code
				,ir_rcv_trx_inf.processing_mode_code
				,ir_rcv_trx_inf.transaction_status_code
				,ir_rcv_trx_inf.quantity
				,ir_rcv_trx_inf.unit_of_measure
				,ir_rcv_trx_inf.auto_transact_code
				,ir_rcv_trx_inf.receipt_source_code
				,ir_rcv_trx_inf.destination_type_code
				,ir_rcv_trx_inf.vendor_num
				,ir_rcv_trx_inf.source_document_code
				,ir_rcv_trx_inf.document_num
				,ir_rcv_trx_inf.document_line_num
				,ir_rcv_trx_inf.document_shipment_line_num
				,ir_rcv_trx_inf.ship_to_location_id
				,ir_rcv_trx_inf.validation_flag
				,ir_rcv_trx_inf.shipment_num
				,ir_rcv_trx_inf.category_id
        ,ir_rcv_trx_inf.interface_source_code
        ,ir_rcv_trx_inf.item_description
        ,ir_rcv_trx_inf.uom_code
        ,ir_rcv_trx_inf.employee_id
        ,ir_rcv_trx_inf.primary_quantity
        ,ir_rcv_trx_inf.primary_unit_of_measure
        ,ir_rcv_trx_inf.vendor_id
        ,ir_rcv_trx_inf.vendor_site_id
        ,ir_rcv_trx_inf.to_organization_id
        ,ir_rcv_trx_inf.po_header_id
        ,ir_rcv_trx_inf.po_line_id
        ,ir_rcv_trx_inf.po_line_location_id
        ,ir_rcv_trx_inf.po_unit_price
        ,ir_rcv_trx_inf.currency_code
        ,ir_rcv_trx_inf.location_id
				,ir_rcv_trx_inf.inspection_status_code
				,ir_rcv_trx_inf.destination_context
				,ir_rcv_trx_inf.source_doc_quantity
				,ir_rcv_trx_inf.source_doc_unit_of_measure);
EXCEPTION
	WHEN OTHERS THEN o_error :=  'Error: '||SQLERRM;
END insert_rcv_trx_inf;
-----------------------------------------------------------------------------------------
PROCEDURE assign_rcv_trx_inf (ir_po_trx_inf		IN		rcv_trx_type
							 ,ir_rcv_header_inf	IN		rcv_headers_interface%ROWTYPE
							 ,or_rcv_trx_inf	OUT		rcv_transactions_interface%ROWTYPE
							 ,o_error			OUT		VARCHAR2)
IS
	v_trx_interface		NUMBER;
BEGIN
	or_rcv_trx_inf	:=	NULL;
	o_error			:=	NULL;
	BEGIN
		SELECT	rcv_transactions_interface_s.nextval
		INTO	v_trx_interface
		FROM	DUAL;
	EXCEPTION
		WHEN OTHERS THEN o_error :=  'Error: '||SQLERRM;
	END;
	or_rcv_trx_inf.interface_transaction_id		        :=	v_trx_interface;
	or_rcv_trx_inf.header_interface_id			          :=	ir_rcv_header_inf.header_interface_id;
	or_rcv_trx_inf.group_id						                :=	ir_rcv_header_inf.group_id;
	or_rcv_trx_inf.last_update_date				            :=	SYSDATE;
	or_rcv_trx_inf.last_updated_by				            :=	FND_GLOBAL.user_id;
	or_rcv_trx_inf.creation_date				              :=	SYSDATE;
	or_rcv_trx_inf.created_by					                :=	FND_GLOBAL.user_id;
	or_rcv_trx_inf.transaction_type				            :=	'RECEIVE';
	or_rcv_trx_inf.transaction_date				            :=	SYSDATE;
	or_rcv_trx_inf.processing_status_code		          :=	'PENDING';
	or_rcv_trx_inf.processing_mode_code			          :=	'BATCH';
	or_rcv_trx_inf.transaction_status_code		        :=	'PENDING';
	or_rcv_trx_inf.quantity						                :=	ir_po_trx_inf.quantity;
	or_rcv_trx_inf.unit_of_measure				            :=	ir_po_trx_inf.unit_of_measure;
	or_rcv_trx_inf.auto_transact_code			            :=	'RECEIVE';--'DELIVER';
	or_rcv_trx_inf.receipt_source_code			          :=	'VENDOR';
--	or_rcv_trx_inf.destination_type_code		:=	'EXPEND';
	or_rcv_trx_inf.vendor_num					                :=	ir_po_trx_inf.vendor_num;
	or_rcv_trx_inf.source_document_code			          :=	'PO';
	or_rcv_trx_inf.document_num					              :=	ir_po_trx_inf.document_num;
	or_rcv_trx_inf.document_line_num			            :=	1;
	or_rcv_trx_inf.document_shipment_line_num	        :=	1;
	or_rcv_trx_inf.ship_to_location_id			          :=	ir_po_trx_inf.ship_to_location_id;
--	or_rcv_trx_inf.validation_flag				:=	'Y';
	or_rcv_trx_inf.shipment_num					              :=	ir_po_trx_inf.shipment_num;
	or_rcv_trx_inf.category_id					              :=	ir_po_trx_inf.category_id;
	or_rcv_trx_inf.interface_source_code		          :=	'RCV';
	or_rcv_trx_inf.item_description				            :=	ir_po_trx_inf.item_description;
	or_rcv_trx_inf.uom_code						                :=	ir_po_trx_inf.unit_of_measure;
	or_rcv_trx_inf.employee_id					              :=	ir_po_trx_inf.employee_id;
	or_rcv_trx_inf.primary_quantity				            :=	ir_po_trx_inf.quantity;
	or_rcv_trx_inf.primary_unit_of_measure		        :=	ir_po_trx_inf.unit_of_measure;
  or_rcv_trx_inf.vendor_id					                :=	ir_po_trx_inf.vendor_id;
	or_rcv_trx_inf.vendor_site_id				              :=	ir_po_trx_inf.vendor_site_id;
	or_rcv_trx_inf.to_organization_id			            :=	ir_po_trx_inf.ship_to_organization_id;
  or_rcv_trx_inf.po_header_id					              :=	ir_po_trx_inf.po_header_id;
  or_rcv_trx_inf.po_line_id					                :=	ir_po_trx_inf.po_line_id;
  or_rcv_trx_inf.po_line_location_id			          :=	ir_po_trx_inf.line_location_id;
  or_rcv_trx_inf.po_unit_price				              :=	ir_po_trx_inf.unit_price;
  or_rcv_trx_inf.currency_code				              :=	ir_po_trx_inf.currency_code;
	or_rcv_trx_inf.location_id					              :=	ir_po_trx_inf.ship_to_location_id;
	or_rcv_trx_inf.inspection_status_code		          :=	'NOT INSPECTED';
	or_rcv_trx_inf.destination_type_code	            :=	NULL ; --'RECEIVING';
	or_rcv_trx_inf.destination_context			          :=	'RECEIVING';
	or_rcv_trx_inf.source_doc_quantity			          :=	ir_po_trx_inf.quantity;
	or_rcv_trx_inf.source_doc_unit_of_measure	        :=	ir_po_trx_inf.unit_of_measure;
  or_rcv_trx_inf.validation_flag                    :=  'Y' ;
	insert_rcv_trx_inf	(ir_rcv_trx_inf	=>	or_rcv_trx_inf
    					,o_error		=>	o_error);
EXCEPTION
	WHEN OTHERS THEN o_error := 'Error: '||SQLERRM;
END assign_rcv_trx_inf;
-----------------------------------------------------------------------------------------
PROCEDURE main_conc
(err_msg		OUT	VARCHAR2
,err_code		OUT	VARCHAR2
,i_source_company	IN	VARCHAR2
,i_source_name		IN	VARCHAR2
,i_organization_id	IN	NUMBER
,i_trx_date		IN	VARCHAR2
,i_trx_source		IN	VARCHAR2
,i_trx_type_rm_id	IN	NUMBER
,i_trx_type_fg_id	IN	NUMBER
,i_trx_type_packing_id	IN	NUMBER
,i_wip_combine_id	IN	NUMBER
,i_accrued_combine_id	IN	NUMBER )
--,o_status		OUT	VARCHAR2)
IS
	r_po_trx_line		po_trx_type;
	r_serial_trx_line	serial_trx_type;
	r_fg_mtl_trx_inf	mtl_transactions_interface%ROWTYPE;
	r_rm_mtl_trx_inf	mtl_transactions_interface%ROWTYPE;
	r_cost_adj_mtl_trx_inf	mtl_transactions_interface%ROWTYPE;
	r_mtl_serial_inf	mtl_serial_numbers_interface%ROWTYPE;
	r_po_trx_inf		rcv_trx_type;
	r_rcv_header_inf	rcv_headers_interface%ROWTYPE;
	r_rcv_trx_inf		rcv_transactions_interface%ROWTYPE;
	v_error			VARCHAR2(200);
	v_comp_record		NUMBER	:=	0;
	v_err_record		NUMBER	:=	0;
	v_adj_comp_record	NUMBER	:=	0;
	v_adj_err_record	NUMBER	:=	0;
	v_po_record		NUMBER	:=	0;
	l_processing_mode_code	VARCHAR2(15) := 'BATCH' ;
	v_group			NUMBER := 0 ;
	v_group_char		VARCHAR2(100);
	v_rm_oh			NUMBER := 0 ;
	v_itf_flag		VARCHAR2(1) := 'Y' ;

	CURSOR c_group_interface (i_req_id            	NUMBER ) IS
		SELECT
		sr.source				source_company,
		sr.transaction_source_name		source_name,
		NVL(g_trx_source,sr.packing_sim)	packing_sim,
		sr.item_segment1			item_number,
		sum(sr.transaction_quantity)		TRANSACTION_QUANTITY,
		sr.transaction_uom,
		sr.organization_code,
		sr.subinventory,
		sr.locator_code,
		sr.material_part,
		SUM(sr.material_qty)			MATERIAL_QTY,
		sr.material_uom,
		sr.material_organization_code,
		sr.material_subinventory,
		sr.material_locator_code,
		mp.organization_id			organization_id,
		mp2.organization_id			mtl_organization_id,
		MIN(sr.serial_no)			fm_serial_no,
		MAX(sr.serial_no)			to_serial_no,
		MIN(sr.material_serial_no)		fm_mtl_serial_no,
		MAX(sr.material_serial_no)		to_mtl_serial_no,
		sr.ref_doc1				ref_doc1,
		sr.ref_doc2				ref_doc2,
		COUNT(sr.serial_no)			fg_serial_record,
		COUNT(sr.material_serial_no)		rm_serial_record
		FROM	dtac_sim_receipt_interface	sr,
		mtl_parameters				mp,
		mtl_parameters				mp2
		WHERE	sr.request_id			= i_req_id
		AND	sr.organization_code		= mp.organization_code(+)
		AND	sr.material_organization_code	= mp2.organization_code(+)
		GROUP BY
		sr.SOURCE,
		sr.transaction_source_name,
		NVL(g_trx_source,sr.packing_sim),
		sr.item_segment1,
		sr.transaction_uom,
		sr.organization_code,
		sr.subinventory,
		sr.locator_code,
		sr.material_part,
		sr.material_qty,
		sr.material_uom,
		sr.material_organization_code,
		sr.material_subinventory,
		sr.material_locator_code,
		mp.organization_id,
		mp2.organization_id ,
		sr.ref_doc1 ,
		sr.ref_doc2  ;

	CURSOR   c_err_trx_itf (i_req_id NUMBER)	IS
		SELECT	sr.transaction_id, mti.error_explanation
		FROM	dtac_sim_receipt_interface sr, mtl_transactions_interface mti
		WHERE	sr.request_id		= i_req_id
		AND	sr.transaction_id	= mti.transaction_interface_id
		AND	mti.process_flag	= 3
		GROUP BY sr.transaction_id,mti.error_explanation;

	CURSOR c_cost_adj_itf	(i_req_id	NUMBER)	IS
		SELECT	t.item_number
		,t.organization_code
		,t.organization_id
		,NVL(g_trx_source,t.packing_sim)	packing_sim
		,SUM(t.transaction_cost)		transaction_cost
		,MAX(t.transaction_uom)			transaction_uom
		FROM(
			SELECT
			sr.item_segment1	item_number,
			sr.organization_code,
			sr.packing_sim,
			mp.organization_id,
			NVL(pl.unit_price*sr.transaction_quantity,0)	transaction_cost,
			sr.transaction_uom	transaction_uom
			FROM	dtac_sim_receipt_interface	sr,
			po_headers_v				ph,
			po_lines_v				pl,
			mtl_parameters				mp
			WHERE	sr.Ref_Doc1		= ph.segment1
			AND	ph.po_header_id		= pl.po_header_id(+)
			AND	sr.organization_code	= mp.organization_code
			AND	sr.request_id		= i_req_id
			AND	sr.process_flag		= 'Y'
			UNION ALL
			SELECT
			sr.item_segment1,
			sr.organization_code,
			sr.packing_sim,
			mp.organization_id,
			NVL(pl.unit_price*sr.repack,0)	transaction_cost,
			sr.transaction_uom		transaction_uom
			FROM 	dtac_sim_receipt_interface	sr,
			po_headers_v				ph,
			po_lines_v				pl,
			mtl_parameters				mp
			WHERE	sr.Ref_Doc2		= ph.segment1
			AND	ph.po_header_id		= pl.po_header_id(+)
			AND	sr.organization_code	= mp.organization_code
			AND	sr.request_id		= i_req_id
			AND	sr.process_flag		= 'Y'
		)	t
		GROUP BY t.item_number
		,t.organization_code
		,t.organization_id
		,NVL(g_trx_source,t.packing_sim);

	CURSOR c_onhand_qty (i_item_number VARCHAR2, i_org_code VARCHAR2) IS
		SELECT SUM(sum_qty) sum_qty
		FROM
		(
			SELECT	NVL(SUM(NVL(moq.transaction_quantity,0)),0) 	sum_qty
			FROM	mtl_onhand_quantities	moq
			,mtl_parameters		  	mp
			,mtl_system_items_vl		msi
			--,mtl_item_locations_kfv mil
			WHERE	moq.organization_id	=	mp.organization_id
			AND	moq.inventory_item_id	=	msi.inventory_item_id
			AND	moq.organization_id	=	msi.organization_id
			--AND	moq.LOCATOR_ID		=	mil.inventory_location_id(+)
			AND	mp.organization_code	=	i_org_code
			AND	msi.segment1		=	i_item_number
			--AND	NVL(mil.concatenated_segments,'X')  = NVL(i_locator,NVL(mil.concatenated_segments,'X'))
			UNION ALL
			SELECT	SUM(mti.primary_quantity)
			FROM	mtl_transactions_interface	mti
			,mtl_parameters				mp
			,mtl_system_items_vl			msi
			WHERE	mti.organization_id	=	mp.organization_id
			AND	mti.inventory_item_id	=	msi.inventory_item_id
			AND	mti.organization_id	=	msi.organization_id
			-- AND	moq.LOCATOR_ID		=	mil.inventory_location_id(+)
			AND	mp.organization_code	=	i_org_code
			AND	msi.segment1		=	i_item_number
			UNION ALL
			SELECT	SUM(mti.primary_quantity)
			FROM	mtl_material_transactions_temp  mti
			,mtl_parameters				mp
			,mtl_system_items_vl			msi
			WHERE	mti.organization_id	=	mp.organization_id
			AND	mti.inventory_item_id	=	msi.inventory_item_id
			AND	mti.organization_id	=	msi.organization_id
			-- AND	moq.LOCATOR_ID		=	mil.inventory_location_id(+)
			AND	mp.organization_code	=	i_org_code
			AND	msi.segment1		=	i_item_number
		);

	CURSOR c_rm_oh (i_warehouse_id NUMBER, i_item_id NUMBER, i_subinv VARCHAR2, i_locator_id NUMBER ) IS
		SELECT	SUM(sum_qty) sum_qty
		FROM
		(
			SELECT	NVL(SUM(NVL(moq.transaction_quantity,0)),0) 	sum_qty
			FROM	mtl_onhand_quantities	moq
			WHERE	moq.organization_id	=	i_warehouse_id
			AND	moq.inventory_item_id	=	i_item_id
			AND	moq.SUBINVENTORY_CODE	=	i_subinv
			AND	NVL(moq.LOCATOR_ID,-99)	=	NVL(i_locator_id,NVL(moq.LOCATOR_ID,-99))
			UNION ALL
			SELECT	SUM(moq.primary_quantity)
			FROM	mtl_transactions_interface	moq
			WHERE	moq.organization_id	=	i_warehouse_id
			AND	moq.inventory_item_id	=	i_item_id
			AND	moq.SUBINVENTORY_CODE	=	i_subinv
			AND	NVL(moq.LOCATOR_ID,-99)	=	NVL(i_locator_id,NVL(moq.LOCATOR_ID,-99))
			UNION ALL
			SELECT	SUM(moq.primary_quantity)
			FROM	mtl_material_transactions_temp	moq
			WHERE	moq.organization_id	=	i_warehouse_id
			AND	moq.inventory_item_id	=	i_item_id
			AND	moq.SUBINVENTORY_CODE	=	i_subinv
			AND	NVL(moq.LOCATOR_ID,-99)	=	NVL(i_locator_id,NVL(moq.LOCATOR_ID,-99))
		);

	CURSOR c_rcv_trx_inf (i_req_id	NUMBER)	IS
		SELECT
		sr.transaction_source_name,
		mp.default_cost_group_id,
		sr.ref_doc1	document_num,
		DECODE(sr.ref_doc1,NULL,NULL,sr.ref_doc1||TO_CHAR(SYSDATE,'YYMMDD'))	shipment_num,
		SUM(NVL(sr.transaction_quantity,0))	quantity,
		pv.segment1	vendor_num,
		ph.agent_id	employee_id,
		pv.vendor_id,
		ph.po_header_id,
		ph.currency_code,
		ph.vendor_site_id
		FROM	dtac_sim_receipt_interface	sr,
			po_headers_v			ph,
			po_vendors			pv,
			mtl_parameters			mp
		WHERE	sr.Ref_Doc1		= ph.segment1
		AND	ph.vendor_id 		= pv.vendor_id(+)
		AND	sr.organization_code	= mp.organization_code(+)
		AND	sr.request_id		= i_req_id
		AND	sr.process_flag		= 'Y'
		GROUP BY
		sr.transaction_source_name,
		mp.default_cost_group_id,
		sr.ref_doc1,
		DECODE(sr.ref_doc1,NULL,NULL,sr.ref_doc1||TO_CHAR(SYSDATE,'YYMMDD')),
		pv.segment1,
		ph.agent_id,
		pv.vendor_id,
		ph.po_header_id,
		ph.currency_code,
		ph.vendor_site_id
		UNION ALL
		SELECT
		sr.transaction_source_name,
		mp.default_cost_group_id,
		sr.ref_doc2	document_num,
		DECODE(sr.ref_doc2,NULL,NULL,sr.ref_doc2||TO_CHAR(SYSDATE,'YYMMDD'))	shipment_num,
		SUM(NVL(sr.repack,0))	quantity,
		pv.segment1		vendor_num,
		ph.agent_id		employee_id,
		pv.vendor_id,
		ph.po_header_id,
		ph.currency_code,
		ph.vendor_site_id
		FROM	dtac_sim_receipt_interface	sr,
			po_headers_v			ph,
			po_vendors			pv,
			mtl_parameters			mp
			--mtl_parameters		mp2,
			--po_lines_v			pl,
			--po_line_locations_all		pll
		WHERE	sr.Ref_Doc2		= ph.segment1(+)
		AND	ph.vendor_id		= pv.vendor_id(+)
		AND	sr.organization_code	= mp.organization_code(+)
		AND	sr.request_id		= i_req_id
		AND	sr.process_flag		= 'Y'
		GROUP BY
		sr.transaction_source_name,
		mp.default_cost_group_id,
		sr.ref_doc2,
		DECODE(sr.ref_doc2,NULL,NULL,sr.ref_doc2||TO_CHAR(SYSDATE,'YYMMDD')),
		pv.segment1,
		ph.agent_id,
		pv.vendor_id,
		ph.po_header_id,
		ph.currency_code,
		ph.vendor_site_id;

	CURSOR c_rcv_del_trx_inf (i_po_header_id	NUMBER)	IS
		SELECT
		pll.ship_to_organization_id	organization_id,
		mp2.organization_code		ship_to_organization_code,
		pl.unit_meas_lookup_code	unit_of_measure,
		pll.ship_to_location_id		ship_to_location_id,
		pl.category_id,
		pl.item_description,
		pl.po_line_id,
		pll.line_location_id,
		pl.unit_price
		FROM	po_lines_v		pl,
			po_line_locations_all	pll,
			mtl_parameters		mp2
		WHERE	pl.po_header_id			= i_po_header_id
		AND	pl.po_line_id 			= pll.po_line_id
		AND	pll.ship_to_organization_id	= mp2.organization_id
		AND	pl.cancel_flag			= 'N'
		ORDER BY pl.line_num;

	CURSOR c_chk_item	(p_org_id	IN	mtl_system_items_b.organization_id%TYPE
				,p_item_number	IN	mtl_system_items_b.segment1%TYPE)	IS
		SELECT	msib.inventory_item_id
		FROM	mtl_system_items_b msib
		WHERE	msib.organization_id	=	p_org_id
		AND	msib.segment1		=	p_item_number;

	CURSOR c_chk_subinv	(p_org_id	IN	mtl_secondary_inventories.organization_id%TYPE
				,p_subinv_code	IN	mtl_secondary_inventories.secondary_inventory_name%TYPE) IS
		SELECT	msi.secondary_inventory_name
		FROM	mtl_secondary_inventories msi
		WHERE	msi.organization_id		=	p_org_id
		AND	msi.secondary_inventory_name	=	p_subinv_code;

	CURSOR c_chk_locator	(p_org_id	IN	mtl_item_locations.organization_id%TYPE
				,p_subinv_code	IN	mtl_item_locations.subinventory_code%TYPE
				,p_locator	IN	mtl_item_locations.segment1%TYPE) IS
		SELECT	mil.inventory_location_id
		FROM	mtl_item_locations	mil
		WHERE	mil.organization_id	=	p_org_id
		AND	mil.subinventory_code	=	p_subinv_code
		AND	mil.segment1		=	p_locator;

	-- trx_sp     VARCHAR2(100) := 'TRX_SP';
	v_fg_trx_hdr_id      		NUMBER ;
	v_rm_trx_hdr_id      		NUMBER ;
	v_cost_adj_trx_hdr_id		NUMBER ;
	--v_req_id			NUMBER := FND_GLOBAL.CONC_REQUEST_ID ;
	v_fg_req_id			NUMBER;
	v_rm_req_id			NUMBER;
	v_cost_adj_req_id		NUMBER;
	v_po_req_id			NUMBER;
	l_rcv_grp_id			NUMBER ;
	v_err_flag			BOOLEAN;
	v_cost_adj_qty			NUMBER;
	v_cost_adj_amt			NUMBER;
	v_fg_serial_comp		NUMBER	:=	0;
	v_fg_serial_err			NUMBER	:=	0;
	v_rm_serial_comp		NUMBER	:=	0;
	v_rm_serial_err			NUMBER	:=	0;
	v_inventory_item_id		mtl_system_items_b.inventory_item_id%TYPE;
	v_secondary_inventory_name	mtl_secondary_inventories.secondary_inventory_name%TYPE;
	v_inventory_location_id		mtl_item_locations.inventory_location_id%TYPE;
	v_det_tab			det_tbl ;
	v_err_rm_item			VARCHAR2(4000);
	v_err_fg_item			VARCHAR2(4000);
	v_status			VARCHAR2(1000);
	v_inv_item_id			NUMBER;
	v_mtl_inv_item_id		NUMBER;
	v_locator_id			NUMBER;
	v_mtl_locator_id		NUMBER;
	v_org_id			NUMBER;
	v_mtl_org_id			NUMBER;
	v_item_type			VARCHAR2(10);
  v_org_code      VARCHAR2(10);

	-- Add by Korn.iCE on 15-Sept.-2009
	-- Reason: For support Transaction_Cost and send parameter to Assign_TRX_INF.
	v_Trans_Cost			Number := 0;

BEGIN
	---------------------------------- Main Concurrent ---------------------------
	write_log('+---------------------------------------------------------------------------+');
	write_log('Start SIM Prodution Interface. ');
	write_log('Start Date: '||TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SS'));
	write_log('+---------------------------------------------------------------------------+');
	write_log('');

	fnd_file.put_line	(fnd_file.log,
				'Parameter.'
				||chr(10)||'i_source_company		=> '||i_source_company
				||chr(10)||'i_source_name	 	=> '||i_source_name
				||chr(10)||'i_organization_id		=> '||TO_CHAR(i_organization_id)
				||chr(10)||'i_trx_date 			=> '||i_trx_date
				||chr(10)||'i_trx_source		=> '||i_trx_source
				||chr(10)||'i_trx_type_rm_id		=> '||TO_CHAR(i_trx_type_rm_id)
				||chr(10)||'i_trx_type_fg_id		=> '||TO_CHAR(i_trx_type_fg_id)
				||chr(10)||'i_trx_type_packing_id	=> '||TO_CHAR(i_trx_type_packing_id)
				||chr(10)||'i_wip_combine_id		=> '||TO_CHAR(i_wip_combine_id)
				||chr(10)||'i_accrued_combine_id	=> '||TO_CHAR(i_accrued_combine_id));

	fnd_file.put_line	(fnd_file.output,
				'Parameter.'
				||chr(10)||'i_source_company		=> '||i_source_company
				||chr(10)||'i_source_name	 	=> '||i_source_name
				||chr(10)||'i_organization_id		=> '||TO_CHAR(i_organization_id)
				||chr(10)||'i_trx_date 			=> '||i_trx_date
				||chr(10)||'i_trx_source		=> '||i_trx_source
				||chr(10)||'i_trx_type_rm_id		=> '||TO_CHAR(i_trx_type_rm_id)
				||chr(10)||'i_trx_type_fg_id		=> '||TO_CHAR(i_trx_type_fg_id)
				||chr(10)||'i_trx_type_packing_id	=> '||TO_CHAR(i_trx_type_packing_id)
				||chr(10)||'i_wip_combine_id		=> '||TO_CHAR(i_wip_combine_id)
				||chr(10)||'i_accrued_combine_id	=> '||TO_CHAR(i_accrued_combine_id)) ;

	write_log('');
	write_log('+---------------------------------------------------------------------------+');
	write_log('Error Code description.');
	write_log('     '||C_FG_ORG_ERROR_MESSAGE);
	write_log('     '||C_FG_ITEM_ERROR_MESSAGE);
	write_log('     '||C_FG_SUBINV_ERROR_MESSAGE);
	write_log('     '||C_FG_LOCATOR_ERROR_MESSAGE);
	write_log('     '||C_RM_ORG_ERROR_MESSAGE);
	write_log('     '||C_RM_ITEM_ERROR_MESSAGE);
	write_log('     '||C_RM_SUBINV_ERROR_MESSAGE);
	write_log('     '||C_RM_LOCATOR_ERROR_MESSAGE);
	write_log('     '||C_RM_ONHAND_ERROR_MESSAGE);
	write_log('+---------------------------------------------------------------------------+');

	g_source_company	:=	i_source_company;
	g_source_name		:=	i_source_name;
	g_organization_id	:=	i_organization_id;
	g_trx_date		:=	TO_DATE(i_trx_date,'YYYY/MM/DD HH24:MI:SS');
	g_trx_source		:=	i_trx_source;
	g_trx_type_rm_id	:=	i_trx_type_rm_id;
	g_trx_type_fg_id	:=	i_trx_type_fg_id;
	g_trx_type_packing_id	:=	i_trx_type_packing_id;
  
  SELECT MIN(organization_code) INTO v_org_code 
  FROM DTAC_SIM_RECEIPT_INTERFACE 
  WHERE	SOURCE = i_source_company
  AND	transaction_source_name	= i_source_name;

  
  IF v_org_code not like 'A%' THEN 
    g_wip_combine_id	:=	119278;
  else
  	g_wip_combine_id	:=	i_wip_combine_id;  --1007
  END IF;
  
	g_accrued_combine_id	:=	i_accrued_combine_id;
	--RETURN ;   --- !!@@

---------------------------------------------------------
	SELECT	mtl_material_transactions_s.nextval
	INTO	v_fg_trx_hdr_id
	FROM	dual;

	SELECT	mtl_material_transactions_s.nextval
	INTO	v_rm_trx_hdr_id
	FROM	dual;

---------------------------------------------------------
	G_REQUEST_ID  :=  FND_GLOBAL.CONC_REQUEST_ID ;
	BEGIN
		UPDATE	dtac_sim_receipt_interface
		SET	request_id	= G_REQUEST_ID,
			process_flag	= 'X'
		WHERE	SOURCE			= i_source_company
		AND	transaction_source_name	= i_source_name
		AND	NVL(selected_flag,'N')	= 'Y'
		AND	NVL(process_flag,'N')	= 'N';
	EXCEPTION WHEN OTHERS THEN
		FND_FILE.put_line(fnd_file.log,'Error when start update request ID '||SQLERRM);
		err_code   := '2' ;
		RETURN ;
	END  ;
	COMMIT ;

---------------------------------------------------------
	-- commit ; RETURN ; -- !!@@
	SAVEPOINT trx_sp;

-------------------------------------------------------------------------------------
	v_group   := 0 ;
	v_det_tab.DELETE ;
	FOR rec IN c_group_interface (G_REQUEST_ID) LOOP
		v_group		:=	v_group + 1 ;
		v_group_char	:=	'GROUP'||TO_CHAR(v_group) ;
		SAVEPOINT v_group_char ; --- !!@@
		v_err_flag	:=	FALSE;
		v_error		:=	NULL;
		-- Check FG Organization
		v_err_fg_item	:=	NULL;
		v_err_record	:=	0;
		v_comp_record	:=	0;
		v_org_id	:=	rec.organization_id;

		IF rec.organization_id IS NULL THEN
			v_err_flag	:=	TRUE;
			v_error		:=	v_error||','||C_FG_ORG_ERROR_CODE;
			v_err_fg_item	:=	'FG Organization : '||rec.organization_code||' NOT FOUND '||CHR(10);
			-- Comment by TM.Pornnicha write_log('FG Organization : '||rec.organization_code||' NOT FOUND ') ;
		ELSE
			-- Check FG Item
			v_inv_item_id	:=	NULL;
			OPEN c_chk_item (rec.organization_id,rec.item_number);
			FETCH c_chk_item INTO v_inventory_item_id;
				v_inv_item_id	:=	v_inventory_item_id;
				IF c_chk_item%NOTFOUND THEN
					v_err_flag	:=	TRUE;
					v_error		:=	v_error||','||C_FG_ITEM_ERROR_CODE;
					v_err_fg_item	:=	'FG ITEM : '||rec.item_number||' NOT FOUND '||CHR(10);
					-- Comment by TM.Pornnicha write_log('FG ITEM : '||rec.item_number||' NOT FOUND ') ;
				END IF;
			CLOSE c_chk_item;

			-- Check FG Subinventory
			OPEN c_chk_subinv (rec.organization_id,rec.subinventory);
			FETCH c_chk_subinv INTO v_secondary_inventory_name;
				IF c_chk_subinv%NOTFOUND THEN
					CLOSE c_chk_subinv;
					v_err_flag	:=	TRUE;
					v_error		:=	v_error||','||C_FG_SUBINV_ERROR_CODE;
					v_err_fg_item	:=	v_err_fg_item||'FG Subinventory  : '||rec.subinventory||' NOT FOUND '||CHR(10);
					-- Comment by TM.Pornnicha write_log('FG Subinventory  : '||rec.subinventory||' NOT FOUND ') ;
				ELSE
					CLOSE c_chk_subinv;
					--Check FG Locator
					v_locator_id	:=	NULL;
					OPEN c_chk_locator (rec.organization_id,rec.subinventory,rec.locator_code);
					FETCH c_chk_locator INTO v_inventory_location_id;
						v_locator_id	:=	v_inventory_location_id;
						IF c_chk_locator%NOTFOUND THEN
							v_err_flag	:=	TRUE;
							v_error		:=	v_error||','||C_FG_LOCATOR_ERROR_CODE;
							v_err_fg_item	:=	v_err_fg_item||'FG Locator  : '||rec.locator_code||' NOT FOUND '||CHR(10);
							-- Comment by TM.Pornnicha write_log('FG Locator  : '||rec.locator_code||' NOT FOUND ') ;
						END IF;
					CLOSE c_chk_locator;
				END IF;
		END IF;

	-------------------------------------- FG
		v_err_rm_item	:=	NULL;

		----- Check RM Organization
		v_mtl_org_id	:=	rec.mtl_organization_id;
		IF rec.mtl_organization_id IS NULL THEN
			v_err_flag	:=	TRUE;
			v_error		:=	v_error||','||C_RM_ORG_ERROR_CODE;
			v_err_rm_item	:=	'RM Organizaiton  : '||rec.material_organization_code||'  NOT FOUND '||CHR(10);
			--write_log('RM Organizaiton  : '||rec.material_organization_code||'  NOT FOUND ') ;
		ELSE
			v_mtl_org_id	:=	rec.mtl_organization_id;

			----- Check RM Item
			v_mtl_inv_item_id	:=	NULL;
			OPEN c_chk_item (rec.mtl_organization_id,rec.material_part);
			FETCH c_chk_item INTO v_inventory_item_id;
				v_mtl_inv_item_id	:=	v_inventory_item_id;
				IF c_chk_item%NOTFOUND THEN
					v_err_flag	:=	TRUE;
					v_error		:=	v_error||','||C_RM_ITEM_ERROR_CODE;
					v_err_rm_item	:=	'RM Item : '||rec.material_part||' NOT FOUND '||CHR(10);
					-- Comment by TM.Pornnicha write_log('RM Item : '||rec.material_part||' NOT FOUND ') ;
				END IF;
			CLOSE c_chk_item;

			----- Check RM Subinventory
			OPEN c_chk_subinv (rec.mtl_organization_id,rec.material_subinventory);
			FETCH c_chk_subinv INTO v_secondary_inventory_name;
				IF c_chk_subinv%NOTFOUND THEN
					CLOSE c_chk_subinv;
					v_err_flag	:=	TRUE;
					v_error		:=	v_error||','||C_RM_SUBINV_ERROR_CODE;
					v_err_rm_item	:=	v_err_rm_item||'RM Subinventory : '||rec.material_subinventory||' NOT FOUND '||CHR(10);
					-- Comment by TM.Pornnicha write_log('RM Subinventory : '||rec.material_subinventory||' NOT FOUND ') ;
				ELSE
					CLOSE c_chk_subinv;
					--Check RM Locator
					v_mtl_locator_id	:=	NULL;
					OPEN c_chk_locator (rec.mtl_organization_id,rec.material_subinventory,rec.material_locator_code);
					FETCH c_chk_locator INTO v_inventory_location_id;
						v_mtl_locator_id	:=	v_inventory_location_id;
						IF c_chk_locator%NOTFOUND THEN
							v_err_flag	:=	TRUE;
							v_error		:=	v_error||','||C_RM_LOCATOR_ERROR_CODE;
							v_err_rm_item	:=	v_err_rm_item||'RM Locator : '||rec.material_locator_code||' NOT FOUND '||CHR(10);
							-- Comment by TM.Pornnicha write_log('RM Locator : '||rec.material_locator_code||' NOT FOUND ') ;
						END IF;
					CLOSE c_chk_locator;
				END IF;
	--- Change Request 06/03/2007

			IF	v_err_flag != TRUE AND
				rec.mtl_organization_id IS NOT NULL AND
				v_secondary_inventory_name IS NOT NULL AND
				v_inventory_location_id IS NOT NULL
			THEN
				OPEN c_rm_oh (rec.mtl_organization_id,v_inventory_item_id,rec.material_subinventory,v_inventory_location_id);
				FETCH c_rm_oh INTO v_rm_oh;
					IF c_rm_oh%NOTFOUND OR v_rm_oh < rec.material_qty THEN
						v_err_flag	:=	TRUE;
						v_error		:=	v_error||','||C_RM_ONHAND_ERROR_CODE;
						v_err_rm_item	:=	v_err_rm_item||'RM Onhand : '||rec.material_part||' NOT Available '||CHR(10);
						-- Comment by TM.Pornnicha write_log('RM Onhand : '||rec.material_part||' NOT Available ') ;
					END IF;
					--CLOSE c_chk_locator;  Modified by TM.TOA 08/03/2007
				CLOSE c_rm_oh;
			END IF ;
		END IF;

		Write_Log('Result : ');
		IF v_err_flag THEN
			Write_Log('FG : '||rec.item_number||' , RM: '||rec.material_part||' , Packing Sim No : '||rec.packing_sim||' Qty : '||rec.material_qty||' Fail.'); -- Added by TM.Pornnicha 07-Mar-2007
			IF v_err_fg_item IS NOT NULL THEN
				Write_Log('	'||v_err_fg_item);
			END IF;
			IF v_err_rm_item IS NOT NULL THEN
				Write_Log('	'||v_err_rm_item);
			END IF;
			GOTO next_record;
		END IF;

		Write_Log('FG : '||rec.item_number||' , RM: '||rec.material_part||' , Packing Sim No : '||rec.packing_sim||' Qty : '||rec.material_qty||' Success.'); -- Added by TM.Pornnicha 07-Mar-2007

		r_po_trx_line					:=	NULL;
		r_po_trx_line.source_company			:=	rec.source_company;
		r_po_trx_line.source_name			:=	rec.source_name;
		r_po_trx_line.packing_sim			:=	rec.packing_sim;
		r_po_trx_line.transaction_date			:=	g_trx_date ; --rec.transaction_date;
		r_po_trx_line.item_number			:=	rec.item_number;
		r_po_trx_line.transaction_quantity		:=	rec.transaction_quantity;
		r_po_trx_line.transaction_uom			:=	rec.transaction_uom;
		r_po_trx_line.organization_code			:=	rec.organization_code;
		r_po_trx_line.subinventory			:=	rec.subinventory;
		r_po_trx_line.locator_code			:=	rec.locator_code;
		r_po_trx_line.material_part			:=	rec.material_part;
		r_po_trx_line.material_qty			:=	rec.material_qty;
		r_po_trx_line.material_uom			:=	rec.material_uom;
		r_po_trx_line.material_organization_code	:=	rec.material_organization_code;
		r_po_trx_line.material_subinventory		:=	rec.material_subinventory;
		r_po_trx_line.material_locator_code		:=	rec.material_locator_code;
		r_po_trx_line.organization_id			:=	rec.organization_id;
		r_po_trx_line.mtl_organization_id		:=	rec.mtl_organization_id;
		r_po_trx_line.fm_serial_no			:=	rec.fm_serial_no;
		r_po_trx_line.to_serial_no			:=	rec.To_Serial_No;
		r_po_trx_line.fm_mtl_serial_no			:=	rec.fm_mtl_serial_no;
		r_po_trx_line.to_mtl_serial_no			:=	rec.To_Mtl_Serial_No;
		r_po_trx_line.ref_doc1				:=	rec.ref_doc1 ;
		r_po_trx_line.ref_doc2				:=	rec.ref_doc2 ;

		r_fg_mtl_trx_inf				:=	NULL;
		r_rm_mtl_trx_inf				:=	NULL;
		r_cost_adj_mtl_trx_inf				:=	NULL;

		-- Start Add by Korn.iCE on 15-Sept-2009.
		-- Reason: Get correctly Transaction_Cost for RM and FG.
		BEGIN
			select	cst.item_cost
			INTO	v_Trans_Cost
			from	mtl_system_items	msi,
				cst_item_costs		cst,
				mtl_parameters		mp
			where	cst.organization_id	=	mp.organization_id
			and	cst.cost_type_id	=	mp.primary_cost_method
			and	cst.inventory_item_id	=	msi.inventory_item_id
			and	cst.organization_id	=	msi.organization_id
			and	msi.organization_id	=	r_po_trx_line.mtl_organization_id
			and	msi.segment1		=	r_po_trx_line.material_part;
		EXCEPTION WHEN OTHERS THEN
			v_Trans_Cost := 0;
		End ;
		-- Finish Add by Korn.iCE on 15-Sept-2009.

		-----insert RM
		assign_trx_inf	(i_trx_type	=>	g_rm_source
				,i_trx_hdr_id	=>	v_rm_trx_hdr_id
				,ir_po_trx_line	=>	r_po_trx_line
				,or_mtl_trx_inf	=>	r_rm_mtl_trx_inf
				,o_error	=>	v_error
				,i_Trans_Cost	=>	v_Trans_Cost); -- Add by Korn.iCE on 15-Sept.-2009.
		IF v_error IS NOT NULL THEN
			v_err_flag	:=	TRUE;
			GOTO next_record;
		END IF;

		-----inssert FG
		assign_trx_inf	(i_trx_type	=>	g_fg_source
				,i_trx_hdr_id	=>	v_fg_trx_hdr_id
				,ir_po_trx_line	=>	r_po_trx_line
				,or_mtl_trx_inf	=>	r_fg_mtl_trx_inf
				,o_error	=>	v_error
				,i_Trans_Cost	=>	v_Trans_Cost); -- Add by Korn.iCE on 15-Sept.-2009.
		IF v_error IS NOT NULL THEN
			v_err_flag	:=	TRUE;
			GOTO next_record;
		END IF;

		FOR r_det_trx_inf IN c_det_interface
			(i_item_code		=>	rec.item_number
			,i_org_code		=>	rec.organization_code
			,i_sub			=>	rec.subinventory
			,i_loc			=>	rec.locator_code
			,i_rm_item_code		=>	rec.material_part
			,i_rm_org_code		=>	rec.material_organization_code
			,i_rm_sub		=>	rec.material_subinventory
			,i_rm_loc		=>	rec.material_locator_code
			,i_packing_sim		=>	rec.packing_sim
			,i_req_id		=>	G_REQUEST_ID
			,i_source_name     	=>	i_source_name
			,i_source_company	=>	i_source_company
			,v_trx_source      	=>	g_trx_source )
		LOOP

			v_item_type	:=	NULL;
			v_status	:=	NULL;
			validate_serial
				(i_item_id		=>	v_inv_item_id
				,i_serial_no		=>	r_det_trx_inf.fm_serial_no
				,i_org_id		=>	r_det_trx_inf.organization_id
				,i_subinv_code		=>	r_det_trx_inf.subinventory
				,i_locator_id		=>	v_locator_id
				,i_mtl_part_id		=>	v_mtl_inv_item_id
				,i_mtl_serial_no	=>	r_det_trx_inf.fm_mtl_serial_no
				,i_mtl_org_id		=>	v_mtl_org_id
				,i_mtl_subinv_code	=>	r_det_trx_inf.material_subinventory
				,i_mtl_locator_id	=>	v_mtl_locator_id
				,i_rm_inf_id		=>	r_rm_mtl_trx_inf.transaction_interface_id
				,i_fg_inf_id		=>	r_fg_mtl_trx_inf.transaction_interface_id
				,o_status		=>	v_status
				,o_err_item_type	=>	v_item_type);

			IF v_status IS NOT NULL THEN
				--Write_Log('		Validate Serial Result	:	'||v_status);
				IF v_item_type = 'FG' THEN
					Write_log('	'||v_status||
					' , Item = '||r_det_trx_inf.item_number||
					' , Subinventory = '||r_det_trx_inf.subinventory||
					' , Locator = '||r_det_trx_inf.locator_code ||
					' , Organization = '|| r_det_trx_inf.organization_code ||' - Fail.');
				ELSIF v_item_type = 'RM' THEN
					Write_log('	'||v_status||
					' , Item = '||r_det_trx_inf.material_part||
					' , Subinventory = '||r_det_trx_inf.material_subinventory||
					' , Locator = '||r_det_trx_inf.material_locator_code ||
					' , Organization = '|| r_det_trx_inf.material_organization_code ||' - Fail.');
				END IF;
				v_err_flag	:=	TRUE;
				EXIT;
			END IF;

			IF r_det_trx_inf.fm_mtl_serial_no IS NOT NULL THEN
				r_serial_trx_line			:=	NULL;
				r_serial_trx_line.fm_serial_number	:=	r_det_trx_inf.fm_mtl_serial_no;
				r_serial_trx_line.to_serial_number	:=	r_det_trx_inf.to_mtl_serial_no;
				r_serial_trx_line.tel_number		:=	r_det_trx_inf.tel_number;
				r_serial_trx_line.expire_date		:=	r_det_trx_inf.expire_date;

				-----inssert RM Serail
				assign_serial_inf
					(i_trx_type		=>	g_rm_source
					,ir_serial_trx_line	=>	r_serial_trx_line
					,ir_mtl_trx_inf		=>	r_rm_mtl_trx_inf
					,or_mtl_serial_inf	=>	r_mtl_serial_inf
					,o_error		=>	v_error);

				IF v_error IS NOT NULL THEN
					v_err_flag	:=	TRUE;
					GOTO next_record;
				END IF;
			END IF;
			r_serial_trx_line			:=	NULL;
			r_serial_trx_line.fm_serial_number	:=	r_det_trx_inf.fm_serial_no;
			r_serial_trx_line.to_serial_number	:=	r_det_trx_inf.to_serial_no;
			r_serial_trx_line.tel_number		:=	r_det_trx_inf.tel_number;
			r_serial_trx_line.expire_date		:=	r_det_trx_inf.expire_date;

			-----inssert FG Serail
			assign_serial_inf
			(i_trx_type		=>	g_fg_source
			,ir_serial_trx_line	=>	r_serial_trx_line
			,ir_mtl_trx_inf		=>	r_fg_mtl_trx_inf
			,or_mtl_serial_inf	=>	r_mtl_serial_inf
			,o_error		=>	v_error);

			IF v_error IS NOT NULL THEN
				v_err_flag	:=	TRUE;
				GOTO next_record;
			END IF;
		END LOOP;
		<<next_record>>

		-----check error
		IF v_err_flag THEN
			v_itf_flag	:=	'N' ;
			ROLLBACK TO v_group_char ;
			v_err_record	:=	v_err_record + 1;
			v_fg_serial_err	:=	v_fg_serial_err	+ rec.fg_serial_record;
			v_rm_serial_err	:=	v_rm_serial_err	+ rec.rm_serial_record;

			UPDATE	dtac_sim_receipt_interface	sr
			SET	sr.process_flag	=	'E',
				sr.message_code	=	SUBSTR(v_error,1,100)
			WHERE	sr.SOURCE				=	rec.source_company
			AND	sr.transaction_source_name		=	rec.source_name
			AND	sr.item_segment1			=	rec.item_number
			AND	sr.transaction_uom			=	rec.transaction_uom
			AND	sr.organization_code			=	rec.organization_code
			AND	NVL(sr.subinventory,'###')		=	NVL(rec.subinventory,'###')
			AND	NVL(sr.locator_code,'###')		=	NVL(rec.locator_code,'###')
			AND	sr.material_part			=	rec.material_part
			AND	sr.material_organization_code 		=	rec.material_organization_code
			AND	NVL(sr.material_subinventory,'###')	=	NVL(rec.material_subinventory,'###')
			AND	NVL(sr.material_locator_code,'###')	=	NVL(rec.material_locator_code,'###')
			AND	sr.request_id				=	G_REQUEST_ID ;
		ELSE
			v_comp_record		:=	v_comp_record + 1;
			v_fg_serial_comp	:=	v_fg_serial_comp + rec.fg_serial_record;
			v_rm_serial_comp	:=	v_rm_serial_comp + rec.rm_serial_record;

			UPDATE	dtac_sim_receipt_interface	sr
			SET	sr.process_flag		=	'Y',
				sr.transaction_id	=	r_fg_mtl_trx_inf.transaction_interface_id,
				sr.rm_transaction_id	=	r_rm_mtl_trx_inf.transaction_interface_id
			WHERE	sr.SOURCE				=	rec.source_company
			AND	sr.transaction_source_name		=	rec.source_name
			AND	sr.item_segment1			=	rec.item_number
			AND	sr.transaction_uom			=	rec.transaction_uom
			AND	sr.organization_code			=	rec.organization_code
			AND	NVL(sr.subinventory,'###')		=	NVL(rec.subinventory,'###')
			AND	NVL(sr.locator_code,'###')		=	NVL(rec.locator_code,'###')
			AND	sr.material_part			=	rec.material_part
			AND	sr.material_uom				=	rec.material_uom
			AND	sr.material_organization_code		=	rec.material_organization_code
			AND	NVL(sr.material_subinventory,'###')	=	NVL(rec.material_subinventory,'###')
			AND	NVL(sr.material_locator_code,'###')	=	NVL(rec.material_locator_code,'###')
			AND	sr.request_id				=	G_REQUEST_ID;

			IF SQL%ROWCOUNT < 1 THEN
				write_log('Can not found record for update process flag');
			END IF;
		END IF;

		write_log('	');
		/*
		write_log(', and submit RM  Error '||v_err_record||' records.');
		write_log(',		RM Serial Error '||v_rm_serial_err||' records.');
		write_log(',		FG Serial Error '||v_fg_serial_err||' records.');
		write_log(', and submit RM  Complete '||v_comp_record||' records.');
		write_log(',		RM Serial Complete '||v_rm_serial_comp||' records.');
		write_log(',		FG Serial Complete '||v_fg_serial_comp||' records.');
		write_log('');
		*/
	END LOOP;  ----- Group

	IF v_itf_flag = 'N' THEN  ---
		ROLLBACK;
		v_comp_record := 0 ;
	ELSE
		COMMIT;
		IF v_comp_record > 0 THEN
			----- Summit Request for RM
			v_rm_req_id := fnd_request.submit_request	(application	=>	'INV',
									program		=>	'INCTCM',
									argument1	=>	TO_CHAR(v_rm_trx_hdr_id),
									argument2	=>	'1',
									argument3	=>	null,
									argument4	=>	null );
			write_log('submit Inventory manager (Transaction Header ID <RM>'||v_rm_trx_hdr_id||'): Request '||v_rm_req_id);
			COMMIT;

			----- Summit Request for FG
			v_fg_req_id := fnd_request.submit_request	(application 	=>	'INV',
									program		=>	'INCTCM',
									argument1	=>	TO_CHAR(v_fg_trx_hdr_id),
									argument2	=>	'1',
									argument3	=>	null,
									argument4	=>	null );
			write_log('submit Inventory manager (Transaction Header ID <FG>'||v_fg_trx_hdr_id||'): Request '||v_fg_req_id);
			COMMIT;

			IF v_fg_req_id > 0 THEN
				----- Wait until request submit have compled.
				Conc_wait(v_fg_req_id);
				----- Update flag error for record that interface fail.
				FOR r_err IN c_err_trx_itf (G_REQUEST_ID) LOOP
					v_err_record	:=	v_err_record + 1;
					v_comp_record	:=	v_comp_record - 1;

					UPDATE	dtac_sim_receipt_interface	sr
					SET	sr.process_flag		=	'E',
						sr.message_code		=	SUBSTR(r_err.error_explanation,1,100)
					WHERE	sr.transaction_id	=	r_err.transaction_id
					AND	sr.request_id		=	G_REQUEST_ID ;
				END LOOP;
				COMMIT;

				v_adj_err_record	:=	0;
				v_adj_comp_record	:=	0;

				----- Insert Cost Adjust
				SELECT mtl_material_transactions_s.NEXTVAL
				INTO   v_cost_adj_trx_hdr_id
				FROM   dual;

				FOR r_cost_adj IN c_cost_adj_itf (G_REQUEST_ID) LOOP
					OPEN c_onhand_qty (r_cost_adj.item_number,r_cost_adj.organization_code);
					FETCH c_onhand_qty INTO v_cost_adj_qty;
						IF c_onhand_qty%NOTFOUND THEN
							v_cost_adj_qty	:=	0;
						END IF;
					CLOSE c_onhand_qty;

					IF v_cost_adj_qty <> 0 THEN
						v_cost_adj_amt	:=	r_cost_adj.transaction_cost/v_cost_adj_qty;
					ELSE
						v_cost_adj_amt	:=	0;
					END IF;

					r_po_trx_line				:=	NULL;
					r_po_trx_line.source_company		:=	i_source_company;
					r_po_trx_line.source_name		:=	i_source_name;
					r_po_trx_line.transaction_date		:=	g_trx_date;
					r_po_trx_line.item_number		:=	r_cost_adj.item_number;
					r_po_trx_line.transaction_quantity	:=	v_cost_adj_qty;
					r_po_trx_line.transaction_uom		:=	r_cost_adj.transaction_uom;
					r_po_trx_line.transaction_cost		:=	v_cost_adj_amt;
					r_po_trx_line.organization_code		:=	r_cost_adj.organization_code;
					r_po_trx_line.organization_id		:=	r_cost_adj.organization_id;
					r_po_trx_line.packing_sim		:=	r_cost_adj.packing_sim;

					assign_trx_inf	(i_trx_type		=>	g_cost_source
							,i_trx_hdr_id		=>	v_cost_adj_trx_hdr_id
							,ir_po_trx_line		=>	r_po_trx_line
							,or_mtl_trx_inf		=>	r_cost_adj_mtl_trx_inf
							,o_error		=>	v_error
							,i_Trans_Cost		=>	0); -- Add by Korn.iCE on 15-Sept.-2009.

					IF v_error IS NOT NULL THEN
						v_adj_err_record	:=	v_adj_err_record + 1;
						write_log('Item-'||r_cost_adj.item_number||', Organization-'||r_cost_adj.organization_code||' cannot insert cost adjust transaction interface record.');
						Write_log(v_error);
					ELSE
						v_adj_comp_record	:=	v_adj_comp_record + 1;
					END IF;

				END LOOP;
				COMMIT;

				write_log('	Submit Adjust Cost Error: '||v_adj_err_record||' records.');
				write_log('	Submit Adjust Cost Complete: '||v_adj_comp_record||' records.');
				----- Summit Request for Cost Adjust
				IF v_adj_comp_record > 0 THEN
					v_cost_adj_req_id := fnd_request.submit_request	(application	=>	'INV',
											program 	=>	'INCTCM',
											argument1 	=>	TO_CHAR(v_cost_adj_trx_hdr_id),
											argument2 	=>	'1',
											argument3 	=>	null,
											argument4 	=>	null );
					write_log('submit Inventory manager (Transaction Header ID <Cost Adjust>'||v_cost_adj_trx_hdr_id||'): Request '||v_cost_adj_req_id);
					COMMIT;
				END IF;

				----- Insert PO
				FOR rec IN c_rcv_trx_inf (G_REQUEST_ID) LOOP
					IF rec.quantity <> 0 THEN
						SAVEPOINT po_trx_sp;
						l_rcv_grp_id			:= rec.default_cost_group_id;
						r_po_trx_inf			:= NULL;
						r_po_trx_inf.quantity		:= rec.quantity;
						r_po_trx_inf.vendor_num		:= rec.vendor_num;
						r_po_trx_inf.document_num	:= rec.document_num;
						r_po_trx_inf.shipment_num	:= rec.shipment_num;
						r_po_trx_inf.employee_id	:= rec.employee_id;
						r_po_trx_inf.vendor_id		:= rec.vendor_id;
						r_po_trx_inf.po_header_id	:= rec.po_header_id;
						r_po_trx_inf.currency_code	:= rec.currency_code;
						r_po_trx_inf.vendor_site_id	:= rec.vendor_site_id;

						FOR r_line IN c_rcv_del_trx_inf (rec.po_header_id) LOOP
							r_po_trx_inf.ship_to_organization_code	:= r_line.ship_to_organization_code;
							r_po_trx_inf.unit_of_measure		:= r_line.unit_of_measure;
							r_po_trx_inf.ship_to_location_id	:= r_line.ship_to_location_id;
							r_po_trx_inf.category_id		:= r_line.category_id;
							r_po_trx_inf.item_description		:= r_line.item_description;
							r_po_trx_inf.po_line_id			:= r_line.po_line_id;
							r_po_trx_inf.line_location_id		:= r_line.line_location_id;
							r_po_trx_inf.unit_price			:= r_line.unit_price;
							r_po_trx_inf.ship_to_organization_id	:= r_line.organization_id;
							EXIT;
						END LOOP;

						assign_rcv_header_inf	(ir_po_trx_inf		=>	r_po_trx_inf
									,i_rcv_grp_id		=>	l_rcv_grp_id
									,or_rcv_header_inf	=>	r_rcv_header_inf
									,o_error		=>	v_error);

						IF v_error IS NULL THEN
							assign_rcv_trx_inf	(ir_po_trx_inf		=>	r_po_trx_inf
										,ir_rcv_header_inf	=>	r_rcv_header_inf
										,or_rcv_trx_inf		=>	r_rcv_trx_inf
										,o_error		=>	v_error);
						END IF;
						IF v_error IS NULL THEN
							v_po_record	:= v_po_record + 1;
						ELSE
							ROLLBACK TO po_trx_sp;
						END IF;
					END IF;
				END LOOP; ----- End Loop fpr Insert PO

				IF v_po_record > 0 THEN
					--o_status := o_status||', and submit Purchese Order Complete '||v_po_record||' records.';
					write_log(' Submit Purchese Order Complete '||v_po_record||' records.');
					COMMIT;

					----------- Submit Receive -------
					v_po_req_id	:= fnd_request.submit_request	(application => 'PO',
											program     => 'RVCTP',
											argument1   => l_processing_mode_code ,
											argument2   => TO_CHAR(l_rcv_grp_id) ) ;
					write_log('Submit Purchese manager (Group Id)'||l_rcv_grp_id||'): Request '||v_po_req_id);
					COMMIT;
				END IF;
			END IF;
		END IF;
	END IF ; -- itf_flag 	write_log('');

	write_log('+---------------------------------------------------------------------------+');
	write_log('End Process '||TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SS')||'.');
	write_log('+---------------------------------------------------------------------------+');
	COMMIT;

	delete_sim_product(v_status);
	COMMIT;

EXCEPTION WHEN OTHERS THEN
	ROLLBACK;
	err_msg		:=	'ERROR-'||SQLCODE||': '||SQLERRM;
	err_code	:=	'2';
END main_conc;

-----------------------------------------------------------------------------------
PROCEDURE main(i_source_company		IN		VARCHAR2
			   ,i_source_name		      	IN		VARCHAR2
			   ,i_organization_id		    IN		NUMBER
			   ,i_trx_date				      IN		DATE
			   ,i_trx_source			      IN		VARCHAR2
			   ,i_trx_type_rm_id		    IN		NUMBER
			   ,i_trx_type_fg_id		    IN		NUMBER
			   ,i_trx_type_packing_id	  IN		NUMBER
			   ,i_wip_combine_id		    IN		NUMBER
			   ,i_accrued_combine_id	  IN		NUMBER
			   ,o_status				        OUT		VARCHAR2) IS
PRAGMA AUTONOMOUS_TRANSACTION;
    v_req_id   	NUMBER ;
    v_flag    	VARCHAR2(100) ;
BEGIN
    Begin
        SELECT  DISTINCT 'Y'
        INTO    v_flag
        FROM    dtac_sim_receipt_interface
        WHERE   SOURCE					            = i_source_company
       	AND	    transaction_source_name		  = i_source_name
      	AND	    NVL(selected_flag,'N')     	= 'Y'
		AND     NVL(process_flag,'N')		    = 'N';
        EXCEPTION WHEN OTHERS THEN
        v_flag := 'N' ;
    End ;

    IF  NVL(v_flag ,'N') = 'N' THEN
        o_status := 'No data to process.' ;
        Rollback ;
        RETURN ;
    END IF ;

		Begin
    v_req_id := fnd_request.submit_request(
			               application => 'INV',
			  program     => 'TACIC113',
			  --description => NULL ,
			  --start_time  => NULL ,
			  --sub_request => FALSE,
			  argument1   => i_source_company ,
			  argument2   => i_source_Name,
  			argument3   => TO_CHAR(i_ORGANIZATION_ID),
			  argument4   => TO_CHAR(i_TRX_DATE,'YYYY/MM/DD HH24:MI:SS'),
			  argument5   => i_TRX_SOURCE ,
			  argument6   => TO_CHAR(i_TRX_TYPE_RM_ID),
			  argument7   => TO_CHAR(i_TRX_TYPE_FG_ID),
			  argument8   => TO_CHAR(i_TRX_TYPE_PACKING_ID),
			  argument9   => TO_CHAR(i_WIP_COMBINE_ID),
			  argument10  => TO_CHAR(i_ACCRUED_COMBINE_ID) ) ;
     Exception WHEN OTHERS THEN
        o_status := 'Submit Error '||SQLERRM ;
        Rollback ;
        RETURN ;
     END ;

     IF v_req_id > 0 THEN
        COMMIT ;
        o_status := 'Please see Request ID : '||TO_CHAR(v_req_id) ;
     ELSE
        ROLLBACK ;
        o_status := 'Can not Submit Request : '||TO_CHAR(v_req_id) ;
     END IF ;

     COMMIT ;

END Main;
-----------------------------------------------------------------------------------------
PROCEDURE set_msg_log (i_tmp_line	IN 	  	c_det_interface%ROWTYPE
                     , io_log_tab	IN OUT	det_tbl  )
IS
	v_row     NUMBER ;
BEGIN
     NULL ;
/*     v_row := io_log_tab.count+1  ;
     io_log_tab(v_row).Order_Source  	:= i_tmp_line.order_source ;
     io_log_tab(v_row).Order_Type    	:= i_tmp_line.order_type ;
     io_log_tab(v_row).customer_po_number   := i_tmp_line.customer_po_number;
--     g_success.Customer_name := i_tmp_header.customer_name ;
     io_log_tab(v_row).Customer_no	:= i_tmp_line.customer_no ;
     io_log_tab(v_row).Price_List_Name    	:= NULL ;
     io_log_tab(v_row).trx_Currency      	:= i_tmp_line.trx_currency ;
--     io_log_tab(v_row).Payment_Term  	:= NULL ;
     io_log_tab(v_row).Item_code     	:= i_tmp_line.item_code ;
     io_log_tab(v_row).Ordered_QTy    := i_tmp_line.ordered_qty ;
     io_log_tab(v_row).Ordered_Qty_UOM           	:= i_tmp_line.ordered_qty_uom ;
     io_log_tab(v_row).Request_date  	:= i_tmp_line.request_date ;
     io_log_tab(v_row).Tax_Code      	:= NULL ;
     io_log_tab(v_row).Row_id         := i_tmp_line.row_id ;*/
END set_msg_log ;
------------------------------------------------------------------------------------------
PROCEDURE Set_msg_title/*(i_event IN VARCHAR2,i_msg IN OUT tmp_line%ROWTYPE  )*/  IS
BEGIN
    NULL ;
    /*
     IF   i_event  = 'T' THEN -- Title
          fnd_file.put_line(fnd_file.log,
          RPAD('Customer PO',20)||' '||
          RPAD('Customer NO',10)||' '||
          RPAD('Order Type',20)||' '||
          RPAD('Description',100)  ) ;
          fnd_file.put_line(fnd_file.log,
          RPAD('-',20,'-')||' '||
          RPAD('-',10,'-')||' '||
          RPAD('-',20,'-')||' '||
          RPAD('-',100,'-')  ) ;
     ELSIF i_event = 'FD' THEN -- First Detail
          fnd_file.put_line(fnd_file.log,
          RPAD(i_msg.Customer_po_number,20,' ')||' '||
          RPAD(NVL(i_msg.Customer_No,' '),10,' ')||' '||
          RPAD(NVL(i_msg.Order_Type,' '),20,' ')||' '||
          i_msg.Err_msg   ) ;
     ELSIF i_event = 'D' THEN -- Detail
          fnd_file.put_line(fnd_file.log,
          RPAD(' ',20,' ')||' '||
          RPAD(' ',10,' ')||' '||
          RPAD(' ',20,' ')||' '||
          i_msg.Err_msg  ) ;
     END IF ;  */

END Set_msg_title ;

------------------------------------------------------------------------------------------
END TAC_IC113_API;
