create or replace package body TAC_Dev_Sale_Cost_by_Product is

  procedure write_output(p_text varchar2) is
  begin
    fnd_file.put_line(fnd_file.output, p_text);
  exception
    when others then
      null;
  end write_output;

  procedure exec_report(err_msg        out varchar2,
                        err_code       out varchar2,
                        date_from      varchar2,
                        date_to        varchar2,
                        product_groups varchar2) is
    line_data varchar2(32000);
    pipestr   varchar2(1) := '|';
    fmt       varchar2(20) := '999,999,999,990.00';
  begin
    /*build headers*/
    line_data := 'Product Group' || pipestr || 'Doc Source' || pipestr ||
                 'Division' || pipestr || 'Item Code' || pipestr ||
                 'Item Desc' || pipestr || 'Unit Sales SUM' || pipestr ||
                 'Total Sales SUM' || pipestr || 'Total Sales ExcVAT' ||
                 pipestr || 'Price Per Unit ExcVAT' || pipestr ||
                 'Total Cost SUM' || pipestr || 'Cost Per Unit' || pipestr ||
                 'Margin' || pipestr || 'Discount IncVAT';
    write_output(line_data);
  
    for r in (SELECT t.DOC_SOURCE,
                     t.DIVISION,
                     t.PRODUCT_GROUP,
                     t.ITEM_CODE,
                     t.ITEM_DESC,
                     SUM(t.DISC_TOT) as DISC_TOT_SUM,
                     DECODE((DECODE((SUM(t.UNIT_SALES)),
                                    0,
                                    0,
                                    ((SUM(t.TOTAL_SALES)) / 1.07) /
                                    (SUM(t.UNIT_SALES)))),
                            0,
                            0,
                            ((DECODE((SUM(t.UNIT_SALES)),
                                     0,
                                     0,
                                     ((SUM(t.TOTAL_SALES)) / 1.07) /
                                     (SUM(t.UNIT_SALES)))) -
                            (DECODE((SUM(t.UNIT_SALES)),
                                     0,
                                     0,
                                     (SUM(t.TOTAL_COST)) /
                                     (SUM(t.UNIT_SALES))))) /
                            (DECODE((SUM(t.UNIT_SALES)),
                                    0,
                                    0,
                                    ((SUM(t.TOTAL_SALES)) / 1.07) /
                                    (SUM(t.UNIT_SALES))))) as margin,
                     DECODE((SUM(t.UNIT_SALES)),
                            0,
                            0,
                            (SUM(t.TOTAL_COST)) /
                            (SUM(t.UNIT_SALES))) as cost_per_unit,
                     SUM(t.TOTAL_COST) as TOTAL_COST_SUM,
                     DECODE((SUM(t.UNIT_SALES)),
                            0,
                            0,
                            ((SUM(t.TOTAL_SALES)) / 1.07) /
                            (SUM(t.UNIT_SALES))) as price_per_unit_excvat,
                     (SUM(t.TOTAL_SALES)) / 1.07 as TOTAL_SALES_SUM_EXCVAT,
                     SUM(t.TOTAL_SALES) as TOTAL_SALES_SUM,
                     SUM(t.UNIT_SALES) as UNIT_SALES_SUM
                FROM (SELECT DOC_DATE,
                             DOC_SOURCE,
                             Division,
                             CASE --- Product Group
                               WHEN substr(ITEM_CODE, 1, 4) in
                                    ('ACPR',
                                     'ACPO',
                                     'ACHW',
                                     'ACZT',
                                     'N25H',
                                     'N25S',
                                     'N25Z') THEN
                                '01 Aircard'
                               WHEN substr(ITEM_CODE, 1, 4) in
                                    ('BBHH', 'BB85', 'BB90', 'BB95', 'BB99') THEN
                                '02 Blackberry'
                               WHEN substr(ITEM_CODE, 1, 4) = 'IPHH' THEN
                                '03 IPhone'
                               WHEN substr(ITEM_CODE, 1, 4) = 'IPTL' THEN
                                '04 IPad'
                               WHEN substr(ITEM_CODE, 1, 5) in
                                    ('HTCHH', 'S12HT') THEN
                                '05 HTC'
                               WHEN (substr(ITEM_CODE, 1, 4) = 'SSHH' or
                                    substr(ITEM_CODE, 1, 5) = 'S12SS' or
                                    substr(ITEM_CODE, 1, 9) = 'SP1-S12SS') THEN
                                '06 Samsung'
                               WHEN (substr(ITEM_CODE, 1, 4) = 'LGHH' or
                                    substr(ITEM_CODE, 1, 5) = 'S12LG') THEN
                                '07 LG'
                               WHEN (substr(ITEM_CODE, 1, 4) = 'NKHH' or
                                    substr(ITEM_CODE, 1, 5) in
                                    ('S01NO', 'N25NK', 'N25NO', 'S12NO') or
                                    substr(ITEM_CODE, 1, 9) = 'SP1-S12NO' or
                                    substr(ITEM_CODE, 1, 10) = 'SP1Q-S12NO') THEN
                                '08 Nokia'
                               ELSE
                                '09 Other'
                             END Product_Group,
                             ITEM_CODE,
                             ITEM_DESC,
                             sum(nvl(unit_sales, 0)) UNIT_SALES,
                             sum(nvl(total_sales, 0)) TOTAL_SALES,
                             sum(nvl(cost_sale, 0)) - sum(nvl(cost_return, 0)) total_cost,
                             SUM(NVL(disc_tot, 0)) disc_tot
                        FROM (SELECT trunc(rct.trx_date) DOC_DATE,
                                     'ERP' DOC_SOURCE,
                                     tt.ATTRIBUTE3 Division,
                                     msi.CONCATENATED_SEGMENTS ITEM_CODE,
                                     msi.DESCRIPTION ITEM_DESC,
                                     ROUND(NVL(decode(tt.TYPE,
                                                      'INV',
                                                      decode(msi.PRIMARY_UOM_CODE,
                                                             NVL(rctl.UOM_CODE,
                                                                 msi.PRIMARY_UOM_CODE),
                                                             rctl.QUANTITY_INVOICED,
                                                             inv_convert.inv_um_convert(rctl.INVENTORY_ITEM_ID,
                                                                                        2,
                                                                                        rctl.QUANTITY_INVOICED,
                                                                                        rctl.UOM_CODE,
                                                                                        msi.PRIMARY_UOM_CODE,
                                                                                        NULL,
                                                                                        NULL)),
                                                      'DM',
                                                      decode(msi.PRIMARY_UOM_CODE,
                                                             NVL(rctl.UOM_CODE,
                                                                 msi.PRIMARY_UOM_CODE),
                                                             rctl.QUANTITY_INVOICED,
                                                             inv_convert.inv_um_convert(rctl.INVENTORY_ITEM_ID,
                                                                                        2,
                                                                                        rctl.QUANTITY_INVOICED,
                                                                                        rctl.UOM_CODE,
                                                                                        msi.PRIMARY_UOM_CODE,
                                                                                        NULL,
                                                                                        NULL)),
                                                      0),
                                               0) ---x C_Inv_Qty
                                           - NVL(abs(decode(tt.TYPE,
                                                            'CM',
                                                            decode(msi.PRIMARY_UOM_CODE,
                                                                   NVL(rctl.UOM_CODE,
                                                                       msi.PRIMARY_UOM_CODE),
                                                                   rctl.QUANTITY_CREDITED,
                                                                   inv_convert.inv_um_convert(rctl.INVENTORY_ITEM_ID,
                                                                                              2,
                                                                                              rctl.QUANTITY_CREDITED,
                                                                                              rctl.UOM_CODE,
                                                                                              msi.PRIMARY_UOM_CODE,
                                                                                              NULL,
                                                                                              NULL)),
                                                            0)),
                                                 0)) unit_sales,
                                     NVL(decode(tt.TYPE,
                                                'INV',
                                                NVL(rctl.EXTENDED_AMOUNT, 0) +
                                                NVL(ltax.EXTENDED_AMOUNT, 0),
                                                'DM',
                                                NVL(rctl.EXTENDED_AMOUNT, 0) +
                                                NVL(ltax.EXTENDED_AMOUNT, 0),
                                                0),
                                         0) ---x C_Inv_Amount
                                     - NVL(abs(decode(tt.TYPE,
                                                      'CM',
                                                      NVL(rctl.EXTENDED_AMOUNT,
                                                          0) + NVL(ltax.EXTENDED_AMOUNT,
                                                                   0),
                                                      0)),
                                           0) total_sales,
                                     0 cost_sale,
                                     0 cost_return,
                                     0 disc_tot
                                FROM ra_customer_trx_all       rct,
                                     ra_customer_trx_lines_all rctl,
                                     ra_customer_trx_lines_all ltax,
                                     ra_cust_trx_types_all     tt,
                                     mtl_system_items_vl       msi,
                                     mtl_item_categories       mic,
                                     mtl_categories            mc,
                                     mtl_category_sets         mcs,
                                     mtl_units_of_measure_vl   uom,
                                     HR_ORGANIZATION_UNITS_V   org
                               WHERE rct.CUSTOMER_TRX_ID =
                                     rctl.CUSTOMER_TRX_ID
                                 AND rct.COMPLETE_FLAG = 'Y'
                                 AND rct.CUST_TRX_TYPE_ID =
                                     tt.CUST_TRX_TYPE_ID
                                 AND rct.ORG_ID = org.organization_id
                                 AND tt.TYPE IN ('INV', 'DM', 'CM')
                                 AND rctl.LINE_TYPE = 'LINE'
                                 AND rctl.INVENTORY_ITEM_ID =
                                     msi.inventory_item_id
                                 AND rctl.warehouse_ID = msi.organization_id
                                 AND rctl.CUSTOMER_TRX_LINE_ID =
                                     ltax.LINK_TO_CUST_TRX_LINE_ID
                                 AND msi.inventory_item_id =
                                     mic.inventory_item_id
                                 AND msi.Organization_Id = mic.Organization_Id
                                 AND mic.category_set_id = mcs.category_set_id
                                 AND mcs.CATEGORY_SET_NAME = 'Inventory'
                                 AND mic.category_id = mc.CATEGORY_ID
                                 AND rctl.UOM_CODE = uom.UOM_CODE(+)
                              UNION ALL
                               (SELECT cms.gl_date DOC_DATE,
                                      'ERP' DOC_SOURCE,
                                      ott.attribute1 Division,
                                      msi.segment1 ITEM_CODE,
                                      msi.description ITEM_DESC,
                                      0 unit_sales,
                                      0 total_sales,
                                      cms.cogs_amount cost_sale,
                                      0 cost_return,
                                      0 disc_tot
                                 FROM mtl_item_categories      micat,
                                      mtl_system_items         msi,
                                      mtl_categories           mcat,
                                      mtl_categorY_SETS        mcats,
                                      OE_TRANSACTION_TYPES_ALL OTT,
                                      CST_MARGIN_SUMMARY       cms,
                                      FND_FLEX_VALUES_VL       ff
                                WHERE mcat.CATEGORY_ID = MICat.CATEGORY_ID
                                  AND mcats.CATEGORY_SET_ID =
                                      MICat.CATEGORY_SET_ID
                                  and mcats.CATEGORY_SET_ID = 1
                                  AND micat.inventory_item_id =
                                      msi.inventory_item_id
                                  AND micat.organization_id =
                                      msi.organization_id
                                  AND MSI.inventory_item_id =
                                      CMS.parent_inventory_item_id
                                  AND OTT.TRANSACTION_TYPE_ID =
                                      CMS.ORDER_TYPE_ID
                                  AND ff.flex_value = ott.Attribute1
                                  AND cms.origin = '1'
                                  AND ff.flex_value_set_id = 1009855
                                  AND msi.organization_id =
                                      (SELECT min(OOD.organization_id)
                                         FROM org_organization_definitions OOD
                                        WHERE OOD.operating_unit = 102)
                               UNION ALL
                               SELECT cms.gl_date DOC_DATE,
                                      'ERP' DOC_SOURCE,
                                      ott.attribute1 Division,
                                      msi.segment1 ITEM_CODE,
                                      msi.description ITEM_DESC,
                                      0 unit_sales,
                                      0 total_sales,
                                      0 cost_sale,
                                      nvl(to_number(trim('-' from cms.cogs_amount)),0) cost_return,
                                      0 disc_tot
                                 FROM mtl_item_categories      micat,
                                      mtl_system_items         msi,
                                      mtl_categories           mcat,
                                      mtl_categorY_SETS        mcats,
                                      OE_TRANSACTION_TYPES_all OTT,
                                      CST_MARGIN_SUMMARY       cms,
                                      FND_FLEX_VALUES_VL       ff
                                WHERE mcat.CATEGORY_ID = MICat.CATEGORY_ID
                                  AND mcats.CATEGORY_SET_ID =
                                      MICat.CATEGORY_SET_ID
                                  AND mcats.CATEGORY_SET_ID = 1
                                  AND micat.inventory_item_id =
                                      msi.inventory_item_id
                                  AND micat.organization_id =
                                      msi.organization_id
                                  AND MSI.inventory_item_id =
                                      CMS.parent_inventory_item_id
                                  AND OTT.TRANSACTION_TYPE_ID =
                                      CMS.ORDER_TYPE_ID
                                  AND ff.flex_value = ott.Attribute1
                                  AND cms.origin = '2'
                                  AND ff.flex_value_set_id = 1009855
                                  AND msi.organization_id =
                                      (SELECT min(OOD.organization_id)
                                         FROM org_organization_definitions OOD
                                        WHERE OOD.operating_unit = 102)
                               )
                              UNION ALL (SELECT t.DOC_DATE,
                                               'POS' DOC_SOURCE,
                                               ' ' Division,
                                               t.ITEM_CODE,
                                               t.ITEM_DESC,
                                               (t.unit_sales) unit_sales,
                                               (t.total_sales) total_sales,
                                               (t.iv_qty * t.iv_cost) cost_sale,
                                               (t.cn_qty * t.cn_cost) cost_return,
                                               t.disc_tot
                                          FROM TAC_DISC_POS_SALE_AND_COST_V t
                                        ))
                       GROUP BY DOC_DATE,
                                DOC_SOURCE,
                                Division,
                                ITEM_CODE,
                                ITEM_DESC) t
               WHERE (t.PRODUCT_GROUP IN
                     (select *
                         from the (select cast(f_str_comma2list(product_groups) as
                                               t_str_commadelimited_tab)
                                     from dual)))
                 AND (t.DOC_DATE BETWEEN date_from AND date_to)
               GROUP BY t.DOC_SOURCE,
                        t.DIVISION,
                        t.PRODUCT_GROUP,
                        t.ITEM_CODE,
                        t.ITEM_DESC
               ORDER BY t.PRODUCT_GROUP ASC,
                        t.DOC_SOURCE    ASC,
                        t.DIVISION      ASC,
                        t.ITEM_DESC     ASC) loop
      line_data := r.product_group || pipestr || 
        r.doc_source || pipestr || 
        r.division || pipestr || 
        r.item_code || pipestr || 
        disc_wrap_cell_str(r.item_desc) || pipestr ||
        r.unit_sales_sum || pipestr || 
        to_char(r.total_sales_sum, fmt) || pipestr || 
        to_char(r.total_sales_sum_excvat, fmt) || pipestr || 
        to_char(r.price_per_unit_excvat, fmt) || pipestr || 
        to_char(r.total_cost_sum, fmt) || pipestr || 
        to_char(r.cost_per_unit, fmt) || pipestr || 
        to_char(r.margin, fmt) || pipestr || 
        to_char(r.disc_tot_sum, fmt);
      write_output(line_data);
    end loop;
  
    err_code := '0';
    err_msg  := 'Report was generated successfully';
  exception
    /*throwing errors*/
    when others then
      err_code := sqlcode;
      err_msg  := sqlerrm;
  end;
end TAC_Dev_Sale_Cost_by_Product;
