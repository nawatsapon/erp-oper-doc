create or replace package PKG_VENDINGM_SALES is

  -- Author  : T888062
  -- Created : 28/10/2015 13:25:48
  -- Purpose : For be using in reciept generation only for order that comes from Vending Machine

  -- Public function and procedure declarations
  procedure process_reciept(
    p_error_code              out number,
    p_error_mesg              out varchar2,
    p_receipt_no              out varchar2,
    p_document_date           out varchar2,
    p_job_no                  out varchar2,
    p_job_seq                 out varchar2,
    p_vending_code            varchar2,
    p_submission_date         varchar2,
    p_payment_code            varchar2,
    p_comma_separated_serials varchar2,
    p_cus_passport            varchar2,
    p_cus_fname               varchar2,
    p_cus_lname               varchar2,
    p_cus_address             varchar2);

end PKG_VENDINGM_SALES;
