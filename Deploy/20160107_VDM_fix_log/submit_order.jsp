<%@ page 
	contentType="application/json; charset=UTF-8" 
	language="java" 
	import="java.sql.*,
		javax.sql.*,
		java.io.*,
		java.util.regex.*,
		javax.naming.*,
		javax.naming.directory.*,
		java.util.*,
		java.lang.*,
		java.lang.Exception,
		java.util.Date,
		org.json.*,
		java.text.SimpleDateFormat" 
	errorPage="" %>
<%
	/* Transaction begins here */
	Date dTranBegin = new Date();
	
	/* 
	 * POST /webapp/kios/submit_order.jsp 
	 * Otherwise, throwing error.
	 */
	request.setCharacterEncoding("UTF-8");
	
	if ("GET".equalsIgnoreCase(request.getMethod())) {
		out.println("Error. Invalid http method. Only POST request is permitted.");
		return;
	}
	
	String firstName = "NULL";
    String lastName = "NULL";
    String passportId = "NULL";
    String address = "NULL";
    String subinvCode = "NULL";
    String paymentType = "NULL";
    String submissionDate = "NULL";
    String serials = "NULL";
    String vendingCode = "NULL";
    
	int P_ERROR_CODE 			= 1;
	int P_ERROR_DESC 			= 2;
	int P_INVOICE_NO 			= 3;
	int P_INVOICE_DATE 			= 4;
	int P_JOB_MONITOR_NO 		= 5;
	int P_JOB_MONITOR_SEQ_NO 	= 6;
    int P_VENDING_CODE 			= 7;
    int P_SUBMISSION_DATE 		= 8;
    int P_PAYMENT_METHOD 		= 9;
    int P_SERIALS 				= 10;
    int P_PASSPORT 				= 11;
    int P_CUSTOMER_FIRST_NAME 	= 12;
    int P_CUSTOMER_LAST_NAME 	= 13;
    int P_CUSTOMER_COUNTRY 		= 14;
    
    SimpleDateFormat defaultDateFmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    
    String rawJson = "";
    String returnDocNo = "";
    String returnDocDate = "";
    String returnJobNo = "";
    String returnJobSeq = ""; 
    
    int errCode = 0;
    String errCodeProg = "-1";
    String errDesc = "";
    boolean optSuc = false;
    Connection con = null;
    
    try {
        // Collect data from request content
	    BufferedReader reader = request.getReader();
	    String line;
	    while ((line = reader.readLine()) != null) {
	        rawJson += line;
	    }
		if (rawJson == "") {
			out.println("Error. Invalid posted data.");
			return;
		}
		
		// Reading all submitted data
		JSONObject postJson = new JSONObject(rawJson);
		
		if (postJson.has("customer")) {
			JSONObject customer = postJson.getJSONObject("customer");
			if (customer.has("firstname"))
				firstName = customer.getString("firstname");
			if (customer.has("lastname"))
	    		lastName = customer.getString("lastname");
	    	if (customer.has("passport_id"))
	    		passportId = customer.getString("passport_id");
	    	if (customer.has("address")) {
	    		JSONObject addr = customer.getJSONObject("address");
	    		if (addr.has("country"))
	    			address = customer.getJSONObject("address").getString("country");
	    	}
		}
		
		if (postJson.has("items")) {
		    JSONArray serialArray = postJson.getJSONArray("items");
		    String SIMs = "";
		    for (int i = 0; i< serialArray.length(); i++) {
		    	SIMs += serialArray.getString(i)+",";
		    }
		    if (SIMs != "")
		    	serials = SIMs.substring(0, SIMs.length()-1);
	    }
	    
	    if (postJson.has("sale_person"))
	    	vendingCode = postJson.getString("sale_person");
	    if (postJson.has("payment_type"))
	    	paymentType = postJson.getString("payment_type");
	    if (postJson.has("submission_date"))
	    	submissionDate = postJson.getString("submission_date");
	    //subinvCode = postJson.getString("location");
	    
	    /* Validate all */
	    if (firstName.equalsIgnoreCase("NULL") || firstName == "" || 
	    	lastName.equalsIgnoreCase("NULL") || lastName == "" || 
	    	passportId.equalsIgnoreCase("NULL") || passportId == "" || 
	    	address.equalsIgnoreCase("NULL") || address == "" || 
	    	paymentType.equalsIgnoreCase("NULL") || paymentType == "" || 
	    	submissionDate.equalsIgnoreCase("NULL") || submissionDate == "" || 
	    	serials.equalsIgnoreCase("NULL") || serials == "" || 
	    	vendingCode.equalsIgnoreCase("NULL") || vendingCode == "") {
	    	
	    	String s = "firstname=" + firstName + ", " +
	    		"lastname=" + lastName + ", " +
	    		"passport_id=" + passportId + ", " +
	    		"address(country)=" + address + ", " +
	    		"payment_type=" + paymentType + ", " +
	    		"submission_date=" + submissionDate + ", " +
	    		"sale_person=" + vendingCode + ", " +
	    		"items=[" + serials + "]";
	    	throw new JSONException("There might be some missing data points. See, " + s + ".");
	    }
	    // END - Reading all submitted data
        
        Context ctx = new InitialContext();
		DataSource odsconn = (DataSource)ctx.lookup("jdbc/PosDS");
		con = odsconn.getConnection();

        CallableStatement cs = con.prepareCall("{call PKG_VENDINGM_SALES.process_reciept(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
        cs.registerOutParameter(P_ERROR_CODE, Types.INTEGER);
        cs.registerOutParameter(P_ERROR_DESC, Types.VARCHAR);
        cs.registerOutParameter(P_INVOICE_NO, Types.VARCHAR);
        cs.registerOutParameter(P_INVOICE_DATE, Types.VARCHAR);
        cs.registerOutParameter(P_JOB_MONITOR_NO, Types.VARCHAR);
        cs.registerOutParameter(P_JOB_MONITOR_SEQ_NO, Types.VARCHAR);
        
        cs.setString(P_VENDING_CODE, vendingCode);
        cs.setString(P_SUBMISSION_DATE, submissionDate);
        cs.setString(P_PAYMENT_METHOD, paymentType);
        cs.setString(P_SERIALS, serials);
        cs.setString(P_PASSPORT, passportId);
        cs.setString(P_CUSTOMER_FIRST_NAME, firstName);
        cs.setString(P_CUSTOMER_LAST_NAME, lastName);
        cs.setString(P_CUSTOMER_COUNTRY, address);
        cs.execute();

		errCode = cs.getInt(P_ERROR_CODE);
        errDesc = cs.getString(P_ERROR_DESC);
        
        returnDocNo = cs.getString(P_INVOICE_NO);
        returnDocDate = cs.getString(P_INVOICE_DATE);
        returnJobNo = cs.getString(P_JOB_MONITOR_NO);
        returnJobSeq = cs.getString(P_JOB_MONITOR_SEQ_NO);
        cs.close();
        con.close();
        
        if (errCode == 0) {
        	optSuc = true;
        } else {
        	errCode = Math.abs(errCode);
        }
        
    } catch (SQLException e) {
    	errCode = e.getErrorCode();
    	errDesc = "Error on POS database: " + e.getMessage().split("\n")[0];
    } catch(JSONException e) {
    	errCode = -1;
    	errDesc = "Invalid JSON format. " + e.getMessage();
    } catch (Exception e) {
    	errCode = -99;
    	errDesc = e.getMessage();
    } finally {
        if (con != null) con = null;
    }
    
    /* Map descriptive error code */
    switch (errCode) {
    	case 20992: 
    		errCodeProg = "ERR002"; break;
    	case 20993: 
    		errCodeProg = "ERR003"; break;
    	case 20994: 
    		errCodeProg = "ERR004"; break;
    	case 20995: 
    		errCodeProg = "ERR005"; break;
    	case 20996: 
    		errCodeProg = "ERR006"; break;		
    	case 20997: 
    		errCodeProg = "ERR007"; break;	
    	case 20998: 
    		errCodeProg = "ERR008"; break;	
    	case 20999: 
    		errCodeProg = "ERR009"; break;	
    	case 20881: 
    		errCodeProg = "ERR010"; break;	
    	case -1: 
    		errCodeProg = "ERR011";
    		break;	
    	case 0: 
    		errCodeProg = "";
    		break;	
    	default: 
    		errCodeProg = "ERR000";
    		errDesc = "Internal Server Error";
    }
    
    String responseText = "";
    if (optSuc == true) {
    	responseText = "{" +  	
	    	"\"response_date\": \"" + returnDocDate + "\"," +
	    	"\"status\": \"SUCCESS\"," +
	    	"\"response_message\": \"Order number " + returnDocNo + " has been saved successfully\"," +
	    	"\"errors\": \"NULL\"" +
	    "}";
    } else {
		responseText = "{" +
	    	"\"response_date\": \"" + defaultDateFmt.format(new Date()) + "\"," +
	    	"\"status\": \"FAIL\"," +
	    	"\"response_message\": \"The order was terminated by POS\"," +
	    	"\"errors\": { \"code\": \"" + errCodeProg + "\", \"message\": \"" + errDesc + "\"}" +
	    "}";
	}
	
	/* Write Service Call Logs */
	String logFile = "/posapp/logs/Vending_to_POS_Order_Submission_" +  new SimpleDateFormat("yyyyMMdd").format(dTranBegin) + ".txt";
	Writer wLog = null;	 
	
	try {
		
		boolean newlyCreated = !new File(logFile).exists();
		wLog = new FileWriter(logFile, true);
		
		if (newlyCreated) {
			wLog.write("BEGINDATETIME|ENDDATETIME|IP_CALLER|POS_SERVER|ENDPOINT|SUMISSION_DATETIME|CUSTOMER|PASSPORT|ADDRESS|SN|SALE_PERSON|PAYMENT_TYPE|RETURN_STATUS|INVOICE_NUMBER|INVOICE_DATE|ERROR_CODE|ERROR_MEG|MONITOR_JOB_ID|MONITOR_JOB_SEQ|REQUEST_JSON\r\n");
			wLog.flush();
		}
		wLog.write(defaultDateFmt.format(dTranBegin) + "|"); //BEGINDATETIME
		wLog.write(defaultDateFmt.format(new Date()) + "|"); //ENDDATETIME
		wLog.write(request.getRemoteAddr() + "|"); //IP_CALLER
		wLog.write(request.getServerName() + "|"); //POS_SERVER
		wLog.write(request.getProtocol().substring(0, 4).toLowerCase() + "://" + request.getServerName() + request.getRequestURI() + "|"); //ENDPOINT
		wLog.write(submissionDate + "|"); //SUMISSION_DATETIME
		wLog.write(firstName + " " + lastName + "|"); //CUSTOMER
		wLog.write(passportId + "|"); //PASSPORT
		wLog.write(address + "|"); //ADDRESS
		wLog.write(serials + "|"); //SN
		wLog.write(vendingCode + "|"); //SALE_PERSON
		wLog.write(paymentType + "|"); //PAYMENT_TYPE
		wLog.write((optSuc?"SUCCESS":"FAIL") + "|"); //RETURN_STATUS
		wLog.write((optSuc? returnDocNo:"") + "|"); //INVOICE_NUMBER
		wLog.write((optSuc? returnDocDate:"") + "|"); //INVOICE_DATE
		wLog.write(errCodeProg + "|"); //ERROR_CODE
		wLog.write(errDesc + "|"); //ERROR_MEG
		wLog.write(returnJobNo + "|"); //MONITOR_JOB_ID
		wLog.write(returnJobSeq + "|"); //MONITOR_JOB_SEQ
		wLog.write(rawJson.replace('\r', '\0').replace('\n', '\0') + "\r\n");
		wLog.flush();
	} catch(Exception ex) {
		System.out.println(new Date().toString() + " Error while trying to write logs into " + logFile + ". See message, " + ex.getMessage());
		ex.printStackTrace();
	} finally {
		if (wLog != null) wLog.close();
	}
    /* Logs ends */
%>


<%= responseText %>