UPDATE [TAC_BANGKOK\T889774].Tbl_Worker_Dev
   SET POSITION_LEVEL = 3,
	   JL_CODE = 'M4',
	   EMP_TPOST = 'Acting Head of Finance Operation Division',
	   EMP_EPOST = 'Acting Head of Finance Operation Division'
 where EMP_LOGIN = 'T869321'
GO

UPDATE [TAC_BANGKOK\T889774].Tbl_Worker_Dev
   SET MGR_LOCAL_ID = (SELECT top 1 EMP_ID FROM [CENTRAL_EMPLOYEE].[dbo].[Tbl_Worker]where EMP_LOGIN = 'T869321')
   , MGR_GLOBAL_ID = (SELECT top 1 GLOBAL_EMP_ID FROM [CENTRAL_EMPLOYEE].[dbo].[Tbl_Worker]where EMP_LOGIN = 'T869321')
 where EMP_LOGIN in ('T882903','T882901','T915021','KalayaCh')
GO


select EMP_ID,GLOBAL_EMP_ID,POSITION_LEVEL,EMP_TPOST,
	   EMP_EPOST, acting.* from [TAC_BANGKOK\T889774].Tbl_Worker_Dev acting where EMP_LOGIN = 'T869321'

select EMP_ID,GLOBAL_EMP_ID,POSITION_LEVEL,EMP_TPOST,
	   EMP_EPOST, cfo.* from [TAC_BANGKOK\T889774].Tbl_Worker_Dev cfo where GLOBAL_emp_ID = '875678'

select MGR_LOCAL_ID,MGR_GLOBAL_ID, emp.* from [TAC_BANGKOK\T889774].Tbl_Worker_Dev emp 
where EMP_LOGIN in ('T882903','T882901','T915021','KalayaCh')