
  CREATE OR REPLACE FORCE VIEW "POS"."INTERNAL_RECEIVE_LITE_V" ("SHIPMENT_HEADER_ID", "SHIPMENT_LINE_ID", "ITEM_CODE", "SHIPPED_QUANTITY", "REQ_LINE", "DELIVERY_LINE_ID", "REQUISITION_LINE_ID", "REQ_DISTRIBUTION_ID", "REQUEST_NO", "DELIVERY_NUMBER", "SERIAL_NUMBER", "LOT_NUMBER", "REQ_NO", "OU_CODE", "SUBINV_CODE") AS 
  SELECT
    /*+DRIVING_SITE(B)*/
    shipment_header_id,
    shipment_line_id,
    D.item_code,
    shipped_quantity,
    req_line,
    to_number(DECODE(wsh.container_flag,'N',wsh.delivery_detail_id,NULL)) delivery_line_id,
    requisition_line_id,
    req_distribution_id,
    ORIG_SYS_DOCUMENT_REF request_no,
    delivery_id delivery_number,
    serial_number,
    lot_number,
    req_no,
    in_req_head.ou_code,
    in_req_head.subinv_code
  FROM in_req_head,
    in_item D,
    (SELECT wsh_v.shipment_header_id,
      shipment_line_id,
      G.line_num req_line,
      source_document_id,
      source_document_line_id,
      ORIG_SYS_DOCUMENT_REF,
      req_distribution_id,
      requisition_line_id,
      delivery_detail_id,
      container_flag,
      shipped_quantity,
      inventory_item_id,
      delivery_id,
      serial_number,
      lot_number
    FROM
      (SELECT wda.delivery_id,
        wdd.container_flag,
        wdd.delivery_detail_id,
        wdd.source_header_id,
        wdd.inventory_item_id,
        wdd.serial_number,
        wdd.lot_number,
        wdd.source_line_id,
        b.orig_sys_document_ref,
        B.source_document_id,
        B.source_document_line_id,
        wdd.shipped_quantity,
        RANK() OVER (PARTITION BY DELIVERY_ID, wdd.INVENTORY_ITEM_ID ORDER BY wdd.SHIPPED_QUANTITY, wdd.DELIVERY_DETAIL_ID) AS WSH_RANK_ID,
        j.shipment_header_id,
        j.shipment_line_id,
        j.req_distribution_id,
        RANK() OVER (PARTITION BY j.SHIPMENT_HEADER_ID, j.ITEM_ID ORDER BY j.QUANTITY_SHIPPED, j.SHIPMENT_LINE_ID) AS J_RANK_ID
      FROM wsh_delivery_details@"POS2ERP_APPS.US.ORACLE.COM" wdd,
        wsh_delivery_assignments@"POS2ERP_APPS.US.ORACLE.COM" wda,
        OE_ORDER_LINES_ALL@"POS2ERP_APPS.US.ORACLE.COM" B,
        RCV_SHIPMENT_LINES@"POS2ERP_APPS.US.ORACLE.COM" j
      WHERE wdd.released_status  = 'C'
      AND wda.delivery_detail_id = wdd.delivery_detail_id
      AND wdd.source_header_id   = b.header_id
      AND wdd.source_line_id     = b.line_id
      AND J.QUANTITY_SHIPPED     > J.QUANTITY_RECEIVED
      AND J.ITEM_ID              = wdd.INVENTORY_ITEM_ID
      AND J.QUANTITY_SHIPPED     = wdd.SHIPPED_QUANTITY
      ) wsh_v,
      PO_REQUISITION_LINES_ALL G,
      RCV_SHIPMENT_HEADERS K
    WHERE j_rank_id                   = wsh_rank_id
    AND wsh_v.SOURCE_DOCUMENT_LINE_ID = G.REQUISITION_LINE_ID
    AND wsh_v.source_document_id      = G.requisition_header_id
    AND wsh_v.SHIPMENT_HEADER_ID      = K.SHIPMENT_HEADER_ID
    AND TO_CHAR(wsh_v.delivery_id)    = K.SHIPMENT_NUM
    ) wsh
  WHERE req_type                             = 'I'
  AND int_req_type                           = 'C'
  AND req_status                            IN ('A', 'I', 'R')
  AND tf_c_o                                IS NOT NULL
  AND SUBSTR(wsh.ORIG_SYS_DOCUMENT_REF, -13) = SUBSTR(in_req_head.req_no, -13)
  AND NOT EXISTS
    (SELECT 'X'
    FROM PB_PARAMETER_SETUP L,
      IN_TRANS_DTL F
    WHERE F.OU_CODE              = in_req_head.OU_CODE
    AND F.SUBINV_CODE            = in_req_head.SUBINV_CODE
    AND F.TRAN_TYPE_CODE         = L.PARAMETER_VALUE
    AND L.PGM_SETUP              = 'DTINRT08'
    AND L.PARAMETER_NAME         = 'Receive from Item Requisition'
    AND F.SHIPMENT_LINE_ID       = wsh.SHIPMENT_LINE_ID
    AND F.SHIPMENT_HEADER_ID     = wsh.SHIPMENT_HEADER_ID
    AND F.OA_REQ_LINE_ID         = wsh.REQUISITION_LINE_ID
    AND F.OA_REQ_DISTRIBUTION_ID = wsh.REQ_DISTRIBUTION_ID
    AND F.OA_REQ_LINE_NUM        = wsh.req_line
    )
  AND D.ou_code             = in_req_head.ou_code
  AND wsh.inventory_item_id = d.oa_item;
