#!/bin/sh
# +==========================================================================+
# |
# |   Program Name:     TAC - Find and Extract zip file - TCNX
# |
# |   Program Type:     Korn Shell Script
# |
# |   Created by:       Wisut Munapisith 
# |
# |
# |   Purpose:          Generic shell script to
# |                   1. Find zip file from LYNX22
# |                   2. Extract zip file to LYNX22
# |
# |
# |   Called By:        Oracle Applications Concurrent Manager Request
# |			run ln -s $FND_TOP/bin/fndcpesr DTTCNX001_1
# |
# |   Program Parameters:
# |      0: Program file name
# |      1: User ID/Password for connect database
# |      2: Last login ID
# |      3: User Name for login application
# |      4: Request ID
# |
# |   Program Parameters:
# |      5: Month
# |      6: Year
# |      7: Folder Name for Read
# |      8: Folder Name for Archive
# |      9: Folder Name for Error
# |
# +===========================================================================+
# | Change History
# | Date      Author      Version Description
# | --------- ----------- ------- ------------------------------------------------
# | 16-Aug-16  Wisut M.    1.00    Created Program
# |
# +===========================================================================+

#echo "Total Number of Parameters : $#"
#echo "Print System Parameters"
#echo "0: $0"
#echo "1: $1"
#echo "2: $2"
#echo "3: $3"
#echo "4: $4"
#echo " "
#echo "Print Program Parameters"
#echo "5: $5"
#echo "6: $6"
#echo "7: $7"
#echo "8: $8"
#echo "9: $9"

# ----------------------------------------------------------------------------
# Declare variable
# ----------------------------------------------------------------------------

v_progfile=$0
v_userid=$1
v_loginid=$2
v_username=$3
v_requestid=$4
v_month=$5
v_year=$6
v_ibxdir=$7
v_hisdir=$8
v_errdir=$9
v_logdir=${10}
v_expireday=${11}
v_logparafile="TAC_TCNX_FIND_AND_EXTRACT_PARAM.log"`date +%y%m%d%H%M%S`
v_filename="Dtac_$6_$5.zip"
v_logname="FIND_ZIP"
#v_logdir="ERP_TCNX_AR_LOG"
v_pathname=""
v_readpath=""
v_hispath=""
v_errpath=""
v_logpath=""
v_filecount=0
v_errortype=""
v_status=""
v_errorcode=""
v_errormsg=""

# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# Get path for read zip file
# ----------------------------------------------------------------------------

load_directories()
{
(sqlplus -s /nolog << end_of_sql
conn $v_userid
set serveroutput on size 99999
exec TAC_PKG_TCNX_CUST.P_GET_DIRECTORIES('IBX','$v_ibxdir');
exec TAC_PKG_TCNX_CUST.P_GET_DIRECTORIES('HIS','$v_hisdir');
exec TAC_PKG_TCNX_CUST.P_GET_DIRECTORIES('ERR','$v_errdir');
exec TAC_PKG_TCNX_CUST.P_GET_DIRECTORIES('LOG','$v_logdir');
exit
end_of_sql
) > $v_logparafile

#v_readpath=`cat $v_logparafile | sed -n '1p;1q' | sed -e "s/PARA_IBX_PATH=//"`
#v_hispath=`cat $v_logparafile | sed -n '2p;2q' | sed -e "s/PARA_HIS_PATH=//"`
#v_errpath=`cat $v_logparafile | sed -n '3p;3q' | sed -e "s/PARA_ERR_PATH=//"`
#v_logpath=`cat $v_logparafile | sed -n '3p;3q' | sed -e "s/PARA_LOG_PATH=//"`
v_readpath=`grep -i "PARA_IBX_PATH" $v_logparafile | sed -e "s/PARA_IBX_PATH=//"`
v_hispath=`grep -i "PARA_HIS_PATH" $v_logparafile | sed -e "s/PARA_HIS_PATH=//"`
v_errpath=`grep -i "PARA_ERR_PATH" $v_logparafile | sed -e "s/PARA_ERR_PATH=//"`
v_logpath=`grep -i "PARA_LOG_PATH" $v_logparafile | sed -e "s/PARA_LOG_PATH=//"`

echo "Inbox Path: $v_readpath"
echo "History Path: $v_hispath"
echo "Error Path: $v_errpath"
echo "Log Path: $v_logpath"

rm $v_logparafile
}

# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# Insert log summary
# ----------------------------------------------------------------------------

insert_logsummary()
{
(sqlplus -s /nolog << end_of_sql
conn $v_userid
set serveroutput on size 99999
exec TAC_PKG_TCNX_CUST.P_INSERT_LOG002_SUMMARY($v_requestid);
exit
end_of_sql
)
}

# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# Insert log detail
# ----------------------------------------------------------------------------

insert_logdetail()
{
(sqlplus -s /nolog << end_of_sql
conn $v_userid
set serveroutput on size 99999
exec TAC_PKG_TCNX_CUST.P_INSERT_LOG002_DETAIL($v_requestid,$v_filecount,'$v_errortype','$v_pathname','$v_filename','$v_status','$v_errorcode','$v_errormsg');
exit
end_of_sql
)
}

# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# Log Monitor
# ----------------------------------------------------------------------------

log_monitor()
{
(sqlplus -s /nolog << end_of_sql
conn $v_userid
set serveroutput on size 99999
exec TAC_PKG_TCNX_CUST.P_LOG_MONITOR('$v_logname',$v_requestid,'$v_logdir');
exit
end_of_sql
)
}

# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# Set Complete Status
# ----------------------------------------------------------------------------

set_complete_status()
{
(sqlplus -s /nolog << end_of_sql
conn $v_userid
set serveroutput on size 99999
exec TAC_PKG_TCNX_CUST.P_SET_COMPLETE_STATUS;
exit
end_of_sql
)
}

# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# Main Program
# ----------------------------------------------------------------------------
echo 'Starting...'

#echo "Total Number of Parameters : $#"
#echo '+----------------------------------------------------------------------------+'
#echo "Print System Parameters:"
#echo '+----------------------------------------------------------------------------+'
#echo "Program File: $v_progfile"
#echo "User ID: $v_userid"
#echo "Logiin: $v_loginid"
#echo "User Name: $v_username"
#echo "Request ID: $v_requestid"
#echo " "
echo '+----------------------------------------------------------------------------+'
echo "Print Program Parameters:"
echo '+----------------------------------------------------------------------------+'
echo "Month: $v_month"
echo "Year: $v_year"
echo "Inbox directory name: $v_ibxdir"
echo "History directory name: $v_hisdir"
echo "Error directory name: $v_errdir"
echo "Log directory name: $v_logdir"
echo "Log parameter file: $v_logparafile"
echo "Zip file name: $v_filename"
echo "Log expiration day: $v_expireday"
echo " "
echo '+----------------------------------------------------------------------------+'
echo "Load Directory Path:"
echo '+----------------------------------------------------------------------------+'
load_directories
echo '+----------------------------------------------------------------------------+'
echo "Insert Log Summary:"
echo '+----------------------------------------------------------------------------+'
insert_logsummary
echo '+----------------------------------------------------------------------------+'
echo "Process:"
echo '+----------------------------------------------------------------------------+'

if [ -d "$v_readpath" ]; then
   echo "Path: '$v_readpath' was found."
   
   if [ -e "$v_readpath/$v_filename" ]; then
      echo "File: '$v_filename' was found."
      
      unzip -o $v_readpath/$v_filename -d $v_readpath
      
      if [ "$?" = 0 ]; then
         echo "Can extract zip file(s): '$v_filename'."
         
         if [ -d "$v_hispath" ]; then
            echo "Path: '$v_hispath' was found."
            
            mv $v_readpath/$v_filename $v_hispath/$v_filename`date +%y%m%d%H%M%S`
            
            if [ "$?" = 0 ]; then
               echo "Zip file move to path: '$v_hispath'."
               
	       echo $v_month > $v_readpath/filelist.txt
               echo $v_year >> $v_readpath/filelist.txt
	       ls $v_readpath/*.xml | awk -F'/' '{print $NF}' >> $v_readpath/filelist.txt

               v_filecount=`grep -c '' $v_readpath/filelist.txt`
	       v_filecount=$v_filecount-2
	       v_pathname=$v_readpath
	       v_errortype="Extract zip file"
               v_status="Complete"
               v_errorcode=""
               v_errormsg=""
               
               echo "File Count: $v_filecount"
               echo "Error Type: $v_errortype"
               echo "Status: $v_status"
               echo "Error Code: $v_errorcode"
               echo "Error Message: $v_errormsg"
               
               echo '+----------------------------------------------------------------------------+'
               echo "Insert Log Detail: "
               echo '+----------------------------------------------------------------------------+'
               insert_logdetail
            else
               echo "DTTCNX001-003: Cannot move zip file (Zip file extraction succeed)."
               
               v_pathname=$v_hispath
	       v_errortype="Move file"
               v_status="Error"
               v_errorcode="DTTCNX001-003"
               v_errormsg="Cannot move zip file (Zip file extraction succeed)."
               
               echo "Error Type: $v_errortype"
               echo "Status: $v_status"
               echo "Error Code: $v_errorcode"
               echo "Error Message: $v_errormsg"
               
               echo '+----------------------------------------------------------------------------+'
               echo "Insert Log Detail: "
               echo '+----------------------------------------------------------------------------+'
               insert_logdetail
               
               echo '+----------------------------------------------------------------------------+'
               echo "Set Complete Status: "
               echo '+----------------------------------------------------------------------------+'
               #set_complete_status
            fi
         else
            echo "DTTCNX001-002: Not found defined path for move (Zip file extraction succeed)."
            
            v_pathname=$v_hispath
	    v_errortype="Move file"
            v_status="Error"
            v_errorcode="DTTCNX001-002"
            v_errormsg="Cannot move zip file (Zip file extraction succeed)."
            
            echo "Error Type: $v_errortype"
            echo "Status: $v_status"
            echo "Error Code: $v_errorcode"
            echo "Error Message: $v_errormsg"
            
            echo '+----------------------------------------------------------------------------+'
            echo "Insert Log Detail: "
            echo '+----------------------------------------------------------------------------+'
            insert_logdetail
            
            echo '+----------------------------------------------------------------------------+'
            echo "Set Complete Status: "
            echo '+----------------------------------------------------------------------------+'
            #set_complete_status
         fi
      else
         echo "DTTCNX001-007: Cannot extract zip file."
         
         v_pathname=$v_readpath
	 v_errortype="Extract zip file"
         v_status="Error"
         v_errorcode="DTTCNX001-007"
         v_errormsg="Cannot extract zip file."
         
         echo "Error Type: $v_errortype"
         echo "Status: $v_status"
         echo "Error Code: $v_errorcode"
         echo "Error Message: $v_errormsg"
         
         echo '+----------------------------------------------------------------------------+'
         echo "Insert Log Detail: "
         echo '+----------------------------------------------------------------------------+'
         insert_logdetail
         
         echo '+----------------------------------------------------------------------------+'
         echo "Set Complete Status: "
         echo '+----------------------------------------------------------------------------+'
         #set_complete_status
         
         if [ -d "$v_errpath" ]; then
            echo "Path: '$v_errpath' was found."
            
            mv $v_readpath/$v_filename $v_errpath/$v_filename`date +%y%m%d%H%M%S`
            
            if [ "$?" = "0" ]; then
               echo "Zip file move to path: '$v_errpath'."
	    else
               echo "DTTCNX001-005: Cannot move zip file (Zip file extraction not succeed)."
               
               v_pathname=$v_errpath
	       v_errortype="Move file"
               v_status="Error"
               v_errorcode="DTTCNX001-005"
               v_errormsg="Cannot move zip file (Zip file extraction not succeed)"
               
               echo "Error Type: $v_errortype"
               echo "Status: $v_status"
               echo "Error Code: $v_errorcode"
               echo "Error Message: $v_errormsg"
               
               echo '+----------------------------------------------------------------------------+'
               echo "Insert Log Detail: "
               echo '+----------------------------------------------------------------------------+'
               insert_logdetail
               
               echo '+----------------------------------------------------------------------------+'
               echo "Set Complete Status: "
               echo '+----------------------------------------------------------------------------+'
               #set_complete_status
            fi
         else
            echo "DTTCNX001-004: Not found defined path for move (Zip file extraction not succeed)."
            
            v_pathname=$v_errpath
	    v_errortype="Move file"
            v_status="Error"
            v_errorcode="DTTCNX001-004"
            v_errormsg="Not found defined path for move (Zip file extraction not succeed)."
            
            echo "Error Type: $v_errortype"
            echo "Status: $v_status"
            echo "Error Code: $v_errorcode"
            echo "Error Message: $v_errormsg"
            
            echo '+----------------------------------------------------------------------------+'
            echo "Insert Log Detail: "
            echo '+----------------------------------------------------------------------------+'
            insert_logdetail
            
            echo '+----------------------------------------------------------------------------+'
            echo "Set Complete Status: "
            echo '+----------------------------------------------------------------------------+'
            #set_complete_status
         fi
      fi
   else
      echo "DTTCNX001-006: Zip file not found."
      
      v_pathname=$v_readpath
      v_errortype="Find zip file"
      v_status="Error"
      v_errorcode="DTTCNX001-006"
      v_errormsg="Zip file not found."
      
      echo "Error Type: $v_errortype"
      echo "Status: $v_status"
      echo "Error Code: $v_errorcode"
      echo "Error Message: $v_errormsg"
      
      echo '+----------------------------------------------------------------------------+'
      echo "Insert Log Detail: "
      echo '+----------------------------------------------------------------------------+'
      insert_logdetail
      
      echo '+----------------------------------------------------------------------------+'
      echo "Set Complete Status: "
      echo '+----------------------------------------------------------------------------+'
      #set_complete_status
   fi
else
   echo "DTTCNX001-001: Not found defined path for read."
   
   v_pathname=$v_readpath
   v_errortype="Find zip file"
   v_status="Error"
   v_errorcode="DTTCNX001-001"
   v_errormsg="Not found defined path for read."
   
   echo "Error Type: $v_errortype"
   echo "Status: $v_status"
   echo "Error Code: $v_errorcode"
   echo "Error Message: $v_errormsg"
   
   echo '+----------------------------------------------------------------------------+'
   echo "Insert Log Detail: "
   echo '+----------------------------------------------------------------------------+'
   insert_logdetail
   
   echo '+----------------------------------------------------------------------------+'
   echo "Set Complete Status: "
   echo '+----------------------------------------------------------------------------+'
   #set_complete_status
fi

echo '+----------------------------------------------------------------------------+'
echo "Log Monitor:"
echo '+----------------------------------------------------------------------------+'
find $v_logpath/DTTCNX002_CHECK_ZIP_FILE*.TXT -mtime +$v_expireday -exec rm {} \;
echo 'Delete log file succeed.'
log_monitor

# ----------------------------------------------------------------------------
# End of Unix Program
