alter session set nls_language = 'AMERICAN';
----------------------------------
-- PA Period
----------------------------------
-- 1 Backup PA_PERIODS_ALL
select * from apps.PA_PERIODS_ALL where period_name = 'FEB-20';
-- 1 Delete PA_PERIODS_ALL
delete from apps.PA_PERIODS_ALL where period_name = 'FEB-20';
commit;

----------------------------------
-- GL Period
----------------------------------
-- 2 Backup gl_periods
select * from apps.gl_periods where period_name = 'FEB-20';
-- 2 Update end date in gl_periods
update apps.gl_periods set end_date = last_day(start_date), last_update_date = sysdate where period_name = 'FEB-20';
commit;

-- 3 Backup gl_period_statuses
select * from apps.gl_period_statuses where period_name = 'FEB-20';
-- 3 Update end date in gl_period_statuses
update apps.gl_period_statuses set closing_status = 'N', last_update_date = sysdate where period_name = 'FEB-20' and closing_status <> 'N';
update apps.gl_period_statuses set end_date = last_day(start_date), last_update_date = sysdate where period_name = 'FEB-20'; 

commit;

-- 4 Backup gl_date_period_map
select * from apps.gl_date_period_map where period_name = 'FEB-20';
select * from apps.gl_date_period_map where period_name = 'FEB-20' and period_name = 'NOT ASSIGNED' and accounting_date = to_date('29/02/2020','dd/mm/yyyy');
-- 4 Assign accounting date 20-Feb-2020 to period FEB-20 in table gl_date_period_map
update gl_date_period_map set period_name = 'FEB-20' where period_type = 21 and period_name = 'NOT ASSIGNED' and accounting_date = to_date('29/02/2020','dd/mm/yyyy');

commit;
-----------------------------------------------------------
-- Manual Copy PA Caneldar from GL for all set of books
-----------------------------------------------------------
