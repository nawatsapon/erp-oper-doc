create or replace package Vending_Machine_To_POS is
  procedure export_sn_with_prices (
    err_msg   out varchar2,
    err_code  out varchar2,
    p_vending_code varchar2,
    p_document_date varchar2
  );
  procedure export_sim_sold_by_others (
    err_msg   out varchar2,
    err_code  out varchar2,
    p_vending_code varchar2,
    p_document_date varchar2
  );
end Vending_Machine_To_POS;
