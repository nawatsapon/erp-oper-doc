Select distinct   RESPONSIBILITY_ID,
  RESPONSIBILITY_NAME
from(
SELECT id SUB_MENU_ID,
  parent_id MENU_ID,
  RESPONSIBILITY_ID,
  RESPONSIBILITY_NAME,
  RPAD('.', (level-1)*2, '.')
  || id AS tree,
  level tree_level,
  CONNECT_BY_ROOT id                                   AS root_id,
  LTRIM(SYS_CONNECT_BY_PATH(promt_desc, '|'), '|') AS menu_path
  --,CONNECT_BY_ISLEAF                                    AS leaf
FROM
  (SELECT FND_RESPONSIBILITY_VL.RESPONSIBILITY_ID,
    FND_MENUS_VL.MENU_ID id,
    FND_MENU_ENTRIES_VL.SUB_MENU_ID parent_id,
    FND_RESPONSIBILITY_VL.RESPONSIBILITY_NAME,
    FND_RESPONSIBILITY_VL.DESCRIPTION AS res_desc,
    FND_MENUS_VL.Menu_NAME,
    FND_MENUS_VL.USER_MENU_NAME,
    FND_MENUS_VL.DESCRIPTION AS Menu_desc,
    FND_MENU_ENTRIES_VL.PROMPT,
    FND_MENU_ENTRIES_VL.DESCRIPTION promt_desc
  FROM FND_MENUS_VL,
    FND_MENU_ENTRIES_VL,
    FND_RESPONSIBILITY_VL
  WHERE 0                  =0
  AND FND_MENUS_VL.menu_id = FND_MENU_ENTRIES_VL.menu_id(+)
  AND FND_MENUS_VL.menu_id = FND_RESPONSIBILITY_VL.menu_id(+)
  ) tab
  START WITH id  = 68015
  --START WITH id is not null
  CONNECT BY parent_id  = PRIOR id
ORDER SIBLINGS BY id
)
where 0=0
--and menu_path like '%Suppliers Entry%'
AND RESPONSIBILITY_ID not IN 
  (SELECT FND_RESP_FUNCTIONS.REsponsibility_id
  FROM FND_RESP_FUNCTIONS
  WHERE 0       =0
  AND RULE_TYPE = 'F'
  AND ACTION_ID = 1348
  )
;


select * from FND_MENU_ENTRIES_VL where DESCRIPTION like 'Suppliers Entry';

select * from FND_MENU_ENTRIES_VL where MENU_ID = 68030 and SUB_MENU_ID is null ;