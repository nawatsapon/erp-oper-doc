SELECT INFO1,
  INFO2,
  INFO3,
  serial COPY1,
  case when count_row = max_row then '*AD' else '*DN' END COPY2,
  case when count_row = max_row then '*DN' END COPY3,
  case when count_row = max_row then '*AR' END COPY4,
  case when count_row = max_row then '*AI' END COPY5
FROM
  (SELECT ROW_NUMBER() OVER (PARTITION BY mtb.INVENTORY_ITEM_CODE,msn.current_subinventory_code, MIL.segment1 order by tmps.serial) count_row,
    COUNT(tmps.serial) OVER (PARTITION BY mtb.INVENTORY_ITEM_CODE,msn.current_subinventory_code, MIL.segment1) max_row,
    mtb.INVENTORY_ITEM_CODE INFO1,
    msn.current_subinventory_code INFO2,
    MIL.segment1 INFO3,
    tmps.serial
  FROM XX_TMP_SERIAL tmps,
    mtl_serial_numbers msn,
    mtl_material_transactions mmt,
    MTL_TRX_TYPES_VIEW mtt,
    (SELECT DISTINCT INVENTORY_ITEM_ID,
      segment1 INVENTORY_ITEM_CODE,
      description,
      ORGANIZATION_ID
    FROM Mtl_System_Items_B
    ) mtb,
    (SELECT lookup_code,
      meaning
    FROM mfg_lookups
    WHERE lookup_type = 'SERIAL_NUM_STATUS'
    ) slp,
    MTL_SECONDARY_INVENTORIES SUB,
    (SELECT DISTINCT ORGANIZATION_ID,
      SUBINVENTORY_CODE,
      INVENTORY_LOCATION_ID,
      segment1
    FROM MTL_ITEM_LOCATIONS
    ) MIL
  WHERE 0=0
    --and SUBSTR(msn.serial_number, 5, 16) = tmps.serial
  AND msn.serial_number              = tmps.serial
  AND msn.last_transaction_id        = mmt.transaction_id
  AND mmt.Transaction_Type_Id        = mtt.TRANSACTION_TYPE_ID(+)
  AND mmt.TRANSACTION_SOURCE_TYPE_ID = mtt.TRANSACTION_SOURCE_TYPE_ID(+)
  AND mmt.Transaction_Action_Id      = mtt.TRANSACTION_ACTION_ID(+)
  AND mmt.SUBINVENTORY_CODE          = SUB.SECONDARY_INVENTORY_NAME
  AND mmt.ORGANIZATION_ID            = SUB.ORGANIZATION_ID
  AND mmt.Inventory_Item_Id          = mtb.INVENTORY_ITEM_ID
  AND mmt.ORGANIZATION_ID            = mtb.ORGANIZATION_ID
  AND msn.status_id                  = slp.lookup_code
  AND msn.CURRENT_LOCATOR_ID         = MIL.INVENTORY_LOCATION_ID
  AND msn.CURRENT_ORGANIZATION_ID    = MIL.ORGANIZATION_ID
  AND msn.CURRENT_SUBINVENTORY_CODE  = MIL.SUBINVENTORY_CODE
  AND mmt.Transaction_Type_Id NOT   IN (118)
  AND msn.CURRENT_SUBINVENTORY_CODE <> 'N3G02B'
  )
ORDER BY 1,2,3 ;