--ALTER SESSION SET CURRENT_SCHEMA = sys;
DECLARE
TYPE d_TotalSize
IS
  TABLE OF NUMBER;
  t_TotalSize d_TotalSize;
TYPE d_file_name
IS
  TABLE OF VARCHAR2(100);
  t_file_name d_file_name;
TYPE d_sub_num
IS
  TABLE OF VARCHAR2(100);
  t_sub_num d_sub_num;
  t_blob BLOB;
  t_len       NUMBER;
  --t_TotalSize NUMBER;
  -- t_file_name VARCHAR2(100);
  -- t_sub_num VARCHAR2(100);
  t_output UTL_FILE.file_type;
  t_position  NUMBER := 1;
  t_chucklen  NUMBER := 4096;
  t_chuck raw(4096);
  t_remain NUMBER;
BEGIN
  -- Get length of blob
  SELECT DBMS_LOB.getlength (EREGISOWN.CUSTOMERATTACHMENT.ATTACHMENT),
    EREGISOWN.TEMP_SUB_NOT_FOUND.SUBSCRIBENUMBER
    || '_1.jpg',
    EREGISOWN.TEMP_SUB_NOT_FOUND.SUBSCRIBENUMBER BULK COLLECT
  INTO t_TotalSize,
    t_file_name,
    t_sub_num
  FROM EREGISOWN.TEMP_SUB_NOT_FOUND,
    EREGISOWN.APPLICATION_PREPAID,
   EREGISOWN.CUSTOMERATTACHMENT
  WHERE 0                                 =0
  AND EREGISOWN.TEMP_SUB_NOT_FOUND.SUBSCRIBENUMBER  = EREGISOWN.APPLICATION_PREPAID.SUBSCRIBENUMBER(+)
  AND EREGISOWN.APPLICATION_PREPAID.CUSTOMERID      = EREGISOWN.CUSTOMERATTACHMENT.CUSTOMERID(+)
  AND EREGISOWN.APPLICATION_PREPAID.activatestatus IN ('25')
  AND EREGISOWN.CUSTOMERATTACHMENT.ATTACHMENT      IS NOT NULL
  AND EREGISOWN.TEMP_SUB_NOT_FOUND.SUBSCRIBENUMBER IN ('66618533489',
'66823157640'
);
  --t_remain   := t_TotalSize;
  FOR indx IN 1 .. t_file_name.COUNT
  LOOP
    t_position := 1;
    t_chucklen  := 4096;
    t_remain := t_TotalSize(indx);
    --dbms_output.put_line(to_char(t_TotalSize(indx)) || '| t_position' || to_char(t_position));
    -- The directory TMP_IMG_FOLDER should exist before executing    
    t_output := UTL_FILE.fopen('TMP_IMG_AUDIT', t_file_name(indx), 'wb', 32760);
    --dbms_output.put_line(t_file_name(indx));
    -- Get BLOB
    SELECT EREGISOWN.CUSTOMERATTACHMENT.ATTACHMENT
    INTO t_blob
    FROM EREGISOWN.TEMP_SUB_NOT_FOUND,
    EREGISOWN.APPLICATION_PREPAID,
    EREGISOWN.CUSTOMERATTACHMENT
    WHERE 0                                 =0
    AND EREGISOWN.TEMP_SUB_NOT_FOUND.SUBSCRIBENUMBER  = EREGISOWN.APPLICATION_PREPAID.SUBSCRIBENUMBER(+)
    AND EREGISOWN.APPLICATION_PREPAID.CUSTOMERID      = EREGISOWN.CUSTOMERATTACHMENT.CUSTOMERID(+)
    AND EREGISOWN.APPLICATION_PREPAID.activatestatus IN ('25')
    AND EREGISOWN.CUSTOMERATTACHMENT.ATTACHMENT      IS NOT NULL
    and EREGISOWN.TEMP_SUB_NOT_FOUND.SUBSCRIBENUMBER IN (t_sub_num(indx));
    --dbms_output.put_line(to_char(t_TotalSize));
    -- Retrieving BLOB
    --dbms_output.put_line(to_char(t_TotalSize(indx)) || '|' || to_char(t_position));
    WHILE t_position < t_TotalSize(indx)
    LOOP
    DBMS_LOB.READ (t_blob, t_chucklen, t_position, t_chuck);
    UTL_FILE.put_raw (t_output, t_chuck);
    UTL_FILE.fflush (t_output);
    t_position   := t_position + t_chucklen;
    t_remain     := t_remain   - t_chucklen;
    IF t_remain   < 4096 THEN
    t_chucklen := t_remain;
    END IF;
    END LOOP;
    --dbms_output.put_line(to_char(t_TotalSize(indx)) || '|' || to_char(t_position)); 
    UTL_FILE.FCLOSE(t_output);
    dbms_output.put_line(t_file_name(indx));    
  END LOOP;
END;