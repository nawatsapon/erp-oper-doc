SELECT ou_code ,
  temp ,
  subinv_code ,
  subinv_desc ,
  subinv_address ,
  subinv_taxid ,
  doc_type ,
  doc_type_desc ,
  doc_date ,
  doc_no ,
  refer_iv_no ,
  upd_by ,
  customer_name ,
  total_amt ,
  vat_rate ,
  vat_amt ,
  net_amt ,
  tax_id ,
  branch_code
FROM
  (
  -- GOODs
  SELECT s.ou_code ,
    'x' temp ,
    s.subinv_code ,
    s.subinv_desc ,
    s.subinv_add
    || ' '
    || pb_amphur_pkg.get_amphur_name(s.amphur_code)
    || ' '
    || pb_province_pkg.get_name(s.province_code)
    || ' '
    || s.postal_code subinv_address ,
    s.subinv_taxid ,
    t.doc_type ,
    DECODE(t.doc_type, 1, 'Invoice', 3, 'Credit Note') doc_type_desc ,
    TRUNC(t.doc_date) doc_date ,
    DECODE(t.status, 'C', '*'
    || t.doc_no, t.doc_no) doc_no ,
    t.refer_iv_no ,
    t.upd_by ,
    t.cust_name customer_name ,
    NVL(DECODE(t.status, 'C', 0, t.total_amt), 0) * DECODE(t.doc_type, '1', 1, '3', -1) total_amt ,
    t.vat_rate                                    * DECODE(t.doc_type, '1', 1, '3', -1) vat_rate ,
    NVL(DECODE(t.status, 'C', 0, t.vat_amt), 0)   * DECODE(t.doc_type, '1', 1, '3', -1) vat_amt ,
    NVL(DECODE(t.status, 'C', 0, t.net_amt), 0)   * DECODE(t.doc_type, '1', 1, '3', -1) net_amt ,
    t.tax_id ,
    t.branch_code
  FROM pb_subinv s ,
    sa_transaction t
  WHERE t.ou_code = :ou_id
  AND t.subinv_code BETWEEN NVL(:p_sub_inven, '$') AND NVL(:e_sub_inven, chr(250))
  AND t.upd_by BETWEEN NVL(:s_user_log, '$') AND NVL(:e_user_log, chr(250))
  AND NVL(t.vat_code, NVL(:s_vat, '$')) BETWEEN NVL(:s_vat, '$') AND NVL(:e_vat, chr(250))
  AND t.doc_date BETWEEN to_date(NVL(:s_date, '01/01/0001'), 'DD/MM/RRRR') AND to_date(NVL(:e_date, '31/12/9990'), 'DD/MM/RRRR') + 0.99999
  AND t.doc_type LIKE NVL(:p_doc_type, '%')
  AND s.ou_code     = t.ou_code
  AND s.subinv_code = t.subinv_code
  AND EXISTS
    (SELECT 'X'
    FROM pb_user_subinven pb
    WHERE pb.ou_code = :ou_id
    AND pb.user_id BETWEEN NVL(:s_user_log, '$') AND NVL(:e_user_log, chr(250))
    AND pb.user_id     = :p_user
    AND pb.ou_code     = s.ou_code
    AND pb.subinv_code = s.subinv_code
    )
  AND :p_serive_goods IN ('G', '%')
    &temp_where
    
    -- SERVICE
  UNION ALL
  SELECT s.ou_code ,
    'x' temp ,
    s.subinv_code ,
    s.subinv_desc ,
    s.subinv_add
    || ' '
    || pb_amphur_pkg.get_amphur_name(s.amphur_code)
    || ' '
    || pb_province_pkg.get_name(s.province_code)
    || ' '
    || s.postal_code subinv_address ,
    s.subinv_taxid ,
    t.doc_type ,
    DECODE(t.doc_type, 1, 'Receipt', 3, 'Receipt Credit') doc_type_desc ,
    TRUNC(t.doc_date) doc_date ,
    t.doc_no ,
    t.refer_iv_no ,
    t.upd_by ,
    t.cust_name customer_name ,
    NVL(t.total_amt, 0) * DECODE(t.doc_type, '1', 1, '3', -1) total_amt ,
    t.vat_rate          * DECODE(t.doc_type, '1', 1, '3', -1) vat_rate ,
    NVL(t.vat_amt, 0)   * DECODE(t.doc_type, '1', 1, '3', -1) vat_amt ,
    NVL(t.net_amt, 0)   * DECODE(t.doc_type, '1', 1, '3', -1) net_amt ,
    t.tax_id ,
    t.branch_code
  FROM pb_subinv s ,
    sc_transaction t
  WHERE t.ou_code = :ou_id
  AND t.subinv_code BETWEEN NVL(:p_sub_inven, '$') AND NVL(:e_sub_inven, chr(250))
  AND t.upd_by BETWEEN NVL(:s_user_log, '$') AND NVL(:e_user_log, chr(250))
  AND NVL(t.vat_code, NVL(:s_vat, '$')) BETWEEN NVL(:s_vat, '$') AND NVL(:e_vat, chr(250))
  AND t.doc_date BETWEEN to_date(NVL(:s_date, '01/01/0001'), 'DD/MM/RRRR') AND to_date(NVL(:e_date, '31/12/9990'), 'DD/MM/RRRR') + 0.99999
  AND t.doc_type LIKE NVL(:p_doc_type, '%')
  AND s.ou_code     = t.ou_code
  AND s.subinv_code = t.subinv_code
  AND EXISTS
    (SELECT 'X'
    FROM pb_user_subinven pb
    WHERE pb.ou_code = :ou_id
    AND pb.user_id BETWEEN NVL(:s_user_log, '$') AND NVL(:e_user_log, chr(250))
    AND pb.user_id     = :p_user
    AND pb.ou_code     = s.ou_code
    AND pb.subinv_code = s.subinv_code
    )
  AND :p_serive_goods IN ('S', '%')
    &temp_where
  )
ORDER BY doc_type ,
  to_date(doc_date, 'DD/MM/RRRR') ,
  doc_no
