SELECT *
FROM
  (SELECT EMPLOYEE_NUMBER,
    LAST_NAME,
    FIRST_NAME,
    CENTRAL_EMP_JOB,
    JOB_ID,
    USER_LAN,
    JOB_NAME,
    ASSIGNMENT_ID,
    EFFECTIVE_START_DATE,
    EFFECTIVE_END_DATE,
    SQLSTATEMENT
  FROM
    (SELECT e.employee_number,
      e.last_name,
      e.first_name,
      e.job CENTRAL_EMP_JOB,
      j.job_id,
      e.user_lan ,
      b.job_name,
      b.ASSIGNMENT_ID,
      b.EFFECTIVE_START_DATE,
      b.EFFECTIVE_END_DATE,
      'update PER_ALL_ASSIGNMENTS_F set job_id='
      || j.job_id
      || ' where ASSIGNMENT_ID='
      || b.ASSIGNMENT_ID
      || ' and EFFECTIVE_START_DATE=to_date('''
      || TO_CHAR(EFFECTIVE_START_DATE,'dd/MM/yyyy')
      || ''',''dd/MM/yyyy'') '
      || ' and EFFECTIVE_END_DATE=to_date('''
      || TO_CHAR(EFFECTIVE_END_DATE,'dd/MM/yyyy')
      || ''',''dd/MM/yyyy''); ' AS SQLStatement
    FROM DTAC_EMP_INBOUND_INT e
    LEFT JOIN
      (SELECT a.last_name,
        a.first_name,
        a.full_name,
        a.employee_number,
        b.ASSIGNMENT_ID,
        b.EFFECTIVE_START_DATE,
        b.EFFECTIVE_END_DATE ,
        c.name job_name
      FROM PER_ALL_PEOPLE_F a
      INNER JOIN PER_ALL_ASSIGNMENTS_F b
      ON a.person_id              = b.person_id
      AND b.effective_start_date <= sysdate
      AND (b.effective_end_date  >= sysdate
      OR b.effective_end_date    IS NULL)
      INNER JOIN PER_JOBS c
      ON b.job_id              = c.job_id
      ) b ON e.employee_number = b.employee_number
    INNER JOIN PER_JOBS j
    ON e.job     = j.name
    WHERE e.job <> b.job_name
    )
  )
