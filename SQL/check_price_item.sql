SELECT distinct mtl.segment1 ,
  a.NAME,
  qppr.product_uom_code, -- qpll.list_price,
  qpll.operand item_price,
  QPLL.START_DATE_ACTIVE,
  QPLL.END_DATE_ACTIVE,
  qppr.product_attr_value
  ,mtl.ORGANIZATION_ID
  ,HRO.NAME
FROM qp_list_headers_all a,
  qp_list_lines qpll,
  qp_pricing_attributes qppr,
  INV.MTL_SYSTEM_ITEMS_B mtl,
  HR_ORGANIZATION_UNITS hro
  --WHERE a.list_header_id = 533750
WHERE a.list_header_id                  = qpll.list_header_id
AND qppr.product_attribute_context      = 'ITEM'
AND TO_NUMBER(qppr.list_line_id)       = qpll.list_line_id
AND NVL(qppr.product_attr_value,'0') = to_char(mtl.inventory_item_id)
ANd mtl.ORGANIZATION_ID = HRO.ORGANIZATION_ID
--ANd mtl.ORGANIZATION_ID = 108
  /*AND mtl.organization_id                 =
  (SELECT qp_util.get_item_validation_org FROM DUAL
  )*/
AND mtl.segment1              = 'PRCO0001511'
AND qpll.list_line_type_code IN ('PLL', 'PBH')
AND qppr.pricing_phase_id     = 1
AND qppr.qualification_ind   IN (4, 6, 20, 22)
AND qpll.pricing_phase_id     = 1
AND qpll.qualification_ind   IN (4, 6, 20, 22)
--AND Name                      = 'TRS_VIP'
order by 1,2,5
;