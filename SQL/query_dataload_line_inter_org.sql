SELECT distinct 
  mtb.INVENTORY_ITEM_CODE COPY1,
  'TAB' COPY2,
  msn.current_subinventory_code COPY3,
  'TAB' COPY4,
  MIL.segment1 COPY5,
  'TAB' COPY6,
  'N3G02B' COPY7,
  'TAB' COPY8,
  'N3G02B03' COPY9,
  'TAB' COPY10,
  'TAB' COPY11,
  count(tmps.serial) COPY12,
  '*DN' COPY13
FROM XX_TMP_SERIAL tmps,
  mtl_serial_numbers msn,
  mtl_material_transactions mmt,
  MTL_TRX_TYPES_VIEW mtt,
  (SELECT DISTINCT INVENTORY_ITEM_ID,
    segment1 INVENTORY_ITEM_CODE,
    description,
    ORGANIZATION_ID
  FROM Mtl_System_Items_B
  ) mtb,
  (SELECT lookup_code,
    meaning
  FROM mfg_lookups
  WHERE lookup_type = 'SERIAL_NUM_STATUS'
  ) slp,
  MTL_SECONDARY_INVENTORIES SUB,
  (select distinct ORGANIZATION_ID,SUBINVENTORY_CODE,INVENTORY_LOCATION_ID,segment1 from MTL_ITEM_LOCATIONS) MIL  
WHERE 0=0
--and SUBSTR(msn.serial_number, 5, 16) = tmps.serial
and msn.serial_number = tmps.serial
AND msn.last_transaction_id        = mmt.transaction_id
AND mmt.Transaction_Type_Id        = mtt.TRANSACTION_TYPE_ID(+)
AND mmt.TRANSACTION_SOURCE_TYPE_ID = mtt.TRANSACTION_SOURCE_TYPE_ID(+)
AND mmt.Transaction_Action_Id      = mtt.TRANSACTION_ACTION_ID(+)
AND mmt.SUBINVENTORY_CODE = SUB.SECONDARY_INVENTORY_NAME
AND mmt.ORGANIZATION_ID = SUB.ORGANIZATION_ID
AND mmt.Inventory_Item_Id          = mtb.INVENTORY_ITEM_ID 
and mmt.ORGANIZATION_ID = mtb.ORGANIZATION_ID
AND msn.status_id = slp.lookup_code
and msn.CURRENT_LOCATOR_ID =	MIL.INVENTORY_LOCATION_ID
and msn.CURRENT_ORGANIZATION_ID =	MIL.ORGANIZATION_ID
and msn.CURRENT_SUBINVENTORY_CODE = MIL.SUBINVENTORY_CODE
and mmt.Transaction_Type_Id not in (118)
and msn.CURRENT_SUBINVENTORY_CODE <> 'N3G02B'
group by mtb.INVENTORY_ITEM_CODE,msn.current_subinventory_code, MIL.segment1
order by mtb.INVENTORY_ITEM_CODE,msn.current_subinventory_code
;