SELECT PERSON_ID,
  supervisor_id ,
  Full_name,
  email_address,
  RPAD('.', (level-1)*2, '.')
  || PERSON_ID AS tree,
  level tree_level,
  CONNECT_BY_ROOT PERSON_ID                       AS root_id,
  LTRIM(SYS_CONNECT_BY_PATH(Full_name, '|'), '|') AS menu_path
  --,CONNECT_BY_ISLEAF                                    AS leaf
FROM
  (SELECT PER_ASSIGNMENTS_F.supervisor_id ,
    PER_PEOPLE_F.*
  FROM PER_PEOPLE_F,
    (SELECT ROW_NUMBER() OVER (PARTITION BY PER_ASSIGNMENTS_F.PERSON_ID ORDER BY PER_ASSIGNMENTS_F.EFFECTIVE_START_DATE ASC) AS Row_nun,
      PER_ASSIGNMENTS_F.*
    FROM PER_ASSIGNMENTS_F
    ) PER_ASSIGNMENTS_F
  WHERE 0                    =0
  AND PER_PEOPLE_F.PERSON_ID = PER_ASSIGNMENTS_F.PERSON_ID
    /*AND LAST_NAME
    || ' '
    || FIRST_NAME IN ('TANTIDA YODHASMUTR')*/
  AND Row_nun = 1
  ) tab
  START WITH LAST_NAME
  || ' '
  || FIRST_NAME IN ('TANTIDA YODHASMUTR') -- MENU "AR_CUSTOMERS_GUI" ID = 67722 , MENU "AP_SUPPLIERS_GUI12" = 68015
  --START WITH id = 67721
  --START WITH id is not null
  CONNECT BY PERSON_ID = PRIOR supervisor_id
ORDER SIBLINGS BY PERSON_ID ;