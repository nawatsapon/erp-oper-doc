SELECT  DISTINCT ou_code,
  trans_code,
  trans_desc,
  oa_acc_segment1 "ACCOUNT",
  oa_acc_segment2 "PRODUCT",
  eff_from,
  eff_to
  --,SC_TRANS_TYPE.*
FROM SC_TRANS_TYPE
WHERE 0                  =0
--AND (TRUNC(eff_to)       > sysdate
--OR SC_TRANS_TYPE.eff_to IS NULL)
and apple_care_status <> 'Y'
order by 1,trans_code