SELECT WFR.TEXT_VALUE,recipient_role,subject,win.*,wfn.*
FROM (SELECT wi.msgid,
    wi.corrid,
    wi.enq_time,
    sysdate,
    wi.state,
    wi.sender_name,
    (SELECT str_value
    FROM TABLE (wi.user_data.header.properties)
    WHERE NAME = 'NOTIFICATION_ID'
    ) notification_id
  FROM apps.wf_notification_in wi
  -- and wi.state = 0; 0= Not yet done; 2=Done
  order by enq_time desc
  ) win,
  wf_notifications wfn,
  (SELECT WF_NOTIFICATION_ATTRIBUTES.NOTIFICATION_ID,
    WF_NOTIFICATION_ATTRIBUTES.TEXT_VALUE
  FROM WF_NOTIFICATION_ATTRIBUTES
  WHERE 0  =0
  AND NAME = 'RESULT'
  ) wfr
WHERE win.notification_id = wfn.notification_id
and win.notification_id = wfr.notification_id
--AND Message_type in ('REQAPPRV','APEXP')
--AND Message_type in ('APEXP')
--AND subject like '%EMP-340780%'
--and recipient_role = 'T115287'
AND wfn.status = 'OPEN'
--AND WFR.TEXT_VALUE is not null
order by wfn.TO_USER,enq_time desc;

 