SELECT gb.* FROM gl_import_references gb WHERE GB.reference_1 = '1995635';
SELECT gh.period_name,
  GB.NAME,
  mta.transaction_id,
  mmt.organization_id,
  msi.segment1,
  mta.transaction_date,
  mta.primary_quantity,
  gcc.segment1
  || '.'
  || gcc.segment2
  || '.'
  || gcc.segment3
  || '.'
  || gcc.segment4
  || '.'
  || gcc.segment5 account,
  DECODE(SIGN(mta.transaction_value), 1, mta.transaction_value, 0, 0, NULL, DECODE(SIGN(mta.base_transaction_value), 1, mta.base_transaction_value, NULL)) Entered_Dr,
  DECODE(SIGN(mta.transaction_value), -1, (-1 * mta.transaction_value), 0, 0, NULL, DECODE(SIGN(mta.base_transaction_value), -1, (-1 * mta.base_transaction_value))) Entered_Cr,
  DECODE(SIGN(mta.base_transaction_value), 1, mta.base_transaction_value, 0, 0, NULL) Accounted_Dr,
  DECODE(SIGN(mta.base_transaction_value), -1, (-1 * mta.base_transaction_value), 0, 0, NULL) Accounted_Cr,
  gh.currency_code,
  mtt.transaction_type_name,
  DECODE(mta.gl_batch_id, -1, 'N', 'Y') "Transfered_Flag",
  mta.gl_batch_id,
  gh.je_header_id
FROM inv.mtl_material_transactions mmt,
  inv.mtl_transaction_types mtt,
  inv.mtl_system_items_b msi,
  inv.mtl_transaction_accounts mta,
  gl.gl_code_combinations gcc,
  gl.gl_je_batches gb,
  gl.gl_je_headers gh,
  gl.gl_je_lines gl,
  gl.gl_import_references gr
WHERE mmt.organization_id   = msi.organization_id
AND msi.inventory_item_id   = mmt.inventory_item_id
AND mmt.transaction_id      = mta.transaction_id
AND gcc.code_combination_id = mta.reference_account
AND mtt.transaction_type_id = mmt.transaction_type_id
AND gb.je_batch_id          = gh.je_batch_id
AND gh.je_header_id         = gl.je_header_id
AND gl.code_combination_id  = mta.reference_account
AND mta.gl_batch_id         = to_number(SUBSTR(gb.name, 1, instr(gb.name, ' ') - 1))
AND gh.je_Category          = 'MTL'
AND gh.je_source            = 'Inventory'
  --and gh.name = 'XXX' ---REPLACE XXX WITH NAME
AND gl.je_line_num  = gr.je_line_num
AND gr.je_header_id = gl.je_header_id
AND gr.je_line_num  = gl.je_line_num
AND mta.gl_batch_id = gr.reference_1
AND gh.period_name  = :period_name -- ENTER THE PERIOD FORMAT(MON-YY) | JUL-18
and mmt.organization_id = 439
  --AND mmt.TRANSACTION_ID = 368695994
  --AND upper(gb.name) LIKE upper('%&gl_batch_name%')
ORDER BY mta.transaction_date;