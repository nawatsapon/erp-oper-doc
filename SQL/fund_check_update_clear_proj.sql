SELECT project_id ,
  task_id ,
  expenditure_type ,
  expenditure_item_date ,
  expenditure_organization_id ,
  ap_invoice_distributions_all.*
FROM ap_invoice_distributions_all
WHERE invoice_id IN
  (SELECT invoice_id
  FROM apps.ap_invoices_all
  WHERE doc_sequence_value IN ('2016045676')
  AND org_id                = 142
  AND invoice_type_lookup_code LIKE 'STANDARD'
  );
  
------------------------------------------------------------------------------



--update system code, project 5 field out--
UPDATE apps.ap_invoice_distributions_all
  --set dist_code_combination_id = 85484
SET project_id                = NULL,
  task_id                     = NULL,
  expenditure_type            = NULL,
  expenditure_item_date       = NULL,
  expenditure_organization_id = NULL
  --select * from apps.ap_invoice_distributions_all
WHERE invoice_id IN
  (SELECT invoice_id
  FROM apps.ap_invoices_all
  WHERE doc_sequence_value IN ('2016045676')
  AND org_id                = 142
  AND invoice_type_lookup_code LIKE 'STANDARD'
  )
AND distribution_line_number = 1;

