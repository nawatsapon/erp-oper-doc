create or replace PACKAGE BODY TAC_SUPPLIER_INTERFACE_UTIL is

  ----------------------------------------------
  -- function : get_map_supplier_stie_code
  -- purpose : map sullplie site code from HOxxx to NOxxx for DTAC-N and to POxxx for PAYSABUY
  -- Author : AP@BAS
  -- Create : 05-Feb-2013
  ----------------------------------------------
  function get_map_supplier_stie_code(p_ho_supplier_stie_code varchar2,
                                      p_map_to_ou             varchar2)
    return varchar2 is
    v_supplier_site_code_mapped varchar2(50);
  begin
    if p_map_to_ou = 'DTN' then
      v_supplier_site_code_mapped := 'N' || to_char(to_number(substr(p_ho_supplier_stie_code,
                                                                     3,
                                                                     999)),
                                                    'fm9900');
    elsif p_map_to_ou = 'PSB' then
      v_supplier_site_code_mapped := 'P' || to_char(to_number(substr(p_ho_supplier_stie_code,
                                                                     3,
                                                                     999)),
                                                    'fm9900');
    else
      v_supplier_site_code_mapped := null;
    end if;
  
    return v_supplier_site_code_mapped;
  exception
    when others then
      return null;
  end get_map_supplier_stie_code;

  /*
  Date : 4-Nov-2011
  Author : Aphichart P.
  Comments
      - Add process to validate vat_register in validate_supplier_summary function
      - Add if-else to classified error.
      - Modify script that update table dtac_ap_suppliers_temp_int by update error_message data.
  */

  ---------------------------------------------------------------------------------
  procedure write_log(param_msg varchar2) is
  begin
    fnd_file.put_line(fnd_file.log, param_msg);
  end write_log;

  ----------------------------------------------
  -- function : get_map_accts_pay_ccid
  -- purpose : map accts_pay_ccid from HO to DTN and PSB
  -- Map value : HO = 01, DTN = 08, PSB = 12
  -- Author : AP@BAS
  -- Create : 05-Feb-2013
  ----------------------------------------------
  function get_map_accts_pay_ccid(p_ho_accts_pay_ccid number,
                                  p_map_to_ou         varchar2) return number is
    v_map_accts_pay_ccid_code varchar2(100);
    v_map_accts_pay_ccid_id   number;
  begin
    select decode(p_map_to_ou,
                  'DTN',
                  '08' || substr(concatenated_segments, 3, 999),
                  'PSB',
                  '12' || substr(concatenated_segments, 3, 999),
                  concatenated_segments) map_concatenated_segments
      into v_map_accts_pay_ccid_code
      from gl_code_combinations_kfv
     where code_combination_id = p_ho_accts_pay_ccid;
  
    -- replace rc code to '0000'
    v_map_accts_pay_ccid_code := substr(v_map_accts_pay_ccid_code,
                                        1,
                                        instr(v_map_accts_pay_ccid_code,
                                              '.',
                                              1,
                                              2)) || g_mapp_rccode_segment ||
                                 substr(v_map_accts_pay_ccid_code,
                                        instr(v_map_accts_pay_ccid_code,
                                              '.',
                                              1,
                                              3),
                                        999);
  
    select code_combination_id
      into v_map_accts_pay_ccid_id
      from gl_code_combinations_kfv
     where concatenated_segments = v_map_accts_pay_ccid_code;
  
    return v_map_accts_pay_ccid_id;
  exception
    when no_data_found then
      write_log('tac_supplier_interface_util.get_map_accts_pay_ccid no_data_found for ' ||
                p_map_to_ou || ' [' || v_map_accts_pay_ccid_code || ']');
      return null;
    when others then
      write_log('tac_supplier_interface_util.get_map_accts_pay_ccid error.');
      return null;
  end get_map_accts_pay_ccid;

  ----------------------------------------------
  -- function : get_map_prepay_ccid
  -- purpose : map prepay_ccid from HO to DTN and PSB
  -- Map value : HO = 01, DTN = 08, PSB = 12
  -- Author : AP@BAS
  -- Create : 05-Feb-2013
  ----------------------------------------------
  function get_map_prepay_ccid(p_ho_prepay_ccid number,
                               p_map_to_ou      varchar2) return number is
    v_map_prepay_ccid_code varchar2(100);
    v_map_prepay_ccid_id   number;
  begin
    select decode(p_map_to_ou,
                  'DTN',
                  '08' || substr(concatenated_segments, 3, 999),
                  'PSB',
                  '12' || substr(concatenated_segments, 3, 999),
                  concatenated_segments) map_concatenated_segments
      into v_map_prepay_ccid_code
      from gl_code_combinations_kfv
     where code_combination_id = p_ho_prepay_ccid;
  
    -- replace rc code to '0000'
    v_map_prepay_ccid_code := substr(v_map_prepay_ccid_code,
                                     1,
                                     instr(v_map_prepay_ccid_code, '.', 1, 2)) ||
                              g_mapp_rccode_segment ||
                              substr(v_map_prepay_ccid_code,
                                     instr(v_map_prepay_ccid_code, '.', 1, 3),
                                     999);
  
    select code_combination_id
      into v_map_prepay_ccid_id
      from gl_code_combinations_kfv
     where concatenated_segments = v_map_prepay_ccid_code;
  
    return v_map_prepay_ccid_id;
  exception
    when no_data_found then
      write_log('tac_supplier_interface_util.get_map_prepay_ccid no_data_found for ' ||
                p_map_to_ou || ' [' || v_map_prepay_ccid_code || ']');
      return null;
    when others then
      write_log('tac_supplier_interface_util.get_map_prepay_ccid error.');
      return null;
  end get_map_prepay_ccid;
  ---------------------------------------------------------------------------------------
  function gen_check_digit(p_acctbasc in varchar2) return number as
    v_temp_num number := 0;
  begin
    begin
      -- if it's not numeric, this will raise an exception
      v_temp_num := to_number(p_acctbasc, '9999999999');
      return 1;
    exception
      when others then
        return 0;
    end;
  end gen_check_digit;
  ---------------------------------------------------------------------------------------
  procedure validate_supplier_summary(i_def_grp in varchar2,
                                      i_req_id  in number,
                                      i_org_id  in number) is
    pragma autonomous_transaction;
  
    i                  number;
    v_supplier_type    varchar2(80);
    v_payment_term     varchar2(50);
    v_pay_group        varchar2(25);
    v_country          varchar2(25);
    v_msg              varchar2(4000);
    temp_var           varchar2(25);
    strerr             varchar2(250);
    v_bank_acc         number;
    v_dup              varchar2(500);
    v_name             varchar2(50);
    v_name_bk          varchar2(50);
    v_name_num         varchar2(50);
    v_name_bk_num      varchar2(50);
    v_name_site        varchar2(50);
    v_name_site_num    varchar2(50);
    v_name2            varchar2(50);
    v_bk_num           varchar2(500);
    v_bknum            varchar2(500);
    v_status           varchar2(25);
    v_status2          varchar2(25);
    v_nameerp          varchar2(50);
    v_nameerp_site     varchar2(50);
    v_nameerp_num      varchar2(50);
    v_nameerp_site_num varchar2(50);
    v_statuserp        varchar2(25);
    v_dup_num_erp      varchar2(500);
    x_name             varchar2(200);
    v_sub_name         varchar2(2000);
    v_chk_dup          number;
    v_flag_dup         varchar2(25);
    v_flag_dup_erp     varchar2(25);
  
  begin
    --FOR rec_supplier IN    c_get_supplier LOOP
    begin
      update dtac_ap_suppliers_temp_int
         set request_id = i_req_id
       where status = 'NEW'
         and default_group = i_def_grp;
    end;
    ----------------------------------------------------------------------------
    --Check Bank Account in Temp Table and ERP Table
    ----------------------------------------------------------------------------
    declare
      b_vendor_number    dtac_ap_suppliers_temp_int.vendor_number%type;
      b_vendor_name      dtac_ap_suppliers_temp_int.vendor_name%type;
      b_attribute4       dtac_ap_suppliers_temp_int.attribute4%type;
      b_vendor_site_code dtac_ap_suppliers_temp_int.vendor_site_code%type;
      b_site_attribute4  dtac_ap_suppliers_temp_int.site_attribute4%type;
      s_vendor_number    dtac_ap_suppliers_temp_int.vendor_number%type;
      s_vendor_name      dtac_ap_suppliers_temp_int.vendor_name%type;
      s_attribute4       dtac_ap_suppliers_temp_int.attribute4%type;
      s_vendor_site_code dtac_ap_suppliers_temp_int.vendor_site_code%type;
      s_site_attribute4  dtac_ap_suppliers_temp_int.site_attribute4%type;
      t_vendor_number    dtac_ap_suppliers_temp_int.vendor_number%type;
      t_vendor_name      dtac_ap_suppliers_temp_int.vendor_name%type;
      t_attribute4       dtac_ap_suppliers_temp_int.attribute4%type;
      t_vendor_site_code dtac_ap_suppliers_temp_int.vendor_site_code%type;
      t_site_attribute4  dtac_ap_suppliers_temp_int.site_attribute4%type;
    
      cursor c_get_temp is
        select dasti.vendor_number,
               dasti.vendor_name,
               dasti.attribute4,
               dasti.vendor_site_code,
               dasti.site_attribute4
          from dtac_ap_suppliers_temp_int dasti
         where dasti.status = 'NEW'
           and dasti.default_group = i_def_grp
           and dasti.request_id = i_req_id
         order by dasti.vendor_name;
      cursor c_get_bank(i_vendor_name in varchar2, i_bank_account in varchar2) is
        select dasti.vendor_number,
               dasti.vendor_name,
               dasti.attribute4,
               dasti.vendor_site_code,
               dasti.site_attribute4
          from dtac_ap_suppliers_temp_int dasti
         where dasti.status = 'NEW'
           and dasti.default_group = i_def_grp
           and dasti.request_id = i_req_id
           and dasti.vendor_name <> i_vendor_name
           and dasti.attribute4 = i_bank_account;
      cursor c_get_bank_site(i_vendor_name in varchar2, i_bank_account in varchar2) is
        select dasti.vendor_number,
               dasti.vendor_name,
               dasti.attribute4,
               dasti.vendor_site_code,
               dasti.site_attribute4
          from dtac_ap_suppliers_temp_int dasti
         where dasti.status = 'NEW'
           and dasti.default_group = i_def_grp
           and dasti.request_id = i_req_id
           and dasti.vendor_name <> i_vendor_name
           and dasti.site_attribute4 = i_bank_account;
      cursor c_get_bank_erp(i_vendor_name in varchar2, i_bank_account in varchar2) is
        select pov.segment1, pov.vendor_name, pov.attribute4
          from po_vendors pov
         where pov.vendor_name <> i_vendor_name
           and pov.attribute4 = i_bank_account
           and pov.end_date_active is null;
      cursor c_get_bank_site_erp(i_vendor_name in varchar2, i_bank_account in varchar2) is
        select pov.segment1,
               pov.vendor_name,
               pvs.vendor_site_code,
               pvs.attribute4
          from po_vendors pov, po_vendor_sites_all pvs
         where pov.vendor_id = pvs.vendor_id
           and pov.vendor_name <> i_vendor_name
           and pvs.attribute4 = i_bank_account
           and pov.end_date_active is null;
    begin
      -----------------------------------------------------------------------
      --Check Bank Account in Temp Table
      -----------------------------------------------------------------------
      v_flag_dup := 'No';
      open c_get_temp;
      loop
        fetch c_get_temp
          into t_vendor_number, t_vendor_name, t_attribute4, t_vendor_site_code, t_site_attribute4;
        exit when c_get_temp%notfound;
      
        open c_get_bank(t_vendor_name, t_attribute4);
        loop
          fetch c_get_bank
            into b_vendor_number, b_vendor_name, b_attribute4, b_vendor_site_code, b_site_attribute4;
          exit when c_get_bank%notfound;
          fnd_file.put_line(fnd_file.output,
                            'Warning: Duplicated Bank Account Number in Temp Table between: (' ||
                            t_vendor_number || ')' || t_vendor_name ||
                            ' and (' || b_vendor_number || ')' ||
                            b_vendor_name);
          v_flag_dup := 'Yes';
        end loop;
        close c_get_bank;
      
      end loop;
      close c_get_temp;
    
      if v_flag_dup = 'No' then
        open c_get_temp;
        loop
          fetch c_get_temp
            into t_vendor_number, t_vendor_name, t_attribute4, t_vendor_site_code, t_site_attribute4;
          exit when c_get_temp%notfound;
        
          open c_get_bank_site(t_vendor_name, t_site_attribute4);
          loop
            fetch c_get_bank_site
              into s_vendor_number, s_vendor_name, s_attribute4, s_vendor_site_code, s_site_attribute4;
            exit when c_get_bank_site%notfound;
            fnd_file.put_line(fnd_file.output,
                              'Warning: Duplicated Bank Account Number Site in Temp Table between:(' ||
                              t_vendor_number || ')' || t_vendor_name ||
                              ' and (' || s_vendor_number || ')' ||
                              s_vendor_name);
          end loop;
          close c_get_bank_site;
        
        end loop;
        close c_get_temp;
      end if;
      -----------------------------------------------------------------------
      --Check Bank Account in ERP Table
      -----------------------------------------------------------------------
      v_flag_dup_erp := 'No';
      open c_get_temp;
      loop
        fetch c_get_temp
          into t_vendor_number, t_vendor_name, t_attribute4, t_vendor_site_code, t_site_attribute4;
        exit when c_get_temp%notfound;
      
        open c_get_bank_erp(t_vendor_name, t_attribute4);
        loop
          fetch c_get_bank_erp
            into b_vendor_number, b_vendor_name, b_attribute4;
          exit when c_get_bank_erp%notfound;
          fnd_file.put_line(fnd_file.output,
                            'Warning: Duplicated Bank Account Number with ERP between: (' ||
                            t_vendor_number || ')' || t_vendor_name ||
                            ' and (' || b_vendor_number || ')' ||
                            b_vendor_name);
        end loop;
        close c_get_bank_erp;
      
        open c_get_bank_site_erp(t_vendor_name, t_site_attribute4);
        loop
          fetch c_get_bank_site_erp
            into s_vendor_number, s_vendor_name, s_vendor_site_code, s_site_attribute4;
          exit when c_get_bank_site_erp%notfound;
          fnd_file.put_line(fnd_file.output,
                            'Warning: Duplicated Bank Account Number Site ' ||
                            s_vendor_site_code || ' with ERP between: (' ||
                            t_vendor_number || ')' || t_vendor_name ||
                            ' and (' || s_vendor_number || ')' ||
                            s_vendor_name);
        end loop;
        close c_get_bank_site_erp;
      
      end loop;
      close c_get_temp;
    
      -----------------------------------------------------------------------
      --Check Bank Account Over 10 digit in Temp Table
      -----------------------------------------------------------------------
      open c_get_temp;
      loop
        fetch c_get_temp
          into t_vendor_number, t_vendor_name, t_attribute4, t_vendor_site_code, t_site_attribute4;
        exit when c_get_temp%notfound;
        if (gen_check_digit(t_attribute4) = 0) or
           (gen_check_digit(t_site_attribute4) = 0) then
          fnd_file.put_line(fnd_file.output,
                            'Warning: Bank Account Number Not Equal Ten Digit: (' ||
                            t_vendor_number || ')' || t_vendor_name);
        end if;
      end loop;
      close c_get_temp;
    
    end;
    ------------------------------------------------------------------------------
  
    -- Only Vat Tax Registration Duplicate for new Supplier
    begin
      update dtac_ap_suppliers_temp_int dat
         set dat.status = 'VAT DUP ERP'
       where exists
       (select 'X'
                from po_vendors pv
               where pv.vat_registration_num = dat.vat_registration_num)
         and dat.request_id = i_req_id
         and dat.vendor_number is null; -- New Supplier ;
    exception
      when others then
        temp_var := 'Temp';
    end;
  
    ----------------------------------
    -- Only Vat Tax Registration Duplicate for new Supplier with Supplier name
    begin
      update dtac_ap_suppliers_temp_int dat
         set dat.status = 'VAT DUP ERP'
       where exists
       (select 'X'
                from po_vendors pv
               where pv.vat_registration_num = dat.vat_registration_num
                 and pv.vendor_name = dat.vendor_name)
         and dat.request_id = i_req_id
         and dat.vendor_number is null; -- New Supplier ;
    exception
      when others then
        temp_var := 'Temp';
    end;
  
    -- #100 Added by AP@IS-ES on 4-Nov-2011
    -- Validate tax must numeric
    declare
      v_tax_valid_flag number;
    begin
      -- check data must number only
      update dtac_ap_suppliers_temp_int dst
         set dst.status = 'ERROR VAT NUMERIC'
       where 0 = decode(replace(translate(dst.vat_registration_num,
                                          '0123456789',
                                          '0000000000'),
                                '0',
                                ''),
                        null,
                        1,
                        0)
         and dst.status = 'NEW'
         and dst.request_id = i_req_id;
    
      -- check data length must 10 or 13 digits
      update dtac_ap_suppliers_temp_int dst
         set dst.status = 'ERROR VAT LENGTH'
       where length(dst.vat_registration_num) not in (10, 13)
         and dst.status = 'NEW'
         and dst.vat_registration_num is not null
         and dst.request_id = i_req_id;
    
      -- check rule if length is 13 digits
      update dtac_ap_suppliers_temp_int dst
         set dst.status = 'ERROR VAT PATTERN'
       where length(dst.vat_registration_num) = 13
         and dst.status = 'NEW'
         and tac_validate_data.validate_supplier_tax(dst.vat_registration_num) = 0
         and dst.request_id = i_req_id;
    
    exception
      when others then
        null;
    end;
    -- #100 End of added
  
    -----------------------------------
    -- Validation Data Dup 1 Tax multi Name
    begin
      update dtac_ap_suppliers_temp_int d1
         set d1.status = 'DUP'
       where d1.request_id = i_req_id
         and (d1.request_id, d1.vat_registration_num, d1.vendor_name,
              d1.vendor_site_code) in
             (select dat.request_id,
                     dat.vat_registration_num,
                     dat.vendor_name,
                     dat.vendor_site_code
                from dtac_ap_suppliers_temp_int dat
               where dat.request_id = d1.request_id
                 and dat.status = 'NEW'
                 and dat.vendor_number is null -- for new Vendor
                 and dat.vendor_name = d1.vendor_name
                 and dat.vat_registration_num = d1.vat_registration_num
                 and dat.vendor_site_code = d1.vendor_site_code
               group by dat.request_id,
                        dat.vat_registration_num,
                        dat.vendor_name,
                        dat.vendor_site_code
              having count('x') > 1);
    exception
      when others then
        temp_var := 'Temp';
    end;
    ----------------------------------
    ---BANK difference in TEMP Table   COMMENT BY TONG 10/10/2007
    begin
      update dtac_ap_suppliers_temp_int d1
         set d1.status = 'BANK DIF'
       where d1.request_id = i_req_id
         and d1.status = 'NEW'
         and d1.vendor_number is null
         and (d1.request_id, d1.vendor_name, d1.vendor_site_code) in
             (select dat.request_id, dat.vendor_name, dat.vendor_site_code
                from dtac_ap_suppliers_temp_int dat
               where dat.request_id = d1.request_id
                 and dat.status = 'NEW'
                 and dat.vendor_number is null -- for new Vendor
                 and dat.vendor_name = d1.vendor_name
                 and dat.vendor_site_code = d1.vendor_site_code
               group by dat.request_id,
                        dat.vendor_name,
                        dat.vendor_site_code
              having count(distinct dat.site_attribute4) > 1);
    exception
      when others then
        temp_var := 'Temp';
    end;
    ----------------------------------
    ---1 NAME mul tax in TEMP Table   COMMENT BY TONG 10/10/2007
    begin
      update dtac_ap_suppliers_temp_int d1
         set d1.status = 'VAT DIF'
       where d1.request_id = i_req_id
         and d1.status = 'NEW'
         and d1.vendor_number is null
         and (d1.vendor_name, d1.vendor_site_code) in
             (select dat.vendor_name, dat.vendor_site_code
                from dtac_ap_suppliers_temp_int dat
               where dat.request_id = d1.request_id
                 and dat.status = 'NEW'
                 and dat.vendor_number is null -- for new Vendor
                 and dat.vendor_name = d1.vendor_name
                 and dat.vendor_site_code = d1.vendor_site_code
               group by dat.vendor_name, dat.vendor_site_code
              having count(distinct nvl(dat.vat_registration_num, '999')) > 1
              union all
              select vendor_name, vendor_site_code
                from po_vendor_sites_all, po_vendors
               where po_vendors.vendor_id = po_vendor_sites_all.vendor_id
                 and po_vendors.vendor_name = d1.vendor_name
                 and vendor_site_code = d1.vendor_site_code);
    exception
      when others then
        temp_var := 'Temp';
    end;
    -----------------------------------------------------------------------
    begin
      -- Validation Duplidate VAT TAX Registration Number
      update dtac_ap_suppliers_temp_int d1
         set status = 'VAT DUP'
       where status = 'NEW'
         and d1.vendor_number is null
         and d1.request_id = i_req_id
         and d1.vat_registration_num in
             (select dat.vat_registration_num
                from dtac_ap_suppliers_temp_int dat
               where dat.request_id = d1.request_id
                 and dat.status = 'NEW'
                 and dat.vendor_number is null
                 and dat.vat_registration_num = d1.vat_registration_num
               group by dat.vat_registration_num
              having count(distinct dat.vendor_name) > 1
              union all
              select vat_registration_num
                from po_vendors
               where vat_registration_num = d1.vat_registration_num);
    exception
      when others then
        temp_var := 'Temp';
    end;
  
    --------------------------------------------------------
    begin
      update dtac_ap_suppliers_temp_int d1
         set d1.status = 'NAME DUP'
       where d1.request_id = i_req_id
         and d1.status = 'NEW'
         and d1.vendor_number is null
         and d1.vendor_name in (select vendor_name from po_vendors);
    exception
      when others then
        temp_var := 'Temp';
    end;
    ----------------------------------   Assign Data to Array
    begin
      open c_sup_int(i_req_id, i_def_grp);
      fetch c_sup_int bulk collect
        into g_sup_int;
      close c_sup_int;
    end;
    --------------------------------
  
    v_dup         := 'ERROR: Duplicated Bank Account Number in Temp Table between ';
    v_bk_num      := 'ERROR: Bank Account Number Not Equal Ten Digit: ';
    v_dup_num_erp := 'ERROR: Duplicated Bank Account Number with ERP between ';
    v_chk_dup     := 0;
    v_name        := ' ';
    if g_sup_int.exists(1) then
      for i in 1 .. g_sup_int.count loop
        --Codbms_output.put_line(' ');mment by pta 16/11/06 g_sup_int.FIRST..g_sup_int.LAST LOOP
        if g_sup_int(i).vendor_site_id is not null then
          g_sup_int(i).vendor_site_sts := 'OLD';
          g_sup_int(i).vendor_cont_sts := 'OLD';
        end if;
      
        --/*Add Chay 1/3/2009
        --erp_vendor_name  Add New Supplier ,Update Suppplier
        begin
          select pov.vendor_name
            into g_sup_int(i).erp_vendor_name
            from po_vendors pov
           where pov.attribute4 =
                 nvl(g_sup_int(i).site_attribute4, g_sup_int(i).attribute4);
        exception
          when others then
            temp_var := 'Temp';
        end;
      
        ------ !!@!'VAT DUP ERP'
        if g_sup_int(i).vendor_id is null and g_sup_int(i).status != 'NEW' then
          --IF g_sup_int(i).vendor_number IS NULL AND g_sup_int(i).status != 'NEW'  THEN -- NAJ
          if g_sup_int(i).status = 'VAT DUP ERP' then
            g_sup_int(i).err_msg := 'Error : TAX Registration Number Duplicate in ERP Table of Source Supplier Name : ' ||
                                    g_sup_int(i).vendor_name;
            --            g_sup_int(i).Status  := 'REJECTED' ;
          elsif g_sup_int(i).status = 'NAME DUP' then
            g_sup_int(i).err_msg := 'Error : Supplier Name already exists in supplier Master of Source Supplier Name : ' ||
                                    g_sup_int(i).vendor_name;
            --            g_sup_int(i).status  := 'REJECTED' ;
            --COMMENT BY TONG 10/10/2007
          elsif g_sup_int(i).status = 'BANK DIF' then
            g_sup_int(i).err_msg := 'Error : Bank Account Error of Source Supplier Name : ' ||
                                    g_sup_int(i).vendor_name;
            --            g_sup_int(i).status  := 'REJECTED' ;
          elsif g_sup_int(i).status = 'VAT DIF' then
            g_sup_int(i).err_msg := 'Error : 1 Supplier site multi tax of Source Supplier Name : ' ||
                                    g_sup_int(i).vendor_name;
            --            g_sup_int(i).status  := 'REJECTED' ;
          elsif g_sup_int(i).status = 'DUP' then
            g_sup_int(i).err_msg := 'Data duplicate in Source Table';
            --            g_sup_int(i).Status  := 'REJECTED' ;
            -- #300 Added by AP@IS-ES on 4/Nov-2011
          elsif g_sup_int(i).status in ('ERROR VAT NUMERIC',
                           'ERROR VAT LENGTH',
                           'ERROR VAT PATTERN') then
            --            g_sup_int(i).Status  := 'REJECTED';
            if g_sup_int(i).status = ('ERROR VAT NUMERIC') then
              g_sup_int(i).err_msg := 'Error : Supplier Tax id should be numeric : ' ||
                                      g_sup_int(i).vendor_name;
            elsif g_sup_int(i).status = ('ERROR VAT LENGTH') then
              g_sup_int(i).err_msg := 'Error : Supplier Tax id should be 10 or 13 digits : ' ||
                                      g_sup_int(i).vendor_name;
            elsif g_sup_int(i).status = ('ERROR VAT PATTERN') then
              g_sup_int(i).err_msg := 'Error : Supplier Tax id does not comply Central Civil-Registration : ' ||
                                      g_sup_int(i).vendor_name;
            end if;
            -- #300 End of added
          else
            g_sup_int(i).err_msg := 'Error : Undefine, Source Supplier Name : ' ||
                                    g_sup_int(i).vendor_name;
            --          g_sup_int(i).Status  := 'REJECTED' ;
          end if;
          g_sup_int(i).status := 'REJECTED';
          -- #200 Added by AP@IS-ES on 4-Nov-2011
        elsif g_sup_int(i).status != 'NEW' and g_sup_int(i).status in
              ('ERROR VAT NUMERIC', 'ERROR VAT LENGTH', 'ERROR VAT PATTERN') then
        
          if g_sup_int(i).status = ('ERROR VAT NUMERIC') then
            g_sup_int(i).err_msg := 'Supplier Tax id should be numeric : ' ||
                                    g_sup_int(i).vendor_name;
          elsif g_sup_int(i).status = ('ERROR VAT LENGTH') then
            g_sup_int(i).err_msg := 'Supplier Tax id should be 10 or 13 digits : ' ||
                                    g_sup_int(i).vendor_name;
          elsif g_sup_int(i).status = ('ERROR VAT PATTERN') then
            g_sup_int(i).err_msg := 'Supplier Tax id does not comply Central Civil-Registration : ' ||
                                    g_sup_int(i).vendor_name;
          end if;
          g_sup_int(i).status := 'REJECTED';
          -- #200 End of added
        elsif g_sup_int(i).vendor_id is null and g_sup_int(i).vendor_number is not null and g_sup_int(i).vat_registration_num is not null and g_sup_int(i).status = 'NEW' then        
          g_sup_int(i).erp_vendor_number := ' ';
          g_sup_int(i).erp_vendor_name := ' ';
          g_sup_int(i).erp_vat_registration_num := ' ';
          begin
            select pov.segment1, pov.vendor_name, vat_registration_num
              into g_sup_int(i).erp_vendor_number,
                   g_sup_int(i).erp_vendor_name,
                   g_sup_int(i).erp_vat_registration_num
              from po_vendors pov
             where pov.segment1 = g_sup_int(i).vendor_number;
          exception
            when others then
              g_sup_int(i).err_msg := 'Source Supplier Number: ' ||
                                      g_sup_int(i).vendor_number ||
                                      ' and Source Supplier Name : ' ||
                                      g_sup_int(i).vendor_name ||
                                      'does not exist in ERP System';
          end;
        
          if g_sup_int(i).vat_registration_num <> g_sup_int(i).erp_vat_registration_num then
            strerr := 'Error : Tax Registration Dose Not Match ';
          elsif g_sup_int(i).vendor_name <> g_sup_int(i).erp_vendor_name then
            strerr := 'Error : Supplier Name Dose Not Match ';
          else
            strerr := 'Error : ';
          end if;
        
          g_sup_int(i).err_msg := strerr ||
                                  ' Source Tax Registration No. : ' ||
                                  g_sup_int(i).vat_registration_num ||
                                  ' ERP    Tax Registration No. : ' ||
                                  g_sup_int(i).erp_vat_registration_num ||
                                  ' Source Supplier Name   : ' ||
                                  g_sup_int(i).vendor_name ||
                                  ' Source Supplier Number : ' ||
                                  g_sup_int(i).vendor_number ||
                                  ' ERP    Supplier Name   : ' ||
                                  g_sup_int(i).erp_vendor_name ||
                                  ' ERP    Supplier Number : ' ||
                                  g_sup_int(i).erp_vendor_number;
        
          g_sup_int(i).status := 'REJECTED';
        
        else
        
          g_sup_int(i).status := 'PROCESSED';
          g_sup_int(i).err_msg := ' ';
          ---!!@@
        
          if g_sup_int(i).vendor_number is null then
            if g_sup_int(i).vendor_name = g_sup_int(i).erp_vendor_name and g_sup_int(i).vat_registration_num = g_sup_int(i).erp_vat_registration_num and g_sup_int(i).vendor_site_id is not null then
              g_sup_int(i).err_msg := 'Error : TAX Registration Number, Supplier Name and Vendor Site Duplicate in ERP Table of Source Supplier Name : ' ||
                                      g_sup_int(i).vendor_name;
              g_sup_int(i).status := 'REJECTED';
            end if;
          end if;
        
          --------------------------------------------------------
          if g_sup_int(i).vendor_id is null then
            null;
          else
            -- Old Supplier
            if (g_sup_int(i).erp_vendor_number != nvl(g_sup_int(i).vendor_number, '**')) and
               g_sup_int(i).vendor_number is not null then
              g_sup_int(i).err_msg := 'Error : Source Supplier Number : ' ||
                                      g_sup_int(i).vendor_number || ' Not Equal ' ||
                                      ' ERP Supplier Number : ' ||
                                      g_sup_int(i).erp_vendor_number ||
                                      ' of Source Supplier Name : ' ||
                                      g_sup_int(i).vendor_name;
              g_sup_int(i).status := 'REJECTED';
            end if;
          end if;
        
          if g_sup_int(i).vendor_site_id is not null then
            begin
              select pvc.vendor_contact_id
                into g_sup_int(i).vendor_site_cont_id
                from po_vendor_contacts pvc
               where pvc.vendor_site_id = g_sup_int(i).vendor_site_id
                    --and    pvc.vendor_id      = g_sup_int(i).vendor_id
                 and rownum = 1;
            exception
              when others then
                g_sup_int(i).vendor_site_cont_id := to_number(null);
            end;
          end if; -- Supplier Site
        
        end if; -- for NEW Supplier
      
        --- Supplier Type
        if g_sup_int(i).vendor_type_lookup_code is not null then
          begin
            select flv.meaning
              into v_supplier_type
              from fnd_lookup_values flv
             where flv.lookup_type = 'VENDOR TYPE'
               and flv.enabled_flag = 'Y'
               and flv.lookup_code = g_sup_int(i).vendor_type_lookup_code;
          exception
            when others then
              g_sup_int(i).err_msg := g_sup_int(i).err_msg || ' ' ||
                                      ' Error :Source Supplier Type : ' ||
                                      g_sup_int(i).vendor_type_lookup_code ||
                                      ' Not Found of Source Supplier Name : ' ||
                                      g_sup_int(i).vendor_name || chr(13);
              g_sup_int(i).status := 'REJECTED';
          end;
        end if;
      
        if g_sup_int(i).terms_name is not null then
          begin
            select at.name
              into v_payment_term
              from ap_terms at
             where at.name = g_sup_int(i).terms_name;
          exception
            when others then
              g_sup_int(i).err_msg := g_sup_int(i).err_msg || ' ' ||
                                      ' Error : Payment Term : ' ||
                                      g_sup_int(i).terms_name ||
                                      ' Not Found of Source Supplier Name : ' ||
                                      g_sup_int(i).vendor_name;
              g_sup_int(i).status := 'REJECTED';
          end;
          v_msg := null;
        end if;
      
        if g_sup_int(i).pay_group_lookup_code is not null then
          begin
            select flv.meaning
              into v_pay_group
              from fnd_lookup_values flv
             where flv.lookup_type = 'PAY GROUP'
               and flv.lookup_code = g_sup_int(i).pay_group_lookup_code;
          exception
            when others then
              g_sup_int(i).err_msg := g_sup_int(i).err_msg || ' ' ||
                                      'Error : Pay group : ' ||
                                      g_sup_int(i).pay_group_lookup_code ||
                                      ' Not Found of Source Supplier Name : ' ||
                                      g_sup_int(i).vendor_name;
              g_sup_int(i).status := 'REJECTED';
          end;
          v_msg := null;
        end if;
      
        if g_sup_int(i).country is not null then
          begin
            select ft.territory_short_name
              into v_country
              from fnd_territories_vl ft
             where ft.territory_code = g_sup_int(i).country;
          exception
            when others then
              g_sup_int(i).err_msg := g_sup_int(i).err_msg || ' ' ||
                                      ' Error : Country : ' || g_sup_int(i).country ||
                                      ' Not Found of Source Supplier Name : ' ||
                                      g_sup_int(i).vendor_name;
              g_sup_int(i).status := 'REJECTED';
          end;
        end if;
        -- END IF ; New Supplier
      end loop;
    
      ------------------------------------
    
      for i in 1 .. g_sup_int.count loop
        --Comment by pta 16/11/06 g_sup_int.FIRST..g_sup_int.LAST LOOP
      
        if substr(g_sup_int(i).status, 1, 8) = 'REJECTED' then
        
          update dtac_ap_suppliers_temp_int
             set status = 'REJECTED'
                 -- #400 Modify by AP@IS-ES on 04-Nov-2011
                ,
                 error_message = substr(g_sup_int(i).err_msg, 1, 999)
          -- #400 End of Modify
           where rowid = g_sup_int(i).row_id;
        end if;
      end loop;
    
    else
      fnd_file.put_line(fnd_file.log, 'No Row Process');
    end if;
  
    commit;
  
  end validate_supplier_summary;

  ------------------------------------------
  procedure insert_dtac_ap_sup_output(ir_dtac_ap_sup_output in dtac_ap_suppliers_output%rowtype) is
  begin
  
    insert into dtac_ap_suppliers_output
      (interface_id,
       interface_date,
       vendor_number,
       vendor_name,
       vat_registration_num,
       retailer_code,
       vendor_site_code,
       status,
       default_group,
       source_file_name,
       erp_vendor_number,
       erp_vendor_name,
       erp_vat_registration_num,
       interface_mode,
       error_message,
       attribute1,
       attribute2,
       attribute3,
       attribute4,
       attribute5,
       attribute6,
       attribute7,
       attribute8,
       attribute9,
       attribute10,
       eai_crtd_dttm)
    values
      (ir_dtac_ap_sup_output.interface_id,
       ir_dtac_ap_sup_output.interface_date,
       ir_dtac_ap_sup_output.vendor_number,
       ir_dtac_ap_sup_output.vendor_name,
       to_char(ir_dtac_ap_sup_output.vat_registration_num), --Boy
       ir_dtac_ap_sup_output.retailer_code,
       ir_dtac_ap_sup_output.vendor_site_code,
       ir_dtac_ap_sup_output.status,
       ir_dtac_ap_sup_output.default_group,
       ir_dtac_ap_sup_output.source_file_name,
       ir_dtac_ap_sup_output.erp_vendor_number,
       ir_dtac_ap_sup_output.erp_vendor_name,
       to_char(ir_dtac_ap_sup_output.erp_vat_registration_num), --Boy
       ir_dtac_ap_sup_output.interface_mode,
       ir_dtac_ap_sup_output.error_message,
       ir_dtac_ap_sup_output.attribute1,
       ir_dtac_ap_sup_output.attribute2,
       ir_dtac_ap_sup_output.attribute3,
       ir_dtac_ap_sup_output.attribute4,
       ir_dtac_ap_sup_output.attribute5,
       ir_dtac_ap_sup_output.attribute6,
       ir_dtac_ap_sup_output.attribute7,
       ir_dtac_ap_sup_output.attribute8,
       ir_dtac_ap_sup_output.attribute9,
       ir_dtac_ap_sup_output.attribute10,
       ir_dtac_ap_sup_output.eai_crtd_dttm);
  end insert_dtac_ap_sup_output;
  ---------------------------------------------------------------------------------

  procedure insert_ap_suppliers_int(ir_ap_suppliers_int in ap_suppliers_int%rowtype) is
  begin
  
    insert into ap_suppliers_int
      (vendor_interface_id,
       vendor_name,
       vendor_name_alt,
       segment1,
       vendor_type_lookup_code,
       terms_name,
       set_of_books_id,
       pay_date_basis_lookup_code,
       pay_group_lookup_code,
       payment_priority,
       invoice_currency_code,
       payment_method_lookup_code,
       terms_date_basis,
       vat_registration_num,
       allow_awt_flag,
       global_attribute_category,
       global_attribute18,
       global_attribute6,
       global_attribute20,
       attribute_category,
       attribute1,
       attribute2,
       attribute3,
       attribute4,
       attribute5,
       attribute6,
       attribute7,
       attribute8,
       attribute9,
       attribute10,
       attribute11,
       attribute12,
       attribute13,
       attribute14,
       attribute15,
       status,
       reject_code,
       request_id,
       creation_date,
       last_update_date,
       created_by,
       last_updated_by)
    values
      (ir_ap_suppliers_int.vendor_interface_id,
       ir_ap_suppliers_int.vendor_name,
       ir_ap_suppliers_int.vendor_name_alt,
       ir_ap_suppliers_int.segment1,
       ir_ap_suppliers_int.vendor_type_lookup_code,
       ir_ap_suppliers_int.terms_name,
       ir_ap_suppliers_int.set_of_books_id,
       ir_ap_suppliers_int.pay_date_basis_lookup_code,
       ir_ap_suppliers_int.pay_group_lookup_code,
       ir_ap_suppliers_int.payment_priority,
       ir_ap_suppliers_int.invoice_currency_code,
       ir_ap_suppliers_int.payment_method_lookup_code,
       ir_ap_suppliers_int.terms_date_basis,
       ir_ap_suppliers_int.vat_registration_num,
       ir_ap_suppliers_int.allow_awt_flag,
       ir_ap_suppliers_int.global_attribute_category,
       ir_ap_suppliers_int.global_attribute18,
       ir_ap_suppliers_int.global_attribute6,
       ir_ap_suppliers_int.global_attribute20,
       ir_ap_suppliers_int.attribute_category,
       ir_ap_suppliers_int.attribute1,
       ir_ap_suppliers_int.attribute2,
       ir_ap_suppliers_int.attribute3,
       ir_ap_suppliers_int.attribute4,
       ir_ap_suppliers_int.attribute5,
       ir_ap_suppliers_int.attribute6,
       ir_ap_suppliers_int.attribute7,
       ir_ap_suppliers_int.attribute8,
       ir_ap_suppliers_int.attribute9,
       ir_ap_suppliers_int.attribute10,
       ir_ap_suppliers_int.attribute11,
       ir_ap_suppliers_int.attribute12,
       ir_ap_suppliers_int.attribute13,
       ir_ap_suppliers_int.attribute14,
       ir_ap_suppliers_int.attribute15,
       ir_ap_suppliers_int.status,
       ir_ap_suppliers_int.reject_code,
       ir_ap_suppliers_int.request_id,
       ir_ap_suppliers_int.creation_date,
       ir_ap_suppliers_int.last_update_date,
       ir_ap_suppliers_int.created_by,
       ir_ap_suppliers_int.last_updated_by);
  end insert_ap_suppliers_int;
  ---------------------------------------------------------------------------------
  procedure insert_ap_sup_site(ir_ap_sup_site in ap_supplier_sites_int%rowtype) is
  begin
  
    write_log('Insert supplier site interface for vendor id = ' ||
              ir_ap_sup_site.vendor_id);
  
    insert into ap_supplier_sites_int
      (vendor_id,
       vendor_interface_id,
       vendor_site_code,
       purchasing_site_flag,
       pay_site_flag,
       address_line1,
       address_line2,
       address_line3,
       address_line4,
       city,
       zip,
       province,
       country,
       payment_method_lookup_code,
       terms_date_basis,
       distribution_set_name,
       accts_pay_code_combination_id,
       prepay_code_combination_id,
       pay_group_lookup_code,
       payment_priority,
       terms_name,
       pay_date_basis_lookup_code,
       invoice_currency_code,
       attribute1,
       attribute2,
       attribute3,
       attribute4,
       /* Add by Tong 19/09/2007*/
       vendor_site_code_alt, --Shop Name
       attribute12, --Mail to Address1
       attribute13, --Mail to Address2
       attribute14, --Mail to Address3
       attribute15, --Owner Name
       attribute5,
       attribute6,
       attribute7,
       attribute8,
       attribute9,
       attribute10,
       attribute_category,
       org_id,
       allow_awt_flag,
       global_attribute6,
       global_attribute20,
       future_dated_payment_ccid,
       status,
       reject_code,
       request_id,
       creation_date,
       last_update_date,
       created_by,
       last_updated_by
       --added by AP@BAS on 14-Oct-2013
      ,
       email_address
       -- Added by AP@BAS on 12-Dec-2013
      ,
       global_attribute_category,
       global_attribute14)
    values
      (ir_ap_sup_site.vendor_id,
       ir_ap_sup_site.vendor_interface_id,
       ir_ap_sup_site.vendor_site_code,
       ir_ap_sup_site.purchasing_site_flag,
       ir_ap_sup_site.pay_site_flag,
       ir_ap_sup_site.address_line1,
       ir_ap_sup_site.address_line2,
       ir_ap_sup_site.address_line3,
       ir_ap_sup_site.address_line4,
       ir_ap_sup_site.city,
       ir_ap_sup_site.zip,
       ir_ap_sup_site.province,
       ir_ap_sup_site.country,
       ir_ap_sup_site.payment_method_lookup_code,
       ir_ap_sup_site.terms_date_basis,
       ir_ap_sup_site.distribution_set_name,
       ir_ap_sup_site.accts_pay_code_combination_id,
       ir_ap_sup_site.prepay_code_combination_id,
       ir_ap_sup_site.pay_group_lookup_code,
       ir_ap_sup_site.payment_priority,
       ir_ap_sup_site.terms_name,
       ir_ap_sup_site.pay_date_basis_lookup_code,
       ir_ap_sup_site.invoice_currency_code,
       ir_ap_sup_site.attribute1,
       ir_ap_sup_site.attribute2,
       ir_ap_sup_site.attribute3,
       ir_ap_sup_site.attribute4,
       /* Add by Tong 19/09/2007*/
       ir_ap_sup_site.vendor_site_code_alt, --Shop Name
       ir_ap_sup_site.attribute12, --Mail to Address1
       ir_ap_sup_site.attribute13, --Mail to Address2
       ir_ap_sup_site.attribute14, --Mail to Address3
       ir_ap_sup_site.attribute15, --Owner Name
       ir_ap_sup_site.attribute5,
       ir_ap_sup_site.attribute6,
       ir_ap_sup_site.attribute7,
       ir_ap_sup_site.attribute8,
       ir_ap_sup_site.attribute9,
       ir_ap_sup_site.attribute10,
       ir_ap_sup_site.attribute_category,
       ir_ap_sup_site.org_id,
       ir_ap_sup_site.allow_awt_flag,
       ir_ap_sup_site.global_attribute6,
       ir_ap_sup_site.global_attribute20,
       ir_ap_sup_site.future_dated_payment_ccid,
       ir_ap_sup_site.status,
       ir_ap_sup_site.reject_code,
       ir_ap_sup_site.request_id,
       ir_ap_sup_site.creation_date,
       ir_ap_sup_site.last_update_date,
       ir_ap_sup_site.created_by,
       ir_ap_sup_site.last_updated_by
       --added by AP@BAS on 14-Oct-2013
      ,
       ir_ap_sup_site.email_address
       -- Added by AP@BAS on 12-Dec-2013
      ,
       ir_ap_sup_site.global_attribute_category,
       ir_ap_sup_site.global_attribute14);
  
    -- Added by AP@BAS on 07-Feb-2013 : dupplicate for DMS, STS
    if (g_default_group in ('DMS', 'STS')) then
      ---------------------------------------------------------------------------------
      -- Insert supplier site to interface table
      -- supplier site information duplicate from HO and change vendor_site_code, org_id
      -- to DTAC-N
      -- Author : AP@BAS
      -- Create : 04-Feb-2013
      ---------------------------------------------------------------------------------
      declare
        v_supplier_site_code_mapped varchar2(50);
        v_accts_pay_ccid            number;
        v_prepay_ccid               number;
      begin
        v_supplier_site_code_mapped := get_map_supplier_stie_code(ir_ap_sup_site.vendor_site_code,
                                                                  'DTN');
        v_accts_pay_ccid            := get_map_accts_pay_ccid(ir_ap_sup_site.accts_pay_code_combination_id,
                                                              'DTN');
        v_prepay_ccid               := get_map_prepay_ccid(ir_ap_sup_site.prepay_code_combination_id,
                                                           'DTN');
        insert into ap_supplier_sites_int
          (vendor_id,
           vendor_interface_id,
           vendor_site_code,
           purchasing_site_flag,
           pay_site_flag,
           address_line1,
           address_line2,
           address_line3,
           address_line4,
           city,
           zip,
           province,
           country,
           payment_method_lookup_code,
           terms_date_basis,
           distribution_set_name,
           accts_pay_code_combination_id,
           prepay_code_combination_id,
           pay_group_lookup_code,
           payment_priority,
           terms_name,
           pay_date_basis_lookup_code,
           invoice_currency_code,
           attribute1,
           attribute2,
           attribute3,
           attribute4,
           /* Add by Tong 19/09/2007*/
           vendor_site_code_alt, --Shop Name
           attribute12, --Mail to Address1
           attribute13, --Mail to Address2
           attribute14, --Mail to Address3
           attribute15, --Owner Name
           attribute5,
           attribute6,
           attribute7,
           attribute8,
           attribute9,
           attribute10,
           attribute_category,
           org_id,
           allow_awt_flag,
           global_attribute6,
           global_attribute20,
           future_dated_payment_ccid,
           status,
           reject_code,
           request_id,
           creation_date,
           last_update_date,
           created_by,
           last_updated_by
           --added by AP@BAS on 14-Oct-2013
          ,
           email_address
           -- Added by AP@BAS on 12-Dec-2013
          ,
           global_attribute_category,
           global_attribute14)
        values
          (ir_ap_sup_site.vendor_id,
           ir_ap_sup_site.vendor_interface_id,
           v_supplier_site_code_mapped
           --,ir_ap_sup_site.vendor_site_code
          ,
           ir_ap_sup_site.purchasing_site_flag,
           ir_ap_sup_site.pay_site_flag,
           ir_ap_sup_site.address_line1,
           ir_ap_sup_site.address_line2,
           ir_ap_sup_site.address_line3,
           ir_ap_sup_site.address_line4,
           ir_ap_sup_site.city,
           ir_ap_sup_site.zip,
           ir_ap_sup_site.province,
           ir_ap_sup_site.country,
           ir_ap_sup_site.payment_method_lookup_code,
           ir_ap_sup_site.terms_date_basis,
           ir_ap_sup_site.distribution_set_name
           
          ,
           v_accts_pay_ccid,
           v_prepay_ccid
           --,ir_ap_sup_site.accts_pay_code_combination_id
           --,ir_ap_sup_site.prepay_code_combination_id
           
          ,
           ir_ap_sup_site.pay_group_lookup_code,
           ir_ap_sup_site.payment_priority,
           ir_ap_sup_site.terms_name,
           ir_ap_sup_site.pay_date_basis_lookup_code,
           ir_ap_sup_site.invoice_currency_code,
           ir_ap_sup_site.attribute1,
           ir_ap_sup_site.attribute2,
           ir_ap_sup_site.attribute3,
           ir_ap_sup_site.attribute4,
           /* Add by Tong 19/09/2007*/
           ir_ap_sup_site.vendor_site_code_alt, --Shop Name
           ir_ap_sup_site.attribute12, --Mail to Address1
           ir_ap_sup_site.attribute13, --Mail to Address2
           ir_ap_sup_site.attribute14, --Mail to Address3
           ir_ap_sup_site.attribute15, --Owner Name
           ir_ap_sup_site.attribute5,
           ir_ap_sup_site.attribute6,
           ir_ap_sup_site.attribute7,
           ir_ap_sup_site.attribute8,
           ir_ap_sup_site.attribute9,
           ir_ap_sup_site.attribute10,
           ir_ap_sup_site.attribute_category,
           g_dtn_org_id
           --,ir_ap_sup_site.org_id
          ,
           ir_ap_sup_site.allow_awt_flag,
           ir_ap_sup_site.global_attribute6,
           ir_ap_sup_site.global_attribute20,
           ir_ap_sup_site.future_dated_payment_ccid,
           ir_ap_sup_site.status,
           ir_ap_sup_site.reject_code,
           ir_ap_sup_site.request_id,
           ir_ap_sup_site.creation_date,
           ir_ap_sup_site.last_update_date,
           ir_ap_sup_site.created_by,
           ir_ap_sup_site.last_updated_by
           --added by AP@BAS on 14-Oct-2013
          ,
           ir_ap_sup_site.email_address
           -- Added by AP@BAS on 12-Dec-2013
          ,
           ir_ap_sup_site.global_attribute_category,
           ir_ap_sup_site.global_attribute14);
      exception
        when others then
          write_log('Insert supplier site interface for DTAC-N error :');
          write_log(SQLCODE || ':' || SQLERRM);
      end;
    
      ---------------------------------------------------------------------------------
      -- Insert supplier site to interface table
      -- supplier site information duplicate from HO and change vendor_site_code, org_id
      -- to BAYSBUY
      -- Author : AP@BAS
      -- Create : 04-Feb-2013
      ---------------------------------------------------------------------------------
      declare
        v_supplier_site_code_mapped varchar2(50);
        v_accts_pay_ccid            number;
        v_prepay_ccid               number;
      begin
        v_supplier_site_code_mapped := get_map_supplier_stie_code(ir_ap_sup_site.vendor_site_code,
                                                                  'PSB');
        v_accts_pay_ccid            := get_map_accts_pay_ccid(ir_ap_sup_site.accts_pay_code_combination_id,
                                                              'PSB');
        v_prepay_ccid               := get_map_prepay_ccid(ir_ap_sup_site.prepay_code_combination_id,
                                                           'PSB');
        insert into ap_supplier_sites_int
          (vendor_id,
           vendor_interface_id,
           vendor_site_code,
           purchasing_site_flag,
           pay_site_flag,
           address_line1,
           address_line2,
           address_line3,
           address_line4,
           city,
           zip,
           province,
           country,
           payment_method_lookup_code,
           terms_date_basis,
           distribution_set_name,
           accts_pay_code_combination_id,
           prepay_code_combination_id,
           pay_group_lookup_code,
           payment_priority,
           terms_name,
           pay_date_basis_lookup_code,
           invoice_currency_code,
           attribute1,
           attribute2,
           attribute3,
           attribute4,
           /* Add by Tong 19/09/2007*/
           vendor_site_code_alt, --Shop Name
           attribute12, --Mail to Address1
           attribute13, --Mail to Address2
           attribute14, --Mail to Address3
           attribute15, --Owner Name
           attribute5,
           attribute6,
           attribute7,
           attribute8,
           attribute9,
           attribute10,
           attribute_category,
           org_id,
           allow_awt_flag,
           global_attribute6,
           global_attribute20,
           future_dated_payment_ccid,
           status,
           reject_code,
           request_id,
           creation_date,
           last_update_date,
           created_by,
           last_updated_by
           --added by AP@BAS on 14-Oct-2013
          ,
           email_address
           -- Added by AP@BAS on 12-Dec-2013
          ,
           global_attribute_category,
           global_attribute14)
        values
          (ir_ap_sup_site.vendor_id,
           ir_ap_sup_site.vendor_interface_id,
           v_supplier_site_code_mapped
           --,ir_ap_sup_site.vendor_site_code
          ,
           ir_ap_sup_site.purchasing_site_flag,
           ir_ap_sup_site.pay_site_flag,
           ir_ap_sup_site.address_line1,
           ir_ap_sup_site.address_line2,
           ir_ap_sup_site.address_line3,
           ir_ap_sup_site.address_line4,
           ir_ap_sup_site.city,
           ir_ap_sup_site.zip,
           ir_ap_sup_site.province,
           ir_ap_sup_site.country,
           ir_ap_sup_site.payment_method_lookup_code,
           ir_ap_sup_site.terms_date_basis,
           ir_ap_sup_site.distribution_set_name
           
          ,
           v_accts_pay_ccid,
           v_prepay_ccid
           --,ir_ap_sup_site.accts_pay_code_combination_id
           --,ir_ap_sup_site.prepay_code_combination_id
           
          ,
           ir_ap_sup_site.pay_group_lookup_code,
           ir_ap_sup_site.payment_priority,
           ir_ap_sup_site.terms_name,
           ir_ap_sup_site.pay_date_basis_lookup_code,
           ir_ap_sup_site.invoice_currency_code,
           ir_ap_sup_site.attribute1,
           ir_ap_sup_site.attribute2,
           ir_ap_sup_site.attribute3,
           ir_ap_sup_site.attribute4,
           /* Add by Tong 19/09/2007*/
           ir_ap_sup_site.vendor_site_code_alt, --Shop Name
           ir_ap_sup_site.attribute12, --Mail to Address1
           ir_ap_sup_site.attribute13, --Mail to Address2
           ir_ap_sup_site.attribute14, --Mail to Address3
           ir_ap_sup_site.attribute15, --Owner Name
           ir_ap_sup_site.attribute5,
           ir_ap_sup_site.attribute6,
           ir_ap_sup_site.attribute7,
           ir_ap_sup_site.attribute8,
           ir_ap_sup_site.attribute9,
           ir_ap_sup_site.attribute10,
           ir_ap_sup_site.attribute_category,
           g_psb_org_id
           --,ir_ap_sup_site.org_id
          ,
           ir_ap_sup_site.allow_awt_flag,
           ir_ap_sup_site.global_attribute6,
           ir_ap_sup_site.global_attribute20,
           ir_ap_sup_site.future_dated_payment_ccid,
           ir_ap_sup_site.status,
           ir_ap_sup_site.reject_code,
           ir_ap_sup_site.request_id,
           ir_ap_sup_site.creation_date,
           ir_ap_sup_site.last_update_date,
           ir_ap_sup_site.created_by,
           ir_ap_sup_site.last_updated_by
           --added by AP@BAS on 14-Oct-2013
          ,
           ir_ap_sup_site.email_address
           -- Added by AP@BAS on 12-Dec-2013
          ,
           ir_ap_sup_site.global_attribute_category,
           ir_ap_sup_site.global_attribute14);
      exception
        when others then
          write_log('Insert supplier site interface for PAYSABUY error :');
          write_log(SQLCODE || ':' || SQLERRM);
      end;
    end if;
  end insert_ap_sup_site;
  ---------------------------------------------------------------------------------
  procedure insert_ap_sup_site_cont(ir_ap_sup_site_cont in ap_sup_site_contact_int%rowtype) is
    -- added by AP@BAS on 06-Feb-2013
    v_dtn_vender_site_code_mapped varchar2(50);
    v_dtn_vender_site_id          number;
    v_psb_vender_site_code_mapped varchar2(50);
    v_psb_vender_site_id          number;
    v_vendor_id                   number;
    --
  begin
    -- added by AP@BAS on 06-Feb-2013 : mapping vendor site code to DTN, PSB
    v_dtn_vender_site_code_mapped := get_map_supplier_stie_code(ir_ap_sup_site_cont.vendor_site_code,
                                                                'DTN');
    v_psb_vender_site_code_mapped := get_map_supplier_stie_code(ir_ap_sup_site_cont.vendor_site_code,
                                                                'PSB');
    --
  
    insert into ap_sup_site_contact_int
      (vendor_site_id,
       vendor_site_code,
       org_id,
       first_name,
       last_name,
       area_code,
       phone,
       status,
       email_address,
       fax,
       contact_name_alt,
       request_id,
       creation_date,
       last_update_date,
       created_by,
       last_updated_by)
    values
      (ir_ap_sup_site_cont.vendor_site_id
       -- remark by AP@BAS on -06-Feb-2013 : change to '' always
       --,ir_ap_sup_site_cont.vendor_site_code
      ,
       '',
       ir_ap_sup_site_cont.org_id,
       ir_ap_sup_site_cont.first_name,
       ir_ap_sup_site_cont.last_name,
       ir_ap_sup_site_cont.area_code,
       ir_ap_sup_site_cont.phone,
       ir_ap_sup_site_cont.status,
       ir_ap_sup_site_cont.email_address,
       ir_ap_sup_site_cont.fax,
       ir_ap_sup_site_cont.contact_name_alt,
       ir_ap_sup_site_cont.request_id,
       ir_ap_sup_site_cont.creation_date,
       ir_ap_sup_site_cont.last_update_date,
       ir_ap_sup_site_cont.created_by,
       ir_ap_sup_site_cont.last_updated_by);
  
    if (g_default_group in ('DMS', 'STS')) then
      -- Added by AP@BAS on 06-Feb-2013 : get vendor id
      begin
        select min(vendor_id)
          into v_vendor_id
          from po_vendor_sites_all
         where vendor_site_id = ir_ap_sup_site_cont.vendor_site_id;
      exception
        when others then
          null;
      end;
      -- Added by AP@BAS on 06-Feb-2013
      -- insert supplier site contact for DTN
      begin
        select vendor_site_id
          into v_dtn_vender_site_id
          from po_vendor_sites_all
         where org_id = g_dtn_org_id
           and vendor_id = v_vendor_id
           and vendor_site_code = v_dtn_vender_site_code_mapped;
      
        insert into ap_sup_site_contact_int
          (vendor_site_id,
           vendor_site_code,
           org_id,
           first_name,
           last_name,
           area_code,
           phone,
           status,
           email_address,
           fax,
           contact_name_alt,
           request_id,
           creation_date,
           last_update_date,
           created_by,
           last_updated_by)
        values
          (v_dtn_vender_site_id,
           '',
           g_dtn_org_id,
           ir_ap_sup_site_cont.first_name,
           ir_ap_sup_site_cont.last_name,
           ir_ap_sup_site_cont.area_code,
           ir_ap_sup_site_cont.phone,
           ir_ap_sup_site_cont.status,
           ir_ap_sup_site_cont.email_address,
           ir_ap_sup_site_cont.fax,
           ir_ap_sup_site_cont.contact_name_alt,
           ir_ap_sup_site_cont.request_id,
           ir_ap_sup_site_cont.creation_date,
           ir_ap_sup_site_cont.last_update_date,
           ir_ap_sup_site_cont.created_by,
           ir_ap_sup_site_cont.last_updated_by);
      exception
        when no_data_found then
          write_log('tac_supplier_interface_util.insert_dtn_contact not found vendor site code ' ||
                    v_dtn_vender_site_code_mapped);
        when others then
          write_log('tac_supplier_interface_util.insert_dtn_contact error ');
      end;
    
      -- Added by AP@BAS on 06-Feb-2013
      -- insert supplier site contact for PSB
      begin
        select vendor_site_id
          into v_psb_vender_site_id
          from po_vendor_sites_all
         where org_id = g_psb_org_id
           and vendor_id = v_vendor_id
           and vendor_site_code = v_psb_vender_site_code_mapped;
      
        insert into ap_sup_site_contact_int
          (vendor_site_id,
           vendor_site_code,
           org_id,
           first_name,
           last_name,
           area_code,
           phone,
           status,
           email_address,
           fax,
           contact_name_alt,
           request_id,
           creation_date,
           last_update_date,
           created_by,
           last_updated_by)
        values
          (v_psb_vender_site_id,
           '',
           g_psb_org_id,
           ir_ap_sup_site_cont.first_name,
           ir_ap_sup_site_cont.last_name,
           ir_ap_sup_site_cont.area_code,
           ir_ap_sup_site_cont.phone,
           ir_ap_sup_site_cont.status,
           ir_ap_sup_site_cont.email_address,
           ir_ap_sup_site_cont.fax,
           ir_ap_sup_site_cont.contact_name_alt,
           ir_ap_sup_site_cont.request_id,
           ir_ap_sup_site_cont.creation_date,
           ir_ap_sup_site_cont.last_update_date,
           ir_ap_sup_site_cont.created_by,
           ir_ap_sup_site_cont.last_updated_by);
      exception
        when no_data_found then
          write_log('tac_supplier_interface_util.insert_psb_contact not found vendor site code ' ||
                    v_dtn_vender_site_code_mapped);
        when others then
          write_log('tac_supplier_interface_util.insert_psb_contact error ');
      end;
    end if;
  
  exception
    when others then
      write_log('tac_supplier_interface_util.insert_ap_sup_site_cont exception.');
  end insert_ap_sup_site_cont;

  ---------------------------------------------------------------------------------
  procedure insert_ap_sup_to_sts(ir_ap_sup_to_sts in dtac_ap_suppliers_to_sts%rowtype) is
  begin
    insert into dtac_ap_suppliers_to_sts
      (seqn,
       retailer_code,
       supplier_number,
       supplier_site_code,
       creation_date,
       eai_stts,
       eai_last_chng_dttm,
       default_group)
    values
      (ir_ap_sup_to_sts.seqn,
       ir_ap_sup_to_sts.retailer_code,
       ir_ap_sup_to_sts.supplier_number,
       ir_ap_sup_to_sts.supplier_site_code,
       ir_ap_sup_to_sts.creation_date,
       ir_ap_sup_to_sts.eai_stts,
       ir_ap_sup_to_sts.eai_last_chng_dttm,
       ir_ap_sup_to_sts.default_group);
  end insert_ap_sup_to_sts;
  ---------------------------------------------------------------------------------
  procedure insert_tmp_eai_supplier_itfc(ir_tmp_eai_supplier_itfc in tmp_eai_supplier_itfc%rowtype) is
  begin
    insert into tmp_eai_supplier_itfc
      (retailer_code, supplier_site_code, supplier_number, eai_crtd_dttm)
    values
      (ir_tmp_eai_supplier_itfc.retailer_code,
       ir_tmp_eai_supplier_itfc.supplier_site_code,
       ir_tmp_eai_supplier_itfc.supplier_number,
       ir_tmp_eai_supplier_itfc.eai_crtd_dttm);
  end insert_tmp_eai_supplier_itfc;
  ---------------------------------------------------------------------------------
  procedure update_po_vendors(ir_po_vendors in po_vendors%rowtype,
                              o_status      out boolean) is
  begin
    o_status := false;
    update po_vendors
       set /* Add by Tong 19/09/2007*/  vat_registration_num = ir_po_vendors.vat_registration_num,
           terms_id              = ir_po_vendors.terms_id,
           pay_group_lookup_code = ir_po_vendors.pay_group_lookup_code,
           global_attribute20    = ir_po_vendors.global_attribute20,
           global_attribute6     = ir_po_vendors.global_attribute6,
           attribute2            = ir_po_vendors.attribute2,
           attribute3            = ir_po_vendors.attribute3,
           attribute4            = ir_po_vendors.attribute4
           
           /* Add by Tong 19/09/2007*/
           --    ,vendor_site_code_alt           =   ir_po_vendors.vendor_site_code_alt   --Shop Name
          ,
           attribute12 = ir_po_vendors.attribute12 --Mail to Address1
          ,
           attribute13 = ir_po_vendors.attribute13 --Mail to Address2
          ,
           attribute14 = ir_po_vendors.attribute14 --Mail to Address3
          ,
           attribute15 = ir_po_vendors.attribute15 --Owner Name
           
          ,
           last_update_date  = sysdate,
           last_updated_by   = fnd_global.user_id,
           last_update_login = fnd_global.login_id
     where vendor_id = ir_po_vendors.vendor_id;
    --write_log('Update vendor');
    o_status := false;
    --COMMIT;
  exception
    when others then
      --ROLLBACK;
      o_status := true;
      write_log('Can not update vendor!');
  end update_po_vendors;
  ---------------------------------------------------------------------------------

  procedure update_po_vendor_sites(ir_po_vendor_sites in po_vendor_sites_all%rowtype,
                                   o_status           out boolean) is
  begin
    write_log('Update Global Attribute14 to => ' ||
              ir_po_vendor_sites.global_attribute14);
  
    -- added by AP@BAS on 06-Feb-2013
    o_status := false;
    --
    update po_vendor_sites_all
       set terms_id              = ir_po_vendor_sites.terms_id,
           pay_group_lookup_code = ir_po_vendor_sites.pay_group_lookup_code,
           address_line1         = ir_po_vendor_sites.address_line1,
           address_line2         = ir_po_vendor_sites.address_line2,
           address_line3         = ir_po_vendor_sites.address_line3,
           address_line4         = ir_po_vendor_sites.address_line4,
           city                  = ir_po_vendor_sites.city,
           zip                   = ir_po_vendor_sites.zip,
           province              = ir_po_vendor_sites.province,
           attribute1            = ir_po_vendor_sites.attribute1,
           attribute2            = ir_po_vendor_sites.attribute2,
           attribute3            = ir_po_vendor_sites.attribute3,
           attribute4            = ir_po_vendor_sites.attribute4
           /* Add by Tong 19/09/2007*/,
           vendor_site_code_alt = ir_po_vendor_sites.vendor_site_code_alt --Shop Name
          ,
           attribute12          = ir_po_vendor_sites.attribute12 --Mail to Address1
          ,
           attribute13          = ir_po_vendor_sites.attribute13 --Mail to Address2
          ,
           attribute14          = ir_po_vendor_sites.attribute14 --Mail to Address3
          ,
           attribute15          = ir_po_vendor_sites.attribute15 --Owner Name
          ,
           vat_registration_num = ir_po_vendor_sites.vat_registration_num,
           global_attribute20   = ir_po_vendor_sites.global_attribute20,
           global_attribute6    = ir_po_vendor_sites.global_attribute6,
           last_update_date     = sysdate,
           last_updated_by      = fnd_global.user_id,
           last_update_login    = fnd_global.login_id
           --added by AP@BAS on 14-Oct-2013
          ,
           email_address = ir_po_vendor_sites.email_address
           -- Added by AP@BAS on 12-Dec-2013
          ,
           global_attribute14 = ir_po_vendor_sites.global_attribute14
     where vendor_site_id = ir_po_vendor_sites.vendor_site_id;
  
    if (g_default_group in ('DMS', 'STS')) then
    
      -- Added by AP@BAS on 05-Feb-2013
      -- update supplier site for DTN
      declare
        v_dtn_vender_site_code_mapped varchar2(50);
        v_dtn_vender_site_id          number;
      begin
        v_dtn_vender_site_code_mapped := get_map_supplier_stie_code(ir_po_vendor_sites.vendor_site_code,
                                                                    'DTN');
        select vendor_site_id
          into v_dtn_vender_site_id
          from po_vendor_sites_all
         where org_id = g_dtn_org_id
           and vendor_id = ir_po_vendor_sites.vendor_id
           and vendor_site_code = v_dtn_vender_site_code_mapped;
      
        update po_vendor_sites_all
           set terms_id              = ir_po_vendor_sites.terms_id,
               pay_group_lookup_code = ir_po_vendor_sites.pay_group_lookup_code,
               address_line1         = ir_po_vendor_sites.address_line1,
               address_line2         = ir_po_vendor_sites.address_line2,
               address_line3         = ir_po_vendor_sites.address_line3,
               address_line4         = ir_po_vendor_sites.address_line4,
               city                  = ir_po_vendor_sites.city,
               zip                   = ir_po_vendor_sites.zip,
               province              = ir_po_vendor_sites.province,
               attribute1            = ir_po_vendor_sites.attribute1,
               attribute2            = ir_po_vendor_sites.attribute2,
               attribute3            = ir_po_vendor_sites.attribute3,
               attribute4            = ir_po_vendor_sites.attribute4
               /* Add by Tong 19/09/2007*/,
               vendor_site_code_alt = ir_po_vendor_sites.vendor_site_code_alt --Shop Name
              ,
               attribute12          = ir_po_vendor_sites.attribute12 --Mail to Address1
              ,
               attribute13          = ir_po_vendor_sites.attribute13 --Mail to Address2
              ,
               attribute14          = ir_po_vendor_sites.attribute14 --Mail to Address3
              ,
               attribute15          = ir_po_vendor_sites.attribute15 --Owner Name
              ,
               vat_registration_num = ir_po_vendor_sites.vat_registration_num,
               global_attribute20   = ir_po_vendor_sites.global_attribute20,
               global_attribute6    = ir_po_vendor_sites.global_attribute6,
               last_update_date     = sysdate,
               last_updated_by      = fnd_global.user_id,
               last_update_login    = fnd_global.login_id
               -- Added by AP@BAS on 12-Dec-2013
              ,
               global_attribute14 = ir_po_vendor_sites.global_attribute14
         where vendor_site_id = v_dtn_vender_site_id;
      
      exception
        when no_data_found then
          o_status := true;
          write_log('update_po_vendor_sites.update_dtn not found vendor site code ' ||
                    v_dtn_vender_site_code_mapped);
        when others then
          o_status := true;
          write_log('update_po_vendor_sites.update_dtn error ');
      end;
    
      -- update supplier site for PSB
    
      -- Added by AP@BAS on 05-Feb-2013
      -- update supplier site for PSB
      declare
        v_psb_vender_site_code_mapped varchar2(50);
        v_psb_vender_site_id          number;
      begin
        v_psb_vender_site_code_mapped := get_map_supplier_stie_code(ir_po_vendor_sites.vendor_site_code,
                                                                    'PSB');
        select vendor_site_id
          into v_psb_vender_site_id
          from po_vendor_sites_all
         where org_id = g_psb_org_id
           and vendor_id = ir_po_vendor_sites.vendor_id
           and vendor_site_code = v_psb_vender_site_code_mapped;
      
        update po_vendor_sites_all
           set terms_id              = ir_po_vendor_sites.terms_id,
               pay_group_lookup_code = ir_po_vendor_sites.pay_group_lookup_code,
               address_line1         = ir_po_vendor_sites.address_line1,
               address_line2         = ir_po_vendor_sites.address_line2,
               address_line3         = ir_po_vendor_sites.address_line3,
               address_line4         = ir_po_vendor_sites.address_line4,
               city                  = ir_po_vendor_sites.city,
               zip                   = ir_po_vendor_sites.zip,
               province              = ir_po_vendor_sites.province,
               attribute1            = ir_po_vendor_sites.attribute1,
               attribute2            = ir_po_vendor_sites.attribute2,
               attribute3            = ir_po_vendor_sites.attribute3,
               attribute4            = ir_po_vendor_sites.attribute4
               /* Add by Tong 19/09/2007*/,
               vendor_site_code_alt = ir_po_vendor_sites.vendor_site_code_alt --Shop Name
              ,
               attribute12          = ir_po_vendor_sites.attribute12 --Mail to Address1
              ,
               attribute13          = ir_po_vendor_sites.attribute13 --Mail to Address2
              ,
               attribute14          = ir_po_vendor_sites.attribute14 --Mail to Address3
              ,
               attribute15          = ir_po_vendor_sites.attribute15 --Owner Name
              ,
               vat_registration_num = ir_po_vendor_sites.vat_registration_num,
               global_attribute20   = ir_po_vendor_sites.global_attribute20,
               global_attribute6    = ir_po_vendor_sites.global_attribute6,
               last_update_date     = sysdate,
               last_updated_by      = fnd_global.user_id,
               last_update_login    = fnd_global.login_id
               -- Added by AP@BAS on 12-Dec-2013
              ,
               global_attribute14 = ir_po_vendor_sites.global_attribute14
         where vendor_site_id = v_psb_vender_site_id;
      
      exception
        when no_data_found then
          o_status := true;
          write_log('update_po_vendor_sites.update_psb not found vendor site code ' ||
                    v_psb_vender_site_code_mapped);
        when others then
          o_status := true;
          write_log('update_po_vendor_sites.update_psb error ');
      end;
    end if;
    -- remark by AP@BAS on 06-Feb-2013 and move to first row of procedure
    -- o_status := false;
  exception
    when others then
      o_status := true;
      write_log('Can not update vendor site!');
  end update_po_vendor_sites;

  ---------------------------------------------------------------------------------
  procedure update_po_vendor_contacts(ir_po_vendor_contacts in po_vendor_contacts%rowtype,
                                      o_status              out boolean) is
  
    v_dtn_vendor_contact_id number;
    v_psb_vendor_contact_id number;
  
    get_info_err exception;
  begin
    -- added by AP@BAS on 06-Feb-2013
    o_status := false;
    --
    update po_vendor_contacts
       set first_name        = ir_po_vendor_contacts.first_name,
           last_name         = ir_po_vendor_contacts.last_name,
           area_code         = ir_po_vendor_contacts.area_code,
           phone             = ir_po_vendor_contacts.phone,
           email_address     = ir_po_vendor_contacts.email_address,
           fax               = ir_po_vendor_contacts.fax,
           last_update_date  = sysdate,
           last_updated_by   = fnd_global.user_id,
           last_update_login = fnd_global.login_id
     where vendor_contact_id = ir_po_vendor_contacts.vendor_contact_id;
  
    -- remark by AP@BAS on -6-Feb-2013 and move to first row of procedure
    -- o_status := false;
  
    if (g_default_group in ('DMS', 'STS')) then
      -- Added by AP@BAS on 06-Feb-2013 : find infomation for update DTN, PSB
      for rec in (select pv.vendor_id,
                         pvs.vendor_site_id,
                         pvc.vendor_contact_id,
                         pvc.first_name,
                         pvc.last_name,
                         tac_supplier_interface_util.get_map_supplier_stie_code(pvs.vendor_site_code,
                                                                                'DTN') dtn_vendor_site_code,
                         tac_supplier_interface_util.get_map_supplier_stie_code(pvs.vendor_site_code,
                                                                                'PSB') psb_vendor_site_code
                    from po_vendors          pv,
                         po_vendor_sites_all pvs,
                         po_vendor_contacts  pvc
                   where pv.vendor_id = pvs.vendor_id
                     and pvs.vendor_site_id = pvc.vendor_site_id
                     and pvs.org_id = g_curr_org_id
                     and pvc.vendor_contact_id =
                         ir_po_vendor_contacts.vendor_contact_id) loop
        -- find vendor_contact_id of DTN
        begin
          select vendor_contact_id
            into v_dtn_vendor_contact_id
            from po_vendors          pv,
                 po_vendor_sites_all pvs,
                 po_vendor_contacts  pvc
           where pv.vendor_id = pvs.vendor_id
             and pvs.vendor_site_id = pvc.vendor_site_id
                
             and pv.vendor_id = rec.vendor_id
             and pvs.vendor_site_code = rec.dtn_vendor_site_code --:dtn_vendor_site_code, :psb_vendor_site_code
             and pvs.org_id = g_dtn_org_id -- :g_dtn_org_id, :g_psb_org_id
             and pvc.first_name = rec.first_name
             and pvc.last_name = rec.last_name;
        
        exception
          when no_data_found then
            o_status := true;
            write_log('update_po_vendor_contacts.vendor_contact_id of DTN data not found.');
            raise get_info_err;
          when others then
            o_status := true;
            write_log('update_po_vendor_contacts.vendor_contact_id of DTN error.');
            raise get_info_err;
        end;
      
        -- find vendor_contact_id of PSB
        begin
          select vendor_contact_id
            into v_psb_vendor_contact_id
            from po_vendors          pv,
                 po_vendor_sites_all pvs,
                 po_vendor_contacts  pvc
           where pv.vendor_id = pvs.vendor_id
             and pvs.vendor_site_id = pvc.vendor_site_id
                
             and pv.vendor_id = rec.vendor_id
             and pvs.vendor_site_code = rec.psb_vendor_site_code --:dtn_vendor_site_code, :psb_vendor_site_code
             and pvs.org_id = g_psb_org_id -- :g_dtn_org_id, :g_psb_org_id
             and pvc.first_name = rec.first_name
             and pvc.last_name = rec.last_name;
        
        exception
          when no_data_found then
            o_status := true;
            write_log('update_po_vendor_contacts.vendor_contact_id of PSB data not found.');
            raise get_info_err;
          when others then
            o_status := true;
            write_log('update_po_vendor_contacts.vendor_contact_id of PSB error.');
            raise get_info_err;
        end;
      
        -- update contact information of DTN
        update po_vendor_contacts
           set first_name        = ir_po_vendor_contacts.first_name,
               last_name         = ir_po_vendor_contacts.last_name,
               area_code         = ir_po_vendor_contacts.area_code,
               phone             = ir_po_vendor_contacts.phone,
               email_address     = ir_po_vendor_contacts.email_address,
               fax               = ir_po_vendor_contacts.fax,
               last_update_date  = sysdate,
               last_updated_by   = fnd_global.user_id,
               last_update_login = fnd_global.login_id
         where vendor_contact_id = v_dtn_vendor_contact_id;
      
        -- update contact information of PSB
        update po_vendor_contacts
           set first_name        = ir_po_vendor_contacts.first_name,
               last_name         = ir_po_vendor_contacts.last_name,
               area_code         = ir_po_vendor_contacts.area_code,
               phone             = ir_po_vendor_contacts.phone,
               email_address     = ir_po_vendor_contacts.email_address,
               fax               = ir_po_vendor_contacts.fax,
               last_update_date  = sysdate,
               last_updated_by   = fnd_global.user_id,
               last_update_login = fnd_global.login_id
         where vendor_contact_id = v_psb_vendor_contact_id;
        exit;
      end loop;
      --
    end if;
  exception
    when others then
      --ROLLBACK;
      o_status := true;
      write_log('Can not update vendor contact!');
  end update_po_vendor_contacts;
  ------------------------------------------------------------------------------
  procedure update_status_temp(i_default_group varchar2,
                               i_vendor_number varchar2,
                               i_vendor_name   varchar2,
                               i_request_id    number,
                               i_status        varchar2) is
    pragma autonomous_transaction;
  begin
    update dtac_ap_suppliers_temp_int t
       set status = i_status
     where t.request_id = i_request_id
       and t.default_group = i_default_group
       and nvl(t.vendor_number, '###') =
           nvl(i_vendor_number, nvl(t.vendor_number, '###'))
       and t.vendor_name = i_vendor_name;
    --    AND        UPPER(t.vendor_name)    =    UPPER(i_vendor_name);
    write_log('Update supplier status Complete');
    commit;
  exception
    when others then
      rollback;
      write_log('Can not update supplier status!');
  end update_status_temp;
  ------------------------------------------------------------------------------
  procedure update_supplier_cont_interface(err_msg  out varchar2,
                                           err_code out varchar2) is
    cursor get_sup_int is
      select *
        from ap_suppliers_int asi
       where asi.import_request_id is not null;
  
    cursor get_sup_site_int(c_vendor_intf_id number) is
      select *
        from ap_supplier_sites_int assi
       where assi.vendor_interface_id = c_vendor_intf_id;
  
    v_vendor_site_id number;
    --    APUPSCINTF
    --    TAC Update Supplier Contact Interface
  begin
    write_log('Update Supplier Site Contacts');
    for rec in get_sup_int loop
      for rec_site in get_sup_site_int(rec.vendor_interface_id) loop
        /*FOR rec_vendor IN get_vendor_site_id(rec_site.vendor_id, rec_site.vendor_site_code) LOOP
            v_vendor_site_id    :=    rec_vendor.vendor_site_id;
        END LOOP;    */
        begin
          select pvs.vendor_site_id
            into v_vendor_site_id
            from po_vendor_sites_all pvs
           where pvs.vendor_id = rec_site.vendor_id
             and vendor_site_code = rec_site.vendor_site_code
             and rownum = 1;
        exception
          when others then
            v_vendor_site_id := null;
        end;
      
        if v_vendor_site_id is not null then
          begin
            update ap_sup_site_contact_int assc
               set assc.vendor_site_id = v_vendor_site_id
             where assc.contact_name_alt = rec_site.vendor_interface_id;
            commit;
            --v_error    :=    'No';
            write_log('Update Supplier Site Contacts Complete');
            err_msg := 'Update Complete ';
          exception
            when others then
              --v_error    :=    'Yes';
              write_log('Cannot Update vendor_site_id in Table AP_SUP_SITE_CONTACT_INT : Vendor ID = ' ||
                        rec_site.vendor_id || ' , Vendor Site ID = ' ||
                        rec_site.vendor_site_code);
              err_msg := 'Cannot Update vendor_site_id in Table AP_SUP_SITE_CONTACT_INT';
              rollback;
          end;
        else
          --v_error    :=    'Yes';
          write_log('Cannot Update vendor_site_id in Table AP_SUP_SITE_CONTACT_INT : No Data Found vendor_site_id in Table PO_VENDOR_SITES_ALL');
          err_msg := '*** No Data Found : vendor_site_id in Table PO_VENDOR_SITES_ALL';
        end if;
      end loop;
    end loop;
  
    /*    IF v_error = 'No' THEN
        write_log('Update Supplier Site Contacts Complete');
        ERR_MSG    :=    'Update Complete ';
    END IF;*/
  
  exception
    when no_data_found then
      --v_error    :=    'Yes';
      write_log('Cannot Update Supplier Site Contacts Because Not Found Data');
      err_msg := '*** No Data Found';
    
  end update_supplier_cont_interface;
  ---------------------------------------------------------------------------------
  procedure update_sts(err_msg out varchar2, err_code out varchar2) is
    cursor get_sup is
      select asi.segment1, asi.vendor_interface_id, dast.vendor_site_code
        from dtac_ap_suppliers_temp_int dast, ap_suppliers_int asi
       where dast.vendor_number is null
         and dast.vendor_name = asi.vendor_name
         and dast.vendor_name_alt = asi.vendor_name_alt;
  
    --TAC Update Suppliers To STS
  begin
    for rec_sup in get_sup loop
      begin
        update dtac_ap_suppliers_to_sts s
           set s.supplier_number    = rec_sup.segment1,
               s.eai_stts           = null,
               s.eai_last_chng_dttm = null
         where s.seqn = rec_sup.vendor_interface_id;
        write_log('*-------- Suppliers for STS Log Report --------*');
        write_log('Supplier Number : ' || rec_sup.segment1 ||
                  ' ; Supplier Site Code : ' || rec_sup.vendor_site_code);
        commit;
      exception
        when others then
          rollback;
          write_log('Can not update supplier sts status!');
      end;
    end loop;
    commit;
  end update_sts;
  ------------------------------------------------------------------------------
  function validate_supplier(i_supplier_name  in varchar2,
                             i_tax_regist_num in varchar2,
                             i_supplier_type  in varchar2,
                             i_term_name      in varchar2,
                             i_pay_group      in varchar2,
                             i_country        in varchar2,
                             i_type           in varchar2,
                             i_def_grp        in varchar2,
                             i_req_id         in number) return boolean is
  
    cursor c_chk_dup_name(c_vendor_name varchar2) is
      select 'x' from po_vendors t where t.vendor_name = c_vendor_name;
  
    cursor c_chk_dup_vat(c_vat_num varchar2) is
      select 'x'
        from po_vendors s
       where s.vat_registration_num = c_vat_num;
  
    v_sup_int           atab;
    i                   number;
    v_vendor_name_exist varchar2(1);
    v_vat_num_exist     varchar2(1);
    v_error             varchar2(1) := 'N';
    v_supplier_type     varchar2(80);
    v_payment_term      varchar2(50);
    v_pay_group         varchar2(25);
    v_country           varchar2(25);
    v_msg               varchar2(1000);
  
  begin
    --FOR rec_supplier IN    c_get_supplier LOOP
    ----------------------------------
    begin
      open c_sup_int(i_req_id, i_def_grp);
      fetch c_sup_int bulk collect
        into v_sup_int;
      close c_sup_int;
    end;
  
    for i in v_sup_int.first .. v_sup_int.last loop
      --DBMS_OUTPUT.PUT_LINE(v_sup_int(i).vendor_name );
      if v_sup_int(i).vendor_sts_int = 'NEW' then
        begin
          select distinct 'Error : Source TAX Registration Number : ' ||
                          dasti.vat_registration_num ||
                          ' Duplicate in Source Table of Source Supplier Name : ' ||
                          g_sup_int(i).vendor_name
            into v_sup_int(i).err_msg
            from dtac_ap_suppliers_temp_int dasti
           where dasti.rowid = v_sup_int(i).row_id
             and dasti.vat_registration_num in
                 (select dat.vat_registration_num
                    from dtac_ap_suppliers_temp_int dat
                   group by dat.vat_registration_num
                  having count(distinct dat.vendor_name) > 1);
          --v_sup_int(i).err_msg := v_msg ;
        exception
          when others then
            v_sup_int(i).err_msg := null;
        end;
      
        begin
          select distinct 'Exists in ERP Supplier',
                          pv.segment1,
                          pv.vendor_name,
                          pv.vat_registration_num
            into v_msg,
                 v_sup_int(i).erp_vendor_number,
                 v_sup_int(i).erp_vendor_name,
                 v_sup_int(i).erp_vat_registration_num
            from po_vendors pv
           where pv.vat_registration_num = v_sup_int(i).vat_registration_num;
          v_sup_int(i).err_msg := v_sup_int(i).err_msg || ' ' ||
                                  'Error : Source TAX Registration Number : ' ||
                                  v_sup_int(i).vat_registration_num || ' ' || v_msg;
        exception
          when others then
            v_sup_int(i).err_msg := null;
        end;
      
        begin
          select distinct 'Already exists in ERP supplier',
                          pv.segment1,
                          pv.vendor_name,
                          pv.vat_registration_num
            into v_msg,
                 v_sup_int(i).erp_vendor_number,
                 v_sup_int(i).erp_vendor_name,
                 v_sup_int(i).erp_vat_registration_num
            from po_vendors pv
           where pv.vendor_name = v_sup_int(i).vendor_name;
          v_sup_int(i).err_msg := v_sup_int(i).err_msg || ' ' ||
                                  ' Error : Source Supplier Name  : ' ||
                                  v_sup_int(i).vendor_name || ' ' || v_msg;
        exception
          when others then
            v_msg := null;
        end;
      
      end if; -- for NEW Supplier
    
      --- Supplier Type
      if v_sup_int(i).vendor_type_lookup_code is not null then
        begin
          select flv.meaning
            into v_supplier_type
            from fnd_lookup_values flv
           where flv.lookup_type = 'VENDOR TYPE'
             and flv.lookup_code = v_sup_int(i).vendor_type_lookup_code;
        exception
          when no_data_found then
            v_sup_int(i).err_msg := v_sup_int(i).err_msg || ' ' ||
                                    ' Error : Source Supplier Type : ' ||
                                    v_sup_int(i).vendor_type_lookup_code || ' Not Found';
        end;
      end if;
    
      if v_sup_int(i).terms_name is not null then
        begin
          select at.name
            into v_payment_term
            from ap_terms at
           where at.name = v_sup_int(i).terms_name;
        exception
          when no_data_found then
            v_sup_int(i).err_msg := v_sup_int(i).err_msg || ' ' ||
                                    'Error : Payment Term : ' ||
                                    v_sup_int(i).terms_name || ' Not Found';
        end;
        v_msg := null;
      end if;
    
      if v_sup_int(i).pay_group_lookup_code is not null then
        begin
          select flv.meaning
            into v_pay_group
            from fnd_lookup_values flv
           where flv.lookup_type = 'PAY GROUP'
             and flv.lookup_code = v_sup_int(i).pay_group_lookup_code;
        exception
          when no_data_found then
            v_sup_int(i).err_msg := v_sup_int(i)
                                   .err_msg || ' ' ||
                                    'Error : Pay group : ' || v_sup_int(i)
                                   .pay_group_lookup_code || ' Not Found';
        end;
        v_msg := null;
      end if;
    
      if v_sup_int(i).country is not null then
        begin
          select ft.territory_short_name
            into v_country
            from fnd_territories_vl ft
           where ft.territory_code = v_sup_int(i).country;
        exception
          when no_data_found then
            v_sup_int(i).err_msg := v_sup_int(i)
                                   .err_msg || ' ' || 'Error : Country : ' ||
                                    v_sup_int(i).country || ' Not Found';
        end;
      end if;
    
    end loop;
    ------------------------------------
  
    --fnd_File.put_line(fnd_file.output,' ---- ');
    --fnd_File.put_line(fnd_file.output,'Retailer ');
    for i in v_sup_int.first .. v_sup_int.last loop
    
      if v_sup_int(i).err_msg is not null then
      
        fnd_file.put_line(fnd_file.output,
                          v_sup_int(i).attribute1 || ' ' || v_sup_int(i)
                          .err_msg);
        update dtac_ap_suppliers_temp_int
           set status     = 'REJECTED',
               attribute6 = 'BBB', --v_sup_int(i).ERP_VENDOR_NUMBER,
               attribute5 = 'AAA', --v_sup_int(i).ERP_VENDOR_NAME,
               attribute7 = 'CCC' --v_sup_int(i).ERP_VAT_REGISTRATION_NUM
         where rowid = v_sup_int(i).row_id;
      
      end if;
    
    end loop;
  
    commit;
  
    return true;
  
  end validate_supplier;
  ---------------------------------------------------------------------------------
  procedure update_po(ir_supplier in atab, i_row in number) is
    v_vendor_id         number;
    v_vendor_site_id    number;
    r_po_vendors        po_vendors%rowtype;
    r_po_vendor_sites   po_vendor_sites_all%rowtype;
    r_po_vendor_contact po_vendor_contacts%rowtype;
    v_status            boolean;
  begin
  
    v_vendor_id := ir_supplier(i_row).vendor_id;
    if v_vendor_id is not null then
      --AND v_vendor_site_id IS NOT NULL THEN
      write_log('Update Supplier : ' || ir_supplier(i_row).vendor_name);
      r_po_vendors.vendor_id            := v_vendor_id;
      r_po_vendors.vat_registration_num := ir_supplier(i_row)
                                          .vat_registration_num;
      r_po_vendors.global_attribute20   := ir_supplier(i_row)
                                          .global_attribute20;
      r_po_vendors.global_attribute6    := ir_supplier(i_row).default_group;
      r_po_vendors.attribute2           := ir_supplier(i_row).attribute2;
      r_po_vendors.attribute3           := ir_supplier(i_row).attribute3;
      r_po_vendors.attribute4           := ir_supplier(i_row).attribute4;
      r_po_vendors.last_update_date     := sysdate;
    
      update_po_vendors(ir_po_vendors => r_po_vendors,
                        o_status      => v_status);
      if v_status then
        return;
      end if;
    
      write_log('Update Supplier site : ' || ir_supplier(i_row)
                .vendor_name);
      r_po_vendor_sites.vendor_id          := v_vendor_id;
      r_po_vendor_sites.global_attribute20 := ir_supplier(i_row)
                                             .global_attribute20;
      r_po_vendor_sites.global_attribute6  := ir_supplier(i_row)
                                             .default_group;
      r_po_vendor_sites.attribute1         := ir_supplier(i_row).attribute1;
      r_po_vendor_sites.address_line1      := ir_supplier(i_row)
                                             .address_line1;
      r_po_vendor_sites.address_line2      := ir_supplier(i_row)
                                             .address_line2;
      r_po_vendor_sites.address_line3      := ir_supplier(i_row)
                                             .address_line3;
      r_po_vendor_sites.address_line4      := ir_supplier(i_row)
                                             .address_line4;
      r_po_vendor_sites.city               := ir_supplier(i_row).city;
      r_po_vendor_sites.zip                := ir_supplier(i_row).zip;
      r_po_vendor_sites.province           := ir_supplier(i_row).province;
      r_po_vendor_sites.last_update_date   := sysdate;
    
      -- added by AP@BAS on 06-Feb-2013
      r_po_vendor_sites.vendor_site_code := ir_supplier(i_row)
                                           .vendor_site_code;
      r_po_vendor_sites.vendor_site_id   := ir_supplier(i_row)
                                           .vendor_site_id;
      --
    
      -- added by AP@BAS on 14-Oct-2013
      r_po_vendor_sites.email_address := ir_supplier(i_row).email_address;
      --
    
      update_po_vendor_sites(ir_po_vendor_sites => r_po_vendor_sites,
                             o_status           => v_status);
      if v_status then
        return;
      end if;
    
      begin
        select s.vendor_site_id
          into v_vendor_site_id
          from po_vendor_sites_all s
         where s.vendor_site_code = ir_supplier(i_row).vendor_site_code
           and rownum = 1;
      exception
        when no_data_found then
          v_vendor_site_id := null;
          write_log('No data Found : Vendor Site Code : ' ||
                    ir_supplier(i_row).vendor_site_code);
      end;
      if v_vendor_site_id is not null then
        write_log('Update Supplier site Contact : ' || ir_supplier(i_row)
                  .vendor_name);
        r_po_vendor_contact.vendor_site_id   := v_vendor_site_id;
        r_po_vendor_contact.first_name       := ir_supplier(i_row)
                                               .first_name;
        r_po_vendor_contact.last_name        := ir_supplier(i_row)
                                               .last_name;
        r_po_vendor_contact.area_code        := ir_supplier(i_row)
                                               .area_code;
        r_po_vendor_contact.phone            := ir_supplier(i_row).phone;
        r_po_vendor_contact.email_address    := ir_supplier(i_row)
                                               .email_address;
        r_po_vendor_contact.fax              := ir_supplier(i_row).fax;
        r_po_vendor_contact.last_update_date := sysdate;
      
        update_po_vendor_contacts(ir_po_vendor_contacts => r_po_vendor_contact,
                                  o_status              => v_status);
      
        if v_status then
          return;
        end if;
      
      end if;
    end if;
  end update_po;
  ---------------------------------------------------------------------------------
  procedure supplier_interface(err_msg          out varchar2,
                               err_code         out varchar2,
                               i_default_group  in varchar2,
                               i_set_of_book_id in varchar2,
                               i_org_id         in varchar2) is
    cursor c_get_supplier_default is
      select dasid.*
        from dtac_suppliers_int_default_v dasid
       where dasid.default_group = i_default_group;
  
    cursor c_chk(req_id in number) is
      select h.*
        from ap_supplier_sites_int h
       where h.import_request_id = req_id;
  
    cursor c_chk_contact(req_id in number) is
      select h.*
        from ap_sup_site_contact_int h
       where h.import_request_id = req_id;
  
    r_ap_suppliers_int    ap_suppliers_int%rowtype;
    r_ap_sup_site         ap_supplier_sites_int%rowtype;
    r_ap_sup_site_cont    ap_sup_site_contact_int%rowtype;
    r_ap_supplier_default dtac_suppliers_int_default_v%rowtype;
    r_ap_sup_to_sts       dtac_ap_suppliers_to_sts%rowtype;
    r_dtac_ap_sup_output  dtac_ap_suppliers_output%rowtype;
    r_po_vendors          po_vendors%rowtype;
    r_po_vendor_sites     po_vendor_sites_all%rowtype;
    r_po_vendor_contact   po_vendor_contacts%rowtype;
  
    v_status      boolean;
    v_error       boolean;
    v_sup_req_id  number;
    v_site_req_id number;
    v_cont_req_id number;
    v_request_id  number;
  
    v_flag      boolean;
    v_idx       number;
    v_hdr_grp   varchar2(300) := '';
    v_hdr_proc  varchar2(1) := 'Y';
    v_chk_dup   dtac_ap_suppliers_temp_int%rowtype;
    v_row_count number;
    i           number;
    v_max_seqn  number;
    v_dup_flag  varchar2(1);
    v_upd_flag  varchar2(1);
  
    v_chk number;
    v_err varchar2(3000);
    v     number;
  
  begin
    -- added by AP@BAS on 05-Feb-2012
    g_curr_org_id   := i_org_id;
    g_default_group := i_default_group;
  
    open c_get_supplier_default;
    fetch c_get_supplier_default
      into r_ap_supplier_default;
    close c_get_supplier_default;
  
    if r_ap_supplier_default.default_group is null then
      write_log('No Data Found : Default Group : ' || i_default_group);
      err_msg := '*** No Data Found : Table DTAC_SUPPLIERS_INT_DEFAULT';
    end if;
  
    v_request_id := fnd_global.conc_request_id;
    fnd_file.put_line(fnd_file.output, ' ---- ');
    fnd_file.put_line(fnd_file.output, 'Retailer ');
  
    ----- Validation Summary
    begin
      write_log('validate_supplier_summary Start Time : ' ||
                to_char(sysdate, 'dd-Mon-yyyy hh24:mi:ss'));
      write_log('GROUP :' || i_default_group || ' REQUEST ID :' ||
                v_request_id || ' ORG :' || i_org_id);
      validate_supplier_summary(i_def_grp => i_default_group,
                                i_req_id  => v_request_id,
                                i_org_id  => i_org_id);
      write_log('validate_supplier_summary Finished Time : ' ||
                to_char(sysdate, 'dd-Mon-yyyy hh24:mi:ss'));
    end;
    write_log('Create Supplier Start Time : ' ||
              to_char(sysdate, 'dd-Mon-yyyy hh24:mi:ss'));
    ----- For Supplier -----
    v_upd_flag := 'N';
    for i in 1 .. g_sup_int.count loop
      --Comment by pta 16/11/06 g_sup_int.FIRST..g_sup_int.LAST LOOP
      -- Bug Data 15-sep-2006
      r_ap_suppliers_int := null;
      if g_sup_int(i).status = 'PROCESSED' then
        v_dup_flag := 'N';
        if i > 1 then
          if g_sup_int(i).vat_registration_num = g_sup_int(i - 1)
          .vat_registration_num then
            v_dup_flag := 'Y';
          end if;
        end if;
      
        ---------------------------------------------------------------------------
      
        if g_sup_int(i).vendor_id is null then
          -- New
          if v_dup_flag = 'Y' then
            g_sup_int(i).vendor_interface_id := g_sup_int(i - 1)
                                               .vendor_interface_id;
            g_sup_int(i).sup_int_flag := g_sup_int(i - 1).sup_int_flag;
          else
            write_log('Insert Supplier : ' || g_sup_int(i)
                      .vendor_name || ' Status =>  ' || g_sup_int(i).status);
          
            select ap_suppliers_int_s.nextval
              into g_sup_int(i).vendor_interface_id
              from dual;
          
            v_idx := t_id.count + 1;
            r_ap_suppliers_int.vendor_interface_id := g_sup_int(i)
                                                     .vendor_interface_id;
            t_id(v_idx).vendor_interface_id := r_ap_suppliers_int.vendor_interface_id;
          
            --Assign value from temp to ap_suppliers_int
            r_ap_suppliers_int.vendor_name               := g_sup_int(i)
                                                           .vendor_name; --UPPER(g_sup_int(i).vendor_name);
            r_ap_suppliers_int.vendor_name_alt           := g_sup_int(i)
                                                           .vendor_name_alt; --UPPER(g_sup_int(i).vendor_name_alt);
            r_ap_suppliers_int.segment1                  := '';
            r_ap_suppliers_int.vendor_type_lookup_code   := g_sup_int(i)
                                                           .vendor_type_lookup_code;
            r_ap_suppliers_int.set_of_books_id           := i_set_of_book_id;
            r_ap_suppliers_int.request_id                := v_request_id;
            r_ap_suppliers_int.vat_registration_num      := g_sup_int(i)
                                                           .vat_registration_num;
            r_ap_suppliers_int.allow_awt_flag            := g_sup_int(i)
                                                           .allow_awt_flag;
            r_ap_suppliers_int.global_attribute_category := 'JA.TH.APXVDMVD.PO_VENDORS';
            r_ap_suppliers_int.global_attribute18        := g_sup_int(i)
                                                           .vat_registration_num;
            r_ap_suppliers_int.global_attribute6         := g_sup_int(i)
                                                           .default_group;
            r_ap_suppliers_int.global_attribute20        := g_sup_int(i)
                                                           .global_attribute20;
            r_ap_suppliers_int.attribute2                := g_sup_int(i)
                                                           .attribute2;
            r_ap_suppliers_int.attribute3                := g_sup_int(i)
                                                           .attribute3;
            r_ap_suppliers_int.attribute4                := g_sup_int(i)
                                                           .attribute4;
            r_ap_suppliers_int.status                    := 'NEW';
          
            -- Global Attribute
          
            --if null get from default
            r_ap_suppliers_int.terms_name            := nvl(g_sup_int(i)
                                                            .terms_name,
                                                            r_ap_supplier_default.terms_name);
            r_ap_suppliers_int.pay_group_lookup_code := nvl(g_sup_int(i)
                                                            .pay_group_lookup_code,
                                                            r_ap_supplier_default.pay_group_lookup_code);
          
            r_ap_suppliers_int.payment_priority           := nvl(g_sup_int(i)
                                                                 .payment_priority,
                                                                 r_ap_supplier_default.payment_priority);
            r_ap_suppliers_int.terms_date_basis           := nvl(g_sup_int(i)
                                                                 .terms_date_basis,
                                                                 r_ap_supplier_default.terms_date_basis);
            r_ap_suppliers_int.pay_date_basis_lookup_code := nvl(g_sup_int(i)
                                                                 .pay_date_basis_lookup_code,
                                                                 r_ap_supplier_default.pay_date_basis_lookup_code);
            r_ap_suppliers_int.payment_method_lookup_code := nvl(g_sup_int(i)
                                                                 .payment_method_lookup_code,
                                                                 r_ap_supplier_default.payment_method_lookup_code);
            r_ap_suppliers_int.invoice_currency_code      := nvl(g_sup_int(i)
                                                                 .invoice_currency_code,
                                                                 r_ap_supplier_default.invoice_currency_code);
          
            /*-- Update by Nu, 13/12/2006 --*/
            r_ap_suppliers_int.attribute_category := g_citibank;
            r_ap_suppliers_int.attribute5         := r_ap_supplier_default.attribute5;
            r_ap_suppliers_int.attribute6         := r_ap_supplier_default.attribute6;
            r_ap_suppliers_int.attribute7         := r_ap_supplier_default.attribute7;
            r_ap_suppliers_int.attribute8         := r_ap_supplier_default.attribute8;
            r_ap_suppliers_int.attribute9         := r_ap_supplier_default.attribute9;
            r_ap_suppliers_int.attribute10        := r_ap_supplier_default.attribute10;
            r_ap_suppliers_int.attribute11        := r_ap_supplier_default.attribute11;
          
            insert_ap_suppliers_int(ir_ap_suppliers_int => r_ap_suppliers_int);
          
            --END IF ; -- v_hdr_proc
            g_sup_int(i).status := 'PROCESSED';
            g_sup_int(i).sup_int_flag := 'INTERFACED';
            g_sup_int(i).interface_mode := 'INSERT';
            g_sup_int(i).err_msg := 'INTERFACE COMPLETED,  ';
          end if;
        else
          -- Old Supplier
        
          if v_dup_flag = 'Y' then
            g_sup_int(i).vendor_interface_id := g_sup_int(i - 1)
                                               .vendor_interface_id;
            g_sup_int(i).sup_int_flag := g_sup_int(i - 1).sup_int_flag;
          else
            write_log('Update Supplier : ' || g_sup_int(i)
                      .vendor_name || ' Status =>  ' || g_sup_int(i).status);
          
            select ap_suppliers_int_s.nextval
              into g_sup_int(i).vendor_interface_id
              from dual;
          
            r_po_vendors           := null;
            r_po_vendors.vendor_id := g_sup_int(i).vendor_id;
          
            if g_sup_int(i).vat_registration_num is not null then
              r_po_vendors.vat_registration_num := g_sup_int(i)
                                                  .vat_registration_num;
            else
              begin
                select vat_registration_num
                  into r_po_vendors.vat_registration_num
                  from po_vendors
                 where vendor_name = g_sup_int(i).vendor_name;
              exception
                when others then
                  fnd_file.put_line(fnd_file.log,
                                    'Vat Registration Num of Vendor Name : ' ||
                                     g_sup_int(i)
                                    .vendor_name || ' Not Found in Vendor.');
              end;
            end if;
          
            if g_sup_int(i).pay_group_lookup_code is not null then
              r_po_vendors.pay_group_lookup_code := g_sup_int(i)
                                                   .pay_group_lookup_code;
            else
              begin
                select pay_group_lookup_code
                  into r_po_vendors.pay_group_lookup_code
                  from po_vendors
                 where vendor_id = g_sup_int(i).vendor_id;
              exception
                when others then
                  fnd_file.put_line(fnd_file.log,
                                    'Pay Group Lookup Code of Vendor Name : ' ||
                                     g_sup_int(i)
                                    .vendor_name || ' Not Found in Vendor.');
              end;
            
            end if;
          
            if g_sup_int(i).terms_name is not null then
            
              begin
                select term_id
                  into r_po_vendors.terms_id
                  from ap_terms
                 where upper(name) = upper(g_sup_int(i).terms_name);
              exception
                when others then
                  fnd_file.put_line(fnd_file.log,
                                    'Terms Name of Vendor Name : ' ||
                                     g_sup_int(i)
                                    .vendor_name || ' Not Found in Vendor.');
              end;
            
            else
              begin
                select terms_id
                  into r_po_vendors.terms_id
                  from po_vendors
                 where vendor_id = g_sup_int(i).vendor_id;
              exception
                when others then
                  fnd_file.put_line(fnd_file.log,
                                    'Terms Name of Vendor Name : ' ||
                                     g_sup_int(i)
                                    .vendor_name || ' Not Found in Vendor.');
              end;
            
            end if;
          
            r_po_vendors.global_attribute18 := g_sup_int(i)
                                              .vat_registration_num;
            r_po_vendors.global_attribute6  := g_sup_int(i).default_group;
            r_po_vendors.global_attribute20 := g_sup_int(i)
                                              .global_attribute20;
            r_po_vendors.attribute2         := g_sup_int(i).attribute2;
            r_po_vendors.attribute3         := g_sup_int(i).attribute3;
            r_po_vendors.attribute4         := g_sup_int(i).attribute4;
            -- Add by Tong 19/09/2007
            r_po_vendors.attribute12      := g_sup_int(i).mail_to_address1;
            r_po_vendors.attribute13      := g_sup_int(i).mail_to_address2;
            r_po_vendors.attribute14      := g_sup_int(i).mail_to_address3;
            r_po_vendors.attribute15      := g_sup_int(i).owner_name;
            r_po_vendors.last_update_date := sysdate;
          
            update_po_vendors(ir_po_vendors => r_po_vendors,
                              o_status      => v_status);
            g_sup_int(i).status := 'PROCESSED';
            g_sup_int(i).sup_int_flag := 'UPDATED';
            v_upd_flag := 'Y';
            g_sup_int(i).interface_mode := 'UPDATE';
            g_sup_int(i).err_msg := 'INTERFACE COMPLETED,  ';
          end if;
        end if; -- NEW Supplier
        --End If Can not Interface
        g_sup_int(i).err_msg := ' ';
      
      end if;
    end loop; -- Supplier Interface
  
    write_log('Create Supplier Finished Time : ' ||
              to_char(sysdate, 'dd-Mon-yyyy hh24:mi:ss'));
  
    v_row_count := 0;
    begin
      commit;
      begin
        select count('x')
          into v_row_count
          from ap_suppliers_int
         where request_id = v_request_id;
      end;
    
      if nvl(v_row_count, 0) > 0 then
        --Supplier Open Interface Import
      
        v_sup_req_id := fnd_request.submit_request(application => 'SQLAP',
                                                   program     => 'APXSUIMP',
                                                   argument1   => 'NEW', -- 1000, N, N, N,
                                                   argument2   => '1000', -- x_org_ref,
                                                   argument3   => 'N',
                                                   argument4   => 'N',
                                                   argument5   => 'N');
      end if;
    
      ------------
      if v_sup_req_id > 0 or v_upd_flag = 'Y' then
        if v_sup_req_id > 0 then
          conc_wait(v_sup_req_id);
        end if;
        -------------
        for i in 1 .. g_sup_int.count loop
          --Comment by pta 16/11/06 g_sup_int.FIRST..g_sup_int.LAST Loop
          --fnd_file.put_line(fnd_file.log,'VENDOR NAME : '||g_sup_int(i).vendor_name ||' Status => '||g_sup_int(i).status ) ;
        
          if g_sup_int(i).status = 'PROCESSED' and g_sup_int(i)
          .sup_int_flag = 'INTERFACED' and g_sup_int(i).vendor_id is null then
            begin
              select segment1, vendor_id
                into g_sup_int(i).vendor_number, g_sup_int(i).vendor_id
                from po_vendors asi
               where asi.vendor_name = g_sup_int(i).vendor_name;
            
              if g_sup_int(i).vendor_number is null then
                g_sup_int(i).status := 'REJECTED';
                g_sup_int(i).err_msg := ' Error : Sup Interface ';
              else
                begin
                  update po_vendors
                     set vat_code = null
                   where vendor_id = g_sup_int(i).vendor_id;
                  --v := 1;
                exception
                  when others then
                    g_sup_int(i).status := 'ERROR';
                    g_sup_int(i).err_msg := g_sup_int(i).err_msg;
                    fnd_file.put_line(fnd_file.log,
                                      'Error : Can not Update Vat Code  ' ||
                                       'Vendor Name : ' || g_sup_int(i)
                                      .vendor_name);
                end;
              end if;
            
            exception
              when others then
                g_sup_int(i).status := 'ERROR';
                g_sup_int(i).err_msg := g_sup_int(i).err_msg;
                fnd_file.put_line(fnd_file.log,
                                  'Error : ' ||
                                  'Vat Registration Number : ' ||
                                  g_sup_int(i).vat_registration_num ||
                                  ' Not Found in Vendor. #1');
                write_log('Error : ' || sqlerrm);
            end;
          
          elsif g_sup_int(i).vendor_number is null and g_sup_int(i)
          .vendor_id is not null then
            --- Get Max Sequence
            --FND_FILE.put_line(fnd_file.log,'Vendor ID '||TO_CHAR(g_sup_int(i).vendor_id) ) ;
            begin
              select segment1
                into g_sup_int(i).vendor_number
                from po_vendors asi
               where asi.vendor_name = g_sup_int(i).vendor_name;
            exception
              when others then
                g_sup_int(i).status := 'ERROR';
                g_sup_int(i).err_msg := g_sup_int(i).err_msg;
                fnd_file.put_line(fnd_file.log,
                                  'Error : ' ||
                                   'Vendor Number of Vendor Name : ' ||
                                   g_sup_int(i)
                                  .vendor_name || ' Not Found in Vendor. #2');
            end;
          
            v_max_seqn := 0;
            begin
              select max(seqn)
                into v_max_seqn
                from dtac_ap_suppliers_to_sts;
            end;
            ------
            -- Begin Modify By CCC Boy 28-MAY-2008
            if g_sup_int(i).source_file_name is null then
              begin
                insert into dtac_ap_suppliers_to_sts
                  (seqn,
                   retailer_code,
                   supplier_number,
                   supplier_site_code,
                   creation_date,
                   eai_stts,
                   eai_last_chng_dttm,
                   default_group)
                values
                  (nvl(v_max_seqn, 0) + 1,
                   g_sup_int(i).attribute1,
                   g_sup_int(i).erp_vendor_number,
                   g_sup_int(i).vendor_site_code,
                   sysdate,
                   null,
                   null,
                   g_sup_int(i).default_group);
              end;
            end if;
            -- End Modify By CCC Boy 28-MAY-2008
          
          end if;
        
        --   fnd_file.put_line(fnd_file.log,'Sts : '||g_sup_int(i).status ) ;
        end loop; -- Loop Supplier
      
        begin
          select max(seqn) into v_max_seqn from dtac_ap_suppliers_to_sts;
        end;
      
        -- BEGIN Modify By CCC Boy 28-MAY-2008
      
        begin
          insert into dtac_ap_suppliers_to_sts
            (seqn,
             retailer_code,
             supplier_number,
             supplier_site_code,
             creation_date,
             eai_stts,
             eai_last_chng_dttm,
             default_group)
            (select rownum + nvl(v_max_seqn, 0) seqn,
                    dasti.attribute1 retailer_code,
                    asi.segment1 supplier_number,
                    dasti.vendor_site_code supplier_site_code,
                    sysdate creation_date,
                    null eai_stts,
                    null eai_last_chng_dttm,
                    i_default_group
               from ap_suppliers_int asi, dtac_ap_suppliers_temp_int dasti
              where nvl(asi.vat_registration_num, '999') =
                    nvl(dasti.vat_registration_num, '999')
                and dasti.request_id = v_request_id
                and asi.request_id = v_request_id
                and dasti.vendor_number is null
                and asi.status = 'PROCESSED'
                and dasti.source_file_name is null);
        end;
      
        -- END Modify By CCC Boy 28-MAY-2008
      
      end if;
      --End If ;
    end;
  
    ---  Submit Concurrent
  
    -- START Modify HERE [AP@BAS]
    ---- Supplier Site ---
    v_upd_flag := 'N';
    for i in 1 .. g_sup_int.count loop
      --Comment by pta 16/11/06 g_sup_int.FIRST..g_sup_int.LAST LOOP
      r_ap_sup_site := null;
      if g_sup_int(i).status = 'PROCESSED' and g_sup_int(i)
      .vendor_id is not null then
        if g_sup_int(i).vendor_site_id is null then
          write_log('Insert Supplier site : (' || g_sup_int(i)
                    .vendor_number || ') ' || g_sup_int(i)
                    .vendor_name || '   ' || g_sup_int(i).vendor_site_code);
        
          --Assign value from temp to ap_supplier_site
          r_ap_sup_site.vendor_interface_id        := g_sup_int(i)
                                                     .vendor_interface_id;
          r_ap_sup_site.vendor_id                  := g_sup_int(i)
                                                     .vendor_id;
          r_ap_sup_site.request_id                 := v_request_id;
          r_ap_sup_site.vendor_site_code           := g_sup_int(i)
                                                     .vendor_site_code;
          r_ap_sup_site.address_line1              := g_sup_int(i)
                                                     .address_line1;
          r_ap_sup_site.address_line2              := g_sup_int(i)
                                                     .address_line2;
          r_ap_sup_site.address_line3              := g_sup_int(i)
                                                     .address_line3;
          r_ap_sup_site.address_line4              := g_sup_int(i)
                                                     .address_line4;
          r_ap_sup_site.city                       := g_sup_int(i).city;
          r_ap_sup_site.zip                        := g_sup_int(i).zip;
          r_ap_sup_site.province                   := g_sup_int(i).province;
          r_ap_sup_site.country                    := g_sup_int(i).country;
          r_ap_sup_site.payment_method_lookup_code := g_sup_int(i)
                                                     .payment_method_lookup_code;
          r_ap_sup_site.terms_date_basis           := g_sup_int(i)
                                                     .site_terms_date_basis;
          r_ap_sup_site.payment_priority           := g_sup_int(i)
                                                     .site_payment_priority;
        
          r_ap_sup_site.terms_name            := nvl(g_sup_int(i)
                                                     .site_terms_name,
                                                     r_ap_supplier_default.terms_name);
          r_ap_sup_site.pay_group_lookup_code := nvl(g_sup_int(i)
                                                     .site_pay_group_lookup_code,
                                                     r_ap_supplier_default.pay_group_lookup_code);
        
          r_ap_sup_site.pay_date_basis_lookup_code := g_sup_int(i)
                                                     .pay_date_basis_lookup_code;
          r_ap_sup_site.invoice_currency_code      := g_sup_int(i)
                                                     .site_invoice_currency_code;
          r_ap_sup_site.attribute1                 := g_sup_int(i)
                                                     .attribute1;
          r_ap_sup_site.attribute2                 := g_sup_int(i)
                                                     .site_attribute2;
          r_ap_sup_site.attribute3                 := g_sup_int(i)
                                                     .site_attribute3;
          r_ap_sup_site.attribute4                 := g_sup_int(i)
                                                     .site_attribute4;
          r_ap_sup_site.org_id                     := i_org_id;
          r_ap_sup_site.allow_awt_flag             := g_sup_int(i)
                                                     .allow_awt_flag;
          r_ap_sup_site.global_attribute_category  := 'JA.TH.APXVDMVD.SITES';
          r_ap_sup_site.global_attribute18         := g_sup_int(i)
                                                     .vat_registration_num;
          r_ap_sup_site.global_attribute20         := g_sup_int(i)
                                                     .global_attribute20;
          r_ap_sup_site.global_attribute6          := g_sup_int(i)
                                                     .default_group; --09/04/2008 Naj
          r_ap_sup_site.status                     := 'NEW';
        
          -- Added by AP@BAS on 12-Dec-2013
          r_ap_sup_site.global_attribute14 := nvl(g_sup_int(i).branch_code,
                                                  '�ӹѡ�ҹ�˭�');
          --
        
          --if null get from default
          r_ap_sup_site.pay_site_flag                 := nvl(g_sup_int(i)
                                                             .pay_site_flag,
                                                             r_ap_supplier_default.pay_site_flag);
          r_ap_sup_site.purchasing_site_flag          := nvl(g_sup_int(i)
                                                             .purchasing_site_flag,
                                                             r_ap_supplier_default.purchase_site_flag);
          r_ap_sup_site.accts_pay_code_combination_id := nvl(r_ap_supplier_default.accts_pay_ccid,
                                                             r_ap_supplier_default.accts_pay_ccid);
          r_ap_sup_site.prepay_code_combination_id    := nvl(r_ap_supplier_default.prepay_ccid,
                                                             r_ap_supplier_default.prepay_ccid);
          r_ap_sup_site.future_dated_payment_ccid     := nvl(r_ap_supplier_default.future_dated_payment_ccid,
                                                             r_ap_supplier_default.future_dated_payment_ccid);
          -- end if null get from default
          r_ap_sup_site.vendor_site_code_alt := g_sup_int(i).shop_name;
        
          if g_sup_int(i).attribute_category is null then
            g_sup_int(i).attribute_category := g_citibank;
          end if;
        
          r_ap_sup_site.attribute_category := nvl(g_sup_int(i)
                                                  .attribute_category,
                                                  g_citibank);
          if g_sup_int(i).attribute_category = g_citibank then
            --'CITIBANK'
            r_ap_sup_site.attribute5  := nvl(g_sup_int(i).attribute5,
                                             r_ap_supplier_default.attribute5 /*''*/);
            r_ap_sup_site.attribute6  := nvl(g_sup_int(i).attribute6,
                                             r_ap_supplier_default.attribute6 /*''*/);
            r_ap_sup_site.attribute7  := nvl(g_sup_int(i).attribute7,
                                             r_ap_supplier_default.attribute7 /*''*/);
            r_ap_sup_site.attribute8  := nvl(g_sup_int(i).attribute8,
                                             r_ap_supplier_default.attribute8 /*''*/);
            r_ap_sup_site.attribute9  := nvl(g_sup_int(i).attribute9,
                                             r_ap_supplier_default.attribute9 /*''*/);
            r_ap_sup_site.attribute10 := nvl(g_sup_int(i).attribute10,
                                             r_ap_supplier_default.attribute10 /*''*/);
            r_ap_sup_site.attribute11 := nvl(g_sup_int(i).attribute11,
                                             r_ap_supplier_default.attribute11 /*''*/);
            r_ap_sup_site.attribute12 := g_sup_int(i).mail_to_address1; --- Add by Tong 19/09/2007
            r_ap_sup_site.attribute13 := g_sup_int(i).mail_to_address2; --- Add by Tong 19/09/2007
            r_ap_sup_site.attribute14 := g_sup_int(i).mail_to_address3; --- Add by Tong 19/09/2007
            r_ap_sup_site.attribute15 := g_sup_int(i).owner_name; --- Add by Tong 19/09/2007
          
          elsif g_sup_int(i).attribute_category = g_kbank then
            --'KBANK'
            r_ap_sup_site.attribute5  := nvl(g_sup_int(i).vendor_site_code,
                                             '');
            r_ap_sup_site.attribute6  := nvl(g_sup_int(i).address_line1, '');
            r_ap_sup_site.attribute7  := nvl(g_sup_int(i).address_line2, '');
            r_ap_sup_site.attribute8  := nvl(g_sup_int(i).address_line3, '');
            r_ap_sup_site.attribute9  := nvl(g_sup_int(i).address_line4, '');
            r_ap_sup_site.attribute10 := nvl(g_sup_int(i).city, '');
            --r_ap_suppliers_int.attribute11    :=    attribute11;
          end if;
        
          -- Added by AP@BAS on 14-Oct-2013 : assign email address value
          r_ap_sup_site.email_address := g_sup_int(i).email_address;
        
          -- Modify by AP@BAS on 06-Feb-2013 : see detail in procedure insert_ap_sup_site
          insert_ap_sup_site(ir_ap_sup_site => r_ap_sup_site);
          --
          g_sup_int(i).sup_site_int_flag := 'INTERFACED';
          g_sup_int(i).interface_mode := 'INSERT';
          --g_sup_int(i).err_msg             := ' INTERFACE COMPLETED,  ' ;
        else
          -- Old Site
        
          r_po_vendor_sites                    := null;
          r_po_vendor_sites.vendor_id          := g_sup_int(i).vendor_id;
          r_po_vendor_sites.vendor_site_id     := g_sup_int(i)
                                                 .vendor_site_id;
          r_po_vendor_sites.global_attribute18 := g_sup_int(i)
                                                 .vat_registration_num;
          r_po_vendor_sites.global_attribute20 := g_sup_int(i)
                                                 .global_attribute20;
          r_po_vendor_sites.global_attribute6  := g_sup_int(i)
                                                 .default_group; --09/04/2008 Naj
        
          r_po_vendor_sites.attribute1 := g_sup_int(i).attribute1;
          r_po_vendor_sites.attribute2 := g_sup_int(i).site_attribute2;
          r_po_vendor_sites.attribute3 := g_sup_int(i).site_attribute3;
          r_po_vendor_sites.attribute4 := g_sup_int(i).site_attribute4;
          /* Add by Tong 19/09/2007  */
          r_po_vendor_sites.vendor_site_code_alt := g_sup_int(i).shop_name;
          r_po_vendor_sites.attribute12          := g_sup_int(i)
                                                   .mail_to_address1;
          r_po_vendor_sites.attribute13          := g_sup_int(i)
                                                   .mail_to_address2;
          r_po_vendor_sites.attribute14          := g_sup_int(i)
                                                   .mail_to_address3;
          r_po_vendor_sites.attribute15          := g_sup_int(i).owner_name;
          r_po_vendor_sites.vat_registration_num := g_sup_int(i)
                                                   .vat_registration_num;
        
          r_po_vendor_sites.address_line1    := g_sup_int(i).address_line1;
          r_po_vendor_sites.address_line2    := g_sup_int(i).address_line2;
          r_po_vendor_sites.address_line3    := g_sup_int(i).address_line3;
          r_po_vendor_sites.address_line4    := g_sup_int(i).address_line4;
          r_po_vendor_sites.city             := g_sup_int(i).city;
          r_po_vendor_sites.zip              := g_sup_int(i).zip;
          r_po_vendor_sites.province         := g_sup_int(i).province;
          r_po_vendor_sites.last_update_date := sysdate;
        
          -- Added by AP@BAS on 14-Oct-2013 : assign email address value
          r_po_vendor_sites.email_address := g_sup_int(i).email_address;
          -- Added by AP@BAS on 12-Dec-2013 : assign branch code
          r_po_vendor_sites.global_attribute14 := nvl(g_sup_int(i)
                                                      .branch_code,
                                                      '�ӹѡ�ҹ�˭�');
          --
          if g_sup_int(i).site_pay_group_lookup_code is not null then
            r_po_vendor_sites.pay_group_lookup_code := g_sup_int(i)
                                                      .site_pay_group_lookup_code;
          else
            begin
              select pay_group_lookup_code
                into r_po_vendor_sites.pay_group_lookup_code
                from po_vendor_sites_all
               where vendor_site_id = g_sup_int(i).vendor_site_id;
            exception
              when others then
                fnd_file.put_line(fnd_file.log,
                                  'Pay Group Lookup Code of Vendor Site Name : ' ||
                                  g_sup_int(i).vendor_site_code ||
                                  ' Not Found in Vendor.');
            end;
          end if;
        
          if g_sup_int(i).site_terms_name is not null then
            begin
              select term_id
                into r_po_vendor_sites.terms_id
                from ap_terms
               where upper(name) = upper(g_sup_int(i).site_terms_name);
            exception
              when others then
                fnd_file.put_line(fnd_file.log,
                                  'Terms Name of Vendor Site Name : ' ||
                                  g_sup_int(i).vendor_site_code ||
                                  ' Not Found in Vendor.');
            end;
          
          else
            begin
              select terms_id
                into r_po_vendor_sites.terms_id
                from po_vendor_sites_all
               where vendor_site_id = g_sup_int(i).vendor_site_id;
            exception
              when others then
                fnd_file.put_line(fnd_file.log,
                                  'Terms Name of Vendor Site Name : ' ||
                                  g_sup_int(i).vendor_site_code ||
                                  ' Not Found in Vendor.');
            end;
          
          end if;
        
          write_log('Update Supplier site : (' || g_sup_int(i)
                    .vendor_number || ') ' || g_sup_int(i)
                    .vendor_name || '   ' || g_sup_int(i).vendor_site_code);
        
          -- Added by AP@BAS on 06-Feb-2013
          r_po_vendor_sites.vendor_site_code := g_sup_int(i)
                                               .vendor_site_code;
          -- Modify by AP@BAS on 06-Feb-2013 : see detail in procedure update_po_vendor_sites
          update_po_vendor_sites(ir_po_vendor_sites => r_po_vendor_sites,
                                 o_status           => v_status);
          --
        
          g_sup_int(i).sup_site_int_flag := 'UPDATED';
          v_upd_flag := 'Y';
          g_sup_int(i).interface_mode := 'UPDATE';
        
          ------------- Supplier Site
        
        end if; -- Site New
      
        g_sup_int(i).err_msg := ' ';
      
      end if; -- Processed
    
    end loop; -- -Supplier Site
  
    v_row_count := 0;
    begin
      begin
        select count('x')
          into v_row_count
          from ap_supplier_sites_int
         where request_id = v_request_id;
      end;
    end;
    if v_row_count > 0 then
      commit;
      --Supplier Sites Open Interface Import
      v_site_req_id := fnd_request.submit_request(application => 'SQLAP',
                                                  program     => 'APXSSIMP',
                                                  argument1   => 'NEW', -- 1000, N, N, N,
                                                  argument2   => '1000', -- x_org_ref,
                                                  argument3   => 'N',
                                                  argument4   => 'N',
                                                  argument5   => 'N');
    end if; ------------
  
    if v_site_req_id > 0 or v_upd_flag = 'Y' then
      if v_site_req_id > 0 then
        conc_wait(v_site_req_id);
      end if;
      for i in 1 .. g_sup_int.count loop
        -- Comment by pta 16/11/06 g_sup_int.FIRST..g_sup_int.LAST Loop
        if g_sup_int(i).status = 'PROCESSED' and g_sup_int(i)
        .sup_site_int_flag = 'INTERFACED' and g_sup_int(i)
        .vendor_id is not null and g_sup_int(i).vendor_site_id is null then
          begin
            select vendor_site_id
              into g_sup_int(i).vendor_site_id
              from po_vendor_sites pvs
             where 1 = 1 --pvs.vendor_interface_id = g_sup_int(i).vendor_interface_id
               and pvs.vendor_id = g_sup_int(i)
            .vendor_id
               and pvs.vendor_site_code = g_sup_int(i)
            .vendor_site_code;
          
            if g_sup_int(i).vendor_site_id is null then
              g_sup_int(i).status := 'REJECTED';
              g_sup_int(i).err_msg := ' Error : Sup Site Interface ';
            else
              begin
                --fnd_file.put_line(fnd_file.log,'TEST : ' || g_sup_int(i).vendor_id || '#######' || g_sup_int(i).vendor_site_id );
                update po_vendor_sites_all
                   set vat_code = null
                 where vendor_id = g_sup_int(i)
                .vendor_id
                   and vendor_site_id = g_sup_int(i).vendor_site_id;
                --v := 1;
              exception
                when others then
                  g_sup_int(i).status := 'ERROR';
                  g_sup_int(i).err_msg := g_sup_int(i).err_msg;
                  fnd_file.put_line(fnd_file.log,
                                    'Error : Can not Update Vat Code Vendor Site ID : ' ||
                                    g_sup_int(i).vendor_site_id);
              end;
            end if;
          
          exception
            when others then
              g_sup_int(i).status := 'ERROR';
              g_sup_int(i).err_msg := 'Error : Sup Site Interface ';
              fnd_file.put_line(fnd_file.log,
                                'Error : ' || 'Vat Registration Number : ' ||
                                g_sup_int(i).vat_registration_num ||
                                ' Not Found in Vendor.');
          end;
        end if;
      end loop;
    end if;
    commit;
  
    ----------------------  site contact
    for i in 1 .. g_sup_int.count loop
      -- Comment by pta 16/11/06 g_sup_int.First..g_sup_int.LAST LOOP
      r_ap_sup_site_cont := null;
      if g_sup_int(i).status = 'PROCESSED' then
      
        if g_sup_int(i).vendor_site_id is not null then
          if g_sup_int(i).vendor_site_cont_id is null then
            -- NEW
            if g_sup_int(i).last_name is not null then
              write_log('Insert Supplier site contact : (' || g_sup_int(i)
                        .vendor_number || ') ' || g_sup_int(i)
                        .vendor_name || '  ' || g_sup_int(i)
                        .first_name || ' ' || g_sup_int(i).last_name);
              --Assign value from temp to ap_supplier_site_contact
              r_ap_sup_site_cont.vendor_site_id := g_sup_int(i)
                                                  .vendor_site_id;
            
              -- Modified by AP@BAS on 06-Feb-2013 : assign vender site code for use in mapping , but not insert into interface table
              r_ap_sup_site_cont.vendor_site_code := g_sup_int(i)
                                                    .vendor_site_code;
              --rap_sup_site_cont.vendor_site_code := '';
              --
            
              r_ap_sup_site_cont.org_id           := i_org_id;
              r_ap_sup_site_cont.first_name       := g_sup_int(i)
                                                    .first_name;
              r_ap_sup_site_cont.last_name        := g_sup_int(i).last_name;
              r_ap_sup_site_cont.area_code        := g_sup_int(i).area_code;
              r_ap_sup_site_cont.phone            := g_sup_int(i).phone;
              r_ap_sup_site_cont.status           := 'NEW';
              r_ap_sup_site_cont.email_address    := g_sup_int(i)
                                                    .email_address;
              r_ap_sup_site_cont.fax              := g_sup_int(i).fax;
              r_ap_sup_site_cont.contact_name_alt := r_ap_suppliers_int.vendor_interface_id;
              r_ap_sup_site_cont.request_id       := g_sup_int(i)
                                                    .request_id;
            
              -- Modify by AP@BAS on 06-Feb-2013 : see detail in procedure insert_ap_sup_site_cont
              insert_ap_sup_site_cont(ir_ap_sup_site_cont => r_ap_sup_site_cont);
              --
            
              g_sup_int(i).sup_site_cont_int_flag := 'INTERFACED';
              g_sup_int(i).interface_mode := 'INSERT';
              g_sup_int(i).err_msg := ' INTERFACE COMPLETED,  ';
            end if; --check last name
          else
            -- Supplier Site Contact Old
            r_po_vendor_contact := null;
            write_log('Update Supplier site Contact : ' || g_sup_int(i)
                      .vendor_name);
            r_po_vendor_contact.vendor_site_id    := g_sup_int(i)
                                                    .vendor_site_id;
            r_po_vendor_contact.vendor_contact_id := g_sup_int(i)
                                                    .vendor_site_cont_id;
            r_po_vendor_contact.first_name        := g_sup_int(i)
                                                    .first_name;
            r_po_vendor_contact.last_name         := g_sup_int(i).last_name;
            r_po_vendor_contact.area_code         := g_sup_int(i).area_code;
            r_po_vendor_contact.phone             := g_sup_int(i).phone;
            r_po_vendor_contact.email_address     := g_sup_int(i)
                                                    .email_address;
            r_po_vendor_contact.fax               := g_sup_int(i).fax;
            r_po_vendor_contact.last_update_date  := sysdate;
          
            -- Added by AP@BAS on 06-Feb-2013 : for user mappint DTN, PSB
            r_ap_sup_site_cont.vendor_site_code := g_sup_int(i)
                                                  .vendor_site_code;
            --
          
            -- Modify by AP@BAS on 06-Feb-2013 : see detail in procedure insert_ap_sup_site_cont
            update_po_vendor_contacts(ir_po_vendor_contacts => r_po_vendor_contact,
                                      o_status              => v_status);
          
            g_sup_int(i).sup_site_cont_int_flag := 'UPDATED';
            g_sup_int(i).interface_mode := 'UPDATE';
            g_sup_int(i).err_msg := ' INTERFACE COMPLETED,  ';
          
          end if; -- Vendor Site Contact
        end if; -- Vendor Site
      
        g_sup_int(i).err_msg := ' INTERFACE COMPLETED,  ';
      
      end if; -- Process
    
    end loop;
    ---------------- Supplier Interface ----------------------
  
    --- Supplier Contact ----
    v_row_count := 0;
    begin
      begin
        select count('x')
          into v_row_count
          from ap_sup_site_contact_int
         where request_id = v_request_id;
      end;
    end;
    if v_row_count > 0 then
      commit;
      --  Supplier Contacts Open Interface Import
      v_cont_req_id := fnd_request.submit_request(application => 'SQLAP',
                                                  program     => 'APXSCIMP',
                                                  argument1   => 'NEW', -- 1000, N, N, N,
                                                  argument2   => '1000', -- x_org_ref,
                                                  argument3   => 'N',
                                                  argument4   => 'N',
                                                  argument5   => 'N');
      ------------
      if v_cont_req_id > 0 then
        conc_wait(v_cont_req_id);
        for i in 1 .. g_sup_int.count loop
          -- Comment by pta 16/11/06 g_sup_int.FIRST..g_sup_int.LAST Loop
          if g_sup_int(i).status = 'PROCESSED' and g_sup_int(i)
          .sup_site_cont_int_flag = 'INTERFACED' and g_sup_int(i)
          .vendor_site_id is not null and g_sup_int(i)
          .vendor_site_cont_id is null then
          
            -- Modify by boy CCC 03/06/2008
            begin
              select vendor_contact_id
                into g_sup_int(i).vendor_site_cont_id
                from po_vendor_contacts pvc
               where 1 = 1 --pvs.vendor_interface_id = g_sup_int(i).vendor_interface_id
                 and pvc.vendor_site_id = g_sup_int(i)
              .vendor_site_id
                 and pvc.first_name = g_sup_int(i)
              .first_name
                 and pvc.last_name = g_sup_int(i).last_name
                 and rownum = 1;
              --AND   pvc.vendor_site_code    = g_sup_int(i).vendor_site_code ;
            
              if g_sup_int(i).vendor_site_cont_id is null then
                g_sup_int(i).status := 'REJECTED';
                g_sup_int(i).err_msg := 'Error : Supplier Site contact interface ';
                fnd_file.put_line(fnd_file.log,
                                  'Vendor Site ' ||
                                   to_char(g_sup_int(i).vendor_site_id) ||
                                   ' fname ' || g_sup_int(i)
                                  .first_name || ' lname ' || g_sup_int(i)
                                  .last_name);
              end if;
            exception
              when others then
                g_sup_int(i).status := 'REJECTED';
                g_sup_int(i).err_msg := 'Error : Supplier Site contact interface ';
            end;
            -- Modify by boy CCC 03/06/2008
          end if;
        end loop;
      end if; -- Contact
    end if;
    --- Supplier Contact ----
  
    --fnd_File.put_line(fnd_file.output,' ---- ');
    --fnd_File.put_line(fnd_file.output,'Retailer ');
  
    for i in 1 .. g_sup_int.count loop
      --Comment by pta 16/11/06 g_sup_int.FIRST..g_sup_int.LAST LOOP
    
      update dtac_ap_suppliers_temp_int
         set status = g_sup_int(i).status
       where rowid = g_sup_int(i).row_id;
    
      if g_sup_int(i).status != 'PROCESSED' then
        if g_sup_int(i).status != 'BANK VAD AT' then
          fnd_file.put_line(fnd_file.output, g_sup_int(i).err_msg);
          fnd_file.put_line(fnd_file.log, g_sup_int(i).err_msg);
        end if;
      end if;
      -----------------------------------------------------------------------
    
      r_dtac_ap_sup_output.interface_id         := g_sup_int(i)
                                                  .vendor_interface_id;
      r_dtac_ap_sup_output.interface_date       := sysdate;
      r_dtac_ap_sup_output.vendor_number        := g_sup_int(i)
                                                  .vendor_number;
      r_dtac_ap_sup_output.vendor_name          := g_sup_int(i).vendor_name;
      r_dtac_ap_sup_output.vat_registration_num := g_sup_int(i)
                                                  .vat_registration_num;
      r_dtac_ap_sup_output.retailer_code        := g_sup_int(i).attribute1;
      r_dtac_ap_sup_output.vendor_site_code     := g_sup_int(i)
                                                  .vendor_site_code;
      r_dtac_ap_sup_output.status               := g_sup_int(i).status;
      r_dtac_ap_sup_output.default_group        := g_sup_int(i)
                                                  .default_group;
    
      begin
        select pv.segment1             erp_vendor_number,
               pv.vendor_name          erp_vendor_name,
               pv.vat_registration_num erp_vat_registration_num
        
          into r_dtac_ap_sup_output.erp_vendor_number,
               r_dtac_ap_sup_output.erp_vendor_name,
               r_dtac_ap_sup_output.erp_vat_registration_num
        
          from po_vendors pv
        
         where (g_sup_int(i).vat_registration_num = pv.vat_registration_num or
                g_sup_int(i).vendor_name = pv.vendor_name);
      exception
        when others then
          r_dtac_ap_sup_output.erp_vendor_number        := '';
          r_dtac_ap_sup_output.erp_vendor_name          := '';
          r_dtac_ap_sup_output.erp_vat_registration_num := '';
      end;
    
      r_dtac_ap_sup_output.interface_mode   := g_sup_int(i).interface_mode;
      r_dtac_ap_sup_output.error_message    := g_sup_int(i).err_msg;
      r_dtac_ap_sup_output.attribute1       := '';
      r_dtac_ap_sup_output.attribute2       := '';
      r_dtac_ap_sup_output.attribute3       := '';
      r_dtac_ap_sup_output.attribute4       := '';
      r_dtac_ap_sup_output.attribute5       := '';
      r_dtac_ap_sup_output.attribute6       := '';
      r_dtac_ap_sup_output.attribute7       := '';
      r_dtac_ap_sup_output.attribute8       := '';
      r_dtac_ap_sup_output.attribute9       := '';
      r_dtac_ap_sup_output.attribute10      := '';
      r_dtac_ap_sup_output.eai_crtd_dttm    := g_sup_int(i).eai_crtd_dttm;
      r_dtac_ap_sup_output.source_file_name := g_sup_int(i)
                                              .source_file_name;
    
      insert_dtac_ap_sup_output(ir_dtac_ap_sup_output => r_dtac_ap_sup_output);
    
    end loop;
  
    commit;
    -------------------------------------------------------------
  
  end supplier_interface;
  ---------------------------------------------------------------------------------

  procedure conc_wait(param_req number) is
    v_result   varchar2(1);
    phase      varchar2(30);
    status     varchar2(30);
    dev_phase  varchar2(30);
    dev_status varchar2(30);
    message    varchar2(1000);
  begin
    commit;
  
    if fnd_concurrent.wait_for_request(param_req,
                                       10,
                                       0,
                                       phase,
                                       status,
                                       dev_phase,
                                       dev_status,
                                       message) then
      null;
    end if;
  end conc_wait;
  ----------------------------------------------

  /*
  ======================================================================
  Customize for migrate data from HO to DTN and PSB ou
  Migration
  ======================================================================
  */

  procedure insert_suppliers_int_migrate(ir_ap_suppliers_int in ap_suppliers_int%rowtype) is
  begin
  
    insert into ap_suppliers_int
      (vendor_interface_id,
       vendor_name,
       vendor_name_alt,
       segment1,
       vendor_type_lookup_code,
       terms_name,
       set_of_books_id,
       pay_date_basis_lookup_code,
       pay_group_lookup_code,
       payment_priority,
       invoice_currency_code,
       payment_method_lookup_code,
       terms_date_basis,
       vat_registration_num,
       allow_awt_flag,
       global_attribute_category,
       global_attribute18,
       global_attribute6,
       global_attribute20,
       attribute_category,
       attribute1,
       attribute2,
       attribute3,
       attribute4,
       attribute5,
       attribute6,
       attribute7,
       attribute8,
       attribute9,
       attribute10,
       attribute11,
       attribute12,
       attribute13,
       attribute14,
       attribute15,
       status,
       reject_code,
       request_id,
       creation_date,
       last_update_date,
       created_by,
       last_updated_by)
    values
      (ir_ap_suppliers_int.vendor_interface_id,
       ir_ap_suppliers_int.vendor_name,
       ir_ap_suppliers_int.vendor_name_alt,
       ir_ap_suppliers_int.segment1,
       ir_ap_suppliers_int.vendor_type_lookup_code,
       ir_ap_suppliers_int.terms_name,
       ir_ap_suppliers_int.set_of_books_id,
       ir_ap_suppliers_int.pay_date_basis_lookup_code,
       ir_ap_suppliers_int.pay_group_lookup_code,
       ir_ap_suppliers_int.payment_priority,
       ir_ap_suppliers_int.invoice_currency_code,
       ir_ap_suppliers_int.payment_method_lookup_code,
       ir_ap_suppliers_int.terms_date_basis,
       ir_ap_suppliers_int.vat_registration_num,
       ir_ap_suppliers_int.allow_awt_flag,
       ir_ap_suppliers_int.global_attribute_category,
       ir_ap_suppliers_int.global_attribute18,
       ir_ap_suppliers_int.global_attribute6,
       ir_ap_suppliers_int.global_attribute20,
       ir_ap_suppliers_int.attribute_category,
       ir_ap_suppliers_int.attribute1,
       ir_ap_suppliers_int.attribute2,
       ir_ap_suppliers_int.attribute3,
       ir_ap_suppliers_int.attribute4,
       ir_ap_suppliers_int.attribute5,
       ir_ap_suppliers_int.attribute6,
       ir_ap_suppliers_int.attribute7,
       ir_ap_suppliers_int.attribute8,
       ir_ap_suppliers_int.attribute9,
       ir_ap_suppliers_int.attribute10,
       ir_ap_suppliers_int.attribute11,
       ir_ap_suppliers_int.attribute12,
       ir_ap_suppliers_int.attribute13,
       ir_ap_suppliers_int.attribute14,
       ir_ap_suppliers_int.attribute15,
       ir_ap_suppliers_int.status,
       ir_ap_suppliers_int.reject_code,
       ir_ap_suppliers_int.request_id,
       ir_ap_suppliers_int.creation_date,
       ir_ap_suppliers_int.last_update_date,
       ir_ap_suppliers_int.created_by,
       ir_ap_suppliers_int.last_updated_by);
  end insert_suppliers_int_migrate;

  ---------------------------------------------------------------------------------
  procedure insert_ap_sup_site_migrate(ir_ap_sup_site in ap_supplier_sites_int%rowtype) is
  begin
  
    write_log('Insert supplier site interface for vendor id = ' ||
              ir_ap_sup_site.vendor_id || ', Global Attribute Category = ' ||
              ir_ap_sup_site.global_attribute_category ||
              ', Branch Code = ' || ir_ap_sup_site.global_attribute14);
  
    -- Added by AP@BAS on 07-Feb-2013 : dupplicate for DMS, STS
    if (g_default_group in ('DMS', 'STS')) then
      ---------------------------------------------------------------------------------
      -- Insert supplier site to interface table
      -- supplier site information duplicate from HO and change vendor_site_code, org_id
      -- to DTAC-N
      -- Author : AP@BAS
      -- Create : 04-Feb-2013
      ---------------------------------------------------------------------------------
      declare
        v_supplier_site_code_mapped varchar2(50);
        v_accts_pay_ccid            number;
        v_prepay_ccid               number;
      begin
        v_supplier_site_code_mapped := get_map_supplier_stie_code(ir_ap_sup_site.vendor_site_code,
                                                                  'DTN');
        v_accts_pay_ccid            := get_map_accts_pay_ccid(ir_ap_sup_site.accts_pay_code_combination_id,
                                                              'DTN');
        v_prepay_ccid               := get_map_prepay_ccid(ir_ap_sup_site.prepay_code_combination_id,
                                                           'DTN');
        insert into ap_supplier_sites_int
          (vendor_id,
           vendor_interface_id,
           vendor_site_code,
           purchasing_site_flag,
           pay_site_flag,
           address_line1,
           address_line2,
           address_line3,
           address_line4,
           city,
           zip,
           province,
           country,
           payment_method_lookup_code,
           terms_date_basis,
           distribution_set_name,
           accts_pay_code_combination_id,
           prepay_code_combination_id,
           pay_group_lookup_code,
           payment_priority,
           terms_name,
           pay_date_basis_lookup_code,
           invoice_currency_code,
           attribute1,
           attribute2,
           attribute3,
           attribute4,
           /* Add by Tong 19/09/2007*/
           vendor_site_code_alt, --Shop Name
           attribute12, --Mail to Address1
           attribute13, --Mail to Address2
           attribute14, --Mail to Address3
           attribute15, --Owner Name
           attribute5,
           attribute6,
           attribute7,
           attribute8,
           attribute9,
           attribute10,
           attribute_category,
           org_id,
           allow_awt_flag,
           global_attribute6,
           global_attribute20,
           future_dated_payment_ccid,
           status,
           reject_code,
           request_id,
           creation_date,
           last_update_date,
           created_by,
           last_updated_by
           -- Added by AP@BAS on 12-Dec-2013
          ,
           global_attribute_category,
           global_attribute14
           --
           )
        values
          (ir_ap_sup_site.vendor_id,
           ir_ap_sup_site.vendor_interface_id,
           v_supplier_site_code_mapped
           --,ir_ap_sup_site.vendor_site_code
          ,
           ir_ap_sup_site.purchasing_site_flag,
           ir_ap_sup_site.pay_site_flag,
           ir_ap_sup_site.address_line1,
           ir_ap_sup_site.address_line2,
           ir_ap_sup_site.address_line3,
           ir_ap_sup_site.address_line4,
           ir_ap_sup_site.city,
           ir_ap_sup_site.zip,
           ir_ap_sup_site.province,
           ir_ap_sup_site.country,
           ir_ap_sup_site.payment_method_lookup_code,
           ir_ap_sup_site.terms_date_basis,
           ir_ap_sup_site.distribution_set_name
           
          ,
           v_accts_pay_ccid,
           v_prepay_ccid
           --,ir_ap_sup_site.accts_pay_code_combination_id
           --,ir_ap_sup_site.prepay_code_combination_id
           
          ,
           ir_ap_sup_site.pay_group_lookup_code,
           ir_ap_sup_site.payment_priority,
           ir_ap_sup_site.terms_name,
           ir_ap_sup_site.pay_date_basis_lookup_code,
           ir_ap_sup_site.invoice_currency_code,
           ir_ap_sup_site.attribute1,
           ir_ap_sup_site.attribute2,
           ir_ap_sup_site.attribute3,
           ir_ap_sup_site.attribute4,
           /* Add by Tong 19/09/2007*/
           ir_ap_sup_site.vendor_site_code_alt, --Shop Name
           ir_ap_sup_site.attribute12, --Mail to Address1
           ir_ap_sup_site.attribute13, --Mail to Address2
           ir_ap_sup_site.attribute14, --Mail to Address3
           ir_ap_sup_site.attribute15, --Owner Name
           ir_ap_sup_site.attribute5,
           ir_ap_sup_site.attribute6,
           ir_ap_sup_site.attribute7,
           ir_ap_sup_site.attribute8,
           ir_ap_sup_site.attribute9,
           ir_ap_sup_site.attribute10,
           ir_ap_sup_site.attribute_category,
           g_dtn_org_id
           --,ir_ap_sup_site.org_id
          ,
           ir_ap_sup_site.allow_awt_flag,
           ir_ap_sup_site.global_attribute6,
           ir_ap_sup_site.global_attribute20,
           ir_ap_sup_site.future_dated_payment_ccid,
           ir_ap_sup_site.status,
           ir_ap_sup_site.reject_code,
           ir_ap_sup_site.request_id,
           ir_ap_sup_site.creation_date,
           ir_ap_sup_site.last_update_date,
           ir_ap_sup_site.created_by,
           ir_ap_sup_site.last_updated_by
           -- Added by AP@BAS on 12-Dec-2013
          ,
           ir_ap_sup_site.global_attribute_category,
           ir_ap_sup_site.global_attribute14);
      exception
        when others then
          write_log('Insert supplier site interface for DTAC-N error :');
          write_log(SQLCODE || ':' || SQLERRM);
      end;
    
      ---------------------------------------------------------------------------------
      -- Insert supplier site to interface table
      -- supplier site information duplicate from HO and change vendor_site_code, org_id
      -- to BAYSBUY
      -- Author : AP@BAS
      -- Create : 04-Feb-2013
      ---------------------------------------------------------------------------------
      declare
        v_supplier_site_code_mapped varchar2(50);
        v_accts_pay_ccid            number;
        v_prepay_ccid               number;
      begin
        v_supplier_site_code_mapped := get_map_supplier_stie_code(ir_ap_sup_site.vendor_site_code,
                                                                  'PSB');
        v_accts_pay_ccid            := get_map_accts_pay_ccid(ir_ap_sup_site.accts_pay_code_combination_id,
                                                              'PSB');
        v_prepay_ccid               := get_map_prepay_ccid(ir_ap_sup_site.prepay_code_combination_id,
                                                           'PSB');
        insert into ap_supplier_sites_int
          (vendor_id,
           vendor_interface_id,
           vendor_site_code,
           purchasing_site_flag,
           pay_site_flag,
           address_line1,
           address_line2,
           address_line3,
           address_line4,
           city,
           zip,
           province,
           country,
           payment_method_lookup_code,
           terms_date_basis,
           distribution_set_name,
           accts_pay_code_combination_id,
           prepay_code_combination_id,
           pay_group_lookup_code,
           payment_priority,
           terms_name,
           pay_date_basis_lookup_code,
           invoice_currency_code,
           attribute1,
           attribute2,
           attribute3,
           attribute4,
           /* Add by Tong 19/09/2007*/
           vendor_site_code_alt, --Shop Name
           attribute12, --Mail to Address1
           attribute13, --Mail to Address2
           attribute14, --Mail to Address3
           attribute15, --Owner Name
           attribute5,
           attribute6,
           attribute7,
           attribute8,
           attribute9,
           attribute10,
           attribute_category,
           org_id,
           allow_awt_flag,
           global_attribute6,
           global_attribute20,
           future_dated_payment_ccid,
           status,
           reject_code,
           request_id,
           creation_date,
           last_update_date,
           created_by,
           last_updated_by
           -- Added by AP@BAS on 12-Dec-2013
          ,
           global_attribute_category,
           global_attribute14)
        values
          (ir_ap_sup_site.vendor_id,
           ir_ap_sup_site.vendor_interface_id,
           v_supplier_site_code_mapped
           --,ir_ap_sup_site.vendor_site_code
          ,
           ir_ap_sup_site.purchasing_site_flag,
           ir_ap_sup_site.pay_site_flag,
           ir_ap_sup_site.address_line1,
           ir_ap_sup_site.address_line2,
           ir_ap_sup_site.address_line3,
           ir_ap_sup_site.address_line4,
           ir_ap_sup_site.city,
           ir_ap_sup_site.zip,
           ir_ap_sup_site.province,
           ir_ap_sup_site.country,
           ir_ap_sup_site.payment_method_lookup_code,
           ir_ap_sup_site.terms_date_basis,
           ir_ap_sup_site.distribution_set_name
           
          ,
           v_accts_pay_ccid,
           v_prepay_ccid
           --,ir_ap_sup_site.accts_pay_code_combination_id
           --,ir_ap_sup_site.prepay_code_combination_id
           
          ,
           ir_ap_sup_site.pay_group_lookup_code,
           ir_ap_sup_site.payment_priority,
           ir_ap_sup_site.terms_name,
           ir_ap_sup_site.pay_date_basis_lookup_code,
           ir_ap_sup_site.invoice_currency_code,
           ir_ap_sup_site.attribute1,
           ir_ap_sup_site.attribute2,
           ir_ap_sup_site.attribute3,
           ir_ap_sup_site.attribute4,
           /* Add by Tong 19/09/2007*/
           ir_ap_sup_site.vendor_site_code_alt, --Shop Name
           ir_ap_sup_site.attribute12, --Mail to Address1
           ir_ap_sup_site.attribute13, --Mail to Address2
           ir_ap_sup_site.attribute14, --Mail to Address3
           ir_ap_sup_site.attribute15, --Owner Name
           ir_ap_sup_site.attribute5,
           ir_ap_sup_site.attribute6,
           ir_ap_sup_site.attribute7,
           ir_ap_sup_site.attribute8,
           ir_ap_sup_site.attribute9,
           ir_ap_sup_site.attribute10,
           ir_ap_sup_site.attribute_category,
           g_psb_org_id
           --,ir_ap_sup_site.org_id
          ,
           ir_ap_sup_site.allow_awt_flag,
           ir_ap_sup_site.global_attribute6,
           ir_ap_sup_site.global_attribute20,
           ir_ap_sup_site.future_dated_payment_ccid,
           ir_ap_sup_site.status,
           ir_ap_sup_site.reject_code,
           ir_ap_sup_site.request_id,
           ir_ap_sup_site.creation_date,
           ir_ap_sup_site.last_update_date,
           ir_ap_sup_site.created_by,
           ir_ap_sup_site.last_updated_by
           -- Added by AP@BAS on 12-Dec-2013
          ,
           ir_ap_sup_site.global_attribute_category,
           ir_ap_sup_site.global_attribute14);
      exception
        when others then
          write_log('Insert supplier site interface for PAYSABUY error :');
          write_log(SQLCODE || ':' || SQLERRM);
      end;
    end if;
  end insert_ap_sup_site_migrate;

  procedure insert_sup_site_cont_migrate(ir_ap_sup_site_cont in ap_sup_site_contact_int%rowtype) is
    -- added by AP@BAS on 06-Feb-2013
    v_dtn_vender_site_code_mapped varchar2(50);
    v_dtn_vender_site_id          number;
    v_psb_vender_site_code_mapped varchar2(50);
    v_psb_vender_site_id          number;
    v_vendor_id                   number;
    --
  begin
    -- added by AP@BAS on 06-Feb-2013 : mapping vendor site code to DTN, PSB
    v_dtn_vender_site_code_mapped := get_map_supplier_stie_code(ir_ap_sup_site_cont.vendor_site_code,
                                                                'DTN');
    v_psb_vender_site_code_mapped := get_map_supplier_stie_code(ir_ap_sup_site_cont.vendor_site_code,
                                                                'PSB');
    --
  
    if (g_default_group in ('DMS', 'STS')) then
      -- Added by AP@BAS on 06-Feb-2013 : get vendor id
      begin
        select min(vendor_id)
          into v_vendor_id
          from po_vendor_sites_all
         where vendor_site_id = ir_ap_sup_site_cont.vendor_site_id;
      exception
        when others then
          null;
      end;
      -- Added by AP@BAS on 06-Feb-2013
      -- insert supplier site contact for DTN
      begin
        select vendor_site_id
          into v_dtn_vender_site_id
          from po_vendor_sites_all
         where org_id = g_dtn_org_id
           and vendor_id = v_vendor_id
           and vendor_site_code = v_dtn_vender_site_code_mapped;
      
        insert into ap_sup_site_contact_int
          (vendor_site_id,
           vendor_site_code,
           org_id,
           first_name,
           last_name,
           area_code,
           phone,
           status,
           email_address,
           fax,
           contact_name_alt,
           request_id,
           creation_date,
           last_update_date,
           created_by,
           last_updated_by)
        values
          (v_dtn_vender_site_id,
           '',
           g_dtn_org_id,
           ir_ap_sup_site_cont.first_name,
           ir_ap_sup_site_cont.last_name,
           ir_ap_sup_site_cont.area_code,
           ir_ap_sup_site_cont.phone,
           ir_ap_sup_site_cont.status,
           ir_ap_sup_site_cont.email_address,
           ir_ap_sup_site_cont.fax,
           ir_ap_sup_site_cont.contact_name_alt,
           ir_ap_sup_site_cont.request_id,
           ir_ap_sup_site_cont.creation_date,
           ir_ap_sup_site_cont.last_update_date,
           ir_ap_sup_site_cont.created_by,
           ir_ap_sup_site_cont.last_updated_by);
      exception
        when no_data_found then
          write_log('tac_supplier_interface_util.insert_dtn_contact not found vendor site code ' ||
                    v_dtn_vender_site_code_mapped);
        when others then
          write_log('tac_supplier_interface_util.insert_dtn_contact error ');
      end;
    
      -- Added by AP@BAS on 06-Feb-2013
      -- insert supplier site contact for PSB
      begin
        select vendor_site_id
          into v_psb_vender_site_id
          from po_vendor_sites_all
         where org_id = g_psb_org_id
           and vendor_id = v_vendor_id
           and vendor_site_code = v_psb_vender_site_code_mapped;
      
        insert into ap_sup_site_contact_int
          (vendor_site_id,
           vendor_site_code,
           org_id,
           first_name,
           last_name,
           area_code,
           phone,
           status,
           email_address,
           fax,
           contact_name_alt,
           request_id,
           creation_date,
           last_update_date,
           created_by,
           last_updated_by)
        values
          (v_psb_vender_site_id,
           '',
           g_psb_org_id,
           ir_ap_sup_site_cont.first_name,
           ir_ap_sup_site_cont.last_name,
           ir_ap_sup_site_cont.area_code,
           ir_ap_sup_site_cont.phone,
           ir_ap_sup_site_cont.status,
           ir_ap_sup_site_cont.email_address,
           ir_ap_sup_site_cont.fax,
           ir_ap_sup_site_cont.contact_name_alt,
           ir_ap_sup_site_cont.request_id,
           ir_ap_sup_site_cont.creation_date,
           ir_ap_sup_site_cont.last_update_date,
           ir_ap_sup_site_cont.created_by,
           ir_ap_sup_site_cont.last_updated_by);
      exception
        when no_data_found then
          write_log('tac_supplier_interface_util.insert_psb_contact not found vendor site code ' ||
                    v_dtn_vender_site_code_mapped);
        when others then
          write_log('tac_supplier_interface_util.insert_psb_contact error ');
      end;
    end if;
  
  exception
    when others then
      write_log('tac_supplier_interface_util.insert_ap_sup_site_cont exception.');
  end insert_sup_site_cont_migrate;

  procedure supplier_interface_migration(err_msg          out varchar2,
                                         err_code         out varchar2,
                                         i_default_group  in varchar2,
                                         i_set_of_book_id in varchar2,
                                         i_org_id         in varchar2) is
    cursor c_get_supplier_default is
      select dasid.*
        from dtac_suppliers_int_default_v dasid
       where dasid.default_group = i_default_group;
  
    cursor c_chk(req_id in number) is
      select h.*
        from ap_supplier_sites_int h
       where h.import_request_id = req_id;
  
    cursor c_chk_contact(req_id in number) is
      select h.*
        from ap_sup_site_contact_int h
       where h.import_request_id = req_id;
  
    r_ap_suppliers_int    ap_suppliers_int%rowtype;
    r_ap_sup_site         ap_supplier_sites_int%rowtype;
    r_ap_sup_site_cont    ap_sup_site_contact_int%rowtype;
    r_ap_supplier_default dtac_suppliers_int_default_v%rowtype;
    r_ap_sup_to_sts       dtac_ap_suppliers_to_sts%rowtype;
    r_dtac_ap_sup_output  dtac_ap_suppliers_output%rowtype;
    r_po_vendors          po_vendors%rowtype;
    r_po_vendor_sites     po_vendor_sites_all%rowtype;
    r_po_vendor_contact   po_vendor_contacts%rowtype;
  
    v_status      boolean;
    v_error       boolean;
    v_sup_req_id  number;
    v_site_req_id number;
    v_cont_req_id number;
    v_request_id  number;
  
    v_flag      boolean;
    v_idx       number;
    v_hdr_grp   varchar2(300) := '';
    v_hdr_proc  varchar2(1) := 'Y';
    v_chk_dup   dtac_ap_suppliers_temp_int%rowtype;
    v_row_count number;
    i           number;
    v_max_seqn  number;
    v_dup_flag  varchar2(1);
    v_upd_flag  varchar2(1);
  
    v_chk number;
    v_err varchar2(3000);
    v     number;
  
  begin
    -- added by AP@BAS on 05-Feb-2012
    g_curr_org_id   := i_org_id;
    g_default_group := i_default_group;
  
    open c_get_supplier_default;
    fetch c_get_supplier_default
      into r_ap_supplier_default;
    close c_get_supplier_default;
  
    if r_ap_supplier_default.default_group is null then
      write_log('No Data Found : Default Group : ' || i_default_group);
      err_msg := '*** No Data Found : Table DTAC_SUPPLIERS_INT_DEFAULT';
    end if;
  
    v_request_id := fnd_global.conc_request_id;
    fnd_file.put_line(fnd_file.output, ' ---- ');
    fnd_file.put_line(fnd_file.output, 'Retailer ');
  
    ----- Validation Summary
    begin
      write_log('GROUP :' || i_default_group || ' REQUEST ID :' ||
                v_request_id || ' ORG :' || i_org_id);
      validate_supplier_summary(i_def_grp => i_default_group,
                                i_req_id  => v_request_id,
                                i_org_id  => i_org_id);
    end;
  
    ----- For Supplier -----
    v_upd_flag := 'N';
    for i in 1 .. g_sup_int.count loop
      --Comment by pta 16/11/06 g_sup_int.FIRST..g_sup_int.LAST LOOP
      -- Bug Data 15-sep-2006
      r_ap_suppliers_int := null;
      if g_sup_int(i).status = 'PROCESSED' then
        v_dup_flag := 'N';
        if i > 1 then
          if g_sup_int(i).vat_registration_num = g_sup_int(i - 1)
          .vat_registration_num then
            v_dup_flag := 'Y';
          end if;
        end if;
      
        ---------------------------------------------------------------------------
      
        if g_sup_int(i).vendor_id is null then
          -- New
          if v_dup_flag = 'Y' then
            g_sup_int(i).vendor_interface_id := g_sup_int(i - 1)
                                               .vendor_interface_id;
            g_sup_int(i).sup_int_flag := g_sup_int(i - 1).sup_int_flag;
          else
            write_log('Insert Supplier : ' || g_sup_int(i)
                      .vendor_name || ' Status =>  ' || g_sup_int(i).status);
          
            select ap_suppliers_int_s.nextval
              into g_sup_int(i).vendor_interface_id
              from dual;
          
            v_idx := t_id.count + 1;
            r_ap_suppliers_int.vendor_interface_id := g_sup_int(i).vendor_interface_id;
            t_id(v_idx).vendor_interface_id := r_ap_suppliers_int.vendor_interface_id;
          
            --Assign value from temp to ap_suppliers_int
            r_ap_suppliers_int.vendor_name               := g_sup_int(i).vendor_name; --UPPER(g_sup_int(i).vendor_name);
            r_ap_suppliers_int.vendor_name_alt           := g_sup_int(i).vendor_name_alt; --UPPER(g_sup_int(i).vendor_name_alt);
            r_ap_suppliers_int.segment1                  := '';
            r_ap_suppliers_int.vendor_type_lookup_code   := g_sup_int(i)
                                                           .vendor_type_lookup_code;
            r_ap_suppliers_int.set_of_books_id           := i_set_of_book_id;
            r_ap_suppliers_int.request_id                := v_request_id;
            r_ap_suppliers_int.vat_registration_num      := g_sup_int(i)
                                                           .vat_registration_num;
            r_ap_suppliers_int.allow_awt_flag            := g_sup_int(i)
                                                           .allow_awt_flag;
            r_ap_suppliers_int.global_attribute_category := 'JA.TH.APXVDMVD.PO_VENDORS';
            r_ap_suppliers_int.global_attribute18        := g_sup_int(i)
                                                           .vat_registration_num;
            r_ap_suppliers_int.global_attribute6         := g_sup_int(i)
                                                           .default_group;
            r_ap_suppliers_int.global_attribute20        := g_sup_int(i)
                                                           .global_attribute20;
            r_ap_suppliers_int.attribute2                := g_sup_int(i)
                                                           .attribute2;
            r_ap_suppliers_int.attribute3                := g_sup_int(i)
                                                           .attribute3;
            r_ap_suppliers_int.attribute4                := g_sup_int(i)
                                                           .attribute4;
            r_ap_suppliers_int.status                    := 'NEW';
          
            -- Global Attribute
          
            --if null get from default
            r_ap_suppliers_int.terms_name                 := nvl(g_sup_int(i)
                                                                .terms_name,
                                                                 r_ap_supplier_default.terms_name);
            r_ap_suppliers_int.pay_group_lookup_code      := nvl(g_sup_int(i)
                                                                 .pay_group_lookup_code,
                                                                 r_ap_supplier_default.pay_group_lookup_code);
            r_ap_suppliers_int.payment_priority           := nvl(g_sup_int(i)
                                                                 .payment_priority,
                                                                 r_ap_supplier_default.payment_priority);
            r_ap_suppliers_int.terms_date_basis           := nvl(g_sup_int(i)
                                                                 .terms_date_basis,
                                                                 r_ap_supplier_default.terms_date_basis);
            r_ap_suppliers_int.pay_date_basis_lookup_code := nvl(g_sup_int(i)
                                                                 .pay_date_basis_lookup_code,
                                                                 r_ap_supplier_default.pay_date_basis_lookup_code);
            r_ap_suppliers_int.payment_method_lookup_code := nvl(g_sup_int(i)
                                                                 .payment_method_lookup_code,
                                                                 r_ap_supplier_default.payment_method_lookup_code);
            r_ap_suppliers_int.invoice_currency_code      := nvl(g_sup_int(i)
                                                                 .invoice_currency_code,
                                                                 r_ap_supplier_default.invoice_currency_code);
          
            /*-- Update by Nu, 13/12/2006 --*/
            r_ap_suppliers_int.attribute_category := g_citibank;
            r_ap_suppliers_int.attribute5         := r_ap_supplier_default.attribute5;
            r_ap_suppliers_int.attribute6         := r_ap_supplier_default.attribute6;
            r_ap_suppliers_int.attribute7         := r_ap_supplier_default.attribute7;
            r_ap_suppliers_int.attribute8         := r_ap_supplier_default.attribute8;
            r_ap_suppliers_int.attribute9         := r_ap_supplier_default.attribute9;
            r_ap_suppliers_int.attribute10        := r_ap_supplier_default.attribute10;
            r_ap_suppliers_int.attribute11        := r_ap_supplier_default.attribute11;
          
            insert_suppliers_int_migrate(ir_ap_suppliers_int => r_ap_suppliers_int);
          
            --END IF ; -- v_hdr_proc
            g_sup_int(i).status := 'PROCESSED';
            g_sup_int(i).sup_int_flag := 'INTERFACED';
            g_sup_int(i).interface_mode := 'INSERT';
            g_sup_int(i).err_msg := 'INTERFACE COMPLETED,  ';
          end if;
        else
          -- Old Supplier
        
          if v_dup_flag = 'Y' then
            g_sup_int(i).vendor_interface_id := g_sup_int(i - 1)
                                               .vendor_interface_id;
            g_sup_int(i).sup_int_flag := g_sup_int(i - 1).sup_int_flag;
          else
            write_log('Update Supplier : ' || g_sup_int(i)
                      .vendor_name || ' Status =>  ' || g_sup_int(i).status);
          
            select ap_suppliers_int_s.nextval
              into g_sup_int(i) .vendor_interface_id
              from dual;
          
            r_po_vendors           := null;
            r_po_vendors.vendor_id := g_sup_int(i).vendor_id;
          
            if g_sup_int(i).vat_registration_num is not null then
              r_po_vendors.vat_registration_num := g_sup_int(i)
                                                  .vat_registration_num;
            else
              begin
                select vat_registration_num
                  into r_po_vendors.vat_registration_num
                  from po_vendors
                 where vendor_name = g_sup_int(i).vendor_name;
              exception
                when others then
                  fnd_file.put_line(fnd_file.log,
                                    'Vat Registration Num of Vendor Name : ' ||
                                     g_sup_int(i)
                                    .vendor_name || ' Not Found in Vendor.');
              end;
            end if;
          
            if g_sup_int(i).pay_group_lookup_code is not null then
              r_po_vendors.pay_group_lookup_code := g_sup_int(i)
                                                   .pay_group_lookup_code;
            else
              begin
                select pay_group_lookup_code
                  into r_po_vendors.pay_group_lookup_code
                  from po_vendors
                 where vendor_id = g_sup_int(i).vendor_id;
              exception
                when others then
                  fnd_file.put_line(fnd_file.log,
                                    'Pay Group Lookup Code of Vendor Name : ' ||
                                     g_sup_int(i)
                                    .vendor_name || ' Not Found in Vendor.');
              end;
            
            end if;
          
            if g_sup_int(i).terms_name is not null then
            
              begin
                select term_id
                  into r_po_vendors.terms_id
                  from ap_terms
                 where upper(name) = upper(g_sup_int(i).terms_name);
              exception
                when others then
                  fnd_file.put_line(fnd_file.log,
                                    'Terms Name of Vendor Name : ' ||
                                     g_sup_int(i)
                                    .vendor_name || ' Not Found in Vendor.');
              end;
            
            else
              begin
                select terms_id
                  into r_po_vendors.terms_id
                  from po_vendors
                 where vendor_id = g_sup_int(i).vendor_id;
              exception
                when others then
                  fnd_file.put_line(fnd_file.log,
                                    'Terms Name of Vendor Name : ' ||
                                     g_sup_int(i)
                                    .vendor_name || ' Not Found in Vendor.');
              end;
            
            end if;
          
            r_po_vendors.global_attribute18 := g_sup_int(i)
                                              .vat_registration_num;
            r_po_vendors.global_attribute6  := g_sup_int(i).default_group;
            r_po_vendors.global_attribute20 := g_sup_int(i)
                                              .global_attribute20;
            r_po_vendors.attribute2         := g_sup_int(i).attribute2;
            r_po_vendors.attribute3         := g_sup_int(i).attribute3;
            r_po_vendors.attribute4         := g_sup_int(i).attribute4;
            -- Add by Tong 19/09/2007
            r_po_vendors.attribute12      := g_sup_int(i).mail_to_address1;
            r_po_vendors.attribute13      := g_sup_int(i).mail_to_address2;
            r_po_vendors.attribute14      := g_sup_int(i).mail_to_address3;
            r_po_vendors.attribute15      := g_sup_int(i).owner_name;
            r_po_vendors.last_update_date := sysdate;
          
            --update_po_vendors(ir_po_vendors => r_po_vendors ,o_status => v_status);
            g_sup_int(i).status := 'PROCESSED';
            g_sup_int(i).sup_int_flag := 'UPDATED';
            v_upd_flag := 'Y';
            g_sup_int(i).interface_mode := 'UPDATE';
            g_sup_int(i).err_msg := 'INTERFACE COMPLETED,  ';
          end if;
        end if; -- NEW Supplier
        --End If Can not Interface
        g_sup_int(i).err_msg := ' ';
      
      end if;
    end loop; -- Supplier Interface
  
    v_row_count := 0;
    begin
      commit;
      begin
        select count('x')
          into v_row_count
          from ap_suppliers_int
         where request_id = v_request_id;
      end;
    
      if nvl(v_row_count, 0) > 0 then
        --Supplier Open Interface Import
      
        v_sup_req_id := fnd_request.submit_request(application => 'SQLAP',
                                                   program     => 'APXSUIMP',
                                                   argument1   => 'NEW', -- 1000, N, N, N,
                                                   argument2   => '1000', -- x_org_ref,
                                                   argument3   => 'N',
                                                   argument4   => 'N',
                                                   argument5   => 'N');
      end if;
    
      ------------
      if v_sup_req_id > 0 or v_upd_flag = 'Y' then
        if v_sup_req_id > 0 then
          conc_wait(v_sup_req_id);
        end if;
        -------------
        for i in 1 .. g_sup_int.count loop
          if g_sup_int(i).status = 'PROCESSED' and g_sup_int(i)
          .sup_int_flag = 'INTERFACED' and g_sup_int(i).vendor_id is null then
            begin
              select segment1, vendor_id
                into g_sup_int(i) .vendor_number, g_sup_int(i) .vendor_id
                from po_vendors asi
               where asi.vendor_name = g_sup_int(i).vendor_name;
            
              if g_sup_int(i).vendor_number is null then
                g_sup_int(i).status := 'REJECTED';
                g_sup_int(i).err_msg := ' Error : Sup Interface ';
              else
                begin
                  update po_vendors
                     set vat_code = null
                   where vendor_id = g_sup_int(i).vendor_id;
                  --v := 1;
                exception
                  when others then
                    g_sup_int(i).status := 'ERROR';
                    g_sup_int(i).err_msg := g_sup_int(i).err_msg;
                    fnd_file.put_line(fnd_file.log,
                                      'Error : Can not Update Vat Code  ' ||
                                       'Vendor Name : ' || g_sup_int(i)
                                      .vendor_name);
                end;
              end if;
            
            exception
              when others then
                g_sup_int(i).status := 'ERROR';
                g_sup_int(i).err_msg := g_sup_int(i).err_msg;
                fnd_file.put_line(fnd_file.log,
                                  'Error : ' ||
                                  'Vat Registration Number : ' ||
                                  g_sup_int(i).vat_registration_num ||
                                  ' Not Found in Vendor.');
            end;
          
          elsif g_sup_int(i).vendor_number is null and g_sup_int(i)
          .vendor_id is not null then
            begin
              select segment1
                into g_sup_int(i) .vendor_number
                from po_vendors asi
               where asi.vendor_name = g_sup_int(i).vendor_name;
            exception
              when others then
                g_sup_int(i).status := 'ERROR';
                g_sup_int(i).err_msg := g_sup_int(i).err_msg;
                fnd_file.put_line(fnd_file.log,
                                  'Error : ' ||
                                   'Vendor Number of Vendor Name : ' ||
                                   g_sup_int(i)
                                  .vendor_name || ' Not Found in Vendor.');
            end;
          end if;
        end loop; -- Loop Supplier
      end if;
      --End If ;
    end;
  
    -- START Modify HERE [AP@BAS]
    ---- Supplier Site ---
    v_upd_flag := 'N';
    for i in 1 .. g_sup_int.count loop
      --Comment by pta 16/11/06 g_sup_int.FIRST..g_sup_int.LAST LOOP
      r_ap_sup_site := null;
      if g_sup_int(i).status = 'PROCESSED' and g_sup_int(i)
      .vendor_id is not null then
        if g_sup_int(i).vendor_site_id is null then
          write_log('Insert Supplier site : (' || g_sup_int(i)
                    .vendor_number || ') ' || g_sup_int(i)
                    .vendor_name || '   ' || g_sup_int(i).vendor_site_code);
        
          --Assign value from temp to ap_supplier_site
          r_ap_sup_site.vendor_interface_id        := g_sup_int(i)
                                                     .vendor_interface_id;
          r_ap_sup_site.vendor_id                  := g_sup_int(i)
                                                     .vendor_id;
          r_ap_sup_site.request_id                 := v_request_id;
          r_ap_sup_site.vendor_site_code           := g_sup_int(i)
                                                     .vendor_site_code;
          r_ap_sup_site.address_line1              := g_sup_int(i)
                                                     .address_line1;
          r_ap_sup_site.address_line2              := g_sup_int(i)
                                                     .address_line2;
          r_ap_sup_site.address_line3              := g_sup_int(i)
                                                     .address_line3;
          r_ap_sup_site.address_line4              := g_sup_int(i)
                                                     .address_line4;
          r_ap_sup_site.city                       := g_sup_int(i).city;
          r_ap_sup_site.zip                        := g_sup_int(i).zip;
          r_ap_sup_site.province                   := g_sup_int(i).province;
          r_ap_sup_site.country                    := g_sup_int(i).country;
          r_ap_sup_site.payment_method_lookup_code := g_sup_int(i)
                                                     .payment_method_lookup_code;
          r_ap_sup_site.terms_date_basis           := g_sup_int(i)
                                                     .site_terms_date_basis;
          r_ap_sup_site.payment_priority           := g_sup_int(i)
                                                     .site_payment_priority;
        
          r_ap_sup_site.terms_name            := nvl(g_sup_int(i)
                                                     .site_terms_name,
                                                     r_ap_supplier_default.terms_name);
          r_ap_sup_site.pay_group_lookup_code := nvl(g_sup_int(i)
                                                     .site_pay_group_lookup_code,
                                                     r_ap_supplier_default.pay_group_lookup_code);
        
          r_ap_sup_site.pay_date_basis_lookup_code := g_sup_int(i)
                                                     .pay_date_basis_lookup_code;
          r_ap_sup_site.invoice_currency_code      := g_sup_int(i)
                                                     .site_invoice_currency_code;
          r_ap_sup_site.attribute1                 := g_sup_int(i)
                                                     .attribute1;
          r_ap_sup_site.attribute2                 := g_sup_int(i)
                                                     .site_attribute2;
          r_ap_sup_site.attribute3                 := g_sup_int(i)
                                                     .site_attribute3;
          r_ap_sup_site.attribute4                 := g_sup_int(i)
                                                     .site_attribute4;
          r_ap_sup_site.org_id                     := i_org_id;
          r_ap_sup_site.allow_awt_flag             := g_sup_int(i)
                                                     .allow_awt_flag;
          r_ap_sup_site.global_attribute_category  := 'JA.TH.APXVDMVD.SITES';
          r_ap_sup_site.global_attribute18         := g_sup_int(i)
                                                     .vat_registration_num;
          r_ap_sup_site.global_attribute20         := g_sup_int(i)
                                                     .global_attribute20;
          r_ap_sup_site.global_attribute6          := g_sup_int(i)
                                                     .default_group; --09/04/2008 Naj
          r_ap_sup_site.status                     := 'NEW';
          -- Added by AP@BAS on 12-Dec-2013
          r_ap_sup_site.global_attribute14 := nvl(g_sup_int(i).branch_code,
                                                  '�ӹѡ�ҹ�˭�');
          --
          --if null get from default
          r_ap_sup_site.pay_site_flag                 := nvl(g_sup_int(i)
                                                             .pay_site_flag,
                                                             r_ap_supplier_default.pay_site_flag);
          r_ap_sup_site.purchasing_site_flag          := nvl(g_sup_int(i)
                                                             .purchasing_site_flag,
                                                             r_ap_supplier_default.purchase_site_flag);
          r_ap_sup_site.accts_pay_code_combination_id := nvl(r_ap_supplier_default.accts_pay_ccid,
                                                             r_ap_supplier_default.accts_pay_ccid);
          r_ap_sup_site.prepay_code_combination_id    := nvl(r_ap_supplier_default.prepay_ccid,
                                                             r_ap_supplier_default.prepay_ccid);
          r_ap_sup_site.future_dated_payment_ccid     := nvl(r_ap_supplier_default.future_dated_payment_ccid,
                                                             r_ap_supplier_default.future_dated_payment_ccid);
          -- end if null get from default
          r_ap_sup_site.vendor_site_code_alt := g_sup_int(i).shop_name;
        
          if g_sup_int(i).attribute_category is null then
            g_sup_int(i).attribute_category := g_citibank;
          end if;
        
          r_ap_sup_site.attribute_category := nvl(g_sup_int(i)
                                                  .attribute_category,
                                                  g_citibank);
          if g_sup_int(i).attribute_category = g_citibank then
            --'CITIBANK'
            r_ap_sup_site.attribute5  := nvl(g_sup_int(i).attribute5,
                                             r_ap_supplier_default.attribute5 /*''*/);
            r_ap_sup_site.attribute6  := nvl(g_sup_int(i).attribute6,
                                             r_ap_supplier_default.attribute6 /*''*/);
            r_ap_sup_site.attribute7  := nvl(g_sup_int(i).attribute7,
                                             r_ap_supplier_default.attribute7 /*''*/);
            r_ap_sup_site.attribute8  := nvl(g_sup_int(i).attribute8,
                                             r_ap_supplier_default.attribute8 /*''*/);
            r_ap_sup_site.attribute9  := nvl(g_sup_int(i).attribute9,
                                             r_ap_supplier_default.attribute9 /*''*/);
            r_ap_sup_site.attribute10 := nvl(g_sup_int(i).attribute10,
                                             r_ap_supplier_default.attribute10 /*''*/);
            r_ap_sup_site.attribute11 := nvl(g_sup_int(i).attribute11,
                                             r_ap_supplier_default.attribute11 /*''*/);
            r_ap_sup_site.attribute12 := g_sup_int(i).mail_to_address1; --- Add by Tong 19/09/2007
            r_ap_sup_site.attribute13 := g_sup_int(i).mail_to_address2; --- Add by Tong 19/09/2007
            r_ap_sup_site.attribute14 := g_sup_int(i).mail_to_address3; --- Add by Tong 19/09/2007
            r_ap_sup_site.attribute15 := g_sup_int(i).owner_name; --- Add by Tong 19/09/2007
          
          elsif g_sup_int(i).attribute_category = g_kbank then
            --'KBANK'
            r_ap_sup_site.attribute5  := nvl(g_sup_int(i).vendor_site_code,
                                             '');
            r_ap_sup_site.attribute6  := nvl(g_sup_int(i).address_line1, '');
            r_ap_sup_site.attribute7  := nvl(g_sup_int(i).address_line2, '');
            r_ap_sup_site.attribute8  := nvl(g_sup_int(i).address_line3, '');
            r_ap_sup_site.attribute9  := nvl(g_sup_int(i).address_line4, '');
            r_ap_sup_site.attribute10 := nvl(g_sup_int(i).city, '');
            --r_ap_suppliers_int.attribute11    :=    attribute11;
          end if;
        
          -- Modify by AP@BAS on 06-Feb-2013 : see detail in procedure insert_ap_sup_site
          insert_ap_sup_site(ir_ap_sup_site => r_ap_sup_site);
          --
          g_sup_int(i).sup_site_int_flag := 'INTERFACED';
          g_sup_int(i).interface_mode := 'INSERT';
          --g_sup_int(i).err_msg             := ' INTERFACE COMPLETED,  ' ;
        else
          -- Old Site HO and insert into DTN and PSB
          write_log('Insert Supplier site for DTN, PSB : (' ||
                    g_sup_int(i).vendor_number || ') ' || g_sup_int(i)
                    .vendor_name || '   ' || g_sup_int(i).vendor_site_code);
          --Assign value from temp to ap_supplier_site
          r_ap_sup_site.vendor_interface_id        := g_sup_int(i)
                                                     .vendor_interface_id;
          r_ap_sup_site.vendor_id                  := g_sup_int(i)
                                                     .vendor_id;
          r_ap_sup_site.request_id                 := v_request_id;
          r_ap_sup_site.vendor_site_code           := g_sup_int(i)
                                                     .vendor_site_code;
          r_ap_sup_site.address_line1              := g_sup_int(i)
                                                     .address_line1;
          r_ap_sup_site.address_line2              := g_sup_int(i)
                                                     .address_line2;
          r_ap_sup_site.address_line3              := g_sup_int(i)
                                                     .address_line3;
          r_ap_sup_site.address_line4              := g_sup_int(i)
                                                     .address_line4;
          r_ap_sup_site.city                       := g_sup_int(i).city;
          r_ap_sup_site.zip                        := g_sup_int(i).zip;
          r_ap_sup_site.province                   := g_sup_int(i).province;
          r_ap_sup_site.country                    := g_sup_int(i).country;
          r_ap_sup_site.payment_method_lookup_code := g_sup_int(i)
                                                     .payment_method_lookup_code;
          r_ap_sup_site.terms_date_basis           := g_sup_int(i)
                                                     .site_terms_date_basis;
          r_ap_sup_site.payment_priority           := g_sup_int(i)
                                                     .site_payment_priority;
        
          r_ap_sup_site.terms_name            := nvl(g_sup_int(i)
                                                     .site_terms_name,
                                                     r_ap_supplier_default.terms_name);
          r_ap_sup_site.pay_group_lookup_code := nvl(g_sup_int(i)
                                                     .site_pay_group_lookup_code,
                                                     r_ap_supplier_default.pay_group_lookup_code);
        
          r_ap_sup_site.pay_date_basis_lookup_code := g_sup_int(i)
                                                     .pay_date_basis_lookup_code;
          r_ap_sup_site.invoice_currency_code      := g_sup_int(i)
                                                     .site_invoice_currency_code;
        
          r_ap_sup_site.attribute1 := g_sup_int(i).attribute1;
          r_ap_sup_site.attribute2 := g_sup_int(i).site_attribute2;
          r_ap_sup_site.attribute3 := g_sup_int(i).site_attribute3;
          r_ap_sup_site.attribute4 := g_sup_int(i).site_attribute4;
        
          r_ap_sup_site.org_id                    := i_org_id;
          r_ap_sup_site.allow_awt_flag            := g_sup_int(i)
                                                    .allow_awt_flag;
          r_ap_sup_site.global_attribute_category := 'JA.TH.APXVDMVD.SITES';
          r_ap_sup_site.global_attribute18        := g_sup_int(i)
                                                    .vat_registration_num;
          r_ap_sup_site.global_attribute20        := g_sup_int(i)
                                                    .global_attribute20;
          r_ap_sup_site.global_attribute6         := g_sup_int(i)
                                                    .default_group; --09/04/2008 Naj
          r_ap_sup_site.status                    := 'NEW';
          -- Added by AP@BAS on 12-Dec-2013
          r_ap_sup_site.global_attribute14 := nvl(g_sup_int(i).branch_code,
                                                  '�ӹѡ�ҹ�˭�');
          --
          --if null get from default
          r_ap_sup_site.pay_site_flag                 := nvl(g_sup_int(i)
                                                             .pay_site_flag,
                                                             r_ap_supplier_default.pay_site_flag);
          r_ap_sup_site.purchasing_site_flag          := nvl(g_sup_int(i)
                                                             .purchasing_site_flag,
                                                             r_ap_supplier_default.purchase_site_flag);
          r_ap_sup_site.accts_pay_code_combination_id := nvl(r_ap_supplier_default.accts_pay_ccid,
                                                             r_ap_supplier_default.accts_pay_ccid);
          r_ap_sup_site.prepay_code_combination_id    := nvl(r_ap_supplier_default.prepay_ccid,
                                                             r_ap_supplier_default.prepay_ccid);
          r_ap_sup_site.future_dated_payment_ccid     := nvl(r_ap_supplier_default.future_dated_payment_ccid,
                                                             r_ap_supplier_default.future_dated_payment_ccid);
          -- end if null get from default
          r_ap_sup_site.vendor_site_code_alt := g_sup_int(i).shop_name;
        
          if g_sup_int(i).attribute_category is null then
            g_sup_int(i).attribute_category := g_citibank;
          end if;
        
          r_ap_sup_site.attribute_category := nvl(g_sup_int(i)
                                                  .attribute_category,
                                                  g_citibank);
          if g_sup_int(i).attribute_category = g_citibank then
            --'CITIBANK'
            r_ap_sup_site.attribute5  := nvl(g_sup_int(i).attribute5,
                                             r_ap_supplier_default.attribute5 /*''*/);
            r_ap_sup_site.attribute6  := nvl(g_sup_int(i).attribute6,
                                             r_ap_supplier_default.attribute6 /*''*/);
            r_ap_sup_site.attribute7  := nvl(g_sup_int(i).attribute7,
                                             r_ap_supplier_default.attribute7 /*''*/);
            r_ap_sup_site.attribute8  := nvl(g_sup_int(i).attribute8,
                                             r_ap_supplier_default.attribute8 /*''*/);
            r_ap_sup_site.attribute9  := nvl(g_sup_int(i).attribute9,
                                             r_ap_supplier_default.attribute9 /*''*/);
            r_ap_sup_site.attribute10 := nvl(g_sup_int(i).attribute10,
                                             r_ap_supplier_default.attribute10 /*''*/);
            r_ap_sup_site.attribute11 := nvl(g_sup_int(i).attribute11,
                                             r_ap_supplier_default.attribute11 /*''*/);
            r_ap_sup_site.attribute12 := g_sup_int(i).mail_to_address1; --- Add by Tong 19/09/2007
            r_ap_sup_site.attribute13 := g_sup_int(i).mail_to_address2; --- Add by Tong 19/09/2007
            r_ap_sup_site.attribute14 := g_sup_int(i).mail_to_address3; --- Add by Tong 19/09/2007
            r_ap_sup_site.attribute15 := g_sup_int(i).owner_name; --- Add by Tong 19/09/2007
          
          elsif g_sup_int(i).attribute_category = g_kbank then
            --'KBANK'
            r_ap_sup_site.attribute5  := nvl(g_sup_int(i).vendor_site_code,
                                             '');
            r_ap_sup_site.attribute6  := nvl(g_sup_int(i).address_line1, '');
            r_ap_sup_site.attribute7  := nvl(g_sup_int(i).address_line2, '');
            r_ap_sup_site.attribute8  := nvl(g_sup_int(i).address_line3, '');
            r_ap_sup_site.attribute9  := nvl(g_sup_int(i).address_line4, '');
            r_ap_sup_site.attribute10 := nvl(g_sup_int(i).city, '');
            --r_ap_suppliers_int.attribute11    :=    attribute11;
          end if;
        
          -- Modify by AP@BAS on 06-Feb-2013 : see detail in procedure insert_ap_sup_site
          insert_ap_sup_site_migrate(ir_ap_sup_site => r_ap_sup_site);
          --
          g_sup_int(i).sup_site_int_flag := 'INTERFACED';
          g_sup_int(i).interface_mode := 'INSERT';
          --
          ------------- Supplier Site
        
        end if; -- Site New
      
        g_sup_int(i).err_msg := ' ';
      
      end if; -- Processed
    
    end loop; -- -Supplier Site
  
    v_row_count := 0;
    begin
      begin
        select count('x')
          into v_row_count
          from ap_supplier_sites_int
         where request_id = v_request_id;
      end;
    end;
    if v_row_count > 0 then
      commit;
      --Supplier Sites Open Interface Import
      v_site_req_id := fnd_request.submit_request(application => 'SQLAP',
                                                  program     => 'APXSSIMP',
                                                  argument1   => 'NEW', -- 1000, N, N, N,
                                                  argument2   => '1000', -- x_org_ref,
                                                  argument3   => 'N',
                                                  argument4   => 'N',
                                                  argument5   => 'N');
    end if; ------------
  
    if v_site_req_id > 0 or v_upd_flag = 'Y' then
      if v_site_req_id > 0 then
        conc_wait(v_site_req_id);
      end if;
      for i in 1 .. g_sup_int.count loop
        -- Comment by pta 16/11/06 g_sup_int.FIRST..g_sup_int.LAST Loop
        if g_sup_int(i).status = 'PROCESSED' and g_sup_int(i)
        .sup_site_int_flag = 'INTERFACED' and g_sup_int(i)
        .vendor_id is not null and g_sup_int(i).vendor_site_id is null then
          begin
            select vendor_site_id
              into g_sup_int(i) .vendor_site_id
              from po_vendor_sites pvs
             where 1 = 1 --pvs.vendor_interface_id = g_sup_int(i).vendor_interface_id
               and pvs.vendor_id = g_sup_int(i)
            .vendor_id
               and pvs.vendor_site_code = g_sup_int(i)
            .vendor_site_code;
          
            if g_sup_int(i).vendor_site_id is null then
              g_sup_int(i).status := 'REJECTED';
              g_sup_int(i).err_msg := ' Error : Sup Site Interface ';
            else
              begin
                --fnd_file.put_line(fnd_file.log,'TEST : ' || g_sup_int(i).vendor_id || '#######' || g_sup_int(i).vendor_site_id );
                update po_vendor_sites_all
                   set vat_code = null
                 where vendor_id = g_sup_int(i)
                .vendor_id
                   and vendor_site_id = g_sup_int(i).vendor_site_id;
                --v := 1;
              exception
                when others then
                  g_sup_int(i).status := 'ERROR';
                  g_sup_int(i).err_msg := g_sup_int(i).err_msg;
                  fnd_file.put_line(fnd_file.log,
                                    'Error : Can not Update Vat Code Vendor Site ID : ' ||
                                    g_sup_int(i).vendor_site_id);
              end;
            end if;
          
          exception
            when others then
              g_sup_int(i).status := 'ERROR';
              g_sup_int(i).err_msg := 'Error : Sup Site Interface ';
              fnd_file.put_line(fnd_file.log,
                                'Error : ' || 'Vat Registration Number : ' ||
                                g_sup_int(i).vat_registration_num ||
                                ' Not Found in Vendor.');
          end;
        end if;
      end loop;
    end if;
    commit;
  
    ----------------------  site contact
    for i in 1 .. g_sup_int.count loop
      -- Comment by pta 16/11/06 g_sup_int.First..g_sup_int.LAST LOOP
      r_ap_sup_site_cont := null;
      if g_sup_int(i).status = 'PROCESSED' then
      
        if g_sup_int(i).vendor_site_id is not null then
          if g_sup_int(i).vendor_site_cont_id is null then
            if g_sup_int(i).last_name is not null then
              null;
            end if; --check last name
          else
            write_log('Insert Supplier site contact : (' || g_sup_int(i)
                      .vendor_number || ') ' || g_sup_int(i)
                      .vendor_name || '  ' || g_sup_int(i)
                      .first_name || ' ' || g_sup_int(i).last_name);
            --Assign value from temp to ap_supplier_site_contact
            r_ap_sup_site_cont.vendor_site_id := g_sup_int(i)
                                                .vendor_site_id;
          
            -- Modified by AP@BAS on 06-Feb-2013 : assign vender site code for use in mapping , but not insert into interface table
            r_ap_sup_site_cont.vendor_site_code := g_sup_int(i)
                                                  .vendor_site_code;
            --rap_sup_site_cont.vendor_site_code := '';
            --
          
            r_ap_sup_site_cont.org_id           := i_org_id;
            r_ap_sup_site_cont.first_name       := g_sup_int(i).first_name;
            r_ap_sup_site_cont.last_name        := g_sup_int(i).last_name;
            r_ap_sup_site_cont.area_code        := g_sup_int(i).area_code;
            r_ap_sup_site_cont.phone            := g_sup_int(i).phone;
            r_ap_sup_site_cont.status           := 'NEW';
            r_ap_sup_site_cont.email_address    := g_sup_int(i)
                                                  .email_address;
            r_ap_sup_site_cont.fax              := g_sup_int(i).fax;
            r_ap_sup_site_cont.contact_name_alt := r_ap_suppliers_int.vendor_interface_id;
            r_ap_sup_site_cont.request_id       := g_sup_int(i).request_id;
          
            -- Modify by AP@BAS on 06-Feb-2013 : see detail in procedure insert_ap_sup_site_cont
            insert_sup_site_cont_migrate(ir_ap_sup_site_cont => r_ap_sup_site_cont);
            --
          
            g_sup_int(i).sup_site_cont_int_flag := 'INTERFACED';
            g_sup_int(i).interface_mode := 'INSERT';
            g_sup_int(i).err_msg := ' INTERFACE COMPLETED,  ';
          end if; -- Vendor Site Contact
        end if; -- Vendor Site
      
        g_sup_int(i).err_msg := ' INTERFACE COMPLETED,  ';
      
      end if; -- Process
    
    end loop;
    ---------------- Supplier Interface ----------------------
  
    --- Supplier Contact ----
    v_row_count := 0;
    begin
      begin
        select count('x')
          into v_row_count
          from ap_sup_site_contact_int
         where request_id = v_request_id;
      end;
    end;
    if v_row_count > 0 then
      commit;
      --  Supplier Contacts Open Interface Import
      v_cont_req_id := fnd_request.submit_request(application => 'SQLAP',
                                                  program     => 'APXSCIMP',
                                                  argument1   => 'NEW', -- 1000, N, N, N,
                                                  argument2   => '1000', -- x_org_ref,
                                                  argument3   => 'N',
                                                  argument4   => 'N',
                                                  argument5   => 'N');
      ------------
      if v_cont_req_id > 0 then
        conc_wait(v_cont_req_id);
        for i in 1 .. g_sup_int.count loop
          -- Comment by pta 16/11/06 g_sup_int.FIRST..g_sup_int.LAST Loop
          if g_sup_int(i).status = 'PROCESSED' and g_sup_int(i)
          .sup_site_cont_int_flag = 'INTERFACED' and g_sup_int(i)
          .vendor_site_id is not null and g_sup_int(i)
          .vendor_site_cont_id is null then
          
            -- Modify by boy CCC 03/06/2008
            begin
              select vendor_contact_id
                into g_sup_int(i) .vendor_site_cont_id
                from po_vendor_contacts pvc
               where 1 = 1 --pvs.vendor_interface_id = g_sup_int(i).vendor_interface_id
                 and pvc.vendor_site_id = g_sup_int(i)
              .vendor_site_id
                 and pvc.first_name = g_sup_int(i)
              .first_name
                 and pvc.last_name = g_sup_int(i).last_name
                 and rownum = 1;
              if g_sup_int(i).vendor_site_cont_id is null then
                g_sup_int(i).status := 'REJECTED';
                g_sup_int(i).err_msg := 'Error : Supplier Site contact interface ';
                fnd_file.put_line(fnd_file.log,
                                  'Vendor Site ' ||
                                   to_char(g_sup_int(i).vendor_site_id) ||
                                   ' fname ' || g_sup_int(i)
                                  .first_name || ' lname ' || g_sup_int(i)
                                  .last_name);
              end if;
            exception
              when others then
                g_sup_int(i).status := 'REJECTED';
                g_sup_int(i).err_msg := 'Error : Supplier Site contact interface ';
            end;
            -- Modify by boy CCC 03/06/2008
          end if;
        end loop;
      end if; -- Contact
    end if;
    --- Supplier Contact ----
  
    for i in 1 .. g_sup_int.count loop
      update dtac_ap_suppliers_temp_int
         set status        = g_sup_int(i).status,
             error_message = g_sup_int(i).err_msg
       where rowid = g_sup_int(i).row_id;
    end loop;
    commit;
    -------------------------------------------------------------
  
  end supplier_interface_migration;

end tac_supplier_interface_util;