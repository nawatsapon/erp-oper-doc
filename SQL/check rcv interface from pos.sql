SELECT rcv_headers_interface.shipment_num,
    rcv_headers_interface.processing_status_code,
    rcv_transactions_interface.interface_transaction_id,
    rcv_transactions_interface.item_num,
    rcv_transactions_interface.quantity,
    rcv_transactions_interface.unit_of_measure,
    rcv_transactions_interface.receipt_source_code,
    rcv_transactions_interface.document_num,
    DECODE(rcv_transactions_interface.receipt_source_code,NULL,'PURCHASE',rcv_transactions_interface.receipt_source_code) tran_type
   ,
    'I0' || rcv_transactions_interface.req_num req_num,
    rcv_transactions_interface.subinventory,
    rcv_transactions_interface.locator,
    po_interface_errors.error_message_name,
    po_interface_errors.error_message,
    po_interface_errors.table_name,
    po_interface_errors.column_name
FROM
    po.rcv_headers_interface,
    po.rcv_transactions_interface,
    po.po_interface_errors
WHERE
    0 = 0
    AND rcv_headers_interface.header_interface_id = rcv_transactions_interface.header_interface_id(+)
    AND rcv_headers_interface.header_interface_id = po_interface_errors.interface_header_id (+)
--and TRUNC(PO_INTERFACE_ERRORS.CREATION_DATE) >= to_date('21/11/2018','dd/mm/yyyy');
--AND RCV_HEADERS_INTERFACE.RECEIPT_SOURCE_CODE = 'INTERNAL ORDER'
--AND RCV_TRANSACTIONS_INTERFACE.DOCUMENT_NUM   = '34N026361800289'
    AND rcv_headers_interface.processing_status_code = 'ERROR'
    AND rcv_headers_interface.group_id = 1
    and rcv_transactions_interface.document_num in ('34N352291700009')
    ;

SELECT
    *
FROM
    po_interface_errors
WHERE 0=0
    and 
    trunc(creation_date) >= TO_DATE('01/07/2019','dd/mm/yyyy');

UPDATE rcv_headers_interface
SET
    processing_request_id = NULL,
    validation_flag = 'Y',
    processing_status_code = 'PENDING'
WHERE
    header_interface_id IN (
        1559663
    );

UPDATE rcv_transactions_interface
SET
    request_id = NULL,
    processing_request_id = NULL,
    validation_flag = 'Y',
    processing_status_code = 'PENDING',
    transaction_status_code = 'PENDING',
    processing_mode_code = 'BATCH'
WHERE
    interface_transaction_id IN (
        1559652,
1559665
    );

SELECT
    *
FROM
    rcv_transactions_interface
WHERE
    interface_transaction_id IN (1535869

    );

SELECT
    *
FROM
    rcv_headers_interface
WHERE
    header_interface_id IN (
        1673151,
        1674153,
        1674151
    );
    
    
    select * from inv.MTL_SERIAL_NUMBERS_INTERFACE where product_transaction_id = 1535869;