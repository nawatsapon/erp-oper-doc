-- 1. Script for sales or return to external customer (Retailer) (Partner code is not null, attribute6)
-- Yodchai L. 30-Sep-2013 join attribute from mtl_serial_numbers
-- Aphichart P. 4-Dec-2014 add mmt.transaction_id to each query for use update key
-- Aphichart P. 4-Dec-2014 add criteria to each query by added "and mmt.attribute11 is null"
-- Aphichart P. 4-Dec-2014 modify start_date parameter back to 2 hours for cover late commit transactions.
SELECT TO_CHAR(ooh.order_number) order_number ,
  mmt.transaction_date document_date -- format date  'DD/MM/YYYY HH24:MI:SS'
  ,
  CASE
    WHEN mmt.transaction_type_id = 33
    THEN 'A'
    ELSE 'D'
  END transaction_type ,
  ship_cas.attribute6 partner_code ,
  msi.segment1 product_code ,
  DECODE(flv.attribute14, 'DEVICE', NULL, mut.serial_number) serial_number -- 'SIM': show serial number, 'DEVICE': Item number
  ,
  ool.unit_selling_price unit_selling_price ,
  'ERP' source_name ,
  NULL warrenty_date ,
  to_date(msn.attribute2, 'yyyy/mm/dd hh24:mi:ss') expiry_date ,
  DECODE(flv.attribute14, 'DEVICE', NVL(msn.attribute1, mut.serial_number), 'AIRCARD', msn.attribute3, NULL) imei_number -- 'SIM': show NULL, 'DEVICE': attribute1, 'AIRCARD+SIM' show attribute3 (UD item number)
  ,
  DECODE(flv.attribute14, 'DEVICE', NULL, msn.attribute1) telephone_number -- 'SIM': show attribute1, 'DEVICE': null, 'AIRCARD+SIM' show attribute1
  ,
  mmt.transaction_id
FROM mtl_material_transactions mmt ,
  mtl_transaction_types mtt ,
  mtl_system_items msi ,
  fnd_lookup_values flv ,
  oe_order_lines_all ool ,
  oe_order_headers_all ooh ,
  oe_order_sources oos ,
  mtl_unit_transactions mut ,
  hz_cust_accounts cust_acct ,
  hz_parties party ,
  hz_cust_site_uses_all ship_su ,
  hz_cust_acct_sites_all ship_cas ,
  hz_party_sites ship_party_site ,
  hz_locations ship_loc ,
  mtl_serial_numbers msn
WHERE 1 = 1
  -- chagen transaction to creation date
  --and mmt.transaction_date >= v_trx_date_start
  --and mmt.transaction_date <= v_trx_date_end
--AND mmt.creation_date           >= to_date('','')
--AND mmt.creation_date           <= v_to_time
AND mmt.transaction_type_id     IN (33, 15) -- (33: 'Sales order issue',15: 'RMA Receipt')
AND mtt.transaction_type_id      = mmt.transaction_type_id
AND msi.inventory_item_id        = mmt.inventory_item_id
AND msi.organization_id          = mmt.organization_id
AND flv.lookup_code              = msi.item_type
AND flv.lookup_type              = 'ITEM_TYPE'
AND flv.attribute14             IS NOT NULL
AND ool.line_id                  = mmt.trx_source_line_id
AND mut.transaction_id           = mmt.transaction_id
AND mut.organization_id          = mmt.organization_id
AND ooh.header_id                = ool.header_id
AND oos.order_source_id          = ooh.order_source_id
AND cust_acct.cust_account_id(+) = ooh.sold_to_org_id
AND cust_acct.party_id           = party.party_id(+)
AND ooh.ship_to_org_id           = ship_su.site_use_id(+)
AND ship_su.cust_acct_site_id    = ship_cas.cust_acct_site_id(+)
AND ship_cas.party_site_id       = ship_party_site.party_site_id
AND ship_cas.attribute6         IS NOT NULL -- ship_cas.ATTRIBUTE6: Partner_code,
AND ship_party_site.location_id  = ship_loc.location_id
AND mut.inventory_item_id        = msn.inventory_item_id
AND mut.serial_number            = msn.serial_number
AND trunc(mmt.transaction_date) = trunc(sysdate)
/*AND (mmt.attribute11            IS NULL
OR p_transaction_date           IS NOT NULL
)*/ -- added by AP@BAS on 4-Dec-2014
UNION ALL
-- 2. Sales to Own distribution (Internal Order -> Inventory Org A01,B01,C01,D01,E01)
-- Yodchai L. 30-Sep-2013 join attribute from mtl_serial_numbers
SELECT TO_CHAR(ooh.order_number) order_number ,
  mmt.transaction_date document_date -- format date  'DD/MM/YYYY HH24:MI:SS'
  ,
  'A' transaction_type
  -- Fix bug Yodchai L. 20-Sep-13
  ,
  sub_tran.attribute5 partner_code ,
  msi.segment1 product_code ,
  DECODE(flv.attribute14, 'DEVICE', NULL, mut.serial_number) serial_number -- 'SIM': show serial number, 'DEVICE': Item number
  ,
  0 unit_selling_price ,
  'ERP' source_name ,
  NULL warrenty_date ,
  to_date(msn.attribute2, 'yyyy/mm/dd hh24:mi:ss') expiry_date ,
  DECODE(flv.attribute14, 'DEVICE', NVL(msn.attribute1, mut.serial_number), 'AIRCARD', msn.attribute3, NULL) imei_number -- 'SIM': show NULL, 'DEVICE': attribute1, 'AIRCARD+SIM' show attribute3 (UD item number)
  ,
  DECODE(flv.attribute14, 'DEVICE', NULL, msn.attribute1) telephone_number -- 'SIM': show attribute1, 'DEVICE': null, 'AIRCARD+SIM' show attribute1
  ,
  mmt.transaction_id
FROM mtl_material_transactions mmt ,
  mtl_transaction_types mtt ,
  mtl_system_items msi ,
  fnd_lookup_values flv ,
  oe_order_lines_all ool ,
  oe_order_headers_all ooh ,
  mtl_unit_transactions mut ,
  mtl_secondary_inventories sub ,
  mtl_secondary_inventories sub_tran ,
  mtl_serial_numbers msn
WHERE 1 = 1
  -- chagen transaction to creation date
  --and mmt.transaction_date >= v_trx_date_start
  --and mmt.transaction_date <= v_trx_date_end
--AND mmt.creation_date       >= v_from_time
--AND mmt.creation_date       <= v_to_time
AND mmt.transaction_type_id IN (62) -- 62: Transaction type = 'Int Order Intr Ship', Source = 'Internal Order'
AND mmt.transaction_type_id  = mtt.transaction_type_id
AND msi.inventory_item_id    = mmt.inventory_item_id
AND msi.organization_id      = mmt.organization_id
AND flv.lookup_code          = msi.item_type
AND flv.lookup_type          = 'ITEM_TYPE'
AND flv.attribute14         IS NOT NULL
AND ool.line_id              = mmt.trx_source_line_id
AND mut.transaction_id       = mmt.transaction_id
AND mut.organization_id      = mmt.organization_id
AND mut.inventory_item_id    = msn.inventory_item_id
AND mut.serial_number        = msn.serial_number
AND ooh.header_id            = ool.header_id
  --and mmt.organization_id = org_mast.organization_id
  --and mmt.transfer_organization_id = org_tran.organization_id(+)
AND mmt.organization_id          = sub.organization_id
AND mmt.subinventory_code        = sub.secondary_inventory_name
AND mmt.transfer_organization_id = sub_tran.organization_id(+)
AND mmt.transfer_subinventory    = sub_tran.secondary_inventory_name(+)
  -- Fix bug Yodchai L. 07-Aug-13
AND sub_tran.attribute5 IS NOT NULL
AND trunc(mmt.transaction_date) = trunc(sysdate)
/*AND (mmt.attribute11    IS NULL
OR p_transaction_date   IS NOT NULL
)*/ -- added by AP@FIN on 4-Dec-2014
-- and sub.attribute5 is not null -- don't fix RTR at source
/*                          and nvl(sub_tran.attribute5, (select min(attribute5)
from mtl_secondary_inventories s
where s.organization_id = mmt.transfer_organization_id)) is not null */
--  and nvl(sub_tran.attribute5, (select min(attribute5) from  mtl_secondary_inventories s where s.ORGANIZATION_ID =mmt.TRANSFER_ORGANIZATION_ID)) <> 5013015380 -- c_wh_rtr_code (destination is not null only)
UNION ALL
-- 3. Sales return from own distribution (return from Inventory Org A01,B01,C01,D01,E01 to A06)
-- Yodchai L. 30-Sep-2013 join attribute from mtl_serial_numbers
SELECT mmt.transaction_source_name order_number ,
  mmt.transaction_date document_date -- format date  'DD/MM/YYYY HH24:MI:SS'
  ,
  'D' transaction_type ,
  sub_tran.attribute5 partner_code ,
  msi.segment1 product_code ,
  DECODE(flv.attribute14, 'DEVICE', NULL, mut.serial_number) serial_number -- 'SIM': show serial number, 'DEVICE': Item number
  ,
  0 unit_selling_price ,
  'ERP' source_name ,
  NULL warrenty_date ,
  to_date(msn.attribute2, 'yyyy/mm/dd hh24:mi:ss') expiry_date ,
  DECODE(flv.attribute14, 'DEVICE', NVL(msn.attribute1, mut.serial_number), 'AIRCARD', msn.attribute3, NULL) imei_number -- 'SIM': show NULL, 'DEVICE': attribute1, 'AIRCARD+SIM' show attribute3 (UD item number)
  ,
  DECODE(flv.attribute14, 'DEVICE', NULL, msn.attribute1) telephone_number -- 'SIM': show attribute1, 'DEVICE': null, 'AIRCARD+SIM' show attribute1
  ,
  mmt.transaction_id
FROM mtl_material_transactions mmt ,
  mtl_transaction_types mtt ,
  mtl_system_items msi ,
  fnd_lookup_values flv ,
  mtl_parameters org_mast ,
  mtl_parameters org_tran ,
  mtl_unit_transactions mut ,
  mtl_secondary_inventories sub ,
  mtl_secondary_inventories sub_tran ,
  mtl_serial_numbers msn
WHERE 1 = 1
  -- chagen transaction to creation date
  --and mmt.transaction_date >= v_trx_date_start
  --and mmt.transaction_date <= v_trx_date_end
--AND mmt.creation_date           >= v_from_time
--AND mmt.creation_date           <= v_to_time
AND mmt.transaction_type_id     IN (164) --'Receive return from shop (I)'
AND mtt.transaction_type_id      = mmt.transaction_type_id
AND msi.inventory_item_id        = mmt.inventory_item_id
AND mmt.organization_id          = msi.organization_id
AND flv.lookup_code              = msi.item_type
AND flv.lookup_type              = 'ITEM_TYPE'
AND flv.attribute14             IS NOT NULL
AND mmt.organization_id          = org_mast.organization_id
AND mmt.transfer_organization_id = org_tran.organization_id(+)
AND mut.transaction_id           = mmt.transaction_id
AND mut.organization_id          = mmt.organization_id
AND mut.inventory_item_id        = msn.inventory_item_id
AND mut.serial_number            = msn.serial_number
AND mmt.organization_id          = sub.organization_id
AND mmt.subinventory_code        = sub.secondary_inventory_name
AND mmt.transaction_source_name LIKE '44%' -- for own distribute beginning with '44'
AND mmt.transfer_organization_id = sub_tran.organization_id
  --and substr(mmt.transaction_source_name, 3, 5) = sub_tran.secondary_inventory_name
  -- Update Yodchai L . To support DTN, PAY, TYP doc no format change 01-Aug-13
AND
  CASE
    WHEN ascii(upper(SUBSTR(mmt.transaction_source_name, 3, 1))) BETWEEN 65 AND 90
    THEN SUBSTR(mmt.transaction_source_name, 3, 6)
    ELSE SUBSTR(mmt.transaction_source_name, 3, 5)
  END = sub_tran.secondary_inventory_name
  --and sub.attribute5 is not null -- (don't concern RTR at destination because this case is return)
AND sub_tran.attribute5 IS NOT NULL
AND trunc(mmt.transaction_date) = trunc(sysdate)
  -- and sub_tran.attribute5 <> c_wh_rtr_code
/*AND (mmt.attribute11  IS NULL
OR p_transaction_date IS NOT NULL
)*/ -- added by AP@BAS on 4-Dec-2014
ORDER BY document_date ,
  order_number ,
  partner_code ,
  product_code ,
  imei_number ,
  serial_number;