select count(*) from V$SESSTAT;

sho parameter open_cursors;

select * from ( select ss.value, sn.name, ss.sid
 from v$sesstat ss, v$statname sn
 where ss.statistic# = sn.statistic#
 and sn.name like '%opened cursors current%'
 order by value desc) where rownum < 11 ;
 
 
 select sid, status, event,  blocking_session "blk_sesn", prev_sql_id  "SQL_ID"  from v$session where sid=392;