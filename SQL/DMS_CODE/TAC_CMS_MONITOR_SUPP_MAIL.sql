create or replace PROCEDURE TAC_CMS_MONITOR_SUPP_MAIL(
    errbuf OUT VARCHAR2,
    retcode OUT NUMBER)
AS
-- Create by T889774@BAS (Operation) Create Date : 06/08/2018

TYPE v_group_id
IS
  TABLE OF DTAC_AP_INVOICES_TEMP_INT.GROUP_ID%TYPE;
  t_group_id v_group_id;
TYPE v_txt_supl_code
IS
  TABLE OF DTAC_AP_INVOICES_TEMP_INT.ACT_VENDOR_NUM%TYPE;
  t_txt_supl_code v_txt_supl_code;
TYPE v_vendor_site_code
IS
  TABLE OF DTAC_AP_INVOICES_TEMP_INT.VENDOR_SITE_CODE%TYPE;
  t_vendor_site_code v_vendor_site_code;
TYPE v_erp_retailercode
IS
  TABLE OF PO_VENDOR_SITES_ALL.ATTRIBUTE1%TYPE;
  t_erp_retailercode v_erp_retailercode;
TYPE v_vat_reg
IS
  TABLE OF PO_VENDOR_SITES_ALL.VAT_REGISTRATION_NUM%TYPE;
  t_vat_reg v_vat_reg;
TYPE v_vendor_id
IS
  TABLE OF PO_VENDOR_SITES_ALL.VENDOR_ID%TYPE;
  t_vendor_id v_vendor_id;
TYPE v_inv_num
IS
  TABLE OF DTAC_AP_INVOICES_TEMP_INT.INVOICE_NUM%TYPE;
  t_inv_num v_inv_num;
  /* Mailing Details */
  v_From VARCHAR2(80) := 'ERPOperationSupport@dtac.co.th'; --'EnterpriseApplicationSupportBOSDept@dtac.co.th';
  v_Recipient_To VARCHAR2(80) := 'DMSOperationSupport@dtac.co.th';
  v_Recipient_CC VARCHAR2(80)  := 'ERPOperationSupport@dtac.co.th';
  v_Subject_E VARCHAR2(100) := 'Monitor Correctness of CMS Supplier Interface Data as of '|| TO_CHAR(FROM_TZ(CAST(SYSDATE AS TIMESTAMP), 'Asia/Bangkok'),'DD Mon YYYY hh24:mi:ss','NLS_DATE_LANGUAGE=AMERICAN') ||' : ERROR';
  v_Subject_S VARCHAR2(100) := 'Monitor Correctness of CMS Supplier Interface Data as of '|| TO_CHAR(FROM_TZ(CAST(SYSDATE AS TIMESTAMP), 'Asia/Bangkok'),'DD Mon YYYY hh24:mi:ss','NLS_DATE_LANGUAGE=AMERICAN') ||' : SUCCESS';
  v_Mail_Host VARCHAR2(30) := 'mail-gw.tac.co.th';
  -- v_Cc1    VARCHAR2(100) := '';
  -- v_Cc2    VARCHAR2(100) := '';
  --v_Cc2    VARCHAR2(100) := 'IS-ERP-Production@dtac.co.th';
  --v_Cc3    VARCHAR2(100) := 'IS-ERP-Financial@dtac.co.th';
  v_Mail_Conn utl_smtp.Connection;
  crlf      VARCHAR2(2)   := CHR(10); --chr(13)||chr(10);
  v_oper    VARCHAR2(100) := 'ERP Operation Support Team';
  p_message VARCHAR2(30000);
  s_subject VARCHAR2(30000);
  IS_ERROR  VARCHAR2(2) := '0';
BEGIN
  p_message := UTL_TCP.crlf || 'I------------------------------------------------------------------------I' || UTL_TCP.crlf || UTL_TCP.crlf;
  p_message := p_message || ' Monitor Correctness of CMS Supplier Interface Data ' || UTL_TCP.crlf || UTL_TCP.crlf;
  p_message := p_message || 'I------------------------------------------------------------------------I' || UTL_TCP.crlf;
  fnd_file.put_line(fnd_file.log,'I---------------------------------------------------------------I');
  fnd_file.put_line(fnd_file.log,' Monitor Correctness of CMS Supplier Interface Data');
  fnd_file.put_line(fnd_file.log,'I---------------------------------------------------------------I');
  SELECT DISTINCT GROUP_ID,
    TXT_SUPL_CODE,
    VENDOR_SITE_CODE BULK COLLECT
  INTO t_group_id,
    t_txt_supl_code,
    t_vendor_site_code
  FROM TAC_CHECK_CMS_ERR_V
  WHERE ERP_SUPPLIER_CODE IS NULL;
  --dbms_output.put_line('1.CHECKING SITE CODE IS NULL');
  p_message          := p_message || UTL_TCP.crlf || '1.CHECKING SITE CODE IS NULL' || UTL_TCP.crlf;
  FND_FILE.PUT_LINE(FND_FILE.LOG, '1.CHECKING SITE CODE IS NULL' );
  IF t_group_id.count > 0 THEN
    IS_ERROR         := '1';
    --dbms_output.put_line('GROUP_ID|ERP_VENDOR_CODE|VENDOR_SITE_CODE');
    p_message := p_message || 'GROUP_ID|ERP_VENDOR_CODE|VENDOR_SITE_CODE' || UTL_TCP.crlf;
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'GROUP_ID|ERP_VENDOR_CODE|VENDOR_SITE_CODE' );
    FOR cTrx IN 1 .. t_group_id.count
    LOOP
      --dbms_output.put_line(t_group_id(cTrx)|| '|' || t_txt_supl_code(cTrx) || '|' || t_vendor_site_code(cTrx));
      p_message := p_message || t_group_id(cTrx)|| '|' || t_txt_supl_code(cTrx) || '|' || t_vendor_site_code(cTrx)|| UTL_TCP.crlf;
      FND_FILE.PUT_LINE(FND_FILE.LOG, t_group_id(cTrx)|| '|' || t_txt_supl_code(cTrx) || '|' || t_vendor_site_code(cTrx)|| UTL_TCP.crlf );
    END LOOP;
  ELSE
    p_message := p_message || 'No Data Found' || UTL_TCP.crlf;
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'No Data Found' );
    --dbms_output.put_line('No Data Found');
  END IF;
  SELECT DISTINCT GROUP_ID,
    TXT_SUPL_CODE,
    VENDOR_SITE_CODE,
    ERP_RETAILERCODE BULK COLLECT
  INTO t_group_id,
    t_txt_supl_code,
    t_vendor_site_code,
    t_erp_retailercode
  FROM TAC_CHECK_CMS_ERR_V
  WHERE ERP_BANKACCOUNT IS NULL;
  --dbms_output.put_line('2.CHECKING BANK CODE IS NULL');
  FND_FILE.PUT_LINE(FND_FILE.LOG, '2.CHECKING BANK CODE IS NULL' );
  p_message          := p_message || UTL_TCP.crlf || '2.CHECKING BANK CODE IS NULL' || UTL_TCP.crlf;
  IF t_group_id.count > 0 THEN
    IS_ERROR         := '1';
    --dbms_output.put_line('GROUP_ID|ERP_VENDOR_CODE|VENDOR_SITE_CODE|RETAILERCODE');
    p_message := p_message || 'GROUP_ID|ERP_VENDOR_CODE|VENDOR_SITE_CODE|RETAILERCODE' || UTL_TCP.crlf;
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'GROUP_ID|ERP_VENDOR_CODE|VENDOR_SITE_CODE|RETAILERCODE' );
    FOR cTrx IN 1 .. t_group_id.count
    LOOP
      --dbms_output.put_line(t_group_id(cTrx)|| '|' || t_txt_supl_code(cTrx) || '|' || t_vendor_site_code(cTrx)|| '|' || t_erp_retailercode(cTrx));
      p_message := p_message || t_group_id(cTrx)|| '|' || t_txt_supl_code(cTrx) || '|' || t_vendor_site_code(cTrx)|| '|' || t_erp_retailercode(cTrx)|| UTL_TCP.crlf;
      FND_FILE.PUT_LINE(FND_FILE.LOG, t_group_id(cTrx)|| '|' || t_txt_supl_code(cTrx) || '|' || t_vendor_site_code(cTrx)|| '|' || t_erp_retailercode(cTrx) );
    END LOOP;
  ELSE
    --dbms_output.put_line('No Data Found');
    p_message := p_message || 'No Data Found' || UTL_TCP.crlf;
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'No Data Found' );
  END IF;
  SELECT DISTINCT GROUP_ID,
    TXT_SUPL_CODE,
    VENDOR_SITE_CODE,
    ERP_RETAILERCODE,
    ERP_VAT_REGIST,
    ERP_VENDOR_ID BULK COLLECT
  INTO t_group_id,
    t_txt_supl_code,
    t_vendor_site_code,
    t_erp_retailercode,
    t_vat_reg,
    t_vendor_id
  FROM TAC_CHECK_CMS_ERR_V
  WHERE END_DATE_ACTIVE IS NOT NULL
  OR INACTIVE_DATE      IS NOT NULL;
  --dbms_output.put_line('3.CHECKING VENDOR END DATE');
  FND_FILE.PUT_LINE(FND_FILE.LOG, '3.CHECKING VENDOR END DATE' );
  p_message          := p_message || UTL_TCP.crlf || '3.CHECKING VENDOR END DATE' || UTL_TCP.crlf;
  IF t_group_id.count > 0 THEN
    IS_ERROR         := '1';
    --dbms_output.put_line('GROUP_ID|ERP_VENDOR_CODE|VENDOR_SITE_CODE|RETAILERCODE|VAT_REGISTRATION_NUM|ERP_VENDOR_ID');
    p_message := p_message || 'GROUP_ID|ERP_VENDOR_CODE|VENDOR_SITE_CODE|RETAILERCODE|VAT_REGISTRATION_NUM|ERP_VENDOR_ID' || UTL_TCP.crlf;
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'GROUP_ID|ERP_VENDOR_CODE|VENDOR_SITE_CODE|RETAILERCODE|VAT_REGISTRATION_NUM|ERP_VENDOR_ID' );
    FOR cTrx IN 1 .. t_group_id.count
    LOOP
      --dbms_output.put_line(t_group_id(cTrx)|| '|' || t_txt_supl_code(cTrx) || '|' || t_vendor_site_code(cTrx)|| '|' || t_erp_retailercode(cTrx)|| '|' || t_vat_reg(cTrx)|| '|' || t_vendor_id(cTrx));
      p_message := p_message || t_group_id(cTrx)|| '|' || t_txt_supl_code(cTrx) || '|' || t_vendor_site_code(cTrx)|| '|' || t_erp_retailercode(cTrx)|| '|' || t_vat_reg(cTrx)|| '|' || t_vendor_id(cTrx) || UTL_TCP.crlf;
      FND_FILE.PUT_LINE(FND_FILE.LOG, t_group_id(cTrx)|| '|' || t_txt_supl_code(cTrx) || '|' || t_vendor_site_code(cTrx)|| '|' || t_erp_retailercode(cTrx)|| '|' || t_vat_reg(cTrx)|| '|' || t_vendor_id(cTrx) );
    END LOOP;
  ELSE
    --dbms_output.put_line('No Data Found');
    p_message := p_message || 'No Data Found' || UTL_TCP.crlf;
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'No Data Found' );
  END IF;
  SELECT DISTINCT group_id,
    INVOICE_NUM BULK COLLECT
  INTO t_group_id,
    t_inv_num
  FROM DTAC_AP_INVOICES_TEMP_INT aa
  WHERE group_id IN
    (SELECT DISTINCT group_id group_id
    FROM WMUSER.DTAC_AP_INVOICES_TEMP_INT
    WHERE group_id LIKE 'CMS%'
    AND regexp_substr(group_id,'[^_]+', 1, 1) =
      (SELECT MAX(regexp_substr(group_id,'[^_]+', 1, 1))
      FROM DTAC_AP_INVOICES_TEMP_INT
      WHERE group_id LIKE 'CMS%'
      )
      AND INVOICE_DATE  >= (sysdate-1/24)
    );
  --dbms_output.put_line('4.SEND MAIL TO ACCOUNT');
  FND_FILE.PUT_LINE(FND_FILE.LOG, '4.SEND MAIL TO ACCOUNT' );
  p_message          := p_message || UTL_TCP.crlf || '4.SEND MAIL TO ACCOUNT' || UTL_TCP.crlf;
  IF t_group_id.count > 0 THEN
    --dbms_output.put_line('GROUP_ID|INVOICE_NUM');
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'GROUP_ID|INVOICE_NUM' );
    p_message := p_message || 'GROUP_ID|INVOICE_NUM' || UTL_TCP.crlf;
    FOR cTrx IN 1 .. t_group_id.count
    LOOP
      dbms_output.put_line(t_group_id(cTrx)|| '|' || t_inv_num(cTrx));
      p_message          := p_message || t_group_id(cTrx)|| '|' || t_inv_num(cTrx) || UTL_TCP.crlf;
      FND_FILE.PUT_LINE(FND_FILE.LOG, t_group_id(cTrx)|| '|' || t_inv_num(cTrx) );
    END LOOP;
    v_Mail_Conn := utl_smtp.Open_Connection(v_Mail_Host, 25);
    utl_smtp.Helo(v_Mail_Conn, v_Mail_Host);
    utl_smtp.Mail(v_Mail_Conn, v_From);
    utl_smtp.Rcpt(v_Mail_Conn, v_Recipient_To);
    utl_smtp.Rcpt(v_Mail_Conn, v_Recipient_CC);
    UTL_SMTP.open_data(v_Mail_Conn);
    UTL_SMTP.write_data(v_Mail_Conn, 'Date: ' || TO_CHAR(FROM_TZ(CAST(SYSDATE AS TIMESTAMP), 'Asia/Bangkok'),'Dy, DD Mon YYYY hh24:mi:ss TZHTZM','NLS_DATE_LANGUAGE=AMERICAN') || UTL_TCP.crlf);
    UTL_SMTP.write_data(v_Mail_Conn, 'To: ' || v_Recipient_To || UTL_TCP.crlf);
    UTL_SMTP.write_data(v_Mail_Conn, 'CC: ' || v_Recipient_CC || UTL_TCP.crlf);
    UTL_SMTP.write_data(v_Mail_Conn, 'From: ' || v_From || UTL_TCP.crlf);
    IF IS_ERROR = '1' THEN
      UTL_SMTP.write_data(v_Mail_Conn, 'Subject: ' || v_Subject_E || UTL_TCP.crlf);
      UTL_SMTP.write_data(v_Mail_Conn, crlf || crlf || 'Dear DMS Operation Support,'|| crlf || crlf);
      UTL_SMTP.write_data(v_Mail_Conn, p_message || UTL_TCP.crlf || UTL_TCP.crlf || UTL_TCP.crlf);
      --UTL_SMTP.write_data(v_Mail_Conn, TO_CHAR(ERR) || ' Concurrent requests have been completed with ERROR.' || UTL_TCP.crlf || UTL_TCP.crlf);
      UTL_SMTP.write_data(v_Mail_Conn, 'Request ID : ' || FND_GLOBAL.CONC_REQUEST_ID);
      UTL_SMTP.write_data(v_Mail_Conn, crlf || crlf || crlf || 'Best Regards,'|| crlf || crlf || v_oper|| crlf || crlf || 'Date: ' || TO_CHAR(sysdate, 'Dy, DD Mon YYYY hh24:mi:ss'));
    ELSE
      UTL_SMTP.write_data(v_Mail_Conn, 'Subject: ' || v_Subject_S || UTL_TCP.crlf);
      UTL_SMTP.write_data(v_Mail_Conn, crlf || crlf || 'Dear DMS Operation Support,'|| crlf || crlf);
      --UTL_SMTP.write_data(v_Mail_Conn, crlf || crlf || 'Dear DMS Operation Support,'|| crlf || crlf || '      TAC CONCURRENT MONITOR has finished succesfully without ERROR. Please check the log of concurrent request: '|| FND_GLOBAL.CONC_REQUEST_ID ||'-TAC CONCURRENT MONITOR DETAIL MAIL for futher details.'|| crlf || crlf || 'ERP Oracle Applications :-'|| UTL_TCP.crlf || 'URL: http://cygnus3.tac.co.th:8000/OA_HTML/OA.jsp?OAFunc=OAHOMEPAGE' || UTL_TCP.crlf);
      UTL_SMTP.write_data(v_Mail_Conn, p_message || UTL_TCP.crlf || UTL_TCP.crlf || UTL_TCP.crlf);
      --UTL_SMTP.write_data(v_Mail_Conn, TO_CHAR(ERR) || ' Concurrent requests have been completed with ERROR.' || UTL_TCP.crlf || UTL_TCP.crlf);
      UTL_SMTP.write_data(v_Mail_Conn, 'Request ID : ' || FND_GLOBAL.CONC_REQUEST_ID);
      UTL_SMTP.write_data(v_Mail_Conn, crlf || crlf || crlf || 'Best Regards,'|| crlf || crlf || v_oper|| crlf || crlf || 'Date: ' || TO_CHAR(sysdate, 'Dy, DD Mon YYYY hh24:mi:ss'));
    END IF;
    UTL_SMTP.close_data(v_Mail_Conn);
    UTL_SMTP.quit(v_Mail_Conn);
    -- ELSE
    -- dbms_output.put_line('No Data Found');
  END IF;
EXCEPTION
WHEN utl_smtp.Transient_Error OR utl_smtp.Permanent_Error THEN
  raise_application_error(-20000, 'Unable to send mail: '||sqlerrm);
END;