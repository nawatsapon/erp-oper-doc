--Update Flag in Sa_Transaction and Sc_Transaction
update SA_TRANSACTION
set INTERFACE_STATSUS              = 'N'
where 0                            =0
and TRUNC(SA_TRANSACTION.DOC_DATE) = TO_DATE('30/01/2016', 'dd/MM/yyyy')
and SA_TRANSACTION.OU_CODE         = 'TAC';
update SA_TRANSACTION
set INTERFACE_STATSUS              = 'N'
where 0                            =0
and TRUNC(SA_TRANSACTION.DOC_DATE) = TO_DATE('31/01/2016', 'dd/MM/yyyy')
and SA_TRANSACTION.OU_CODE         = 'TAC';
update SC_TRANSACTION
set INTERFACE_STATSUS              = 'N'
where 0                            =0
and TRUNC(SC_TRANSACTION.DOC_DATE) = TO_DATE('30/01/2016', 'dd/MM/yyyy')
and SC_TRANSACTION.OU_CODE         = 'TAC';
update SC_TRANSACTION
set INTERFACE_STATSUS              = 'N'
where 0                            =0
and TRUNC(SC_TRANSACTION.DOC_DATE) = TO_DATE('31/01/2016', 'dd/MM/yyyy')
and SC_TRANSACTION.OU_CODE         = 'TAC';

--Delete From SA_EDIT_JV
DELETE
FROM SA_EDIT_JV
WHERE 0             =0
AND ACCOUNTING_DATE = to_date('30/01/2016','dd/MM/yyyy')
AND OU_CODE         = 'TAC';
DELETE
FROM SA_EDIT_JV
WHERE 0             =0
AND ACCOUNTING_DATE = to_date('31/01/2016','dd/MM/yyyy')
AND OU_CODE         = 'TAC';

--Delete From GL in POS and ERP
DELETE
FROM GL_INTERFACE
WHERE 0                    =0
AND USER_JE_SOURCE_NAME    = 'POINT OF SALES'
AND TRUNC(ACCOUNTING_DATE) = TO_DATE('30/01/2016','dd/MM/yyyy')
AND (REFERENCE4 NOT LIKE 'D%'
OR REFERENCE4 NOT LIKE 'P%');
DELETE
FROM GL_INTERFACE
WHERE 0                    =0
AND USER_JE_SOURCE_NAME    = 'POINT OF SALES'
AND TRUNC(ACCOUNTING_DATE) = TO_DATE('31/01/2016','dd/MM/yyyy')
AND (REFERENCE4 NOT LIKE 'D%'
OR REFERENCE4 NOT LIKE 'P%');
