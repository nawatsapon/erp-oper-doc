SELECT count(rct.TRX_NUMBER) COUNT_INV,
  trunc(rct.TRX_DATE,'MM') mon
  /*,cust.ship_to_location,
  tt.NAME INV_TYPE,
  
  org.LOCATION_CODE,
  msi.CONCATENATED_SEGMENTS C_Product_Code ,
  msi.DESCRIPTION C_Description ,
  DECODE(msi.PRIMARY_UOM_CODE,NVL(rctl.UOM_CODE,msi.PRIMARY_UOM_CODE),rctl.QUANTITY_INVOICED,inv_convert.inv_um_convert(rctl.INVENTORY_ITEM_ID,2,rctl.QUANTITY_INVOICED,rctl.UOM_CODE,msi.PRIMARY_UOM_CODE,NULL,NULL)) C_Inv_Qty ,
  NVL(rctl.EXTENDED_AMOUNT,0)+ NVL(ltax.EXTENDED_AMOUNT,0) C_Inv_Amount ,
  to_number(NULL) C_CN_Qty ,
  to_Number(NULL) C_CN_Amount,
  NULL CN_NO,
  NULL CN_TYPE,
  NULL CN_DATE*/
FROM ra_customer_trx_all rct ,
  ra_customer_trx_lines_all rctl ,
  ra_customer_trx_lines_all ltax ,
  ra_cust_trx_types_all tt ,
  mtl_system_items_vl msi ,
  mtl_item_categories mic ,
  mtl_categories mc ,
  mtl_category_sets mcs ,
  mtl_units_of_measure_vl uom ,
  HR_ORGANIZATION_UNITS_V org,
  (SELECT DISTINCT hca.account_number customer_number,
    hcsua1.LOCATION bill_to_location,
    hcsua2.LOCATION ship_to_location
  FROM hz_parties hp,
    hz_party_sites hps,
    hz_cust_accounts hca,
    hz_cust_acct_sites_all hcasa1,
    hz_cust_site_uses_all hcsua1,
    hz_locations hl,
    fnd_territories_tl ftt,
    hz_cust_acct_sites_all hcasa2,
    hz_cust_site_uses_all hcsua2,
    hr_operating_units hrou,
    AR_CUSTOMERS ac
  WHERE hp.party_id               = hps.party_id(+)
  AND hp.party_id                 = hca.party_id(+)
  AND hcasa1.party_site_id(+)     = hps.party_site_id
  AND hcasa2.party_site_id(+)     = hps.party_site_id
  AND hcsua1.cust_acct_site_id(+) = hcasa1.cust_acct_site_id
  AND hcsua2.cust_acct_site_id(+) = hcasa2.cust_acct_site_id
  AND hcsua1.site_use_code(+)     = 'BILL_TO'
  AND hcsua2.site_use_code(+)     = 'SHIP_TO'
    --            AND hcasa1.org_id(+) = fnd_profile.VALUE ('org_id')
    --            AND hcasa2.org_id(+) = fnd_profile.VALUE ('org_id')
  AND hps.location_id    = hl.location_id
  AND hl.country         = ftt.territory_code
  AND hca.account_number = ac.CUSTOMER_NUMBER
  AND hcsua2.org_id      = hrou.ORGANIZATION_ID
  AND ftt.LANGUAGE       = USERENV ('LANG')
    --and hca.account_number = '1357'
  ) cust
WHERE rct.CUSTOMER_TRX_ID      = rctl.CUSTOMER_TRX_ID
AND rct.COMPLETE_FLAG          = 'Y'
AND rct.CUST_TRX_TYPE_ID       = tt.CUST_TRX_TYPE_ID
AND rct.ORG_ID                 = org.organization_id
AND tt.TYPE                   IN ('INV','DM')
AND rctl.LINE_TYPE             = 'LINE'
AND rctl.INVENTORY_ITEM_ID     = msi.inventory_item_id
AND rctl.warehouse_ID          = msi.organization_id
AND rctl.CUSTOMER_TRX_LINE_ID  = ltax.LINK_TO_CUST_TRX_LINE_ID
AND msi.inventory_item_id      = mic.inventory_item_id
AND msi.Organization_Id        = mic.Organization_Id
AND mic.category_set_id        = mcs.category_set_id
AND mcs.CATEGORY_SET_NAME      = 'Inventory'
AND mic.category_id            = mc.CATEGORY_ID
AND rctl.UOM_CODE              = uom.UOM_CODE(+)
AND RCT.SHIP_TO_CUSTOMER_ID = cust.customer_number
--AND msi.CONCATENATED_SEGMENTS IN ('PRCO0000131', 'PRCO0000141', 'PRCO0000161', 'PRCO0000361', 'PRCO0000411', 'PRCO0000471', 'PRCO0000531', 'PRCO0000771', 'PRCO0000781', 'PRCO0001021', 'PRCO0001151', 'PRCO0001251', 'S18WPA81001', 'S18WPB07001', 'S18WPB60001', 'PRCO0000132', 'PRCO0000134', 'PRCO0000136', 'PRCO0000142', 'PRCO0000144', 'PRCO0000146', 'PRCO0000421', 'PRCO0000941', 'PRCO0001281', 'S18WPA67001', 'S18WPA87001', 'S18WPB13001')
AND TRUNC(rct.trx_date)       >= to_date('01/01/2017','dd/mm/yyyy')
AND TRUNC(rct.trx_date)       <= to_date('30/04/2017','dd/mm/yyyy')
and (cust.ship_to_location like 'H3%' or  cust.ship_to_location like 'H1%' or  cust.ship_to_location like 'N1%')
group by trunc(rct.TRX_DATE,'MM')
