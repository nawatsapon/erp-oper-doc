SELECT REPLACE( DESCRIPTION, chr(10),'') rep_des,
  FA_ADDITIONS_TL.*
FROM FA_ADDITIONS_TL
WHERE 0                         =0
AND instr(DESCRIPTION, chr(10)) > 0
AND asset_id                   IN
  ( SELECT asset_id FROM FA_ADDITIONS_B WHERE asset_number = '10727682'
  );
UPDATE FA_ADDITIONS_TL
SET DESCRIPTION                   = REPLACE( DESCRIPTION, chr(10),'')
WHERE instr(DESCRIPTION, chr(10)) > 0;
UPDATE FA_ADDITIONS_TL
SET DESCRIPTION                   = REPLACE( DESCRIPTION, chr(13),'')
WHERE instr(DESCRIPTION, chr(13)) > 0;