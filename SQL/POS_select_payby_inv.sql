SELECT SA_TRANSACTION.DOC_DATE INVOICE_DATE,
  SA_TRANSACTION.DOC_NO INVOICE_NO,
  SA_TRANS_DTL.ITEM_CODE PRODUCT_CODE,
  IN_ITEM.ITEM_DESC DESCRIPTION,
  SA_TRANS_DTL.PRICE_LIST_CODE,
  --SA_TRANS_PAY.PAYMENT_CODE,
  --SA_PAYMENT.PAYMENT_DESC,
  SA_TRANSACTION.CUST_NAME,
  SA_TRANSACTION.SALESPERSON USRNAME,
  SA_SALESPERSON.PERSON_NAME USER_FULLNAME,
  SA_TRANSACTION.SUBINV_CODE,
  PB_SUBINV.SUBINV_DESC,
  SA_TRANS_DTL.UNITPRICE,
  SA_TRANS_DTL.QTY
FROM SA_TRANSACTION,
  SA_TRANS_DTL,
  --SA_TRANS_PAY,
  SA_SALESPERSON ,
  IN_ITEM,
  --SA_PAYMENT,
  PB_SUBINV
WHERE 0                        =0
AND SA_TRANSACTION.OU_CODE     = SA_TRANS_DTL.OU_CODE
AND SA_TRANSACTION.SUBINV_CODE = SA_TRANS_DTL.SUBINV_CODE
AND SA_TRANSACTION.DOC_NO      = SA_TRANS_DTL.DOC_NO
  --AND SA_TRANSACTION.OU_CODE           = SA_TRANS_PAY.OU_CODE
  --AND SA_TRANSACTION.SUBINV_CODE       = SA_TRANS_PAY.SUBINV_CODE
  --AND SA_TRANSACTION.DOC_NO            = SA_TRANS_PAY.DOC_NO
AND SA_TRANSACTION.SALESPERSON = SA_SALESPERSON.PERSON_CODE(+)
AND SA_TRANS_DTL.ITEM_CODE     = IN_ITEM.ITEM_CODE(+)
AND SA_TRANS_DTL.OU_CODE       = IN_ITEM.OU_CODE(+)
  --AND SA_TRANS_PAY.PAYMENT_CODE        = SA_PAYMENT.PAYMENT_CODE
AND SA_TRANSACTION.OU_CODE          = PB_SUBINV.OU_CODE
AND SA_TRANSACTION.SUBINV_CODE      = PB_SUBINV.SUBINV_CODE
AND TRUNC(SA_TRANSACTION.DOC_DATE) >= to_date('01/12/2017','dd/mm/yyyy')
AND TRUNC(SA_TRANSACTION.DOC_DATE) <= to_date('31/12/2017','dd/mm/yyyy')
AND SA_TRANSACTION.OU_CODE = 'PAY'
AND SA_TRANSACTION.DOC_TYPE         = 1
--ANd SA_TRANSACTION.SUBINV_CODE in  ('02131')
ORDER BY SA_TRANSACTION.DOC_NO
