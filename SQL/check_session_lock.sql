--1) To determine what session has a lock on this record, please execute the following steps:
  --       a) Run the following to determine what tables are locked:
                --- --- ---

                SELECT *
                FROM v$locked_object a, dba_objects b
                WHERE a.object_id = b.object_id
                AND b.object_name like '%'
                ORDER BY b.object_name;
                --- --- ---
   --      b) Look at the results and insert whatever AP_% tables are returned from a) into the script below:
                --- --- ---

                SELECT l.*, o.owner object_owner, o.object_name
                FROM SYS.all_objects o, v$lock l
                WHERE l.TYPE = 'TM'
                AND o.object_id = l.id1
                AND o.object_name in ('IN_MULTISERIAL_TEMP')
                order by SID
                ;
                --- ---

                SELECT SID, SERIAL#
                FROM v$session
                WHERE SID = 1285;
                --- --- ---
            
 -- 2) Once the locking sessions have been identified, please use the below command to kill such sessions.
            --- --- ---

            ALTER SYSTEM KILL SESSION 'sid,serial#' IMMEDIATE;
