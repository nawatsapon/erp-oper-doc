select V_OUTPUT
from
  (select case
      when PO_NO like '991%'
      then 'PO-HO'
      when PO_NO like '992%'
      then 'PO-PSL'
      when PO_NO like '993%'
      then 'PO-KKC'
      when PO_NO like '994%'
      then 'PO-SRC'
      when PO_NO like '995%'
      then 'PO-SRT'
      when PO_NO like '801%'
      then 'DTN-PO'
      when PO_NO like '121%'
      then 'PB_PO'
      when PO_NO like '151%'
      then 'PO-DTAC-A'
      when PO_NO like '601%'
      then 'PO-DTAC-I'
      when PO_NO like '701%'
      then 'PO-DTAC-W'
    end
    || '|'
    || TO_CHAR(PO_LIST.PO_NO)
    || '|'
    || TO_CHAR(PO_LIST.LINE_NUM)
    || '|'
    || TO_CHAR(PO_LIST.APPROVE_DATE)
    || '|'
    || PO_LIST.AUTHORIZATION_STATUS
    || '|'
    || TO_CHAR(PO_LIST.NONRECOVERABLE_TAX) 
    || '|'
    || TO_CHAR(PO_LIST.RECOVERABLE_TAX)
    || '|'
    || TO_CHAR(PO_LIST.TAX_CODE_ID)
    V_OUTPUT
    --PO_LIST.*
  from
    ( select distinct PO_HEADERS_ALL.SEGMENT1 PO_NO,
      PO_LINES_ALL.LINE_NUM LINE_NUM,
      TO_CHAR(PO_HEADERS_ALL.APPROVED_DATE,'dd/mm/yyyy HH24:MI:SS') APPROVE_DATE,
      PO_HEADERS_ALL.AUTHORIZATION_STATUS,
      PO_DISTRIBUTIONS_ALL.NONRECOVERABLE_TAX,
      PO_DISTRIBUTIONS_ALL.RECOVERABLE_TAX,
      PO_LINE_LOCATIONS_ALL.TAX_CODE_ID
    from PO.PO_HEADERS_ALL,
      PO.PO_LINES_ALL,
      PO.PO_LINE_LOCATIONS_ALL,
      PO.PO_DISTRIBUTIONS_ALL
    where PO_HEADERS_ALL.PO_HEADER_ID        = PO_LINES_ALL.PO_HEADER_ID
    AND PO_HEADERS_ALL.PO_HEADER_ID  = PO_LINE_LOCATIONS_ALL.PO_HEADER_ID
    and PO_LINES_ALL.PO_LINE_ID = PO_LINE_LOCATIONS_ALL.PO_LINE_ID
    and PO_LINES_ALL.PO_LINE_ID              = PO_DISTRIBUTIONS_ALL.PO_LINE_ID
    and TRUNC(PO_HEADERS_ALL.APPROVED_DATE) >= sysdate-240
    and PO_HEADERS_ALL.TYPE_LOOKUP_CODE      = 'STANDARD'
    and PO_LINES_ALL.FROM_HEADER_ID         is not null
    and (PO_DISTRIBUTIONS_ALL.RECOVERY_RATE is null
    or PO_DISTRIBUTIONS_ALL.RECOVERY_RATE    < 100)
    and PO_HEADERS_ALL.CANCEL_FLAG          is null -- SWE 280509
    and PO_HEADERS_ALL.TYPE_LOOKUP_CODE     <> 'PLANNED'
    order by 1,2,3,4
    ) PO_LIST  
  );
