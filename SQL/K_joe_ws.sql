SELECT rsh.receipt_num GRN_NUMBER,--rt.*
  DECODE(are.tax_entity_id,10000,'DTAC',10050,'DTN',10094,'PSB') Company_Code,
  poh.segment1
  ||DECODE(po_release.release_num,NULL,NULL,'-'
  ||po_release.release_num) PO_Number,
  -- rt.*
  pol.line_num PO_Line_Number,
  rsl.shipment_line_status_code GRN_STATUS,
  rsl.line_num GRN_LINE_NUMBER,
  item.item_code ITEM_CODE,
  rsl.item_description LINE_DESCRIPTION,
  rsl.quantity_received GRN_QTY, -- if rsl.destination_type_code='INVENTORY'  then qty if EXPENSE THEN AMOUNT
  rsl.unit_of_measure GRN_UOM,
  (NVL(rsl.quantity_received,0)*NVL(pol.unit_price,0)) GRN_AMOUNT,--rsl.quantity_received GRN_AMOUNT,
  poh.currency_code GRN_CURRENCY,
  0 TOTAL_GRN_AMOUNT,
  org.organization_code ORG_CODE,
  pv.segment1 VENDOR_NUMBER,
  pvs.vendor_site_code VENDOR_SITE_CODE,
  payterm.name PAYMENT_TERM,
  SUM(pll.quantity-pll.quantity_received-pll.quantity_rejected-pll.quantity_cancelled)*NVL(pol.unit_price,0) PO_Remaining_Amount
  -- bulk collect into o_grn_infomation
FROM rcv_shipment_headers rsh ,
  rcv_shipment_lines rsl ,
  rcv_transactions rt ,
  (SELECT msi.inventory_item_id,
    msi.segment1 ITEM_CODE
  FROM mtl_system_items_b msi,
    mtl_parameters mp
  WHERE msi.organization_id=mp.organization_id
  AND msi.organization_id  =mp.master_organization_id
  ) ITEM ,
  po_headers_all poh ,
  PO_RELEASES_ALL po_release ,
  po_lines_all pol ,
  po_line_locations_all pll ,
  po_distributions_all pd ,
  po_vendors pv ,
  po_vendor_sites_all pvs ,
  ap_terms payterm ,
  org_organization_definitions org ,
  AP_REPORTING_ENTITIES_all are
WHERE 1         =1 --rsh.receipt_num='8017000506' --'20160001447'
AND poh.segment1='8017009934'
  --  AND poh.segment1||DECODE(po_release.release_num,NULL,NULL,'-'||po_release.release_num)='8017009934-6'
AND poh.po_header_id=po_release.po_header_id(+)
AND poh.org_id      =po_release.org_id(+)
  -- AND nvl(po_release.release_num,'0')=p_release_num
AND are.tax_entity_id        = 10050
AND rsh.shipment_header_id   =rsl.shipment_header_id
AND rsl.shipment_header_id   =rt.shipment_header_id
AND rsl.shipment_line_id     =rt.shipment_line_id
AND NVL(rsl.po_release_id,0) = NVL(rt.po_release_id,0)
AND rt.source_document_code  ='PO'
AND rt.transaction_type      ='RECEIVE'
AND receipt_source_code      ='VENDOR'
AND rsl.item_id              =item.inventory_item_id(+)
AND rt.po_header_id          =pll.po_header_id
AND rt.po_line_id            =pll.po_line_id
AND rt.po_line_location_id   =pd.line_location_id
AND rt.po_distribution_id    =pd.po_distribution_id
AND NVL(rt.po_release_id,0)  = NVL(po_release.po_release_id,0)
AND poh.po_header_id         =pol.po_header_id
AND pol.po_header_id         =pll.po_header_id
AND pol.po_line_id           =pll.po_line_id
AND pll.po_header_id         =pd.po_header_id
AND pll.po_line_id           =pd.po_line_id
AND pll.line_location_id     =pd.line_location_id
AND poh.vendor_id            =pv.vendor_id
AND poh.vendor_id            =pvs.vendor_id
AND poh.vendor_site_id       =pvs.vendor_site_id
AND poh.terms_id             =payterm.term_id
  --and pvs.terms_id=payterm.term_id  /* change by suttisak */
  /* get payment term from po instead supplier site setup */
AND pll.ship_to_organization_id=org.organization_id
AND org.OPERATING_UNIT         = are.ORG_ID
GROUP BY rsh.receipt_num,                                        --rt.*
  DECODE(are.tax_entity_id,10000,'DTAC',10050,'DTN',10094,'PSB'),-- Company Code
  poh.segment1
  ||DECODE(po_release.release_num,NULL,NULL,'-'
  ||po_release.release_num),--poh.segment1,
  pol.line_num,
  rsl.shipment_line_status_code,
  rsl.line_num,
  item.item_code,
  rsl.item_description,
  (NVL(rsl.quantity_received,0)*NVL(pol.unit_price,0)),--rsl.quantity_received, -- if rsl.destination_type_code='INVENTORY'  then qty if EXPENSE THEN AMOUNT
  rsl.unit_of_measure,
  rsl.quantity_received,
  poh.currency_code,
  pv.segment1,
  pvs.vendor_site_code,
  payterm.name,
  org.organization_code,
  NVL(pol.unit_price,0);
