SELECT wn.*,wno.*
FROM
  (SELECT o.deq_time deq_time ,
    o.enq_time enq_time ,
    DECODE(o.state ,0 ,'PENDING' ,2 ,'EMAIL SENT' ,3 ,'EXCEPTION' ,o.state) state ,
    to_number(
    (SELECT str_value
    FROM TABLE(o.user_data.header.properties)
    WHERE NAME = 'NOTIFICATION_ID'
    )) p_notification_id ,
    (SELECT str_value
    FROM TABLE(o.user_data.header.properties)
    WHERE NAME = 'ROLE'
    ) p_Role ,
    o.user_data
  FROM applsys.wf_notification_out o
  ) wno ,
  applsys.wf_notifications wn
WHERE wn.notification_id = wno.p_notification_id
--and WN.SUBJECT like '%360669%'
and wn.MESSAGE_TYPE = 'CREATEPO'
;

