SELECT
     flex_value,
     flex_value_meaning,
     description,
     enabled_flag
 FROM
     fnd_flex_values_vl
 WHERE
     ( ( '' IS NULL )
       OR ( structured_hierarchy_level IN (
         SELECT
             hierarchy_id
         FROM
             fnd_flex_hierarchies_vl h
         WHERE
             h.flex_value_set_id = 1009748
             AND h.hierarchy_name LIKE ''
     ) ) )
     --AND ( flex_value = '1659' )
     AND ( flex_value_set_id = 1009748 )
 ORDER BY
     flex_value