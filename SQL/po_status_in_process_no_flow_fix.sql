SELECT wf_item_type,
  wf_item_key,
  po_header_id,
  segment1,
  revision_num,
  type_lookup_code,
  approved_date
FROM po_headers_all
WHERE segment1      = '991018012977'
AND NVL(org_id,-99) = NVL(102,-99)
  -- bug 5015493: Need to allow reset of blankets and PPOs also.
  -- and type_lookup_code = 'STANDARD'
--AND authorization_status     IN ('IN PROCESS', 'PRE-APPROVED')
AND NVL(cancel_flag, 'N')     = 'N'
AND NVL(closed_code, 'OPEN') <> 'FINALLY_CLOSED';
SELECT NVL(MAX(sequence_num), 0)
FROM po_action_history
WHERE object_type_code  IN ('PO', 'PA')
AND object_sub_type_code = 'STANDARD'
AND object_id            = 438781
AND action_code         IS NULL;
SELECT NVL(MAX(sequence_num), 0)
FROM po_action_history
WHERE object_type_code  IN ('PO', 'PA')
AND object_sub_type_code = 'STANDARD'
AND object_id            = 438781
AND action_code          = 'SUBMIT';
SELECT level,
  item_type,
  item_key,
  end_date
FROM wf_items
  START WITH item_type LIKE 'POAPPRV'
AND item_key LIKE '436843-208369'
  CONNECT BY prior item_type = parent_item_type
AND prior item_key           = parent_item_key
ORDER BY level DESC;
SELECT 'Y' x_open_notif_exist
  --into x_open_notif_exist
FROM dual
WHERE EXISTS
  (SELECT 'open notifications'
  FROM wf_item_activity_statuses wias,
    wf_notifications wfn,
    po_headers_all poh
  WHERE wias.notification_id   IS NOT NULL
  AND wias.notification_id      = wfn.group_id
  AND wfn.status                = 'OPEN'
  AND wias.item_type            = 'POAPPRV'
  AND wias.item_key             = poh.wf_item_key
  AND NVL(poh.org_id,-99)       = NVL(142,-99)
  AND poh.segment1              ='8018012567'
  AND poh.authorization_status IN ('IN PROCESS', 'PRE-APPROVED')
  );
SELECT 'Y' l_change_req_exists
  --into l_change_req_exists
FROM dual
WHERE EXISTS
  (SELECT 'po with change request'
  FROM po_headers_all h
  WHERE h.segment1           = '8018012567'
  AND NVL(h.org_id,          -99)     = NVL(142, -99)
  AND h.change_requested_by IN ('REQUESTER', 'SUPPLIER')
  );
SELECT 'Y'
INTO x_active_wf_exists
FROM wf_items wfi
WHERE wfi.item_type = pos.wf_item_type
AND wfi.item_key    = pos.wf_item_key
AND wfi.end_date   IS NULL;
---------- Code FIX ---------------------

-----BACK UP Before update ------
SELECT authorization_status,
wf_item_type,
wf_item_key,
approved_flag,
  DECODE(approved_date, NULL, 'INCOMPLETE', 'REQUIRES REAPPROVAL') authorization_status,
  DECODE(approved_date, NULL, 'N', 'R') approved_flag
FROM po_headers_all
WHERE po_header_id = '439059';

---- Update ---
UPDATE po_headers_all
SET authorization_status = 'INCOMPLETE',
  wf_item_type           = NULL,
  wf_item_key            = NULL,
  approved_flag          = 'N'
WHERE po_header_id       = '439059';

-----BACK UP Before update ------

SELECT action_code,
  action_date,
  note
FROM po_action_history
WHERE object_id      = '439059'
AND object_type_code = DECODE('STANDARD', 'STANDARD','PO', 'PLANNED', 'PO', --future plan to enhance for planned PO
  'PA')
AND object_sub_type_code = 'STANDARD'
AND sequence_num         = 3
AND action_code         IS NULL;

---- Update ---
UPDATE po_action_history
SET action_code = 'NO ACTION',
  action_date   = TRUNC(sysdate),
  note          = 'updated by reset script on '
  ||TO_CHAR(TRUNC(sysdate))
WHERE object_id      = '439059'
AND object_type_code = DECODE('STANDARD', 'STANDARD','PO', 'PLANNED', 'PO', --future plan to enhance for planned PO
  'PA')
AND object_sub_type_code = 'STANDARD'
AND sequence_num         = 3
AND action_code         IS NULL;

----------- END CODE FIX ---------------
SELECT NVL(purch_encumbrance_flag,'N')
  --into l_purch_encumbrance_flag
FROM financials_system_params_all fspa
WHERE NVL(fspa.org_id,-99) = NVL(142,-99);
SELECT 'Y'
  --into disallow_script
FROM dual
WHERE NOT EXISTS
  (SELECT 'encumbrance data'
  FROM gl_je_lines
  WHERE reference_3 IN
    (SELECT TO_CHAR(po_distribution_id)
    FROM po_distributions_all pod
    WHERE pod.po_header_id = '439059'
    )
  AND reference_1 = 'PO'
  );