declare
      l_input varchar2(4000) := 'IVN026381647101,IVN026381647092,IVN026381647060,IVN026381647097,IVN026381647071,IVN026381647099,IVN026381647113,IVN026381647080,IVN026381647112,IVN026381647085,IVN026381647100,IVN026381647067,IVN026381647073,IVN026381647109,IVN026381647104,IVN026381647096,IVN026381647098,IVN026381647119';
      l_count binary_integer;
      l_array dbms_utility.lname_array;
      invoice            VARCHAR2(20) ;-- set invoice no that need update multi sim issue
      printscript        BOOLEAN      := true;              -- set true if need to print script or false if need update
      serial             VARCHAR2(20);
      ChkIN_TRANS_SN_LOT NUMBER;
      intranssnlot IN_TRANS_SN_LOT%rowtype;
    begin
    dbms_utility.comma_to_table
      ( list   => regexp_replace(l_input,'(^|,)','\1x')
      , tablen => l_count
      , tab    => l_array
     );
    --dbms_output.put_line(l_count);
     for i in 1 .. l_count
     loop
       
        invoice := substr(l_array(i),2);
        dbms_output.put_line('-- Invoice no. ' || invoice);
         IF (printscript) THEN
            DBMS_OUTPUT.put_line('update IN_TRANS_HEAD set TF_C_O = null where DOC_NO = ''' || invoice || ''';');
            DBMS_OUTPUT.put_line('update IN_TRANS_DTL set TF_C_O = null where DOC_NO = ''' || invoice || ''';');
            DBMS_OUTPUT.put_line('update IN_TRANS_SN_LOT set TF_C_O = null where DOC_NO = ''' || invoice || ''';');
          ELSE
            UPDATE IN_TRANS_HEAD SET TF_C_O = NULL WHERE DOC_NO = invoice;
            UPDATE IN_TRANS_DTL SET TF_C_O = NULL WHERE DOC_NO = invoice;
            UPDATE IN_TRANS_SN_LOT
            SET TF_C_O   = NULL
            WHERE DOC_NO = invoice;
          END IF;
          FOR cur_rec1 IN
          (SELECT SEQ_NO FROM SA_TRANS_DTL WHERE DOC_NO = invoice
          )
          LOOP
            FOR cur_rec2 IN
            (SELECT serial,
              SEQ_SN_LOT
            FROM SA_TRANS_SN_LOT
            WHERE DOC_NO = invoice
            AND SEQ_NO   = cur_rec1.SEQ_NO
            )
            LOOP
              SELECT COUNT(*)
              INTO ChkIN_TRANS_SN_LOT
              FROM IN_TRANS_SN_LOT
              WHERE DOC_NO         = invoice
              AND Serial           = cur_rec2.Serial;
              IF ChkIN_TRANS_SN_LOT<=0 THEN
                -- DBMS_OUTPUT.put_line(cur_rec1.SEQ_NO || '    ' || cur_rec2.SEQ_SN_LOT ||'   '|| cur_rec2.Serial);
                SELECT OU_CODE,
                  SUBINV_CODE,
                  TRAN_TYPE_CODE,
                  DOC_NO,
                  ITEM_CODE,
                  LOCATOR_CODE,
                  cur_rec1.SEQ_NO,
                  cur_rec2.SEQ_SN_LOT,
                  cur_rec2.Serial,
                  LOT,
                  QTY,
                  UPD_BY,
                  UPD_DATE,
                  UPD_PGM,
                  TRANSFER,
                  TF_S_C,
                  CREATE_BY,
                  CREATE_DATE,
                  MOBILE_NO,
                  EXPIRE_DATE,
                  NULL
                INTO intranssnlot
                FROM IN_TRANS_SN_LOT
                WHERE DOC_NO = invoice
                AND LINE_SEQ = cur_rec1.SEQ_NO
                AND rownum   = 1;
                IF (printscript) THEN
                  DBMS_OUTPUT.put_line('Insert into IN_TRANS_SN_LOT (OU_CODE,SUBINV_CODE,TRAN_TYPE_CODE,DOC_NO,ITEM_CODE,LOCATOR_CODE,LINE_SEQ,SEQ,SERIAL,LOT,QTY,UPD_BY,UPD_DATE,UPD_PGM,TRANSFER,TF_S_C,CREATE_BY,CREATE_DATE,MOBILE_NO,EXPIRE_DATE,TF_C_O) values (' || ''''||intranssnlot.OU_CODE || ''',' || ''''||intranssnlot.SUBINV_CODE || ''',' || ''''||intranssnlot.TRAN_TYPE_CODE || ''',' || ''''||intranssnlot.DOC_NO || ''',' || ''''||intranssnlot.ITEM_CODE || ''',' || ''''||intranssnlot.LOCATOR_CODE || ''',' || ''||intranssnlot.LINE_SEQ || ',' || ''||intranssnlot.SEQ || ',' || ''''||intranssnlot.SERIAL || ''',' || 'null'||','|| ''''||intranssnlot.QTY || ''',' || ''''||intranssnlot.UPD_BY || ''',' || ''''||intranssnlot.UPD_DATE || ''',' || ''''||intranssnlot.UPD_PGM || ''',' || 'null,null,null,null,null,null,null);');
                ELSE
                  INSERT
                  INTO IN_TRANS_SN_LOT
                    (
                      OU_CODE,
                      SUBINV_CODE,
                      TRAN_TYPE_CODE,
                      DOC_NO,
                      ITEM_CODE,
                      LOCATOR_CODE,
                      LINE_SEQ,
                      SEQ,
                      SERIAL,
                      LOT,
                      QTY,
                      UPD_BY,
                      UPD_DATE,
                      UPD_PGM,
                      TRANSFER,
                      TF_S_C,
                      CREATE_BY,
                      CREATE_DATE,
                      MOBILE_NO,
                      EXPIRE_DATE,
                      TF_C_O
                    )
                    VALUES
                    (
                      intranssnlot.OU_CODE,
                      intranssnlot.SUBINV_CODE,
                      intranssnlot.TRAN_TYPE_CODE,
                      intranssnlot.DOC_NO,
                      intranssnlot.ITEM_CODE,
                      intranssnlot.LOCATOR_CODE,
                      intranssnlot.LINE_SEQ,
                      intranssnlot.SEQ,
                      intranssnlot.SERIAL,
                      NULL,
                      intranssnlot.QTY,
                      intranssnlot.UPD_BY,
                      intranssnlot.UPD_DATE,
                      intranssnlot.UPD_PGM,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      NULL
                    );
                END IF;
              END IF;
            END LOOP;
          END LOOP;
    end loop;
   end;
   /