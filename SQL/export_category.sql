SELECT *
FROM
  (SELECT CATEGORY_ID,
    CATEGORY_CODE,
    DESCRIPTION,
    PERSON_NAME,
    START_DATE,
    END_DATE
  FROM
    (SELECT a.category_id,
      (segment1
      ||'.'
      ||segment2
      ||'.'
      ||segment3
      ||'.'
      ||segment4) category_code,
      a.description,
      b.party_name person_name,
      b.start_date,
      b.end_date
    FROM MTL_CATEGORIES a,
      MTL_OBJECT_GRANTs_V b
      where a.category_id = b.instance_pk1_value(+)
    AND b.end_date                            IS NULL
    )
  );
