SELECT po_headers_all.CLOSED_DATE,
PO_RELEASES_ALL.Release_date,
--po_headers_all.segment1,
PO.PO_RELEASES_ALL.CLOSED_CODE,
PO_RELEASES_ALL.release_num,
PO_RELEASES_ALL.*
FROM po_headers_all, PO.PO_RELEASES_ALL
WHERE po_headers_all.PO_HEADER_ID = PO_RELEASES_ALL.PO_HEADER_ID
AND TYPE_LOOKUP_CODE = 'BLANKET'
--AND po_headers_all.CLOSED_DATE       >= to_date('29/10/2015','dd/mm/yyyy')
--AND PO_RELEASES_ALL.Release_date >= to_date('29/10/2015','dd/mm/yyyy')
and po_headers_all.CLOSED_DATE  <= PO_RELEASES_ALL.Release_date
--AND CLOSED_CODE <>  'CLOSED'