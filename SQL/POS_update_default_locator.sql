create or replace procedure TAC_ADD_DEFAULT_LOC is
begin
DECLARE
type tab_ou IS TABLE OF VARCHAR2(10 BYTE);
  T_ou tab_ou;
type tab_subinv IS TABLE OF VARCHAR2(10 BYTE);
  T_subinv tab_subinv;
type tab_locator IS TABLE OF VARCHAR2(10 BYTE);
  T_locator tab_locator;
  
BEGIN
  
    SELECT aa.SUBINV_CODE,aa.OA_LOCATOR_CODE,aa.OU_SUB
    BULK COLLECT INTO T_subinv,T_locator,T_ou
      FROM
        (SELECT COUNT(PB_SUBINV.SUBINV_CODE) OVER (PARTITION BY PB_SUBINV.SUBINV_CODE ORDER BY PB_SUBINV.SUBINV_CODE DESC) cnt_sub,
          PB_SUBINV.OU_CODE OU_SUB,
          PB_SUBINV.SUBINV_CODE,
          PB_SUBINV.DEFAULT_LOC,
          PB_LOCATOR.OU_CODE OU_LOC,
          PB_LOCATOR.OA_LOCATOR_CODE,
          PB_LOCATOR.LOC_DESC
        FROM PB_SUBINV,
          PB_LOCATOR
        WHERE 0                    =0
        AND PB_SUBINV.SUBINV_CODE  = PB_LOCATOR.SUBINV_CODE
        AND PB_SUBINV.OU_CODE      = PB_LOCATOR.OU_CODE
        AND PB_SUBINV.DEFAULT_LOC IS NULL
        --AND (PB_LOCATOR.OA_LOCATOR_CODE LIKE '%001')
        --and PB_LOCATOR.LOC_DESC = 'Good'
        and PB_LOCATOR.USE_DEFAULT = 'Y'
        ) aa
      WHERE aa.cnt_sub = 1
      AND aa.OA_LOCATOR_CODE not like '%A%';
  dbms_output.put_line('count update : '|| T_subinv.count);  
  
  FOR cSub IN 1 .. T_subinv.count
  LOOP
    dbms_output.put_line('OU '|| T_ou(cSub) ||' Subinv '|| T_subinv(cSub) || ' Locator ' || T_locator(cSub));
    update PB_SUBINV set DEFAULT_LOC = T_locator(cSub) where SUBINV_CODE = T_subinv(cSub) and OU_CODE = T_ou(cSub);  
  END LOOP;
END;
END;