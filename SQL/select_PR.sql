SELECT *
FROM
  (SELECT PO_REQUISITION_HEADERS_ALL.REQUISITION_HEADER_ID,
    PO_REQUISITION_LINES_ALL.REQUISITION_LINE_ID,
    PO_REQUISITION_HEADERS_ALL.SEGMENT1 AS PR_NO,
    PO_REQUISITION_HEADERS_ALL.AUTHORIZATION_STATUS,
    PO_REQUISITION_LINES_ALL.LINE_NUM,
    PO_REQUISITION_LINES_ALL.ITEM_ID,
    PO_REQUISITION_LINES_ALL.ITEM_DESCRIPTION,
    PO_REQUISITION_LINES_ALL.UNIT_MEAS_LOOKUP_CODE AS UOM,
    PO_REQUISITION_LINES_ALL.QUANTITY,
    PO_REQUISITION_LINES_ALL.UNIT_PRICE,
    PO_REQUISITION_LINES_ALL.CURRENCY_CODE,
    PO_REQUISITION_LINES_ALL.RATE,
    PO_REQUISITION_HEADERS_ALL.CREATION_DATE,
    GL_CODE_COMBINATIONS.SEGMENT11                                                                                                                                                       AS ACCOUNT_CODE ,
    PO_REQUISITION_LINES_ALL.QUANTITY    *PO_REQUISITION_LINES_ALL.UNIT_PRICE*NVL(PO_REQUISITION_LINES_ALL.RATE,1)                                                                       AS AMOUNT ,
    SUM(PO_REQUISITION_LINES_ALL.QUANTITY*PO_REQUISITION_LINES_ALL.UNIT_PRICE*NVL(PO_REQUISITION_LINES_ALL.RATE,1)) OVER (PARTITION BY PO_REQUISITION_HEADERS_ALL.REQUISITION_HEADER_ID) AS TOTAL_AMT,
    MTL_CATEGORIES_B.SEGMENT1
    || '.'
    || MTL_CATEGORIES_B.SEGMENT2
    || '.'
    || MTL_CATEGORIES_B.SEGMENT3
    || '.'
    ||MTL_CATEGORIES_B.SEGMENT4            AS "CATEGORY" ,
    PO_REQUISITION_HEADERS_ALL.PREPARER_ID AS Requestor_ID,
    REQ_TAB.FULL_NAME                      AS REQUESTOR_NAME,
    REQ_TAB.EMPLOYEE_NUMBER                AS REQUESTOR_EMP_NO,
    PO_REQ_DISTRIBUTIONS_ALL.PROJECT_ID,
    PO_REQ_DISTRIBUTIONS_ALL.EXPENDITURE_TYPE ,
    BUYER_TAB.FULL_NAME      AS BUYER,
    PA_PROJECTS_ALL.segment1 AS "Project Code",
    PA_PROJECTS_ALL.NAME     AS "Project Name",
    PA_TASKS.TASK_NAME,
    ACTION_HIS.ACTION_DATE AS APPROVE_DATE,
    ACTION_HIS.employee_id,
    APPROVER_TAB.FULL_NAME       AS APPROVER,
    APPROVER_TAB.EMPLOYEE_NUMBER AS Last_Approver_EMP_No,
    APPROVER_TAB.ATTRIBUTE3      AS "GROUP"
    --PO_REQ_DISTRIBUTIONS_ALL.*
  FROM PO_REQUISITION_HEADERS_ALL ,
    PO_REQUISITION_LINES_ALL,
    PO_REQ_DISTRIBUTIONS_ALL,
    (SELECT *
    FROM
      (SELECT ROW_NUMBER() OVER (PARTITION BY PERSON_ID ORDER BY EFFECTIVE_END_DATE DESC) CNT,
        PERSON_ID,
        FULL_NAME,
        EMPLOYEE_NUMBER,
        ATTRIBUTE3
      FROM PER_ALL_PEOPLE_F
      )
    WHERE 0 =0
    AND CNT = 1
    ) REQ_TAB,
    (SELECT *
    FROM
      (SELECT ROW_NUMBER() OVER (PARTITION BY PERSON_ID ORDER BY EFFECTIVE_END_DATE DESC) CNT,
        PERSON_ID,
        FULL_NAME,
        EMPLOYEE_NUMBER,
        ATTRIBUTE3
      FROM PER_ALL_PEOPLE_F
      )
    WHERE 0 =0
    AND CNT = 1
    ) BUYER_TAB,
    (SELECT *
    FROM
      (SELECT ROW_NUMBER() OVER (PARTITION BY PERSON_ID ORDER BY EFFECTIVE_END_DATE DESC) CNT,
        PERSON_ID,
        FULL_NAME,
        EMPLOYEE_NUMBER,
        ATTRIBUTE3
      FROM PER_ALL_PEOPLE_F
      )
    WHERE 0 =0
    AND CNT = 1
    ) APPROVER_TAB,
    GL_CODE_COMBINATIONS,
    MTL_CATEGORIES_B, -- previously Select wrong table MTL_CATEGORIES
    PA_PROJECTS_ALL,
    PA_TASKS,
    (SELECT *
    FROM PO_ACTION_HISTORY ph1
    WHERE ph1.sequence_num =
      (SELECT MAX(sequence_num)
      FROM PO_ACTION_HISTORY ph2
      WHERE ph1.object_id = ph2.object_id
      AND ph2.action_code = 'APPROVE'
      )
    ) ACTION_HIS
  WHERE 0                                              =0
  AND PO_REQUISITION_HEADERS_ALL.REQUISITION_HEADER_ID = PO_REQUISITION_LINES_ALL.REQUISITION_HEADER_ID
  AND PO_REQUISITION_LINES_ALL.REQUISITION_LINE_ID     = PO_REQ_DISTRIBUTIONS_ALL.REQUISITION_LINE_ID(+)
  AND PO_REQUISITION_HEADERS_ALL.PREPARER_ID           = REQ_TAB.PERSON_ID(+)
  AND PO_REQ_DISTRIBUTIONS_ALL.CODE_COMBINATION_ID     = GL_CODE_COMBINATIONS.CODE_COMBINATION_ID
  AND PO_REQUISITION_LINES_ALL.category_id             = MTL_CATEGORIES_B.CATEGORY_ID(+)
  AND PO_REQUISITION_LINES_ALL.SUGGESTED_BUYER_ID      = BUYER_TAB.PERSON_ID (+)
  AND PO_REQ_DISTRIBUTIONS_ALL.PROJECT_ID              = PA_PROJECTS_ALL.PROJECT_ID (+)
  AND PO_REQ_DISTRIBUTIONS_ALL.task_id                 = PA_TASKS.task_id (+)
  AND PO_REQUISITION_HEADERS_ALL.requisition_header_id = ACTION_HIS.object_id
  AND ACTION_HIS.employee_id                           = APPROVER_TAB.PERSON_ID(+)
  AND PO_REQUISITION_HEADERS_ALL.CREATION_DATE BETWEEN to_date('01/06/2016','dd/mm/yyyy') AND to_date('01/07/2016','dd/mm/yyyy')
    --AND PO_REQUISITION_LINES_ALL.LAST_UPDATE_LOGIN    = '0' -- Cause line missing
  AND PO_REQUISITION_HEADERS_ALL.APPROVED_DATE        IS NOT NULL
  AND PO_REQUISITION_HEADERS_ALL.AUTHORIZATION_STATUS <> 'SYSTEM_SAVED'
    --AND ACTION_HIS.ACTION_DATE BETWEEN to_date('01/01/2015','dd/mm/yyyy') AND to_date('29/03/2016','dd/mm/yyyy')
   -- AND PO_REQUISITION_HEADERS_ALL.SEGMENT1              = '10160014330'
  )
WHERE 0 =0
  --AND TOTAL_AMT > 40000000
