SELECT MENU || '|' ||
LOAD01 || '|' ||
LOAD02 || '|' ||
LOAD03 || '|' ||
LOAD04 || '|' ||
LOAD05 || '|' ||
LOAD06 || '|' ||
LOAD07 || '|' || 
LOAD08 || '|' ||
LOAD09 || '|' ||
LOAD10 as v_output
FROM
  (SELECT
    CASE
      WHEN PO_NO LIKE '991%'
      THEN 'PO-HO'
      WHEN PO_NO LIKE '992%'
      THEN 'PO-PSL'
      WHEN PO_NO LIKE '993%'
      THEN 'PO-KKC'
      WHEN PO_NO LIKE '994%'
      THEN 'PO-SRC'
      WHEN PO_NO LIKE '995%'
      THEN 'PO-SRT'
      WHEN PO_NO LIKE '801%'
      THEN 'DTN-PO'
      WHEN PO_NO LIKE '121%'
      THEN 'PB_PO'
      WHEN PO_NO LIKE '151%'
      THEN 'PO-DTAC-A'
      WHEN PO_NO LIKE '601%'
      THEN 'PO-DTAC-I'
      WHEN PO_NO LIKE '701%'
      THEN 'PO-DTAC-W'
    END MENU,
    PO_LIST.*
  FROM
    ( SELECT DISTINCT PO_HEADERS_ALL.segment1 PO_NO,
      NULL release_num,
      '-' release_date,
      TO_CHAR(PO_HEADERS_ALL.approved_date,'dd/mm/yyyy HH24:MI:SS') approve_date,
      PO_HEADERS_ALL.AUTHORIZATION_STATUS,
      PO_HEADERS_ALL.WF_ITEM_KEY,
      PO_HEADERS_ALL.segment1 LOAD01,
      '*AJ' LOAD02,
      '*ATO' LOAD03,
      'TAB' LOAD04,
      'ENT' LOAD05,
      'ENT' LOAD06,
      '*AVF' LOAD07,
      NULL LOAD08,
      NULL LOAD09,
      NULL LOAD10
      --,PO_HEADERS_ALL.*
    FROM PO_HEADERS_ALL,
      PO_LINES_ALL,
      PO_VENDOR_SITES_ALL
    WHERE PO_HEADERS_ALL.PO_HEADER_ID        = PO_LINES_all.PO_HEADER_ID
    AND PO_HEADERS_ALL.VENDOR_ID             = PO_VENDOR_SITES_ALL.VENDOR_ID
    AND PO_HEADERS_ALL.VENDOR_SITE_ID        = PO_VENDOR_SITES_ALL.VENDOR_SITE_ID
    AND TRUNC(PO_HEADERS_ALL.approved_date) >= sysdate-1
    AND PO_HEADERS_ALL.TYPE_LOOKUP_CODE      = 'STANDARD'
    AND PO_LINES_ALL.from_header_id         IS NOT NULL
      /*AND ((PO_HEADERS_ALL.PO_HEADER_ID IN
      (SELECT DISTINCT PO_RELEASES_ALL.PO_HEADER_ID
      FROM PO_RELEASES_ALL
      WHERE PO_RELEASES_ALL.APPROVED_DATE >= to_date('27/10/2015','dd/mm/yyyy')
      AND PO_RELEASES_ALL.CANCEL_FLAG     IS NULL
      ) ) )*/
    AND PO_HEADERS_ALL.cancel_flag                                                          IS NULL -- SWE 280509
    AND PO_HEADERS_ALL.revised_date                                                         IS NULL -- SWE -010609
    AND PO_HEADERS_ALL.type_lookup_code                                                     <> 'PLANNED'
    AND PO_VENDOR_SITES_ALL.SUPPLIER_NOTIF_METHOD                                            = 'EMAIL'
    AND PO_VENDOR_SITES_ALL.EMAIL_ADDRESS                                                   IS NOT NULL
    AND SUBSTR(PO_HEADERS_ALL.WF_ITEM_KEY, 0, INSTR(PO_HEADERS_ALL.WF_ITEM_KEY, '-')-1) NOT IN
      (SELECT SUBSTR(wfn.CONTEXT, INSTR(wfn.CONTEXT, ':')                           +1, INSTR(wfn.CONTEXT, '-') - INSTR(wfn.CONTEXT, ':')-1)
      FROM wf_notifications wfn
      WHERE wfn.message_name = 'EMAIL_PO_PDF'
      AND wfn.mail_status    = 'SENT'
      )
    -- and PO_HEADERS_ALL.SEGMENT1 in ('995011001095','991015001521')
    --ORDER BY TO_CHAR(PO_HEADERS_ALL.approved_date,'dd/mm/yyyy hh:mm:ss')
    UNION
    SELECT poh.segment1 PO_NO,
      por.release_num,
      TO_CHAR(por.release_date,'dd/mm/yyyy HH24:MI:SS') release_date,
      TO_CHAR(por.approved_date,'dd/mm/yyyy HH24:MI:SS') approve_date,
      por.authorization_status,
      POR.WF_ITEM_KEY,
      poh.segment1 LOAD01,
      'TAB' LOAD02,
      'TAB' LOAD03,
      TO_CHAR(por.release_num) LOAD04,
      '*AJ' LOAD05,
      '*ATO' LOAD06,
      'TAB' LOAD07,
      'ENT' LOAD08,
      'ENT' LOAD09,
      '*AVF' LOAD010
    FROM po_releases_all por,
      po_headers_all poh,
      PO_VENDOR_SITES_ALL ves
    WHERE TRUNC(por.approved_date)                                    >= sysdate-1
    AND por.po_header_id                                               = poh.po_header_id
    AND por.cancel_flag                                               IS NULL -- SWE 280509
    AND poh.revised_date                                              IS NULL -- SWE -010609
    AND poh.vendor_id                                                  = ves.vendor_id
    AND poh.vendor_site_id                                             = ves.vendor_site_id
    AND ves.supplier_notif_method                                      = 'EMAIL'
    AND ves.email_address                                             IS NOT NULL
    AND poh.type_lookup_code                                          <> 'PLANNED'
    AND SUBSTR(POR.WF_ITEM_KEY, 0, INSTR(POR.WF_ITEM_KEY, '-')-1) NOT IN
      (SELECT SUBSTR(wfn.CONTEXT, INSTR(wfn.CONTEXT, ':')     +1, INSTR(wfn.CONTEXT, '-') - INSTR(wfn.CONTEXT, ':')-1)
      FROM wf_notifications wfn
      WHERE wfn.message_name = 'EMAIL_PO_PDF'
      AND wfn.mail_status    = 'SENT'
      )
    ) PO_LIST
  ORDER BY 1,5
  );