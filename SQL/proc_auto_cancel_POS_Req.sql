CREATE OR REPLACE PROCEDURE TAC_AUTO_CANCEL_POS_DOC(
    errbuf OUT VARCHAR2,
    retcode OUT NUMBER)
AS
  v_From VARCHAR2(80) := 'ERPOperationSupport@dtac.co.th'; --'EnterpriseApplicationSupportBOSDept@dtac.co.th';
  v_Recipient VARCHAR2(80) := 'ERPOperationSupport@dtac.co.th';
  v_cc        VARCHAR2(80) := 'ER-MD@dtac.co.th'; 
  --v_Recipient VARCHAR2(80) := 'tantida.yodhasmutr@dtac.co.th';
  v_Mail_Host VARCHAR2(30) := 'mail-gw.tac.co.th';
  v_Mail_Conn utl_smtp.Connection;
  crlf      VARCHAR2(2)   := CHR(10); --chr(13)||chr(10);
  v_oper    VARCHAR2(100) := 'ERP Operation Support Team';
  p_message VARCHAR2(30000);
  s_subject VARCHAR2(30000);
  -- May 30,2018 Create Program by TTD
  --- ERP PARAMETER
type tab_POS_DOC_NO
IS
  TABLE OF VARCHAR2(60);
  t_POS_DOC_NO tab_POS_DOC_NO;
type tab_ORDER_NUMBER
IS
  TABLE OF NUMBER;
  t_ORDER_NUMBER tab_ORDER_NUMBER;
type tab_ORDER_TYPE
IS
  TABLE OF VARCHAR2(30);
  t_ORDER_TYPE tab_ORDER_TYPE;
  --- POS PARAMETER
type tab_OU_CODE
IS
  TABLE OF VARCHAR2(3);
  t_OU_CODE tab_OU_CODE;
type tab_REQ_NO
IS
  TABLE OF VARCHAR2(15);
  t_REQ_NO tab_REQ_NO;
type tab_SUB_CODE
IS
  TABLE OF VARCHAR2(10);
  t_SUB_CODE tab_SUB_CODE;
type tab_CRE_BY
IS
  TABLE OF VARCHAR2(15);
  t_CRE_BY tab_CRE_BY;
type tab_REQ_DATE
IS
  TABLE OF DATE;
  t_REQ_DATE tab_REQ_DATE;
  
CNT NUMBER := 0; 
  
BEGIN
  p_message := UTL_TCP.crlf || 'I---------------------------------------------------------------I' || UTL_TCP.crlf ;
  p_message := p_message || ' POS REQUISITION TO BE UPDATE ' || UTL_TCP.crlf;
  p_message := p_message || 'I---------------------------------------------------------------I' || UTL_TCP.crlf;
  p_message := p_message || 'ERP_ORDER_NUMBER|ERP_ORDER_TYPE|POS_DOC_NO|POS_OU_CODE|POS_SUB_CODE|POS_CREATE_BY|POS_REQ_DATE' || UTL_TCP.crlf;
  fnd_file.put_line(fnd_file.log,'I---------------------------------------------------------------I');
  fnd_file.put_line(fnd_file.log,' POS REQUISITION TO BE UPDATE ');
  fnd_file.put_line(fnd_file.log,'I---------------------------------------------------------------I');
  fnd_file.put_line(fnd_file.log,'ERP_ORDER_NUMBER|ERP_ORDER_TYPE|POS_DOC_NO|POS_OU_CODE|POS_SUB_CODE|POS_CREATE_BY|POS_REQ_DATE');
  SELECT DISTINCT 'I0'
    || OOHA.ORIG_SYS_DOCUMENT_REF POS_DOC_NO,
    OOHA.ORDER_NUMBER,
    OTTT.NAME ORDER_TYPE BULK COLLECT
  INTO t_POS_DOC_NO,
    t_ORDER_NUMBER,
    t_ORDER_TYPE
  FROM OE_Order_Headers_All OOHA,
    OE_Transaction_Types_TL OTTT
  WHERE 0                =0
  AND OOHA.Order_Type_ID = OTTT.Transaction_Type_ID(+)
  AND OTTT.NAME LIKE 'S99-INT-%'
  AND OOHA.CANCELLED_FLAG = 'Y'
  AND TRUNC(OOHA.LAST_UPDATE_DATE) = TRUNC(sysdate) ;
  FOR cJob                                       IN 1 .. t_POS_DOC_NO.count
  LOOP
    SELECT DISTINCT OU_CODE,
      REQ_NO,
      SUBINV_CODE,
      CREATE_BY,
      REQ_DATE BULK COLLECT
    INTO t_OU_CODE,
      t_REQ_NO,
      t_SUB_CODE,
      t_CRE_BY,
      t_REQ_DATE
    FROM IN_REQ_HEAD
    WHERE REQ_NO    = t_POS_DOC_NO(cJob)
    AND REQ_STATUS <> 'C';
    IF t_REQ_NO.count <= 0 THEN
    FND_FILE.PUT_LINE(FND_FILE.LOG, t_ORDER_NUMBER(cJob) || '|' || t_ORDER_TYPE(cJob) || '|' || t_POS_DOC_NO(cJob) || '-POS Requisition not found or already cancel.');
    END IF;
    FOR cSub IN 1 .. t_REQ_NO.count
    LOOP
      CNT := CNT+1;
      FND_FILE.PUT_LINE(FND_FILE.LOG, t_ORDER_NUMBER(cJob) || '|' || t_ORDER_TYPE(cJob) || '|' || t_REQ_NO(cSub) || '|' || t_OU_CODE(cSub) || '|' || t_SUB_CODE(cSub) || '|' || t_CRE_BY(cSub) || '|' || t_REQ_DATE(cSub));
      p_message := p_message || t_ORDER_NUMBER(cJob) || '|' || t_ORDER_TYPE(cJob) || '|' || t_REQ_NO(cSub) || '|' || t_OU_CODE(cSub) || '|' || t_SUB_CODE(cSub) || '|' || t_CRE_BY(cSub) || '|' || t_REQ_DATE(cSub) || UTL_TCP.crlf;
      UPDATE IN_REQ_HEAD
      SET REQ_STATUS  = 'C'
      WHERE REQ_NO    = t_REQ_NO(cSub)
      AND REQ_STATUS <> 'C';
      --ROLLBACK;
      COMMIT;
    END LOOP;
  END LOOP;
  IF CNT > 0 THEN
    v_Mail_Conn        := UTL_SMTP.Open_Connection(v_Mail_Host, 25);
    UTL_SMTP.Helo(v_Mail_Conn, v_Mail_Host);
    UTL_SMTP.Mail(v_Mail_Conn, v_From);
    UTL_SMTP.Rcpt(v_Mail_Conn, v_Recipient);
    UTL_SMTP.Rcpt(v_Mail_Conn, v_cc);
    UTL_SMTP.open_data(v_Mail_Conn);
    UTL_SMTP.write_data(v_Mail_Conn, 'Subject: ERP Auto Cancel POS Internal Requisition : Request ID ' || FND_GLOBAL.CONC_REQUEST_ID || UTL_TCP.crlf);
    UTL_SMTP.write_data(v_Mail_Conn, 'Date: ' || TO_CHAR(FROM_TZ(CAST(SYSDATE AS TIMESTAMP), 'Asia/Bangkok'),'Dy, DD Mon YYYY hh24:mi:ss TZHTZM','NLS_DATE_LANGUAGE=AMERICAN') || UTL_TCP.crlf);
    UTL_SMTP.write_data(v_Mail_Conn, 'To: ' || v_Recipient || UTL_TCP.crlf);
    UTL_SMTP.write_data(v_Mail_Conn, 'From: ' || v_From || UTL_TCP.crlf);
    UTL_SMTP.write_data(v_Mail_Conn, crlf || crlf || 'Dear Operation Support,'|| crlf || crlf || '      POS Requisition below will be cancel. Please check the log of concurrent request: '|| FND_GLOBAL.CONC_REQUEST_ID ||'-TAC Auto Cancel POS Internal Requisition for futher details.' || UTL_TCP.crlf);
    UTL_SMTP.write_data(v_Mail_Conn, p_message || UTL_TCP.crlf || UTL_TCP.crlf || UTL_TCP.crlf);
    UTL_SMTP.write_data(v_Mail_Conn, crlf || crlf || crlf || 'Best Regards,'|| crlf || crlf || v_oper|| crlf || crlf || 'Date: ' || TO_CHAR(sysdate, 'Dy, DD Mon YYYY hh24:mi:ss'));
    UTL_SMTP.close_data(v_Mail_Conn);
    UTL_SMTP.quit(v_Mail_Conn);
  END IF;
EXCEPTION
WHEN UTL_SMTP.Transient_Error OR UTL_SMTP.Permanent_Error THEN
  raise_application_error(-20000, 'Unable to send mail: '||sqlerrm);
END;
