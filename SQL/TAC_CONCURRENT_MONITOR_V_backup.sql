
  CREATE OR REPLACE FORCE VIEW "APPS"."TAC_CONCURRENT_MONITOR_V" ("MAX_CON", "CODE", "STATUS_DESC", "REQUEST_ID", "CONCURRENT_PROGRAM_NAME", "RESPONSIBILITY_NAME", "USER_NAME", "Paramaters", "PRIORITY_REQUEST_ID", "PRIORITY", "FREQUENCY", "REQUESTED_START_DATE", "ACTUAL_START_DATE", "ACTUAL_COMPLETION_DATE", "ACTIVITY", "STATUS", "COMPLETION_TEXT") AS 
  SELECT "MAX_CON","CODE","STATUS_DESC","REQUEST_ID","CONCURRENT_PROGRAM_NAME","RESPONSIBILITY_NAME","USER_NAME","Paramaters","PRIORITY_REQUEST_ID","PRIORITY","FREQUENCY","REQUESTED_START_DATE","ACTUAL_START_DATE","ACTUAL_COMPLETION_DATE","ACTIVITY","STATUS","COMPLETION_TEXT"
FROM
  (SELECT
    CASE --TTD@BAS
      WHEN DECODE (FND_CONCURRENT_REQUESTS.DESCRIPTION, NULL, FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME, FND_CONCURRENT_REQUESTS.DESCRIPTION
        || ' ('
        || FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME
        || ')' ) IN ('Workflow Background Process','Transfer transactions to GL','TAC : Interface AP Invoice - FXTH')
      THEN MAX(FND_CONCURRENT_REQUESTS.REQUEST_ID) OVER (PARTITION BY NVL(FND_CONCURRENT_REQUESTS.DESCRIPTION,FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME) ,FND_RESPONSIBILITY_TL.RESPONSIBILITY_NAME,FND_CONCURRENT_REQUESTS.ARGUMENT_TEXT )
      ELSE MAX(FND_CONCURRENT_REQUESTS.REQUEST_ID) OVER (PARTITION BY NVL(FND_CONCURRENT_REQUESTS.DESCRIPTION,FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME) ,FND_RESPONSIBILITY_TL.RESPONSIBILITY_NAME )
    END AS max_con,
    (
    CASE
      WHEN STATUS_DESC = 'Normal'
      THEN 0
      WHEN (STATUS_DESC                           = 'Warning'
      AND FND_CONCURRENT_REQUESTS.COMPLETION_TEXT = 'No data was found in the GL_INTERFACE table.')
      THEN 0
      WHEN (STATUS_DESC = 'Warning'
      AND DECODE (FND_CONCURRENT_REQUESTS.DESCRIPTION, NULL, FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME, FND_CONCURRENT_REQUESTS.DESCRIPTION
        || ' ('
        || FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME
        || ')') = 'Pick Selection List Generation - SRS')
      THEN 99
      WHEN STATUS_DESC = 'Warning'
      THEN 99
      ELSE 1
    END) CODE,
    STATUS_DESC,
    FND_CONCURRENT_REQUESTS.REQUEST_ID,
    CASE
      WHEN DECODE (FND_CONCURRENT_REQUESTS.DESCRIPTION, NULL, FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME, FND_CONCURRENT_REQUESTS.DESCRIPTION
        || ' ('
        || FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME
        || ')' ) IN ('Workflow Background Process','Transfer transactions to GL')
      THEN DECODE (FND_CONCURRENT_REQUESTS.DESCRIPTION, NULL, FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME, FND_CONCURRENT_REQUESTS.DESCRIPTION
        || ' ('
        || FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME
        || ')' )
        || ' ('
        || FND_CONCURRENT_REQUESTS.ARGUMENT_TEXT
        || ')'
      ELSE DECODE (FND_CONCURRENT_REQUESTS.DESCRIPTION, NULL, FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME, FND_CONCURRENT_REQUESTS.DESCRIPTION
        || ' ('
        || FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME
        || ')' )
    END CONCURRENT_PROGRAM_NAME ,
    --APP.APPLICATION_SHORT_NAME,
    --APP.APPLICATION_NAME ,
    FND_RESPONSIBILITY_TL.RESPONSIBILITY_NAME ,
    FND_USER.USER_NAME,
    --ORGANIZATION
    FND_CONCURRENT_REQUESTS.ARGUMENT_TEXT AS "Paramaters",
    FND_CONCURRENT_REQUESTS.PRIORITY_REQUEST_ID,
    --PRIORITY_REQUEST_NAME
    FND_CONCURRENT_REQUESTS.PRIORITY,
    (FND_CONCURRENT_REQUESTS.RESUBMIT_INTERVAL
    ||' '
    ||FND_CONCURRENT_REQUESTS.RESUBMIT_INTERVAL_UNIT_CODE) FREQUENCY ,
    --U.USER_NAME REQUESTED_BY ,
    FND_CONCURRENT_REQUESTS.REQUESTED_START_DATE,
    FND_CONCURRENT_REQUESTS.ACTUAL_START_DATE,
    FND_CONCURRENT_REQUESTS.ACTUAL_COMPLETION_DATE,
    FND_CONCURRENT_PROGRAMS_TL.DESCRIPTION ACTIVITY ,
    PHASE_DESC
    || ' : '
    || STATUS_DESC STATUS,
    FND_CONCURRENT_REQUESTS.COMPLETION_TEXT
  FROM FND_CONCURRENT_PROGRAMS_TL,
    (SELECT *
    FROM
      (SELECT
        CASE
          WHEN STATUS_CODE IN ('A','Z')
          THEN 'Waiting'
          WHEN STATUS_CODE = 'B'
          THEN 'Resuming'
          WHEN STATUS_CODE IN ('C','I','R')
          THEN 'Normal'
          WHEN STATUS_CODE = 'E'
          THEN 'Error'
          WHEN STATUS_CODE = 'F'
          THEN 'Scheduled'
          WHEN STATUS_CODE = 'G'
          THEN 'Warning'
          WHEN STATUS_CODE = 'H'
          THEN 'On Hold'
          WHEN STATUS_CODE = 'M'
          THEN 'No Manager'
          WHEN STATUS_CODE = 'Q'
          THEN 'Standby'
          WHEN STATUS_CODE = 'S'
          THEN 'Suspended'
          WHEN STATUS_CODE = 'T'
          THEN 'Terminating'
          WHEN STATUS_CODE = 'U'
          THEN 'Disabled'
          WHEN STATUS_CODE = 'W'
          THEN 'Paused'
          WHEN STATUS_CODE = 'X'
          THEN 'Terminated'
        END AS STATUS_DESC,
        CASE
          WHEN PHASE_CODE = 'C'
          THEN 'Completed'
          WHEN PHASE_CODE = 'I'
          THEN 'Inactive'
          WHEN PHASE_CODE = 'P'
          THEN 'Pending'
          WHEN PHASE_CODE = 'R'
          THEN 'Running'
        END AS PHASE_DESC,
        FND_CONCURRENT_REQUESTS.*
      FROM FND_CONCURRENT_REQUESTS
      WHERE 0                                              =0
      AND TRUNC(FND_CONCURRENT_REQUESTS.ACTUAL_START_DATE) = TRUNC(sysdate -1)
      )
      -- WHERE max_con = REQUEST_ID
    ) FND_CONCURRENT_REQUESTS,
    FND_RESPONSIBILITY_TL,
    FND_USER
  WHERE 0                                                 =0
  AND FND_CONCURRENT_PROGRAMS_TL.CONCURRENT_PROGRAM_ID(+) = FND_CONCURRENT_REQUESTS.CONCURRENT_PROGRAM_ID
  AND FND_CONCURRENT_REQUESTS.RESPONSIBILITY_ID           = FND_RESPONSIBILITY_TL.RESPONSIBILITY_ID
  AND FND_CONCURRENT_REQUESTS.REQUESTED_BY                = FND_USER.USER_ID
  AND FND_USER.USER_NAME                                 IN ('DTAC','SYSADMIN')
  AND (DECODE (FND_CONCURRENT_REQUESTS.DESCRIPTION, NULL, FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME, FND_CONCURRENT_REQUESTS.DESCRIPTION
    || ' ('
    || FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME
    || ')' ) IN ( 'Actual Cost Worker' , 'TAC Import AR Invoice Data Alert - VGS (Check Periodic Alert)' -- Not found
    , 'Create Internal Orders' , 'General Ledger Transfer Program'                                       --OK
    , 'Margin Analysis Load Run'                                                                         --OK
    , 'Order Import'                                                                                     --There're 5 Order Import from 5 different OU of TAC : Import POS internal order --OK
    , 'PRC: Maintain Budgetary Control Balances'                                                         --OK
    , 'PRC: Update Project Summary Amounts'                                                              --OK
    --, 'PRC: Interface Expense Reports from Payables'                                                        --OK
    --, 'PRC: Interface Supplier Costs'
    , 'Payables Accounting Process'                                                                                                                    --C
    , 'Payables Transfer to General Ledger' , 'Pick Selection List Generation - SRS'                                                                   --OK
    , 'Process transaction interface'                                                                                                                  --2 duplicated transactios (same time same parameters). should one be deleted??
    , 'Program - Automatic Posting'                                                                                                                    --'Program - Automatic Posting (Sub ledger Posting)'  --Submodule Posting = 2  --OK
    , 'Program - Import Journals' , 'Program - Maintain Budget Organization'                                                                           --OK
    , 'Program - Maintain Summary Templates'                                                                                                           --OK
    , 'Purge Obsolete Workflow Runtime Data'                                                                                                           --OK
    , 'Receiving Transaction Processor'                                                                                                                --OK
    , 'TAC Create FND_USER' , 'TAC Create FND_USER From Interface Employee (Set) (Report Set)' , 'TAC_EREFILL Autoinvoice Master Program (Report Set)' --C
    , 'TAC_OTHER Autoinvoice Master Program (Report Set)'                                                                                              --C
    , 'TAC_PREMIUM Autoinvoice Master Program (Report Set)'                                                                                            --C
    , 'TAC_SIMCARD Autoinvoice Master Program (Report Set)'                                                                                           ---C
    , 'TAC_VOUCHER Autoinvoice Master Program (Report Set)'                                                                                            --C
    , 'Requisition Import' , 'TAC Account Combination Interface'                                                                                       --OK
    , 'TAC ERP to DMS Allocate and De-Allocate' , 'TAC Allocate / De-Allocate data from ERP' , 'TAC ERP to DMS Bank Master' , 'TAC ERP to DMS Item Master' , 'TAC ERP to DMS Partner Back' , 'TAC ERP to Discovery Product Master' , 'TAC Journal Sources '
    || chr(38)
    ||' Categories Interface'                                               --OK
    , 'TAC Organization Chart Interface Version 4'                          --OK
    , 'TAC Organization Clean Data'                                         --OK
    , 'TAC Pre-Activate SIM Interface'                                      --OK
    , 'TAC : Import POS internal order (Not Auto Release) HO (Report Set)'  --OK
    , 'TAC : Import POS internal order (Not Auto Release) SRC (Report Set)' --OK
    , 'TAC : Import POS internal order (Not Auto Release) PSL (Report Set)' --OK
    , 'TAC : Import POS internal order (Not Auto Release) KKC (Report Set)' --OK
    , 'TAC : Import POS internal order (Not Auto Release) SRT (Report Set)' --OK
    , 'TAC UD and Voucher Supplier Stock Balance Update'                    --OK
    , 'TAC Update SIM Activate Date in Serial Number Master Data'           --OK
    , 'Transfer transactions to GL'                                         --12 Org  --OK
    -- , 'Program - Create Journals'  --C
    --'Program - Automatic Posting  (Encumbrance Posting)' --Encumbrance Posting = 1  --OK
    , 'Import Shipping Locations'                 --OK
    , 'TAC Synchronize username changed from OID' --OK
    , 'TAC Export Sales Data from ERP to Peugeot' --OK
    , 'TAC Discovery Interface GL' , 'TAC EDI PO Interface' , 'Synchronize Workflow LOCAL tables (Report Set)'
    --, 'Workflow Background Process'      --UNION        --Missing 2
    --, 'Workflow Background Process (PA Budget Integration Workflow)'   --OK
    --, 'Workflow Background Process (OM Order Line)'       --OK
    --, 'Workflow Background Process (Expenses:::Yes:Yes:Yes)'     --OK
    --, 'Workflow Background Process (PO Requisition Approval:::Yes:No:)'  --OK
    --, 'Workflow Background Process (PO Requisition Approval:::Yes:Yes:)'  --OK
    --, 'Workflow Background Process (PO Approval:::Yes:Yes:)'     --OK
    --, 'Workflow Background Process (PO Approval:::Yes:No:)'     --Missing
    --, 'Workflow Background Process (PO Approval:::No:Yes:)'     --Missing
    -- , 'TAC AP Supplier Interface Program'  --C
    --, 'JA Thai Vendor WHT Certificate Report for Generate WHT Number (DTAC) AP (NEW)' --Users' Request --UNION OK
    --'Cost Manager (Actual Cost Worker)'  --there're 6 org running  --OK
    --, 'CTB Paylink Cheque'  --Users' requests --UN.ION OK
    --'Payables Transfer to General Ledger (HO)'  --OK All
    --'Payables Transfer to General Ledger (PSL)'
    --'Payables Transfer to General Ledger (KKC)'
    --'Payables Transfer to General Ledger (SRC)'
    --'Payables Transfer to General Ledger (SRT)'
    --, 'Program - Import Journals'  --UNION OK
    --,'JA Thai Vendor WHT Certificate Report for Generate WHT Number (DTAC) AP (NEW)'
    --, 'CTB Paylink Cheque' --Users' Requests
    --, 'CTB Media Clearing'                                                                                  --Users' Requests
    , 'Workflow Background Process'
    --, 'Journal Import'
    , 'TAC : Interface Create PO - FXTH'
    , 'TAC : Interface AP Invoice - FXTH'
    , 'TAC : Export Master Employee - ARMS'
    , 'TAC : Export Asset Master PO - ARMS'
    , 'TAC : Export Asset Location - ARMS'
    , 'TAC : Export Master Asset Category - ARMS (All OU)'
    )
  OR DECODE (FND_CONCURRENT_REQUESTS.DESCRIPTION, NULL, FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME, FND_CONCURRENT_REQUESTS.DESCRIPTION
    || ' ('
    || FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME
    || ')' ) LIKE 'TAC : Import POS%' )
  )
WHERE 0     =0
AND max_con = REQUEST_ID
  --AND FND_CONCURRENT_REQUESTS.RESPONSIBILITY_APPLICATION_ID = AP.APPLICATION_ID(+)
  -- and FND_CONCURRENT_REQUESTS.DESCRIPTION is not null
;
