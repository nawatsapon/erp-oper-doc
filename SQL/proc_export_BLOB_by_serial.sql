--ALTER SESSION SET CURRENT_SCHEMA = sys;
DECLARE
TYPE d_TotalSize
IS
  TABLE OF NUMBER;
  t_TotalSize d_TotalSize;
TYPE d_file_name
IS
  TABLE OF VARCHAR2(100);
  t_file_name d_file_name;
TYPE d_sub_num
IS
  TABLE OF VARCHAR2(100);
  t_sub_num d_sub_num;
  t_blob BLOB;
  t_len       NUMBER;
  --t_TotalSize NUMBER;
  -- t_file_name VARCHAR2(100);
  -- t_sub_num VARCHAR2(100);
  t_output UTL_FILE.file_type;
  t_position  NUMBER := 1;
  t_chucklen  NUMBER := 4096;
  t_chuck raw(4096);
  t_remain NUMBER;
BEGIN
  -- *** Select ROW to export here. ***
  -- Get length of blob
  SELECT DBMS_LOB.getlength (CUSTOMERATTACHMENT.ATTACHMENT),
    TMP_NBTC_LIST.SIMSERIAL
    || '_1.jpg',
    TMP_NBTC_LIST.SIMSERIAL BULK COLLECT
  INTO t_TotalSize,
    t_file_name,
    t_sub_num
  FROM TMP_NBTC_LIST,
  APPLICATION_PREPAID,
  CUSTOMERATTACHMENT
  WHERE 0                                 =0
  AND TMP_NBTC_LIST.SIMSERIAL  = APPLICATION_PREPAID.SIMCARD(+)
  AND APPLICATION_PREPAID.CUSTOMERID      = CUSTOMERATTACHMENT.CUSTOMERID(+)
  --AND EREGISOWN.APPLICATION_PREPAID.activatestatus IN ('25')
  AND CUSTOMERATTACHMENT.ATTACHMENT      IS NOT NULL
 -- AND TMP_NBTC_LIST.SIMSERIAL IN ('8966051503450576074',
--'8966051505468112768'
--)
  ;
  --t_remain   := t_TotalSize;
  
  -- *** Get and Render BLOB ***
  FOR indx IN 1 .. t_file_name.COUNT
  LOOP
    t_position := 1;
    t_chucklen  := 4096;
    t_remain := t_TotalSize(indx);
    --dbms_output.put_line(to_char(t_TotalSize(indx)) || '| t_position' || to_char(t_position));
    -- The directory TMP_IMG_FOLDER should exist before executing    
    t_output := UTL_FILE.fopen('TMP_IMG_AUDIT', t_file_name(indx), 'wb', 32760);
    --dbms_output.put_line(t_file_name(indx));
    -- *** Get BLOB ***
    SELECT CUSTOMERATTACHMENT.ATTACHMENT
    INTO t_blob
    FROM TMP_NBTC_LIST,
    APPLICATION_PREPAID,
    CUSTOMERATTACHMENT
    WHERE 0                                 =0
    AND TMP_NBTC_LIST.SIMSERIAL  = APPLICATION_PREPAID.SIMCARD(+)
    AND APPLICATION_PREPAID.CUSTOMERID      = CUSTOMERATTACHMENT.CUSTOMERID(+)
    --AND APPLICATION_PREPAID.activatestatus IN ('25')
    AND CUSTOMERATTACHMENT.ATTACHMENT      IS NOT NULL
    and TMP_NBTC_LIST.SIMSERIAL IN (t_sub_num(indx));
    --dbms_output.put_line(to_char(t_TotalSize));
    --  **** Render BLOB ***
    --dbms_output.put_line(to_char(t_TotalSize(indx)) || '|' || to_char(t_position));
    WHILE t_position < t_TotalSize(indx)
    LOOP
    DBMS_LOB.READ (t_blob, t_chucklen, t_position, t_chuck);
    UTL_FILE.put_raw (t_output, t_chuck);
    UTL_FILE.fflush (t_output);
    t_position   := t_position + t_chucklen;
    t_remain     := t_remain   - t_chucklen;
    IF t_remain   < 4096 THEN
    t_chucklen := t_remain;
    END IF;
    END LOOP;
    --dbms_output.put_line(to_char(t_TotalSize(indx)) || '|' || to_char(t_position)); 
    UTL_FILE.FCLOSE(t_output);
    dbms_output.put_line(t_file_name(indx));    
  END LOOP;
END;