SELECT subinv_code,
  subinv_desc,
  doc_no,
  doc_date,
  item_code,
  card_price,
  unitprice,
  item_desc,
  SUM(iv_amount) iv_amount,
  SUM(iv_qty) iv_qty,
  SUM(cn_amount) cn_amount,
  SUM(cn_qty) cn_qty,
  SUM(amount) amount,
  SUM(qty) qty,
  REFER_IV_NO,
  REF_INV.REF_DOC_DATE
FROM
  (SELECT ts.subinv_code,
    sub.subinv_desc subinv_desc,
    ts.doc_no,
    TO_DATE(TO_CHAR(ts.doc_date,'DD/MM/RRRR'),'DD/MM/RRRR') doc_date ,
    td.item_code,
    td.unitprice,
    it.card_price,
    it.item_desc item_desc,
    DECODE(ts.doc_type, '1', td.AMOUNT - NVL(DECODE(SIGN(ts.prom_disc_tot - SUM(ROUND((td.AMOUNT/ts.GROSS_AMT) * ts.prom_disc_tot, 2)) over (partition BY ts.OU_CODE, ts.SUBINV_CODE, ts.DOC_NO, ts.DOC_TYPE)), 0, ROUND((td.AMOUNT/ts.GROSS_AMT) * ts.prom_disc_tot, 2), DECODE(COUNT(td.seq_no) over (partition BY ts.OU_CODE, ts.SUBINV_CODE, ts.DOC_NO, ts.DOC_TYPE order by td.seq_no, td.item_code) , COUNT(td.seq_no) over (partition BY ts.OU_CODE, ts.SUBINV_CODE, ts.DOC_NO, ts.DOC_TYPE) ,ROUND((td.AMOUNT/ts.GROSS_AMT) * ts.prom_disc_tot, 2) + ts.prom_disc_tot - SUM(ROUND((td.AMOUNT/ts.GROSS_AMT) * ts.prom_disc_tot, 2)) over (partition BY ts.OU_CODE, ts.SUBINV_CODE, ts.DOC_NO, ts.DOC_TYPE) , ROUND((td.AMOUNT/ts.GROSS_AMT) * ts.prom_disc_tot, 2))), 0), '3', NULL) IV_AMOUNT ,
    SUM(DECODE(ts.doc_type,'1',td.qty)) IV_QTY,
    DECODE(ts.doc_type, '3', td.AMOUNT         - NVL(DECODE(SIGN(ts.prom_disc_tot - SUM(ROUND((td.AMOUNT/ts.GROSS_AMT) * ts.prom_disc_tot, 2)) over (partition BY ts.OU_CODE, ts.SUBINV_CODE, ts.DOC_NO, ts.DOC_TYPE)), 0, ROUND((td.AMOUNT/ts.GROSS_AMT) * ts.prom_disc_tot, 2), DECODE(COUNT(td.seq_no) over (partition BY ts.OU_CODE, ts.SUBINV_CODE, ts.DOC_NO, ts.DOC_TYPE order by td.seq_no, td.item_code) , COUNT(td.seq_no) over (partition BY ts.OU_CODE, ts.SUBINV_CODE, ts.DOC_NO, ts.DOC_TYPE) ,ROUND((td.AMOUNT/ts.GROSS_AMT) * ts.prom_disc_tot, 2) + ts.prom_disc_tot - SUM(ROUND((td.AMOUNT/ts.GROSS_AMT) * ts.prom_disc_tot, 2)) over (partition BY ts.OU_CODE, ts.SUBINV_CODE, ts.DOC_NO, ts.DOC_TYPE) ,ROUND((td.AMOUNT/ts.GROSS_AMT) * ts.prom_disc_tot, 2))), 0), '1', NULL) * -1 CN_AMOUNT,
    SUM(DECODE(ts.doc_type,'3',                -1*td.qty)) CN_QTY,
    NVL(DECODE(ts.doc_type, '1', td.AMOUNT     - NVL(DECODE(SIGN(ts.prom_disc_tot - SUM(ROUND((td.AMOUNT/ts.GROSS_AMT) * ts.prom_disc_tot, 2)) over (partition BY ts.OU_CODE, ts.SUBINV_CODE, ts.DOC_NO, ts.DOC_TYPE)), 0, ROUND((td.AMOUNT/ts.GROSS_AMT) * ts.prom_disc_tot, 2), DECODE(COUNT(td.seq_no) over (partition BY ts.OU_CODE, ts.SUBINV_CODE, ts.DOC_NO, ts.DOC_TYPE order by td.seq_no, td.item_code) , COUNT(td.seq_no) over (partition BY ts.OU_CODE, ts.SUBINV_CODE, ts.DOC_NO, ts.DOC_TYPE) ,ROUND((td.AMOUNT/ts.GROSS_AMT) * ts.prom_disc_tot, 2) + ts.prom_disc_tot - SUM(ROUND((td.AMOUNT/ts.GROSS_AMT) * ts.prom_disc_tot, 2)) over (partition BY ts.OU_CODE, ts.SUBINV_CODE, ts.DOC_NO, ts.DOC_TYPE) ,ROUND((td.AMOUNT/ts.GROSS_AMT) * ts.prom_disc_tot, 2))), 0), '3', NULL) , 0) + NVL(DECODE(ts.doc_type, '3', td.AMOUNT - NVL(DECODE(SIGN(ts.prom_disc_tot - SUM(ROUND((td.AMOUNT/ts.GROSS_AMT) * ts.prom_disc_tot, 2)) over (partition BY ts.OU_CODE, ts.SUBINV_CODE, ts.DOC_NO, ts.DOC_TYPE)), 0, ROUND((
    td.AMOUNT                                  /ts.GROSS_AMT) * ts.prom_disc_tot, 2), DECODE(COUNT(td.seq_no) over (partition BY ts.OU_CODE, ts.SUBINV_CODE, ts.DOC_NO, ts.DOC_TYPE order by td.seq_no, td.item_code) , COUNT(td.seq_no) over (partition BY ts.OU_CODE, ts.SUBINV_CODE, ts.DOC_NO, ts.DOC_TYPE) ,ROUND((td.AMOUNT/ts.GROSS_AMT) * ts.prom_disc_tot, 2) + ts.prom_disc_tot - SUM(ROUND((td.AMOUNT/ts.GROSS_AMT) * ts.prom_disc_tot, 2)) over (partition BY ts.OU_CODE, ts.SUBINV_CODE, ts.DOC_NO, ts.DOC_TYPE) ,ROUND((td.AMOUNT/ts.GROSS_AMT) * ts.prom_disc_tot, 2))), 0), '1', NULL) * -1, 0) AMOUNT,
    NVL(SUM(DECODE(ts.doc_type,'1',td.qty)),0) + NVL(SUM(DECODE(ts.doc_type,'3',-1*td.qty)),0) QTY,
    TS.REFER_IV_NO
  FROM pb_subinv sub,
    IN_CAT ct,
    in_item it,
    pb_area_dtl a,
    pb_sales_area sa,
    sa_trans_dtl td,
    sa_transaction ts
  WHERE 0                  =0
  AND NVL(ts.status,'N')  <> 'C'
  AND TRUNC(ts.doc_date)  >= to_date('01/06/2017','dd/mm/yyyy')
  AND TRUNC(ts.doc_date)  <= to_date('31/07/2017','dd/mm/yyyy')
  AND a.inactive          IS NULL
  AND NVL(ts.GROSS_AMT, 0) > 0
  AND td.ou_code           = ts.ou_code
  AND td.subinv_code       = ts.subinv_code
  AND td.doc_type          = ts.doc_type
  AND td.doc_no            = ts.doc_no
  AND a.ou_code            = ts.ou_code
  AND a.subinv_code        = ts.subinv_code
  AND it.ou_code           = td.ou_code
  AND it.item_code         = td.item_code
  AND ct.ou_code           = it.ou_code
  AND ct.cat_set_code      = it.cat_set_code
  AND ct.cat_code          = it.cat_code
  AND sa.ou_code           = a.ou_code
  AND sa.area_code         = a.area_code
  AND sub.ou_code          = td.ou_code
  AND sub.subinv_code      = td.subinv_code
  and td.item_code          IN ('PRCO0000131',
'PRCO0000132',
'PRCO0000134',
'PRCO0000136',
'PRCO0000141',
'PRCO0000142',
'PRCO0000144',
'PRCO0000146',
'PRCO0000161',
'PRCO0000361',
'PRCO0000411',
'PRCO0000421',
'PRCO0000471',
'PRCO0000531',
'PRCO0000771',
'PRCO0000781',
'PRCO0000941',
'PRCO0001021',
'PRCO0001151',
'PRCO0001251',
'PRCO0001281',
'S18WPA67001',
'S18WPA81001',
'S18WPA87001',
'S18WPB07001',
'S18WPB13001',
'S18WPB60001',
'PRCO0001511',
'PRCO0001461',
'PRCO0001451',
'PRCO0001441' ) 
  GROUP BY ts.subinv_code,
    sub.subinv_desc,
    TO_DATE(TO_CHAR(ts.doc_date,'DD/MM/RRRR'),'DD/MM/RRRR'),
    ts.OU_CODE,
    ts.SUBINV_CODE,
    ts.DOC_NO,
    ts.DOC_TYPE,
    td.seq_no,
    td.amount,
    ct.cat_set_code ,
    td.item_code,
    it.card_price,
    td.unitprice,
    ts.doc_type,
    ts.prom_disc_tot,
    ts.Gross_Amt,
    it.item_desc,
    TS.REFER_IV_NO
  ) INV_CN,
  (select DOC_NO REF_DOC_NO,doc_date REF_DOC_DATE from sa_transaction) REF_INV
  where INV_CN.REFER_IV_NO = REF_INV.REF_DOC_NO(+)
GROUP BY subinv_code,
  subinv_desc,
  DOC_NO,
  doc_date,
  item_code,
  card_price,
  unitprice,
  item_desc,
  REFER_IV_NO,
  REF_INV.REF_DOC_DATE
ORDER BY subinv_code,
  item_code,
  doc_date