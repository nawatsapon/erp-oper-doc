SELECT DISTINCT CN.CREATE_DATE,
  CN.PART_CODE,
  CN.SERIAL,
  MSN.ATTRIBUTE1 SUB,
  CN.UNIT_LIST_PRICE,
  MSN.ATTRIBUTE2 EXPIRE_DATE,
  CN.TRX_NUMBER,
  CN.C_INV_QTY,
  CN.TAX_CODE,
  --AVAT.TAX_RATE,
  (
  SELECT DISTINCT TAX_RATE
  FROM AR_VAT_TAX_ALL_B
  WHERE TAX_CODE = CN.TAX_CODE
  ) TAX_RATE,
  (SELECT rct.trx_number
  FROM ra_customer_trx_all rct ,
    ra_customer_trx_lines_all rctl
  WHERE 0                  =0
  AND rct.CUSTOMER_TRX_ID  = rctl.CUSTOMER_TRX_ID
  AND customer_trx_line_id = cn.reference_customer_trx_line_id
  ) invoice_num
FROM
  (SELECT --OEH.ORDER_NUMBER,
    --OEH.ORDER_TYPE,
    ool.reference_header_id,
    ool.reference_line_id,
    MMT.INVENTORY_ITEM_ID,
    TO_CHAR(MMT.TRANSACTION_DATE,'dd/mm/yyyy HH24:MM:SS') CREATE_DATE,
    OOL.ORDERED_ITEM PART_CODE,
    SERIAL_NUMBER SERIAL,
    NULL SUB,
    OOL.UNIT_LIST_PRICE,
    NULL EXPIRE_DATE,
    oeh.order_number TRX_NUMBER,
    OOL.ORDERED_QUANTITY C_INV_QTY,
    ool.TAX_CODE,
    ool.reference_customer_trx_line_id
  FROM mtl_material_transactions mmt,
    MTL_UNIT_TRANSACTIONS_ALL_V msl,
    oe_order_lines_all ool,
    (SELECT OTY.Name order_type,
      oeh.*
    FROM oe_order_headers_all oeh,
      OE_TRANSACTION_TYPES_TL OTY
    WHERE 0               =0
    AND oeh.ORDER_TYPE_ID = OTY.TRANSACTION_TYPE_ID
      --AND ORDER_NUMBER      = '2017000143'
      --AND OTY.NAME          = 'R02-PR-N11'
    ) oeh
  WHERE 0                                 =0
  AND mmt.trx_source_line_id              = ool.line_id
  AND MMT.INVENTORY_ITEM_ID               = OOL.INVENTORY_ITEM_ID
  AND MMT.TRANSACTION_ID                  = msl.TRANSACTION_ID
  AND MMT.ORGANIZATION_ID                 = MSL.ORGANIZATION_ID
  AND mmt.transaction_type_id             = 15
  AND oeh.header_id                       = ool.header_id
  AND ool.REFERENCE_CUSTOMER_TRX_LINE_ID IS NOT NULL
  AND TRUNC(MMT.TRANSACTION_DATE)        >= to_date('01/05/2018','dd/mm/yyyy')
  AND TRUNC(MMT.TRANSACTION_DATE)        < to_date('01/06/2018','dd/mm/yyyy')
  AND MMT.INVENTORY_ITEM_ID              IN ('530803', '530804', '530805', '530806', '530807', '530808', '530809', '530810', '530812', '563743', '570668', '570669', '593262', '630193', '684745', '684746', '724743', '826766', '893979', '923757', '956759', '972117', '972120', '972123', '972127', '995753', '995754', '1001753', '1022758', '1075756', '1096756', '1109753', '1109754', '1109755', '364743', '419755', '430743', '481744', '484743', '747744', '1075757', '1079757', '1108759', '1108760', '302798', '337926', '1077753', '1189785', '1189786')
  ) cn,
  (SELECT inventory_item_id,
    serial_number,
    attribute1,
    attribute2
  FROM MTL_SERIAL_NUMBERS
  ) MSN
WHERE 0                   =0
AND MSN.SERIAL_NUMBER     = cn.SERIAL
AND MSN.inventory_item_id = cn.INVENTORY_ITEM_ID ;
SELECT * FROM oe_order_lines_all
