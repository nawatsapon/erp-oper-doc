SELECT rct.TRX_NUMBER,
  tt.NAME INV_TYPE,
  rct.TRX_DATE,
  org.LOCATION_CODE,
  msi.CONCATENATED_SEGMENTS C_Product_Code ,
  msi.DESCRIPTION C_Description ,
  DECODE(msi.PRIMARY_UOM_CODE,NVL(rctl.UOM_CODE,msi.PRIMARY_UOM_CODE),rctl.QUANTITY_INVOICED,inv_convert.inv_um_convert(rctl.INVENTORY_ITEM_ID,2,rctl.QUANTITY_INVOICED,rctl.UOM_CODE,msi.PRIMARY_UOM_CODE,NULL,NULL)) C_Inv_Qty ,
  NVL(rctl.EXTENDED_AMOUNT,0)+ NVL(ltax.EXTENDED_AMOUNT,0) C_Inv_Amount ,
  to_number(NULL) C_CN_Qty ,
  to_Number(NULL) C_CN_Amount,
  NULL CN_NO,
  NULL CN_TYPE,
  NULL CN_DATE,
  AVAT.TAX_CODE,
  AVAT.TAX_RATE
FROM ra_customer_trx_all rct ,
  ra_customer_trx_lines_all rctl ,
  ra_customer_trx_lines_all ltax ,
  ra_cust_trx_types_all tt ,
  mtl_system_items_vl msi ,
  mtl_item_categories mic ,
  mtl_categories mc ,
  mtl_category_sets mcs ,
  mtl_units_of_measure_vl uom ,
  HR_ORGANIZATION_UNITS_V org,
  AR_VAT_TAX_ALL_B AVAT
WHERE rct.CUSTOMER_TRX_ID      = rctl.CUSTOMER_TRX_ID
AND rct.COMPLETE_FLAG          = 'Y'
AND rct.CUST_TRX_TYPE_ID       = tt.CUST_TRX_TYPE_ID
AND rct.ORG_ID                 = org.organization_id
AND tt.TYPE                   IN ('INV','DM')
AND rctl.LINE_TYPE             = 'LINE'
AND rctl.INVENTORY_ITEM_ID     = msi.inventory_item_id
AND rctl.warehouse_ID          = msi.organization_id
AND rctl.CUSTOMER_TRX_LINE_ID  = ltax.LINK_TO_CUST_TRX_LINE_ID
AND msi.inventory_item_id      = mic.inventory_item_id
AND msi.Organization_Id        = mic.Organization_Id
AND mic.category_set_id        = mcs.category_set_id
AND mcs.CATEGORY_SET_NAME      = 'Inventory'
AND mic.category_id            = mc.CATEGORY_ID
AND rctl.UOM_CODE              = uom.UOM_CODE(+)
AND rctl.vat_tax_id            = AVAT.vat_tax_id(+)
AND rctl.SET_OF_BOOKS_ID       = AVAT.SET_OF_BOOKS_ID(+)
AND msi.CONCATENATED_SEGMENTS IN ('PRCO0000131', 'PRCO0000132', 'PRCO0000134', 'PRCO0000136', 'PRCO0000141', 'PRCO0000142', 'PRCO0000144', 'PRCO0000146', 'PRCO0000161', 'PRCO0000361', 'PRCO0000411', 'PRCO0000421', 'PRCO0000471', 'PRCO0000531', 'PRCO0000771', 'PRCO0000781', 'PRCO0000941', 'PRCO0001021', 'PRCO0001151', 'PRCO0001251', 'PRCO0001281', 'PRCO0001351', 'PRCO0001361', 'PRCO0001371', 'PRCO0001381', 'PRCO0001441', 'PRCO0001451', 'PRCO0001461', 'PRCO0001511', 'PRCO0001671', 'PRCO0001751', 'PRCO0001801', 'PRCO0001811', 'PRCO0001821', 'S18WPA67001', 'S18WPA81001', 'S18WPA87001', 'S18WPB07001', 'S18WPB13001', 'S18WPB60001', 'PRCO0001681', 'PRCO0001701', 'PRCO0001781', 'PRCO0001791', 'S18WPA38001', 'S18WPA49001', 'SSPR0000011', 'PRCO0001951', 'PRCO0001961')
AND TRUNC(rct.trx_date)       >= to_date('01/05/2018','dd/mm/yyyy')
AND TRUNC(rct.trx_date)       < to_date('01/06/2018','dd/mm/yyyy')
UNION ALL
SELECT inv_h.TRX_NUMBER INV_NO,
  inv_tt.NAME INV_TYPE,
  INV_H.TRX_DATE INV_DATE,
  org.LOCATION_CODE,
  msi.concatenated_segments C_Product_Code ,
  msi.description C_Description ,
  to_number(NULL) C_Inv_Qty ,
  to_Number(NULL) C_Inv_Amount ,
  DECODE(msi.PRIMARY_UOM_CODE,NVL(rctl.UOM_CODE,msi.PRIMARY_UOM_CODE),rctl.QUANTITY_CREDITED,inv_convert.inv_um_convert(rctl.INVENTORY_ITEM_ID,2,rctl.QUANTITY_CREDITED,rctl.UOM_CODE,msi.PRIMARY_UOM_CODE,NULL,NULL)) C_CN_Qty ,
  NVL(rctl.EXTENDED_AMOUNT,0)+ NVL(ltax.EXTENDED_AMOUNT,0) C_CN_Amount,
  rct.TRX_NUMBER CN_NO,
  tt.NAME CN_TYPE,
  rct.TRX_DATE CN_DATE,
  AVAT.TAX_CODE,
  AVAT.TAX_RATE
FROM ra_customer_trx_all rct ,
  ra_customer_trx_lines_all rctl ,
  ra_customer_trx_lines_all ltax ,
  ra_cust_trx_types_all tt ,
  mtl_system_items_vl msi ,
  mtl_item_categories mic ,
  mtl_categories mc ,
  mtl_category_sets mcs ,
  mtl_units_of_measure_vl uom ,
  HR_ORGANIZATION_UNITS_V org,
  ra_customer_trx_all inv_h,
  ra_cust_trx_types_all inv_tt,
  AR_VAT_TAX_ALL_B AVAT
WHERE rct.CUSTOMER_TRX_ID         = rctl.CUSTOMER_TRX_ID
AND rct.COMPLETE_FLAG             = 'Y'
AND rct.CUST_TRX_TYPE_ID          = tt.CUST_TRX_TYPE_ID
AND rct.ORG_ID                    = org.organization_id
AND tt.TYPE                       = 'CM'
AND rctl.LINE_TYPE                = 'LINE'
AND rctl.INVENTORY_ITEM_ID        = msi.inventory_item_id
AND rctl.Warehouse_ID             = msi.organization_id
AND rctl.CUSTOMER_TRX_LINE_ID     = ltax.LINK_TO_CUST_TRX_LINE_ID
AND msi.inventory_item_id         = mic.inventory_item_id
AND msi.Organization_Id           = mic.Organization_Id
AND mic.category_set_id           = mcs.category_set_id
AND inv_h.CUST_TRX_TYPE_ID        = inv_tt.CUST_TRX_TYPE_ID
AND inv_tt.TYPE                  IN ('INV','DM')
AND rctl.previous_customer_trx_id = inv_h.customer_trx_id
AND mcs.category_set_name         = 'Inventory'
AND mic.category_id               = mc.category_id
AND rctl.uom_code                 = uom.uom_code(+)
AND rctl.vat_tax_id               = AVAT.vat_tax_id(+)
AND rctl.SET_OF_BOOKS_ID          = AVAT.SET_OF_BOOKS_ID(+)
AND msi.CONCATENATED_SEGMENTS    IN ('PRCO0000131', 'PRCO0000132', 'PRCO0000134', 'PRCO0000136', 'PRCO0000141', 'PRCO0000142', 'PRCO0000144', 'PRCO0000146', 'PRCO0000161', 'PRCO0000361', 'PRCO0000411', 'PRCO0000421', 'PRCO0000471', 'PRCO0000531', 'PRCO0000771', 'PRCO0000781', 'PRCO0000941', 'PRCO0001021', 'PRCO0001151', 'PRCO0001251', 'PRCO0001281', 'PRCO0001351', 'PRCO0001361', 'PRCO0001371', 'PRCO0001381', 'PRCO0001441', 'PRCO0001451', 'PRCO0001461', 'PRCO0001511', 'PRCO0001671', 'PRCO0001751', 'PRCO0001801', 'PRCO0001811', 'PRCO0001821', 'S18WPA67001', 'S18WPA81001', 'S18WPA87001', 'S18WPB07001', 'S18WPB13001', 'S18WPB60001', 'PRCO0001681', 'PRCO0001701', 'PRCO0001781', 'PRCO0001791', 'S18WPA38001', 'S18WPA49001')
  -- Start Parameter --
AND TRUNC(rct.trx_date) >= to_date('01/05/2018','dd/mm/yyyy')
AND TRUNC(rct.trx_date) < to_date('01/06/2018','dd/mm/yyyy');
