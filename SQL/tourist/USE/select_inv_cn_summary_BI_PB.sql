SELECT CUST.CUSTOMER_NUMBER,
  CUST.CUSTOMER_NAME,
  rct.TRX_NUMBER,
  tt.NAME INV_TYPE,
  rct.TRX_DATE,
  org.LOCATION_CODE,
  msi.CONCATENATED_SEGMENTS C_Product_Code ,
  NVL(msi.DESCRIPTION,rctl.DESCRIPTION) C_Description ,
  DECODE(msi.PRIMARY_UOM_CODE,NVL(rctl.UOM_CODE,msi.PRIMARY_UOM_CODE),rctl.QUANTITY_INVOICED,inv_convert.inv_um_convert(rctl.INVENTORY_ITEM_ID,2,rctl.QUANTITY_INVOICED,rctl.UOM_CODE,msi.PRIMARY_UOM_CODE,NULL,NULL)) C_Inv_Qty ,
  NVL(rctl.EXTENDED_AMOUNT,0)+ NVL(ltax.EXTENDED_AMOUNT,0) C_Inv_Amount ,
  to_number(NULL) C_CN_Qty ,
  to_Number(NULL) C_CN_Amount,
  NULL CN_NO,
  NULL CN_TYPE,
  NULL CN_DATE,
  'INV' DOC_TYPE
  --,rct.ORG_ID
FROM ra_customer_trx_all rct ,
  ra_customer_trx_lines_all rctl ,
  ra_customer_trx_lines_all ltax ,
  ra_cust_trx_types_all tt ,
  RA_CUSTOMERS cust,
  mtl_system_items_vl msi ,
  /*mtl_item_categories mic ,
  mtl_categories mc ,
  mtl_category_sets mcs ,*/
  mtl_units_of_measure_vl uom ,
  HR_ORGANIZATION_UNITS_V org
WHERE rct.CUSTOMER_TRX_ID     = rctl.CUSTOMER_TRX_ID
AND rct.COMPLETE_FLAG         = 'Y'
AND rct.CUST_TRX_TYPE_ID      = tt.CUST_TRX_TYPE_ID
AND rct.ORG_ID                = org.organization_id
AND tt.TYPE                  IN ('INV','DM')
AND rctl.LINE_TYPE            = 'LINE'
AND rctl.INVENTORY_ITEM_ID    = msi.inventory_item_id(+)
AND rctl.warehouse_ID         = msi.organization_id(+)
AND rctl.CUSTOMER_TRX_LINE_ID = ltax.LINK_TO_CUST_TRX_LINE_ID
and RCT.BILL_TO_CUSTOMER_ID = CUST.CUSTOMER_ID(+)
  /*AND msi.inventory_item_id      = mic.inventory_item_id
  AND msi.Organization_Id        = mic.Organization_Id
  AND mic.category_set_id        = mcs.category_set_id
  AND mcs.CATEGORY_SET_NAME      = 'Inventory'
  AND mic.category_id            = mc.CATEGORY_ID*/
AND rctl.UOM_CODE = uom.UOM_CODE(+)
  --AND rct.TRX_NUMBER = '2018000001'
  --and tt.NAME = '5P-SIN-0846'
AND rct.ORG_ID           = 218
AND TRUNC(rct.trx_date) >= to_date('01/10/2017','dd/mm/yyyy')
AND TRUNC(rct.trx_date) <= to_date('28/02/2018','dd/mm/yyyy')
UNION ALL
SELECT CUST.CUSTOMER_NUMBER,
  CUST.CUSTOMER_NAME,
  inv_h.TRX_NUMBER INV_NO,
  inv_tt.NAME INV_TYPE,
  INV_H.TRX_DATE INV_DATE,
  org.LOCATION_CODE,
  msi.concatenated_segments C_Product_Code ,
  NVL(msi.description,rctl.DESCRIPTION) C_Description ,
  to_number(NULL) C_Inv_Qty ,
  to_Number(NULL) C_Inv_Amount ,
  DECODE(msi.PRIMARY_UOM_CODE,NVL(rctl.UOM_CODE,msi.PRIMARY_UOM_CODE),rctl.QUANTITY_CREDITED,inv_convert.inv_um_convert(rctl.INVENTORY_ITEM_ID,2,rctl.QUANTITY_CREDITED,rctl.UOM_CODE,msi.PRIMARY_UOM_CODE,NULL,NULL)) C_CN_Qty ,
  NVL(rctl.EXTENDED_AMOUNT,0)+ NVL(ltax.EXTENDED_AMOUNT,0) C_CN_Amount,
  rct.TRX_NUMBER CN_NO,
  tt.NAME CN_TYPE,
  rct.TRX_DATE CN_DATE,
  'CN' DOC_TYPE
FROM ra_customer_trx_all rct ,
  ra_customer_trx_lines_all rctl ,
  ra_customer_trx_lines_all ltax ,
  ra_cust_trx_types_all tt ,
  RA_CUSTOMERS cust,
  mtl_system_items_vl msi ,
  /*mtl_item_categories mic ,
  mtl_categories mc ,
  mtl_category_sets mcs ,*/
  mtl_units_of_measure_vl uom ,
  HR_ORGANIZATION_UNITS_V org,
  ra_customer_trx_all inv_h,
  ra_cust_trx_types_all inv_tt
WHERE rct.CUSTOMER_TRX_ID     = rctl.CUSTOMER_TRX_ID
AND rct.COMPLETE_FLAG         = 'Y'
AND rct.CUST_TRX_TYPE_ID      = tt.CUST_TRX_TYPE_ID
AND rct.ORG_ID                = org.organization_id
AND tt.TYPE                   = 'CM'
AND rctl.LINE_TYPE            = 'LINE'
AND rctl.INVENTORY_ITEM_ID    = msi.inventory_item_id(+)
AND rctl.Warehouse_ID         = msi.organization_id(+)
AND rctl.CUSTOMER_TRX_LINE_ID = ltax.LINK_TO_CUST_TRX_LINE_ID
and rct.BILL_TO_CUSTOMER_ID = CUST.CUSTOMER_ID(+)
  /*AND msi.inventory_item_id      = mic.inventory_item_id
  AND msi.Organization_Id        = mic.Organization_Id
  AND mic.category_set_id        = mcs.category_set_id
  AND mcs.category_set_name      = 'Inventory'
  AND mic.category_id            = mc.category_id*/
AND inv_h.CUST_TRX_TYPE_ID        = inv_tt.CUST_TRX_TYPE_ID
AND inv_tt.TYPE                  IN ('INV','DM')
AND rctl.previous_customer_trx_id = inv_h.customer_trx_id
AND rctl.uom_code                 = uom.uom_code(+)
AND rct.ORG_ID                    = 218
  -- Start Parameter --
AND TRUNC(rct.trx_date) >= to_date('01/10/2017','dd/mm/yyyy')
AND TRUNC(rct.trx_date) <= to_date('28/02/2018','dd/mm/yyyy') ;


select * from RA_CUSTOMERS