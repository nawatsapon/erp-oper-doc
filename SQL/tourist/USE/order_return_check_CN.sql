SELECT CN.CREATE_DATE,
  CN.PART_CODE,
  CN.SERIAL,
  CN.UNIT_LIST_PRICE,
  CN.TRX_NUMBER,
  CN.C_INV_QTY
FROM
  (SELECT --OEH.ORDER_NUMBER,
    --OEH.ORDER_TYPE,
    ool.reference_header_id,
    ool.reference_line_id,
    MMT.INVENTORY_ITEM_ID,
    TO_CHAR(MMT.TRANSACTION_DATE,'dd/mm/yyyy HH24:MM:SS') CREATE_DATE,
    OOL.ORDERED_ITEM PART_CODE,
    SERIAL_NUMBER SERIAL,
    NULL SUB,
    OOL.UNIT_LIST_PRICE,
    NULL EXPIRE_DATE,
    oeh.order_number TRX_NUMBER,
    OOL.ORDERED_QUANTITY C_INV_QTY
  FROM mtl_material_transactions mmt,
    MTL_UNIT_TRANSACTIONS_ALL_V msl,
    oe_order_lines_all ool,
    (SELECT OTY.Name order_type,
      oeh.*
    FROM oe_order_headers_all oeh,
      OE_TRANSACTION_TYPES_TL OTY
    WHERE 0               =0
    AND oeh.ORDER_TYPE_ID = OTY.TRANSACTION_TYPE_ID
      AND ORDER_NUMBER      in ('2019000061',
'2019000060',
'2019000059',
'2019000058',
'2019000057',
'2019000056',
'2019000055',
'2019000054',
'2019000053',
'2019000052',
'2019000051',
'2019000049',
'2019000050',
'2019000048',
'2019000047',
'2019000046',
'2019000045',
'2019000044',
'2019000043',
'2019000042',
'2019000041',
'2019000040',
'2019000039')
      AND OTY.NAME          = 'R02-PR-N9'
    ) oeh
  WHERE 0                          =0
  AND mmt.trx_source_line_id       = ool.line_id
  AND MMT.INVENTORY_ITEM_ID        = OOL.INVENTORY_ITEM_ID
  AND MMT.TRANSACTION_ID           = msl.TRANSACTION_ID
  AND MMT.ORGANIZATION_ID          = MSL.ORGANIZATION_ID
  AND mmt.transaction_type_id      = 15
  AND oeh.header_id                = ool.header_id
   ) cn;
SELECT * FROM oe_order_lines_all
