/*BEGIN
dbms_application_info.set_client_info(142);
END;*/

SELECT TO_CHAR(INV.TRANSACTION_DATE,'dd/mm/yyyy HH24:MM:SS') CREATE_DATE,
  INV.C_PRODUCT_CODE PART_CODE,
  INV.SERIAL_NUMBER SERIAL,
  MSN.ATTRIBUTE1 SUB,
  INV.PRICE_LIST PRICE_LIST,
  --to_char(to_date(MSN.ATTRIBUTE2,'yyyy/mm/dd'),'dd/mm/yyyy') EXPIRE_DATE
  --to_date(MSN.ATTRIBUTE2, 'YYYY/MM/DD HH:MI:SS') EXPIRE_DATE
  MSN.ATTRIBUTE2 EXPIRE_DATE,
  INV.TRX_NUMBER,
  INV.C_INV_QTY,
  INV.ORG_NAME,
  INV.tax_rate
FROM
  (SELECT RCT.CUSTOMER_TRX_ID,
    RCT.TRX_NUMBER,
    LTAX.CUSTOMER_TRX_LINE_ID,
    RCT.OE_HEADER_ID,
    OEL.LINE_ID,
    MMT.INVENTORY_ITEM_ID,
    MSI.CONCATENATED_SEGMENTS C_PRODUCT_CODE ,
    MSI.DESCRIPTION C_DESCRIPTION ,
    DECODE(MSI.PRIMARY_UOM_CODE,NVL(RCTL.UOM_CODE,MSI.PRIMARY_UOM_CODE),RCTL.QUANTITY_INVOICED,INV_CONVERT.INV_UM_CONVERT(RCTL.INVENTORY_ITEM_ID,2,RCTL.QUANTITY_INVOICED,RCTL.UOM_CODE,MSI.PRIMARY_UOM_CODE,NULL,NULL)) C_INV_QTY ,
    NVL(RCTL.EXTENDED_AMOUNT,0)+ NVL(LTAX.EXTENDED_AMOUNT,0) C_INV_AMOUNT ,
    OEH.ORDER_NUMBER,
    OEH.ORDER_TYPE,
    OEL.UNIT_SELLING_PRICE PRICE_LIST,
    WSH.SRC_REQUESTED_QUANTITY
    --tt.TYPE --,
    ,
    WSH.SOURCE_HEADER_ID,
    WSH.SOURCE_LINE_ID,
    --WSN.FM_SERIAL_NUMBER ,
    WSH.LAST_UPDATE_DATE,
    mmt.transaction_id,
    mmt.TRANSACTION_DATE,
    MSN.SERIAL_NUMBER,
    MSN.ATTRIBUTE1,
    MSN.ATTRIBUTE2,
    RCT.ORG_NAME,
    LTAX.tax_rate
  FROM
    (SELECT ARP_TRX_LINE_UTIL.GET_OE_HEADER_ID(RCT.INTERFACE_HEADER_ATTRIBUTE6, RCT.INTERFACE_HEADER_CONTEXT) OE_HEADER_ID,
      HRU.name ORG_NAME,
      RCT.*
    FROM RA_CUSTOMER_TRX_ALL RCT,
      HR_ORGANIZATION_UNITS HRU
    WHERE 0        =0
    AND RCT.ORG_ID = HRU.ORGANIZATION_ID
      --AND TRUNC(rct.trx_date) >= to_date('01/10/2017','dd/mm/yyyy')
      --AND TRUNC(rct.trx_date) <= to_date('31/10/2017','dd/mm/yyyy')
    ) RCT ,
    RA_CUSTOMER_TRX_LINES_ALL RCTL ,
    RA_CUSTOMER_TRX_LINES_ALL LTAX ,
    RA_CUST_TRX_TYPES_ALL TT ,
    MTL_SYSTEM_ITEMS_VL MSI,
    (SELECT OTY.name ORDER_TYPE,
      OEH.*
    FROM OE_ORDER_HEADERS_ALL OEH,
      OE_TRANSACTION_TYPES_TL OTY
    WHERE 0               =0
    AND OEH.ORDER_TYPE_ID = OTY.TRANSACTION_TYPE_ID
      --AND ORDER_NUMBER      = '2013000073'
      --AND OTY.NAME          = 'S02-PR-N11'
    ) OEH ,
    OE_ORDER_LINES_ALL OEL ,
    WSH_DELIVERABLES_V WSH,
    (SELECT TRANSACTION_ID,
      INVENTORY_ITEM_ID,
      ORGANIZATION_ID,
      TRX_SOURCE_LINE_ID,
      TRANSACTION_DATE
    FROM mtl_material_transactions
    WHERE SOURCE_CODE = 'ORDER ENTRY'
    ) mmt,
    mtl_unit_transactions MSN
    --,WSH_SERIAL_NUMBERS WSN
  WHERE RCT.CUSTOMER_TRX_ID          = RCTL.CUSTOMER_TRX_ID
  AND RCT.COMPLETE_FLAG              = 'Y'
  AND RCT.CUST_TRX_TYPE_ID           = TT.CUST_TRX_TYPE_ID
  AND TT.type                       IN ('INV','DM')
  AND RCTL.LINE_TYPE                 = 'LINE'
  AND RCTL.INVENTORY_ITEM_ID         = MSI.INVENTORY_ITEM_ID
  AND RCTL.WAREHOUSE_ID              = MSI.ORGANIZATION_ID
  AND RCTL.CUSTOMER_TRX_LINE_ID      = LTAX.LINK_TO_CUST_TRX_LINE_ID
  AND RCTL.SALES_ORDER               = TO_CHAR(OEH.ORDER_NUMBER)
  AND RCTL.INTERFACE_LINE_ATTRIBUTE2 = OEH.ORDER_TYPE
  AND RCT.OE_HEADER_ID               = OEH.HEADER_ID
  AND RCT.ORG_ID                     = OEH.ORG_ID
  AND OEH.HEADER_ID                  = OEL.HEADER_ID
  AND RCTL.INVENTORY_ITEM_ID         = OEL.INVENTORY_ITEM_ID
  AND RCTL.SALES_ORDER_LINE          = OEL.LINE_NUMBER
  AND RCTL.SALES_ORDER               = OEH.ORDER_NUMBER
  AND RCT.INTERFACE_HEADER_ATTRIBUTE3= TO_CHAR(WSH.DELIVERY_ID)
  AND WSH.SOURCE_HEADER_ID           = OEH.HEADER_ID
  AND WSH.SOURCE_LINE_ID             = OEL.LINE_ID
  AND mmt.TRX_SOURCE_LINE_ID         = OEL.LINE_ID
  AND MmT.INVENTORY_ITEM_ID          = OEL.INVENTORY_ITEM_ID
  AND MmT.INVENTORY_ITEM_ID          = MSN.INVENTORY_ITEM_ID
  AND MmT.TRANSACTION_ID             = MSN.TRANSACTION_ID
  AND RCT.TRX_NUMBER                IN ('2014000209', '2014000229', '2014000318', '2015000004', '2015000071', '2015000161', '2015000266', '2015000426', '2015000472', '2015000563', '2015000567', '2015000628', '2015000667', '2015000704', '2015000712', '2015000801', '2016000027', '2016000047', '2016000105', '2016000132', '2016000174', '2016000177', '2016000191', '2016000261', '2016000335', '2016000440', '2016000484', '2016000496', '2016000517', '2016000518', '2016000539', '2016000542', '2016000549', '2016000572', '2016000582', '2016000588', '2016000606', '2016000661', '2016000669', '2016000670', '2016000687', '2016000726', '2016000745', '2016000758', '2016000769', '2016000775', '2016000792', '2016000829', '2016000836', '2016000847', '2016000873', '2016000890', '2016000906', '2016000913', '2016000919', '2016000953', '2016000958', '2017000003', '2017000004', '2017000013', '2017000031', '2017000052', '2017000065', '2017000074', '2017000081', '2017000094', '2017000106', '2017000116',
    '2017000135', '2017000155', '2017000174', '2017000213', '2017000225', '2017000254', '2017000279', '2017000296', '2017000319', '2017000343', '2017000351', '2017000363', '2017000378', '2017000400', '2017000440', '2017000451', '2017000483', '2017000189', '2017000195')
    --AND RCT.CUSTOMER_TRX_ID             = 392343
  AND MSI.INVENTORY_ITEM_ID = to_number(RTRIM(LTRIM( :INVENTORY_ITEM_ID)))
  --AND LTAX.tax_rate         = 0
    --AND MSI.CONCATENATED_SEGMENTS = 'PRCO0000141'
    -- AND WSH.DELIVERY_DETAIL_ID = WSN.DELIVERY_DETAIL_ID
  AND NOT EXISTS
    (SELECT 'X'
    FROM
      (SELECT --OEH.ORDER_NUMBER,
        --OEH.ORDER_TYPE,
        ool.reference_header_id,
        ool.reference_line_id,
        SERIAL_NUMBER
      FROM mtl_material_transactions mmt,
        MTL_UNIT_TRANSACTIONS_ALL_V msl,
        oe_order_lines_all ool,
        (SELECT OTY.Name order_type,
          oeh.*
        FROM oe_order_headers_all oeh,
          OE_TRANSACTION_TYPES_TL OTY
        WHERE 0               =0
        AND oeh.ORDER_TYPE_ID = OTY.TRANSACTION_TYPE_ID
          --AND ORDER_NUMBER      = '2017000143'
          --AND OTY.NAME          = 'R02-PR-N11'
        ) oeh
      WHERE 0                     =0
      AND mmt.trx_source_line_id  = ool.line_id
      AND MMT.INVENTORY_ITEM_ID   = OOL.INVENTORY_ITEM_ID
      AND MMT.TRANSACTION_ID      = msl.TRANSACTION_ID
      AND MMT.ORGANIZATION_ID     = MSL.ORGANIZATION_ID
      AND mmt.transaction_type_id = 15
      AND oeh.header_id           = ool.header_id
      AND MMT.INVENTORY_ITEM_ID   = to_number(RTRIM(LTRIM( :INVENTORY_ITEM_ID)))
        --AND TRUNC(MMT.LAST_UPDATE_DATE) >= to_date('01/05/2017','dd/mm/yyyy')
        --AND TRUNC(MMT.LAST_UPDATE_DATE) <= to_date('31/05/2017','dd/mm/yyyy')
      ) cn
    WHERE 0               =0
    AND RCT.OE_HEADER_ID  = cn.reference_header_id
    AND OEL.LINE_ID       = cn.reference_line_id
    AND MSN.SERIAL_NUMBER = cn.SERIAL_NUMBER
    )
    --and rct.interface_header_attribute1 = '2016000008'
  ) INV,
  (SELECT inventory_item_id,
    serial_number,
    attribute1,
    attribute2
  FROM MTL_SERIAL_NUMBERS
  ) MSN
WHERE INV.INVENTORY_ITEM_ID = MSN.INVENTORY_ITEM_ID
AND INV.SERIAL_NUMBER       = MSN.serial_number ;
