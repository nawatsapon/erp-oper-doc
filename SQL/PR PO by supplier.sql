--alter session set nls_language = 'AMERICAN'
SELECT prh.segment1 "PR Number" ,
  prl.line_num ,
  prl.item_description ,
  prl.unit_meas_lookup_code "UOM" ,
  prl.quantity ,
  prl.unit_price ,
  prl.currency_code ,
  prl.rate ,
  prh.creation_date ,
  glc.segment11 Account_code ,
  prl.quantity*prl.unit_price*NVL(prl.rate,1) "Amount" ,
  cat.SEGMENT1
  || '.'
  || cat.SEGMENT2
  || '.'
  || cat.SEGMENT3
  || '.'
  ||cat.SEGMENT4 "CATEGORY" ,
  buy.full_name "Buyer Name" ,
  pera.person_id Requestor_ID ,
  per.full_name "Requester Name" ,
  pera.assignment_number REQUESTOR_EMP_NO
  --ffv.description "Deparment Name"
  ,
  ffv.DEPT ,
  ffv.DIV ,
  prd.project_id ,
  f.segment1 "Project Code" ,
  f.name "Project Name" ,
  g.task_number ,
  prd.expenditure_type ,
  pp.PERSON_ID ,
  pp.FULL_NAME "Approver Name" ,
  pp.EMPLOYEE_NUMBER Last_Approver_EMP_No ,
  pp.ATTRIBUTE3 "GROUP" ,
  ffv2.DEPT ,
  ffv2.DIV ,
  pha.segment1 "PO"
  -- ,pha.vendor_id
  --  ,apv.vendor_id
  ,
  apv.vendor_number ,
  apv.vendor_name
FROM po_requisition_headers_all prh ,
  po_requisition_lines_all prl ,
  po_req_distributions_all prd ,
  gl_code_combinations glc
  --gl_code_combinations glc2
  ,
  PA_PROJECTS_ALL f ,
  PA_TASKS g ,
  po_distributions_all pda ,
  po_headers_all pha ,
  mtl_categories cat ,
  per_all_people_f per ,
  AP_VENDORS_V apv ,
  (SELECT person_id ,
    assignment_id ,
    EFFECTIVE_START_DATE ,
    EFFECTIVE_END_DATE ,
    DEFAULT_CODE_COMB_ID ,
    PER_ASSIGNMENTS_F.ASSIGNMENT_NUMBER
  FROM PER_ASSIGNMENTS_F
  WHERE effective_end_date >= sysdate
    /*and job_id >= 112*/
  ) pera ,
  (SELECT person_id ,
    assignment_id ,
    EFFECTIVE_START_DATE ,
    EFFECTIVE_END_DATE ,
    DEFAULT_CODE_COMB_ID ,
    PER_ASSIGNMENTS_F.ASSIGNMENT_NUMBER
  FROM PER_ASSIGNMENTS_F
  WHERE effective_end_date >= sysdate
    /*and job_id >= 112*/
  ) perb ,
  GL_CODE_COMBINATIONS gcc ,
  Gl_Code_Combinations Gcc2
  /*       (select flex_value_meaning  description  SUMMARY_FLAG
  from FND_FLEX_VALUES_VL
  where FLEX_VALUE_SET_ID = 1009748) FFV */
  ,
  (SELECT FFVL2.FLEX_VALUE,
    FFVL2.DESCRIPTION DEPT,
    FFVL1.DESCRIPTION DIV
    --  FVNH.PARENT_FLEX_VALUE
  FROM FND_FLEX_VALUE_NORM_HIERARCHY FVNH,
    FND_FLEX_VALUES_VL FFVL1,
    FND_FLEX_VALUES_VL FFVL2
  WHERE FVNH.FLEX_VALUE_SET_ID = 1009748
  AND FFVL1.FLEX_VALUE_SET_ID  = 1009748
  AND FFVL2.FLEX_VALUE_SET_ID  = 1009748
  AND FVNH.PARENT_FLEX_VALUE   = FFVL1.FLEX_VALUE
  AND FFVL2.FLEX_VALUE BETWEEN FVNH.CHILD_FLEX_VALUE_LOW AND FVNH.CHILD_FLEX_VALUE_HIGH
    --AND NVL(TRUNC(FFVL2.END_DATE_ACTIVE)
  ) FFV ,
  (SELECT FFVL2.FLEX_VALUE,
    FFVL2.DESCRIPTION DEPT,
    FFVL1.DESCRIPTION DIV
    --
  FROM FND_FLEX_VALUE_NORM_HIERARCHY FVNH,
    FND_FLEX_VALUES_VL FFVL1,
    FND_FLEX_VALUES_VL FFVL2
  WHERE FVNH.FLEX_VALUE_SET_ID = 1009748
  AND FFVL1.FLEX_VALUE_SET_ID  = 1009748
  AND FFVL2.FLEX_VALUE_SET_ID  = 1009748
  AND FVNH.PARENT_FLEX_VALUE   = FFVL1.FLEX_VALUE
  AND FFVL2.FLEX_VALUE BETWEEN FVNH.CHILD_FLEX_VALUE_LOW AND FVNH.CHILD_FLEX_VALUE_HIGH
    --AND NVL(TRUNC(FFVL2.END_DATE_ACTIVE)
  ) FFV2 ,
  per_all_people_f buy ,
  (SELECT *
  FROM PO_ACTION_HISTORY ph1
  WHERE ph1.sequence_num =
    (SELECT MAX(sequence_num)
    FROM PO_ACTION_HISTORY ph2
    WHERE ph1.object_id = ph2.object_id
    AND ph2.action_code = 'APPROVE'
    )
  ) phis ,
  (SELECT person_id ,
    assignment_id ,
    EFFECTIVE_START_DATE ,
    EFFECTIVE_END_DATE ,
    DEFAULT_CODE_COMB_ID ,
    JOB_ID
  FROM PER_ASSIGNMENTS_F
  WHERE effective_end_date >= sysdate
  ) pera2 ,
  PER_PEOPLE_F pp
WHERE prh.requisition_header_id = prl.requisition_header_id
AND prl.requisition_line_id     = prd.requisition_line_id
AND prl.category_id             = cat.CATEGORY_ID
AND prd.project_id              = f.project_id (+)
AND prd.task_id                 = g.task_id (+)
AND prh.preparer_id             = per.person_id(+)
AND prl.suggested_buyer_id      = buy.person_id (+)
AND pera.DEFAULT_CODE_COMB_ID   = gcc.code_combination_id(+)
AND gcc.segment10               = ffv.FLEX_VALUE (+)
AND per.person_id               = pera.PERSON_ID
AND prl.last_update_login      <> '0'
AND prh.authorization_status   <> 'SYSTEM_SAVED'
  --AND prh.creation_date BETWEEN to_date('01/01/2014','dd/mm/yyyy') AND to_date('30/09/2015','dd/mm/yyyy')
AND prh.requisition_header_id = phis.object_id
AND phis.employee_id          = pera2.person_id
AND pera2.person_id           = pp.person_id
AND glc.code_combination_id   = prd.code_combination_id
AND pp.PERSON_ID              = perb.person_id (+)
AND perb.DEFAULT_CODE_COMB_ID = gcc2.code_combination_id(+)
AND gcc2.segment10            = ffv2.FLEX_VALUE (+)
AND prd.distribution_id       = pda.req_distribution_id(+)
AND pha.po_header_id(+)       = pda.po_header_id
  -- and pp.FULL_NAME = 'JIRAPUN, Miss. SHINAWATRA'    --Jirapun Shinawatra,  Nattaputch Wongreanthong ,Mr. Tim Jacobus Lambertus
AND pha.vendor_id = '101334'
AND pha.vendor_id = apv.vendor_id(+)
ORDER BY prh.segment1