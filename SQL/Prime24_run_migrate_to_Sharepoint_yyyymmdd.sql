SELECT TO_CHAR(begintransdatetime,'yyyymmdd') AS transaction_date,
  COUNT(*)                                    AS TRAN,
  './script_resend_test.sh ' || TO_CHAR(begintransdatetime,'yyyymmdd') || ' ' || COUNT(*) code
FROM EREGISOWN.application_prepaid
WHERE activatestatus IN ('40')
AND customerid       IN
  (SELECT customerid FROM EREGISOWN.customerattachment WHERE type = '40'
  )
  --and to_char(begintransdatetime,'yyyymmdd') = '20150907'
GROUP BY TO_CHAR(begintransdatetime,'yyyymmdd')
ORDER BY TO_CHAR(begintransdatetime,'yyyymmdd')
