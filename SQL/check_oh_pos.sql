SELECT a.ou_code,
  a.subinv_code,
  a.locator_code,
  c.locator_type,
  a.item_code,
  b.cat_code,
  SUM (a.erp_onhand) erp_onhand,
  SUM (a.erp_onhand + a.tran_qty) onhand_qty,
  SUM (a.erp_onhand + a.tran_qty - a.req_qty - a.reserve_qty) available_qty,
  sum(a.tran_qty) tran_qty,
  sum(a.req_qty) req_qty,
  SUM(a.reserve_qty) reserve_qty,
  MAX (a.oa_item_cost) oa_item_cost,
  b.control_by
FROM
  (SELECT ou_code,
    subinv_code,
    locator_code,
    SUBSTR (item_code, 1, 20) item_code,
    oa_item_cost,
    NVL (erp_onhand, 0) erp_onhand,
    0 tran_qty,
    0 req_qty,
    0 reserve_qty
  FROM in_item_onhand_v
  WHERE 0         =0
  AND ou_code     = 'TAC'
  AND item_code   = 'N980106053I'
  AND subinv_code = '02507'
  /*This subquery  will return erp onhand  in  item level */
  UNION ALL
  SELECT ou_code,
    subinv_code,
    locator_code,
    item_code,
    0 oa_item_cost,
    0 erp_onhand,
    NVL (tran_qty, 0),
    0 req_qty,
    0 reserve_qty
  FROM in_item_tran_v
  WHERE 0         =0
  AND ou_code     = 'TAC'
  AND item_code   = 'N980106053I'
  AND subinv_code = '02507'
  UNION ALL
  SELECT ou_code,
    subinv_code,
    locator_code,
    item_code,
    0 oa_item_cost,
    0 erp_onhand,
    0 tran_qty,
    NVL (req_qty, 0) req_qty,
    0 reserve_qty
  FROM in_item_req_v
  WHERE 0         =0
  AND ou_code     = 'TAC'
  AND item_code   = 'N980106053I'
  AND subinv_code = '02507'
  UNION ALL
  SELECT ou_code,
    subinv_code,
    locator_code,
    item_code,
    0 oa_item_cost,
    0 erp_onhand,
    0 tran_qty,
    0 req_qty,
    NVL(reserve_qty, 0) reserve_qty
  FROM in_item_reserve_v
  WHERE 0         =0
  AND ou_code     = 'TAC'
  AND item_code   = 'N980106053I'
  AND subinv_code = '02507'
  ) a,
  in_item b,
  pb_locator c
WHERE a.ou_code    = b.ou_code
AND a.item_code    = b.item_code
AND c.ou_code      = a.ou_code
AND c.subinv_code  = a.subinv_code
AND c.locator_code = a.locator_code
GROUP BY a.ou_code,
  a.subinv_code,
  a.locator_code,
  c.locator_type,
  a.item_code,
  b.cat_code,
  b.control_by ;
SELECT 'REQ' RESERVE_TYPE,
  A.REQ_NO,
  b.ou_code,
  a.send_subinv_code subinv_code,
  b.locator_code,
  b.item_code,
  b.qty req_qty
FROM in_req_head a,
  in_req_dtl c,
  in_req_sn_lot b
WHERE b.req_type  IN ('I', 'C')
AND a.req_status   = 'S'
AND a.int_req_type = 'S'
  /*sender accept STATUS*/
AND a.issue_no      IS NULL
AND b.ou_code        = c.ou_code
AND b.subinv_code    = c.subinv_code
AND b.req_no         = c.req_no
AND b.req_type       = c.req_type
AND b.item_code      = c.item_code
AND b.ou_code        = a.ou_code
AND b.subinv_code    = a.subinv_code
AND b.req_no         = a.req_no
AND b.req_type       = a.req_type
AND b.item_code      = 'PRCO0000131'
AND send_subinv_code = 'N02638'
UNION ALL
SELECT 'CLM' RESERVE_TYPE,
  ou_code,
  subinv_code,
  clm_locator_code locator_code ,
  clm_item_code item_code,
  COUNT(*) qty
FROM in_sp_clm
WHERE clm_status = 'P'
GROUP BY 'CLM' ,
  ou_code,
  subinv_code,
  clm_locator_code ,
  clm_item_code ;
SELECT *
FROM IN_REQ_HEAD,
  in_req_dtl
WHERE 0                =0
AND IN_REQ_HEAD.REQ_NO = in_req_dtl.REQ_NO
  --and IN_REQ_HEAD.REQ_NO like 'I0N031%18000%'
AND IN_REQ_HEAD.INT_REQ_TYPE = 'S'
AND IN_REQ_HEAD.REQ_STATUS  <> 'C'
  --and IN_REQ_HEAD.REQ_STATUS = 'R'
  --and in_req_dtl.ISSUE_QTY = 0
  and in_req_head.subinv_code = '02507'
AND item_code = 'N980106053I' ;
SELECT COUNT(*),
  item_code,
  CREATE_BY,
  REQ_NO
FROM in_req_sn_lot
WHERE item_code       IN ('PRCO0000501','PRCO0001041')
AND TRUNC(CREATE_DATE) = TRUNC(sysdate)
GROUP BY item_code,
  CREATE_BY,
  REQ_NO;
SELECT *
FROM in_item_tran_v
WHERE 0         =0
AND ou_code     = 'DTN'
AND item_code   = 'PRCO0000111'
AND subinv_code = 'N02762';


SELECT d.ou_code,
    d.subinv_code,
    d.locator_code,
    d.item_code,
    c.tran_type_code,
    DECODE (c.TYPE, 1, d.qty, 3, d.qty, 2, -d.qty, 4, -d.qty, 0) qty,
    a.doc_no
  FROM in_trans_type c,
    in_trans_head a,
    in_trans_dtl d
  WHERE (d.tf_c_o IS NULL
  OR (d.tf_c_o    IS NOT NULL
  AND d.tf_id     IN
    (SELECT mtl.transaction_interface_id FROM mtl_transactions_interface mtl
    )))
    /*(d.tf_c_o IS NULL or (d.tf_c_o is not null and exists (select 'x'
    from  mtl_transactions_interface mtl
    where mtl.transaction_interface_id = d.tf_id))) */
    --and     mtl.transaction_source_name = a.doc_no
    --and     mtl.transaction_type_id = c.oa_trans_type
    --and     mtl.item_segment1 = d.item_code
    --and     mtl.source_code = 'POS')))
  AND d.tran_type_code = c.tran_type_code
  AND DECODE (c.TYPE, 2, TO_DATE ('01/01/1001', 'dd/mm/yyyy'), 4, TO_DATE ('01/01/1001', 'dd/mm/yyyy'), a.aprv_date ) IS NOT NULL
    /* c.type = 2,4 is Inventory trans out witch this one effect a available_qty immediatly */
  AND NVL (a.CANCEL, 'N') = 'N'
  AND d.ou_code           = a.ou_code
  AND d.subinv_code       = a.subinv_code
  AND d.tran_type_code    = a.tran_type_code
  AND d.doc_no            = a.doc_no
  AND a.ou_code     = 'DTN'
AND d.item_code   = 'PRCO0000111'
AND a.subinv_code = 'N02762';


select * from  in_trans_head a,
    in_trans_dtl d where 0=0  AND d.ou_code           = a.ou_code
  AND d.subinv_code       = a.subinv_code
  AND d.tran_type_code    = a.tran_type_code
  AND d.doc_no            = a.doc_no
  AND a.ou_code     = 'DTN'
AND d.item_code   = 'PRCO0000131'
--AND a.subinv_code = 'N02762'
AND d.tran_type_code = 32
--and trunc(A.DOC_DATE) >= to_date('01/07/2018','dd/mm/yyyy')
and to_locator = 'N02638'
;

select * from IN_TRANS_SN_LOT where 0=0 
and DOC_NO = '42N027621800553'
and SERIAL = '01TI6605180645660691';