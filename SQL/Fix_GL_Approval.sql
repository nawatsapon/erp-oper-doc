------------------- Backup table -------------------------
Select * FROM gl_bc_packets gbp where gbp.je_batch_id = 4472355;

select * FROM gl_bc_packets gbp
    WHERE je_batch_id IN (SELECT je_batch_id
                         FROM gl_je_batches
                         WHERE name like 'ORN09_19 for Reclassify IR netting'   --X_Batch_Name
                         AND set_of_books_id  = 1001    --X_Set_Of_books_id
                         AND budgetary_control_status = 'I'
                         )
    AND gbp.set_of_books_id = 1001    --X_Set_Of_books_id
    AND status_code      IN ('P', 'C');   

------------------- Step 1 ---------------------------
DELETE FROM gl_bc_packets gbp
    WHERE je_batch_id IN (SELECT je_batch_id
                         FROM gl_je_batches
                         WHERE name = 'ORN09_19 for Reclassify IR netting'   --X_Batch_Name
                         AND set_of_books_id  = 1001    --X_Set_Of_books_id
                         AND budgetary_control_status = 'I'
                         )
    AND gbp.set_of_books_id = 1001    --X_Set_Of_books_id
    AND status_code      IN ('P', 'C');   

------------------- Step 2 ---------------------------
UPDATE gl_je_batches
    SET budgetary_control_status = 'R'
    WHERE je_batch_id  IN (SELECT je_batch_id
                          FROM gl_je_batches
                          WHERE name = 'ORN09_19 for Reclassify IR netting'   --X_Batch_Name
                          AND set_of_books_id  = 1001    --X_Set_Of_books_id
                          AND budgetary_control_status = 'I'
                          )
    AND budgetary_control_status = 'I';
    
    
    select * from gl_je_batches WHERE je_batch_id  IN (SELECT je_batch_id
                          FROM gl_je_batches
                          WHERE name like 'ORN09%'   --X_Batch_Name
                          AND set_of_books_id  = 1001    --X_Set_Of_books_id
                          AND budgetary_control_status = 'I'
                          )
    AND budgetary_control_status = 'I';
    

