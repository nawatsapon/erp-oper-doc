SELECT l.subinv_code,
  l.serial,
  sn.attribute1 phone_num,
  p.std_price
FROM in_trans_sn_lot l ,
  in_trans_head h ,
  sa_price_item p ,
  in_item i ,
  mtl_serial_numbers sn
WHERE l.ou_code      = 'DTN'
AND l.subinv_code    = 'N02638'
--AND l.tran_type_code = 34
AND l.item_code     IN
  (SELECT            *
  FROM the
    (SELECT CAST(to_str_comma_delimited('PRCO0000131,PRCO0000411') AS type_str_comma_delimited)
    FROM dual
    )
  )
AND h.ou_code         = l.ou_code
AND h.subinv_code     = l.subinv_code
AND h.tran_type_code  = l.tran_type_code
AND h.doc_no          = l.doc_no
AND p.item_code       = l.item_code
AND p.ou_code         = l.ou_code
AND l.ou_code         = i.ou_code
AND l.item_code       = i.item_code
AND i.OA_ITEM         = sn.inventory_item_id
AND l.serial          = sn.serial_number
AND sn.current_status = 3
--AND h.doc_date      >= to_date('13/01/2016','dd/mm/yyyy')
--AND h.doc_date       < to_date('14/01/2016','dd/mm/yyyy') +1
AND h.doc_date      >= (TRUNC(sysdate)-3)
AND h.doc_date      <= TRUNC(sysdate)
AND p.pricelist_code = 'CASH'
AND p.start_date    <= sysdate
AND (p.end_date     IS NULL
OR p.end_date       >= sysdate)
