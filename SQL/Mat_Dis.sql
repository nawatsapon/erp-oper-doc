--select * from  CST_INV_DISTRIBUTION_V where
SELECT  CID.ORGANIZATION_CODE
,       HRO.NAME
,       MMT.TRANSACTION_ID
,       MSI.SEGMENT1
,       MSI.DESCRIPTION
,       MTY.TRANSACTION_TYPE_NAME
,       TRUNC(MMT.TRANSACTION_DATE) AS TRANSACTION_DATE
,       MMT.TRANSACTION_QUANTITY
,       MMT.TRANSACTION_UOM
,       MMT.TRANSACTION_SOURCE_NAME
,       CID.UNIT_COST
,       NVL(MMT.ACTUAL_COST,0) AS ACTUAL_COST
,       GCC.CONCATENATED_SEGMENTS   AS ACCOUNT_CODE_COMBINATION
,       CID.LINE_TYPE_NAME
,       CID.BASE_TRANSACTION_VALUE
,       CID.Transaction_id
FROM MTL_MATERIAL_TRANSACTIONS      MMT
,    MTL_SYSTEM_ITEMS_B             MSI
,    HR_ALL_ORGANIZATION_UNITS      HRO
,    MTL_TRANSACTION_TYPES          MTY
,    CST_INV_DISTRIBUTION_V         CID
,    GL_CODE_COMBINATIONS_KFV       GCC
WHERE MMT.TRANSACTION_TYPE_ID IN (32, 42, 100001, 100002)
AND   MMT.INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
AND   MMT.ORGANIZATION_ID = MSI.ORGANIZATION_ID
AND   MMT.ORGANIZATION_ID = HRO.ORGANIZATION_ID
AND   MMT.TRANSACTION_TYPE_ID = MTY.TRANSACTION_TYPE_ID
AND   TRUNC(MMT.TRANSACTION_DATE) BETWEEN :FROM_DATE AND :TO_DATE
AND   MMT.TRANSACTION_ID =  CID.TRANSACTION_ID
AND   MMT.ORGANIZATION_ID = CID.ORGANIZATION_ID
AND   CID.REFERENCE_ACCOUNT = GCC.CODE_COMBINATION_ID
--AND   CID.Transaction_id = 11932
ORDER BY MMT.TRANSACTION_ID, BASE_TRANSACTION_VALUE DESC

