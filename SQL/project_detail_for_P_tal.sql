SELECT
    *
FROM
    pa_projects_all paj,
    pa_project_players ppp,
    per_all_people_f peo
WHERE
    0 = 0
    AND paj.project_id = ppp.project_id
    AND ppp.person_id = peo.person_id (+);

-----------------------------------------------------

SELECT
    proj.*,
    pat.attribute1   rc_code,
    rcd.description rc_desc,
    bud.burdened_cost
FROM
    (
        SELECT
            *
        FROM
            (
                SELECT
                    ppa.project_id,
                    ppa.segment1              project_code,
                    ppa.project_type,
                    pps.project_status_name   status,
                    ppa.name,
                    haou.name                 hr_org,
                    papf.full_name,
                    pprtt.meaning             role1
   -- , pat.attribute1 
                FROM
                    pa.pa_project_parties papp,
                    pa.pa_projects_all ppa,
                    pa.pa_project_role_types_tl pprtt,
                    pa.pa_project_role_types_b pprtb,
                    pa.pa_project_statuses pps,
                    hr.per_all_people_f papf,
                    hr.per_all_assignments_f paaf,
                    hr.hr_locations_all hla,
                    hr.per_phones pp,
                    hr.hr_all_organization_units haou
    -- , pa_tasks pat
                WHERE
                    ppa.project_id = papp.project_id
--   AND ppa.project_id = pat.project_id
                    AND papp.resource_source_id = papf.person_id
                    AND pprtt.project_role_id = pprtb.project_role_id
                    AND pprtt.project_role_id = papp.project_role_id
                    AND ppa.project_status_code = pps.project_status_code
                    AND papf.person_id = paaf.person_id
                    AND paaf.location_id = hla.location_id (+)
                    AND papf.person_id = pp.parent_id (+)
                    AND ppa.carrying_out_organization_id = haou.organization_id
                    AND SYSDATE BETWEEN papf.effective_start_date AND papf.effective_end_date
                    AND SYSDATE BETWEEN paaf.effective_start_date AND paaf.effective_end_date
                    AND SYSDATE BETWEEN papp.start_date_active AND nvl(papp.end_date_active,SYSDATE)
                    AND nvl(pp.date_to,SYSDATE + 1) > SYSDATE
                    AND nvl(pp.phone_type,'W1') = 'W1'
                    --AND ppa.segment1 =:projectnum
            ) PIVOT (
                LISTAGG ( full_name,',' ) WITHIN GROUP (
                    ORDER BY
                        full_name
                )
            full_name
                FOR role1
                IN ( 'Project Manager - 1' pm1,'Project Manager - 2' pm2,'Project Manager - 3' pm3 )
            )
    ) proj,
    pa_tasks pat,
    (
        SELECT
            flex_value,
            description
        FROM
            fnd_flex_values_vl
        WHERE
            ( ( '' IS NULL )
              OR ( structured_hierarchy_level IN (
                SELECT
                    hierarchy_id
                FROM
                    fnd_flex_hierarchies_vl h
                WHERE
                    h.flex_value_set_id = 1009748
                    AND h.hierarchy_name LIKE ''
            ) ) )
     --AND ( flex_value = '1659' )
            AND ( flex_value_set_id = 1009748 )
    ) rcd
    ,(SELECT
     t2.project_id,
     t2.burdened_cost,
     t2.version_number
 FROM
     (
         SELECT
             MAX(version_number) AS version_number,
             project_id
         FROM
             pa_budget_versions
         GROUP BY
             project_id
     ) t1,
     (
         SELECT
             *
         FROM
             pa_budget_versions
     ) t2
 WHERE
     0 = 0
     AND t1.version_number = t2.version_number
     AND t1.project_id = t2.project_id
     and t2.budget_status_code = 'B'
     ) bud
WHERE 0=0
and    proj.project_id = pat.project_id(+)
and  pat.attribute1 = rcd.flex_value(+)  
and proj.project_id = bud.project_id(+)
    order by 2
    ;
------------------------------------------------------------

SELECT
    *
FROM
    pa_tasks