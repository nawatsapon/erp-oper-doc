SELECT MIL.SEGMENT1,
  moq.SUBINVENTORY_CODE ,
  MTB.SEGMENT1
  --,MOQ.TRANSACTION_QUANTITY
  ,
  MOQ.ORGANIZATION_ID ,
  SUM(MOQ.TRANSACTION_QUANTITY) OH_QTY
FROM mtl_onhand_quantities moq,
  (SELECT DISTINCT ORGANIZATION_ID,
    SUBINVENTORY_CODE,
    INVENTORY_LOCATION_ID,
    segment1
  FROM MTL_ITEM_LOCATIONS
  ) MIL,
  INV.MTL_SYSTEM_ITEMS_B MTB
WHERE 0=0
  --moq.organization_id   = 298 -- N02
  --AND moq.inventory_item_id   = 479748 -- BSUS0640001
AND moq.SUBINVENTORY_CODE IN ('02708', 'N02708')
AND MOQ.LOCATOR_ID        = MIL.INVENTORY_LOCATION_ID
AND MOQ.ORGANIZATION_ID   = MIL.ORGANIZATION_ID
AND moq.SUBINVENTORY_CODE = MIL.SUBINVENTORY_CODE
AND MOQ.INVENTORY_ITEM_ID = MTB.INVENTORY_ITEM_ID
AND MOQ.ORGANIZATION_ID   = MTB.ORGANIZATION_ID
GROUP BY MIL.SEGMENT1,
  moq.SUBINVENTORY_CODE ,
  MTB.SEGMENT1 ,
  MOQ.ORGANIZATION_ID
ORDER BY 2,1,3;

-- Query Location from file using excel formula ="UNION select a.rowid, a.* from PB_SUBINV a where subinv_code like '%"&C2&"'"

--UNION select a.rowid, a.* from PB_SUBINV a where subinv_code like '%02708'
--UNION select a.rowid, a.* from PB_SUBINV a where subinv_code like '%35218'
--UNION select a.rowid, a.* from PB_SUBINV a where subinv_code like '%44205'
--UNION select a.rowid, a.* from PB_SUBINV a where subinv_code like '%38302'


DELETE FROM PB_USER_SUBINVEN where user_id not in (select user_id from TEMP_ASS_LIST ) and subinv_code like '%02223'; 


select * from PB_USER_SUBINVEN where user_id not in (select user_id from TEMP_ASS_LIST ) and subinv_code like '%02223';
