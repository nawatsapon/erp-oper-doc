SET SERVEROUTPUT ON -- Test VAT :: v_CTAX = 386762
DECLARE
  v_CTAX  NUMBER := RTRIM(LTRIM('&CUSTOMER_TRX_ID'));
  v_BFVAT NUMBER := RTRIM(LTRIM('&BEFORE_VAT'));
  v_VAT   NUMBER := RTRIM(LTRIM('&VAT'));
type tab_ctx_Line_id
IS
  TABLE OF VARCHAR2(20 BYTE);
  T_ctx_Line_id tab_ctx_Line_id;
type tab_line_typ
IS
  TABLE OF VARCHAR2(20 BYTE);
  T_tab_line_typ tab_line_typ;
BEGIN
  dbms_output.put_line('v_CTAX : '|| v_CTAX);
  SELECT CUSTOMER_TRX_LINE_ID,
    LINE_TYPE BULK COLLECT
  INTO T_ctx_Line_id,
    T_tab_line_typ
  FROM RA_CUSTOMER_TRX_LINES_ALL
  WHERE CUSTOMER_TRX_ID = v_CTAX;
  FOR cLine IN 1 .. T_ctx_Line_id.count
  LOOP
    IF T_tab_line_typ(cLine) = 'LINE' THEN
     /* -- Update Before TAX
      UPDATE RA_CUSTOMER_TRX_LINES_ALL
      SET UNIT_SELLING_PRICE   = v_BFVAT,  -- before vat (old = -11880)
        EXTENDED_AMOUNT        = -v_BFVAT, -- before vat
        REVENUE_AMOUNT         = -v_BFVAT  -- before vat
      WHERE CUSTOMER_TRX_ID    = v_CTAX
      
      -- Update distribution line
      update RA_CUST_TRX_LINE_GL_DIST_ALL
            set amount = -v_BFVAT -- Before vat 
            where cust_trx_line_gl_dist_id in (T_tab_line_typ(cLine));*/
            
      dbms_output.put_line('-----Update Line-----');     
      dbms_output.put_line('UNIT_SELLING_PRICET: ' || TO_CHAR(v_BFVAT));
      dbms_output.put_line('EXTENDED_AMOUNT : ' || TO_CHAR(-v_BFVAT));
      dbms_output.put_line('REVENUE_AMOUNT : ' || TO_CHAR(-v_BFVAT));
      dbms_output.put_line('CUSTOMER_TRX_ID: ' || TO_CHAR(v_CTAX));
      dbms_output.put_line('------------------------------------');
      dbms_output.put_line('cust_trx_line_gl_dist_id: ' || TO_CHAR(T_ctx_Line_id(cLine)));
      
    ELSIF T_tab_line_typ(cLine) = 'TAX' THEN
      -- Update TAX
    /*  UPDATE RA_CUSTOMER_TRX_LINES_ALL
      SET EXTENDED_AMOUNT      = -v_VAT,  -- vat (old = 0)
        TAXABLE_AMOUNT         = -v_BFVAT -- before vat
      WHERE CUSTOMER_TRX_ID    = v_CTAX
      
      -- Update distribution line
      update RA_CUST_TRX_LINE_GL_DIST_ALL
      set amount = -v_VAT -- vat 
      where cust_trx_line_gl_dist_id in (T_tab_line_typ(cLine));*/
      
      dbms_output.put_line('-----Update TAX-----'); 
      dbms_output.put_line('EXTENDED_AMOUNT : ' || TO_CHAR(-v_VAT));
      dbms_output.put_line('TAXABLE_AMOUNT : ' || TO_CHAR(-v_BFVAT));
      dbms_output.put_line('CUSTOMER_TRX_ID: ' || TO_CHAR(v_CTAX));
      dbms_output.put_line('------------------------------------');
      dbms_output.put_line('cust_trx_line_gl_dist_id: ' || TO_CHAR(T_ctx_Line_id(cLine)));
      
    ELSE
      dbms_output.put_line('OTHER CASE');
    END IF;
  END LOOP;
END;
/