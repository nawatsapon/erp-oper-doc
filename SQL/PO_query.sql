SELECT poh.type_lookup_code,
  poh.segment1 po_number,
  po_release.release_num,
  poh.PO_HEADER_ID,
  PO_HEADERS_SV3.GET_PO_STATUS(poh.PO_HEADER_ID) PO_STATUS,
  DECODE(POL.LINE_NUM, NULL, pol.closed_code, NVL(pol.closed_code, 'OPEN')) po_closure_code,
  POH.COMMENTS,
  pol.line_num,
  pll.shipment_num,
  pd.distribution_num,
  MCB.SEGMENT1 CATEGORY,
  POH.CREATION_DATE,
  PVD.SEGMENT1 VENDOR_CODE,
  PVD.VENDOR_NAME,
  PVS.VENDOR_SITE_CODE,
  (GL_CODE_COMBINATIONS.SEGMENT1 
  || '.' || GL_CODE_COMBINATIONS.SEGMENT11
  || '.' || GL_CODE_COMBINATIONS.SEGMENT10
  || '.' || GL_CODE_COMBINATIONS.SEGMENT12
  || '.' || GL_CODE_COMBINATIONS.SEGMENT13
  || '.' || GL_CODE_COMBINATIONS.SEGMENT14
  || '.' || GL_CODE_COMBINATIONS.SEGMENT18
  || '.' || GL_CODE_COMBINATIONS.SEGMENT19
  || '.' || GL_CODE_COMBINATIONS.SEGMENT21
  ) ACCOUNT_CODE,
  MCB.*
FROM PO_HEADERS_ALL POH,
  PO_RELEASES_ALL PO_RELEASE,
  PO_LINES_ALL POL,
  PO_LINE_LOCATIONS_ALL PLL,
  PO_DISTRIBUTIONS_ALL PD,
  MTL_CATEGORIES_B MCB,
  PO_VENDORS PVD,
  PO_VENDOR_SITES_ALL PVS,
  GL_CODE_COMBINATIONS
WHERE 1=1
  --AND trunc(POH.CREATION_DATE) >= to_date('01/01/2017','dd/mm/yyyy')
AND POH.closed_date          IS NULL
AND POH.TYPE_LOOKUP_CODE     <> 'QUOTATION'
AND POH.PO_HEADER_ID          = PO_RELEASE.PO_HEADER_ID(+)
AND POH.ORG_ID                = PO_RELEASE.ORG_ID(+)
AND POH.PO_HEADER_ID          = POL.PO_HEADER_ID
AND POL.PO_HEADER_ID          = PLL.PO_HEADER_ID
AND POL.PO_LINE_ID            = PLL.PO_LINE_ID
AND PLL.PO_HEADER_ID          = PD.PO_HEADER_ID
AND PLL.PO_LINE_ID            = PD.PO_LINE_ID
AND PLL.LINE_LOCATION_ID      = PD.LINE_LOCATION_ID
AND NVL(PLL.PO_RELEASE_ID, 0) = NVL(PO_RELEASE.PO_RELEASE_ID, 0)
AND POL.CATEGORY_ID           = MCB.CATEGORY_ID
AND POH.VENDOR_ID             = PVD.VENDOR_ID
AND POH.VENDOR_ID             = PVS.VENDOR_ID
AND POH.VENDOR_SITE_ID        = PVS.VENDOR_SITE_ID
and PD.CODE_COMBINATION_ID = GL_CODE_COMBINATIONS.CODE_COMBINATION_ID
--and GL_CODE_COMBINATIONS.segment11 in ('54202')
and trunc(POH.CREATION_DATE) >= to_date('01/01/2018','dd/mm/yyyy')
--ANd poh.segment1 = '991018002333'
and PO_HEADERS_SV3.GET_PO_STATUS(poh.PO_HEADER_ID) = 'Pre-Approved'
/*AND MCB.segment1             <> 'A'
AND (PLL.MATCH_OPTION         = 'R'
AND PLL.RECEIPT_REQUIRED_FLAG = 'N'
OR PLL.MATCH_OPTION           = 'P'
AND PLL.RECEIPT_REQUIRED_FLAG = 'Y')*/ ;