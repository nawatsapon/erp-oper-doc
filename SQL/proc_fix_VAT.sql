SET SERVEROUTPUT ON
DECLARE
  v_CUSTOMER_TRX_ID NUMBER(15,0) NOT NULL := RTRIM(LTRIM('&CUSTOMER_TRX_ID'));
  v_BEFORE_VAT      NUMBER NOT NULL       := RTRIM(LTRIM('&BEFORE_VAT'));
  v_VAT             NUMBER NOT NULL       := RTRIM(LTRIM('&VAT'));
  v_UNIT_PRICE      NUMBER NOT NULL       := RTRIM(LTRIM('&UNIT_PRICE'));
type tab_cust_trx_line_id
IS
  TABLE OF NUMBER(15,0);
  T_cust_trx_line_id tab_cust_trx_line_id;
type tab_line_type
IS
  TABLE OF VARCHAR2(20 BYTE);
  T_line_type tab_line_type;
type tab_cust_trx_line_gl_dist_id
IS
  TABLE OF NUMBER(15,0);
  T_cust_trx_line_gl_dist_id tab_cust_trx_line_gl_dist_id;
type tab_ACCOUNT_CLASS
IS
  TABLE OF VARCHAR2(20 BYTE);
  T_ACCOUNT_CLASS tab_ACCOUNT_CLASS;
type tab_UNIT_SELLING_PRICE
IS
  TABLE OF NUMBER;
  T_UNIT_SELLING_PRICE tab_UNIT_SELLING_PRICE;
type tab_EXTENDED_AMOUNT
IS
  TABLE OF NUMBER;
  T_EXTENDED_AMOUNT tab_EXTENDED_AMOUNT;
type tab_REVENUE_AMOUNT
IS
  TABLE OF NUMBER;
  T_REVENUE_AMOUNT tab_REVENUE_AMOUNT;
type tab_amount
IS
  TABLE OF NUMBER;
  T_amount tab_amount;
type tab_ACCTD_AMOUNT
IS
  TABLE OF NUMBER;
  T_ACCTD_AMOUNT tab_ACCTD_AMOUNT;
BEGIN
  dbms_output.put_line('v_CUSTOMER_TRX_ID : '|| TO_CHAR(v_CUSTOMER_TRX_ID));
  dbms_output.put_line('v_BEFORE_VAT : '|| TO_CHAR(v_BEFORE_VAT));
  dbms_output.put_line('v_VAT : '|| TO_CHAR(v_VAT));
  dbms_output.put_line('v_UNIT_PRICE : '|| TO_CHAR(v_UNIT_PRICE));
  
   /*------------------------- Update Line --------------------*/
  SELECT customer_trx_line_id,
    line_type,
    UNIT_SELLING_PRICE,
    EXTENDED_AMOUNT,
    REVENUE_AMOUNT
    BULK COLLECT
  INTO T_cust_trx_line_id,
    T_line_type,
    T_UNIT_SELLING_PRICE,
    T_EXTENDED_AMOUNT,
    T_REVENUE_AMOUNT
  FROM AR.RA_CUSTOMER_TRX_LINES_ALL a
  WHERE CUSTOMER_TRX_ID = v_CUSTOMER_TRX_ID order by line_type;
  dbms_output.put_line('Trying to Update Line :: '|| TO_CHAR(T_cust_trx_line_id.count));
  IF T_cust_trx_line_id.count = 2 THEN
    FOR cTrx IN 1 .. T_cust_trx_line_id.count
    LOOP      
      dbms_output.put_line('Data Before FIX - LINE TYPE :: '|| T_line_type(cTrx));
      dbms_output.put_line('UNIT SELLING PRICE :: '|| T_UNIT_SELLING_PRICE(cTrx));
      dbms_output.put_line('EXTENDED AMOUNT :: '|| T_EXTENDED_AMOUNT(cTrx));
      dbms_output.put_line('REVENUE AMOUNT :: '|| T_REVENUE_AMOUNT(cTrx));
      IF T_line_type(cTrx) = 'LINE' THEN
        dbms_output.put_line('LINE Update ::'|| T_cust_trx_line_id(cTrx));
        dbms_output.put_line('v_BEFORE_VAT : '|| TO_CHAR(v_BEFORE_VAT));
        update RA_CUSTOMER_TRX_LINES_ALL
        set UNIT_SELLING_PRICE = v_UNIT_PRICE -- unit before vat
        ,EXTENDED_AMOUNT = -v_BEFORE_VAT -- before vat
        ,REVENUE_AMOUNT = -v_BEFORE_VAT -- before vat
        --,LAST_UPDATED_BY = -79119 --not edit
        where CUSTOMER_TRX_ID = v_CUSTOMER_TRX_ID -- 2010000057
        and LINE_TYPE = 'LINE'
        and customer_trx_line_id = T_cust_trx_line_id(cTrx); 
        
      dbms_output.put_line('Data After FIX - LINE TYPE :: '|| T_line_type(cTrx));
      dbms_output.put_line('UNIT SELLING PRICE :: '|| v_UNIT_PRICE);
      dbms_output.put_line('EXTENDED AMOUNT :: '|| -v_BEFORE_VAT);
      dbms_output.put_line('REVENUE AMOUNT :: '|| -v_BEFORE_VAT);
        -- line id
      ELSIF T_line_type(cTrx) = 'TAX' THEN
        dbms_output.put_line('TAX Update :: ' || T_cust_trx_line_id(cTrx));
        dbms_output.put_line('v_VAT : '|| TO_CHAR(v_VAT));
         update RA_CUSTOMER_TRX_LINES_ALL
        set EXTENDED_AMOUNT = -v_VAT -- vat (old = 0)
        ,TAXABLE_AMOUNT = -v_BEFORE_VAT -- before vat
        --,LAST_UPDATED_BY = -79119 --not edit
        where CUSTOMER_TRX_ID = v_CUSTOMER_TRX_ID -- 2010000057
        and LINE_TYPE = 'TAX'
        and customer_trx_line_id = T_cust_trx_line_id(cTrx); -- tax id     
      dbms_output.put_line('Data After FIX - LINE TYPE :: '|| T_line_type(cTrx));
      dbms_output.put_line('EXTENDED AMOUNT :: '|| -v_BEFORE_VAT);
      dbms_output.put_line('TAXABLE_AMOUNT :: '|| -v_BEFORE_VAT);
      END IF;
     
    END LOOP;
  END IF;
  /*------------------------- Update Distribute --------------------*/
  SELECT RA_CUST_TRX_LINE_GL_DIST_ALL.CUST_TRX_LINE_GL_DIST_ID,
    RA_CUST_TRX_LINE_GL_DIST_ALL.ACCOUNT_CLASS ,
    RA_CUST_TRX_LINE_GL_DIST_ALL.AMOUNT,
    RA_CUST_TRX_LINE_GL_DIST_ALL.ACCTD_AMOUNT
    BULK COLLECT
  INTO T_cust_trx_line_gl_dist_id,
    T_ACCOUNT_CLASS,
    T_amount,
    T_ACCTD_AMOUNT
  FROM RA_CUST_TRX_LINE_GL_DIST_ALL
  WHERE CUSTOMER_TRX_ID      IN (v_CUSTOMER_TRX_ID)
  AND CUSTOMER_TRX_LINE_ID   IS NOT NULL;
  dbms_output.put_line('Trying to Update Distribute :: '|| TO_CHAR(T_cust_trx_line_id.count));
  IF T_cust_trx_line_gl_dist_id.count = 2 THEN
    FOR cGlTrx IN 1 .. T_cust_trx_line_gl_dist_id.count
    LOOP
      dbms_output.put_line('Data Before FIX - ACCOUNT CLASS :: '|| T_ACCOUNT_CLASS(cGlTrx));
      dbms_output.put_line('AMOUNT :: '|| T_amount(cGlTrx));
      dbms_output.put_line('ACCTD_AMOUNT :: '|| T_ACCTD_AMOUNT(cGlTrx));    
      IF T_ACCOUNT_CLASS(cGlTrx) = 'REV' THEN
        dbms_output.put_line('REV Update ::'|| T_cust_trx_line_gl_dist_id(cGlTrx));
        dbms_output.put_line('v_BEFORE_VAT : '|| TO_CHAR(v_BEFORE_VAT));
        update RA_CUST_TRX_LINE_GL_DIST_ALL
          set amount = -v_BEFORE_VAT,  -- Before vat 
          ACCTD_AMOUNT = -v_BEFORE_VAT -- Before vat 
          where cust_trx_line_gl_dist_id = T_cust_trx_line_gl_dist_id(cGlTrx); -- Revenue 
        -- line id
      ELSIF T_ACCOUNT_CLASS(cGlTrx) = 'TAX' THEN
        dbms_output.put_line('TAX Update :: ' || T_cust_trx_line_gl_dist_id(cGlTrx));
        dbms_output.put_line('v_VAT : '|| TO_CHAR(v_VAT));
         update RA_CUST_TRX_LINE_GL_DIST_ALL
            set amount = -v_VAT, -- vat 
            ACCTD_AMOUNT = -v_VAT -- vat 
            where cust_trx_line_gl_dist_id = T_cust_trx_line_gl_dist_id(cGlTrx); -- TAX
      END IF;
      dbms_output.put_line('Data After FIX - ACCOUNT CLASS :: '|| T_ACCOUNT_CLASS(cGlTrx));
      dbms_output.put_line('AMOUNT :: '|| -v_BEFORE_VAT);
      dbms_output.put_line('ACCTD_AMOUNT :: '|| -v_BEFORE_VAT);
    END LOOP;
  END IF;
END;
/