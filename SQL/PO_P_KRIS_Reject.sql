SELECT poh.requisition_header_id,
  HRU.ORGANIZATION_NAME,
  POH.SEGMENT1 PR_NUM,
  pahs.action_date SUBMIT_DATE,
  paha.action_date Reject_DATE,
  phdl.total_amt AMOUNT,
  PHDL.ACCOUNT_CODE,
  PHDL.item_description,
  PHDL.RC_CODE PO_COST_CENTER,
  RC_DESC_PO.DESCRIPTION PO_COST_CENTER_DESC,
  poh.attribute1 TAC_REQUESTER_COST_CENTER,
  RC_DESC_TAC.DESCRIPTION TAC_REQ_COST_CENTER_DESC,
  poh.attribute2 DIV,
  poh.attribute3 GRP,
  PAHC.CNT_APP,
  SPER.FULL_NAME Submit_by,
  APER.FULL_NAME Reject_By,
  poh.AUTHORIZATION_STATUS
FROM PO_REQUISITION_HEADERS_ALL poh,
  HRBG_ORGANIZATION_UNIT hru,
  (SELECT object_id,
    ph1.action_date,
    employee_id
  FROM PO_ACTION_HISTORY ph1
  WHERE 0                  =0
  AND ph1.object_type_code = 'REQUISITION'
  AND ph1.sequence_num     =
    (SELECT MAX(sequence_num)
    FROM PO_ACTION_HISTORY ph2
    WHERE ph1.object_id = ph2.object_id
    AND ph2.action_code = 'SUBMIT'
    )
  ) PAHS,
  (SELECT object_id,
    ph1.action_date,
    employee_id
  FROM PO_ACTION_HISTORY ph1
  WHERE 0                  =0
  AND ph1.object_type_code = 'REQUISITION'
  AND ph1.sequence_num     =
    (SELECT MAX(sequence_num)
    FROM PO_ACTION_HISTORY ph2
    WHERE ph1.object_id = ph2.object_id
    AND ph2.action_code = 'REJECT'
    )
  ) PAHA,
  (SELECT object_id,
    COUNT(*) CNT_APP
  FROM PO_ACTION_HISTORY ph1
  WHERE 0                  =0
  AND ph1.object_type_code = 'REQUISITION'
  AND ph1.action_code      = 'APPROVE'
  AND ph1.ACTION_DATE      >
    (SELECT phs.action_date
    FROM PO_ACTION_HISTORY phs
    WHERE 0                  =0
    AND phs.object_type_code = 'REQUISITION'
    AND phs.sequence_num     =
      (SELECT MAX(sequence_num)
      FROM PO_ACTION_HISTORY ph2
      WHERE phs.object_id = ph2.object_id
      AND ph2.action_code = 'SUBMIT'
      )
    AND ph1.object_id = phs.object_id
    )
  GROUP BY object_id
  )PAHC,
  (SELECT pol.requisition_header_id,
    SUM(pol.QUANTITY*pol.UNIT_PRICE*NVL(pol.RATE,1)) OVER (PARTITION BY pol.REQUISITION_HEADER_ID) AS TOTAL_AMT,
    pod.code_combination_id,
    gc.segment10 RC_CODE,
    gc.segment11 ACCOUNT_CODE,
    pol.item_description
  FROM po_requisition_lines_all pol,
    po_req_distributions_all pod,
    gl_code_combinations gc
  WHERE 0                     =0
  AND pol.requisition_line_id = pod.requisition_line_id
  AND pod.code_combination_id = gc.code_combination_id
  ) PHDL ,
  PER_ALL_PEOPLE_F SPER ,
  PER_ALL_PEOPLE_F APER ,
  (SELECT distinct FLEX_VALUE,
    DESCRIPTION
  FROM FND_FLEX_VALUES_VL
  WHERE ((''                     IS NULL)
  OR (structured_hierarchy_level IN
    (SELECT hierarchy_id
    FROM fnd_flex_hierarchies_vl h
    WHERE h.flex_value_set_id = 1009748
    AND h.hierarchy_name LIKE ''
    )))
  AND (FLEX_VALUE_SET_ID=1009748)
  ) RC_DESC_PO,
  (SELECT distinct FLEX_VALUE,
    DESCRIPTION
  FROM FND_FLEX_VALUES_VL
  WHERE ((''                     IS NULL)
  OR (structured_hierarchy_level IN
    (SELECT hierarchy_id
    FROM fnd_flex_hierarchies_vl h
    WHERE h.flex_value_set_id = 1009748
    AND h.hierarchy_name LIKE ''
    )))
  AND (FLEX_VALUE_SET_ID=1009748)
  ) RC_DESC_TAC
  /*,(SELECT ffdep.flex_value,
  FFDep.description dept,
  ffdiv.description div,
  ffg.description grp
  FROM FND_FLEX_VALUE_CHILDREN_V FFG,
  FND_FLEX_VALUE_CHILDREN_V FFDiv,
  FND_FLEX_VALUE_CHILDREN_V FFDep
  WHERE 0                     =0
  AND (ffg.FLEX_VALUE_SET_ID  =1009748)
  AND (ffg.PARENT_FLEX_VALUE  ='ZZZZ')
  AND FFDiv.parent_flex_value = FFG.flex_value
  AND FFDep.parent_flex_value = FFDiv.flex_value
  AND FFDiv.FLEX_VALUE_SET_ID  =1009748
  AND FFDep.FLEX_VALUE_SET_ID  =1009748
  ) ffv*/
WHERE 0                       =0
AND POH.ORG_ID                = HRU.ORGANIZATION_ID
AND POH.requisition_header_id = PAHS.object_id
AND POH.requisition_header_id = PAHA.object_id
AND POH.requisition_header_id = PAHC.object_id
AND poh.requisition_header_id = phdl.requisition_header_id
AND PAHS.employee_id          = SPER.PERSON_ID
AND PAHA.employee_id          = APER.PERSON_ID
AND PHDL.RC_CODE = RC_DESC_PO.FLEX_VALUE
AND poh.attribute1 = RC_DESC_TAC.FLEX_VALUE
AND TRUNC(POH.CREATION_DATE) >= to_date('01/01/2017','dd/mm/yyyy')
AND TRUNC(POH.CREATION_DATE) <= to_date('31/12/2017','dd/mm/yyyy')
AND POH.APPROVED_DATE        IS NULL
AND POH.AUTHORIZATION_STATUS <> 'SYSTEM_SAVED'
AND poh.type_lookup_code     <> 'INTERNAL'
AND poh.AUTHORIZATION_STATUS = 'REJECTED'
  --AND PHDL.RC_CODE = ffv.flex_value(+)
  --AND POH.SEGMENT1 = '8017001888'
ORDER BY 2,
  3;
SELECT distinct FLEX_VALUE,
  FLEX_VALUE_MEANING,
  DESCRIPTION
FROM FND_FLEX_VALUES_VL
WHERE ((''                     IS NULL)
OR (structured_hierarchy_level IN
  (SELECT hierarchy_id
  FROM fnd_flex_hierarchies_vl h
  WHERE h.flex_value_set_id = 1009748
  AND h.hierarchy_name LIKE ''
  )))
AND (FLEX_VALUE_SET_ID=1009748) ;


--select distinct action_code  from PO_ACTION_HISTORY where object_type_code = 'REQUISITION'
