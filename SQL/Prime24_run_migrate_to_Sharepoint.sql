SELECT TO_CHAR(begintransdatetime,'yyyy-mm-dd') AS transaction_date,
  COUNT(*)                                      AS TRAN
FROM EREGISOWN.application_prepaid
WHERE activatestatus IN ('40')
AND customerid       IN
  (SELECT customerid FROM EREGISOWN.customerattachment WHERE type = '40'
  )
GROUP BY TO_CHAR(begintransdatetime,'yyyy-mm-dd')
ORDER BY TO_CHAR(begintransdatetime,'yyyy-mm-dd')
