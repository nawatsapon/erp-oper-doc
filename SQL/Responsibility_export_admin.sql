SELECT DISTINCT application_name,
  r.RESPONSIBILITY_ID,
  r.responsibility_name,
  --security_group_name,
  pp.employee_number,
  pp.last_name,
  pp.first_name,
  pp.full_name,
  user_name,
  gcc.segment10 RC_Code,
  -- org.description RC_Name,
  ur.start_date RESP_START,
  --greatest(to_date(u.start_date), to_date(ur.start_date), to_date(r.start_date)) start_date,
  ur.end_date RESP_END,
  u.START_DATE USER_START,
  u.END_DATE USER_END
  --,DECODE(least(NVL(u.end_date, to_date('01/01/4712', 'DD/MM/YYYY')), NVL(ur.end_date, to_date('01/01/4712', 'DD/MM/YYYY')), NVL(r.end_date, to_date('01/01/4712', 'DD/MM/YYYY'))), to_date('01/01/4712', 'DD/MM/YYYY'), '', least(NVL(u.end_date, NVL(ur.end_date, r.end_date)), NVL(ur.end_date, NVL(u.end_date, r.end_date)), NVL(r.end_date, NVL(u.end_date, ur.end_date)))) end_date
FROM
  (SELECT *
  FROM fnd_user
  WHERE 0         =0
  AND (END_DATE  IS NULL
  OR END_DATE     > sysdate)
  AND START_DATE <= sysdate
    --AND FND_USER.USER_NAME IN ('KRITIMAS', 'BUSSABAM', 'NATAMONC', 'CHATRUDS', 'TANYONGW', 'THAWEEC', 'SULOMRIT', 'T889307', 'T891991', 'SUPISARH', 'CHUTHAMP', 'CHOTIROT', 'KRISSADA', 'JANTANS', 'T889840', 'T866551', 'DANUCHANAT', 'MONTINEE', 'HATTAYA', 'APIPHAN', 'T889300' )
  ) u,
  (SELECT *
  FROM fnd_user_resp_groups_all
  WHERE (END_DATE IS NULL
  OR END_DATE      > sysdate)
  AND START_DATE  <= sysdate
  ) ur,
  (SELECT APPLSYS.FND_RESPONSIBILITY.END_DATE,
    APPLSYS.FND_RESPONSIBILITY.APPLICATION_ID,
    APPLSYS.FND_RESPONSIBILITY.RESPONSIBILITY_ID,
    APPLSYS.FND_RESPONSIBILITY.START_DATE,
    APPLSYS.FND_RESPONSIBILITY.RESPONSIBILITY_KEY,
    APPLSYS.FND_RESPONSIBILITY.MENU_ID,
    APPLSYS.FND_RESPONSIBILITY.GROUP_APPLICATION_ID,
    APPLSYS.FND_RESPONSIBILITY_TL.RESPONSIBILITY_NAME,
    APPLSYS.FND_RESPONSIBILITY.VERSION
  FROM APPLSYS.FND_RESPONSIBILITY,
    APPLSYS.FND_RESPONSIBILITY_TL
  WHERE 0                                          =0
  AND APPLSYS.FND_RESPONSIBILITY.RESPONSIBILITY_ID = APPLSYS.FND_RESPONSIBILITY_TL.RESPONSIBILITY_ID
  AND APPLSYS.FND_RESPONSIBILITY.APPLICATION_ID    = APPLSYS.FND_RESPONSIBILITY_TL.APPLICATION_ID
  AND 0                                            = 0
  AND (APPLSYS.FND_RESPONSIBILITY.END_DATE        IS NULL
  OR APPLSYS.FND_RESPONSIBILITY.END_DATE           > SysDate)
  ) r,
  FND_APPLICATION_TL a,
  FND_SECURITY_GROUPS s,
  per_people_f pp,
  (SELECT *
  FROM per_assignments_f
  WHERE (EFFECTIVE_END_DATE IS NULL
  OR EFFECTIVE_END_DATE      > sysdate)
  AND EFFECTIVE_START_DATE  <= sysdate
  ) pa,
  gl_code_combinations gcc
  --,FND_FLEX_VALUES_VL org
WHERE 0                              =0
AND u.user_id                        = ur.user_id(+)
AND ur.responsibility_application_id = r.application_id
AND ur.responsibility_id             = r.responsibility_id
AND r.application_id                 = a.application_id
AND ur.security_group_id             = s.security_group_id(+)
  --AND ur.start_date                                <= sysdate
  --AND NVL(ur.end_date, sysdate + 1)                 > sysdate
  --AND u.start_date                <= sysdate
  --AND NVL(u.end_date, sysdate + 1) > sysdate
  --AND r.start_date                                 <= sysdate
  --AND NVL(r.end_date, sysdate + 1)                  > sysdate
AND r.version           IN ('4', 'W', 'M')
AND r.RESPONSIBILITY_ID IN
  (SELECT FND_RESPONSIBILITY_VL.RESPONSIBILITY_ID
  FROM FND_MENUS_VL,
    FND_MENU_ENTRIES_VL,
    FND_RESPONSIBILITY_VL
  WHERE 0                  =0
  AND FND_MENUS_VL.menu_id = FND_MENU_ENTRIES_VL.menu_id(+)
  AND FND_MENUS_VL.menu_id = FND_RESPONSIBILITY_VL.menu_id(+)
    --AND PROMPT LIKE '%Supplier%'
  --AND FND_MENU_ENTRIES_VL.DESCRIPTION LIKE 'Suppliers Entry'
  AND FND_RESPONSIBILITY_VL.RESPONSIBILITY_ID IN
    ( SELECT DISTINCT RESPONSIBILITY_ID
    FROM
      (SELECT id SUB_MENU_ID,
        parent_id MENU_ID,
        RESPONSIBILITY_ID,
        RESPONSIBILITY_NAME,
        RPAD('.', (level-1)*2, '.')
        || id AS tree,
        level tree_level,
        CONNECT_BY_ROOT id                               AS root_id,
        LTRIM(SYS_CONNECT_BY_PATH(promt_desc, '|'), '|') AS menu_path
        --,CONNECT_BY_ISLEAF                                    AS leaf
      FROM
        (SELECT FND_RESPONSIBILITY_VL.RESPONSIBILITY_ID,
          FND_MENUS_VL.MENU_ID id,
          FND_MENU_ENTRIES_VL.SUB_MENU_ID parent_id,
          FND_RESPONSIBILITY_VL.RESPONSIBILITY_NAME,
          FND_RESPONSIBILITY_VL.DESCRIPTION AS res_desc,
          FND_MENUS_VL.Menu_NAME,
          FND_MENUS_VL.USER_MENU_NAME,
          FND_MENUS_VL.DESCRIPTION AS Menu_desc,
          FND_MENU_ENTRIES_VL.PROMPT,
          FND_MENU_ENTRIES_VL.DESCRIPTION promt_desc
        FROM FND_MENUS_VL,
          FND_MENU_ENTRIES_VL,
          FND_RESPONSIBILITY_VL
        WHERE 0                  =0
        AND FND_MENUS_VL.menu_id = FND_MENU_ENTRIES_VL.menu_id(+)
        AND FND_MENUS_VL.menu_id = FND_RESPONSIBILITY_VL.menu_id(+)
        ) tab
        START WITH id = 67869 -- MENU "AR_CUSTOMERS_GUI" ID = 67722 , MENU "AP_SUPPLIERS_GUI12" = 68015
        --START WITH id is not null
        CONNECT BY parent_id = PRIOR id
      ORDER SIBLINGS BY id
      )
    WHERE 0=0
      and menu_path like '%Define%user%'
      
      -------------------- For exclude function ------------------------
    AND RESPONSIBILITY_ID NOT IN
      (SELECT FND_RESP_FUNCTIONS.REsponsibility_id
      FROM FND_RESP_FUNCTIONS
      WHERE 0       =0
      AND RULE_TYPE = 'F'
      AND ACTION_ID = 1348
      )
    )
  )
AND u.Employee_ID = pp.person_id(+)
  --AND TO_CHAR(pa.EFFECTIVE_END_DATE, 'DD-MON-RRRR') = '31-DEC-4712'
AND pp.person_id            = pa.person_id(+)
AND pa.default_code_comb_id = gcc.code_combination_id(+)
  --AND gcc.segment10           = org.flex_value(+)
  --AND org.flex_value_set_id   = 1009748
  --AND RESPONSIBILITY_NAME     = 'User Management'
  --and PP.EMPLOYEE_NUMBER in (
  --'95'
  --)
ORDER BY USER_NAME,
  1,2
