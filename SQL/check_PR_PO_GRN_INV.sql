SELECT pr_number ,
  po_number ,
  receipt_num GRN_number,
  invoice_num ,
  division_name ,
  department_name ,
  cost_center ,
  requester_name ,
  requester_div_name ,
  requester_dept_name ,
  requester_cost_center ,
  project_code ,
  project_name ,
  pr_header_description ,
  pr_approval_status ,
  pr_created_date ,
  pr_line_num ,
  pr_line_description ,
  activity_code ,
  activity_desc ,
  sub_activity_code ,
  sub_activity_desc ,
  application_code ,
  application_name ,
  pr_date ,
  account_number ,
  vendor_perspective_code ,
  vendor_perspective ,
  pr_rate ,
  pr_line_amount ,
  pr_amount ,
  category_code ,
  category_name ,
  category_group ,
  supplier_code ,
  supplier_name ,
  po_line_num ,
  po_description ,  
  po_line_description ,
  po_approval_status ,
  po_closure_code ,
  po_submit_date ,
  po_rate ,
  po_line_amount ,
  po_amount_per_pr ,
  receipt_amount ,
  receipt_date ,
  po_receipt_amount ,  
  invoice_date ,
  invoice_amount ,
  invoice_amount_per_pr ,
  invoice_amount_per_po_line ,
  invoice_rate ,
  NVL(pr_line_amount, 0)   - NVL(po_line_amount, 0) pr_line_outstanding_amount ,
  NVL(pr_amount, 0)        - NVL(po_amount_per_pr, 0) pr_outstanding_amount ,
  NVL(po_line_amount, 0)   - NVL(invoice_amount_per_po_line, 0) po_line_outstanding_amount ,
  NVL(po_amount_per_pr, 0) - NVL(invoice_amount_per_pr, 0) po_outstanding_amount ,
  DECODE(SUBSTR(account_number, 1, 2), '50', 'OPEX', '51', 'OPEX', '52', 'ADMIN', '53', 'ADMIN', '55', 'ADMIN', '56', 'ADMIN', 'CAPEX') project_type
FROM
  (SELECT a.division_name ,
    department_name ,
    a.cost_center ,
    a.requester_name ,
    a.requester_div_name ,
    a.requester_dept_name ,
    a.requester_cost_center ,
    a.project_code ,
    a.project_name ,
    a.pr_number ,
    a.pr_header_description ,
    a.pr_approval_status ,
    a.pr_created_date ,
    a.pr_line_num ,
    a.pr_line_description ,
    a.activity_code ,
    a.activity_desc ,
    a.sub_activity_code ,
    a.sub_activity_desc ,
    a.application_code ,
    a.application_name ,
    a.pr_date ,
    a.account_number ,
    a.vendor_perspective_code ,
    a.vendor_perspective ,
    a.pr_rate ,
    a.sum_pr_distribution_amount pr_line_amount ,
    tac_pr_to_po_pkg.get_req_total(a.pr_header_id, 'THB') pr_amount
    -- item category info
    ,
    a.category_code ,
    a.category_name ,
    a.category_group ,
    a.supplier_code ,
    a.supplier_name ,
    b.po_number ,
    b.po_description ,
    b.po_line_num ,
    b.po_line_description ,
    b.po_approval_status ,
    DECODE(b.po_line_num, NULL, b.po_closure_code, NVL(b.po_closure_code, 'OPEN')) po_closure_code ,
    b.po_submit_date ,
    b.po_rate ,
    b.sum_po_distribution_amount po_line_amount ,
    tac_pr_to_po_pkg.get_po_amount(a.pr_header_id) po_amount_per_pr ,
    c.receipt_num ,
    c.receipt_amount ,
    c.receipt_date ,
    tac_pr_to_po_pkg.get_po_receipt_amount(a.project_id, a.type_code, a.pr_line_id, b.po_header_id) po_receipt_amount ,
    d.invoice_num ,
    d.invoice_date ,
    d.invoice_amount ,
    tac_pr_to_po_pkg.get_invoice_amount_per_pr(a.pr_header_id) invoice_amount_per_pr ,
    tac_pr_to_po_pkg.get_invoice_amount(a.pr_header_id, a.pr_line_id, b.po_header_id, b.po_line_id) invoice_amount_per_po_line ,
    NVL(c.invoice_rate, 1) invoice_rate
  FROM
    (SELECT project_id ,
      project_code ,
      project_name ,
      type_code ,
      type_name ,
      pr_header_id ,
      pr_number ,
      pr_header_description ,
      DECODE(SUBSTR(ltrim(pr_header_description), 1, 1) ,'(' ,SUBSTR(ltrim(pr_header_description), 2, instr(ltrim(pr_header_description), ')', 1, 1) - 2) ,'NULL') pr_header_erp_project_code ,
      NVL(SUBSTR(ltrim(pr_header_description) ,instr(ltrim(pr_header_description), '_', 1, 1)                                                        + 1 ,instr(ltrim(pr_header_description), '_', 1, 2) - instr(ltrim(pr_header_description), '_', 1, 1) - 1) ,'NULL') pr_header_rfc_number ,
      pr_created_date ,
      pr_date ,
      pr_approval_status ,
      pr_line_id ,
      pr_line_num ,
      pr_line_description ,
      supplier_name ,
      supplier_code ,
      DECODE(SUBSTR(ltrim(pr_line_description), 1, 1) ,'(' ,SUBSTR(ltrim(pr_line_description), 2, instr(ltrim(pr_line_description), ')', 1, 1) - 2) ,'NULL') pr_line_erp_project_code ,
      NVL(SUBSTR(ltrim(pr_line_description) ,instr(ltrim(pr_line_description), '_', 1, 1)                                                      + 1 ,instr(ltrim(pr_line_description), '_', 1, 2) - instr(ltrim(pr_line_description), '_', 1, 1) - 1) ,'NULL') pr_line_rfc_number ,
      requester_id ,
      requester_name ,
      division_name ,
      department_name ,
      cost_center ,
      account_number
      --- add assignment for requester account
      ,
      requester_div_name ,
      requester_dept_name ,
      requester_cost_center ,
      activity_code ,
      activity_desc ,
      sub_activity_code ,
      sub_activity_desc ,
      application_code ,
      application_name ,
      pr_rate
      -- item category info
      ,
      category_code ,
      category_name ,
      category_group
      --vendor perspective
      ,
      vendor_perspective_code ,
      vendor_perspective ,
      sum_pr_distribution_amount
    FROM
      (SELECT -- PR Data
        prj.project_id ,
        prj.segment1 project_code ,
        prj.name project_name ,
        gcc.segment19 type_code ,
        tac_pr_to_po_pkg.get_acc_segment_name(prd.set_of_books_id, 'SEGMENT19', gcc.segment19) type_name ,
        prh.requisition_header_id pr_header_id ,
        prh.segment1 pr_number ,
        prh.description pr_header_description ,
        prh.creation_date pr_created_date ,
        prh.creation_date pr_date ,
        prh.authorization_status pr_approval_status ,
        prl.requisition_line_id pr_line_id ,
        prl.line_num pr_line_num ,
        prl.item_description pr_line_description ,
        prl.suggested_vendor_name supplier_name ,
        pv.segment1 supplier_code ,
        prl.to_person_id requester_id ,
        pap.title
        || ' '
        || pap.last_name
        || '  '
        || pap.first_name requester_name
        -- add pr distribution for division, department, coscenter, account
        ,
        tac_pr_to_po_pkg.get_division_name(gcc.segment10) division_name ,
        tac_pr_to_po_pkg.get_department_name(gcc.segment10) department_name ,
        gcc.segment10 cost_center ,
        gcc.segment11 account_number
        -- add assignment for requester account
        ,
        tac_pr_to_po_pkg.get_division_name(rgcc.segment10) requester_div_name ,
        tac_pr_to_po_pkg.get_department_name(rgcc.segment10) requester_dept_name ,
        rgcc.segment10 requester_cost_center ,
        prl.attribute12 activity_code ,
        act.activity_desc ,
        prl.attribute13 sub_activity_code ,
        sact.sub_activity_desc ,
        prl.attribute14 application_code ,
        app.application_name application_name
        -- pr line rate
        ,
        prl.rate pr_rate
        -- item category info
        ,
        tac_pr_to_po_pkg.get_category_info(prl.category_id, 'GROUP') category_group ,
        tac_pr_to_po_pkg.get_category_info(prl.category_id, 'CODE') category_code ,
        tac_pr_to_po_pkg.get_category_info(prl.category_id, 'NAME') category_name
        -- perspective
        ,
        gcc.segment19 vendor_perspective_code ,
        gl_flexfields_pkg.get_description_sql(gcc.chart_of_accounts_id, 9, gcc.segment19) vendor_perspective ,
        SUM((prd.req_line_quantity * NVL(prl.unit_price, 1))) sum_pr_distribution_amount
      FROM po_requisition_headers_all prh ,
        po_requisition_lines_all prl ,
        po_req_distributions_all prd ,
        pa_projects_all prj ,
        gl_code_combinations gcc ,
        per_all_people_f pap ,
        po_vendors pv
        --- add assignment for requester account
        ,
        per_all_assignments_f asg ,
        gl_code_combinations_kfv rgcc ,
        (SELECT fv.flex_value activity_code ,
          fv.description activity_desc
        FROM fnd_flex_values_vl fv ,
          fnd_flex_value_sets fvs
        WHERE fv.flex_value_set_id  = fvs.flex_value_set_id
        AND fvs.flex_value_set_name = 'TAC_PR_Activity'
        ) act ,
        (SELECT fv.parent_flex_value_low activity_code ,
          fv.flex_value sub_activity_code ,
          fv.description sub_activity_desc
        FROM fnd_flex_values_vl fv ,
          fnd_flex_value_sets fvs
        WHERE fv.flex_value_set_id  = fvs.flex_value_set_id
        AND fvs.flex_value_set_name = 'TAC_PR_Sub_Activity'
        ) sact ,
        (SELECT fv.flex_value application_code ,
          fv.description application_name
        FROM fnd_flex_values_vl fv ,
          fnd_flex_value_sets fvs
        WHERE fv.flex_value_set_id  = fvs.flex_value_set_id
        AND fvs.flex_value_set_name = 'TAC_PR_Application_Name'
        ) app
      WHERE prh.requisition_header_id = prl.requisition_header_id
      AND prl.requisition_line_id     = prd.requisition_line_id
      AND prd.project_id              = prj.project_id(+)
      AND prd.code_combination_id     = gcc.code_combination_id
      AND prl.to_person_id            = pap.person_id(+)
      AND prl.suggested_vendor_name   = pv.vendor_name(+)
      AND ((pap.person_id            IS NOT NULL
      AND sysdate BETWEEN NVL(pap.effective_start_date, sysdate) AND NVL(pap.effective_end_date, sysdate + 1))
      OR (pap.person_id IS NULL))
        --- add assignment for requester account
      AND pap.person_id = asg.person_id(+)
      AND sysdate BETWEEN NVL(asg.effective_start_date, sysdate) AND NVL(asg.effective_end_date, sysdate + 1)
      AND 'Y'                      = NVL(asg.primary_flag, 'Y')
      AND asg.default_code_comb_id = rgcc.code_combination_id(+)
      AND prl.attribute12          = act.activity_code(+)
      AND prl.attribute12          = sact.activity_code(+)
      AND prl.attribute13          = sact.sub_activity_code(+)
      AND prl.attribute14          = app.application_code(+)
        -- Paramters
      AND prh.segment1 IN ('1009019482', '1010003050', '1010008751', '1010017366', '1011004307', '1011018007', '1014016371', '1014019587', '10150007913', '10150007914', '10150007915', '10150007916', '10150018684', '10160003902' )
     -- AND prh.segment1 <= p_pr_number_to
      GROUP BY prj.project_id ,
        prj.segment1 ,
        prj.name ,
        prd.set_of_books_id ,
        gcc.segment19 ,
        prh.requisition_header_id ,
        prh.segment1 ,
        prh.description ,
        prh.creation_date ,
        prh.authorization_status ,
        prl.requisition_line_id ,
        prl.line_num ,
        prl.item_description ,
        prl.suggested_vendor_name ,
        pv.segment1 ,
        prl.to_person_id ,
        pap.title ,
        pap.last_name ,
        pap.first_name ,
        gcc.segment10 ,
        gcc.segment11 ,
        rgcc.segment10 ,
        prl.attribute12 ,
        act.activity_desc ,
        prl.attribute13 ,
        sact.sub_activity_desc ,
        prl.attribute14 ,
        app.application_name ,
        prl.rate
        -- item category info
        ,
        prl.category_id
        -- vendor perspective
        ,
        gcc.chart_of_accounts_id ,
        gcc.segment19
      HAVING SUM((prd.req_line_quantity * NVL(prl.unit_price, 1))) > 0
      )
    ) a ,
    (SELECT project_id ,
      project_code ,
      type_code ,
      pr_header_id ,
      pr_number ,
      pr_line_id ,
      po_header_id ,
      po_number ,
      po_description ,
      po_approval_status ,
      po_submit_date ,
      po_line_id ,
      po_line_num ,
      po_line_description ,
      po_closure_code ,
      MAX(po_distribution_type) po_distribution_type ,
      MAX(po_distribution_rate) po_rate ,
      SUM(cal_po_distribution_amount) sum_po_distribution_amount
    FROM
      (SELECT -- PR Data
        prj.project_id ,
        prj.segment1 project_code ,
        gcc.segment19 type_code ,
        tac_pr_to_po_pkg.get_acc_segment_name(prd.set_of_books_id, 'SEGMENT19', gcc.segment19) type_name ,
        prh.requisition_header_id pr_header_id ,
        prh.segment1 pr_number ,
        prh.description pr_header_description ,
        prh.creation_date pr_created_date ,
        prh.authorization_status pr_approval_status ,
        prl.requisition_line_id pr_line_id ,
        prl.line_num pr_line_num ,
        prl.quantity pr_line_quantity ,
        prl.unit_price pr_line_unit_price ,
        prl.quantity * prl.unit_price cal_pr_line_amount ,
        prd.distribution_id pr_distribution_id ,
        prd.distribution_num pr_distribution_num ,
        prd.req_line_quantity
        -- PO Data
        ,
        poh.po_header_id ,
        poh.segment1 po_number ,
        poh.comments po_description ,
        poh.authorization_status po_approval_status ,
        poh.submit_date po_submit_date ,
        pol.po_line_id ,
        pol.line_num po_line_num ,
        pol.item_description po_line_description ,
        pol.closed_code po_closure_code ,
        pol.quantity po_line_quantity ,
        pol.amount po_line_amount ,
        pol.unit_price po_line_unit_price ,
        poll.line_location_id ,
        poll.shipment_num ,
        poll.shipment_type ,
        poll.quantity po_dist_quantity ,
        pod.po_distribution_id ,
        pod.distribution_num po_distribution_num ,
        pod.distribution_type po_distribution_type ,
        pod.quantity_ordered ,
        pod.rate po_distribution_rate ,
        (pod.quantity_ordered * pol.unit_price * NVL(pod.rate, 1)) cal_po_distribution_amount
      FROM po_requisition_headers_all prh ,
        po_requisition_lines_all prl ,
        po_req_distributions_all prd ,
        pa_projects_all prj ,
        po_distributions_all pod ,
        po_line_locations_all poll ,
        po_lines_all pol ,
        po_headers_all poh ,
        gl_code_combinations gcc
      WHERE prh.requisition_header_id                              = prl.requisition_header_id
      AND prl.requisition_line_id                                  = prd.requisition_line_id
      AND prd.project_id                                           = prj.project_id(+)
      AND prd.code_combination_id                                  = gcc.code_combination_id
      AND prd.distribution_id                                      = pod.req_distribution_id(+)
      AND pod.line_location_id                                     = poll.line_location_id(+)
      AND poll.po_line_id                                          = pol.po_line_id(+)
      AND pol.po_header_id                                         = poh.po_header_id(+)
      AND NVL(poll.shipment_type, '#')                            != 'SCHEDULED'
      AND pod.quantity_ordered * pol.unit_price * NVL(pod.rate, 1) > 0
        -- Parameters
      AND prh.segment1 IN ('1009019482', '1010003050', '1010008751', '1010017366', '1011004307', '1011018007', '1014016371', '1014019587', '10150007913', '10150007914', '10150007915', '10150007916', '10150018684', '10160003902' )
      --AND prh.segment1 <= p_pr_number_to
      )
    GROUP BY project_id ,
      project_code ,
      type_code ,
      pr_header_id ,
      pr_number ,
      pr_line_id ,
      po_header_id ,
      po_number ,
      po_description ,
      po_approval_status ,
      po_submit_date ,
      po_line_id ,
      po_line_num ,
      po_line_description ,
      po_closure_code
    ) b ,
    (
    --Receving for Planned PO and Blanket PO
    SELECT prd.requisition_line_id pr_line_id ,
      pod.req_distribution_id ,
      pord.po_header_id ,
      pord.po_line_id ,
      pord.distribution_num ,
      pord.distribution_type ,
      pord.quantity_ordered ,
      pord.rate ,
      porl.price_override unit_price ,
      rsh.receipt_num ,
      TRUNC(rsh.creation_date, 'dd') receipt_date ,
      rt.transaction_id rcv_transaction_id ,
      rt.transaction_date ,
      rt.transaction_type ,
      DECODE(rt.transaction_type, 'RETURN TO RECEIVING', rt.quantity * (-1), rt.quantity) receipt_quantity ,
      DECODE(rt.transaction_type, 'RETURN TO RECEIVING', rt.quantity * (-1), rt.quantity) * porl.price_override * NVL(pord.rate, 1) receipt_amount ,
      tac_pr_to_po_pkg.get_invoice_rate(pord.po_distribution_id) invoice_rate
    FROM po_req_distributions_all prd ,
      po_distributions_all pod ,
      po_distributions_all pord ,
      rcv_transactions rt ,
      rcv_shipment_lines rsl ,
      rcv_shipment_headers rsh ,
      po_line_locations_all porl
    WHERE prd.distribution_id   = pod.req_distribution_id
    AND pod.distribution_type  IN ('PLANNED', 'BLANKET')
    AND pod.po_distribution_id  = DECODE(pord.distribution_type, 'BLANKET', pord.po_distribution_id, pord.source_distribution_id)
    AND pord.distribution_type IN ('SCHEDULED', 'BLANKET')
    AND pord.po_distribution_id = rt.po_distribution_id
    AND rt.shipment_line_id     = rsl.shipment_line_id
    AND rsl.shipment_header_id  = rsh.shipment_header_id
    AND pord.line_location_id   = porl.line_location_id
    AND rt.transaction_type    IN ('RECEIVE', 'RETURN TO RECEIVING')
    AND pord.po_release_id     IS NOT NULL
      -- Filter for PR number
    AND EXISTS
      (SELECT prl1.requisition_line_id
      FROM po_requisition_lines_all prl1 ,
        po_requisition_headers_all prh1
      WHERE prl1.requisition_header_id = prh1.requisition_header_id
        -- Exists link
      AND prl1.requisition_line_id = prd.requisition_line_id
        -- Parameters
      AND prh1.segment1 IN ('1009019482', '1010003050', '1010008751', '1010017366', '1011004307', '1011018007', '1014016371', '1014019587', '10150007913', '10150007914', '10150007915', '10150007916', '10150018684', '10160003902' )
      --AND prh1.segment1 <= p_pr_number_to
      )
    UNION ALL
    --Receving for Standard PO
    SELECT prd.requisition_line_id pr_line_id ,
      pord.req_distribution_id ,
      pord.po_header_id ,
      pord.po_line_id ,
      pord.distribution_num ,
      pord.distribution_type ,
      pord.quantity_ordered ,
      pord.rate ,
      porl.price_override unit_price ,
      rsh.receipt_num ,
      rsh.creation_date receipt_date ,
      rt.transaction_id ,
      rt.transaction_date ,
      rt.transaction_type ,
      DECODE(rt.transaction_type, 'RETURN TO RECEIVING', rt.quantity * (-1), rt.quantity) receipt_quantity ,
      DECODE(rt.transaction_type, 'RETURN TO RECEIVING', rt.quantity * (-1), rt.quantity) * porl.price_override * NVL(pord.rate, 1) receipt_amount ,
      tac_pr_to_po_pkg.get_invoice_rate(pord.po_distribution_id) invoice_rate
    FROM po_req_distributions_all prd ,
      po_distributions_all pord ,
      rcv_transactions rt ,
      rcv_shipment_lines rsl ,
      rcv_shipment_headers rsh ,
      po_line_locations_all porl
    WHERE prd.distribution_id   = pord.req_distribution_id
    AND pord.distribution_type  = 'STANDARD'
    AND pord.po_distribution_id = rt.po_distribution_id
    AND rt.shipment_line_id     = rsl.shipment_line_id
    AND rsl.shipment_header_id  = rsh.shipment_header_id
    AND pord.line_location_id   = porl.line_location_id
    AND rt.transaction_type    IN ('RECEIVE', 'RETURN TO RECEIVING')
      -- Filter for PR number
    AND EXISTS
      (SELECT prl2.requisition_line_id
      FROM po_requisition_lines_all prl2 ,
        po_requisition_headers_all prh2
      WHERE prl2.requisition_header_id = prh2.requisition_header_id
        -- Exists link
      AND prl2.requisition_line_id = prd.requisition_line_id
        -- Parameters
      AND prh2.segment1 IN ('1009019482', '1010003050', '1010008751', '1010017366', '1011004307', '1011018007', '1014016371', '1014019587', '10150007913', '10150007914', '10150007915', '10150007916', '10150018684', '10160003902' )
      --AND prh2.segment1 <= p_pr_number_to
      )
    ) c ,
    (
    -- Invoie match receipt
    SELECT rcv_inv.rcv_transaction_id ,
      rcv_inv.invoice_id ,
      ai.invoice_num ,
      ai.invoice_date ,
      SUM(aid.amount * NVL(aid.exchange_rate, 1)) invoice_amount
    FROM
      (SELECT a.rcv_transaction_id ,
        a.po_distribution_id ,
        a.invoice_id
      FROM ap_invoice_distributions_all a
      WHERE a.rcv_transaction_id IS NOT NULL
      AND a.po_distribution_id   IS NOT NULL
      ) rcv_inv ,
      ap_invoices_all ai ,
      ap_invoice_distributions_all aid
    WHERE rcv_inv.invoice_id           = ai.invoice_id
    AND ai.invoice_id                  = aid.invoice_id
    AND aid.line_type_lookup_code NOT IN ('AWT', 'TAX')
    GROUP BY rcv_inv.rcv_transaction_id ,
      rcv_inv.invoice_id ,
      ai.invoice_num ,
      ai.invoice_date
    ) d
  WHERE a.pr_header_id     = b.pr_header_id(+)
  AND a.pr_line_id         = b.pr_line_id(+)
  AND b.pr_line_id         = c.pr_line_id(+)
  AND b.po_header_id       = c.po_header_id(+)
  AND b.po_line_id         = c.po_line_id(+)
  AND c.rcv_transaction_id = d.rcv_transaction_id(+)
  )
ORDER BY division_name ,
  department_name ,
  cost_center ,
  pr_number ,
  pr_line_num ,
  po_number ,
  po_line_num;