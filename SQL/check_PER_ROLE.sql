SELECT *
FROM WF_LOCAL_ROLES
WHERE 0=0
--and TRUNC(start_date) >= to_date('01/03/2017','dd/mm/yyyy')
--and TRUNC(start_date) <= to_date('31/03/2017','dd/mm/yyyy')
and display_name like 'PER_ROLE%'
order by start_date

;

select ppei.pei_information3, pr.role_name, ppei.person_id, count(ppei.person_id) 
from per_people_extra_info ppei,
pqh_roles pr
where ppei.pei_information3 = pr.role_id
and ppei.information_type = 'PQH_ROLE_USERS' 
group by ppei.pei_information3, pr.role_name, ppei.person_id
having count(ppei.person_id) > 1;


SELECT 'PER_ROLE:' || PER.PERSON_ID wf_name, 'PER_ROLE' wf_role, PER.PERSON_ID, per.employee_number, count(*)
  from PER_ALL_PEOPLE_F PER
 where trunc(SYSDATE) between PER.EFFECTIVE_START_DATE and
       PER.EFFECTIVE_END_DATE
 group by 'PER_ROLE:' || PER.PERSON_ID, 'PER_ROLE', PER.PERSON_ID, per.employee_number
having count(*) > 1;


select person_id,
       effective_start_date,
       effective_end_date,
       full_name,
       date_of_birth,
       employee_number,
       national_identifier,
       creation_date,
       last_update_date
  from HR.per_all_people_f
 where person_id = 124098
 order by effective_start_date