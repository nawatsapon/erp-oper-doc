SELECT COUNT(*) FROM XX_TMP_SERIAL;
DELETE FROM XX_TMP_SERIAL ;
SELECT tmps.serial,
  mmt.*
FROM mtl_unit_transactions mut,
  XX_TMP_SERIAL tmps,
  (SELECT TRANSACTION_ID,
    INVENTORY_ITEM_ID,
    ORGANIZATION_ID,
    TRX_SOURCE_LINE_ID,
    TRANSACTION_DATE
  FROM mtl_material_transactions
  WHERE SOURCE_CODE = 'ORDER ENTRY'
  ) mmt
WHERE 0                   =0
AND tmps.serial           = mut.serial_number(+)
AND mmt.INVENTORY_ITEM_ID = mut.INVENTORY_ITEM_ID
AND mmt.TRANSACTION_ID    = mut.TRANSACTION_ID;
--------------------------------------------------------------------
SELECT DISTINCT tmps.serial,
  mtb.INVENTORY_ITEM_CODE,
  mtb.description ITEM_DESC,
  msn.current_subinventory_code,
  MIL.segment1 curr_Locator,
  SUB.DESCRIPTION,
  msn.status_id,
  slp.meaning Serial_status,
  mmt.TRANSACTION_ID,
  mmt.TRANSACTION_DATE,
  mmt.INVENTORY_ITEM_ID,
  mmt.transaction_source_name,
  mtt.Transaction_Type_Name,
  mtt.DESCRIPTION
FROM XX_TMP_SERIAL tmps,
  mtl_serial_numbers msn,
  mtl_material_transactions mmt,
  MTL_TRX_TYPES_VIEW mtt,
  (SELECT DISTINCT INVENTORY_ITEM_ID,
    segment1 INVENTORY_ITEM_CODE,
    description,
    ORGANIZATION_ID
  FROM Mtl_System_Items_B
  ) mtb,
  (SELECT lookup_code,
    meaning
  FROM mfg_lookups
  WHERE lookup_type = 'SERIAL_NUM_STATUS'
  ) slp,
  MTL_SECONDARY_INVENTORIES SUB,
  (SELECT DISTINCT ORGANIZATION_ID,
    SUBINVENTORY_CODE,
    INVENTORY_LOCATION_ID,
    segment1
  FROM MTL_ITEM_LOCATIONS
  ) MIL
WHERE 0=0
  --and SUBSTR(msn.serial_number, 5, 16) = tmps.serial
AND msn.serial_number              = tmps.serial
AND msn.last_transaction_id        = mmt.transaction_id(+)
AND mmt.Transaction_Type_Id        = mtt.TRANSACTION_TYPE_ID(+)
AND mmt.TRANSACTION_SOURCE_TYPE_ID = mtt.TRANSACTION_SOURCE_TYPE_ID(+)
AND mmt.Transaction_Action_Id      = mtt.TRANSACTION_ACTION_ID(+)
AND mmt.SUBINVENTORY_CODE          = SUB.SECONDARY_INVENTORY_NAME
AND mmt.ORGANIZATION_ID            = SUB.ORGANIZATION_ID
AND mmt.Inventory_Item_Id          = mtb.INVENTORY_ITEM_ID
AND mmt.ORGANIZATION_ID            = mtb.ORGANIZATION_ID
AND msn.status_id                  = slp.lookup_code
AND msn.CURRENT_LOCATOR_ID         = MIL.INVENTORY_LOCATION_ID
AND msn.CURRENT_ORGANIZATION_ID    = MIL.ORGANIZATION_ID
AND msn.CURRENT_SUBINVENTORY_CODE  = MIL.SUBINVENTORY_CODE
ORDER BY 1 ;
SELECT GEN_OBJECT_ID,LAST_TRANSACTION_ID FROM mtl_serial_numbers;
SELECT msn.serial_number,
  SUBSTR(stmp.serial, 3, 16),
  msn.current_subinventory_code,
  msn.current_organization_id,
  msn.current_status,
  msn.inventory_item_id
FROM mtl_serial_numbers msn ,
  XX_TEMP_SERIAL stmp
WHERE 0                              =0
AND SUBSTR(msn.serial_number, 5, 16) = stmp.serial ;
SELECT *
FROM MTL_SERIAL_NUMBERS
WHERE SUBSTR(serial_number, 5, 16) = '6605170948258844';
SELECT * FROM MTL_SERIAL_NUMBERS_ALL_V;
SELECT * FROM mtl_material_transactions;
UPDATE XX_TEMP_SERIAL
SET XX_TEMP_SERIAL.ATTRIBUTE1 = mtl_serial_numbers.Serial
FROM mtl_serial_numbers ,
  XX_TEMP_SERIAL
WHERE 0                          =0
AND SUBSTR(serial_number, 5, 16) = serial ;
--
--
UPDATE XX_TEMP_SERIAL
SET XX_TEMP_SERIAL.ATTRIBUTE1 = ( select MTL_SERIAL_NUMBERS.SERIAL_NUMBER
FROM mtl_serial_numbers , XX_TEMP_SERIAL
WHERE 0                          =0
AND SUBSTR(serial_number, 5, 16) = serial);


select XX_TEMP_SERIAL.serial,MTL_SERIAL_NUMBERS.SERIAL_NUMBER
FROM mtl_serial_numbers , XX_TEMP_SERIAL
WHERE 0                          =0
AND SUBSTR(serial_number, 5, 16) = serial;
