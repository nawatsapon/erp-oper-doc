SELECT
     hd.segment1                 vendor_code,
     hd.vendor_name,
     hd.vendor_name_alt,
     hd.vat_registration_num,
     hd.end_date_active,
     hd.vendor_type_lookup_code,
     hd.vat_code,
     hd.match_option,
     site.vendor_site_code,
     site.inactive_date,
     site.vat_registration_num   site_vat_registration_num,
     site.address_line1,
     site.address_line2,
     site.address_line3,
     site.city,
     site.state,
     site.zip,
     site.province,
     site.country,
     site.phone,
     site.pay_group_lookup_code,
     site.attribute3             bank_code,
     bk.flex_value_meaning,
     site.attribute4             bank_account,
     bk.flex_value               bank_code_flx,
     bk.description              bank_name,
     site.attribute5             client_code,
     site.attribute6             debit_account,
     pifo_su.user_name           supp_last_updated_by,
     pifo_su.full_name           supp_last_upd_by_full,
     pifo_si.user_name           site_last_updated_by,
     pifo_si.full_name           site_last_upd_by_full,
     hd.bank_account_name,
     hd.bank_account_num,
     hd.bank_num,
     hd.bank_branch_type,
     hd.enabled_flag             sup_enabled_flag,
     hd.start_date_active        sup_start_date_active,
     hd.last_updated_by,
     hd.last_update_date vendor_last_update_date,
     site.last_update_date site_last_update_date,
     hd.last_update_login,
     hd.accts_pay_code_combination_id
 FROM
     po.po_vendors hd,
     po.po_vendor_sites_all site,
     hr_organization_units org,
     (
         SELECT DISTINCT
             fnd_user.user_id,
             fnd_user.user_name,
             per_all_people_f.full_name
         FROM
             fnd_user,
             per_all_people_f
         WHERE
             fnd_user.employee_id = per_all_people_f.person_id
     ) pifo_su,
     (
         SELECT DISTINCT
             fnd_user.user_id,
             fnd_user.user_name,
             per_all_people_f.full_name
         FROM
             fnd_user,
             per_all_people_f
         WHERE
             fnd_user.employee_id = per_all_people_f.person_id
     ) pifo_si,
     (
         SELECT
             flex_value,
             flex_value_meaning,
             description
         FROM
             fnd_flex_values_vl
         WHERE
             ( ( '' IS NULL )
               OR ( structured_hierarchy_level IN (
                 SELECT
                     hierarchy_id
                 FROM
                     fnd_flex_hierarchies_vl h
                 WHERE
                     h.flex_value_set_id = 1009802
                     AND h.hierarchy_name LIKE ''
             ) ) )
             AND ( flex_value_set_id = 1009802 )
     ) bk
 WHERE
     1 = 1 --hd.END_DATE_ACTIVE, Site.INACTIVE_DATE
     AND hd.vendor_id = site.vendor_id (+)
     AND site.org_id = org.organization_id
     AND hd.last_updated_by = pifo_su.user_id (+)
     AND site.last_updated_by = pifo_si.user_id (+)
     AND site.attribute3 = bk.flex_value (+)
     AND ( trunc(hd.last_update_date) BETWEEN TO_DATE('01/01/2019','dd/mm/yyyy') AND TO_DATE('31/08/2019','dd/mm/yyyy')
           OR trunc(site.last_update_date) BETWEEN TO_DATE('01/01/2019','dd/mm/yyyy') AND TO_DATE('31/08/2019','dd/mm/yyyy') )
--AND HD.Global_Attribute6 in ('STS','DMS')
  --and Site.attribute1 is not null
 ORDER BY
     1,
     2
  --And site.Vendor_Site_Code Like '%DTN%' and site.accts_pay_code_combination_id = 4633
  --and hd.vendor_name like '%ອ��Թ��%'
  --and trunc(Hd.LAST_UPDATE_DATE) >= to_date('01/01/2016','dd/mm/yyyy')
  
  --AND Site.vendor_site_code LIKE '%B%'
  --and Hd.last_update_date > to_date('31/12/2015 23:59:59','dd/mm/yyyy HH24:MI:SS')
  --