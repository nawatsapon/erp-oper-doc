"select org.organization_name, mt.subinventory_code, subinv.description, 
mt.transaction_source_name, st.segment1 item, st.description item_desc, mt.transaction_date, abs(mt.transaction_quantity) qty
from mtl_material_transactions mt, MTL_SYSTEM_ITEMS st, 
org_organization_definitions org, mtl_secondary_inventories subinv
where mt.inventory_item_id = st.inventory_item_id and st.organization_id = mt.organization_id
and org.organization_id = mt.organization_id and st.organization_id = org.organization_id
and mt.subinventory_code = subinv.secondary_inventory_name
and mt.organization_id = subinv.organization_id
and trunc(mt.transaction_date) between '01/JUL/2017' and '31/JUL/2017'
and mt.transaction_type_id = '118'
and st.segment1 in ('APPLEPENCIT'
,'IPAC0RU1100'
,'IPAC0050030'
,'IPAC0050040'
,'IPAC0050050'
,'IPAC0RU0300'
,'IPAC0RU0800'
,'IPAC05C0020'
,'IPAC05C0030'
,'IPAC05C0040'
,'IPAC05C0050'
,'IPAC05C0060'
,'IPAC05C0070'
,'IPAC6000010'
,'IPAC6000020'
,'IPAC6000030'
,'IPAC6000040'
,'IPAC6000050'
,'IPAC6000060'
,'IPAC6000070'
,'IPAC6000080'
,'IPAC6000090'
,'IPAC6000100'
,'IPAC6000110'
,'IPAC6P00010'
,'IPAC6P00020'
,'IPAC6P00030'
,'IPAC6P00040'
,'IPAC6P00050'
,'IPAC6P00060'
,'IPAC6P00070'
,'IPAC6P00080'
,'IPAC6P00090'
,'IPAC6P00100'
,'IPAC6P00110')"
