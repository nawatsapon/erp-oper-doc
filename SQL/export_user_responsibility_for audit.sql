SELECT DISTINCT application_name,
  r.responsibility_name,
  --security_group_name,
  pp.employee_number,
  pp.last_name,
  pp.first_name,
  pp.full_name,
  user_name,
  gcc.segment10 RC_Code,
  -- org.description RC_Name,
  TO_CHAR(ur.start_date,'dd/mm/yyyy') start_date,
  --greatest(to_date(u.start_date), to_date(ur.start_date), to_date(r.start_date)) start_date,
  TO_CHAR(ur.end_date,'dd/mm/yyyy') end_date,
  TO_CHAR(u.START_DATE,'dd/mm/yyyy') USER_START_DATE,
  TO_CHAR(u.END_DATE,'dd/mm/yyyy') USER_END_DATE
  --,DECODE(least(NVL(u.end_date, to_date('01/01/4712', 'DD/MM/YYYY')), NVL(ur.end_date, to_date('01/01/4712', 'DD/MM/YYYY')), NVL(r.end_date, to_date('01/01/4712', 'DD/MM/YYYY'))), to_date('01/01/4712', 'DD/MM/YYYY'), '', least(NVL(u.end_date, NVL(ur.end_date, r.end_date)), NVL(ur.end_date, NVL(u.end_date, r.end_date)), NVL(r.end_date, NVL(u.end_date, ur.end_date)))) end_date
FROM
  (SELECT *
  FROM fnd_user
  WHERE 0         =0
  AND (END_DATE  IS NULL
  OR END_DATE     > sysdate)
  AND START_DATE <= sysdate
    --AND FND_USER.USER_NAME NOT IN ('ICONSULT','APPLAYBACK','APRECORDER','DTAC','FAPLAYBACK','FARECORDER','PLAYBACK','RECORDER','SYSADMIN','WIZARD' )
  ) u,
  (SELECT *
  FROM fnd_user_resp_groups_all
  WHERE (END_DATE IS NULL
  OR END_DATE      > sysdate)
  AND START_DATE  <= sysdate
  ) ur,
  (SELECT APPLSYS.FND_RESPONSIBILITY.END_DATE,
    APPLSYS.FND_RESPONSIBILITY.APPLICATION_ID,
    APPLSYS.FND_RESPONSIBILITY.RESPONSIBILITY_ID,
    APPLSYS.FND_RESPONSIBILITY.START_DATE,
    APPLSYS.FND_RESPONSIBILITY.RESPONSIBILITY_KEY,
    APPLSYS.FND_RESPONSIBILITY.MENU_ID,
    APPLSYS.FND_RESPONSIBILITY.GROUP_APPLICATION_ID,
    APPLSYS.FND_RESPONSIBILITY_TL.RESPONSIBILITY_NAME,
    APPLSYS.FND_RESPONSIBILITY.VERSION
  FROM APPLSYS.FND_RESPONSIBILITY,
    APPLSYS.FND_RESPONSIBILITY_TL
  WHERE 0                                          =0
  AND APPLSYS.FND_RESPONSIBILITY.RESPONSIBILITY_ID = APPLSYS.FND_RESPONSIBILITY_TL.RESPONSIBILITY_ID
  AND APPLSYS.FND_RESPONSIBILITY.APPLICATION_ID    = APPLSYS.FND_RESPONSIBILITY_TL.APPLICATION_ID
  AND 0                                            = 0
  AND (APPLSYS.FND_RESPONSIBILITY.END_DATE        IS NULL
  OR APPLSYS.FND_RESPONSIBILITY.END_DATE           > SysDate)
  ) r,
  FND_APPLICATION_TL a,
  FND_SECURITY_GROUPS s,
  (SELECT *
  FROM per_people_f
  WHERE (EFFECTIVE_END_DATE IS NULL
  OR EFFECTIVE_END_DATE      > sysdate)
  AND EFFECTIVE_START_DATE  <= sysdate
  )pp,
  (SELECT *
  FROM per_assignments_f
  WHERE (EFFECTIVE_END_DATE IS NULL
  OR EFFECTIVE_END_DATE      > sysdate)
  AND EFFECTIVE_START_DATE  <= sysdate
  ) pa,
  gl_code_combinations gcc
  --,FND_FLEX_VALUES_VL org
WHERE 0                              =0
AND u.user_id                        = ur.user_id(+)
AND ur.responsibility_application_id = r.application_id
AND ur.responsibility_id             = r.responsibility_id
AND r.application_id                 = a.application_id
AND ur.security_group_id             = s.security_group_id(+)
  --AND ur.start_date                                <= sysdate
  --AND NVL(ur.end_date, sysdate + 1)                 > sysdate
  --AND u.start_date                <= sysdate
  --AND NVL(u.end_date, sysdate + 1) > sysdate
  --AND r.start_date                                 <= sysdate
  --AND NVL(r.end_date, sysdate + 1)                  > sysdate
    --AND gcc.segment10           = org.flex_value(+)
      --AND org.flex_value_set_id   = 1009748
AND r.version    IN ('4', 'W', 'M')
AND u.Employee_ID = pp.person_id(+)
  --AND TO_CHAR(pa.EFFECTIVE_END_DATE, 'DD-MON-RRRR') = '31-DEC-4712'
AND pp.person_id            = pa.person_id(+)
AND pa.default_code_comb_id = gcc.code_combination_id(+)
--AND pp.full_name            = 'PRASERT, Mr. SENCHATTURAT'
  --and r.responsibility_name = 'Inventory'

  --AND RESPONSIBILITY_NAME     = 'User Management'
  --and PP.EMPLOYEE_NUMBER in (
  --'95'
  --)
ORDER BY USER_NAME,
  1,2 ;
SELECT * FROM per_people_f pp WHERE pp.full_name = 'PRASERT, Mr. SENCHATTURAT'