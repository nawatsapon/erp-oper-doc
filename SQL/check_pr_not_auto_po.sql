SELECT distinct prh.segment1,PRH.REQUISITION_HEADER_ID,PRH.CREATION_DATE
FROM po_requisition_headers_all prh,
  po_requisition_lines_all prl ,
  po_req_distributions_all prd,
  po_distributions_all pda
WHERE prl.BLANKET_PO_HEADER_ID IS NOT NULL
AND prh.requisition_header_id   = prl.requisition_header_id
AND prl.requisition_line_id     = prd.requisition_line_id(+)
AND prd.distribution_id         = pda.req_distribution_id(+) 
AND pda.req_distribution_id is null
and trunc(PRH.CREATION_DATE) >= to_date('01/08/2017','dd/mm/yyyy')
AND prh.authorization_status = 'APPROVED'
order by PRH.CREATION_DATE,prh.segment1;
/*and prh.segment1 in ('10170015522',
'10170015521',
'10170015520',
'10170015517',
'10170015515',
'10170015513',
'10170015512',
'10170015505',
'10170015293',
'10170015291')*/
;
