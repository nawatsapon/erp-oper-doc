SELECT pha.segment1 "PO",
  prh.segment1 "PR Number" ,
  prl.line_num ,
  prl.item_description ,
  prl.unit_meas_lookup_code "UOM" ,
  prl.quantity ,
  prl.unit_price ,
  prl.currency_code ,
  prl.rate ,
  prh.creation_date PR_creation_date,
  glc.segment11 Account_code ,
  prl.quantity*prl.unit_price*NVL(prl.rate,1) "Amount",
  cat.SEGMENT1
  || '.'
  || cat.SEGMENT2
  || '.'
  || cat.SEGMENT3
  || '.'
  ||cat.SEGMENT4 "CATEGORY",
  buy.full_name "Buyer Name",
   per.full_name "Requester Name" ,
  pera.assignment_number REQUESTOR_EMP_NO,
  ffv.DEPT ,
  ffv.DIV ,
  f.segment1 "Project Code" ,
  f.name "Project Name" ,
  g.task_number,
  prd.EXPENDITURE_TYPE,
  per_final.full_name "Last_Approver Name",
  per_final.EMPLOYEE_NUMBER Last_Approver_EMP_No,
  FFV2.DEPT ,
  FFV2.DIV 
  /*,phis.object_id,
  prh.authorization_status,
  phis.EMPLOYEE_ID ,     
  prh.preparer_id,
  pera.person_id Requestor_ID , 
  phis_final.EMPLOYEE_ID, 
  pha.creation_date PO_creation_date,
  PO_HEADERS_SV3.GET_PO_STATUS(pda.PO_HEADER_ID) PO_APPROVAL_STATUS,
  apv.vendor_number ,
  apv.vendor_name,
  PLTB.LINE_TYPE*/
FROM po_requisition_headers_all prh,
  po_requisition_lines_all prl ,
  po_req_distributions_all prd,
  PO_ACTION_HISTORY phis,
  gl_code_combinations glc,
  mtl_categories cat,
  PA_PROJECTS_ALL f ,
  PA_TASKS g ,
  (SELECT *
  FROM
    (SELECT ROW_NUMBER() OVER(PARTITION BY PERSON_ID ORDER BY effective_start_date DESC) AS Row_No,
      PER_PEOPLE_F.*
    FROM PER_PEOPLE_F
    )
  WHERE Row_No =1
  ) buy ,
  (SELECT *
  FROM
    (SELECT ROW_NUMBER() OVER(PARTITION BY PERSON_ID ORDER BY effective_start_date DESC) AS Row_No,
      PER_PEOPLE_F.*
    FROM PER_PEOPLE_F
    )
  WHERE Row_No =1
  ) per ,
  (SELECT person_id ,
    assignment_id ,
    EFFECTIVE_START_DATE ,
    EFFECTIVE_END_DATE ,
    DEFAULT_CODE_COMB_ID ,
    PER_ASSIGNMENTS_F.ASSIGNMENT_NUMBER
  FROM PER_ASSIGNMENTS_F
  WHERE effective_end_date >= sysdate
  ) pera,
  (SELECT FFVL2.FLEX_VALUE,
    FFVL2.DESCRIPTION DEPT,
    FFVL1.DESCRIPTION DIV
    --  FVNH.PARENT_FLEX_VALUE
  FROM FND_FLEX_VALUE_NORM_HIERARCHY FVNH,
    FND_FLEX_VALUES_VL FFVL1,
    FND_FLEX_VALUES_VL FFVL2
  WHERE FVNH.FLEX_VALUE_SET_ID = 1009748
  AND FFVL1.FLEX_VALUE_SET_ID  = 1009748
  AND FFVL2.FLEX_VALUE_SET_ID  = 1009748
  AND FVNH.PARENT_FLEX_VALUE   = FFVL1.FLEX_VALUE
  AND FFVL2.FLEX_VALUE BETWEEN FVNH.CHILD_FLEX_VALUE_LOW AND FVNH.CHILD_FLEX_VALUE_HIGH
    --AND NVL(TRUNC(FFVL2.END_DATE_ACTIVE)
  ) FFV,
  GL_CODE_COMBINATIONS gcc,
  (SELECT *
  FROM
    (SELECT ROW_NUMBER() OVER(PARTITION BY ph1.object_id ORDER BY sequence_num DESC) AS Row_No,
      ph1.EMPLOYEE_ID,
      ph1.object_id
    FROM PO_ACTION_HISTORY ph1
    WHERE ph1.action_code = 'APPROVE'
    AND object_type_code  = 'REQUISITION'
    )
  WHERE Row_No = 1
  ) phis_final,
  (SELECT *
  FROM
    (SELECT ROW_NUMBER() OVER(PARTITION BY PERSON_ID ORDER BY effective_start_date DESC) AS Row_No,
      PER_PEOPLE_F.*
    FROM PER_PEOPLE_F
    )
  WHERE Row_No =1
  ) per_final,
  (SELECT FFVL2.FLEX_VALUE,
    FFVL2.DESCRIPTION DEPT,
    FFVL1.DESCRIPTION DIV
    --
  FROM FND_FLEX_VALUE_NORM_HIERARCHY FVNH,
    FND_FLEX_VALUES_VL FFVL1,
    FND_FLEX_VALUES_VL FFVL2
  WHERE FVNH.FLEX_VALUE_SET_ID = 1009748
  AND FFVL1.FLEX_VALUE_SET_ID  = 1009748
  AND FFVL2.FLEX_VALUE_SET_ID  = 1009748
  AND FVNH.PARENT_FLEX_VALUE   = FFVL1.FLEX_VALUE
  AND FFVL2.FLEX_VALUE BETWEEN FVNH.CHILD_FLEX_VALUE_LOW AND FVNH.CHILD_FLEX_VALUE_HIGH
    --AND NVL(TRUNC(FFVL2.END_DATE_ACTIVE)
  ) FFV2,
  GL_CODE_COMBINATIONS gcc2,
  (SELECT person_id ,
    assignment_id ,
    EFFECTIVE_START_DATE ,
    EFFECTIVE_END_DATE ,
    DEFAULT_CODE_COMB_ID ,
    PER_ASSIGNMENTS_F.ASSIGNMENT_NUMBER
  FROM PER_ASSIGNMENTS_F
  WHERE effective_end_date >= sysdate
    /*and job_id >= 112*/
  ) perb ,
  po_distributions_all pda,
  po_headers_all pha,
  AP_VENDORS_V apv,
  PO.PO_LINE_TYPES_TL PLTB
WHERE 0                       =0
AND prh.requisition_header_id = phis.object_id
AND prh.requisition_header_id = prl.requisition_header_id
AND prl.requisition_line_id   = prd.requisition_line_id(+)
AND prd.code_combination_id   = glc.code_combination_id(+)
AND prl.category_id           = cat.CATEGORY_ID(+)
AND prd.project_id            = f.project_id (+)
AND prd.task_id               = g.task_id (+)
AND prl.suggested_buyer_id    = buy.person_id (+)
AND prh.preparer_id           = per.person_id(+)
AND per.person_id             = pera.PERSON_ID(+)
AND pera.DEFAULT_CODE_COMB_ID = gcc.code_combination_id(+)
AND gcc.segment10             = ffv.FLEX_VALUE (+)
AND prh.requisition_header_id = phis_final.object_id
AND phis_final.EMPLOYEE_ID    = per_final.person_id
AND per_final.PERSON_ID       = perb.person_id (+)
AND perb.DEFAULT_CODE_COMB_ID = gcc2.code_combination_id(+)
AND gcc2.segment10            = ffv2.FLEX_VALUE (+)
AND prd.distribution_id       = pda.req_distribution_id(+)
AND pda.po_header_id          = pha.po_header_id(+)
AND pha.vendor_id             = apv.vendor_id(+)
AND PRL.LINE_TYPE_ID          = PLTB.LINE_TYPE_ID(+)
  --AND phis.EMPLOYEE_ID          IN (145099 ,131657,124846)
AND phis.action_code         = 'APPROVE'
AND prh.authorization_status = 'APPROVED'
AND prh.creation_date BETWEEN to_date('01/01/2017','dd/mm/yyyy') AND to_date('30/06/2017','dd/mm/yyyy')
ORDER BY 1,3 ;