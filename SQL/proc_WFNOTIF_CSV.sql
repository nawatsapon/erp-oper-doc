create or replace PROCEDURE wfnotif_CSV(tsn IN number, type out NUMBER)
AS
  CURSOR c_data
  IS
    SELECT NVL(TO_CHAR(wf_notifications.NOTIFICATION_ID),' ') NOTIFICATION_ID,
      NVL(TO_CHAR(wf_notifications.GROUP_ID),' ') GROUP_ID,
      NVL(TO_CHAR(wf_notifications.MESSAGE_TYPE),' ') MESSAGE_TYPE,
      NVL(TO_CHAR(wf_notifications.MESSAGE_NAME),' ') MESSAGE_NAME,
      NVL(TO_CHAR(wf_notifications.RECIPIENT_ROLE),' ') RECIPIENT_ROLE,
      NVL(TO_CHAR(wf_notifications.STATUS),' ') STATUS,
      NVL(TO_CHAR(wf_notifications.ACCESS_KEY),' ') ACCESS_KEY,
      NVL(TO_CHAR(wf_notifications.MAIL_STATUS),' ') MAIL_STATUS,
      NVL(TO_CHAR(wf_notifications.PRIORITY),' ') PRIORITY,
      NVL(TO_CHAR(wf_notifications.BEGIN_DATE, 'dd/mm/yyyy hh:mm:ss'),' ') BEGIN_DATE,
      NVL(TO_CHAR(wf_notifications.END_DATE, 'dd/mm/yyyy hh:mm:ss'),' ') END_DATE,
      NVL(TO_CHAR(wf_notifications.DUE_DATE, 'dd/mm/yyyy hh:mm:ss'),' ') DUE_DATE,
      NVL(TO_CHAR(wf_notifications.RESPONDER),' ') RESPONDER,
      NVL(TO_CHAR(wf_notifications.USER_COMMENT),' ') USER_COMMENT,
      NVL(TO_CHAR(wf_notifications.CALLBACK),' ')CALLBACK,
      NVL(TO_CHAR(wf_notifications.CONTEXT),' ') CONTEXT,
      NVL(TO_CHAR(wf_notifications.ORIGINAL_RECIPIENT),' ') ORIGINAL_RECIPIENT,
      NVL(TO_CHAR(wf_notifications.FROM_USER),' ') FROM_USER,
      NVL(TO_CHAR(wf_notifications.TO_USER),' ') TO_USER,
      NVL(TO_CHAR(wf_notifications.SUBJECT),' ') SUBJECT,
      NVL(TO_CHAR(wf_notifications.LANGUAGE),' ') LANGUAGE,
      NVL(TO_CHAR(wf_notifications.MORE_INFO_ROLE),' ') MORE_INFO_ROLE,
      NVL(TO_CHAR(wf_notifications.FROM_ROLE),' ') FROM_ROLE,
      NVL(TO_CHAR(wf_notifications.SECURITY_GROUP_ID),' ') SECURITY_GROUP_ID,
      NVL(TO_CHAR(wf_notifications.USER_KEY),' ') USER_KEY,
      NVL(TO_CHAR(wf_notifications.ITEM_KEY),' ') ITEM_KEY
    FROM wf_notifications
    WHERE 0                                =0
    --AND TRUNC(wf_notifications.BEGIN_DATE) > To_date('20120101','yyyymmdd')
    AND (wf_notifications.END_DATE is null or wf_notifications.END_DATE >= sysdate-1)
    AND wf_notifications.MESSAGE_TYPE LIKE '%PO%'  ;
  v_file UTL_FILE.FILE_TYPE;
BEGIN
  v_file := UTL_FILE.FOPEN(location => 'TMP_DUMP_NOTIFY', filename => 'wfnotif' || '_' || to_char(sysdate, 'yyyymmdd') || '.csv', open_mode => 'w', max_linesize => 32767);
   UTL_FILE.PUT_LINE(v_file, 'NOTIFICATION_ID' || '|' ||
                              'GROUP_ID' || '|' || 
                              'MESSAGE_TYPE' || '|' || 
                              'MESSAGE_NAME' || '|' || 
                              'RECIPIENT_ROLE' || '|' || 
                              'STATUS' || '|' || 
                              'ACCESS_KEY' || '|' || 
                              'MAIL_STATUS' || '|' || 
                              'PRIORITY' || '|' || 
                              'BEGIN_DATE' || '|' || 
                              'END_DATE' || '|' || 
                              'DUE_DATE' || '|' || 
                              'RESPONDER' || '|' || 
                              'USER_COMMENT' || '|' || 
                              'CALLBACK' || '|' || 
                              'CONTEXT' || '|' || 
                              'ORIGINAL_RECIPIENT' || '|' || 
                              'FROM_USER' || '|' || 
                              'TO_USER' || '|' || 
                              'SUBJECT' || '|' || 
                              'LANGUAGE' || '|' || 
                              'MORE_INFO_ROLE' || '|' || 
                              'FROM_ROLE' || '|' || 
                              'SECURITY_GROUP_ID' || '|' || 
                              'USER_KEY' || '|' || 
                              'ITEM_KEY');
  FOR cur_rec IN c_data
  LOOP    
    UTL_FILE.PUT_LINE(v_file, cur_rec.NOTIFICATION_ID || '|' ||
                              cur_rec.GROUP_ID || '|' || 
                              cur_rec.MESSAGE_TYPE || '|' || 
                              cur_rec.MESSAGE_NAME || '|' || 
                              cur_rec.RECIPIENT_ROLE || '|' || 
                              cur_rec.STATUS || '|' || 
                              cur_rec.ACCESS_KEY || '|' || 
                              cur_rec.MAIL_STATUS || '|' || 
                              cur_rec.PRIORITY || '|' || 
                              cur_rec.BEGIN_DATE || '|' || 
                              cur_rec.END_DATE || '|' || 
                              cur_rec.DUE_DATE || '|' || 
                              cur_rec.RESPONDER || '|' || 
                              cur_rec.USER_COMMENT || '|' || 
                              cur_rec.CALLBACK || '|' || 
                              cur_rec.CONTEXT || '|' || 
                              cur_rec.ORIGINAL_RECIPIENT || '|' || 
                              cur_rec.FROM_USER || '|' || 
                              cur_rec.TO_USER || '|' || 
                              cur_rec.SUBJECT || '|' || 
                              cur_rec.LANGUAGE || '|' || 
                              cur_rec.MORE_INFO_ROLE || '|' || 
                              cur_rec.FROM_ROLE || '|' || 
                              cur_rec.SECURITY_GROUP_ID || '|' || 
                              cur_rec.USER_KEY || '|' || 
                              cur_rec.ITEM_KEY);
  END LOOP;
  UTL_FILE.FCLOSE(v_file);
EXCEPTION
WHEN OTHERS THEN
  UTL_FILE.FCLOSE(v_file);
  RAISE;
END;