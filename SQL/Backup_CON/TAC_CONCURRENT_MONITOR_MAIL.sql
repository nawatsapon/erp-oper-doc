create or replace PROCEDURE TAC_CONCURRENT_MONITOR_MAIL(
    errbuf OUT VARCHAR2,
    retcode OUT NUMBER)
AS
  CURSOR CONCURRENT
  IS
    SELECT *
    FROM
      (SELECT
        CASE
          WHEN DECODE (FND_CONCURRENT_REQUESTS.DESCRIPTION, NULL, FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME, FND_CONCURRENT_REQUESTS.DESCRIPTION
            || ' ('
            || FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME
            || ')' ) IN ('Workflow Background Process','Transfer transactions to GL')
          THEN MAX(FND_CONCURRENT_REQUESTS.REQUEST_ID) OVER (PARTITION BY NVL(FND_CONCURRENT_REQUESTS.DESCRIPTION,FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME) ,FND_RESPONSIBILITY_TL.RESPONSIBILITY_NAME,FND_CONCURRENT_REQUESTS.ARGUMENT_TEXT )
          ELSE MAX(FND_CONCURRENT_REQUESTS.REQUEST_ID) OVER (PARTITION BY NVL(FND_CONCURRENT_REQUESTS.DESCRIPTION,FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME) ,FND_RESPONSIBILITY_TL.RESPONSIBILITY_NAME )
        END AS max_con,
        (
        CASE
          WHEN STATUS_DESC = 'Normal'
          THEN 0
          WHEN (STATUS_DESC                           = 'Warning'
          AND FND_CONCURRENT_REQUESTS.COMPLETION_TEXT = 'No data was found in the GL_INTERFACE table.')
          THEN 0
          WHEN (STATUS_DESC = 'Warning'
          AND DECODE (FND_CONCURRENT_REQUESTS.DESCRIPTION, NULL, FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME, FND_CONCURRENT_REQUESTS.DESCRIPTION
            || ' ('
            || FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME
            || ')') = 'Pick Selection List Generation - SRS')
          THEN 99
          WHEN STATUS_DESC = 'Warning'
          THEN 99
          ELSE 1
        END) CODE,
        STATUS_DESC,
        FND_CONCURRENT_REQUESTS.REQUEST_ID,
        CASE
          WHEN DECODE (FND_CONCURRENT_REQUESTS.DESCRIPTION, NULL, FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME, FND_CONCURRENT_REQUESTS.DESCRIPTION
            || ' ('
            || FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME
            || ')' ) IN ('Workflow Background Process','Transfer transactions to GL')
          THEN DECODE (FND_CONCURRENT_REQUESTS.DESCRIPTION, NULL, FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME, FND_CONCURRENT_REQUESTS.DESCRIPTION
            || ' ('
            || FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME
            || ')' )
            || ' ('
            || FND_CONCURRENT_REQUESTS.ARGUMENT_TEXT
            || ')'
          ELSE DECODE (FND_CONCURRENT_REQUESTS.DESCRIPTION, NULL, FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME, FND_CONCURRENT_REQUESTS.DESCRIPTION
            || ' ('
            || FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME
            || ')' )
        END CONCURRENT_PROGRAM_NAME ,
        --APP.APPLICATION_SHORT_NAME,
        --APP.APPLICATION_NAME ,
        FND_RESPONSIBILITY_TL.RESPONSIBILITY_NAME ,
        FND_USER.USER_NAME,
        --ORGANIZATION
        FND_CONCURRENT_REQUESTS.ARGUMENT_TEXT AS "Paramaters",
        FND_CONCURRENT_REQUESTS.PRIORITY_REQUEST_ID,
        --PRIORITY_REQUEST_NAME
        FND_CONCURRENT_REQUESTS.PRIORITY,
        (FND_CONCURRENT_REQUESTS.RESUBMIT_INTERVAL
        ||' '
        ||FND_CONCURRENT_REQUESTS.RESUBMIT_INTERVAL_UNIT_CODE) FREQUENCY ,
        --U.USER_NAME REQUESTED_BY ,
        FND_CONCURRENT_REQUESTS.REQUESTED_START_DATE,
        FND_CONCURRENT_REQUESTS.ACTUAL_START_DATE,
        FND_CONCURRENT_REQUESTS.ACTUAL_COMPLETION_DATE,
        FND_CONCURRENT_PROGRAMS_TL.DESCRIPTION ACTIVITY ,
        PHASE_DESC
        || ' : '
        || STATUS_DESC STATUS,
        FND_CONCURRENT_REQUESTS.COMPLETION_TEXT
      FROM FND_CONCURRENT_PROGRAMS_TL,
        (SELECT *
        FROM
          (SELECT
            CASE
              WHEN STATUS_CODE IN ('A','Z')
              THEN 'Waiting'
              WHEN STATUS_CODE = 'B'
              THEN 'Resuming'
              WHEN STATUS_CODE IN ('C','I','R')
              THEN 'Normal'
              WHEN STATUS_CODE = 'E'
              THEN 'Error'
              WHEN STATUS_CODE = 'F'
              THEN 'Scheduled'
              WHEN STATUS_CODE = 'G'
              THEN 'Warning'
              WHEN STATUS_CODE = 'H'
              THEN 'On Hold'
              WHEN STATUS_CODE = 'M'
              THEN 'No Manager'
              WHEN STATUS_CODE = 'Q'
              THEN 'Standby'
              WHEN STATUS_CODE = 'S'
              THEN 'Suspended'
              WHEN STATUS_CODE = 'T'
              THEN 'Terminating'
              WHEN STATUS_CODE = 'U'
              THEN 'Disabled'
              WHEN STATUS_CODE = 'W'
              THEN 'Paused'
              WHEN STATUS_CODE = 'X'
              THEN 'Terminated'
            END AS STATUS_DESC,
            CASE
              WHEN PHASE_CODE = 'C'
              THEN 'Completed'
              WHEN PHASE_CODE = 'I'
              THEN 'Inactive'
              WHEN PHASE_CODE = 'P'
              THEN 'Pending'
              WHEN PHASE_CODE = 'R'
              THEN 'Running'
            END AS PHASE_DESC,
            FND_CONCURRENT_REQUESTS.*
          FROM FND_CONCURRENT_REQUESTS
          WHERE 0                                              =0
          AND TRUNC(FND_CONCURRENT_REQUESTS.ACTUAL_START_DATE) = TRUNC(sysdate -1)
          )
        -- WHERE max_con = REQUEST_ID
        ) FND_CONCURRENT_REQUESTS,
        FND_RESPONSIBILITY_TL,
        FND_USER
      WHERE 0                                                 =0
      AND FND_CONCURRENT_PROGRAMS_TL.CONCURRENT_PROGRAM_ID(+) = FND_CONCURRENT_REQUESTS.CONCURRENT_PROGRAM_ID
      AND FND_CONCURRENT_REQUESTS.RESPONSIBILITY_ID           = FND_RESPONSIBILITY_TL.RESPONSIBILITY_ID
      AND FND_CONCURRENT_REQUESTS.REQUESTED_BY                = FND_USER.USER_ID
      AND (DECODE (FND_CONCURRENT_REQUESTS.DESCRIPTION, NULL, FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME, FND_CONCURRENT_REQUESTS.DESCRIPTION
        || ' ('
        || FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME
        || ')' ) IN ( 'Actual Cost Worker' , 'TAC Import AR Invoice Data Alert - VGS (Check Periodic Alert)'                           -- Not found
        , 'Create Internal Orders' , 'General Ledger Transfer Program'                                          --OK
        , 'Margin Analysis Load Run'                                                                            --OK
        , 'Order Import'                                                                                        --There're 5 Order Import from 5 different OU of TAC : Import POS internal order --OK
        , 'PRC: Maintain Budgetary Control Balances'                                                            --OK
        , 'PRC: Update Project Summary Amounts'                                                                 --OK
        --, 'PRC: Interface Expense Reports from Payables'                                                        --OK
        --, 'PRC: Interface Supplier Costs' 
        , 'Payables Accounting Process'                                       --C
        , 'Payables Transfer to General Ledger' 
        , 'Pick Selection List Generation - SRS'                        --OK
        , 'Process transaction interface'                                                                       --2 duplicated transactios (same time same parameters). should one be deleted??
        , 'Program - Automatic Posting'                                                                         --'Program - Automatic Posting (Sub ledger Posting)'  --Submodule Posting = 2  --OK
        , 'Program - Import Journals' , 'Program - Maintain Budget Organization'                                --OK
        , 'Program - Maintain Summary Templates'                                                                --OK
        , 'Purge Obsolete Workflow Runtime Data'                                                                --OK
        , 'Receiving Transaction Processor'                                                                     --OK
        , 'TAC Create FND_USER From Interface Employee' , 'TAC_EREFILL Autoinvoice Master Program (Report Set)' --C
        , 'TAC_OTHER Autoinvoice Master Program (Report Set)'                                                   --C
        , 'TAC_PREMIUM Autoinvoice Master Program (Report Set)'                                                 --C
        , 'TAC_SIMCARD Autoinvoice Master Program (Report Set)'                                                ---C
        , 'TAC_VOUCHER Autoinvoice Master Program (Report Set)'                                                 --C
        , 'Requisition Import' , 'TAC Account Combination Interface'                                            --OK
        , 'TAC Allocate / De-Allocate data from ERP' , 'TAC ERP to DMS Bank Master' , 'TAC ERP to DMS Item Master' , 'TAC ERP to DMS Partner Back' , 'TAC ERP to Discovery Product Master' , 'TAC Journal Sources '
        || chr(38)
        ||' Categories Interface'                                               --OK
        , 'TAC Organization Chart Interface Version 4'                          --OK
        , 'TAC Organization Clean Data'                                         --OK
        , 'TAC Pre-Activate SIM Interface'                                      --OK
        , 'TAC : Import POS internal order (Not Auto Release) HO (Report Set)'  --OK
        , 'TAC : Import POS internal order (Not Auto Release) SRC (Report Set)' --OK
        , 'TAC : Import POS internal order (Not Auto Release) PSL (Report Set)' --OK
        , 'TAC : Import POS internal order (Not Auto Release) KKC (Report Set)' --OK
        , 'TAC : Import POS internal order (Not Auto Release) SRT (Report Set)' --OK
        , 'TAC UD and Voucher Supplier Stock Balance Update'                    --OK
        , 'TAC Update SIM Activate Date in Serial Number Master Data'           --OK
        , 'Transfer transactions to GL'                                         --12 Org  --OK
        -- , 'Program - Create Journals'  --C
        --'Program - Automatic Posting  (Encumbrance Posting)' --Encumbrance Posting = 1  --OK
        , 'Import Shipping Locations'                 --OK
        , 'TAC Synchronize username changed from OID' --OK
        , 'TAC Export Sales Data from ERP to Peugeot' --OK
        --, 'Workflow Background Process'      --UNION        --Missing 2
        --, 'Workflow Background Process (PA Budget Integration Workflow)'   --OK
        --, 'Workflow Background Process (OM Order Line)'       --OK
        --, 'Workflow Background Process (Expenses:::Yes:Yes:Yes)'     --OK
        --, 'Workflow Background Process (PO Requisition Approval:::Yes:No:)'  --OK
        --, 'Workflow Background Process (PO Requisition Approval:::Yes:Yes:)'  --OK
        --, 'Workflow Background Process (PO Approval:::Yes:Yes:)'     --OK
        --, 'Workflow Background Process (PO Approval:::Yes:No:)'     --Missing
        --, 'Workflow Background Process (PO Approval:::No:Yes:)'     --Missing
        -- , 'TAC AP Supplier Interface Program'  --C
        --, 'JA Thai Vendor WHT Certificate Report for Generate WHT Number (DTAC) AP (NEW)' --Users' Request --UNION OK
        --'Cost Manager (Actual Cost Worker)'  --there're 6 org running  --OK
        --, 'CTB Paylink Cheque'  --Users' requests --UN.ION OK
        --'Payables Transfer to General Ledger (HO)'  --OK All
        --'Payables Transfer to General Ledger (PSL)'
        --'Payables Transfer to General Ledger (KKC)'
        --'Payables Transfer to General Ledger (SRC)'
        --'Payables Transfer to General Ledger (SRT)'
        --, 'Program - Import Journals'  --UNION OK
        --,'JA Thai Vendor WHT Certificate Report for Generate WHT Number (DTAC) AP (NEW)' 
        --, 'CTB Paylink Cheque' --Users' Requests
        --, 'CTB Media Clearing'                                                                                  --Users' Requests
        , 'Workflow Background Process'
        --, 'Journal Import'
        )
      OR DECODE (FND_CONCURRENT_REQUESTS.DESCRIPTION, NULL, FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME, FND_CONCURRENT_REQUESTS.DESCRIPTION
        || ' ('
        || FND_CONCURRENT_PROGRAMS_TL.USER_CONCURRENT_PROGRAM_NAME
        || ')' ) LIKE 'TAC : Import POS%' )
      )
    WHERE 0     =0
    AND max_con = REQUEST_ID
      --AND FND_CONCURRENT_REQUESTS.RESPONSIBILITY_APPLICATION_ID = AP.APPLICATION_ID(+)
      -- and FND_CONCURRENT_REQUESTS.DESCRIPTION is not null
      ;
    CON CONCURRENT%ROWTYPE;
    SUC NUMBER := 0;
    ERR NUMBER := 0;
    WRN NUMBER := 0;
    CNT NUMBER := 1;
    /* Mailing Details */
    v_From VARCHAR2(80) := 'ERPOperationSupport@dtac.co.th'; --'EnterpriseApplicationSupportBOSDept@dtac.co.th';
    v_Recipient VARCHAR2(80) := 'ERPOperationSupport@dtac.co.th';
    --v_Recipient  VARCHAR2(80)  := 'tantida.yodhasmutr@dtac.co.th';
    v_Subject_E  VARCHAR2(100) := 'Monitor Interface ERP of '||SYSDATE||' : ERROR';
    v_Subject_S  VARCHAR2(100) := 'Monitor Interface ERP of '||SYSDATE||' : SUCCESS';
    v_Subject_W  VARCHAR2(100) := 'Monitor Interface ERP of '||SYSDATE||' : WARNING';
    v_Subject_EW VARCHAR2(100) := 'Monitor Interface ERP of '||SYSDATE||' : ERROR/WARNING';
    v_Mail_Host  VARCHAR2(30)  := 'mail-gw.tac.co.th';
    -- v_Cc1    VARCHAR2(100) := '';
    -- v_Cc2    VARCHAR2(100) := '';
    --v_Cc2    VARCHAR2(100) := 'IS-ERP-Production@dtac.co.th';
    --v_Cc3    VARCHAR2(100) := 'IS-ERP-Financial@dtac.co.th';
    v_Mail_Conn utl_smtp.Connection;
    crlf      VARCHAR2(2)   := CHR(10); --chr(13)||chr(10);
    v_oper    VARCHAR2(100) := 'ERP Operation Support Team';
    p_message VARCHAR2(30000);
    s_subject VARCHAR2(30000);
  BEGIN
    v_Mail_Conn := utl_smtp.Open_Connection(v_Mail_Host, 25);
    utl_smtp.Helo(v_Mail_Conn, v_Mail_Host);
    utl_smtp.Mail(v_Mail_Conn, v_From);
    utl_smtp.Rcpt(v_Mail_Conn, v_Recipient);
    UTL_SMTP.open_data(v_Mail_Conn);
    p_message := UTL_TCP.crlf || 'I---------------------------------------------------------------I' || UTL_TCP.crlf ;
    p_message := p_message || ' Concurrent Monitoring Status ' || UTL_TCP.crlf;
    p_message := p_message || 'I---------------------------------------------------------------I' || UTL_TCP.crlf;
    fnd_file.put_line(fnd_file.log,'I---------------------------------------------------------------I');
    fnd_file.put_line(fnd_file.log,' Concurrent Monitoring Status');
    fnd_file.put_line(fnd_file.log,'I---------------------------------------------------------------I');
    OPEN CONCURRENT;
    LOOP
      FETCH CONCURRENT INTO CON;
      EXIT
    WHEN CONCURRENT%NOTFOUND;
      p_message  := p_message || crlf || ' ';
      IF CON.CODE = 0 THEN
        FND_FILE.PUT_LINE(FND_FILE.LOG, CNT ||' SUCCESS : '||CON.REQUEST_ID||' - '||CON.CONCURRENT_PROGRAM_NAME||' <'||CON.RESPONSIBILITY_NAME||'>' );
        p_message   := p_message || CNT || ' SUCCESS : '||CON.REQUEST_ID||' - '||CON.CONCURRENT_PROGRAM_NAME||' <'||CON.RESPONSIBILITY_NAME||'>';
        SUC         := SUC + 1;
      ELSIF CON.CODE = 1 THEN
        FND_FILE.PUT_LINE(FND_FILE.LOG, ' **ERROR : '||CON.REQUEST_ID||' - '||CON.CONCURRENT_PROGRAM_NAME||' <'||CON.RESPONSIBILITY_NAME||'>' );
        p_message   := p_message || CNT || ' **ERROR : '||CON.REQUEST_ID||' - '||CON.CONCURRENT_PROGRAM_NAME||' <'||CON.RESPONSIBILITY_NAME||'>';
        ERR         := ERR + 1;
      ELSIF CON.CODE = 99 THEN
        FND_FILE.PUT_LINE(FND_FILE.LOG, ' WARNING : '||CON.REQUEST_ID||' - '||CON.CONCURRENT_PROGRAM_NAME||' <'||CON.RESPONSIBILITY_NAME||'>' );
        p_message := p_message || CNT || ' WARNING : '||CON.REQUEST_ID||' - '||CON.CONCURRENT_PROGRAM_NAME||' <'||CON.RESPONSIBILITY_NAME||'>';
        WRN       := WRN + 1;
      END IF;
      CNT := CNT +1;
    END LOOP;
    IF SUC + ERR > 0 THEN
      --FND_FILE.PUT_LINE(FND_FILE.LOG,CHR(10)||' '||SUC||' Concurrent requests have been completed successfully.');
      FND_FILE.PUT_LINE(FND_FILE.LOG,CHR(10)||' '||ERR||' Concurrent requests have been completed with ERROR.'||CHR(10));
      IF WRN > 0 THEN
        FND_FILE.PUT_LINE(FND_FILE.LOG,CHR(10)||' '||WRN||' Concurrent requests have been completed with WARNING.'||CHR(10));
      END IF;
    ELSE
      FND_FILE.PUT_LINE(FND_FILE.LOG,CHR(10)||' No concurrent requests have beed found.'||CHR(10));
    END IF;
    CLOSE CONCURRENT;
    UTL_SMTP.write_data(v_Mail_Conn, 'Date: ' || TO_CHAR(FROM_TZ(CAST(SYSDATE AS TIMESTAMP), 'Asia/Bangkok'),'Dy, DD Mon YYYY hh24:mi:ss TZHTZM','NLS_DATE_LANGUAGE=AMERICAN') || UTL_TCP.crlf);
    UTL_SMTP.write_data(v_Mail_Conn, 'To: ' || v_Recipient || UTL_TCP.crlf);
    UTL_SMTP.write_data(v_Mail_Conn, 'From: ' || v_From || UTL_TCP.crlf);
    IF ERR > 0 AND WRN = 0 THEN
      BEGIN
        UTL_SMTP.write_data(v_Mail_Conn, 'Subject: ' || v_Subject_E || UTL_TCP.crlf);
        UTL_SMTP.write_data(v_Mail_Conn, crlf || crlf || 'Dear Operation Support,'|| crlf || crlf || '      TAC CONCURRENT MONITOR has finished succesfully with ERROR. Please check the log of concurrent request: '|| FND_GLOBAL.CONC_REQUEST_ID ||'-TAC CONCURRENT MONITOR DETAIL MAIL for futher details.'|| crlf || crlf || 'ERP Oracle Applications :-'|| UTL_TCP.crlf || 'URL: http://cygnus3.tac.co.th:8000/OA_HTML/OA.jsp?OAFunc=OAHOMEPAGE' || UTL_TCP.crlf);
        UTL_SMTP.write_data(v_Mail_Conn, p_message || UTL_TCP.crlf || UTL_TCP.crlf || UTL_TCP.crlf);
        UTL_SMTP.write_data(v_Mail_Conn, TO_CHAR(ERR) || ' Concurrent requests have been completed with ERROR.' || UTL_TCP.crlf || UTL_TCP.crlf);
      END;
    ELSIF ERR > 0 AND WRN > 0 THEN
      BEGIN
        UTL_SMTP.write_data(v_Mail_Conn, 'Subject: ' || v_Subject_EW || UTL_TCP.crlf);
        UTL_SMTP.write_data(v_Mail_Conn, crlf || crlf ||'Dear Operation Support,'|| crlf || crlf || '      TAC CONCURRENT MONITOR has finished succesfully with ERROR/WARNING. Please check the log of concurrent request: '|| FND_GLOBAL.CONC_REQUEST_ID ||'-TAC CONCURRENT MONITOR DETAIL MAIL for futher details.'|| crlf || crlf || 'ERP Oracle Applications :-'|| UTL_TCP.crlf || 'URL: http://cygnus3.tac.co.th:8000/OA_HTML/OA.jsp?OAFunc=OAHOMEPAGE' || UTL_TCP.crlf);
        UTL_SMTP.write_data(v_Mail_Conn, p_message || UTL_TCP.crlf || UTL_TCP.crlf || UTL_TCP.crlf);
        UTL_SMTP.write_data(v_Mail_Conn, TO_CHAR(ERR) || ' Concurrent requests have been completed with ERROR.' || UTL_TCP.crlf);
        UTL_SMTP.write_data(v_Mail_Conn, TO_CHAR(WRN) || ' Concurrent requests have been completed with WARNING.' || UTL_TCP.crlf || UTL_TCP.crlf );
      END;
    ELSIF ERR = 0 AND WRN > 0 THEN
      BEGIN
        UTL_SMTP.write_data(v_Mail_Conn, 'Subject: ' || v_Subject_W || UTL_TCP.crlf);
        UTL_SMTP.write_data(v_Mail_Conn, crlf || crlf ||'Dear Operation Support,'|| crlf || crlf || '      TAC CONCURRENT MONITOR has finished succesfully with WARNING. Please check the log of concurrent request: '|| FND_GLOBAL.CONC_REQUEST_ID ||'-TAC CONCURRENT MONITOR DETAIL MAIL for futher details.'|| crlf || crlf || 'ERP Oracle Applications :-'|| UTL_TCP.crlf || 'URL: http://cygnus3.tac.co.th:8000/OA_HTML/OA.jsp?OAFunc=OAHOMEPAGE' || UTL_TCP.crlf);
        UTL_SMTP.write_data(v_Mail_Conn, p_message || UTL_TCP.crlf || UTL_TCP.crlf || UTL_TCP.crlf);
        UTL_SMTP.write_data(v_Mail_Conn, TO_CHAR(WRN) || ' Concurrent requests have been completed with WARNING.' || UTL_TCP.crlf || UTL_TCP.crlf );
      END;
    ELSIF ERR = 0 AND WRN = 0 THEN
      BEGIN
        UTL_SMTP.write_data(v_Mail_Conn, 'Subject: ' || v_Subject_S || UTL_TCP.crlf);
        UTL_SMTP.write_data(v_Mail_Conn, crlf || crlf || 'Dear Operation Support,'|| crlf || crlf || '      TAC CONCURRENT MONITOR has finished succesfully with no error. Please check the log of concurrent request: '|| FND_GLOBAL.CONC_REQUEST_ID ||'-TAC CONCURRENT MONITOR DETAIL MAIL for futher details.'|| crlf || crlf || 'ERP Oracle Applications :-'|| UTL_TCP.crlf || 'URL: http://cygnus3.tac.co.th:8000/OA_HTML/OA.jsp?OAFunc=OAHOMEPAGE' || UTL_TCP.crlf);
        UTL_SMTP.write_data(v_Mail_Conn, p_message || UTL_TCP.crlf || UTL_TCP.crlf || UTL_TCP.crlf);
      END;
    END IF;
    UTL_SMTP.write_data(v_Mail_Conn, 'Request ID : ' || FND_GLOBAL.CONC_REQUEST_ID);
    UTL_SMTP.write_data(v_Mail_Conn, crlf || crlf || crlf || 'Best Regards,'|| crlf || crlf || v_oper|| crlf || crlf || 'Date: ' || TO_CHAR(sysdate, 'Dy, DD Mon YYYY hh24:mi:ss'));
    UTL_SMTP.close_data(v_Mail_Conn);
    UTL_SMTP.quit(v_Mail_Conn);
    
    EXCEPTION
    WHEN utl_smtp.Transient_Error OR utl_smtp.Permanent_Error THEN
    raise_application_error(-20000, 'Unable to send mail: '||sqlerrm);
    
  END;