CREATE OR REPLACE PROCEDURE TAC_CONCURRENT_MONITOR(
    errbuf OUT VARCHAR2,
    retcode OUT NUMBER)
AS
  CURSOR CONCURRENT
  IS
    SELECT (
      CASE
        WHEN REPLACE(L2.MEANING, ' ', '') = 'Normal'
          /*AND R.COMPLETION_TEXT = 'Normal completion'*/
        THEN 0
        WHEN REPLACE(L2.MEANING, ' ', '') = 'Warning'
        AND R.COMPLETION_TEXT             = 'No data was found in the GL_INTERFACE table.'
        THEN 0
        WHEN REPLACE(L2.MEANING, ' ', '') = 'Warning'
        AND DECODE (R.DESCRIPTION, NULL, PT.USER_CONCURRENT_PROGRAM_NAME, R.DESCRIPTION
          || ' ('
          || PT.USER_CONCURRENT_PROGRAM_NAME
          || ')') = 'Pick Selection List Generation - SRS'
        THEN 99
        ELSE 1
      END) CODE ,
      R.REQUEST_ID ,
      DECODE (R.DESCRIPTION, NULL, PT.USER_CONCURRENT_PROGRAM_NAME, R.DESCRIPTION
      || ' ('
      || PT.USER_CONCURRENT_PROGRAM_NAME
      || ')' ) CONCURRENT_PROGRAM_NAME ,
      APP.APPLICATION_SHORT_NAME,
      APP.APPLICATION_NAME ,
      RES.RESPONSIBILITY_NAME ,
      (
      CASE
        WHEN PT.USER_CONCURRENT_PROGRAM_NAME = 'Transfer transactions to GL'
        THEN
          (SELECT NAME
          FROM HR_ALL_ORGANIZATION_UNITS
          WHERE ORGANIZATION_ID = SUBSTR(R.ARGUMENT_TEXT,0,3)
          )
      WHEN PT.USER_CONCURRENT_PROGRAM_NAME = 'Actual Cost Worker'
      THEN
        (SELECT NAME
        FROM HR_ALL_ORGANIZATION_UNITS
        WHERE ORGANIZATION_ID = SUBSTR(SUBSTR(R.ARGUMENT_TEXT,INSTR(R.ARGUMENT_TEXT,',')+2),0,3)
        )
    END) ORGANIZATION ,
      (
      CASE
        WHEN PT.USER_CONCURRENT_PROGRAM_NAME = 'Program - Automatic Posting'
        AND SUBSTR(R.ARGUMENT_TEXT,-1,1)     = 1
        THEN 'Encumbrance Posting'
        WHEN PT.USER_CONCURRENT_PROGRAM_NAME = 'Program - Automatic Posting'
        AND SUBSTR(R.ARGUMENT_TEXT,-1,1)     = 2
        THEN 'Submodule Posting'
        ELSE R.ARGUMENT_TEXT
      END) PARAMETERS --, R.ARGUMENT_TEXT PARAMETERS
      ,
      R.PRIORITY_REQUEST_ID ,
      (
      CASE
        WHEN R.REQUEST_ID = R.PRIORITY_REQUEST_ID
        THEN NULL
        ELSE DECODE (R2.DESCRIPTION, NULL, PT2.USER_CONCURRENT_PROGRAM_NAME, R2.DESCRIPTION
          || ' ('
          || PT2.USER_CONCURRENT_PROGRAM_NAME
          || ')')
      END) PRIORITY_REQUEST_NAME ,
      R.PRIORITY ,
      NULL TYPE ,
      (R.RESUBMIT_INTERVAL
      ||' '
      ||R.RESUBMIT_INTERVAL_UNIT_CODE) FREQUENCY ,
      U.USER_NAME REQUESTED_BY ,
      R.REQUESTED_START_DATE START_AT ,
      R.ACTUAL_COMPLETION_DATE END_AT ,
      PT.DESCRIPTION ACTIVITY ,
      L1.MEANING
      ||' '
      ||REPLACE(L2.MEANING, ' ', '') STATUS ,
      R.COMPLETION_TEXT
    FROM FND_CONCURRENT_PROGRAMS_TL PT ,
      FND_USER U ,
      FND_CONCURRENT_REQUESTS R ,
      FND_LOOKUPS L1 ,
      FND_LOOKUPS L2 ,
      FND_APPLICATION_VL APP ,
      FND_CONCURRENT_PROGRAMS_TL PT2 ,
      FND_CONCURRENT_REQUESTS R2 ,
      FND_RESPONSIBILITY_TL RES
    WHERE 1                             = 1
    AND R.PRIORITY_REQUEST_ID           = R2.REQUEST_ID (+)
    AND R.RESPONSIBILITY_ID             = RES.RESPONSIBILITY_ID
    AND R.RESPONSIBILITY_APPLICATION_ID = APP.APPLICATION_ID
    AND R.CONCURRENT_PROGRAM_ID         = PT.CONCURRENT_PROGRAM_ID (+)
    AND R.PROGRAM_APPLICATION_ID        = PT.APPLICATION_ID (+)
    AND PT.LANGUAGE                     = USERENV ('LANG')
    AND R2.CONCURRENT_PROGRAM_ID        = PT2.CONCURRENT_PROGRAM_ID (+)
    AND R2.PROGRAM_APPLICATION_ID       = PT2.APPLICATION_ID (+)
    AND PT2.LANGUAGE                    = USERENV ('LANG')
    AND R.REQUESTED_BY                  = U.USER_ID
    AND L1.LOOKUP_TYPE                  = 'CP_PHASE_CODE'
    AND L1.LOOKUP_CODE                  = R.PHASE_CODE
    AND L2.LOOKUP_TYPE                  = 'CP_STATUS_CODE'
    AND L2.LOOKUP_CODE                  = R.STATUS_CODE
    AND U.USER_NAME                    IN ('DTAC','SYSADMIN')
    AND R.REQUESTED_START_DATE          = NVL(
      (SELECT MAX(REQUESTED_START_DATE)
      FROM FND_CONCURRENT_REQUESTS
      WHERE CONCURRENT_PROGRAM_ID       = R.CONCURRENT_PROGRAM_ID
      AND RESPONSIBILITY_APPLICATION_ID = R.RESPONSIBILITY_APPLICATION_ID
      AND RESPONSIBILITY_ID             = R.RESPONSIBILITY_ID
        --AND ARGUMENT_TEXT = R.ARGUMENT_TEXT  --Added for Workflow Background Process
      AND REQUESTED_START_DATE BETWEEN SYSDATE -15 AND SYSDATE - 1/1440
      ),
      (SELECT MIN(REQUESTED_START_DATE)
      FROM FND_CONCURRENT_REQUESTS
      WHERE CONCURRENT_PROGRAM_ID       = R.CONCURRENT_PROGRAM_ID
      AND RESPONSIBILITY_APPLICATION_ID = R.RESPONSIBILITY_APPLICATION_ID
      AND RESPONSIBILITY_ID             = R.RESPONSIBILITY_ID
        --AND ARGUMENT_TEXT = R.ARGUMENT_TEXT  --Added for Workflow Background Process
      AND REQUESTED_START_DATE > SYSDATE
      ) )
    AND (DECODE (R.DESCRIPTION, NULL, PT.USER_CONCURRENT_PROGRAM_NAME , R.DESCRIPTION
      || ' ('
      || PT.USER_CONCURRENT_PROGRAM_NAME
      || ')') IN ('TAC Organization Chart Interface Version 4'                --OK
      , 'TAC Organization Clean Data'                                         --OK
      , 'Receiving Transaction Processor'                                     --OK
      , 'Order Import'                                                        --There're 5 Order Import from 5 different OU of TAC : Import POS internal order --OK
      , 'TAC : Import POS internal order (Not Auto Release) HO (Report Set)'  --OK
      , 'TAC : Import POS internal order (Not Auto Release) SRC (Report Set)' --OK
      , 'TAC : Import POS internal order (Not Auto Release) PSL (Report Set)' --OK
      , 'TAC : Import POS internal order (Not Auto Release) KKC (Report Set)' --OK
      , 'TAC : Import POS internal order (Not Auto Release) SRT (Report Set)' --OK
      -- , 'Payables Accounting Process'  --C
      , 'General Ledger Transfer Program' --OK
      --, 'TAC_EREFILL Autoinvoice Master Program (Report Set)'  --C
      --, 'TAC_OTHER Autoinvoice Master Program (Report Set)'  --C
      --, 'TAC_PREMIUM Autoinvoice Master Program (Report Set)'  --C
      --, 'TAC_SIMCARD Autoinvoice Master Program (Report Set)'  ---C
      --, 'TAC_VOUCHER Autoinvoice Master Program (Report Set)'  --C
      , 'TAC Allocate / De-Allocate data from ERP'                  --OK
      , 'TAC UD and Voucher Supplier Stock Balance Update'          --OK
      , 'TAC Update SIM Activate Date in Serial Number Master Data' --OK
      , 'TAC Pre-Activate SIM Interface'                            --OK
      --, 'Transfer transactions to GL' --12 Org  --OK
      , 'Process transaction interface' --2 duplicated transactios (same time same parameters). should one be deleted??
      -- , 'Program - Create Journals'  --C
      , 'Program - Maintain Summary Templates'   --OK
      , 'Program - Maintain Budget Organization' --OK
      , 'Program - Automatic Posting'            --'Program - Automatic Posting (Sub ledger Posting)'  --Submodule Posting = 2  --OK
      --'Program - Automatic Posting  (Encumbrance Posting)' --Encumbrance Posting = 1  --OK
      , 'TAC Account Combination Interface'            --OK
      , 'TAC Journal Sources & Categories Interface'   --OK
      , 'PRC: Maintain Budgetary Control Balances'     --OK
      , 'PRC: Update Project Summary Amounts'          --OK
      , 'PRC: Interface Expense Reports from Payables' --OK
      , 'Import Shipping Locations'                    --OK
      , 'Margin Analysis Load Run'                     --OK
      , 'TAC Synchronize username changed from OID'    --OK
      , 'TAC Export Sales Data from ERP to Peugeot'    --OK
      --, 'Workflow Background Process'      --UNION        --Missing 2
      --, 'Workflow Background Process (PA Budget Integration Workflow)'   --OK
      --, 'Workflow Background Process (OM Order Line)'       --OK
      --, 'Workflow Background Process (Expenses:::Yes:Yes:Yes)'     --OK
      --, 'Workflow Background Process (PO Requisition Approval:::Yes:No:)'  --OK
      --, 'Workflow Background Process (PO Requisition Approval:::Yes:Yes:)'  --OK
      --, 'Workflow Background Process (PO Approval:::Yes:Yes:)'     --OK
      --, 'Workflow Background Process (PO Approval:::Yes:No:)'     --Missing
      --, 'Workflow Background Process (PO Approval:::No:Yes:)'     --Missing
      , 'Purge Obsolete Workflow Runtime Data' --OK
      , 'Pick Selection List Generation - SRS' --OK
      -- , 'TAC AP Supplier Interface Program'  --C
      --, 'JA Thai Vendor WHT Certificate Report for Generate WHT Number (DTAC) AP (NEW)' --Users' Request --UNION OK
      , 'Actual Cost Worker'            --'Cost Manager (Actual Cost Worker)'  --there're 6 org running  --OK
      , 'PRC: Interface Supplier Costs' --OK
      --, 'CTB Paylink Cheque'  --Users' requests --UNION OK
      --, 'CTB Media Clearing'  --Users' requests --UNION OK
      , 'Payables Transfer to General Ledger' --'Payables Transfer to General Ledger (HO)'  --OK All
      --'Payables Transfer to General Ledger (PSL)'
      --'Payables Transfer to General Ledger (KKC)'
      --'Payables Transfer to General Ledger (SRC)'
      --'Payables Transfer to General Ledger (SRT)'
      --, 'Program - Import Journals'  --UNION OK
      )
      --OR (PT.USER_CONCURRENT_PROGRAM_NAME = 'Journal Import' AND R.ARGUMENT4 IS NULL)  --OK All 20110516 - comment out
      --Journal Import (Source: Purchasing)  --DTAC
      --Journal Import (Source: Receivables)  --DTAC
      --Journal Import (Source: Other)   --DTAC
      --Journal Import (Source: Purchasing DTAC-N) --PATUMWADEE
      --OR PT.USER_CONCURRENT_PROGRAM_NAME = 'Order Import'
      )
    --ORDER BY NVL(R.DESCRIPTION,PT.USER_CONCURRENT_PROGRAM_NAME), R.PRIORITY_REQUEST_ID
    UNION ALL --Only 'Workflow Background Process'
    SELECT (
      CASE
        WHEN REPLACE(L2.MEANING, ' ', '') = 'Normal'
          /*AND R.COMPLETION_TEXT = 'Normal completion'*/
        THEN 0
        WHEN REPLACE(L2.MEANING, ' ', '') = 'Warning'
        AND R.COMPLETION_TEXT             = 'No data was found in the GL_INTERFACE table.'
        THEN 0
        ELSE 1
      END) CODE ,
      R.REQUEST_ID ,
      DECODE (R.DESCRIPTION, NULL, PT.USER_CONCURRENT_PROGRAM_NAME, R.DESCRIPTION
      || ' ('
      || PT.USER_CONCURRENT_PROGRAM_NAME
      || ')' ) CONCURRENT_PROGRAM_NAME ,
      APP.APPLICATION_SHORT_NAME,
      APP.APPLICATION_NAME ,
      RES.RESPONSIBILITY_NAME ,
      NULL ORGANIZATION ,
      R.ARGUMENT_TEXT PARAMETERS ,
      R.PRIORITY_REQUEST_ID ,
      (
      CASE
        WHEN R.REQUEST_ID = R.PRIORITY_REQUEST_ID
        THEN NULL
        ELSE DECODE (R2.DESCRIPTION, NULL, PT2.USER_CONCURRENT_PROGRAM_NAME, R2.DESCRIPTION
          || ' ('
          || PT2.USER_CONCURRENT_PROGRAM_NAME
          || ')')
      END) PRIORITY_REQUEST_NAME ,
      R.PRIORITY ,
      NULL TYPE ,
      (R.RESUBMIT_INTERVAL
      ||' '
      ||R.RESUBMIT_INTERVAL_UNIT_CODE) FREQUENCY ,
      U.USER_NAME REQUESTED_BY ,
      R.REQUESTED_START_DATE START_AT ,
      R.ACTUAL_COMPLETION_DATE END_AT ,
      PT.DESCRIPTION ACTIVITY ,
      L1.MEANING
      ||' '
      ||REPLACE(L2.MEANING, ' ', '') STATUS ,
      R.COMPLETION_TEXT
    FROM FND_CONCURRENT_PROGRAMS_TL PT ,
      FND_USER U ,
      FND_CONCURRENT_REQUESTS R ,
      FND_LOOKUPS L1 ,
      FND_LOOKUPS L2 ,
      FND_APPLICATION_VL APP ,
      FND_CONCURRENT_PROGRAMS_TL PT2 ,
      FND_CONCURRENT_REQUESTS R2 ,
      FND_RESPONSIBILITY_TL RES
    WHERE 1                             = 1
    AND R.PRIORITY_REQUEST_ID           = R2.REQUEST_ID (+)
    AND R.RESPONSIBILITY_ID             = RES.RESPONSIBILITY_ID
    AND R.RESPONSIBILITY_APPLICATION_ID = APP.APPLICATION_ID
    AND R.CONCURRENT_PROGRAM_ID         = PT.CONCURRENT_PROGRAM_ID (+)
    AND R.PROGRAM_APPLICATION_ID        = PT.APPLICATION_ID (+)
    AND PT.LANGUAGE                     = USERENV ('LANG')
    AND R2.CONCURRENT_PROGRAM_ID        = PT2.CONCURRENT_PROGRAM_ID (+)
    AND R2.PROGRAM_APPLICATION_ID       = PT2.APPLICATION_ID (+)
    AND PT2.LANGUAGE                    = USERENV ('LANG')
    AND R.REQUESTED_BY                  = U.USER_ID
    AND L1.LOOKUP_TYPE                  = 'CP_PHASE_CODE'
    AND L1.LOOKUP_CODE                  = R.PHASE_CODE
    AND L2.LOOKUP_TYPE                  = 'CP_STATUS_CODE'
    AND L2.LOOKUP_CODE                  = R.STATUS_CODE
    AND U.USER_NAME                    IN ('DTAC','SYSADMIN')
    AND R.REQUESTED_START_DATE          = NVL(
      (SELECT MAX(REQUESTED_START_DATE)
      FROM FND_CONCURRENT_REQUESTS
      WHERE CONCURRENT_PROGRAM_ID       = R.CONCURRENT_PROGRAM_ID
      AND RESPONSIBILITY_APPLICATION_ID = R.RESPONSIBILITY_APPLICATION_ID
      AND RESPONSIBILITY_ID             = R.RESPONSIBILITY_ID
      AND ARGUMENT_TEXT                 = R.ARGUMENT_TEXT --Added for Workflow Background Process
      AND REQUESTED_START_DATE BETWEEN SYSDATE -15 AND SYSDATE - 1/1440
      ),
      (SELECT MIN(REQUESTED_START_DATE)
      FROM FND_CONCURRENT_REQUESTS
      WHERE CONCURRENT_PROGRAM_ID       = R.CONCURRENT_PROGRAM_ID
      AND RESPONSIBILITY_APPLICATION_ID = R.RESPONSIBILITY_APPLICATION_ID
      AND RESPONSIBILITY_ID             = R.RESPONSIBILITY_ID
      AND ARGUMENT_TEXT                 = R.ARGUMENT_TEXT --Added for Workflow Background Process
      AND REQUESTED_START_DATE          > SYSDATE
      ) )
    AND (DECODE (R.DESCRIPTION, NULL, PT.USER_CONCURRENT_PROGRAM_NAME , R.DESCRIPTION
      || ' ('
      || PT.USER_CONCURRENT_PROGRAM_NAME
      || ')') IN ('Workflow Background Process' --Missing 2
      --, 'Workflow Background Process (PA Budget Integration Workflow)'   --OK
      --, 'Workflow Background Process (OM Order Line)'       --OK
      --, 'Workflow Background Process (Expenses:::Yes:Yes:Yes)'     --OK
      --, 'Workflow Background Process (PO Requisition Approval:::Yes:No:)'  --OK
      --, 'Workflow Background Process (PO Requisition Approval:::Yes:Yes:)'  --OK
      --, 'Workflow Background Process (PO Approval:::Yes:Yes:)'     --OK
      --, 'Workflow Background Process (PO Approval:::Yes:No:)'     --Missing
      --, 'Workflow Background Process (PO Approval:::No:Yes:)'     --Missing
      ))
    AND R.REQUEST_ID = R.PRIORITY_REQUEST_ID
    UNION ALL --For 'JA Thai Vendor WHT Certificate Report for Generate WHT Number (DTAC) AP (NEW)'  --Users' Requests --Related to a Problem
    --'CTB Paylink Cheque'  --Users' Requests
    --'CTB Media Clearing'  --Users' Requests
    SELECT (
      CASE
        WHEN REPLACE(L2.MEANING, ' ', '') = 'Normal'
          /*AND R.COMPLETION_TEXT = 'Normal completion'*/
        THEN 0
        WHEN REPLACE(L2.MEANING, ' ', '') = 'Warning'
        AND R.COMPLETION_TEXT             = 'No data was found in the GL_INTERFACE table.'
        THEN 0
        WHEN REPLACE(L2.MEANING, ' ', '') = 'Error'
        AND DECODE (R.DESCRIPTION, NULL, PT.USER_CONCURRENT_PROGRAM_NAME, R.DESCRIPTION
          || ' ('
          || PT.USER_CONCURRENT_PROGRAM_NAME
          || ')') IN ('CTB Paylink Cheque','CTB Media Clearing')
        THEN 99
        ELSE 1
      END) CODE ,
      R.REQUEST_ID ,
      DECODE (R.DESCRIPTION, NULL, PT.USER_CONCURRENT_PROGRAM_NAME, R.DESCRIPTION
      || ' ('
      || PT.USER_CONCURRENT_PROGRAM_NAME
      || ')' ) CONCURRENT_PROGRAM_NAME ,
      APP.APPLICATION_SHORT_NAME,
      APP.APPLICATION_NAME ,
      RES.RESPONSIBILITY_NAME ,
      NULL ORGANIZATION ,
      R.ARGUMENT_TEXT PARAMETERS ,
      R.PRIORITY_REQUEST_ID ,
      (
      CASE
        WHEN R.REQUEST_ID = R.PRIORITY_REQUEST_ID
        THEN NULL
        ELSE DECODE (R2.DESCRIPTION, NULL, PT2.USER_CONCURRENT_PROGRAM_NAME, R2.DESCRIPTION
          || ' ('
          || PT2.USER_CONCURRENT_PROGRAM_NAME
          || ')')
      END) PRIORITY_REQUEST_NAME ,
      R.PRIORITY ,
      NULL TYPE ,
      (R.RESUBMIT_INTERVAL
      ||' '
      ||R.RESUBMIT_INTERVAL_UNIT_CODE) FREQUENCY ,
      U.USER_NAME REQUESTED_BY ,
      R.REQUESTED_START_DATE START_AT ,
      R.ACTUAL_COMPLETION_DATE END_AT ,
      PT.DESCRIPTION ACTIVITY ,
      L1.MEANING
      ||' '
      ||REPLACE(L2.MEANING, ' ', '') STATUS ,
      R.COMPLETION_TEXT
    FROM FND_CONCURRENT_PROGRAMS_TL PT ,
      FND_USER U ,
      FND_CONCURRENT_REQUESTS R ,
      FND_LOOKUPS L1 ,
      FND_LOOKUPS L2 ,
      FND_APPLICATION_VL APP ,
      FND_CONCURRENT_PROGRAMS_TL PT2 ,
      FND_CONCURRENT_REQUESTS R2 ,
      FND_RESPONSIBILITY_TL RES
    WHERE 1                             = 1
    AND R.PRIORITY_REQUEST_ID           = R2.REQUEST_ID (+)
    AND R.RESPONSIBILITY_ID             = RES.RESPONSIBILITY_ID
    AND R.RESPONSIBILITY_APPLICATION_ID = APP.APPLICATION_ID
    AND R.CONCURRENT_PROGRAM_ID         = PT.CONCURRENT_PROGRAM_ID (+)
    AND R.PROGRAM_APPLICATION_ID        = PT.APPLICATION_ID (+)
    AND PT.LANGUAGE                     = USERENV ('LANG')
    AND R2.CONCURRENT_PROGRAM_ID        = PT2.CONCURRENT_PROGRAM_ID (+)
    AND R2.PROGRAM_APPLICATION_ID       = PT2.APPLICATION_ID (+)
    AND PT2.LANGUAGE                    = USERENV ('LANG')
    AND U.USER_ID                       = R.REQUESTED_BY
    AND L1.LOOKUP_TYPE                  = 'CP_PHASE_CODE'
    AND L1.LOOKUP_CODE                  = R.PHASE_CODE
    AND L2.LOOKUP_TYPE                  = 'CP_STATUS_CODE'
    AND L2.LOOKUP_CODE                  = R.STATUS_CODE
      --AND U.USER_NAME IN ('DTAC','SYSADMIN','PATUMWADEE', 'SIHTHADS')
    AND R.REQUESTED_START_DATE = NVL(
      (SELECT MAX(REQUESTED_START_DATE)
      FROM FND_CONCURRENT_REQUESTS
      WHERE CONCURRENT_PROGRAM_ID       = R.CONCURRENT_PROGRAM_ID
      AND RESPONSIBILITY_APPLICATION_ID = R.RESPONSIBILITY_APPLICATION_ID
      AND RESPONSIBILITY_ID             = R.RESPONSIBILITY_ID
        --AND ARGUMENT_TEXT = R.ARGUMENT_TEXT  --Added for Workflow Background Process
      AND REQUESTED_START_DATE BETWEEN SYSDATE -15 AND SYSDATE - 1/1440
      ),
      (SELECT MIN(REQUESTED_START_DATE)
      FROM FND_CONCURRENT_REQUESTS
      WHERE CONCURRENT_PROGRAM_ID       = R.CONCURRENT_PROGRAM_ID
      AND RESPONSIBILITY_APPLICATION_ID = R.RESPONSIBILITY_APPLICATION_ID
      AND RESPONSIBILITY_ID             = R.RESPONSIBILITY_ID
        --AND ARGUMENT_TEXT = R.ARGUMENT_TEXT  --Added for Workflow Background Process
      AND REQUESTED_START_DATE > SYSDATE
      ) )
    AND (DECODE (R.DESCRIPTION, NULL, PT.USER_CONCURRENT_PROGRAM_NAME , R.DESCRIPTION
      || ' ('
      || PT.USER_CONCURRENT_PROGRAM_NAME
      || ')') IN ('JA Thai Vendor WHT Certificate Report for Generate WHT Number (DTAC) AP (NEW)' --Users' Requests
      , 'CTB Paylink Cheque'                                                                      --Users' Requests
      , 'CTB Media Clearing'                                                                      --Users' Requests
      ) )
    UNION ALL /*--For Program - Import Journals*/
    SELECT (
      CASE
        WHEN REPLACE(L2.MEANING, ' ', '') = 'Normal'
          /*AND R.COMPLETION_TEXT = 'Normal completion'*/
        THEN 0
        WHEN REPLACE(L2.MEANING, ' ', '') = 'Warning'
        AND R.COMPLETION_TEXT             = 'No data was found in the GL_INTERFACE table.'
        THEN 0
        WHEN REPLACE(L2.MEANING, ' ', '') = 'Warning'
        AND R.COMPLETION_TEXT             = 'Not all of your data was imported successfully or it was imported with warnings.  Please review output file.'
        THEN 99
        ELSE 1
      END) CODE ,
      R.REQUEST_ID ,
      DECODE (R.DESCRIPTION, NULL, PT.USER_CONCURRENT_PROGRAM_NAME, R.DESCRIPTION
      || ' ('
      || PT.USER_CONCURRENT_PROGRAM_NAME
      || ')' ) CONCURRENT_PROGRAM_NAME ,
      APP.APPLICATION_SHORT_NAME,
      APP.APPLICATION_NAME ,
      RES.RESPONSIBILITY_NAME ,
      NULL ORGANIZATION ,
      R.ARGUMENT_TEXT PARAMETERS ,
      R.PARENT_REQUEST_ID PRIORITY_REQUEST_ID ,
      (DECODE (R2.DESCRIPTION, NULL, PT2.USER_CONCURRENT_PROGRAM_NAME , R2.DESCRIPTION
      || ' ('
      || PT2.USER_CONCURRENT_PROGRAM_NAME
      || ')') )
      ||' ('
      ||R2.ARGUMENT_TEXT
      ||')' PRIORITY_REQUEST_NAME ,
      R.PRIORITY ,
      NULL TYPE ,
      (R.RESUBMIT_INTERVAL
      ||' '
      ||R.RESUBMIT_INTERVAL_UNIT_CODE) FREQUENCY ,
      U.USER_NAME REQUESTED_BY ,
      R.REQUESTED_START_DATE START_AT ,
      R.ACTUAL_COMPLETION_DATE END_AT ,
      PT.DESCRIPTION ACTIVITY ,
      L1.MEANING
      ||' '
      ||REPLACE(L2.MEANING, ' ', '') STATUS ,
      R.COMPLETION_TEXT
    FROM FND_CONCURRENT_PROGRAMS_TL PT ,
      FND_USER U ,
      FND_CONCURRENT_REQUESTS R ,
      FND_LOOKUPS L1 ,
      FND_LOOKUPS L2 ,
      FND_APPLICATION_VL APP ,
      FND_CONCURRENT_PROGRAMS_TL PT2 ,
      FND_CONCURRENT_REQUESTS R2 ,
      FND_RESPONSIBILITY_TL RES
    WHERE 1                             = 1
    AND R.PARENT_REQUEST_ID             = R2.REQUEST_ID (+)
    AND R.RESPONSIBILITY_ID             = RES.RESPONSIBILITY_ID
    AND R.RESPONSIBILITY_APPLICATION_ID = APP.APPLICATION_ID
    AND R.CONCURRENT_PROGRAM_ID         = PT.CONCURRENT_PROGRAM_ID (+)
    AND R.PROGRAM_APPLICATION_ID        = PT.APPLICATION_ID (+)
    AND PT.LANGUAGE                     = USERENV ('LANG')
    AND R2.CONCURRENT_PROGRAM_ID        = PT2.CONCURRENT_PROGRAM_ID (+)
    AND R2.PROGRAM_APPLICATION_ID       = PT2.APPLICATION_ID (+)
    AND PT2.LANGUAGE                    = USERENV ('LANG')
    AND R.REQUESTED_BY                  = U.USER_ID
    AND L1.LOOKUP_TYPE                  = 'CP_PHASE_CODE'
    AND L1.LOOKUP_CODE                  = R.PHASE_CODE
    AND L2.LOOKUP_TYPE                  = 'CP_STATUS_CODE'
    AND L2.LOOKUP_CODE                  = R.STATUS_CODE
      --AND U.USER_NAME IN ('DTAC','SYSADMIN','PATUMWADEE')
    AND R.REQUESTED_START_DATE BETWEEN SYSDATE -1 AND SYSDATE - 1/1440
    AND (DECODE (R.DESCRIPTION, NULL, PT.USER_CONCURRENT_PROGRAM_NAME , R.DESCRIPTION
      || ' ('
      || PT.USER_CONCURRENT_PROGRAM_NAME
      || ')')               IN ('Journal Import'))
    AND R.PARENT_REQUEST_ID IN
      (SELECT R.REQUEST_ID
      FROM FND_CONCURRENT_PROGRAMS_TL PT ,
        FND_USER U ,
        FND_CONCURRENT_REQUESTS R
        --, FND_LOOKUPS L1
        --, FND_LOOKUPS L2
        --, FND_APPLICATION_VL APP
        --, FND_CONCURRENT_PROGRAMS_TL PT2
        --, FND_CONCURRENT_REQUESTS R2
        --, FND_RESPONSIBILITY_TL RES
      WHERE 1 = 1
        --AND R.PRIORITY_REQUEST_ID = R2.REQUEST_ID (+)
        --AND R.RESPONSIBILITY_ID = RES.RESPONSIBILITY_ID
        --AND R.RESPONSIBILITY_APPLICATION_ID = APP.APPLICATION_ID
      AND R.CONCURRENT_PROGRAM_ID  = PT.CONCURRENT_PROGRAM_ID (+)
      AND R.PROGRAM_APPLICATION_ID = PT.APPLICATION_ID (+)
      AND PT.LANGUAGE              = USERENV ('LANG')
        --AND R2.CONCURRENT_PROGRAM_ID = PT2.CONCURRENT_PROGRAM_ID (+)
        --AND R2.PROGRAM_APPLICATION_ID = PT2.APPLICATION_ID (+)
        --AND PT2.LANGUAGE = USERENV ('LANG')
      AND R.REQUESTED_BY = U.USER_ID
        --AND L1.LOOKUP_TYPE = 'CP_PHASE_CODE' AND L1.LOOKUP_CODE = R.PHASE_CODE
        --AND L2.LOOKUP_TYPE = 'CP_STATUS_CODE' AND L2.LOOKUP_CODE = R.STATUS_CODE
      AND U.USER_NAME           IN ('DTAC','SYSADMIN')
      AND R.REQUESTED_START_DATE = NVL(
        (SELECT MAX(REQUESTED_START_DATE)
        FROM FND_CONCURRENT_REQUESTS
        WHERE CONCURRENT_PROGRAM_ID       = R.CONCURRENT_PROGRAM_ID
        AND RESPONSIBILITY_APPLICATION_ID = R.RESPONSIBILITY_APPLICATION_ID
        AND RESPONSIBILITY_ID             = R.RESPONSIBILITY_ID
          --AND ARGUMENT_TEXT = R.ARGUMENT_TEXT  --Added for Workflow Background Process
        AND REQUESTED_START_DATE BETWEEN SYSDATE -15 AND SYSDATE - 1/1440
        ),
        (SELECT MIN(REQUESTED_START_DATE)
        FROM FND_CONCURRENT_REQUESTS
        WHERE CONCURRENT_PROGRAM_ID       = R.CONCURRENT_PROGRAM_ID
        AND RESPONSIBILITY_APPLICATION_ID = R.RESPONSIBILITY_APPLICATION_ID
        AND RESPONSIBILITY_ID             = R.RESPONSIBILITY_ID
          --AND ARGUMENT_TEXT = R.ARGUMENT_TEXT  --Added for Workflow Background Process
        AND REQUESTED_START_DATE > SYSDATE
        ) )
      AND (DECODE (R.DESCRIPTION, NULL, PT.USER_CONCURRENT_PROGRAM_NAME , R.DESCRIPTION
        || ' ('
        || PT.USER_CONCURRENT_PROGRAM_NAME
        || ')') IN ('Program - Import Journals'))
      );
    CON CONCURRENT%ROWTYPE;
    SUC NUMBER := 0;
    ERR NUMBER := 0;
    WRN NUMBER := 0;
    /* Mailing Details */
    v_From      VARCHAR2(80)  := 'ERPOperationSupport@dtac.co.th'; --'EnterpriseApplicationSupportBOSDept@dtac.co.th';
    v_Recipient VARCHAR2(80)  := 'ERPOperationSupport@dtac.co.th';
    v_Subject   VARCHAR2(100) := 'Monitor Interface ERP of '||SYSDATE||' : ERROR';
    v_Subject0  VARCHAR2(100) := 'Monitor Interface ERP of '||SYSDATE||' : SUCCESS';
    v_Subject1  VARCHAR2(100) := 'Monitor Interface ERP of '||SYSDATE||' : WARNING';
    v_Subject2  VARCHAR2(100) := 'Monitor Interface ERP of '||SYSDATE||' : ERROR/WARNING';
    v_Mail_Host VARCHAR2(30)  := 'mail-gw.tac.co.th';
    -- v_Cc1    VARCHAR2(100) := '';
    -- v_Cc2    VARCHAR2(100) := '';
    --v_Cc2    VARCHAR2(100) := 'IS-ERP-Production@dtac.co.th';
    --v_Cc3    VARCHAR2(100) := 'IS-ERP-Financial@dtac.co.th';
    v_Mail_Conn utl_smtp.Connection;
    crlf   VARCHAR2(2)   := CHR(10); --chr(13)||chr(10);
    v_oper VARCHAR2(100) := 'ERP Operation Support Team';
  BEGIN
    fnd_file.put_line(fnd_file.log,'I---------------------------------------------------------------I');
    fnd_file.put_line(fnd_file.log,' Concurrent Monitoring Status');
    fnd_file.put_line(fnd_file.log,'I---------------------------------------------------------------I');
    OPEN CONCURRENT;
    LOOP
      FETCH CONCURRENT INTO CON;
      EXIT
    WHEN CONCURRENT%NOTFOUND;
      IF CON.CODE = 0 THEN
        FND_FILE.PUT_LINE(FND_FILE.LOG, ' SUCCESS : '||CON.REQUEST_ID||' - '||CON.CONCURRENT_PROGRAM_NAME||' <'||NVL(CON.ORGANIZATION,CON.RESPONSIBILITY_NAME)||'>' );
        SUC         := SUC + 1;
      ELSIF CON.CODE = 1 THEN
        FND_FILE.PUT_LINE(FND_FILE.LOG, ' **ERROR : '||CON.REQUEST_ID||' - '||CON.CONCURRENT_PROGRAM_NAME||' <'||NVL(CON.ORGANIZATION,CON.RESPONSIBILITY_NAME)||'>' );
        ERR         := ERR + 1;
      ELSIF CON.CODE = 99 THEN
        FND_FILE.PUT_LINE(FND_FILE.LOG, ' WARNING : '||CON.REQUEST_ID||' - '||CON.CONCURRENT_PROGRAM_NAME||' <'||NVL(CON.ORGANIZATION,CON.RESPONSIBILITY_NAME)||'>' );
        WRN := WRN + 1;
      END IF;
    END LOOP;
    IF SUC + ERR > 0 THEN
      --FND_FILE.PUT_LINE(FND_FILE.LOG,CHR(10)||' '||SUC||' Concurrent requests have been completed successfully.');
      FND_FILE.PUT_LINE(FND_FILE.LOG,CHR(10)||' '||ERR||' Concurrent requests have been completed with ERROR.'||CHR(10));
      IF WRN > 0 THEN
        FND_FILE.PUT_LINE(FND_FILE.LOG,CHR(10)||' '||WRN||' Concurrent requests have been completed with WARNING.'||CHR(10));
      END IF;
    ELSE
      FND_FILE.PUT_LINE(FND_FILE.LOG,CHR(10)||' No concurrent requests have beed found.'||CHR(10));
    END IF;
    CLOSE CONCURRENT;
    IF ERR > 0 AND WRN = 0 THEN
      BEGIN
        v_Mail_Conn := utl_smtp.Open_Connection(v_Mail_Host, 25);
        utl_smtp.Helo(v_Mail_Conn, v_Mail_Host);
        utl_smtp.Mail(v_Mail_Conn, v_From);
        utl_smtp.Rcpt(v_Mail_Conn, v_Recipient);
        -- UTL_SMTP.RCPT(v_Mail_Conn, v_Cc1);
        -- UTL_SMTP.RCPT(v_Mail_Conn, v_Cc2);
        --UTL_SMTP.RCPT(v_Mail_Conn, v_Cc3);
        utl_smtp.Data(v_Mail_Conn, 'Date: ' || TO_CHAR(sysdate, 'Dy, DD Mon YYYY hh24:mi:ss') || crlf || 'From: ' || v_From || crlf || 'Subject: '|| v_Subject || crlf || 'To: ' || v_Recipient || crlf ||
        -- 'Cc: '     || v_Cc1||'; '||v_Cc2|| crlf ||
        --'Cc: '     || v_Cc1||'; '||v_Cc2||'; '||v_Cc3|| crlf ||
        crlf || 'Dear Operation Support,'|| crlf || crlf || crlf || '      Please check the log of concurrent request: "TAC CONCURRENT MONITOR" for ERROR details.'|| crlf || crlf || crlf || 'ERP Oracle Applications :-'|| crlf || crlf || 'URL: http://cygnus3.tac.co.th:8000/OA_HTML/OA.jsp?OAFunc=OAHOMEPAGE'|| crlf || crlf || crlf || 'Best Regards,'|| crlf || crlf || v_oper|| crlf || crlf || 'Date: ' || TO_CHAR(sysdate, 'Dy, DD Mon YYYY hh24:mi:ss') );
      EXCEPTION
      WHEN utl_smtp.Transient_Error OR utl_smtp.Permanent_Error THEN
        raise_application_error(-20000, 'Unable to send mail: '||sqlerrm);
      END;
    ELSIF ERR > 0 AND WRN > 0 THEN
      BEGIN
        v_Mail_Conn := utl_smtp.Open_Connection(v_Mail_Host, 25);
        utl_smtp.Helo(v_Mail_Conn, v_Mail_Host);
        utl_smtp.Mail(v_Mail_Conn, v_From);
        utl_smtp.Rcpt(v_Mail_Conn, v_Recipient);
        --UTL_SMTP.RCPT(v_Mail_Conn, v_Cc1);
        -- UTL_SMTP.RCPT(v_Mail_Conn, v_Cc2);
        --UTL_SMTP.RCPT(v_Mail_Conn, v_Cc3);
        utl_smtp.Data(v_Mail_Conn, 'Date: ' || TO_CHAR(sysdate, 'Dy, DD Mon YYYY hh24:mi:ss') || crlf || 'From: ' || v_From || crlf || 'Subject: '|| v_Subject2 || crlf || 'To: ' || v_Recipient || crlf ||
        -- 'Cc: '     || v_Cc1||'; '||v_Cc2|| crlf ||
        --'Cc: '     || v_Cc1||'; '||v_Cc2||'; '||v_Cc3|| crlf ||
        crlf || 'Dear Operation Support,'|| crlf || crlf || crlf || '      Please check the log of concurrent request: "TAC CONCURRENT MONITOR" for ERROR/WARNING details.'|| crlf || crlf || crlf || 'ERP Oracle Applications :-'|| crlf || crlf || 'URL: http://cygnus3.tac.co.th:8000/OA_HTML/OA.jsp?OAFunc=OAHOMEPAGE'|| crlf || crlf || crlf || 'Best Regards,'|| crlf || crlf || v_oper|| crlf || crlf || 'Date: ' || TO_CHAR(sysdate, 'Dy, DD Mon YYYY hh24:mi:ss') );
      EXCEPTION
      WHEN utl_smtp.Transient_Error OR utl_smtp.Permanent_Error THEN
        raise_application_error(-20000, 'Unable to send mail: '||sqlerrm);
      END;
    ELSIF ERR = 0 AND WRN > 0 THEN
      BEGIN
        v_Mail_Conn := utl_smtp.Open_Connection(v_Mail_Host, 25);
        utl_smtp.Helo(v_Mail_Conn, v_Mail_Host);
        utl_smtp.Mail(v_Mail_Conn, v_From);
        utl_smtp.Rcpt(v_Mail_Conn, v_Recipient);
        --UTL_SMTP.RCPT(v_Mail_Conn, v_Cc1);
        -- UTL_SMTP.RCPT(v_Mail_Conn, v_Cc2);
        --UTL_SMTP.RCPT(v_Mail_Conn, v_Cc3);
        utl_smtp.Data(v_Mail_Conn, 'Date: ' || TO_CHAR(sysdate, 'Dy, DD Mon YYYY hh24:mi:ss') || crlf || 'From: ' || v_From || crlf || 'Subject: '|| v_Subject1 || crlf || 'To: ' || v_Recipient || crlf ||
        -- 'Cc: '     || v_Cc1||'; '||v_Cc2|| crlf ||
        --'Cc: '     || v_Cc1||'; '||v_Cc2||'; '||v_Cc3|| crlf ||
        crlf || 'Dear Operation Support,'|| crlf || crlf || crlf || '      Please check the log of concurrent request: "TAC CONCURRENT MONITOR" for WARNING details.'|| crlf || crlf || crlf || 'ERP Oracle Applications :-'|| crlf || crlf || 'URL: http://cygnus3.tac.co.th:8000/OA_HTML/OA.jsp?OAFunc=OAHOMEPAGE'|| crlf || crlf || crlf || 'Best Regards,'|| crlf || crlf || v_oper|| crlf || crlf || 'Date: ' || TO_CHAR(sysdate, 'Dy, DD Mon YYYY hh24:mi:ss') );
      EXCEPTION
      WHEN utl_smtp.Transient_Error OR utl_smtp.Permanent_Error THEN
        raise_application_error(-20000, 'Unable to send mail: '||sqlerrm);
      END;
    ELSIF ERR = 0 AND WRN = 0 THEN
      BEGIN
        v_Mail_Conn := utl_smtp.Open_Connection(v_Mail_Host, 25);
        utl_smtp.Helo(v_Mail_Conn, v_Mail_Host);
        utl_smtp.Mail(v_Mail_Conn, v_From);
        utl_smtp.Rcpt(v_Mail_Conn, v_Recipient);
        --UTL_SMTP.RCPT(v_Mail_Conn, v_Cc1);
        --UTL_SMTP.RCPT(v_Mail_Conn, v_Cc2);
        --UTL_SMTP.RCPT(v_Mail_Conn, v_Cc3);
        utl_smtp.Data(v_Mail_Conn, 'Date: ' || TO_CHAR(sysdate, 'Dy, DD Mon YYYY hh24:mi:ss') || crlf || 'From: ' || v_From || crlf || 'Subject: '|| v_Subject0 || crlf || 'To: ' || v_Recipient || crlf ||
        -- 'Cc: '     || v_Cc1||'; '||v_Cc2|| crlf ||
        --'Cc: '     || v_Cc1||'; '||v_Cc2||'; '||v_Cc3|| crlf ||
        crlf || 'Dear Operation Support,'|| crlf || crlf || crlf || '      TAC CONCURRENT MONITOR has finished succesfully with no errors. Please check its log file for further details.'|| crlf || crlf || crlf || 'ERP Oracle Applications :-'|| crlf || crlf || 'URL: http://cygnus3.tac.co.th:8000/OA_HTML/OA.jsp?OAFunc=OAHOMEPAGE'|| crlf || crlf || crlf || 'Best Regards,'|| crlf || crlf || v_oper|| crlf || crlf || 'Date: ' || TO_CHAR(sysdate, 'Dy, DD Mon YYYY hh24:mi:ss') );
      EXCEPTION
      WHEN utl_smtp.Transient_Error OR utl_smtp.Permanent_Error THEN
        raise_application_error(-20000, 'Unable to send mail: '||sqlerrm);
      END;
    END IF;
  END;
