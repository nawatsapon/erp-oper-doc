create or replace procedure TAC_ADD_USER_SUB(p_subinv VARCHAR2) is
begin
DECLARE
  v_subinv   VARCHAR2(10 BYTE) := p_subinv;
  
type tab_user IS TABLE OF VARCHAR2(15 BYTE);
  T_user tab_user;
type tab_uORG IS TABLE OF VARCHAR2(3 BYTE);
  T_uORG tab_uORG;
type tab_subinv IS TABLE OF VARCHAR2(10 BYTE);
  T_subinv tab_subinv;
type tab_sORG IS TABLE OF VARCHAR2(3 BYTE);
  T_sORG tab_sORG;
type tab_chk_subinv IS TABLE OF VARCHAR2(10 BYTE);
  T_chk tab_chk_subinv;
BEGIN
  
  dbms_output.put_line('v_subinv : '|| v_subinv);  
  SELECT SU_USER_ORG.USER_ID,SU_USER_ORG.OU_CODE  BULK COLLECT
    INTO T_user,T_uORG FROM TEMP_ASS_LIST,SU_USER_ORG WHERE TEMP_ASS_LIST.USER_ID = SU_USER_ORG.USER_ID;
  FOR cUser IN 1 .. T_uORG.count
  LOOP
    -- Check user organization
    IF T_uORG.count > 0 THEN    
    -- Check whether subinventory is exist or not
    SELECT SUBINV_CODE,OU_CODE BULK COLLECT INTO T_subinv,T_sORG FROM PB_SUBINV where  SUBINV_CODE = v_subinv and OU_CODE = T_uORG(cUser);
      IF T_subinv.count > 0 THEN
        FOR indx IN 1 .. T_subinv.COUNT
        LOOP
          -- Check whether subinventory already assign for the user or not    
          SELECT subinv_code BULK COLLECT
          INTO T_chk
          FROM PB_USER_SUBINVEN
          WHERE USER_ID   = T_user(cUser)
          AND OU_CODE     = T_sORG(indx)
          AND subinv_code = T_subinv(indx);
          IF T_chk.count  > 0 THEN
            dbms_output.put_line('Subinv '|| T_subinv(indx) || ' already existed for user ' || T_user(cUser));
          ELSE
            -- Assign subinventory to users
            INSERT INTO PB_USER_SUBINVEN
            (USER_ID, OU_CODE,SUBINV_CODE,UPD_BY,UPD_DATE)
            VALUES
            (T_user(cUser),T_sORG(indx) ,T_subinv(indx),'T889774',CURRENT_TIMESTAMP);
            commit;
            dbms_output.put_line('Add :' || T_sORG(indx) || ' | ' || T_subinv(indx) || ' | '|| T_user(cUser));
          END IF;
        END LOOP;
      --ELSE
        --dbms_output.put_line('Sub-inventory not avilable');
      END IF;
    ELSE
      dbms_output.put_line('NO ORG OR NO USER');
    END IF;
  END LOOP;
END;
END;