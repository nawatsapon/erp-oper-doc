BEGIN
  dbms_application_info.set_client_info(142);
END;
SELECT rct.interface_header_attribute1 ref_invoice_number_dcp ,
  rct.trx_number transaction_number ,
  rct.rac_bill_to_customer_num bill_to_number ,
  TO_CHAR(rct.trx_date, 'DD-MM-YYYY') transaction_date ,
  NVL(mml.name, ctl.description) memo_line_name ,
  TO_CHAR(ctd.amount, 'fm9999999999990.00') distribution_amount ,
  TO_CHAR(tax.extended_amount, 'fm99999990.00') tax_amount ,
  TO_CHAR(NVL(ctd.amount,0) + NVL(tax.extended_amount,0), 'fm99999999990.00') unit_price
  --,to_char(ctl.unit_selling_price, 'fm99999999990.00') unit_price -- Exclude VAT not use unitprice directly
FROM ra_customer_trx_partial_v rct
INNER JOIN ra_batch_sources bs
ON (rct.batch_source_id = bs.batch_source_id)
INNER JOIN ra_customer_trx_lines_v ctl
ON (rct.customer_trx_id = ctl.customer_trx_id)
LEFT JOIN ar_memo_lines_vl mml
ON (ctl.inventory_item_id = mml.memo_line_id)
INNER JOIN ra_cust_trx_line_gl_dist_v ctd
ON (ctl.customer_trx_line_id = ctd.customer_trx_line_id)
LEFT JOIN ar_tax_lines_v tax
ON (ctl.customer_trx_line_id = tax.link_to_cust_trx_line_id)
WHERE 0                      =0
AND ctl.line_type            = 'LINE'
AND rct.complete_flag        = 'Y'
AND rct.trx_date            >= to_date('2016/11/05 00:00:00', 'yyyy/mm/dd hh24:mi:ss')
AND rct.trx_date             < to_date('2016/11/05 23:59:59', 'yyyy/mm/dd hh24:mi:ss') + 1
AND bs.name                  = 'TCNX';
SELECT rct.batch_source_id,rct.*
FROM ra_customer_trx_partial_v rct
INNER JOIN ra_batch_sources bs
ON (rct.batch_source_id = bs.batch_source_id)
WHERE 0=0
AND rct.trx_date            >= to_date('2016/11/01 00:00:00', 'yyyy/mm/dd hh24:mi:ss')
AND rct.trx_date             < to_date('2016/11/28 23:59:59', 'yyyy/mm/dd hh24:mi:ss') + 1
AND bs.name                  = 'TCNX';
--AND rct.batch_source_id = 1221
;