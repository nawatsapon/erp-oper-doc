SELECT USER_GROUP.user_name,
  resp_all.*,
  func.function_name,
  func.description f_desc,
  func.user_function_name
FROM
  ( SELECT DISTINCT resp.APPLICATION_ID,
    resp.responsibility_id,
    resp.RESPONSIBILITY_NAME,
    resp.DESCRIPTION,
    menu_all.*
  FROM
    (SELECT REPLACE (NVL(SUBSTR(root_id, 1, instr(root_id,'|')), root_id),'|','') root_id_cut,
      tree_menu.Menu_NAME,
      tree_menu.MENU,
      tree_menu.PROMPT,
      tree_menu.FUNCTION_ID,
      tree_menu.menu_path
    FROM
      (SELECT id SUB_MENU_ID,
        parent_id MENU_ID,
        Menu_NAME,
        FUNCTION_ID,
        NVL(promt_desc,Menu_NAME) MENU,
        PROMPT,
        RPAD('.', (level-1)*2, '.')
        || id AS tree,
        level tree_level,
        LTRIM(SYS_CONNECT_BY_PATH(id, '|'), '|') root_id,
        --CONNECT_BY_ROOT RESPONSIBILITY_ID AS root_id,
        LTRIM(SYS_CONNECT_BY_PATH(Menu_NAME, '|'), '|') AS menu_path
      FROM
        (SELECT FND_MENUS_VL.MENU_ID id,
          FND_MENU_ENTRIES_VL.SUB_MENU_ID parent_id,
          FND_MENUS_VL.Menu_NAME,
          FND_MENUS_VL.USER_MENU_NAME,
          FND_MENU_ENTRIES_VL.FUNCTION_ID,
          FND_MENUS_VL.DESCRIPTION AS Menu_desc,
          FND_MENU_ENTRIES_VL.PROMPT,
          FND_MENU_ENTRIES_VL.DESCRIPTION promt_desc
        FROM FND_MENUS_VL,
          FND_MENU_ENTRIES_VL
        WHERE 0                  =0
        AND FND_MENUS_VL.menu_id = FND_MENU_ENTRIES_VL.menu_id
        ) tab
        --START WITH id = 67869 -- MENU "AR_CUSTOMERS_GUI" ID = 67722 , MENU "AP_SUPPLIERS_GUI12" = 68015
        --START WITH id is not null
        CONNECT BY id = PRIOR parent_id
      ORDER SIBLINGS BY id
      ) tree_menu
    WHERE root_id IS NOT NULL
    ) menu_all,
    FND_RESPONSIBILITY_VL resp
  WHERE 0                      =0
  AND menu_all.root_id_cut     = resp.menu_id
  --AND resp.RESPONSIBILITY_NAME = 'Inventory'
    -- AND resp_all.root_res_name_cut = 'FAR_INV_REQUESTOR'
  AND resp.APPLICATION_ID = 401
  ) resp_all,
  (SELECT u.user_id,
    ur.responsibility_application_id,
    ur.responsibility_id,
    u.Employee_ID,
    U.user_name
  FROM
    (SELECT *
    FROM fnd_user
    WHERE 0         =0
    AND (END_DATE  IS NULL
    OR END_DATE     > sysdate)
    AND START_DATE <= sysdate
      --AND FND_USER.USER_NAME IN ('KRITIMAS', 'BUSSABAM', 'NATAMONC', 'CHATRUDS', 'TANYONGW', 'THAWEEC', 'SULOMRIT', 'T889307', 'T891991', 'SUPISARH', 'CHUTHAMP', 'CHOTIROT', 'KRISSADA', 'JANTANS', 'T889840', 'T866551', 'DANUCHANAT', 'MONTINEE', 'HATTAYA', 'APIPHAN', 'T889300' )
    ) u,
    (SELECT *
    FROM fnd_user_resp_groups_all
    WHERE (END_DATE IS NULL
    OR END_DATE      > sysdate)
    AND START_DATE  <= sysdate
    ) ur
  WHERE u.user_id = ur.user_id
  ) user_group,
  fnd_form_functions_vl func
WHERE resp_all.responsibility_id = user_group.responsibility_id
AND resp_all.APPLICATION_ID      = USER_GROUP.RESPONSIBILITY_APPLICATION_ID
  --and resp_all.APPLICATION_ID = func.APPLICATION_ID(+)
AND resp_all.FUNCTION_ID = func.FUNCTION_ID(+)
--AND USER_GROUP.user_name = 'NUANCHAK'
AND USER_GROUP.user_name        IN ('AKECHAIS', 'ANUWATP', 'APHICHON', 'BENJAPOI', 'CHAIWATR', 'CHIRAPON', 'CHUMNANT', 'CHUTINAT', 'D33968', 'DUSITD', 'KAMPHOLK', 'KASEMSAH', 'KHOMSANC', 'KIATTIRS', 'KITTICHT', 'KITTIK', 'KITTIKOO', 'KOMSONCH', 'KRISSANK', 'MONGKOTH', 'MONTREEJ', 'NARINC', 'NARONGN', 'NOPPADOP', 'ONG-ATC', 'PANIDAV', 'PANOMPOJ', 'PANUPONP', 'PASSAWAS', 'PHAIBOOL', 'Pramuan', 'PRAPASP', 'RAWINR', 'RUANGDEP', 'RUNGWITJ', 'SAKDAC', 'SAMATACN', 'SINEENARTT', 'SITTIKOS', 'SITTISCH', 'SOMPOPR', 'SUMONCHN', 'SUMONTHP', 'SURASEE', 'T874428', 'T875391', 'T875905', 'T876142', 'T876144', 'T876944', 'T877214', 'T903015', 'THANOMDS', 'THAWATE', 'TOSSAPOS', 'UNAROME', 'VEERASAK', 'VEERASAK', 'WANRAWEE', 'WATCHARP', 'WEERAPAS', 'WIJITN', 'WIWATW', 'YANNADATCH', 'YUTANAN')
ORDER BY 1
  --and BB.MENU_NAME = 'WF_ADMIN_NOTIF_TAB'