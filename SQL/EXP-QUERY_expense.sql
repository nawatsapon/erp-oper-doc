SELECT distinct
  erh.REPORT_SUBMITTED_DATE     "Date of Request",
  f.employee_number             "EMP Number",
  erh.Total                     "Total Amount",
  erl.Distribution_Line_Number  "Line",
  erl.Amount                    "Line Amount",
  erh.Description               "Expense Description",
  f.full_name                   "Requester Name",   
  PJ.NAME                       "Position",
  fu.description                "Approver Name", 
  PJ2.NAME                      "Position",                                
  an.entered_date               "Date of Approval",
  c.concatenated_segments       "Account Codes",
  ppa.segment1                  "Project Number",
  ppa.name                      "Project Name",
  erl.Justification             "Justification",
  erh.description               "Purpose", 
  ------------------------------------------------------------------------------
  erl.start_expense_date,
  erl.end_expense_date,
  erh.Invoice_Num,
  --erh.REPORT_HEADER_ID,
  --erh.Employee_ID,
  f.employee_number,
  f.full_name request_by,
  erl.Distribution_Line_Number,
  erh.Description,
  erh.REPORT_SUBMITTED_DATE,
  erh.expense_status_code,
  erl.Item_Description,
  erl.Amount,
  erh.Total,
  erh.PAYMENT_CURRENCY_CODE,
  C.Segment1
  || '.'
  || C.Segment11
  || '.'
  || C.Segment10
  || '.'
  || C.Segment12 GL_Account,
  erl.VAT_Code,
  fu.user_name user_name_approval,
  fu.description Full_name_appproval,
  TO_CHAR(an.notes_detail) Note,
  an.entered_date approve_date,
  --max( an.entered_date)
  erh.REPORT_HEADER_ID, 
  erh.Employee_ID, 
  erh.Description ,
  ert.report_type expense_template_name,
  c.segment11 acc_code,
  c.segment10 rc_code,
  c.concatenated_segments account_segments,
  empgcc.segment10 employee_rc_code,
  decode(c.segment10, empgcc.segment10,'','Y') allocate_flag,
  ltrim(replace(ou.name,'TAC OU -','')) ou_name,
  ai.gl_date gl_date,
  ai.doc_sequence_value doc_sequence_value,
  ai.amount_paid invoice_paid_amount,
  ai.invoice_id invoice_id,
  ai.invoice_date invoice_date,
  f.employee_number employee_number,
  f.last_name || ' ' || f.first_name employee_name,
  uch.user_name header_create_by,
  ucl.user_name line_create_by,
  ppa.segment1 project_number,
  ppa.name project_name,
  tsk.task_number task_number,
  tsk.task_name task_name,
  ltrim(replace(expd_org.name,'TAC OU -','')) expenditure_org_name,
  alc.description approval_status,
  nvl(rpa.web_friendly_prompt, rpa.prompt) expense_type,
  f2.employee_number EMP_APPROVE_NUMBER
    
FROM AP_EXPENSE_REPORT_HEADERS_ALL erh ,
  AP_EXPENSE_REPORT_LINES_ALL erl ,
  ap_expense_reports_all ert,
  gl_code_combinations_KFV c ,
  PER_ALL_PEOPLE_F f ,
  apps.ap_notes an ,
  apps.fnd_user fu,
  gl_code_combinations_v empgcc,
  hr_organization_units ou,
  ap_invoices_all ai,
  fnd_user uch,
  fnd_user ucl,
  pa_projects_all ppa,
  pa_tasks tsk,
  hr_organization_units expd_org,
  ap_lookup_codes alc,
  ap_expense_report_params_all rpa,
  PER_ALL_ASSIGNMENTS_F PAF,
  PER_JOBS PJ,
  PER_ALL_ASSIGNMENTS_F PAF2,
  PER_JOBS PJ2,
  PER_ALL_PEOPLE_F f2
WHERE erh.REPORT_HEADER_ID = erl.REPORT_HEADER_ID
AND erl.code_combination_id = c.code_combination_id
AND ert.expense_report_id = erh.expense_report_id
AND erh.EMPLOYEE_ID = f.PERSON_ID(+)
and erh.employee_ccid = empgcc.code_combination_id
  --and erh.REPORT_SUBMITTED_DATE >= to_date('01/01/2015','dd/mm/yyyy')
  --and erh.REPORT_SUBMITTED_DATE <= to_date('25/09/2015','dd/mm/yyyy')
AND an.source_object_id = erh.report_header_id
AND an.entered_by = fu.user_id
and erh.created_by = uch.user_id(+)
and erl.created_by = ucl.user_id(+)
AND an.notes_detail LIKE 'Approver Action:%'
and erl.org_id = ou.organization_id
and erh.org_id = ai.org_id(+)
and erh.invoice_num = ai.invoice_num(+)
and erl.project_id = ppa.project_id(+)
and erl.project_id = tsk.project_id(+)
and erl.task_id = tsk.task_id(+)
and erl.expenditure_organization_id = expd_org.organization_id(+)
and alc.lookup_type(+) = 'EXPENSE REPORT STATUS'
and erh.expense_status_code = alc.lookup_code(+)
and erl.org_id = rpa.org_id(+)
and erl.web_parameter_id = rpa.parameter_id(+)
AND PAF.person_id(+) = f.person_id
and sysdate between PAF.effective_start_date and PAF.effective_end_date
AND f2.employee_number = fu.fax(+)
AND PAF2.person_id(+) = f2.person_id
and sysdate between PAF2.effective_start_date and PAF2.effective_end_date
AND PJ2.JOB_ID = PAF2.JOB_ID
AND PJ.JOB_ID = PAF.JOB_ID
--and erh.total >= 2000000000
--and f2.employee_number = '5473' -- EMP APPROVE
--and f.employee_number = ''    -- EMP
and erh.invoice_num = 'EMP-263358' -- INV
--and trunc(erh.creation_date) = sysdate -- Create Date
ORDER BY entered_date ASC






