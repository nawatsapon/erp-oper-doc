SET SERVEROUTPUT ON
DECLARE
type tab_ou is table of VARCHAR2(3 BYTE);
  t_ou tab_ou;
type tab_subinv is table of VARCHAR2(10 BYTE);
  t_subinv tab_subinv;
type tab_doc_type is table of VARCHAR2(3 BYTE);
  t_doc_type tab_doc_type;
type tab_doc_no is table of VARCHAR2(15 BYTE);
  t_doc_no tab_doc_no;
type tab_upd_by is table of VARCHAR2(15 BYTE);
  t_upd_by tab_upd_by;
type tab_upd_date is table of DATE;
  t_upd_date tab_upd_date;
type tab_upd_pgm is table of VARCHAR2(8 BYTE);
  t_upd_pgm tab_upd_pgm;

BEGIN
  SELECT distinct a.OU_CODE,
  a.SUBINV_CODE,
  a.DOC_TYPE,
  a.DOC_NO,
  a.UPD_BY,
  a.UPD_DATE,
  a.UPD_PGM
   BULK COLLECT INTO t_ou,
    t_subinv,
    t_doc_type,
    t_doc_no,
    t_upd_by,
    t_upd_date,
    t_upd_pgm
  FROM SA_TRANSACTION a,
  SA_TRANS_DTL d,
  SA_TRANS_PAY p
WHERE 0                =0
AND a.DOC_NO           = p.DOC_NO(+)
AND a.OU_CODE          = p.OU_CODE(+)
AND a.DOC_NO = d.DOC_NO(+)
AND a.OU_CODE = d.OU_CODE(+)
AND TRUNC(a.DOC_DATE) >= sysdate-1
AND p.DOC_NO          IS NULL 
--and d.ITEM_CODE like 'FRE%30'
--AND a.DOC_NO  = 'IVN029591700983'
;
  dbms_output.put_line('count insert : '|| t_doc_no.count);
  FOR cSub IN 1 .. t_doc_no.count
  LOOP
    dbms_output.put_line('OU '|| T_ou(cSub) ||' Subinv '|| T_subinv(cSub) || ' DOC_TYPE ' || t_doc_type(cSub) || ' doc_no ' || t_doc_no(cSub)|| ' upd_by ' || t_upd_by(cSub)|| ' upd_date ' || to_char(t_upd_date(cSub),'dd/mm/yyyy')|| ' upd_pgm ' || t_upd_pgm(cSub));
    
    INSERT INTO SA_TRANS_PAY
  (
    OU_CODE,
    SUBINV_CODE,
    DOC_TYPE,
    DOC_NO,
    PAYMENT_CODE,
    REF_NO,
    BANK_CODE,
    CREDIT_CARD,
    CR_BANK_RATE,
    PAY_AMT,
    TF_S_C,
    TF_C_O,
    UPD_BY,
    UPD_DATE,
    UPD_PGM,
    APPROVE_NO,
    BATCH_NAME,
    LOCKBOX_FILE,
    EDC_NO,
    BANK_ACCOUNT,
    PAY_NET
  )
  VALUES
  (
    T_ou(cSub),
    T_subinv(cSub),
    t_doc_type(cSub),
    t_doc_no(cSub),
    'CASH',
    NULL,
    NULL,
    NULL,
    NULL,
    0,
    NULL,
    NULL,
    t_upd_by(cSub),
    t_upd_date(cSub),
    'DTSADT02',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    0
  );

    
   /* UPDATE PB_SUBINV
    SET DEFAULT_LOC   = T_locator(cSub)
    WHERE SUBINV_CODE = T_subinv(cSub)
    AND OU_CODE       = T_ou(cSub);*/
  END LOOP;
END;
/