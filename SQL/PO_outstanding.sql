SELECT --query_group query_group, closed_code closed_code
  supplier supplier ,
  supplier_name supplier_name ,
  po_release_number po_release_number ,
  due_date due_date ,
  created_date created_date ,
  action_date action_date ,
  approval_status approval_status ,
  buyer buyer
  --        ,release_type release_type
  --        ,po_type_lookup_code po_type_lookup_code
  ,
  po_type po_type ,
  operation_unit operation_unit ,
  line line ,
  ship ship ,
  ship_to ship_to ,
  category_description category_description ,
  item_description item_description ,
  uom uom ,
  original_qty original_qty
  --      ,quantity_released quantity_released
  --      ,quantity_released_cancelled quantity_released_cancelled
  --      ,quantity_returned quantity_returned
  --      ,quantity_released_received quantity_released_received
  --      ,quantity_released_cancelled_x quantity_released_cancelled_x
  --        ,decode(po_type_lookup_code,'PLANNED',decode(po_type,'PLANNED',(original_qty - nvl(quantity_released,0) + nvl(quantity_released_cancelled,0) + nvl(quantity_returned,0)),original_qty),original_qty)qty_remaining
  --        ,unit_price * decode(po_type_lookup_code,'PLANNED',decode(po_type,'PLANNED',(original_qty - nvl(quantity_released,0) + nvl(quantity_released_cancelled,0) + nvl(quantity_returned,0)- nvl(quantity_released_received,0)),original_qty),original_qty) amount_remaining
  ,
  NVL(quantity_released_received,0) received_qty ,
  NVL(quantity_returned,0) quantity_returned ,
  DECODE(po_type_lookup_code,'PLANNED',DECODE(po_type,'PLANNED',(original_qty - NVL(quantity_released,0) + NVL(quantity_released_cancelled,0)),original_qty-NVL(quantity_released_received,0)+ NVL(quantity_returned,0)),original_qty-NVL(quantity_released_received,0)+ NVL(quantity_returned,0)) remaining_qty ,
  unit_price unit_price ,
  unit_price * DECODE(po_type_lookup_code,'PLANNED',DECODE(po_type,'PLANNED',(original_qty - NVL(quantity_released,0) + NVL(quantity_released_cancelled,0)),original_qty-NVL(quantity_released_received,0)+ NVL(quantity_returned,0)),original_qty-NVL(quantity_released_received,0)+ NVL(quantity_returned,0)) remaining_amt
  --      ,received_qty
  --      ,remaining_qty
  --      ,unit_price
  --      ,remaining_amt
  ,
  currency_code currency_code ,
  exchange_rate exchange_rate
  --      ,remaining_amt_thb remaining_amt_thb
  -- Modyfied by AP@BAS on 17-Nov-2015
  -- IM190549 : Change REMAINING_AMT_THB to REMAINING_QTY * EXCHANGE_RATE
  -- ,exchange_rate * unit_price * decode(po_type_lookup_code,'PLANNED',decode(po_type,'PLANNED',(original_qty - nvl(quantity_released,0) + nvl(quantity_released_cancelled,0) + nvl(quantity_returned,0)),original_qty),original_qty) remaining_amt_thb
  ,
  unit_price * DECODE(po_type_lookup_code ,'PLANNED' ,DECODE(po_type ,'PLANNED' ,(original_qty - NVL(quantity_released, 0) + NVL(quantity_released_cancelled, 0)) ,original_qty - NVL(quantity_released_received, 0) + NVL(quantity_returned, 0)) ,original_qty - NVL(quantity_released_received, 0) + NVL(quantity_returned, 0)) * exchange_rate remaining_amt_thb ,
  po_due po_due ,
  MIN(pr_reference)              AS pr_reference ,
  MIN(requester)                 AS requester ,
  MIN(original_rc_code)          AS original_rc_code ,
  MIN(original_depart_requester) AS original_depart_requester ,
  MIN(division_name)             AS division_name ,
  MIN(group_name)                AS group_name ,
  MIN(budget_account_code)       AS budget_account_code ,
  MIN(budget_description)        AS budget_description ,
  MIN(budget_rc_code)            AS budget_rc_code
FROM
  (SELECT DISTINCT 'Query#1' query_group,
    pov.segment1 supplier ,
    pov.vendor_name supplier_name ,
    poh.segment1 AS po_release_number ,
    NVL(TRUNC(poll.promised_date), poll.need_by_date) due_date ,
    poh.creation_date created_date ,
    pah_appr.action_date ,
    NVL(poh.authorization_status, 'INCOMPLETE') approval_status ,
    hre.full_name buyer ,
    NULL release_type ,
    poh.type_lookup_code po_type_lookup_code ,
    poll.closed_code ,
    poh.type_lookup_code po_type ,
    hro.name AS operation_unit ,
    pol.line_num line ,
    poll.shipment_num ship ,
    mp.organization_code ship_to ,
    REPLACE(REPLACE(REPLACE(mcat.description, chr(10), ' '), chr(13), ' '), '|', ' ') category_description ,
    REPLACE(REPLACE(REPLACE(pol.item_description, chr(10), ' '), chr(13), ' '), '|', ' ') item_description ,
    pol.unit_meas_lookup_code uom ,
    DECODE(plt.order_type_lookup_code, 'RATE', poll.amount, 'FIXED PRICE', poll.amount, poll.quantity) original_qty ,
    DECODE(plt.order_type_lookup_code, 'RATE', poll.amount_received, 'FIXED PRICE', poll.amount_received, poll.quantity_received) quantity_released ,
    DECODE(plt.order_type_lookup_code, 'RATE', poll.amount_cancelled, 'FIXED PRICE', poll.amount_cancelled, poll.quantity_cancelled) quantity_released_cancelled
    --                      ,null quantity_returned
    ,
    (SELECT SUM(NVL(rt.source_doc_quantity, 0))
    FROM rcv_transactions rt ,
      po_distributions_all pd2 ,
      po_headers_all ph2
    WHERE rt.po_distribution_id = pd2.po_distribution_id
    AND pd2.po_header_id        = ph2.po_header_id
    AND rt.transaction_type     = 'RETURN TO VENDOR'
    AND rt.po_distribution_id  IS NOT NULL
    AND rt.po_distribution_id   = pod.po_distribution_id
    ) quantity_returned
    /*
    ,decode(plt.order_type_lookup_code,
    'RATE',
    poll.amount_received,
    'FIXED PRICE',
    poll.amount_received,
    poll.quantity_received) quantity_released_received
    */
    ,
    (SELECT SUM(NVL(rt.source_doc_quantity, 0))
    FROM rcv_transactions rt ,
      po_distributions_all pd2 ,
      po_headers_all ph2
    WHERE rt.po_distribution_id = pd2.po_distribution_id
    AND pd2.po_header_id        = ph2.po_header_id
    AND rt.transaction_type    IN ('RECEIVE')
    AND rt.po_distribution_id  IS NOT NULL
    AND rt.po_distribution_id   = pod.po_distribution_id
    ) quantity_released_received ,
    DECODE(plt.order_type_lookup_code, 'RATE', poll.amount_cancelled, 'FIXED PRICE', poll.amount_cancelled, poll.quantity_cancelled) quantity_released_cancelled_x ,
    DECODE(plt.order_type_lookup_code, 'RATE', poll.amount_received, 'FIXED PRICE', poll.amount_received, poll.quantity_received) received_qty ,
    DECODE(plt.order_type_lookup_code, 'RATE', poll.amount, 'FIXED PRICE', poll.amount, poll.quantity) - DECODE(plt.order_type_lookup_code, 'RATE', poll.amount_received, 'FIXED PRICE', poll.amount_received, poll.quantity_received) - DECODE(plt.order_type_lookup_code, 'RATE', poll.amount_cancelled, 'FIXED PRICE', poll.amount_cancelled, poll.quantity_cancelled) remaining_qty ,
    NVL(to_number(DECODE(plt.order_type_lookup_code, 'AMOUNT', NULL, poll.price_override)), 1) unit_price ,
    (poll.quantity - DECODE(plt.order_type_lookup_code, 'RATE', poll.amount_received, 'FIXED PRICE', poll.amount_received, poll.quantity_received) - DECODE(plt.order_type_lookup_code, 'RATE', poll.amount_cancelled, 'FIXED PRICE', poll.amount_cancelled, poll.quantity_cancelled)) * NVL(to_number(DECODE(plt.order_type_lookup_code, 'AMOUNT', NULL, poll.price_override)), 1) remaining_amt ,
    poh.currency_code                                                                                                                                                                                                                                                                                                                                                                                            AS currency_code ,
    NVL(poh.rate, 1)                                                                                                                                                                                                                                                                                                                                                                                             AS exchange_rate ,
    ROUND((poll.quantity                                                                                                                 - DECODE(plt.order_type_lookup_code, 'RATE', poll.amount_received, 'FIXED PRICE', poll.amount_received, poll.quantity_received) - DECODE(plt.order_type_lookup_code, 'RATE', poll.amount_cancelled, 'FIXED PRICE', poll.amount_cancelled, poll.quantity_cancelled)) * NVL(to_number(DECODE(plt.order_type_lookup_code, 'AMOUNT', NULL, poll.price_override)), 1) * NVL(poh.rate, 1), 2) AS remaining_amt_thb ,
    ROUND((DECODE(plt.order_type_lookup_code, 'RATE', poll.amount_received, 'FIXED PRICE', poll.amount_received, poll.quantity_received) / (DECODE(plt.order_type_lookup_code, 'RATE', poll.amount, 'FIXED PRICE', poll.amount, poll.quantity) - DECODE(plt.order_type_lookup_code, 'RATE', poll.amount_cancelled, 'FIXED PRICE', poll.amount_cancelled, poll.quantity_cancelled))) * 100, 2) po_due ,
    prh.segment1 AS pr_reference ,
    papf.full_name requester ,
    prh.attribute1 original_rc_code ,
    REPLACE(REPLACE(fv_ori_req.description, chr(10), ' '), chr(13), ' ') original_depart_requester ,
    prh.attribute2 division_name ,
    prh.attribute3 group_name ,
    gcc.segment11 budget_account_code ,
    REPLACE(REPLACE(REPLACE(fv_budget.description, chr(10), ' '), chr(13), ' '), '|', ' ') budget_description ,
    gcc.segment10 budget_rc_code
  FROM po_headers_all poh ,
    po_lines_all pol ,
    po_line_locations_all poll ,
    po_distributions_all pod ,
    po_vendors pov ,
    mtl_categories mcat ,
    per_all_people_f hre ,
    mtl_parameters mp ,
    po_req_distributions_all prd ,
    po_requisition_lines_all prl ,
    po_requisition_headers_all prh ,
    per_all_people_f papf ,
    po_line_types plt ,
    (SELECT object_id,
      MAX(action_date) AS action_date
    FROM po_action_history
    WHERE object_type_code    = 'PO'
    AND object_sub_type_code IN ('STANDARD', 'PLANNED')
    AND action_code           = 'APPROVE'
    GROUP BY object_id
    ) pah_appr ,
    hr_all_organization_units hro ,
    (SELECT DISTINCT ffv.description,
      ffv.flex_value
    FROM fnd_flex_values_vl ffv
    WHERE ffv.flex_value_set_id = 1009748
    ) fv_ori_req ,
    gl_code_combinations gcc ,
    (SELECT DISTINCT ffv.description,
      ffv.flex_value
    FROM fnd_flex_values_vl ffv
    WHERE ffv.flex_value_set_id = 1009747
    ) fv_budget
  WHERE
    --Po Condition--
    poh.authorization_status        IN ('APPROVED', 'PRE-APPROVED', 'REQUIRES REAPPROVAL')
  AND poh.type_lookup_code          IN ('STANDARD')
  AND NVL(poll.closed_code, 'OPEN') IN ('OPEN')
  AND NVL(poll.cancel_flag, 'N')     = 'N'
    --     and NVL(poll.quantity_received, 0) < NVL(poll.quantity, 0) - NVL(poll.quantity_cancelled, 0)
    --     and poll.shipment_type IN ('STANDARD')
    --PO Link--
  AND poh.po_header_id             = pol.po_header_id
  AND pol.po_header_id             = poll.po_header_id(+)
  AND pol.po_line_id               = poll.po_line_id(+)
  AND poll.po_header_id            = pod.po_header_id(+)
  AND poll.po_line_id              = pod.po_line_id(+)
  AND poll.line_location_id        = pod.line_location_id(+)
  AND pol.category_id              = mcat.category_id
  AND poh.vendor_id                = pov.vendor_id
  AND pov.vendor_name             IS NOT NULL
  AND poll.ship_to_organization_id = mp.organization_id(+)
    --Buyer--
  AND poh.agent_id         = hre.person_id
  AND hre.employee_number IS NOT NULL
  AND sysdate BETWEEN hre.effective_start_date AND hre.effective_end_date
    --PR Link--
  AND pod.req_distribution_id = prd.distribution_id(+)
  AND prd.requisition_line_id = prl.requisition_line_id(+)
    --Requester--
  AND pod.deliver_to_person_id = papf.person_id(+)
  AND pol.line_type_id         = plt.line_type_id
  AND papf.employee_number(+) IS NOT NULL
  AND sysdate BETWEEN papf.effective_start_date(+) AND papf.effective_end_date(+)
  AND poh.po_header_id          = pah_appr.object_id(+)
  AND poh.org_id                = hro.organization_id(+)
  AND prl.requisition_header_id = prh.requisition_header_id(+)
  AND prh.attribute1            = fv_ori_req.flex_value(+)
  AND pod.budget_account_id     = gcc.code_combination_id(+)
  AND gcc.segment11             = fv_budget.flex_value(+)
  AND poh.currency_code  <> 'THB'
    /* Fixed by BS  on 5 Aug 2016 */
  AND (DECODE(plt.order_type_lookup_code, 'RATE', poll.amount, 'FIXED PRICE', poll.amount, poll.quantity) - DECODE(plt.order_type_lookup_code, 'RATE', poll.amount_cancelled, 'FIXED PRICE', poll.amount_cancelled, poll.quantity_cancelled)) !=0
  /*-------------------------------*/
  --PARAMETER--
  --     and poh.creation_date > sysdate - 2
  --    and poh.segment1 in (:p_po_number1, :p_po_number2) -- 991011013019, 991011013020, 991011013022
  UNION ALL
  --blanket. Plan PO---
  SELECT DISTINCT 'Query#2' query_group,
    pov.segment1 supplier ,
    pov.vendor_name supplier_name ,
    DECODE(por.release_type ,'SCHEDULED',poh.segment1
    || ' - '
    || por.release_num
    -- Modify by AP@IS-ES on 25-Nov-2011
    -- to display release number for blanket PO
    ,'BLANKET',poh.segment1
    || ' - '
    || por.release_num
    -- end of modify
    ,poh.segment1) AS po_release_number ,
    NVL(TRUNC(poll.promised_date), poll.need_by_date) due_date ,
    NVL(por.creation_date,poh.creation_date) created_date ,
    pah_appr.action_date ,
    NVL(NVL(por.authorization_status,poh.authorization_status), 'INCOMPLETE') approval_status ,
    hre.full_name buyer ,
    por.release_type release_type ,
    poh.type_lookup_code po_type_lookup_code ,
    poll.closed_code ,
    DECODE(poh.type_lookup_code, 'PLANNED', NVL(por.release_type,poh.type_lookup_code), poh.type_lookup_code) AS po_type
    --                              poh.type_lookup_code) as po_type
    ,
    hro.NAME AS operation_unit ,
    pol.line_num line ,
    poll.shipment_num ship ,
    mp.organization_code ship_to ,
    REPLACE(REPLACE(REPLACE(mcat.description, chr(10), ' '), chr(13), ' '), '|', ' ') category_description ,
    REPLACE(REPLACE(REPLACE(pol.item_description, chr(10), ' '), chr(13), ' '), '|', ' ') item_description ,
    pol.unit_meas_lookup_code uom ,
    DECODE(plt.order_type_lookup_code, 'RATE', poll.amount, 'FIXED PRICE', poll.amount, poll.quantity) original_qty
    -- start
    -- PO Plan line Qty ??? release ??????
    ,
    DECODE(NVL(por.release_type, poh.type_lookup_code), 'PLANNED',
    (SELECT SUM(a.quantity)
    FROM po_line_locations_all a ,
      po_releases_all b
    WHERE a.po_release_id = b.po_release_id
      --and nvl(a.cancel_flag, 'N') = 'N'
      --and nvl(b.cancel_flag, 'N') = 'N'
    AND a.po_header_id = poll.po_header_id
    AND a.po_line_id   = poll.po_line_id
    ), to_number(NULL)) quantity_released
    -- PO Plan line Qty ???release ????????? Cancel
    ,
    DECODE(NVL(por.release_type, poh.type_lookup_code), 'PLANNED',
    (SELECT SUM(a.quantity)
    FROM po_line_locations_all a ,
      po_releases_all b
    WHERE a.po_release_id       = b.po_release_id
    AND NVL(a.cancel_flag, 'N') = 'Y'
      --        and nvl(b.cancel_flag, 'N') = 'Y'
    AND a.po_header_id = poll.po_header_id
    AND a.po_line_id   = poll.po_line_id
    ), to_number(NULL)) quantity_released_cancelled ,
    (SELECT SUM(NVL(rt.source_doc_quantity, 0))
    FROM rcv_transactions rt ,
      po_distributions_all pd2 ,
      po_headers_all ph2
    WHERE rt.po_distribution_id = pd2.po_distribution_id
    AND pd2.po_header_id        = ph2.po_header_id
    AND rt.transaction_type     = 'RETURN TO VENDOR'
    AND rt.po_distribution_id  IS NOT NULL
    AND rt.po_distribution_id   = pod.po_distribution_id
    ) quantity_returned
    /*
    ,decode(plt.order_type_lookup_code,
    'RATE',
    poll.amount_received,
    'FIXED PRICE',
    poll.amount_received,
    poll.quantity_received) quantity_released_received
    */
    ,
    (SELECT SUM(NVL(rt.source_doc_quantity, 0))
    FROM rcv_transactions rt ,
      po_distributions_all pd2 ,
      po_headers_all ph2
    WHERE rt.po_distribution_id                                                                          = pd2.po_distribution_id
    AND pd2.po_header_id                                                                                 = ph2.po_header_id
    AND rt.transaction_type                                                                             IN ('RECEIVE')
    AND rt.po_distribution_id                                                                           IS NOT NULL
    AND DECODE(NVL(por.release_type,poh.type_lookup_code),'PLANNED',rt.po_line_id,rt.po_distribution_id) = DECODE(NVL(por.release_type,poh.type_lookup_code),'PLANNED',pol.po_line_id,pod.po_distribution_id)
    )quantity_released_received
    --              and rt.po_distribution_id = pod.po_distribution_id) quantity_released_received
    ,
    DECODE(plt.order_type_lookup_code, 'RATE', poll.amount_cancelled, 'FIXED PRICE', poll.amount_cancelled, poll.quantity_cancelled) quantity_released_cancelled_x
    ---- end
    ,
    DECODE(plt.order_type_lookup_code, 'RATE', poll.amount_received, 'FIXED PRICE', poll.amount_received, poll.quantity_received) received_qty ,
    DECODE(plt.order_type_lookup_code, 'RATE', poll.amount, 'FIXED PRICE', poll.amount, poll.quantity) - DECODE(plt.order_type_lookup_code, 'RATE', poll.amount_received, 'FIXED PRICE', poll.amount_received, poll.quantity_received) - DECODE(plt.order_type_lookup_code, 'RATE', poll.amount_cancelled, 'FIXED PRICE', poll.amount_cancelled, poll.quantity_cancelled) remaining_qty ,
    NVL(to_number(DECODE(plt.order_type_lookup_code, 'AMOUNT', NULL, poll.price_override)), 1) unit_price ,
    (poll.quantity - DECODE(plt.order_type_lookup_code, 'RATE', poll.amount_received, 'FIXED PRICE', poll.amount_received, poll.quantity_received) - DECODE(plt.order_type_lookup_code, 'RATE', poll.amount_cancelled, 'FIXED PRICE', poll.amount_cancelled, poll.quantity_cancelled)) * NVL(to_number(DECODE(plt.order_type_lookup_code, 'AMOUNT', NULL, poll.price_override)), 1) remaining_amt ,
    poh.currency_code                                                                                                                                                                                                                                                                                                                                                                                            AS currency_code ,
    NVL(poh.rate, 1)                                                                                                                                                                                                                                                                                                                                                                                             AS exchange_rate ,
    ROUND((poll.quantity                                                                                                                 - DECODE(plt.order_type_lookup_code, 'RATE', poll.amount_received, 'FIXED PRICE', poll.amount_received, poll.quantity_received) - DECODE(plt.order_type_lookup_code, 'RATE', poll.amount_cancelled, 'FIXED PRICE', poll.amount_cancelled, poll.quantity_cancelled)) * NVL(TO_NUMBER(DECODE(plt.order_type_lookup_code, 'AMOUNT', NULL, poll.price_override)), 1) * NVL(poh.rate, 1), 2) AS remaining_amt_thb ,
    ROUND((DECODE(plt.order_type_lookup_code, 'RATE', poll.amount_received, 'FIXED PRICE', poll.amount_received, poll.quantity_received) / (DECODE(plt.order_type_lookup_code, 'RATE', poll.amount, 'FIXED PRICE', poll.amount, poll.quantity) - DECODE(plt.order_type_lookup_code, 'RATE', poll.amount_cancelled, 'FIXED PRICE', poll.amount_cancelled, poll.quantity_cancelled))) * 100, 2) po_due ,
    prh.segment1 AS pr_reference ,
    papf.full_name requester ,
    prh.attribute1 original_rc_code ,
    REPLACE(REPLACE(fv_ori_req.description, chr(10), ' '), chr(13), ' ') original_depart_requester ,
    prh.attribute2 division_name ,
    prh.attribute3 group_name ,
    gcc.segment11 budget_account_code ,
    REPLACE(REPLACE(REPLACE(fv_budget.description, chr(10), ' '), chr(13), ' '), '|', ' ') budget_description ,
    gcc.segment10 budget_rc_code
  FROM po_headers_all poh ,
    po_lines_all pol ,
    po_line_locations_all poll ,
    po_distributions_all pod ,
    po_vendors pov ,
    po_releases_all por ,
    mtl_categories mcat ,
    per_all_people_f hre ,
    mtl_parameters mp ,
    po_req_distributions_all prd ,
    po_requisition_lines_all prl ,
    po_requisition_headers_all prh ,
    per_all_people_f papf ,
    po_line_types plt ,
    (SELECT object_id,
      MAX(action_date) AS action_date
    FROM po_action_history
    WHERE object_type_code   IN ('PA', 'PO') -- PA for blanket
    AND object_sub_type_code IN ('STANDARD', 'PLANNED','BLANKET')
    AND action_code           = 'APPROVE'
    GROUP BY object_id
    ) pah_appr ,
    hr_all_organization_units hro ,
    (SELECT DISTINCT ffv.description,
      ffv.flex_value
    FROM fnd_flex_values_vl ffv
    WHERE ffv.flex_value_set_id = 1009748
    ) fv_ori_req ,
    gl_code_combinations gcc ,
    (SELECT DISTINCT ffv.description,
      ffv.flex_value
    FROM fnd_flex_values_vl ffv
    WHERE ffv.flex_value_set_id = 1009747
    ) fv_budget
  WHERE
    --Po Condition--
    poh.authorization_status       IN ('APPROVED', 'PRE-APPROVED', 'REQUIRES REAPPROVAL')
  AND (por.authorization_status    IS NULL
  OR por.authorization_status      IN ('APPROVED', 'PRE-APPROVED', 'REQUIRES REAPPROVAL'))
  AND poh.type_lookup_code         IN ('BLANKET', 'PLANNED')
  AND NVL(poll.closed_code, 'OPEN') = ('OPEN') -- ????????? closed_code = 'CLOSED FOR RECEIVING'
  AND NVL(poll.cancel_flag, 'N')    = 'N'
    --     and nvl(poll.quantity_received, 0) < nvl(poll.quantity, 0) - nvl(poll.quantity_cancelled, 0)
    --     and poll.shipment_type IN ('BLANKET', 'SCHEDULED')
    --PO Link--
  AND poh.po_header_id      = pol.po_header_id
  AND pol.po_header_id      = poll.po_header_id(+)
  AND pol.po_line_id        = poll.po_line_id(+)
  AND poll.po_header_id     = pod.po_header_id(+)
  AND poll.po_line_id       = pod.po_line_id(+)
  AND poll.line_location_id = pod.line_location_id(+)
  AND pol.category_id       = mcat.category_id
  AND poh.vendor_id         = pov.vendor_id
  AND pov.vendor_name      IS NOT NULL
    -- POR Link--
  AND poll.po_header_id  = por.po_header_id(+)
  AND poll.po_release_id = por.po_release_id(+)
  AND pol.line_type_id   = plt.line_type_id
    --Buyer--
  AND poh.agent_id         = hre.person_id
  AND hre.employee_number IS NOT NULL
  AND sysdate BETWEEN hre.effective_start_date AND hre.effective_end_date
  AND poll.ship_to_organization_id = mp.organization_id(+)
    --PR LINK--
  AND pod.req_distribution_id = prd.distribution_id(+)
  AND prd.requisition_line_id = prl.requisition_line_id(+)
    --Requester--
  AND pod.deliver_to_person_id = papf.person_id(+)
  AND papf.employee_number(+) IS NOT NULL
  AND sysdate BETWEEN papf.effective_start_date(+) AND papf.effective_end_date(+)
  AND poh.po_header_id          = pah_appr.object_id(+)
  AND poh.org_id                = hro.organization_id(+)
  AND prl.requisition_header_id = prh.requisition_header_id(+)
  AND prh.attribute1            = fv_ori_req.flex_value(+)
  AND pod.budget_account_id     = gcc.code_combination_id(+)
  AND gcc.segment11             = fv_budget.flex_value(+)
  AND poh.currency_code  <> 'THB'
    --PARAMETER--
    --    and poh.creation_date > sysdate - 2
    --    and poh.segment1 in (:p_po_number1,:p_po_number2) -- 991011013022, 991011013024, 991011013025
    /*Fixed by BS on 5 Aug 2016 */
  AND (DECODE(plt.order_type_lookup_code, 'RATE', poll.amount, 'FIXED PRICE', poll.amount, poll.quantity) - DECODE(plt.order_type_lookup_code, 'RATE', poll.amount_cancelled, 'FIXED PRICE', poll.amount_cancelled, poll.quantity_cancelled)) !=0
    /*--------------------------*/
  )
WHERE DECODE(po_type_lookup_code,'PLANNED',DECODE(po_type,'PLANNED',(original_qty - NVL(quantity_released,0) + NVL(quantity_released_cancelled,0)),original_qty-NVL(quantity_released_received,0)+ NVL(quantity_returned,0)),original_qty-NVL(quantity_released_received,0)+ NVL(quantity_returned,0)) > 0
GROUP BY query_group,
  closed_code ,
  supplier ,
  supplier_name ,
  po_release_number ,
  due_date ,
  created_date ,
  action_date ,
  approval_status ,
  buyer ,
  release_type ,
  po_type_lookup_code ,
  po_type ,
  operation_unit ,
  line ,
  ship ,
  ship_to ,
  category_description ,
  item_description ,
  uom ,
  original_qty ,
  quantity_released ,
  quantity_released_cancelled ,
  quantity_returned ,
  quantity_released_received ,
  quantity_released_cancelled_x ,
  received_qty ,
  remaining_qty ,
  unit_price ,
  remaining_amt ,
  currency_code ,
  exchange_rate ,
  remaining_amt_thb ,
  po_due
ORDER BY supplier,
  po_release_number,
  line,
  ship
  
   