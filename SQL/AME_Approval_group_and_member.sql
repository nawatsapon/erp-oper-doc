SELECT ar.rule_id,
  fl.meaning AS RULE_TYPE_DESC,
  art.description rule_name,
  ar.start_date,
  ar.end_date,
  acu.condition_id,
  ame_utility_pkg.get_condition_description (acu.condition_id) condition,
  aty.name action_type,
  ame_utility_pkg.get_action_description (ameactionusageeo.action_id) AS approver_group,
  FU.DESCRIPTION APPROVER_NAME ,
  AGM.PARAMETER
FROM ame_rules ar,
  ame_rules_tl art,
  ame_condition_usages acu,
  ame_action_usages ameactionusageeo,
  ame_actions_vl act,
  ame_action_types_vl aty,
  (SELECT *
  FROM ame_action_type_usages
  WHERE rule_type <> 2
  AND SYSDATE BETWEEN start_date AND NVL (end_date - (1 / 86400), SYSDATE)
  ) atu,
  AME_APPROVAL_GROUPS_TL AG,
  AME_APPROVAL_GROUP_MEMBERS AGM,
  fnd_lookups fl ,
  FND_USER FU
WHERE ar.rule_id = art.rule_id
AND art.language = 'US'
AND TRUNC (SYSDATE) BETWEEN ar.start_date AND NVL ( ar.end_date, TO_DATE ('31-DEC-4712', 'DD-MON-YYYY'))
  --AND UPPER (art.description) LIKE '%VARAWUT%'
AND acu.rule_id = ar.rule_id
AND TRUNC (SYSDATE) BETWEEN acu.start_date AND NVL ( acu.end_date, TO_DATE ('31-DEC-4712', 'DD-MON-YYYY'))
AND ( (SYSDATE BETWEEN ameactionusageeo.start_date AND NVL (ameactionusageeo.end_date - (1 / 86400), SYSDATE))
OR (SYSDATE                     < ameactionusageeo.start_date
AND ameactionusageeo.start_date < NVL (ameactionusageeo.end_date, ameactionusageeo.start_date + (1 / 86400))))
AND SYSDATE BETWEEN act.start_date AND NVL (act.end_date                                      - (1 / 86400), SYSDATE)
AND SYSDATE BETWEEN aty.start_date AND NVL (aty.end_date                                      - (1 / 86400), SYSDATE)
AND aty.action_type_id            = atu.action_type_id
AND act.action_id                 = ameactionusageeo.action_id
AND act.action_type_id            = aty.action_type_id
AND AG.APPROVAL_GROUP_ID          = AGM.APPROVAL_GROUP_ID
AND TO_CHAR(AG.APPROVAL_GROUP_ID) = ACT.PARAMETER
AND fl.lookup_type                = 'AME_RULE_TYPE'
AND fl.enabled_flag               = 'Y'
AND fl.lookup_code                = AR.RULE_TYPE
AND ameactionusageeo.rule_id      = ar.rule_id
AND AGM.PARAMETER                 = FU.USER_NAME(+)
AND act.action_type_id = 10013
--AND art.description LIKE 'DDM Requisition%'


  -- AND AGM.PARAMETER         = 'T706498'
  --and acu.condition_id = 57417
  --AND ame_utility_pkg.get_condition_description (acu.condition_id) like '%0158%'
UNION

SELECT ar.rule_id,
  fl.meaning AS RULE_TYPE_DESC,
  art.description rule_name,
  ar.start_date,
  ar.end_date,
  acu.condition_id,
  ame_utility_pkg.get_condition_description (acu.condition_id) condition,
  aty.name action_type,
  ame_utility_pkg.get_action_description (ameactionusageeo.action_id) AS approver_group,
  NULL APPROVER_NAME ,
  NULL PARAMETER
FROM ame_rules ar,
  ame_rules_tl art,
  ame_condition_usages acu,
  ame_action_usages ameactionusageeo,
  ame_actions_vl act,
  ame_action_types_vl aty,
  (SELECT *
  FROM ame_action_type_usages
  WHERE rule_type not in (2)
  AND SYSDATE BETWEEN start_date AND NVL (end_date - (1 / 86400), SYSDATE)
  ) atu, 
  fnd_lookups fl 
WHERE ar.rule_id = art.rule_id
AND art.language = 'US'
AND TRUNC (SYSDATE) BETWEEN ar.start_date AND NVL ( ar.end_date, TO_DATE ('31-DEC-4712', 'DD-MON-YYYY'))
  --AND UPPER (art.description) LIKE '%VARAWUT%'
AND acu.rule_id = ar.rule_id
AND TRUNC (SYSDATE) BETWEEN acu.start_date AND NVL ( acu.end_date, TO_DATE ('31-DEC-4712', 'DD-MON-YYYY'))
AND ( (SYSDATE BETWEEN ameactionusageeo.start_date AND NVL (ameactionusageeo.end_date - (1 / 86400), SYSDATE))
OR (SYSDATE                     < ameactionusageeo.start_date
AND ameactionusageeo.start_date < NVL (ameactionusageeo.end_date, ameactionusageeo.start_date + (1 / 86400))))
AND SYSDATE BETWEEN act.start_date AND NVL (act.end_date                                      - (1 / 86400), SYSDATE)
AND SYSDATE BETWEEN aty.start_date AND NVL (aty.end_date                                      - (1 / 86400), SYSDATE)
AND aty.action_type_id            = atu.action_type_id
AND act.action_id                 = ameactionusageeo.action_id
AND act.action_type_id            = aty.action_type_id
AND fl.lookup_type                = 'AME_RULE_TYPE'
AND fl.enabled_flag               = 'Y'
AND fl.lookup_code                = AR.RULE_TYPE
AND ameactionusageeo.rule_id      = ar.rule_id
AND act.action_type_id <> 10013
--AND art.description LIKE 'DDM Requisition%'

  -- AND AGM.PARAMETER         = 'T706498'
  --and acu.condition_id = 57417
  --AND ame_utility_pkg.get_condition_description (acu.condition_id) like '%0158%'
  
  ;
SELECT * FROM APPLSYS.FND_USER

--and acu.condition_id = 57417
;
--AND ame_utility_pkg.get_condition_description (acu.condition_id) like 'PM%'

---------------------------------- action  query ------------------------------------------------------------------------
SELECT *
FROM AME_APPROVAL_GROUPS_TL AG,
  AME_APPROVAL_GROUP_MEMBERS AGM,
  AME_ACTIONS ACT
WHERE 0                   =0
AND AG.APPROVAL_GROUP_ID = AGM.APPROVAL_GROUP_ID
AND to_char(AG.APPROVAL_GROUP_ID) = ACT.PARAMETER
AND AGM.PARAMETER         = 'T869321' ;

select * from ame_rules