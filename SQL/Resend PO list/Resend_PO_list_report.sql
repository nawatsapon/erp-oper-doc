SELECT poh.segment1, poh.approved_date, poh.authorization_status,POh.WF_ITEM_KEY
  FROM po_headers_all poh, PO_VENDOR_SITES_ALL ves
WHERE poh.approved_date >= to_date('29/10/2015','dd/mm/yyyy') 
   --and por.po_header_id = poh.po_header_id
--  and por.cancel_flag is null  -- SWE 280509
   and poh.revised_date is null -- SWE -010609
   and poh.vendor_id = ves.vendor_id
   and poh.vendor_site_id = ves.vendor_site_id
   and ves.supplier_notif_method = 'EMAIL'
   and ves.email_address is not null
   and poh.type_lookup_code <> 'PLANNED'
AND SUBSTR(POh.WF_ITEM_KEY, 0, INSTR(POh.WF_ITEM_KEY, '-')-1) NOT IN (
  SELECT SUBSTR(wfn.CONTEXT, INSTR(wfn.CONTEXT, ':')+1, INSTR(wfn.CONTEXT, '-') - INSTR(wfn.CONTEXT, ':')-1)
  FROM wf_notifications wfn
  WHERE wfn.message_name = 'EMAIL_PO_PDF'
  --and wfn.mail_status = 'SENT'
  )
  order by poh.segment1,poh.approved_date;

-- without category "A"
/*SELECT DISTINCT poh.segment1,
  poh.approved_date,
  poh.authorization_status,
  POh.WF_ITEM_KEY
FROM po_headers_all poh,PO_LINES_all pol, MTL_CATEGORIES_B_KFV KFV,
  PO_VENDOR_SITES_ALL ves
WHERE poh.approved_date >= to_date('26/10/2015','dd/mm/yyyy')
  --and por.po_header_id = poh.po_header_id
  --  and por.cancel_flag is null  -- SWE 280509
AND poh.revised_date                                              IS NULL -- SWE -010609
AND poh.vendor_id                                                  = ves.vendor_id
AND poh.vendor_site_id                                             = ves.vendor_site_id
AND poh.PO_HEADER_ID                                               = pol.PO_HEADER_ID
and pol.CATEGORY_ID      = KFV.CATEGORY_ID
AND KFV.segment1 <> 'A'
AND ves.supplier_notif_method                                      = 'EMAIL'
AND ves.email_address                                             IS NOT NULL
AND poh.type_lookup_code                                          <> 'PLANNED'
AND SUBSTR(POh.WF_ITEM_KEY, 0, INSTR(POh.WF_ITEM_KEY, '-')-1) NOT IN
  (SELECT SUBSTR(wfn.CONTEXT, INSTR(wfn.CONTEXT, ':')     +1, INSTR(wfn.CONTEXT, '-') - INSTR(wfn.CONTEXT, ':')-1)
  FROM wf_notifications wfn
  WHERE wfn.message_name = 'EMAIL_PO_PDF'
  AND wfn.mail_status    = 'SENT'
  )
ORDER BY poh.segment1,
  poh.approved_date;*/
