SELECT r.request_id,
  r.actual_start_date,
  u.user_name,
  r.concurrent_program_id,
  pp.concurrent_program_name,
  r.oracle_process_id spid,
  t.description
FROM apps.fnd_concurrent_requests r ,
  apps.fnd_user u,
  apps.fnd_concurrent_programs_tl t,
  apps.fnd_concurrent_programs pp
WHERE r.phase_code          = 'R'
AND r.status_code           = 'R'
AND r.requested_by          = u.user_id
AND r.concurrent_program_id = t.concurrent_program_id
AND t.concurrent_program_id = pp.concurrent_program_id
