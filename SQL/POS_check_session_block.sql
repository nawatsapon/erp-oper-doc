--Check session is blocking:
SELECT
  (SELECT username FROM v$session WHERE sid=a.sid
  ) blocker,
  a.sid,
  ' is blocking ',
  (SELECT username FROM v$session WHERE sid=b.sid
  ) blockee,
  b.sid
FROM v$lock a,
  v$lock b
WHERE a.block = 1
AND b.request > 0
AND a.id1     = b.id1
AND a.id2     = b.id2;
--#######################################################
--Check session:
SELECT c.owner,
  c.object_name,
  c.object_type,
  b.sid,
  b.serial#,
  b.status,
  b.osuser,
  b.machine
FROM v$locked_object a ,
  v$session b,
  dba_objects c
WHERE b.sid     = a.session_id
AND a.object_id = c.object_id;
ALTER system kill session '1342,49847';
