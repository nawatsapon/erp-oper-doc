SELECT ar.rule_id,
  art.description rule_name,
  ar.start_date,
  ar.end_date,
  acu.condition_id,
  ame_utility_pkg.get_condition_description (acu.condition_id) condition,
  aty.name action_type,
  ame_utility_pkg.get_action_description (ameactionusageeo.action_id) AS approver_group,
  art.LAST_UPDATE_DATE RULE_Update,
  acu.LAST_UPDATE_DATE CON_UPdate,
  acu.LAST_UPDATED_BY,
  aty.LAST_UPDATE_DATE action_update,
  act.action_id,
  acu.condition_id
FROM ame_rules ar,
  ame_rules_tl art,
  ame_condition_usages acu,
  ame_action_usages ameactionusageeo,
  ame_actions_vl act,
  ame_action_types_vl aty,
  (SELECT *
  FROM ame_action_type_usages
  WHERE rule_type <> 2
  AND SYSDATE BETWEEN start_date AND NVL (end_date - (1 / 86400), SYSDATE)
  ) atu
WHERE ar.rule_id = art.rule_id
AND art.language = 'US'
AND TRUNC (SYSDATE) BETWEEN ar.start_date AND NVL ( ar.end_date, TO_DATE ('31-DEC-4712', 'DD-MON-YYYY'))
  --AND UPPER (art.description) LIKE '%VARAWUT%'
AND acu.rule_id = ar.rule_id
AND TRUNC (SYSDATE) BETWEEN acu.start_date AND NVL ( acu.end_date, TO_DATE ('31-DEC-4712', 'DD-MON-YYYY'))
AND ( (SYSDATE BETWEEN ameactionusageeo.start_date AND NVL (ameactionusageeo.end_date - (1 / 86400), SYSDATE))
OR (SYSDATE                     < ameactionusageeo.start_date
AND ameactionusageeo.start_date < NVL (ameactionusageeo.end_date, ameactionusageeo.start_date + (1 / 86400))))
AND SYSDATE BETWEEN act.start_date AND NVL (act.end_date                                      - (1 / 86400), SYSDATE)
AND SYSDATE BETWEEN aty.start_date AND NVL (aty.end_date                                      - (1 / 86400), SYSDATE)
AND aty.action_type_id       = atu.action_type_id
AND act.action_id            = ameactionusageeo.action_id
AND act.action_type_id       = aty.action_type_id
AND ameactionusageeo.rule_id = ar.rule_id
  AND art.description LIKE '%PM2_PEERAPONG_LERANGRUNGWASON%'
    --and acu.condition_id in (579536,
--307523)
--AND ame_utility_pkg.get_action_description (ameactionusageeo.action_id) LIKE '%PM_Kittipong_Kijsanayothin' 
;
---------
SELECT ame_utility_pkg.get_action_description (action_id),
  ame_action_usages.*
FROM ame_action_usages
WHERE 0=0
AND ame_utility_pkg.get_action_description (action_id) LIKE '%PM_Parinya_Jutasen';
---------------------------------- condition  query ------------------------------------------------------------------------
SELECT STRVAL.STRING_VALUE,
  STRVAL.*
FROM HR.AME_STRING_VALUES STRVAL
WHERE 0                 =0
--AND strval.condition_id in (579537)
  AND STRVAL.STRING_VALUE LIKE '0074'
AND TRUNC(STRVAL.START_DATE) <= SYSDATE
AND TRUNC(STRVAL.END_DATE)    > SYSDATE
ORDER BY 3;
--AND p_effective_date BETWEEN strval.start_date AND NVL(strval.end_date-(1/86400),p_effective_date) ORDER BY strval.string_value;
-------------
SELECT ar.rule_id,
  fl.meaning AS RULE_TYPE_DESC,
  APPT.Application_name,
  art.description rule_name,
  ar.USAGE_START_DATE,
  ar.USAGE_END_DATE,
  acu.condition_id,
  ame_utility_pkg.get_condition_description (acu.condition_id) condition,
  aty.name action_type,
  ame_utility_pkg.get_action_description (ameactionusageeo.action_id) AS approver_group,
  FU.DESCRIPTION APPROVER_NAME ,
  ACT.PARAMETER,
  act.action_type_id
  --,AGM.*
FROM AME_RULES_V ar,
  ame_rules_tl art,
  ame_condition_usages acu,
  ame_action_usages ameactionusageeo,
  ame_actions_vl act,
  ame_action_types_vl aty,
  (SELECT *
  FROM ame_action_type_usages
  WHERE rule_type <> 2
  AND SYSDATE BETWEEN start_date AND NVL (end_date - (1 / 86400), SYSDATE)
  ) atu,
  AME_APPROVAL_GROUPS_TL AG,
  AME_APPROVAL_GROUP_MEMBERS AGM,
  AME_APPROVAL_GROUP_CONFIG AGC,
  fnd_lookups fl ,
  FND_USER FU,
  AME_CALLING_APPS_TL APPT
WHERE ar.rule_id = art.rule_id
AND art.language = 'US'
AND TRUNC (SYSDATE) BETWEEN AR.USAGE_START_DATE AND NVL ( AR.USAGE_END_DATE, TO_DATE ('31-DEC-4712', 'DD-MON-YYYY'))
  --AND UPPER (art.description) LIKE '%VARAWUT%'
AND acu.rule_id = ar.rule_id
AND TRUNC (SYSDATE) BETWEEN acu.start_date AND NVL ( acu.end_date, TO_DATE ('31-DEC-4712', 'DD-MON-YYYY'))
AND ( (SYSDATE BETWEEN ameactionusageeo.start_date AND NVL (ameactionusageeo.end_date - (1 / 86400), SYSDATE))
OR (SYSDATE                     < ameactionusageeo.start_date
AND ameactionusageeo.start_date < NVL (ameactionusageeo.end_date, ameactionusageeo.start_date + (1 / 86400))))
AND SYSDATE BETWEEN act.start_date AND NVL (act.end_date                                      - (1 / 86400), SYSDATE)
AND SYSDATE BETWEEN aty.start_date AND NVL (aty.end_date                                      - (1 / 86400), SYSDATE)
AND aty.action_type_id            = atu.action_type_id
AND act.action_id                 = ameactionusageeo.action_id
AND act.action_type_id            = aty.action_type_id
AND AG.APPROVAL_GROUP_ID          = AGM.APPROVAL_GROUP_ID
AND TO_CHAR(AG.APPROVAL_GROUP_ID) = ACT.PARAMETER(+)
AND fl.lookup_type                = 'AME_RULE_TYPE'
AND fl.enabled_flag               = 'Y'
AND fl.lookup_code                = AR.RULE_TYPE
AND ameactionusageeo.rule_id      = ar.rule_id
AND AG.APPROVAL_GROUP_ID          = AGC.APPROVAL_GROUP_ID
AND ar.AME_APPLICATION_ID         = AGC.APPLICATION_ID
AND AGC.APPLICATION_ID            = APPT.APPLICATION_ID
AND AGC.START_DATE               <= sysdate
AND (AGC.END_DATE                 > sysdate
OR AGC.END_DATE                  IS NULL)
AND AGM.PARAMETER                 = FU.USER_NAME(+)
AND act.action_type_id           IN (10013,10006,10007)
--AND AGM.PARAMETER         in ('LEELAWAB','THIRAPHANT')
--AND art.description = 'Pre-Approval for Technology Group'
--and acu.condition_id = 57417
--AND ame_utility_pkg.get_condition_description (acu.condition_id) like '%0158%'
UNION
SELECT ar.rule_id,
  fl.meaning AS RULE_TYPE_DESC,
  APPT.Application_name,
  art.description rule_name,
  ar.USAGE_START_DATE,
  ar.USAGE_END_DATE,
  acu.condition_id,
  ame_utility_pkg.get_condition_description (acu.condition_id) condition,
  aty.name action_type,
  ame_utility_pkg.get_action_description (ameactionusageeo.action_id) AS approver_group,
  NULL APPROVER_NAME ,
  ACT.PARAMETER,
  act.action_type_id
FROM AME_RULES_V ar,
  ame_rules_tl art,
  ame_condition_usages acu,
  ame_action_usages ameactionusageeo,
  ame_actions_vl act,
  ame_action_types_vl aty,
  (SELECT *
  FROM ame_action_type_usages
  WHERE rule_type <> 2
  AND SYSDATE BETWEEN start_date AND NVL (end_date - (1 / 86400), SYSDATE)
  ) atu,
  fnd_lookups fl ,
  AME_CALLING_APPS_TL APPT
WHERE ar.rule_id = art.rule_id
AND art.language = 'US'
AND TRUNC (SYSDATE) BETWEEN AR.USAGE_START_DATE AND NVL ( AR.USAGE_END_DATE, TO_DATE ('31-DEC-4712', 'DD-MON-YYYY'))
  --AND UPPER (art.description) LIKE '%VARAWUT%'
AND acu.rule_id = ar.rule_id
AND TRUNC (SYSDATE) BETWEEN acu.start_date AND NVL ( acu.end_date, TO_DATE ('31-DEC-4712', 'DD-MON-YYYY'))
AND ( (SYSDATE BETWEEN ameactionusageeo.start_date AND NVL (ameactionusageeo.end_date - (1 / 86400), SYSDATE))
OR (SYSDATE                     < ameactionusageeo.start_date
AND ameactionusageeo.start_date < NVL (ameactionusageeo.end_date, ameactionusageeo.start_date + (1 / 86400))))
AND SYSDATE BETWEEN act.start_date AND NVL (act.end_date                                      - (1 / 86400), SYSDATE)
AND SYSDATE BETWEEN aty.start_date AND NVL (aty.end_date                                      - (1 / 86400), SYSDATE)
AND aty.action_type_id       = atu.action_type_id
AND act.action_id            = ameactionusageeo.action_id
AND act.action_type_id       = aty.action_type_id
AND fl.lookup_type           = 'AME_RULE_TYPE'
AND fl.enabled_flag          = 'Y'
AND fl.lookup_code           = AR.RULE_TYPE
AND ameactionusageeo.rule_id = ar.rule_id
AND ar.AME_APPLICATION_ID    = APPT.APPLICATION_ID
AND act.action_type_id NOT  IN (10013,10006,10007) ;
SELECT                       *
FROM AME_APPROVAL_GROUPS_TL AG,
  AME_APPROVAL_GROUP_CONFIG AGC
WHERE 0                           =0
AND AG.APPROVAL_GROUP_ID          = AGC.APPROVAL_GROUP_ID
AND AGC.START_DATE               <= sysdate
AND (AGC.END_DATE                 > sysdate
OR AGC.END_DATE                  IS NULL)
AND TO_CHAR(AG.APPROVAL_GROUP_ID) = '200173' ;
SELECT * FROM AME_RULES_V;
SELECT ar.rule_id,
  fl.meaning AS RULE_TYPE_DESC,
  art.description rule_name,
  ar.start_date,
  ar.end_date,
  acu.condition_id,
  ame_utility_pkg.get_condition_description (acu.condition_id) condition,
  aty.name action_type,
  ame_utility_pkg.get_action_description (ameactionusageeo.action_id) AS approver_group,
  FU.DESCRIPTION APPROVER_NAME ,
  AGM.*
FROM ame_rules ar,
  ame_rules_tl art,
  ame_condition_usages acu,
  ame_action_usages ameactionusageeo,
  ame_actions_vl act,
  ame_action_types_vl aty,
  (SELECT *
  FROM ame_action_type_usages
  WHERE rule_type <> 2
  AND SYSDATE BETWEEN start_date AND NVL (end_date - (1 / 86400), SYSDATE)
  ) atu,
  AME_APPROVAL_GROUPS_TL AG,
  AME_APPROVAL_GROUP_MEMBERS AGM,
  fnd_lookups fl ,
  FND_USER FU
WHERE ar.rule_id = art.rule_id
AND art.language = 'US'
AND TRUNC (SYSDATE) BETWEEN ar.start_date AND NVL ( ar.end_date, TO_DATE ('31-DEC-4712', 'DD-MON-YYYY'))
  --AND UPPER (art.description) LIKE '%VARAWUT%'
AND acu.rule_id = ar.rule_id
AND TRUNC (SYSDATE) BETWEEN acu.start_date AND NVL ( acu.end_date, TO_DATE ('31-DEC-4712', 'DD-MON-YYYY'))
AND ( (SYSDATE BETWEEN ameactionusageeo.start_date AND NVL (ameactionusageeo.end_date - (1 / 86400), SYSDATE))
OR (SYSDATE                     < ameactionusageeo.start_date
AND ameactionusageeo.start_date < NVL (ameactionusageeo.end_date, ameactionusageeo.start_date + (1 / 86400))))
AND SYSDATE BETWEEN act.start_date AND NVL (act.end_date                                      - (1 / 86400), SYSDATE)
AND SYSDATE BETWEEN aty.start_date AND NVL (aty.end_date                                      - (1 / 86400), SYSDATE)
AND aty.action_type_id            = atu.action_type_id
AND act.action_id                 = ameactionusageeo.action_id
AND act.action_type_id            = aty.action_type_id
AND AG.APPROVAL_GROUP_ID          = AGM.APPROVAL_GROUP_ID
AND TO_CHAR(AG.APPROVAL_GROUP_ID) = ACT.PARAMETER
AND fl.lookup_type                = 'AME_RULE_TYPE'
AND fl.enabled_flag               = 'Y'
AND fl.lookup_code                = AR.RULE_TYPE
AND ameactionusageeo.rule_id      = ar.rule_id
AND AGM.PARAMETER                 = FU.USER_NAME(+)
  -- AND AGM.PARAMETER         = 'POTIDAR'
  --and acu.condition_id = 57417
AND ame_utility_pkg.get_condition_description (acu.condition_id) LIKE '%LARS%' ;
SELECT *
FROM HR.AME_APPROVAL_GROUPS_TL AG,
  AME_APPROVAL_GROUP_MEMBERS AGM,
  AME_ACTIONS ACT,
  ame_action_usages aau,
  ame_rules ar
WHERE 0                           =0
AND AG.APPROVAL_GROUP_ID          = AGM.APPROVAL_GROUP_ID
AND TO_CHAR(AG.APPROVAL_GROUP_ID) = ACT.PARAMETER
  --AND AGM.PARAMETER         = 'PONGRUTP'
AND TRUNC(ACT.START_DATE) <= sysdate
AND (TRUNC(ACT.END_DATE)  >= sysdate
OR ACT.END_DATE           IS NULL)
AND act.action_id          = aau.action_id
AND ( (SYSDATE BETWEEN aau.start_date AND NVL (aau.end_date - (1 / 86400), SYSDATE))
OR (SYSDATE         < aau.start_date
AND aau.start_date  < NVL (aau.end_date, aau.start_date + (1 / 86400))))
AND aau.rule_id     = ar.rule_id(+)
AND AR.DESCRIPTION IS NULL
  --and AG.USER_APPROVAL_GROUP_NAME like 'Y16 DG%'
  ;
SELECT aau.*,
  ACT.START_DATE,
  ACT.END_DATE,
  AAU.RULE_ID,
  AG.USER_APPROVAL_GROUP_NAME,
  AG.DESCRIPTION GROUP_DESC,
  ACT.DESCRIPTION,
  AGM.PARAMETER
FROM AME_ACTIONS ACT,
  ame_action_usages aau,
  AME_APPROVAL_GROUPS_TL AG,
  AME_APPROVAL_GROUP_MEMBERS AGM
WHERE 0                           =0
AND act.action_id                 = aau.action_id(+)
AND AG.APPROVAL_GROUP_ID          = AGM.APPROVAL_GROUP_ID
AND TO_CHAR(AG.APPROVAL_GROUP_ID) = ACT.PARAMETER
AND TRUNC(ACT.START_DATE)        <= sysdate
AND (TRUNC(ACT.END_DATE)         >= sysdate
OR ACT.END_DATE                  IS NULL)
AND AAU.START_DATE               <= sysdate
AND AAU.END_DATE                 <= sysdate
and ACT.DESCRIPTION like '%Parinya%'
--and AG.DESCRIPTION = 'PARINYA, Mr. JUTASEN'
--AND ame_utility_pkg.get_action_description (act.action_id) LIKE '%PM_Parinya_Jutasen'
ORDER BY ACT.DESCRIPTION;


select * from AME_APPROVAL_GROUP_MEMBERS where parameter = 'KITTIK';

select * from AME_APPROVAL_GROUPS_TL where approval_group_id in (508173,
232188);

select * from AME_ACTIONS where PARAMETER = '473173';

select * from ame_action_usages where action_id = '424762';

SELECT * FROM HR.AME_CONDITIONS where CONDITION_ID = 601536;


select * from APPLSYS.FND_USER where USER_ID = 6615;