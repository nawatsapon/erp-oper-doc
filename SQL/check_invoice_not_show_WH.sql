SELECT AID.ROWID ROW_ID ,
  AI1.Invoice_num,
  AI2.Invoice_num p_invoice,
  AID.TAX_CODE_ID,
  TC.NAME VAT_CODE ,
  AID.GLOBAL_ATTRIBUTE12 GLOBAL_ATTRIBUTE12 ,
  AID.GLOBAL_ATTRIBUTE13 GLOBAL_ATTRIBUTE13 ,
  AID.GLOBAL_ATTRIBUTE14 GLOBAL_ATTRIBUTE14 ,
  AID.GLOBAL_ATTRIBUTE15 GLOBAL_ATTRIBUTE15 ,
  AID.GLOBAL_ATTRIBUTE16 GLOBAL_ATTRIBUTE16 ,
  AID.GLOBAL_ATTRIBUTE17 GLOBAL_ATTRIBUTE17 ,
  AID.GLOBAL_ATTRIBUTE18 GLOBAL_ATTRIBUTE18 ,
  AID.GLOBAL_ATTRIBUTE19 GLOBAL_ATTRIBUTE19 ,
  AID.GLOBAL_ATTRIBUTE20 GLOBAL_ATTRIBUTE20 ,
  AID.ACCOUNTING_EVENT_ID ACCOUNTING_EVENT_ID
  /*bug 4711763*/
  ,
  AID.ACCOUNTING_DATE ACCOUNTING_DATE ,
  AID.ACCRUAL_POSTED_FLAG ACCRUAL_POSTED_FLAG ,
  AID.ACCTS_PAY_CODE_COMBINATION_ID ACCTS_PAY_CODE_COMBINATION_ID ,
  AID.AMOUNT AMOUNT ,
  AID.ASSETS_ADDITION_FLAG ASSETS_ADDITION_FLAG ,
  AID.ASSETS_TRACKING_FLAG ASSETS_TRACKING_FLAG ,
  AID.ATTRIBUTE1 ATTRIBUTE1 ,
  AID.ATTRIBUTE10 ATTRIBUTE10 ,
  AID.ATTRIBUTE11 ATTRIBUTE11 ,
  AID.ATTRIBUTE12 ATTRIBUTE12 ,
  AID.ATTRIBUTE13 ATTRIBUTE13 ,
  AID.ATTRIBUTE14 ATTRIBUTE14 ,
  AID.ATTRIBUTE15 ATTRIBUTE15 ,
  AID.ATTRIBUTE2 ATTRIBUTE2 ,
  AID.ATTRIBUTE3 ATTRIBUTE3 ,
  AID.ATTRIBUTE4 ATTRIBUTE4 ,
  AID.ATTRIBUTE5 ATTRIBUTE5 ,
  AID.ATTRIBUTE6 ATTRIBUTE6 ,
  AID.ATTRIBUTE7 ATTRIBUTE7 ,
  AID.ATTRIBUTE8 ATTRIBUTE8 ,
  AID.ATTRIBUTE9 ATTRIBUTE9 ,
  AID.ATTRIBUTE_CATEGORY ATTRIBUTE_CATEGORY ,
  AID.AWT_FLAG AWT_FLAG ,
  AID.AWT_GROSS_AMOUNT AWT_GROSS_AMOUNT ,
  AID.AWT_GROUP_ID AWT_GROUP_ID ,
  AID.AWT_INVOICE_ID AWT_INVOICE_ID ,
  AID.AWT_ORIGIN_GROUP_ID AWT_ORIGIN_GROUP_ID ,
  AID.AWT_TAX_RATE_ID AWT_TAX_RATE_ID ,
  AID.BASE_AMOUNT BASE_AMOUNT ,
  AID.BASE_INVOICE_PRICE_VARIANCE BASE_INVOICE_PRICE_VARIANCE ,
  AID.BASE_QUANTITY_VARIANCE BASE_QUANTITY_VARIANCE ,
  AID.BATCH_ID BATCH_ID ,
  AID.CASH_JE_BATCH_ID CASH_JE_BATCH_ID ,
  AID.CASH_POSTED_FLAG CASH_POSTED_FLAG ,
  AID.CREATED_BY CREATED_BY ,
  AID.CREATION_DATE CREATION_DATE ,
  AID.DESCRIPTION DESCRIPTION ,
  AID.DISTRIBUTION_LINE_NUMBER DISTRIBUTION_LINE_NUMBER ,
  AID.DIST_CODE_COMBINATION_ID DIST_CODE_COMBINATION_ID ,
  AID.ENCUMBERED_FLAG ENCUMBERED_FLAG ,
  AID.EXCHANGE_DATE EXCHANGE_DATE ,
  AID.EXCHANGE_RATE EXCHANGE_RATE ,
  AID.EXCHANGE_RATE_TYPE EXCHANGE_RATE_TYPE ,
  AID.EXCHANGE_RATE_VARIANCE EXCHANGE_RATE_VARIANCE ,
  AID.EXPENDITURE_ITEM_DATE EXPENDITURE_ITEM_DATE ,
  AID.EXPENDITURE_ORGANIZATION_ID EXPENDITURE_ORGANIZATION_ID ,
  AID.EXPENDITURE_TYPE EXPENDITURE_TYPE ,
  AID.FINAL_MATCH_FLAG FINAL_MATCH_FLAG ,
  AID.INCOME_TAX_REGION INCOME_TAX_REGION ,
  AID.INVOICE_ID INVOICE_ID ,
  AID.INVOICE_PRICE_VARIANCE INVOICE_PRICE_VARIANCE ,
  AID.JE_BATCH_ID JE_BATCH_ID ,
  AID.LAST_UPDATED_BY LAST_UPDATED_BY ,
  AID.LAST_UPDATE_DATE LAST_UPDATE_DATE ,
  AID.LAST_UPDATE_LOGIN LAST_UPDATE_LOGIN ,
  AID.LINE_TYPE_LOOKUP_CODE LINE_TYPE_LOOKUP_CODE ,
  AID.MATCH_STATUS_FLAG MATCH_STATUS_FLAG ,
  AID.ORG_ID ORG_ID ,
  AID.OTHER_INVOICE_ID OTHER_INVOICE_ID ,
  AID.PACKET_ID PACKET_ID ,
  AID.PARENT_INVOICE_ID PARENT_INVOICE_ID ,
  AID.PA_ADDITION_FLAG PA_ADDITION_FLAG ,
  AID.PA_QUANTITY PA_QUANTITY ,
  AID.PERIOD_NAME PERIOD_NAME ,
  AID.POSTED_AMOUNT POSTED_AMOUNT ,
  AID.POSTED_BASE_AMOUNT POSTED_BASE_AMOUNT ,
  AID.POSTED_FLAG POSTED_FLAG ,
  AID.PO_DISTRIBUTION_ID PO_DISTRIBUTION_ID ,
  PD.DISTRIBUTION_NUM PO_DISTRIBUTION_NUMBER ,
  NVL( AID.PREPAY_AMOUNT_REMAINING, DECODE( AI1.INVOICE_TYPE_LOOKUP_CODE, 'PREPAYMENT', DECODE( AI1.PAYMENT_STATUS_FLAG, 'Y', DECODE( AID.LINE_TYPE_LOOKUP_CODE,'ITEM', AID.AMOUNT, AID.PREPAY_AMOUNT_REMAINING ), AID.PREPAY_AMOUNT_REMAINING ), AID.PREPAY_AMOUNT_REMAINING ) ) PREPAY_AMOUNT_REMAINING ,
  AID.PRICE_VAR_CODE_COMBINATION_ID PRICE_VAR_CODE_COMBINATION_ID ,
  AID.PROGRAM_APPLICATION_ID PROGRAM_APPLICATION_ID ,
  AID.PROGRAM_ID PROGRAM_ID ,
  AID.PROGRAM_UPDATE_DATE PROGRAM_UPDATE_DATE ,
  AID.PROJECT_ACCOUNTING_CONTEXT PROJECT_ACCOUNTING_CONTEXT ,
  AID.PROJECT_ID PROJECT_ID ,
  AID.QUANTITY_INVOICED QUANTITY_INVOICED ,
  AID.QUANTITY_VARIANCE QUANTITY_VARIANCE ,
  AID.RATE_VAR_CODE_COMBINATION_ID RATE_VAR_CODE_COMBINATION_ID ,
  AID.REFERENCE_1 REFERENCE_1 ,
  AID.REFERENCE_2 REFERENCE_2 ,
  AID.REQUEST_ID REQUEST_ID ,
  AID.REVERSAL_FLAG REVERSAL_FLAG ,
  AID.SET_OF_BOOKS_ID SET_OF_BOOKS_ID ,
  AID.STAT_AMOUNT STAT_AMOUNT ,
  AID.TASK_ID TASK_ID ,
  AID.TYPE_1099 TYPE_1099 ,
  AID.UNIT_PRICE UNIT_PRICE ,
  AID.USSGL_TRANSACTION_CODE USSGL_TRANSACTION_CODE ,
  AID.USSGL_TRX_CODE_CONTEXT USSGL_TRX_CODE_CONTEXT ,
  AI1.INVOICE_NUM INVOICE_NUM ,
  AI2.INVOICE_NUM PARENT_INVOICE_NUM ,
  AB.BATCH_NAME BATCH_NAME ,
  AITR.REGION_LONG_NAME INCOME_TAX_REGION_NAME ,
  AP_INVOICE_DISTRIBUTIONS_PKG.GET_POSTED_STATUS( AID.ACCRUAL_POSTED_FLAG, AID.CASH_POSTED_FLAG, AID.POSTED_FLAG) POSTED_STATUS ,
  ALC.DISPLAYED_FIELD LINE_TYPE ,
  ALC2.DISPLAYED_FIELD STATUS ,
  ALC3.DISPLAYED_FIELD AWT_CREATION_METHOD ,
  ALC4.DISPLAYED_FIELD POSTED_STATUS_DISP ,
  AWT.NAME AWT_GROUP_NAME ,
  GJB1.NAME JE_BATCH_NAME ,
  GJB2.NAME CASH_JE_BATCH_NAME ,
  GSOB.CHART_OF_ACCOUNTS_ID CHART_OF_ACCOUNTS_ID ,
  GSOB.NAME SET_OF_BOOKS_NAME ,
  GSOB.SHORT_NAME SET_OF_BOOKS_SHORT_NAME ,
  PAP.SEGMENT1 PROJECT ,
  PAT.TASK_NUMBER TASK ,
  HOU.NAME EXPENDITURE_ORGANIZATION_NAME ,
  PD.ACCRUE_ON_RECEIPT_FLAG PO_ACCRUE_ON_RECEIPT_FLAG ,
  PD.PO_HEADER_ID PO_HEADER_ID ,
  PD.PO_LINE_ID PO_LINE_ID ,
  PL.LINE_NUM PO_LINE_NUMBER ,
  PD.LINE_LOCATION_ID LINE_LOCATION_ID ,
  PLL.SHIPMENT_NUM PO_LINE_LOCATION_NUMBER ,
  PH.SEGMENT1 PO_NUMBER ,
  PL.UNIT_MEAS_LOOKUP_CODE UNIT_MEAS_LOOKUP_CODE ,
  TC.TAX_TYPE TAX_TYPE ,
  AP_INVOICE_DISTRIBUTIONS_PKG.GET_UOM( AID.DIST_CODE_COMBINATION_ID, GSOB.CHART_OF_ACCOUNTS_ID) UNIT_OF_MEASURE ,
  DECODE(AID.TAX_CALCULATED_FLAG, 'Y', 'D', DECODE(AID.LINE_TYPE_LOOKUP_CODE, 'TAX', 'D', NVL(AID.AMOUNT_INCLUDES_TAX_FLAG,'D'))) ,
  AID.TAX_RECOVERY_RATE ,
  AID.TAX_RECOVERY_OVERRIDE_FLAG ,
  AID.TAX_RECOVERABLE_FLAG ,
  AID.TAX_CODE_OVERRIDE_FLAG ,
  AID.TAX_CALCULATED_FLAG ,
  PD.CODE_COMBINATION_ID PO_CODE_COMBINATION_ID ,
  AID.GLOBAL_ATTRIBUTE_CATEGORY GLOBAL_ATTRIBUTE_CATEGORY ,
  AID.GLOBAL_ATTRIBUTE1 GLOBAL_ATTRIBUTE1 ,
  AID.GLOBAL_ATTRIBUTE2 GLOBAL_ATTRIBUTE2 ,
  AID.GLOBAL_ATTRIBUTE3 GLOBAL_ATTRIBUTE3 ,
  AID.GLOBAL_ATTRIBUTE4 GLOBAL_ATTRIBUTE4 ,
  AID.GLOBAL_ATTRIBUTE5 GLOBAL_ATTRIBUTE5 ,
  AID.GLOBAL_ATTRIBUTE6 GLOBAL_ATTRIBUTE6 ,
  AID.GLOBAL_ATTRIBUTE7 GLOBAL_ATTRIBUTE7 ,
  AID.GLOBAL_ATTRIBUTE8 GLOBAL_ATTRIBUTE8 ,
  AID.GLOBAL_ATTRIBUTE9 GLOBAL_ATTRIBUTE9 ,
  AID.GLOBAL_ATTRIBUTE10 GLOBAL_ATTRIBUTE10 ,
  AID.GLOBAL_ATTRIBUTE11 GLOBAL_ATTRIBUTE11 ,
  PLL.PO_RELEASE_ID ,
  PD.DESTINATION_TYPE_CODE ,
  AID.INVOICE_DISTRIBUTION_ID ,
  AID.MATCHED_UOM_LOOKUP_CODE ,
  RTXNS.TRANSACTION_DATE ,
  AID.RCV_TRANSACTION_ID ,
  RSH.RECEIPT_NUM ,
  RSH.SHIPMENT_HEADER_ID ,
  RSL.LINE_NUM ,
  RSL.SHIPMENT_LINE_ID ,
  AID.TAX_CODE_ID ,
  AID.AWARD_ID ,
  AID.GMS_BURDENABLE_RAW_COST ,
  AID.PREPAY_DISTRIBUTION_ID ,
  AID.PREPAY_TAX_PARENT_ID ,
  AP_INVOICES_UTILITY_PKG.GET_PREPAY_NUMBER(AID.PREPAY_DISTRIBUTION_ID) ,
  AP_INVOICES_UTILITY_PKG.GET_PREPAY_DIST_NUMBER(AID.PREPAY_DISTRIBUTION_ID) ,
  NVL(AID.INVOICE_INCLUDES_PREPAY_FLAG,'N') ,
  AID.START_EXPENSE_DATE ,
  AID.MERCHANT_DOCUMENT_NUMBER ,
  AID.MERCHANT_NAME ,
  AID.MERCHANT_REFERENCE ,
  AID.MERCHANT_TAX_REG_NUMBER ,
  AID.MERCHANT_TAXPAYER_ID ,
  AID.COUNTRY_OF_SUPPLY ,
  AID.PARENT_REVERSAL_ID ,
  PLL.CLOSED_CODE ,
  AID.PRICE_CORRECT_INV_ID ,
  AID.PRICE_CORRECT_QTY ,
  PLT.MATCHING_BASIS
  /*Bug:2953946*/
  ,
  AID.DIST_MATCH_TYPE
  /*Bug:2953946*/
  ,
  AID.AMOUNT_VARIANCE
  /*Bug:2953946*/
  ,
  AID.BASE_AMOUNT_VARIANCE
  /*Bug:2953946*/
  ,
  AID.WEB_PARAMETER_ID
  /*3637618*/
FROM
  (SELECT
    CASE
      WHEN GLOBAL_ATTRIBUTE12 IS NULL
      OR GLOBAL_ATTRIBUTE13   IS NULL
      OR GLOBAL_ATTRIBUTE14   IS NULL
      OR GLOBAL_ATTRIBUTE15   IS NULL
      --OR GLOBAL_ATTRIBUTE17   IS NULL
      --OR GLOBAL_ATTRIBUTE18   IS NULL
      THEN 'FALSE'
      ELSE 'TRUE'
    END IS_NULL_WH,
    AP_INVOICE_DISTRIBUTIONS_ALL.*
  FROM AP_INVOICE_DISTRIBUTIONS_ALL
  ) AID,
  AP_INVOICES_ALL AI1,
  AP_INVOICES_ALL AI2,
  AP_AWT_GROUPS AWT,
  AP_BATCHES AB,
  AP_INCOME_TAX_REGIONS AITR,
  AP_LOOKUP_CODES ALC,
  AP_LOOKUP_CODES ALC2,
  AP_LOOKUP_CODES ALC3,
  AP_LOOKUP_CODES ALC4,
  AP_TAX_CODES_ALL TC,
  GL_JE_BATCHES GJB1,
  GL_JE_BATCHES GJB2,
  GL_SETS_OF_BOOKS GSOB,
  HR_ORGANIZATION_UNITS HOU,
  PA_PROJECTS_ALL PAP,
  PA_TASKS PAT,
  PO_DISTRIBUTIONS PD,
  PO_HEADERS PH,
  PO_LINES PL,
  PO_LINE_TYPES PLT,
  /*Bug:2953946*/
  PO_LINE_LOCATIONS PLL,
  RCV_TRANSACTIONS RTXNS,
  RCV_SHIPMENT_HEADERS RSH,
  RCV_SHIPMENT_LINES RSL
WHERE AID.INVOICE_ID                = AI1.INVOICE_ID
AND AID.PARENT_INVOICE_ID           = AI2.INVOICE_ID (+)
AND AID.BATCH_ID                    = AB.BATCH_ID (+)
AND AID.INCOME_TAX_REGION           = AITR.REGION_SHORT_NAME (+)
AND ALC.LOOKUP_TYPE (+)             = 'INVOICE DISTRIBUTION TYPE'
AND ALC.LOOKUP_CODE (+)             = AID.LINE_TYPE_LOOKUP_CODE
AND ALC2.LOOKUP_TYPE (+)            = 'NLS TRANSLATION'
AND ALC2.LOOKUP_CODE (+)            = DECODE(AID.MATCH_STATUS_FLAG , NULL,'NEVER APPROVED', 'N', 'NEVER APPROVED', 'T', 'NEEDS REAPPROVAL', 'A', 'APPROVED', 'S', 'NEVER APPROVED')
AND ALC3.LOOKUP_TYPE (+)            = 'AWT FLAG'
AND ALC3.LOOKUP_CODE (+)            = AID.AWT_FLAG
AND ALC4.LOOKUP_TYPE (+)            = 'POSTING STATUS'
AND ALC4.LOOKUP_CODE (+)            = AP_INVOICE_DISTRIBUTIONS_PKG .GET_POSTED_STATUS( AID.ACCRUAL_POSTED_FLAG, AID.CASH_POSTED_FLAG, AID.POSTED_FLAG)
AND AID.JE_BATCH_ID                 = GJB1.JE_BATCH_ID (+)
AND AID.CASH_JE_BATCH_ID            = GJB2.JE_BATCH_ID (+)
AND AID.SET_OF_BOOKS_ID             = GSOB.SET_OF_BOOKS_ID
AND AID.PROJECT_ID                  = PAP.PROJECT_ID (+)
AND AID.TASK_ID                     = PAT.TASK_ID (+)
AND AID.EXPENDITURE_ORGANIZATION_ID = HOU.ORGANIZATION_ID (+)
AND AID.PO_DISTRIBUTION_ID          = PD.PO_DISTRIBUTION_ID (+)
AND PD.PO_HEADER_ID                 = PH.PO_HEADER_ID (+)
AND PD.LINE_LOCATION_ID             = PLL.LINE_LOCATION_ID (+)
AND PLL.PO_LINE_ID                  = PL.PO_LINE_ID (+)
  /*Bug:2953946*/
AND PL.LINE_TYPE_ID        = PLT.LINE_TYPE_ID(+)
AND AID.AWT_GROUP_ID       = AWT.GROUP_ID (+)
AND AID.TAX_CODE_ID        = TC.TAX_ID (+)
AND AID.RCV_TRANSACTION_ID = RTXNS.TRANSACTION_ID (+)
AND RTXNS.SHIPMENT_LINE_ID = RSL.SHIPMENT_LINE_ID (+)
AND RSL.SHIPMENT_HEADER_ID = RSH.SHIPMENT_HEADER_ID (+)
AND AID.invoice_id        IN
  (SELECT AP_INVOICE_PAYMENTS_ALL.INVOICE_ID
  FROM AP.AP_INVOICE_PAYMENTS_ALL,
    AP.AP_CHECKS_ALL
  WHERE PERIOD_NAME = 'DEC-16'
    --AND INVOICE_ID      = 1533755
  AND (REVERSAL_FLAG                  IS NULL
  OR REVERSAL_FLAG                     ='N')
  AND AP_INVOICE_PAYMENTS_ALL.check_id = AP_CHECKS_ALL.check_id
  --AND AP_CHECKS_ALL.CHECK_NUMBER       = '10205324'
  AND AP_CHECKS_ALL.CHECK_NUMBER is not null
  )
AND AID.LINE_TYPE_LOOKUP_CODE = 'ITEM'
AND AID.TAX_CODE_ID           = 10002
--AND AID.GLOBAL_ATTRIBUTE12   IS NULL
AND IS_NULL_WH = 'FALSE'
AND AID.AWT_GROUP_ID is not null
  ;