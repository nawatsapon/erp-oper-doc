SELECT NVL(SUM(NVL(moq.transaction_quantity,0)),0) sum_qty
FROM mtl_onhand_quantities moq
WHERE moq.organization_id   = 298 -- N02
AND moq.inventory_item_id   = 479748 -- BSUS0640001
AND moq.SUBINVENTORY_CODE   = 'N3G02A'
AND NVL(moq.LOCATOR_ID,-99) = 3752 -- N3G02A02
UNION ALL
SELECT SUM(moq.primary_quantity)
FROM mtl_transactions_interface moq
WHERE moq.organization_id   = 298
AND moq.inventory_item_id   = 479748
AND moq.SUBINVENTORY_CODE   = 'N3G02A'
AND NVL(moq.LOCATOR_ID,-99) = 3752
UNION ALL
SELECT SUM(moq.primary_quantity)
FROM mtl_material_transactions_temp moq
WHERE moq.organization_id   = 298
AND moq.inventory_item_id   = 479748
AND moq.SUBINVENTORY_CODE   = 'N3G02A'
AND NVL(moq.LOCATOR_ID,-99) = 3752