UPDATE WSH_TRIP_STOPS
SET PENDING_INTERFACE_FLAG = 'N'
WHERE STOP_ID             IN
  (SELECT WTS.STOP_ID--count(*)
    --    into l_untrxd_rec_count
  FROM WSH_DELIVERY_DETAILS WDD,
    WSH_DELIVERY_ASSIGNMENTS WDA,
    WSH_NEW_DELIVERIES WND,
    WSH_DELIVERY_LEGS WDL,
    WSH_TRIP_STOPS WTS
  WHERE WDD.SOURCE_CODE        = 'OE'
  AND WDD.RELEASED_STATUS      = 'C'
  AND WDD.INV_INTERFACED_FLAG IN ('N' ,'P')
    --        and wdd.organization_id = :l_organization_id
  AND WDA.DELIVERY_DETAIL_ID      = WDD.DELIVERY_DETAIL_ID
  AND WND.DELIVERY_ID             = WDA.DELIVERY_ID
  AND WND.STATUS_CODE            IN ('CL','IT')
  AND WDL.DELIVERY_ID             = WND.DELIVERY_ID
  AND WTS.PENDING_INTERFACE_FLAG IN ('Y', 'P')
    --and trunc(wts.actual_departure_date) between :l_closing_fm_date and :l_closing_to_date
  AND WDL.PICK_UP_STOP_ID    = WTS.STOP_ID
  AND WDA.DELIVERY_DETAIL_ID = 1262092 -- Delivery NO.
  ) ;
UPDATE WSH_DELIVERY_DETAILS
SET INV_INTERFACED_FLAG = 'Y',
  OE_INTERFACED_FLAG    = 'Y',
  WV_FROZEN_FLAG        = 'Y'
WHERE 0                 =0
  --and OE_INTERFACED_FLAG = 'N'
  --AND INV_INTERFACED_FLAG = 'N'
  --INV_INTERFACED_FLAG = 'P' andto_char(DATE_SCHEDULED,'YYYYMM') = '201711'
AND INVENTORY_ITEM_ID  = 943754
AND CUSTOMER_ID        = 511224
AND DELIVERY_DETAIL_ID = 1262092 -- Delivery NO.
  ;
SELECT DELIVERY_DETAIL_ID,
  OE_INTERFACED_FLAG,
  INV_INTERFACED_FLAG,
  WV_FROZEN_FLAG
FROM wsh_delivery_details
  --set INV_INTERFACED_FLAG = 'Y',WV_FROZEN_FLAG = 'Y', OE_INTERFACED_FLAG = 'Y'
WHERE 0=0
  --and OE_INTERFACED_FLAG = 'N'
  --AND INV_INTERFACED_FLAG = 'N'
  --INV_INTERFACED_FLAG = 'P' andto_char(DATE_SCHEDULED,'YYYYMM') = '201711'
AND INVENTORY_ITEM_ID  = 943754
AND CUSTOMER_ID        = 511224
AND DELIVERY_DETAIL_ID = 1262092 -- Delivery NO.
  ;
SELECT pending_interface_flag,
  wsh_trip_stops.*
FROM wsh_trip_stops
WHERE stop_id IN
  (SELECT wts.stop_id--count(*)
    --    into l_untrxd_rec_count
  FROM wsh_delivery_details wdd,
    WSH_DELIVERY_ASSIGNMENTS wda,
    wsh_new_deliveries wnd,
    wsh_delivery_legs wdl,
    wsh_trip_stops wts
  WHERE wdd.source_code        = 'OE'
  AND wdd.released_status      = 'C'
  AND wdd.inv_interfaced_flag IN ('N' ,'P')
    --        and wdd.organization_id = :l_organization_id
  AND wda.delivery_detail_id      = wdd.delivery_detail_id
  AND wnd.delivery_id             = wda.delivery_id
  AND wnd.status_code            IN ('CL','IT')
  AND wdl.delivery_id             = wnd.delivery_id
  AND wts.pending_interface_flag IN ('Y', 'P')
    --and trunc(wts.actual_departure_date) between :l_closing_fm_date and :l_closing_to_date
  AND wdl.pick_up_stop_id    = wts.stop_id
  AND wda.delivery_detail_id = 1262092 -- Delivery NO.
  ) ;
SELECT pending_interface_flag,
  wsh_trip_stops.*
FROM wsh_trip_stops
WHERE stop_id = 1113999;
UPDATE wsh_trip_stops
SET pending_interface_flag = 'Y'
WHERE stop_id              = 1113999;
SELECT PRIMARY_RESERVATION_QUANTITY,
  RESERVATION_QUANTITY
FROM MTL_RESERVATIONS
WHERE reservation_id = 2319715;  -- RESERVATION_ID
UPDATE MTL_RESERVATIONS
SET PRIMARY_RESERVATION_QUANTITY = 0,
  RESERVATION_QUANTITY           =0
WHERE RESERVATION_ID             = 2319715; -- RESERVATION_ID
SELECT PRIMARY_UOM_QUANTITY ,
  LINE_ITEM_QUANTITY ,
  COMPLETED_QUANTITY ,
  LINE_ITEM_RESERVATION_QTY ,
  RESERVATION_QUANTITY,
  MTL_DEMAND.*
FROM MTL_DEMAND
WHERE DEMAND_SOURCE_LINE = 979273; -- Sale Order Line_ID
UPDATE MTL_DEMAND
SET PRIMARY_UOM_QUANTITY =0,
  LINE_ITEM_QUANTITY     = 0
WHERE DEMAND_SOURCE_LINE = 979273; -- Sale Order Line_ID