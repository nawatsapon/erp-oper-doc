SELECT aa.*
FROM
  (SELECT fcr.request_id ID ,
    fl.meaning "Status",
    fcpv.concurrent_program_name "Short Name" ,
    fcpv.user_concurrent_program_name "Long Name" ,
    fcr.requested_by "Requested By ID",
    fu.user_name "Requestor Username",
    TO_CHAR(fcr.actual_start_date, 'DD-MON-RR HH24:MI:SS') "Start" ,
    TO_CHAR(fcr.ACTUAL_COMPLETION_DATE, 'DD-MON-RR HH24:MI:SS') "Completion" ,
    DECODE (
    (SELECT RTRIM(LTRIM(REGEXP_SUBSTR (fcrf.argument_text,'[^,]+',1,24))) FROM dual
    ),'RELEASE',
    (SELECT hr.NAME
      ||': '
      || poh.segment1
      ||'-'
      || por.release_num doc_num
    FROM po_headers_all poh,
      po_releases_all por,
      hr_all_organization_units hr
    WHERE poh.org_id      = hr.organization_id
    AND por.org_id        = poh.org_id
    AND poh.po_header_id  = por.po_header_id
    AND por.po_release_id = to_number(RTRIM(LTRIM(REGEXP_SUBSTR (fcrf.argument_text,'[^,]+',1,21))) )
    ),
    (SELECT poh.segment1
      ||': '
      || hr.NAME doc_num
    FROM po_headers_all poh,
      hr_all_organization_units hr
    WHERE poh.org_id     = hr.organization_id
    AND poh.po_header_id = to_number(RTRIM(LTRIM(REGEXP_SUBSTR (fcrf.argument_text,'[^,]+',1,21))) )
    )) as subject ,
    fcrf.argument_text "Parameters"
  FROM fnd_concurrent_requests fcr,
    fnd_concurrent_programs_vl fcpv,
    FND_CONC_REQUESTS_FORM_V fcrf,
    fnd_lookups fl,
    fnd_user fu
  WHERE fl.LOOKUP_TYPE             = 'CP_STATUS_CODE'
  AND fl.lookup_code               = fcr.status_code
  AND fcr.concurrent_program_id    = fcpv.concurrent_program_id
  AND fcr.program_application_id   = fcpv.application_id
  AND FCR.REQUEST_ID               = FCRF.REQUEST_ID
  AND fcr.actual_start_date        > SYSDATE - 31 -- alter 31 if a shorter or longer time period is required
  AND fcpv.concurrent_program_name = 'POXPOPDF'   -- PO Output for Communication
  AND fl.lookup_code NOT          IN ('C','I')    -- 'Normal' & ' Normal'  <<-- Return requests that did not complete as normal
    --AND fl.lookup_code  IN ('C','I') -- 'Normal' & ' Normal'  <<-- Return requests that did complete as normal
  AND fcr.requested_by = fu.user_id(+)
  ORDER BY fcr.actual_start_date,
    fcr.request_id
  ) aa
WHERE 0=0
--order by subject
and aa.subject LIKE '%8017008986%'
;


SELECT wf_item_type, wf_item_key
FROM po_headers
WHERE segment1 = 391359; 
