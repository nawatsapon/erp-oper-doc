SELECT CUST_ACCT.Account_Number customer_code,
  Substrb(PARTY.Party_Name,1,50) customer_name,
  WDD.Source_Header_Number order_number,
  OOHA.Ordered_Date order_date,
  OTTT.name order_type,
  DECODE(OTTT.Name, 'S99-INT-HO', OOHA.Orig_Sys_Document_Ref, OOHA.Cust_PO_Number) PO_IR_Number,
  mp.organization_code ORG,
  WND.Attribute1 Despatch_No,
  WTS.Actual_Departure_Date Despatch_date,
  WND.Name Delivery_name,
  WDD.SubInventory SubInventory,
  DECODE(WDD.Locator_ID, NULL, '-', INV_Project.Get_LocSegS(WDD.Locator_ID, WDD.ORGANIZATION_ID )) Locator,
  msi.segment1 Item_code,
  msi.description Item_description,
  -- WND.Volume_UOM_Code UOM,
  NVL(WDD.Picked_Quantity,0) Order_Qty,
  NVL(WDD.Shipped_Quantity,0) Shipped_Qty,
  (NVL(WDD.Picked_Quantity, 0) - NVL(WDD.Shipped_Quantity, 0)) Pending_Qty,
  WDD.Gross_Weight Weight,
  WDD.Weight_UOM_Code UOM,
  bill_su.location Bill_To_Location ,
  bill_loc.address1 Bill_To_Address1,
  ship_su.location Ship_To_Location,
  ship_loc.address1 Ship_To_Address1,
  wpb.creation_date released_date,
  OOHA.creation_date order_creation_date
FROM HZ_PARTIES PARTY,
  HZ_CUST_ACCOUNTS CUST_ACCT,
  WSH_TRIP_STOPS WTS,
  WSH_TRIPS WT,
  WSH_DELIVERY_DETAILS WDD,
  WSH_DELIVERY_ASSIGNMENTS WDA,
  WSH_DELIVERY_LEGS WDL,
  WSH_NEW_DELIVERIES WND,
  ORG_ORGANIZATION_DEFINITIONS OOD,
  oe_transaction_types_all ott,
  MTL_PARAMETERS MP,
  mtl_system_items msi,
  mtl_item_categories mic,
  mtl_categories mc,
  WSH_LOCATIONS_V WLV,
  OE_Order_Headers_All OOHA,
  OE_Transaction_Types_TL OTTT,
  hz_cust_acct_sites_all bill_cas,
  hz_cust_site_uses_all bill_su,
  hz_party_sites bill_party_site,
  hz_locations bill_loc,
  hz_cust_acct_sites_all ship_cas,
  hz_cust_site_uses_all ship_su,
  hz_party_sites ship_party_site,
  hz_locations ship_loc,
  wsh_picking_batches wpb
WHERE WDD.ORGANIZATION_ID             = OOD.ORGANIZATION_ID
AND WDD.DELIVERY_DETAIL_ID            = WDA.DELIVERY_DETAIL_ID
AND WDA.DELIVERY_ID                   = WND.DELIVERY_ID
AND WDA.DELIVERY_ID                   = WDL.DELIVERY_ID
AND WDL.PICK_UP_STOP_ID               = WTS.STOP_ID
AND NVL(wnd.shipment_direction, 'O') IN ('O', 'IO')
AND WTS.TRIP_ID                       = WT.TRIP_ID
AND WTS.STOP_LOCATION_ID              = WDD.SHIP_FROM_LOCATION_ID
AND CUST_ACCT.PARTY_ID                = PARTY.PARTY_ID(+)
AND WDD.CUSTOMER_ID                   = CUST_ACCT.CUST_ACCOUNT_ID(+)
AND WDA.DELIVERY_ID                  IS NOT NULL
AND WDD.RELEASED_STATUS              IN ( 'C','I')
AND WDD.CONTAINER_FLAG                = 'N'
AND WDD.Organization_ID               =mp.organization_id
AND WDD.Inventory_Item_ID             =msi.Inventory_Item_ID
AND WDD.Organization_id               =msi.organization_id
AND msi.inventory_item_id             =mic.inventory_item_id
AND msi.organization_id               =mic.organization_id
AND mic.category_id                   =mc.category_id
AND mic.CATEGORY_SET_ID               =1
AND WDD.Source_Header_Type_id         = ott.transaction_type_id(+)
AND WDD.SHIP_TO_LOCATION_ID           = WLV.WSH_LOCATION_ID(+)
AND WDD.Source_Header_Number          = OOHA.Order_Number(+)
AND OOHA.Order_Type_ID                = OTTT.Transaction_Type_ID(+)
AND OTT.Transaction_Type_ID           =OTTT.Transaction_Type_ID
AND msi.organization_id               =mp.organization_id
AND ooha.invoice_to_org_id            = bill_su.site_use_id(+)
AND bill_su.cust_acct_site_id         = bill_cas.cust_acct_site_id(+)
AND bill_cas.party_site_id            = bill_party_site.party_site_id
AND bill_party_site.LOCATION_ID       = bill_loc.LOCATION_ID
AND ooha.ship_to_org_id               = ship_su.site_use_id(+)
AND ship_su.cust_acct_site_id         = ship_cas.cust_acct_site_id(+)
AND ship_cas.party_site_id            = ship_party_site.party_site_id
AND ship_party_site.LOCATION_ID       = ship_loc.LOCATION_ID
AND wdd.batch_id                      = wpb.batch_id(+)
  --and trunc(WDD.DATE_SCHEDULED) between to_date('01/01/2015','dd/mm/yyyy') and to_date('31/12/2015','dd/mm/yyyy')
AND trunc(WTS.Actual_Departure_Date) between to_date('01/01/2015','dd/mm/yyyy') and to_date('31/12/2015','dd/mm/yyyy')
AND msi.segment1              IN ('S18WP800001', 'S18WP950001', 'S18WPA13001', 'S18WPA23001', 'S18WPA31001', 'S18WPA32001', 'S18WPA73001', 'S18WPA74001', 'S18WPA88001', 'S18WPA89001', 'S18WPA90001', 'S18WPA91001', 'S18WPA92001', 'S18WPB24001', 'S18WPB25001', 'S18WPB26001', 'S18WPB50001', 'PRSM0000551', 'PRSM0000671', 'PRSM0000801', 'S18WPB61001', 'S18WPB61002', 'S18WPB61003', 'S18WPB62001') --item
ORDER BY CUST_ACCT.Account_Number;