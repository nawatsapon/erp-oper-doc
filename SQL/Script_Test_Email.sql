DECLARE
  l_mail_conn UTL_SMTP.connection;
  p_to        VARCHAR2(30) := 'tantida.yodhasmutr@dtac.co.th';
  p_from      VARCHAR2(80) := 'tantida.yodhasmutr@dtac.co.th';
  p_message   VARCHAR2(80) := 'test';
  --p_smtp_host VARCHAR2(30) := 'gemini7.tac.co.th';
  p_smtp_host VARCHAR2(30) := 'mail-gw.tac.co.th';
BEGIN
  l_mail_conn := UTL_SMTP.open_connection(p_smtp_host, 25);
  UTL_SMTP.helo(l_mail_conn, p_smtp_host);
  UTL_SMTP.mail(l_mail_conn, p_from);
  UTL_SMTP.rcpt(l_mail_conn, p_to);
  UTL_SMTP.data(l_mail_conn, p_message || UTL_TCP.crlf || UTL_TCP.crlf);
  UTL_SMTP.quit(l_mail_conn);
END;