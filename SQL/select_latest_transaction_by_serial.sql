SELECT *
FROM
  (SELECT ROW_NUMBER() OVER (PARTITION BY Serial_Number ORDER BY MTL_MATERIAL_TRANSACTIONS.TRANSACTION_ID DESC) AS trans_id,
    MTL_MATERIAL_TRANSACTIONS.TRANSACTION_ID,
    Serial_Number,
    TRANSACTION_DATE,
    SUBINVENTORY_CODE,
    MTL_MATERIAL_TRANSACTIONS.INVENTORY_ITEM_ID,
    Mtl_Item.INVENTORY_ITEM_CODE,
    transaction_source_name,
    Mtl_Trx_Types_View.Transaction_Type_Name,
    MTL_TRX_TYPES_VIEW.DESCRIPTION
  FROM MTL_MATERIAL_TRANSACTIONS,
    MTL_TRX_TYPES_VIEW,
    (select distinct INVENTORY_ITEM_ID,segment1 INVENTORY_ITEM_CODE from INV.Mtl_System_Items_B) mtl_item,
    (SELECT transaction_id,
      A.Serial_Number
    FROM mtl_unit_transactions a,
      mtl_system_items item,
      Xx_Temp_Serial
    WHERE 0             =0
    AND a.serial_number = Xx_Temp_Serial.Serial
      --and a.serial_number        = '012845001434392'
    AND a.organization_id = 198
      --AND a.inventory_item_id      = 511759
    AND (item.organization_id+0) = 198
    AND item.inventory_item_id   = a.inventory_item_id
    AND item.lot_control_code    = 1
      --AND a.transaction_date      >= to_date('14-12-2013 00:00:00','DD-MM-YYYY HH24:MI:SS')
    ) mtl_serial
  WHERE Mtl_Material_Transactions.TRANSACTION_ACTION_ID NOT IN (24,30)  
  AND Mtl_Material_Transactions.Transaction_Type_Id = MTL_TRX_TYPES_VIEW.TRANSACTION_TYPE_ID(+)
  AND Mtl_Material_Transactions.TRANSACTION_SOURCE_TYPE_ID = MTL_TRX_TYPES_VIEW.TRANSACTION_SOURCE_TYPE_ID(+)
  AND Mtl_Material_Transactions.Transaction_Action_Id = MTL_TRX_TYPES_VIEW.TRANSACTION_ACTION_ID(+)
  AND (( ORGANIZATION_ID           = 198 )
    --AND ( INVENTORY_ITEM_ID                       = 511759)
  AND (MTL_MATERIAL_TRANSACTIONS.transaction_id = mtl_serial.transaction_id
  and MTL_MATERIAL_TRANSACTIONS.Inventory_Item_Id = Mtl_Item.INVENTORY_ITEM_ID
    --AND transaction_date                         >= to_date('14-12-2013 00:00:00','DD-MM-YYYY HH24:MI:SS')
    )
  AND (LOGICAL_TRANSACTION =2
  OR LOGICAL_TRANSACTION  IS NULL ))
  ORDER BY TRANSACTION_DATE DESC,
    TRANSACTION_ID DESC,
    SUBINVENTORY_CODE
  )
WHERE trans_id = 1 
order by 4
;