CREATE OR REPLACE PROCEDURE TAC_CHECK_AME_RULE_BY_USER(
    err_msg OUT VARCHAR2 ,
    err_code OUT VARCHAR2
    ,p_username IN VARCHAR2,
    p_msg_proj IN VARCHAR2,
    p_msg_fin IN VARCHAR2 
    )
AS
  i INTEGER;

--p_username VARCHAR2(20) := 'T551389';
--p_msg_proj VARCHAR2(1000) := 'costing team parameter';
--p_msg_fin  VARCHAR2(1000) := 'finance team parameter';

TYPE v_cnt_rec
IS
  TABLE OF integer;
  t_cnt_rec v_cnt_rec;

TYPE v_cont_msg
IS
  TABLE OF VARCHAR2(240);
  t_cont_msg v_cont_msg;
TYPE v_app_name
IS
  TABLE OF AME_CALLING_APPS_TL.Application_name%TYPE;
  t_app_name v_app_name;
TYPE v_rule_type_desc
IS
  TABLE OF fnd_lookups.meaning%TYPE;
  t_rule_type_desc v_rule_type_desc;
TYPE v_rule_id
IS
  TABLE OF AME_RULES_V.rule_id%TYPE;
  t_rule_id v_rule_id;
TYPE v_rule_name
IS
  TABLE OF ame_rules_tl.description%TYPE;
  t_rule_name v_rule_name;
TYPE v_usage_start_date
IS
  TABLE OF AME_RULES_V.USAGE_START_DATE%TYPE;
  t_usage_start_date v_usage_start_date;
TYPE v_usage_end_date
IS
  TABLE OF AME_RULES_V.USAGE_END_DATE%TYPE;
  t_usage_end_date v_usage_end_date;
TYPE v_condition_id
IS
  TABLE OF ame_condition_usages.condition_id%TYPE;
  t_condition_id v_condition_id;
TYPE v_condition
IS
  TABLE OF VARCHAR2(1000);
  t_condition v_condition;
TYPE v_action_type
IS
  TABLE OF ame_action_types_vl.name%TYPE;
  t_action_type v_action_type;
TYPE v_approver_group
IS
  TABLE OF VARCHAR2(1000);
  t_approver_group v_approver_group;
TYPE v_approver_name
IS
  TABLE OF FND_USER.DESCRIPTION%TYPE;
  t_approver_name v_approver_name;
TYPE v_Approver_User
IS
  TABLE OF AME_APPROVAL_GROUP_MEMBERS.PARAMETER%TYPE;
  t_Approver_User v_Approver_User;
TYPE v_ORDER_NUMBER
IS
  TABLE OF AME_APPROVAL_GROUP_MEMBERS.ORDER_NUMBER%TYPE;
  t_ORDER_NUMBER v_ORDER_NUMBER;
  CURSOR FIND_USER
  IS
    SELECT Fnd_User.User_Name,
      Per_All_People_F.Full_Name,
      Per_Jobs_Tl.Name
    FROM Fnd_User,
      Per_All_People_F,
      Per_Assignments_F,
      PER_JOBS_TL
    WHERE 0                        =0
    AND Fnd_User.Employee_Id       = Per_All_People_F.Person_Id
    AND Per_All_People_F.Person_Id = Per_Assignments_F.Person_Id
    AND Per_Assignments_F.Job_Id   = Per_Jobs_Tl.Job_Id
    AND rownum                     =1
    AND Fnd_User.User_Name = p_username
    AND Description                            IS NOT NULL
    AND Per_Assignments_F.Effective_Start_Date <= Sysdate
    AND (Per_Assignments_F.Effective_End_Date  IS NULL
    OR Per_Assignments_F.Effective_End_Date     > sysdate ) ;
    
    FND FIND_USER%ROWTYPE;
    
BEGIN

    fnd_file.put_line(fnd_file.output,'TAC AME APPROVAL REPORT');
    --dbms_output.put_line('TAC AME APPROVAL REPORT');
    OPEN FIND_USER;
    LOOP
      FETCH FIND_USER INTO FND;
      EXIT WHEN FIND_USER%NOTFOUND;
      fnd_file.put_line(fnd_file.output,'USER_NAME: ' || FND.USER_NAME);
      fnd_file.put_line(fnd_file.output,'FULL_NAME: ' || FND.Full_Name);
      fnd_file.put_line(fnd_file.output,'POSITION LEVEL: ' || FND.Name);
      --dbms_output.put_line('USER_NAME: ' || FND.USER_NAME);
      --dbms_output.put_line('FULL_NAME: ' || FND.Full_Name);
      --dbms_output.put_line('POSITION LEVEL: ' || FND.Name);
  SELECT rownum row_cnt,
    Contact_person,
    Application_name,
    RULE_TYPE_DESC,
    rule_id,
    rule_name,
    USAGE_START_DATE,
    USAGE_END_DATE,
    condition_id,
    condition,
    action_type,
    approver_group,
    APPROVER_NAME,
    APPROVER_USER,
    ORDER_NUMBER 
    BULK COLLECT INTO 
    t_cnt_rec,
    t_cont_msg,
    t_app_name,
    t_rule_type_desc,
    t_rule_id,
    t_rule_name,
    t_usage_start_date,
    t_usage_end_date,
    t_condition_id,
    t_condition,
    t_action_type,
    t_approver_group,
    t_approver_name,
    t_Approver_User,
    t_ORDER_NUMBER
  FROM
    (SELECT
      CASE
        WHEN ame_utility_pkg.get_condition_description (acu.condition_id) LIKE '%PROJECT%'
        THEN NVL(p_msg_proj,'contact costing team')
        ELSE NVL(p_msg_fin,'contact financial team')
      END Contact_person,
      APPT.Application_name,
      fl.meaning AS RULE_TYPE_DESC,
      ar.rule_id,
      REPLACE( REPLACE(art.description,CHR(10),''), CHR(13), '') rule_name,
      ar.USAGE_START_DATE,
      ar.USAGE_END_DATE,
      acu.condition_id,
      ame_utility_pkg.get_condition_description (acu.condition_id) condition,
      aty.name action_type,
      ame_utility_pkg.get_action_description (ameactionusageeo.action_id) AS approver_group,
      FU.DESCRIPTION APPROVER_NAME ,
      AGM.PARAMETER APPROVER_USER,
      AGM.ORDER_NUMBER
      --,AGM.*
    FROM AME_RULES_V ar,
      ame_rules_tl art,
      ame_condition_usages acu,
      ame_action_usages ameactionusageeo,
      ame_actions_vl act,
      ame_action_types_vl aty,
      (SELECT *
      FROM ame_action_type_usages
      WHERE rule_type <> 2
      AND SYSDATE BETWEEN start_date AND NVL (end_date - (1 / 86400), SYSDATE)
      ) atu,
      AME_APPROVAL_GROUPS_TL AG,
      AME_APPROVAL_GROUP_MEMBERS AGM,
      AME_APPROVAL_GROUP_CONFIG AGC,
      fnd_lookups fl ,
      FND_USER FU,
      AME_CALLING_APPS_TL APPT
    WHERE ar.rule_id = art.rule_id
    AND art.language = 'US'
    AND TRUNC (SYSDATE) BETWEEN AR.USAGE_START_DATE AND NVL ( AR.USAGE_END_DATE, TO_DATE ('31-DEC-4712', 'DD-MON-YYYY'))
      --AND UPPER (art.description) LIKE '%VARAWUT%'
    AND acu.rule_id = ar.rule_id
    AND TRUNC (SYSDATE) BETWEEN acu.start_date AND NVL ( acu.end_date, TO_DATE ('31-DEC-4712', 'DD-MON-YYYY'))
    AND ( (SYSDATE BETWEEN ameactionusageeo.start_date AND NVL (ameactionusageeo.end_date - (1 / 86400), SYSDATE))
    OR (SYSDATE                     < ameactionusageeo.start_date
    AND ameactionusageeo.start_date < NVL (ameactionusageeo.end_date, ameactionusageeo.start_date + (1 / 86400))))
    AND SYSDATE BETWEEN act.start_date AND NVL (act.end_date                                      - (1 / 86400), SYSDATE)
    AND SYSDATE BETWEEN aty.start_date AND NVL (aty.end_date                                      - (1 / 86400), SYSDATE)
    AND aty.action_type_id            = atu.action_type_id
    AND act.action_id                 = ameactionusageeo.action_id
    AND act.action_type_id            = aty.action_type_id
    AND AG.APPROVAL_GROUP_ID          = AGM.APPROVAL_GROUP_ID
    AND TO_CHAR(AG.APPROVAL_GROUP_ID) = ACT.PARAMETER(+)
    AND fl.lookup_type                = 'AME_RULE_TYPE'
    AND fl.enabled_flag               = 'Y'
    AND fl.lookup_code                = AR.RULE_TYPE
    AND ameactionusageeo.rule_id      = ar.rule_id
    AND AG.APPROVAL_GROUP_ID          = AGC.APPROVAL_GROUP_ID
    AND ar.AME_APPLICATION_ID         = AGC.APPLICATION_ID
    AND AGC.APPLICATION_ID            = APPT.APPLICATION_ID
    AND AGC.START_DATE               <= sysdate
    AND (AGC.END_DATE                 > sysdate
    OR AGC.END_DATE                  IS NULL)
    AND AGM.PARAMETER                 = FU.USER_NAME(+)
    AND act.action_type_id           IN (10013,10006,10007)
    AND AGM.PARAMETER                 = FND.USER_NAME
    UNION
    SELECT
      CASE
        WHEN ame_utility_pkg.get_condition_description (acu.condition_id) LIKE '%PROJECT%'
        THEN NVL(p_msg_proj,'contact costing team')
        ELSE NVL(p_msg_fin,'contact financial team')
      END Contact_person,
      APPT.Application_name,
      fl.meaning AS RULE_TYPE_DESC,
      ar.rule_id,
      REPLACE( REPLACE(art.description,CHR(10),''), CHR(13), '') rule_name,
      ar.USAGE_START_DATE,
      ar.USAGE_END_DATE,
      acu.condition_id,
      ame_utility_pkg.get_condition_description (acu.condition_id) condition,
      aty.name action_type,
      ame_utility_pkg.get_action_description (ameactionusageeo.action_id) AS approver_group,
      FU.DESCRIPTION APPROVER_NAME ,
      AGM.PARAMETER APPROVER_USER,
      AGM.ORDER_NUMBER
      --,AGM.*
    FROM AME_RULES_V ar,
      ame_rules_tl art,
      ame_condition_usages acu,
      ame_action_usages ameactionusageeo,
      ame_actions_vl act,
      ame_action_types_vl aty,
      (SELECT *
      FROM ame_action_type_usages
      WHERE rule_type <> 2
      AND SYSDATE BETWEEN start_date AND NVL (end_date - (1 / 86400), SYSDATE)
      ) atu,
      AME_APPROVAL_GROUPS_TL AG,
      AME_APPROVAL_GROUP_MEMBERS AGM,
      AME_APPROVAL_GROUP_CONFIG AGC,
      fnd_lookups fl ,
      FND_USER FU,
      AME_CALLING_APPS_TL APPT
    WHERE ar.rule_id = art.rule_id
    AND art.language = 'US'
    AND TRUNC (SYSDATE) BETWEEN AR.USAGE_START_DATE AND NVL ( AR.USAGE_END_DATE, TO_DATE ('31-DEC-4712', 'DD-MON-YYYY'))
      --AND UPPER (art.description) LIKE '%VARAWUT%'
    AND acu.rule_id = ar.rule_id
    AND TRUNC (SYSDATE) BETWEEN acu.start_date AND NVL ( acu.end_date, TO_DATE ('31-DEC-4712', 'DD-MON-YYYY'))
    AND ( (SYSDATE BETWEEN ameactionusageeo.start_date AND NVL (ameactionusageeo.end_date - (1 / 86400), SYSDATE))
    OR (SYSDATE                     < ameactionusageeo.start_date
    AND ameactionusageeo.start_date < NVL (ameactionusageeo.end_date, ameactionusageeo.start_date + (1 / 86400))))
    AND SYSDATE BETWEEN act.start_date AND NVL (act.end_date                                      - (1 / 86400), SYSDATE)
    AND SYSDATE BETWEEN aty.start_date AND NVL (aty.end_date                                      - (1 / 86400), SYSDATE)
    AND aty.action_type_id            = atu.action_type_id
    AND act.action_id                 = ameactionusageeo.action_id
    AND act.action_type_id            = aty.action_type_id
    AND AG.APPROVAL_GROUP_ID          = AGM.APPROVAL_GROUP_ID
    AND TO_CHAR(AG.APPROVAL_GROUP_ID) = ACT.PARAMETER(+)
    AND fl.lookup_type                = 'AME_RULE_TYPE'
    AND fl.enabled_flag               = 'Y'
    AND fl.lookup_code                = AR.RULE_TYPE
    AND ameactionusageeo.rule_id      = ar.rule_id
    AND AG.APPROVAL_GROUP_ID          = AGC.APPROVAL_GROUP_ID
    AND ar.AME_APPLICATION_ID         = AGC.APPLICATION_ID
    AND AGC.APPLICATION_ID            = APPT.APPLICATION_ID
    AND AGC.START_DATE               <= sysdate
    AND (AGC.END_DATE                 > sysdate
    OR AGC.END_DATE                  IS NULL)
    AND AGM.PARAMETER                 = FU.USER_NAME(+)
    AND act.action_type_id           IN (10013,10006,10007)
    AND ame_utility_pkg.get_condition_description (acu.condition_id) LIKE '%'
      || FND.Full_Name
      || '%'
    )
  ORDER BY 1,
    Application_name,
    RULE_TYPE_DESC,
    rule_name ;  
    END LOOP;
    CLOSE FIND_USER;    
    fnd_file.put_line(fnd_file.output,'NO#|CONTACT_PERSON|RULE_NAME|APPLICATION_NAME|RULE_TYPE_DESC|CONDITION|APPROVER_GROUP|APPROVER_NAME|APPROVER_USER|USAGE_START_DATE|USAGE_END_DATE|ORDER_NUMBER|RULE_ID|CONDITION_ID|ACTION_TYPE');
    --dbms_output.put_line('NO#|CONTACT_PERSON|APPLICATION_NAME|RULE_TYPE_DESC|RULE_ID|RULE_NAME|USAGE_START_DATE|USAGE_END_DATE|CONDITION_ID|CONDITION|ACTION_TYPE|APPROVER_GROUP|APPROVER_NAME|APPROVER_USER|ORDER_NUMBER');
    FOR indx IN 1 .. t_cnt_rec.COUNT
     LOOP
    fnd_file.put_line(fnd_file.output,t_cnt_rec(indx) || '|' || t_cont_msg(indx) ||  '|' || t_rule_name(indx) || '|' || t_app_name(indx)|| '|' || t_rule_type_desc(indx) || '|' || t_condition(indx) || '|' || t_approver_group(indx) || '|' || t_approver_name(indx)  || '|' || t_Approver_User(indx) || '|' || t_usage_start_date(indx) || '|' || t_usage_end_date(indx) || '|' || t_ORDER_NUMBER(indx) || '|' || t_rule_id(indx) || '|' || t_condition_id(indx) || '|' || t_action_type(indx));
    --dbms_output.put_line(t_cnt_rec(indx) || '|' || t_cont_msg(indx) || '|' || t_app_name(indx)|| '|' || t_rule_type_desc(indx) || '|' || t_rule_id(indx) || '|' || t_rule_name(indx) || '|' || t_usage_start_date(indx) || '|' || t_usage_end_date(indx)  || '|' || t_condition_id(indx) || '|' || t_condition(indx) || '|' || t_action_type(indx) || '|' || t_approver_group(indx) || '|' || t_approver_name(indx) || '|' || t_Approver_User(indx) || '|' || t_ORDER_NUMBER(indx));
    END LOOP;
END;