SELECT cr.set_of_books_id c_sob_id ,
  hp.party_name c_cust_name
  /*  ,DECODE( hcsu.cust_acct_site_id, NULL, NULL,
  ARH_ADDR_PKG.ARXTW_FORMAT_ADDRESS(hl.address_style
  , hl.address1
  , hl.address2
  , hl.address3
  , hl.address4
  , hl.province
  , hl.postal_code
  , hl.county
  , NULL
  , NULL
  , NULL))  C_cust_addr */
  ,
  DECODE(hcsu.cust_acct_site_id ,NULL ,NULL ,hl.address_style
  || ' '
  || hl.address1
  || ' '
  || hl.address2
  || ' '
  || hl.address3
  || ' '
  || hl.address4
  || ' '
  || hl.province
  || ' '
  || hl.postal_code
  || ' '
  || hl.county) c_cust_addr ,
  arm.attribute1 c_tax_inv_flag ,
  cr.doc_sequence_value c_doc_num ,
  TRUNC(cr.receipt_date) c_receipt_date ,
  arm.attribute2 c_receipt_type
  --,NVL(abb.bank_name,abb2.bank_name)                   C_cust_bank_name
  --,NVL(abb.bank_branch_name,abb2.bank_branch_name)                 C_cust_bank_branch
  ,
  cr.attribute1 c_cust_bank_name ,
  cr.attribute2 c_cust_bank_branch ,
  cr.receipt_number c_cheque_num ,
  aps.due_date c_maturity_date ,
  arm.attribute3 c_region_code ,
  cr.cash_receipt_id c_cash_receipt_id ,
  cr.comments c_detail ,
  cr.attribute3 c_print_count ,
  arm.attribute3 c_division ,
  cr.currency_code c_currency_code ,
  hp.tax_reference tax_registration_number ,
  hcas.attribute7 branch_code,
  cr.*
FROM hz_cust_accounts hca ,
  hz_parties hp ,
  HZ_CUST_SITE_USES_ALL hcsu ,
  HZ_CUST_ACCT_SITES_ALL hcas ,
  hz_party_sites hps ,
  hz_locations hl ,
  fnd_territories_vl ft ,
  ap_bank_branches abb ,
  ap_bank_branches abb2 ,
  AP_BANK_ACCOUNTS_ALL aba ,
  ar_receipt_methods arm ,
  ar_payment_schedules aps ,
  AR_CASH_RECEIPTS_ALL cr
WHERE 0=0
--and cr.doc_sequence_value    IS NOT NULL
AND cr.pay_from_customer        = hca.cust_account_id(+)
AND hca.party_id                = hp.party_id(+)
AND cr.customer_site_use_id     = hcsu.site_use_id(+)
AND hcsu.cust_acct_site_id      = hcas.cust_acct_site_id(+)
AND hcas.party_site_id          = hps.party_site_id
AND hps.location_id             = hl.location_id(+)
AND hl.country                  = ft.territory_code(+)
AND cr.receipt_method_id        = arm.receipt_method_id
AND cr.customer_bank_account_id = aba.bank_account_id(+)
AND aba.bank_branch_id          = abb.bank_branch_id(+)
AND cr.customer_bank_branch_id  = abb2.bank_branch_id(+)
AND cr.cash_receipt_id          = aps.cash_receipt_id(+)
  ------Parameter--------
--AND TRUNC(cr.receipt_date) BETWEEN TRUNC(NVL(:p_trx_date_from, cr.receipt_date)) AND TRUNC(NVL(:p_trx_date_to, cr.receipt_date))
--AND TRUNC(aps.gl_date) BETWEEN TRUNC(NVL(:p_gl_date_from, aps.gl_date)) AND TRUNC(NVL(:p_gl_date_to, aps.gl_date))
AND NVL(cr.doc_sequence_value, -9999999) BETWEEN NVL(:p_doc_num_from, NVL(cr.doc_sequence_value, -9999999)) AND NVL(:p_doc_num_to, NVL(cr.doc_sequence_value, -9999999))
AND NVL(cr.receipt_number, '###') BETWEEN NVL(:p_receipt_num_from, NVL(cr.receipt_number, '###')) AND NVL(:p_receipt_num_to, NVL(cr.receipt_number, '###'))
--AND NVL(hca.account_number, '###') BETWEEN NVL(:p_cust_num_from, NVL(hca.account_number, '###')) AND NVL(:p_cust_num_to, NVL(hca.account_number, '###'))
AND NVL(hp.party_name, '###') BETWEEN NVL(:p_cust_name_from, NVL(hp.party_name, '###')) AND NVL(:p_cust_name_to, NVL(hp.party_name, '###'))
AND NVL(arm.name, '###') BETWEEN NVL(:p_payment_method_from, NVL(arm.name, '###')) AND NVL(:p_payment_method_to, NVL(arm.name, '###'))
AND NVL(arm.attribute2, '###') = NVL(:p_receipt_type, NVL(arm.attribute2, '###'))
ORDER BY cr.cash_receipt_id;

-- AMT
SELECT *					
		FROM xla_ar_rec_ael_sl_v	xar
		WHERE 0=0    
    --and xar.application_id 	=	:p_ar_application_id 
		--AND xar.set_of_books_id 	=	:c_sob_id 
		AND xar.trx_hdr_table 		=	'CR' 
		AND xar.acct_line_type		=	'REC'
		AND xar.trx_hdr_id				=	:c_cash_receipt_id;
    
	--adjust
		SELECT */*decode(:c_currency_code,'THB',SUM(NVL(xaa.accounted_cr,0)-NVL(xaa.accounted_dr,0))
	  																		,SUM(NVL(xaa.entered_cr,0)-NVL(xaa.entered_dr,0)))	*/
		FROM	AR.AR_ADJUSTMENTS_ALL			adj
				,xla_ar_adj_ael_sl_v	xaa
		WHERE 1=1	
    AND adj.ASSOCIATED_CASH_RECEIPT_ID	=	:c_cash_receipt_id
		AND adj.adjustment_id									=	xaa.trx_hdr_id
		AND adj.customer_trx_id								=	xaa.applied_to_trx_hdr_id
		AND adj.status	NOT IN ('R','U')
		--AND xaa.application_id 								= :p_ar_application_id 
		AND xaa.set_of_books_id 							=	:c_sob_id 
		AND xaa.trx_hdr_table 								=	'ADJ' 
		AND xaa.acct_line_type								=	'REC'
    --AND ADJ.AMOUNT in (7230.24,506.12,-506.12,-7230.24)
    
    ;
    
    
    
    select adj.ASSOCIATED_CASH_RECEIPT_ID,adj.* from AR_ADJUSTMENTS_ALL adj WHERE 1=1	
    AND ADJ.ADJUSTMENT_ID = 196777;
    
    select * from AR_CASH_RECEIPTS_ALL where CASH_RECEIPT_ID = 194275;
    
    select ADJUSTMENT_ID_LAST,AR_PAYMENT_SCHEDULES_ALL.* from AR_PAYMENT_SCHEDULES_ALL where ADJUSTMENT_ID_LAST is not null;